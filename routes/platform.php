<?php

use App\Orchid\Screens\Examples\ExampleCardsScreen;
use App\Orchid\Screens\Examples\ExampleChartsScreen;
use App\Orchid\Screens\Examples\ExampleFieldsAdvancedScreen;
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Examples\ExampleTextEditorsScreen;
use App\Orchid\Screens\MainScreen;
use App\Orchid\Screens\Payment\PaymentEditScreen;
use App\Orchid\Screens\Payment\PaymentListScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\Task\TaskAvailableListScreen;
use App\Orchid\Screens\Task\TaskEditScreen;
use App\Orchid\Screens\Task\TaskListScreen;
use App\Orchid\Screens\Task\TaskViewScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use App\Orchid\Screens\UserTask\UserTaskEditScreen;
use App\Orchid\Screens\UserTask\UserTaskListScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', MainScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('User'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{role}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Example...
Route::screen('example', ExampleScreen::class)
    ->name('platform.example')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Example screen');
    });

Route::screen('example-fields', ExampleFieldsScreen::class)->name('platform.example.fields');
Route::screen('example-layouts', ExampleLayoutsScreen::class)->name('platform.example.layouts');
Route::screen('example-charts', ExampleChartsScreen::class)->name('platform.example.charts');
Route::screen('example-editors', ExampleTextEditorsScreen::class)->name('platform.example.editors');
Route::screen('example-cards', ExampleCardsScreen::class)->name('platform.example.cards');
Route::screen('example-advanced', ExampleFieldsAdvancedScreen::class)->name('platform.example.advanced');

Route::screen('tasks/create', TaskEditScreen::class)->name('platform.tasks.create');
Route::screen('tasks/{task}/edit', TaskEditScreen::class)->name('platform.tasks.edit');
Route::screen('tasks/list', TaskListScreen::class)->name('platform.tasks.list');

Route::screen('tasks/{task}/view', TaskViewScreen::class)->name('platform.tasks.view');
Route::screen('tasks/available', TaskAvailableListScreen::class)->name('platform.tasks.available.list');

Route::screen('user_tasks/list', UserTaskListScreen::class)->name('platform.user_tasks.list');
Route::screen('user_tasks/{user_task}/edit', UserTaskEditScreen::class)->name('platform.user_tasks.edit');
Route::screen('user_tasks/admin/list', App\Orchid\Screens\UserTask\admin\UserTaskListScreen::class)->name('platform.user_tasks.admin.list');
Route::screen('user_tasks/admin/{user_task}/edit', App\Orchid\Screens\UserTask\admin\UserTaskEditScreen::class)->name('platform.user_tasks.admin.edit');

Route::screen('payments/list', PaymentListScreen::class)->name('platform.payments.list');
Route::screen('payments/create', PaymentEditScreen::class)->name('platform.payments.create');
Route::screen('payments/{payment}/edit', PaymentEditScreen::class)->name('platform.payments.edit');
Route::screen('transactions/list', App\Orchid\Screens\Transaction\TransactionListScreen::class)->name('platform.transactions.list');
Route::screen('transactions/create', App\Orchid\Screens\Transaction\TransactionEditScreen::class)->name('platform.transactions.create');
Route::screen('transactions/{transaction}/edit', App\Orchid\Screens\Transaction\TransactionEditScreen::class)->name('platform.transactions.edit');
Route::screen('transfers/list', App\Orchid\Screens\Transfer\TransferListScreen::class)->name('platform.transfers.list');
