<?php

use Barryvdh\Debugbar\Facades\Debugbar;
use engine\app\models\Engine;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Debugbar::startMeasure('Routes');

Engine::getRequestedDomainEloquent()?->registerRoutes(withoutPrefix: true);

Debugbar::stopMeasure('Routes');
