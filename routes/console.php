<?php

use App\Services\RegisterSchedules;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote')->hourly();

/* инициализируем расписания */
(new RegisterSchedules)->register();
