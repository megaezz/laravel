<?php

namespace App\Services\Engine;

use App\Models\Config;
use App\Models\Domain;

class DomainErrors
{
    public function __invoke(Domain $domain)
    {

        $errors = collect();

        if (! $domain->mirror_of) {
            // если это сайт с фильмами - то проверяем всякое
            if ($domain->cinemaDomain and ($domain->cinemaDomain->include_serials or $domain->cinemaDomain->include_movies)) {
                if (! $domain->favicon_src) {
                    $errors[] = 'Нет иконки';
                }
                // if (!$domain->logo_src) {
                //     $errors[] = 'Нет логотипа';
                // }
                if (! $domain->domain_for_client->parent_domain_or_self->recaptcha_key_name) {
                    $errors[] = "{$domain->domain_for_client->parent_domain_or_self->domain} &mdash; нет рекапчи";
                }
                /* если активен любой рекламный блок в футере */
                if ($domain->blocks->where('block', 'footer')->where('active', true)->count()) {
                    /* если активен блок видеоролла, проверяем на всех ли родительских доменах указан videoroll_id */
                    if ($domain->blocks->where('template', 'promo/videoroll.net/script_vpaut.html')->where('active', true)->count()) {

                        if (! $domain->domain_for_client->parent_domain_or_self->videoroll_id) {
                            $errors[] = "{$domain->domain_for_client->parent_domain_or_self->domain} &mdash; Стоит видеоролл, но нет videoroll_id ({$domain->domain_for_client->domain})";
                        }
                    }
                    /* если менее 30к трафа, то нужен ли видео блок? */
                    if ($domain->metrika?->visits and $domain->metrika?->visits < 10000) {
                        if ($domain->blocks->whereIn('template', ['promo/videoroll.net/script_vpaut.html', 'promo/movieads.ru/script.html'])->where('active', true)->count()) {
                            $errors[] = 'Менее 10к трафика в месяц ('.ceil($domain->metrika->visits / 1000).'к), но стоит видео блок';
                        }
                    }
                } else {
                    /* если более 30к трафа, то вероятно нужен рекламный блок */
                    // if ($domain->metrika?->visits and $domain->metrika?->visits > 10000) {
                    //     $errors[] = 'Более 10к трафика в месяц (' . ceil($domain->metrika->visits / 1000) . 'к), но нет стикера, либо видеоблока, ' . ($domain->domain_for_client->parent_domain_or_self->videoroll_id ? 'videoroll_id есть' : 'videoroll_id нет');
                    // }
                }
                /* проверяем на аллоху */
                if ($domain->metrika?->visits > 5000) {
                    if (! $domain->domain_for_client->parent_domain_or_self->added_to_alloha) {
                        $traffic = ceil($domain->metrika->visits / 1000);
                        $errors[] = "Более 5к трафика в месяц ({$traffic}к), но не добавлен в аллоху ({$domain->domain_for_client->parent_domain_or_self->domain})";
                    }
                }
            }

            if (in_array($domain->type, ['cinema', 'videohub'])) {
                if (! $domain->metrika) {
                    $errors[] = 'Не указана метрика';
                }
                if (! $domain->site_name) {
                    $errors[] = 'Отсутствует site_name';
                }
                if (! $domain->yandex_token_email) {
                    $errors[] = 'Не указан Яндекс аккаунт для метрики и вебмастера';
                }
            }

            if (! $domain->redirect_to and $domain->client_redirect_to) {
                $errors[] = "Указан клиентский редирект на {$domain->client_domain->domain}, но не указан основной редирект";
            }
            if ($domain->client_redirect_to && $domain->client_redirect) {
                $errors[] = 'Указан клиентский редирект, но не отключен редирект клиента на основу';
            }
            if ($domain->type != 'trash' and ! $domain->on) {
                $errors[] = 'Сайт выключен';
            }
            if ($domain->yandex_token_email and $domain->metrika->yandex_token_email and $domain->yandex_token_email == $domain->metrika->yandex_token_email) {
                $errors[] = 'Указан одинаковый яндекс токен для сайта и для метрики. Можно убрать токен метрики.';
            }
            if ($domain->yandexRedirectTo and $domain->access_scheme != 'v3') {
                $errors[] = 'Указан отдельный домен для Яндекса, но схема доступа не v3';
            }
            if ($domain->access_scheme == 'v3' and $domain->redirect_to == $domain->yandex_redirect_to) {
                // $errors[] = 'Схема доступа v3, но поисковые домены одинаковые';
            }
            /* если есть метрика, но нет data->query, то уведомляем */
            if ($domain->metrika and ! isset($domain->metrika->data->query)) {
                $errors[] = 'Нет данных Яндекс метрики';
            }
            foreach ($domain->related_parent_domains as $parentDomain) {
                if ($parentDomain->renewal_error) {
                    $errors[] = "{$parentDomain->domain}".
                        ($domain->actual_mirrors->pluck('parent_domain_or_self.domain')->unique()->contains($parentDomain->domain) ? ' актуальный' : '').
                        " {$parentDomain->renewal_error} ({$parentDomain->whois->registrar})";
                }
                if ($parentDomain->renewed and ! $parentDomain->whois?->expirationDate) {
                    $errors[] = "{$parentDomain->domain} &mdash; нет данных whois";
                }
            }
            /* если нет ни одного продленного родительского домена, не считая маскировочных, то сообщаем об этом */
            // если наш домен маскировочный, то не исключаем маскировочные домены из проверки, это нужно чтобы в trash доменах не отображалась ошибка нет продленных доменов на маскировочных доменах
            if (! $domain->related_parent_domains->filter(fn ($parent) => ($domain->is_masking_domain ? true : ! $parent->is_masking_domain) and $parent->renewed)->count()) {
                $errors[] = 'Ни одного продленного домена';
            } else {
                /* если есть не продленные актуальные домены, то сообщаем об этом, заодно показываем какие есть продленные неактуальные */
                /* при таком подходе не будет куча сообщений о непродленных маскировочных доменах, но в случае если актуальный маскировочный не продлен, то мы получим сообщение об этом */
                if ($domain->actual_mirrors->where('parent_domain_or_self.renewed', false)->count()) {
                    if ($domain->related_parent_domains->where('renewed', true)->count()) {
                        $errors[] = 'Один из актуальных доменов не продлен ('.$domain->actual_mirrors->where('parent_domain_or_self.renewed', false)->pluck('domain')->implode(', ').'), но есть продленные зеркала: '.$domain->related_parent_domains->where('is_masking_domain', false)->where('renewed', true)->pluck('domain')->implode(', ');
                    } else {
                        $errors[] = 'Один из актуальных доменов не продлен: '.$domain->actual_mirrors->where('parent_domain_or_self.renewed', false)->pluck('domain')->implode(', ');
                    }
                }
            }
        }

        /* если домен мониторится и мониторинг false то сообщаем */
        if ($domain->is_monitored) {
            if (! $domain->monitoring) {
                $errors[] = 'Ошибка мониторинга '.collect(Config::cached()->monitored_countries)->filter(fn ($country) => ! $domain->countryMonitoring($country))->implode(', ');
            }
            foreach (Config::cached()->monitored_countries as $country) {
                if ($domain->knownCountryBlock($country) and $domain->countryMonitoring($country)) {
                    $errors[] = "Отмечен заблокированным, но мониторится доступным по {$country}";
                }
            }
        }

        return $errors;
    }
}
