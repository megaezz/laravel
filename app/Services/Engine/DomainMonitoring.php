<?php

namespace App\Services\Engine;

use App\Models\Config;
use App\Models\Domain;
use App\Models\Proxy;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;

class DomainMonitoring
{
    public function __invoke(bool $failed = false, bool $onlyActualMirrors = false, ?string $domain = null)
    {

        $countries = collect();

        foreach (Config::cached()->monitored_countries as $countryCode) {
            $proxy = Proxy::where('active', true)->where('country', $countryCode)->inRandomOrder();
            if ($countryCode == 'RU') {
                $proxy->where('rkn', true);
            }
            $countries[] = (object) [
                'code' => $countryCode,
                'proxy' => $proxy->firstOrFail()->proxy,
            ];
        }

        dump($countries->map(fn ($country) => "{$country->code}: {$country->proxy}")->toArray());

        $domains = Domain::with('parentDomain')->monitored()->orderBy('monitored_at')->limit(10);

        if ($domain) {
            $domains->where('domain', $domain);
        }

        if ($failed) {
            $domains->where('monitoring', false);
        }

        if ($onlyActualMirrors) {
            // получаем все актуальные домены
            $actualMirrors = Domain::query()
                ->with('redirectTo')
                ->with('yandexRedirectTo')
                ->with('clientRedirectTo')
                ->whereIn('type', ['cinema', 'copy', 'videohub'])
                ->whereOn(true)
                ->get()
                ->append('actualMirrors')
                ->flatMap(function ($domain) {
                    return $domain->actualMirrors->pluck('domain');
                });

            // фильтруем выборку по актуальным доменам без известной блокировки
            $domains
                ->whereIn('domain', $actualMirrors)
                ->where('known_ru_block', false);
        }

        $domains = $domains->get();

        $client = new Client([
            'timeout' => 5,
            'headers' => [
                'User-Agent' => 'awmzone',
            ],
        ]);

        $promises = [];

        $results = collect();

        foreach ($domains as $domain) {

            foreach ($countries as $country) {

                $url = "https://{$domain->domain}/monitoring";
                // не понял как https сделать
                // dd($domain->toRoute->absolute()->monitoring());

                $promises[] = $client->getAsync($url, ['proxy' => $country->proxy])->then(

                    function ($response) use ($domain, $country, $results) {

                        $data = json_decode($response->getBody());

                        $domain->countryMonitoring($country->code, false);

                        if (isset($data->up) and $data->up === true) {

                            $domain->countryMonitoring($country->code, true);
                        }

                        $domain->monitored_at = now();

                        $results[] = (object) [
                            'domain' => $domain,
                            'country' => $country,
                            'response' => $response,
                        ];
                    },

                    function ($reason) use ($domain, $country, $results) {

                        $domain->countryMonitoring($country->code, false);
                        $domain->monitored_at = now();

                        $results[] = (object) [
                            'domain' => $domain,
                            'country' => $country,
                            'reason' => $reason,
                        ];
                    },

                );
            }

        }

        Utils::settle($promises)->wait();

        // считаем общий monitoring и сохраняем все сразу один раз только здесь
        $domains->each(function ($domain) use ($countries) {
            $domain->monitoring = $countries->every(function ($country) use ($domain) {
                // если проставлена известная блокировка, то инвертируем результат мониторинга и получается, что если сайт недоступен то все ок, а если доступен то это проблема мониторинга, ведь должен быть недоступен
                return $domain->knownCountryBlock($country->code) ? ! $domain->countryMonitoring($country->code) : $domain->countryMonitoring($country->code);
            });
            $domain->save();
        });

        // оставляем только указанны поля в коллекции обработанных доменов и выводим их
        // dump($domains->map(function($domain) use ($countries) {
        //     return $domain->only(array_merge(
        //         ['domain', 'monitoring'],
        //         $countries->map(fn($country) => "monitoring_" . strtolower($country->code))->toArray()
        //     ));
        // })->toArray());

        return $results;
    }
}
