<?php

namespace App\Services\Engine;

use App\Models\Domain;
use cinema\app\models\eloquent\DomainMovie;
use cinema\app\models\eloquent\Group;
use cinema\app\models\eloquent\MovieEpisode;
use cinema\app\models\eloquent\Tag;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
Возвращает собранную ссылку по имени маршрута.
Чтобы получить готовую ссылку на роут, нужно обратиться к методу ->{имя роута} и передать какой-нибудь объект или массив или ничего не передавать.
Если нужен абсолютный путь, то вторым аргументом true, либо ->{имя роута}Absolute
! Если регать сразу роуты с префиксом и без префикса, то лара удаляет роуты, из-за того что они дублируют друг друга
*/
class Routes
{
    public Domain $domain;

    public bool $absolute;

    public function __construct()
    {
        /* определяем значения по умолчанию, чтобы далее в __call не было ошибки доступа к не объявленным свойствам */
        $this->setDefaults();
    }

    public function __call(string $method, $arguments)
    {

        /* запоминаем текущие настройки absolute, потому что затем они сбрасываются */
        $absolute = $this->absolute;

        /* сбрасываем значения на дефолтные перед каждым выполнением */
        $this->setDefaults();

        /* сначала регистрируем роуты, повторных регистраций быть не должно, т.к. это предусмотрено в RegisterRoutesForDomainService */
        $this->domain->registerRoutes();

        $routeName = "{$this->domain->domain}_".Str::snake($method);

        /* если такого роута не существует, но есть метод для мегавеб роута, то возвращаем его */
        if (! Route::has($routeName)) {
            if (method_exists($this, "{$method}Megaweb")) {
                /* предусматриваем абсолютный путь (протокол берем из primary) */
                return ($absolute ? "{$this->domain->primary_domain->protocol}://{$this->domain->domain}" : null).call_user_func_array([$this, "{$method}Megaweb"], $arguments);
            } else {
                throw new \Exception("Роут {$routeName} не существует и метод {$method}Megaweb не существует");
            }
        }

        /* если есть приватный метод для формирования переменных для роута, то используем его, если нет, то просто вызываем роут в который передаем аргументы */
        if (method_exists($this, $method)) {
            return route($routeName, call_user_func_array([$this, $method], $arguments), $absolute);
        } else {
            /* пока непонятно, нужна ли такая логика или нет, пока пускай срабатывает исключение, если что доделать */
            // throw new \Exception("Метод {$method} для формирования переменных для роута не существует в Routes.");
            /* если есть метод для формирования роутов, то туда аргументы передаются через запятую, если нет, то ожидаем, что будет только 1 аргумент - массив с переменными для роута, поэтому читаем $arguments[0] */
            /* подтягиваем slug из роута для домена, если такой есть (может быть запрошен не роут для домена, там вообще slug нет) */
            $arguments[0]['slug'] = $this->domain->primary_domain->routes->where('name', Str::snake($method))->first()?->slug;

            return route($routeName, $arguments[0], $absolute);
        }
    }

    public function setDefaults()
    {
        $this->absolute = false;
    }

    public function absolute()
    {
        $this->absolute = true;

        return $this;
    }

    public function domain(Domain $domain)
    {
        $this->domain = $domain;

        return $this;
    }

    private function movieView(DomainMovie $domainMovie)
    {

        return [
            null => [
                'id' => $domainMovie->id,
                /* оборачиваю в urlencode, т.к. без этого laravel сам оборачивает во что-то похожее, но случаются 2 процента подряд %% и ссылка не валидная */
                'slug' => urlencode($domainMovie->movie->title_ru.$domainMovie->dmca),
            ],
            'translit' => [
                'id' => $domainMovie->id,
                'slug' => Str::slug($domainMovie->movie->title_ru.$domainMovie->dmca),
            ],
        ][$this->domain->primary_domain->cinemaDomain->routes_type];
    }

    private function movieViewMegaweb(DomainMovie $domainMovie)
    {
        return $this->domain->primary_domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb);
    }

    private function episodeView(DomainMovie $domainMovie, MovieEpisode $episode)
    {
        return [
            'id' => $episode->id,
            'dmca' => $domainMovie->dmca,
        ];
    }

    private function groupView(Group $group)
    {

        return [
            null => [
                'id' => $group->id,
                'slug' => $group->name_ru.$this->domain->primary_domain->cinemaDomain->groups->where('group_id', $group->id)->first()?->dmca,
            ],
            'translit' => [
                'id' => $group->id,
                'slug' => Str::slug($group->name_ru).$this->domain->primary_domain->cinemaDomain->groups->where('group_id', $group->id)->first()?->dmca,
            ],
        ][$this->domain->primary_domain->cinemaDomain->routes_type];
    }

    private function groupViewMegaweb(Group $group)
    {
        return $this->domain->primary_domain->cinemaDomain->megaweb->getGroupLink($group->megaweb);
    }

    private function genreViewMegaweb(Tag $tag)
    {
        /* заранее подготовливает domainTagMegaweb, т.к. там нужно указать домен, чтобы подгрузился dmca */
        /* TODO: говнище */
        $domainTagMegaweb = $tag->megaweb;
        $domainTagMegaweb->setDomain($this->domain->primary_domain->domain);

        return $this->domain->primary_domain->cinemaDomain->megaweb->getGenreLink($domainTagMegaweb);
    }

    private function index()
    {
        return [
            /* тут еще локаль должна быть */
            // 'locale' => (\App::getLocale() == 'ru') ? null : \App::getLocale(),
            'dmca' => $this->domain->primary_domain->routes->where('name', 'index')->first()->slug,
        ];
    }

    private function top50()
    {
        return [
            /* тут еще локаль должна быть */
            'dmca' => $this->domain->primary_domain->routes->where('name', 'top50')->first()->slug,
        ];
    }
}
