<?php

namespace App\Services;

use App\Models\Schedule;
use engine\app\models\F;
use Illuminate\Console\Command as BaseCommand;

class Command extends BaseCommand
{
    public $cron;

    public function handler()
    {
        throw new \Exception('You need to implement the handler method in the derived class.');
    }

    /* чтобы этот метод использовался, нужно handle в родительском методе переименовать в handler */
    public function handle()
    {
        /* Вызов родительского handle() метода команды */
        $result = $this->handler();

        $this->cron()->run_time = F::runtime();
        $this->cron()->save();

        // if ($this->cron()) {
        //     $this->cron()->run_time = F::runtime();
        //     $this->cron()->save();
        // }

        return $result;
    }

    public function cron()
    {

        if ($this->cron) {
            return $this->cron;
        }

        /* получаем строку команду которую ввели, если она есть в кронах, то подтянется инфа */
        // global $argv;
        // $argvEdited = $argv;
        // unset($argvEdited[0]);
        // $command = implode(' ', $argvEdited);

        /* проверяем на null, т.к. cron может быть false, если не найден в бд */
        // if (!is_null($this->cron)) {
        //     return $this->cron;
        // }

        if (empty($this->signature)) {
            throw new \Exception('Не указана сигнатура команды');
        }

        $type = explode(':', $this->getName())[0] ?? throw new \Exception('Невозможно определить бд крона');

        $cron = Schedule::where('command', $this->getName())->first();

        if (! $cron) {
            throw new \Exception("Команда с сигнатурой {$this->getName()} не найдена в расписаниях");
            // $this->error("Команда с сигнатурой {$this->getName()} не найдена в расписаниях");
        }

        /* теперь не выдаем эту ошибку, т.к. все команды используют этот базовый класс, в том числе у которых нет next_launch_delay */
        // if (!$cron?->next_launch_delay) {
        if (! $cron->next_launch_delay) {
            // throw new \Exception('Не указан период задержки между последним полным выполнением');
        }

        $this->cron = $cron;
        // $this->cron = $cron ?? false;

        return $this->cron;

    }

    public function canBeLaunched()
    {

        $cron = $this->cron();

        /* если крон отсутствует в бд, то всегда можем запустить команду */
        // if (!$cron) {
        //     return true;
        // }

        /* если не указана дата последнего обновления, значит задание еще ни разу не выполнялось, можно запускаться */
        if (! $cron->finished_at) {
            return true;
        }

        /* на основании даты последнего обновления получаем дату начала следующего обновления (не забываем клонировать дату через copy, чтобы изначальная дата не изменялась) */
        $nextLaunchAt = $cron->finished_at->copy()->addMinutes($cron->next_launch_delay);

        /* если время еще не пришло, то уходим */
        if (now() < $nextLaunchAt) {
            throw new \Exception("Пока что не можем начать выполнение, ждем {$nextLaunchAt->format('Y-m-d H:i:s')}");
        }

        return true;
    }

    public function finished()
    {

        $cron = $this->cron();
        $cron->temp_var = null;
        $cron->temp_var_2 = null;
        $cron->finished_at = now();
        $cron->save();

    }

    /* запустить замыкание, если это возможно. повторит его до истечения указанного количества секунд */
    public function launch(\Closure $closure)
    {

        try {
            $this->canBeLaunched();
        } catch (\Exception $e) {
            $this->warn($e->getMessage());

            return;
        }

        /* если крона нет то возникает ошибка, т.к. неоткуда брать инфу сколько секунд выполнять (repeat_for) */
        $cron = $this->cron();

        if ($cron->repeat_for) {
            F::repeatFor($closure, $cron->repeat_for);
        } else {
            $closure();
        }

        /* если после выполнения canBeLaunch == false, то еще раз применяем finished, чтобы обнулить переменные */
        try {
            $this->canBeLaunched();
        } catch (\Exception $e) {
            $this->warn($e->getMessage());
            $this->finished();
            $this->warn('Обнулили временные переменные');
        }
    }
}
