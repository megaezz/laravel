<?php

namespace App\Services\Madeline;

use danog\MadelineProto\Settings;
use danog\MadelineProto\Settings\AppInfo;
use danog\MadelineProto\Settings\Database\Mysql;
use danog\MadelineProto\Settings\Logger;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;

class SaulGoodieSettings
{
    public static function session()
    {
        return storage_path('madeline/saulgoodie');
    }

    public static function settings()
    {
        // создаем сессию и задаем настройки
        $settings = new Settings;
        $settings->setAppInfo((new AppInfo)->setApiId(26937334)->setApiHash('4a6846ebd03058ab595b2d7eced3b3a0'));
        $settings->setDb((new Mysql)->setUri('tcp://mariadb_secondary')->setPassword('root'));
        $settings->setLogger((new Logger)->setExtra(storage_path('madeline/madeline.log')));

        return $settings;
    }

    public static function api()
    {
        return new \danog\MadelineProto\API(self::session());
    }

    public static function login()
    {

        $MadelineProto = new \danog\MadelineProto\API(self::session(), self::settings());
        $MadelineProto->start();

        // этот метод не заработал (менять настройки текущей сессии), поэтому пока просто создаем новую сессию
        //  $MadelineProto->updateSettings($settings);
    }

    public static function tmpStorage()
    {
        return storage_path('madeline/tmp');
    }

    public static function buyTokenInlineKeyboard($token)
    {
        return new InlineKeyboardMarkup([
            [
                [
                    'text' => '💥 Купить на FASOL',
                    'url' => "https://t.me/fasol_robot/?start=ref_kimarvla_ca_{$token}",
                ],
            ],
            [
                [
                    'text' => '🚀 На Bloom',
                    'url' => "https://t.me/BloomSolana_bot/?start=ref_ENRLKALOQG_ca_{$token}",
                ],
                [
                    'text' => '💰 На GMGN',
                    'url' => 'https://t.me/GMGN_sol03_bot?start=i_tz0e7Jzo',
                ],
            ],
        ]);
    }
}
