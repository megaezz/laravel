<?php

namespace App\Services\Cinema;

use App\Models\Cinema\MovieUpload;

class MovieUploadTranscodingCommand
{
    public function __invoke(MovieUpload $movieUpload)
    {
        if (! $movieUpload->source_video) {
            throw new \Exception('Отсутствует source_video');
        }

        // TODO: временно, подменяем путь
        $sourceVideo = '/mnt/storage/app/cinema/torrent-downloads'.str_replace('/downloads', '', $movieUpload->source_video);
        $logFile = "/mnt/storage/app/cinema/torrent-downloads/{$movieUpload->id}.log";
        $uploadDir = "/mnt/storage/app/public/cinema/turbo/{$movieUpload->id}";
        // $basePath = '/home';

        return 'nohup bash -c \\'.PHP_EOL.
        "'mkdir -p {$uploadDir} && \\".PHP_EOL.
        "ffmpeg -i \"{$sourceVideo}\" \\".PHP_EOL.
        '-filter_complex "[0:v]split=3[v720][v480][v360]; \\'.PHP_EOL.
        '[v720]scale=w=1280:h=720[v720out]; \\'.PHP_EOL.
        '[v480]scale=w=854:h=480[v480out]; \\'.PHP_EOL.
        '[v360]scale=w=640:h=360[v360out]" \\'.PHP_EOL.
        '-map "[v720out]" -map 0:a -c:v:0 libx264 -b:v:0 2500k -maxrate:v:0 2675k -bufsize:v:0 3750k \\'.PHP_EOL.
        '-map "[v480out]" -map 0:a -c:v:1 libx264 -b:v:1 1500k -maxrate:v:1 1600k -bufsize:v:1 2250k \\'.PHP_EOL.
        '-map "[v360out]" -map 0:a -c:v:2 libx264 -b:v:2 800k -maxrate:v:2 856k -bufsize:v:2 1200k \\'.PHP_EOL.
        '-c:a aac -b:a 128k -ar 48000 -ac 2 \\'.PHP_EOL.
        '-f hls \\'.PHP_EOL.
        '-hls_time 6 \\'.PHP_EOL.
        '-hls_playlist_type vod \\'.PHP_EOL.
        '-hls_segment_type fmp4 \\'.PHP_EOL.
        "-hls_segment_filename \"{$uploadDir}/segment_%v_%03d.m4s\" \\".PHP_EOL.
        '-master_pl_name "hls.m3u8" \\'.PHP_EOL.
        '-var_stream_map "v:0,a:0 v:1,a:1 v:2,a:2" \\'.PHP_EOL.
        "\"{$uploadDir}/stream_%v.m3u8\" && \\".PHP_EOL.
        "rm {$logFile} \\".PHP_EOL.
        "'> {$logFile} 2>&1 &";
        // добавил здесь сразу &, теперь это команда для одного транскодирования
        // Не актуально: нужен общий & в конце - он добавляется в bulkAction
    }

    public function transcodingCommand1080p(MovieUpload $movieUpload)
    {
        $basePath = '/home';

        return 'nohup bash -c \\'.PHP_EOL.
        "'mkdir -p {$basePath}/downloads/{$movieUpload->id} && \\".PHP_EOL.
        "ffmpeg -i \"{$basePath}{$movieUpload->source_video}\" \\".PHP_EOL.
        '-filter_complex "[0:v]split=4[v1080][v720][v480][v360]; \\'.PHP_EOL.
        '[v1080]scale=w=1920:h=1080[v1080out]; \\'.PHP_EOL.
        '[v720]scale=w=1280:h=720[v720out]; \\'.PHP_EOL.
        '[v480]scale=w=854:h=480[v480out]; \\'.PHP_EOL.
        '[v360]scale=w=640:h=360[v360out]" \\'.PHP_EOL.
        '-map "[v1080out]" -map 0:a -c:v:0 libx264 -b:v:0 5000k -maxrate:v:0 5350k -bufsize:v:0 7500k \\'.PHP_EOL.
        '-map "[v720out]" -map 0:a -c:v:1 libx264 -b:v:1 2500k -maxrate:v:1 2675k -bufsize:v:1 3750k \\'.PHP_EOL.
        '-map "[v480out]" -map 0:a -c:v:2 libx264 -b:v:2 1500k -maxrate:v:2 1600k -bufsize:v:2 2250k \\'.PHP_EOL.
        '-map "[v360out]" -map 0:a -c:v:3 libx264 -b:v:3 800k -maxrate:v:3 856k -bufsize:v:3 1200k \\'.PHP_EOL.
        '-c:a aac -b:a 128k -ar 48000 -ac 2 \\'.PHP_EOL.
        '-f hls \\'.PHP_EOL.
        '-hls_time 6 \\'.PHP_EOL.
        '-hls_playlist_type vod \\'.PHP_EOL.
        '-hls_segment_type fmp4 \\'.PHP_EOL.
        "-hls_segment_filename \"{$basePath}/downloads/{$movieUpload->id}/segment_%v_%03d.m4s\" \\".PHP_EOL.
        '-master_pl_name "hls.m3u8" \\'.PHP_EOL.
        '-var_stream_map "v:0,a:0 v:1,a:1 v:2,a:2 v:3,a:3" \\'.PHP_EOL.
        "\"{$basePath}/downloads/{$movieUpload->id}/stream_%v.m3u8\" && \\".PHP_EOL.
        "rm {$basePath}/downloads/{$movieUpload->id}.log \\".PHP_EOL.
        "'> {$basePath}/downloads/{$movieUpload->id}.log 2>&1";
        // нужен общий & в конце - он добавляется в bulkAction
    }
}
