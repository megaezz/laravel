<?php

namespace App\Services\Cinema;

class Zerocdn
{
    /*
    Конфигурируем rclone:
    Url: https://[login].mng.zerocdn.com/
    Username: [login]
    Password: пароль к админке (не апи кей, как в инструкции написано)

    rclone mount Zerocdn:/ /mnt/zerocdn --daemon
    */

    public function __construct(private $login, private $apiSecret, private $cdnDomain) {}

    // передаем публичную ссылку из zerocdn и получаем подписанную ссылку
    public function getSignedUrl($url = null, $method = null, $ip = null)
    {
        $method = empty($method) ? 'ip' : $method;
        if (empty($url)) {
            throw new \Exception('URL is required to use Zerocdn::getSignedUrl');
        }
        // меняем в ссылке домен на актуальный
        $parseUrl = parse_url($url);
        $url = "https://{$this->cdnDomain}{$parseUrl['path']}";
        $ip = $ip ?? request()->ip();
        // $ip = '31.44.53.20';
        // F::dump($url);
        // F::dump(basename($url));
        $parseUrl = parse_url($url);
        $pathinfo = pathinfo($parseUrl['path']);
        // если basename с точкой, считаем что запрашивается файл
        if (strstr(basename($url), '.')) {
            $dirname = $pathinfo['dirname'].'/';
            $basename = $pathinfo['basename'];
        } else {
            $dirname = $parseUrl['path'];
            $basename = '';
        }
        // F::dump($dirname);
        // F::dump($basename);
        // F::dump($parseUrl);
        $cdnDomain = $parseUrl['host'];
        // получаем также главный домен, чтобы ставить куки именно на него, чтобы они были доступны с поддоменов
        $mainDomain = substr(stristr($parseUrl['host'], '.'), 1);
        // F::dump($cookieValue);
        $datetime = new \DateTime;
        // нам нужно возвращать метку времени в нулевой временной зоне
        $datetime->setTimezone(new \DateTimeZone('UTC'));
        // время жизни ссылки 12 часов
        $datetime->modify('+12 hours');
        $deadline = $datetime->format('YmdH');
        if ($method === 'cookie') {
            $cookieName = $cdnDomain.'-UID';
            $cookieNamePhp = str_replace('.', '_', $cookieName);
            if (empty($_COOKIE[$cookieNamePhp])) {
                // F::dump('ok');
                // время жизни куки пользователя неделя
                $cookieValue = 'test';
                setcookie($cookieName, $cookieValue, time() + 604800, '/', $mainDomain);
            } else {
                $cookieValue = $_COOKIE[$cookieNamePhp];
            }
            // генерируем подпись
            $signature = md5("{$dirname}-{$cookieValue}-{$deadline}-{$this->apiSecret}");
        }
        if ($method === 'ip') {
            // F::dump($dirname);
            // генерируем подпись
            $signature = md5("{$dirname}-{$ip}-{$deadline}-{$this->apiSecret}");
        }
        // собираем подписанную ссылку
        // $signedUrl = $parseUrl['scheme'].'://'.$cdnDomain.'/'.$signature.':'.$deadline.$parseUrl['path'];
        $signedUrl = "{$parseUrl['scheme']}://{$cdnDomain}{$dirname}{$signature}:{$deadline}/{$basename}";

        // F::dump($signedUrl);
        return $signedUrl;
    }

    // передаем публичную ссылку из zerocdn и получаем подписанную ссылку
    public function getSignedUrl2($url = null, $method = null)
    {
        $method = empty($method) ? 'ip' : $method;
        if (empty($url)) {
            throw new \Exception('URL is required to use Zerocdn::getSignedUrl');
        }
        $ip = request()->ip();
        // $ip = '31.44.53.20';
        $parseUrl = parse_url($url);
        $pathinfo = pathinfo($parseUrl['path']);
        $dirname = $pathinfo['dirname'].'/';
        $basename = $pathinfo['basename'];
        // F::dump($basename);
        // F::dump($parseUrl);
        $cdnDomain = $parseUrl['host'];
        // получаем также главный домен, чтобы ставить куки именно на него, чтобы они были доступны с поддоменов
        $mainDomain = substr(stristr($parseUrl['host'], '.'), 1);
        // F::dump($cookieValue);
        $datetime = new \DateTime;
        // нам нужно возвращать метку времени в нулевой временной зоне
        $datetime->setTimezone(new \DateTimeZone('UTC'));
        // время жизни ссылки 3 часа
        $datetime->modify('+3 hours');
        $deadline = $datetime->format('YmdH');
        if ($method === 'cookie') {
            $cookieName = $cdnDomain.'-UID';
            $cookieNamePhp = str_replace('.', '_', $cookieName);
            if (empty($_COOKIE[$cookieNamePhp])) {
                // F::dump('ok');
                // время жизни куки пользователя неделя
                $cookieValue = 'test';
                setcookie($cookieName, $cookieValue, time() + 604800, '/', $mainDomain);
            } else {
                $cookieValue = $_COOKIE[$cookieNamePhp];
            }
            // генерируем подпись
            $signature = md5("{$dirname}-{$cookieValue}-{$deadline}-{$this->apiSecret}");
        }
        if ($method === 'ip') {
            // F::dump($dirname);
            // генерируем подпись
            $signature = md5("{$dirname}-{$ip}-{$deadline}-{$this->apiSecret}");
        }
        // собираем подписанную ссылку
        // $signedUrl = $parseUrl['scheme'].'://'.$cdnDomain.'/'.$signature.':'.$deadline.$parseUrl['path'];
        $signedUrl = "{$parseUrl['scheme']}://{$cdnDomain}{$dirname}{$signature}:{$deadline}/{$basename}";

        return $signedUrl;
    }

    // передаем публичную ссылку из zerocdn и получаем подписанную ссылку
    public function getSignedUrlByCookie($url = null)
    {
        // $url = 'http://cdn.awmzone.pro/movies/925eae93c01c0ac05f020ae3f04fb26f43c78615/240.mp4';
        if (empty($url)) {
            throw new \Exception('URL is required to use Zerocdn::getSignedUrlByCookie');
        }
        $parseUrl = parse_url($url);
        $cdnDomain = $parseUrl['host'];
        // получаем также главный домен, чтобы ставить куки именно на него, чтобы они были доступны с поддоменов
        $mainDomain = substr(stristr($parseUrl['host'], '.'), 1);
        // F::dump($mainDomain);
        // F::dump(parse_url($url));
        // F::dump($_COOKIE);
        $cookieName = $cdnDomain.'-UID';
        // имя куки из массива $_COOKIE будет отличаться, т.к. точки заменяются на _
        $cookieNamePhp = str_replace('.', '_', $cookieName);
        if (empty($_COOKIE[$cookieNamePhp])) {
            // F::dump('ok');
            // время жизни куки пользователя неделя
            $cookieValue = 'test';
            setcookie($cookieName, $cookieValue, time() + 604800, '/', $mainDomain);
        } else {
            $cookieValue = $_COOKIE[$cookieNamePhp];
        }
        // F::dump($cookieValue);
        $datetime = new \DateTime;
        // нам нужно возвращать метку времени в нулевой временной зоне
        $datetime->setTimezone(new \DateTimeZone('UTC'));
        // время жизни ссылки 3 часа
        $datetime->modify('+3 hours');
        $deadline = $datetime->format('YmdH');
        // генерируем подпись
        $signature = md5("{$parseUrl['path']}-{$cookieValue}-{$deadline}-{$this->apiSecret}");

        // собираем подписанную ссылку
        $signedUrl = "{$parseUrl['scheme']}://{$cdnDomain}/{$signature}:{$deadline}{$parseUrl['path']}";

        return $signedUrl;
    }
}
