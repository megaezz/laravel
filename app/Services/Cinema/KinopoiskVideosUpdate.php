<?php

namespace App\Services\Cinema;

use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Kinopoisk;
use engine\app\models\F;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;

class KinopoiskVideosUpdate
{
    public function __invoke($cron)
    {
        /* лимит апи - 20 запросов в секунду */
        $limit = 20;
        $client = new Client([
            'base_uri' => 'https://kinopoiskapiunofficial.tech/api/v2.2/films/',
            'headers' => [
                'access-token' => Config::cached()->kinopoiskApiUnofficialToken,
                'Accept' => 'application/json',
            ],
        ]);
        $time1 = F::runtime();
        $promises = [];
        $kinopoisks = Kinopoisk::where('id', '>', $cron->temp_var ?? 0)->limit($limit)->get()->each(function ($kinopoisk) use (&$client, &$promises, &$cron) {
            // $response = $client->get("{$kinopoisk->id}/videos");
            // $kinopoisk->videos = json_decode($response->getBody(), true);
            // $kinopoisk->save();
            $promise = $client->getAsync("{$kinopoisk->id}/videos");
            $promise->then(
                function ($response) use (&$kinopoisk) {
                    $kinopoisk->videos = json_decode($response->getBody(), true);
                    $kinopoisk->save();
                },
            );
            $promises[] = $promise;
            $cron->temp_var = $kinopoisk->id;
        });
        Utils::settle($promises)->wait();
        $time2 = F::runtime();

        /* сохраняем текущее значение temp_var */
        // TODO: записывается ли реально последнее значение? ведь асинхронно выполняется.
        $cron->save();

        /* если не осталось элементов, значит мы закончили */
        if (! count($kinopoisks)) {
            $cron->finished_at = now();
            $cron->temp_var = null;
            $cron->save();

            return 'Обновление завершено';
        }

        usleep(1000000);

        dump($time2 - $time1.'ms.');

        return $cron->temp_var;
    }
}
