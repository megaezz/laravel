<?php

namespace App\Services\Cinema;

use App\Models\Schedule;
use cinema\app\models\eloquent\MovieEpisode;
use cinema\app\models\eloquent\Myshows;
use cinema\app\models\Movie;
use cinema\app\models\Movies;
use cinema\app\models\MyShowsMe;
use cinema\app\models\Tag;
use engine\app\models\Engine;
use engine\app\models\F;

class MyshowsUpdate
{
    public function __construct(private Schedule $cron) {}

    public function run()
    {
        Engine::setType('cinema');
        /* если нет ни одного непроставленного чеккера, значит задание выполнено */
        $done = Myshows::whereChecked(false)->limit(1)->get()->count() ? false : true;
        /* если нет ни одного проставленного чеккера, значит задание стартует */
        /* $start = Myshows::whereChecked(true)->limit(1)->get()->count() ? false : true; */
        $result = null;
        if ($done) {
            $this->cron->finished_at = now();
            $this->cron->temp_var = null;
            $this->cron->save();
            $result .= 'База myshows обновлена';
            $result .= $this->addNew();
            $result .= $this->resetCheckers();

            return $result;
        }
        $result .= $this->update();

        return $result;
    }

    /* добавляет новые ID сервиса myshows.me в базу myshows.me */
    public function addNew()
    {
        $max_id = Myshows::max('id') ?: 0;
        $request = new \stdClass;
        $request->jsonrpc = '2.0';
        $request->method = 'shows.Ids';
        $request->params = new \stdClass;
        $request->params->fromId = $max_id;
        $request->params->count = 1000;
        $request->id = 1;
        $answer = MyShowsMe::apiRequest($request);
        if (! isset($answer->result)) {
            dd($answer);
        }
        foreach ($answer->result as $id) {
            $myshows = new Myshows;
            $myshows->id = $id;
            $myshows->save();
            // F::query('insert into '.F::typetab('myshows').' set id = \''.$id.'\';');
        }
        dump('Добавлено '.count($answer->result).' строк');
    }

    // прогон по базе MyShows.me, обновление data и нахождение соответствия с movie_id
    public function update()
    {
        Engine::setDisplayErrors(true);
        $limit = 5;
        $myshowses = Myshows::select('id')->whereChecked(false)->orderByDesc('id')->limit($limit);
        $rows = $myshowses->count();
        $myshowses = $myshowses->get();
        /* dd($myshowses); */
        $locales = ['en', 'ua', 'ru'];
        $objects = [];
        /* ID 2340 создает проблемы, там куча данных */
        ini_set('memory_limit', '512M');
        foreach ($myshowses as $myshows) {
            foreach ($locales as $locale) {
                $object = new \stdClass;
                $object->locale = $locale;
                $object->myshows_id = $myshows->id;
                $object->curl = new \stdClass;
                $object->curl->ch = MyShowsMe::getCurl(MyShowsMe::getShowDataRequest($myshows->id), $locale);
                $objects[] = $object;
            }
        }
        $result = F::multiCurl($objects);
        /* dump($result); */
        $list = '';
        foreach ($result as $object) {
            $object->result = json_decode($object->result);
            if (! isset($object->result->result)) {
                /* dump($object); */
                if (isset($object->result->error)) {
                    if ($object->result->error->data == 'show was not found') {
                        if ($object->locale == 'ru') {
                            Myshows::find($object->myshows_id)->delete();
                            $list .= $object->myshows_id.' не найден в API, удалили'.PHP_EOL;

                            continue;
                        }
                    }
                    $list .= $object->myshows_id.': '.$object->result->error->data.PHP_EOL;
                } else {
                    // dd($object);
                    $list .= 'Неизвестная ошибка с объектом ответа: '.json_encode($object->result).PHP_EOL;
                }

                continue;
            }
            $data = $object->result->result;
            $myshows = Myshows::find($object->myshows_id);
            $movie_id = null;
            if (! empty($data->kinopoiskId) and $object->locale == 'ru') {
                /* ищем соответствие в cinema.movies */
                $movies = new Movies;
                $movies->setKinopoiskId($data->kinopoiskId);
                $arr_movies = $movies->get();
                if ($arr_movies) {
                    /* dump($data); */
                    $movie_id = $arr_movies[0]['id'];
                    $myshows->movie_id = $movie_id;
                    $movie = new Movie($movie_id);
                    $movie->setMyshowsId($data->id);
                    if ($data->started) {
                        $started = null;
                        if (preg_match('/^([\w]+)\/([\d]+)\/([\d]+)$/', $data->started)) {
                            $started = \DateTime::createFromFormat('M/d/Y', $data->started);
                        }
                        if (preg_match('/^([\w]+)\/([\d]+)$/', $data->started)) {
                            $started = \DateTime::createFromFormat('M/Y', $data->started);
                        }
                        if (preg_match('/^([\d]+)$/', $data->started)) {
                            $started = \DateTime::createFromFormat('Y', $data->started);
                        }
                        if ($started) {
                            $movie->setStarted($started->format('Y-m-d'));
                        }
                    }
                    if ($data->ended) {
                        $ended = null;
                        if (preg_match('/^([\w]+)\/([\d]+)\/([\d]+)$/', $data->ended)) {
                            $ended = \DateTime::createFromFormat('M/d/Y', $data->ended);
                        }
                        if (preg_match('/^([\w]+)\/([\d]+)$/', $data->ended)) {
                            $ended = \DateTime::createFromFormat('M/Y', $data->ended);
                        }
                        if (preg_match('/^([\d]+)$/', $data->ended)) {
                            $ended = \DateTime::createFromFormat('Y', $data->ended);
                        }
                        if ($ended) {
                            $movie->setEnded($ended->format('Y-m-d'));
                        }
                    }
                    if (isset($data->network->title)) {
                        $tagId = Tag::getIdByName($data->network->title);
                        /* если такого тега нет, создаем его */
                        if (! $tagId) {
                            $tag = new Tag(Tag::add());
                            $tag->setType('studio');
                            $tag->setName($data->network->title);
                            $tag->save();
                            $tagId = $tag->getId();
                        }
                        /* добавляем тег к фильму, если он не существует */
                        $movie->addTag($tagId);
                        /* dump($data); */
                    }
                    /* F::dump($data); */
                    /* записываем эпизоды */
                    foreach (($data->episodes ?? []) as $e) {
                        /* F::dump($data); */
                        /* F::dump($airDate->getTimestamp()); */

                        $movieEpisode = MovieEpisode::query()
                            ->where('movie_id', $movie->getId())
                            ->where('season', $e->seasonNumber)
                            ->where('episode', $e->episodeNumber)
                            ->first();

                        if (! $movieEpisode) {
                            $movieEpisode = new MovieEpisode;
                            $movieEpisode->movie_id = $movie->getId();
                            $movieEpisode->season = $e->seasonNumber;
                            $movieEpisode->episode = $e->episodeNumber;
                        }

                        /* TODO: добавляю вручную 4 часа. т.к. при записи в бд 1970-01-01 00:00:00 появляется ошибка из-за того что бд в моковском времени, и она смещает UTC время на 4 часа назад и не может добавить это значение, пробовал ->setTimezone() - не работает, т.к. этот метод тоже смещает время, не получается просто сменить зону без смещения */
                        // $movieEpisode->air_date = now()->parse($e->airDateUTC)->addHours(4) ?? null;
                        /* забил хуй, просто сменил тип поля с timestamp на datetime, т.к. все равно бывают записи и ранее 1970 года, а timestamp такое не умеет */
                        $movieEpisode->air_date = $e->airDateUTC ?? null;
                        $movieEpisode->title = $e->title;
                        $movieEpisode->short_name = $e->shortName;

                        if ($movieEpisode->isDirty()) {
                            $movieEpisode->save();
                        }

                        // F::query('
                        // 	insert into '.F::typetab('movie_episodes_data').'
                        // 	set movie_id = \''.$movie->getId().'\',
                        // 	season = \''.$e->seasonNumber.'\',
                        // 	episode = \''.$e->episodeNumber.'\',
                        // 	air_date = from_unixtime('.($airDate?$airDate->getTimestamp():'null').'),
                        // 	title = \''.F::escape_string($e->title).'\',
                        // 	short_name = \''.F::escape_string($e->shortName).'\'
                        // 	on duplicate key update
                        // 	air_date = from_unixtime('.($airDate?$airDate->getTimestamp():'null').'),
                        // 	title = \''.F::escape_string($e->title).'\',
                        // 	short_name = \''.F::escape_string($e->shortName).'\'
                        // 	;');
                    }
                    $movie->setStatus($data->status);
                    /* F::dump($movie); */
                    $movie->save();
                }
            }
            if ($object->locale == 'ru') {
                $myshows->data = $data;
                /* отмечаем строку, потому что локаль ru - последняя в цикле */
                $myshows->checked = true;
            } else {
                $myshows->{'data_'.$object->locale} = $data;
            }
            $myshows->save();
        }
        $secondsPer1row = 5 / $limit;
        $minutesToEnd = $rows * $secondsPer1row / 60;
        $this->cron->temp_var = "Осталось {$rows} строк";
        $this->cron->save();

        return 'Обновлено '.count($myshowses).' строк из '.$rows.'. Осталось '.round($minutesToEnd, 2).' мин.'.PHP_EOL.$list;
    }

    public function resetCheckers()
    {
        Myshows::whereChecked(true)->update(['checked' => false]);
        dump('Сбросили чеккеры myshows');
    }
}
