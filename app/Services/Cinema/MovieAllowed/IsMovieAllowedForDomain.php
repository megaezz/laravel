<?php

namespace App\Services\Cinema\MovieAllowed;

use cinema\app\models\eloquent\Domain;
use cinema\app\models\eloquent\Movie;

class IsMovieAllowedForDomain
{
    /* TODO не доделано */
    /* подходит ли movie для этого домена? */
    public function __invoke(Domain $domain, Movie|int $movie)
    {
        if (! $movie instanceof Movie) {
            $movie = Movie::find($movie);
            if (! $movie) {
                throw new \Exception('Фильм не найден');
            }
        }
        /* если контент этого типа не нужен, то пропускаем */
        if ($movie->type == 'serial' and ! $domain->include_serials) {
            return ['result' => false, 'message' => 'Сериалы запрещены'];
        }
        if ($movie->type == 'trailer' and ! $domain->include_trailers) {
            return ['result' => false, 'message' => 'Трейлеры запрещены'];
        }
        if ($movie->type == 'movie' and ! $domain->include_movies) {
            return ['result' => false, 'message' => 'Фильмы запрещены'];
        }
        if ($movie->type == 'game' and ! $domain->include_games) {
            return ['result' => false, 'message' => 'Игры запрещены'];
        }
        if ($movie->type == 'youtube' and ! $domain->include_youtube) {
            return ['result' => false, 'message' => 'Youtube ролики запрещены'];
        }
        /* в идеале должно быть так, но пока что $movie->deleted используется только для скрытия плеера, продумать */
        /*if ($movie->deleted) {
            return array('result' => false, 'message' => 'Фильм удален');
        }*/

        /* если указан минимальный год, то добавляем только фильмы не старше этого года */
        if ($domain->moviesMinYear) {
            if ($movie->year) {
                if ($movie->year < $domain->moviesMinYear) {
                    return ['result' => false, 'message' => 'Разрешены только фильмы с '.$domain->moviesMinYear.' года'];
                }
            }
        }
        /* если нужно добавлять только определенные категории, то сопоставляем фильм с выбранными категориями */
        if ($domain->useOnlyChoosenGroups) {
            $allowedGroups = $domain->groups()->pluck('group_id');
            $movieGroups = $movie->groups()->pluck('id');
            /* если нужно добавлять только определенные категории, то сопоставляем фильм с выбранными категориями, обязательно isNotEmpty, иначе некорректно работает, и возвращает true если allowedGroups пустая коллекция, т.к. результат intersect - коллекция, и если она пустая, то она возвращает true все равно */
            if (! $movieGroups->intersect($allowedGroups)->isNotEmpty()) {
                return ['result' => false, 'message' => "Не относится к разрешенным категориям. Разрешены: {$allowedGroups->implode(', ') }, фильм в {$movieGroups->implode(', ')}"];
            }
        }
        /* если можно добавлять только легальные плееры, проверяем */
        if ($domain->legalMovies) {
            if (! $movie->players) {
                return ['result' => false, 'message' => 'Нет легального плеера'];
            }
        }
        /* если нельзя добавлять русские фильмы, но они ранее существовали на сайте, то проверяем и деактивируем такие видео */
        if (! $domain->allowRussianMovies) {
            /* получаем ID тега Россия чтобы использовать в цикле */
            if ($movie->tags->pluck('tag')->contains('Россия')) {
                return ['result' => false, 'message' => 'Русские фильмы запрещены'];
            }
        }
        /* если нельзя добавлять рискованные студии, но они ранее сущ., то проверяем и деактивируем такие видео */
        if (! $domain->allowRiskyStudios) {
            if ($movie->tags->contains('risky', true)) {
                return ['result' => false, 'message' => 'Рискованные теги запрещены'];
            }
        }
        if ($domain->allow_movies_only_from) {
            if (! (new IsMovieExistsOnParsedDomain)($domain->allow_movies_only_from, $movie)) {
                return ['result' => false, 'message' => "Разрешены только фильмы с {$domain->allow_movies_only_from}"];
            }
        }

        return ['result' => true];
    }

    // подходит ли movie для этого домена?
    public function getIsMovieAllowed($movie_id = null)
    {
        $movie = new Movie($movie_id);
        // если контент этого типа не нужен, то пропускаем
        if ($movie->getType() == 'serial' and ! $this->getIncludeSerials()) {
            return ['result' => false, 'message' => 'Сериалы запрещены'];
        }
        if ($movie->getType() == 'trailer' and ! $this->getIncludeTrailers()) {
            return ['result' => false, 'message' => 'Трейлеры запрещены'];
        }
        if ($movie->getType() == 'movie' and ! $this->getIncludeMovies()) {
            return ['result' => false, 'message' => 'Фильмы запрещены'];
        }
        if ($movie->getType() == 'game' and ! $this->getIncludeGames()) {
            return ['result' => false, 'message' => 'Игры запрещены'];
        }
        if ($movie->getType() == 'youtube' and ! $this->getIncludeYoutube()) {
            return ['result' => false, 'message' => 'Youtube ролики запрещены'];
        }
        /* в идеале должно быть так, но пока что $movie->deleted используется только для скрытия плеера, продумать */
        /*if ($movie->getDeleted()) {
            return array('result' => false, 'message' => 'Фильм удален');
        }*/

        // если указан минимальный год, то добавляем только фильмы не старше этого года
        if ($this->getMoviesMinYear()) {
            if ($movie->getYear()) {
                if ($movie->getYear() < $this->getMoviesMinYear()) {
                    return ['result' => false, 'message' => 'Разрешены только фильмы с '.$this->getMoviesMinYear().' года'];
                }
            }
        }
        // если нужно добавлять только определенные категории, то сопоставляем фильм с выбранными категориями
        if ($this->getUseOnlyChoosenGroups()) {
            $isGroupAllowed = false;
            $groups = new Groups;
            $groups->setMovieId($movie->getId());
            $movieGroups = $groups->get();
            $groups = new Groups;
            $groups->setDomain($this->getDomain());
            $groups->setUseDomainGroup(true);
            $allowedGroups = [];
            foreach (F::cache($groups, 'get', null, 600) as $v) {
                $allowedGroups[] = $v['id'];
            }
            foreach ($movieGroups as $v) {
                if (in_array($v['id'], $allowedGroups)) {
                    $isGroupAllowed = true;
                    break;
                }
            }
            if (! $isGroupAllowed) {
                return ['result' => false, 'message' => 'Не относится к разрешенным категориям'];
            }
        }
        // если можно добавлять только легальные плееры, проверяем
        if ($this->getLegalMovies()) {
            if (! $movie->hasLegalPlayer()) {
                return ['result' => false, 'message' => 'Нет легального плеера'];
            }
        }
        // если нельзя добавлять русские фильмы, но они ранее существовали на сайте, то проверяем и деактивируем такие видео
        if (! $this->getAllowRussianMovies()) {
            // получаем ID тега Россия чтобы использовать в цикле
            $russianTagId = Tag::getIdByName('Россия');
            if (in_array($russianTagId, $movie->getTags())) {
                return ['result' => false, 'message' => 'Русские фильмы запрещены'];
            }
        }
        // если нельзя добавлять рискованные студии, но они ранее сущ., то проверяем и деактивируем такие видео
        if (! $this->getAllowRiskyStudios()) {
            // получаем список рискованных тегов
            $tags = new Tags;
            $tags->setRisky(true);
            $riskyTags = F::cache($tags, 'get', null, 600);
            // получаем список тегов видео
            $movieTags = $movie->getTags();
            // сопоставляем
            foreach ($riskyTags as $riskyTag) {
                if (in_array($riskyTag['id'], $movieTags)) {
                    return ['result' => false, 'message' => 'Рискованные теги запрещены'];
                }
            }
        }

        return ['result' => true];
    }
}
