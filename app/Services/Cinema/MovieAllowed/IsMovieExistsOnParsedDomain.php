<?php

namespace App\Services\Cinema\MovieAllowed;

use App\Models\ParsedUrl;
use cinema\app\models\eloquent\Movie;

class IsMovieExistsOnParsedDomain
{
    public function __invoke(string $parsedDomain, Movie $movie)
    {

        $parsedUrl = ParsedUrl::query()
            ->where('domain', $parsedDomain)
            ->where('movie_id', $movie->id)
            ->first();

        return $parsedUrl ? true : false;

    }
}
