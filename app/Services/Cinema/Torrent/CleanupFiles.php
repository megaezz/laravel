<?php

namespace App\Services\Cinema\Torrent;

use App\Models\Cinema\MovieUpload;
use App\Models\Cinema\Torrent;

class CleanupFiles
{
    public function __invoke(Torrent $torrent)
    {

        if ($torrent->movie->type !== 'serial' and $torrent->aria2->downloadedMovies()->count() > 1) {
            throw new \Exception("Не сериал, но файлов в торренте: {$torrent->aria2->downloadedMovies()->count()}");
        }

        $files = [];

        foreach ($torrent->aria2->downloadedMovies() as $file) {

            if (preg_match('/[Ss](?P<season>\d+)[Ee](?P<episode>\d+)/', basename($file), $matches)) {
                $seasonNum = (int) $matches['season'];
                $episodeNum = (int) $matches['episode'];

                if ($torrent->movie->type !== 'serial') {
                    throw new \Exception("Не сериал, но определился сезон {$seasonNum} эпизод {$episodeNum}");
                }

                if (! $torrent->is_forced) {
                    // проверяем, есть ли уже такой эпизод с таким переводом
                    $movieUpload = $torrent->movie
                        ->uploads()
                        ->where('dubbing_id', $torrent->dubbing_id)
                        ->whereHas(
                            'episode',
                            fn ($query) => $query
                                ->where('season', $seasonNum)
                                ->where('episode', $episodeNum)
                        )
                        ->first();

                    if ($movieUpload) {
                        continue;
                        // throw new \Exception("Уже есть такой эпизод");
                    }
                }

                $files[] = $file;

            } else {
                if ($torrent->movie->type === 'serial') {
                    throw new \Exception('Сериал, но не определился сезон и эпизод');
                }

                if (! $torrent->is_forced) {
                    // проверяем, есть ли уже такой MovieUpload
                    $movieUpload = $torrent->movie
                        ->uploads()
                        ->where('dubbing_id', $torrent->dubbing_id)
                        ->first();

                    if ($movieUpload) {
                        continue;
                    }
                }

                $files[] = $file;
            }
        }

        return collect($files);
    }
}
