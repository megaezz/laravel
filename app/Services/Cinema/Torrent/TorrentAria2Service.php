<?php

namespace App\Services\Cinema\Torrent;

use App\Models\Cinema\Torrent;
use App\Services\Aria2;
use Illuminate\Support\Facades\Storage;

class TorrentAria2Service
{
    private Aria2 $aria2;

    public function __construct(public Torrent $torrent)
    {
        $this->aria2 = new Aria2('http://aria2:6800/jsonrpc');
    }

    public function torrentData()
    {
        try {
            $result = $this->aria2->status($this->torrent->aria2_gid)['result'];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage()];
        }

        return $result;
    }

    public function contentData()
    {
        if (! isset($this->torrent->aria2_torrent_data['followedBy'][0])) {
            return null;
        }
        try {
            $result = $this->aria2->status($this->torrent->aria2_torrent_data['followedBy'][0])['result'];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage()];
        }

        return $result;
    }

    public function download()
    {
        // хз почему proxy а не nginx, лень разбираться, на локале работало с nginx, на проде нет
        $url = 'http://proxy'.Storage::url($this->torrent->src);
        $gid = $this->aria2->addByUrl($url)['result'];
        $this->torrent->aria2_gid = $gid;
        // переводим признак очистки в false, чтобы очистка отработала при повторной загрузке
        $this->torrent->is_cleaned_up = false;
        $this->torrent->save();

        return $gid;
    }

    // удаляет торрент и все записи о нем из бд
    public function remove()
    {
        $results = collect();

        try {
            $gids = collect()->push($this->torrent->aria2_gid);
            $result = $this->aria2->status($this->torrent->aria2_gid);
        } catch (\Exception $e) {
            $results[] = $e->getMessage();
        }

        if (isset($result['result']['followedBy'][0])) {
            $gids->push($result['result']['followedBy'][0]);
        }

        foreach ($gids as $gid) {
            try {
                $this->aria2->remove($gid);
                $results[] = "{$gid} remove: ok";
            } catch (\Exception $e) {
                $results[] = "{$gid} remove: {$e->getMessage()}";
            }

            try {
                $this->aria2->removeDownloadResult($gid);
                $results[] = "{$gid} removeDownloadResult: ok";
            } catch (\Exception $e) {
                $results[] = "{$gid} removeDownloadResult: {$e->getMessage()}";
            }
        }

        $this->torrent->aria2_gid = null;
        $this->torrent->aria2_torrent_data = null;
        $this->torrent->aria2_content_data = null;
        $this->torrent->save();

        return $results;
    }

    public function downloadedMovies()
    {
        if (empty($this->torrent->aria2_content_data['files'])) {
            return collect();
        }
        $files = collect($this->torrent->aria2_content_data['files']);

        return $files->filter(fn ($file) => in_array(pathinfo($file['path'], PATHINFO_EXTENSION), ['mkv', 'mp4']))->pluck('path');
    }

    public function selectedMovies()
    {
        if (empty($this->torrent->aria2_content_data['files'])) {
            return collect();
        }

        $files = collect($this->torrent->aria2_content_data['files'])->where('selected', 'true');

        return $files->filter(fn ($file) => in_array(pathinfo($file['path'], PATHINFO_EXTENSION), ['mkv', 'mp4']))->pluck('path');
    }

    public function downloadStatus()
    {
        if (! $this->torrent->aria2_gid) {
            return null;
        }

        if ($this->torrent->is_downloaded) {
            return null;
        }

        if (isset($this->torrent->aria2_torrent_data['status']) and $this->torrent->aria2_torrent_data['status'] === 'active') {
            $torrentStatus = empty($this->torrent->aria2_torrent_data['totalLength']) ? 'нет данных' : (round($this->torrent->aria2_torrent_data['completedLength'] / $this->torrent->aria2_torrent_data['totalLength'] * 100, 1).'% ('.round($this->torrent->aria2_torrent_data['totalLength'] / 1024 / 1024 / 1024, 1).' Гб)');
        } else {
            $torrentStatus = $this->torrent->aria2_torrent_data['status'] ?? 'нет данных';
        }

        if (isset($this->torrent->aria2_content_data['status']) and $this->torrent->aria2_content_data['status'] === 'active') {
            $contentStatus = empty($this->torrent->aria2_content_data['totalLength']) ? 'нет данных' : (round($this->torrent->aria2_content_data['completedLength'] / $this->torrent->aria2_content_data['totalLength'] * 100, 1).'% ('.round($this->torrent->aria2_content_data['totalLength'] / 1024 / 1024 / 1024, 1).' Гб) '.round($this->torrent->aria2_content_data['downloadSpeed'] / 1024 / 1024, 1).' мбит');
        } else {
            $contentStatus = $this->torrent->aria2_content_data['status'] ?? 'нет данных';
        }

        return
            (($torrentStatus === 'complete') ? null : ("Torrent: {$torrentStatus} / ")).
            "Content: {$contentStatus}";
        // $results = collect();
        // $result = $aria2->status($this->torrent->aria2_gid);
        // $results[$result['result']['gid']] = $result['result']['status'];
        // if (isset($result['result']['followedBy'][0])) {
        //     $result = $aria2->status($result['result']['followedBy'][0]);
        //     $results[$result['result']['gid']] = $result['result']['status'];
        // }
    }

    public function selectOnlyNeededFiles()
    {
        if (! $this->torrent->aria2_content_data) {
            throw new \Exception('Content Data отсутствует');
        }
        // коллекция всех файлов торрента
        $files = collect($this->torrent->aria2_content_data['files']);
        // только нужные файлы
        $cleanupFiles = (new CleanupFiles)($this->torrent);
        // получаем индексы нужных файлов
        $indexes = $cleanupFiles->map(fn ($file) => $files->where('path', $file)->first()['index']);
        // определяем gid контента
        $gid = $this->torrent->aria2_content_data['gid'];
        // отмечаем только нужные файлы, если не выбрано ни одного файла, то удаляем задачу, иначе не получается сделать
        if ($indexes->count()) {
            $result = $this->aria2->changeOptions($gid, ['select-file' => $indexes->implode(',')]);
        } else {
            $result = $this->remove();
        }

        return $result;
    }
}
