<?php

namespace App\Services\Cinema\Torrent;

use App\Models\Cinema\MovieUpload;
use App\Models\Cinema\Torrent;
use cinema\app\models\eloquent\MovieEpisode;

class CreateMovieUploadsFromTorrent
{
    public function __invoke(Torrent $torrent)
    {

        if ($torrent->movie->type !== 'serial' and $torrent->aria2->downloadedMovies()->count() > 1) {
            throw new \Exception("Не сериал, но файлов в торренте: {$torrent->aria2->downloadedMovies()->count()}");
        }

        $filesActions = [];

        $files = (new CleanupFiles)($torrent);

        foreach ($files as $file) {

            $actions = [];
            $seasonNum = null;
            $episodeNum = null;

            $isSerial = preg_match('/[Ss](?P<season>\d+)[Ee](?P<episode>\d+)/', basename($file), $matches);

            if ($isSerial) {
                $seasonNum = (int) $matches['season'];
                $episodeNum = (int) $matches['episode'];

                if ($torrent->movie->type !== 'serial') {
                    throw new \Exception("Не сериал, но определился сезон {$seasonNum} эпизод {$episodeNum}");
                }

                // проверяем, существует ли такой эпизод в принципе
                $episode = $torrent->movie
                    ->episodes()
                    ->where('season', $seasonNum)
                    ->where('episode', $episodeNum)
                    ->first();

                if (! $episode) {
                    $episode = new MovieEpisode;
                    $episode->movie()->associate($torrent->movie);
                    $episode->season = $seasonNum;
                    $episode->episode = $episodeNum;
                    // dd($episode);
                    $episode->save();
                    $actions[] = "Добавили {$episode->season_episode} для {$episode->movie->title}";
                }
            }

            // создаем MovieUpload
            $movieUpload = new MovieUpload;
            $movieUpload->movie()->associate($torrent->movie);
            if ($isSerial) {
                $movieUpload->episode()->associate($episode);
            }
            $movieUpload->torrent()->associate($torrent);
            $movieUpload->dubbing()->associate($torrent->dubbing);
            $movieUpload->source_video = $file;
            $movieUpload->save();

            $actions[] =
            "Создали MovieUpload: #{$movieUpload->id} {$movieUpload->movie->title} – ".
            ($isSerial ? "{$movieUpload->episode->season_episode} – " : null).
            "{$movieUpload->dubbing->short_or_full_name}";

            $filesActions[$file] = $actions;
        }

        return collect($filesActions);
    }
}
