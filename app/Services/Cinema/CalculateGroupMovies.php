<?php

namespace App\Services\Cinema;

use cinema\app\models\eloquent\Group;

class CalculateGroupMovies
{
    public function __invoke($cron)
    {
        /* когда бд на удаленном сервере, то работает медленно, т.к. запросы вставляются построчно, а переписывать через массовый инсерт не хочу, т.к. сейчас красиво */
        /* поэтому лимит делаем 1, в случае, если перееду обратно на локальную бд - сделать лимит 10 */
        $limit = 1;
        $chunk_size = 1000;

        /* на каком id остановились? */
        $last_id = $cron->temp_var ?? 0;

        /* берем N следующих записей */
        $groups = Group::query()
            ->where('id', '>', $last_id)
            ->where('calc_by_tags', true)
            ->orderBy('id')
            ->limit($limit)
            ->get();

        foreach ($groups as $group) {

            $group_movies = $group->calculateMovies()->select('id')->get();

            dump("group: {$group->id}");
            dump('расчитали видео: '.$group_movies->count());
            dump('в базе сейчас видео для этой группы: '.$group->movies()->count());

            $syncInfo = $group->movies()->sync($group_movies);

            dump('удалили: '.count($syncInfo['detached']).', добавили: '.count($syncInfo['attached']).', обновили: '.count($syncInfo['updated']));
        }

        if ($groups->count()) {
            /* записываем в конфиг последний обработанный id */
            $cron->temp_var = $group->id;
            $cron->save();

            return "Обработано {$groups->count()}. Последний id: {$group->id}";
        } else {
            /* если не найдено записей, значит задача выполнена, устанавливаем finished_at */
            $cron->temp_var = null;
            $cron->finished_at = now();
            $cron->save();

            return 'Завершено';
        }
    }
}
