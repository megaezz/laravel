<?php

namespace App\Services\Cinema\Balancers;

use App\Models\Proxy;
use cinema\app\models\eloquent\Hdvb;
use cinema\app\models\eloquent\Movie;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;

class HdvbUpdate
{
    public function __invoke($limitPages, $page)
    {
        $apiKey = '6721e3457628a64855fc0991f1b271af';
        $limit = 30;
        $url = "https://apivb.com/api/filter.json?token={$apiKey}&limit={$limit}&page=";
        $pageTo = $page + $limitPages - 1;

        $proxy = Proxy::where('active', true)->where('hdvb', true)->inRandomOrder()->firstOrFail();

        $clientOptions = [
            'proxy' => $proxy->proxy,
            // глючило, добавил юзер агента на всякий случай, скорее всего никак не роляет
            'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
        ];

        $client = new Client($clientOptions);

        $promises = [];
        $nextPage = $page;
        $finished = false;

        for ($i = $page; $i <= $pageTo; $i++) {
            $promises[] = $client->getAsync("{$url}{$i}");
        }

        $responses = Utils::settle($promises)->wait();

        $messages = [
            ["Используем прокси: {$proxy->proxy}"],
        ];

        /* вместо замыкания используем цикл, чтобы получать верное значение nextPage */
        foreach ($responses as $response) {

            if (empty($response['value'])) {
                throw new \Exception('Ошибка получения данных');
            }

            $result = json_decode($response['value']->getBody());

            if (empty($result->results)) {
                $messages[] = ['Нет данных: '.$response['value']->getBody()];

                continue;
            }

            if ($result->pagination->next_page) {
                preg_match('/\&page=([\d]+)/', $result->pagination->next_page, $matches);
                $nextPage = $matches[1];
            } else {
                $finished = true;
            }

            $messages[] = ['Hdvb: страница '.($nextPage - 1).', результатов: '.(isset($result->results) ? count($result->results) : 0)];

            foreach ($result->results as $data) {

                $hdvb = Hdvb::find($data->player->token);
                if (! $hdvb) {
                    $hdvb = new Hdvb;
                    $hdvb->token = $data->player->token;
                }

                /* сопоставляем movie */
                $movie = null;
                if (! empty($data->kinopoisk_id)) {
                    $movie = Movie::where('kinopoisk_id', $data->kinopoisk_id)->first();
                }

                $hdvb->movie_id = $movie->id ?? null;
                $hdvb->data = $data;
                /* чтобы апдейтилось даже если нет изменений, чтобы потом удалять старье в HdvbClear */
                $hdvb->updated_at = now();
                $hdvb->save();

            }
        }

        return [
            'next_page' => $nextPage,
            'messages' => $messages,
            'message' => "Обновление Hdvb. Страниц: {$limitPages}",
            'finished' => $finished,
        ];
    }
}
