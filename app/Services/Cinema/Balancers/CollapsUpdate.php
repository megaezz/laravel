<?php

namespace App\Services\Cinema\Balancers;

use cinema\app\models\eloquent\Collaps;
use cinema\app\models\eloquent\Movie;
use GuzzleHttp\Client;

class CollapsUpdate
{
    public function __invoke($limit, $page)
    {
        $page = $page ?? 1;
        $url = "https://apicollaps.cc/list?token=56d54e93de7819ef9b8f8b0100aef673&sort=activate_time&limit={$limit}&page={$page}";
        $client = new Client;
        $response = $client->request('GET', $url);
        if (empty($response->getBody())) {
            throw new \Exception("Нет ответа {$url}");
        }
        $data = json_decode($response->getBody());
        if (empty($data->results)) {
            throw new \Exception("results не существует {$url}");
        }
        $totalPages = ceil($data->total / $limit);

        foreach ($data->results as $result) {
            $collaps = Collaps::find($result->id);
            if (! $collaps) {
                $collaps = new Collaps;
                $collaps->id = $result->id;
            }

            /* сопоставляем movie */
            $movie = null;
            if (! empty($result->kinopoisk_id)) {
                $movie = Movie::where('kinopoisk_id', $result->kinopoisk_id)->first();
            }
            if (! $movie and ! empty($result->imdb_id)) {
                $movie = Movie::where('imdb_id', $result->imdb_id)->first();
            }

            $collaps->movie_id = $movie->id ?? null;
            $collaps->data = $result;
            $collaps->save();
        }

        return [
            'next_page' => $page + 1,
            'message' => 'Обработано: '.count($data->results)." элементов. Страница {$page} из {$totalPages}.",
            'finished' => $data->next_page ? false : true,
        ];
    }
}
