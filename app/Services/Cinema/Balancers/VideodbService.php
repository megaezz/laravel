<?php

namespace App\Services\Cinema\Balancers;

use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\Videodb;
use engine\app\models\F;
use Illuminate\Support\Str;

class VideodbService
{
    public function __invoke($limitPages, $limitItems, $offset)
    {
        $startTime = F::microtime_float();
        $limitItems = $limitItems ?? 100;
        $limitPages = $limitPages ?? 10;
        $offset = $offset ?? 0;
        $objects = [];
        for ($i = 0; $i < $limitPages; $i++) {
            $object = new \stdClass;
            $object->url = "https://videodb.win/api/v1/medias/?limit={$limitItems}&offset={$offset}";
            $object->curl = new \stdClass;
            $object->curl->userpwd = Config::cached()->videodb_credentials->login.':'.Config::cached()->videodb_credentials->password;
            $objects[] = $object;
            $offset = $offset + $limitItems;
        }
        $objects = F::multiCurl($objects);
        $time['after_multicurl'] = round(F::microtime_float() - $startTime, 4);
        $list = '';
        foreach ($objects as $object) {

            $result = json_decode($object->result);

            if (! $result) {
                logger()->alert("Некорректный json ({$object->url}): ".Str::limit($object->result, 50));

                continue;
            }

            if (! isset($result->results)) {
                logger()->alert("Result не существует: ({$object->url})");

                continue;
            }

            foreach ($result->results as $v) {

                $videodb = Videodb::find($v->id);

                /* если нет, такого id - добавляем */
                if (! $videodb) {
                    $videodb = new Videodb;
                    $videodb->id = $v->id;
                }

                /* обновляем поля videodb */
                /* сначала обновляем data, т.к. ниже атрибуты берут данные из data */
                $videodb->data = $v;
                /* ставим временную метку, что запись существует в апи на данный момент */
                $videodb->is_exists_on_date = now();
                $videodb->max_quality = $videodb->data->max_quality ?? null;
                $videodb->translation = $videodb->data->translation->title ?? null;
                $videodb->season = $videodb->data->content_object->season->num ?? null;
                $videodb->episode = $videodb->data->content_object->num ?? null;
                $videodb->videodb_show_id = $videodb->data->content_object->tv_series_id ?? null;
                $videodb->kinopoisk_id = $videodb->data_kinopoisk_id;

                /* начинаем поиск соответствия в таблице cinema */

                $movie = null;

                if ($videodb->data_kinopoisk_id) {
                    $movie = Movie::whereKinopoiskId($videodb->data_kinopoisk_id)->first();
                }

                if (! $movie and $videodb->data_imdb_id) {
                    $movie = Movie::whereImdbId($videodb->data_imdb_id)->first();
                }

                $listAdded = collect();

                /* TODO: а если у записи меняется kinopisk_id? наверное надо каждый раз заново искать соответствия? */
                /* если нашли соответствие - просто ставим метку videodb, если нет - создаем новый фильм */
                if ($movie) {
                    $movie->videodb = true;
                    $listAdded[] = 'movie существует';
                } else {
                    /* если есть id кинопоиска, то фильм нам нужен, если нет - не нужен */
                    if ($videodb->data_kinopoisk_id) {
                        $movie = new Movie;
                        $movie->type = $videodb->type;
                        $movie->kinopoisk_id = $videodb->data_kinopoisk_id;
                        $movie->imdb_id = $videodb->data_imdb_id;
                        $movie->title_ru = $videodb->data_title_ru;
                        $movie->title_en = $videodb->data_title_en;
                        $movie->videodb = true;
                        $listAdded[] = 'добавлен movie';
                    } else {
                        $listAdded[] = 'не добавлен movie, т.к. нет ID кинопоиска';
                    }
                }

                if ($movie and $movie->isDirty()) {
                    $movie->save();
                }

                /* если была найдена связь с $movie - сохраняем в videodb */
                if ($movie) {
                    $videodb->movie_id = $movie->id;

                    /* если есть связь с movie и указан сезон или эпизод - пытаемся найти соответствие в эпизодах */
                    $movieEpisode = null;

                    if ($videodb->season or $videodb->episode) {
                        $movieEpisode = $movie->episodes()
                            ->where('season', ltrim($videodb->season, '0'))
                            ->where(function ($query) use ($videodb) {
                                $query
                                    ->where('episode', $videodb->episode)
                                    ->orWhere('episode', ltrim($videodb->episode, '0'));
                            })
                            ->first();

                        /* если найден эпизод, записываем его в videodb */
                        if ($movieEpisode) {
                            $videodb->movie_episode_id = $movieEpisode->id;
                            $listAdded[] = 'добавлен MovieEpisode';
                        } else {
                            $listAdded[] = 'MovieEpisode не найден';
                        }
                    }
                }

                $videodb->checked = true;
                if ($videodb->isDirty()) {
                    $videodb->save();
                }

                $list .= "#{$videodb->id} (kp: {$videodb->data_kinopoisk_id}, imdb: {$videodb->data_imdb_id}) -> {$videodb->movie_id} {$listAdded->implode(', ')}".PHP_EOL;
            }
        }

        $message =
        'Выполнено за: '.round(F::microtime_float() - $startTime, 4).' сек.'.PHP_EOL.
        'Multicurl: '.$time['after_multicurl'].' сек.'.PHP_EOL.
        'Далее '.($result?->next ?? 'нет данных').PHP_EOL.
        '---'.PHP_EOL.
        $list.PHP_EOL;

        return [
            'next_offset' => $offset,
            'message' => $message,
            'finished' => isset($result->results) ? ! count($result->results) : false,
        ];
    }
}
