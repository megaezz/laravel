<?php

namespace App\Services\Cinema\Balancers;

use cinema\app\models\eloquent\Alloha;
use cinema\app\models\eloquent\Movie;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;

class AllohaUpdate
{
    public function __invoke($page)
    {
        /* было 5 но постоянно вываливает ошибку, сделал 2 - тоже, 1 - норм */
        $limitPages = 1;
        $page = $page ?? 1;
        $pageTo = $page + $limitPages - 1;
        $url = 'https://api.alloha.tv/?token=b8823fb7495df944e5944fb56c6778&list&page=';
        $client = new Client;
        $promises = [];
        $nextPage = $page;
        $error = null;

        for ($i = $page; $i <= $pageTo; $i++) {
            $promise = $client->getAsync("{$url}{$i}");
            $promises[] = $promise;
        }

        $responses = Utils::settle($promises)->wait();

        /* вместо замыкания используем цикл, чтобы получать верное значение nextPage */
        foreach ($responses as $response) {
            if (empty($response['value'])) {
                $error = 'Ошибка при получении данных';
                break;
            }
            $result = json_decode($response['value']->getBody());
            if (empty($result->data)) {
                $error = 'Пустой ответ';
                break;
            }
            $nextPage = $result->next_page;
            foreach ($result->data as $data) {
                $alloha = Alloha::find($data->token_movie);
                if (! $alloha) {
                    $alloha = new Alloha;
                    $alloha->token_movie = $data->token_movie;
                }

                /* сопоставляем movie */
                $movie = null;
                if (! empty($data->id_kp)) {
                    $movie = Movie::where('kinopoisk_id', $data->id_kp)->first();
                }
                if (! $movie and ! empty($data->imdb_id)) {
                    $movie = Movie::where('imdb_id', str_replace('tt', '', $data->imdb_id))->first();
                }

                $alloha->movie_id = $movie->id ?? null;
                $alloha->data = $data;
                $alloha->save();
            }
        }

        return [
            'next_page' => $nextPage,
            'message' => "Обновление Alloha. Страниц: {$limitPages}. Далее со страницы: {$nextPage}",
            'error' => $error,
            'finished' => ($page == $nextPage or ! $nextPage) ? true : false,
        ];
    }
}
