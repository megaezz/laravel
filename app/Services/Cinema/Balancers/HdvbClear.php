<?php

namespace App\Services\Cinema\Balancers;

use cinema\app\models\eloquent\Hdvb;

class HdvbClear
{
    public function __invoke()
    {
        // разрешенный процент удаляемых строк от общего количества строк
        $allowed_percent = 1;
        // какая должна быть минимальная разница между минимальной и максимальной датами в БД, чтобы мы начали удаление
        $diff_between_min_max_days = 3;
        $rows = Hdvb::count();
        $min_max_days = Hdvb::selectRaw('date(min(updated_at)) as min_day,date(max(updated_at)) as max_day, datediff(max(updated_at),min(updated_at)) as diff')->first();
        $log = 'Без действий';
        if ($min_max_days->diff < $diff_between_min_max_days) {
            $log = 'Hdvb: минимальная дата ('.$min_max_days->min_day.') меньше максимальной ('.$min_max_days->max_day.') менее чем на '.$diff_between_min_max_days.' дней';
            logger()->info($log);

            return $log;
        }
        // находим сколько строк
        $min_day_rows = Hdvb::whereRaw('updated_at <= ?', [now()->parse($min_max_days->max_day)->subdays($diff_between_min_max_days)])->count();
        // если к удалению больше allowed_percent, то не трогаем
        $percent = round($min_day_rows / $rows * 100, 2);
        if ($percent > $allowed_percent) {
            $log = 'Hdvb: Не удаляем '.$min_day_rows.' строк, т.к. это больше '.$allowed_percent.'%';
            logger()->alert($log);

            return $log;
        } else {
            Hdvb::whereRaw('date(updated_at) = ?', [$min_max_days->min_day])->delete();
            $log = 'Hdvb: Удалили '.$min_day_rows.' строк, '.$percent.'%';
            logger()->alert($log);

            return $log;
        }

        return $log;
    }
}
