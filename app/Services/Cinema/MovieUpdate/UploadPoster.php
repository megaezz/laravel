<?php

namespace App\Services\Cinema\MovieUpdate;

use cinema\app\models\eloquent\Movie;
use engine\app\models\Engine;
use Illuminate\Support\Facades\Http;

class UploadPoster
{
    public function __invoke(Movie $movie)
    {

        $postersDir = Engine::getStaticPath().'/types/cinema/posters';
        $postersWebPDir = Engine::getStaticPath().'/types/cinema/posters-webp';

        /* создаем каталог jpeg постеров если его нет */
        if (! is_dir($postersDir)) {
            mkdir($postersDir);
        }

        /* создаем каталог webp постеров если его нет */
        if (! is_dir($postersWebPDir)) {
            mkdir($postersWebPDir);
        }

        /* путь создаваемого файла */
        $path = "{$postersDir}/{$movie->id}.jpg";
        $pathWebP = "{$postersWebPDir}/{$movie->id}.webp";

        /* проверяем, есть ли уже файл */
        if (file_exists($path)) {

            /* если существующий постер - не заглушка, то отмечаем в бд, если заглушка, то далее патаемся обновить его */
            /* удалять постеры - заглушки наверное не стоит, чтобы не бесить пс */
            /* TODO: определять не по размеру файла */
            if (filesize($path) != 2430) {

                /* если webp постер тоже есть, то отмечаем что постер есть и уходим */
                if (file_exists($pathWebP)) {
                    if ($movie->poster) {
                        return 'Постер уже существует';
                    } else {
                        $movie->poster = true;

                        return 'Постер уже был сохранен, проставили метку';
                    }
                }

            }
        }

        /* если мы здесь, значит файла постера нет, проставляем, т.к. в базе может быть poster = true */
        $movie->poster = false;

        if (! $movie->do_update) {
            return 'Запрещено через do_update = true';
        }

        $poster = null;

        if (! $movie->kinopoisk_id) {
            return 'Отсутствует kinopoisk_id';
        }

        /* формируем, ссылку и запрашиваем заголовок, разрешая редиректы, если no-poster.gif, то постера нет */
        /* TODO: что если ссылка изменится? как узнаю? выбрасывать в телегу уведомление */
        $url = "https://st.kp.yandex.net/images/film_iphone/iphone360_{$movie->kinopoisk_id}.jpg";
        $response = Http::withOptions([
            'allow_redirects' => true,
            'timeout' => 5,
        ])->head($url);
        $poster = ($response->effectiveUri() == 'https://st.kp.yandex.net/images/no-poster.gif') ? null : $response->effectiveUri();

        // dump($url, $response);

        /* если указан путь для копирования постера - копируем оттуда и сразу затираем */
        if ($movie->copy_poster_url) {
            $poster = $movie->copy_poster_url;
            $movie->copy_poster_url = null;
        }

        if (! $poster) {
            /* если постер есть у коллапса или аллохи, берем его */
            if (! empty($movie->collapses?->first()->data->poster)) {
                $poster = $movie->collapses->first()->data->poster;
            } elseif (! empty($movie->allohas?->first()->data->poster)) {
                $poster = $movie->allohas->first()->data->poster;
                // } elseif (!empty($movie->hdvbs?->first()->data->poster)) {
                // $poster = $movie->hdvbs->first()->data->poster;
            } else {
                return 'Постер отсутствует';
            }
        }

        try {
            $response = Http::timeout(5)->get($poster);
            // ->withOptions(['proxy' => $proxy])
            if (! $response->successful()) {
                throw new \Exception("HTTP status: {$response->status()}");
            }
        } catch (\Exception $e) {
            $message = "Не удалось скачать постер: {$poster}. {$e->getMessage()}";
            logger()->alert($message);

            return $message;
        }

        $file = $response->body();

        if (file_put_contents($path, $file)) {
            /* проверяем, в правильном ли формате находится картинка */
            $format = mime_content_type($path);
            if ($format == 'image/png') {
                $posterImg = imagecreatefrompng($path);
                /* пересохраняем в верном формате */
                imagejpeg($posterImg, $path);
            }
            if ($format == 'image/gif') {
                $posterImg = imagecreatefromgif($path);
                /* пересохраняем в верном формате */
                imagejpeg($posterImg, $path);
            }
            /* создаем WebP версию картинки */
            if (function_exists('imagewebp')) {
                $posterImg = imagecreatefromjpeg($path);
                if ($posterImg) {
                    if (imagewebp($posterImg, $pathWebP)) {
                        $movie->poster = true;

                        return "Сохранили постер {$poster}";
                    } else {
                        $message = "Не удалось создать WebP версию постера: {$poster}";
                        logger()->alert($message);

                        return $message;
                    }
                } else {
                    $message = "Не jpg файл: {$poster}";
                    logger()->alert($message);

                    return $message;
                }
            }
        } else {
            $message = "Не получилось загрузить скачаный постер: {$poster}";
            logger()->alert($message);

            return $message;
        }
    }
}
