<?php

namespace App\Services\Cinema\MovieUpdate;

use cinema\app\models\MovieTags;
use cinema\app\models\Tag;

class UpdateFromKinopoisk
{
    public function __invoke($movie = null, $data = null)
    {
        $yearExplode = explode('-', $data->year);
        if (count($yearExplode) > 1) {
            $year = $yearExplode[0];
        } else {
            $year = $data->year;
        }
        // F::dump($year);
        // if (!$movie->getYear()) {
        if ($year) {
            $movie->setYear((int) $year);
        }
        // if (!$movie->getTitleRu()) {
        if ($data->nameRu) {
            $movie->setTitleRu($data->nameRu);
        }
        // if (!$movie->getTitleEn()) {
        if ($data->nameEn) {
            $movie->setTitleEn($data->nameEn);
        }
        // if (!$movie->getDescription()) {
        if ($data->description) {
            $movie->setDescription($data->description);
        }
        if ($data->rating) {
            $movie->setKinopoiskRating($data->rating);
        }
        if ($data->ratingVoteCount) {
            $movie->setKinopoiskVotes($data->ratingVoteCount);
        }
        if ($data->ratingImdb) {
            $movie->setImdbRating($data->ratingImdb);
        }
        if ($data->ratingImdbVoteCount) {
            $movie->setImdbVotes($data->ratingImdbVoteCount);
        }
        if (! $movie->getTagline() and $data->slogan) {
            $movie->setTagline($data->slogan);
        }
        if ($data->ratingAgeLimits) {
            $movie->setAge($data->ratingAgeLimits);
        }
        $movieType = $data->type ?? null;
        if ($movieType == 'TV_SHOW') {
            $movie->setType('serial');
        }
        if ($movieType == 'FILM') {
            $movie->setType('movie');
        }
        $listTags = [];
        // добавляем тэг "тип фильма"
        if ($movie->getType()) {
            // удаляем теги
            $movie->removeTag(Tag::getIdByName('serial'));
            $movie->removeTag(Tag::getIdByName('movie'));
            // пишем тег
            $movie->addTagByName($movie->getType());

            $listTags[] = $movie->getType();
        }
        // F::dump($data);
        $actors = [];
        // if (isset($data->creators->actor)) {
        // 	foreach ($data->creators->actor as $v2) {
        // 		$actors[] = $v2->name_person_ru;
        // 	}
        // }
        $directors = [];
        // if (isset($data->creators->director)) {
        // 	foreach ($data->creators->director as $v2) {
        // 		$directors[] = $v2->name_person_ru;
        // 	}
        // }
        $genres = [];
        if (isset($data->genres)) {
            foreach ($data->genres as $v2) {
                $genres[] = $v2->genre;
            }
        }
        $countries = [];
        if (isset($data->countries)) {
            foreach ($data->countries as $v2) {
                $countries[] = $v2->country;
            }
        }
        // F::dump($data);
        // добавляем жанры и страны
        $tags_arr = [
            'genre' => $genres,
            'country' => $countries,
            'actor' => $actors,
            'director' => $directors,
            'studio' => [],
            'year' => ($year ? [$year] : []),
        ];
        foreach ($tags_arr as $type => $arr) {
            // если массива тэгов нет то идем дальше
            if (! $arr) {
                continue;
            }
            foreach ($arr as $v) {
                // если передается пустой тег, то пропускаем, иначе пробуем получить ID тега
                if (empty($v)) {
                    continue;
                } else {
                    $tagId = Tag::getIdByName($v);
                }
                // флаг нужен для отчета
                $created = false;
                // если такого тэга нет, то создаем его с типом type
                if (! $tagId) {
                    // создаем тэг одновременно создавая класс
                    $tag = new Tag(Tag::add());
                    $tag->setType($type);
                    $tag->setName($v);
                    $tag->save();
                    $tagId = $tag->getId();
                    $created = true;
                }
                // если тип тэга - год, то сначала удаляем все теги данного типа
                $tag = new Tag($tagId);
                // F::dump($tag);
                if ($tag->getType() == 'year') {
                    // получаем список тегов с годом для фильма
                    $movieTags = new MovieTags($movie->getId());
                    $movieTags->setType('year');
                    $arrMovieTags = $movieTags->get();
                    // если больше 1 тега года, то значит сносим годики и пишем текущий тег года
                    if (count($arrMovieTags) > 1) {
                        // удаляем эти теги
                        foreach ($arrMovieTags as $yearMovieTag) {
                            $movie->removeTag($yearMovieTag['id']);
                        }
                    }
                    // F::dump($movie->getTags());
                }
                // если такого тэга нет в списке тэгов для данного фильма, то добавляем его
                if ($movie->addTag($tagId)) {
                    $listTags[] = ($created ? 'создан ' : '')."{$v} ({$type})";
                }
            }
        }
        $info['tags'] = $listTags;

        return $info;
    }
}
