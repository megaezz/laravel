<?php

namespace App\Services\Cinema\MovieUpdate;

use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\Tag;
use engine\app\models\Engine;

class Handler
{
    public function __invoke(int $limit, ?int $lastId = 0)
    {

        /* TODO: пока еще нужно, убрать когда избавлюсь тут от остатков megaweb */
        Engine::setType('cinema');

        $movies = Movie::query()
            ->with('kinopoisk')
            ->where('type', '!=', 'youtube')
            ->whereNotNull('kinopoisk_id')
            ->orderBy('id')
            ->where('id', '>', $lastId)
            ->limit($limit)
            ->get();

        /* получаем заранее foreign и СНГ теги для applyForeignOrSngTag, чтобы экономить на запросах */
        $foreignAndSngTags = Tag::whereIn('tag', ['foreign', 'СНГ'])->get();

        $list = collect();

        foreach ($movies as $movie) {

            if (! $movie->kinopoisk?->data) {
                $list[] = "Фильм #{$movie->id} не найден в базе Kinopoisk API";
                $movie->save();

                continue;
            }

            /* обновление фильма из базы кинопоиска, через json..json идет преобразование массива в объект */
            $info = (new UpdateFromKinopoisk)($movie->megaweb, json_decode(json_encode($movie->kinopoisk->data)));
            // TODO сохраняю т.к. были изменения в setMovieFromKinopiskData
            $movie->megaweb->save();

            /* подтягиваем изменения которые случились в $movie->megaweb */
            $movie->refresh();

            /* правим теги foreign/sng */
            $foreignOrSngTagInfo = $movie->applyForeignOrSngTag($foreignAndSngTags);

            /* обновляем постер */
            $posterInfo = (new UploadPoster)($movie);
            $movie->save();

            $list[] = "#{$movie->id}".PHP_EOL.
            "Постер: {$posterInfo}".PHP_EOL.
            ($foreignOrSngTagInfo ? 'поправили теги foreign/СНГ' : 'не правили теги foreign/СНГ').PHP_EOL.
            ($info['tags'] ? ('Добавили тэги: '.collect($info['tags'])->implode(', ')) : '');
        }

        return ['lastId' => $movie->id, 'items' => $list];
    }
}
