<?php

namespace App\Services\Cinema;

use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\MovieTag;

class CalculateRelatedMovies
{
    public function __invoke($cron)
    {
        $limit = 20;
        $chunk_size = 1000;

        /* на каком id остановились? */
        $last_id = $cron->temp_var ?? 0;

        /* берем N следующих записей */
        $movies = Movie::query()
            ->where('id', '>', $last_id)
            ->whereIn('cinema.type', ['movie', 'serial', 'trailer'])
            ->orderBy('id')
            ->limit($limit)
            ->get();

        foreach ($movies as $movie) {

            /* находим похожие для текущего фильма с нужным названием полей для последующей вставки */
            // $related_movies = $movie->relatedMovies()->select([Movie::raw('id as related_movie_id'),'q'])->get()->toArray();

            $related_movies = MovieTag::query()
                ->selectRaw("cinema_id as related_movie_id, {$movie->id} as movie_id, count(*) as q")
                ->whereIn('tag_id', $movie->tags->modelKeys())
                ->where('cinema_id', '!=', $movie->id)
                ->groupBy('related_movie_id')
                ->having('q', '>', 4)
                ->get();

            dump("movie_id: {$movie->id}");
            dump('расчитали похожиие: '.$related_movies->count());
            dump('в базе сейчас похожих: '.$movie->relatedMovies()->count());

            /* удаляем сущесвующие похожие, чтобы не заморачиваться с duplicate и чтобы не было старых неактуальных записей */
            /* делаем это порционно чтобы не сдохла БД */
            // do {
            //     $deleted = $movie
            //     ->relatedMovies()
            //     ->limit(500)
            //     ->delete();
            // } while ($deleted > 0);

            // $movie->relatedMovies()->delete();

            /* если сразу вставлять большое количество строк, то часто появляется ошибка подготовленного запроса: too many placeholders, поэтому разбиваем на части */
            $chunks = $related_movies->chunk($chunk_size);

            /* Сначала вставим или обновим записи */
            foreach ($chunks as $chunk) {
                /* вставляем массово одним запросом по 1000 элементов */
                /* не используем createMany, т.к. в этом случае вставка идет построчно */
                // $movie->relatedMovies()->insert($chunk->toArray());
                $movie->relatedMovies()->upsert(
                    $chunk->toArray(),
                    /* Уникальные столбцы */
                    ['related_movie_id'],
                    /* Столбцы для обновления */
                    ['q']
                );
            }

            dump('обновили все похожие в бд');

            /* Соберём все related_movie_id из $relatedMovies */
            $relatedMovieIds = $related_movies->pluck('related_movie_id')->toArray();

            /* Удаляем лишние строки */
            $deleted = $movie->relatedMovies()
                ->whereNotIn('related_movie_id', $relatedMovieIds)
                ->delete();

            dump('удалили лишние строки: '.$deleted);
            dump('---');
            // dd(\DB::getQueryLog());
        }

        if ($movies->count()) {
            /* записываем в конфиг последний обработанный id */
            $cron->temp_var = $movie->id;
            $cron->save();

            return "Обработано {$movies->count()}. Последний id: {$movie->id}";
        } else {
            /* если не найдено записей, значит задача выполнена, устанавливаем finished_at */
            $cron->temp_var = null;
            $cron->finished_at = now();
            $cron->save();

            return 'Завершено';
        }
    }
}
