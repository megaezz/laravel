<?php

namespace App\Services\Cinema\Turbo;

use App\Models\Domain;
use cinema\app\models\eloquent\MovieEpisode;
use Illuminate\Support\Str;

// принимает domain и выдает ссылки на все его страницы, где размещены материалы приоритетной студии
class LinksToPrioritizedUploadsOnDomain
{
    public function __invoke(Domain $domain)
    {
        // получаем все domainMovie, которые содержат загрузки приоритетной студии
        $domainMovies = $domain->mirror_of_or_self->cinemaDomain->movies()
            ->where('active', true)
            // ->withWhereRelation('movie.uploads', 'dubbing_id', $domain->priorityDubbing->id)
            // берем только загрузку с последним добавленным эпизодом (по дате, не стал определять именно последний эпизод, времени жалко)
            ->withWhereHas(
                'movie.uploads',
                fn($query) =>
                $query
                    ->with('episode')
                    ->where('dubbing_id', $domain->priorityDubbing->id)
                    ->where('hls_exists', true)
                    ->orderBy('created_at')
                    ->limit(1)
            )
            ->get();

        $links = collect();

        foreach ($domainMovies as $domainMovie) {
            $playLastEpisode = $domainMovie->movie->uploads->first()->episode
                ? "#play-{$domainMovie->movie->uploads->first()->episode->se}"
                : '';
            $links[] = $domain->toRoute->absolute()->movieView($domainMovie) . '?acdhk' . Str::random(5) . $playLastEpisode;
        }

        return $links;
    }
}
