<?php

namespace App\Services\Cinema\Turbo;

use App\Models\Cinema\MovieUpload;
use App\Models\Domain;
use cinema\app\models\eloquent\Movie;
use Illuminate\Support\Str;

// принимает movieUpload и выдает ссылки на все страницы турбо доменов, где он стоит первым в плеере
class LinksWhereThisUploadIsPrioritized
{
    private static array $cachedLinks;

    public function __invoke(MovieUpload $movieUpload)
    {
        // если уже посчитаны ссылки для этого movie и этой студии, то сразу выдаем их
        if (isset(self::$cachedLinks[$movieUpload->movie_id][$movieUpload->dubbing_id])) {
            return self::$cachedLinks[$movieUpload->movie_id][$movieUpload->dubbing_id];
        }

        // получаем домены с такой же приоритетной студией как у этого movieUpload и в которых есть такой же фильм
        $domains = Domain::query()
            ->onlyTurbo()
            ->with('turbo.dubbing')
            ->where(
                fn($query) =>
                $query
                    ->withWhereHas(
                        'cinemaDomain.movies',
                        fn($query) => $query
                            ->where('movie_id', $movieUpload->movie_id)
                            ->where('active', true)
                    )
                    ->orWhere(
                        fn($query) =>
                        $query->withWhereHas(
                            'mirrorOf.cinemaDomain.movies',
                            fn($query) =>
                            $query
                                ->where('movie_id', $movieUpload->movie_id)
                                ->where('active', true)
                        )
                    )
            )
            ->get()
            ->filter(fn($domain) => $domain->priorityDubbing?->id === $movieUpload->dubbing->id);

        $links = collect();

        foreach ($domains as $domain) {
            $playEpisode = $movieUpload->episode ? "#play-{$movieUpload->episode->se}" : '';
            $links[] = $domain->toRoute->absolute()->movieView($domain->mirror_of_or_self->cinemaDomain->movies->first()) . '?acdhk' . Str::random(5) . $playEpisode;
        }

        self::$cachedLinks[$movieUpload->movie_id][$movieUpload->dubbing_id] = $links;

        return $links;
    }
}
