<?php

namespace App\Services\Cinema;

use App\Models\Cinema\MovieUpload;
use Barryvdh\Debugbar\Facades\Debugbar;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\DomainMovie;
use cinema\app\models\eloquent\Dubbing;
use cinema\app\models\eloquent\Videodb;
use cinema\app\models\Videodb as ModelsVideodb;
use engine\app\models\Engine;
use Illuminate\Support\Facades\Storage;

class PlaylistForPlayerjs
{
    public function __invoke(DomainMovie $domainMovie)
    {
        // TODO: этот способ правильный, но оказалось, что далеко не для всех загрузок Videodb есть соответствия в MovieEpisode, поэтому решил отдельно получать MovieUploads и Videodbs и потом их объединять, в дальнейшем исправить эту ситуацию и раскоментировать этот способ
        // получаем все эпизодики вместе с MovieUploads
        // $episodes = $domainMovie->movie
        // ->episodes()
        // // подгружаем только эпизоды, к которым есть выложенные загрузки и одновременно добавляем их в with
        // ->withWhereHas('uploads', fn($query) =>
        //     // TODO: сразу сортировать озвучки по приоритетности
        //     $query
        //     ->with('dubbing')
        //     ->where('hls_exists', true)
        // )
        // // пробуем сортировать здесь, чтобы дальше не пришлось сортировать коллекцию, проверить останется ли после группировки
        // ->orderBy('season')
        // ->orderBy('episode')
        // ->get()
        // // группируем все это добро по сезонам
        // ->groupBy('season');

        // TODO: обработать ситуацию, когда для эпизода есть более одной озвучки от одной и той же студии, в этом случае нужно выбрать более свежую по дате, т.к. она будет более качественная. Сделать это надо отдельно для MovieUploads и Videodbs.
        // TODO: для MovieUploads поднимать наверх только одну приоритетную озвучку (да?)
        // TODO: проверять здесь - если это актуальный turbo домен, то нужны заморочки с приоритетом загрузок
        // TODO: сделать, чтобы если нет приоритетной озвучки - mergedDubbings сортировался по translations.order, а то сейчас все-равно загрузки отображаются сверху даже когда этого и не надо
        $priorityDubbing = Engine::requestedDomain()->priorityDubbing;

        Debugbar::startMeasure('playlistForPlayerjs');
        // получаем все загрузки и группируем их по сезонам и эпизодам
        $uploadDubbings = $domainMovie->movie
            ->uploads()
            ->with('dubbing')
            ->with('episode')
            ->where('hls_exists', true)
            ->get()
            // особо нет смысла сортировать здесь, т.к. дальше сортируется merged коллекция
            // ->sortBy(['episode.season', 'episode.episode'])
            // поднимаем здесь приоритетную озвучку для домена на первое место, чтобы после мерджа об этом не заморачиваться
            // ->sortByDesc('dubbing.turbo_order')
            // если существует приоритетная озвучка для домена, то подкидываем ее наверх
            ->when(
                $priorityDubbing,
                fn($uploadDubbings) => $uploadDubbings
                    ->sortBy(fn($movieUpload) => $movieUpload->dubbing?->id === $priorityDubbing?->id ? 0 : 1)
            )
            ->map(function ($movieUpload) {
                // Дублируем season и episode на первом уровне, чтобы далее было проще сгруппировать с Videodb
                $movieUpload->season_num = $movieUpload->episode?->season;
                $movieUpload->episode_num = $movieUpload->episode?->episode;

                return $movieUpload;
            });
        // ->groupBy(['episode.season', 'episode.episode']);

        // теперь получаем все загрузки Videodb и группируем их по сезонам и эпизодам
        $videodbDubbings = $domainMovie->movie
            ->videodbs()
            ->selectRaw('videodb.*, translations.name, translations.videodb_name, translations.order, cast(season as unsigned) as season_num, cast(episode as unsigned) as episode_num')
            ->leftJoin('cinema.translations', 'translations.videodb_name', 'videodb.translation')
            // а здесь, на уровне бд, есть смысл сортировать по сезону-эпизоду, чтобы потом сортирование merged коллекции занимало меньше времени
            ->orderBy('season_num')
            ->orderBy('episode_num')
            ->orderByDesc('translations.order')
            ->orderBy('videodb.translation')
            ->get();
        // ->groupBy(['season', 'episode']);

        // объединям MovieUploads с Videodb в таком порядке, чтобы MovieUploads были сверху. И группируем сразу.
        $mergedDubbings = $uploadDubbings
            ->merge($videodbDubbings)
            ->sortBy(['season_num', 'episode_num'])
            ->groupBy(['season_num', 'episode_num']);

        // если ничего нет, то null, это нужно для компонента Videodb, чтобы выдать фейк плеер в случае если нет контента
        if (! $mergedDubbings->count()) {
            return null;
        }

        $playlist = $mergedDubbings->map(function ($episodes, $season) {
            return [
                'title' => "Сезон $season",
                'folder' => $episodes->map(function ($dubbings, $episode) use ($season) {
                    return [
                        'id' => "S{$season}E{$episode}",
                        'title' => "Серия {$episode}",
                        'file' => ModelsVideodb::pjsBase64Encrypt(
                            $dubbings->map(function ($video) {
                                if ($video instanceof MovieUpload) {
                                    return "{{$video->dubbing->short_or_full_name}}" . Storage::disk('storage')->url($video->hls) . ';';
                                }
                                if ($video instanceof Videodb) {
                                    $signedUrl = Config::cached()->zerocdnService->getSignedUrl($video->data->hls, 'ip');

                                    return "{{$video->translation}}{$signedUrl};";
                                }
                            })->implode('')
                        ),
                    ];
                })->values(),
            ];
        })->values();

        $data = new \stdClass;
        $data->movieTitle = $domainMovie->movie->title;
        $data->movieYear = $domainMovie->movie->year;
        $data->movieType = $domainMovie->movie->localed_type;
        $data->playlist = $playlist;

        Debugbar::stopMeasure('playlistForPlayerjs');

        return $data;
    }
}
