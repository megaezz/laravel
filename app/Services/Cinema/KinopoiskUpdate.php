<?php

namespace App\Services\Cinema;

use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Kinopoisk;
use engine\app\models\Engine;
use engine\app\models\F;

class KinopoiskUpdate
{
    public function __invoke($cron)
    {
        /* лимит апи - 20 запросов в секунду, делаем лимит 1000, чтобы было 10 запросов по 100 за раз, получается с запасом в 2 раза */
        $limit = 1000;
        $token = Config::cached()->kinopoiskApiUnofficialToken;
        $idFrom = $cron->temp_var ?? 1;
        $i = 0;
        $p = 0;
        $kinopoiskIds = [];
        $list = '';
        for ($id = $idFrom; $id < ($idFrom + $limit); $id++) {
            $i++;
            if ($i > 100) {
                $i = 1;
                $p++;
            }
            $kinopoiskIds[$p][] = $id;
        }
        foreach ($kinopoiskIds as $v) {
            $url = 'https://kinopoiskapiunofficial.tech/api/v3/films/'.implode(',', $v).'?append_to_response=RATING';
            // $url = 'https://kinopoiskapiunofficial.tech/api/v2.1/films/301?append_to_response=RATING';
            $object = new \stdClass;
            $object->url = $url;
            $object->curl = new \stdClass;
            $object->curl->httpheader = ['access-token: '.$token];
            $objects[] = $object;
        }
        $objects = F::multiCurl($objects);
        // F::dump($objects);
        $added = 0;
        foreach ($objects as $obj) {
            $result = json_decode($obj->result);
            if (isset($result->error)) {
                $list .= '<li style="color: red;">Ошибка запроса '.$obj->url.' ('.$result->error.')</li>';

                continue;
            }
            // F::dump($result);
            if (! isset($result->films)) {
                // слишком быстро, обычно когда перебираются много несущесвующих id подряд, тормозим и уходим
                usleep(1000000);

                return 'Притормозили';
                // F::alertLite(F::jsRedirect(F::referer_url(),1));
                // dd($result->films);
            }
            foreach ($result->films as $data) {
                $yearExplode = explode('-', $data->year);
                $yearFrom = null;
                $yearTo = null;
                if (count($yearExplode) > 1) {
                    $yearFrom = $yearExplode[0];
                    $yearTo = $yearExplode[1];
                } else {
                    $yearFrom = $data->year;
                }
                // F::dump($result);
                // F::dump($data);

                $kinopoisk = Kinopoisk::firstOrNew(['id' => $data->filmId]);
                $kinopoisk->rating = $data->rating;
                $kinopoisk->ratingVoteCount = $data->ratingVoteCount;
                $kinopoisk->yearFrom = $yearFrom ? (int) $yearFrom : null;
                $kinopoisk->yearTo = $yearTo ? (int) $yearTo : null;
                $kinopoisk->data = $data;
                $kinopoisk->save();

                $added++;
                $list .= '<li>'.$data->filmId.'</li>';
            }
        }
        $cron->temp_var = $id;
        $cron->save();

        if (! $added and $idFrom > (Kinopoisk::max('id') + 500000)) {
            // $arr = F::query_assoc('select max(id) as max_id from '.F::typetab('kinopoisk').';');
            // F::alertLite('База кинопоиска обновлена. Max(id) = '.$arr['max_id']);
            $cron->finished_at = now();
            $cron->temp_var = null;
            $cron->save();

            return 'Обновление завершено';
        }

        // <ol>'.$list.'</ol>
        return 'Добавлено '.$added.' материалов. Далее с: '.$id.', лимит: '.$limit.PHP_EOL.'Выполнено за: '.Engine::getRunningTime().' сек.'.PHP_EOL.'Время: '.Engine::getRunningTime().PHP_EOL.'Память: '.round(memory_get_usage(true) / 1024 / 1024).'mb';
        // sleep(1);
        /* 0.5 сек */
        usleep(500000);
        // return html_entity_decode(strip_tags($result));
        // F::alertLite($result);
    }
}
