<?php

namespace App\Services\Cinema;

use App\Jobs\LoadMemorandum;
use App\Models\Abuse;

class AddAbusesFromEmails
{
    public function __invoke($service, $text)
    {
        if ($service === 'dmca') {
            return $this->dmca($text);
        }

        if ($service === 'memorandum') {
            return $this->memorandum($text);
        }

        if ($service === 'ркн') {
            return $this->rkn($text);
        }

        return $this->any($service, $text);
    }

    public function dmca($text)
    {
        /* находим в тексте все наши ссылки */
        $pattern = '/(?<=\n)(http.*\:\/\/((?!kinopoisk|chillingeffects|lumendatabase|google|twitter|imdb|videomore|youtube|facebook|instagram|yandex|vk\.com|ok\.me|t\.me|eais|videx|ntv\.|webkontrol|vimeo|digiguardians|vindex|viasat|vipplay|ok\.ru|yadi\.sk|1plus1\.video|1tv\.ru|rutube\.ru|sudum\.net|starmediafilm\.ru|zetserial\.online|russia\.tv|smotrim\.ru).)+)\/(.*)(?=\n|$)/';

        preg_match_all($pattern, $text, $matches);

        /* создаем коллекцию из найденных ссылок и убираем дубли */
        $matches = collect($matches[0])->unique();

        $routes = [];

        foreach ($matches as $url) {

            $parsed_url = parse_url($url);

            /* собираем ссылку заново, учитывая что домен может быть написан по-русски и с пробелами, вызывая ошибку реквеста 404, path может отсутствовать */
            $url_formated = Abuse::formatUrl($parsed_url['scheme'].'://'.str_replace(' ', '', idn_to_ascii($parsed_url['host'])).($parsed_url['path'] ?? null));

            /* сохраняем ссылку в abuses */
            Abuse::firstOrCreate([
                'query' => $url_formated,
                'service' => 'dmca',
            ]);
        }

        return 'Получено '.count($matches).' ссылок';
    }

    public function memorandum($text)
    {
        $urls = collect(explode(PHP_EOL, $text))->map(fn ($url) => trim($url))->each(function ($url) {
            LoadMemorandum::dispatch($url);
        });

        return "Получено CSV файлов: {$urls->count()}";
    }

    public function rkn($text)
    {
        /* находим в тексте все наши ссылки */
        $pattern = '/https?\:\/\/((?!kinopoisk|google|twitter|imdb|videomore|youtube|facebook|instagram|yandex|vk\.com|ok\.me|t\.me|eais|videx|ntv\.|webkontrol|vimeo|digiguardians|vindex|viasat|vipplay|ok\.ru|yadi\.sk|1plus1\.video|1tv\.ru|rutube\.ru|sudum\.net|starmediafilm\.ru|zetserial\.online|russia\.tv|smotrim\.ru).)+[^\s]/';
        preg_match_all($pattern, $text, $matches);

        /* создаем коллекцию из найденных ссылок и убираем дубли */
        $matches = collect($matches[0])->unique();

        foreach ($matches as $url) {

            $parsed_url = parse_url($url);

            /* собираем ссылку заново, учитывая что домен может быть написан по-русски и с пробелами, вызывая ошибку реквеста 404, path может отсутствовать */
            $url_formated = Abuse::formatUrl($parsed_url['scheme'].'://'.str_replace(' ', '', idn_to_ascii($parsed_url['host'])).($parsed_url['path'] ?? null));

            /* сохраняем ссылку в abuses */
            Abuse::firstOrCreate([
                'query' => $url_formated,
                'service' => 'ркн',
            ]);
        }

        return 'Получено '.count($matches).' ссылок';
    }

    public function any($service, $text)
    {
        $pattern = '/https?:\/\/((?!(www\.)?(kinopoisk|google|twitter|imdb|videomore|youtube|facebook|instagram|yandex|vk\.com|ok\.me|t\.me|eais|videx|ntv\.|webkontrol|vimeo|digiguardians|vindex|viasat|vipplay|ok\.ru|yadi\.sk|1plus1\.video|1tv\.ru|rutube\.ru|sudum\.net|starmediafilm\.ru|zetserial\.online|russia\.tv|smotrim\.ru|drive\.vindex|drive\.google))[^\s]{21,})/';
        preg_match_all($pattern, $text, $matches);
        $arr = empty($matches[1]) ? throw new \Exception('No links found') : $matches[1];
        $inserted = 0;
        $ignored = 0;

        foreach ($arr as $url) {
            /* сохраняем ссылку в abuses */
            Abuse::firstOrCreate([
                'query' => $url,
                'service' => $service,
            ]);
        }

        return 'Получено '.count($arr).' ссылок';
    }
}
