<?php

namespace App\Services\Cinema\RouteHandlers;

use App\Models\Domain;
use engine\app\services\RegisterRoutesForDomainService;
use Illuminate\Http\Request;

/* определяет в какой обработчик передать реквест, Laravel или Megaweb */

class RequestHandlerResolver
{
    public function __invoke(Request $request, $service)
    {

        $handlerNamespace = '\App\Services\Cinema\RouteHandlers';

        /* получаем домен из реквеста */
        $domain = Domain::findOrFail($request->getHost());

        /* получаем роут по реквесту, передаем домен, чтобы избежать дублирования запросов */
        $route = RegisterRoutesForDomainService::getRouteByRequest($request, $domain);

        if (! $route) {
            return false;
        }

        /* если это нормальный роут (не any), то обработчиком будет одноименный метод */
        $exploded = explode('@', $route->getActionName());
        $controller = $exploded[0];
        $method = $exploded[1] ?? null;

        // не помню зачем я явно указал контроллеры и методы, убрал эту логику и перенес return в условие снизу, затестить так
        // if (
        // 	$controller == '\App\Http\Controllers\Types\Cinema\MainController' or
        // 	($controller == '\cinema\app\controllers\MainController' and ($method == 'index' or $method == 'top50' or $method == 'watch'))
        // ) {
        // 	return (new ("{$handlerNamespace}\Laravel"))->{$method}($request, $route, $domain, $service);
        // }

        /* если общий роут для v1, v2 то проверяем реквест по megaweb роутеру */
        if ($controller == 'engine\app\controllers\MegawebController') {

            /* проверяем на megaweb роут */
            $routeMegaweb = \engine\app\models\Router::getRouteByRequest($request);

            if (! $routeMegaweb) {
                throw new \Exception('Не найден megaweb роут для запроса');
            }

            /* если megaweb роута не существует, либо контроллера нет в списке, то мы не нашли обработчик */
            // if (!($routeMegaweb and in_array($routeMegaweb->getController(), ['Main', 'cinema\app\controllers\MainController']))) {
            // 	return false;
            // }

            /* возвращаем обработчик megaweb роута с одноименным методом */
            return (new ("{$handlerNamespace}\Megaweb"))->{$routeMegaweb->getMethod()}($request, $routeMegaweb, $domain, $service);

        } else {

            if (! $method) {
                throw new \Exception('Обработчик роутов с контроллером, но без метода - не реализован еще');
            }

            /* если не MegawebController, то возвращаем обработчик Laravel роутов */
            return (new ("{$handlerNamespace}\Laravel"))->{$method}($request, $route, $domain, $service);

        }

        return false;

    }
}
