<?php

namespace App\Services\Cinema\RouteHandlers;

use App\Models\Abuse;
use App\Models\Domain;
use cinema\app\models\eloquent\DomainGroup;
use cinema\app\models\eloquent\DomainMovie;
use cinema\app\models\eloquent\DomainTag;
use engine\app\models\F;
use engine\app\models\Router;
use engine\app\services\RegisterRoutesForDomainService;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class Dmca
{
    public function __call(string $method, $arguments)
    {
        throw new \Exception("Нет обработчика DMCA для роута {$method}");
    }

    public function movieView(Request $request, Route|Router $route, Domain $domain, DomainMovie $domainMovie)
    {

        if ($domain->primary_domain->cinemaDomain->show_trailer_for_non_sng) {
            return ['message' => 'стоит настройка показывать трейлеры для не снг стран, значит оспариваем жалобы в гугле и не меняем ссылки'];
        }

        /* формируем канонические ссылки */
        $canonicals = $domain->primary_domain->actual_crawler_mirrors->map(function ($mirror) use ($domainMovie) {
            return Abuse::formatUrl($mirror->toRoute->absolute()->movieView($domainMovie));
        });

        $requested = Abuse::formatUrl($request->url());

        if ($canonicals->contains($requested)) {
            $domainMovie->dmca = F::generateDmca();
            $domainMovie->save();

            /* снова формировать канонические ссылки, чтобы показать результат, придумать как это сделать полаконничне, скорее всего надо вынести генерацию канонических ссылок в отдельный метод/сервис */

            return ['message' => 'каноническая ссылка, меняем DMCA'];
        }

        return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не меняем DMCA'];
    }

    public function groupView(Request $request, Route|Router $route, Domain $domain, DomainGroup $domainGroup)
    {

        if ($domain->primary_domain->cinemaDomain->show_trailer_for_non_sng) {
            return ['message' => 'стоит настройка показывать трейлеры для не снг стран, значит оспариваем жалобы в гугле и не меняем ссылки'];
        }

        /* формируем канонические ссылки */
        $canonicals = $domain->primary_domain->actual_crawler_mirrors->map(function ($mirror) use ($domainGroup) {
            // return $mirror->domain . $domain->toRoute->groupView($domainGroup->group);
            return Abuse::formatUrl($mirror->toRoute->absolute()->groupView($domainGroup->group));
        });

        $requested = Abuse::formatUrl($request->url());

        if ($canonicals->contains($requested)) {
            $domainGroup->dmca = F::generateDmca();
            $domainGroup->save();

            return ['message' => 'каноническая ссылка, меняем DMCA'];
        }

        return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не меняем DMCA'];
    }

    public function genreView(Request $request, Route|Router $route, Domain $domain, DomainTag $domainTag)
    {

        if ($domain->primary_domain->cinemaDomain->show_trailer_for_non_sng) {
            return ['message' => 'стоит настройка показывать трейлеры для не снг стран, значит оспариваем жалобы в гугле и не меняем ссылки'];
        }

        /* формируем канонические ссылки */
        $canonicals = $domain->primary_domain->actual_crawler_mirrors->map(function ($mirror) use ($domainTag) {
            return Abuse::formatUrl($mirror->toRoute->absolute()->genreView($domainTag->tag));
        });

        $requested = Abuse::formatUrl($request->url());

        if ($canonicals->contains($requested)) {
            $domainTag->dmca = F::generateDmca();
            $domainTag->save();

            return ['message' => 'каноническая ссылка, меняем DMCA'];
        }

        return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не меняем DMCA'];
    }

    public function index(Request $request, Route $route, Domain $domain)
    {

        if ($domain->primary_domain->cinemaDomain->show_trailer_for_non_sng) {
            return ['message' => 'стоит настройка показывать трейлеры для не снг стран, значит оспариваем жалобы в гугле и не меняем ссылки'];
        }

        // $canonical = $domain->toRoute->index();

        /* если канонический адрес - просто слеш, то убираем его, т.к ларавель убирает его в реквесте */
        // if ($canonical == '/') {
        // 	$canonical = '';
        // }

        /* формируем канонические ссылки */
        $canonicals = $domain->primary_domain->actual_crawler_mirrors->map(function ($mirror) {
            // return $mirror->domain . $canonical;
            return Abuse::formatUrl($mirror->toRoute->absolute()->index());
        });

        $requested = Abuse::formatUrl($request->url());

        if ($canonicals->contains($requested)) {
            $routeSettings = RegisterRoutesForDomainService::getRouteSettings($route);
            $routeSettings->slug = F::generateDmca();
            $routeSettings->save();

            return ['message' => 'каноническая ссылка, меняем DMCA'];
        }

        return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не меняем DMCA'];
    }

    public function top50(Request $request, Route $route, Domain $domain)
    {

        if ($domain->primary_domain->cinemaDomain->show_trailer_for_non_sng) {
            return ['message' => 'стоит настройка показывать трейлеры для не снг стран, значит оспариваем жалобы в гугле и не меняем ссылки'];
        }

        // $canonical = $domain->toRoute->top50();

        /* формируем канонические ссылки */
        $canonicals = $domain->primary_domain->actual_crawler_mirrors->map(function ($mirror) {
            // return $mirror->domain . $canonical;
            return Abuse::formatUrl($mirror->toRoute->absolute()->top50());
        });

        $requested = Abuse::formatUrl($request->url());

        if ($canonicals->contains($requested)) {
            $routeSettings = RegisterRoutesForDomainService::getRouteSettings($route);
            $routeSettings->slug = F::generateDmca();
            $routeSettings->save();

            return ['message' => 'каноническая ссылка, меняем DMCA'];
        }

        return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не меняем DMCA'];
    }
}
