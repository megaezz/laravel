<?php

namespace App\Services\Cinema\RouteHandlers;

use App\Models\Abuse;
use Illuminate\Support\Facades\Request;

class AbuseHandler
{
    public function __invoke($limit = 100)
    {

        $handlers = [
            'dmca' => \App\Services\Cinema\RouteHandlers\Dmca::class,
            'memorandum' => \App\Services\Cinema\RouteHandlers\Dmca::class,
            'ркн' => \App\Services\Cinema\RouteHandlers\Rkn::class,
        ];

        $links = Abuse::whereIn('service', array_keys($handlers))->whereHandled(false)->limit($limit)->get();

        foreach ($links as $link) {

            // dump("Обрабатываем ссылку: $link->query");

            /* создаем реквест с данным URL, чтобы проверить наличие реквеста в зарегистрированных роутах */
            $request = Request::create('https://'.$link->query, 'GET');

            $handler = false;

            try {
                $handler = (new RequestHandlerResolver)($request, $handlers[$link->service]);
            } catch (\Exception $e) {
                $link->handler_error = true;
                $link->handler_message = $e->getMessage();
            }

            if ($handler) {
                $link->handler_message = $handler['message'];
            } else {
                $link->handler_error = true;
                if (! $link->handler_message) {
                    $link->handler_message = 'роут не найден';
                }
            }

            $link->handled = true;
            $link->save();
        }

        return $links;
    }
}
