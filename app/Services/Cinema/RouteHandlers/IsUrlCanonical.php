<?php

namespace App\Services\Cinema\RouteHandlers;

use App\Models\Abuse;
use App\Models\Domain;
use cinema\app\models\eloquent\DomainMovie;
use engine\app\models\Router;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class IsUrlCanonical
{
    public function __call(string $method, $arguments)
    {
        throw new \Exception("Нет обработчика проверки ссылки на каноничность для роута {$method}");
    }

    public function movieView(Request $request, Route|Router $route, Domain $domain, DomainMovie $domainMovie)
    {

        /* формируем канонические ссылки */
        $canonicals = $domain->primary_domain->actual_crawler_mirrors->map(function ($mirror) use ($domainMovie) {
            return Abuse::formatUrl($mirror->toRoute->absolute()->movieView($domainMovie));
        });

        $requested = Abuse::formatUrl($request->url());

        return [
            'result' => $canonicals->contains($requested) ? true : false,
            'canonicals' => $canonicals,
        ];
    }
}
