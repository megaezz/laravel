<?php

namespace App\Services\Cinema\RouteHandlers;

use App\Models\Abuse;
use App\Models\Domain;
use cinema\app\models\eloquent\DomainMovie;
use cinema\app\models\eloquent\MovieEpisode;
use engine\app\models\Router;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class Rkn
{
    public function __call(string $method, $arguments)
    {
        throw new \Exception("Нет обработчика РКН для роута {$method}");
    }

    public function movieView(Request $request, Route|Router $route, Domain $domain, DomainMovie $domainMovie)
    {

        /* формируем канонические ссылки */
        $canonicals = $domain->primary_domain->actual_crawler_mirrors->map(function ($mirror) use ($domainMovie) {
            return Abuse::formatUrl($mirror->toRoute->absolute()->movieView($domainMovie));
        });

        $requested = Abuse::formatUrl($request->url());

        $movie = $domainMovie;

        /* если плеер уже удален то уходим */
        if ($movie->deleted) {
            return ['message' => 'плеер уже удален, без действий', 'domain' => $domain];
        }

        /* если домен помечен как забаненный, то ничего не нужно удалять, уходим */
        /* TODO: теперь могут быть разные домены под гугл и яндекс, поэтому считаем домен в бане, если все актуальные зеркала для ботов в известном бане, в дальнейшем продумать как сделать чтобы для гугла не удалялось а для яндекса удалялось, хотя в принципе сейчас удалений не происходит, т.к. все сайты на схеме "жалобы на страницу", поэтому может и не надо */
        if ($domain->primary_domain->every_actual_crawler_mirror_in_known_ru_block) {
            $movie->rkn = true;
            $movie->save();

            return ['message' => 'домен в бане, плеер не удаляем', 'domain' => $domain];
        }

        /* проверяем нет ли на домене настройки удалять плеер только если есть жалоба ркн на страницу */
        if ($domain->primary_domain->cinemaDomain->rkn_scheme == 'delete_player_when_ru_and_not_canonical') {
            /* ничего не удалять, просто решить, менять ссылку или нет. если переданный uri совпадает с текущим, то меняем dmca */
            if ($canonicals->contains($requested)) {
                $movie->dmca = $movie->generateDmca();
                $movie->save();

                return ['message' => 'каноническая ссылка, меняем DMCA', 'domain' => $domain, 'dmca' => true];
            } else {
                return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не меняем DMCA', 'domain' => $domain];
            }
        }

        /* проверяем нет ли на домене настройки удалять плеер только если есть жалоба ркн на страницу */
        if (in_array($domain->primary_domain->cinemaDomain->rkn_scheme, ['delete_player_only_if_abuse_on_page', 'delete_player_if_ru_and_abuse_on_page'])) {
            /* ничего не удалять, просто решить, менять ссылку или нет. если переданный uri совпадает с текущим, то меняем dmca */
            if ($canonicals->contains($requested)) {
                $movie->dmca = $movie->generateDmca();
                $movie->save();

                return ['message' => 'каноническая ссылка, удаляем плеер по этой ссылке и меняем DMCA', 'domain' => $domain, 'dmca' => true];
            } else {
                if ($domain->primary_domain->actual_crawler_mirrors->pluck('domain')->contains($domain->domain)) {
                    return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), основа, удаляем плеер по ссылке, не меняем DMCA', 'domain' => $domain, 'main_domain' => true];
                } else {
                    return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не основа, не удаляем плеер по ссылке, не меняем DMCA', 'domain' => $domain];
                }
            }
        }

        /* проверяем нет ли на домене настройки удалять плеер только с основы */
        if ($domain->primary_domain->cinemaDomain->rkn_scheme == 'delete_player_only_if_main_domain') {
            /* если запрошенный домен - основной, то удаляем плеер, если нет, то ничего не делаем */
            if ($domain->primary_domain->actual_crawler_mirrors->pluck('domain')->contains($domain->domain)) {
                $movie->deleted = true;
                $movie->rkn = true;
                $movie->save();

                return ['message' => 'основа, удаляем плеер', 'domain' => $domain, 'deleted' => true];
            } else {
                /* если запрошенный домен не основа, значит можно не удалять */
                $movie->rkn = true;
                $movie->save();

                return ['message' => 'не основа, не удаляем плеер', 'domain' => $domain];
            }
        }

        /* если ничего из этого нет, то удаляем плеер */
        $movie->deleted = true;
        $movie->rkn = true;
        $movie->save();

        return ['message' => 'удалили', 'domain' => $domain, 'deleted' => true];
    }

    public function episodeView(Request $request, Route $route, Domain $domain, DomainMovie $domainMovie, MovieEpisode $episode)
    {

        /* формируем канонические ссылки */
        $canonicals = $domain->primary_domain->actual_crawler_mirrors->map(function ($mirror) use ($domainMovie, $episode) {
            return Abuse::formatUrl($mirror->toRoute->absolute()->episodeView($domainMovie, $episode));
        });

        $requested = Abuse::formatUrl($request->url());

        $movie = $domainMovie;

        /* если плеер уже удален то уходим */
        if ($movie->deleted) {
            return ['message' => 'плеер уже удален, без действий', 'domain' => $domain];
        }

        /* если домен помечен как забаненный, то ничего не нужно удалять, уходим */
        /* TODO: теперь могут быть разные домены под гугл и яндекс, поэтому считаем домен в бане, если все актуальные зеркала для ботов в известном бане, в дальнейшем продумать как сделать чтобы для гугла не удалялось а для яндекса удалялось, хотя в принципе сейчас удалений не происходит, т.к. все сайты на схеме "жалобы на страницу", поэтому может и не надо */
        if ($domain->primary_domain->every_actual_crawler_mirror_in_known_ru_block) {
            $movie->rkn = true;
            $movie->save();

            return ['message' => 'домен в бане, плеер не удаляем', 'domain' => $domain];
        }

        /* проверяем нет ли на домене настройки удалять плеер только если есть жалоба ркн на страницу */
        if ($domain->primary_domain->cinemaDomain->rkn_scheme == 'delete_player_when_ru_and_not_canonical') {
            /* ничего не удалять, просто решить, менять ссылку или нет. если переданный uri совпадает с текущим, то меняем dmca */
            if ($canonicals->contains($requested)) {
                $movie->dmca = $movie->generateDmca();
                $movie->save();

                return ['message' => 'каноническая ссылка, меняем DMCA', 'domain' => $domain, 'dmca' => true];
            } else {
                return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не меняем DMCA', 'domain' => $domain];
            }
        }

        /* проверяем нет ли на домене настройки удалять плеер только если есть жалоба ркн на страницу */
        if (in_array($domain->primary_domain->cinemaDomain->rkn_scheme, ['delete_player_only_if_abuse_on_page', 'delete_player_if_ru_and_abuse_on_page'])) {
            /* ничего не удалять, просто решить, менять ссылку или нет. если переданный uri совпадает с текущим, то меняем dmca */
            if ($canonicals->contains($requested)) {
                $movie->dmca = $movie->generateDmca();
                $movie->save();

                return ['message' => 'каноническая ссылка, удаляем плеер по этой ссылке и меняем DMCA', 'domain' => $domain, 'dmca' => true];
            } else {
                if ($domain->primary_domain->actual_crawler_mirrors->pluck('domain')->contains($domain->domain)) {
                    return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), основа, удаляем плеер по ссылке, не меняем DMCA', 'domain' => $domain, 'main_domain' => true];
                } else {
                    return ['message' => 'не каноническая ссылка ('.$canonicals->implode(', ').'), не основа, не удаляем плеер по ссылке, не меняем DMCA', 'domain' => $domain];
                }
            }
        }

        /* проверяем нет ли на домене настройки удалять плеер только с основы */
        if ($domain->primary_domain->cinemaDomain->rkn_scheme == 'delete_player_only_if_main_domain') {
            /* если запрошенный домен - основной, то удаляем плеер, если нет, то ничего не делаем */
            if ($domain->primary_domain->actual_crawler_mirrors->pluck('domain')->contains($domain->domain)) {
                $movie->deleted = true;
                $movie->rkn = true;
                $movie->save();

                return ['message' => 'основа, удаляем плеер', 'domain' => $domain, 'deleted' => true];
            } else {
                /* если запрошенный домен не основа, значит можно не удалять */
                $movie->rkn = true;
                $movie->save();

                return ['message' => 'не основа, не удаляем плеер', 'domain' => $domain];
            }
        }

        /* если ничего из этого нет, то удаляем плеер */
        $movie->deleted = true;
        $movie->rkn = true;
        $movie->save();

        return ['message' => 'удалили', 'domain' => $domain, 'deleted' => true];

    }
}
