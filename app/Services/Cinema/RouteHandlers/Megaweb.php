<?php

namespace App\Services\Cinema\RouteHandlers;

use App\Models\Domain;
use cinema\app\models\eloquent\DomainGroup;
use cinema\app\models\eloquent\DomainTag;
use cinema\app\models\eloquent\Group;
use cinema\app\models\eloquent\Tag;
use engine\app\models\Engine;
use engine\app\models\Router;
use Illuminate\Http\Request;

class Megaweb
{
    public function __construct()
    {
        /* TODO: убрать говнокод */
        /* иначе megaweb->getGroupLink, megaweb->getGenreLink приводят к ошибке */
        Engine::setType('cinema');
    }

    /* если нужного метода нет, значит не нужно никакой дополнительно обработки реквеста - просто пробрасываем его дальше */
    public function __call(string $method, $arguments)
    {
        return (new $arguments[3])->{$method}($arguments[0], $arguments[1], $arguments[2]);
    }

    public function watch(Request $request, Router $route, Domain $domain, $service)
    {

        if (! isset($route->getVars()['movieID'])) {
            throw new \Exception('movieID отсутствует в роуте');
        }

        if ($domain->primary_domain->cinemaDomain->useMovieId) {
            $domainMovie = $domain->primary_domain->cinemaDomain->movies()->whereMovieId($route->getVars()['movieID'])->first();
        } else {
            $domainMovie = $domain->primary_domain->cinemaDomain->movies()->whereId($route->getVars()['movieID'])->first();
        }

        if (empty($domainMovie)) {
            throw new \Exception('Видео не найдено для домена');
        }

        return (new $service)->movieView($request, $route, $domain, $domainMovie);
    }

    public function groupCinema(Request $request, Router $route, Domain $domain, $service)
    {

        if (! isset($route->getVars()['groupID'])) {
            throw new \Exception('groupID отсутствует в роуте');
        }

        $group = Group::whereId($route->getVars()['groupID'])->first();

        if (empty($group)) {
            throw new \Exception('Группа не найдена');
        }

        /* если нет domainGroup, то создаем */

        $domainGroup = $domain->primary_domain->cinemaDomain->groups()->whereGroupId($group->id)->first();

        if (! $domainGroup) {
            $domainGroup = new DomainGroup;
            $domainGroup->group_id = $group->id;
            $domainGroup->domain = $domain->primary_domain->domain;
            $domainGroup->save();
        }

        return (new $service)->groupView($request, $route, $domain, $domainGroup);
    }

    public function genreCinema(Request $request, Router $route, Domain $domain, $service)
    {

        if (! isset($route->getVars()['tagId'])) {
            throw new \Exception('tagId отсутствует в роуте');
        }

        $tag = Tag::whereId($route->getVars()['tagId'])->first();

        if (empty($tag)) {
            throw new \Exception('Тег не найден');
        }

        /* если нет domainTag, то создаем */

        $domainTag = $domain->primary_domain->cinemaDomain->tags()->whereTagId($tag->id)->first();

        if (! $domainTag) {
            $domainTag = new DomainTag;
            $domainTag->tag_id = $tag->id;
            $domainTag->domain = $domain->primary_domain->domain;
            $domainTag->save();
        }

        return (new $service)->genreView($request, $route, $domain, $domainTag);
    }
}
