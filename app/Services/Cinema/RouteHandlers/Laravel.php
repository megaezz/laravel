<?php

namespace App\Services\Cinema\RouteHandlers;

use App\Models\Domain;
use cinema\app\models\eloquent\DomainGroup;
use cinema\app\models\eloquent\Group;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class Laravel
{
    /* если нет обработчика то прокидываем запрос дальше */
    public function __call(string $method, $arguments)
    {
        return (new $arguments[3])->{$method}($arguments[0], $arguments[1], $arguments[2]);
    }

    public function movieView(Request $request, Route $route, Domain $domain, $service)
    {

        $domainMovie = $domain->primary_domain->cinemaDomain->movies()->whereId($route->parameter('id'))->first();

        if (empty($domainMovie)) {
            throw new \Exception('Видео не найдено для домена');
        }

        return (new $service)->{__FUNCTION__}($request, $route, $domain, $domainMovie);

    }

    /* здесь обрабатываются эпизоды, т.к. они смотрят в megaweb MainController в метод watch через laravel роутер */
    public function watch(Request $request, Route $route, Domain $domain, $service)
    {

        $episode = $domain->primary_domain->cinemaDomain->episodes()->whereId($route->parameter('id'))->first();

        if (empty($episode)) {
            throw new \Exception('Эпизод не найден для домена');
        }

        $domainMovie = $domain->primary_domain->cinemaDomain->movies()->whereMovieId($episode->movie_id)->first();

        if (empty($domainMovie)) {
            throw new \Exception('Фильм не найден для эпизода');
        }

        return (new $service)->episodeView($request, $route, $domain, $domainMovie, $episode);

    }

    public function groupView(Request $request, Route $route, Domain $domain, $service)
    {

        $group = Group::whereId($route->parameter('id'))->first();

        if (empty($group)) {
            throw new \Exception('Группа не найдена');
        }

        /* если нет domainGroup, то создаем */

        $domainGroup = $domain->primary_domain->cinemaDomain->groups()->whereGroupId($group->id)->first();

        if (! $domainGroup) {
            $domainGroup = new DomainGroup;
            $domainGroup->group_id = $group->id;
            $domainGroup->domain = $domain->primary_domain->domain;
            $domainGroup->save();
        }

        return (new $service)->groupView($request, $route, $domain, $domainGroup);

    }
}
