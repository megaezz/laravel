<?php

namespace App\Services\Cinema;

use cinema\app\models\eloquent\Domain;
use cinema\app\models\eloquent\DomainMovie;
use cinema\app\models\eloquent\Movie;

class AddDomainMovies
{
    // пробегаем по всем видео и добавляем их если они еще не добавлены к доменам, поддерживающим автодобавление
    public function __invoke(int $limit, ?int $lastId = 0)
    {

        $movies = Movie::query()
            ->with('tags')
            ->limit($limit)
            ->where('type', '!=', 'youtube')
            ->where('id', '>', $lastId)
            ->orderBy('id')
            ->get();

        $domains = Domain::query()
            ->with('engineDomain')
        /* экономим запросы для ->every_actual_crawler_mirror_in_known_ru_block */
            ->with('engineDomain.redirectTo')
            ->with('engineDomain.yandexRedirectTo')
            ->whereRelation('engineDomain', 'type', 'cinema')
            ->whereMoviesWithUniqueDescription(false)
            ->get();

        $actions = [];

        foreach ($movies as $movie) {

            foreach ($domains as $domain) {

                /* если moviesPerDay не 0 и не NULL или уже достигнуто максимальное кол-во, то пропускаем */
                if (! $domain->moviesPerDay or ($domain->moviesAddedPerDay >= $domain->moviesPerDay)) {
                    $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'пропускаем из-за превышения лимита'];

                    continue;
                }

                /* существует ли уже такой фильм для домена */
                $domainMovie = DomainMovie::whereMovieId($movie->id)->whereDomain($domain->domain)->first();

                /* подходит ли фильм домену */
                $isMovieAllowed = $domain->isMovieAllowed($movie);
                $actions[$movie->id]['domains'][$domain->domain]['allowed'] = $isMovieAllowed;

                if ($domainMovie) {

                    /* если фильм был ранее удален из-за ркн но сайт забанен, то восстанавливаем */
                    /* TODO: если например домен для гугла забанен в рф, а для яндекса нет, то все равно видео для яндекса восстановлены не будут, подумать */
                    if ($domain->engineDomain->every_actual_crawler_mirror_in_known_ru_block) {

                        if ($domainMovie->deleted and $domainMovie->rkn) {
                            $domainMovie->deleted = false;
                            $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'был удален плеер, вернули', 'color' => 'green'];
                        }

                    }

                    if ($isMovieAllowed['result']) {

                        if ($domainMovie->active) {
                            $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'существует и активно'];
                        } else {
                            $domainMovie->active = true;
                            $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'существует, активировали', 'color' => 'green'];
                        }

                    } else {

                        if ($domainMovie->active) {
                            $domainMovie->active = false;
                            $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'существует, деактивировали', 'color' => 'red'];
                        } else {
                            $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'существует и неактивно'];
                        }

                    }

                    $domainMovie->save();

                } else {

                    if (! $isMovieAllowed['result']) {
                        $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'пропускаем'];

                        continue;
                    }

                    /* создаем фильм для домена */
                    $createdDomainMovie = new DomainMovie;
                    $createdDomainMovie->movie_id = $movie->id;
                    $createdDomainMovie->active = true;

                    /* если установлен флаг useAltDescription, то ищем рандомное альтернативное описание */
                    if ($domain->useAltDescriptions) {
                        if ($createdDomainMovie->random_alternative_description) {
                            $createdDomainMovie->description = $createdDomainMovie->random_alternative_description;
                            $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'нашли альтернативное описание', 'color' => 'green'];
                        } else {
                            $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'не нашли альтернативное описание', 'color' => 'red'];

                        }
                    }

                    /* добавляем созданный фильм для домена */
                    $domain->movies()->save($createdDomainMovie);

                    /* инкремент счетчика добавленных фильмов */
                    $domain->moviesAddedPerDay = $domain->moviesAddedPerDay + 1;
                    $domain->save();

                    $actions[$movie->id]['domains'][$domain->domain]['messages'][] = ['message' => 'добавили', 'color' => 'green'];
                }
            }

            $actions[$movie->id]['model'] = $movie;
        }

        return ['lastId' => ($movie->id ?? null), 'actions' => collect($actions)];

        // return view('types.cinema.admin.add-movies')
        // ->with('actions', $actions)
        // ->with('resetCheckers', $resetCheckers)
        // ->with('moviesRows', $moviesRows)
        // ->with('limit', $limit)
        // ->with('jsRedirect', F::jsRedirect("/?r=admin/Actions/addMoviesToDomainMovies&limit={$limit}", 2));
    }
}
