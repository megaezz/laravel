<?php

namespace App\Services;

use GuzzleHttp\Client;

class UptimeKumaUpdate
{
    public $clientSettings;

    public $requestSettings;

    /* все ок работает, данные подтягиваются, только почему то выводятся даже удаленные мониторинги, да и из полезной инфы только статус 0 - 1 и среднее время ответа на запрос, потом продолжить если апи улучшится */
    public function __invoke()
    {
        // Установите базовый URL вашего Uptime Kuma сервера и API-ключ
        $this->apiBaseUrl = 'https://uptime.awmzone.net';
        $this->apiKey = 'uk1_vPOrGJXmZOVmbd3NnEjkIk3k6gX0-uZ-3OY4unzD';

        // Создаем экземпляр Guzzle клиента
        $this->client = new Client([
            'base_uri' => $this->apiBaseUrl,
        ]);

        $response = $this->client->get('/metrics', [
            'auth' => ['', $this->apiKey],
        ]);

        $data = $response->getBody()->getContents();

        $metrics = collect($this->parseMetrics($data));

        dd($metrics);
        dd($metrics->where('name', 'monitor_status'));
    }

    protected function parseMetrics($rawData)
    {
        $lines = explode("\n", $rawData);
        $metrics = [];

        foreach ($lines as $line) {
            $line = trim($line);

            // Пропускаем пустые строки и комментарии
            if (empty($line) || strpos($line, '#') === 0) {
                continue;
            }

            // Разделяем строку на метрику и значение
            if (preg_match('/^([a-zA-Z_:][a-zA-Z0-9_:]*)(?:\{(.*)\})?\s+([0-9e\+\-\.]+)$/', $line, $matches)) {
                $metricName = $matches[1];
                $metricValue = (float) $matches[3];

                $labels = [];
                if (! empty($matches[2])) {
                    // Разделяем лейблы
                    foreach (explode(',', $matches[2]) as $label) {
                        [$key, $value] = explode('=', $label);
                        $labels[$key] = trim($value, '"');
                    }
                }

                $metrics[] = [
                    'name' => $metricName,
                    'labels' => $labels,
                    'value' => $metricValue,
                ];
            }
        }

        return $metrics;
    }
}
