<?php

declare(strict_types=1);

namespace App\Services;

use App\Jobs\Retranslator;
use App\Models\TelegramMessage;
use danog\MadelineProto\EventHandler\Attributes\Handler;
use danog\MadelineProto\EventHandler\Message;
use danog\MadelineProto\EventHandler\Plugin\RestartPlugin;
use danog\MadelineProto\EventHandler\SimpleFilter\Incoming;
use danog\MadelineProto\SimpleEventHandler;

class TelegramEventHandler extends SimpleEventHandler
{
    // !!! Change this to your username !!!
    public const ADMIN = '@saulgoodie';

    /**
     * Get peer(s) where to report errors.
     */
    public function getReportPeers()
    {
        return [self::ADMIN];
    }

    /**
     * Returns a set of plugins to activate.
     *
     * See here for more info on plugins: https://docs.madelineproto.xyz/docs/PLUGINS.html
     */
    public static function getPlugins(): array
    {
        return [
            // Offers a /restart command to admins that can be used to restart the bot, applying changes.
            // Make sure to run in a bash while loop when running via CLI to allow self-restarts.
            RestartPlugin::class,
        ];
    }

    /**
     * Handle incoming updates from users, chats and channels.
     */
    #[Handler]
    public function handleMessage(Incoming&Message $message): void
    {

        // сохраняем все подряд
        $telegramMessage = TelegramMessage::fillFromMadeline($message);
        $telegramMessage->save();

        // отправляем все подряд, а там он разберется
        Retranslator::dispatch($telegramMessage);

    }
}
