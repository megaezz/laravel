<?php

namespace App\Services\Parsers;

use App\Models\Doctor\StomFirmsClinic;
use App\Models\ParsedUrl;
use App\Models\Proxy;
use DiDom\Document;

class StomFirms
{
    public function __invoke($lastUrl)
    {

        /* используем российский активный прокси */
        $proxy = Proxy::where('country', 'RU')->whereActive(true)->first()->proxy;

        if (! $proxy) {
            throw new \Exception('Нет нужного прокси');
        }

        /* короче блять нахуй не использовать base_uri в Guzzle, потому что потом открывается крысиная нора из проблем, например нужно жутко костылить, чтобы получить полный адрес внутри блока then асинхронного запроса и прочая гадость */
        /* с www т.к. в сайтмепах указан с www, чтобы одинаковые домены были потом в базе для сайтмэпа и остальных страниц */
        /* сделать чтобы парсились все поддомены (www, msk и другие города) */
        $baseUrl = 'https://msk.stom-firms.ru';

        /* настраиваем парсер страниц на использование прокси и использовать сохраненные страницы */
        $pageParser = ParsedUrl::parse();
        $pageParser->clientSettings = ['proxy' => $proxy];
        $pageParser->useStoredData = true;

        /* настраиваем парсер сайтмэп на использование прокси и использовать сохраненную страницу если это первое выполнение и не использовать если повторные */
        $sitemapParser = ParsedUrl::parse();
        $sitemapParser->clientSettings = ['proxy' => $proxy];
        $sitemapParser->useStoredData = $lastUrl ? true : false;

        /* получаем и парсим страницу сайтмэп */
        try {
            $sitemapPage = new Document($sitemapParser->parse("{$baseUrl}/sitemap.xml")->first()->content);
        } catch (\Exception $e) {
            logger()->alert(__METHOD__." ошибка при получении sitemap.xml: {$e->getMessage()}");

            return false;
        }

        /* получаем все ссылки из сайтмэпа */
        $urls = collect($sitemapPage->find('url loc'));

        // dd($urls->first()->text());

        $urlsToBeParsed = [];
        $foundLastPage = false;
        $limit = 100;

        /* находим последнюю обработанную ссылку и берем $limit следующих ссылок */
        foreach ($urls as $url) {
            /* если последняя обработанная страница еще не найдена, то занимаемся в цикле лишь этим, пока не найдем */
            if (! $foundLastPage) {
                /* если есть lastUrl то ждем пока не дойдем до нужной ссылки */
                if ($lastUrl) {
                    if ($url->text() == $lastUrl) {
                        $foundLastPage = true;
                    }

                    /* не имеет разницы нашли или не нашли нужную ссылку, нам в обоих случаях нужен continue */
                    continue;
                } else {
                    /* если lastUrl не указана, то берем ссылки из начала */
                    $foundLastPage = true;
                }
            }

            $urlsToBeParsed[] = $url->text();
            if (count($urlsToBeParsed) == $limit) {
                break;
            }
        }

        /* парсим все полученные страницы */
        $parsedPages = $pageParser->parse($urlsToBeParsed);

        /* если закончились страницы для парсинга, то готово */
        $finished = $parsedPages->count() ? false : true;
        /* записываем последнюю загруженную страницу если она есть */
        $lastUrl = collect($urlsToBeParsed)->last();

        return ['finished' => $finished, 'data' => $lastUrl];
    }

    /* записать все клиники в бд */
    public function fillClinicsFromParsedUrls()
    {

        $parsedUrls = ParsedUrl::query()
            ->whereNotIn('id', function ($query) {
                $query
                    ->select('parsed_url_id')
                    ->from('doctor.stom_firms_clinics');
            })
            ->where('domain', 'msk.stom-firms.ru')
            ->where('url', 'like', '%clinics.php%')
            ->limit(500)
            ->get();

        foreach ($parsedUrls as $parsedUrl) {
            $document = new Document($parsedUrl->content);
            // preg_match('/i=([\d]+)$/', $parsedUrl->url, $matches);
            // $stomId = $matches[1];
            // $clinic = \App\Models\Types\Engine\Clinic::where('stom_id', $stomId)->first();
            // if (!$clinic) {
            // $clinic = new \App\Models\Types\Engine\Clinic;
            // }
            $clinic = new StomFirmsClinic;
            $clinic->parsed_url_id = $parsedUrl->id;
            $clinic->phone = $document->find('#firmPhone .commonText__phone')[0]->getAttribute('title');
            $clinic->address = $document->find('span[itemprop="address"]')[0]->text();
            $clinic->name = $document->find('h1[itemprop="name"]')[0]->text();
            $clinic->save();

            dump($clinic->toArray());
        }

        dd('done');
    }
}
