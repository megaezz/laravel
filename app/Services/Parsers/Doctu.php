<?php

namespace App\Services\Parsers;

use App\Models\ParsedUrl;
use App\Models\Proxy;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Facades\Storage;

class Doctu
{
    public function __invoke($data)
    {

        /* страниц пагинации за раз */
        $limitPages = 2;
        /* элементов на странице пагинации */
        /* !! не изменять, если берем сохраненные страницы */
        $limitItemsPerPage = 1000;
        $usePaginationStoredData = true;
        $useFullPageStoredData = true;

        /* используем российский активный прокси */
        $proxy = Proxy::where('country', 'LV')->whereActive(true)->first()?->proxy;

        if (! $proxy) {
            throw new \Exception('Нет нужного прокси');
        }

        $endpoints = collect([
            (object) [
                'endpoint' => 'clinics/getList',
                'full' => (object) [
                    'endpoint' => 'clinics/read/{id}',
                    'method' => 'get',
                    'pagination' => false,
                ],
                'method' => 'post',
                'model' => \App\Models\Doctor\Clinic::class,
            ],
            (object) [
                'endpoint' => 'networks/getList',
                'full' => (object) [
                    'endpoint' => 'networks/read/{id}',
                    'method' => 'post',
                    'pagination' => false,
                ],
                'method' => 'post',
                'model' => \App\Models\Doctor\Network::class,
            ],
            (object) [
                'endpoint' => 'doctors/getList',
                'full' => (object) [
                    'endpoint' => 'doctors/read/{id}',
                    'method' => 'get',
                    'pagination' => false,
                ],
                'method' => 'post',
                'model' => \App\Models\Doctor\Doctor::class,
            ],
            (object) [
                'endpoint' => 'pricelists/doctu/getList',
                'full' => (object) [
                    'endpoint' => 'pricelists/doctu/{id}/getLinesWithTags',
                    'method' => 'post',
                    'pagination' => true,
                ],
                'method' => 'post',
                'model' => \App\Models\Doctor\DoctuPricelist::class,
            ],
            (object) [
                'endpoint' => 'pricelists/medflex/getList',
                'full' => (object) [
                    'endpoint' => 'pricelists/medflex/{id}/getLines',
                    'method' => 'post',
                    'pagination' => true,
                ],
                'method' => 'post',
                'model' => \App\Models\Doctor\MedflexPricelist::class,
            ],
            // таймаут
            // (object)[
            // 	'endpoint' => 'orders/calltracking/getList',
            // 	'method' => 'post',
            // ],
            (object) [
                'endpoint' => 'orders/calltracking-reports/getList',
                'method' => 'post',
                'model' => \App\Models\Doctor\CalltrackingReport::class,
            ],
            (object) [
                'endpoint' => 'moderation/reviews/getList',
                'method' => 'post',
                'model' => \App\Models\Doctor\Review::class,
            ],
            (object) [
                'endpoint' => 'moderation/reviewComments/getList',
                'method' => 'post',
                'model' => \App\Models\Doctor\ReviewComment::class,
            ],
            (object) [
                'endpoint' => 'showcards/getList',
                'method' => 'post',
                'model' => \App\Models\Doctor\Showcard::class,
            ],
            (object) [
                'endpoint' => 'settings/mapspam/getList',
                'method' => 'post',
                'model' => \App\Models\Doctor\Mapspam::class,
            ],
        ]);

        /* если данных нет, то загружаем все файлы сейчас */
        if (empty($data)) {
            // $this->saveJsons();
        }

        /* если данные не переданы, заполняем начальными, если данные в виде строки, значит надо раcкодировать */
        $data = $data ? (is_string($data) ? json_decode($data) : (object) $data) : (object) ['endpoint' => $endpoints->first()->endpoint, 'page' => 1];
        $endpoint = $endpoints->where('endpoint', $data->endpoint)->first();
        $pages = collect(range($data->page, $data->page + $limitPages - 1));

        $urls = [];
        foreach ($pages as $page) {
            /* тут не обязательно указывать ?page=, т.к. в пост запросе тоже передается номер страницы, но в бд будет перезаписываться одна и та же страница, поэтому добавляем номер */
            $urls[] = "https://manager.doctu.ru/admin/{$endpoint->endpoint}?page={$page}";
        }

        $cookies = [
            'cf_clearance' => 'QWkNLfIdv4nuJipPSl0eISHLukIu13YC2bFZWq5DIYQ-1724677770-1.2.1.1-rEXwkgtZFsovIdLbGjOAspBIJ_oIape5vg5C7oQ1KRyzQrOLq5EsSrlsTiwT6V6qnt1ASaICrD9ahTIvDpALoBkzfwMBV6PYhSYky4WuaDnOmKVG3e2NpngKUBoNGJQUiSInMFqK8Zphj2Bnz5_BoF_K02IQafOjvsdiGyQ3z8Y.t0U7n7cG24IoCtKsPCdK2UB8KBLBWtUSaxH.MIH3534dfjMwo224nO3FLnrj9qEnK0v_Fx4IqscaVSfTN32BazsUDtXWlWRL8PHqL2QxQtVqHRYWpXz1XUh43dHy7d0.ErOImJT16qGA6CDcIj53klAibnmZk7UKQbMA_zxEO4s.wbRddMSuk5LjDUydA36zApguTQJSKMQO1Ylr8yh2cbEBnkedmG79FcE2u8brhyKJAgvMRJVTpZcriD4wO5ibh.JTS2B22K3oCr6kBa_Z',
            'doctu_session' => 'eyJpdiI6IklRUFN2cm1WSmhwenpjdzBIMm82UUE9PSIsInZhbHVlIjoic3JWZzNaNm5FZ1l6dy90WExWNHV3NDN5ZjJUUlR5U0l2SlMvbDY2OFZiaUNtcHBPYzcvRTJ5eEJBWGVWSklmL1A5UjJHci9sMk5UbU14ekx3WTJpUkd4TXFOVTBQbmUrdHFxeGZWSDR0YWlSL0hOUlc1S2V5M0dOZGpMbjFVbW8iLCJtYWMiOiJhMzk3M2ZhYmI4ZmVmMTkzOGYzMWE0OTYzMjJkNGI1ZWJkMTZiZTczNDI0OTc0MGQzN2M5MjhlNDM4NTkyNGMzIiwidGFnIjoiIn0%3D',
            'XSRF-TOKEN' => 'eyJpdiI6Ijh3dXRIQ3ZVWDJMUnNGNk1uMTB2MXc9PSIsInZhbHVlIjoiaVd4OVA3eUxjVml4RkZiNk05UDNnZTV4ZFVWOFp4eXNTeDhRcjR2am5XMnNjSGJOb2l6T2pOMVFXWWJiWjdKcDByY3hkbjliQnBzc0hHcEpHRGVkUExta3BBYTFrbURSMjArQmdZOUF2bDVkbXlScnI5TmxvMWRLaUxQdEFyUUMiLCJtYWMiOiIwNTIyNjA1MWZlZmI5Y2MxMzhjN2I3YjI2N2EzZDY3YzFkNjcxNDdhYWRkMjU3YjJhNzQwZTYzOWUyN2FhMzdlIiwidGFnIjoiIn0=',
        ];

        // Создаем CookieJar с заданными куками
        $cookieJar = CookieJar::fromArray($cookies, 'manager.doctu.ru');

        /* настраиваем парсер страниц на использование прокси и использовать сохраненные страницы */
        $pageParser = ParsedUrl::parse();
        $pageParser->method = $endpoint->method ?? 'get';
        $pageParser->useStoredData = $usePaginationStoredData;
        $pageParser->clientSettings = [
            /* такой таймаут из за долгой загрузки страниц пагинации Reviews */
            'timeout' => 60,
            'proxy' => $proxy,
            'cookies' => $cookieJar,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15',
                'Content-Type' => 'application/json',
                'Sec-Fetch-Dest' => 'empty',
                'Pragma' => 'no-cache',
                'Accept' => 'application/json, text/javascript, */*; q=0.01',
                'Sec-Fetch-Site' => 'same-origin',
                'Cache-Control' => 'no-cache',
                'Sec-Fetch-Mode' => 'cors',
                'Accept-Language' => 'en-GB,en;q=0.9',
                // 'Referer' => 'https://manager.doctu.ru/admin/settings/mapspam',
                'Accept-Encoding' => 'gzip, deflate, br',
                'X-Requested-With' => 'XMLHttpRequest',
                'AdditionalIdentifier' => '7',
                'X-XSRF-TOKEN' => $cookies['XSRF-TOKEN'],
            ],
        ];

        /* если post запрос то добавляем json данные в запрос */
        if ($pageParser->method == 'post') {
            $pageParser->clientSettings['json'] = [
                'filter' => [],
                'order' => [],
                'params' => [],
                /* убираем, т.к. через get передается норм, а иначе он слушается данных из post и всегда одинаковые страница при асинхронных запросах */
                // 'page' => $data->page,
                'limit' => $limitItemsPerPage,
                'lazycount' => true,
            ];
        }

        $parsedPages = $pageParser->parse($urls);

        // dd($urls, $parsedPages);

        /* если у эндпоинта есть расширенный эндпоинт - обрабатываем его тоже */
        if (isset($endpoint->full->endpoint)) {
            /* формируем ссылки на расширенные данные */
            $fullUrls = [];
            foreach ($parsedPages as $parsedPage) {
                foreach ($parsedPage->content->data as $item) {
                    $fullUrls[] = 'https://manager.doctu.ru/admin/'.str_replace('{id}', $item->id, $endpoint->full->endpoint);
                }
            }
            /* меняем настройку лимита, чтобы загрузились сразу все расширенные данные */
            $pageParser->clientSettings['json']['limit'] = 10000;
            /* меняем метод на нужный */
            $pageParser->method = $endpoint->full->method;
            $parsedFullPages = collect();
            collect($fullUrls)->chunk(1000)->each(function ($urls, $index) use (&$parsedFullPages, $pageParser) {
                $parsedFullPages = $parsedFullPages->merge($pageParser->parse($urls));
                // dd($urls, $parsedFullPages);
                // dump($index);
            });
        }

        /* отдаем страницы обработчику */
        $pagesSaver = $this->pageSaver(pages: $parsedPages, fullPages: ($parsedFullPages ?? []), endpoint: $endpoint);

        $finished = false;
        $data->page = $pages->last() + 1;

        /* если мало данных, значит с этим эндпоинтом закончили */
        if (count($parsedPages->last()->content->data) < $limitItemsPerPage) {
            /* если текущий эндпоинт последний, то финиш */
            if ($endpoints->last()->endpoint == $data->endpoint) {
                $finished = true;
            } else {
                /* если не последний, то переходим к следующему */
                /* сначала находим индекс текущего эндпоинта */
                $index = $endpoints->search(function ($item) use ($data) {
                    return $item->endpoint === $data->endpoint;
                });
                /* и берем следующий от него */
                $data->endpoint = $endpoints->get($index + 1)->endpoint;
                $data->page = 1;
            }
        }

        /* приводим $data к массиву, иначе модель ругается, потом все равно приведем к объекту */
        $result = ['finished' => $finished, 'data' => (array) $data];

        // dump('элементов', count($parsedPages->last()->content->data));
        // dd($result);
        // dump($urls);

        // dd('ok');
        // dd($parsedPage->content, $parsedPage->headers, $result);

        return $result;
    }

    public function pageSaver($pages, $fullPages, $endpoint)
    {

        /* подготавливаем список id чтобы загрузить модели одним запросом, т.к. по одной почему-то очень долго */
        foreach ($pages as $page) {
            foreach ($page->content->data as $item) {
                $modelIds[] = $item->id;
            }
        }

        /* загружаем сразу все модели одним запросом */
        $models = $endpoint->model::whereIn('doctu_id', $modelIds)->get();

        foreach ($pages as $page) {

            foreach ($page->content->data as $item) {

                // $model = $endpoint->model::where('doctu_id', $item->id)->first();
                $model = $models->where('doctu_id', $item->id)->first();

                if (! $model) {
                    $model = new $endpoint->model;
                    $model->doctu_id = $item->id;
                }

                $model->data = $item;

                /* если для эндпоинта есть расширенный эндпоинт, то ищем его среди $fullPages и сохраняем */
                if (isset($endpoint->full->endpoint)) {
                    $fullUrl = 'https://manager.doctu.ru/admin/'.str_replace('{id}', $item->id, $endpoint->full->endpoint);
                    $fullPage = $fullPages->where('url', $fullUrl)->first();
                    $model->data_wide = $fullPage->content;
                }

                $model->save();
            }
        }

        return true;

    }

    public function saveJsons()
    {
        $jsons = [
            [
                'file' => 'area.json',
                'model' => \App\Models\Doctor\Area::class,
                'doctu_id' => 'value',
            ],
            [
                'file' => 'cities.json',
                'model' => \App\Models\Doctor\City::class,
                'doctu_id' => 'value',
            ],
            [
                'file' => 'clinicType.json',
                'model' => \App\Models\Doctor\ClinicType::class,
                'doctu_id' => 'value',
            ],
            [
                'file' => 'district.json',
                'model' => \App\Models\Doctor\District::class,
                'doctu_id' => 'value',
            ],
            [
                'file' => 'doctorDeletionReasons.json',
                'model' => \App\Models\Doctor\DoctorDeleteReason::class,
                'doctu_id' => 'code',
            ],
            [
                'file' => 'doctorSpeciality.json',
                'model' => \App\Models\Doctor\DoctorSpecialty::class,
                'doctu_id' => 'value',
            ],
            [
                'file' => 'locality.json',
                'model' => \App\Models\Doctor\Locality::class,
            ],
            [
                'file' => 'metros.json',
                'model' => \App\Models\Doctor\Metro::class,
                'doctu_id' => 'value',
            ],
            [
                'file' => 'servicesTree.json',
                'model' => \App\Models\Doctor\Service::class,
                'doctu_id' => 'id',
            ],
            // [
            // 	'file' => 'user.json',
            // 	'model' => \App\Models\Doctor\User::class,
            // ],
        ];

        foreach ($jsons as $json) {

            $datas = json_decode(Storage::get("doctor/{$json['file']}"));

            foreach ($datas as $data) {

                $model = $json['model']::where('data', json_encode($data))->first();

                if (! $model) {
                    $model = new $json['model'];
                    $model->doctu_id = $model->$json['doctu_id'];
                    $model->data = $data;
                    $model->save();
                }

            }
        }

        return true;
    }
}
