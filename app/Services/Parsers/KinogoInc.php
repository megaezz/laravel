<?php

namespace App\Services\Parsers;

use App\Models\ParsedUrl;
use App\Models\Proxy;
use cinema\app\models\eloquent\Alloha;
use cinema\app\models\eloquent\Hdvb;
use DiDom\Document;

class KinogoInc
{
    public function __invoke($pageNum)
    {
        /* используем казахский активный прокси */
        $proxy = Proxy::where('country', 'UA')->whereActive(true)->first()->proxy;

        if (! $proxy) {
            throw new \Exception('Нет нужного прокси');
        }

        /* настраиваем парсер фильмов на использование украинского прокси и использовать сохраненные страницы */
        $movieParser = ParsedUrl::parse();
        $movieParser->clientSettings = ['proxy' => $proxy, 'base_uri' => 'https://kinogo.inc'];
        $movieParser->useStoredData = true;

        /* настраиваем парсер страниц пагинации на использование украинского прокси и НЕ использовать сохраненные страницы */
        $paginationParser = ParsedUrl::parse();
        $paginationParser->clientSettings = ['proxy' => $proxy, 'base_uri' => 'https://kinogo.inc'];
        $paginationParser->useStoredData = false;

        /* получаем номер следующей страницы пагинации */
        $pageNum = $pageNum + 1;

        /* получаем страницу пагинации */
        $paginationParsedUrl = $paginationParser->parse("https://kinogo.inc/page/{$pageNum}")->first();

        if ($paginationParsedUrl->error) {
            logger()->alert(__METHOD__." ошибка при получении пагинации: {$paginationParsedUrl->error}");

            return ['finished' => false, 'data' => $pageNum - 1];
            // throw new \Exception(__METHOD__ . " ошибка при получении пагинации: {$paginationParsedUrl->error}");
        }

        $paginationPage = new Document($paginationParsedUrl->content);

        /* ищем все фильмы на странице пагинации */
        $shortStories = $paginationPage->find('.shortstory');

        $movieUrls = [];

        /* собираем массив со ссылками на страницы фильмов */
        foreach ($shortStories as $shortStorie) {
            $movieUrls[] = $shortStorie->first('.zagolovki a')->getAttribute('href');
        }

        /* парсим сразу все страницы фильмов */
        $movies = $movieParser->parse($movieUrls);

        foreach ($movies as $parsedMovie) {
            if (empty($parsedMovie->content)) {
                logger()->alert(__METHOD__.": Нет контента: {$parsedMovie->url}, пропускаем");

                continue;
            }
            $movieDom = new Document($parsedMovie->content);
            $players = $movieDom->find('#contentss .tabs li');
            foreach ($players as $player) {
                $iframeLink = $player->getAttribute('data-src');

                /* это аллоха */
                if (strstr($iframeLink, '/?token_movie=')) {
                    preg_match('/\?token_movie=(.+)&token/', $iframeLink, $matches);
                    $token = $matches[1] ?? null;
                    $alloha = $token ? Alloha::find($token) : null;
                    if ($alloha) {
                        $parsedMovie->movie_id_alloha = $alloha->movie_id;
                    }
                }

                /* это hdvb */
                if (strstr($iframeLink, 'vid1719968931')) {
                    preg_match('/movie\/(.+)\/iframe/', $iframeLink, $matches);
                    $token = $matches[1] ?? null;
                    $hdvb = $token ? Hdvb::find($token) : null;
                    if ($hdvb) {
                        $parsedMovie->movie_id_hdvb = $hdvb->movie_id;
                    }
                }

                /* прописываем результирующее значение movie_id */
                $parsedMovie->movie_id = $parsedMovie->movie_id_hdvb ?? $parsedMovie->movie_id;
                $parsedMovie->movie_id = $parsedMovie->movie_id_alloha ?? $parsedMovie->movie_id;

                /* какая гениальная идея заложена здесь? не дописал? */
                if ($parsedMovie->movie_id_hdvb == $parsedMovie->movie_id_alloha) {
                }
            }

            $parsedMovie->save();
        }

        /* если последний элемент пагинации - span, то мы на последней странице */
        /* иногда последняя страница пустая, поэтому еще считаем последней страницей если null */
        $isLastPage = in_array($paginationPage->first('.bot-navigation')?->lastChild()?->tagName(), ['span', null]);

        /* обходим косяк с 12й страницей */
        if ($isLastPage and $pageNum == 12) {
            $isLastPage = false;
        }

        return ['finished' => $isLastPage, 'data' => $pageNum];
    }
}
