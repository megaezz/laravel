<?php

namespace App\Services;

use DiDom\Document;
use GuzzleHttp\Client;

class IsDuckDuckGoBot
{
    public function __invoke($ip)
    {

        $cacheKey = 'duckduckgo_ips';

        /* кешируем на сутки код, получающий актуальные IP ботов DuckDuckGo */
        $ips = \Cache::remember($cacheKey, 86400, function () use ($cacheKey) {
            try {
                $client = new Client;
                $response = $client->get('http://duckduckgo.com/duckduckbot.html');
                $page = new Document($response->getBody()->getContents());
                $ipList = $page->find('.main ul li');
                $ips = array_map(function ($item) {
                    return $item->text();
                }, $ipList);
                if (empty($ips)) {
                    throw new \Exception('Пустой список IP');
                }

                return $ips;
            } catch (\Exception $e) {
                /* TODO: предыдущее значение кеша не выдается */
                /* если выпало исключение, уведомляем об этом громко и пробуем оставить предыдущее значение кеша, если оно есть */
                logger()->alert("Не удалось получить список IP DuckDuckGo: {$e->getMessage()}");

                return \Cache::get($cacheKey, []);
            }
        });

        if (in_array($ip, $ips)) {
            return true;
        }

        return false;
    }
}
