<?php

namespace App\Services\Tests;

use Tests\TestCase;

class Clients
{
    /**
     * Create a new class instance.
     */
    public function __construct(private TestCase $testCase) {}

    public function googleBot()
    {
        return $this->testCase
            ->withHeaders([
                'User-Agent' => 'Googlebot',
                'CF-Connecting-IP' => '66.249.66.1',
            ])
            ->withServerVariables([
                'REMOTE_ADDR' => '1.2.3.4',
            ]);
    }

    public function yandexBot()
    {
        return $this->testCase
            ->withHeaders([
                'User-Agent' => 'YandexBot',
                'CF-Connecting-IP' => '5.255.253.42',
            ])
            ->withServerVariables([
                'REMOTE_ADDR' => '1.2.3.4',
            ]);
    }

    public function client()
    {
        return $this->testCase
            ->withHeaders([
                'User-Agent' => 'Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Mobile Safari/537.36',
                'CF-Connecting-IP' => '1.2.3.4',
            ])
            ->withServerVariables([
                'REMOTE_ADDR' => '1.2.3.4',
            ]);
    }
}
