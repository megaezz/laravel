<?php

namespace App\Services;

use App\Models\ParsedUrl;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;

class ParseUrls
{
    public $clientSettings;

    public $requestSettings;

    public $useStoredData;

    public $method;

    /* TODO:
    возможность загрузки списка ссылок порциями по limit штук,
    возможность указать requestSettings для каждой url, т.е. скорее всего вместо url нужно передавать $request'ы
    */

    public function __construct($clientSettings = [], $requestSettings = [], $useStoredData = false, $method = 'get')
    {
        $this->clientSettings = $clientSettings;
        $this->requestSettings = $requestSettings;
        $this->useStoredData = $useStoredData;
        $this->method = $method;
    }

    public function parse($urls)
    {

        $urls = is_string($urls) ? [$urls] : $urls;

        $clientSettings = [
            'timeout' => 5,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4.1 Safari/605.1.15',
            ],
        ];

        /* заменяем дефолтные настройки кастомными */
        $clientSettings = array_merge($clientSettings, $this->clientSettings);

        $client = new Client($clientSettings);

        $requestSettings = [
            // 'allow_redirects' => [
            // 	'max' => 0,
            // ],
        ];

        /* заменяем дефолтные настройки кастомными */
        $requestSettings = array_merge($requestSettings, $this->requestSettings);

        $results = [];
        $promises = [];

        foreach ($urls as $url) {

            $parsedUrl = ParsedUrl::where('url', $url)->first();

            /* если страница существует, она не с ошибкой и стоит настройка использовать сохраненные данные, то просто возвращаем модель */
            if ($parsedUrl and ! $parsedUrl->error and $this->useStoredData) {
                $results[] = $parsedUrl;

                continue;
            }

            /* если страницы нет, то создаем модель */
            if (! $parsedUrl) {
                $parsedUrl = new ParsedUrl;
            }

            $method = "{$this->method}Async";

            $promises[] = $client->$method($url, $requestSettings)->then(

                function ($response) use ($url, $parsedUrl, &$results) {
                    /* короче блять нахуй не использовать base_uri в Guzzle, потому что потом открывается крысиная нора из проблем, например нужно жутко костылить, чтобы получить полный адрес внутри блока then асинхронного запроса и прочая гадость */
                    /* вот этот заеб нужен блять чтобы получить полный URL внутри замыкания, если используется базовый адрес, по-другому блять никак, а если используется базовый адрес, но $url все равно введен полностью? то все блять приехали? */
                    // $baseUri = (string)$client->getConfig()['base_uri'];
                    // if ($baseUri) {
                    // 	$fullUrl = rtrim($baseUri, '/') . '/' . ltrim($url, '/');
                    // } else {
                    // 	$fullUrl = $url;
                    // }
                    $parsedUrl->domain = parse_url($url)['host'];
                    $parsedUrl->url = $url;
                    $parsedUrl->headers = $response->getHeaders();
                    /* записываем контент в переменную, т.к. последующие вызовы ->getContents() вернут [], мда уж */
                    $content = $response->getBody()->getContents();
                    $decodedJson = json_decode($content);
                    $isJson = (json_last_error() === JSON_ERROR_NONE);
                    if ($isJson) {
                        $parsedUrl->content = $decodedJson;
                    } else {
                        $parsedUrl->content = $content;
                    }
                    $parsedUrl->error = null;
                    $parsedUrl->save();
                    $results[] = $parsedUrl;
                },

                /* TODO: что делаем в случае ошибки? */
                function ($reason) use ($url, $parsedUrl, &$results) {
                    $parsedUrl->domain = parse_url($url)['host'];
                    $parsedUrl->url = $url;
                    $parsedUrl->headers = null;
                    $parsedUrl->content = null;
                    $parsedUrl->error = $reason->getMessage();
                    $parsedUrl->save();
                    $results[] = $parsedUrl;
                    // throw new \Exception($reason->getMessage());
                    // $results[] = false;
                },

            );
        }
        /* выполняем все асинхронные запросы */
        Utils::settle($promises)->wait();

        return collect($results);
    }
}
