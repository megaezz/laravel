<?php

namespace App\Services;

use GuzzleHttp\Client;

class Aria2
{
    protected $client;

    protected $rpcUrl;

    public function __construct($rpcUrl)
    {
        $this->client = new Client;
        $this->rpcUrl = 'http://aria2:6800/jsonrpc';
    }

    public function base($method, $gid = null)
    {
        return [
            'json' => [
                'jsonrpc' => '2.0',
                'method' => "aria2.{$method}",
                'params' => [
                    'token:P3TERX',
                ],
                'id' => 1,
            ],
        ];
    }

    public function call($options)
    {
        $response = $this->client->post($this->rpcUrl, $options);
        $body = $response->getBody();
        $data = json_decode($body, true);

        return $data;
    }

    public function status($gid)
    {
        $options = $this->base('tellStatus');
        $options['json']['params'][] = $gid;

        return $this->call($options);
    }

    // удаляет загрузку, если она активна (вместе с файлами)
    public function remove($gid)
    {
        $options = $this->base('remove');
        $options['json']['params'][] = $gid;

        return $this->call($options);
    }

    // просто очищает завершённую задачу, не удаляет с диска
    public function removeDownloadResult($gid)
    {
        $options = $this->base('removeDownloadResult');
        $options['json']['params'][] = $gid;

        return $this->call($options);
    }

    public function addByUrl($url)
    {
        $options = $this->base('addUri');
        $options['json']['params'][] = [$url];

        return $this->call($options);
    }

    public function active()
    {
        $options = $this->base('tellActive');

        return $this->call($options);
    }

    public function waiting()
    {
        $options = $this->base('tellWaiting');
        $options['json']['params'][] = 0;
        $options['json']['params'][] = 1000;

        return $this->call($options);
    }

    public function stopped()
    {
        $options = $this->base('tellStopped');
        $options['json']['params'][] = 0;
        $options['json']['params'][] = 1000;

        return $this->call($options);
    }

    public function changeOptions($gid, array $changedOptions)
    {
        $options = $this->base('changeOption');
        $options['json']['params'][] = $gid;
        // $options['json']['params'][] = json_encode($changedOptions);
        $options['json']['params'][] = $changedOptions;

        // dd($options);

        return $this->call($options);
    }
}
