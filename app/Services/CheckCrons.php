<?php

namespace App\Services;

use App\Models\Schedule;

class CheckCrons
{
    public function __invoke()
    {

        $schedules = Schedule::where('active', true)->get();

        $withDelay = $schedules->whereNotNull('next_launch_delay')->whereNotNull('finished_at');

        $staleCrons = collect();

        foreach ($withDelay as $schedule) {
            /* предполагаем что следующий финиш должен быть не позже чем дата текущего финиша + время задержки + 1 день + еще 6 часов (добавил из-за cinema:update-kinopoisk-videos, он не успевает завершиться) */
            $nextFinishShouldBeAt = $schedule->finished_at->addMinutes($schedule->next_launch_delay)->addDay()->addMinutes(360);
            /* если текущая дата позже чем предполагаемый следующий финиш, значит задание тормознуло, нужно разобраться */
            if (now() > $nextFinishShouldBeAt) {
                $staleCrons[] = $schedule;
            }
        }

        return [
            'staleCrons' => $staleCrons,
            'errorCrons' => $schedules->where('done', false),
        ];
    }
}
