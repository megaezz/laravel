<?php

namespace App\Services;

use App\Models\Config;
use App\Models\Schedule as ModelsSchedule;
use Illuminate\Support\Facades\Schedule;

class RegisterSchedules
{
    public function register()
    {

        /* получаем все активные задания, для которых проставлен period */
        $schedules = ModelsSchedule::where('active', true)->where('done', true)->get();

        foreach ($schedules as $schedule) {

            /* если движок отключен - тормозим с заданиями, оставляя только те, которые помечены как switched_off_rinning = 1 */
            if ((! Config::cached()->switch or ! Config::cached()->cronSwitch or ! Config::cached()->crons_enabled) and ! $schedule->even_in_maintenance_mode) {
                continue;
            }

            // without_overlapping - добавить

            /* создаем расписание с командой */
            Schedule::command($schedule->command)
                /* какой период */
                // ->{$cron->period}(collect(['dailyAt'])->contains($cron->period) ? $cron->at : null)
                ->{$schedule->period}()
                /* даем имя */
                ->name($schedule->command)
                /* выполняем только на проде, но если dev=1, то на local тоже */
                ->environments($schedule->dev ? ['production', 'local'] : ['production'])
                // ->before(function () use ($cron) {
                //     // dump('before');
                //     logger()->info('before');
                //     $cron->done = false;
                //     $cron->save();
                // })
                ->onSuccess(function () use ($schedule) {
                    // dump('onSuccess');
                    $schedule->last_time = now();
                    // $cron->done = true;
                    $schedule->save();
                })
                ->onFailure(function () use ($schedule) {
                    // dump('onFailure');
                    // logger()->info('onFailure');
                    $schedule->done = false;
                    $schedule->save();
                })
                /* команды всегда выполняем в фоне, потому что можем */
                ->runInBackground()
                ->onOneServer();
        }
    }
}
