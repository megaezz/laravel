<?php

namespace App\Services\Crons\Engine;

class ParseSites
{
    public function __invoke($parserClass, $data)
    {

        $parsers = collect([
            (object) [
                'parser' => \App\Services\Parsers\KinogoInc::class,
                'on' => true,
            ],
            (object) [
                'parser' => \App\Services\Parsers\StomFirms::class,
                'on' => false,
            ],
            (object) [
                'parser' => \App\Services\Parsers\Doctu::class,
                'on' => false,
            ],
        ]);

        /* если парсер передан - проверяем наличие парсера в доступных, если не передан - берем первый доступный */
        if ($parserClass) {
            $parser = $parsers->where('on', true)->where('parser', $parserClass)->first();
        } else {
            $parser = $parsers->where('on', true)->first();
        }

        if (! $parser) {
            throw new \Exception("Парсер {$parserClass} не найден в доступных парсерах");
        }

        $result = (new $parser->parser)($data);

        /* если результат finished, то либо переходим к следующему доступному парсеру, либо заканчиваем */
        if ($result['finished']) {
            $currentKey = $parsers->search($parser);
            /* получаем следующий доступный парсер */
            $parser = $parsers->where('on', true)->slice($currentKey + 1, 1)->first();
            /* если следующий доступный парсер существует, то меняем finished на false */
            if ($parser) {
                $result['finished'] = false;
            }
        }

        return ['parser' => $parser?->parser, 'result' => $result];
    }
}
