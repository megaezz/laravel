<?php

namespace App\Services\Crons\Engine;

use App\Models\DnsProvider;

class CloudflareUpdate
{
    public function __invoke()
    {

        /* обновляем 10 давно не обновленных аккаунтов */
        $accounts = DnsProvider::where('service', 'cloudflare')->orderBy('updated_at')->limit(10)->get();

        foreach ($accounts as $account) {
            $account->zones = $account->dns()->zones();
            /* принудительно дергаем модель, чтобы изменилось updated_at даже в случае отсутствия изменений в моделе */
            $account->touch();
            $account->save();
        }

        return true;
    }
}
