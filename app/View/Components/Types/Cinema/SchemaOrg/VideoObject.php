<?php

namespace App\View\Components\Types\Cinema\SchemaOrg;

use App\Models\Domain;
use cinema\app\models\eloquent\DomainMovie;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class VideoObject extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(public DomainMovie $domainMovie, public Domain $requestedDomain) {}

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.types.cinema.schema-org.video-object');
    }

    public function video()
    {
        $videoObject = new \stdClass;
        $videoObject->{'@context'} = 'https://schema.org';
        $videoObject->{'@type'} = 'VideoObject';
        $videoObject->name = $this->domainMovie->movie->title;
        $movieDescriptionOrDomainMovieDescriptionShort = Str::limit($this->domainMovie->description ?? $this->domainMovie->movie->description, 160);
        $videoObject->description = $movieDescriptionOrDomainMovieDescriptionShort ? $movieDescriptionOrDomainMovieDescriptionShort : $this->domainMovie->movie->title;
        $videoObject->thumbnailUrl = asset($this->domainMovie->movie->poster_or_placeholder);
        $videoObject->uploadDate = $this->domainMovie->add_date->toIso8601String();
        $videoObject->embedUrl = $this->requestedDomain->toRoute->absolute()->embedPlayers(['domainMovie' => $this->domainMovie->id]);

        return json_encode($videoObject, JSON_UNESCAPED_UNICODE);
    }

    public function movie()
    {
        $movieObject = new \stdClass;
        $movieObject->{'@context'} = 'https://schema.org';
        $movieObject->{'@type'} = 'Movie';
        $movieObject->name = $this->domainMovie->movie->title;
        $movieObject->image = asset($this->domainMovie->movie->poster_or_placeholder);
        $movieObject->dateCreated = $this->domainMovie->add_date->toIso8601String();
        $movieObject->url = $this->requestedDomain->toRoute->absolute()->movieView($this->domainMovie);
        $directors = $this->domainMovie->movie->tags->where('type', 'director');
        if ($directors) {
            $movieObject->director = new \stdClass;
            $movieObject->director->{'@type'} = 'Person';
            $movieObject->director->name = $directors->implode(', ');
        }
        $votes = $this->domainMovie->movie->kinopoisk_votes + $this->domainMovie->movie->imdb_votes;
        if ($votes) {
            $movieObject->aggregateRating = new \stdClass;
            $movieObject->aggregateRating->{'@type'} = 'AggregateRating';
            $movieObject->aggregateRating->bestRating = 10;
            $movieObject->aggregateRating->worstRating = 0;
            $movieObject->aggregateRating->ratingValue = round($this->domainMovie->movie->rating_sko, 1);
            $movieObject->aggregateRating->ratingCount = $votes;
            // $movieObject->aggregateRating->reviewCount = 10;
        }

        return json_encode($movieObject, JSON_UNESCAPED_UNICODE);
    }
}
