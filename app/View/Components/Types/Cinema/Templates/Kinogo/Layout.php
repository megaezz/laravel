<?php

namespace App\View\Components\Types\Cinema\Templates\Kinogo;

use Closure;
use engine\app\models\Engine;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Layout extends Component
{
    public $domain;

    public $requestedDomain;

    public $lastYearFilmsGroup;

    public $foreignShowsGroup;

    /**
     * Create a new component instance.
     */
    public function __construct(public $headers)
    {
        $this->domain = Engine::getDomainEloquent();
        $this->requestedDomain = Engine::getRequestedDomainEloquent();
        // TODO - сделать это нормально
        /* пиздец, пробовал это сделать через вычисляемый метод, но какого-то хрена если тут нет результата, то возвращается InvokableComponentVariable и никак не проверить это на существование, поэтому лучше нахрен свойствами делать */
        $this->lastYearFilmsGroup = $this->domain->cinemaDomain->mergedGroups()->whereIn('groups.name', ['Фильмы '.now()->year, 'Фильмы 2024'])->orderByDesc('groups.name')->first();
        $this->foreignShowsGroup = $this->domain->cinemaDomain->mergedGroups()
            ->where('groups.name', 'Зарубежные сериалы')
            ->cacheFor(now()->addDays(1))
            ->first();

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.types.cinema.templates.kinogo.layout');
    }

    public function topMovies()
    {
        $movies = $this->domain->cinemaDomain->movies()
            ->with('movie')
            ->select('domain_movies.*')
            ->join('cinema.cinema', 'cinema.id', '=', 'domain_movies.movie_id')
            ->whereIn('cinema.year', [now()->year, now()->subYear()->year])
            ->where('cinema.deleted', false)
            ->where('cinema.poster', true)
            ->where('domain_movies.active', true)
            ->where('domain_movies.deleted', false)
            ->where('domain_movies.deleted_page', false)
            ->orderByDesc('cinema.year')
            ->orderByDesc('cinema.rating_sko')
            ->orderByDesc('cinema.imdb_votes')
            ->limit(7)
            ->cacheFor(now()->addDays(1))
            ->get();

        return $movies;
    }

    public function countryGroups()
    {
        $countries = ['США', 'Великобритания', 'Россия', 'Индия', 'Германия', 'Франция', 'Казахстан', 'Украина'];

        return $this->domain->cinemaDomain
            ->mergedGroups()
            ->whereIn('groups.name', $countries)
            ->orderByRaw("FIELD(groups.name, '".implode("', '", $countries)."')")
            ->cacheFor(now()->addDays(1))
            ->get();
    }

    public function genres()
    {
        // таких нет
        // Ожидаемые фильмы, Дорамы
        $genres = ['Биографические', 'Боевики', 'Вестерны', 'Военные', 'Детективы', 'Документальные', 'Драмы', 'Исторические', 'Комедии', 'Криминальные', 'Мелодрамы', 'Мультфильмы', 'Приключения', 'Семейные', 'Триллеры', 'Ужасы', 'Фантастика', 'Фэнтези', 'Ожидаемые фильмы', 'Аниме', 'Дорамы', 'Детские', 'Спортивные', 'Реалити-шоу'];

        return $this->domain->cinemaDomain
            ->mergedGroups()
            ->whereIn('groups.plural', $genres)
            ->orderByRaw("FIELD(groups.plural, '".implode("', '", $genres)."')")
            ->cacheFor(now()->addDays(1))
            ->get();
    }

    public function showCountryGroups()
    {
        $countries = ['Русские сериалы', 'Зарубежные сериалы', 'Турецкие сериалы'];

        return $this->domain->cinemaDomain
            ->mergedGroups()
            ->whereIn('groups.name', $countries)
            ->orderByRaw("FIELD(groups.name, '".implode("', '", $countries)."')")
            ->cacheFor(now()->addDays(1))
            ->get();
    }

    public function mostWanted()
    {
        return $this->domain->cinemaDomain->movies()
            ->whereRelation('movie', 'year', now()->year)
            ->orderByDesc('add_date')
            ->limit(2)
            ->cacheFor(now()->addDays(1))
            ->get();
    }

    public function lastEpisodes()
    {
        return $this->domain->cinemaDomain->movies()
            ->with([
                'episodes' => function ($query) {
                    $query
                        ->where('air_date', '<=', now()->today())
                        ->orderByDesc('air_date')
                        ->cacheFor(now()->addDays(1));
                },
                'movie' => function ($query) {
                    $query->cacheFor(now()->addDays(1));
                },
            ])
            ->whereIn('movie_id',
                $this->domain->cinemaDomain->episodes()
                    ->select('movie_id')
                    ->where('air_date', '<=', now()->today())
                    ->orderByDesc('air_date')
                    ->limit(10)
                    ->cacheFor(now()->addDays(1))
                    ->get()
            )
            ->cacheFor(now()->addDays(1))
            ->get();
    }

    public function lastComments()
    {
        return $this->domain->cinemaDomain
            ->movieComments()
            ->orderByDesc('date')
            ->limit(5)
            ->cacheFor(now()->addDays(1))
            ->get();
    }

    /* сделал до 2020 потому что первый сайт на этом шаблоне заполнялся только с 2020 года, надо сделать чтобы было до 2014 и пустые категории не отображались */
    public function lastYearsGroups()
    {
        $groupNames = [];
        $fromYear = now()->year;
        for ($year = $fromYear; $year >= 2020; $year--) {
            $groupNames[] = "Фильмы {$year}";
        }

        return $this->domain->cinemaDomain
            ->mergedGroups()
            ->whereIn('groups.name', $groupNames)
            ->orderByDesc('groups.name')
            ->cacheFor(now()->addDays(1))
            ->get();
    }
}
