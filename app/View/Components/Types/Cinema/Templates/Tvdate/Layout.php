<?php

namespace App\View\Components\Types\Cinema\Templates\Tvdate;

use Closure;
use engine\app\models\Engine;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Layout extends Component
{
    public $domain;

    /**
     * Create a new component instance.
     */
    public function __construct(public $headers)
    {
        $this->domain = Engine::getDomainEloquent();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.types.cinema.templates.tvdate.layout');
    }
}
