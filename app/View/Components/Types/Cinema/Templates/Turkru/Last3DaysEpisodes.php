<?php

namespace App\View\Components\Types\Cinema\Templates\Turkru;

use cinema\app\models\eloquent\Domain;
use cinema\app\models\eloquent\MovieEpisode;
use cinema\app\models\eloquent\Videodb;
use Illuminate\View\Component;

class Last3DaysEpisodes extends Component
{
    public $days;

    public Domain $domain;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Domain $domain)
    {

        // $this->days = [now(),now()->subDays(1),now()->parse('2022-04-09')];
        $this->days = [now(), now()->subDays(1), now()->subDays(2)];
        $this->domain = $domain;

        // foreach ($days as $day) {
        //     $episodes[] = MovieEpisode::whereHas('videodbs',function($query) use ($day) {
        //         $query->whereBetween('date',[$day->copy()->startOfDay(),$day->copy()->endOfDay()]);
        //     })->get();

        //     /* получаем все обновления videodb за 3 дня и оставляем только уникальные эпизоды и дубляжи */
        //     $this->videodbsByDay[$day->format('Y-m-d')] = Videodb::query()
        //     ->select('movie_id','season','episode','translation')
        //     ->whereBetween('date',[$day->copy()->startOfDay(),$day->copy()->endOfDay()])
        //     ->distinct()
        //     ->get();
        // }

        // dd($episodes);

        // $this->videodbsByDay = collect($this->videodbsByDay);

        /* получаем уникальные фильмы */
        // $this->updatedMovies = $this->videodbs->groupBy('movie_id');

        // dd($this->updatedMovies);

        // $updatedOnDay = $videodbs->where('');

        /* получаем фильмы, для которых есть обновления базы videodb за указанные даты */
        /*
        foreach ($days as $day) {
            $movies = $domain
            ->movies()
            ->whereHas('videodbs',function($query) use ($day) {
                $query->whereBetween('date',[$day->copy()->startOfDay(),$day->copy()->endOfDay()]);
            })->get();
        }
        */

        /* получаем */
        /*
        $movie->videodbs()->select('movie_id','season','episode')->whereBetween('date',[$day->copy()->startOfDay(),$day->copy()->endOfDay()])->distinct()->get();
        */

        // foreach ( as $videodbItem)
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.types.cinema.templates.turkru.last-3-days-episodes');
    }
}
