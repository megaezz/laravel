<?php

namespace App\View\Components\Types\Cinema\Admin\Rewrite;

use cinema\app\controllers\WorkerController;
use Closure;
use engine\app\models\Template;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Layout extends Component
{
    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {

        $workerController = new WorkerController;
        $t = new Template;
        $workerController->setMenuVars($t);

        return view('components.types.cinema.admin.rewrite.layout')
            ->with('baseRows', $t->getVar('baseRows'))
            ->with('myRows', $t->getVar('myRows'))
            ->with('moderationRows', $t->getVar('moderationRows'))
            ->with('menuModerationActive', $t->getVar('menuModerationActive'))
            ->with('menuRewritedActive', $t->getVar('menuRewritedActive'))
            ->with('menuRulesActive', $t->getVar('menuRulesActive'))
            ->with('menuBaseActive', $t->getVar('menuBaseActive'))
            ->with('menuRewriteActive', $t->getVar('menuRewriteActive'))
            ->with('emailAlertBlock', $t->getVar('emailAlertBlock'))
            ->with('reworkAlertBlock', $t->getVar('reworkAlertBlock'))
            ->with('user', $workerController->getUser())
            ->with('metrika', (new Template('admin/metrika'))->get())
            ->with('jivosite', (new Template('admin/rewrite/jivosite'))->get());
        // ->with('', $t->getVar(''))
        // ->with('', $t->getVar(''))
        // ->with('', $t->getVar(''))
    }
}
