<?php

namespace App\View\Components\Types\Engine;

use App\Models\Config;
use Closure;
use engine\app\models\Engine;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Metrika extends Component
{
    public $domain;

    public $engineConfig;

    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        $this->domain = Engine::getDomainEloquent();
        $this->engineConfig = Config::cached();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.types.engine.metrika');
    }
}
