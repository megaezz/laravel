<?php

namespace App\View\Components\Types\Engine;

use App\Models\Config;
use Closure;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CheckVps extends Component
{
    public $nodes;

    public $code;

    /* этот компонент выводится сразу в роут через метод response, возможно наговнокодил, т.к. переменные не пробрасываются в шаблон, пришлось вручную это делать */

    /**
     * Create a new component instance.
     */
    public function __construct()
    {

        /* получаем А записи интересующих хостов */
        $dns['master'] = dns_get_record(Config::cached()->master_hostname, DNS_A);
        $dns['node'] = dns_get_record(Config::cached()->vps_hostname, DNS_A);

        $nodes = [];

        /* пробегаемся по каждой А записи и пробуем получить имена хостов, и маскируем результат */
        foreach ($dns as $name => $domain) {
            $i = 0;
            foreach ($domain as $record) {

                $hostname = gethostbyaddr($record['ip']);

                /* если у нас нет имени хоста, выводим замаскированный ip, если есть - выводим замаскированное имя хоста */
                if ($hostname == $record['ip']) {
                    $digits = explode('.', $record['ip']);
                    $title = "{$name} {$digits[0]} ... {$digits[3]}";
                } else {
                    $hostname = str_replace('.awmzone.net', '', $hostname);
                    /* если хост длинный - сокращаем его, чтобы не запалить хост или ip */
                    if (mb_strlen($hostname) > 7) {
                        $hostname = mb_substr($hostname, 0, 6);
                    }
                    $title = $hostname;
                }

                $nodes[] = (object) [
                    'title' => $title,
                    'ip' => $record['ip'],
                ];
            }
        }

        $client = new Client;

        $promises = [];

        $nodes = collect($nodes);

        /* пробегаемся по каждой ноде и создаем promise для асинхронного запроса, записываем их */
        foreach ($nodes as $key => $node) {
            $promises[] = $client->getAsync($node->ip, ['connect_timeout' => 3])->then(
                function ($response) use ($key, &$nodes) {
                    $nodes[$key]->body = $response->getBody()->getContents();
                },
                function ($reason) use ($key, &$nodes) {
                    $nodes[$key]->body = method_exists($reason, 'getResponse') ? $reason->getResponse()->getBody()->getContents() : false;
                },
            );
        }

        /* ждем выполнения запросов */
        Utils::settle($promises)->wait();

        /* добавляем нодам статус available */
        $nodes->map(function ($node) {
            $node->available = strpos($node->body, 'Oops!');

            return $node;
        });

        $this->nodes = $nodes;

        $this->code = $nodes->where('available', false)->count() ? 503 : 200;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.types.engine.check-vps')
        // не понимаю почему не передается nodes в шаблон без этой строки, должно работать же
            ->with('nodes', $this->nodes);
    }

    public function response()
    {
        return response($this->render(), $this->code);
    }
}
