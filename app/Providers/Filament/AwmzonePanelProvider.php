<?php

namespace App\Providers\Filament;

use Althinect\FilamentSpatieRolesPermissions\FilamentSpatieRolesPermissionsPlugin;
use App\Filament\Awmzone\Pages\Dashboard;
use App\Filament\Awmzone\Pages\Images;
use Filament\Http\Middleware\Authenticate;
use Filament\Http\Middleware\DisableBladeIconComponents;
use Filament\Http\Middleware\DispatchServingFilamentEvent;
use Filament\Navigation\NavigationItem;
use Filament\Panel;
use Filament\PanelProvider;
use Filament\Support\Colors\Color;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AwmzonePanelProvider extends PanelProvider
{
    // регистрируем провайдер, только если запрос поступает на awmzone домен, либо мы в консоли
    public function register(): void
    {
        if (request()->host() === config('app.awmzone_domain') or app()->runningInConsole()) {
            parent::register();
        }
    }

    public function panel(Panel $panel): Panel
    {
        // если мы в filament - то снимаем защиту с моделей, чтобы не пришлось добавлять все в $fillable
        Model::unguard();

        // убираем "Все" из пагинации для всех таблиц
        Table::configureUsing(function (Table $table) {
            $table->paginated([10, 25, 50, 100]);
        });

        // нужно сообщить в панель текущий домен, иначе при использовании ->emailVerification() будет ошибка об этом при выполнении команд php artisan, но если включить эту залупу, то будет ошибка с currentDomain
        // Filament::currentDomain('local.awmzone.net');

        return $panel
            ->default()
            // включаем оповещения для экспорта
            ->databaseNotifications()
            ->brandName('AWMZONE')
            ->id('awmzone')
            ->path('/')
            ->login()
            // ->registration()
            ->passwordReset()
            // после включения этой залупы появляется непонятная ошибка с currentDomain
            // ->emailVerification()
            ->sidebarCollapsibleOnDesktop()
            ->colors([
                'primary' => Color::Indigo,
                'gray' => Color::Slate,
            ])
            ->discoverResources(in: app_path('Filament/Awmzone/Resources'), for: 'App\\Filament\\Awmzone\\Resources')
            ->discoverPages(in: app_path('Filament/Awmzone/Pages'), for: 'App\\Filament\\Awmzone\\Pages')
            ->pages([
                // Pages\Dashboard::class,
                Dashboard::class,
                Images::class,
            ])
            ->navigationItems([
                // может пригодиться: когда первой ссылкой в навигации стоит внешний роут, то при заходе в панель сразу редиректит туда, и никак не попасть в панель, поэтому либо ставим первой ссылкой другой навигационный элемент, либо используем priority для управления этим поведением
                // ->priority(1)
                NavigationItem::make('Pulse')
                    ->group(___('мониторинг')->ucfirst())
                    ->url('/pulse')
                    ->visible(fn () => Auth::user()?->can('viewPulse'))
                // ->icon('heroicon-o-chart-bar'),
                    ->icon('/types/engine/template/admin/images/pulse.png'),
                NavigationItem::make('Telescope')
                    ->group(___('мониторинг')->ucfirst())
                    ->url('/telescope')
                    ->visible(fn () => Auth::user()?->can('everything'))
                // ->icon('heroicon-o-chart-bar'),
                    ->icon('/vendor/telescope/favicon.ico'),
                NavigationItem::make('Старая админка')
                    // не могу через именованный роут сделать ссылку, т.к. на этом этапе они еще не зарегистрированы почему-то
                    ->url('/mirrors')
                    ->visible(fn () => Auth::user()->can('access old awmzone admin'))
                    ->icon('heroicon-o-arrow-left')
                    // ->group('Старая админка')
                    ->sort(1),
            ])
            ->discoverWidgets(in: app_path('Filament/Awmzone/Widgets'), for: 'App\\Filament\\Awmzone\\Widgets')
            ->widgets([
                // Widgets\AccountWidget::class,
                // Widgets\FilamentInfoWidget::class,
                // StatsOverview::class,
            ])
            ->middleware([
                'web',
                // CronSwitch надо исключить, поэтому вместо dynamic - перечисляем middleware вручную
                \App\Http\Middleware\Engine\Ban::class,
                \App\Http\Middleware\Engine\AccessScheme::class,
                // 'dynamic',
                DisableBladeIconComponents::class,
                DispatchServingFilamentEvent::class,
            ])
            ->authMiddleware([
                Authenticate::class,
            ])
            ->plugin(FilamentSpatieRolesPermissionsPlugin::make())
            ->plugin(\TomatoPHP\FilamentWallet\FilamentWalletPlugin::make())
            // ->domains(Domain::find('awmzone.net')->self_and_mirrors->pluck('domain')->toArray())
            // ->domains($this->domains)
            ->domain(config('app.awmzone_domain'));
    }
}
