<?php

namespace App\Providers\Filament;

use engine\app\models\Engine;
use Filament\Http\Middleware\Authenticate;
use Filament\Http\Middleware\DisableBladeIconComponents;
use Filament\Http\Middleware\DispatchServingFilamentEvent;
use Filament\Pages;
use Filament\Panel;
use Filament\PanelProvider;
use Filament\Support\Colors\Color;
use Filament\Tables\Table;
use Filament\Widgets;
use Illuminate\Database\Eloquent\Model;

class ZdravzdravPanelProvider extends PanelProvider
{
    // регистрируем провайдер, только если запрос поступает на awmzone домен, либо мы в консоли
    public function register(): void
    {
        if (request()->host() === config('app.zdravzdrav_domain') or app()->runningInConsole()) {
            parent::register();
        }
    }

    public function panel(Panel $panel): Panel
    {
        // если мы в filament - то снимаем защиту с моделей, чтобы не пришлось добавлять все в $fillable
        Model::unguard();

        // убираем "Все" из пагинации для всех таблиц
        Table::configureUsing(function (Table $table) {
            $table->paginated([10, 25, 50, 100]);
        });

        $domain = Engine::getDomainEloquent();

        return $panel
            ->brandLogo("/storage/images/h100{$domain?->logo_src}")
            ->favicon("/storage/images/w100{$domain?->favicon_src}")
            ->default()
            ->databaseNotifications()
            ->brandName('ZdravZdrav')
            ->id('zdravzdrav')
            ->path('panel')
            ->login()
            ->registration()
            ->passwordReset()
            // после включения этой залупы появляется непонятная ошибка с currentDomain
            // ->emailVerification()
            ->sidebarCollapsibleOnDesktop()
            ->colors([
                'primary' => Color::Indigo,
                'gray' => Color::Slate,
            ])
            ->discoverResources(in: app_path('Filament/Zdravzdrav/Resources'), for: 'App\\Filament\\Zdravzdrav\\Resources')
            ->discoverPages(in: app_path('Filament/Zdravzdrav/Pages'), for: 'App\\Filament\\Zdravzdrav\\Pages')
            ->pages([
                // Pages\Dashboard::class,
            ])
            ->discoverWidgets(in: app_path('Filament/Zdravzdrav/Widgets'), for: 'App\\Filament\\Zdravzdrav\\Widgets')
            ->widgets([
                // Widgets\AccountWidget::class,
                // Widgets\FilamentInfoWidget::class,
            ])
            ->middleware([
                'web',
                'dynamic',
                DisableBladeIconComponents::class,
                DispatchServingFilamentEvent::class,
            ])
            ->authMiddleware([
                Authenticate::class,
            ])
            // ->domains($this->domains)
            ->domain(config('app.zdravzdrav_domain'))
            ->navigationGroups([
                'Врачи',
                'Клиники',
                'Отзывы',
                'Локиции',
            ]);
    }
}
