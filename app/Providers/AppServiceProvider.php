<?php

namespace App\Providers;

use App\Models\Config;
use Barryvdh\Debugbar\Facades\Debugbar;
use engine\app\models\Engine;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        /* здесь прописываем middleware, которые нужно чтобы повторялись при апдейтах компонента,
        если они присутствовали на исходном роуте */
        // осталась проблема - эти middleware при первой загрузке lazy компонента применяются дважды, я не понимаю в чем причина
        Livewire::addPersistentMiddleware([
            \App\Http\Middleware\MegawebAuthorization::class,
            \App\Http\Middleware\Engine\CronSwitcher::class,
            \App\Http\Middleware\Engine\Ban::class,
            \App\Http\Middleware\Engine\AccessScheme::class,
            \Spatie\Permission\Middleware\PermissionMiddleware::class,
        ]);

        // режим бога
        Gate::before(function ($user, $ability) {
            // пробовал через can - работает, но с глюками, например когда меняешь пользователя с кнопки - крашится, поэтому используем вариант с hasRole, собственно он и описан в документации spatie для режима супер-админа
            // return $user->can('everything')
            return $user->hasRole('Владелец') ? true : null;
        });

        // регистрируем политики для моделей из пакета spatie/laravel-permission, для управления доступом к соответствующим разделам в filament
        Gate::policy(\Spatie\Permission\Models\Role::class, \App\Policies\Vendor\RolePolicy::class);
        Gate::policy(\Spatie\Permission\Models\Permission::class, \App\Policies\Vendor\PermissionPolicy::class);
        // регистрируем политику для модели Activity из пакета spatie/laravel-activitylog для ActivityResource в filament
        Gate::policy(\Spatie\Activitylog\Models\Activity::class, \App\Policies\Vendor\ActivityPolicy::class);
        // регистрируем политику для моделей из пакета TomatoPHP\FilamentWallet, чтобы в filament ресурсы не отображались кому не надо
        Gate::policy(\TomatoPHP\FilamentWallet\Models\Transaction::class, \App\Policies\Vendor\TransactionPolicy::class);
        Gate::policy(\TomatoPHP\FilamentWallet\Models\Transfer::class, \App\Policies\Vendor\TransferPolicy::class);
        Gate::policy(\TomatoPHP\FilamentWallet\Models\Wallet::class, \App\Policies\Vendor\WalletPolicy::class);

        /* если дебаг отключен, но есть куки mmnas, то активируем debug режим */
        // было в Debug middleware, но перенес сюда, т.к. иначе не появляется дебагбар на проде
        if (! config('app.debug') and ((Config::cached()->debugByGet and request()->hasCookie('mmnas')) or Config::cached()->debug)) {
            config(['app.debug' => true]);
        }

        /* чтобы запросики при генерации роутов тоже писались в дебагбар */
        if (config('app.debug')) {
            // когда закоментировано, то дебагбар отображает "Connection Established", но зато неудобно получать массив запросов
            // DB::enableQueryLog();
            // если debugbar иницилизируется здесь, то timeline какой-то всратый становится, но зато видно запросы и timeline того что происходит до middleware (генерация роутов, конфигов и тд.)
            Debugbar::enable();
        }

        config([
            'app.name' => Engine::getDomainEloquent()?->site_name ?? 'Laravel',
            // 'app.debug' => Config::cached()->debug,
            // добавляем mailer из бд в конфиг
            'mail.mailers.custom' => Config::cached()->mailer->settings,
            // берем адрес и имя отправителя из домена
            'mail.from' => [
                'address' => Engine::getDomainEloquent()?->email,
                'name' => Engine::getDomainEloquent()?->site_name,
            ],
            'filesystems.disks.storage' => [
                'driver' => 'local',
                'root' => storage_path('app/public'),
                'url' => Config::cached()->storage_url,
                'visibility' => 'public',
            ],
        ]);
    }
}
