<?php

namespace App\Contracts;

interface DnsInterface
{
    public function getZone();

    public function getDnsRecords(?string $type = '', array|string|null $content = null, int $page = 1, int $perPage = 100000);

    public function addDnsRecord(string $type, string $domain, string $content, bool $proxied, string $priority = '');

    // public function getCnameRecord();

    public function deleteDnsRecords(?string $type = '', ?string $domain = '', string $content = '');

    public function createZone();

    public function deleteZone();

    public function activationCheck();

    public function applyDefaultSettings();

    public function getNameservers();

    public function addVpsRecord();

    public function addGoogleRecord();

    public function deleteCnameRecord();

    public function saveRecordsFromProviderToDatabase();

    public function saveRecordsFromDatabaseToProvider();

    public function setTls13(bool $b);

    public function setEch(bool $b);
}
