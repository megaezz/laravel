<?php

use App\Models\Lang;
use Illuminate\Support\Str;

function ___(string|array $key, array $replace = [], ?string $locale = null, ?string $page = null)
{

    // если первый аргумент - массив, то возвращаем результат localed
    if (is_array($key)) {
        return Str::of(localed($key));
    }

    // если первый аргумент строка - то возвращаем локаль из бд
    return Str::of(Lang::localed($key, $replace, $locale, $page));
}

// локали могут быть переданы как единым массивом ['ru' => '', 'en => ''], так и несколькимипараметрами: ['ru' => ''], ['en' => '']
function localed(...$locales)
{

    if (! isset($locales[0])) {
        throw new \Exception('Не переданы варианты перевода');
    }

    if (count($locales) > 1) {
        $arr = $locales;
        unset($locales);
        foreach ($arr as $variant) {
            foreach ($variant as $locale => $string) {
                $locales[$locale] = $string;
            }
        }
    } else {
        $locales = $locales[0];
    }

    if (! count($locales)) {
        throw new \Exception('Пустой массив локалей');
    }

    $currentLocale = app()->getLocale();

    if (! isset($locales[$currentLocale])) {
        logger()->alert("Не найдена локаль {$currentLocale} в: ".json_encode($locales));

        /* если нужная локаль не найдена, то выводим дефолтную или хоть какую-то */
        return $locales[config('app.locale')] ?? reset($locales);
    }

    return $locales[$currentLocale];
}

function domain_route($name, $parameters = [], $absolute = true)
{
    return route(domain_routename($name), $parameters, $absolute);
}

function domain_routename(string $name)
{
    return request()->getHost().'_'.$name;
}
