<?php

namespace App\Jobs;

use App\Models\Cinema\Torrent;
use App\Services\Aria2;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Support\Facades\Storage;

class UploadTorrent implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(public Torrent $torrent)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        /*
        принимаем объект торрента
        парсим торрент
        создаем соответствующие MovieUploads
        загружаем торренты на secondary сервер
        транскодируем видео
        отправляем на cdn
        сохраняем ссылки на cdn hls в MovieUploads
        */

        $aria2 = new Aria2('http://aria2:6800/jsonrpc');
        $torrentFileUrl = 'https://awmzone.net'.Storage::url($this->torrent->src);
        $gid = $aria2->addByUrl($torrentFileUrl)['result'];
        $this->torrent->aria2_gid = $gid;
        $this->torrent->save();
        logger()->alert("Начали загрузку торрента, gid: {$gid}");
    }

    public function mkvTranscoding($mkvFile)
    {
        $command = 'ffmpeg -i '.$mkvFile.' \
        -filter_complex "[0:v]split=2[v1080][v720]; \
        [v1080]scale=w=1920:h=1080[v1080out]; \
        [v720]scale=w=1280:h=720[v720out]" \
        -map "[v1080out]" -map 0:a -c:v:0 libx264 -b:v:0 5000k -maxrate:v:0 5350k -bufsize:v:0 7500k \
        -map "[v720out]" -map 0:a -c:v:1 libx264 -b:v:1 2500k -maxrate:v:1 2675k -bufsize:v:1 3750k \
        -c:a aac -b:a 128k -ar 48000 -ac 2 \
        -f hls \
        -hls_time 6 \
        -hls_playlist_type vod \
        -hls_segment_type fmp4 \
        -hls_segment_filename "segment_%v_%03d.m4s" \
        -master_pl_name "master.m3u8" \
        -var_stream_map "v:0,a:0 v:1,a:1" \
        "stream_%v.m3u8"';
    }
}
