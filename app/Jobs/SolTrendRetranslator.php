<?php

namespace App\Jobs;

use App\Models\TelegramMessage;
use App\Services\Madeline\SaulGoodieSettings;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;

class SolTrendRetranslator implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(public TelegramMessage $telegramMessage)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        if ($this->telegramMessage->sender_id !== -1002085434351) {
            return;
        }

        $type = null;
        $message = $this->telegramMessage->message;

        // определяем сообщение - оповещение роста
        if (
            ! $type and
            preg_match('/^💹 (?P<x>\d+(\.\d+)?)x (?P<from>\d+(\.\d+)?)(?P<from_unit>[KMB]) ↗️ (?P<to>\d+(\.\d+)?)(?P<to_unit>[KMB]) \((?P<ex>\d+(\.\d+)?)x from Exclusive\)$/', $message, $matches)
        ) {
            $type = 'volume growth';
            $message = "🚀 {$matches['x']}x с {$matches['from']}{$matches['from_unit']} до {$matches['to']}{$matches['to_unit']} ({$matches['ex']}x в Эксклюзивном)";
        }

        // определяем сообщение - сигнал (нет реплая и содержит текст на кнопках Buy on)
        if (
            ! $type and
            preg_match('/^💊 (?P<token>[A-Za-z0-9]+)\n'.
            '┌(?P<name>.+) \((?P<symbol>.+)\)\n'.
            '├USD:\s+\$(?P<price>[\d\.]+)\n'.
            '├MC:\s+\$(?P<mc>[\d\.]+[KM]?)\n'.
            '├Vol:\s+\$(?P<vol>[\d\.]+[KM]?)\n'.
            '├Seen:\s+(?P<seen>.+)(?P<seen_unit>[ms]) ago\n'.
            '├Dex:\s+(?P<dex>\w+)\n'.
            '├Dex Paid:\s+(?P<dex_paid_flag>[🟢🔴🟡]+)\n'.
            '├Holder:\s+Top 10:\s+(?P<holder_flag>[🟢🔴🟡]+) (?P<holder_percent>[\d\.]+)%\n'.
            '└TH:\s+(?P<th>[\d\.\|]+)\n\n'.
            '🔎 Deep scan by Z99Bot\n'.
            '(.+)\n\n'.
            '📈 Chart:\s+(?P<chart>https?:\/\/[^\s]+)$/m', $message, $matches)
        ) {
            $type = 'signal';

            $seenUnit = match ($matches['seen_unit']) {
                'm' => 'м',
                's' => 'с',
                default => '?',
            };

            $message = "💊 <code>{$matches['token']}</code>\n".
            "┌{$matches['name']} (<a href=\"https://www.pump.fun/{$matches['token']}\">{$matches['symbol']}</a>)\n".
            "├<code>USD:      </code>\${$matches['price']}\n".
            "├<code>MC:       </code>\${$matches['mc']}\n".
            "├<code>Vol:      </code>\${$matches['vol']}\n".
            "├<code>Seen:     </code>{$matches['seen']}{$seenUnit} назад\n".
            "├<code>Dex:      </code>{$matches['dex']}\n".
            "├<code>Dex Paid: </code>{$matches['dex_paid_flag']}\n".
            "├<code>Holder:   </code>Топ 10: {$matches['holder_flag']} 40%\n".
            "└<code>TH:       </code>{$matches['th']}\n\n".
            "🔎 Анализ на <a href=\"https://gmgn.ai/sol/token/{$matches['token']}\">GMGN</a>\n".
            // "⚡️ Автопокупка • <a href=\"https://t.me/fasol_robot\"><b>FASOL</b></a> • <a href=\"https://t.me/BloomSolana_bot\">Bloom</a> • <a href=\"https://t.me/maestro\">Maestro</a>\n\n" .
            "📈 <a href=\"https://dexscreener.com/solana/{$matches['token']}\">График</a>";

            $inlineKeyboard = SaulGoodieSettings::buyTokenInlineKeyboard($matches['token']);
        }

        // если не опознали тип - пропускаем
        if (! $type) {
            logger()->alert("Не опознали тип сообщения {$this->telegramMessage->id}. Пропускаем.");

            return;
        }

        // синхронно, т.к. мы уже в задании
        SendByAwmzoneBotTo::dispatchSync(
            to: 'kriptololSolTrends',
            message: $message,
            retranslationOf: $this->telegramMessage,
            replyMarkup: $inlineKeyboard ?? null,
            parseMode: 'HTML',
        );
    }
}
