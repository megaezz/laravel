<?php

namespace App\Jobs;

use App\Models\Config;
use App\Models\TelegramMessage;
use Barryvdh\Debugbar\Facades\Debugbar;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Queue\Middleware\ThrottlesExceptions;
use TelegramBot\Api\BotApi;

class SendByAwmzoneBotTo implements ShouldQueue
{
    use Queueable;

    public $receivers = [
        // 'kriptololSolTrendsChannel' => -1002463662107,
        // 'kriptololSolTrendsExclusiveChannel' => -1002337102739,
        // 'kriptololWhalesBuyingChannel' => -1002352875970,
        // 'kriptololXPumpChannel' => -1002393784628,
        'kriptololSolTrends' => [
            -1002463662107,
            [
                'chat_id' => -1002278507794,
                'thread_id' => 3,
            ],
        ],
        'kriptololSolTrendsExclusive' => [
            -1002337102739,
            [
                'chat_id' => -1002278507794,
                'thread_id' => 5,
            ],
        ],
        'kriptololWhalesBuying' => [
            -1002352875970,
            [
                'chat_id' => -1002278507794,
                'thread_id' => 6,
            ],
        ],
        'kriptololXPump' => [
            -1002393784628,
            [
                'chat_id' => -1002278507794,
                'thread_id' => 2,
            ],
        ],
        'me' => 275834308,
        'monax' => 157149450,
        'awmzoneAdmins' => [
            275834308,
            157149450,
        ],
    ];

    /**
     * Create a new job instance.
     */
    public function __construct(
        public string $to,
        public string $message,
        public ?TelegramMessage $retranslationOf = null,
        public ?int $reply_of = null,
        public $replyMarkup = null,
        public $parseMode = null,
        public $picture = null,
    ) {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $bot = new BotApi(Config::cached()->telegram_token);

        $chats = $this->receivers[$this->to] ?? throw new Exception('Unknown receiver');
        $chats = is_array($chats) ? $chats : [$chats];

        // dd($replyToByRetranslationMessage ?? null);

        foreach ($chats as $chatId) {

            $threadId = null;

            if (is_array($chatId)) {
                $threadId = $chatId['thread_id'];
                $chatId = $chatId['chat_id'];
            }

            // если исходное сообщение имеет replyId то и нам надо
            // TODO: аккуратненько подумать как выбирать сообщение среди replyOf
            if (! $this->reply_of and $this->retranslationOf and $this->retranslationOf->reply_of) {
                // $this->retranslationOf - исходное сообщение на канале-доноре, если мы здесь значит уже знаем, что оно является ответом на другое сообщение, а именно на одно из этих $this->retranslationOf->replyOf()
                // чтобы выбрать какое именно из replyOf использовать, нужно узнать а было ли какое то из них донором для сообщения бота на этот же самый канал $this->to, если да, то именно оно и будет нашим искомым reply_message_id
                $replyToByRetranslationMessage = $this->retranslationOf
                    // получаем список сообщений, который удовлетворяют нашему reply_id
                    ->replyOf()
                    // нам нужно отобрать среди этих одинаковых сообщений именно то, которое было тоже ретранслировано в этот же чат, тогда 100% мы угадали и будет корректный reply_id
                    // ->whereHas('retranslations', fn($query) => $query->where('chat_id', $chatId))
                    ->whereRelation('retranslations', 'chat_id', $chatId)
                    // берем смело первое же, все, мы нашли тут replyOf сообщение нашего исходного сообщения, теперь еще нужно получить дочернее сообщение которое было ретранслировано от него
                    ->first()
                    // для этого находим сообщения, которые были ретранслированы от этого найденного
                    ?->retranslations()
                    // и учитывая, что у нас уже было сверху условие whereRelation, значит мы уже можем тут ничего не проверять, просто берем первое же сообщение, оно будет для этого же чата
                    ->first()
                    ?->message_id;
                // $replyToByRetranslationMessage = $this->retranslationOf->replyOf()->has('retranslationOf')->first()?->message_id;

                if (! $replyToByRetranslationMessage) {
                    logger()->alert("Не получилось найти аналог ретранслируемого сообщения для {$this->retranslationOf->id} – не отправляем");

                    return;
                }
            }

            Debugbar::startMeasure('send message');
            if ($this->picture) {
                $message = $bot->sendPhoto(
                    chatId: $chatId,
                    caption: $this->message,
                    photo: new \CURLFile($this->picture),
                    parseMode: $this->parseMode,
                    replyToMessageId: $this->reply_of ?? $replyToByRetranslationMessage ?? null,
                    replyMarkup: $this->replyMarkup,
                    messageThreadId: $threadId,
                );
            } else {
                $message = $bot->sendMessage(
                    chatId: $chatId,
                    text: $this->message,
                    parseMode: $this->parseMode,
                    disablePreview: true,
                    replyToMessageId: $this->reply_of ?? $replyToByRetranslationMessage ?? null,
                    replyMarkup: $this->replyMarkup,
                    messageThreadId: $threadId,
                );
            }
            Debugbar::stopMeasure('send message');

            // из $bot никак не получить id бота ($senderId) а он нам нужен, поэтому колхозим
            preg_match('/^(?P<id>\d+):/', Config::cached()->telegram_token, $matches);
            $telegramMessage = TelegramMessage::fillFromBotApi($bot, $message, $matches['id']);

            if ($this->retranslationOf) {
                // если передано исходное сообщение, которые мы ретранслируем, то запоминаем исходное
                $telegramMessage->retranslationOf()->associate($this->retranslationOf);
            }

            $telegramMessage->save();
        }
    }

    // тротлинг на случай, если api не будет работать, надо затестить
    public function middleware(): array
    {
        return [new ThrottlesExceptions(10, 5 * 60)];
    }

    // function failed(\Exception $e) {
    //     logger()->info($e->getMessage());
    // }
}
