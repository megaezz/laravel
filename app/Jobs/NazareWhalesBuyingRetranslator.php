<?php

namespace App\Jobs;

use App\Models\TelegramMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;

class NazareWhalesBuyingRetranslator implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(public TelegramMessage $telegramMessage)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (! ($this->telegramMessage->sender_id === -1001883173941 and $this->telegramMessage->data->topicId === 200045)) {
            return;
        }

        $message = null;

        // определяем сообщение - оповещение роста
        if (preg_match('/A (?P<token_from>\$[\w\s]+) whale just bought (?P<amount>\$[\d\.KMB]+) of (?P<token_to>\$[\w\.]+) at (?P<mc>\$[\d\.KMB]+) MC 🐳/', $this->telegramMessage->message, $matches)
        ) {
            $message = "{$matches['token_from']} кит только что купил {$matches['amount']} токена {$matches['token_to']} на {$matches['mc']} капитализации";
        }

        // если не опознали тип - пропускаем
        if (! $message) {
            logger()->alert("Не опознали сообщение {$this->telegramMessage->id}. Пропускаем.");

            return;
        }

        // синхронно, т.к. мы уже в задании
        SendByAwmzoneBotTo::dispatchSync(
            to: 'kriptololWhalesBuying',
            message: $message,
            retranslationOf: $this->telegramMessage,
            replyMarkup: $inlineKeyboard ?? null,
            parseMode: 'HTML',
        );
    }
}
