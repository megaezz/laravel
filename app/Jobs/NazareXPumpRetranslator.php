<?php

namespace App\Jobs;

use App\Models\TelegramMessage;
use App\Services\Madeline\SaulGoodieSettings;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Support\Facades\Storage;

class NazareXPumpRetranslator implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(public TelegramMessage $telegramMessage)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (! ($this->telegramMessage->sender_id === -1001883173941 and $this->telegramMessage->data->topicId === 196558)) {
            return;
        }

        $message = null;
        $picture = null;

        // если сообщение содержит медиа - сохраняем во временную папку, чтобы потом легко переслать ботом
        if ($this->telegramMessage->data->media?->botApiFileId) {
            $exists = Storage::disk('madeline')->exists("tmp/{$this->telegramMessage->data->media->fileName}");
            if (! $exists) {
                // logger()->alert("Сохраняем media для {$this->telegramMessage->id}");
                SaulGoodieSettings::api()->downloadToDir(
                    $this->telegramMessage->data->media->botApiFileId,
                    SaulGoodieSettings::tmpStorage()
                );
            }
            $picture = Storage::disk('madeline')->path("tmp/{$this->telegramMessage->data->media->fileName}");
        }

        // определяем сообщение - сигнал
        if (
            preg_match('/^🏵\s(?P<name>.+)\n'.
            '(?P<token>[a-zA-Z0-9]+)\n\n'.
            '💰\sPrice:\s(?P<price>[0-9.]+)\$\n'.
            '📊\sMCap:\s(?P<mc>[0-9.]+[KMB])\$\n'.
            '💸\sVolume\s24H:\s(?P<vol>[0-9.]+[KMB])\$\n\n'.
            'Project Links:\s(?P<links>.+)/m', $this->telegramMessage->message, $matches)
        ) {

            $message = "🏵 <code>{$matches['token']}</code>\n".
            "┌<a href=\"https://www.pump.fun/{$matches['token']}\">{$matches['name']}</a>\n".
            "├<code>USD:      </code>\${$matches['price']}\n".
            "├<code>MC:       </code>\${$matches['mc']}\n".
            "├<code>Vol:      </code>\${$matches['vol']}\n".
            "🔎 Анализ на <a href=\"https://gmgn.ai/sol/token/{$matches['token']}\">GMGN</a>\n".
            "📈 <a href=\"https://dexscreener.com/solana/{$matches['token']}\">График</a>";

            $inlineKeyboard = SaulGoodieSettings::buyTokenInlineKeyboard($matches['token']);
        }

        // 🪙\s(?P<name>.+)\n┣💹\sGrowth:\s(?P<growth>[0-9.]+x)\n┗📈\sMC:\s\$(?P<mc_start>[0-9.]+[KM]?)\s➜\s\$(?P<mc_end>[0-9.]+[KM]?)\n\s🤖\sBots:\s(?P<bots>.+)
        if (! $message and preg_match('/^🪙\s(?P<name>.+)\n'.
        '┣💹\sGrowth:\s(?P<growth>[0-9.]+x)\n'.
        '┗📈\sMC:\s\$(?P<mc_start>[0-9.]+[KM]?)\s➜\s\$(?P<mc_end>[0-9.]+[KM]?)/m', $this->telegramMessage->message, $matches)) {

            $message = "🪙 {$matches['name']}\n".
            "┣💹 Growth: {$matches['growth']}\n".
            "┗📈 MC: \${$matches['mc_start']} ➜ \${$matches['mc_end']}";

            // $inlineKeyboard = SaulGoodieSettings::buyTokenInlineKeyboard($matches['token']);

        }

        // если не опознали тип - пропускаем
        if (! $message) {
            logger()->alert("Не опознали сообщение {$this->telegramMessage->id}. Пропускаем.");

            return;
        }

        // синхронно, т.к. мы уже в задании
        SendByAwmzoneBotTo::dispatchSync(
            to: 'kriptololXPump',
            message: $message,
            retranslationOf: $this->telegramMessage,
            replyMarkup: $inlineKeyboard ?? null,
            parseMode: 'HTML',
            picture: $picture,
        );
    }
}
