<?php

namespace App\Jobs;

use App\Models\Abuse;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Support\Str;

class LoadMemorandum implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(public $url)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            $result = $this->saver($this->url);
        } catch (\Exception $e) {
            logger()->emergency("Ошибка при обработке меморандума ({$this->url}): {$e->getMessage()}");

            return;
        }

        logger()->alert("Обработан меморандум ({$this->url}): {$result}");
    }

    public function saver($csvUrl)
    {
        $csv = fopen($csvUrl, 'r');
        $duplicated = 0;
        $new = 0;

        /* здесь разбираем csv файл в массив */
        while (($data = fgetcsv($csv)) !== false) {
            /* первая строка не нужна */
            if ($data[2] == 'Url') {
                continue;
            }
            $url = str_replace(' ', '+', urldecode($data[2]));

            /* собираем ссылку заново */
            $url_formated = Abuse::formatUrl($url);

            // Проверяем длину строки
            if (Str::length($url_formated) > 1000) {
                // Обрезаем строку до 1000 символов
                $url_formated = Str::limit($url_formated, 600, '');
                // Записываем сообщение в логи Laravel
                logger()->alert(__METHOD__.' слишком длинная строка запроса, обрезаем до 600 символов: '.$url_formated);
            }

            /* сохраняем ссылку в abuses */
            $link = Abuse::firstOrCreate([
                'query' => $url_formated,
                'service' => 'memorandum',
            ]);

            $duplicated = $duplicated + ($link->wasRecentlyCreated ? 0 : 1);
            $new = $new + ($link->wasRecentlyCreated ? 1 : 0);

        }
        fclose($csv);

        return 'Получено '.($new + $duplicated).' ссылок, новых: '.$new.', дублей: '.$duplicated;
    }
}
