<?php

namespace App\Jobs;

use App\Models\TelegramMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;

class Retranslator implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new job instance.
     */
    public function __construct(public TelegramMessage $telegramMessage)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        SolTrendRetranslator::dispatchSync($this->telegramMessage);
        SolTrendExclusiveRetranslator::dispatchSync($this->telegramMessage);
        NazareWhalesBuyingRetranslator::dispatchSync($this->telegramMessage);
        NazareXPumpRetranslator::dispatchSync($this->telegramMessage);
    }
}
