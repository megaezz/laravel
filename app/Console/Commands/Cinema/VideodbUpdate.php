<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class VideodbUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:videodb-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные videodb по апи';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->launch(function () {
            $result = (new \App\Services\Cinema\Balancers\VideodbService)(10, 100, $this->cron()->temp_var);
            if ($result['finished']) {
                $this->finished();
                /* Когда закончили - чистим Videodb от удаленных материалов */
                $this->info((new \cinema\app\controllers\CronController)->clearVideodb());

            } else {
                $this->cron()->temp_var = $result['next_offset'];
                $this->cron()->save();
            }
            $this->table(['Сообщение', 'Следующий offset', 'Выполнено'], [[$result['message'], $result['next_offset'], $result['finished'] ? 'Да' : 'Нет']]);
        });
    }
}
