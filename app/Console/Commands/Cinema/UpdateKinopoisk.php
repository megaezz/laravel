<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class UpdateKinopoisk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-kinopoisk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные Kinopoisk';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->launch(function () {
            $this->info((new \App\Services\Cinema\KinopoiskUpdate)($this->cron()));
        });
    }
}
