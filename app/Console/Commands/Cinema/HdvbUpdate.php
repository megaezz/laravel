<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class HdvbUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:hdvb-update {--limit= : Лимит} {--timeout= : Таймаут}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные hdvb по апи';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        // было 5, сделал 1 когда глючило
        $limit = $this->option('limit') ?? 1;

        $this->launch(function () use ($limit) {
            try {
                $result = (new \App\Services\Cinema\Balancers\HdvbUpdate)($limit, $this->cron()->temp_var);
            } catch (\Exception $e) {
                $message = "Со страницы: {$this->cron()->temp_var} ".$e->getMessage();
                // через __METHOD__ не работает, т.к. это замыкание, и оно не видит метода и возвращает {closure}
                logger()->alert("HdvbUpdate: {$message}");
                $this->error($message);

                return;
            }
            if ($result['finished']) {
                $this->finished();
                /* когда закончили - выполняем hdvb-clear для очистки бд от давно не обновлявшиеся записей, возможно оно и не нужно, я чекал в базе все ок, даже там где updated_at 2 мес назад */
                $this->warn((new \App\Services\Cinema\Balancers\HdvbClear)());
            } else {
                $this->cron()->temp_var = $result['next_page'];
                $this->cron()->save();
            }
            $this->table(['Сообщения'], $result['messages']);
            $this->table(['Сообщение', 'Следующая страница', 'Выполнено'], [[$result['message'], $result['next_page'], $result['finished'] ? 'Да' : 'Нет']]);
            if ($this->option('timeout')) {
                sleep($this->option('timeout'));
            }
        });
    }
}
