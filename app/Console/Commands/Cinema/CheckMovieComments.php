<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CheckMovieComments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:check-movie-comments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Уведомить, если коментов больше, чем обычно';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \cinema\app\controllers\CronController)->checkMovieComments());
    }
}
