<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CalculateGroupMovies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:calculate-group-movies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Расчитывает фильмы для категорий';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->launch(function () {
            $this->info((new \App\Services\Cinema\CalculateGroupMovies)($this->cron()));
        });
    }
}
