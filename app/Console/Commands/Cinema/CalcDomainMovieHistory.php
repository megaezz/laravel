<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CalcDomainMovieHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:calc-domain-movie-history';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Записать текущую статистику таблицы domain_movies';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        \DB::insert('
            insert into cinema.domain_movie_histories (domain, `all`, active, deleted, created_at)
            (select domain, count(*) as `all`, count(case when active = 1 then 1 end) as active, count(case when deleted = 1 then 1 end) as deleted,
            current_timestamp
            from cinema.domain_movies where domain is not null group by domain);
            ');
        $this->info('Текущая статистика domain_movies записана');
    }
}
