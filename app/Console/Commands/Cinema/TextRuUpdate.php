<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class TextRuUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:text-ru-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Получает/отправляет тексты на проверку';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \cinema\app\controllers\CronController)->textRuGet());
        $this->info((new \cinema\app\controllers\CronController)->textRuSend());
    }
}
