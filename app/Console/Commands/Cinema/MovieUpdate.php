<?php

namespace App\Console\Commands\Cinema;

use App\Services\Cinema\MovieUpdate\Handler;
use App\Services\Command;

class MovieUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:movie-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить Movies';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->launch(function () {

            $limit = 50;

            $result = (new Handler)($limit, $this->cron()->temp_var ?? 0);

            $this->cron()->temp_var = $result['lastId'];
            $this->cron()->save();

            if ($result['items']->count() < $limit) {
                $this->finished();
            }

            foreach ($result['items'] as $item) {
                $this->info($item);
                $this->info('---');
            }
        });
    }
}
