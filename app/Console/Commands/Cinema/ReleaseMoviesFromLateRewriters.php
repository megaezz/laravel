<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\DomainMovie;

class ReleaseMoviesFromLateRewriters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:release-movies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отобрать задания у запоздалых исполнителей';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        // $result = (new \cinema\app\controllers\CronController)->releaseMoviesFromLateRewriters();

        $domainMovies = DomainMovie::query()
            ->where('description_length', 0)
            ->whereNotNull('booked_for_login')
            ->whereNotIn('booked_for_login', ['ozerin', 'seo-fast', 'seo-vip'])
            ->where(\DB::raw('TIMESTAMPDIFF(MINUTE, book_date, CURRENT_TIMESTAMP)'), '>=', Config::cached()->book_minutes)
            ->get();

        foreach ($domainMovies as $domainMovie) {
            $domainMovie->booked_for_login = 'ozerin';
            $domainMovie->book_date = null;
            $domainMovie->writing = false;
            $domainMovie->save();
        }

        $count = count($domainMovies);
        $ids = $count ? "({$domainMovies->pluck('id')->implode(', ')})" : null;

        $result = "Освобождено от брони заданий: {$count} {$ids}";

        logger()->info($result);
        $this->info($result);
    }
}
