<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class UpdateAlloha extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-alloha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные аллохи по апи';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->launch(function () {
            $result = (new \App\Services\Cinema\Balancers\AllohaUpdate)($this->cron()->temp_var);
            if ($result['error']) {
                $this->warn($result['error']);
            } else {
                if ($result['finished']) {
                    $this->finished();
                } else {
                    $this->cron()->temp_var = $result['next_page'];
                    $this->cron()->save();
                }
                $this->table(['Сообщение', 'Следующая страница', 'Выполнено'], [[$result['message'], $result['next_page'], $result['finished'] ? 'Да' : 'Нет']]);
            }
        });
    }
}
