<?php

namespace App\Console\Commands\Cinema;

use App\Models\Cinema\MovieUpload;
use App\Models\Cinema\Torrent;
use App\Models\Node;
use Illuminate\Console\Command;

class UpdateTorrents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-torrents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить инфу по торрентам';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $torrents = Torrent::where('is_handled', false)->get();

        foreach ($torrents as $torrent) {

            if (! $torrent->is_downloaded) {
                if ($torrent->aria2_gid) {

                    // обновляем инфу по скачиванию
                    $torrent->aria2_torrent_data = $torrent->aria2->torrentData();
                    $torrent->aria2_content_data = $torrent->aria2->contentData();
                    $this->info("#{$torrent->id} обновили инфу");

                    // помечаем торрент скачанным, если complete
                    if (isset($torrent->aria2_content_data['status']) and $torrent->aria2_content_data['status'] === 'complete') {
                        $this->info("#{$torrent->id} отметили скачанным");
                        logger()->alert("Торрент #{$torrent->id} загружен");
                        $torrent->is_downloaded = true;
                    }

                    // помечаем только нужные файлы
                    if (! $torrent->is_cleaned_up) {
                        try {
                            $torrent->aria2->selectOnlyNeededFiles();
                            $torrent->is_cleaned_up = true;
                            $this->info("#{$torrent->id} пометили нужные файлы");
                            logger()->alert("Торрент #{$torrent->id} – пометили нужные файлы");
                        } catch (\Exception $e) {
                            // не делаем алерт ошибки с Content Data, т.к. она частая
                            if (! $e->getMessage() === 'Content Data отсутствует') {
                                logger()->alert("Торрент #{$torrent->id} – ошибка селекта нужных файлов: {$e->getMessage()}");
                            }
                            $this->info("#{$torrent->id} ошибка селекта нужных файлов: {$e->getMessage()}");
                        }
                    }
                } else {
                    // начинаем автоматическое скачивание только если еще не было очистки, иначе, в случае если выбрано 0 файлов, будет вечно отменяться и снова скачиваться
                    if (! $torrent->is_cleaned_up) {
                        $torrent->aria2->download();
                        logger()->alert("Торрент #{$torrent->id} – начали скачивание");
                    }
                }
            }

            if ($torrent->is_downloaded and ! $torrent->is_handled) {
                // если скачано хоть что-то, если добавлено в загрузки хоть что-то и оно существует, то отмечаемся ок
                if (
                    $torrent->aria2->downloadedMovies()->count() > 0 and $torrent->uploads->count() > 0 and $torrent->uploads->count() === $torrent->uploads->where('hls_exists', true)->count()
                ) {
                    $torrent->is_handled = true;
                } else {
                    // TODO: тут автоматически нужно создавать загрузки, но если создалось 0 загрузок, то постоянно будет создавать загрузки
                    // если у торрента нет загрузок, то выполняем createMovieUploads
                    if (! $torrent->uploads()->count()) {
                        $torrent->createMovieUploads();
                        logger()->alert("Торрент #{$torrent->id} – создали movie uploads");
                    }
                }
            }

            $torrent->save();
        }

        // если все скачано и выложено, то очищаем downloads
        if (!Torrent::where('is_handled', false)->count() and !MovieUpload::where('hls_exists', false)->count()) {
            try {
                $isEmpty = empty(trim(Node::secondary()->command("ls -A '/home/laravel/storage/app/cinema/torrent-downloads'"))) ? true : false;
            } catch (\Exception $e) {
                logger()->alert("Ошибка проверки пусто ли в torrent-downloads: {$e->getMessage()}");
                return;
            }
            if ($isEmpty) {
                return;
            }
            $this->info("Все скачано и обработано, очищаем downloads");
            logger()->alert("Все скачано и обработано, очищаем downloads");
            try {
                Torrent::clearDownloads();
            } catch (\Exception $e) {
                $this->info("Ошибка при очистке: {$e->getMessage()}");
                logger()->alert("Ошибка при очистке: {$e->getMessage()}");
            }
        }

        $this->info("Обновлено {$torrents->count()} торрентов");
    }
}
