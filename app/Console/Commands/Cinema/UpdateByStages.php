<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;
use cinema\app\services\cinema_update\UpdateStagesService;
use engine\app\models\F;

class UpdateByStages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-by-stages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Выполняет необходимые задания последовательно';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $update_stages = new UpdateStagesService;
        try {
            $can_run = $update_stages->canRun();
        } catch (\Exception $e) {
            $can_run = false;
            $this->warn($e->getMessage());
        }
        if ($can_run) {
            F::repeatFor(function () use ($update_stages) {
                $this->info($update_stages->run());
            }, 50);
        }
    }
}
