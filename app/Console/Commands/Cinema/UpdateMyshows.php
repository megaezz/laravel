<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class UpdateMyshows extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-myshows';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные Myshows';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $service = new \App\Services\Cinema\MyshowsUpdate($this->cron());
        $this->launch(function () use ($service) {
            $this->info($service->run());
        });
    }
}
