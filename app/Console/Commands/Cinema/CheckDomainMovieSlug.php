<?php

namespace App\Console\Commands\Cinema;

use App\Models\Cinema\DomainMovie;
use Illuminate\Console\Command;

// use App\Services\Command;

class CheckDomainMovieSlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:check-domain-movie-slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Уведомляет, если не проставлен slug в domainMovie';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $domainMovies = DomainMovie::whereNotNull('domain')->whereNull('slug');

        $count = $domainMovies->count();

        if ($count) {
            $message = "Найдены domainMovies с отсутствующим slug: {$count}";
            // logger()->alert($message);
        } else {
            $message = 'Slug везде проставлен';
        }

        $this->info($message);

    }
}
