<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CalTempBalances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:calc-temp-balances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Расчитывает кешированный баланс для пользователей';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \cinema\app\controllers\CronController)->calcTempBalances());
    }
}
