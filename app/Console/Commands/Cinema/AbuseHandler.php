<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class AbuseHandler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:abuse-handler {--limit= : Лимит}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обработать абузы';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $links = (new \App\Services\Cinema\RouteHandlers\AbuseHandler)(limit: $this->option('limit'));

        if (! $links->count()) {
            $this->info('Нечего обрабатывать');
        }

        foreach ($links as $link) {
            $this->info($link->query);
            if ($link->handler_error) {
                $this->error($link->handler_message);
            } else {
                $this->warn($link->handler_message);
            }
            $this->newLine();
        }
    }
}
