<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CalcDescriptionWaitTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:calc-desc-wait-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Расчитать среднее время ожидания на выполнение заданий';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \cinema\app\controllers\CronController)->calcDescriptionWaitTime());
    }
}
