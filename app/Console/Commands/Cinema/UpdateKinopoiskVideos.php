<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class UpdateKinopoiskVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-kinopoisk-videos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные о видео в Kinopoisk';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {

        /* команда обновления кинопоиска, чтобы проверить выполняется ли она сейчас */
        $kinopoiskUpdateCommand = new \App\Console\Commands\Cinema\UpdateKinopoisk;

        try {
            $kinopoiskUpdateCanBeLaunched = $kinopoiskUpdateCommand->canBeLaunched();
        } catch (\Exception $e) {
            $kinopoiskUpdateCanBeLaunched = false;
        }

        if ($kinopoiskUpdateCanBeLaunched) {
            $this->alert('Пока что не можем начать выполнение, выполняется cinema:update-kinopoisk, избегаем превышения рейт лимита.');
        } else {
            $this->launch(function () {
                $this->info((new \App\Services\Cinema\KinopoiskVideosUpdate)($this->cron()));
            });
        }

    }
}
