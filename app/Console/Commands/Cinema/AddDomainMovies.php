<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;
use cinema\app\models\eloquent\Domain;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\OutputInterface;

class AddDomainMovies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:add-domain-movies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Добавить фильмы на сайты';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->launch(function () {

            /* ведем лог запросов, чтобы отобразить ниже */
            DB::enableQueryLog();

            $limit = 10;

            /* если первое выполнение, то сбрасываем счетчики moviesAddedPerDay */
            if (! $this->cron()->temp_var) {
                Domain::query()->update(['moviesAddedPerDay' => null]);
                $this->warn('Сбросили счетчик Domain::moviesAddedPerDay');
            }

            $result = (new \App\Services\Cinema\AddDomainMovies)($limit, $this->cron()->temp_var ?? 0);

            $this->cron()->temp_var = $result['lastId'];
            $this->cron()->save();

            if ($result['actions']->count() < $limit) {
                $this->finished();
            }

            // dd(\DB::getQueryLog());

            $this->warn('Запросов: '.count(DB::getQueryLog()));

            foreach ($result['actions'] as $action) {
                $this->info("#{$action['model']->id}");

                foreach ($action['domains'] as $domain => $domainData) {

                    $messages = collect();

                    foreach ($domainData['messages'] as $message) {

                        $color = $message['color'] ?? null;

                        if ($color == 'red') {
                            $messages[] = "<fg=red>{$message['message']}</>";
                        } elseif ($color == 'green') {
                            $messages[] = "<fg=green>{$message['message']}</>";
                        } else {
                            /* если обычный текст - не выводим, там ничего важного */
                            // $messages[] = $message['message'];
                        }
                    }

                    /* отображаем строку с доменом только когда есть сообщения */
                    if ($messages->count()) {
                        /* используем эту залупу, чтобы окрашивать рызнм цветом внутри одной строки */
                        $this->output->writeln("{$domain}: {$messages->implode(',')}", OutputInterface::OUTPUT_NORMAL);
                        // $this->line("{$domain}: {$messages->implode(',')}");
                    }

                }
                // $this->info($action);
                // $this->info("---");
            }
        });
    }
}
