<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CreateDomainGroupTextsByChatgpt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:create-domain-group-texts-by-chatgpt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создать тексты категорий через ChatGPT';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $result = (new \cinema\app\services\CreateDomainGroupTextsByChatgpt)->run();

        if (isset($result['id']) and isset($result['source']) and isset($result['rewrited'])) {
            $this->line($result['id']);
            $this->warn('Исходное:');
            $this->info($result['source']);
            $this->warn('Переписанное:');
            $this->info($result['rewrited']);
        } else {
            $this->info($result);
        }
    }
}
