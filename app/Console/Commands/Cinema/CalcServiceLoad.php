<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CalcServiceLoad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:calc-service-load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Расчитать нагрузку на биржу';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \cinema\app\controllers\CronController)->calcServiceLoad());
    }
}
