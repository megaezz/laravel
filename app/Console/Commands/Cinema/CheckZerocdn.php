<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CheckZerocdn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:check-zerocdn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Уведомить, если высокая скорость на Zerocdn';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \cinema\app\controllers\CronController)->checkZerocdn());
    }
}
