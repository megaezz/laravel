<?php

namespace App\Console\Commands\Cinema;

use App\Models\Cinema\MovieUpload;
use Illuminate\Console\Command;

class UpdateMovieUploads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-movie-uploads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить инфу по загрузкам';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // получаем все загрузки в статусе транскодируемых и проверяем не появился ли hls
        $transcodingMovieUploads = MovieUpload::where('is_transcoding', true)->get();

        foreach ($transcodingMovieUploads as $movieUpload) {

            if (! $movieUpload->hls_exists and $movieUpload->calc_hls_exists) {
                logger()->alert("MovieUpload #{$movieUpload->id} – обнаружен hls");
                $this->info("MovieUpload #{$movieUpload->id} – hls обнаружен");
            } else {
                $this->warn("MovieUpload #{$movieUpload->id} – hls не обнаружен");
            }

            $movieUpload->hls_exists = $movieUpload->calc_hls_exists;
            $movieUpload->save();
        }

        // есть активное транскодирование? (еще раз делаем такой же запрос, специально, вдруг выше где-то убрался статус is_transcoding)
        $transcodingCount = MovieUpload::where('is_transcoding', true)->count();

        // если нет запущенных транскодирований, то можно запустить одно
        if (! $transcodingCount) {

            // берем одну загрузку без hls и без статуса транскодирования и запускаем для нее команду
            $movieUpload = MovieUpload::where('hls_exists', false)->where('is_transcoding', false)->first();

            if (! $movieUpload) {
                $this->info('Нечего транскодировать');

                return;
            }

            try {
                $movieUpload->transcode();
                logger()->alert("MovieUpload #{$movieUpload->id} – запущено транскодирование");
            } catch (\Exception $e) {
                logger()->alert("MovieUpload #{$movieUpload->id} – ошибка запуска транскодирования: {$e->getMessage()}");
            }
        }
    }
}
