<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class CalculateRelatedMovies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:calc-related-movies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Расчитывает похожие фильмы';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->launch(function () {
            $this->info((new \App\Services\Cinema\CalculateRelatedMovies)($this->cron()));
        });
    }
}
