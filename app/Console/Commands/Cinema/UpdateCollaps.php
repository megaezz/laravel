<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class UpdateCollaps extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-collaps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные коллапса по апи';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->launch(function () {
            $result = (new \App\Services\Cinema\Balancers\CollapsUpdate)(250, $this->cron()->temp_var);
            if ($result['finished']) {
                $this->finished();
            } else {
                $this->cron()->temp_var = $result['next_page'];
                $this->cron()->save();
            }
            $this->table(['Сообщение', 'Следующая страница', 'Выполнено'], [[$result['message'], $result['next_page'], $result['finished'] ? 'Да' : 'Нет']]);
        });
    }
}
