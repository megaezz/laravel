<?php

namespace App\Console\Commands\Cinema;

use App\Services\Command;

class UpdateTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cinema:update-translations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить таблицу translations из Videodb';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \cinema\app\controllers\CronController)->updateTranslations());
    }
}
