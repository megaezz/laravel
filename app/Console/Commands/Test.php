<?php

namespace App\Console\Commands;

use App\Jobs\NazareWhalesBuyingRetranslator;
use App\Jobs\NazareXPumpRetranslator;
use App\Jobs\SendByAwmzoneBotTo;
use App\Jobs\SolTrendExclusiveRetranslator;
use App\Jobs\SolTrendRetranslator;
use App\Models\Abuse;
use App\Models\Cinema\Article;
use App\Models\Cinema\DomainMovie;
use App\Models\Cinema\MovieUpload;
use App\Models\Cinema\Torrent;
use App\Models\Cinema\TurboDomain;
use App\Models\Config as EngineConfig;
use App\Models\Domain;
use App\Models\ParsedUrl;
use App\Models\Proxy;
use App\Models\TelegramMessage;
use App\Services\Aria2;
use App\Services\Cinema\LinksForTurbo;
use App\Services\Cinema\PlaylistForPlayerjs;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Domain as EloquentDomain;
use cinema\app\models\eloquent\Dubbing;
use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\MovieEpisode;
use cinema\app\models\eloquent\Task;
use cinema\app\models\eloquent\Videodb;
use engine\app\models\Engine;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tst {method : Метод} {--o|option= : Опция 1} {--option2= : Опция 2} {--option3= : Опция 3} {--test : Тест}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $method = $this->argument('method');

        if (! $method) {
            return $this->error('Укажите название метода');
        }

        return $this->{$method}();
    }

    public function technitiumApi()
    {
        $domain = Domain::find('turboserial.com');
        // $domain = Domain::find('local.turboserial.com');
        // $domain = Domain::find('kinoroom.link');

        dd($domain->dns()->getZone());
    }

    /* проверяет все ли днс записи переехали с mas90@mail.ru на новые аккаунты */
    public function cloudflareMoveChecker()
    {
        $domains = Domain::whereNotNull('cloudflare_account_email')->where('cloudflare_account_email', '!=', 'ozerin.payment@gmail.com')->where('move_checked', false)->limit(50)->get();
        foreach ($domains as $domain) {
            // сверяем НС на старом и новом аккаунте клауда
            $fromAccountDns = collect($domain->dns->getDnsRecords()->result);
            $toAccountDns = collect($domain->dns->getDnsRecords()->result);
            $diff = [];
            $fromAccountDns->each(function ($record) use ($toAccountDns, &$diff) {
                $exists = $toAccountDns
                    ->where('name', $record->name)
                    ->where('type', $record->type)
                    ->where('content', $record->content)
                    ->where('proxied', $record->proxied)
                    ->first();
                if (! $exists) {
                    $diff[] = $record;
                }
            });
            dump($domain->domain, $diff);
            $domain->move_checked = true;
            $domain->save();
        }
        dd('done');
    }

    /* проверяем праильность работы requestHandler и генерации ссылок */
    public function requestHandler()
    {
        // $url = Domain::findOrFail('local.turboserial.com')->toRoute->prefix()->episodeView(\cinema\app\models\eloquent\DomainMovie::find(37116), \cinema\app\models\eloquent\MovieEpisode::find(357974));
        // dd($url);
        // $url = Domain::findOrFail('local-c.kinogo.awmzone.net')->toRoute->absolute()->movieView(\cinema\app\models\eloquent\DomainMovie::find(13157968));
        // dd($url);

        $url = 'http://local.turboserial.com/group/2~Зарубежные+сериалы~21-10-04';
        $url = 'http://local-google.turboserial.com/group/2~Зарубежные+сериалы~21-10-04';
        $url = 'http://local-c.kinogo.awmzone.net/31-biograficeskie';
        $url = 'http://local-y.kinogo.awmzone.net/31-biograficeskie';

        // $url = 'http://local.turboserial.com/watch/293234~Досон,+заключенный+№+10';
        // $url = 'http://local-c.kinogo.awmzone.net/13297628-legion-supergeroev.html';
        // $url = 'http://local.turboserial.com/group/2~Зарубежные+сериалы~21-10-04';
        // $url = 'http://local.turboserial.com/genres/1891~Детский';
        // $url = 'http://local-google.turboserial.com/genres/1891~Детский';
        // $url = 'http://local.turboserial.com/';
        // $url = 'http://local-yandex.turboserial.com/';
        // $url = 'http://local.turboserial.com/top50';
        // $url = 'http://local-yandex.turboserial.com/top50';
        // $url = 'http://local.turboserial.com/episode/357974~ddd';
        // $url = 'http://local-google.turboserial.com/episode/357974';
        // $url = 'http://local.kinogo.awmzone.net';
        $url = 'http://m.zagonkatv.one/shows/15888405-%D0%97%D0%B8%D0%BC%D0%BE%D1%80%D0%BE%D0%B4%D0%BE%D0%BA~33-18-06';

        $request = Request::create($url, 'GET');

        /*
        $domain = Domain::find('local-c.kinogo.awmzone.net')->registerRoutes();
        $response = app()->handle($request);

        // Проверяем успешность запроса
        if ($response->isSuccessful()) {
            // Получаем содержимое ответа
            $content = $response->getContent();
            // Ваш код обработки ответа здесь
            dd($content);
        } else {
            dd($response);
            // Обрабатываем ошибку
            dd("Request failed with status: " . $response->getStatusCode());
        }

        dd($request->get());
        */

        dd((new \App\Services\Cinema\RouteHandlers\RequestHandlerResolver)($request, \App\Services\Cinema\RouteHandlers\Dmca::class)['message']);

        $domain = Domain::find('local-c.kinogo.awmzone.net');
        // $domain = Domain::find('seasongo.net');
        // $domain = Domain::find('n18.seasongo.info');
        $domain->registerRoutes(true);
        $domainMovie = $domain->primary_domain->cinemaDomain->movies()->first();

        dd(
            $domain->toRoute->movieView($domainMovie),
            $domain->toRoute->absolute()->movieView($domainMovie),
            $domain->toRoute->movieView($domainMovie),
        );
    }

    /* сравниваем работу isMovieAllowed старой версии и новой */
    public function isMovieAllowed()
    {
        Engine::setType('cinema');
        Engine::setDomain('awmzone.net');

        $randomMovies = Movie::where('type', '!=', 'youtube')->inRandomOrder()->limit(5000)->get();

        foreach ($randomMovies as $randomMovie) {
            $randomDomain = Domain::inRandomOrder()->whereNull('allow_movies_only_from')->first();

            $results = [
                'megaweb' => $randomDomain->megaweb->getIsMovieAllowed($randomMovie->id),
                'laravel' => $randomDomain->isMovieAllowed($randomMovie),
            ];

            if ($results['megaweb'] != $results['laravel']) {
                dump([
                    'domain' => $randomDomain->domain,
                    'movie' => $randomMovie->id,
                ]);
                dump($results);
                // $this->alert('Отличаются');
            }
        }

        dd('ok');
    }

    public function toRoute()
    {

        $domain = Domain::find('turboserial.com');

        $routes = [
            $domain->toRoute->absolute()->index(),
            $domain->mirrors()->first()->toRoute->absolute()->index(),
            route('index', null, true, ['domain' => 'sub.example.com', 'scheme' => 'https']),
        ];

        dd($routes);
    }

    public function associateDomainToAbuses()
    {
        $abuses = Abuse::where('id', '>', EngineConfig::cached()->last_handled_rkn_link_id ?? 0)->limit(50000)->orderBy('id')->get();

        $abuses->each(function ($abuse) {
            Abuse::associateDomain($abuse);
            $abuse->save();
            $this->info("{$abuse->query} -> {$abuse->domain?->domain}");
        });

        EngineConfig::cached()->last_handled_rkn_link_id = $abuses->last()->id;

        EngineConfig::cached()->save();

        dd("Last: {$abuses->last()->id}");
    }

    public function duckDuckBot()
    {
        dd((new \App\Services\IsDuckDuckGoBot)('20.204.241.148'));
    }

    public function uptimeKumaUpdate()
    {
        dd((new \App\Services\UptimeKumaUpdate)());
    }

    /* учимся отдавать старые данные из кеша в случае ошибки */
    public function cacheRemember()
    {

        $cacheKey = 'example_data';

        $data = Cache::remember($cacheKey, 10, function () use ($cacheKey) {
            try {
                $data = ['some', 'data'];

                return $data;
            } catch (\Exception $e) {
                return Cache::get($cacheKey, []);
            }
        });

        dd($data);
    }

    public function fillFromDoctu()
    {

        $modelName = $this->option('option');
        $fromId = $this->option('option2');

        if (! $modelName) {
            return $this->error('Нужно указать имя модели --option=Model');
        }

        /* проходимся по всем моделям порционно по 1000 элементов и производим изменения */
        $builder = "\\App\\Models\\Doctor\\{$modelName}"::query();

        if ($fromId) {
            $builder->where('id', '>=', $fromId);
        }

        $builder->chunkById(1000, function ($items) {
            $items->each(function ($item) {
                $item->fillFromDoctu();
                if ($this->option('test')) {
                    dd($item);
                }
                $item->save();
            });

            $this->info("Last ID: {$items->last()->id}");
        });
    }

    public function serviceTree()
    {

        DB::connection('doctor')->enableQueryLog();

        $services = \App\Models\Doctor\Service::with('parentServiceRecursive')->whereIn('id', [2314, 3594])->get();

        // dd($services->first()->rootService->id);

        // dd($services->toArray());

        $groupedByParent = $services->groupBy(function ($service) {
            return $service->rootService;
        });

        $grouped = [];
        foreach ($groupedByParent as $parent => $childrens) {
            $grouped[] = (object) ['parent' => json_decode($parent), 'childrens' => $childrens];
            // dump(json_decode($parent)->id . ": " . count($childrens));
        }

        dd(collect($grouped));

        // dd('ok');

        // dd($services->groupBy(function($service){return $service->rootService->id;})->toArray());

        // dd(\DB::connection('doctor')->getQueryLog());
    }

    public function uploadMoviePoster()
    {
        // 42143
        // 57623
        // $cinemaUpdate = (new \cinema\app\services\cinema_update\CinemaService)->update();
        // dd($cinemaUpdate);

        $movie = Movie::find(290544);
        $uploader = (new \App\Services\Cinema\MovieUpdate\UploadPoster)($movie);
        $movie->save();
        dd($uploader);
    }

    public function kinocontent()
    {
        dd(Task::find(2462609)->is_confirmed);
        $tasks = Task::where('customer', 'ozerin')->express()->get();
        dd($tasks);
    }

    public function saveRecords()
    {
        $domain = Domain::find('local.xn--80aaomqiwgfv.cam')->parent_domain_or_self;
        dd($domain->dns->saveRecordsFromDatabaseToProvider());
    }

    // меняем настройки ECH на всех рдоительских доменах под клаудом
    public function cloudflare()
    {

        dd(Domain::find('kinoza.top')->dns->getZoneSettings());

        $limit = $this->option('option') ?? 1;

        $domains = Domain::query()
            ->doesntHave('parentDomain')
            ->whereRelation('dnsProvider', 'service', 'cloudflare')
            ->where('renewed', true)
            ->where(function ($query) {
                $query
                    ->where('tls_13', false)
                    ->orWhere('ech', true);
            });

        dump("Осталось: {$domains->count()}");

        $domains = $domains
            ->limit($limit)
            ->get();

        foreach ($domains as $domain) {
            $domain->tls_13 = true;
            $domain->ech = false;
            $domain->save();
            dump($domain->domain);
        }
    }

    public function domainErrors()
    {
        dd(Domain::find('ru.xn--80aaomqiwgfv.com')->errors);
    }

    public function whois()
    {

        dd(Domain::find('alfateaser.com')->whois);

        $domain = 'coldfilm.pics';

        if (preg_match('/\.(\w+)$/', $domain, $matches)) {
            $zone = $matches[1] ?? throw new \Exception('Нет зоны');
        }

        $host = "whois.nic.{$zone}";

        $output = shell_exec(escapeshellcmd("whois -h {$host} {$domain}"));

        dd($output);
    }

    public function mail()
    {
        $domain = Domain::find('awmzone.net');

        Mail::raw('Привет! Это простое текстовое сообщение.', function ($message) use ($domain) {
            $message
                ->from($domain->email, $domain->site_name)
                ->to('mas90@mail.ru')->subject('Тестовое сообщение');
        });
    }

    public function mysql()
    {
        $q = 100000;
        $speedtest = DB::table('speedtest');
        $speedtest->truncate();
        for ($i = 0; $i <= $q; $i++) {
            $speedtest->insert(['data' => sha1(time())]);
        }
        $time = Engine::getRunningTime();
        $this->info("Создано {$q} строк за {$time} сек");
    }

    public function article()
    {
        $article = \App\Models\Cinema\Article::first();
        dd($article->cinemaDomain->domain);
    }

    public function hasDatabasePrefix()
    {
        dd(\cinema\app\models\eloquent\Myshows::first()->touch());
    }

    public function videodb()
    {
        $videodbs = Movie::find(293640)->videodbs()->get();
        dd($videodbs->toArray());
    }

    public function domainsForTurbo()
    {
        $domains = Domain::query()
            ->where('type', 'cinema')
            ->whereNotIn('domain', [
                'vdb.awmzone.pro',
                'xn--d1aihcfio7a4e.com',
                'ru.filmy2021.cam',
                'ru.hdreska.club',
                'kinosok.net',
                'kinotext.info',
                'kinocontent.club',
                // не работал
                'ru.gidonline.bond',
                // не работал
                'ru.xn--e1adeid2baadu.org',
            ])
            ->get()
            // только продленные
            ->filter(fn($domain) => $domain->related_parent_domains->filter(fn($parent) => ($domain->is_masking_domain ? true : ! $parent->is_masking_domain) and $parent->renewed)->count());

        // dd($domains->pluck('domain'));

        $list = collect();

        foreach ($domains as $domain) {
            $googleDomain = "https://{$domain->getCrawlerDomain('Google')->domain_decoded}";
            $yandexDomain = "https://{$domain->getCrawlerDomain('Yandex')->domain_decoded}";
            $clientDomain = $domain->client_redirect_to ? "https://{$domain->clientRedirectTo->domain_decoded}" : null;
            if ($clientDomain == 'nu6.lordfilms.video') {
                // dd($yandexDomain == $googleDomain);
                // dd($googleDomain, $yandexDomain, $clientDomain);
            }
            if ($clientDomain) {
                if ($yandexDomain == $googleDomain) {
                    // $list[] = "{$googleDomain} -> {$clientDomain}";
                    $list[] = "{$googleDomain}";
                } else {
                    // $list[] = "{$googleDomain} -> {$clientDomain}";
                    // $list[] = "{$yandexDomain} -> {$clientDomain}";
                    $list[] = "{$googleDomain}";
                    $list[] = "{$yandexDomain}";
                }
            } else {
                if ($yandexDomain == $googleDomain) {
                    $list[] = $googleDomain;
                } else {
                    $list[] = $googleDomain;
                    $list[] = $yandexDomain;
                }
            }
        }

        $list->shuffle()->each(function ($item, $key) {
            $key++;
            $this->info("{$key}. {$item}");
        });
    }

    public function sendByAwmzoneBotTo()
    {
        $job = new SendByAwmzoneBotTo('kriptololChannel', 'test', TelegramMessage::find(196047));
        $job->handle();
    }

    public function reply()
    {
        dd(TelegramMessage::find(196047)->retranslations());
    }

    public function retranslator()
    {
        $volume = TelegramMessage::find(196109);
        $signalExclusive = TelegramMessage::find(196106);
        $volumeExclusive = TelegramMessage::find(196107);
        // $message = $signal;
        $message = $volume;
        // SolTrendExclusiveRetranslator::dispatchSync($message);
        // SolTrendRetranslator::dispatchSync($message);

        return 'ok';
        // SendByAwmzoneBotTo::dispatchSync(
        //     to: 'kriptololChannel',
        //     message: $message->message,
        //     retranslationOf: $message,
        // );
    }

    public function whaleBuyingRetranslator()
    {
        $message = TelegramMessage::find(196110);
        NazareWhalesBuyingRetranslator::dispatchSync($message);
    }

    public function aria2()
    {
        $method = $this->option('option');
        $argument = $this->option('option2');
        if ($this->option('option3')) {
            $option3 = json_decode($this->option('option3'), associative: true);
            // dd($option3);
            $result = (new Aria2('http://aria2:6800/jsonrpc'))->{$method}($this->option('option2'), $option3);
        } else {
            $result = (new Aria2('http://aria2:6800/jsonrpc'))->{$method}($this->option('option2'));
        }
        dd($result);
    }

    public function torrent()
    {
        // dd(filesize('/var/www/public/storage/cinema/torrents/01JJCWRKTF35D95Z3NRQPTRQZX.torrent'));
        dd(Storage::disk('public')->size('cinema/torrents/01JJCWRKTF35D95Z3NRQPTRQZX.torrent'));
    }

    public function awmzonebot()
    {
        logger()->alert('test ddd');
        // SendByAwmzoneBotTo::dispatchSync(to: 'me', message: 'test');
    }

    public function xpump()
    {
        // $signal = 196704;
        $signal = 196700;
        // $growth = 196702;
        $growth = 196699;

        $message = TelegramMessage::find($growth);
        NazareXPumpRetranslator::dispatchSync($message);
    }

    public function madeline()
    {
        $MadelineProto = new \danog\MadelineProto\API(storage_path('madeline/saulgoodie'));
        // $messages = $MadelineProto->messages->getMessages([
        //     'id' => [200653]
        // ]);
        // dd($messages);
        $link = $MadelineProto->downloadToDir('AgACAgIAAx0CcD70NQABAzjgZ6FRVFvUvSV5--OwFoP-KF1AEvUAAtntMRtJVBFJ1TJ-abTeE2cBAAMCAAN5AAMvBA', storage_path('madeline/tmp'));
        dd($link);
    }

    public function postToThread()
    {
        return SendByAwmzoneBotTo::dispatchSync(to: 'kriptololSolTrends', message: 'test');
    }

    public function torrentFiles()
    {
        $torrent = Torrent::find(3);
        dd($torrent->downloaded_movies);
        $files = collect($torrent->aria2_content_data['files']);
        dd($torrent->aria2_content_data['files']);
    }

    public function movieUploadOptions()
    {
        $movieUpload = new MovieUpload;
        $options = $movieUpload->torrent?->downloaded_movies
            // ->diff($movieUpload->torrent->uploads()->where('id', '!=', $movieUpload->id)->get()->pluck('source_video'))
            ->sort()
            ->mapWithKeys(fn($value) => [$value => basename($value)]);

        dd($options);
    }

    public function createMovieUploads()
    {
        $torrent = Torrent::find(17);
        dd($torrent->createMovieUploads());
    }

    public function aria2Data()
    {
        $torrent = Torrent::find(17);
        dd($torrent->aria2_content_data);
    }

    public function zerocdnSignedUrl()
    {
        // dd(Config::cached()->videodb_credentials);
        // $url = 'https://cdn.awmzone.pro/turbo/95/hls.m3u8';
        $url = 'http://cdn.awmzone1.pro/tvseries/7608be3962eb324fa610dc9d7f073e4e5e129c3d/hls.m3u8';
        $signedUrl = Config::cached()->zerocdnService->getSignedUrl(url: $url, method: 'ip', ip: '1.47.153.242');
        dd($signedUrl);
    }

    public function dubbingUploads()
    {
        dd(Dubbing::find(1807)->movieUploads()->ddrawsql());
    }

    public function linksForTurbo()
    {
        $movie = Movie::find(1);

        $result = (new LinksForTurbo)($movie);

        dd($result);
    }

    public function relatedDomains()
    {
        // $domain = Domain::find('turboserial.com');
        // dd($domain->related_parent_domains->pluck('domain'));

        // Domain::query()
        // ->where();

        // $oldMethodDomains = Domain::query()
        // ->where('type', 'cinema')
        // ->get()
        // ->filter(fn($domain) => $domain->related_parent_domains->filter(fn ($parent) => ($domain->is_masking_domain ? true : ! $parent->is_masking_domain) and $parent->renewed)->count());

        $domains = Domain::query()
            ->where('type', 'cinema')
            ->hasRenewedParentDomainOrHasNonMaskingMirrorsThatHasRenewedParentDomains()
            ->get();

        // dd($domains->diff($oldMethodDomains)->pluck('domain'));

        dd($domains->count());

        $mirrorsWithSelf = Domain::query()
            ->where(
                fn($query) => $query
                    ->where('domain', $domain)
                    ->orWhere('mirror_of', $domain)
            );

        $mirrorsWithSelfThatHasRenewedParentDomains = $mirrorsWithSelf
            ->whereRelation('parentDomain', 'is_masking_domain', false)
            ->hasRenewedParentDomain();
        dd($mirrorsWithSelfThatHasRenewedParentDomains->pluck('domain'));
    }

    public function clinicJob()
    {

        $failedJob = DB::table('failed_jobs')->where('id', 2)->first();

        dd(unserialize(json_decode($failedJob->payload)->data->command));
        $decoded = json_decode($json);

        if (json_last_error() !== JSON_ERROR_NONE) {
            dd(json_last_error_msg());
        }

        dd($decoded);
    }

    public function domainHasCinemaDomain()
    {
        // dd(DomainMovie::first()->engineDomain);
        // dd(EloquentDomain::first()->engineDomain);
        // dd(Domain::first());
        dd(Domain::whereHas('cinemaDomain')->ddrawsql());
    }

    public function spatieTranslation()
    {
        $article = Article::where('name->ru', 'Русский')->first();
        $article->name = ['ru' => 'Русский', 'en' => 'English'];
        $article->save();
        dd('ok');
        $article = new Article;
        $article->name = 'Русский';
        $article->save();
    }

    public function localizableTest()
    {
        // dump(app()->getLocale(), config('app.locale'));
        // App::setlocale('fr');
        // dump(app()->getLocale(), config('app.locale'));
        // dd('ok');
        // app()->setLocale('fr');
        dd(Movie::first()->title);
    }

    public function tableName()
    {
        dd((new Domain)->getTable());
    }

    public function selectonlyNeededFiles()
    {
        $torrent = Torrent::find(145);
        dd($torrent->aria2->selectonlyNeededFiles());
    }

    public function kinogoParser()
    {
        $proxy = Proxy::where('country', 'UA')->whereActive(true)->first()->proxy;
        $movieParser = ParsedUrl::parse();
        $movieParser->clientSettings = ['proxy' => $proxy, 'base_uri' => 'https://kinogo.inc'];
        $movieParser->useStoredData = true;
        $result = $movieParser->parse('https://kinogo.inc/films/28122-rayn.html');
        dd($result);
    }

    public function playlistForPlayerjs()
    {
        // dd(MovieEpisode::find(405011)->uploads);
        // cериал в котором есть и videodb и uploads в разных озвучках
        $domainMovie = DomainMovie::find(16820);
        // просто фильм
        $domainMovie = DomainMovie::find(13273129);
        // фильм, для которого нет видео
        $domainMovie = DomainMovie::find(16772);
        $playlist = (new PlaylistForPlayerjs)($domainMovie);
        dd($playlist);
    }

    public function movieUploadTouch()
    {
        dd(MovieUpload::find(4)->transcodingCommand());
    }

    function onlyRedirectTo()
    {
        $domain = $this->option('option');
        dd(Domain::where('domain', $domain)->onlyRedirectTo()->first());
    }

    function turboDomains()
    {
        dd(TurboDomain::find('turboserial.com')->priority_dubbing);
        $domains = Domain::onlyTurbo()->pluck('domain');
        dd($domains);
    }

    function currentDubbing()
    {
        dd(TurboDomain::find('baskino7.com')->currentDubbing()->limit(1)->get());
    }

    function missingTurboDomains()
    {
        $domains = Domain::query()->onlyTurbo();

        dd($domains->ddrawsql());
    }

    function domainDecoded()
    {
        Domain::chunk(20, fn($domains) => $domains->each(function ($domain) {
            $domain->domain_decoded = $domain->domain_decoded;
            $domain->save();
        }));
    }
}
