<?php

namespace App\Console\Commands\Engine;

use App\Models\Cache;
use App\Models\Config;
use App\Services\Command;
use engine\app\models\F;

class ClearCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:clear-cache {--i|iterations= : Итераций}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удалить устаревший кеш';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $lifetime = Config::cached()->cacheLifetime / 60 / 60 / 24;

        /* когда удаляет больше 30к записей - начинает люто глючить, особенно если работает вместе с related_movies, поэтому временным решением ограничиваем количество итераций до 6 (как раз 30к записей) и повышаем частоту работы крона, чтобы гарантировать очистку кеша */
        // $iterations = $this->option('iterations') ?? 6;
        $iterations = $this->option('iterations');

        /* удаляем записи созданные более cacheLifetime секунд назад */
        /* удаляем порционально чтобы БД не сдохла */

        $i = 0;
        $deletedSum = 0;
        /* удалять по 1000 строк - рекомендация с https://mariadb.com/kb/en/big-deletes/ */
        $chunkSize = 1000;
        /* максимальное время на удаление одной порции 1 сек */
        $maxTime = 2000;
        /* спим 1 сек между удалениями порций */
        $sleepTime = 500;
        /* удалять кеш до этой даты, расчитываем ее здесь, иначе в do она все время увеличивается, всегда есть что удалить и получается бесконечное выполнение */
        $beforeDate = now()->subDays($lifetime);

        $toBeDeleted = Cache::where('updated_at', '<', $beforeDate)->count();

        $this->info("К удалению: {$toBeDeleted} записей");

        // foreach ($toBeDeleted->chunk($chunkSize) as $chunk) {

        // foreach ($chunk as $cache) {
        /* приходится удалять так, т.к. модель кеш неправильно описана, она считает hash первичным ключом, хотя в действительности он составной - hash, domain */
        // $deleted = Cache::where('hash', $cache->hash)->where('domain', $cache->domain)->delete();
        // $deletedSum += $deleted;
        // }

        // $i++;

        // if ($iterations and $i >= $iterations ) {
        // $this->info("Завершено после {$i} итераций");
        // break;
        // }

        /* после каждых chunkSize даем базе просраться */
        // $this->info("Удалено {$chunkSize} записей, ждем 5 сек");
        // sleep(5);
        // }

        do {

            $timeStart = F::runtime();

            $deleted = Cache::query()
                ->where('updated_at', '<', $beforeDate)
                ->limit($chunkSize)
                ->delete();

            $timeEnd = F::runtime();

            $deletedSum = $deletedSum + $deleted;
            $i++;

            $time = $timeEnd - $timeStart;

            /* после каждых chunkSize даем базе просраться */
            $this->info("Удалено {$deleted} записей за {$time} ms, ждем {$sleepTime} ms");

            if ($iterations and $i >= $iterations) {
                $this->info("Завершено после {$i} итераций");
                break;
            }

            if ($time > $maxTime) {
                $message = "Удаление порции кэша заняло более {$maxTime} ms ({$time} ms), останавливаем выполнение";
                $this->info($message);
                logger()->alert($message);
                break;
            }

            usleep($sleepTime * 1000);

        } while ($deleted > 0);

        $message = "Удалили кэш старше {$lifetime} дней: {$deletedSum}";
        logger()->alert($message);
        $this->info($message);
    }
}
