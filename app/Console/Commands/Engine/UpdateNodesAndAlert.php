<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class UpdateNodesAndAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:update-nodes-and-alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные нод и сообщить о проблемах';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \engine\app\services\UpdateNodeDatasAndAlert)->handler());
    }
}
