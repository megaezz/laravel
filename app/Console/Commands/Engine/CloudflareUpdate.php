<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class CloudflareUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:cloudflare-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные Cloudflare аккаунтов';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->info((new \App\Services\Crons\Engine\CloudflareUpdate)());
    }
}
