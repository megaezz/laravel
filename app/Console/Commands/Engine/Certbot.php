<?php

namespace App\Console\Commands\Engine;

use App\Models\Domain;
use App\Services\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

class Certbot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:certbot {action? : Действие (по умолчанию autoObtain)} {--domain= : Домен} {--validation= : Токен валидации} {--dry : Выполнить в тестовом режиме}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Выдать wildcard сертификат';

    protected Domain $domain;

    private $configDir = 'storage/certbot/certs';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        /* если не указан action, то выполняем autoObtain */
        $action = $this->argument('action') ? Str::camel($this->argument('action')) : 'autoObtain';

        /* для всех действий, кроме renew, autoObtain и delete нужно указать домен */
        if (! in_array($action, ['renew', 'autoObtain', 'certificates', 'delete'])) {

            if (! $this->option('domain')) {
                return $this->error('Specify --domain');
            }

            $this->domain = Domain::findOrFail($this->option('domain'));
        }

        if (! method_exists($this, $action)) {
            return $this->error("Action {$action} doesn't exist");
        }

        return $this->{$action}();
    }

    public function authHook()
    {

        $validation = $this->option('validation');

        $result = $this->domain->dns->addDnsRecord(type: 'TXT', domain: "_acme-challenge.{$this->domain->domain}", content: $validation, proxied: false);

        $result = json_encode($result);

        $this->info("DNS record {$validation} added for {$this->domain->domain}. Result: {$result}");

        /* спим, чтобы сертбот смог увидеть проставленную запись */
        sleep(20);
    }

    public function cleanupHook()
    {

        $this->domain->dns->deleteDnsRecords(type: 'TXT', domain: "_acme-challenge.{$this->domain->domain}");

        $this->info("DNS records _acme-challenge removed from {$this->domain->domain}");
    }

    public function obtainCertificate()
    {

        try {
            return $this->info($this->obtainCertificateHandler(domain: $this->domain, dry: $this->option('dry')));
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function obtainCertificateHandler($domain, $dry)
    {

        $email = 'xrest.contact@gmail.com';

        $dry = $dry ? '--dry-run' : null;

        $command = "certbot certonly --config-dir {$this->configDir} --manual --preferred-challenges=dns --manual-public-ip-logging-ok --expand --agree-tos --manual-auth-hook 'scripts/auth-hook.sh' --manual-cleanup-hook 'scripts/cleanup-hook.sh' -d {$domain->domain} -d *.{$domain->domain} --email {$email} --no-eff-email {$dry}";

        $process = Process::fromShellCommandline($command);
        $process->setTimeout(3600);
        $process->run();

        if (! $process->isSuccessful()) {
            throw new \Exception($process->getErrorOutput());
        }

        return $process->getOutput();
    }

    public function renew()
    {
        passthru("certbot renew --config-dir {$this->configDir}");
        /* этот способ не работает когда есть непродленные сертификаты */
        // $process = Process::fromShellCommandline("certbot renew --config-dir {$this->configDir}");
        // $process->run();
        // $this->info($process->getOutput());
    }

    // просто вызвать engine:certbot delete и выбрать номер из списка
    public function delete()
    {
        passthru("certbot delete --config-dir {$this->configDir}");
    }

    /* почему то не работает через артизан когда есть непродленные сертификаты, только напрямую работает */
    public function certificates()
    {
        passthru("certbot certificates --config-dir {$this->configDir}");
        /* этот способ не работает когда есть непродленные сертификаты */
        // $process = Process::fromShellCommandline("certbot certificates --config-dir {$this->configDir}");
        // $process->run();
        // $this->info($process->getOutput());
    }

    /* смотрим для каких доменов есть сертификаты, сопоставляет с бд, и если нет нужного сертификата - выдает */
    public function autoObtain()
    {

        $exists = collect(Storage::disk('certbot')->directories('certs/live'))->map(function ($directory) {
            return basename($directory);
        });

        $domain = Domain::query()
            ->where('certbot', true)
            ->where('renewed', true)
            ->whereNotIn('domain', $exists)
            ->first();

        if (! $domain) {
            return $this->info('Нет доменов, требующих выдачи сертификатов');
        }

        $text = "Выдаем сертификат для {$domain->domain}";
        $this->info($text);
        logger()->alert($text);

        try {
            $result = $this->obtainCertificateHandler(domain: $domain, dry: $this->option('dry'));
            $text = "Результат выдачи сертификата для {$domain->domain}: {$result}";
            logger()->alert($text);
            exec("chmod -R 755 {$this->configDir}/live");
            exec("chmod -R 755 {$this->configDir}/archive");
            $chmodInfo = 'Проставили рекурсивно 755 на live и archive';
            logger()->alert($chmodInfo);
            $this->info($chmodInfo);

            return $this->info($text);
        } catch (\Exception $e) {
            $text = "Ошибка в процессе выдачи сертификата для {$domain->domain}: {$e->getMessage()}";
            logger()->alert($text);

            return $this->error($text);
        }
    }
}
