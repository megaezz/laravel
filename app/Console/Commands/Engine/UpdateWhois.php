<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class UpdateWhois extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:update-whois {domain? : Домен}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные whois';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \engine\app\services\UpdateDomainsWhois)(domain: $this->argument('domain')));
    }
}
