<?php

namespace App\Console\Commands\Engine;

use App\Models\Config;
use App\Models\Log;
use App\Services\Command;

class ClearLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:clear-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удалить устаревшие логи';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $lifetime = Config::cached()->logsLifeTime / 60 / 60 / 24;

        $deleted = Log::where('date', '<', now()->subDays($lifetime))->delete();

        $message = "Удалили логи старше {$lifetime} дней: {$deleted}";

        logger()->alert($message);
        $this->info($message);
    }
}
