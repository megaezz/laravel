<?php

namespace App\Console\Commands\Engine;

use App\Models\Config;
use App\Services\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class CheckQueryQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:check-query-queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверить очередь запросов и отключить движок если высокая';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $q = count(DB::select('SHOW FULL PROCESSLIST'));
        $config = Config::cached();
        $action = null;
        $doLog = false;
        if ($q > $config->maxQueryQueue) {
            $action = ($config->cronSwitch) ? 'Выключили движок' : 'Оставили движок выключенным';
            $config->cronSwitch = false;
            // Artisan::call('down', ['--refresh' => 2]);
            $doLog = true;
        } else {
            $action = ($config->cronSwitch) ? 'Все ок' : 'Включили движок';
            $doLog = $config->cronSwitch ? false : true;
            $config->cronSwitch = true;
            // Artisan::call('up');
        }
        $config->save();
        $message = "Очередь запросов: {$q} шт. {$action}";
        if ($doLog) {
            logger()->alert($message);
        }
        $this->info($message);
    }
}
