<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class UpdateMetrika extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:update-metrika';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные метрики';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \engine\app\controllers\CronController)->getMetrikaData());
    }
}
