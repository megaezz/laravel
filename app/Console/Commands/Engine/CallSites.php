<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class CallSites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:call-sites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Дернуть главные рандомных сайтов (чтобы сформировался кеш)';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \engine\app\controllers\CronController)->callSites());
    }
}
