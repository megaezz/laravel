<?php

namespace App\Console\Commands\Engine;

use App\Models\Ip;
use App\Services\Command;

class UpdateIps extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:update-ips';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные IP адресов';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $limit = 50;

        $ips = Ip::orderBy('updated_at')->limit($limit)->get();

        foreach ($ips as $ip) {
            try {
                $ip->updateData();
                /* если были изменения, сохраняем, если нет, просто меняем updated_at */
                if ($ip->isDirty()) {
                    $ip->save();
                } else {
                    $ip->touch();
                }
            } catch (\Exception $e) {
                logger()->alert("Ошибка при обновлении данных IP: {$e->getMessage()}");
            }
        }

        $ips = $ips->map(function ($ip) {
            $ip->is_real_bot_txt = $ip->is_real_bot ? 'Yes' : 'No';

            return $ip->only(['ip', 'hostname', 'is_real_bot_txt', 'created_at', 'updated_at']);
        })->toArray();

        $this->table(
            ['IP', 'Hostname', 'Is real bot', 'Created At', 'Updated At'],
            $ips
        );
    }
}
