<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;
use engine\app\models\F;

/* Оказывается Laravel сам чистит сессии, точно таким же способом как и здесь. Берет SESSION_LIFETIME и чистит каждый рандомный реквест через лотерею 2 из 100. Я изменил в конфиге сессий лотерею на 0 из 100. */

class ClearSessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:clear-sessions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удалить истекшие сессии';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $lifetime = config('session.lifetime');

        $deletedSum = 0;
        /* удалять по 1000 строк - рекомендация с https://mariadb.com/kb/en/big-deletes/ */
        $chunkSize = 1000;
        /* максимальное время на удаление одной порции 1 сек */
        $maxTime = 2000;
        /* спим 1 сек между удалениями порций */
        $sleepTime = 500;
        /* удалять сессии до этой даты, расчитываем ее здесь, иначе в do она все время увеличивается, всегда есть что удалить и получается бесконечное выполнение */
        $beforeDate = now()->subMinutes($lifetime)->timestamp;

        do {

            $timeStart = F::runtime();

            $deleted = \App\Models\Session::query()
                ->where('last_activity', '<', $beforeDate)
                ->limit($chunkSize)
                ->delete();
            $deletedSum = $deletedSum + $deleted;

            $timeEnd = F::runtime();

            $time = $timeEnd - $timeStart;

            /* после каждых chunkSize даем базе просраться */
            $this->info("Удалено {$deleted} записей за {$time} ms, ждем {$sleepTime} ms");

            if ($time > $maxTime) {
                $message = "Удаление порции laravel сессий заняло более {$maxTime} ms ({$time} ms), останавливаем выполнение";
                $this->info($message);
                logger()->alert($message);
                break;
            }

            usleep($sleepTime * 1000);

        } while ($deleted > 0);

        $message = "Удалили истекшие сессии: {$deletedSum}";

        logger()->alert($message);
        $this->info($message);
    }
}
