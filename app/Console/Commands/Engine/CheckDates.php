<?php

namespace App\Console\Commands\Engine;

use engine\app\models\F;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:check-dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Сравнить время бд с серверным';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $this->info('ok');
        /* проверяем даты на бд и серверах */

        $arr['php'] = now()->format('Y-m-d H:i:s');
        $arr['mysql'] = DB::select('SELECT current_timestamp')[0]->current_timestamp;
        $arr['mysql_v2'] = F::query_assoc('select current_timestamp')['current_timestamp'];
        dump($arr);
    }
}
