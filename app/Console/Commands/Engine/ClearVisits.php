<?php

namespace App\Console\Commands\Engine;

use App\Models\Config;
use App\Models\Visit;
use App\Services\Command;
use engine\app\models\F;

class ClearVisits extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:clear-visits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удалить устаревшие визиты';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $lifetime = Config::cached()->visits_lifetime;

        /* удаляем порционно с учетом затраченного времени, чтобы БД не сдохла */

        $i = 0;
        $deletedSum = 0;
        /* удалять по 1000 строк - рекомендация с https://mariadb.com/kb/en/big-deletes/ */
        $chunkSize = 1000;
        /* максимальное время на удаление одной порции 1 сек */
        $maxTime = 2000;
        /* спим 1 сек между удалениями порций */
        $sleepTime = 500;
        /* удалять визиты до этой даты, расчитываем ее здесь, иначе в do она все время увеличивается, всегда есть что удалить и получается бесконечное выполнение */
        $beforeDate = now()->subDays($lifetime);

        $toBeDeleted = Visit::where('date', '<', $beforeDate)->count();

        $this->info("К удалению: {$toBeDeleted} записей");

        do {

            $timeStart = F::runtime();

            $deleted = Visit::query()
                ->where('date', '<', $beforeDate)
                ->limit($chunkSize)
                ->delete();

            $timeEnd = F::runtime();

            $deletedSum = $deletedSum + $deleted;
            $i++;

            $time = $timeEnd - $timeStart;

            /* после каждых chunkSize даем базе просраться */
            $this->info("Удалено {$deleted} записей за {$time} ms, ждем {$sleepTime} ms");

            if ($time > $maxTime) {
                $message = "Удаление порции визитов заняло более {$maxTime} ms ({$time} ms), останавливаем выполнение";
                $this->info($message);
                logger()->alert($message);
                break;
            }

            usleep($sleepTime * 1000);

        } while ($deleted > 0);

        $message = "Удалили визиты старше {$lifetime} дней: {$deletedSum}";
        logger()->alert($message);
        $this->info($message);
    }
}
