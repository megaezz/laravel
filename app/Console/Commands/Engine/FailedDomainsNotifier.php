<?php

namespace App\Console\Commands\Engine;

use App\Models\Config;
use App\Models\Domain;
use App\Services\Command;

class FailedDomainsNotifier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:failed-domains-notifier';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Уведомление о failed доменах';

    /**
     * Execute the console command.
     */
    public function handler()
    {

        $domains = Domain::with('parentDomain')
            ->monitored()
            ->where('monitoring', false)
        // в боте нас интересуют в основном только неизвестные блоки по RU, поэтому добавляем это условие, чтобы не было уведомления о разблокированных по ру сайтам
            ->where('monitoring_ru', false)
            ->get();

        $logLimit = 4;

        $countries = collect(Config::cached()->monitored_countries);

        if ($domains->count()) {
            logger()->alert('Ошибки мониторинга:'.PHP_EOL.$domains->take($logLimit)->map(fn ($domain) => '['.$countries->filter(fn ($country) => ! $domain->countryMonitoring($country))->implode(', ').'] '.$domain->domain_decoded)->implode(PHP_EOL).(($domains->count() > $logLimit) ? (PHP_EOL.'и еще '.($domains->count() - $logLimit).' шт.') : ''));
        }
    }
}
