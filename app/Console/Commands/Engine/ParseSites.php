<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class ParseSites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:parse-sites {--parser= : Класс парсера} {--data= : Данные парсеру}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсить сайты';

    /**
     * Execute the console command.
     */
    public function handler()
    {
        $this->launch(function () {

            /* если передан парсер через опцию - используем его, если нет - данные крона */
            if ($this->option('parser')) {
                $parser = $this->option('parser');
                $data = $this->option('data');
            } else {
                $parser = $this->cron()->temp_var;
                $data = $this->cron()->temp_var_2;
            }

            /* не оборачиваю в try catch, чтобы крон отвалился */
            $data = (new \App\Services\Crons\Engine\ParseSites)($parser, $data);

            // try {
            //     $data = (new \App\Services\Crons\Engine\ParseSites)($this->cron()->temp_var, $this->cron()->temp_var_2);
            // } catch (\Exception $e) {
            //     $this->error($e->getMessage());
            //     return;
            // }

            /* сохраняем данные крона, только если не передан парсер вручную */
            if (! $this->option('parser')) {
                if ($data['result']['finished']) {
                    $this->finished();
                } else {
                    $this->cron()->temp_var = $data['parser'];
                    $this->cron()->temp_var_2 = $data['result']['data'];
                    $this->cron()->save();
                }
            }

            $this->table(['Парсер', 'Данные'], [[$data['parser'], json_encode($data['result'])]]);
        });
    }
}
