<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class DomainMonitoringActual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:domain-monitoring-actual';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Мониторинг всех актуальных доменов и зеркал';

    /**
     * Execute the console command.
     */
    public function handler()
    {

        passthru('php artisan engine:domain-monitoring --actual');

    }
}
