<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class CheckCrons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:check-crons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Уведомить, если отвалились кроны';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $result = (new \App\Services\CheckCrons)();

        if ($result['errorCrons']->count()) {
            $message = "Отвалились кроны: {$result['errorCrons']->pluck('command')->implode(', ')}";
            $this->info($message);
            logger()->alert($message);
        }

        if ($result['staleCrons']->count()) {
            $message = "Кроны давно не завершаются, проверить: {$result['staleCrons']->pluck('command')->implode(', ')}";
            $this->info($message);
            logger()->alert($message);
        }

        if (! $result['errorCrons']->count() and ! $result['staleCrons']->count()) {
            $this->info('Все ок');
        }

        // $this->info((new \engine\app\controllers\CronController)->checkCrons());
    }
}
