<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class CheckRequestsPerSecond extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:check-requests-per-sec';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Уведомить, если большое количество http запросов в секунду';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $this->info((new \engine\app\controllers\CronController)->checkRequestsPerSecond());
    }
}
