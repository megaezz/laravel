<?php

namespace App\Console\Commands\Engine;

use App\Models\Ban;
use App\Models\Config;
use App\Services\Command;

class CalcBanQueriesPerSecond extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:calc-ban-queries-per-second';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Расчитать количество забаненных запросов в секунду';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $last_quantity = Config::cached()->ban_queries;
        $now_quantity = (int) Ban::sum('q');
        Config::cached()->ban_queries = $now_quantity;
        /* делим на 60, потому что задание выполняется раз в минуту, поэтому если выполнять задание чаще минуты, то данные будут некорректные. корявенько, но пока так. */
        Config::cached()->ban_queries_per_second = ceil(($now_quantity - $last_quantity) / 60);
        Config::cached()->save();
        $this->info(Config::cached()->ban_queries_per_second);
    }
}
