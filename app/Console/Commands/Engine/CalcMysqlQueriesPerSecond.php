<?php

namespace App\Console\Commands\Engine;

use App\Models\Config;
use App\Services\Command;
use Illuminate\Support\Facades\DB;

class CalcMysqlQueriesPerSecond extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:calc-mysql-queries-per-second';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Расчитать количество mysql запросов в секунду';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handler()
    {
        $last_quantity = Config::cached()->mysql_queries;
        $now_quantity = (int) DB::select("SHOW GLOBAL STATUS LIKE 'Queries';")[0]->Value;
        Config::cached()->mysql_queries = $now_quantity;
        /* делим на 60, потому что задание выполняется раз в минуту, поэтому если выполнять задание чаще минуты, то данные будут некорректные. корявенько, но пока так. */
        Config::cached()->mysql_queries_per_second = ceil(($now_quantity - $last_quantity) / 60);
        Config::cached()->save();
        $this->info(Config::cached()->mysql_queries_per_second);
    }
}
