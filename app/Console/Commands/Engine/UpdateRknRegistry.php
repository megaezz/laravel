<?php

namespace App\Console\Commands\Engine;

use App\Models\Rkn;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class UpdateRknRegistry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:update-rkn-registry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить реестр РКН';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $data = Http::timeout(300)->get('https://reestr.rublacklist.net/api/v3/snapshot/')->json();
        // $data = Storage::json('snapshot.json');

        // берем первый элемент массива, не зная имя его ключа (всегда разная дата) с помощью reset и делаем коллекцию из него
        $items = collect(reset($data));

        $chunk = 5000;
        $totalCount = 0;

        $items->chunk($chunk)->each(function ($items) use (&$totalCount) {
            // получаем записи, которые уже есть в бд
            $rkns = Rkn::with('domains')->whereIn('id', $items->pluck('id'));
            // обновляем updated_at на выборке
            $rkns->update(['updated_at' => now()]);
            // получаем коллекцию из выборки и делаем ключами id
            $rkns = $rkns->get()->keyBy('id');
            // оставляем только записи, которых нет в бд
            // $items = $items->filter(fn($item) => !isset($rkns[$item['id']]));

            foreach ($items as $item) {
                $rkn = $rkns->get($item['id']);
                // если такой записи нет в бд, то создаем
                if (! $rkn) {
                    $rkn = new Rkn;
                    $rkn->id = $item['id'];
                }
                // теперь обновляем данные
                $rkn->data = $item;
                // сохраняемся если надо
                if ($rkn->isDirty()) {
                    $rkn->save();
                }
                if (! $rkn->domains->count()) {
                    $rkn->domains()->createMany(collect($rkn->data->domains)->map(fn ($domain) => ['domain' => $domain]));
                }
            }

            $totalCount = $totalCount + $items->count();
            $this->info("Обработано: {$items->count()}. Всего: {$totalCount}");
        });

        $this->info('Готово');
        logger()->alert('Обновлен реестр РКН');
    }
}
