<?php

namespace App\Console\Commands\Engine;

use App\Services\Madeline\SaulGoodieSettings;
use App\Services\TelegramEventHandler;
use Illuminate\Console\Command;

class SaulGoodieProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:saul-goodie-process {--login : Создать сессию}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Процесс, получающий все события из телеграм аккаунта SaulGoodie';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($this->option('login')) {
            return SaulGoodieSettings::login();
        }

        if (app()->environment('production')) {
            TelegramEventHandler::startAndLoop(SaulGoodieSettings::session());
        } else {
            $this->info('Не прод, просто спим 60 секунд для супервайзера');
            sleep(60);
        }
    }
}
