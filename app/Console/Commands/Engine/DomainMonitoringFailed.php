<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class DomainMonitoringFailed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:domain-monitoring-failed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Мониторинг всех проблемных доменов и зеркал';

    /**
     * Execute the console command.
     */
    public function handler()
    {

        passthru('php artisan engine:domain-monitoring --failed');

    }
}
