<?php

namespace App\Console\Commands\Engine;

use Illuminate\Console\Command;

class CheckDbConnections extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:check-db-connections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверить доступные соединения с БД';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        // Получаем все доступные соединения
        $connections = collect(config('database.connections'))
            ->where('driver', 'mariadb')
        // избегаем странность, что само по себе создается соединение mariadb и вызывает тут ошибку
            ->where('password', '!=', '');

        foreach ($connections as $connection => $settings) {
            try {
                $dbTime = \DB::connection($connection)->query()->selectRaw('unix_timestamp(current_timestamp) as time')->first()->time;
            } catch (\Exception $e) {
                $message = "Connection \"{$connection}\": {$e->getMessage()}";
                logger()->alert($message);
                $this->error($message);

                continue;
            }
            $phpTime = time();
            // допустимая разница между временем mysql и php - 10 секунд
            if (abs($dbTime != $phpTime) > 10) {
                $message = "Connection \"{$connection}\": PHP and DB Time dismatch ({$dbTime} vs {$phpTime})";
                logger()->alert($message);
                $this->error($message);
            } else {
                $message = "Connection \"{$connection}\": Все ок";
                $this->info($message);
            }
        }
    }
}
