<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:backup {--info : Инструкции} {--dry : Вывести только команды} {--log : Показать лог} {actions? : Действия}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создать бекап';

    /**
     * Execute the console command.
     */
    public function handler()
    {

        $backupService = new \engine\app\services\Backup(dry: $this->option('dry'));

        if ($this->option('info')) {
            $this->info($backupService->info());

            return;
        }

        if ($this->option('log')) {
            passthru($backupService->log());

            return;
        }

        try {
            $this->info($backupService->handler(actions: $this->argument('actions')));
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            if (! $this->option('dry')) {
                logger()->alert($e->getMessage());
            }
        }
    }
}
