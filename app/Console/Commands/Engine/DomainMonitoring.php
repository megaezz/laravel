<?php

namespace App\Console\Commands\Engine;

use App\Services\Command;
use Illuminate\Support\Str;

class DomainMonitoring extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'engine:domain-monitoring {domain? : Домен} {--failed : Только с ошибками} {--actual : Только актуальные}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Мониторинг всех активных доменов и зеркал';

    /**
     * Execute the console command.
     */
    public function handler()
    {

        $results = (new \App\Services\Engine\DomainMonitoring)(
            failed: $this->option('failed'),
            domain: $this->argument('domain'),
            onlyActualMirrors: $this->option('actual'),
        );

        $i = 1;

        foreach ($results as $result) {

            if ($result->domain->countryMonitoring($result->country->code)) {
                $this->info("[{$result->country->code}] {$result->domain->domain}: {$result->response->getBody()}");
            } else {
                if (isset($result->response)) {
                    $content = Str::limit(preg_replace('/\s+/', ' ', $result->response->getBody()), 150);
                    $this->warn("[{$result->country->code}] {$result->domain->domain}: {$content}");
                } elseif (isset($result->reason)) {
                    $this->error("[{$result->country->code}] {$result->domain->domain}: {$result->reason->getMessage()}");
                } else {
                    $this->error("[{$result->country->code}] {$result->domain->domain}: not handled");
                }
            }

            $i++;
        }

    }
}
