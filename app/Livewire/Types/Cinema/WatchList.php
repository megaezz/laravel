<?php

namespace App\Livewire\Types\Cinema;

use App\Models\Domain;
use Illuminate\Support\Facades\Session;
use Livewire\Attributes\Computed;
use Livewire\Component;

class WatchList extends Component
{
    public Domain $domain;

    public int $limit = 30;

    public string $template;

    public function render()
    {
        return view($this->template);
    }

    #[Computed]
    public function movies()
    {

        $watchList = Session::get('watchList', []);

        return $this->domain
            ->cinemaDomain
            ->movies()
            ->whereIn('id', $watchList)
            ->with('movie')
            ->with('movie.tags')
            ->limit($this->limit)
            ->get();
    }
}
