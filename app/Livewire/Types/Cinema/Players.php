<?php

namespace App\Livewire\Types\Cinema;

use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\DomainMovie;
use engine\app\models\Engine;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class Players extends Component
{
    public array|Collection $players;

    /* почему передаю ID, а не модель: если передавать модель, то в режиме lazy модель загружается вместе с отношениями, видимо загружаются те отношения, с которыми модель была передана в компонент */
    public int $domainMovieId;

    /* но поддерживается и передача модели */
    public DomainMovie $domainMovie;

    public string $template;

    public ?string $country;

    public bool $isRealBot = false;

    public ?object $activePlayer;

    public Collection $availablePlayers;

    public function render()
    {
        return view($this->template);
    }

    public function players()
    {
        return [
            [
                'player' => 'videodb',
                'name' => 'Плеер 1',
                'component' => 'livewire',
            ],
            [
                'player' => 'allohatv',
                'name' => 'Плеер 2',
                'component' => 'component',
            ],
            [
                'player' => 'hdvb',
                'name' => 'Плеер 3',
                'component' => 'component',
            ],
            [
                'player' => 'trailer',
                'name' => 'Трейлер',
                'component' => 'component',
            ],
        ];
    }

    public function mount()
    {
        $this->players = collect($this->players())->map(function ($item) {
            return (object) $item;
        });
        if (! isset($this->domainMovie)) {
            $this->domainMovie = DomainMovie::findOrFail($this->domainMovieId);
        }
        /* если в компонент не передана страна, то используем из Engine */
        $this->country = $this->country ?? Engine::getClientCountry();
        /* является ли клиент реальным ботом */
        $this->isRealBot = Engine::isRealBot();
        /* один раз считаем доступные плееры, чтобы не было пересчета при смене вкладок */
        $this->availablePlayers = $this->availablePlayers();
        /* если для фильма указан mainPlayer - используем его, если нет, то main_player сайта, если нет, то main_player общий */
        $mainPlayer = $this->domainMovie->movie->mainPlayer ?? ($this->domainMovie->cinemaDomain->main_player ?? Config::cached()->main_player);
        $this->setActivePlayer($mainPlayer);
    }

    public function setActivePlayer($player)
    {
        $this->activePlayer = $this->availablePlayers->where('player', $player)->first() ?? ($this->availablePlayers->first() ?? null);
    }

    /* какие плееры доступны для данного материала, гео и настроек домена? */
    public function availablePlayers()
    {

        $availablePlayers = clone $this->players;

        foreach ($availablePlayers as $index => $player) {
            $existMethod = 'is'.ucfirst($player->player).'Available';
            if (! method_exists($this, $existMethod)) {
                throw new \Exception("Отсутствует метод {$existMethod}");
            }
            if (! $this->{$existMethod}()) {
                $availablePlayers->forget($index);
            }
        }

        /* если есть настройка показывать трейлер для не снг, то проверяем страну и на реального бота и меняем main_player на trailer и оставляем в массиве плееров только trailer, если нужно */
        if ($this->domainMovie->cinemaDomain->show_trailer_for_non_sng and ($this->isRealBot or ! in_array($this->country, ['RU', 'UA', 'KZ', 'BY']))) {
            return $availablePlayers->where('player', 'trailer');
        }

        /* если ничего так и не было выведено, то выводим последнее значение плеера */
        return $availablePlayers;
    }

    public function isTrailerAvailable()
    {
        return $this->domainMovie->movie->kinopoisk?->mainTrailer ? true : false;
    }

    public function isAllohatvAvailable()
    {
        return $this->domainMovie->movie->allohas()->first() ? true : false;
    }

    public function isVideodbAvailable()
    {
        /* всегда true, чтобы отображался фейковый плеер */
        return true;
        /* return $this->domainMovie->movie->videodbs()->first() ? true : false; */
    }

    public function isHdvbAvailable()
    {
        $country = $this->country;
        if ($country and ! in_array($country, ['RU', 'UA', 'KZ', 'BY'])) {
            $country = 'EU';
        }

        return $this->domainMovie->movie->hdvbs()->first()?->getPlayer($country) ? true : false;
    }

    public function isInWatchList()
    {
        return in_array($this->domainMovie->id, $this->getWatchList());
    }

    public function getWatchList()
    {
        return Session::get('watchList', []);
    }

    public function addToWatchList()
    {
        Session::push('watchList', $this->domainMovie->id);
    }

    public function removeFromWatchList()
    {
        Session::put('watchList', array_diff($this->getWatchList(), [$this->domainMovie->id]));
    }

    public function toggleWatchList()
    {
        $this->isInWatchList() ? $this->removeFromWatchList() : $this->addToWatchList();
    }
}
