<?php

namespace App\Livewire\Types\Cinema\Templates\Kinogo;

use Livewire\Attributes\Computed;
use Livewire\Component;
use Livewire\WithoutUrlPagination;
use Livewire\WithPagination;

class MovieList extends Component
{
    use WithoutUrlPagination, WithPagination;

    public $domain;

    public $order = 'mr';

    public $orders = [
        'mr' => 'по дате',
        'bt' => 'по алфавиту',
        'tr' => 'по рейтингу',
        'mv' => 'по просмотрам',
        'mc' => 'по комментариям',
        'by' => 'по году',
    ];

    public $genres;

    public $genre_id;

    public $years;

    public $year;

    public $countries;

    public $country_id;

    public $group;

    #[Computed]
    public function movies()
    {

        $movies = $this->domain->cinemaDomain->movies();

        $movies = $this->setOrder($movies);
        $movies = $this->setGenre($movies);
        $movies = $this->setCountry($movies);
        $movies = $this->setYear($movies);
        $movies = $this->setGroup($movies);

        $movies = $movies
            ->with('movie', 'movie.tags', 'movie.groups')
            ->paginate(10);

        return $movies;
    }

    #[Computed]
    public function genre()
    {
        return $this->genre_id ? $this->domain->cinemaDomain->mergedGroups()->where('type', 'genre')->findOrFail($this->genre_id) : null;
    }

    #[Computed]
    public function country()
    {
        return $this->country_id ? $this->domain->cinemaDomain->mergedGroups()->where('type', 'country')->findOrFail($this->country_id) : null;
    }

    public function mount()
    {

        /* если в компонент передана какая-то группа, то выясняем, что за тип этой группы, чтобы проставить нужный фильтр, если это возможно */
        /* TODO если группа не входит в этот список, то продумать как показывать нужные фильмы */
        if ($this->group) {
            if ($this->group->type == 'genre') {
                $this->genre_id = $this->group->id;
                $this->group = null;
            } elseif ($this->group->type == 'country') {
                $this->country_id = $this->group->id;
                $this->group = null;
            }
        }

        $this->genres = $this->domain->cinemaDomain->mergedGroups()
            ->where('type', 'genre')
            ->orderBy('name')
            ->cacheFor(now()->addDays(1))
            ->get();
        $this->countries = $this->domain->cinemaDomain->mergedGroups()
            ->where('type', 'country')
            ->orderBy('name')
            ->cacheFor(now()->addDays(1))
            ->get();
        $this->years = $this->domain->cinemaDomain->movies()
            ->join('cinema.cinema', 'cinema.id', 'domain_movies.movie_id')
            ->selectRaw('distinct cinema.year')
            ->orderByDesc('cinema.year')
            ->cacheFor(now()->addDays(1))
            ->get()
            ->pluck('year');
    }

    public function updated($propertyName)
    {

        if ($propertyName == 'order') {
            if (! isset($this->orders[$this->order])) {
                throw new \Exception('Переданная сортировка не существует');
            }
        }

    }

    public function setOrder($movies)
    {

        /* most recent */
        if ($this->order == 'mr') {
            $movies = $movies->orderByDesc('add_date');
        }

        if (in_array($this->order, ['bt', 'tr', 'mv', 'by'])) {
            $movies = $movies->join('cinema.cinema', 'cinema.id', 'domain_movies.movie_id');
        }

        /* by title */
        if ($this->order == 'bt') {
            $movies = $movies
                ->orderBy('cinema.title_ru');
        }

        /* top rated */
        if ($this->order == 'tr') {
            $movies = $movies
                ->orderBy('cinema.rating_sko');
        }

        /* most viewed */
        if ($this->order == 'mv') {
            $movies = $movies
                ->orderBy('cinema.imdb_votes');
        }

        /* by year */
        if ($this->order == 'by') {
            $movies = $movies
                ->orderBy('cinema.year');
        }

        /* most commented */
        if ($this->order == 'mc') {
            $movies = $movies
                ->withCount('comments')
                ->orderBy('comments_count');
        }

        return $movies;
    }

    public function setGenre($movies)
    {
        return $this->genre ? $movies->whereRelation('movie.groups', 'id', '=', $this->genre->id) : $movies;
    }

    public function setCountry($movies)
    {
        return $this->country ? $movies->whereRelation('movie.groups', 'id', '=', $this->country->id) : $movies;
    }

    public function setYear($movies)
    {
        return $this->year ? $movies->whereRelation('movie', 'year', '=', $this->year) : $movies;
    }

    public function setGroup($movies)
    {
        return $this->group ? $movies->whereRelation('movie.groups', 'id', '=', $this->group->id) : $movies;
    }

    public function resetFilters()
    {
        $this->reset('order');
        $this->reset('year');
        $this->reset('genre_id');
        $this->reset('country_id');
    }

    public function render()
    {
        return view('livewire.types.cinema.templates.kinogo.movie-list');
    }
}
