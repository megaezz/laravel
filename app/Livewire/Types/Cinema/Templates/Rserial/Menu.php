<?php

namespace App\Livewire\Types\Cinema\Templates\Rserial;

use App\Models\Config;
use App\Models\Domain;
use cinema\app\models\eloquent\Group;
use cinema\app\models\eloquent\Tag;
use Livewire\Attributes\Computed;
use Livewire\Component;

class Menu extends Component
{
    public ?string $activeSection;

    public Domain $domain;

    public function render()
    {
        return view('livewire.types.cinema.templates.rserial.menu');
    }

    #[Computed]
    public function sections()
    {
        $sections = [
            [
                'name' => 'genres',
                'button' => 'Жанры',
            ],
            [
                'name' => 'countries',
                'button' => 'Страны',
            ],
            [
                'name' => 'channels',
                'button' => '<i class="fas fa-tv"></i> ТВ каналы',
            ],
            [
                'name' => 'compilations',
                'button' => '<i class="fas fa-list"></i> Подборки',
            ],
        ];

        return $sections;
    }

    public function tagsHavingMovies(string $type)
    {
        return Tag::query()
            ->where('type', $type)
            ->whereActive(true)
            ->whereHas('movies', function ($query) {
                $query
                    ->whereActive(true)
                    ->whereDeleted(false)
                    ->whereHas('domainMovies', function ($query) {
                        $query->where('domain', $this->domain->domain);
                    });
            })
            ->orderBy('tag')
            ->cacheFor(Config::cached()->cacheSeconds)
            ->get();
    }

    public function groupsHavingMovies(string $type)
    {
        return Group::query()
            ->where('type', $type)
            ->whereActive(true)
            ->whereHas('movies', function ($query) {
                $query
                    ->whereActive(true)
                    ->whereDeleted(false)
                    ->whereHas('domainMovies', function ($query) {
                        $query->whereDomain($this->domain->domain);
                    });
            })
            ->cacheFor(Config::cached()->cacheSeconds)
            ->get();
    }

    #[Computed]
    public function compilations()
    {
        return $this->groupsHavingMovies('compilation');
    }

    #[Computed]
    public function genres()
    {
        return $this->tagsHavingMovies('genre');
    }

    #[Computed]
    public function channels()
    {
        return $this->tagsHavingMovies('channel');
    }
}
