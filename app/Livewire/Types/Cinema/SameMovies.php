<?php

namespace App\Livewire\Types\Cinema;

use App\Models\Domain;
use cinema\app\models\eloquent\DomainMovie;
use Livewire\Attributes\Computed;
use Livewire\Component;

class SameMovies extends Component
{
    public string $domainName;

    public int $domainMovieId;

    public string $template;

    public DomainMovie $domainMovie;

    public Domain $domain;

    /* лимит */
    public int $limit;

    /* текущий лимит */
    public int $currentLimit;

    /* максимальное количество похожих видео */
    public int $maxLimit = 100;

    public function mount()
    {
        $this->currentLimit = $this->limit;
        /* загружаю домен вместо того, чтобы пробрасывать, т.к. livewire загружает все связи которые были в изначальной модели, куча запросов */
        $this->domain = Domain::findOrFail($this->domainName);
        $this->domainMovie = $this->domain->cinemaDomain->movies()->findOrFail($this->domainMovieId);
    }

    #[Computed]
    public function movies()
    {
        return $this->domainMovie
            ->relatedMovies()
            ->activeRelatedMovies()
            ->with('movie')
            ->with('movie.tags')
            ->limit($this->currentLimit)
            ->get();
    }

    public function render()
    {
        return view($this->template);
    }

    public function showMore()
    {
        $this->currentLimit = $this->currentLimit + $this->limit;
    }

    #[Computed]
    public function showMoreButton()
    {
        return $this->currentLimit <= $this->maxLimit;
    }
}
