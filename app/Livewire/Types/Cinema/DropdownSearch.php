<?php

namespace App\Livewire\Types\Cinema;

use App\Models\Domain;
use cinema\app\models\eloquent\DomainMovie;
use engine\app\models\F;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class DropdownSearch extends Component
{
    public string $query;

    public ?Collection $movies;

    public Domain $domain;

    public $focus = false;

    public $filter_opened = false;

    public $filter_name = 'Все';

    public $template;

    public function mount($template)
    {
        $this->template = $template;
    }

    public function render()
    {
        return view($this->template);
    }

    public function updatedQuery()
    {
        /* удаляем спец символы, иначе ошибка, баг в mysql с boolean mode */
        /* удаляет все кроме: любые буквы, любые цифры, пробелы */
        $cleanedQuery = preg_replace('/[^\p{L}\p{N}\s]/u', '', trim($this->query));

        if (! $cleanedQuery or strlen($cleanedQuery) < 3) {
            /* return нужен, чтобы выполнение завершилось */
            return $this->movies = null;
        }

        $variants = [$cleanedQuery, F::fixLayoutToRus($cleanedQuery)];

        foreach ($variants as $searchQuery) {

            /* здесь запрашиваем только ID, чтобы не перегружать кеш */
            $this->movies = $this->domain->cinemaDomain
                ->movies()
                ->select('id')
                ->available()
                ->whereHas('movie', function ($query) use ($searchQuery) {
                    // $query->whereRaw("MATCH(title_ru, title_en) AGAINST(? IN BOOLEAN MODE)", [$searchQuery . '*']);
                    $query->whereRaw('MATCH(title_ru) AGAINST(?)', [$searchQuery]);
                })
                ->limit(10)
                ->cacheFor(now()->addDay())
                ->get();

            /* заполняем модель */
            $this->movies = DomainMovie::with('movie')->findMany($this->movies->pluck('id'));

            if ($this->movies->count()) {
                break;
            }
        }
    }

    public function onFocus()
    {
        $this->focus = true;
    }

    public function onBlur()
    {
        $this->focus = false;
        $this->filter_opened = false;
    }

    public function toggleFilterOpened()
    {
        $this->filter_opened = ! $this->filter_opened;
        if ($this->filter_opened) {
            $this->focus = true;
        }
    }

    public function setFilter($name)
    {
        $this->filter_name = $name;
        $this->filter_opened = false;
    }

    public function submit()
    {
        /* тут мы должны отправлять на роут search */
    }
}
