<?php

namespace App\Livewire\Types\Cinema\Players;

use App\Models\Cinema\MovieUpload;
use App\Models\Config as EngineConfig;
use App\Services\Cinema\PlaylistForPlayerjs;
use cinema\app\models\eloquent\Config as CinemaConfig;
use cinema\app\models\eloquent\DomainMovie;
use cinema\app\models\eloquent\Videodb as VideodbModel;
use cinema\app\models\Videodb as ModelsVideodb;
use engine\app\models\Engine;
use engine\app\models\F;
use Illuminate\Support\Facades\Storage;
use Livewire\Attributes\Computed;
use Livewire\Component;

class Videodb extends Component
{
    public $playerjsVersion;

    public $reload;

    public ?string $translation;

    public ?VideodbModel $videodb;

    public DomainMovie $domainMovie;

    public function mount()
    {
        $this->playerjsVersion = CinemaConfig::cached()->playerjsVersion;
        $this->reload = EngineConfig::cached()->reload;

        $ban = (Engine::getClientBan() and Engine::getClientBan()->hide_player) ? true : false;

        if ($ban) {
            F::error('Offline', 'Player banned for client', false);
        }

        $this->videodb = empty($_GET['videodbId']) ? null : VideodbModel::findOrFail($_GET['videodbId']);
        $this->translation = empty($_GET['translation']) ? null : F::checkstr($_GET['translation']);

        if (! $this->videodb) {
            $this->videodb = $this->domainMovie
                ->videodbs()
                ->orderByRaw('cast(season as unsigned) desc')
                ->orderByRaw('cast(episode as unsigned) desc')
                ->orderByDesc('max_quality')
                ->first();
        }
    }

    public function videodbOrUploadExists()
    {
        return $this->videodb or $this->domainMovie?->movie?->uploads()->where('hls_exists', true)->count();
    }

    public function render()
    {
        return view('livewire.types.cinema.players.videodb');
    }

    #[Computed]
    public function playerJsData()
    {
        // return base64_encode(json_encode(
        //     $this->videodb ?
        //     $this->videodb->playerJsData : (
        //         $this->domainMovie?->movie?->uploads()->where('hls_exists', true)->count() ?
        //         $this->uploadsJsData :
        //         $this->fakePlayerJsData
        //     )
        // ));

        $playlist = (new PlaylistForPlayerjs)($this->domainMovie);

        return base64_encode(json_encode($playlist ? $playlist : $this->fakePlayerJsData));
    }

    #[Computed]
    public function uploadsJsData()
    {
        // TODO: как здесь обработать ситуацию, если есть 2 варианта эпизода от одной и той же студии? никак, вместо того чтобы колхозить, нужно переписать плейлист на работу от MovieEpisodeData
        $groupedEpisodes = $this->domainMovie
            ->movie
            ->uploads()
            ->with('dubbing')
            ->with('episode')
            ->where('hls_exists', true)
            ->get()
            ->groupBy(['episode.season', 'episode.episode'])
            ->sortKeys();

        // $groupedEpisodes = MovieUpload::query()
        // ->with('dubbing')
        // ->with('episode')
        // ->get()
        // ->groupBy(['episode.season', 'episode.episode'])
        // ->sortKeys();

        // dd($groupedEpisodes);

        $playlist = $groupedEpisodes->map(function ($seasonEpisodes, $season) {
            return [
                'title' => "Сезон $season",
                'folder' => $seasonEpisodes->map(function ($episodeTranslations, $episode) {
                    return [
                        'title' => "Серия {$episode}",
                        'file' => ModelsVideodb::pjsBase64Encrypt($episodeTranslations->map(fn ($upload) => "{{$upload->dubbing->short_or_full_name}}".Storage::disk('storage')->url($upload->hls).';')->implode('')),
                    ];
                })->values(),
            ];
        })->values();

        // dd($playlist);

        $data = new \stdClass;
        $data->movieTitle = $this->domainMovie->movie->title;
        $data->movieYear = $this->domainMovie->movie->year;
        $data->movieType = $this->domainMovie->movie->localed_type;
        $data->playlist = $playlist;

        return $data;
    }

    #[Computed]
    public function fakePlayerJsData()
    {
        $data = new \stdClass;
        $data->movieTitle = $this->domainMovie->movie->title;
        $data->movieYear = $this->domainMovie->movie->year;
        $data->movieType = "- {$this->domainMovie->movie->localed_type}";
        //
        $seasonObj = new \stdClass;
        $seasonObj->title = 'Сезон 1';
        $episodeObj = new \stdClass;
        $episodeObj->title = 'Серия 1';
        // текущий сезон и эпизод, чтобы получать в плеере и формировать название
        $episodeObj->id = new \stdClass;
        $episodeObj->id->season = 1;
        $episodeObj->id->episode = 1;
        $episodeObj->file = '{Оригинал}/types/cinema/template/players/videodb/video_unavailable.mp4;';
        $episodesArr = [$episodeObj];
        $seasonObj->folder = $episodesArr;
        $seasonsArr = [$seasonObj];
        //
        $data->playlist = $seasonsArr;

        return $data;
    }
}
