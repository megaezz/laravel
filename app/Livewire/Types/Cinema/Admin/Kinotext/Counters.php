<?php

namespace App\Livewire\Types\Cinema\Admin\Kinotext;

use Livewire\Component;

class Counters extends Component
{
    public User $user;

    public function render()
    {
        return view('livewire.types.cinema.admin.kinotext.counters');
    }

    public function availableTasksCount()
    {
        return $this->worker
            ->availableTasks()
            ->count();

        return $movies->getRows();
    }

    public function getModerationRows()
    {
        return $this->moderator
            ->toBeModeratedTasks()
            ->count();
        // считаем сколько заданий на модерации
        $movies = new Movies;
        $movies->setDomainMovieShowToCustomer(false);
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieRework(false);

        return $movies->getRows();
    }
}
