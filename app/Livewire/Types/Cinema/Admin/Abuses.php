<?php

namespace App\Livewire\Types\Cinema\Admin;

use App\Models\Abuse;
use Livewire\Component;
use Livewire\WithPagination;

class Abuses extends Component
{
    use WithPagination;

    public $service;

    public $handled;

    public $handler_error;

    public $challenged_on_google;

    public $reinstated_on_google;

    public $canceled_on_google;

    public $toBeCounterComplained;

    public $urlSearch;

    public $selectedLinks = [];

    public $selectAllLinks;

    public $actionWithSelectedLinks;

    public $limit;

    public bool $showMassUrlSearch = false;

    public ?string $massUrlSearch = null;

    /* нужен чтобы в шаблоне показывать количество строк */
    public array $massUrlSearchRows = [];

    public function mount()
    {
        $this->limit = session()->get('cinema.abuses.limit', 15);
        $this->service = session()->get('cinema.abuses.service', null);
        $this->handled = session()->get('cinema.abuses.handled', null);
        $this->handler_error = session()->get('cinema.abuses.handler_error', null);
    }

    public function render()
    {
        return view('livewire.types.cinema.admin.abuses');
    }

    public function getServicesProperty()
    {
        /* используем сортировку массива sortBy, т.к. orderBy выводит enum поля в порядке их описания в таблице */
        return Abuse::select('service')->distinct()->get()->sortBy('service')->pluck('service');
    }

    public function getLinksProperty()
    {
        $links = Abuse::orderByDesc('created_at');

        if ($this->service) {
            $links->whereService($this->service);
        }

        if ($this->handled) {
            $links->whereHandled(filter_var($this->handled, FILTER_VALIDATE_BOOLEAN));
        }

        if ($this->handler_error) {
            $links->whereHandlerError(filter_var($this->handler_error, FILTER_VALIDATE_BOOLEAN));
        }

        if ($this->challenged_on_google) {
            if (filter_var($this->challenged_on_google, FILTER_VALIDATE_BOOLEAN)) {
                $links->whereNotNull('challenged_on_google_at');
            } else {
                $links->whereNull('challenged_on_google_at');
            }
        }

        if ($this->reinstated_on_google) {
            if (filter_var($this->reinstated_on_google, FILTER_VALIDATE_BOOLEAN)) {
                $links->whereNotNull('reinstated_on_google_at');
            } else {
                $links->whereNull('reinstated_on_google_at');
            }
        }

        if ($this->canceled_on_google) {
            if (filter_var($this->canceled_on_google, FILTER_VALIDATE_BOOLEAN)) {
                $links->whereNotNull('canceled_on_google_at');
            } else {
                $links->whereNull('canceled_on_google_at');
            }
        }

        if ($this->toBeCounterComplained) {
            /* домен или главный домен зеркала имеет настройку показывать трейлеры для не снг */
            $links->where(function ($query) {
                $query
                // ->whereHas('domain', function($query){
                //     $query
                //     ->where('domain', '')
                // })
                    ->whereRelation('domain.cinemaDomain', 'show_trailer_for_non_sng', filter_var($this->toBeCounterComplained, FILTER_VALIDATE_BOOLEAN))
                    ->orWhereRelation('domain.mirrorOf.cinemaDomain', 'show_trailer_for_non_sng', filter_var($this->toBeCounterComplained, FILTER_VALIDATE_BOOLEAN));
            });
        }

        if ($this->massUrlSearchRows) {

            $links->where(function ($query) {
                foreach ($this->massUrlSearchRows as $index => $url) {

                    $url = Abuse::formatUrl(trim($url));

                    if ($index == 0) {
                        $query->where('query', 'like', '%'.str_replace(['%', '_'], ['\%', '\_'], $url).'%');
                    } else {
                        $query->orWhere('query', 'like', '%'.str_replace(['%', '_'], ['\%', '\_'], $url).'%');
                    }
                }
            });
        }

        if ($this->urlSearch) {
            $links->where('query', 'like', '%'.str_replace(['%', '_'], ['\%', '\_'], $this->urlSearch).'%');
        }

        /* курсор пагинацию использовать не получилось, т.к. не срабатывает resetPage */
        return $links->simplePaginate($this->limit);
    }

    /* при изменении страницы пагинации обнуляем чекбоксы, это встроенный метод */
    public function updatedPage($page)
    {
        $this->selectedLinks = [];
        $this->selectAllLinks = false;
    }

    /* при изменении свойства selectAllLinks - отмечаем или снимаем чекбоксы */
    public function updatedSelectAllLinks()
    {
        if ($this->selectAllLinks) {
            foreach ($this->links as $link) {
                $this->selectedLinks[] = $link->id;
            }
        } else {
            $this->selectedLinks = [];
        }
    }

    public function submitActionWithSelectedLinks()
    {

        if ($this->actionWithSelectedLinks == 'setHandledErrorFalse') {
            foreach ($this->selectedLinks as $linkId) {
                $link = Abuse::find($linkId);
                $link->handler_error = false;
                $link->save();
            }
            session()->flash('status', 'Отметили '.count($this->selectedLinks).' ссылок');
            $this->resetPage();
        }

        if ($this->actionWithSelectedLinks == 'formGoogleCounterComplaint') {

            $links = [];

            foreach ($this->selectedLinks as $linkId) {
                $link = Abuse::find($linkId);
                $link->challenged_on_google_at = now();
                $link->save();
                $links[] = $link;
            }

            session()->flash('status', view('types.cinema.admin.google-counter-complaint', ['links' => $links]));
            $this->resetPage();
        }

        if ($this->actionWithSelectedLinks == 'setAsReinstatedOnGoogle') {

            foreach ($this->selectedLinks as $linkId) {
                $link = Abuse::find($linkId);
                $link->reinstated_on_google_at = now();
                $link->save();
            }

            session()->flash('status', 'Отметили отмеченные ссылки восстановлеными в Гугле');
            $this->resetPage();
        }

        if ($this->actionWithSelectedLinks == 'setAsCanceledOnGoogle') {

            foreach ($this->selectedLinks as $linkId) {
                $link = Abuse::find($linkId);
                $link->canceled_on_google_at = now();
                $link->save();
            }

            session()->flash('status', 'Отметили отмеченные ссылки отмененными в Гугле');
            $this->resetPage();
        }

        $this->selectedLinks = [];

    }

    /* при выборе фильтров обнуляем страницу, снимаем чекбоксы */
    public function updatedService()
    {
        session()->put('cinema.abuses.service', $this->service);
        $this->resetPage();
    }

    public function updatedHandled()
    {
        session()->put('cinema.abuses.handled', $this->handled);
        $this->resetPage();
    }

    public function updatedHandlerError()
    {
        session()->put('cinema.abuses.handler_error', $this->handler_error);
        $this->resetPage();
    }

    public function updatedLimit()
    {
        $this->resetPage();
        session()->put('cinema.abuses.limit', $this->limit);
    }

    public function updatedMassUrlSearch()
    {
        $this->massUrlSearchRows = explode(PHP_EOL, trim($this->massUrlSearch));
        /* убираем сразу пустые строки иначе из-за этого потом ебанина */
        foreach ($this->massUrlSearchRows as $index => $row) {
            if (empty(trim($row))) {
                unset($this->massUrlSearchRows[$index]);

                continue;
            }
        }
    }
}
