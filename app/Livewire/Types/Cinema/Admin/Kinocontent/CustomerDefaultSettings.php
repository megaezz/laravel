<?php

namespace App\Livewire\Types\Cinema\Admin\Kinocontent;

use cinema\app\models\eloquent\User;
use Livewire\Attributes\Validate;
use Livewire\Component;

class CustomerDefaultSettings extends Component
{
    public User $user;

    public bool $opened = false;

    #[Validate('nullable|int|lt:symbolsTo')]
    public $symbolsFrom;

    #[Validate('nullable|int|gt:symbolsFrom')]
    public $symbolsTo;

    #[Validate('nullable|string')]
    public $customerComment;

    #[Validate('nullable|string')]
    public $customerPrivateComment;

    #[Validate('nullable|boolean')]
    public $checkIsMovieInDoneTasks;

    #[Validate('nullable|boolean')]
    public $customerAutoExpress;

    public function render()
    {
        return view('livewire.types.cinema.admin.kinocontent.customer-default-settings');
    }

    public function mount()
    {
        $this->symbolsFrom = $this->user->symbols_from;
        $this->symbolsTo = $this->user->symbols_to;
        $this->customerComment = $this->user->customer_comment;
        $this->customerPrivateComment = $this->user->customer_private_comment;
        $this->checkIsMovieInDoneTasks = $this->user->checkIsMovieInDoneTasks;
        $this->customerAutoExpress = $this->user->customerAutoExpress;
    }

    public function save()
    {

        /* оставляем дропдаун открытым */
        $this->opened = true;

        $this->validate();

        $this->user->symbols_from = $this->symbolsFrom ?: null;
        $this->user->symbols_to = $this->symbolsTo ?: null;
        $this->user->customer_comment = $this->customerComment;
        $this->user->customer_private_comment = $this->customerPrivateComment;
        $this->user->checkIsMovieInDoneTasks = $this->checkIsMovieInDoneTasks;
        $this->user->customerAutoExpress = $this->customerAutoExpress;

        $this->user->save();
    }
}
