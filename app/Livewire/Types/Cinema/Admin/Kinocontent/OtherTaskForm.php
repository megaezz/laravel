<?php

namespace App\Livewire\Types\Cinema\Admin\Kinocontent;

use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\User;
use Livewire\Attributes\Validate;
use Livewire\Component;

class OtherTaskForm extends Component
{
    public User $user;

    public Movie $movie;

    #[Validate('nullable|int|lt:symbolsTo')]
    public $symbolsFrom;

    #[Validate('nullable|int|gt:symbolsFrom')]
    public $symbolsTo;

    #[Validate('required|string')]
    public $comment;

    #[Validate('nullable|string')]
    public $privateComment;

    #[Validate('boolean')]
    public $isAdult = false;

    public function render()
    {
        return view('livewire.types.cinema.admin.kinocontent.other-task-form');
    }

    public function mount()
    {
        $this->symbolsFrom = $this->user->symbols_from;
        $this->symbolsTo = $this->user->symbols_to;
        $this->comment = $this->user->customer_comment;
        $this->privateComment = $this->user->customer_private_comment;
        $this->movie = Movie::where('mw_unique_id', 'not_in_base')->firstOrFail();
    }

    public function add()
    {

        $this->validate();

        $task = $this->user->addTask($this->movie);
        $task->symbols_from = $this->symbolsFrom ?: null;
        $task->symbols_to = $this->symbolsTo ?: null;
        $task->customer_comment = $this->comment;
        $task->private_comment = $this->privateComment;
        $task->adult = $this->isAdult;
        $task->save();

        /* сбросить до дефолтного */
        $this->mount();

        $this->dispatch('refresh')->to(TaskQueue::class);

    }
}
