<?php

namespace App\Livewire\Types\Cinema\Admin\Kinocontent;

use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\User;
use Livewire\Attributes\Computed;
use Livewire\Component;

class MovieTaskForm extends Component
{
    public User $user;

    #[Validate('nullable|string')]
    public $searchQuery;

    public bool $found = false;

    public function render()
    {
        return view('livewire.types.cinema.admin.kinocontent.movie-task-form');
    }

    #[Computed]
    public function filteredMovies()
    {

        if (\Str::length($this->searchQuery) < 3) {
            return collect();
        }

        preg_match('/kinopoisk\.ru\/(film|series)\/(\d+)/', $this->searchQuery, $found);

        if ($found) {
            $this->searchQuery = $found[2];
        }

        return Movie::query()
            ->whereIn('type', ['movie', 'serial', 'trailer'])
            ->where(function ($query) {
                $query
                    ->whereLike('title_ru', "%{$this->searchQuery}%")
                    ->orWhereLike('title_en', "%{$this->searchQuery}%")
                    ->orWhereLike('kinopoisk_id', "%{$this->searchQuery}%");
            })
            ->limit(10)
            ->orderByDesc('kinopoisk_rating')
            ->get();
    }
}
