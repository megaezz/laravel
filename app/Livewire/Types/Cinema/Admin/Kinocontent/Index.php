<?php

namespace App\Livewire\Types\Cinema\Admin\Kinocontent;

use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\User;
use Livewire\Component;

class Index extends Component
{
    public User $user;

    public Config $cinemaConfig;

    public string $taskType;

    protected $listeners = [
        'error' => 'error',
        'alert' => 'alert',
    ];

    public function mount()
    {
        $this->taskType = \Session::get('taskType', 'movie');
        $this->cinemaConfig = Config::cached();
    }

    public function updated($property, $value)
    {
        if ($property == 'taskType') {
            \Session::put('taskType', $value);
        }
    }

    public function render()
    {
        return view('livewire.types.cinema.admin.kinocontent.index');
    }

    public function error($message)
    {
        session()->flash('error', $message);
    }

    public function alert($message)
    {
        session()->flash('message', $message);
    }
}
