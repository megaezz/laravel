<?php

namespace App\Livewire\Types\Cinema\Admin\Kinocontent;

use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\User;
use Livewire\Attributes\Computed;
use Livewire\Component;

class TaskQueue extends Component
{
    public User $user;

    public Config $cinemaConfig;

    protected $listeners = [
        'refresh' => '$refresh',
    ];

    public function render()
    {
        return view('livewire.types.cinema.admin.kinocontent.task-queue');
    }

    #[Computed]
    public function tasks()
    {
        return $this->user
            ->createdTasks()
            ->with('movie')
            ->notDone()
            ->orderByDesc('add_date')
            ->limit(1000)
            ->get();
    }

    public function clearAll()
    {
        $count = $this->user
            ->createdTasks()
            ->notConfirmed()
            ->notDone()
            ->delete();

        $this->dispatch('alert', "Удалено заданий:  {$count}")->to(Index::class);
    }

    public function confirmAll()
    {

        $notConfirmedTasks = $this->tasks->where('is_confirmed', false);

        foreach ($notConfirmedTasks as $task) {
            $task->setConfirm(true);
            $task->save();
        }

        /* TODO: почему то не обновлялись дочерние компоненты, пришлось сделать через dispatch, не разобрался как отправить обновление к определенному ID компонента, поэтому обновляем все сразу */
        $this->dispatch('refresh')->to(TaskInQueue::class);

        $this->dispatch('alert', "Подтверждено заданий:  {$notConfirmedTasks->count()}")->to(Index::class);
    }
}
