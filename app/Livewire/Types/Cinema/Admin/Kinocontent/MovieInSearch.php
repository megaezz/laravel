<?php

namespace App\Livewire\Types\Cinema\Admin\Kinocontent;

use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\User;
use Livewire\Component;

class MovieInSearch extends Component
{
    public User $user;

    public Movie $movie;

    public bool $added = false;

    public bool $alreadyInQueueAlerted = false;

    public bool $alreadyDoneAlerted = false;

    public function render()
    {
        return view('livewire.types.cinema.admin.kinocontent.movie-in-search');
    }

    public function add()
    {

        /* если еще не уведомляли о том что в очереди есть такое задание, то проверяем */
        if (! $this->alreadyInQueueAlerted) {
            if ($this->user->createdTasks()->notDone()->where('movie_id', $this->movie->id)->exists()) {
                $this->alreadyInQueueAlerted = true;

                return $this->js("alert('В очереди уже есть такое задание. Нажмите еще раз, чтобы все равно добавить.')");
            }
        }

        if (! $this->alreadyDoneAlerted) {
            if ($this->user->checkIsMovieInDoneTasks) {
                if ($this->user->createdTasks()->done()->where('movie_id', $this->movie->id)->exists()) {
                    $this->alreadyDoneAlerted = true;

                    return $this->js("alert('В выполненных уже есть такое задание. Нажмите еще раз, чтобы все равно добавить.')");
                }
            }
        }

        try {
            $task = $this->user->addTask($this->movie);
        } catch (\Exception $e) {
            return $this->dispatch('error', $e->getMessage())->to(Index::class);
        }

        $this->dispatch('refresh')->to(TaskQueue::class);

        $this->added = true;
    }
}
