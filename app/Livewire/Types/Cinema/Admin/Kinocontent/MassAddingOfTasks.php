<?php

namespace App\Livewire\Types\Cinema\Admin\Kinocontent;

use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\User;
use Livewire\Attributes\Validate;
use Livewire\Component;

class MassAddingOfTasks extends Component
{
    public User $user;

    #[Validate('string')]
    public $list;

    #[Validate('boolean')]
    public bool $checkDuplicates = false;

    #[Validate('boolean')]
    public bool $checkDone = false;

    #[Validate('boolean')]
    public bool $checkInQueue = false;

    public array $result = [];

    public bool $opened = false;

    public function render()
    {
        return view('livewire.types.cinema.admin.kinocontent.mass-adding-of-tasks');
    }

    public function add()
    {

        $this->opened = true;

        $this->validate();

        $explodedByEnter = explode(PHP_EOL, $this->list);
        $explodedByDots = explode(',', $this->list);

        $strings = (count($explodedByEnter) > count($explodedByDots)) ? $explodedByEnter : $explodedByDots;

        if (count($strings) > 200) {
            return $this->dispatch('error', 'Максимум 200 элементов списка')->to(Index::class);
        }

        $done = [];
        $notFound = [];
        $alreadyDone = [];
        $alreadyInQueue = [];
        $duplicated = [];
        $handledKpIds = [];

        foreach ($strings as $string) {
            $string = trim($string);
            /* если пустая строка - пропускаем */
            if (empty($string)) {
                continue;
            }
            preg_match('/kinopoisk\.ru\/(film|series)\/(\d+)/', $string, $found);
            $kpId = $found ? $found[2] : $string;

            if ($this->checkDuplicates) {
                if (in_array($kpId, $handledKpIds)) {
                    $duplicated[] = $string;

                    continue;
                }
            }

            $handledKpIds[] = $kpId;

            $movie = Movie::where('kinopoisk_id', $kpId)->first();

            if ($movie) {
                if ($this->checkDone) {
                    if ($this->user->createdTasks()->done()->where('movie_id', $movie->id)->exists()) {
                        $alreadyDone[] = $string;

                        continue;
                    }
                }
                if ($this->checkInQueue) {
                    if ($this->user->createdTasks()->notDone()->where('movie_id', $movie->id)->exists()) {
                        $alreadyInQueue[] = $string;

                        continue;
                    }
                }
                $done[] = $string;
                $this->user->addTask($movie);
            } else {
                $notFound[] = $string;
            }
        }

        $this->result = [
            'done' => $done,
            'notFound' => $notFound,
            'alreadyDone' => $alreadyDone,
            'alreadyInQueue' => $alreadyInQueue,
            'duplicated' => $duplicated,
        ];

        $this->dispatch('refresh')->to(TaskQueue::class);
    }
}
