<?php

namespace App\Livewire\Types\Cinema\Admin\Kinocontent;

use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Task;
use cinema\app\models\eloquent\User;
use Livewire\Attributes\Validate;
use Livewire\Component;

class TaskInQueue extends Component
{
    public Task $task;

    public User $user;

    public Config $cinemaConfig;

    public bool $opened = false;

    #[Validate('nullable|string')]
    public $customerComment;

    #[Validate('nullable|string')]
    public $privateComment;

    /* валидация что symboldFrom должен быть меньше чем symbolsTo (lowerThen) */
    #[Validate('nullable|int|lt:symbolsTo')]
    public $symbolsFrom;

    /* валидация что symboldTo должен быть больше чем symbolsFrom (greaterThen) */
    #[Validate('nullable|int|gt:symbolsFrom')]
    public $symbolsTo;

    protected $listeners = [
        'refresh' => '$refresh',
    ];

    public function mount()
    {
        $this->customerComment = $this->task->customer_comment;
        $this->privateComment = $this->task->private_comment;
        $this->symbolsFrom = $this->task->symbols_from;
        $this->symbolsTo = $this->task->symbols_to;
    }

    public function render()
    {
        return view('livewire.types.cinema.admin.kinocontent.task-in-queue');
    }

    public function delete()
    {

        if ($this->task->is_done) {
            return $this->dispatch('error', 'Нельзя удалить, т.к. задание было выполнено')->to(Index::class);
        }

        if ($this->task->is_booked) {
            return $this->dispatch('error', 'Нельзя удалить, задание забронировано копирайтером')->to(Index::class);
        }

        $this->task->delete();

        $this->dispatch('refresh')->to(TaskQueue::class);
    }

    public function duplicate()
    {

        $this->task->duplicate();

        $this->dispatch('refresh')->to(TaskQueue::class);
    }

    public function confirmToggle()
    {

        if ($this->task->is_booked) {
            return $this->dispatch('error', 'Запрещено. Задание уже забронировано.')->to(Index::class);
        }

        $this->task->setConfirm(! $this->task->is_confirmed);
        $this->task->save();
        $this->task->refresh();
    }

    public function expressToggle()
    {

        if ($this->task->is_booked) {
            return $this->dispatch('error', 'Запрещено. Задание уже забронировано.')->to(Index::class);
        }

        $this->task->setExpress(! $this->task->is_express);
        $this->task->save();
        $this->task->refresh();
    }

    public function save()
    {

        $this->opened = true;

        $this->validate();

        $this->task->customer_comment = $this->customerComment;
        $this->task->private_comment = $this->privateComment;
        $this->task->symbols_from = $this->symbolsFrom ?: null;
        $this->task->symbols_to = $this->symbolsTo ?: null;

        $this->task->save();
    }
}
