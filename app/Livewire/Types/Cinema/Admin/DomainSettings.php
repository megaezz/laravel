<?php

namespace App\Livewire\Types\Cinema\Admin;

use App\Models\ParsedUrl;
use App\Models\User;
use App\Services\Cinema\MovieAllowed\IsMovieAllowedForDomain;
use cinema\app\models\eloquent\Domain;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;

class DomainSettings extends Component
{
    public Domain $domain;

    protected $rules = [
        'domain.domain' => '',
        'domain.topMoviesLimit' => '',
        'domain.watchLink' => '',
        'domain.groupLink' => '',
        'domain.genreLink' => '',
        'domain.moviesLimit' => '',
        'domain.moviesPerDay' => '',
        'domain.watchLinkEn' => '',
        'domain.watchMovieLink' => '',
        'domain.watchSerialLink' => '',
        'domain.recaptchaVersion' => '',
        'domain.include_serials' => '',
        'domain.include_movies' => '',
        'domain.include_trailers' => '',
        'domain.include_games' => '',
        'domain.include_youtube' => '',
        'domain.movies_with_unique_description' => '',
        'domain.watchCalcSameMovies' => '',
        'domain.calcThumbGenresList' => '',
        'domain.useAltDescriptions' => '',
        'domain.allowRussianMovies' => '',
        'domain.rknForRuOnly' => '',
        'domain.moviesWithPoster' => '',
        'domain.rknBanned' => '',
        'domain.allowRiskyStudios' => '',
        'domain.legalMovies' => '',
        'domain.moviesMinYear' => '',
        'domain.useOnlyChoosenGroups' => '',
        'domain.useInRedirectToAnyMovie' => '',
        'domain.useMovieId' => '',
        'domain.useTagIdForGenres' => '',
        'domain.dmcaQuestion' => '',
        'domain.sitemapMoviesPaginationLink' => '',
        'domain.episodically' => '',
        'domain.rkn_scheme' => '',
        'domain.hide_canonical' => '',
        'domain.index_route' => '',
        'domain.index_search' => '',
        'domain.main_player' => '',
        'domain.ai_descriptions' => '',
        'domain.index_movies_in_yandex' => '',
        'domain.show_trailer_for_non_sng' => '',
        'domain.allow_movies_only_from' => '',
        'domain.routes_type' => '',
    ];

    public ?string $copy_domain;

    public User $user;

    public array $isMovieAllowedVars = [
        'result' => null,
        'movie_id' => null,
    ];

    public function render()
    {
        return view('livewire.types.cinema.admin.domain-settings');
    }

    public function mount()
    {
        $this->user = auth()->user();
    }

    public function getCopyDomainsProperty()
    {
        return Domain::where('domain', '!=', $this->domain->domain)->get();
    }

    /* срабатывает автоматически, когда изменяется переменная $this->copy_domain */
    public function updatedCopyDomain()
    {
        /* если передано пустое значение, то есть пункт "Не выбран", то обратно заполняем как было */
        if (empty($this->copy_domain)) {
            $this->copy_domain = $this->domain->domain;
        }
        $domain = Domain::findOrFail($this->copy_domain);
        /* запоминаем ключ, т.к. он затирается, не знаю как по-другому сделать */
        $key = $this->domain->domain;
        /* подготовливаем атрибуты, исключая некоторые */
        $fillableAttributes = Arr::except($domain->getAttributes(), ['domain', 'created_at', 'updated_at']);
        /* копируем */
        foreach ($fillableAttributes as $attribute => $value) {
            $this->domain->{$attribute} = $value;
        }
    }

    public function updateDomain()
    {
        Gate::authorize('everything');
        $this->domain->save();
        $this->domain->refresh();
        $this->js("alert('Настройки изменены')");
    }

    public function isMovieAllowed()
    {
        try {
            $this->isMovieAllowedVars['result'] = (new IsMovieAllowedForDomain)($this->domain, $this->isMovieAllowedVars['movie_id']);
        } catch (\Throwable $e) {
            unset($this->isMovieAllowedVars['result']['result']);
            $this->isMovieAllowedVars['result']['message'] = $e->getMessage();
        }
    }

    public function getPropertiesProperty()
    {
        $properties = collect([
            'watchLink' => [
                'type' => 'text',
            ],
            'groupLink' => [
                'type' => 'text',
            ],
            'genreLink' => [
                'type' => 'text',
            ],
            'watchLinkEn' => [
                'type' => 'text',
            ],
            'watchMovieLink' => [
                'type' => 'text',
            ],
            'watchSerialLink' => [
                'type' => 'text',
            ],
            'episodeLink' => [
                'type' => 'text',
            ],
            'sitemapMoviesPaginationLink' => [
                'type' => 'text',
            ],
            'recaptchaVersion' => [
                'type' => 'text',
            ],
            'topMoviesLimit' => [
                'type' => 'int',
                'name' => 'Фильмов в топе',
            ],
            'moviesLimit' => [
                'type' => 'int',
                'name' => 'Фильмов на стр-це',
            ],
            'moviesPerDay' => [
                'type' => 'int',
                'name' => 'Добавлять в день',
            ],
            'moviesMinYear' => [
                'type' => 'int',
                'name' => 'Минимальный год',
            ],
            'rkn_scheme' => [
                'type' => 'select',
                'options' => [
                    ['name' => 'delete_player_when_ru_and_not_canonical'],
                    ['name' => 'delete_player_only_if_main_domain'],
                    ['name' => 'delete_player_only_if_abuse_on_page'],
                ],
                'option_name' => 'name',
            ],
            'index_route' => [
                'type' => 'text',
            ],
            'main_player' => [
                'type' => 'select',
                'options' => [
                    ['name' => 'allohatv'],
                    ['name' => 'hdvb'],
                    ['name' => 'videodb'],
                    ['name' => 'trailer'],
                ],
                'option_name' => 'name',

            ],
            'allow_movies_only_from' => [
                'type' => 'select',
                'options' => ParsedUrl::selectRaw('distinct domain')->get(),
                'option_name' => 'domain',
                'name' => 'Фильмы только с парсинга',
            ],
            // не доделал, не знаю как быть с routes_type = null, он пустую строку записывает в бд вместо null
            // 'routes_type' => [
            //     'type' => 'select',
            //     'options' => Domain::selectRaw('distinct routes_type')->get(),
            //     'option_name' => 'routes_type',
            //     'name' => 'Тип роутов'
            // ],
            'include_serials' => [
                'type' => 'boolean',
                'name' => 'Сериалы',
            ],
            'include_movies' => [
                'type' => 'boolean',
                'name' => 'Фильмы',
            ],
            'include_trailers' => [
                'type' => 'boolean',
                'name' => 'Трейлеры',
            ],
            'include_games' => [
                'type' => 'boolean',
                'name' => 'Игры',
            ],
            'include_youtube' => [
                'type' => 'boolean',
                'name' => 'Youtube',
            ],
            'movies_with_unique_description' => [
                'type' => 'boolean',
                'name' => 'Только уник. описания',
            ],
            'watchCalcSameMovies' => [
                'type' => 'boolean',
                'name' => 'Генер. похожие на стр-це watch',
            ],
            'calcThumbGenresList' => [
                'type' => 'boolean',
                'name' => 'Генер. список жанров для тумб',
            ],
            'useAltDescriptions' => [
                'type' => 'boolean',
                'name' => 'Альтерн. описания',
            ],
            'allowRussianMovies' => [
                'type' => 'boolean',
                'name' => 'Разрешены РФ фильмы',
            ],
            'rknForRuOnly' => [
                'type' => 'boolean',
                'name' => 'Скрывать плеер только для РФ',
            ],
            'moviesWithPoster' => [
                'type' => 'boolean',
                'name' => 'Только фильмы с постером',
            ],
            'rknBanned' => [
                'type' => 'boolean',
                'name' => 'Забанен в РФ',
            ],
            'allowRiskyStudios' => [
                'type' => 'boolean',
                'name' => 'Разрешить риск. студии',
            ],
            'legalMovies' => [
                'type' => 'boolean',
                'name' => 'Легальные фильмы',
            ],
            'useOnlyChoosenGroups' => [
                'type' => 'boolean',
                'name' => 'Добавлять только фильмы в разрешенных категориях',
            ],
            'useInRedirectToAnyMovie' => [
                'type' => 'boolean',
            ],
            'useMovieId' => [
                'type' => 'boolean',
            ],
            'useTagIdForGenres' => [
                'type' => 'boolean',
            ],
            'dmcaQuestion' => [
                'type' => 'boolean',
            ],
            'episodically' => [
                'type' => 'boolean',
            ],
            'hide_canonical' => [
                'type' => 'boolean',
            ],
            'index_search' => [
                'type' => 'boolean',
            ],
            'ai_descriptions' => [
                'type' => 'boolean',
            ],
            'index_movies_in_yandex' => [
                'type' => 'boolean',
            ],
            'show_trailer_for_non_sng' => [
                'type' => 'boolean',
            ],
            'created_at' => [
                'type' => null,
            ],
            'updated_at' => [
                'type' => null,
            ],
            'moviesAddedPerDay' => [
                'type' => null,
            ],
            'engine_domain' => [
                'type' => null,
            ],
            'domain' => [
                'type' => null,
            ],
        ]);

        return $properties;
    }
}
