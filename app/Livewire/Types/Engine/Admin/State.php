<?php

namespace App\Livewire\Types\Engine\Admin;

use App\Models\Ban;
use App\Models\Config;
use App\Models\DnsProvider;
use App\Models\Log;
use App\Models\Node;
use App\Models\Schedule;
use App\Models\Visit;
use Barryvdh\Debugbar\Facades\Debugbar;
use engine\app\models\F;
use engine\app\models\GeoIP;
use Illuminate\Support\Facades\DB;
use Livewire\Attributes\Computed;
use Livewire\Component;

class State extends Component
{
    public $showFakePlugsStat = false;

    public $showMysqlProcesslist = false;

    public $showIsNotRealBotStat = false;

    public $showCloudflareStat = false;

    public $poll = true;

    public function render()
    {
        Debugbar::startMeasure('state');

        $arrByUserAgent = Visit::query()
            ->selectRaw('userAgent,round(count(*)/60,1) as requestsPerSec')
            ->whereBetween('date', [DB::raw('date_sub(current_timestamp,interval 1 minute)'), DB::raw('current_timestamp')])
            ->groupBy('userAgent')
            ->orderByDesc('requestsPerSec')
            ->limit(10)
            ->get();
        $arrByIp = Visit::query()
            ->selectRaw('ip,round(count(*)/60,1) as requestsPerSec')
            ->whereBetween('date', [DB::raw('date_sub(current_timestamp,interval 1 minute)'), DB::raw('current_timestamp')])
            ->groupBy('ip')
            ->orderByDesc('requestsPerSec')
            ->limit(10)
            ->get();
        $arrByDomain = Visit::query()
            ->selectRaw('host,round(count(*)/60,1) as requestsPerSec')
            ->whereBetween('date', [DB::raw('date_sub(current_timestamp,interval 1 minute)'), DB::raw('current_timestamp')])
            ->groupBy('host')
            ->orderByDesc('requestsPerSec')
            ->limit(10)
            ->get();
        $view = view('livewire.types.engine.admin.state');
        $view->with('visitsByUa', $arrByUserAgent);
        $list = '';
        foreach ($arrByIp as $k => $v) {
            $arrByIp[$k]['geoIp'] = new GeoIP($v['ip']);
            $arrByIp[$k]['ban'] = Ban::query()
                ->where('ip', '!=', '')
                ->whereRaw('? REGEXP ip', [$v['ip']])
                ->first();
        }
        $view->with('visitsByIp', $arrByIp);
        $view->with('visitsByDomain', $arrByDomain);
        // получаем кроны
        $view->with('failedCrons', Schedule::where('active', true)->where('done', false)->get());

        $nodes = Node::whereMonitored(true)->get();

        $requestsByRoute = Visit::query()
            ->selectRaw("SUBSTRING_INDEX(SUBSTRING_INDEX(query, '/', -2), '/', 1) AS route, count(*) as q, avg(time) as avg_time")
        // where date BETWEEN (NOW() - INTERVAL 1 MINUTE) AND NOW()
            ->whereBetween('date', [DB::raw('date_sub(current_timestamp,interval 1 minute)'), DB::raw('current_timestamp')])
            ->groupBy('route')
            ->orderByDesc('q')
            ->limit(15)
            ->get();

        $avgTime = Visit::query()
            ->selectRaw('avg(time) as avg_time')
            ->whereRaw('date > date_sub(current_timestamp,interval 1 minute)')
            ->first();

        $logs = Log::with('visit')->orderByDesc('date')->limit(10)->get();

        $opcache = opcache_get_status();

        $phpFpmProcesses = trim(shell_exec('pgrep -c php-fpm'));

        $mysqlConnections = DB::select("SHOW STATUS LIKE 'Threads_connected'")[0]->Value;

        $banRequestsPerSec = Config::cached()->ban_queries_per_second;

        $mysqlRequestsPerSec = Config::cached()->mysql_queries_per_second;

        $requestsPerSec = F::getVisitsPerSecond();

        Debugbar::stopMeasure('state');

        $view = $view
            ->with('nodes', $nodes)
            ->with('requestsPerSec', $requestsPerSec)
            ->with('mysqlRequestsPerSec', $mysqlRequestsPerSec)
            ->with('banRequestsPerSec', $banRequestsPerSec)
            ->with('mysqlConnections', $mysqlConnections)
            ->with('phpFpmProcesses', $phpFpmProcesses)
            ->with('opcache', $opcache)
            ->with('requestsByRoute', $requestsByRoute)
            ->with('avgTime', $avgTime)
            ->with('logs', $logs);

        return $view;
    }

    public function makeCronAsDone(Schedule $schedule)
    {
        $schedule->done = true;
        $schedule->save();
    }

    #[Computed]
    public function cloudflareAccounts()
    {
        return DnsProvider::where('service', 'cloudflare')->get();
    }

    #[Computed]
    public function fakePlugsStat()
    {

        Debugbar::startMeasure('fakeErrors');

        $byUserAgent = Log::query()
            ->select('visits.userAgent', DB::raw('count(*) as q'))
            ->join('visits', 'visits.id', 'logs.visit_id')
            ->where('logs.date', '>=', now()->subDay())
            ->where('logs.text', 'like', 'Показали фейковую заглушку%')
            ->groupBy('visits.userAgent')
            ->orderByDesc('q')
            ->limit(20)
            ->get();

        $byIp = Log::query()
            ->select('visits.ip', DB::raw('count(*) as q'))
            ->join('visits', 'visits.id', 'logs.visit_id')
            ->where('logs.date', '>=', now()->subDay())
            ->where('logs.text', 'like', 'Показали фейковую заглушку%')
            ->groupBy('visits.ip')
            ->orderByDesc('q')
            ->limit(30)
            ->get();

        $byReferer = Log::query()
            ->select('visits.referer', DB::raw('count(*) as q'))
            ->join('visits', 'visits.id', 'logs.visit_id')
            ->where('logs.date', '>=', now()->subDay())
            ->where('logs.text', 'like', 'Показали фейковую заглушку%')
            ->groupBy('visits.referer')
            ->orderByDesc('q')
            ->limit(30)
            ->get();

        Debugbar::stopMeasure('fakeErrors');

        return (object) ['byIp' => $byIp, 'byUserAgent' => $byUserAgent, 'byReferer' => $byReferer];
    }

    #[Computed]
    public function isNotRealBotStat()
    {

        Debugbar::startMeasure('isNotRealBotStat');

        $byUserAgent = Log::query()
            ->select('visits.userAgent', DB::raw('count(*) as q'))
            ->join('visits', 'visits.id', 'logs.visit_id')
            ->where('logs.date', '>=', now()->subDay())
            ->where('logs.text', 'Is not real bot')
            ->groupBy('visits.userAgent')
            ->orderByDesc('q')
            ->limit(20)
            ->get();

        $byIp = Log::query()
            ->select('visits.ip', DB::raw('count(*) as q'))
            ->join('visits', 'visits.id', 'logs.visit_id')
            ->where('logs.date', '>=', now()->subDay())
            ->where('logs.text', 'Is not real bot')
            ->groupBy('visits.ip')
            ->orderByDesc('q')
            ->limit(30)
            ->get();

        Debugbar::stopMeasure('isNotRealBotStat');

        return (object) ['byIp' => $byIp, 'byUserAgent' => $byUserAgent];
    }

    #[Computed]
    public function mysqlProcesslist()
    {
        return DB::select('SHOW FULL PROCESSLIST');
    }
}
