<?php

namespace App\Livewire\Types\Engine\Admin;

use App\Livewire\Types\Engine\Admin\Mirrors\DomainCard;
use App\Models\DnsProvider;
use App\Models\Domain;
use App\Models\RecaptchaKey;
use App\Models\User;
use App\Models\YandexToken;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Computed;
use Livewire\Attributes\On;
use Livewire\Component;

class DomainSettings extends Component
{
    public Domain $domain;

    protected $rules = [
        'domain.domain' => 'required|string',
        'domain.mirror_of' => 'required|string',
        'domain.redirect_to' => 'required|string',
        'domain.client_redirect_to' => 'required|string',
        'domain.ru_domain' => 'required|string',
        'domain.next_ru_domain' => 'required|string',
        'domain.client_redirect' => 'required|boolean',
        'domain.access_scheme' => 'required|string',
        'domain.type' => 'required|string',
        'domain.on' => 'required|boolean',
        'domain.auth' => 'required|boolean',
        'domain.www' => 'required|string',
        'domain.only_https' => 'required|boolean',
        'domain.only_http' => 'required|boolean',
        'domain.reborn' => 'required|boolean',
        'domain.engine' => 'required|string',
        'domain.site_name' => 'required|string',
        'domain.type_template' => 'required|string',
        'domain.videoroll_id' => 'required|integer',
        'domain.movieads_id' => 'required|string',
        'domain.logo_src' => 'required|string',
        'domain.favicon_src' => 'required|string',
        'domain.bg_src' => 'required|string',
        'domain.comment' => 'required|string',
        'domain.google_analytics_id' => 'required|string',
        'domain.brand' => 'required|boolean',
        'domain.yandexAllowed' => 'required|boolean',
        'domain.recaptcha_key_name' => 'required|string',
        'domain.added_to_alloha' => 'required|boolean',
        'domain.is_masking_domain' => 'required|boolean',
        'domain.ru_block' => 'required|boolean',
        'domain.renewed' => 'required|boolean',
        'domain.yandex_token_email' => 'required|boolean',
        'domain.alloha_txt' => 'required|string',
        'domain.yandex_redirect_to' => 'required|string',
        'domain.known_ru_block' => 'required|boolean',
        'domain.known_kz_block' => 'required|boolean',
        'domain.dns_provider_id' => 'required|integer',
        'domain.certbot' => 'required|boolean',
        'domain.monitoring' => 'required|boolean',
        'domain.allow_not_valid_referers_for_clients' => 'required|boolean',
        'domain.logo_html' => 'required|string',
        'domain.tls_13' => 'required|boolean',
        'domain.ech' => 'required|boolean',
    ];

    public ?string $copy_domain;

    public ?string $mirror;

    public ?string $subdomain;

    public ?string $subdomain_type;

    public ?string $txt_record_content;

    public ?int $metrika_id;

    public User $user;

    public ?string $moveToThisCloudflareAccount = null;

    public ?array $dnsMessages = [];

    public ?bool $isModal = false;

    public ?bool $showDnsSettings = false;

    public function render()
    {
        return view('livewire.types.engine.admin.domain-settings');
    }

    public function mount()
    {
        $this->user = Auth::user();
        // domain теперь определяется в контроллере и пробрасывается из шаблона в livewire компонент, иначе не работал lazy режим
        // $this->domain = Domain::with('metrika')->findOrFail(request()->input('domain'));
        $this->mirror = $this->domain->redirect_to;
        $this->metrika_id = $this->domain->metrika?->metrika_id;
        /* если для переноса доступен только 1 аккаунт, то сразу выбираем его */
        $this->moveToThisCloudflareAccount = ($this->availableForTransferCloudflareAccounts->count() == 1) ? $this->availableForTransferCloudflareAccounts->first()->id : null;
    }

    public function getCopyDomainsProperty()
    {
        return Domain::whereNotIn('type', ['mirror', 'trash'])->where('domain', '!=', $this->domain->domain)->get();
    }

    /* срабатывает автоматически, когда изменяется переменная $this->copy_domain */
    public function updatedCopyDomain()
    {
        /* если передано пустое значение, то есть пункт "Не выбран", то обратно заполняем как было */
        if (empty($this->copy_domain)) {
            $this->copy_domain = $this->domain->domain;
        }
        $domain = Domain::findOrFail($this->copy_domain);
        $this->domain->logo_src = $this->domain->logo_src ? $this->domain->logo_src : $domain->logo_src;
        $this->domain->favicon_src = $this->domain->favicon_src ? $this->domain->favicon_src : $domain->favicon_src;
        $this->domain->bg_src = $this->domain->bg_src ? $this->domain->bg_src : $domain->bg_src;
        $attributes = [
            'engine',
            'type',
            'on',
            'www',
            'only_https',
            'type_template',
            'only_http',
            'client_redirect',
            'yandexAllowed',
            'access_scheme',
        ];
        foreach ($attributes as $attribute) {
            $this->domain->{$attribute} = $domain->{$attribute};
        }
    }

    #[On('update-domain')]
    public function updateDomain()
    {

        /* жуткий костыль-прекостыль, но без понятия как сейчас быстро сделать по-другому */
        if ($this->user->can('everything')) {
            /* метрику обрабатываем отдельно, ибо какие-то глюки со связями в livewire или я тупой */
            /* если передан пустой id метрики, то удаляем связь, если нет, то обновляем значение или создаем связь */
            if (empty($this->metrika_id)) {
                $this->domain->metrika()->delete();
            } else {
                if ($this->domain->metrika) {
                    /* из-за того что первичный ключ - metrika_id нельзя использовать тут просто updateOrCreate, приходится изъебываться */
                    /* если связь существует - обновляем */
                    $this->domain->metrika->metrika_id = $this->metrika_id;
                    $this->domain->metrika->save();
                } else {
                    /* если не существует - создаем */
                    $this->domain->metrika()->create(['metrika_id' => $this->metrika_id]);
                }
            }
            /* если передана пустая строка, то null, потому что null никак не получается передать прямо из формы */
            $this->domain->mirror_of = empty($this->domain->mirror_of) ? null : $this->domain->mirror_of;
            $this->domain->redirect_to = empty($this->domain->redirect_to) ? null : $this->domain->redirect_to;
            $this->domain->yandex_redirect_to = empty($this->domain->yandex_redirect_to) ? null : $this->domain->yandex_redirect_to;
            $this->domain->client_redirect_to = empty($this->domain->client_redirect_to) ? null : $this->domain->client_redirect_to;
            $this->domain->ru_domain = empty($this->domain->ru_domain) ? null : $this->domain->ru_domain;
            $this->domain->next_ru_domain = empty($this->domain->next_ru_domain) ? null : $this->domain->next_ru_domain;
            $this->domain->access_scheme = empty($this->domain->access_scheme) ? null : $this->domain->access_scheme;
            $this->domain->type = empty($this->domain->type) ? null : $this->domain->type;
            $this->domain->www = empty($this->domain->www) ? null : $this->domain->www;
            $this->domain->engine = empty($this->domain->engine) ? null : $this->domain->engine;
            $this->domain->type_template = empty($this->domain->type_template) ? null : $this->domain->type_template;
            $this->domain->recaptcha_key_name = empty($this->domain->recaptcha_key_name) ? null : $this->domain->recaptcha_key_name;
            $this->domain->dns_provider_id = empty($this->domain->dns_provider_id) ? null : $this->domain->dns_provider_id;
            $this->domain->logo_html = empty($this->domain->logo_html) ? null : $this->domain->logo_html;
            $this->domain->save();
            /* без рефреша обновленна метрика появлялась только при перезагрузки страницы */
            $this->domain->refresh();
        } else {
            $allowed_properties = ['logo_src', 'bg_src', 'favicon_src', 'movieads_id', 'site_name'];
            $domain = Domain::find($this->domain->domain);
            foreach ($allowed_properties as $property) {
                $domain->{$property} = $this->domain->{$property};
            }
            $domain->save();
            $this->domain = $domain;
        }
        // TODO: есть проблема, если меняем например родительский домен, то карточка не обновляется, придумать как обновлять нужные карточки либо все сразу
        // посылаем событие на обновление карточки домена
        // $this->dispatch("domain-updated.{$this->domain->mirror_of_or_self->domain}")->to(DomainCard::class);
        // $this->dispatch('domain-updated')->to(Mirrors::class);
        $this->dispatch('refresh')->to(DomainCard::class);
        // после этого события, когда уже точно сохранение произошло - будет закрыто модальное окно
        $this->dispatch('domain-updated')->to(self::class);
        $this->alert('Настройки изменены');
    }

    public function createMirror()
    {
        /* если домен существует и имеет тип trash, то при подтверждении операции (реализовано в шаблоне), делаем его зеркалом */
        if ($this->existedMirror) {
            if ($this->existedMirror->type == 'trash') {
                $mirror = $this->existedMirror;
            } else {
                return $this->js("alert('Домен уже существует и имеет тип {$this->existedMirror->type}. Создание зеркала невозможно.')");
            }
        } else {
            $mirror = new Domain;
        }
        $mirror->domain = $this->mirror;
        $mirror->type = 'mirror';
        $mirror->mirrorOf()->associate($this->domain);
        /* сохраняем здесь, чтобы сработало событие created и создался parent_domain, он нужен в dns() */
        $mirror->save();
        $mirror->refresh();
        try {
            $mirror->dns->addVpsRecord();
            $this->js("alert('Зеркало создано')");
        } catch (\Exception $e) {
            /* что-то пошло не так, удаляем созданную модель */
            $mirror->delete();
            // throw new \Exception("Не получилось добавить CNAME запись: {$e->getMessage()}");
            $this->js("alert(\"Не получилось добавить CNAME запись: {$e->getMessage()}\")");
        }
    }

    public function createSubdomain()
    {
        if (empty($this->subdomain)) {
            $this->js("alert('Введите субдомен')");
        }
        $new_domain = $this->subdomain . '.' . $this->domain->domain;
        $domain = new Domain;
        $domain->domain = $new_domain;
        $domain->type = $this->subdomain_type;
        try {
            $domain->dns->addVpsRecord();
            $domain->save();
            $this->js("alert('Поддомен создан')");
        } catch (\Exception $e) {
            // throw new \Exception("Не получилось добавить CNAME запись: {$e->getMessage()}");
            $this->js("alert(\"Не получилось добавить CNAME запись: {$e->getMessage()}\")");
        }
    }

    public function clearDomainDnsRecords()
    {
        try {
            $this->domain->dns->deleteDnsRecords();
            $this->js("alert('DNS записи очищены')");
        } catch (\Exception $e) {
            $this->js("alert(\"Ошибка при очистке записей: {$e->getMessage()}\")");
        }
    }

    public function addGoogleDns()
    {
        try {
            $this->domain->dns->addGoogleRecord();
            $this->js('alert("CNAME Google создана")');
        } catch (\Exception $e) {
            $this->js("alert(\"Не получилось добавить CNAME запись: {$e->getMessage()}\")");
        }
    }

    public function addMosopenDns()
    {
        try {
            $this->domain->dns->addMosopenRecord();
            $this->js('alert("CNAME Mosopen создана")');
        } catch (\Exception $e) {
            $this->js("alert(\"Не получилось добавить CNAME запись: {$e->getMessage()}\")");
        }
    }

    public function deleteCname()
    {
        try {
            $this->domain->dns->deleteCnameRecord();
            $this->js("alert(\"CNAME записи для {$this->domain->domain} удалены\")");
        } catch (\Exception $e) {
            $this->js("alert(\"Не получилось удалить CNAME записи: {$e->getMessage()}\")");
        }
    }

    public function addVpsDns()
    {
        try {
            $this->domain->dns->addVpsRecord();
            $this->js('alert("CNAME Vps создана")');
        } catch (\Exception $e) {
            $this->js("alert(\"Не получилось добавить CNAME запись: {$e->getMessage()}\")");
        }
    }

    public function createTxtRecord()
    {
        try {
            $this->domain->dns->addDnsRecord('TXT', $this->domain->domain, $this->txt_record_content, false);
            $this->js("alert('TXT запись добавлена')");
        } catch (\Exception $e) {
            $this->js("alert(\"Не получилось добавить TXT запись: {$e->getMessage()}\")");
        }
    }

    #[Computed]
    public function existedMirror()
    {
        /* иначе выдает ошибку can't access before initialization */
        if (isset($this->mirror)) {
            return Domain::find($this->mirror);
        }

        return false;
    }

    public function getTypesProperty()
    {
        return Domain::selectRaw('distinct type')->get();
    }

    #[Computed]
    public function dnsProviders()
    {
        return DnsProvider::all();
    }

    /* фильтруем уже загруженные аккаунты */
    #[Computed]
    public function availableForTransferCloudflareAccounts()
    {
        return $this->dnsProviders
            ->where('service', 'cloudflare')
            ->where('id', '!=', $this->domain->dns_provider_id)
            ->where('available_for_transfer', true)
            ->where('zones.result_info.total_count', '<', 20);
    }

    public function createZoneAndSetDefaultSettings()
    {

        try {
            /* создаем новую доменную зону на новом аккаунте */
            if ($this->domain->dns->getZone()) {
                $this->js("alert('Зона уже существует, идем дальше')");
            } else {
                $this->domain->dns->createZone();
            }
        } catch (\Exception $e) {
            return $this->js("alert('Ошибка при добавлении домена: {$e->getMessage()}')");
        }

        /* применяем дефолтные настройки */
        $this->domain->dns->applyDefaultSettings();

        // отключаем ECH здесь, апи отработает автоматически при сохранении
        $this->domain->ech = false;
        $this->domain->save();

        $nameServers = collect($this->domain->dns->getNameservers())->implode(',');

        /* выдаем какие NS нужно проставить */
        $this->js("alert('Настроили домен {$this->domain->domain} на аккаунте {$this->domain->dns->dnsProvider->email}. NS: {$nameServers}')");
    }

    public function saveRecordsFromProviderToDatabase()
    {
        try {
            $this->dnsMessages = $this->domain->dns->saveRecordsFromProviderToDatabase();
        } catch (\Exception $e) {
            $this->dnsMessages[] = $e->getMessage();
        }
    }

    public function saveRecordsFromDatabaseToProvider()
    {
        try {
            $this->dnsMessages = $this->domain->dns->saveRecordsFromDatabaseToProvider();
        } catch (\Exception $e) {
            $this->dnsMessages[] = $e->getMessage();
        }
    }

    public function moveDomainToAnotherCloudflareAccount()
    {

        try {
            $this->dnsMessages = $this->domain->dns->moveToAnotherAccount(DnsProvider::findOrFail($this->moveToThisCloudflareAccount));
            $this->domain->refresh();
        } catch (\Exception $e) {
            return $this->js("alert('Ошибка: {$e->getMessage()}')");
        }
    }

    public function deleteZone()
    {
        $result = json_encode($this->domain->dns->deleteZone());
        $this->js("alert('Результат удаления: {$result}')");
    }

    public function activationCheck()
    {
        $result = json_encode($this->domain->dns->activationCheck());
        $this->js("alert('Результат: {$result}')");
    }

    public function alert($message)
    {
        session()->flash('message', $message);
    }

    public function getPropertiesProperty()
    {
        $redirect_to_domains = Domain::whereMirrorOf($this->domain->domain)->orWhere('domain', $this->domain->domain)->orderByDesc('add_date')->get();
        $properties = collect([
            'domain' => [
                'type' => 'text',
                'name' => 'Домен',
                'show_if_mirror' => true,
            ],
            'mirror_of' => [
                'type' => 'select',
                'name' => 'Является зеркалом',
                'options' => Domain::doesntHave('mirrorOf')->where('domain', '!=', $this->domain->domain)->get(),
                'option_name' => 'domain',
            ],
            'redirect_to' => [
                'type' => 'select',
                'name' => 'Редирект на',
                'options' => $redirect_to_domains,
                'option_name' => 'domain',
            ],
            'yandex_redirect_to' => [
                'type' => 'select',
                'name' => 'Редирект Яндекса на',
                'options' => $redirect_to_domains,
                'option_name' => 'domain',
            ],
            'client_redirect_to' => [
                'type' => 'select',
                'name' => 'Редирект клиента на',
                'options' => $redirect_to_domains,
                'option_name' => 'domain',
            ],
            'ru_domain' => [
                'type' => 'select',
                'name' => 'Домен для РФ',
                'options' => $redirect_to_domains,
                'option_name' => 'domain',
            ],
            'next_ru_domain' => [
                'type' => 'select',
                'name' => 'След. домен для РФ',
                'options' => $redirect_to_domains,
                'option_name' => 'domain',
            ],
            'client_redirect' => [
                'type' => 'boolean',
                'name' => 'Редирект клиентов на основу',
            ],
            'access_scheme' => [
                'type' => 'select',
                'name' => 'Схема доступа',
                'options' => [['access_scheme' => 'v1'], ['access_scheme' => 'v2'], ['access_scheme' => 'v3']],
                'option_name' => 'access_scheme',
            ],
            'type' => [
                'type' => 'select',
                'name' => 'Движок',
                'options' => $this->types,
                'option_name' => 'type',
                'show_if_mirror' => true,
            ],
            'on' => [
                'type' => 'boolean',
                'name' => 'Состояние',
            ],
            'www' => [
                'type' => 'select',
                'name' => 'WWW',
                'options' => [['www' => 'www_only'], ['www' => 'on'], ['www' => 'off']],
                'option_name' => 'www',
            ],
            'only_https' => [
                'type' => 'boolean',
                'name' => 'Только https',
            ],
            'only_http' => [
                'type' => 'boolean',
                'name' => 'Только http',
            ],
            'engine' => [
                'type' => 'select',
                'name' => 'Версия движка',
                'options' => Domain::selectRaw('distinct engine')->get(),
                'option_name' => 'engine',
            ],
            'site_name' => [
                'type' => 'text',
                'name' => 'Название',
            ],
            'type_template' => [
                'type' => 'select',
                'name' => 'Шаблон',
                'options' => Domain::selectRaw('distinct type_template')->whereNotNull('type_template')->where('type_template', '!=', '')->get(),
                'option_name' => 'type_template',
            ],
            'yandex_token_email' => [
                'type' => 'select',
                'name' => 'Аккаунт Яндекса',
                'options' => YandexToken::all(),
                'option_name' => 'email',
            ],
            'videoroll_id' => [
                'type' => 'int',
                'name' => 'Videoroll ID',
                'show_if_parent_only' => true,
            ],
            'movieads_id' => [
                'type' => 'text',
                'name' => 'Movieads ID',
                'show_if_parent_only' => true,
            ],
            'recaptcha_key_name' => [
                'type' => 'select',
                'name' => 'Рекапча',
                'options' => RecaptchaKey::all(),
                'option_name' => 'name',
                'show_if_parent_only' => true,
            ],
            'dns_provider_id' => [
                'type' => 'select',
                'name' => 'DNS провайдер',
                'options' => $this->dnsProviders->map(function ($provider) {
                    return ['id' => $provider->id, 'option_name' => "[{$provider->service}] {$provider->email}"];
                }),
                'option_name' => 'option_name',
                'option_value' => 'id',
                'show_if_parent_only' => true,
            ],
            'logo_src' => [
                'type' => 'text',
                'name' => 'Logo Src',
            ],
            'favicon_src' => [
                'type' => 'text',
                'name' => 'Favicon Src',
            ],
            'bg_src' => [
                'type' => 'text',
                'name' => 'Bg Src',
            ],
            'yandexAllowed' => [
                'type' => 'boolean',
                'name' => 'Открыт для Яндекса',
            ],
            'added_to_alloha' => [
                'type' => 'boolean',
                'name' => 'Добавлен в аллоху',
                'show_if_parent_only' => true,
            ],
            'is_masking_domain' => [
                'type' => 'boolean',
                'name' => 'Маскировочный',
                'show_if_parent_only' => true,
            ],
            'ru_block' => [
                'type' => 'boolean',
                'name' => 'Заблочен в РФ',
                'show_if_mirror' => true,
            ],
            'renewed' => [
                'type' => 'boolean',
                'name' => 'Продлен',
                'show_if_parent_only' => true,
            ],
            'alloha_txt' => [
                'type' => 'text',
                'name' => 'Alloha код',
                'show_if_parent_only' => true,
            ],
            'google_analytics_id' => [
                'type' => 'text',
                'name' => 'Google Analytics',
            ],
            'known_ru_block' => [
                'type' => 'boolean',
                'name' => 'Известный РФ блок',
                'show_if_mirror' => true,
            ],
            'known_kz_block' => [
                'type' => 'boolean',
                'name' => 'Известный KZ блок',
                'show_if_mirror' => true,
            ],
            'certbot' => [
                'type' => 'boolean',
                'name' => 'Получать сертификат',
                'show_if_parent_only' => true,
            ],
            'monitoring' => [
                'type' => 'boolean',
                'name' => 'Рез. мониторинга',
                'show_if_mirror' => true,
            ],
            'allow_not_valid_referers_for_clients' => [
                'type' => 'boolean',
                'name' => 'Разрешать не валидные рефереры для клиентов',
            ],
            'tls_13' => [
                'type' => 'boolean',
                'name' => 'TLS 1.3',
                'show_if_parent_only' => true,
            ],
            'ech' => [
                'type' => 'boolean',
                'name' => 'ECH',
                'show_if_parent_only' => true,
            ],
            'logo_html' => [
                'type' => 'textarea',
                'name' => 'Logo HTML',
            ],
            'comment' => [
                'type' => 'textarea',
                'name' => 'Заметки',
            ],
            'to_transfer' => [
                'type' => null,
            ],
            'to_registrar' => [
                'type' => null,
            ],
            'code' => [
                'type' => null,
            ],
            'transfer_reason' => [
                'type' => null,
            ],
            'expired' => [
                'type' => null,
            ],
            'owner' => [
                'type' => null,
            ],
            'status' => [
                'type' => null,
            ],
            'charset' => [
                'type' => null,
            ],
            'registrar' => [
                'type' => null,
            ],
            'parent_domain' => [
                'type' => null,
            ],
            'mobile_version' => [
                'type' => null,
            ],
            'create_date' => [
                'type' => null,
            ],
            'lang' => [
                'type' => null,
            ],
            'source_charset' => [
                'type' => null,
            ],
            'rewrite_function' => [
                'type' => null,
            ],
            'additional_path' => [
                'type' => null,
            ],
            'mysql_charset' => [
                'type' => null,
            ],
            'add_date' => [
                'type' => null,
            ],
            'updated_at' => [
                'type' => null,
            ],
            'yandex_redirect' => [
                'type' => null,
                'name' => 'Редиректить Яндекс?',
            ],
            'google_redirect' => [
                'type' => null,
                'name' => 'Редиректить Гугл?',
            ],
            'whois' => [
                'type' => null,
            ],
            /* игнорим старое свойство metrika_id */
            'metrika_id' => [
                'type' => null,
            ],
            /* чтобы связь не отображалась как ненайденное свойство */
            'metrika' => [
                'type' => null,
            ],
            'parent_domain_domain' => [
                'type' => null,
            ],
            'whois_data' => [
                'type' => null,
            ],
            'reborn' => [
                'type' => null,
                'name' => 'Метка Reborn',
            ],
            'brand' => [
                'type' => null,
                'name' => 'Брендовый',
            ],
            'auth' => [
                'type' => null,
                'name' => 'Запрашивать авторизацию',
            ],
            'ru_block_checked_at' => [
                'type' => null,
            ],
            'monitored_at' => [
                'type' => null,
            ],
            'whois_data_updated_at' => [
                'type' => null,
            ],
            /* чтобы связь не отображалась как ненайденное свойство */
            'mirrors' => [
                'type' => null,
            ],
            /* чтобы связь не отображалась как ненайденное свойство (появляется при переносе домена между cloudflare) */
            'dns_provider' => [
                'type' => null,
            ],
            /* чтобы связь не отображалась как ненайденное свойство */
            'parent_dns_records' => [
                'type' => null,
            ],
            'monitoring_ru' => [
                'type' => null,
            ],
            'monitoring_ua' => [
                'type' => null,
            ],
            'monitoring_kz' => [
                'type' => null,
            ],
            'monitoring_lv' => [
                'type' => null,
            ],
            'full_ru_block' => [
                'type' => null,
            ],
            'ignoreBan' => [
                'type' => null,
            ],
            'ignoreSwitch' => [
                'type' => null,
            ],
            'domain_decoded' => [
                'type' => null,
            ],
            // игнорим поле email, будет реализовано в filament
            'email' => [
                'type' => null,
            ],
        ]);
        /* если доменчик зеркало и не родительский, то скрываем все лишние поля */
        if ($this->domain->is_mirror and ! $this->domain->is_parent_domain) {
            $properties = $properties->map(function ($property) {
                if (empty($property['show_if_mirror'])) {
                    $property['type'] = null;
                }

                return $property;
            });
        }
        if (! $this->domain->is_parent_domain) {
            $properties = $properties->map(function ($property) {
                if ($property['show_if_parent_only'] ?? null) {
                    $property['type'] = null;
                }

                return $property;
            });
        }

        return $properties;
    }
}
