<?php

namespace App\Livewire\Types\Engine\Admin;

use App\Models\Config;
use App\Models\Domain;
use App\Models\User;
use cinema\app\models\eloquent\Domain as CinemaDomain;
use cinema\app\models\eloquent\Group;
use cinema\app\models\eloquent\Tag;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Livewire\Attributes\Computed;
use Livewire\Attributes\Url;
use Livewire\Component;

class Mirrors extends Component
{
    // TODO: все ок кроме того, что чекбоксы не отмечены после перезагрузки страницы
    #[Url(as: 'f')]
    public array $domain_filter = ['hasRenewedDomains' => true];

    public bool $show_blocked_domains = false;

    public bool $show_failed_domains = false;

    public ?string $create_domain_name;

    public ?string $list_template = 'full';

    public User $user;

    public $groups;

    public $tags;

    public Group $choosed_group;

    public Tag $choosed_tag;

    public $filtered_domains_limit = 10;

    // здесь содержится домен, который соответствует точному поисковому запросу
    public ?Domain $searchedDomain;

    public int $allDomainsCount;

    public Collection $monitoredCountries;

    // список доменов для отображения ссылок на настройки
    public ?string $domainList = null;

    public function render()
    {
        return view('livewire.types.engine.admin.mirrors');
    }

    public function mount()
    {

        // в фильтрах меняем строковый true и false на булевы, т.к. livewire почему-то не умеет этого делать если #url на массиве, только если на обычных переменных
        foreach ($this->domain_filter as &$filter) {
            if ($filter === 'true') {
                $filter = true;
            }
            if ($filter === 'false') {
                $filter = false;
            }
        }

        unset($filter);

        $this->user = Auth::user();
        $this->domain_filter['type'] = session()->get('domain_filter_type', 'cinema');
        $this->domain_filter['order'] = session()->get('domain_filter_order', null);
        $this->show_blocked_domains = session()->get('show_blocked_domains', $this->show_blocked_domains);
        $this->show_failed_domains = session()->get('show_failed_domains', $this->show_failed_domains);
        $this->list_template = session()->get('list_template', $this->list_template);
        $this->groups = Group::all();
        $this->tags = Tag::where('type', 'genre')->get();
        $this->allDomainsCount = Domain::count();
        $this->monitoredCountries = collect(Config::cached()->monitored_countries);
    }

    public function getDomainsInListProperty()
    {
        $exploded = explode(PHP_EOL, trim($this->domainList ?? ''));
        $domains = collect($exploded);
        $domains = $domains->map(function ($domain) {
            return trim(idn_to_ascii($domain));
        });

        return $domains ? Domain::whereIn('domain', $domains)->get() : collect();
    }

    public function getFilteredDomainsProperty()
    {
        $domains = Domain::query();

        if ($this->user->cannot('everything')) {
            $domains->whereIn('type', ['cinema', 'copy', 'blog']);
        }

        if ($this->domain_filter['type']) {
            $domains->whereType($this->domain_filter['type']);
        }
        if ($this->domain_filter['order']) {
            if ($this->domain_filter['order'] == 'add_date') {
                $domains->orderBy('add_date');
            }
            if ($this->domain_filter['order'] == 'add_date desc') {
                $domains->orderByDesc('add_date');
            }
        }
        if ($this->domain_filter['brand'] ?? null) {
            $domains->whereBrand(true);
        }
        if ($this->domain_filter['yandexNotAllowed'] ?? null) {
            $domains->where('yandexAllowed', false);
        }
        if ($this->domain_filter['accessSchemeV1'] ?? null) {
            $domains->whereAccessScheme('v1');
        }
        if ($this->domain_filter['accessSchemeV2'] ?? null) {
            $domains->whereAccessScheme('v2');
        }
        if ($this->domain_filter['accessSchemeV3'] ?? null) {
            $domains->whereAccessScheme('v3');
        }
        if ($this->domain_filter['hreflang'] ?? null) {
            $domains->where(function ($query) {
                $query
                    ->whereNotNull('ru_domain')
                    ->orWhereNotNull('next_ru_domain');
            });
        }
        if ($this->domain_filter['withoutClientDomain'] ?? null) {
            $domains->whereNull('client_redirect_to');
        }
        if ($this->domain_filter['type_template'] ?? null) {
            $domains->whereTypeTemplate($this->domain_filter['type_template']);
        }
        if ($this->domain_filter['with_client_redirect'] ?? null) {
            $domains->whereClientRedirect($this->domain_filter['with_client_redirect']);
        }
        if ($this->domain_filter['is_off'] ?? null) {
            $domains->whereOn(false);
        }
        if ($this->domain_filter['is_masking_domain'] ?? null) {
            $domains->whereIsMaskingDomain(true);
        }
        if ($this->domain_filter['allow_not_valid_referers_for_clients'] ?? null) {
            $domains->where('allow_not_valid_referers_for_clients', true);
        }

        // домены и их зеркала где есть ошибка мониторинга
        if ($this->domain_filter['failedMonitoring'] ?? null) {
            $domains->where(function ($query) {
                $query
                    ->where(fn ($query) => $query->monitored()->where('monitoring', false))
                    ->orWhereHas('mirrors', fn ($mirror) => $mirror->monitored()->where('monitoring', false));
            });
        }

        /* cinema */
        if (($this->domain_filter['type'] ?? null) == 'cinema') {
            if ($this->domain_filter['useAltDescriptions'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'useAltDescriptions', true);
            }
            if ($this->domain_filter['withoutRussianMovies'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'allowRussianMovies', false);
            }
            if ($this->domain_filter['withRussianMovies'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'allowRussianMovies', true);
            }
            if ($this->domain_filter['rknForRuOnly'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'rknForRuOnly', true);
            }
            if ($this->domain_filter['serialsOnly'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'include_movies', false);
                $domains->whereRelation('cinemaDomain', 'include_serials', true);
            }
            if ($this->domain_filter['includeYoutube'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'include_youtube', true);
            }
            if ($this->domain_filter['withoutRiskyStudios'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'allowRiskyStudios', false);
            }
            if ($this->domain_filter['withRiskyStudios'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'allowRiskyStudios', true);
            }
            if ($this->domain_filter['moviesMinYear'] ?? null) {
                $domains->whereHas('cinemaDomain', function ($query) {
                    $query->whereNotNull('moviesMinYear');
                });
            }
            if ($this->domain_filter['withDeletePlayerOnlyIfMainDomain'] ?? null) {
                $domains->whereHas('cinemaDomain', function ($query) {
                    $query->whereRknScheme('delete_player_only_if_main_domain');
                });
            }
            if ($this->domain_filter['withDeletePlayerOnlyIfAbuseOnPage'] ?? null) {
                $domains->whereHas('cinemaDomain', function ($query) {
                    $query->whereRknScheme('delete_player_only_if_abuse_on_page');
                });
            }
            if ($this->domain_filter['withDeletePlayerWhenRuAndNotCanonical'] ?? null) {
                $domains->whereHas('cinemaDomain', function ($query) {
                    $query->whereRknScheme('delete_player_when_ru_and_not_canonical');
                });
            }
            if ($this->domain_filter['withMovieads'] ?? null) {
                $domains->whereHas('blocks', function ($query) {
                    $query->whereTemplate('promo/movieads.ru/script.html')->whereActive(true);
                });
            }
            if ($this->domain_filter['withVideoroll'] ?? null) {
                $domains->whereHas('blocks', function ($query) {
                    $query->whereTemplate('promo/videoroll.net/script_vpaut.html')->whereActive(true);
                });
            }
            if ($this->domain_filter['withHdvbSticker'] ?? null) {
                $domains->whereHas('blocks', function ($query) {
                    $query->whereTemplate('promo/hdvb/sticker.html')->whereActive(true);
                });
            }
            if ($this->domain_filter['episodically'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'episodically', true);
            }
            if ($this->domain_filter['withAiDescriptions'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'ai_descriptions', true);
            }
            if ($this->domain_filter['showTrailerForNonSng'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'show_trailer_for_non_sng', true);
            }
            if ($this->domain_filter['hideCanonical'] ?? null) {
                $domains->whereRelation('cinemaDomain', 'hide_canonical', true);
            }
        }
        /*
        if (!empty($this->domain_filter['search'])) {
            $search = $this->domain_filter['search'];
            // находим все домены, поддомены, и комментарии с вхождением строки
            $domains->where(function($query) use ($search) {
                $query->where('domain','like',"%{$search}%");
                // $query->orWhere('domain_decoded','like',"%{$search}%");
                $query->orWhere('comment','like',"%{$search}%");
                $query->orWhereRelation('mirrors','domain','like',"%{$search}%");
                // $query->orWhereRelation('mirrors','domain_decoded','like',"%{$search}%");
            });
        }
        */
        $domains = $domains
        /* withCount уже можно убрать, т.к. уже подгружаем все зеркала ниже */
        // ->withCount('mirrors')
            ->with('mirrors')
            ->with('mirrors.parentDomain')
            ->with('mirrors.parentDomain.dnsProvider')
            ->with('parentDomain')
            ->with('parentDomain.dnsProvider')
            ->with('cinemaDomain', function ($query) {
                $query
                    ->withCount('groups')
                    ->withCount('tags');
            })
        // ->with('cinemaDomain.groups')
        // ->with('cinemaDomain.tags')
            ->with('redirectTo')
        // эта подгрузка связи как будто не имеет смысла, но нужна для nextMirror в блокировках
            ->with('redirectTo.mirrorOf')
            ->with('redirectTo.parentDomain')
            ->with('yandexRedirectTo')
        // эта подгрузка связи как будто не имеет смысла, но нужна для nextMirror в блокировках
            ->with('yandexRedirectTo.mirrorOf')
            ->with('yandexRedirectTo.parentDomain')
            ->with('clientRedirectTo')
        // эта подгрузка связи как будто не имеет смысла, но нужна для nextMirror в блокировках
            ->with('clientRedirectTo.mirrorOf')
            ->with('clientRedirectTo.parentDomain')
            ->with('blocks')
            ->with('metrika')
        // ->limit(10)
            ->get();

        /* чтобы использовать далее в use () */
        $domain_filter = $this->domain_filter;

        /* здесь расчитываем фильтры для engine домена, которые не можем расчитать через запрос отношений */
        $domains = $domains->filter(function ($domain) use ($domain_filter) {
            $results = [];
            if ($domain_filter['with_traffic'] ?? null) {
                if ($domain->metrika?->visits) {
                    $results[] = $domain->metrika->visits >= 10000;
                } else {
                    $results[] = false;
                }
            }
            if ($domain_filter['googleMoreThenYandex'] ?? null) {
                $results[] = $domain->metrika?->google_percent > $domain->metrika?->yandex_percent;
            }
            if ($domain_filter['yandexMoreThenGoogle'] ?? null) {
                $results[] = $domain->metrika?->yandex_percent > $domain->metrika?->google_percent;
            }
            if ($domain_filter['blocked'] ?? null) {
                $results[] = $domain->ru_blocked_current_mirrors->count() ? true : false;
            }
            if ($domain_filter['renewError'] ?? null) {
                /* если хотя бы один родительский домен заканчивается - то домен подходит под условие */
                $results[] = $domain->related_parent_domains->some->renewal_error ? true : false;
            }
            if ($domain_filter['doesntHaveRenewedDomains'] ?? null) {
                $results[] = ! $domain->related_parent_domains->filter(fn ($parent) => ($domain->is_masking_domain ? true : ! $parent->is_masking_domain) and $parent->renewed)->count();
            }
            if ($domain_filter['hasRenewedDomains'] ?? null) {
                $results[] = $domain->related_parent_domains->filter(fn ($parent) => ($domain->is_masking_domain ? true : ! $parent->is_masking_domain) and $parent->renewed)->count();
            }
            /* не используем здесь every_actual_crawler_mirror_in_known_block, чтобы видеть домены, в которых хотя бы одно актуальное зеркало для бота находится в известном блоке */
            if ($domain_filter['ruKnownBlock'] ?? null) {
                $results[] = $domain->actual_mirrors->contains('known_ru_block', true);
            }
            if ($domain_filter['hasFolder'] ?? null) {
                $results[] = $domain->has_folder;
            }

            if ($this->domain_filter['technitium'] ?? null) {
                $results[] = $domain->related_parent_domains->contains(function ($parentDomain) {
                    return $parentDomain->dnsProvider?->service == 'technitium';
                });
            }

            if ($domain_filter['hasBlockedDomains'] ?? null) {
                $results[] = $domain->ru_blocked_current_mirrors->count() ? true : false;
            }

            if ($domain_filter['hasFailedDomains'] ?? null) {
                $results[] = ($domain->errors->count() or $domain->mirrors->contains(function ($mirror) {
                    return $mirror->errors->count();
                })) ? true : false;
            }

            return in_array(false, $results) ? false : true;
        });

        /* сортировки по отношениям с вычисляемыми атрибутами */
        if ($this->domain_filter['order']) {
            if ($this->domain_filter['order'] == 'metrika.visits') {
                $domains = $domains->sortBy('metrika.visits');
            }
            if ($this->domain_filter['order'] == 'metrika.visits desc') {
                $domains = $domains->sortByDesc('metrika.visits');
            }
            if ($this->domain_filter['order'] == 'old mirrors first') {
                $domains = $domains->sortBy(function ($domain) {
                    /* сначала сделал через min, но если одна из двух дат null, то min будет null и из-за этого некорректно отрабатывает, лучше max */
                    return max($domain->redirectTo?->add_date, $domain->yandexRedirectTo?->add_date);
                });
            }
            if ($this->domain_filter['order'] == 'recent mirrors first') {
                $domains = $domains->sortByDesc(function ($domain) {
                    return max($domain->redirectTo?->add_date, $domain->yandexRedirectTo?->add_date);
                });
            }
        }

        if (! empty($this->domain_filter['search'])) {
            $search = $this->domain_filter['search'];
            /* находим все домены, поддомены, и комментарии с вхождением строки */
            $domains = $domains->filter(function ($domain) use ($search) {
                if ($domain->domain and Str::contains($domain->domain, $search)) {
                    return true;
                }
                if ($domain->domain_decoded and Str::contains($domain->domain_decoded, $search)) {
                    return true;
                }
                if ($domain->comment and Str::contains($domain->comment, $search)) {
                    return true;
                }
                /* используем contains - чтобы не перебирал весь список, достаточно одного вхождения */
                $mirrors = $domain->mirrors->contains(function ($domain) use ($search) {
                    if ($domain->domain and Str::contains($domain->domain, $search)) {
                        return true;
                    }
                    if ($domain->domain_decoded and Str::contains($domain->domain_decoded, $search)) {
                        return true;
                    }
                });
                if ($mirrors) {
                    return true;
                }
            });
        }

        return $domains;
    }

    public function getBlockedDomainsProperty()
    {
        return $this->filtered_domains
            ->filter(function ($domain) {
                return $domain->ru_blocked_current_mirrors->count();
            });
    }

    public function getFailedDomainsProperty()
    {
        return $this->filtered_domains->filter(function ($domain) {
            return $domain->errors->count() or $domain->mirrors->contains(function ($mirror) {
                return $mirror->errors->count();
            });
        });
    }

    public function getMetrikaTotalVisitsProperty()
    {
        $total = 0;
        $this->filtered_domains->each(function ($domain) use (&$total) {
            if ($domain->metrika?->visits) {
                $total = $total + $domain->metrika->visits;
            }
        });

        return $total;
    }

    public function getTypesProperty()
    {
        $types = Domain::selectRaw('type, count(*) as q')->groupBy('type');
        if ($this->user->cannot('everything')) {
            $types->whereIn('type', ['cinema', 'copy', 'blog']);
        }

        return $types->get();
    }

    public function setDomainFilter($filter, $value)
    {
        if ($filter == 'type_template') {
            $value = ($value == ($this->domain_filter['type_template'] ?? null)) ? null : $value;
        }
        $this->domain_filter[$filter] = $value;
        if (in_array($filter, ['type', 'order'])) {
            session()->put("domain_filter_{$filter}", $value);
        }
    }

    public function createDomain()
    {
        $domain = new Domain;
        $domain->domain = $this->create_domain_name;
        $domain->type = $this->domain_filter['type'] ?? 'engine';
        $domain->save();
        if ($domain->type == 'cinema') {
            $cinema_domain = new CinemaDomain;
            $cinema_domain->domain = $this->create_domain_name;
            $cinema_domain->save();
        }
        $this->create_domain_name = null;
    }

    public function updatedShowBlockedDomains()
    {
        session()->put('show_blocked_domains', $this->show_blocked_domains);
    }

    public function updatedShowFailedDomains()
    {
        session()->put('show_failed_domains', $this->show_failed_domains);
    }

    public function updatedListTemplate()
    {
        session()->put('list_template', $this->list_template);
    }

    public function updatedDomainFilter()
    {
        if (isset($this->domain_filter['search'])) {
            $this->domain_filter['search'] = trim($this->domain_filter['search']);
            // если есть домен с точным вхождением поисковой строки, то запоминаем его
            $this->searchedDomain = Domain::where('domain', $this->domain_filter['search'])->first();
        }
    }

    public function setFilteredDomainsLimit($limit)
    {
        $this->filtered_domains_limit = $limit;
    }

    #[Computed]
    public function activeDomainFilters()
    {
        return collect($this->domain_filter)
            ->except(['order', 'type', 'search', 'hasBlockedDomains', 'hasFailedDomains', 'hasRenewedDomains'])
            ->filter(fn ($filter) => $filter);
    }
}
