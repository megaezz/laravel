<?php

namespace App\Livewire\Types\Engine\Admin\Mirrors;

use App\Models\Domain;
use Livewire\Attributes\On;
use Livewire\Component;

class DomainCard extends Component
{
    public Domain $domain;

    public bool $showMirrors = false;

    public bool $showGroups = false;

    public bool $showGroupEditor = false;

    public bool $showGenres = false;

    public bool $showCommentEditor = false;

    public ?string $comment;

    public $view = null;

    protected $listeners = [
        'refresh' => '$refresh',
    ];

    public function mount()
    {
        $this->comment = $this->domain->comment;
    }

    public function render()
    {
        return view($this->view ?? 'livewire.types.engine.admin.mirrors.domain-card');
    }

    public function editComment()
    {
        $this->domain->comment = $this->comment;
        $this->domain->save();
        $this->showCommentEditor = false;
    }

    public function showMetrika()
    {
        dump($this->domain->metrika->data);
    }

    public function showWhois()
    {
        $this->domain->related_parent_domains->each(function ($domain) {
            dump($domain->domain);
            dump($domain->whois);
        });
    }

    // слушаем событие обновления домена, чтобы обновить определенную карточку
    #[On('domain-updated.{domain.domain}')]
    public function refresh()
    {
        $this->domain->refresh();
    }

    public function setMirror(Domain $mirror, string $type)
    {
        if ($mirror->mirrorOf?->domain !== $this->domain->domain) {
            return $this->js("alert('Вообще-то это не зеркало для {$this->domain->domain}')");
        }
        if ($type == 'redirect') {
            // для прошлого зеркала автоматически прописываем известный блок
            $this->domain->redirectTo->known_ru_block = true;
            $this->domain->redirectTo->save();
            $this->domain->redirectTo()->associate($mirror);
        }
        if ($type == 'client_redirect') {
            // для прошлого зеркала автоматически прописываем известный блок
            $this->domain->clientRedirectTo->known_ru_block = true;
            $this->domain->clientRedirectTo->save();
            $this->domain->clientRedirectTo()->associate($mirror);
        }
        if ($type == 'yandex_redirect') {
            // для прошлого зеркала автоматически прописываем известный блок
            $this->domain->yandexRedirectTo->known_ru_block = true;
            $this->domain->yandexRedirectTo->save();
            $this->domain->yandexRedirectTo()->associate($mirror);
        }
        $this->domain->save();
    }

    public function createNextMirror(Domain $mirror)
    {
        $next_mirror = new Domain;
        $next_mirror->domain = $mirror->next_mirror;
        $next_mirror->type = 'mirror';
        $next_mirror->mirrorOf()->associate($this->domain);
        /* все-таки надо сохранять здесь, чтобы создалась связь parentDomain по событию createing */
        $next_mirror->save();
        $next_mirror->refresh();
        try {
            $next_mirror->dns->addVpsRecord();
            // $next_mirror->save();
        } catch (\Exception $e) {
            /* что-то пошло не так, удаляем созданный домен */
            $next_mirror->delete();
            $this->js("alert(\"Не получилось добавить CNAME запись: {$e->getMessage()}\")");
        }
    }
}
