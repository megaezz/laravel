<?php

namespace App\Livewire\Types\Engine\Admin\Mirrors;

use App\Models\Domain;
use Livewire\Attributes\On;
use Livewire\Component;

class DomainSettingsModal extends Component
{
    public ?Domain $domain;

    public ?string $contentType = null;

    public function render()
    {
        return view('livewire.types.engine.admin.mirrors.domain-settings-modal');
    }

    #[On('set-domain')]
    public function setDomain(?string $domain)
    {
        $this->contentType = null;
        $this->domain = $domain ? Domain::findOrFail($domain) : null;
    }

    #[On('show-rkns')]
    public function showRkns(?string $domain)
    {
        $this->setDomain($domain);
        $this->contentType = 'rkn';
    }
}
