<?php

namespace App\Livewire\Types\Engine\Admin;

use App\Models\Ban;
use Livewire\Attributes\Computed;
use Livewire\Component;
use Livewire\WithPagination;

class Bans extends Component
{
    use WithPagination;

    public $limit = 30;

    public $orderBy;

    public $orders = [
        'updated_at desc',
        'updated_at',
        'created_at desc',
        'created_at',
    ];

    public function mount()
    {
        $this->orderBy = collect($this->orders)->first();
    }

    public function render()
    {
        return view('livewire.types.engine.admin.bans');
    }

    #[Computed]
    public function bans()
    {
        $bans = Ban::orderByRaw($this->orderBy);

        return $bans->simplePaginate($this->limit);
    }
}
