<?php

namespace App\Livewire\Types\Engine\Admin;

use App\Models\Domain;
use App\Models\Route;
/* чтобы работыли computed свойства */
use Livewire\Attributes\Computed;
use Livewire\Component;

class DomainRoutes extends Component
{
    public ?Route $route = null;

    public Domain $domain;

    public int $selectedRouteId;

    public ?string $copyFromDomain;

    public ?string $newRoutePattern;

    protected $rules = [
        'route.http_methods' => '',
        'route.name' => '',
        'route.pattern' => '',
        'route.vars' => '',
        'route.template' => '',
        'route.alias' => '',
        'route.controller' => '',
        'route.method' => '',
        'route.middleware' => '',
        'route.var1' => '',
        'route.var2' => '',
        'route.var3' => '',
        'route.var4' => '',
        'route.var5' => '',
        'route.comment' => '',
        'route.title' => '',
        'route.description' => '',
        'route.keywords' => '',
        'route.h1' => '',
        'route.topText' => '',
        'route.bottomText' => '',
        'route.bread' => '',
        'route.text1' => '',
        'route.text2' => '',
        'route.h2' => '',
        'route.redirectToAlias' => '',
    ];

    public function render()
    {
        return view('livewire.types.engine.admin.domain-routes');
    }

    public function mount() {}

    public function saveRoute()
    {
        $this->route->save();
        /* чтобы обновились роуты сбоку, если они были изменены */
        $this->domain->refresh();
        session()->flash('message', 'Сохранено');
    }

    public function copyRoutes()
    {
        if (empty($this->copyFromDomain)) {
            throw new \Exception('Выберите домен');
        }
        $copyRoutes = Domain::where('type', $this->domain->type)->whereNull('mirror_of')->findOrFail($this->copyFromDomain)->routes;
        $copyRoutes->each(function ($route) {
            $this->domain->routes()->save($route->replicate());
        });
        /* чтобы обновились роуты сбоку, если они были изменены */
        $this->domain->refresh();
        session()->flash('message', "Скопировали роуты из {$this->copyFromDomain}");
    }

    public function createRoute()
    {
        if (empty($this->newRoutePattern)) {
            throw new \Exception('Укажите паттерн для роута');
        }
        /* используем make вместо create, т.к. он не сохраняет сразу в бд, и. случае если что-то пойдет не так - не останется ничего лишнего в бд */
        $newRoute = $this->domain->routes()->make();
        $newRoute->pattern = $this->newRoutePattern;
        $newRoute->save();
        /* чтобы обновились роуты сбоку, если они были изменены */
        $this->domain->refresh();
        session()->flash('message', "Роут {$this->newRoutePattern} успешно добавлен");
    }

    public function deleteRoute()
    {
        $this->route->delete();
        /* чтобы обновились роуты сбоку, если они были изменены */
        $this->domain->refresh();
        session()->flash('message', "Роут {$this->route->pattern} удален");
    }

    public function updated($property)
    {

        /* вместо updatedSelectedRouteId, так покрасивше */
        /* если изменяется свойство selectedRouteId - подтягиваем нужный роут */
        if ($property === 'selectedRouteId') {
            $this->route = $this->domain->routes()->findOrFail($this->selectedRouteId);
        }

    }

    #[Computed]
    public function copyFromDomains()
    {
        return Domain::where('type', $this->domain->type)->whereNull('mirror_of')->get();
    }
}
