<?php

namespace App\Livewire\Types\Engine\Admin;

use App\Models\Abuse;
use App\Models\Visit;
use engine\app\models\GeoIP;
use Livewire\Attributes\Computed;
use Livewire\Attributes\Url;
use Livewire\Component;

class VisitAnalysis extends Component
{
    #[Url]
    public $type;

    #[Url]
    public $service;

    #[Url]
    public $days;

    #[Url]
    public $limit;

    #[Url]
    public $withBanInfo;

    #[Url]
    public $withRealBots;

    #[Url]
    public $q;

    public function render()
    {
        return view('livewire.types.engine.admin.visit-analysis');
    }

    public function placeholder()
    {
        return view('components.placeholders.dots');
    }

    #[Computed]
    public function services()
    {
        return Abuse::distinct()->get(['service'])->pluck('service');
    }

    #[Computed]
    public function types()
    {
        return ['all', 'links'];
    }

    public function updated($property, $value)
    {
        if (in_array($value, ['null', 'true', 'false'])) {
            $this->{$property} = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        }
    }

    #[Computed]
    public function visits()
    {

        if (! in_array($this->type, $this->types)) {
            throw new \Exception('Такой тип выборки не существует');
        }

        $visits = Visit::query()
            ->with('ipRelation')
            ->fromSub(function ($query) {

                if ($this->type == 'all') {
                    $query
                        ->selectRaw('visits.ip, COUNT(*) as q');
                }

                if ($this->type == 'links') {
                    $query
                        ->selectRaw('visits.ip, count(distinct abuses.query) as q')
                        ->join('engine.abuses', 'visits.query', '=', 'abuses.query');

                    if ($this->service) {
                        $query->where('abuses.service', $this->service);
                    }
                }

                $query
                    ->from('visits')
                    ->whereBetween('date', [now()->subDays($this->days), now()])
                    ->groupBy('ip')
                    ->having('q', '>', $this->q);

            }, 'visits2')
            ->leftJoin('engine.ban', function ($join) {
                $join->on('visits2.ip', 'REGEXP', 'ban.ip')->where('ban.ip', '!=', '');
            })
            ->select('visits2.ip', 'visits2.q', 'ban.description', 'ban.hide_player', 'ban.active')
            ->orderBy('visits2.ip')
            ->orderByDesc('visits2.q')
            ->limit($this->limit);

        if ($this->withBanInfo === true) {
            $visits->whereNotNull('ban.ip');
        }

        if ($this->withBanInfo === false) {
            $visits->whereNull('ban.ip');
        }

        $visits = $visits->get();

        /* TODO: этот говнокод нужен, потому что не используется модель Visit, надо переписать запрос */
        foreach ($visits as $visit) {
            $visit->geoIp = new GeoIp($visit->ip);
        }

        /* если стоит опция withRealBots, то проходим по циклу, определяем реальных ботов и убираем их или оставляем */
        if ($this->withRealBots === true or $this->withRealBots === false) {
            foreach ($visits as $key => $visit) {
                if ($visit->ipRelation?->is_real_bot) {
                    if ($this->withRealBots === false) {
                        unset($visits[$key]);
                    }
                } else {
                    if ($this->withRealBots === true) {
                        unset($visits[$key]);
                    }
                }
            }
        }

        return $visits;
    }
}
