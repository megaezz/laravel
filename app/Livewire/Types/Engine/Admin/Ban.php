<?php

namespace App\Livewire\Types\Engine\Admin;

use App\Models\Ban as BanModel;
use Livewire\Component;

class Ban extends Component
{
    public BanModel $ban;

    public bool $active;

    public bool $hidePlayer;

    public string $description;

    public bool $descriptionEditor = false;

    public bool $deleteConfirmed = false;

    public function render()
    {
        return view('livewire.types.engine.admin.ban');
    }

    public function mount()
    {
        $this->active = $this->ban->active;
        $this->hidePlayer = $this->ban->hide_player;
        $this->description = $this->ban->description;
    }

    public function delete()
    {
        if ($this->deleteConfirmed) {
            $this->ban->delete();
            $this->dispatch('deleted');
        } else {
            $this->deleteConfirmed = true;
        }
    }

    public function save()
    {
        $this->ban->active = $this->active;
        $this->ban->hide_player = $this->hidePlayer;
        $this->ban->description = $this->description;
        $this->ban->save();
    }

    public function updateDescription()
    {
        $this->descriptionEditor = false;
        $this->save();
    }

    public function updated($propertyName)
    {
        if (in_array($propertyName, ['active', 'hidePlayer'])) {
            $this->save();
        }
    }
}
