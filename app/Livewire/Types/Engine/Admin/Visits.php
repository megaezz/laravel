<?php

namespace App\Livewire\Types\Engine\Admin;

use App\Models\Ban;
use App\Models\Log;
use App\Models\Visit;
use engine\app\models\GeoIP;
use Livewire\Attributes\Computed;
use Livewire\Attributes\Url;
use Livewire\Component;
use Livewire\WithPagination;

class Visits extends Component
{
    use WithPagination;

    public ?Ban $ban = null;

    #[Url]
    public $ip;

    #[Url]
    public $days = 1;

    #[Url]
    public $userAgent;

    #[Url]
    public $ipRegexp;

    #[Url]
    public $userAgentRegexp;

    #[Url]
    public $limit = 100;

    #[Url]
    public $host;

    /* расчитать абузы на страницы */
    #[Url]
    public $calcQueryWasAbusedByServices = false;

    #[Url]
    public $data = 'visits';

    public $poll = false;

    #[Url]
    public $filterOnlyRealBotIp = false;

    #[Url]
    public $filterNotRenewed = false;

    #[Url]
    public $showBanForm = false;

    #[Url]
    public $filterLogText = false;

    #[Url]
    public $filterHasBan = false;

    #[Url]
    public $filterErrors = null;

    #[Url]
    public $logTexts = [
        'Показали фейк заглушку' => 'Показали фейковую заглушку%',
        'Is not real bot' => 'Is not real bot',
        'Domain doesn\'t exist' => '%doesn\'t exist',
        'Route was not recognized' => 'Route was not recognized',
        'syntax error, unexpected token "*"' => 'syntax error, unexpected token "*"',
        'Ошибка eval' => 'Ошибка eval%',
        'Ошибка PHP' => 'PHP ошибка%',
        'Favicon.ico не указан для сайта' => 'Favicon.ico не указан для сайта',
    ];

    public $message;

    public array $createBanForm = ['ip' => null, 'hide_player' => true, 'active' => false, 'description' => null];

    public function render()
    {

        $this->message = $this->filterOnlyRealBotIp ? 'Ограничили время 20 минутами' : null;

        return view('livewire.types.engine.admin.visits');
    }

    public function mount()
    {
        if (request()->has('limit')) {
            $this->limit = request()->get('limit');
        }
        if (request()->has('days')) {
            $this->days = request()->get('days');
        }
        if (request()->has('ip')) {
            $this->ip = request()->get('ip');
        }
        if (request()->has('userAgent')) {
            $this->userAgent = request()->get('userAgent');
        }
        if (request()->has('host')) {
            $this->host = request()->get('host');
        }
        if (request()->has('ipRegexp')) {
            $this->ipRegexp = request()->get('ipRegexp');
        }
        if (request()->has('userAgentRegexp')) {
            $this->userAgentRegexp = request()->get('userAgentRegexp');
        }
        if (request()->has('calcQueryWasAbusedByServices')) {
            $this->calcQueryWasAbusedByServices = request()->get('calcQueryWasAbusedByServices');
        }
        if (request()->has('filterLogText')) {
            $this->filterLogText = request()->get('filterLogText');
        }
        if (request()->has('filterOnlyRealBotIp')) {
            $this->filterOnlyRealBotIp = request()->get('filterOnlyRealBotIp');
        }
        if (request()->has('data')) {
            $this->data = request()->get('data');
        }
        if (request()->has('ban')) {
            $this->ban = Ban::findOrFail(request()->get('ban'));
        }
        $this->createBanForm['ip'] = $this->ip ? '^'.preg_quote($this->ip).'$' : null;
        // $get = ['r' => 'admin/Admin/visits', 'ip' => $ip, 'page' => $page, 'ip_regexp' => $ip_regexp, 'limit' => $limit, 'days' => $days];

    }

    #[Computed]
    public function ipBan()
    {
        if ($this->ip) {
            return Ban::where('ip', '!=', '')
                ->whereRaw('? REGEXP ip', [$this->ip])
                ->first();
        }

        return null;
    }

    #[Computed]
    public function geoIp()
    {
        if ($this->ip) {
            return new GeoIP($this->ip);
        }

        return null;
    }

    #[Computed]
    public function visits()
    {

        $visits = Visit::query()
            ->with('ipRelation')
            ->orderByDesc('date');

        if ($this->calcQueryWasAbusedByServices) {
            $visits->with('queryWasAbusedByServices');
        }

        if ($this->days) {
            $visits->whereBetween('date', [now()->subDays($this->days), now()]);
        }

        $visits = $this->applyVisitFilters($visits);

        $visits = $visits->simplePaginate($this->limit);

        /* если нужно отфильтровать pagination коллекцию, то пользуемся этим способом */
        /* отфильтровываем коллекцию, содержащуюся в Pagination инстансе */
        // $filteredCollection = $visits->getCollection()->filter(function($visit) {
        //     $results = [];
        //     if ($this->filterOnlyRealBotIp) {
        //         $results[] = $visit->ipRelation?->is_real_bot;
        //     }
        //     return in_array(false, $results) ? false : true;
        // });

        /* меняем коллекцию в Pagination на отфильтрованную */
        // $visits->setCollection($filteredCollection);

        return $visits;
    }

    #[Computed]
    public function logs()
    {

        $logs = Log::query()
            ->with('visit')
            ->with('visit.ipRelation')
            ->orderByDesc('logs.date');

        if ($this->calcQueryWasAbusedByServices) {
            $logs->with('visit.queryWasAbusedByServices');
        }

        if ($this->days) {
            $logs->whereBetween('date', [now()->subDays($this->days), now()]);
        }

        $logs->whereHas('visit', function ($visit) {
            $this->applyVisitFilters($visit);
        });

        $logs = $this->applyLogFilters($logs);

        return $logs->simplePaginate($this->limit);
    }

    public function applyVisitFilters($visits)
    {

        if ($this->ip) {
            $visits->where('ip', $this->ip);
        }

        if ($this->userAgent) {
            $visits->where('userAgent', $this->userAgent);
        }

        if ($this->host) {
            $visits->where('host', 'like', "%{$this->host}%");
        }

        if ($this->ipRegexp) {
            $visits->where('ip', 'regexp', $this->ipRegexp);
        }

        if ($this->userAgentRegexp) {
            $visits->where('userAgent', 'regexp', $this->userAgentRegexp);
        }

        if ($this->filterNotRenewed) {
            $visits->whereHas('domainRelation', function ($domain) {
                $domain->where(function ($query) {
                    $query
                        ->where(function ($query) {
                            $query
                                ->whereNull('parent_domain_domain')
                                ->where('renewed', false);
                        })
                        ->orWhereRelation('parentDomain', 'renewed', false);
                });
            });
        }

        if ($this->filterOnlyRealBotIp) {

            $visits
            /* TODO: ограничиваем время 30 минутами, т.к. иначе запрос пиздецки долгий, сделать по уму */
                ->whereBetween('date', [now()->subMinutes(20), now()])
                ->whereRelation('ipRelation', 'is_real_bot', true);
            // ->join('engine.ips', 'visits.ip', 'ips.ip')
            // ->where('ips.is_real_bot', true);
        }

        if ($this->filterHasBan) {
            $visits->hasBan();
        }

        if ($this->ban) {
            $visits->ban($this->ban);
        }

        return $visits;
    }

    public function applyLogFilters($logs)
    {

        if ($this->filterLogText) {

            if (! isset($this->logTexts[$this->filterLogText])) {
                throw new \Exception("Текст лога \"{$this->filterLogText}\" отсутствует в logTexts");
            }

            $logs->where('text', 'like', $this->logTexts[$this->filterLogText]);
        }

        if ($this->filterErrors) {
            $logs
                ->where('type', 'ERROR')
                ->whereNotIn('text', ['Favicon.ico не указан для сайта', 'Показали страницу 404', 'Route was not recognized']);
        }

        return $logs;
    }

    /* не используется */
    public function fakePlugs($logs)
    {
        return $logs
            ->where('logs.text', 'like', 'Показали фейковую заглушку%');
        // ->whereHas('visit', function($query){
        //     $query
        //     ->where('query', 'not like', '%favicon.ico%')
        //     ->where('query', 'not like', '%wlwmanifest.xml%')
        //     ->where('query', 'not like', '%robots.txt%')
        //     ->where('query', 'not like', '%.env%')
        //     ->whereNull('referer')
        //     ->where('userAgent', 'not like', '%GuzzleHttp%')
        //     ->where('userAgent', 'not like', '%Bytespider%')
        //     ->where('userAgent', 'not like', '%keys-so-bot%')
        //     ->where('userAgent', 'not like', '%DotBot%')
        //     ->where('userAgent', 'not like', '%python-requests%');
        // });
    }

    public function createBan()
    {
        $ip = $this->createBanForm['ip'] ?? throw new \Exception('Specify IP');
        $hide_player = $this->createBanForm['hide_player'] ?? false;
        $active = $this->createBanForm['active'] ?? false;
        $description = $this->createBanForm['description'] ?? throw new \Exception('Description required');
        $ban = new Ban;
        $ban->ip = $ip;
        $ban->hide_player = $hide_player;
        $ban->active = $active;
        $ban->description = $description;
        $ban->save();
    }

    public function deleteBan()
    {
        $this->ipBan->delete();
        /* иначе изменения видно только после перезагрузки страницы, лень разбираться почему */
        unset($this->ipBan);
    }

    public function updated($property, $value)
    {
        if ($property == 'filterLogText') {
            $this->filterErrors = false;
        }
        if ($property == 'filterErrors') {
            $this->filterLogText = null;
        }
    }
}
