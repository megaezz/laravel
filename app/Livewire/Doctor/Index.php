<?php

namespace App\Livewire\Doctor;

use App\Models\Doctor\City;
use App\Models\Doctor\Clinic;
use App\Models\Doctor\ClinicType;
use App\Models\Doctor\Doctor;
use App\Models\Doctor\DoctorSpecialty;
use App\Models\Doctor\Review;
use App\Models\Doctor\Service;
use Illuminate\Http\Request;
use Livewire\Attributes\Computed;
use Livewire\Component;

class Index extends Component
{
    public $reviewsCount;

    public $doctorsCount;

    public $clinicsCount;

    public $showCityPopup = false;

    public $cities;

    public $cityId;

    public City $city;

    public $showDropDown = false;

    public $dropDownCitySearch;

    public $specialties;

    public $specialtyId;

    public DoctorSpecialty $specialty;

    public $showDropDownSpecialties = false;

    public $dropDownSpecialtySearch;

    /* показывать нижние блоки: врачи, клиники, услуги */
    public $showCityEntities = false;

    public function render()
    {
        return view('livewire.doctor.index');
    }

    public function mount(Request $request)
    {
        $this->reviewsCount = Review::cacheFor(now()->addDays(1))->count();
        $this->doctorsCount = Doctor::cacheFor(now()->addDays(1))->count();
        $this->clinicsCount = Clinic::cacheFor(now()->addDays(1))->count();
        $this->cities = City::select('id', 'name')->get();
        /* TODO: сделать чтобы зависило от города */
        $this->specialties = DoctorSpecialty::select('id', 'name')->get();
        $this->cityId = $request->session()->get('cityId');
    }

    public function updated($property, $value)
    {

        if ($property == 'cityId') {
            $city = City::where('id', $value)->first();
            if (! $city) {
                return $this->js("alert('Такого городка нет вообще-то')");
            }
            session()->put('cityId', $value);
            $this->city = $city;
            $this->showCityPopup = false;
        }

        if ($property == 'specialtyId') {
            $specialty = DoctorSpecialty::where('id', $value)->first();
            if (! $specialty) {
                return $this->js("alert('Такого специализации нет вообще-то')");
            }
            $this->specialty = $specialty;
            $this->showDropDownSpecialties = false;
            /* чтобы не оставались буквы в форме поиска */
            $this->dropDownSpecialtySearch = null;
        }
    }

    #[Computed]
    public function dropDownCities()
    {
        if (! $this->dropDownCitySearch or mb_strwidth($this->dropDownCitySearch) < 1) {
            return $this->cities;
        }

        return $this->cities->filter(function ($city) {
            return mb_stripos($city->name, $this->dropDownCitySearch) !== false;
        });
    }

    #[Computed]
    public function dropDownSpecialties()
    {
        if (! $this->dropDownSpecialtySearch or mb_strwidth($this->dropDownSpecialtySearch) < 1) {
            return $this->specialties;
        }

        return $this->specialties->filter(function ($specialty) {
            return mb_stripos($specialty->name, $this->dropDownSpecialtySearch) !== false;
        });
    }

    #[Computed]
    public function cityDoctorSpecialties()
    {
        /* выбираем специализации в которых есть доктора у которых есть клиники в данном городе */
        return DoctorSpecialty::query()
            ->whereRelation('doctors.clinics', 'city_id', $this->city->id)
        /* считаем для каждой специализации колчество докторов у которых есть клиники в данном городе */
            ->withCount(['doctors' => function ($query) {
                $query->whereRelation('clinics', 'city_id', $this->city->id);
            }])
            ->orderByDesc('doctors_count')
            ->limit(8)
            ->cacheFor(now()->addDays(1))
            ->get();
    }

    #[Computed]
    public function cityClinicTypes()
    {
        /* выбираем типы клиник для которых есть клиники в данном городе */
        return ClinicType::query()
            ->whereRelation('clinics', 'city_id', $this->city->id)
        /* считаем для каждого типа клиник количество клиник, которые есть в данном городе */
            ->withCount(['clinics' => function ($query) {
                $query->where('city_id', $this->city->id);
            }])
            ->orderByDesc('clinics_count')
            ->limit(8)
            ->cacheFor(now()->addDays(1))
            ->get();
    }

    #[Computed]
    public function cityServices()
    {

        /* TODO: убрать data и data_wide из кеша */
        $key = json_encode([__METHOD__, 'city' => $this->city->id]);

        return \Cache::remember($key, now()->addDay(), function () {
            /* выбираем услуги, которые есть в клиниках данного города */
            $services = Service::with('parentServiceRecursive')
                ->whereRelation('clinicServices.clinic', 'city_id', $this->city->id)
                ->get();

            /* группируем сервисы по родителю */
            $groupedByParent = $services->groupBy(function ($service) {
                return $service->rootService->id;
            });

            /* формируем как надо */
            $parentServices = [];
            foreach ($groupedByParent as $childrens) {
                $parentServices[] = (object) [
                    /* получаем родительскую модель у любого чилдрена и преобразовываем в stdClass, чтобы убрались hidden поля */
                    'parent' => json_decode($childrens->first()->rootService->toJson()),
                    // нужна только сумма
                    // 'childrens' => $childrens
                ];
            }

            // dd($parentServices);

            return collect($parentServices)->take(8);
        });
    }
}
