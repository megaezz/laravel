<?php

namespace App\Livewire\Pulse;

use Laravel\Pulse\Livewire\Card;
use Livewire\Attributes\Lazy;

#[Lazy]
class LatestLogs extends Card
{
    public function render()
    {
        return view('livewire.pulse.latest-logs');
    }
}
