<?php

namespace App\Orchid\Screens\Transfer;

use App\Models\Product;
use App\Models\Transfer;
use App\Models\User;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class TransferListScreen extends Screen
{
    public $permission = 'platform.systems.payments';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'transfers' => Transfer::with('deposit')
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Transfers';
    }

    /**
     * Display header description.
     */
    public function description(): ?string
    {
        return 'Transfers';
    }

    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus'),
            // ->href(route('platform.payments.create')),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('transfers', [

                TD::make('id', __('ID'))
                    ->sort()
                    ->filter(Input::make()),

                TD::make('amount', __('Amount'))
                    ->sort()
                    ->filter(Input::make())
                    ->render(function (Transfer $transfer) {
                        return $transfer->deposit->amount.'$';
                    }),

                TD::make('from', __('From'))
                    ->render(function (Transfer $transfer) {
                        // dd($transfer->from);
                        $from = $transfer->from->holder;
                        if (is_a($from, User::class)) {
                            return $from->telegram;
                        }
                        if (is_a($from, Product::class)) {
                            return $from->title;
                        }

                        return 'unknown';
                    }),

                TD::make('to', __('To'))
                    ->render(function (Transfer $transfer) {
                        // dd($transfer->to);
                        $to = $transfer->to;
                        if (is_a($to, User::class)) {
                            return $to->telegram;
                        }
                        if (is_a($to, Product::class)) {
                            return $to->title;
                        }

                        return 'unknown';
                    }),

                TD::make('created_at', __('Created'))
                    ->sort()
                    ->filter(Input::make())
                    ->render(function (Transfer $transfer) {
                        return $transfer->created_at->toDateTimeString();
                    }),

                TD::make(__('Actions'))
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Transfer $transfer) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([
                                Link::make(__('Edit'))
                                    // ->route('platform.payments.edit', $transaction->id)
                                    ->icon('pencil'),

                            ]);
                    }),
            ]),
        ];
    }
}
