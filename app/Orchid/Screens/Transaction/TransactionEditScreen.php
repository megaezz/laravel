<?php

namespace App\Orchid\Screens\Transaction;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\RadioButtons;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class TransactionEditScreen extends Screen
{
    public $permission = 'platform.systems.transactions';

    public $transaction;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Transaction $transaction): iterable
    {
        return [
            'transaction' => $transaction,
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Create transaction';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Save')
                ->icon('check')
                ->method('save'),

            Button::make(__('Remove'))
                ->icon('trash')
                ->method('remove')
                ->confirm(__('You shure want to delete this transaction?'))
                ->canSee($this->transaction->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                RadioButtons::make('action')
                    ->options([
                        'deposit' => 'Deposit',
                        'withdraw' => 'Withdraw',
                    ])
                    ->help('Choose an action'),

                Input::make('transaction.amount')
                    ->title('Amount')
                    ->required()
                    ->placeholder('Amount'),

                Input::make('transaction.meta.comment')
                    ->title('Comment')
                    ->required()
                    ->placeholder('Comment'),

                Input::make('transaction.meta.txid')
                    ->title('Txid')
                    ->placeholder('Txid'),

                Relation::make('user_id')
                    ->fromModel(User::class, 'telegram')
                    ->title(__('User'))
                    ->required(),
            ]),
        ];
    }

    public function save(Request $request, Transaction $transaction)
    {
        $request->validate([
            'action' => 'required',
            'transaction.meta.comment' => 'required',
            'transaction.amount' => 'numeric|required',
            'user_id' => 'exists:users,id|numeric|required',
        ]);

        $user = User::findOrFail($request->get('user_id'));
        if ($request->get('action') == 'deposit') {
            $user->deposit($request->get('transaction')['amount'], $request->get('transaction')['meta']);
            Toast::info('Deposit has been added successfully');
        }
        if ($request->get('action') == 'withdraw') {
            try {
                $user->withdraw($request->get('transaction')['amount'], $request->get('transaction')['meta']);
            } catch (\Exception $e) {
                return back()->withErrors(['Wallet balance is too low']);
            }
            Toast::info('Withdraw has been added successfully');
        }

        return redirect()->route('platform.transactions.list');
    }

    public function remove(Transaction $transaction)
    {
        $transaction->delete();

        Toast::info(__('transaction has been removed'));

        return redirect()->route('platform.transactions.list');
    }
}
