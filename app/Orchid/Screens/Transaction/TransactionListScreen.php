<?php

namespace App\Orchid\Screens\Transaction;

use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Str;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class TransactionListScreen extends Screen
{
    public $permission = 'platform.systems.payments';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'transactions' => Transaction::filters()
                ->defaultSort('id', 'desc')
                ->paginate(),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Transactions';
    }

    /**
     * Display header description.
     */
    public function description(): ?string
    {
        return 'Transactions';
    }

    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->href(route('platform.transactions.create')),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('transactions', [

                TD::make('id', __('ID'))
                    ->sort()
                    ->filter(Input::make()),

                TD::make('amount', __('Amount'))
                    ->sort()
                    ->filter(Input::make())
                    ->render(function (Transaction $transaction) {
                        return $transaction->amount.'$';
                    }),

                TD::make('entity', __('Entity'))
                    ->render(function (Transaction $transaction) {
                        $holder = $transaction->wallet->holder;
                        if (is_a($holder, User::class)) {
                            return $holder->telegram;
                        }
                        if (is_a($holder, Product::class)) {
                            return $holder->title;
                        }
                        throw new \Exception('Unknown entity');
                    }),

                TD::make('meta.txid', __('Txid'))
                    ->defaultHidden()
                    ->render(function (Transaction $transaction) {
                        return Str::limit($transaction->meta['txid'] ?? null, 30);
                    }),

                TD::make('comment', __('Comment'))
                    ->filter(Input::make())
                    ->render(function (Transaction $transaction) {
                        return $transaction->meta['comment'] ?? null;
                    }),

                TD::make('created_at', __('Created'))
                    ->sort()
                    ->filter(Input::make())
                    ->render(function (Transaction $transaction) {
                        return $transaction->created_at->toDateTimeString();
                    }),

                TD::make(__('Actions'))
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Transaction $transaction) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make('Edit')
                                    ->route('platform.transactions.edit', $transaction->id)
                                    ->icon('pencil'),

                            ]);
                    }),
            ]),
        ];
    }
}
