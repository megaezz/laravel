<?php

namespace App\Orchid\Screens\Task;

use App\Models\Task;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class TaskAvailableListScreen extends Screen
{
    public $permission = 'platform.index';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Request $request): iterable
    {
        return [
            'tasks' => $request->user()->availableTasks()->filters()->defaultSort('id', 'desc')->paginate(),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'All tasks';
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('tasks', [
                TD::make('title', __('Title'))
                    ->sort()
                    ->cantHide()
                    ->filter(Input::make())
                    ->render(function (Task $task) {
                        return Link::make($task->title)
                            ->route('platform.tasks.view', $task->id);
                    }),

                TD::make('created_at', __('Created'))
                    ->sort()
                    ->render(function (Task $task) {
                        return $task->created_at->toDateTimeString();
                    }),
            ]),
        ];
    }
}
