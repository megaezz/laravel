<?php

namespace App\Orchid\Screens\Task;

use App\Models\Task;
use App\Models\UserTask;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class TaskViewScreen extends Screen
{
    public $permission = 'platform.index';

    public $task;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Request $request, Task $task): iterable
    {
        if (! $request->user()->hasAccess('platform.systems.tasks')) {
            $task = $request->user()->availableTasks()->findOrFail($task->id);
        }

        return [
            'task' => $task,
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Task';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Start')
                ->icon('control-play')
                ->method('start'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::view('task.view'),
            Layout::view('task.fields'),
        ];

        return $layouts;
    }

    public function start(Request $request, Task $task)
    {
        $user_task = $request->user()->tasks()->whereTaskId($task->id)->first();
        if (! $user_task) {
            $user_task = new UserTask;
            $user_task->user()->associate($request->user());
            $user_task->task()->associate($task);
            $user_task->save();
        }

        return redirect()->route('platform.user_tasks.edit', ['user_task' => $user_task->id]);
    }
}
