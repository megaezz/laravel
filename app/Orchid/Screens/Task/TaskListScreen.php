<?php

declare(strict_types=1);

namespace App\Orchid\Screens\Task;

use App\Models\Task;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class TaskListScreen extends Screen
{
    public $permission = 'platform.systems.tasks';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'tasks' => Task::filters()->defaultSort('id', 'desc')->paginate(),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Manage tasks';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->href(route('platform.tasks.create')),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('tasks', [
                TD::make('title', __('Title'))
                    ->sort()
                    ->cantHide()
                    ->filter(Input::make())
                    ->render(function (Task $task) {
                        return Link::make($task->title)
                            ->route('platform.tasks.edit', $task->id);
                    }),

                TD::make('published', __('Published'))
                    ->render(function (Task $task) {
                        return $task->published ? '<i class="text-success">●</i>' : '<i class="text-danger">●</i>';
                    }),

                TD::make('created_at', __('Created'))
                    ->sort()
                    ->render(function (Task $task) {
                        return $task->created_at->toDateTimeString();
                    }),
            ]),
        ];
    }
}
