<?php

namespace App\Orchid\Screens\Task;

use App\Models\Task;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Matrix;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class TaskEditScreen extends Screen
{
    public $permission = 'platform.systems.tasks';

    public $task;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Task $task): iterable
    {
        return [
            'task' => $task,
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Edit task';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->method('remove')
                ->confirm(__('You shure want to delete this task?'))
                ->canSee($this->task->exists),

            Button::make('Preview')
                ->icon('eye')
                ->method('preview')
                ->canSee($this->task->exists),

            Button::make('Save')
                ->icon('check')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Switcher::make('task.published')
                    ->sendTrueOrFalse()
                    ->placeholder('Published'),

                Input::make('task.title')
                    ->title('Title')
                    ->required()
                    ->placeholder('Task title'),

                Quill::make('task.text')
                    ->title('Text')
                    ->required()
                    ->height('300px')
                    ->placeholder('Insert text of task here ...'),

                Matrix::make('task.fields')
                    ->columns([
                        __('Field name') => 'name',
                        __('Field type') => 'type',
                        __('Required') => 'required',
                        __('Description') => 'description',
                        'ID' => 'id',
                    ])
                    ->fields([
                        'id' => Input::make()->type('hidden'),
                        'name' => Input::make()->type('text')->required(),
                        'type' => Select::make()
                            ->required()
                            ->options([
                                'numeric' => __('Numeric'),
                                'string' => __('String'),
                                'boolean' => __('Boolean'),
                                'cropper' => __('Image'),
                            ]),
                        'required' => Switcher::make()
                            ->sendTrueOrFalse()
                            ->placeholder('Required'),
                        'description' => TextArea::make(),
                    ])
                    ->title('Fields'),
            ]),
        ];
    }

    public function save(Request $request, Task $task)
    {
        // dd($request);
        // dd($task->fields()->get());
        $request->validate([
            'task.id' => 'numeric',
            'task.title' => 'required|min:6|max:200',
            'task.text' => 'required|min:10',
            'task.fields' => 'required',
        ]);

        $task->fill($request->get('task'));
        $task->save();

        // удаляем ненужные поля
        foreach ($task->fields as $taskField) {
            $exists = false;
            foreach ($request->get('task')['fields'] as $getField) {
                if ($taskField->id == $getField['id']) {
                    $exists = true;
                }
            }
            if (! $exists) {
                try {
                    $taskField->delete();
                } catch (\Exception $exception) {
                    Toast::error('Can not remove field "'.$taskField->name.'", probably somebody already completed this task.');

                    return redirect()->route('platform.tasks.edit', ['task' => $task->id]);
                }
            }
        }
        // создаем или обновляем переданные поля
        foreach ($request->get('task')['fields'] as $field) {
            if ($field['id']) {
                $task->fields()->find($field['id'])->fill($field)->save();
            } else {
                $task->fields()->create($field);
            }
        }

        Toast::info(__('Task has been saved successfully'));

        return redirect()->route('platform.tasks.edit', ['task' => $task->id]);
    }

    public function remove(Task $task)
    {
        try {
            $task->delete();
            Toast::info(__('Task has been removed'));
        } catch (\Exception $exception) {
            Toast::error('Can not delete this task. Probably somebody has already completed it.');

            return redirect()->route('platform.tasks.edit', ['task' => $task->id]);
        }

        return redirect()->route('platform.tasks.list');
    }

    public function preview(Task $task)
    {
        return redirect()->route('platform.tasks.view', ['task' => $task]);
    }
}
