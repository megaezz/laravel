<?php

namespace App\Orchid\Screens;

use App\Models\Payment;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class MainScreen extends Screen
{
    public $permission = 'platform.index';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Request $request): iterable
    {
        return [
            'metrics' => [
                'balance' => [
                    'value' => '$'.$request->user()->balance,
                ],
                'users' => [
                    'value' => User::count(),
                ],
                'tasks' => [
                    'value' => Task::count(),
                ],
                'payments' => [
                    'value' => Payment::count(),
                    'diff' => 0,
                ],
                'earnings' => [
                    'value' => '$'.Payment::sum('value'),
                    'diff' => 0,
                ],
            ],
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Get started';
    }

    /**
     * Display header description.
     */
    public function description(): ?string
    {
        return 'Welcome to Crypto.lol platform.';
    }

    public function layout(): iterable
    {
        return [
            Layout::metrics([
                'Balance' => 'metrics.balance',
                'Users' => 'metrics.users',
                'Tasks' => 'metrics.tasks',
                'Payments' => 'metrics.payments',
                'Total Earnings' => 'metrics.earnings',
            ]),
            // Layout::view('platform::partials.welcome'),
        ];
    }
}
