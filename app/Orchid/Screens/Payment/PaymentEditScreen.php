<?php

namespace App\Orchid\Screens\Payment;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PaymentEditScreen extends Screen
{
    public $permission = 'platform.systems.payments';

    public $payment;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Payment $payment): iterable
    {
        return [
            'payment' => $payment,
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Edit payment';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Save')
                ->icon('check')
                ->method('save'),

            Button::make(__('Remove'))
                ->icon('trash')
                ->method('remove')
                ->confirm(__('You shure want to delete this payment?'))
                ->canSee($this->payment->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('payment.value')
                    ->title('Amount')
                    ->required()
                    ->placeholder('Amount'),

                Input::make('payment.comment')
                    ->title('Comment')
                    ->required()
                    ->placeholder('Comment'),

                Input::make('payment.txid')
                    ->title('Txid')
                    ->placeholder('Txid'),

                Relation::make('payment.user_id')
                    ->fromModel(User::class, 'telegram')
                    ->title(__('User'))
                    ->required(),
            ]),
        ];
    }

    public function save(Request $request, Payment $payment)
    {
        $request->validate([
            'payment.comment' => 'required',
            'payment.value' => 'numeric|required',
            'payment.user_id' => 'exists:users,id|required',
        ]);

        $payment->fill($request->get('payment'));
        $payment->save();

        Toast::info('Payment has been saved successfully.');

        return redirect()->route('platform.payments.list');
    }

    public function remove(Payment $payment)
    {
        $payment->delete();

        Toast::info(__('Payment has been removed'));

        return redirect()->route('platform.payments.list');
    }
}
