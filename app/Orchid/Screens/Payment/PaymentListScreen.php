<?php

namespace App\Orchid\Screens\Payment;

use App\Models\Payment;
use Illuminate\Support\Str;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class PaymentListScreen extends Screen
{
    public $permission = 'platform.systems.payments';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'payments' => Payment::with('user')
                ->filters()
                ->defaultSort('id', 'desc')
                ->paginate(),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Users payments';
    }

    /**
     * Display header description.
     */
    public function description(): ?string
    {
        return 'Manage of payments';
    }

    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->href(route('platform.payments.create')),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('payments', [

                TD::make('id', __('ID'))
                    ->sort()
                    ->filter(Input::make())
                    ->render(function (Payment $payment) {
                        return $payment->id;
                    }),

                TD::make('value', __('Amount'))
                    ->sort()
                    ->filter(Input::make())
                    ->render(function (Payment $payment) {
                        return $payment->value;
                    }),

                TD::make('user.telegram', __('User'))
                    ->render(function (Payment $payment) {
                        return $payment->user->telegram;
                    }),

                TD::make('payment.txid', __('Txid'))
                    ->defaultHidden()
                    ->render(function (Payment $payment) {
                        return Str::limit($payment->txid, 30);
                    }),

                TD::make('comment', __('Comment'))
                    ->filter(Input::make())
                    ->render(function (Payment $payment) {
                        return $payment->comment;
                    }),

                TD::make('created_at', __('Created'))
                    ->sort()
                    ->filter(Input::make())
                    ->render(function (Payment $payment) {
                        return $payment->created_at->toDateTimeString();
                    }),

                TD::make(__('Actions'))
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Payment $payment) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.payments.edit', $payment->id)
                                    ->icon('pencil'),

                            ]);
                    }),
            ]),
        ];
    }
}
