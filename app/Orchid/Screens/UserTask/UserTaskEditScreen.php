<?php

namespace App\Orchid\Screens\UserTask;

use App\Models\UserTask;
use App\Models\UserTaskField;
use App\Orchid\Layouts\UserTask\UserTaskFieldsLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class UserTaskEditScreen extends Screen
{
    public $permission = 'platform.index';

    public $user_task;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Request $request, UserTask $user_task): iterable
    {
        $user_task = $request->user()->tasks()->findOrFail($user_task->id);

        return [
            'user_task' => $user_task,
            'user_task_fields' => $user_task->fields->keyBy('task_field_id'),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Task';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Refuse')
                ->icon('close')
                ->method('remove'),

            Button::make('Send to check')
                ->icon('control-play')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::view('task.view', ['task' => $this->user_task->task]),
            UserTaskFieldsLayout::class,
        ];
    }

    public function save(Request $request, UserTask $user_task)
    {
        $request->validate([
            'user_task_fields' => 'required',
        ]);

        // проверяем принадлежит ли задание пользователю
        $user_task = $request->user()->tasks()->findOrFail($user_task->id);

        // перебираем поля задания и смотрим, есть ли они в переданном массиве
        foreach ($user_task->task->fields as $field) {
            // если есть, то проверяем, есть ли уже созданное значение с этим полем, если есть - изменяем его, нет - создаем
            if (isset($request->get('user_task_fields')[$field->id])) {
                $userTaskField = $user_task->fields()->whereTaskFieldId($field->id)->first();
                if (! $userTaskField) {
                    $userTaskField = new UserTaskField;
                    $userTaskField->userTask()->associate($user_task);
                    $userTaskField->taskField()->associate($field);
                }
                $userTaskField->value = $request->get('user_task_fields')[$field->id]['value'] ?? null;
                $userTaskField->save();
            }
        }

        Toast::success(__('Task has been saved successfully'));

        return redirect()->route('platform.user_tasks.edit', ['user_task' => $user_task->id]);
    }

    public function remove(Request $request, UserTask $user_task)
    {
        $user_task = $request->user()->tasks()->find($user_task->id);
        if ($user_task->completed) {
            Toast::error('Can not delete completed task');

            return back();
        }
        $user_task->delete();
        Toast::success(__('Task has been deleted'));

        return redirect()->route('platform.user_tasks.list');
    }
}
