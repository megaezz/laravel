<?php

namespace App\Orchid\Screens\UserTask;

use App\Models\UserTask;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class UserTaskListScreen extends Screen
{
    public $permission = 'platform.index';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Request $request): iterable
    {
        return [
            'user_tasks' => $request->user()->tasks()->filters()->defaultSort('id', 'desc')->paginate(),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'My tasks';
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('user_tasks', [
                TD::make('title', __('Title'))
                    ->sort()
                    ->cantHide()
                    ->filter(Input::make())
                    ->render(function (UserTask $user_task) {
                        return Link::make($user_task->task->title)
                            ->route('platform.user_tasks.edit', $user_task->id);
                    }),

                TD::make('status', __('Status'))
                    ->sort()
                    ->cantHide()
                    ->filter(Input::make())
                    ->render(function (UserTask $user_task) {
                        if ($user_task->status == 'accepted') {
                            return '<i class="text-success">●</i> '.__('Accepted');
                        }
                        if ($user_task->status == 'declined') {
                            return '<i class="text-danger">●</i> '.__('Declined');
                        }
                        if ($user_task->status == 'correction') {
                            return '<i class="text-warning">●</i> '.__('Correction');
                        }

                        return null;
                    }),

                TD::make('created_at', __('Created'))
                    ->sort()
                    ->render(function (UserTask $user_task) {
                        return $user_task->task->created_at->toDateTimeString();
                    }),
            ]),
        ];
    }
}
