<?php

namespace App\Orchid\Screens\UserTask\admin;

use App\Models\UserTask;
use App\Models\UserTaskField;
use App\Orchid\Layouts\UserTask\UserTaskFieldsLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class UserTaskEditScreen extends Screen
{
    public $permission = 'platform.systems.user_tasks';

    public $user_task;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Request $request, UserTask $user_task): iterable
    {
        $user_task = UserTask::findOrFail($user_task->id);

        return [
            'user_task' => $user_task,
            'user_task_fields' => $user_task->fields->keyBy('task_field_id'),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'Task';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Decline'))
                ->icon('close')
                ->method('decline')
                ->novalidate(),

            Button::make(__('Correct'))
                ->icon('pencil')
                ->method('correction')
                ->novalidate(),

            Button::make('Save')
                ->icon('save-alt')
                ->method('save'),

            Button::make('Accept')
                ->icon('check')
                ->method('accept'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::view('task.view', ['task' => $this->user_task->task]),
            UserTaskFieldsLayout::class,
        ];
    }

    public function save(Request $request, UserTask $user_task)
    {
        $request->validate([
            'user_task_fields' => 'required',
        ]);

        // перебираем поля задания и смотрим, есть ли они в переданном массиве
        foreach ($user_task->task->fields as $field) {
            // если есть, то проверяем, есть ли уже созданное значение с этим полем, если есть - изменяем его, нет - создаем
            if (isset($request->get('user_task_fields')[$field->id])) {
                $userTaskField = $user_task->fields()->whereTaskFieldId($field->id)->first();
                if (! $userTaskField) {
                    $userTaskField = new UserTaskField;
                    $userTaskField->userTask()->associate($user_task);
                    $userTaskField->taskField()->associate($field);
                }
                $userTaskField->value = $request->get('user_task_fields')[$field->id]['value'] ?? null;
                $userTaskField->save();
            }
        }

        Toast::success(__('Task has been saved successfully'));

        return back();
    }

    public function accept(UserTask $user_task)
    {
        $user_task->status = 'accepted';
        $user_task->save();
        Toast::success('Task has been accepted');

        return redirect()->route('platform.user_tasks.admin.list');
    }

    public function decline(UserTask $user_task)
    {
        $user_task->status = 'declined';
        $user_task->save();
        Toast::error('Task has been declined');

        return redirect()->route('platform.user_tasks.admin.list');
    }

    public function correction(UserTask $user_task)
    {
        $user_task->status = 'correction';
        $user_task->save();
        Toast::warning('Task has been sent to correction');

        return redirect()->route('platform.user_tasks.admin.list');
    }
}
