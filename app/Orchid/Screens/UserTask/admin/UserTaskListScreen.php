<?php

namespace App\Orchid\Screens\UserTask\admin;

use App\Models\UserTask;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class UserTaskListScreen extends Screen
{
    public $permission = 'platform.systems.user_tasks';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'user_tasks' => UserTask::completed()->onCorrection(false)->declined(false)->filters()->defaultSort('id', 'desc')->paginate(),
        ];
    }

    /**
     * Display header name.
     */
    public function name(): ?string
    {
        return 'UserTaskListScreen';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('user_tasks', [
                TD::make('task.title', __('Title'))
                    ->sort()
                    ->cantHide()
                    ->filter(Input::make())
                    ->render(function (UserTask $user_task) {
                        return Link::make($user_task->task->title)
                            ->route('platform.user_tasks.admin.edit', $user_task->id);
                    }),

                TD::make('user.name', __('User'))
                    ->cantHide()
                    ->filter(Input::make())
                    ->render(function (UserTask $user_task) {
                        return Link::make($user_task->user->name)
                            ->route('platform.systems.users.edit', $user_task->user->id);
                    }),

                TD::make('created_at', __('Created'))
                    ->sort()
                    ->render(function (UserTask $user_task) {
                        return $user_task->created_at->toDateTimeString();
                    }),
            ]),
        ];
    }
}
