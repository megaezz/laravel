<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class UserSocialsLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('user.telegram')
                ->type('text')
                ->title('Telegram')
                ->placeholder('Telegram'),

            Input::make('user.twitter')
                ->type('text')
                ->title('Twitter')
                ->placeholder('Twitter'),

            Input::make('user.discord')
                ->type('text')
                ->title('Discord')
                ->placeholder('Discord'),
        ];
    }
}
