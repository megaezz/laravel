<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Layouts\Rows;

class UserSubscriptionLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            DateTimer::make('user.subscription_end_date')
                ->title('Subscription ends'),

            Switcher::make('user.telegram_access')
                ->sendTrueOrFalse()
                ->placeholder('Telegram access'),
        ];
    }
}
