<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class UserWalletsLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('user.usdt_trc20')
                ->type('text')
                ->title('USDT (TRC-20)')
                ->placeholder('USDT (TRC-20)'),

            Input::make('user.otc_wallet')
                ->type('text')
                ->title(__('OTC Wallet').' (ERC-20/BEP-20/Polygon/AVAX)')
                ->placeholder('Wallet'),

            Input::make('user.wallet_solana')
                ->type('text')
                ->title(__('OTC Wallet').' (Solana)')
                ->placeholder('Wallet'),
        ];
    }
}
