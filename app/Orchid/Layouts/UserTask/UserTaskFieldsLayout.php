<?php

namespace App\Orchid\Layouts\UserTask;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Switcher;
use Orchid\Screen\Layouts\Rows;

class UserTaskFieldsLayout extends Rows
{
    public function __construct()
    {
        $this->title = __('Checking data');
    }

    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        $fields = [];

        foreach ($this->query->get('user_task')->task->fields as $field) {
            if ($field->type == 'boolean') {
                $fields[] = Switcher::make("user_task_fields.$field->id.value")
                    ->required($field->required)
                    ->sendTrueOrFalse()
                    ->title($field->name)
                    ->help($field->description ? $field->description : '')
                    ->placeholder($field->name);
            }

            if ($field->type == 'string') {
                $fields[] = Input::make("user_task_fields.$field->id.value")
                    ->required($field->required)
                    ->title($field->name)
                    ->help($field->description ? $field->description : '');
            }

            if ($field->type == 'numeric') {
                $fields[] = Input::make("user_task_fields.$field->id.value")
                    ->required($field->required)
                    ->title($field->name)
                    ->help($field->description ? $field->description : '');
            }

            if ($field->type == 'cropper') {
                $fields[] = Picture::make("user_task_fields.$field->id.value")
                    ->required($field->required)
                    ->maxFileSize(2)
                    ->title($field->name)
                    ->help($field->description ? $field->description : '');
            }
        }

        return $fields;
    }
}
