<?php

declare(strict_types=1);

namespace App\Orchid;

use App\Models\Task;
use App\Models\UserTask;
use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [
            Menu::make('All tasks')
                ->icon('task')
                ->title('Tasks')
                ->route('platform.tasks.available.list')
                ->badge(function () {
                    return $this->app->auth->user()->availableTasks()->count();
                }),

            Menu::make('My tasks')
                ->icon('check')
                ->route('platform.user_tasks.list')
                ->badge(function () {
                    return $this->app->auth->user()->tasks()->count();
                }),

            Menu::make('Manage tasks')
                ->icon('task')
                ->permission('platform.systems.tasks')
                ->title('Administration')
                ->route('platform.tasks.list')
                ->badge(function () {
                    return Task::count();
                }),

            Menu::make('Check the tasks')
                ->icon('eyeglasses')
                ->route('platform.user_tasks.admin.list')
                ->permission('platform.systems.user_tasks')
                ->badge(function () {
                    return UserTask::completed(true)->onCorrection(false)->declined(false)->checked(false)->count();
                }),

            Menu::make(__('Users'))
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users'),

            Menu::make(__('Payments'))
                ->icon('credit-card')
                ->route('platform.payments.list')
                ->permission('platform.systems.payments'),

            Menu::make(__('Transactions'))
                ->icon('credit-card')
                ->route('platform.transactions.list')
                ->permission('platform.systems.transactions'),

            Menu::make(__('Transfers'))
                ->icon('credit-card')
                ->route('platform.transfers.list')
                ->permission('platform.systems.transfers'),

            Menu::make(__('Roles'))
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles')
                ->title(__('Access rights')),

            /*

            Menu::make('Example screen')
                ->icon('monitor')
                ->route('platform.example')
                ->title('Navigation')
                ->badge(function () {
                    return 6;
                }),

            Menu::make('Dropdown menu')
                ->icon('code')
                ->list([
                    Menu::make('Sub element item 1')->icon('bag'),
                    Menu::make('Sub element item 2')->icon('heart'),
                ]),

            Menu::make('Basic Elements')
                ->title('Form controls')
                ->icon('note')
                ->route('platform.example.fields'),

            Menu::make('Advanced Elements')
                ->icon('briefcase')
                ->route('platform.example.advanced'),

            Menu::make('Text Editors')
                ->icon('list')
                ->route('platform.example.editors'),

            Menu::make('Overview layouts')
                ->title('Layouts')
                ->icon('layers')
                ->route('platform.example.layouts'),

            Menu::make('Chart tools')
                ->icon('bar-chart')
                ->route('platform.example.charts'),

            Menu::make('Cards')
                ->icon('grid')
                ->route('platform.example.cards')
                ->divider(),

            Menu::make('Documentation')
                ->title('Docs')
                ->icon('docs')
                ->url('https://orchid.software/en/docs'),

            Menu::make('Changelog')
                ->icon('shuffle')
                ->url('https://github.com/orchidsoftware/platform/blob/master/CHANGELOG.md')
                ->target('_blank')
                ->badge(function () {
                    return Dashboard::version();
                }, Color::DARK()),
            */
        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users'))
                ->addPermission('platform.systems.tasks', __('Tasks'))
                ->addPermission('platform.systems.payments', __('Payments'))
                ->addPermission('platform.systems.transactions', __('Transactions'))
                ->addPermission('platform.systems.transfers', __('Transfers'))
                ->addPermission('platform.systems.user_tasks', __('User tasks')),
        ];
    }
}
