<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ShowcardResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ShowcardResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditShowcard extends EditRecord
{
    protected static string $resource = ShowcardResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
