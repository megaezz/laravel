<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ShowcardResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ShowcardResource;
use Filament\Resources\Pages\CreateRecord;

class CreateShowcard extends CreateRecord
{
    protected static string $resource = ShowcardResource::class;
}
