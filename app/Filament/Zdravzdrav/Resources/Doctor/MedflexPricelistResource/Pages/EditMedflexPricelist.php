<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\MedflexPricelistResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\MedflexPricelistResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMedflexPricelist extends EditRecord
{
    protected static string $resource = MedflexPricelistResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
