<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\MedflexPricelistResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\MedflexPricelistResource;
use Filament\Resources\Pages\CreateRecord;

class CreateMedflexPricelist extends CreateRecord
{
    protected static string $resource = MedflexPricelistResource::class;
}
