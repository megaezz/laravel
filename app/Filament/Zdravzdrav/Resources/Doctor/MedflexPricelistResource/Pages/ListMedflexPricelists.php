<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\MedflexPricelistResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\MedflexPricelistResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListMedflexPricelists extends ListRecords
{
    protected static string $resource = MedflexPricelistResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
