<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorResource\Pages;
use App\Models\Doctor\Doctor;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class DoctorResource extends Resource
{
    protected static ?string $model = Doctor::class;

    protected static ?string $navigationIcon = 'heroicon-o-user';

    protected static ?string $navigationGroup = 'Врачи';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('url')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('img')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('firstName')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('middleName')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('secondName')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('rank')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('degree')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('category')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('experienceStart')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('sex'),
                Forms\Components\Textarea::make('info')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('rating')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('bestCityCharId')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('doctu_id')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\Textarea::make('data')
                    ->columnSpanFull(),
                Forms\Components\Textarea::make('data_wide')
                    ->columnSpanFull(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('url')
                    ->searchable(),
                Tables\Columns\TextColumn::make('img')
                    ->searchable(),
                Tables\Columns\TextColumn::make('firstName')
                    ->searchable(),
                Tables\Columns\TextColumn::make('middleName')
                    ->searchable(),
                Tables\Columns\TextColumn::make('secondName')
                    ->searchable(),
                Tables\Columns\TextColumn::make('rank')
                    ->searchable(),
                Tables\Columns\TextColumn::make('degree')
                    ->searchable(),
                Tables\Columns\TextColumn::make('category')
                    ->searchable(),
                Tables\Columns\TextColumn::make('experienceStart')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('sex'),
                Tables\Columns\TextColumn::make('rating')
                    ->searchable(),
                Tables\Columns\TextColumn::make('bestCityCharId')
                    ->searchable(),
                Tables\Columns\TextColumn::make('doctu_id')
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDoctors::route('/'),
            'create' => Pages\CreateDoctor::route('/create'),
            'edit' => Pages\EditDoctor::route('/{record}/edit'),
        ];
    }
}
