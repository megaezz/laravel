<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctuPricelistResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctuPricelistResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDoctuPricelist extends EditRecord
{
    protected static string $resource = DoctuPricelistResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
