<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctuPricelistResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctuPricelistResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDoctuPricelist extends CreateRecord
{
    protected static string $resource = DoctuPricelistResource::class;
}
