<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctuPricelistResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctuPricelistResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListDoctuPricelists extends ListRecords
{
    protected static string $resource = DoctuPricelistResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
