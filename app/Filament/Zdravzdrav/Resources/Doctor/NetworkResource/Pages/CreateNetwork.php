<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\NetworkResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\NetworkResource;
use Filament\Resources\Pages\CreateRecord;

class CreateNetwork extends CreateRecord
{
    protected static string $resource = NetworkResource::class;
}
