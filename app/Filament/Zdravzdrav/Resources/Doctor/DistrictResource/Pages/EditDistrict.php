<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DistrictResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DistrictResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDistrict extends EditRecord
{
    protected static string $resource = DistrictResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
