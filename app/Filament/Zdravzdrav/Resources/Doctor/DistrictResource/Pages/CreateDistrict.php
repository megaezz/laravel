<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DistrictResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DistrictResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDistrict extends CreateRecord
{
    protected static string $resource = DistrictResource::class;
}
