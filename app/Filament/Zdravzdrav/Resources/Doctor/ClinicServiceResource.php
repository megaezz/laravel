<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor;

use App\Filament\Zdravzdrav\Resources\Doctor\ClinicServiceResource\Pages;
use App\Models\Doctor\ClinicService;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class ClinicServiceResource extends Resource
{
    protected static ?string $model = ClinicService::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationGroup = 'Клиники';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('doctu_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('clinic_id')
                    ->required()
                    ->numeric(),
                Forms\Components\TextInput::make('name')
                    ->maxLength(2000)
                    ->default(null),
                Forms\Components\Textarea::make('description')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('status')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('code')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('mixed_price')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('author_id')
                    ->maxLength(100)
                    ->default(null),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('doctu_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('clinic_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('status')
                    ->searchable(),
                Tables\Columns\TextColumn::make('code')
                    ->searchable(),
                Tables\Columns\TextColumn::make('mixed_price')
                    ->searchable(),
                Tables\Columns\TextColumn::make('author_id')
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListClinicServices::route('/'),
            'create' => Pages\CreateClinicService::route('/create'),
            'edit' => Pages\EditClinicService::route('/{record}/edit'),
        ];
    }
}
