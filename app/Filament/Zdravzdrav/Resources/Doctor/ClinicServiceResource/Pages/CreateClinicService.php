<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ClinicServiceResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ClinicServiceResource;
use Filament\Resources\Pages\CreateRecord;

class CreateClinicService extends CreateRecord
{
    protected static string $resource = ClinicServiceResource::class;
}
