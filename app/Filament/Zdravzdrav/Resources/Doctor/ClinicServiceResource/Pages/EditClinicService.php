<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ClinicServiceResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ClinicServiceResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditClinicService extends EditRecord
{
    protected static string $resource = ClinicServiceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
