<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\UserResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\UserResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListUsers extends ListRecords
{
    protected static string $resource = UserResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
