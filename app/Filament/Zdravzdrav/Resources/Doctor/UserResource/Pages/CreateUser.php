<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\UserResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\UserResource;
use Filament\Resources\Pages\CreateRecord;

class CreateUser extends CreateRecord
{
    protected static string $resource = UserResource::class;
}
