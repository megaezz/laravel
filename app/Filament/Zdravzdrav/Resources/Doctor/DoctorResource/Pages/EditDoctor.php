<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctorResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDoctor extends EditRecord
{
    protected static string $resource = DoctorResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
