<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctorResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDoctor extends CreateRecord
{
    protected static string $resource = DoctorResource::class;
}
