<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctorSpecialtyResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorSpecialtyResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListDoctorSpecialties extends ListRecords
{
    protected static string $resource = DoctorSpecialtyResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
