<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctorSpecialtyResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorSpecialtyResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDoctorSpecialty extends EditRecord
{
    protected static string $resource = DoctorSpecialtyResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
