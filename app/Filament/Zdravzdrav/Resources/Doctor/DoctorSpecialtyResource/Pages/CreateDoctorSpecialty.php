<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctorSpecialtyResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorSpecialtyResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDoctorSpecialty extends CreateRecord
{
    protected static string $resource = DoctorSpecialtyResource::class;
}
