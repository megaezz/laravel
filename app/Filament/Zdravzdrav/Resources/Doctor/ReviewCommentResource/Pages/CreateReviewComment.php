<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ReviewCommentResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ReviewCommentResource;
use Filament\Resources\Pages\CreateRecord;

class CreateReviewComment extends CreateRecord
{
    protected static string $resource = ReviewCommentResource::class;
}
