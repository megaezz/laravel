<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ReviewCommentResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ReviewCommentResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditReviewComment extends EditRecord
{
    protected static string $resource = ReviewCommentResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
