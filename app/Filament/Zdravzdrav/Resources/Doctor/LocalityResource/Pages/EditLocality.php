<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\LocalityResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\LocalityResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditLocality extends EditRecord
{
    protected static string $resource = LocalityResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
