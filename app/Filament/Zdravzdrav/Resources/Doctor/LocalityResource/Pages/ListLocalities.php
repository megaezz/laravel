<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\LocalityResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\LocalityResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListLocalities extends ListRecords
{
    protected static string $resource = LocalityResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
