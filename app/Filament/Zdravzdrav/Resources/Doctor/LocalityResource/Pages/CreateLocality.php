<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\LocalityResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\LocalityResource;
use Filament\Resources\Pages\CreateRecord;

class CreateLocality extends CreateRecord
{
    protected static string $resource = LocalityResource::class;
}
