<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\AreaResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\AreaResource;
use Filament\Resources\Pages\CreateRecord;

class CreateArea extends CreateRecord
{
    protected static string $resource = AreaResource::class;
}
