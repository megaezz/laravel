<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\AreaResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\AreaResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditArea extends EditRecord
{
    protected static string $resource = AreaResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
