<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\MapspamResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\MapspamResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListMapspams extends ListRecords
{
    protected static string $resource = MapspamResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
