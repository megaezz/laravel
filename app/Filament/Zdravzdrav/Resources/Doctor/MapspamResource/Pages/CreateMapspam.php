<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\MapspamResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\MapspamResource;
use Filament\Resources\Pages\CreateRecord;

class CreateMapspam extends CreateRecord
{
    protected static string $resource = MapspamResource::class;
}
