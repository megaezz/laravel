<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ReviewResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ReviewResource;
use Filament\Resources\Pages\CreateRecord;

class CreateReview extends CreateRecord
{
    protected static string $resource = ReviewResource::class;
}
