<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ReviewResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ReviewResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditReview extends EditRecord
{
    protected static string $resource = ReviewResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
