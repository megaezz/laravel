<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\CityResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\CityResource;
use Filament\Resources\Pages\CreateRecord;

class CreateCity extends CreateRecord
{
    protected static string $resource = CityResource::class;
}
