<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ServiceResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ServiceResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListServices extends ListRecords
{
    protected static string $resource = ServiceResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
