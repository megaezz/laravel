<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ServiceResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ServiceResource;
use Filament\Resources\Pages\CreateRecord;

class CreateService extends CreateRecord
{
    protected static string $resource = ServiceResource::class;
}
