<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ClinicTypeResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ClinicTypeResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListClinicTypes extends ListRecords
{
    protected static string $resource = ClinicTypeResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
