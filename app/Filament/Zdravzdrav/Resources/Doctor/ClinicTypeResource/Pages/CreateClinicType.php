<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ClinicTypeResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ClinicTypeResource;
use Filament\Resources\Pages\CreateRecord;

class CreateClinicType extends CreateRecord
{
    protected static string $resource = ClinicTypeResource::class;
}
