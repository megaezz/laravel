<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ClinicResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\ClinicResource;
use Filament\Resources\Pages\CreateRecord;

class CreateClinic extends CreateRecord
{
    protected static string $resource = ClinicResource::class;
}
