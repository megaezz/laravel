<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\ClinicResource\Pages;

use App\Filament\Exports\Doctor\ClinicExporter;
use App\Filament\Zdravzdrav\Resources\Doctor\ClinicResource;
use Filament\Actions;
use Filament\Actions\ExportAction;
use Filament\Resources\Pages\ListRecords;

class ListClinics extends ListRecords
{
    protected static string $resource = ClinicResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            ExportAction::make()
                ->label('Экспорт')
                ->icon('heroicon-o-arrow-down-tray')
                ->exporter(ClinicExporter::class)
                ->chunkSize(1000),
        ];
    }
}
