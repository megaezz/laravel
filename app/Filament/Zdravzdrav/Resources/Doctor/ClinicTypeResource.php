<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor;

use App\Filament\Zdravzdrav\Resources\Doctor\ClinicTypeResource\Pages;
use App\Models\Doctor\ClinicType;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class ClinicTypeResource extends Resource
{
    protected static ?string $model = ClinicType::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $navigationGroup = 'Клиники';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('doctu_id')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('name')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('name_plural')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('proprietary')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\Textarea::make('data')
                    ->columnSpanFull(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('doctu_id')
                    ->searchable(),
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('name_plural')
                    ->searchable(),
                Tables\Columns\TextColumn::make('proprietary')
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListClinicTypes::route('/'),
            'create' => Pages\CreateClinicType::route('/create'),
            'edit' => Pages\EditClinicType::route('/{record}/edit'),
        ];
    }
}
