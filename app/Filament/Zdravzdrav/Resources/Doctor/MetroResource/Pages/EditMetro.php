<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\MetroResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\MetroResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMetro extends EditRecord
{
    protected static string $resource = MetroResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
