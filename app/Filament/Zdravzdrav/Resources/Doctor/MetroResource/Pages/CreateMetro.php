<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\MetroResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\MetroResource;
use Filament\Resources\Pages\CreateRecord;

class CreateMetro extends CreateRecord
{
    protected static string $resource = MetroResource::class;
}
