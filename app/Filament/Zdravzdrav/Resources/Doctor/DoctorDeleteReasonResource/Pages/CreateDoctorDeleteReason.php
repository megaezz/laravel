<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctorDeleteReasonResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorDeleteReasonResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDoctorDeleteReason extends CreateRecord
{
    protected static string $resource = DoctorDeleteReasonResource::class;
}
