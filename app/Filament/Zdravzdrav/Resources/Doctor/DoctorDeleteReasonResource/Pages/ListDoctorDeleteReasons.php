<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctorDeleteReasonResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorDeleteReasonResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListDoctorDeleteReasons extends ListRecords
{
    protected static string $resource = DoctorDeleteReasonResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
