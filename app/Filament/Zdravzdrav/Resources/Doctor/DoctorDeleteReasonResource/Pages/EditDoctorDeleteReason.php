<?php

namespace App\Filament\Zdravzdrav\Resources\Doctor\DoctorDeleteReasonResource\Pages;

use App\Filament\Zdravzdrav\Resources\Doctor\DoctorDeleteReasonResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDoctorDeleteReason extends EditRecord
{
    protected static string $resource = DoctorDeleteReasonResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
