<?php

namespace App\Filament\Zdravzdrav\Pages;

use Filament\Pages\Page;

class Dashboard extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-home';

    protected static string $view = 'filament.zdravzdrav.pages.dashboard';

    public function getTitle(): string
    {
        return ___('дашборд')->ucfirst();
    }

    public static function getNavigationLabel(): string
    {
        return ___('дашборд')->ucfirst();
    }

    protected function getHeaderWidgets(): array
    {
        return [

        ];
    }
}
