<?php

namespace App\Filament\Awmzone\Pages;

use App\Filament\Awmzone\Widgets\LatestLogs;
use App\Filament\Awmzone\Widgets\StatsOverview;
use Filament\Pages\Page;
use Illuminate\Support\Facades\Auth;

class Dashboard extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-home';

    protected static string $view = 'filament.pages.dashboard';

    public function getTitle(): string
    {
        return ___('дашборд')->ucfirst();
    }

    public static function getNavigationLabel(): string
    {
        return ___('дашборд')->ucfirst();
    }

    protected function getHeaderWidgets(): array
    {
        return [
            StatsOverview::class,
            LatestLogs::class,
        ];
    }

    public static function canAccess(): bool
    {
        return Auth::user()->can('view awmzone dashboard page');
    }
}
