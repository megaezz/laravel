<?php

namespace App\Filament\Awmzone\Pages;

use engine\app\models\Engine;
use Filament\Pages\Page;
use Illuminate\Support\Facades\Auth;

class Images extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-photo';

    protected static string $view = 'filament.awmzone.pages.images';

    public static function getNavigationGroup(): ?string
    {
        return ___('кино')->ucfirst();
    }

    public function getTitle(): string
    {
        return ___('картинки')->ucfirst();
    }

    public static function getNavigationLabel(): string
    {
        return ___('картинки')->ucfirst();
    }

    public function getTypes()
    {
        $root = Engine::getRootPath().'/types/cinema/template/images';
        $folders = ['bg', 'logo', 'favicon'];

        foreach ($folders as $folder) {
            $paths = [];
            $path = $root.'/'.$folder;
            $iter = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::SELF_FIRST,
                /*при блоке прав чтения не отвалится
                Ignore "Permission denied" (>>на которую у него нет прав на чтение)*/
                \RecursiveIteratorIterator::CATCH_GET_CHILD
            );
            foreach ($iter as $path => $dir) {
                if ($dir->isFile()) {
                    $pathinfo = pathinfo($path);
                    if ($pathinfo['basename'] == '.DS_Store') {
                        continue;
                    }
                    $paths[] = str_replace(Engine::getRootPath(), '', $path);
                }
            }
            $arr[$folder] = $paths;
        }

        return $arr;
    }

    public static function canAccess(): bool
    {
        return Auth::user()->can('view awmzone images page');
    }
}
