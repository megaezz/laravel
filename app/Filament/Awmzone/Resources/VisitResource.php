<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\VisitResource\Pages;
use App\Models\Visit;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class VisitResource extends Resource
{
    protected static ?string $model = Visit::class;

    protected static ?string $navigationIcon = 'heroicon-o-face-smile';

    public static function getModelLabel(): string
    {
        return ___('визит')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('визиты')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\DateTimePicker::make('date')
                    ->required(),
                Forms\Components\TextInput::make('host')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('query')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('userAgent')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('referer')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('get')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('post')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('cookie')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('ip')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('domain')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('time')
                    ->numeric()
                    ->default(null),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('date', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('date')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable(),
                Tables\Columns\TextColumn::make('host')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->searchable(),
                Tables\Columns\TextColumn::make('query')
                    ->limit(70)
                    ->description(fn (Visit $visit) => Str::limit($visit->userAgent, 70)),
                // Tables\Columns\TextColumn::make('userAgent'),
                Tables\Columns\TextColumn::make('referer')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('get')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('post')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('cookie')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('ip'),
                Tables\Columns\TextColumn::make('domain')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('time')
                    ->numeric(),
            ])
            ->filters([
                TernaryFilter::make('domainIsRenewed')
                    ->native(false)
                    ->query(fn (Builder $query, $data) => is_null($data['value']) ? $query : $query->whereHas('domainRelation', fn ($domain) => $domain->where(fn ($query) => $query
                        ->where(fn ($query) => $query
                            ->whereNull('parent_domain_domain')
                            ->where('renewed', $data['value'])
                        )
                        ->orWhereRelation('parentDomain', 'renewed', false)
                    )
                    )),
                Filter::make('hasBan')
                    ->toggle()
                    ->query(fn (Builder $query) => $query->hasBan()),
                Filter::make('onlyRealBotIp')
                    ->toggle()
                    ->query(fn (Builder $query) => $query
                        /* TODO: ограничиваем время 30 минутами, т.к. иначе запрос пиздецки долгий, сделать по уму */
                        ->whereBetween('date', [now()->subMinutes(20), now()])
                        ->whereRelation('ipRelation', 'is_real_bot', true)
                    ),
                Filter::make('calcQueryWasAbusedByServices')
                    ->baseQuery(fn (Builder $query) => $query->with('queryWasAbusedByServices'))
                    ->toggle(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListVisits::route('/'),
            // 'create' => Pages\CreateVisit::route('/create'),
            // 'edit' => Pages\EditVisit::route('/{record}/edit'),
        ];
    }
}
