<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\DnsProviderResource\Pages;
use App\Models\DnsProvider;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class DnsProviderResource extends Resource
{
    protected static ?string $model = DnsProvider::class;

    protected static ?string $navigationIcon = 'heroicon-o-cloud';

    public static function getModelLabel(): string
    {
        return ___('днс провайдер')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('днс провайдеры')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('email')
                    ->email()
                    ->required()
                    ->maxLength(100),
                Forms\Components\TextInput::make('api_key')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('password')
                    ->password()
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\Toggle::make('available_for_transfer')
                    ->required(),
                Forms\Components\Textarea::make('zones')
                    ->columnSpanFull(),
                Forms\Components\Toggle::make('allow_migrate_from')
                    ->required(),
                Forms\Components\TextInput::make('service'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('email')
                    ->searchable(),
                Tables\Columns\TextColumn::make('api_key')
                    ->searchable(),
                Tables\Columns\IconColumn::make('available_for_transfer')
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('allow_migrate_from')
                    ->boolean(),
                Tables\Columns\TextColumn::make('service'),
                Tables\Columns\TextColumn::make('zones')
                    ->label('Зоны')
                    ->state(function (DnsProvider $dnsProvider) {
                        $zones = collect($dnsProvider->zones?->result);

                        return "{$zones->where('status', 'active')->count()} / {$zones->count()}";
                    }),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDnsProviders::route('/'),
            'create' => Pages\CreateDnsProvider::route('/create'),
            'edit' => Pages\EditDnsProvider::route('/{record}/edit'),
        ];
    }
}
