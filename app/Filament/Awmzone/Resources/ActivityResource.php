<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\ActivityResource\Pages;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;
use Spatie\Activitylog\Models\Activity;

class ActivityResource extends Resource
{
    protected static ?string $model = Activity::class;

    protected static ?string $navigationIcon = 'heroicon-o-clock';

    public static function getNavigationGroup(): ?string
    {
        return ___('мониторинг')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('активность')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('активности')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('log_name')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Textarea::make('description')
                    ->required()
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('subject_type')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('event')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('subject_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('causer_type')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('causer_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\Textarea::make('properties')
                    ->afterStateHydrated(function ($component, $state) {
                        $component->state(json_encode($state));
                    })
                    ->columnSpanFull()
                    ->readOnly(),
                Forms\Components\TextInput::make('batch_uuid'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('id', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('id')
                    ->toggleable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('log_name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('subject_type')
                    ->searchable(),
                Tables\Columns\TextColumn::make('event')
                    ->searchable(),
                Tables\Columns\TextColumn::make('subject_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('causer_type')
                    ->searchable(),
                Tables\Columns\TextColumn::make('causer_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('properties')
                    ->limit(100)
                    // TODO: update преобразуется в null
                    // ->formatStateUsing(fn ($state) => json_encode(json_decode($state), JSON_UNESCAPED_UNICODE))
                    ->toggleable()
                    ->wrap(),
                Tables\Columns\TextColumn::make('batch_uuid'),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                TernaryFilter::make('causer_type')
                    ->native(false)
                    ->label('Инициатор')
                    ->nullable(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListActivities::route('/'),
            'create' => Pages\CreateActivity::route('/create'),
            'edit' => Pages\EditActivity::route('/{record}/edit'),
        ];
    }
}
