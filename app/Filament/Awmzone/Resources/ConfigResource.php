<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\ConfigResource\Pages;
use App\Models\Config;
use App\Models\Mailer;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class ConfigResource extends Resource
{
    protected static ?string $model = Config::class;

    protected static ?string $navigationIcon = 'heroicon-o-cog';

    public static function getModelLabel(): string
    {
        return ___('конфиг')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('конфиги')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('sessionTimeout')
                    ->required()
                    ->numeric(),
                Forms\Components\Toggle::make('switch')
                    ->required(),
                Forms\Components\TextInput::make('reload')
                    ->required()
                    ->numeric(),
                Forms\Components\Toggle::make('displayErrors')
                    ->required(),
                Forms\Components\Toggle::make('logLongQueries')
                    ->required(),
                Forms\Components\TextInput::make('cacheLifetime')
                    ->required()
                    ->numeric(),
                Forms\Components\TextInput::make('cacheSeconds')
                    ->numeric()
                    ->default(null),
                Forms\Components\Toggle::make('cacheUseDB')
                    ->required(),
                Forms\Components\Toggle::make('routerCache')
                    ->required(),
                Forms\Components\Toggle::make('debug')
                    ->required(),
                Forms\Components\Toggle::make('proxiesEnabled')
                    ->required(),
                Forms\Components\Toggle::make('debugByGet')
                    ->required(),
                Forms\Components\TextInput::make('logsLifeTime')
                    ->required()
                    ->numeric(),
                Forms\Components\TextInput::make('maxQueryQueue')
                    ->required()
                    ->numeric(),
                Forms\Components\Toggle::make('cronSwitch')
                    ->required(),
                Forms\Components\Select::make('mailer_id')
                    ->relationship(name: 'mailer', titleAttribute: 'name', modifyQueryUsing: fn ($query) => $query->where('active', true))
                    ->getOptionLabelFromRecordUsing(fn (Mailer $mailer) => "[{$mailer->transport}] {$mailer->host}:{$mailer->port} {$mailer->encryption} {$mailer->region}")
                    ->searchable()
                    ->preload(),
                Forms\Components\TextInput::make('proxy')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('ru_proxy')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\Toggle::make('dev_mode')
                    ->required(),
                Forms\Components\TextInput::make('requests_per_second_max')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('free_space_min')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('common_metrika_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('thumbor_server')
                    ->maxLength(50)
                    ->default(null),
                Forms\Components\TextInput::make('thumbor_secret_key')
                    ->maxLength(50)
                    ->default(null),
                Forms\Components\TextInput::make('mysql_queries')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('mysql_queries_per_second')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('ban_queries')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('ban_queries_per_second')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('visits_lifetime')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('ru_block_timeout')
                    ->required()
                    ->numeric()
                    ->default(10),
                Forms\Components\TextInput::make('chatgpt_api_key')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('db_dump_dir')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('dir_to_be_backed_up')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('borg_repository')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('borg_repository_password')
                    ->password()
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('vps_hostname')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('master_hostname')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\Toggle::make('save_visits')
                    ->required(),
                Forms\Components\Toggle::make('save_logs')
                    ->required(),
                Forms\Components\Toggle::make('crons_enabled')
                    ->required(),
                Forms\Components\Toggle::make('log_banned_visits')
                    ->required(),
                Forms\Components\TagsInput::make('monitored_countries'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->columns([
                Tables\Columns\TextColumn::make('sessionTimeout')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('switch')
                    ->boolean(),
                Tables\Columns\TextColumn::make('reload')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('displayErrors')
                    ->boolean(),
                Tables\Columns\IconColumn::make('logLongQueries')
                    ->boolean(),
                Tables\Columns\TextColumn::make('cacheLifetime')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('cacheSeconds')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('cacheUseDB')
                    ->boolean(),
                Tables\Columns\IconColumn::make('routerCache')
                    ->boolean(),
                Tables\Columns\IconColumn::make('debug')
                    ->boolean(),
                Tables\Columns\IconColumn::make('proxiesEnabled')
                    ->boolean(),
                Tables\Columns\IconColumn::make('debugByGet')
                    ->boolean(),
                Tables\Columns\TextColumn::make('logsLifeTime')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('maxQueryQueue')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('cronSwitch')
                    ->boolean(),
                Tables\Columns\TextColumn::make('smtpConfig')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('proxy'),
                Tables\Columns\TextColumn::make('ru_proxy'),
                Tables\Columns\IconColumn::make('dev_mode')
                    ->boolean(),
                Tables\Columns\TextColumn::make('requests_per_second_max')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('free_space_min')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('common_metrika_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('thumbor_server'),
                Tables\Columns\TextColumn::make('thumbor_secret_key'),
                Tables\Columns\TextColumn::make('mgw5_la'),
                Tables\Columns\TextColumn::make('mysql_queries')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('mysql_queries_per_second')
                    ->numeric(),
                Tables\Columns\TextColumn::make('ban_queries')
                    ->numeric(),
                Tables\Columns\TextColumn::make('ban_queries_per_second')
                    ->numeric(),
                Tables\Columns\TextColumn::make('visits_lifetime')
                    ->numeric(),
                Tables\Columns\TextColumn::make('ru_block_timeout')
                    ->numeric(),
                Tables\Columns\TextColumn::make('chatgpt_api_key'),
                Tables\Columns\TextColumn::make('db_dump_dir'),
                Tables\Columns\TextColumn::make('dir_to_be_backed_up'),
                Tables\Columns\TextColumn::make('borg_repository'),
                Tables\Columns\TextColumn::make('vps_hostname'),
                Tables\Columns\TextColumn::make('master_hostname'),
                Tables\Columns\IconColumn::make('save_visits')
                    ->boolean(),
                Tables\Columns\IconColumn::make('save_logs')
                    ->boolean(),
                Tables\Columns\IconColumn::make('crons_enabled')
                    ->boolean(),
                Tables\Columns\IconColumn::make('log_banned_visits')
                    ->boolean(),
                Tables\Columns\TextColumn::make('monitored_countries'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListConfigs::route('/'),
            // 'create' => Pages\CreateConfig::route('/create'),
            // 'edit' => Pages\EditConfig::route('/{record}/edit'),
        ];
    }
}
