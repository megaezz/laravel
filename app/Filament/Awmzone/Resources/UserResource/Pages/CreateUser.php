<?php

namespace App\Filament\Awmzone\Resources\UserResource\Pages;

use App\Filament\Awmzone\Resources\UserResource;
use Filament\Resources\Pages\CreateRecord;

class CreateUser extends CreateRecord
{
    protected static string $resource = UserResource::class;
}
