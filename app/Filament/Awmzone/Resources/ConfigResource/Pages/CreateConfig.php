<?php

namespace App\Filament\Awmzone\Resources\ConfigResource\Pages;

use App\Filament\Awmzone\Resources\ConfigResource;
use Filament\Resources\Pages\CreateRecord;

class CreateConfig extends CreateRecord
{
    protected static string $resource = ConfigResource::class;
}
