<?php

namespace App\Filament\Awmzone\Resources\DnsProviderResource\Pages;

use App\Filament\Awmzone\Resources\DnsProviderResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDnsProvider extends CreateRecord
{
    protected static string $resource = DnsProviderResource::class;
}
