<?php

namespace App\Filament\Awmzone\Resources\DnsProviderResource\Pages;

use App\Filament\Awmzone\Resources\DnsProviderResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDnsProvider extends EditRecord
{
    protected static string $resource = DnsProviderResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
