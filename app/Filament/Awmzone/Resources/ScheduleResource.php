<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\ScheduleResource\Pages;
use App\Models\Schedule;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class ScheduleResource extends Resource
{
    protected static ?string $model = Schedule::class;

    protected static ?string $navigationIcon = 'heroicon-o-calendar-days';

    public static function getModelLabel(): string
    {
        return ___('расписание')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('расписания')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('command')
                    ->required()
                    ->maxLength(100),
                Forms\Components\Toggle::make('active')
                    ->required(),
                Forms\Components\Toggle::make('done')
                    ->required(),
                Forms\Components\Toggle::make('dev')
                    ->required(),
                Forms\Components\TextInput::make('period')
                    ->maxLength(30)
                    ->default(null),
                Forms\Components\TextInput::make('at')
                    ->maxLength(10)
                    ->default(null),
                Forms\Components\TextInput::make('repeat_for')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('next_launch_delay')
                    ->numeric()
                    ->default(null),
                Forms\Components\Toggle::make('even_in_maintenance_mode')
                    ->required(),
                Forms\Components\Toggle::make('without_overlapping')
                    ->required(),
                Forms\Components\TextInput::make('temp_var')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('temp_var_2')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\Textarea::make('data')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('run_time')
                    ->numeric()
                    ->default(null),
                Forms\Components\DateTimePicker::make('last_time'),
                Forms\Components\DateTimePicker::make('finished_at'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucFirst()))
            ->defaultSort('id', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('command')
                    ->searchable(),
                Tables\Columns\IconColumn::make('active')
                    ->boolean(),
                Tables\Columns\IconColumn::make('done')
                    ->boolean(),
                Tables\Columns\IconColumn::make('dev')
                    ->boolean(),
                Tables\Columns\TextColumn::make('period')
                    ->searchable(),
                Tables\Columns\TextColumn::make('at')
                    ->searchable(),
                Tables\Columns\TextColumn::make('repeat_for')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('next_launch_delay')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('even_in_maintenance_mode')
                    ->boolean(),
                Tables\Columns\IconColumn::make('without_overlapping')
                    ->boolean(),
                Tables\Columns\TextColumn::make('temp_var')
                    ->searchable(),
                Tables\Columns\TextColumn::make('temp_var_2')
                    ->searchable(),
                Tables\Columns\TextColumn::make('run_time')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('last_time')
                    ->dateTime()
                    ->sortable(),
                Tables\Columns\TextColumn::make('finished_at')
                    ->dateTime()
                    ->sortable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSchedules::route('/'),
            'create' => Pages\CreateSchedule::route('/create'),
            'edit' => Pages\EditSchedule::route('/{record}/edit'),
        ];
    }
}
