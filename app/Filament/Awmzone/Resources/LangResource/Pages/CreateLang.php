<?php

namespace App\Filament\Awmzone\Resources\LangResource\Pages;

use App\Filament\Awmzone\Resources\LangResource;
use Filament\Resources\Pages\CreateRecord;

class CreateLang extends CreateRecord
{
    protected static string $resource = LangResource::class;
}
