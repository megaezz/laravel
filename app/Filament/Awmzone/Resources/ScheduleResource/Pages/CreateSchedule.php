<?php

namespace App\Filament\Awmzone\Resources\ScheduleResource\Pages;

use App\Filament\Awmzone\Resources\ScheduleResource;
use Filament\Resources\Pages\CreateRecord;

class CreateSchedule extends CreateRecord
{
    protected static string $resource = ScheduleResource::class;
}
