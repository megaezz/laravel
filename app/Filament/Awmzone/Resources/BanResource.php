<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\BanResource\Pages;
use App\Models\Ban;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class BanResource extends Resource
{
    protected static ?string $model = Ban::class;

    protected static ?string $navigationIcon = 'heroicon-o-shield-exclamation';

    public static function getModelLabel(): string
    {
        return ___('бан')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('баны')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('ip')
                    ->required()
                    ->maxLength(255)
                    ->default(''),
                Forms\Components\TextInput::make('user_agent')
                    ->required()
                    ->maxLength(255)
                    ->default(''),
                Forms\Components\TextInput::make('referer')
                    ->required()
                    ->maxLength(255)
                    ->default(''),
                Forms\Components\TextInput::make('query')
                    ->required()
                    ->maxLength(255)
                    ->default(''),
                Forms\Components\TextInput::make('description')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\Toggle::make('active')
                    ->required(),
                Forms\Components\TextInput::make('q')
                    ->required()
                    ->numeric()
                    ->default(0),
                Forms\Components\Toggle::make('hide_player')
                    ->required(),
                Forms\Components\TextInput::make('q_player')
                    ->required()
                    ->numeric()
                    ->default(0),
                Forms\Components\Toggle::make('hide_metrika')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('id', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('ip')
                    ->searchable(),
                Tables\Columns\TextColumn::make('user_agent')
                    ->searchable(),
                Tables\Columns\TextColumn::make('referer')
                    ->searchable(),
                Tables\Columns\TextColumn::make('query')
                    ->searchable(),
                Tables\Columns\TextColumn::make('description')
                    ->searchable(),
                Tables\Columns\IconColumn::make('active')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('q')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable(),
                Tables\Columns\IconColumn::make('hide_player')
                    ->boolean(),
                Tables\Columns\TextColumn::make('q_player')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('hide_metrika')
                    ->boolean(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListBans::route('/'),
            // 'create' => Pages\CreateBan::route('/create'),
            // 'edit' => Pages\EditBan::route('/{record}/edit'),
        ];
    }
}
