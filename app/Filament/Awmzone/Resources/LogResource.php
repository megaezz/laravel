<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\LogResource\Pages;
use App\Models\Log;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;

class LogResource extends Resource
{
    protected static ?string $model = Log::class;

    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    public static function getNavigationGroup(): ?string
    {
        return ___('мониторинг')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('лог')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('логи')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\DateTimePicker::make('date')
                    ->label('Дата')
                    ->required(),
                Forms\Components\Textarea::make('text')
                    ->label('Текст')
                    ->required()
                    ->columnSpanFull(),
                Forms\Components\Textarea::make('backtrace')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('type')
                    ->label('Тип')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\TextInput::make('visit_id')
                    ->label('Визит')
                    ->numeric()
                    ->default(null),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('date', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('date')
                    ->label('Дата')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable(),
                Tables\Columns\TextColumn::make('visit.ip')
                    ->label('IP')
                    ->searchable(),
                Tables\Columns\TextColumn::make('text')
                    ->label('Текст')
                    ->limit(70)
                    ->searchable(),
                Tables\Columns\TextColumn::make('visit.query')
                    ->label('Запрос')
                    ->searchable(),
                Tables\Columns\TextColumn::make('visit.userAgent')
                    ->label('User Agent')
                    ->limit(70)
                    ->searchable(),
                Tables\Columns\TextColumn::make('visit.referer')
                    ->label('Referer')
                    ->limit(70)
                    ->searchable(),
                Tables\Columns\TextColumn::make('backtrace')
                    ->limit(70),
                Tables\Columns\TextColumn::make('type')
                    ->label('Тип')
                    ->searchable(),
            ])
            ->filters([
                SelectFilter::make('text')
                    ->options([
                        'errors' => 'Исключения без мусора',
                        'Показали фейк заглушку' => 'Показали фейковую заглушку%',
                        'Is not real bot' => 'Is not real bot',
                        'Domain doesn\'t exist' => '%doesn\'t exist',
                        'Route was not recognized' => 'Route was not recognized',
                        'syntax error, unexpected token "*"' => 'syntax error, unexpected token "*"',
                        'Ошибка eval' => 'Ошибка eval%',
                        'Ошибка PHP' => 'PHP ошибка%',
                    ])
                    ->query(fn (Builder $query, $data) => is_null($data['value'])
                        ? $query
                        : (
                            ($data['value'] === 'errors')
                            ? $query
                                ->where('type', 'ERROR')
                                ->whereNotIn('text', ['Favicon.ico не указан для сайта', 'Показали страницу 404', 'Route was not recognized'])
                            : $query->where('text', 'like', "%{$data['value']}%")
                        )
                    )
                    ->native(false),
                TernaryFilter::make('domainIsRenewed')
                    ->native(false)
                    ->query(fn (Builder $query, $data) => is_null($data['value']) ? $query : $query->whereHas('visit.domainRelation', fn ($domain) => $domain->where(fn ($query) => $query
                        ->where(fn ($query) => $query
                            ->whereNull('parent_domain_domain')
                            ->where('renewed', $data['value'])
                        )
                        ->orWhereRelation('parentDomain', 'renewed', false)
                    )
                    )),
                Filter::make('hasBan')
                    ->toggle()
                    ->query(fn (Builder $query) => $query->whereHas('visit', fn ($visit) => $visit->hasBan())),
                Filter::make('onlyRealBotIp')
                    ->toggle()
                    ->query(fn (Builder $query) => $query->whereHas('visit', fn ($visit) => $visit
                        /* TODO: ограничиваем время 30 минутами, т.к. иначе запрос пиздецки долгий, сделать по уму */
                        ->whereBetween('date', [now()->subMinutes(20), now()])
                        ->whereRelation('ipRelation', 'is_real_bot', true)
                    )),
                Filter::make('calcQueryWasAbusedByServices')
                    ->baseQuery(fn (Builder $query) => $query->with('visit.queryWasAbusedByServices'))
                    ->toggle(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListLogs::route('/'),
            // 'create' => Pages\CreateLog::route('/create'),
            // 'edit' => Pages\EditLog::route('/{record}/edit'),
        ];
    }
}
