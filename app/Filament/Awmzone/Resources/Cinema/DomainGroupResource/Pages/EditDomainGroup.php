<?php

namespace App\Filament\Awmzone\Resources\Cinema\DomainGroupResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\DomainGroupResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDomainGroup extends EditRecord
{
    protected static string $resource = DomainGroupResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
