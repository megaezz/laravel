<?php

namespace App\Filament\Awmzone\Resources\Cinema\DomainGroupResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\DomainGroupResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDomainGroup extends CreateRecord
{
    protected static string $resource = DomainGroupResource::class;
}
