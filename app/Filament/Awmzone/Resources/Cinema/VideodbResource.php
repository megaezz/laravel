<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\VideodbResource\Pages;
use App\Models\Cinema\Videodb;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class VideodbResource extends Resource
{
    protected static ?string $model = Videodb::class;

    protected static ?string $navigationIcon = 'heroicon-o-arrow-top-right-on-square';

    public static function getNavigationGroup(): ?string
    {
        return ___('кино')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('movie_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\Textarea::make('data')
                    ->columnSpanFull(),
                Forms\Components\Toggle::make('checked')
                    ->required(),
                Forms\Components\DateTimePicker::make('date')
                    ->required(),
                Forms\Components\TextInput::make('max_quality')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('translation')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('episode')
                    ->maxLength(10)
                    ->default(null),
                Forms\Components\TextInput::make('season')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('videodb_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('videodb_show_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('kinopoisk_id')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\DateTimePicker::make('is_exists_on_date'),
                Forms\Components\TextInput::make('movie_episode_id')
                    ->numeric()
                    ->default(null),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('id', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('id')
                    ->label('ID')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('movie_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('checked')
                    ->boolean(),
                Tables\Columns\TextColumn::make('date')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable(),
                Tables\Columns\TextColumn::make('max_quality')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('translation')
                    ->searchable(),
                Tables\Columns\TextColumn::make('episode')
                    ->searchable(),
                Tables\Columns\TextColumn::make('season')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('videodb_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('videodb_show_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('kinopoisk_id')
                    ->searchable(),
                Tables\Columns\TextColumn::make('is_exists_on_date')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable(),
                Tables\Columns\TextColumn::make('movie_episode_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListVideodbs::route('/'),
            // 'create' => Pages\CreateVideodb::route('/create'),
            // 'edit' => Pages\EditVideodb::route('/{record}/edit'),
        ];
    }
}
