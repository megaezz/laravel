<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\GroupResource\Pages;
use cinema\app\models\eloquent\Group;
use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\Tag;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;

class GroupResource extends Resource
{
    protected static ?string $model = Group::class;

    protected static ?string $navigationIcon = 'heroicon-o-table-cells';

    public static function getNavigationGroup(): ?string
    {
        return ___('кино')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('группа')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('группы')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->columns(3)
            ->schema([
                Forms\Components\FileUpload::make('thumb')
                    ->label('Картинка')
                    ->directory('cinema/images/groups')
                    ->image()
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('name')
                    ->label('Название на русском')
                    ->required()
                    ->maxLength(40),
                Forms\Components\TextInput::make('name_en')
                    ->label('Название на английском')
                    ->maxLength(40)
                    ->default(null),
                Forms\Components\TextInput::make('name_ua')
                    ->label('Название на украинском')
                    ->maxLength(40)
                    ->default(null),
                Forms\Components\Select::make('type')
                    ->label('Тип')
                    ->required()
                    ->options(Group::distinct()->pluck('type', 'type')->sort()),
                Forms\Components\TextInput::make('plural')
                    ->label('Множественное число')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('plural2')
                    ->label('Множественное число, вар. 2')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('emoji')
                    ->label('Эмодзи'),
                Forms\Components\Toggle::make('active')
                    ->label('Активна')
                    ->inline(false),
                Forms\Components\Toggle::make('calc_by_tags')
                    ->label('На основе тегов')
                    ->inline(false)
                    ->reactive(),
                Forms\Components\Select::make('tags')
                    ->label('Теги')
                    ->multiple()
                    ->searchable()
                    ->allowHtml()
                    ->relationship('tags', 'tag')
                    // ищем слева, иначе например тег movie невозможно найти
                    ->getSearchResultsUsing(fn ($query) => Tag::where('tag', 'LIKE', $query.'%')->pluck('tag', 'id'))
                    ->visible(fn ($get) => $get('calc_by_tags')),
                Forms\Components\Select::make('movies')
                    ->label('Фильмы')
                    ->multiple()
                    ->searchable()
                    ->placeholder('Название или Kinopoisk ID')
                    ->relationship('movies')
                    ->getSearchResultsUsing(fn ($query) => Movie::query()
                        ->whereNotIn('type', ['youtube'])
                        ->where('title_ru', 'LIKE', "%{$query}%")
                        ->orWhere('title_en', 'LIKE', "%{$query}%")
                        ->orWhere('kinopoisk_id', $query)
                        ->get()
                        // здесь отображается вид элементов в поиске
                        ->mapWithKeys(fn ($movie) => [$movie->id => "{$movie->title_ru} ({$movie->localed_type}, {$movie->year})"])
                        ->toArray()
                    )
                    // здесь отображается вид уже добавленных элементов (но они мигают между собой я хз в чем там дело)
                    ->getOptionLabelFromRecordUsing(fn ($record) => "{$record->title_ru} ({$record->localed_type}, {$record->year})")
                    ->visible(fn ($get) => ! $get('calc_by_tags')),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('created_at', 'desc')
            ->columns([
                Tables\Columns\ImageColumn::make('thumb'),
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('tags.tag')
                    ->badge(),
                Tables\Columns\IconColumn::make('active')
                    ->boolean(),
                Tables\Columns\TextColumn::make('type'),
                Tables\Columns\TextColumn::make('plural')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('plural2')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('name_en')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('name_ua')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('calc_by_tags')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->boolean(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                TernaryFilter::make('calc_by_tags')
                    ->label('На основе тегов')
                    ->native(false),
                SelectFilter::make('type')
                    ->label('Тип')
                    ->native(false)
                    ->options(Group::distinct()->pluck('type', 'type')->sort()),

            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Action::make('domainGroups')
                    ->label('На доменах')
                    ->url(fn (Group $record): string => route('filament.awmzone.resources.cinema.domain-groups.index', ['tableFilters' => ['group_id' => ['value' => $record->id]]])),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGroups::route('/'),
            'create' => Pages\CreateGroup::route('/create'),
            'edit' => Pages\EditGroup::route('/{record}/edit'),
        ];
    }
}
