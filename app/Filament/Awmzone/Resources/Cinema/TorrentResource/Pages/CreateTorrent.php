<?php

namespace App\Filament\Awmzone\Resources\Cinema\TorrentResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\TorrentResource;
use Filament\Resources\Pages\CreateRecord;

class CreateTorrent extends CreateRecord
{
    protected static string $resource = TorrentResource::class;
}
