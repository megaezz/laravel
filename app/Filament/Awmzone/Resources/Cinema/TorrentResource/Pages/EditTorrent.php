<?php

namespace App\Filament\Awmzone\Resources\Cinema\TorrentResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\TorrentResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTorrent extends EditRecord
{
    protected static string $resource = TorrentResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
