<?php

namespace App\Filament\Awmzone\Resources\Cinema\TorrentResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\TorrentResource;
use App\Models\Cinema\Torrent;
use Filament\Actions;
use Filament\Notifications\Notification;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ListTorrents extends ListRecords
{
    protected static string $resource = TorrentResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->mutateFormDataUsing(function (array $data): array {
                    // сохраняем пользователя, который добавил торрент
                    $data['created_by_id'] = Auth::user()->id;

                    return $data;
                })
                ->before(function ($action, array $data) {
                    // проверяем есть ли уже обработанный торрент с таким хешем
                    $hash = md5(Storage::disk('public')->get($data['src']));
                    $existedTorrent = Torrent::where('hash', $hash)->first();
                    if ($existedTorrent) {
                        Notification::make()
                            ->body("Этот торрент уже был добавлен: {$existedTorrent->name}")
                            ->warning()
                            ->send();
                        // прерываем выполнение
                        $action->halt();
                    }

                    // проверяем, есть ли уже торрент с таким же фильмом и студией

                }),
            Actions\Action::make('clearDownloads')
                ->authorize(fn () => Auth::user()->can('everything'))
                ->label('Очистить загрузки')
                ->icon('heroicon-o-trash')
                ->action(function () {

                    $user = Auth::user();

                    // отправляем в фон, т.к. у www-data нет .ssh ключей (подробнее в stack.yml, в комментах к php-fpm)
                    dispatch(function () use ($user) {
                        Notification::make()->title('Очистка загрузок')->body(Torrent::clearDownloads())->sendToDatabase($user);
                    });

                    Notification::make()->body('Команда отправлена')->send();
                }),
        ];
    }
}
