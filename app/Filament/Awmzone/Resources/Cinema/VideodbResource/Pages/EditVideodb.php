<?php

namespace App\Filament\Awmzone\Resources\Cinema\VideodbResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\VideodbResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditVideodb extends EditRecord
{
    protected static string $resource = VideodbResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
