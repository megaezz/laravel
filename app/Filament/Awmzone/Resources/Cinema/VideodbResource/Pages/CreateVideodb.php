<?php

namespace App\Filament\Awmzone\Resources\Cinema\VideodbResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\VideodbResource;
use Filament\Resources\Pages\CreateRecord;

class CreateVideodb extends CreateRecord
{
    protected static string $resource = VideodbResource::class;
}
