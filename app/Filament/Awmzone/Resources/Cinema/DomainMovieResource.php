<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\DomainMovieResource\Pages;
use App\Models\Cinema\DomainMovie;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;

class DomainMovieResource extends Resource
{
    protected static ?string $model = DomainMovie::class;

    protected static ?string $navigationIcon = 'heroicon-o-film';

    public static function getNavigationGroup(): ?string
    {
        return ___('кино')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('фильм на сайте')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('фильмы на сайтах')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('domain')
                    ->maxLength(30)
                    ->default(null),
                Forms\Components\TextInput::make('movie_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\DateTimePicker::make('add_date')
                    ->required(),
                Forms\Components\Textarea::make('description')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('description_writer')
                    ->maxLength(30)
                    ->default(null),
                Forms\Components\DateTimePicker::make('description_date'),
                Forms\Components\TextInput::make('booked_for_login')
                    ->maxLength(30)
                    ->default(null),
                Forms\Components\TextInput::make('description_price_per_1k')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('text_ru_description_id')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Textarea::make('text_ru_result')
                    ->columnSpanFull(),
                Forms\Components\Textarea::make('text_ru_wide_result')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('text_ru_unique')
                    ->numeric()
                    ->default(null),
                Forms\Components\Toggle::make('active')
                    ->required(),
                Forms\Components\TextInput::make('dmca')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Toggle::make('deleted')
                    ->required(),
                Forms\Components\TextInput::make('customer')
                    ->maxLength(30)
                    ->default(null),
                Forms\Components\TextInput::make('customer_price_per_1k')
                    ->numeric()
                    ->default(null),
                Forms\Components\Toggle::make('rework')
                    ->required(),
                Forms\Components\Textarea::make('rework_comment')
                    ->columnSpanFull(),
                Forms\Components\Toggle::make('show_to_customer')
                    ->required(),
                Forms\Components\Toggle::make('customer_archived')
                    ->required(),
                Forms\Components\Toggle::make('to_top')
                    ->required(),
                Forms\Components\Toggle::make('writing')
                    ->required(),
                Forms\Components\Textarea::make('customer_comment')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('symbols_from')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('symbols_to')
                    ->numeric()
                    ->default(null),
                Forms\Components\DateTimePicker::make('book_date'),
                Forms\Components\Textarea::make('private_comment')
                    ->columnSpanFull(),
                Forms\Components\Toggle::make('to_bottom')
                    ->required(),
                Forms\Components\Toggle::make('rkn')
                    ->required(),
                Forms\Components\Toggle::make('deleted_page')
                    ->required(),
                Forms\Components\Toggle::make('do_redirect')
                    ->required(),
                Forms\Components\Toggle::make('block_foreigners')
                    ->required(),
                Forms\Components\Toggle::make('customer_like'),
                Forms\Components\Toggle::make('checked')
                    ->required(),
                Forms\Components\TextInput::make('expected_price')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('description_length')
                    ->required()
                    ->numeric()
                    ->default(0),
                Forms\Components\DateTimePicker::make('rework_date'),
                Forms\Components\TextInput::make('moderator')
                    ->maxLength(30)
                    ->default(null),
                Forms\Components\TextInput::make('moderation_price_per_1k')
                    ->required()
                    ->numeric()
                    ->default(0.0),
                Forms\Components\DateTimePicker::make('customer_confirm_date'),
                Forms\Components\Toggle::make('adult')
                    ->required(),
                Forms\Components\TextInput::make('customer_test')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('slug')
                    ->maxLength(255)
                    ->default(null),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('id', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('id')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('domain'),
                Tables\Columns\TextColumn::make('movie_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('add_date')
                    ->dateTime()
                    ->sortable(),
                Tables\Columns\TextColumn::make('description_writer'),
                Tables\Columns\TextColumn::make('description_date')
                    ->dateTime()
                    ->sortable(),
                Tables\Columns\TextColumn::make('booked_for_login'),
                Tables\Columns\TextColumn::make('description_price_per_1k')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('text_ru_description_id'),
                Tables\Columns\TextColumn::make('text_ru_unique')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('active')
                    ->boolean(),
                Tables\Columns\TextColumn::make('dmca'),
                Tables\Columns\IconColumn::make('deleted')
                    ->boolean(),
                Tables\Columns\TextColumn::make('customer'),
                Tables\Columns\TextColumn::make('customer_price_per_1k')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\IconColumn::make('rework')
                    ->boolean(),
                Tables\Columns\IconColumn::make('show_to_customer')
                    ->boolean(),
                Tables\Columns\IconColumn::make('customer_archived')
                    ->boolean(),
                Tables\Columns\IconColumn::make('to_top')
                    ->boolean(),
                Tables\Columns\IconColumn::make('writing')
                    ->boolean(),
                Tables\Columns\TextColumn::make('symbols_from')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('symbols_to')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('book_date')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable(),
                Tables\Columns\IconColumn::make('to_bottom')
                    ->boolean(),
                Tables\Columns\IconColumn::make('rkn')
                    ->boolean(),
                Tables\Columns\IconColumn::make('deleted_page')
                    ->boolean(),
                Tables\Columns\IconColumn::make('do_redirect')
                    ->boolean(),
                Tables\Columns\IconColumn::make('block_foreigners')
                    ->boolean(),
                Tables\Columns\IconColumn::make('customer_like')
                    ->boolean(),
                Tables\Columns\IconColumn::make('checked')
                    ->boolean(),
                Tables\Columns\TextColumn::make('expected_price')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('description_length')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('rework_date')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable(),
                Tables\Columns\TextColumn::make('moderator'),
                Tables\Columns\TextColumn::make('moderation_price_per_1k')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('customer_confirm_date')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable(),
                Tables\Columns\IconColumn::make('adult')
                    ->boolean(),
                Tables\Columns\TextColumn::make('customer_test')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('slug'),
            ])
            ->filters([
                SelectFilter::make('domain')
                    ->label('Домен')
                    ->searchable()
                    ->preload()
                    ->relationship('cinemaDomain', 'domain'),
                SelectFilter::make('movie_id')
                    ->label('Фильм')
                    ->searchable()
                    ->relationship('movie', 'title_ru', fn ($query) => $query->where('type', '!=', 'youtube')),
                TernaryFilter::make('active')
                    ->native(false),
                TernaryFilter::make('description')
                    ->label('С описанием')
                    ->native(false)
                    ->nullable(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDomainMovies::route('/'),
            // 'create' => Pages\CreateDomainMovie::route('/create'),
            // 'edit' => Pages\EditDomainMovie::route('/{record}/edit'),
        ];
    }
}
