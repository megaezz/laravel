<?php

namespace App\Filament\Awmzone\Resources\Cinema\Turbo\DomainResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\Turbo\DomainResource;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Dubbing;
use Filament\Actions;
use Filament\Actions\Action;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Support\Facades\Auth;

class ListDomains extends ListRecords
{
    protected static string $resource = DomainResource::class;

    protected function getHeaderActions(): array
    {
        $config = Config::cached();

        return [
            Actions\CreateAction::make(),
            Action::make('turboSettings')
                // ->authorize(fn() => Auth::user()->can('everything'))
                ->label(function () use ($config) {
                    $dubbing = $config->priorityDubbingOverride;
                    return "Приоритет: " . ($dubbing ? "{$dubbing->short_or_full_name} сверху" : 'обычный');
                })
                ->action(function (array $data) use ($config) {
                    $config->priorityDubbingOverride()->associate($this->booleanToDubbing($data['priorityDubbingOverride']));
                    $config->save();
                })
                ->form([
                    Toggle::make('priorityDubbingOverride')
                        ->live()
                        ->default($this->dubbingToBoolean($config->priorityDubbingOverride))
                        ->label(function (bool $state) {
                            $dubbing = $this->booleanToDubbing($state);
                            return $dubbing ? "{$dubbing->short_or_full_name} сверху" : 'обычный';
                        }),
                ]),
        ];
    }

    function booleanToDubbing(bool $b)
    {
        return $b ? Dubbing::where('videodb_name', '1win Studio')->first() : null;
    }

    function dubbingToBoolean(?Dubbing $dubbing = null)
    {
        return $dubbing ? true : false;
    }
}
