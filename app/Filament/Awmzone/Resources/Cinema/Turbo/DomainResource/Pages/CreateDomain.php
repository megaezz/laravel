<?php

namespace App\Filament\Awmzone\Resources\Cinema\Turbo\DomainResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\Turbo\DomainResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDomain extends CreateRecord
{
    protected static string $resource = DomainResource::class;
}
