<?php

namespace App\Filament\Awmzone\Resources\Cinema\Turbo\DomainResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\Turbo\DomainResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDomain extends EditRecord
{
    protected static string $resource = DomainResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
