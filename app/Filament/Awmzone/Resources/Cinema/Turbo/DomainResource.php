<?php

namespace App\Filament\Awmzone\Resources\Cinema\Turbo;

use App\Filament\Awmzone\Resources\Cinema\Turbo\DomainResource\Pages;
use App\Models\Domain;
use App\Services\Cinema\Turbo\LinksToPrioritizedUploadsOnDomain;
use cinema\app\models\eloquent\Dubbing;
use Filament\Forms;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\ToggleButtons;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\HtmlString;

class DomainResource extends Resource
{
    protected static ?string $model = Domain::class;

    protected static ?string $navigationIcon = 'heroicon-o-globe-alt';

    // реализуем авторизацию не через политику Domain, т.к. в ней реализована логика для другого DomainResource

    public static function canViewAny(): bool
    {
        return Auth::user()->can('view cinema turbo domains');
    }

    public static function canView($record): bool
    {
        return Auth::user()->can('view cinema turbo domains');
    }

    public static function canEdit($record): bool
    {
        return Auth::user()->can('update cinema turbo domains');
    }


    public static function getModelLabel(): string
    {
        return ___('домен')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('домены')->ucfirst();
    }

    public static function getNavigationGroup(): ?string
    {
        return ___('турбо')->ucfirst();
    }

    public static function dubbings()
    {
        return ['Полное дублирование (Dubляж)', '1win Studio', '1win Express'];
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Group::make([
                    Group::make()
                        ->relationship('turbo')
                        ->schema([
                            Select::make('dubbing_id')
                                ->label('Приоритетная студия')
                                ->searchable()
                                ->native(false)
                                ->relationship('dubbing', 'videodb_name'),
                            ToggleButtons::make('dubbing_id')
                                ->label('')
                                ->inline()
                                ->options(Dubbing::whereHas('torrents')->get()->mapWithKeys(fn(Dubbing $dubbing) => [$dubbing->id => $dubbing->videodb_name])->toArray()),
                        ]),
                ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->modifyQueryUsing(fn($query) => $query->onlyTurbo()->with('mirrorOf'))
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->columns([
                Tables\Columns\ImageColumn::make('mirror_of_or_self.favicon_src')
                    ->width(20)
                    ->height(20)
                    ->circular()
                    ->label('')
                    ->state(fn(Domain $domain) => $domain->mirror_of_or_self->favicon_src ? ('/images/h40' . $domain->mirror_of_or_self->favicon_src) : null),
                TextColumn::make('domain')
                    ->label('Домен')
                    ->sortable()
                    // ищет по домену и по зеркалам
                    ->copyable()
                    ->searchable(),
                // ->searchable(
                //     query: fn($query, $search) => $query
                //         ->where('domain', 'like', "%{$search}%")
                //         ->orWhere(
                //             fn($query) =>
                //             $query
                //                 ->whereRelation('mirrorOf.mirrors', 'domain', 'like', "%{$search}%")
                //                 ->orWhereRelation('mirrors', 'domain', 'like', "%{$search}%")
                //         )
                // ),
                Tables\Columns\TextColumn::make('domain_decoded')
                    ->label('Декодированный')
                    ->sortable()
                    ->searchable()
                    ->copyable()
                    ->toggleable(),
                TextColumn::make('turbo.dubbing.videodb_name')
                    ->label('Приоритетная студия')
                    ->toggleable()
                    ->sortable(),
                TextColumn::make('priorityDubbing.videodb_name')
                    ->label('Текущая приоритетная студия')
                    ->toggleable(),
                IconColumn::make('monitoring_ru')
                    ->label('Доступен в РФ')
                    ->toggleable()
                    ->sortable()
                    ->boolean(),
            ])
            ->filters([
                TernaryFilter::make('dubbingExists')
                    ->label('Указана приоритетная студия')
                    ->queries(
                        true: fn($query) => $query->whereHas('turbo.dubbing'),
                        false: fn($query) => $query->doesntHave('turbo.dubbing'),
                    )
                    ->native(false),
                SelectFilter::make('dubbing')
                    ->label('Приоритетная студия')
                    ->multiple()
                    ->preload()
                    ->native(false)
                    ->relationship('turbo.dubbing', 'videodb_name', fn($query) => $query->whereHas('turboDomains')),
                TernaryFilter::make('monitoring_ru')
                    ->label('Доступен в РФ')
                    ->native(false),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\Action::make('links')
                    ->label('Ссылки')
                    ->modalContent(function (Domain $domain) {

                        try {
                            $links = (new LinksToPrioritizedUploadsOnDomain)($domain);
                        } catch (\Exception $e) {
                            Notification::make()
                                ->title('Ссылки')
                                ->body($e->getMessage())
                                ->warning()
                                ->send();

                            return;
                        }

                        $formatted =
                            '<ol class="list-decimal">' .
                            $links->map(fn($link) => "<li><a href=\"{$link}\">{$link}</a></li>")->implode('') .
                            '</ol>';

                        return new HtmlString($formatted);
                    }),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDomains::route('/'),
            // 'create' => Pages\CreateDomain::route('/create'),
            // 'edit' => Pages\EditDomain::route('/{record}/edit'),
        ];
    }
}
