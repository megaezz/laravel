<?php

namespace App\Filament\Awmzone\Resources\Cinema\ArticleResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\ArticleResource;
use Filament\Resources\Pages\CreateRecord;

class CreateArticle extends CreateRecord
{
    protected static string $resource = ArticleResource::class;
}
