<?php

namespace App\Filament\Awmzone\Resources\Cinema\ArticleResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\ArticleResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditArticle extends EditRecord
{
    protected static string $resource = ArticleResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
