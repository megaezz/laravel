<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\AbuseResource\Pages;
use App\Filament\Awmzone\Resources\Cinema\AbuseResource\Pages\BulkCreate;
use App\Models\Abuse;
use Filament\Forms;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Actions\BulkAction;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AbuseResource extends Resource
{
    protected static ?string $model = Abuse::class;

    protected static ?string $navigationIcon = 'heroicon-o-envelope';

    public static function getNavigationGroup(): ?string
    {
        return ___('кино')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('абуза')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('абузы')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('query')
                    ->label('URL')
                    ->required()
                    ->maxLength(255)
                    ->default(''),
                Forms\Components\TextInput::make('service')
                    ->label('Сервис')
                    ->required()
                    ->maxLength(30),
                Forms\Components\Toggle::make('handled')
                    ->label('Обработано')
                    ->required(),
                Forms\Components\Toggle::make('handler_error')
                    ->label('Ошибка обработки')
                    ->required(),
                Forms\Components\Textarea::make('handler_message')
                    ->label('Результат обработки')
                    ->columnSpanFull(),
                Forms\Components\DateTimePicker::make('challenged_on_google_at')
                    ->label('Оспорено в Google'),
                Forms\Components\DateTimePicker::make('reinstated_on_google_at')
                    ->label('Восстановлено в Google'),
                Forms\Components\TextInput::make('domain_domain')
                    ->label('Домен')
                    ->maxLength(30)
                    ->default(null),
                Forms\Components\DateTimePicker::make('canceled_on_google_at')
                    ->label('Отклонено в Google'),
            ]);
    }

    public static function table(Table $table): Table
    {
        $user = Auth::user();

        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('created_at', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('created_at')
                    ->label('Добавлено')
                    ->toggleable()
                    ->dateTime('j F Y H:i')
                    ->sortable(),
                Tables\Columns\TextColumn::make('query')
                    ->label('Ссылка')
                    ->limit(60)
                    ->state(fn (Abuse $abuse) => urldecode($abuse->query))
                    ->toggleable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('service')
                    ->label('Сервис')
                    ->badge()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label('Изменено')
                    ->dateTime()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('handler_message')
                    ->label('Результат обработки')
                    ->icon(function (Abuse $abuse) {
                        if ($abuse->handled and $abuse->handler_error) {
                            return 'heroicon-o-x-circle';
                        } elseif ($abuse->handled) {
                            return 'heroicon-o-check-circle';
                        } else {
                            return 'heroicon-o-clock';
                        }
                    })
                    ->iconColor(function (Abuse $abuse) {
                        if ($abuse->handled and $abuse->handler_error) {
                            return 'danger';
                        } elseif ($abuse->handled) {
                            return 'success';
                        } else {
                            return 'gray';
                        }
                    })
                    // если null, то пишем что-то, чтобы иконка часов отображалась
                    ->state(fn (Abuse $abuse) => $abuse->handler_message ?? '...')
                    ->limit(60)
                    ->toggleable(),
                Tables\Columns\TextColumn::make('challenged_on_google_at')
                    ->label('Оспорено в Google')
                    ->toggleable()
                    ->dateTime('j F Y H:i'),
                Tables\Columns\TextColumn::make('reinstated_on_google_at')
                    ->label('Восстановлено в Google')
                    ->toggleable()
                    ->dateTime('j F Y H:i'),
                Tables\Columns\TextColumn::make('domain_domain')
                    ->label('Домен')
                    ->toggleable(),
                Tables\Columns\TextColumn::make('canceled_on_google_at')
                    ->label('Отклонено в Google')
                    ->toggleable()
                    ->dateTime('j F Y H:i'),
            ])
            ->filters([
                Filter::make('massSearch')
                    ->label('Массовый поиск')
                    ->indicateUsing(fn ($data) => $data['text'] ? 'Массовый поиск' : null)
                    ->form([
                        Textarea::make('text')
                            ->label('Массовый поиск')
                            ->rows(5),
                    ])
                    ->query(function (Builder $query, array $data): Builder {

                        $rows = explode(PHP_EOL, trim($data['text'] ?? ''));

                        // убираем сразу пустые строки иначе из-за этого потом ебанина
                        foreach ($rows as $index => $row) {
                            if (empty(trim($row))) {
                                unset($rows[$index]);

                                continue;
                            }
                        }

                        $query->where(function ($query) use ($rows) {
                            foreach ($rows as $index => $url) {

                                $url = Abuse::formatUrl(trim($url));

                                if ($index == 0) {
                                    $query->where('query', 'like', '%'.str_replace(['%', '_'], ['\%', '\_'], $url).'%');
                                } else {
                                    $query->orWhere('query', 'like', '%'.str_replace(['%', '_'], ['\%', '\_'], $url).'%');
                                }
                            }
                        });

                        return $query;
                    }),
                SelectFilter::make('service')
                    ->label('Сервис')
                    ->multiple()
                    ->native(false)
                    ->options(Abuse::distinct()->pluck('service')->sort()->mapWithKeys(fn ($service) => [$service => Str::ucfirst($service)]))
                    ->default(['dmca', 'memorandum']),
                SelectFilter::make('domain.domain')
                    ->label('Домен')
                    ->native(false)
                    ->searchable()
                    ->relationship('domain', 'domain', fn ($query) => $query->has('abuses')),
                TernaryFilter::make('handled')
                    ->label('Обработано')
                    ->native(false),
                TernaryFilter::make('handler_error')
                    ->label('Ошибка обработчика')
                    ->native(false),
                // TernaryFilter::make('to_be_counter_complained')
                // ->label('Подлежат оспариванию')
                // ->attribute(null)
                // ->queries(
                //     true: fn (Builder $query) => $query->where(fn($query) => $query
                //         ->whereRelation('domain.cinemaDomain', 'show_trailer_for_non_sng', true)
                //         ->orWhereRelation('domain.mirrorOf.cinemaDomain', 'show_trailer_for_non_sng', true)
                //     ),
                //     false: fn (Builder $query) => $query->where(fn($query) => $query
                //         ->whereRelation('domain.cinemaDomain', 'show_trailer_for_non_sng', false)
                //         ->orWhereRelation('domain.mirrorOf.cinemaDomain', 'show_trailer_for_non_sng', false)
                //     ),
                // ),
                // Filter::make('toBeCounterComplained')
                //     ->label('Подлежат оспариванию')
                //     ->indicateUsing(fn($data) => $data['toBeCounterComplained'] ? 'Подлежат оспариванию' : null)
                //     ->form([
                //         Filter::make('toBeCounterComplained')
                //             ->label('Подлежат оспариванию')
                //             ->toggle(),
                //         ])
                //     ->query(fn($query, $data) => $query
                //         ->whereRelation('domain.cinemaDomain', 'show_trailer_for_non_sng', true)
                //         ->orWhereRelation('domain.mirrorOf.cinemaDomain', 'show_trailer_for_non_sng', true)
                //     ),
                // SelectFilter::make('show_trailer_for_non_sng')
                // ->label('Show Trailer for Non-SNG')
                // ->options([
                //     '1' => 'Yes',
                //     '0' => 'No',
                // ])
                // ->query(function (Builder $query, array $data) {
                //     if ($data === '1') {
                //         $query
                //             ->whereRelation('domain.cinemaDomain', 'show_trailer_for_non_sng', true)
                //             ->orWhereRelation('domain.mirrorOf.cinemaDomain', 'show_trailer_for_non_sng', true);
                //     } elseif ($data === '0') {
                //         $query
                //             ->whereRelation('domain.cinemaDomain', 'show_trailer_for_non_sng', false)
                //             ->orWhereRelation('domain.mirrorOf.cinemaDomain', 'show_trailer_for_non_sng', false);
                //     }
                // }),
                TernaryFilter::make('challenged_on_google_at')
                    ->label('Оспорено в Google')
                    ->native(false)
                    ->nullable(),
                TernaryFilter::make('reinstated_on_google_at')
                    ->label('Восстановлено в Google')
                    ->native(false)
                    ->nullable(),
                TernaryFilter::make('canceled_on_google_at')
                    ->label('Отклонено в Google')
                    ->native(false)
                    ->nullable(),
            ])
            ->filtersFormColumns(3)
            ->filtersFormSchema(fn ($filters) => [
                Section::make('Массовый поиск')
                    ->schema([
                        $filters['massSearch'],
                    ])
                    ->collapsible()
                    ->collapsed(true),
                $filters['service'],
                $filters['domain.domain'],
                $filters['handled'],
                $filters['handler_error'],
                $filters['challenged_on_google_at'],
                $filters['reinstated_on_google_at'],
                $filters['canceled_on_google_at'],
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->headerActions([

            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    BulkAction::make('setHandledErrorFalse')
                        ->label('Сбросить ошибку обработчика')
                        ->deselectRecordsAfterCompletion()
                        ->authorize($user->can('everything'))
                        ->action(function ($records) {
                            $records->each->update(['handler_error' => false]);
                        }),
                    BulkAction::make('formGoogleCounterComplaint')
                        ->label('Оспорено в Google')
                        ->deselectRecordsAfterCompletion()
                        ->action(function ($records) {
                            $records->each->update(['challenged_on_google_at' => now()]);
                        }),
                    BulkAction::make('setAsReinstatedOnGoogle')
                        ->label('Восстановлено в Google')
                        ->deselectRecordsAfterCompletion()
                        ->authorize($user->can('everything'))
                        ->action(function ($records) {
                            $records->each->update(['reinstated_on_google_at' => now()]);
                        }),
                    BulkAction::make('setAsCanceledOnGoogle')
                        ->label('Отклонено в Google')
                        ->deselectRecordsAfterCompletion()
                        ->authorize($user->can('everything'))
                        ->action(function ($records) {
                            $records->each->update(['canceled_on_google_at' => now()]);
                        }),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAbuses::route('/'),
            // 'bulkCreate' => BulkCreate::route('/bulkCreate'),
            // 'create' => Pages\CreateAbuse::route('/create'),
            // 'edit' => Pages\EditAbuse::route('/{record}/edit'),
        ];
    }
}
