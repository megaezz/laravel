<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\TorrentResource\Pages;
use App\Models\Cinema\Torrent;
use App\Services\Aria2;
use cinema\app\models\eloquent\Dubbing;
use cinema\app\models\eloquent\Movie;
use Filament\Forms;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Form;
use Filament\Notifications\Notification;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

class TorrentResource extends Resource
{
    protected static ?string $model = Torrent::class;

    protected static ?string $navigationIcon = 'heroicon-o-arrow-up-tray';

    public static function getNavigationGroup(): ?string
    {
        return ___('турбо')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('торрент')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('торренты')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                FileUpload::make('src')
                    ->columnSpanFull()
                    ->label('Торрент')
                    ->directory('cinema/torrents')
                    ->required(),
                Forms\Components\Select::make('movie_id')
                    ->label('Фильм')
                    ->placeholder('Название RU/EN, Kinopoisk ID, Imdb ID')
                    ->required()
                    ->searchable()
                    ->relationship('movie')
                    ->live()
                    ->getSearchResultsUsing(
                        fn ($search) => Movie::query()
                            ->withCount('torrents')
                            ->whereNotIn('type', ['youtube'])
                            ->where(fn ($query) => $query
                                ->where('title_ru', 'LIKE', "{$search}%")
                                ->orWhere('title_en', 'LIKE', "{$search}%")
                                ->orWhere('kinopoisk_id', $search)
                                ->orWhere('imdb_id', $search)
                            )
                            ->orderByDesc('torrents_count')
                            ->limit(50)
                            ->get()
                            // здесь отображается вид элементов в поиске
                            ->mapWithKeys(fn ($movie) => [$movie->id => "{$movie->title_ru} / {$movie->title_en} ({$movie->localed_type}, {$movie->year}), тор.: {$movie->torrents_count}"])
                            ->toArray()
                    )
                    // здесь отображается вид уже добавленных элементов (но они мигают между собой я хз в чем там дело)
                    ->getOptionLabelFromRecordUsing(fn ($record) => "{$record->title_ru} ({$record->localed_type}, {$record->year})"),
                Forms\Components\Group::make([
                    Forms\Components\Select::make('dubbing_id')
                        ->label('Дубляж')
                        ->required()
                        ->searchable()
                        ->relationship('dubbing', 'videodb_name')
                        // отображаем количество только мне
                        ->getOptionLabelFromRecordUsing(fn (Dubbing $dubbing) => "{$dubbing->short_or_full_name}".(Auth::user()->can('everything') ? " [{$dubbing->q}]" : null)),
                    Forms\Components\ToggleButtons::make('dubbing_id')
                        ->label('')
                        ->inline()
                        ->options(Dubbing::whereIn('videodb_name', ['Полное дублирование (Dubляж)', '1win Studio', 'Полное дублирование (Nova)', 'Полное дублирование (infinity)'])->get()->mapWithKeys(fn (Dubbing $dubbing) => [$dubbing->id => $dubbing->short_or_full_name])->toArray()),
                ]),
                Forms\Components\TextInput::make('amount')
                    ->visible(fn () => Auth::user()->can('everything'))
                    ->numeric()
                    ->default(null),
                Fieldset::make()
                    ->columns(4)
                    ->visible(fn () => Auth::user()->can('everything'))
                    ->schema([
                        Forms\Components\Toggle::make('is_downloaded'),
                        Forms\Components\Toggle::make('is_cleaned_up'),
                        Forms\Components\Toggle::make('is_handled'),
                        Forms\Components\Toggle::make('is_forced'),
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('created_at', 'desc')
            ->modifyQueryUsing(fn (Builder $query) => $query
                ->withCount('uploads')
                ->withCount(['uploads as existed_uploads_count' => fn ($query) => $query->where('hls_exists', true)])
            )
            ->columns([
                Tables\Columns\TextColumn::make('id')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->searchable(),
                Tables\Columns\ImageColumn::make('movie.poster_or_placeholder')
                    ->label('Poster')
                    ->toggleable()
                    ->checkFileExistence(false)
                    ->state(fn (Torrent $torrent) => $torrent->movie->poster ? 'images/w100/'.$torrent->movie->poster_or_placeholder : null),
                Tables\Columns\TextColumn::make('movie.title_ru')
                    ->label('Фильм')
                    ->limit(30)
                    ->toggleable()
                    ->description(fn (Torrent $torrent) => Str::limit("{$torrent->movie->title_en}, {$torrent->movie->localed_type}, {$torrent->movie->year}", 40))
                    ->searchable(),
                Tables\Columns\TextColumn::make('dubbing.videodb_name')
                    ->label('Дубляж')
                    ->toggleable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('original_filename')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->searchable(),
                Tables\Columns\TextColumn::make('hash')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->searchable(),
                Tables\Columns\TextColumn::make('src')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->limit(25)
                    ->searchable(),
                Tables\Columns\IconColumn::make('is_downloaded')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->label('Загружен')
                    ->boolean(),
                Tables\Columns\IconColumn::make('is_cleaned_up')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->boolean(),
                Tables\Columns\TextColumn::make('download_status')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->state(fn (Torrent $torrent) => $torrent->aria2->downloadStatus()),
                Tables\Columns\TextColumn::make('downloaded_movies')
                    ->label('Movies')
                    ->toggleable()
                    ->badge()
                    ->state(fn (Torrent $torrent) => count($torrent->aria2->downloadedMovies())),
                Tables\Columns\TextColumn::make('selected_movies')
                    ->label('Selected')
                    ->toggleable()
                    ->badge()
                    ->state(fn (Torrent $torrent) => count($torrent->aria2->selectedMovies())),
                Tables\Columns\TextColumn::make('uploads_count')
                    ->label('Uploads')
                    ->toggleable()
                    ->badge(),
                Tables\Columns\TextColumn::make('existed_uploads_count')
                    ->label('Existed')
                    ->toggleable()
                    ->badge(),
                Tables\Columns\IconColumn::make('is_handled')
                    ->label('Обработано')
                    ->toggleable()
                    ->boolean(),
                Tables\Columns\IconColumn::make('is_forced')
                    ->toggleable()
                    ->boolean(),
                Tables\Columns\TextColumn::make('amount')
                    ->money('USD')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('created_at')
                    ->label('Дата создания')
                    ->dateTime('j F Y, H:i:s')
                    ->toggleable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                TernaryFilter::make('is_downloaded')
                    ->label('Загружен')
                    ->native(false),
                TernaryFilter::make('is_handled')
                    ->label('Обработан')
                    ->native(false),
                SelectFilter::make('dubbing')
                    ->label('Дубляж')
                    ->native(false)
                    ->relationship('dubbing', 'videodb_name', fn ($query) => $query->whereHas('torrents')),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\Action::make('load')
                    ->authorize(fn () => Auth::user()->can('everything'))
                    ->label('Загрузить')
                    ->visible(fn (Torrent $torrent) => ! $torrent->is_downloaded and ! $torrent->aria2_gid)
                    ->action(function (Torrent $torrent) {
                        $gid = $torrent->aria2->download();
                        Notification::make()
                            ->title('Торрент')
                            ->body("Начали загрузку торрента, gid: {$gid}")
                            ->send();
                    }),
                Tables\Actions\Action::make('createMovieUploads')
                    ->label('Создать загрузки')
                    // TODO: отображать когда торрент загружен, но всех нужных загрузок нет
                    ->authorize(fn () => Auth::user()->can('everything'))
                    ->visible(fn (Torrent $torrent) => ! $torrent->is_handled and $torrent->is_downloaded and ! $torrent->uploads_count)
                    ->modalContent(function (Torrent $torrent, $action) {

                        try {
                            $actions = $torrent->createMovieUploads();
                        } catch (\Exception $e) {
                            Notification::make()
                                ->title('Создание загрузок')
                                ->body($e->getMessage())
                                ->warning()
                                ->send();

                            return;
                        }

                        $formattedActions =
                        '<ol class="list-decimal">'.
                        $actions->map(fn ($fileActions, $file) => "<li>
                                {$file}:
                                <ul class=\"list-disc\">".
                                collect($fileActions)->map(fn ($fileAction) => "<li>{$fileAction}</li>")->implode('').
                                '</ul>
                            </li>')->implode('').
                        '</ol>';

                        return new HtmlString($formattedActions);
                    }),
                Tables\Actions\Action::make('cleanup_files')
                    ->label('Cleanup files')
                    ->authorize(fn () => Auth::user()->can('everything'))
                    ->visible(fn (Torrent $torrent) => $torrent->aria2_content_data and ! $torrent->is_downloaded)
                    ->action(function (Torrent $torrent) {
                        try {
                            $result = $torrent->aria2->selectOnlyNeededFiles();
                        } catch (\Exception $e) {
                            $result = $e->getMessage();
                        }

                        Notification::make()
                            ->title('Cleanup')
                            ->body(fn () => is_array($result) ? json_encode($result) : $result)
                            ->send();
                    }),
                Tables\Actions\Action::make('remove_downloads')
                    ->label('Удалить загрузки')
                    ->authorize(fn () => Auth::user()->can('everything'))
                    ->visible(fn (Torrent $torrent) => $torrent->aria2_gid)
                    ->action(function (Torrent $torrent) {
                        Notification::make()
                            ->title('Результат удаления загрузок')
                            ->body(fn () => $torrent->aria2->remove()->implode(PHP_EOL))
                            ->send();
                    }),
                // Tables\Actions\CreateAction::make('create_same')
                //     ->label('Create same')
                //     ->form([
                //         FileUpload::make('src')
                //             ->columnSpanFull()
                //             ->label('Торрент')
                //             ->directory('cinema/torrents')
                //             ->required(),
                //         Forms\Components\TextInput::make('movie_id')
                //             ->label('Фильм')
                //             ->disabled()
                //         ]),
                // Tables\Actions\CreateAction::make('create_same')
                //     ->label('Новый торрент')
                //     ->authorize(fn () => Auth::user()->can('everything'))
                //     ->visible(fn (Torrent $torrent) => $torrent->is_handled)
                //     ->record(function (Torrent $torrent) {
                //         $newTorrent = new Torrent;
                //         $newTorrent->dubbing_id = $torrent->dubbing_id;
                //         $newTorrent->movie_id = $torrent->movie_id;
                //         dd($newTorrent);
                //         return $newTorrent;
                //     }),
                // ->mutateFormDataUsing(function (array $data, Torrent $torrent) {
                //     $data[]
                // }),
                // ->action(function (Torrent $torrent) {

                // }),
                DeleteAction::make(),
                // Tables\Actions\Action::make('sample_url')
                //     ->label('На сайте')
                //     // TODO: добавить условие, что если еще нет на сайте - кнопка неактивна
                //     ->visible(fn (Torrent $torrent) => $torrent->is_handled)
                //     ->disabled(fn (Torrent $torrent) => ! $torrent->movie->sample_url)
                //     ->url(fn (Torrent $torrent) => $torrent->movie->sample_url),
                // Tables\Actions\Action::make('status')
                //     ->label('Статус')
                //     ->visible(fn (Torrent $torrent) => $torrent->aria2_gid)
                //     ->action(function (Torrent $torrent) {
                //         $aria2 = new Aria2('http://aria2:6800/jsonrpc');
                //         $results = collect();
                //         $result = $aria2->status($torrent->aria2_gid);
                //         $results[$result['result']['gid']] = $result['result']['status'];
                //         if (isset($result['result']['followedBy'][0])) {
                //             $result = $aria2->status($result['result']['followedBy'][0]);
                //             $results[$result['result']['gid']] = $result['result']['status'];
                //         }
                //         Notification::make()
                //             ->title('Торрент')
                //             ->body($results->map(fn ($status, $gid) => "{$gid}: {$status}")->implode(PHP_EOL))
                //             ->send();
                //     }),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTorrents::route('/'),
            // 'create' => Pages\CreateTorrent::route('/create'),
            // 'edit' => Pages\EditTorrent::route('/{record}/edit'),
        ];
    }
}
