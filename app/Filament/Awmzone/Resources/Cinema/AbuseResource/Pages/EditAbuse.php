<?php

namespace App\Filament\Awmzone\Resources\Cinema\AbuseResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\AbuseResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditAbuse extends EditRecord
{
    protected static string $resource = AbuseResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
