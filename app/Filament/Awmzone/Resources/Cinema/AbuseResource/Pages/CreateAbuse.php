<?php

namespace App\Filament\Awmzone\Resources\Cinema\AbuseResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\AbuseResource;
use Filament\Resources\Pages\CreateRecord;

class CreateAbuse extends CreateRecord
{
    protected static string $resource = AbuseResource::class;
}
