<?php

namespace App\Filament\Awmzone\Resources\Cinema\AbuseResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\AbuseResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;

class ListAbuses extends ListRecords
{
    protected static string $resource = AbuseResource::class;

    protected function paginateTableQuery(Builder $query): Paginator
    {
        return $query->simplePaginate(($this->getTableRecordsPerPage() === 'all') ? $query->count() : $this->getTableRecordsPerPage());
    }

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            BulkCreate::bulkCreateForm(),
        ];
    }
}
