<?php

namespace App\Filament\Awmzone\Resources\Cinema\AbuseResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\AbuseResource;
use App\Models\Abuse;
use App\Services\Cinema\AddAbusesFromEmails;
use Filament\Actions\Action;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\ToggleButtons;
use Filament\Notifications\Notification;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Support\Str;

class BulkCreate extends CreateRecord
{
    protected static string $resource = AbuseResource::class;

    public static function bulkCreateForm()
    {
        return Action::make('Добавить списком')
            ->label('Добавить списком')
            ->button()
            ->icon('heroicon-o-plus-circle')
            ->action(function (array $data) {
                try {
                    $result = (new AddAbusesFromEmails)($data['service'], $data['text']);
                } catch (\Exception $e) {
                    Notification::make()
                        ->title('Ошибка при добавлении')
                        ->body($e->getMessage())
                        ->warning()
                        ->send();

                    return;
                }

                Notification::make()
                    ->title('Добавлены абузы '.mb_ucfirst($data['service']))
                    ->body($result)
                    ->success()
                    ->send();
            })
            ->form([
                Select::make('service')
                    ->label('Сервис')
                    ->native(false)
                    ->searchable()
                    ->options(Abuse::distinct()->pluck('service')->sort()->mapWithKeys(fn ($service) => [$service => Str::ucfirst($service)]))
                    ->required(),
                ToggleButtons::make('service')
                    ->label('')
                    ->inline()
                    ->options(collect(['ркн', 'dmca', 'memorandum', 'group-ib', 'kinopoisk', 'isola', 'starmedia', 'icm', 'прав'])->mapWithKeys(fn ($service) => [$service => Str::ucfirst($service)])),
                Textarea::make('text')
                    ->label('Текст')
                    ->rows(20)
                    ->required(),
            ]);
    }
}
