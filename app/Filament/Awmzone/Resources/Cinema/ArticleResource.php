<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\ArticleResource\Pages;
use App\Models\Cinema\Article;
use App\Models\Domain;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class ArticleResource extends Resource
{
    protected static ?string $model = Article::class;

    protected static ?string $navigationIcon = 'heroicon-o-newspaper';

    public static function getNavigationGroup(): ?string
    {
        return ___('кино')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('статья')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('статьи')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->columns(12)
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->required()
                    ->label('Заголовок')
                    ->columnSpanFull()
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Select::make('domain')
                    ->label('Домен')
                    ->columnSpan(3)
                    ->relationship(
                        name: 'engineDomain',
                        modifyQueryUsing: fn (Builder $query) => $query->where('type', 'cinema'),
                    )
                    ->searchable()
                    ->preload()
                    ->getOptionLabelFromRecordUsing(fn (Domain $domain) => $domain->domain_decoded)
                    ->default(null),
                Forms\Components\TextInput::make('domain_movie_id')
                    ->columnSpan(2)
                    ->numeric()
                    // ->relationship(
                    //     name: 'domainMovie',
                    //     modifyQueryUsing: fn (Builder $query) => $query->where('type', 'cinema'),
                    // )
                    // ->searchable()
                    // ->preload()
                    // ->getOptionLabelFromRecordUsing(fn (Domain $domain) => $domain->domain_decoded)
                    ->default(null),
                Forms\Components\Select::make('acceptor')
                    ->label('Акцептор')
                    ->columnSpan(3)
                    ->relationship(
                        name: 'acceptorDomain',
                        modifyQueryUsing: fn (Builder $query) => $query->where('type', 'cinema'),
                    )
                    ->searchable()
                    ->preload()
                    ->getOptionLabelFromRecordUsing(fn (Domain $domain) => $domain->domain_decoded)
                    ->default(null),
                Forms\Components\TextInput::make('chpu')
                    ->label('ЧПУ')
                    ->columnSpan(3)
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Toggle::make('active')
                    ->label('Активно')
                    ->columnSpan(1)
                    ->inline(false),
                Forms\Components\FileUpload::make('main_image_src')
                    ->label('Главная картинка')
                    ->columnSpan(6)
                    ->directory('cinema/images/articles')
                    ->image(),
                Forms\Components\Textarea::make('text_preview')
                    ->label('Превью текста')
                    ->columnSpan(6)
                    ->rows(5),
                Forms\Components\RichEditor::make('text')
                    ->required()
                    ->label('Текст')
                    ->fileAttachmentsDirectory('cinema/images/articles')
                    ->columnSpanFull(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('add_date', 'desc')
            ->columns([
                Tables\Columns\ImageColumn::make('main_image_src')
                    ->label(''),
                // отключаем проверку существования файла, т.к. изображение генерится на лету
                // ->checkFileExistence(false)
                // ->state(fn(Article $article) => $article->main_image_src ? 'images/w200/' . $article->main_image_src : null),
                // Tables\Columns\IconColumn::make('active')
                //     ->boolean(),
                Tables\Columns\TextColumn::make('title')
                    ->limit(50)
                    ->description(fn (Article $article) => Str::limit(htmlspecialchars_decode(strip_tags($article->text)), 50))
                    ->searchable(),
                Tables\Columns\TextColumn::make('add_date')
                    ->label('Создано')
                    ->toggleable()
                    ->dateTime('j F Y H:i')
                    ->sortable(),
                Tables\Columns\TextColumn::make('apply_date')
                    ->label('Добавлено на сайт')
                    ->icon('heroicon-o-check-circle')
                    ->iconColor('success')
                    ->state(fn (Article $article) => $article->apply_date?->translatedFormat('j F Y')),
                // Tables\Columns\TextColumn::make('url')
                //     ->searchable(),
                Tables\Columns\TextColumn::make('engineDomain.domain_decoded')
                    ->label('Домен')
                    ->badge()
                    ->searchable(),
                Tables\Columns\TextColumn::make('chpu')
                    ->label('ЧПУ')
                    ->toggleable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->label('Обновлено')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListArticles::route('/'),
            'create' => Pages\CreateArticle::route('/create'),
            'edit' => Pages\EditArticle::route('/{record}/edit'),
        ];
    }
}
