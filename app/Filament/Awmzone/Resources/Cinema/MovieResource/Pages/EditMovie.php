<?php

namespace App\Filament\Awmzone\Resources\Cinema\MovieResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\MovieResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMovie extends EditRecord
{
    protected static string $resource = MovieResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
