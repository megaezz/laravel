<?php

namespace App\Filament\Awmzone\Resources\Cinema\MovieResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\MovieResource;
use Filament\Resources\Pages\CreateRecord;

class CreateMovie extends CreateRecord
{
    protected static string $resource = MovieResource::class;
}
