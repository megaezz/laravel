<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\MovieResource\Pages;
use cinema\app\models\eloquent\Movie;
use Filament\Forms;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Table;

class MovieResource extends Resource
{
    protected static ?string $model = Movie::class;

    protected static ?string $navigationIcon = 'heroicon-o-film';

    public static function getNavigationGroup(): ?string
    {
        return ___('кино')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('фильм')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('фильмы')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Fieldset::make('Основное')
                    ->schema([
                        Forms\Components\TextInput::make('title_ru')
                            ->required()
                            ->maxLength(255)
                            ->default(null),
                        Forms\Components\TextInput::make('title_en')
                            ->required()
                            ->maxLength(255)
                            ->default(null),
                        Forms\Components\TextInput::make('year')
                            ->required(),
                        Forms\Components\Select::make('type')
                            ->required()
                            ->native(false)
                            ->options([
                                'movie' => 'Фильм',
                                'serial' => 'Сериал',
                                'trailer' => 'Трейлер',
                                'youtube' => 'Youtube',
                            ]),
                        Forms\Components\TextInput::make('kinopoisk_id')
                            ->maxLength(9)
                            ->default(null),
                        Forms\Components\TextInput::make('imdb_id')
                            ->maxLength(255)
                            ->default(null),
                    ]),
                Forms\Components\TextInput::make('mw_unique_id')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('copy_poster_url')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Toggle::make('poster')
                    ->required(),
                Forms\Components\DateTimePicker::make('added_at'),
                Forms\Components\Toggle::make('deleted')
                    ->required(),
                Forms\Components\DateTimePicker::make('add_date')
                    ->default(now())
                    ->required(),
                Forms\Components\Toggle::make('checked')
                    ->required(),
                Forms\Components\Toggle::make('mw_deleted')
                    ->required(),
                Forms\Components\TextInput::make('kinopoisk_rating')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('kinopoisk_votes')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('imdb_rating')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('imdb_votes')
                    ->numeric()
                    ->default(null),
                Forms\Components\Textarea::make('description')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('kinopoisk_rating_sko')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('imdb_rating_sko')
                    ->numeric()
                    ->default(null),
                Forms\Components\Toggle::make('rkn_block')
                    ->required(),
                Forms\Components\TextInput::make('embed')
                    ->maxLength(255)
                    ->default(''),
                Forms\Components\TextInput::make('embed_trailer')
                    ->maxLength(255)
                    ->default(''),
                Forms\Components\TextInput::make('direct')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Toggle::make('do_update')
                    ->required(),
                Forms\Components\TextInput::make('rating_sko')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('myshows_id')
                    ->numeric()
                    ->default(null),
                Forms\Components\TextInput::make('status')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\DatePicker::make('started'),
                Forms\Components\DatePicker::make('ended'),
                Forms\Components\TextInput::make('age')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('source_type')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('tagline')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Textarea::make('lordDescription')
                    ->columnSpanFull(),
                Forms\Components\Toggle::make('hdvb_4k')
                    ->required(),
                Forms\Components\TextInput::make('mainPlayer'),
                Forms\Components\TextInput::make('blockStr')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Toggle::make('collaps')
                    ->required(),
                Forms\Components\Toggle::make('videodb')
                    ->required(),
                Forms\Components\Toggle::make('videocdn')
                    ->required(),
                Forms\Components\TextInput::make('youtube_id')
                    ->maxLength(30)
                    ->default(null),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('id', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('id')
                    ->searchable()
                    ->sortable()
                    ->toggleable(),
                Tables\Columns\ImageColumn::make('poster_or_placeholder')
                    ->label('Poster')
                    ->toggleable()
                    ->checkFileExistence(false)
                    ->state(fn ($record) => $record->poster ? 'images/w100/'.$record->poster_or_placeholder : null),
                Tables\Columns\TextColumn::make('title_ru')
                    ->searchable()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('title_en')
                    ->searchable()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('kinopoisk_id')
                    ->searchable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('imdb_id')
                    ->searchable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('mw_unique_id')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('copy_poster_url')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('added_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('year')
                    ->toggleable(),
                Tables\Columns\IconColumn::make('deleted')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('add_date')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('checked')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('mw_deleted')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('kinopoisk_rating')
                    ->numeric()
                    ->sortable()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('kinopoisk_votes')
                    ->numeric()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('imdb_rating')
                    ->numeric()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('imdb_votes')
                    ->numeric()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('type')
                    ->toggleable(),
                Tables\Columns\TextColumn::make('kinopoisk_rating_sko')
                    ->numeric()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('imdb_rating_sko')
                    ->numeric()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('rkn_block')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('embed')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('embed_trailer')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('direct')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('do_update')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('rating_sko')
                    ->numeric()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('myshows_id')
                    ->numeric()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('status')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('started')
                    ->date()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('ended')
                    ->date()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('age')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('source_type')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('tagline')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('hdvb_4k')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('mainPlayer')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('blockStr')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('collaps')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('videodb')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('videocdn')
                    ->boolean()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('imdb_id')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('youtube_id')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                SelectFilter::make('type')
                    ->multiple()
                    ->label('Тип')
                    ->native(false)
                    ->options(Movie::distinct()->pluck('type', 'type')->sort()),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Action::make('domainMovies')
                    ->label('На доменах')
                    ->url(fn (Movie $record): string => route('filament.awmzone.resources.cinema.domain-movies.index', ['tableFilters' => ['movie_id' => ['value' => $record->id]]])),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMovies::route('/'),
            // 'create' => Pages\CreateMovie::route('/create'),
            // 'edit' => Pages\EditMovie::route('/{record}/edit'),
        ];
    }
}
