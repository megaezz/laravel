<?php

namespace App\Filament\Awmzone\Resources\Cinema\DomainMovieResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\DomainMovieResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;

class ListDomainMovies extends ListRecords
{
    protected static string $resource = DomainMovieResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

    protected function paginateTableQuery(Builder $query): Paginator
    {
        return $query->simplePaginate(($this->getTableRecordsPerPage() === 'all') ? $query->count() : $this->getTableRecordsPerPage());
    }
}
