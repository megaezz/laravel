<?php

namespace App\Filament\Awmzone\Resources\Cinema\DomainMovieResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\DomainMovieResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDomainMovie extends CreateRecord
{
    protected static string $resource = DomainMovieResource::class;
}
