<?php

namespace App\Filament\Awmzone\Resources\Cinema\DomainMovieResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\DomainMovieResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDomainMovie extends EditRecord
{
    protected static string $resource = DomainMovieResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
