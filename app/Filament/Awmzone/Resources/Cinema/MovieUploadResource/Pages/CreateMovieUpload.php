<?php

namespace App\Filament\Awmzone\Resources\Cinema\MovieUploadResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\MovieUploadResource;
use Filament\Resources\Pages\CreateRecord;

class CreateMovieUpload extends CreateRecord
{
    protected static string $resource = MovieUploadResource::class;
}
