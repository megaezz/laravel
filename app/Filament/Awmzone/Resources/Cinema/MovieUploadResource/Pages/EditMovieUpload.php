<?php

namespace App\Filament\Awmzone\Resources\Cinema\MovieUploadResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\MovieUploadResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditMovieUpload extends EditRecord
{
    protected static string $resource = MovieUploadResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
