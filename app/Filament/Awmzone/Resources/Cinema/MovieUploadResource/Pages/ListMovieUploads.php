<?php

namespace App\Filament\Awmzone\Resources\Cinema\MovieUploadResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\MovieUploadResource;
use App\Filament\Awmzone\Resources\Cinema\MovieUploadResource\Widgets\TurboStats;
use App\Filament\Exports\Cinema\MovieUploadExporter;
use Filament\Actions;
use Filament\Actions\ExportAction;
use Filament\Resources\Pages\ListRecords;

class ListMovieUploads extends ListRecords
{
    protected static string $resource = MovieUploadResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
            ExportAction::make()
                ->label('Экспорт')
                ->icon('heroicon-o-arrow-down-tray')
                ->exporter(MovieUploadExporter::class)
                ->chunkSize(10),
        ];
    }

    protected function getHeaderWidgets(): array
    {
        return [
            TurboStats::class,
        ];
    }
}
