<?php

namespace App\Filament\Awmzone\Resources\Cinema\MovieUploadResource\Widgets;

use App\Models\Cinema\MovieUpload;
use App\Models\User;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;
use Illuminate\Support\Facades\Auth;

class TurboStats extends BaseWidget
{
    protected function getStats(): array
    {
        $sum = (float) MovieUpload::where('hls_exists', true)->sum('amount');
        $paid = (float) User::where('email', 'mas90@mail.ru')->first()->balanceFloat;
        // избегаем неточностей при работе с float
        $debt = round($sum - $paid, 2);

        return [
            Stat::make('Всего', "{$sum} $"),
            Stat::make('Оплачено', "{$paid} $"),
            Stat::make('Долг', "{$debt} $"),
        ];
    }

    public static function canView(): bool
    {
        return Auth::user()->can('view awmzone turbostats widget');
    }
}
