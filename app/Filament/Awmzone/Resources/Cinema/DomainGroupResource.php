<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\DomainGroupResource\Pages;
use App\Models\Cinema\DomainGroup;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;

class DomainGroupResource extends Resource
{
    protected static ?string $model = DomainGroup::class;

    protected static ?string $navigationIcon = 'heroicon-o-table-cells';

    public static function getNavigationGroup(): ?string
    {
        return ___('кино')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('группа на сайте')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('группы на сайтах')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\FileUpload::make('img')
                    ->label('Картинка')
                    ->directory('cinema/images/groups')
                    ->columnSpanFull()
                    ->image(),
                Forms\Components\Select::make('domain')
                    ->label('Домен')
                    ->relationship('cinemaDomain', 'domain')
                    ->searchable()
                    ->preload()
                    ->required(),
                Forms\Components\Select::make('group_id')
                    ->label('Группа')
                    ->relationship('group', 'name')
                    ->searchable()
                    ->preload()
                    ->required(),
                Forms\Components\TextInput::make('h1')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('title')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\RichEditor::make('text')
                    ->required()
                    ->label('Текст')
                    ->fileAttachmentsDirectory('cinema/images/groups')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('alt_id')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('dmca')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\Toggle::make('active')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('created_at', 'desc')
            ->columns([
                Tables\Columns\ImageColumn::make('img'),
                Tables\Columns\TextColumn::make('domain')
                    ->searchable(),
                Tables\Columns\TextColumn::make('group.name')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('h1'),
                Tables\Columns\TextColumn::make('title'),
                Tables\Columns\IconColumn::make('active')
                    ->boolean(),
                Tables\Columns\TextColumn::make('alt_id'),
                Tables\Columns\TextColumn::make('dmca'),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                SelectFilter::make('domain')
                    ->label('Домен')
                    ->searchable()
                    ->preload()
                    ->relationship('cinemaDomain', 'domain'),
                SelectFilter::make('group_id')
                    ->label('Группа')
                    ->searchable()
                    ->preload()
                    ->relationship('group', 'name'),
                TernaryFilter::make('active')
                    ->native(false),
                TernaryFilter::make('text')
                    ->label('С текстом')
                    ->native(false)
                    ->nullable(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDomainGroups::route('/'),
            // 'create' => Pages\CreateDomainGroup::route('/create'),
            // 'edit' => Pages\EditDomainGroup::route('/{record}/edit'),
        ];
    }
}
