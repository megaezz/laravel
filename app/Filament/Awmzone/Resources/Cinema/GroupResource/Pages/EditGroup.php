<?php

namespace App\Filament\Awmzone\Resources\Cinema\GroupResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\GroupResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditGroup extends EditRecord
{
    protected static string $resource = GroupResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
