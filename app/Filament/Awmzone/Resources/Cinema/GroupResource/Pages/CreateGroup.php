<?php

namespace App\Filament\Awmzone\Resources\Cinema\GroupResource\Pages;

use App\Filament\Awmzone\Resources\Cinema\GroupResource;
use Filament\Resources\Pages\CreateRecord;

class CreateGroup extends CreateRecord
{
    protected static string $resource = GroupResource::class;
}
