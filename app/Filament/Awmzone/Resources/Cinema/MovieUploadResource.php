<?php

namespace App\Filament\Awmzone\Resources\Cinema;

use App\Filament\Awmzone\Resources\Cinema\MovieUploadResource\Pages;
use App\Models\Cinema\MovieUpload;
use App\Models\Cinema\Torrent;
use App\Services\Cinema\Turbo\LinksWhereThisUploadIsPrioritized;
use cinema\app\models\eloquent\Dubbing;
use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\MovieEpisode;
use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Form;
use Filament\Forms\Get;
use Filament\Notifications\Notification;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Grouping\Group;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

class MovieUploadResource extends Resource
{
    protected static ?string $model = MovieUpload::class;

    protected static ?string $navigationIcon = 'heroicon-o-folder-arrow-down';

    public static function getNavigationGroup(): ?string
    {
        return ___('турбо')->ucfirst();
    }

    public static function getModelLabel(): string
    {
        return ___('загрузка')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('загрузки')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('torrent_id')
                    ->label('Торрент')
                    ->preload(10)
                    ->searchable()
                    ->relationship(
                        name: 'torrent',
                        modifyQueryUsing: function ($query, $operation) {
                            if ($operation === 'create') {
                                return $query
                                    ->where('is_handled', false)
                                    ->where('is_downloaded', true)
                                    ->orderByDesc('id');
                            } else {
                                return $query->orderByDesc('id');
                            }
                        }
                    )
                    ->live()
                    ->afterStateUpdated(function ($state, $set) {
                        if (! $state) {
                            return;
                        }
                        $torrent = Torrent::find($state);
                        $set('movie_id', $torrent->movie_id);
                        $set('dubbing_id', $torrent->dubbing_id);
                    })
                    // потому что name - аксессор
                    ->getOptionLabelFromRecordUsing(fn ($record) => $record->name),
                Forms\Components\Select::make('dubbing_id')
                    ->label('Дубляж')
                    ->required()
                    ->searchable(['name', 'videodb_name'])
                    ->relationship('dubbing')
                    ->getOptionLabelFromRecordUsing(fn (Dubbing $dubbing) => "{$dubbing->short_or_full_name} [{$dubbing->q}]"),
                Forms\Components\Select::make('movie_id')
                    ->label('Фильм')
                    ->placeholder('Название или Kinopoisk ID')
                    ->searchable(['title_ru', 'kinopoisk_id'])
                    ->relationship(name: 'movie', modifyQueryUsing: fn ($query) => $query->whereNotIn('type', ['youtube']))
                    ->getOptionLabelFromRecordUsing(fn (Movie $movie) => "{$movie->title_ru} ({$movie->localed_type}, {$movie->year})"),
                Forms\Components\Select::make('movie_episode_id')
                    ->label('Эпизод')
                    ->native(false)
                    ->preload(10)
                    // ->optionsLimit(1000)
                    ->relationship(
                        name: 'episode',
                        modifyQueryUsing: fn ($query, $get) => $get('movie_id')
                            ? $query
                                ->where('movie_id', $get('movie_id'))
                                ->orderBy('season')
                                ->orderBy('episode')
                            : $query->whereRaw('1 = 0')
                    )
                    ->getOptionLabelFromRecordUsing(fn (MovieEpisode $episode) => $episode->season_episode),
                Forms\Components\Select::make('source_video')
                    ->native(false)
                    ->options(
                        function (Get $get, $operation) {
                            if (! $get('torrent_id')) {
                                return null;
                            }
                            $torrent = Torrent::findOrFail($get('torrent_id'));
                            // исключаем файлы уже выбранные в других uploads
                            if ($operation == 'create') {
                                $excludeFiles = $torrent->uploads()->pluck('source_video');
                            } else {
                                $excludeFiles = $torrent->uploads()->where('id', '!=', $get('id'))->pluck('source_video');
                            }

                            return $torrent?->aria2->downloadedMovies()
                                ->diff($excludeFiles)
                                ->sort()
                                ->mapWithKeys(fn ($value) => [$value => basename($value)]);
                        }
                    ),
                Forms\Components\Select::make('quality')
                    ->options(['720' => '720p', '1080' => '1080p'])
                    ->native(false)
                    ->default(null),
                // Toggle::make('active'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->defaultPaginationPageOption(100)
            ->defaultSort('created_at', 'desc')
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultGroup('movieAndStudio')
            // скрываем опцию выбора направления сортировки, т.к. захардкоржено всегда по убыванию (т.к. нельзя указать дефолтную сортировку, пришлось сделать так)
            ->groupingDirectionSettingHidden()
            ->groups([
                Group::make('movieAndStudio')
                    ->label('Фильм и студия')
                    ->groupQueryUsing(fn (Builder $query) => $query->groupBy('movie_id, dubbing_id'))
                    ->orderQueryUsing(fn (Builder $query, string $direction) => $query
                        ->orderByDesc('created_at')
                    )
                    ->getTitleFromRecordUsing(fn (MovieUpload $movieUpload): string => collect([
                        $movieUpload->movie->title_ru,
                        $movieUpload->movie->title_en,
                        $movieUpload->movie->localed_type,
                        $movieUpload->movie->year,
                        $movieUpload->dubbing->videodb_name,
                        // пустой фильтр убирает пустые значения
                    ])->filter()->implode(' / ')),
                // ->getDescriptionFromRecordUsing(fn (MovieUpload $movieUpload): string => "{$movieUpload->dubbing->videodb_name}")
            ])
            ->columns([
                Tables\Columns\TextColumn::make('id')
                    ->label('ID')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->sortable()
                    ->searchable(),
                Tables\Columns\ImageColumn::make('movie.poster_or_placeholder')
                    ->label('Poster')
                    ->toggleable()
                    ->checkFileExistence(false)
                    ->state(fn (MovieUpload $movieUpload) => $movieUpload->movie->poster ? 'images/w100/'.$movieUpload->movie->poster_or_placeholder : null),
                Tables\Columns\TextColumn::make('movie.title_ru')
                    ->label('Фильм')
                    ->limit(30)
                    ->description(fn (MovieUpload $movieUpload) => Str::limit("{$movieUpload->movie->localed_type}, {$movieUpload->movie->year}".($movieUpload->episode ? ", {$movieUpload->episode->season_episode}" : null), 40))
                    ->searchable(),
                Tables\Columns\TextColumn::make('movie.imdb_id')
                    ->label('Imdb Id')
                    ->toggleable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('movie.kinopoisk_id')
                    ->label('Kinopoisk Id')
                    ->toggleable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('dubbing.videodb_name')
                    ->label('Студия')
                    ->toggleable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('torrent.name')
                    ->label('Торрент')
                    // ->action(
                    //     Tables\Actions\Action::make('goToTorrent')
                    //         ->url(fn ($record) => route('filament.awmzone.resources.torrent.view', ['record' => $record->torrent_id]))
                    // )
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('source_video')
                    ->toggleable(isToggledHiddenByDefault: true)
                    ->searchable(),
                Tables\Columns\TextColumn::make('quality')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('hls')
                    ->label('HLS')
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('is_transcoding')
                    ->boolean()
                    ->toggleable(),
                Tables\Columns\IconColumn::make('hls_exists')
                    ->label('Выложено')
                    ->boolean()
                    ->toggleable(),
                // Tables\Columns\TextColumn::make('size')
                //     ->label('Размер')
                //     ->state(fn (MovieUpload $movieUpload) => "{$movieUpload->size} Gb")
                //     ->toggleable(),
                Tables\Columns\TextColumn::make('amount')
                    ->label('Тариф')
                    ->money('USD')
                    ->toggleable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                SelectFilter::make('id')
                    ->preload()
                    ->options(MovieUpload::orderBy('id')->pluck('id', 'id')->toArray())
                    ->visible(fn () => Auth::user()->can('everything'))
                    ->multiple()
                    ->native(false),
                SelectFilter::make('dubbing')
                    ->label('Студия')
                    ->multiple()
                    ->preload()
                    ->native(false)
                    // TODO: whereHas('uploads') наверное сделать надо, затестить
                    ->relationship('dubbing', 'videodb_name', fn ($query) => $query->whereHas('torrents')),
                TernaryFilter::make('hls_exists')
                    ->label('Выложено')
                    ->native(false),
                SelectFilter::make('movie.type')
                    ->query(fn ($query, $data) => $data['value'] ? $query->whereRelation('movie', 'type', $data['value']) : null)
                    ->label('Тип контента')
                    ->native(false)
                    ->options(['movie' => 'Фильм', 'serial' => 'Сериал']),
                SelectFilter::make('quality')
                    ->visible(fn () => Auth::user()->can('everything'))
                    ->label('Качество')
                    ->native(false)
                    ->options(['720' => '720p', '1080' => '1080p']),
                SelectFilter::make('torrent')
                    ->visible(fn () => Auth::user()->can('everything'))
                    ->label('Торрент')
                    ->multiple()
                    ->native(false)
                    ->relationship('torrent', 'id'),
                Filter::make('created_at_range')
                    ->form([
                        DatePicker::make('from')
                            ->label('С даты')
                            ->displayFormat('j F Y'),
                        // ->native(false),
                        DatePicker::make('to')
                            ->label('По дату')
                            ->displayFormat('j F Y'),
                        // ->native(false),
                    ])
                    ->query(function ($query, array $data) {
                        return $query
                            ->when($data['from'] ?? null, fn ($q, $date) => $q->whereDate('created_at', '>=', $date))
                            ->when($data['to'] ?? null, fn ($q, $date) => $q->whereDate('created_at', '<=', $date));
                    }),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\Action::make('transcode')
                    ->label('Транскодировать')
                    ->authorize(fn () => Auth::user()->can('everything'))
                    ->visible(fn (MovieUpload $movieUpload) => ! $movieUpload->hls_exists and ! $movieUpload->is_transcoding)
                    ->action(function (MovieUpload $movieUpload) {

                        $user = Auth::user();

                        // отправляем в фон, т.к. у www-data нет .ssh ключей (подробнее в stack.yml, в комментах к php-fpm)
                        dispatch(function () use ($user, $movieUpload) {
                            try {
                                $movieUpload->transcode();
                                Notification::make()->body("Транскодирование #{$movieUpload->id} запущено")->success()->sendToDatabase($user);
                            } catch (\Exception $e) {
                                Notification::make()->body("Ошибка постановки на транскодирование #{$movieUpload->id}: {$e->getMessage()}")->warning()->sendToDatabase($user);
                            }
                        });

                        Notification::make()->body('Команда отправлена')->send();
                    }),
                Tables\Actions\Action::make('links')
                    ->label('Ссылки')
                    ->visible(fn (MovieUpload $movieUpload) => $movieUpload->hls_exists)
                    // ->disabled(fn(MovieUpload $movieUpload) => !$movieUpload->movie->sample_url)
                    ->modalContent(function (MovieUpload $movieUpload) {

                        try {
                            $links = (new LinksWhereThisUploadIsPrioritized)($movieUpload);
                        } catch (\Exception $e) {
                            Notification::make()
                                ->title('Ссылки')
                                ->body($e->getMessage())
                                ->warning()
                                ->send();

                            return;
                        }

                        $formatted =
                        '<ol class="list-decimal">'.
                        $links->map(fn ($link) => "<li><a href=\"{$link}\">{$link}</a></li>")->implode('').
                        '</ol>';

                        return new HtmlString($formatted);
                    }),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                    // Tables\Actions\BulkAction::make('transcodeCommand')
                    //     ->label('Транскодировать (команда)')
                    //     ->authorize(fn () => Auth::user()->can('everything'))
                    //     ->form(fn (Collection $records) => [
                    //         Forms\Components\Textarea::make('code')
                    //             ->rows(25)
                    //             ->default(MovieUpload::transcodingInBackgroundCommand($records))
                    //             // ->default(Node::secondary()->command(MovieUpload::transcodingInBackgroundCommand($records), dry: true))
                    //             ->readOnly()
                    //             ->required(),
                    //     ]),
                    // Tables\Actions\BulkAction::make('transcode')
                    //     ->label('Транскодировать')
                    //     ->authorize(fn () => Auth::user()->can('everything'))
                    //     ->action(function (Collection $records) {

                    //         $user = Auth::user();

                    //         // отправляем в фон, т.к. у www-data нет .ssh ключей (подробнее в stack.yml, в комментах к php-fpm)
                    //         dispatch(function () use ($user, $records) {
                    //             Notification::make()->title('Транскодировать')->body(MovieUpload::transcode($records))->sendToDatabase($user);
                    //         });

                    //         Notification::make()->body('Команда отправлена')->send();
                    //     }),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMovieUploads::route('/'),
            // 'create' => Pages\CreateMovieUpload::route('/create'),
            // 'edit' => Pages\EditMovieUpload::route('/{record}/edit'),
        ];
    }
}
