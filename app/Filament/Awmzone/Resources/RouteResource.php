<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\RouteResource\Pages;
use App\Models\Route;
use Filament\Forms;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Components\Tabs\Tab;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Table;

class RouteResource extends Resource
{
    protected static ?string $model = Route::class;

    protected static ?string $navigationIcon = 'heroicon-o-square-3-stack-3d';

    public static function getModelLabel(): string
    {
        return ___('роут')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('роуты')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->columns(3)
            ->schema([
                Forms\Components\TextInput::make('pattern')
                    ->maxLength(100)
                    ->default(null),
                Forms\Components\Select::make('domain')
                    ->preload()
                    ->searchable()
                    ->relationship('domain', 'domain', fn ($query) => $query->doesntHave('mirrorOf')),
                Forms\Components\TextInput::make('type')
                    ->maxLength(30)
                    ->default(null),
                Tabs::make()
                    ->columnSpanFull()
                    ->tabs([
                        Tab::make('Параметры')
                            ->schema([
                                Fieldset::make('Исполнители')
                                    ->columns(3)
                                    ->schema([
                                        Forms\Components\TextInput::make('controller')
                                            ->maxLength(100)
                                            ->default(null),
                                        Forms\Components\TextInput::make('method')
                                            ->maxLength(30)
                                            ->default(null),
                                        Forms\Components\TextInput::make('template')
                                            ->maxLength(100)
                                            ->default(null),
                                    ]),
                                Fieldset::make('Переменные для megaweb роута')
                                    ->columns(5)
                                    ->schema([
                                        Forms\Components\TextInput::make('var1')
                                            ->maxLength(30)
                                            ->default(null),
                                        Forms\Components\TextInput::make('var2')
                                            ->maxLength(30)
                                            ->default(null),
                                        Forms\Components\TextInput::make('var3')
                                            ->maxLength(30)
                                            ->default(null),
                                        Forms\Components\TextInput::make('var4')
                                            ->maxLength(30)
                                            ->default(null),
                                        Forms\Components\TextInput::make('var5')
                                            ->maxLength(30)
                                            ->default(null),
                                    ]),
                                Fieldset::make('Laravel')
                                    ->columns(3)
                                    ->schema([
                                        Forms\Components\TextInput::make('name')
                                            ->maxLength(100)
                                            ->default(null),
                                        Forms\Components\TextInput::make('http_methods')
                                            ->maxLength(30)
                                            ->default(null),
                                        Forms\Components\TextInput::make('vars')
                                            ->maxLength(100)
                                            ->default(null),
                                        Forms\Components\TextInput::make('slug')
                                            ->maxLength(30)
                                            ->default(null),
                                    ]),
                                Fieldset::make('Middlewares')
                                    ->columns(3)
                                    ->schema([
                                        Forms\Components\TextInput::make('middleware')
                                            ->maxLength(100)
                                            ->default(null),
                                        Forms\Components\Toggle::make('csrf_middleware')
                                            ->required(),
                                        Forms\Components\Toggle::make('ban_middleware')
                                            ->required(),
                                        Forms\Components\Toggle::make('cron_switcher_middleware')
                                            ->required(),
                                        Forms\Components\Toggle::make('access_scheme_middleware')
                                            ->required(),
                                    ]),
                                Fieldset::make('Пережитки')
                                    ->columns(3)
                                    ->schema([
                                        Forms\Components\Toggle::make('cache')
                                            ->required(),
                                        Forms\Components\TextInput::make('alias')
                                            ->maxLength(100)
                                            ->default(null),
                                        Forms\Components\Toggle::make('priority')
                                            ->required(),
                                        Forms\Components\Toggle::make('redirectToAlias')
                                            ->required(),
                                    ]),
                            ]),
                        Tab::make('Заголовки')
                            ->schema([
                                Forms\Components\TextInput::make('comment')
                                    ->maxLength(200)
                                    ->default(null),
                                Forms\Components\Textarea::make('title')
                                    ->columnSpanFull(),
                                Forms\Components\Textarea::make('description')
                                    ->columnSpanFull(),
                                Forms\Components\TextInput::make('keywords')
                                    ->maxLength(300)
                                    ->default(null),
                                Forms\Components\Textarea::make('h1')
                                    ->columnSpanFull(),
                                Forms\Components\Textarea::make('h2')
                                    ->columnSpanFull(),
                                Forms\Components\Textarea::make('bread')
                                    ->columnSpanFull(),
                                Forms\Components\Textarea::make('topText')
                                    ->columnSpanFull(),
                                Forms\Components\Textarea::make('bottomText')
                                    ->columnSpanFull(),
                                Forms\Components\Textarea::make('text1')
                                    ->columnSpanFull(),
                                Forms\Components\Textarea::make('text2')
                                    ->columnSpanFull(),
                            ]),
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('id', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('domain')
                    ->searchable(),
                Tables\Columns\TextColumn::make('http_methods')
                    ->searchable(),
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('pattern')
                    ->searchable(),
                Tables\Columns\TextColumn::make('vars')
                    ->searchable(),
                Tables\Columns\TextColumn::make('template')
                    ->searchable(),
                Tables\Columns\IconColumn::make('cache')
                    ->boolean(),
                Tables\Columns\TextColumn::make('alias')
                    ->searchable(),
                Tables\Columns\TextColumn::make('controller')
                    ->searchable(),
                Tables\Columns\TextColumn::make('method')
                    ->searchable(),
                Tables\Columns\TextColumn::make('middleware')
                    ->searchable(),
                Tables\Columns\TextColumn::make('var1')
                    ->searchable(),
                Tables\Columns\TextColumn::make('var2')
                    ->searchable(),
                Tables\Columns\TextColumn::make('var3')
                    ->searchable(),
                Tables\Columns\TextColumn::make('var4')
                    ->searchable(),
                Tables\Columns\TextColumn::make('var5')
                    ->searchable(),
                Tables\Columns\TextColumn::make('comment')
                    ->searchable(),
                Tables\Columns\TextColumn::make('keywords')
                    ->searchable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\IconColumn::make('redirectToAlias')
                    ->boolean(),
                Tables\Columns\IconColumn::make('priority')
                    ->boolean(),
                Tables\Columns\TextColumn::make('slug')
                    ->searchable(),
                Tables\Columns\TextColumn::make('type')
                    ->searchable(),
                Tables\Columns\IconColumn::make('csrf_middleware')
                    ->boolean(),
                Tables\Columns\IconColumn::make('ban_middleware')
                    ->boolean(),
                Tables\Columns\IconColumn::make('cron_switcher_middleware')
                    ->boolean(),
                Tables\Columns\IconColumn::make('access_scheme_middleware')
                    ->boolean(),
            ])
            ->filters([
                SelectFilter::make('domain')
                    ->searchable()
                    ->preload()
                    ->relationship('domain', 'domain', fn ($query) => $query->doesntHave('mirrorOf')),
            ])
            ->actions([
                Tables\Actions\EditAction::make()
                    ->slideOver(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListRoutes::route('/'),
            // 'create' => Pages\CreateRoute::route('/create'),
            // 'edit' => Pages\EditRoute::route('/{record}/edit'),
        ];
    }
}
