<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\DomainResource\Pages;
use App\Models\DnsProvider;
use App\Models\Domain;
use App\Models\ParsedUrl;
use App\Models\User;
use Filament\Forms;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Components\Tabs\Tab;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\ViewColumn;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\TernaryFilter;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class DomainResource extends Resource
{
    protected static ?string $model = Domain::class;
    // появляется глобальный поиск наверху
    // protected static ?string $recordTitleAttribute = 'domain';

    protected static ?string $navigationIcon = 'heroicon-o-globe-alt';

    public static function getModelLabel(): string
    {
        return ___('домен')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('домены')->ucfirst();
    }

    public static function redirectToDomains(Builder $query, Domain $domain)
    {
        return $query
            ->where('mirror_of', $domain->domain)
            ->orWhere('domain', $domain->domain)
            ->orderByDesc('add_date');
    }

    public static function domainSchema(User $user)
    {
        return [
            Forms\Components\TextInput::make('domain')
                ->disabled($user->cannot('everything'))
                ->label(___('домен')->ucfirst())
                ->prefixIcon('heroicon-o-globe-alt')
                ->prefix(fn (Domain $domain) => $domain->mirror_of_or_self->protocol.'://')
                ->maxLength(30),
            Forms\Components\Select::make('type')
                ->disabled($user->cannot('everything'))
                ->label('Тип')
                ->required()
                ->searchable()
                ->options(Domain::forCurrentUser()->distinct()->pluck('type')->sort()->mapWithKeys(fn ($type) => [$type => Str::ucfirst($type)])),
            Forms\Components\Select::make('mirror_of')
                ->disabled($user->cannot('everything'))
                ->label(___('Является зеркалом для'))
                ->searchable()
                ->preload()
                ->relationship('mirrorOf', 'domain', fn ($query, Domain $domain) => $query
                    ->doesntHave('mirrorOf')
                    ->where('domain', '!=', $domain->domain)
                    ->orderByDesc('add_date')
                ),
            Forms\Components\Toggle::make('known_ru_block')
                ->disabled($user->cannot('everything'))
                ->label(___('Известный блок по РФ'))
                ->inline(false),
        ];
    }

    public static function primaryDomainSchema(User $user)
    {
        return [
            Fieldset::make('Редиректы')
                ->columns(5)
                ->schema([
                    Forms\Components\Select::make('redirect_to')
                        ->disabled($user->cannot('everything'))
                        ->label('Зеркало для Гугла')
                        ->searchable()
                        ->preload()
                        ->relationship('redirectTo', 'domain', fn ($query, Domain $domain) => self::redirectToDomains($query, $domain)),
                    Forms\Components\Select::make('yandex_redirect_to')
                        ->disabled($user->cannot('everything'))
                        ->label('Зеркало для Яндекса')
                        ->searchable()
                        ->preload()
                        ->relationship('yandexRedirectTo', 'domain', fn ($query, Domain $domain) => self::redirectToDomains($query, $domain)),
                    Forms\Components\Select::make('client_redirect_to')
                        ->disabled($user->cannot('everything'))
                        ->label('Клиентское зеркало')
                        ->searchable()
                        ->preload()
                        ->relationship('clientRedirectTo', 'domain', fn ($query, Domain $domain) => self::redirectToDomains($query, $domain)),
                    Forms\Components\Select::make('ru_domain')
                        ->disabled($user->cannot('everything'))
                        ->label('Домен для РФ')
                        ->searchable()
                        ->preload()
                        ->relationship('ruDomain', 'domain', fn ($query, Domain $domain) => self::redirectToDomains($query, $domain)),
                    Forms\Components\Select::make('next_ru_domain')
                        ->disabled($user->cannot('everything'))
                        ->label('Следующий домен для РФ')
                        ->searchable()
                        ->preload()
                        ->relationship('nextRuDomain', 'domain', fn ($query, Domain $domain) => self::redirectToDomains($query, $domain)),
                ]),
            Forms\Components\ToggleButtons::make('access_scheme')
                ->disabled($user->cannot('everything'))
                ->label('Схема доступа')
                ->inline()
                ->required()
                ->options(Domain::distinct()->pluck('access_scheme')->sort()->mapWithKeys(fn ($scheme) => [$scheme => Str::ucfirst($scheme)])),
            // Forms\Components\Select::make('www')
            // ->label('www')
            // ->native(false)
            // ->options(Domain::distinct()->pluck('www')),
            Forms\Components\ToggleButtons::make('engine')
                ->disabled($user->cannot('everything'))
                ->label('Движок')
                ->inline()
                ->required()
                ->options(Domain::distinct()->pluck('engine')->sort()->mapWithKeys(fn ($engine) => [$engine => Str::ucfirst($engine)])),
            Forms\Components\TextInput::make('site_name')
                ->label('Имя сайта')
                ->maxLength(255)
                ->default(''),
            Forms\Components\Select::make('type_template')
                ->disabled($user->cannot('everything'))
                ->label('Шаблон')
                ->searchable()
                ->preload()
                ->options(Domain::whereNotNull('type_template')->where('type_template', '!=', '')->distinct()->pluck('type_template')->sort()->mapWithKeys(fn ($template) => [$template => Str::ucfirst($template)])),
            Forms\Components\Select::make('yandex_token_email')
                ->disabled($user->cannot('everything'))
                ->label('Аккаунт Яндекса')
                ->native(false)
                ->relationship('yandexToken', 'email'),
            Forms\Components\TextInput::make('logo_src')
                ->label('Логотип')
                ->maxLength(255)
                ->default(null),
            Forms\Components\TextInput::make('bg_src')
                ->label('Фон')
                ->maxLength(255)
                ->default(null),
            Forms\Components\TextInput::make('favicon_src')
                ->label('Иконка')
                ->maxLength(255)
                ->default(null),
            Forms\Components\TextInput::make('google_analytics_id')
                ->disabled($user->cannot('everything'))
                ->label('ID Google аналитики')
                ->maxLength(100)
                ->default(null),
            Fieldset::make('Редакторская')
                ->columns(2)
                ->schema([
                    Forms\Components\MarkdownEditor::make('logo_html')
                        ->disabled($user->cannot('everything')),
                    Forms\Components\MarkdownEditor::make('comment')
                        ->disabled($user->cannot('everything')),
                ]),
            Fieldset::make('Переключательская')
                ->columns(3)
                ->schema([
                    Forms\Components\Toggle::make('client_redirect')
                        ->disabled($user->cannot('everything'))
                        ->label('Редирект клиентов на основу'),
                    Forms\Components\Toggle::make('on')
                        ->disabled($user->cannot('everything'))
                        ->label('Включен'),
                    Forms\Components\Toggle::make('only_https')
                        ->disabled($user->cannot('everything'))
                        ->label('Только https'),
                    Forms\Components\Toggle::make('only_http')
                        ->disabled($user->cannot('everything'))
                        ->label('Только http'),
                    Forms\Components\Toggle::make('yandexAllowed')
                        ->disabled($user->cannot('everything'))
                        ->label('Яндекс открыт'),
                    Forms\Components\Toggle::make('allow_not_valid_referers_for_clients')
                        ->disabled($user->cannot('everything'))
                        ->label('Разрешать не валидные рефереры для клиентов'),
                ]),
        ];
    }

    public static function parentDomainSchema(User $user)
    {
        return [
            Forms\Components\TextInput::make('videoroll_id')
                ->disabled($user->cannot('everything'))
                ->label('Videoroll ID')
                ->numeric()
                ->default(null),
            Forms\Components\TextInput::make('movieads_id')
                ->disabled($user->cannot('everything'))
                ->label('Movieads ID')
                ->maxLength(100)
                ->default(null),
            Forms\Components\Select::make('recaptcha_key_name')
                ->disabled($user->cannot('everything'))
                ->label('Recaptcha')
                ->relationship('recaptchaKey', 'name')
                ->searchable()
                ->preload(),
            Forms\Components\Select::make('dns_provider_id')
                ->disabled($user->cannot('everything'))
                ->label('DNS провайдер')
                ->native(false)
                ->searchable()
                ->preload()
                ->relationship('dnsProvider')
                ->getOptionLabelFromRecordUsing(fn (DnsProvider $provider) => "[{$provider->service}] {$provider->email}"),
            Forms\Components\TextInput::make('alloha_txt')
                ->disabled($user->cannot('everything'))
                ->label('Alloha код')
                ->maxLength(100)
                ->default(null),
            Forms\Components\Toggle::make('added_to_alloha')
                ->disabled($user->cannot('everything'))
                ->label('Добавлен в Alloha'),
            Forms\Components\Toggle::make('is_masking_domain')
                ->disabled($user->cannot('everything'))
                ->label('Маскировочный'),
            Forms\Components\Toggle::make('renewed')
                ->disabled($user->cannot('everything'))
                ->label('Продлен'),
            Forms\Components\Toggle::make('certbot')
                ->disabled($user->cannot('everything'))
                ->label('Certbot'),
            Forms\Components\Toggle::make('tls_13')
                ->disabled($user->cannot('everything'))
                ->label('TLS 1.3'),
            Forms\Components\Toggle::make('ech')
                ->disabled($user->cannot('everything'))
                ->label('ECH'),
        ];
    }

    public static function cinemaDomainSchema(User $user)
    {
        return [
            Fieldset::make('Ссылки')
                ->columns(3)
                ->schema([
                    Forms\Components\TextInput::make('watchLink')
                        ->disabled($user->cannot('everything'))
                        ->label('Watch'),
                    Forms\Components\TextInput::make('groupLink')
                        ->disabled($user->cannot('everything'))
                        ->label('Group'),
                    Forms\Components\TextInput::make('genreLink')
                        ->disabled($user->cannot('everything'))
                        ->label('Genre'),
                    Forms\Components\TextInput::make('watchLinkEn')
                        ->disabled($user->cannot('everything'))
                        ->label('Watch EN'),
                    Forms\Components\TextInput::make('watchMovieLink')
                        ->disabled($user->cannot('everything'))
                        ->label('Watch Movie'),
                    Forms\Components\TextInput::make('watchSerialLink')
                        ->disabled($user->cannot('everything'))
                        ->label('Watch Serial'),
                    Forms\Components\TextInput::make('episodeLink')
                        ->disabled($user->cannot('everything'))
                        ->label('Episode'),
                    Forms\Components\TextInput::make('sitemapMoviesPaginationLink')
                        ->disabled($user->cannot('everything'))
                        ->label('Sitemap Movies'),
                ]),
            Forms\Components\TextInput::make('topMoviesLimit')
                ->disabled($user->cannot('everything'))
                ->label('Топ фильмов шт.')
                ->numeric(),
            Forms\Components\TextInput::make('moviesLimit')
                ->disabled($user->cannot('everything'))
                ->label('Фильмов на странице')
                ->numeric(),
            Forms\Components\TextInput::make('moviesPerDay')
                ->disabled($user->cannot('everything'))
                ->label('Добавлять по шт.')
                ->numeric(),
            Forms\Components\TextInput::make('moviesMinYear')
                ->disabled($user->cannot('everything'))
                ->label('С какого года')
                ->numeric(),
            Forms\Components\Select::make('rkn_scheme')
                ->disabled($user->cannot('everything'))
                ->label('Схема обработки РКН')
                ->native(false)
                ->options(collect([
                    'delete_player_when_ru_and_not_canonical',
                    'delete_player_only_if_main_domain',
                    'delete_player_only_if_abuse_on_page',
                ])->mapWithKeys(fn ($scheme) => [$scheme => Str::ucfirst($scheme)])),
            Forms\Components\TextInput::make('index_route')
                ->disabled($user->cannot('everything')),
            Forms\Components\Select::make('main_player')
                ->disabled($user->cannot('everything'))
                ->label('Главный плеер')
                ->native(false)
                ->options(collect([
                    'allohatv',
                    'hdvb',
                    'videodb',
                    'trailer',
                ])->mapWithKeys(fn ($player) => [$player => Str::ucfirst($player)])),
            Forms\Components\Select::make('allow_movies_only_from')
                ->disabled($user->cannot('everything'))
                ->label('Фильмы только с парсинга')
                ->native(false)
                ->options(ParsedUrl::distinct()->pluck('domain')->mapWithKeys(fn ($domain) => [$domain => Str::ucfirst($domain)])),
            Fieldset::make('Добавлять на сайт')
                ->columns(3)
                ->schema([
                    Forms\Components\Toggle::make('include_serials')
                        ->disabled($user->cannot('everything'))
                        ->label('Сериалы'),
                    Forms\Components\Toggle::make('include_movies')
                        ->disabled($user->cannot('everything'))
                        ->label('Фильмы'),
                    Forms\Components\Toggle::make('include_trailers')
                        ->disabled($user->cannot('everything'))
                        ->label('Трейлеры'),
                    Forms\Components\Toggle::make('include_games')
                        ->disabled($user->cannot('everything'))
                        ->label('Игры'),
                    Forms\Components\Toggle::make('include_youtube')
                        ->disabled($user->cannot('everything'))
                        ->label('Youtube'),
                ]),
            Fieldset::make('Контент')
                ->columns(3)
                ->schema([
                    Forms\Components\Toggle::make('movies_with_unique_description')
                        ->disabled($user->cannot('everything'))
                        ->label('Только уник. описания'),
                    Forms\Components\Toggle::make('useAltDescriptions')
                        ->disabled($user->cannot('everything'))
                        ->label('Альтерн. описания'),
                    Forms\Components\Toggle::make('moviesWithPoster')
                        ->disabled($user->cannot('everything'))
                        ->label('Только фильмы с постером'),
                    Forms\Components\Toggle::make('legalMovies')
                        ->disabled($user->cannot('everything'))
                        ->label('Легальные фильмы'),
                    Forms\Components\Toggle::make('allowRiskyStudios')
                        ->disabled($user->cannot('everything'))
                        ->label('Разрешить риск. студии'),
                    Forms\Components\Toggle::make('useOnlyChoosenGroups')
                        ->disabled($user->cannot('everything'))
                        ->label('Добавлять только фильмы в разрешенных категориях'),
                    Forms\Components\Toggle::make('allowRussianMovies')
                        ->disabled($user->cannot('everything'))
                        ->label('Разрешены РФ фильмы'),
                    Forms\Components\Toggle::make('ai_descriptions')
                        ->disabled($user->cannot('everything'))
                        ->label('AI описания'),
                ]),
            Fieldset::make('Трюки')
                ->columns(3)
                ->schema([
                    Forms\Components\Toggle::make('rknForRuOnly')
                        ->disabled($user->cannot('everything'))
                        ->label('Скрывать плеер только для РФ'),
                    Forms\Components\Toggle::make('hide_canonical')
                        ->disabled($user->cannot('everything')),
                    Forms\Components\Toggle::make('index_search')
                        ->disabled($user->cannot('everything')),
                    Forms\Components\Toggle::make('index_movies_in_yandex')
                        ->disabled($user->cannot('everything')),
                    Forms\Components\Toggle::make('show_trailer_for_non_sng')
                        ->disabled($user->cannot('everything')),
                    Forms\Components\Toggle::make('dmcaQuestion')
                        ->disabled($user->cannot('everything')),
                    Forms\Components\Toggle::make('episodically')
                        ->disabled($user->cannot('everything')),
                    Forms\Components\Toggle::make('useInRedirectToAnyMovie')
                        ->disabled($user->cannot('everything')),
                ]),
            Fieldset::make('Пережитки')
                ->columns(3)
                ->schema([
                    Forms\Components\Toggle::make('watchCalcSameMovies')
                        ->disabled($user->cannot('everything'))
                        ->label('Генер. похожие на стр-це watch'),
                    Forms\Components\Toggle::make('calcThumbGenresList')
                        ->disabled($user->cannot('everything'))
                        ->label('Генер. список жанров для тумб'),
                    Forms\Components\Toggle::make('useMovieId')
                        ->disabled($user->cannot('everything')),
                    Forms\Components\Toggle::make('useTagIdForGenres')
                        ->disabled($user->cannot('everything')),
                ]),
        ];
    }

    public static function form(Form $form): Form
    {
        $user = Auth::user();

        return $form
            ->schema([
                Tabs::make()
                    ->columnSpanFull()
                    ->tabs([
                        Tab::make('Engine')
                            ->schema([
                                Section::make('Настройки домена')
                                    ->collapsible()
                                    ->columns(4)
                                    ->schema(self::domainSchema($user)),
                                Section::make('Настройки основы')
                                    ->hidden(fn (Domain $domain) => $domain->mirrorOf)
                                    ->collapsible()
                                    ->columns(4)
                                    ->schema(self::primaryDomainSchema($user)),
                                Section::make('Настройки родительского домена')
                                    ->hidden(fn (Domain $domain) => $domain->parentDomain)
                                    ->collapsible()
                                    ->columns(5)
                                    ->schema(self::parentDomainSchema($user)),
                            ]),
                        Tab::make('Cinema')
                            ->columns(3)
                            ->visible(fn (Domain $domain) => $domain->cinemaDomain)
                            ->schema(self::cinemaDomainSchema($user)),
                    ]),

                // Forms\Components\TextInput::make('metrika_id')
                //     ->numeric()
                //     ->default(null),
                // Forms\Components\Toggle::make('to_transfer'),
                // Forms\Components\TextInput::make('to_registrar'),
                // Forms\Components\TextInput::make('code'),
                // Forms\Components\TextInput::make('transfer_reason')
                // ->maxLength(255)
                // ->default(null),
                // Forms\Components\Toggle::make('ru_block')
                //     ->required(),
                // Forms\Components\Toggle::make('expired')
                // ->required(),
                // Forms\Components\Toggle::make('reborn')
                // ->required(),
                // Forms\Components\TextInput::make('owner')
                // ->required(),
                // Forms\Components\TextInput::make('status')
                //     ->required(),
                // Forms\Components\TextInput::make('charset')
                // ->required(),
                // Forms\Components\TextInput::make('registrar'),
                // Forms\Components\TextInput::make('mobile_version')
                // ->required(),
                // Forms\Components\DatePicker::make('create_date'),
                // Forms\Components\TextInput::make('lang')
                // ->required(),
                // Forms\Components\TextInput::make('source_charset')
                // ->required(),
                // Forms\Components\TextInput::make('rewrite_function')
                // ->maxLength(100)
                // ->default(null),
                // Forms\Components\TextInput::make('additional_path'),
                // Forms\Components\TextInput::make('mysql_charset')
                // ->required(),
                // Forms\Components\Toggle::make('auth')
                //     ->required(),
                // Forms\Components\Toggle::make('yandex_redirect')
                //     ->required(),
                // Forms\Components\Toggle::make('google_redirect')
                //     ->required(),
                // Forms\Components\DateTimePicker::make('add_date')
                //     ->required(),
                // Forms\Components\Toggle::make('brand')
                //     ->required(),
                // Forms\Components\Textarea::make('whois_data')
                //     ->columnSpanFull(),
                // Forms\Components\TextInput::make('parent_domain_domain')
                // ->maxLength(30)
                // ->default(null),
                // Forms\Components\DateTimePicker::make('ru_block_checked_at'),
                // Forms\Components\Toggle::make('monitoring')
                // ->required(),
                // Forms\Components\Toggle::make('monitoring_ru')
                // ->required(),
                // Forms\Components\Toggle::make('monitoring_ua')
                // ->required(),
                // Forms\Components\Toggle::make('monitoring_kz')
                // ->required(),
                // Forms\Components\Toggle::make('monitoring_lv')
                // ->required(),
                // Forms\Components\DateTimePicker::make('monitored_at'),
                // Forms\Components\DateTimePicker::make('whois_data_updated_at'),
                // Forms\Components\Toggle::make('full_ru_block')
                // ->required(),
                // Forms\Components\Toggle::make('ignoreBan')
                //     ->required(),
                // Forms\Components\Toggle::make('ignoreSwitch')
                //     ->required(),
            ]);
    }

    public static function tableNew(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->paginated([10, 25, 50, 100])
            ->modifyQueryUsing(fn (Builder $query) => $query
                ->select('domains.*')
                ->with('parentDomain')
                ->with('redirectTo.parentDomain')
                ->with('yandexRedirectTo.parentDomain')
                ->with('clientRedirectTo.parentDomain')
                ->with('metrika')
                ->forCurrentUser()
                ->withLatestCrawlerMirrorCreationDate()
            )
            ->columns([
                ViewColumn::make('custom')
                    ->view('filament.awmzone.domain-column'),
            ])
            ->filters([
                SelectFilter::make('type')
                    ->label('Тип')
                    ->native(false)
                    ->options(Domain::forCurrentUser()->distinct()->pluck('type')->sort()->mapWithKeys(fn ($type) => [$type => Str::ucfirst($type)])),
                TernaryFilter::make('yandexAllowed')
                    ->native(false)
                    ->label('Открыт для Яндекса'),
                TernaryFilter::make('on')
                    ->native(false)
                    ->label('Включен'),
                TernaryFilter::make('client_redirect_to')
                    ->native(false)
                    ->label('С клиентским зеркалом')
                    ->nullable(),
                TernaryFilter::make('client_redirect')
                    ->native(false)
                    ->label('Редирект клиента на основу'),
                TernaryFilter::make('is_masking_domain')
                    ->native(false)
                    ->label('Маскировочный'),
                TernaryFilter::make('allow_not_valid_referers_for_clients')
                    ->native(false)
                    ->label('Разрешать не валидные рефереры для клиентов'),
                SelectFilter::make('access_scheme')
                    ->native(false)
                    ->label('Схема доступа')
                    ->options(Domain::distinct()->pluck('access_scheme')->sort()->mapWithKeys(fn ($scheme) => [$scheme => Str::ucfirst($scheme)])),
                SelectFilter::make('dnsProvider')
                    ->native(false)
                    ->relationship('dnsProvider', 'service')
                    ->getOptionLabelFromRecordUsing(fn (DnsProvider $provider) => "[{$provider->service}] {$provider->email}"),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                // ->slideOver(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->paginated([10, 25, 50, 100])
            ->modifyQueryUsing(fn (Builder $query) => $query
                ->select('domains.*')
                ->with('parentDomain')
                ->with('redirectTo.parentDomain')
                ->with('yandexRedirectTo.parentDomain')
                ->with('clientRedirectTo.parentDomain')
                ->forCurrentUser()
                ->withLatestCrawlerMirrorCreationDate()
            )
            ->columns([
                Tables\Columns\ImageColumn::make('favicon_src')
                    ->width(20)
                    ->height(20)
                    ->circular()
                    ->label('')
                // ->checkFileExistence(false)
                // /storage сам подставляется в начале
                    ->state(fn (Domain $domain) => $domain->favicon_src ? ('/images/h40'.$domain->favicon_src) : null),
                Tables\Columns\TextColumn::make('domain_decoded')
                // ->icon('heroicon-o-clock')
                // ->iconColor('danger')
                    ->label('Домен')
                    ->searchable('domain')
                    ->sortable(['add_date'])
                    ->toggleable()
                    ->copyable()
                    ->color(fn (Domain $domain) => match ($domain->parent_domain_or_self->renewed) {
                        false => 'gray',
                        true => 'white'
                    })
                    ->description(fn (Domain $domain) => $domain->add_date->translatedFormat('j F Y')),
                Tables\Columns\TextColumn::make('redirectTo.domain_decoded')
                    ->label('Google')
                    ->searchable('domain')
                    ->toggleable()
                    ->copyable()
                    ->color(fn (Domain $domain) => match ($domain->redirectTo->parent_domain_or_self->renewed) {
                        false => 'gray',
                        true => 'white'
                    })
                    ->description(fn (Domain $domain) => $domain->redirectTo?->add_date->translatedFormat('j F Y')),
                Tables\Columns\TextColumn::make('yandexRedirectTo.domain_decoded')
                    ->label('Яндекс')
                    ->searchable('domain')
                    ->toggleable()
                    ->copyable()
                    ->color(fn (Domain $domain) => match ($domain->yandexRedirectTo->parent_domain_or_self->renewed) {
                        false => 'gray',
                        true => 'white'
                    })
                    ->description(fn (Domain $domain) => $domain->yandexRedirectTo?->add_date->translatedFormat('j F Y')),
                Tables\Columns\TextColumn::make('clientRedirectTo.domain_decoded')
                    ->label('Клиентский')
                    ->searchable('domain')
                    ->toggleable()
                    ->copyable()
                    ->color(fn (Domain $domain) => match ($domain->clientRedirectTo->parent_domain_or_self->renewed) {
                        false => 'gray',
                        true => 'white'
                    })
                    ->description(fn (Domain $domain) => $domain->clientRedirectTo?->add_date->translatedFormat('j F Y')),
                Tables\Columns\TextColumn::make('latest_crawler_mirror_creation_date')
                    ->label('Дата переклейки')
                    ->dateTime('j F Y')
                    ->sortable()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('type_template')
                    ->label('Шаблон')
                    ->badge()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('metrika.daily_visits')
                    ->label('Визиты')
                    ->toggleable()
                    ->sortable()
                    ->icon('heroicon-s-chart-bar')
                    ->description(fn (Domain $domain) => collect([
                        $domain->metrika?->ru_percent ? "РФ {$domain->metrika->ru_percent}%" : null,
                        $domain->metrika?->yandex_percent ? "Yandex {$domain->metrika->yandex_percent}%" : null,
                        $domain->metrika?->google_percent ? "Google {$domain->metrika->google_percent}%" : null,
                        $domain->metrika?->bing_percent ? "Bing {$domain->metrika->bing_percent}%" : null,
                        $domain->metrika?->yahoo_percent ? "Yahoo {$domain->metrika->yahoo_percent}%" : null,
                        $domain->metrika?->duckduckgo_percent ? "DuckDuckGo {$domain->metrika->duckduckgo_percent}%" : null,
                    ])->filter(fn ($value) => ! is_null($value))->implode(', ')
                    )
                    ->wrap()
                // ->width(200)
                    ->url(fn (Domain $domain) => "https://metrika.yandex.ru/stat/search_engines?group=day&amp;period=month&amp;accuracy=1&amp;id={$domain->metrika?->metrika_id}"),
                Tables\Columns\TextColumn::make('comment')
                    ->label('Комментарий')
                    ->wrap()
                    ->searchable()
                    ->toggleable(),
            ])
            ->filters([
                SelectFilter::make('type')
                    ->label('Тип')
                    ->native(false)
                    ->options(Domain::forCurrentUser()->distinct()->pluck('type')->sort()->mapWithKeys(fn ($type) => [$type => Str::ucfirst($type)])),
                TernaryFilter::make('yandexAllowed')
                    ->native(false)
                    ->label('Открыт для Яндекса'),
                TernaryFilter::make('on')
                    ->native(false)
                    ->label('Включен'),
                TernaryFilter::make('client_redirect_to')
                    ->native(false)
                    ->label('С клиентским зеркалом')
                    ->nullable(),
                TernaryFilter::make('client_redirect')
                    ->native(false)
                    ->label('Редирект клиента на основу'),
                TernaryFilter::make('is_masking_domain')
                    ->native(false)
                    ->label('Маскировочный'),
                TernaryFilter::make('allow_not_valid_referers_for_clients')
                    ->native(false)
                    ->label('Разрешать не валидные рефереры для клиентов'),
                SelectFilter::make('access_scheme')
                    ->native(false)
                    ->label('Схема доступа')
                    ->options(Domain::distinct()->pluck('access_scheme')->sort()->mapWithKeys(fn ($scheme) => [$scheme => Str::ucfirst($scheme)])),
                SelectFilter::make('dnsProvider')
                    ->native(false)
                    ->relationship('dnsProvider', 'service')
                    ->getOptionLabelFromRecordUsing(fn (DnsProvider $provider) => "[{$provider->service}] {$provider->email}"),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                // ->slideOver(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    // Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDomains::route('/'),
            // 'create' => Pages\CreateDomain::route('/create'),
            // 'edit' => Pages\EditDomain::route('/{record}/edit'),
        ];
    }
}
