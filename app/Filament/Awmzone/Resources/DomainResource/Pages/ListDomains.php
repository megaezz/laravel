<?php

namespace App\Filament\Awmzone\Resources\DomainResource\Pages;

use App\Filament\Awmzone\Resources\DomainResource;
use Filament\Actions;
use Filament\Resources\Components\Tab;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;

class ListDomains extends ListRecords
{
    protected static string $resource = DomainResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make()
                ->slideOver(),
        ];
    }

    public function monitoringProblems(Builder $query): Builder
    {
        return $query->where(
            fn ($query) => $query
                ->where(
                    fn ($query) => $query
                        ->monitored()
                        ->where('monitoring', false)
                )
                ->orWhereHas(
                    'mirrors',
                    fn ($query) => $query
                        ->monitored()
                        ->where('monitoring', false)
                )
        );
    }

    public function getTabs(): array
    {
        return [
            'all' => Tab::make('Все'),
            // 'blocks' => Tab::make('Блокировки')->badge(fn () => $this->getFilteredTableQuery()->clone()->get()->filter(fn(Domain $domain) => $domain->ru_blocked_current_mirrors->count() ? true : false)->count()),
            'monitoring_problems' => Tab::make('Проблемы мониторинга')
                ->modifyQueryUsing(fn (Builder $query) => $this->monitoringProblems($query))
                ->badge(fn () => $this->monitoringProblems($this->getFilteredTableQuery()->clone())->count()),
            // 'errors' => Tab::make('Ошибки')->badge(fn () => $this->getFilteredTableQuery()->clone()->get()->filter(fn(Domain $domain) => ($domain->errors->count() or $domain->mirrors->contains(function ($mirror) {
            //     return $mirror->errors->count();
            // })) ? true : false)->count()),
        ];
    }

    protected function paginateTableQuery(Builder $query): Paginator
    {
        return $query->simplePaginate($this->getTableRecordsPerPage());
    }
}
