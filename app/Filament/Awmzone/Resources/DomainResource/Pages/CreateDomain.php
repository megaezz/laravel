<?php

namespace App\Filament\Awmzone\Resources\DomainResource\Pages;

use App\Filament\Awmzone\Resources\DomainResource;
use Filament\Resources\Pages\CreateRecord;

class CreateDomain extends CreateRecord
{
    protected static string $resource = DomainResource::class;
}
