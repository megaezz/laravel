<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\LangResource\Pages;
use App\Models\Lang;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Table;

class LangResource extends Resource
{
    protected static ?string $model = Lang::class;

    protected static ?string $navigationIcon = 'heroicon-o-language';

    public static function getModelLabel(): string
    {
        return ___('перевод')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('переводы')->ucfirst();
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('page')
                    ->nullable()
                    ->options([

                    ]),
                Forms\Components\TextInput::make('key')
                    ->required()
                    ->maxLength(300)
                    ->default(''),
                Forms\Components\TextInput::make('ru')
                    ->maxLength(300)
                    ->default(null),
                Forms\Components\TextInput::make('ua')
                    ->maxLength(300)
                    ->default(null),
                Forms\Components\TextInput::make('en')
                    ->maxLength(300)
                    ->default(null),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('created_at', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('page')
                    ->toggleable()
                    ->badge(),
                Tables\Columns\TextColumn::make('key')
                    ->limit(20)
                    ->searchable(),
                Tables\Columns\TextColumn::make('ru')
                    ->limit(20)
                    ->searchable(),
                Tables\Columns\TextColumn::make('ua')
                    ->limit(20)
                    ->searchable(),
                Tables\Columns\TextColumn::make('en')
                    ->limit(20)
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),

            ])
            ->filters([
                SelectFilter::make('page')->options([

                ]),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListLangs::route('/'),
            // 'create' => Pages\CreateLang::route('/create'),
            // 'edit' => Pages\EditLang::route('/{record}/edit'),
        ];
    }
}
