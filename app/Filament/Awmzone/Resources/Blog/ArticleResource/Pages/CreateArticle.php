<?php

namespace App\Filament\Awmzone\Resources\Blog\ArticleResource\Pages;

use App\Filament\Awmzone\Resources\Blog\ArticleResource;
use Filament\Resources\Pages\CreateRecord;

class CreateArticle extends CreateRecord
{
    protected static string $resource = ArticleResource::class;
}
