<?php

namespace App\Filament\Awmzone\Resources\RouteResource\Pages;

use App\Filament\Awmzone\Resources\RouteResource;
use Filament\Resources\Pages\CreateRecord;

class CreateRoute extends CreateRecord
{
    protected static string $resource = RouteResource::class;
}
