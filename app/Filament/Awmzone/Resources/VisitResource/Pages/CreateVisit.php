<?php

namespace App\Filament\Awmzone\Resources\VisitResource\Pages;

use App\Filament\Awmzone\Resources\VisitResource;
use Filament\Resources\Pages\CreateRecord;

class CreateVisit extends CreateRecord
{
    protected static string $resource = VisitResource::class;
}
