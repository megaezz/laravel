<?php

namespace App\Filament\Awmzone\Resources\LogResource\Pages;

use App\Filament\Awmzone\Resources\LogResource;
use Filament\Resources\Pages\CreateRecord;

class CreateLog extends CreateRecord
{
    protected static string $resource = LogResource::class;
}
