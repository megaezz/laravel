<?php

namespace App\Filament\Awmzone\Resources\ActivityResource\Pages;

use App\Filament\Awmzone\Resources\ActivityResource;
use Filament\Resources\Pages\CreateRecord;

class CreateActivity extends CreateRecord
{
    protected static string $resource = ActivityResource::class;
}
