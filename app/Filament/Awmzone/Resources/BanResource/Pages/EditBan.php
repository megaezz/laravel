<?php

namespace App\Filament\Awmzone\Resources\BanResource\Pages;

use App\Filament\Awmzone\Resources\BanResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditBan extends EditRecord
{
    protected static string $resource = BanResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
