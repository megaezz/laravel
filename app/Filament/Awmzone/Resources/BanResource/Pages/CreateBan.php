<?php

namespace App\Filament\Awmzone\Resources\BanResource\Pages;

use App\Filament\Awmzone\Resources\BanResource;
use Filament\Resources\Pages\CreateRecord;

class CreateBan extends CreateRecord
{
    protected static string $resource = BanResource::class;
}
