<?php

namespace App\Filament\Awmzone\Resources;

use App\Filament\Awmzone\Resources\UserResource\Pages;
use App\Models\User;
use Filament\Facades\Filament;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use TomatoPHP\FilamentWallet\Filament\Actions\WalletAction;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon = 'heroicon-o-user';

    public static function getModelLabel(): string
    {
        return ___('пользователь')->ucfirst();
    }

    public static function getPluralModelLabel(): string
    {
        return ___('пользователи')->ucfirst();
    }

    // помещаем ресурс в группу пакета spatie roles permissions
    public static function getNavigationGroup(): ?string
    {
        return __(config('filament-spatie-roles-permissions.navigation_section_group', 'filament-spatie-roles-permissions::filament-spatie.section.roles_and_permissions'));
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\TextInput::make('email')
                    ->email()
                    ->maxLength(255)
                    ->default(null),
                Forms\Components\DateTimePicker::make('email_verified_at'),
                // Forms\Components\TextInput::make('password')
                //     ->password()
                //     ->maxLength(255)
                //     ->default(null),
                Select::make('roles')->multiple()->relationship('roles', 'name'),
                // Forms\Components\TextInput::make('telegram')
                //     ->maxLength(255)
                //     ->default(null),
                // Forms\Components\TextInput::make('days_left')
                //     ->numeric()
                //     ->default(null),
                // Forms\Components\TextInput::make('rate')
                //     ->numeric()
                //     ->default(null),
                // Forms\Components\DateTimePicker::make('last_payment_date'),
                // Forms\Components\DateTimePicker::make('subscription_end_date'),
                // Forms\Components\Toggle::make('eternal_subscription')
                //     ->required(),
                // Forms\Components\Toggle::make('telegram_access')
                //     ->required(),
                // Forms\Components\TextInput::make('twitter')
                //     ->maxLength(255)
                //     ->default(null),
                // Forms\Components\TextInput::make('discord')
                //     ->maxLength(255)
                //     ->default(null),
                // Forms\Components\TextInput::make('usdt_trc20')
                //     ->maxLength(255)
                //     ->default(null),
                // Forms\Components\TextInput::make('otc_wallet')
                //     ->maxLength(255)
                //     ->default(null),
                // Forms\Components\TextInput::make('wallet_solana')
                //     ->maxLength(255)
                //     ->default(null),
                // Forms\Components\TextInput::make('referer_id')
                //     ->numeric()
                //     ->default(null),
                // Forms\Components\TextInput::make('login')
                //     ->maxLength(255)
                //     ->default(null),
                // Forms\Components\TextInput::make('level')
                //     ->maxLength(30)
                //     ->default(null),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->persistSortInSession()
            ->persistFiltersInSession()
            ->filtersTriggerAction(fn ($action) => $action->button()->label(___('фильтры')->ucfirst()))
            ->defaultSort('id', 'desc')
            ->modifyQueryUsing(fn (Builder $query) => $query->whereNotNull('email'))
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('email')
                    ->searchable(),
                Tables\Columns\TextColumn::make('balanceFloat')
                    ->badge()
                    ->money('USD'),
                Tables\Columns\TextColumn::make('roles')
                    ->sortable()
                    ->formatStateUsing(fn (User $user) => $user->roles->pluck('name')->join(', ')),
                Tables\Columns\TextColumn::make('email_verified_at')
                    ->dateTime()
                    ->sortable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('j F Y, H:i:s')
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                // Tables\Columns\TextColumn::make('telegram')
                //     ->searchable(),
                // Tables\Columns\TextColumn::make('days_left')
                //     ->numeric()
                //     ->sortable(),
                // Tables\Columns\TextColumn::make('rate')
                //     ->numeric()
                //     ->sortable(),
                // Tables\Columns\TextColumn::make('last_payment_date')
                //     ->dateTime()
                //     ->sortable(),
                // Tables\Columns\TextColumn::make('subscription_end_date')
                //     ->dateTime()
                //     ->sortable(),
                // Tables\Columns\IconColumn::make('eternal_subscription')
                //     ->boolean(),
                // Tables\Columns\IconColumn::make('telegram_access')
                //     ->boolean(),
                // Tables\Columns\TextColumn::make('twitter')
                //     ->searchable(),
                // Tables\Columns\TextColumn::make('discord')
                //     ->searchable(),
                // Tables\Columns\TextColumn::make('usdt_trc20')
                //     ->searchable(),
                // Tables\Columns\TextColumn::make('otc_wallet')
                //     ->searchable(),
                // Tables\Columns\TextColumn::make('wallet_solana')
                //     ->searchable(),
                // Tables\Columns\TextColumn::make('referer_id')
                //     ->numeric()
                //     ->sortable(),
                // Tables\Columns\TextColumn::make('login')
                //     ->searchable(),
                // Tables\Columns\TextColumn::make('level')
                //     ->searchable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Action::make('loginAs')
                    ->label('Войти')
                    ->visible(fn () => Auth::user()->can('everything'))
                    ->icon('heroicon-o-arrow-left-end-on-rectangle')
                    ->action(function (User $user) {
                        if (Auth::user()->cannot('everything')) {
                            abort(403);
                        }

                        Auth::login($user);

                        return redirect(Filament::getUrl());
                    })
                    ->requiresConfirmation()
                    ->color('primary'),
                WalletAction::make('wallet'),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
        ];
    }
}
