<?php

namespace App\Filament\Awmzone\Widgets;

use App\Models\Log;
use Filament\Tables;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Support\Facades\Auth;

class LatestLogs extends BaseWidget
{
    public function table(Table $table): Table
    {
        return $table
            ->defaultPaginationPageOption(5)
            ->query(
                Log::with('visit')->orderByDesc('date')
            )
            ->columns([
                Tables\Columns\TextColumn::make('date')
                    ->dateTime('j F Y, H:i:s'),
                Tables\Columns\TextColumn::make('text')
                    ->limit(50),
            ]);
    }

    public static function canView(): bool
    {
        return Auth::user()->can('everything');
    }
}
