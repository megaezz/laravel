<?php

namespace App\Filament\Awmzone\Widgets;

use App\Models\Config;
use App\Models\Node;
use App\Models\Visit;
use engine\app\models\F;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;
use Illuminate\Support\Facades\DB;
use Livewire\Attributes\Computed;

class StatsOverview extends BaseWidget
{
    protected static ?string $pollingInterval = '1s';

    protected function getStats(): array
    {
        $stats = collect();

        foreach (Node::whereMonitored(true)->get() as $node) {
            $stats->push(Stat::make(strtoupper($node->name), 'LA '.($node->load_average[0] ?? '?')));
        }

        $opcache = opcache_get_status();

        $phpFpmProcesses = trim(shell_exec('pgrep -c php-fpm'));

        $mysqlConnections = DB::select("SHOW STATUS LIKE 'Threads_connected'")[0]->Value;

        $banRequestsPerSec = Config::cached()->ban_queries_per_second;

        $mysqlRequestsPerSec = Config::cached()->mysql_queries_per_second;

        $requestsPerSec = F::getVisitsPerSecond();

        return $stats->merge([
            Stat::make('HTTP', "$requestsPerSec rps / ".round($this->avgTime->avg_time ?? 0).' ms')
                ->chart($this->visitsPerMinuteForTheLast10Minutes),
            Stat::make('BANNED', "$banRequestsPerSec rps"),
            Stat::make('DB', "$mysqlRequestsPerSec qps / $mysqlConnections con"),
            Stat::make('PHP-FPM', "$phpFpmProcesses proc"),
            // Stat::make('Unique views', '192.1k')
            // ->description('32k increase')
            // ->descriptionIcon('heroicon-m-arrow-trending-up')
            // ->chart([7, 2, 10, 3, 15, 4, 17])
            // ->color('success'),
            Stat::make(
                'OPCACHE',
                round($opcache['memory_usage']['used_memory'] / 1024 / 1024).' Mb / '.
                round($opcache['memory_usage']['free_memory'] / 1024 / 1024).' Mb / '.
                round($opcache['memory_usage']['current_wasted_percentage'], 1).'% '.
                ($opcache['opcache_enabled'] ? '' : 'отключен ').
                ($opcache['cache_full'] ? 'переполнен ' : '')
            ),
        ])
            ->toArray();
    }

    #[Computed]
    public function visitsPerMinuteForTheLast10Minutes()
    {
        return Visit::query()
            ->selectRaw('DATE_FORMAT(date, "%Y-%m-%d %H:%i") as minute, COUNT(*) as visit_count')
            ->where('date', '>=', now()->subMinutes(10))
            ->groupBy('minute')
            ->orderBy('minute', 'asc')
            ->get()
            ->pluck('visit_count')
            ->toArray();
    }

    #[Computed]
    public function avgTime()
    {
        return Visit::query()
            ->selectRaw('avg(time) as avg_time')
            ->whereRaw('date > date_sub(current_timestamp,interval 1 minute)')
            ->first();
    }
}
