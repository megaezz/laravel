<?php

namespace App\Filament\Exports\Doctor;

use App\Models\Doctor\Clinic;
use Carbon\CarbonInterface;
use Filament\Actions\Exports\ExportColumn;
use Filament\Actions\Exports\Exporter;
use Filament\Actions\Exports\Models\Export;
use Illuminate\Database\Eloquent\Builder;

class ClinicExporter extends Exporter
{
    protected static ?string $model = Clinic::class;

    public static function getColumns(): array
    {
        return [
            ExportColumn::make('id'),
            ExportColumn::make('doctu_id'),
            ExportColumn::make('name'),
            ExportColumn::make('name_official'),
            ExportColumn::make('registry_phone'),
            ExportColumn::make('calltouch_phone'),
            ExportColumn::make('website'),
            ExportColumn::make('city_id'),
            ExportColumn::make('address'),
            ExportColumn::make('parent_clinic_id'),
            ExportColumn::make('has_active_subscription'),
            ExportColumn::make('email'),
            ExportColumn::make('report_to'),
            ExportColumn::make('created_at'),
            ExportColumn::make('updated_at'),
        ];
    }

    public static function getCompletedNotificationBody(Export $export): string
    {
        $body = 'Your clinic export has completed and '.number_format($export->successful_rows).' '.str('row')->plural($export->successful_rows).' exported.';

        if ($failedRowsCount = $export->getFailedRowsCount()) {
            $body .= ' '.number_format($failedRowsCount).' '.str('row')->plural($failedRowsCount).' failed to export.';
        }

        return $body;
    }

    // чтобы задание при ошибке повторялось только в течение минуты
    public function getJobRetryUntil(): ?CarbonInterface
    {
        return now()->addMinutes(1);
    }

    public static function modifyQuery(Builder $query): Builder
    {
        // пришлось повысить memory_limit, я так и не понял почему экспорт не мог до конца делаться, даже при том, что я тут запрос заколхозил исключив data и data_wide
        ini_set('memory_limit', '512M');

        return $query->select('id', 'doctu_id', 'name', 'name_official', 'registry_phone', 'calltouch_phone', 'website', 'city_id', 'address', 'parent_clinic_id', 'has_active_subscription', 'email', 'report_to', 'created_at', 'updated_at');
    }

    // public function getJobConnection(): ?string
    // {
    //     return 'sync';

    //     return null;
    // }
}
