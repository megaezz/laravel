<?php

namespace App\Filament\Exports\Cinema;

use App\Models\Cinema\MovieUpload;
use App\Services\Cinema\Turbo\LinksWhereThisUploadIsPrioritized;
use Carbon\CarbonInterface;
use Filament\Actions\Exports\ExportColumn;
use Filament\Actions\Exports\Exporter;
use Filament\Actions\Exports\Models\Export;
use Illuminate\Database\Eloquent\Builder;

class MovieUploadExporter extends Exporter
{
    protected static ?string $model = MovieUpload::class;

    public static function getColumns(): array
    {
        return [
            ExportColumn::make('movie.title_ru')
                ->label('Название Ru'),
            ExportColumn::make('movie.title_en')
                ->label('Название En'),
            ExportColumn::make('movie.year')
                ->label('Год'),
            ExportColumn::make('episode.season')
                ->label('Сезон'),
            ExportColumn::make('episode.episode')
                ->label('Эпизод'),
            ExportColumn::make('dubbing.short_or_full_name')
                ->label('Студия'),
            ExportColumn::make('amount')
                ->label('Тариф'),
            ExportColumn::make('created_at')
                ->label('Дата создания'),
            ExportColumn::make('links')
                ->label('Ссылки')
                ->state(function (MovieUpload $movieUpload) {
                    try {
                        $links = (new LinksWhereThisUploadIsPrioritized)($movieUpload);
                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }

                    return $links->implode(PHP_EOL);
                }),
        ];
    }

    // чтобы задание при ошибке повторялось только в течение минуты
    public function getJobRetryUntil(): ?CarbonInterface
    {
        return now()->addMinutes(1);
    }

    public static function modifyQuery(Builder $query): Builder
    {
        // пришлось повысить memory_limit, я так и не понял почему экспорт не мог до конца делаться часто
        ini_set('memory_limit', '512M');

        return $query;
    }

    public static function getCompletedNotificationBody(Export $export): string
    {
        $body = 'Your movie upload export has completed and '.number_format($export->successful_rows).' '.str('row')->plural($export->successful_rows).' exported.';

        if ($failedRowsCount = $export->getFailedRowsCount()) {
            $body .= ' '.number_format($failedRowsCount).' '.str('row')->plural($failedRowsCount).' failed to export.';
        }

        return $body;
    }

    // public function getJobConnection(): ?string
    // {
    //     return 'sync';
    // }
}
