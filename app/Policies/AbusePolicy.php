<?php

namespace App\Policies;

use App\Models\Abuse;
use App\Models\User;

class AbusePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return $user->can('view abuses');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Abuse $abuse): bool
    {
        return $user->can('view abuses');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Abuse $abuse): bool
    {
        return $user->can('update abuses');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Abuse $abuse): bool
    {
        return $user->can('delete abuses');
    }

    public function deleteAny(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Abuse $abuse): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Abuse $abuse): bool
    {
        return false;
    }
}
