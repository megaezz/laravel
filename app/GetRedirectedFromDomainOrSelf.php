<?php

namespace App;

use App\Models\Domain;
use App\Models\Visit;
use engine\app\models\Engine;
use Illuminate\Support\Facades\DB;

// если пользователь находится на клиентском зеркале, нам нужно определить откуда его редиректнуло, для этого, если 1 поисковый домен, то возвращаем его, либо находим откуда он пришел через историю визитов.
class GetRedirectedFromDomainOrSelf
{
    /**
     * Create a new class instance.
     */
    public function __invoke(Visit $visit)
    {
        // если визит сохранен, то у нас уже будет доступ к отношению domainRelation,
        // а если не сохранен, считаем, что мы сейчас на этом домене
        if ($visit->exists) {
            // dump('Визит существует, берем домен из отношения');
            $domain = $visit->domainRelation;
        } else {
            // dump('Визит не существует, берем текущий объект домена');
            $domain = Engine::requestedDomain();
        }

        // если мы не текущем клиентском домене, то нам не нужно высчитывать откуда был редирект
        if (!$domain->is_client_domain) {
            // dump('Мы не на актуальном клиентском домене, возвращаем текущий домен');
            return $domain;
        }

        // если актуальный домен единственный - то считаем что редирект был с него
        if ($domain->mirror_of_or_self->actual_crawler_mirrors->count() === 1) {
            // dump('У главного домена только один актуальный поисковый домен, считаем что редирект с него');
            return $domain->main_domain;
        }

        // dump('У главного домена несколько актуальных поисковых доменов, поэтому ищем редирект через историю визитов');

        if (session('redirected_from') === false) {
            // dump("В сессии записано false, значит уже искали и не нашли, возвращаем текущий домен");
            return $domain;
        }

        // session()->put('redirected_from', 'local.turboserial.com');
        // dd(session('redirected_from'));

        if (session('redirected_from')) {
            // dump("Есть запись в сессии – " . session('redirected_from'));
            $domainFromSession = Domain::find(session('redirected_from'));
            if ($domainFromSession) {
                // dump("Домен из сессии найден в бд");
                if ($domain->mirror_of_or_self->actual_crawler_mirrors->contains($domainFromSession)) {
                    // dump("Домен записанный в сессии ({$domainFromSession->domain}) является актуальным поисковым доменом для нашего, используем его");
                    return $domainFromSession;
                } else {
                    session()->forget('redirected_from');
                    // dump("Домен записанный в сессии ({$domainFromSession->domain}) не является актуальным поисковым доменом, удаляем его из сессии и продолжаем");
                }
            } else {
                session()->forget('redirected_from');
                // dump("Домен из сессии найден в бд, удаляем его из сесии и продолжаем");
            }
        }

        // если нет, то ищем через историю визитов
        // запрос: ищем визит
        $redirectedVisit = Visit::query()
            // с этого же ip
            ->where('ip', $visit->ip)
            // за последний день
            ->where('date', '>=', now()->subDay())
            // для которого есть
            ->whereExists(function ($query) use ($visit) {
                $query->select(DB::raw(1))
                    // домен
                    ->from('laravel.domains')
                    // где клиентское зеркало - наш текущий домен
                    ->where('client_redirect_to', $visit->host)
                    // а один из поисковых доменов - хост этого визита
                    ->where(function ($query) {
                        $query->whereColumn('redirect_to', 'visits.host')
                            ->orWhereColumn('yandex_redirect_to', 'visits.host');
                    });
            })
            ->orderByDesc('date')
            ->limit(1)
            ->first();

        if ($redirectedVisit) {
            if ($redirectedVisit->domainRelation) {
                if ($domain->mirror_of_or_self->actual_crawler_mirrors->contains($redirectedVisit->domainRelation)) {
                    // dump("Нашли через визиты домен {$redirectedVisit->domainRelation->domain}, и он является актуальным поисковым доменом для нашего, записываем его в сессию и возвращаем");
                    session()->put('redirected_from', $redirectedVisit->domainRelation->domain);
                    return $redirectedVisit->domainRelation;
                } else {
                    // dump("Нашли через визиты домен {$redirectedVisit->domainRelation->domain}, но он почему-то не является актуальным поисковым доменом");
                }
            } else {
                // dump("Нашли визит {$redirectedVisit->id} но он почему-то не содержит отношения домена");
            }
        }

        // dump("Не нашли домен через визиты, записываем false в сессию и возвращаем текущий домен");
        session()->put('redirected_from', false);
        return $domain;
    }
}
