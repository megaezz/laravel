<?php

namespace App\Traits;

use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity as BaseLogsActivity;

trait LogsActivity
{
    use BaseLogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
        // ->dontLogIfAttributesChangedOnly(['updated_at'])
            ->logAll()
            ->logExcept(['updated_at', 'monitored_at'])
            ->dontSubmitEmptyLogs()
            ->logOnlyDirty();
    }
}
