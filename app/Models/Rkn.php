<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rkn extends Model
{
    use HasFactory;

    public $incrementing = false;

    public static $lastUpdate;

    protected $casts = [
        'data' => 'object',
    ];

    public function domains()
    {
        return $this->hasMany(RknDomain::class);
    }

    public function deletedAt(): Attribute
    {
        return Attribute::make(
            get: function () {
                if (! self::$lastUpdate) {
                    // определяем время, когда началось последнее обновление
                    self::$lastUpdate = Schedule::where('command', 'engine:update-rkn-registry')->firstOrFail()->updated_at->copy()->startOfHour();
                }

                return ($this->updated_at < self::$lastUpdate) ? $this->updated_at : null;
            }
        )->shouldCache();
    }
}
