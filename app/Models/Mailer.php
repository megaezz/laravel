<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Mailer extends Model
{
    public function settings(): Attribute
    {
        return Attribute::make(
            get: function () {
                $attributes = collect($this->getAttributes());

                return $attributes
                    ->filter(fn ($value, $key) => $value !== null and ! in_array($key, ['id', 'active']))
                    ->toArray();
            }
        )->shouldCache();
    }

    public function mailer(): Attribute
    {
        return Attribute::make(
            get: function () {
                return Mail::build($this->settings);
            }
        )->shouldCache();
    }
}
