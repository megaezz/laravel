<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    public $timestamps = false;

    protected $casts = [
        'last_activity' => 'timestamp',
    ];
}
