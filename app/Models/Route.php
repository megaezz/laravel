<?php

namespace App\Models;

use App\Traits\LogsActivity;
use engine\app\models\F;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    use LogsActivity;

    public function domain()
    {
        return $this->belongsTo(Domain::class, 'domain');
    }

    public function getHeaders($args = [])
    {
        $arr = ['title', 'description', 'keywords', 'h1', 'topText', 'bottomText', 'bread', 'text1', 'text2', 'h2'];
        $headers = new \stdClass;
        foreach ($arr as $v) {
            /* с помощью ?? '' избавляемся от ошибок passing null to parametr... */
            // $headers->{$v} = Blade::render($this->{$v} ?? '', $args);
            $headers->{$v} = F::render($this->{$v} ?? '', $args);
        }

        return $headers;
    }
}
