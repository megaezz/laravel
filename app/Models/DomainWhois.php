<?php

namespace App\Models;

class DomainWhois
{
    public $registrar;

    public $expirationDate;

    public function __construct(Domain $domain)
    {

        if (! $domain->whois_data) {
            return;
        }

        preg_match('/Registry Expiry Date: (.+)/', $domain->whois_data, $matches);

        $this->expirationDate = isset($matches[1]) ? now()->parse($matches[1]) : null;

        if (! $this->expirationDate) {
            preg_match('/paid-till: (.+)/', $domain->whois_data, $matches);
            $this->expirationDate = isset($matches[1]) ? now()->parse($matches[1]) : null;
        }

        preg_match('/Registrar: (.+)/', $domain->whois_data, $matches);

        $this->registrar = $matches[1] ?? null;

    }
}
