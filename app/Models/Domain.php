<?php

namespace App\Models;

use App\Models\Cinema\TurboDomain;
use App\Models\Scopes\OnlyRedirectToDomains;
use App\Models\Scopes\OnlyTurboDomains;
use App\Observers\DomainObserver;
use App\Services\Engine\DomainErrors;
use App\Services\Engine\Routes;
use App\Traits\LogsActivity;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Domain as CinemaDomain;
// use Rennokki\QueryCache\Traits\QueryCacheable;
use engine\app\models\Domain as DomainMegaweb;
use engine\app\services\RegisterRoutesForDomainService;
use engine\app\services\YandexWebmasterService;
use Illuminate\Database\Eloquent\Attributes\ObservedBy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

#[ObservedBy([DomainObserver::class])]
class Domain extends Model
{
    // используем трейт, для того чтобы он добавил дефолтную бд в имя таблицы, т.к. иначе $cinemaDomain->engineDomain - выдает объект App\Models\Domain, но с атрибутами из cinema.domains
    use HasDatabasePrefix;

    // use QueryCacheable;
    use LogsActivity;

    const CREATED_AT = 'add_date';

    public $incrementing = false;

    protected $primaryKey = 'domain';

    protected $keyType = 'string';

    protected $casts = [
        'ru_block' => 'boolean',
        'on' => 'boolean',
        'only_https' => 'boolean',
        'only_http' => 'boolean',
        'yandex_redirect' => 'boolean',
        'google_redirect' => 'boolean',
        'client_redirect' => 'boolean',
        'brand' => 'boolean',
        'yandexAllowed' => 'boolean',
        'auth' => 'boolean',
        'added_to_alloha' => 'boolean',
        'is_masking_domain' => 'boolean',
        'renewed' => 'boolean',
        'known_ru_block' => 'boolean',
        'certbot' => 'boolean',
        'monitoring' => 'boolean',
        'monitoring_ru' => 'boolean',
        'monitoring_ua' => 'boolean',
        'monitoring_kz' => 'boolean',
        'monitoring_lv' => 'boolean',
        'monitored_at' => 'datetime',
        'whois_data_updated_at' => 'datetime',
        'allow_not_valid_referers_for_clients' => 'boolean',
        'tls_13' => 'boolean',
        'ech' => 'boolean',
        'ignoreBan' => 'boolean',
        'ignoreSwitch' => 'boolean',
    ];

    public function mirrorOf()
    {
        return $this->belongsTo(self::class, 'mirror_of');
    }

    public function routes()
    {
        return $this->hasMany(Route::class, 'domain');
    }

    public function redirectTo()
    {
        return $this->belongsTo(self::class, 'redirect_to');
    }

    public function yandexRedirectTo()
    {
        return $this->belongsTo(self::class, 'yandex_redirect_to');
    }

    public function clientRedirectTo()
    {
        return $this->belongsTo(self::class, 'client_redirect_to');
    }

    public function ruDomain()
    {
        return $this->belongsTo(self::class, 'ru_domain');
    }

    public function nextRuDomain()
    {
        return $this->belongsTo(self::class, 'next_ru_domain');
    }

    public function cinemaDomain()
    {
        return $this->belongsTo(CinemaDomain::class, 'domain');
    }

    public function mirrors()
    {
        return $this->hasMany(self::class, 'mirror_of');
    }

    public function blocks()
    {
        return $this->hasMany(DomainBlock::class, 'domain');
    }

    /* времнный алиас для совместимости после объединения Domain и DomainLaravel */
    public function domainBlocks()
    {
        return $this->blocks();
    }

    public function metrika()
    {
        return $this->hasOne(DomainMetrika::class, 'domain');
    }

    public function recaptchaKey()
    {
        return $this->belongsTo(RecaptchaKey::class);
    }

    public function dnsProvider()
    {
        return $this->belongsTo(DnsProvider::class);
    }

    public function parentDomain()
    {
        return $this->belongsTo(self::class);
    }

    public function yandexToken()
    {
        return $this->belongsTo(YandexToken::class);
    }

    public function dnsRecords()
    {
        return $this->hasMany(DomainDnsRecord::class, 'domain');
    }

    public function parentDnsRecords()
    {
        return $this->hasMany(DomainDnsRecord::class, 'parent_domain');
    }

    public function rkns()
    {
        return $this->hasManyThrough(Rkn::class, RknDomain::class, 'domain', 'id', null, 'rkn_id');
    }

    public function abuses()
    {
        return $this->hasMany(Abuse::class);
    }

    function turbo()
    {
        return $this->hasOne(TurboDomain::class);
    }

    /* TODO: надо бы объединить с атрибутом is_monitored, но не придумал как */
    /* выбирает из базы домены, которые должны быть промониторены. (продленные родительские домены и их поддомены) */
    public function scopeMonitored($query)
    {
        return $query
            /* мониторим продленные родительские домены и их поддомены */
            ->where(function ($query) {
                $query
                    ->where(function ($query) {
                        $query
                            ->whereNull('parent_domain_domain')
                            ->where('renewed', true);
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->whereRelation('parentDomain', 'renewed', true);
                    });
            })
            /* кроме треш доменов */
            ->where('type', '!=', 'trash')
            /* и кроме выключенных основ */
            ->whereNot(function ($query) {
                $query
                    ->whereNull('mirror_of')
                    ->where('on', false);
            });
    }

    public function scopeWithLatestCrawlerMirrorCreationDate(Builder $query)
    {
        return $query->addSelect(
            DB::raw('
            GREATEST(
                add_date,
                COALESCE((SELECT add_date FROM domains AS sub1 WHERE sub1.domain = domains.redirect_to), 0),
                COALESCE((SELECT add_date FROM domains AS sub2 WHERE sub2.domain = domains.yandex_redirect_to), 0)
            ) as latest_crawler_mirror_creation_date
        '),
        );
    }

    public function scopeForCurrentUser(Builder $query)
    {
        $user = Auth::user();

        if (! $user) {
            throw new \Exception('Отсутствует аутентифицированный пользователь');
        }

        if ($user->can('everything')) {
            return $query;
        }

        if ($user->can('view domains')) {
            return $query->whereIn('type', ['cinema', 'copy', 'blog']);
        }

        return $query->where('id', -1);
    }

    public function scopeHasRenewedNonMaskingParentDomain(Builder $query)
    {
        return $query
            ->where(
                fn($query) => $query
                    ->whereHas(
                        'parentDomain',
                        fn($query) =>
                        $query
                            ->where('renewed', true)
                            ->where('is_masking_domain', false)
                    )
                    ->orWhere(
                        fn($query) => $query
                            ->whereDoesntHave('parentDomain')
                            ->where('renewed', true)
                            ->where('is_masking_domain', false)
                    )
            );
    }

    public function scopeHasRenewedNonMaskingParentDomainOrHasRenewedNonMaskingMirrors(Builder $query)
    {
        return $query
            ->where(
                fn($query) => $query
                    // домен имеет продленное родительское зеркало
                    ->hasRenewedNonMaskingParentDomain()
                    // или имеет зеркала, которые имеют продленное родительское зеркало
                    ->orWhereHas('mirrors', fn($query) => $query->hasRenewedNonMaskingParentDomain())
            );
    }

    function scopeOnlyRedirectTo($query)
    {
        return OnlyRedirectToDomains::apply($query);
    }

    function scopeOnlyTurbo($query)
    {
        return OnlyTurboDomains::apply($query);
    }

    /* мониторится ли этот домен (является продленным родительским доменом или его поддоменом) */
    /* TODO: надо бы объединить со scopeMonitored, но не придумал как */
    /* мониторятся: все продленные родительские домены, все домены у которых продлены родительские домены, при условии что они не в статусе trash и не являются основами в статусе off */
    /* не мониторятся: не зеркала в статусе off, все trash домены */
    public function isMonitored(): Attribute
    {
        return Attribute::make(
            get: function () {
                // return ($this->on and $this->type != 'trash' and !$this->parent_domain_domain and $this->renewed) or ($this->parentDomain?->renewed);
                return
                    /* мониторим продленные родительские домены и их поддомены */ ((! $this->parent_domain_domain and $this->renewed) or ($this->parentDomain?->renewed)) and
                    /* кроме треш доменов */
                    $this->type != 'trash' and
                    /* и кроме выключенных основ */
                    ! (! $this->mirror_of and ! $this->on);
            }
        )->shouldCache();
    }

    public function parentDomainOrSelf(): Attribute
    {
        return Attribute::make(
            get: function () {
                /* возможно здесь нужно clone $this или $this->replicate(), подумать */
                return $this->parentDomain ?? $this;
            }
        )->shouldCache();
    }

    public function calculatedParentDomain(): Attribute
    {
        return Attribute::make(
            get: function () {
                $arr = explode('.', $this->domain);
                if (count($arr) <= 2) {
                    return null;
                }
                $arr = array_reverse($arr);
                $parentDomain = $arr[1] . '.' . $arr[0];

                return $parentDomain;
            }
        )->shouldCache();
    }

    // function nextMirror(): Attribute {
    //     return Attribute::make(
    //         get: function() {
    //             preg_match('/^([a-z\-]{1,20})([\d]*)\.(.+)$/', $this->domain,$arr);
    //             if (empty($arr[1])) {
    //                 return false;
    //             }
    //             $mirror['sub_name'] = $arr[1];
    //             $mirror['sub_digit'] = $arr[2];
    //             $mirror['sub_domain'] = $arr[3];
    //             if (!$mirror['sub_digit']) {
    //                 $mirror['sub_digit'] = 0;
    //             }
    //             $mirror['sub_digit']++;
    //             return $mirror['sub_name'].$mirror['sub_digit'].'.'.$mirror['sub_domain'];
    //         }
    //     )->shouldCache();
    // }

    public function nextMirrorDecoded(): Attribute
    {
        return Attribute::make(
            get: function () {
                return idn_to_utf8($this->next_mirror);
            }
        )->shouldCache();
    }

    public function nextMirrorCreated(): Attribute
    {
        return Attribute::make(
            get: function () {
                return self::find($this->nextMirror) ? true : false;
            }
        )->shouldCache();
    }

    /* возвращает каким видом редиректа является данное зеркало, либо null если это основа или не актуальное зеркало */
    public function mirrorRedirectType(): Attribute
    {
        return Attribute::make(
            get: function () {
                /* если не является ничьим зеркалом то null */
                // if (!$this->mirror_of) {
                //     return null;
                // }
                /* если это зеркало равно redirect_to на основе */
                if ($this->domain == $this->mirror_of_or_self->redirect_to) {
                    return 'redirect';
                }
                /* если это зеркало равно yandex_redirect_to на основе */
                if ($this->domain == $this->mirror_of_or_self->yandex_redirect_to) {
                    return 'yandex_redirect';
                }
                /* если это зеркало равно client_redirect_to на основе */
                if ($this->domain == $this->mirror_of_or_self->client_redirect_to) {
                    return 'client_redirect';
                }

                /* в иных случаях это просто зеркало, возвращаем null */
                return null;
            }
        )->shouldCache();
    }

    /* возвращает тип редиректа данного зеркала на русском */
    public function mirrorRedirectTypeRu(): Attribute
    {
        return Attribute::make(
            get: function () {
                $translations = [
                    'redirect' => 'основной редирект',
                    'client_redirect' => 'клиентский редирект',
                    'yandex_redirect' => 'редирект Яндекса',
                ];

                return $translations[$this->mirror_redirect_type] ? $translations[$this->mirror_redirect_type] : null;
            }
        )->shouldCache();
    }

    public function lastRedirectTo(): Attribute
    {
        return Attribute::make(
            get: function () {
                $redirect_to = $this;
                while ($redirect_to->redirect_to) {
                    $redirect_to = $redirect_to->redirect_to;
                }

                return $redirect_to;
            }
        )->shouldCache();
    }

    public function mainDomain(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->redirect_to ? $this->redirectTo : $this;
            }
        )->shouldCache();
    }

    /* алиас для mainDomain */
    public function canonical(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->main_domain;
            }
        )->shouldCache();
    }

    /* TODO: лишний метод, дублирует поведение clientRedirectTo */
    public function clientDomain(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->clientRedirectTo ? $this->clientRedirectTo : null;
            }
        )->shouldCache();
    }

    /* TODO: удалить или сделать алиасом для actualMirrors */
    public function currentMirrors(): Attribute
    {
        return Attribute::make(
            get: function () {
                $mirrors[] = $this->main_domain;
                if ($this->client_domain) {
                    $mirrors[] = $this->client_domain;
                }

                return collect($mirrors);
            }
        )->shouldCache();
    }

    public function dns(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->parent_domain_or_self->dnsProvider?->dns($this->parent_domain_or_self, $this);
            }
        )->shouldCache();
    }

    public function megaweb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new DomainMegaweb(null, $this);
            }
        )->shouldCache();
    }

    /* лучше его убрать и оставить mirrorOfOrSelf */
    public function primaryDomain(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->mirror_of ? $this->mirrorOf : $this;
            }
        )->shouldCache();
    }

    /* абсолютно такой же метод, как primaryDomain, но нейминг правильнее отражает его суть */
    public function mirrorOfOrSelf(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->mirror_of ? $this->mirrorOf : $this;
            }
        )->shouldCache();
    }

    public function protocol(): Attribute
    {
        return Attribute::make(
            get: function () {
                if ($this->only_http) {
                    return 'http';
                }
                if ($this->only_https) {
                    return 'https';
                }

                return 'https';
            }
        )->shouldCache();
    }

    public function canonicalRoute(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->protocol . '://' . $this->canonical->domain;
            }
        )->shouldCache();
    }

    /* TODO: добиться того, чтобы использовать только 1 раз, в конструкторе Engine */
    /* вроде бы этот метод сейчас и не нужен, т.к. подмены не происходит, www, local и т.д. добавлены как зеркала */
    public static function cleanDomain($domain)
    {
        $exphost = explode(':', $domain);
        $domain = str_replace('www.', '', $domain);
        /* подмена домена */
        if (preg_match('/local\.([\w\.]+)/', $domain)) {
            $domain = preg_replace('/(local|vds)\.([\w\.]+)/', '\\2', $domain);
        }

        return $domain;
    }

    /* TODO: оставляю временно, чтобы не было ошибок */
    public function getMetrikaDataCachedAttribute()
    {
        return null;
    }

    /* TODO: оставляю временно, чтобы не было ошибок */
    public function getAddDateISO8601Attribute()
    {
        return (new \DateTime($this->add_date))->format('c');
    }

    /* TODO: оставляю временно, чтобы не было ошибок */
    public function getIsMainDomainAttribute()
    {
        // dd($this->domain,$this->primary_domain->main_domain->domain);
        return $this->domain == $this->primary_domain->main_domain->domain;
    }

    /* TODO: временный метод, для Mirrors. не использовать нигде больше */
    public function domainForClient(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->client_domain ? $this->client_domain : $this->main_domain;
            }
        )->shouldCache();
    }

    /* TODO: сомнительно, убрать если нигде не используется */
    /* является ли домен актуальным клиентским доменом */
    public function isClientDomain(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->mirrorOf?->client_redirect_to == $this->domain;
            }
        )->shouldCache();
    }

    /* TODO: сомнительно, убрать если нигде не используется */
    /* является ли домен родительским доменом */
    public function isParentDomain(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->parent_domain_domain ? false : true;
            }
        )->shouldCache();
    }

    /* TODO: сомнительно, убрать если нигде не используется */
    /* является ли домен зеркалом */
    public function isMirror(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->mirror_of ? true : false;
            }
        )->shouldCache();
    }

    /* TODO: убрать отсюда parent_domain_object */
    public function nextMirror(): Attribute
    {
        return Attribute::make(
            get: function () {
                // preg_match('/^([a-z\-]{1,20})([\d]*)\.(.+)$/', $this->domain,$arr);
                preg_match('/^([a-z\-]{1,20})([\d]*)\.(([\w\d-]+)\.([\w]+))$/', $this->domain, $arr);
                if (empty($arr[1])) {
                    return false;
                }
                /* если это актуальный клиентский домен и родитель этого домена - маскировочный домен и он заблочен или не продлен, то генерируем зеркало на основе другого подходящего маскировочного домена */
                if ($this->is_client_domain and $this->parentDomain?->is_masking_domain and (! $this->monitoring_ru or ! $this->parentDomain?->renewed)) {
                    $masking_domain = self::whereIsMaskingDomain(true)->where('monitoring_ru', true)->where('known_ru_block', false)->whereRenewed(true)->first();
                    if ($masking_domain) {
                        $mirror['sub_domain'] = $masking_domain->domain;
                    } else {
                        $mirror['sub_domain'] = '?';
                    }
                } else {
                    $mirror['sub_domain'] = $arr[3];
                }
                $mirror['sub_name'] = $arr[1];
                $mirror['sub_digit'] = $arr[2] ?? 0;
                $mirror['sub_digit']++;

                // if (empty($mirror['sub_name'])) {
                //     return false;
                // }
                return "{$mirror['sub_name']}{$mirror['sub_digit']}.{$mirror['sub_domain']}";
            }
        )->shouldCache();
    }

    public function whois(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new DomainWhois($this);
            }
        )->shouldCache();
    }

    /* возвращает все родительские домены связанные с этим */
    public function relatedParentDomains(): Attribute
    {
        return Attribute::make(
            get: function () {
                // получаем все зеркала, добавляем к ним себя, получаем всех родителей для них и оставляем уникальные
                return (clone $this->mirrors)->push($this)->pluck('parent_domain_or_self')->unique('domain');
            }
        )->shouldCache();
    }

    public function selfAndMirrors(): Attribute
    {
        return Attribute::make(
            get: function () {
                // получаем все зеркала, добавляем к ним себя
                return (clone $this->mirrors)->push($this);
            }
        )->shouldCache();
    }

    /* возвращает все актуальные забаненные включенные и продленные домены без пометки забаненных в рф */
    public function ruBlockedCurrentMirrors(): Attribute
    {
        return Attribute::make(
            get: function () {
                if (! $this->on) {
                    return collect();
                }

                return $this->actual_mirrors->filter(function ($domain) {
                    /* если заблочен, не известная блокировка и продлен */
                    return ! $domain->monitoring_ru and ! $domain->known_ru_block and $domain->parent_domain_or_self->renewed;
                });
            }
        )->shouldCache();
    }

    public function yandexWebmaster(): Attribute
    {
        return Attribute::make(
            get: function () {
                if (! $this->yandexToken) {
                    throw new \Exception("Не указан яндекс токен для домена {$this->domain}");
                }

                return new YandexWebmasterService($this->yandexToken->token);
            }
        )->shouldCache();
    }

    /* TODO: можно в принципе писать через ->primary_domain на всякий случай, лишних запросов не будет, если мы на главном домене */
    /* вернуть домен для бота, исходя из его имени */
    public function getCrawlerDomain($crawler)
    {
        if (! $crawler) {
            // если не бот, то возвращаем домен для ботов, если его нет, то текущий домен
            return $this->redirect_to ? $this->redirectTo : $this;
        }
        if ($crawler == 'Yandex') {
            // если яндекс бот, то возвращаем домен для яндекса, если его нет, то домен для ботов, если его нет, то текущий домен
            return $this->yandex_redirect_to ? $this->yandexRedirectTo : ($this->redirect_to ? $this->redirectTo : $this);
        } else {
            // если бот, но не яндекс, то возвращаем домен для ботов, если его нет, то текущий домен
            return $this->redirect_to ? $this->redirectTo : $this;
        }
    }

    /* TODO: можно в принципе писать через ->primary_domain на всякий случай, лишних запросов не будет, если мы на главном домене */
    /* исходя из имени бота вернуть домен для бота, либо клиентский */
    public function getCrawlerOrClientDomain($crawler)
    {
        return (! $crawler and $this->client_redirect_to) ? $this->clientRedirectTo : $this->getCrawlerDomain($crawler);
    }

    /* возвращает actualCrawlerMirrors + clientRedirectTo */
    public function actualMirrors(): Attribute
    {
        return Attribute::make(
            get: function () {
                $actualMirrors = collect()->merge($this->actual_crawler_mirrors);
                if ($this->clientRedirectTo) {
                    $actualMirrors->push($this->clientRedirectTo);
                }

                return $actualMirrors->unique();
            }
        )->shouldCache();
    }

    /* возвращает все актуальные зеркала для ботов */
    public function actualCrawlerMirrors(): Attribute
    {
        return Attribute::make(
            get: function () {
                $redirectTo = $this->redirect_to ? $this->redirectTo : $this;
                $yandexRedirectTo = $this->yandex_redirect_to ? $this->yandexRedirectTo : $redirectTo;

                return collect([$redirectTo, $yandexRedirectTo])->unique();
            }
        )->shouldCache();
    }

    /* если все актуальные зеркала домена в известном ркн блоке, то считаем, что домен в известном ркн блоке */
    public function everyActualCrawlerMirrorInKnownRuBlock(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->actual_crawler_mirrors->every('known_ru_block', true);
            }
        )->shouldCache();
    }

    /* является ли данное зеркало одним из актуальных зеркал для бота */
    public function isInActualCrawlerMirrors(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->primary_domain->actual_crawler_mirrors->pluck('domain')->contains($this->domain);
            }
        )->shouldCache();
    }

    /* возвращает сервис - билдер роутов */
    public function toRoute(): Attribute
    {
        return Attribute::make(
            get: function () {
                $routes = new Routes;
                $routes->domain = $this;

                return $routes;
            }
        )->shouldCache();
    }

    /* регистрирует роуты для данного домена */
    public function registerRoutes(bool $withoutPrefix = false)
    {
        return (new RegisterRoutesForDomainService)->register($this, $withoutPrefix);
    }

    public function renewalError(): Attribute
    {
        return Attribute::make(
            get: function () {
                if ($this->parent_domain_or_self->renewed and $this->parent_domain_or_self->whois?->expirationDate and $this->parent_domain_or_self->whois?->expirationDate < now()->addWeeks(2)) {
                    return "заканчивается {$this->parent_domain_or_self->whois->expirationDate->format('j F Y')}";
                }

                /* отлавливаем закончившиеся домены с internetbs, т.к. там expirationDate после истечения срока домена все равно увеличиваетсяна на год */
                // if ($this->parent_domain_or_self->renewed and $this->parent_domain_or_self->whois?->data?->nameServers == ['ns1.ibspark.com','ns2.ibspark.com']) {
                // return "закончился на internetbs";
                // }
                return false;
            }
        )->shouldCache();
    }

    public function hasFolder(): Attribute
    {
        return Attribute::make(
            get: function () {
                return File::exists(public_path("sites/{$this->domain}"));
            }
        )->shouldCache();
    }

    /* роуты для домена вместе с роутами для типов */
    public function mixedRoutes(): Attribute
    {
        return Attribute::make(
            get: function () {
                $types = ($this->primary_domain->type == 'engine') ? ['engine'] : ['engine', $this->primary_domain->type];

                return Route::where(function ($query) use ($types) {
                    $query
                        ->where(function ($query) use ($types) {
                            $query
                                ->whereNull('domain')
                                ->whereIn('type', $types);
                        })
                        ->orWhere('domain', $this->primary_domain->domain);
                })
                    ->whereNotNull('pattern')
                    ->whereNotNull('name')
                    ->where('active', true)
                    ->get();
            }
        )->shouldCache();
    }

    public function dnsRecordsByMirrors(): Attribute
    {
        return Attribute::make(
            get: function () {
                // return $this->parent_domain_or_self->mirrors;
            }
        )->shouldCache();
    }

    public function countryMonitoring(string $countryCode, ?bool $b = null)
    {
        $monitoringAttribute = 'monitoring_' . strtolower($countryCode);
        if (isset($b)) {
            $this->$monitoringAttribute = $b;
        } else {
            return $this->$monitoringAttribute;
        }
    }

    public function knownCountryBlock(string $countryCode, ?bool $b = null)
    {
        $knownBlockAttribute = 'known_' . strtolower($countryCode) . '_block';
        if (isset($b)) {
            $this->$knownBlockAttribute = $b;
        } else {
            return $this->$knownBlockAttribute;
        }
    }

    public function errors(): Attribute
    {
        return Attribute::make(
            get: function () {
                return (new DomainErrors)($this);
            }
        )->shouldCache();
    }

    public function maskRkns(): Attribute
    {
        return Attribute::make(
            get: function () {
                return Rkn::whereRelation('domains', 'domain', "*.{$this->domain}")->get();
            }
        )->shouldCache();
    }

    public function priorityDubbing(): Attribute
    {
        return Attribute::make(
            get: function () {
                return Config::cached()->priorityDubbingOverride ?? $this->turbo?->dubbing ?? Config::cached()->fallbackPriorityDubbing;
            }
        )->shouldCache();
    }
}
