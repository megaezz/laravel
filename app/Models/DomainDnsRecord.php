<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DomainDnsRecord extends Model
{
    protected $casts = [
        'proxied' => 'boolean',
    ];
}
