<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DnsProvider extends Model
{
    protected $casts = [
        'zones' => 'object',
    ];

    public function dns(?Domain $domain = null, ?Domain $subdomain = null)
    {

        if ($this->service == 'cloudflare') {
            return new Cloudflare($this, $domain, $subdomain);
        }

        if ($this->service == 'technitium') {
            return new Technitium($this, $domain, $subdomain);
        }

        throw new \Exception('Не указан DNS провайдер, либо реализация отсутствует');
    }
}
