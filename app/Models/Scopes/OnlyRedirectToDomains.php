<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OnlyRedirectToDomains
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public static function apply(Builder $query): Builder
    {
        return $query
            // если есть mirrorOf для которого этот домен redirectTo
            ->whereExists(
                fn($query) =>
                $query->select(DB::raw(1))
                    ->from('laravel.domains as mirrorOf')
                    ->whereColumn('mirrorOf.domain', 'domains.mirror_of')
                    ->where(
                        fn($query) =>
                        $query
                            ->whereColumn('mirrorOf.redirect_to', 'domains.domain')
                            ->orWhereColumn('mirrorOf.yandex_redirect_to', 'domains.domain')
                    )
            )
            // или нет mirrorOf и нет redirectTo, значит это сам себе redirectTo
            ->orWhere(
                fn($query) =>
                $query
                    ->doesntHave('mirrorOf')
                    ->where(
                        fn($query) =>
                        $query
                            ->doesntHave('redirectTo')
                            ->orWhereColumn('domains.domain', 'domains.redirect_to')
                        // про яндекс тут не пишем, т.к. по моему не надо, не могу сообразить
                    )
            );
    }
}
