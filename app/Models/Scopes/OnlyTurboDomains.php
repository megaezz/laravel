<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

class OnlyTurboDomains
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public static function apply(Builder $query): Builder
    {
        return $query
            // выбираем только актуальные домены поисковых систем
            ->onlyRedirectTo()
            // которые являются зеркалами cinema доменов или сами являются cinema доменами, которые в свою очередь имеют продленный родительский домен, либо имеют немаскировочные зеркала с продленным родительским доменом
            ->where(
                fn($query) =>
                $query
                    ->whereHas(
                        'mirrorOf',
                        fn($query) => $query
                            ->where('type', 'cinema')
                            ->hasRenewedNonMaskingParentDomainOrHasRenewedNonMaskingMirrors()
                            ->has('cinemaDomain.movies')
                    )
                    ->orWhere(
                        fn($query) => $query
                            ->doesntHave('mirrorOf')
                            ->where('type', 'cinema')
                            ->hasRenewedNonMaskingParentDomainOrHasRenewedNonMaskingMirrors()
                            ->has('cinemaDomain.movies')
                    )
            );
    }
}
