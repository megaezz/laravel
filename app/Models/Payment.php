<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = ['value', 'comment', 'user_id'];

    protected $allowedFilters = ['id', 'value', 'comment', 'created_at'];

    protected $allowedSorts = ['id', 'value', 'created_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
