<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SentMail extends Model
{
    public function mailer()
    {
        return $this->belongsTo(Mailer::class);
    }
}
