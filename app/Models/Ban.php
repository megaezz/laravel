<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ban extends Model
{
    protected $casts = [
        'q' => 'integer',
    ];

    protected static function booted()
    {
        /* выполняем после сохранения модели, чтобы не было кучи уведомлений, в случае множества одновременных запросов */
        static::updated(function ($ban) {
            /* уведомлять если апдейтится q старого бана */
            if ($ban->wasChanged('q')) {
                $diffInDays = now()->diffInDays($ban->getOriginal('update_time'));
                if ($diffInDays > 30) {
                    logger()->alert("Апдейтится бан {$ban->id} впервые за {$diffInDays} дней");
                }
            }
        });
    }
}
