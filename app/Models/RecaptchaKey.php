<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecaptchaKey extends Model
{
    protected $primaryKey = 'name';

    protected $keyType = 'string';
}
