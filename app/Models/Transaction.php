<?php

namespace App\Models;

use Bavix\Wallet\Models\Transaction as TransactionBavix;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaction extends TransactionBavix
{
    use HasFactory;
}
