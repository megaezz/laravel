<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'text', 'published'];

    protected $allowedFilters = ['title'];

    protected $casts = [
        'published' => 'boolean',
    ];

    public function userTasks()
    {
        return $this->hasMany(UserTask::class);
    }

    public function fields()
    {
        return $this->hasMany(TaskField::class);
    }

    public function scopePublished($query, $published = true)
    {
        return $query->wherePublished($published);
    }
}
