<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RknDomain extends Model
{
    use HasFactory;

    protected $fillable = ['domain'];

    public function rkn()
    {
        return $this->belongsTo(Rkn::class);
    }
}
