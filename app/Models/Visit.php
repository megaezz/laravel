<?php

namespace App\Models;

use App\GetRedirectedFromDomainOrSelf;
use engine\app\models\GeoIP;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Visit extends Model
{
    const CREATED_AT = 'date';

    const UPDATED_AT = null;

    protected static function booted()
    {
        static::created(function ($visit) {
            /* уведомлять если был визит на не продленный домен, пока что не использую, т.к. получается 2 лишних запроса на каждый визит, лучше реализовать уведомление в кроне */
            // if ($visit->domainRelation and !$visit->domainRelation->parent_domain_or_self->renewed) {
            //     logger()->alert("Визит {$visit->id} на непродленный домен {$visit->domainRelation->domain}");
            // }
        });
    }

    public function queryWasAbusedByServices()
    {
        return $this->hasMany(Abuse::class, 'query', 'query');
    }

    public function geoIp(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new GeoIp($this->ip);
            }
        )->shouldCache();
    }

    public function ipRelation()
    {
        return $this->belongsTo(Ip::class, 'ip');
    }

    /* host и domain одинаковые, используем хост, потому что на нем есть индекс */
    public function domainRelation()
    {
        return $this->belongsTo(Domain::class, 'host');
    }

    // function ipRelationOrCachedOrCreate(): Attribute {
    //     return Attribute::make(
    //         get: function() {
    //             return $this->ip ? ($this->ipRelation ?? Ip::cachedOrCreate($this->ip)) : null;
    //         }
    //     )->shouldCache();
    // }

    public function scopeHasBan($query, $onlyActive = false)
    {
        return $query->whereExists(function ($subquery) use ($onlyActive) {
            $subquery->select(DB::raw(1))
                ->from('bans')
                ->where(function ($query) {
                    $query
                        ->where('ip', '')
                        ->orWhereRaw('visits.ip REGEXP ip');
                })
                ->where(function ($query) {
                    $query
                        ->where('user_agent', '')
                        ->orWhereRaw('visits.userAgent REGEXP user_agent');
                })
                ->where(function ($query) {
                    $query
                        ->where('referer', '')
                        ->orWhereRaw('visits.referer REGEXP referer');
                })
                ->where(function ($query) {
                    $query
                        ->where('query', '')
                        ->orWhereRaw('visits.query REGEXP query');
                });

            if ($onlyActive) {
                $subquery
                    ->where(function ($query) {
                        $query
                            ->where('active', true)
                            ->orWhere('hide_player', true);
                    });
            }
        });
    }

    public function scopeBan($query, Ban $ban)
    {
        return $query->where(function ($query) use ($ban) {
            if ($ban->ip) {
                $query->where('ip', 'regexp', $ban->ip);
            }
            if ($ban->user_agent) {
                $query->where('userAgent', 'regexp', $ban->user_agent);
            }
            if ($ban->referer) {
                $query->where('referer', 'regexp', $ban->referer);
            }
            if ($ban->query) {
                $query->where('query', 'regexp', $ban->query);
            }

            return $query;
        });
    }

    public function bans(): Attribute
    {
        return Attribute::make(
            get: function () {
                return Ban::query()
                    ->where(function ($query) {
                        $query
                            ->where('ip', '')
                            ->orWhereRaw('? REGEXP ip', [$this->ip]);
                    })
                    ->where(function ($query) {
                        $query
                            ->where('user_agent', '')
                            ->orWhereRaw('? REGEXP user_agent', [$this->userAgent]);
                    })
                    ->where(function ($query) {
                        $query
                            ->where('referer', '')
                            ->orWhereRaw('? REGEXP referer', [$this->referer]);
                    })
                    ->where(function ($query) {
                        $query
                            ->where('query', '')
                            ->orWhereRaw('? REGEXP query', [$this->query]);
                    })
                    ->get();
            }
        )->shouldCache();
    }

    function redirectedFromOrSelf(): Attribute
    {
        return Attribute::make(get: fn() => (new GetRedirectedFromDomainOrSelf)($this))->shouldCache();
    }
}
