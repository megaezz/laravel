<?php

namespace App\Models;

use Bavix\Wallet\Interfaces\Customer;
use Bavix\Wallet\Traits\CanPay;
use Bavix\Wallet\Traits\HasWalletFloat;
use Filament\Models\Contracts\FilamentUser;
use Filament\Panel;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements Customer, FilamentUser
    // , MustVerifyEmail
{
    use \App\Traits\LogsActivity;
    use CanPay;
    use HasRoles;
    use HasWalletFloat;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tasks()
    {
        return $this->hasMany(UserTask::class);
    }

    // не взятые задания
    public function availableTasks()
    {
        return Task::published()->whereDoesntHave('userTasks', function ($query) {
            $query->whereRelation('user', 'id', '=', $this->id);
        });
        // return $this->hasManyThrough(Task::class,UserTask::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function getSubscriptionDaysLeftAttribute()
    {
        return (new \DateTime)->diff(new \DateTime($this->subscription_end_date))->format('%R%a');
        // return date_diff(new \DateTime(), new \DateTime($this->subscription_end_date))->days;
    }

    public function canAccessPanel(Panel $panel): bool
    {
        return $this->can("access {$panel->getId()} panel");
    }
}
