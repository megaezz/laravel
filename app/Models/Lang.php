<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lang extends Model
{
    // чтобы автоматически добавлялись строки при отсутствии перевода
    protected $fillable = ['key', 'page', 'tries'];

    public function keyHash(): Attribute
    {
        return Attribute::make(
            get: function () {
                return sha1($this->key);
            }
        )->shouldCache();
    }

    public static function langArray($path)
    {
        preg_match('#lang/([^/]+)/(.+)\.php$#', $path, $matches);
        $locale = $matches[1] ?? throw new Exception('Не удалось определить локаль');
        $page = $matches[2] ?? throw new Exception('Не удалось определить файл локали');
        $langs = self::class;
        $langs = ($page === 'default')
        ? $langs::whereNull('page')
        : $langs::where('page', $page);

        return $langs
            ->get()
            ->pluck($locale, 'key_hash')
            ->toArray();
    }

    public static function localed(string $key, array $replace, ?string $locale, ?string $page)
    {

        $locale = $locale ?? app()->getLocale();
        $toFind = ($page ?? 'default').'.'.sha1($key);
        $result = __($toFind, $replace, $locale);

        // если результат вернул ключ, то перевод не нашелся
        // добавляем ключ в бд, уведомляем и возвращаем нехешированный ключ
        if ($result === $toFind) {
            $lang = self::updateOrCreate([
                'key' => $key,
                'page' => $page,
            ], [
                'tries' => DB::raw('tries + 1'),
            ]);
            if ($lang->wasRecentlyCreated) {
                // throw new Exception("Не найдена локаль {$locale} для {$page}: {$key}");
                logger()->alert("Не найдена локаль {$locale} для {$key}".($page ? " [{$page}]" : '').', добавили в Lang');
            }

            return $key;
        }

        return $result;
    }
}
