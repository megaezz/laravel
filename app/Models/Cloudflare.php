<?php

namespace App\Models;

use App\Contracts\DnsInterface;
use Cloudflare\API\Adapter\Guzzle;
use Cloudflare\API\Auth\APIKey;
use Cloudflare\API\Endpoints\DNS;
use Cloudflare\API\Endpoints\SSL;
use Cloudflare\API\Endpoints\TLS;
use Cloudflare\API\Endpoints\Zones;

class Cloudflare implements DnsInterface
{
    private Guzzle $adapter;

    public function __construct(public DnsProvider $dnsProvider, private ?Domain $domain, private ?Domain $subdomain)
    {
        $key = new APIKey($this->dnsProvider->email, $this->dnsProvider->api_key);
        $this->adapter = new Guzzle($key);
    }

    public function zones()
    {
        $zones = new Zones($this->adapter);

        return $zones->listZones(perPage: 1000);
    }

    public function getZone()
    {
        $zones = new Zones($this->adapter);
        $zones = $zones->listZones($this->domain->domain);

        return isset($zones->result[0]) ? $zones->result[0] : null;
    }

    public function getDnsRecords(?string $type = '', array|string|null $content = null, int $page = 1, int $perPage = 100000)
    {
        $zone = $this->getZone();
        if (! $zone) {
            throw new \Exception("Не найдена зона домена {$this->domain->domain}");
        }
        $dns = new DNS($this->adapter);
        /* смотрим какой у нас сабдомен - родительский или нет, чтобы решить фильтровать по нему записи или нет */
        $domain = $this->subdomain->is_parent_domain ? '' : $this->subdomain->domain;
        /* т.к. $content теперь array|string для разных реализаций, то дефолтное значение '' указываем тут */
        $content = $content ?? '';

        return $dns->listRecords($zone->id, $type, $domain, $content, $page, $perPage);
    }

    public function addDnsRecord(string $type, string $domain, string $content, bool $proxied, string $priority = '')
    {
        $zone = $this->getZone();
        if (! $zone) {
            throw new \Exception("Не найдена зона домена {$this->domain->domain}");
        }
        $dns = new DNS($this->adapter);

        return $dns->addRecord($zone->id, $type, $domain, $content, 0, $proxied, $priority);
    }

    // function getCnameRecord() {
    //     $dns = $this->getDnsRecords('CNAME',$this->subdomain->domain);
    //     return isset($dns->result[0])?$dns->result[0]:null;
    // }

    public function deleteDnsRecords(?string $type = '', ?string $domain = '', string $content = '')
    {
        $dns = new DNS($this->adapter);
        $records = $this->getDnsRecords($type, $content);

        $records = collect($records->result);

        /* если указан домен, то оставляем записи только с этим доменом */
        /* сделано, чтобы удалять __achme (их нет в базе, поэтому иначе никак не удалить) */
        /* плюс ко всему если сабдомен - родительский домен, что getDnsRecords выдаст весь список записей и удалится все, поэтому обязательно указываем сабдомен для удаления */
        if ($domain) {
            $records = $records->where('name', $domain);
        }

        foreach ($records as $record) {
            $dns->deleteRecord($record->zone_id, $record->id);
        }

        return true;
    }

    public function createZone()
    {
        $zones = new Zones($this->adapter);

        return $zones->addZone($this->domain->domain);
    }

    public function deleteZone()
    {
        $zones = new Zones($this->adapter);

        return $zones->deleteZone($this->getZone()->id);
    }

    public function activationCheck()
    {
        $zones = new Zones($this->adapter);

        return $zones->activationCheck($this->getZone()->id);
    }

    public function applyDefaultSettings()
    {

        $zone = $this->getZone();

        /* flexible ssl */
        $ssl = new SSL($this->adapter);
        $result = $ssl->updateSSLSetting($zone->id, 'flexible');

        /* Automatic HTTPS Rewrites - OFF (вручную, нет в SDK) */
        $result = json_decode($this->adapter->patch("zones/{$zone->id}/settings/automatic_https_rewrites", ['value' => 'off'])->getBody());

        /* always_use_https OFF */
        $result = json_decode($this->adapter->patch("zones/{$zone->id}/settings/always_use_https", ['value' => 'off'])->getBody());

        /* Brotli ON */
        $result = json_decode($this->adapter->patch("zones/{$zone->id}/settings/brotli", ['value' => 'on'])->getBody());

        /* Bypass caching of robots.txt */
        $data = [
            'rules' => [
                [
                    'action' => 'set_cache_settings',
                    'description' => 'Bypass caching of robots.txt',
                    'expression' => '(http.request.uri eq "/robots.txt")',
                    'action_parameters' => [
                        'cache' => false,
                    ],
                ],
            ],
        ];

        $result = json_decode($this->adapter->put("zones/{$zone->id}/rulesets/phases/http_request_cache_settings/entrypoint", $data)->getBody());

        return true;

        // Cache Rules
        // dd(json_decode($this->adapter->get("zones/{$zone->id}/rulesets/phases/http_request_cache_settings/entrypoint")->getBody()));

        // $zoneSettings = new ZoneSettings($this->adapter);
        // получить все настройки
        // $result = json_decode($this->adapter->get("zones/{$zone->id}/settings")->getBody());
        // dd($result);
    }

    public function getNameservers()
    {
        return $this->getZone()->name_servers;
    }

    public function addVpsRecord()
    {
        return $this->addDnsRecord('CNAME', $this->subdomain->domain, Config::cached()->vps_hostname, true);
    }

    public function addGoogleRecord()
    {
        return $this->addDnsRecord('CNAME', $this->subdomain->domain, 'google.awmzone.net', false);
    }

    public function addMosopenRecord()
    {
        return $this->addDnsRecord('CNAME', $this->subdomain->domain, 'mosopen.awmzone.net', false);
    }

    public function deleteCnameRecord()
    {
        return $this->deleteDnsRecords('CNAME', $this->subdomain->domain);
    }

    public function moveToAnotherAccount(DnsProvider $dnsProvider)
    {

        $fromAccount = $this;

        if (! $fromAccount->dnsProvider->allow_migrate_from) {
            throw new \Exception("Миграция из аккаунта {$fromAccount->dnsProvider->email} не разрешена");
        }

        try {

            $messages = [];

            $toAccount = $dnsProvider->dns($this->domain->parent_domain_or_self, $this->domain);

            $messages[] = "{$fromAccount->dnsProvider->email} -> {$toAccount->dnsProvider->email}";

            try {
                /* создаем новую доменную зону на новом аккаунте */
                if ($toAccount->getZone()) {
                    $messages[] = 'Зона уже существует';
                } else {
                    $toAccount->createZone();
                }
            } catch (\Exception $e) {
                throw new \Exception("Ошибка при добавлении зоны: {$e->getMessage()}");
                /* игнорим ошибку при создании зоны, чтобы можно было повторно нажимать на кнопку переноса */
            }

            /* применяем дефолтные настройки */
            $toAccount->applyDefaultSettings();

            /* получаем уже существующие записи */
            $existedDnsRecords = collect($toAccount->getDnsRecords()->result);

            /* получаем все днс записи со старого аккаунта и копируем их в новый аккаунт */
            foreach ($fromAccount->getDnsRecords()->result as $record) {

                /* если такой записи нет, добавляем */
                $isRecordExists = $existedDnsRecords
                    ->where('type', $record->type)
                    ->where('name', $record->name)
                    ->where('content', $record->content)
                    ->where('proxied', $record->proxied);

                if (isset($record->priority)) {
                    $isRecordExists = $isRecordExists->where('priority', $record->priority);
                }

                if ($isRecordExists->first()) {
                    $messages[] = "{$record->type} {$record->name} {$record->content} существует";
                } else {
                    $toAccount->addDnsRecord(
                        $record->type,
                        $record->name,
                        $record->content,
                        $record->proxied,
                        isset($record->priority) ? $record->priority : ''
                    );
                    $messages[] = "{$record->type} {$record->name} {$record->content} добавлена";
                }
            }

            /* меняем cloudflare аккаунт и сохраняем */
            $this->domain->dns_provider_id = $toAccount->dnsProvider->id;
            $this->domain->save();

            $messages[] = 'Готово';
            /* выдаем какие NS нужно проставить */
            $messages[] = collect($toAccount->getZone()->name_servers)->implode(',');

        } catch (\Exception $e) {
            $messages[] = $e->getMessage();
        }

        return $messages;
    }

    public function saveRecordsFromDatabaseToProvider()
    {

        $messages = [];

        try {
            /* создаем новую доменную зону на новом аккаунте */
            if ($this->getZone()) {
                $messages[] = 'Зона уже существует';
            } else {
                $this->createZone();
            }
        } catch (\Exception $e) {
            throw new \Exception("Ошибка при добавлении зоны: {$e->getMessage()}");
        }

        /* применяем дефолтные настройки */
        $this->applyDefaultSettings();

        /* получаем все записи от провайдера */
        $existedDnsRecords = collect($this->getDnsRecords()->result);

        foreach ($this->domain->parentDnsRecords as $record) {

            /* если такой записи нет, добавляем */
            $isRecordExists = $existedDnsRecords
                ->where('type', $record->type)
                ->where('name', $record->name)
                ->where('content', $record->content)
                ->where('proxied', $record->proxied);

            /* скопировал из cloudflare, не знаю нужно тут такое или нет */
            if (isset($record->priority)) {
                $isRecordExists = $isRecordExists->where('priority', $record->priority);
            }

            $isRecordExists = $isRecordExists->isNotEmpty();

            if ($isRecordExists) {
                $messages[] = "{$record->type} {$record->name} {$record->content} существует";
            } else {
                $this->addDnsRecord(
                    $record->type,
                    $record->name,
                    $record->content,
                    $record->proxied,
                    isset($record->priority) ? $record->priority : ''
                );
                $messages[] = "{$record->type} {$record->name} {$record->content} добавлена";
            }
        }

        /* выдаем какие NS нужно проставить */
        $messages[] = collect($this->getZone()->name_servers)->implode(',');

        $messages[] = 'Готово';

        return $messages;
    }

    public function saveRecordsFromProviderToDatabase()
    {

        $records = $this->getDnsRecords()->result;

        $messages = [];

        foreach ($records as $record) {

            $dnsRecord = $dnsRecord = $this->domain->parentDnsRecords
                ->where('domain', $record->name)
                ->where('type', $record->type)
                ->where('content', $record->content)
                ->first();

            if ($dnsRecord) {
                $messages[] = "{$record->type} {$record->name} {$record->content} существует";
            } else {
                $dnsRecord = new DomainDnsRecord;
                $dnsRecord->domain = Domain::find($record->name)?->domain;
                $dnsRecord->name = $record->name;
                $dnsRecord->type = $record->type;
                $dnsRecord->content = $record->content;
                $dnsRecord->proxied = $record->proxied;
                $dnsRecord->priority = $record->priority ?? null;
                $this->domain->parentDnsRecords()->save($dnsRecord);
                $messages[] = "{$record->type} {$record->name} {$record->content} добавлена";
            }

        }

        return $messages;
    }

    public function setTls13(bool $b)
    {
        $zone = $this->getZone();
        $tls = new TLS($this->adapter);
        if ($b) {
            $result = $tls->enableTls13($zone->id);
        } else {
            $result = $tls->disableTls13($zone->id);
        }

        return $result;
    }

    public function getZoneSettings()
    {
        return json_decode($this->adapter->get("zones/{$this->getZone()->id}/settings")->getBody());
    }

    public function setEch(bool $b)
    {
        return json_decode($this->adapter->patch("zones/{$this->getZone()->id}/settings/ech", ['value' => ($b ? 'on' : 'off')])->getBody());
    }
}
