<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTask extends Model
{
    use HasFactory;

    protected $allowedFilters = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function fields()
    {
        return $this->hasMany(UserTaskField::class);
    }

    public function scopeCompleted($query, $bool = true)
    {
        return $query->whereCompleted($bool);
    }

    public function scopeChecked($query, $bool = true)
    {
        return $query->whereChecked($bool);
    }

    public function scopeAccepted($query, $bool = true)
    {
        return $bool ? $query->whereStatus('accepted') : $query->where(function ($query) {
            $query->where('status', '!=', 'accepted');
            $query->orWhereNull('status');
        });
    }

    public function scopeDeclined($query, $bool = true)
    {
        return $bool ? $query->whereStatus('declined') : $query->where(function ($query) {
            $query->where('status', '!=', 'declined');
            $query->orWhereNull('status');
        });
    }

    public function scopeOnCorrection($query, $bool = true)
    {
        return $bool ? $query->whereStatus('correction') : $query->where(function ($query) {
            $query->where('status', '!=', 'correction');
            $query->orWhereNull('status');
        });
    }
}
