<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTaskField extends Model
{
    use HasFactory;

    public function userTask()
    {
        return $this->belongsTo(UserTask::class);
    }

    public function taskField()
    {
        return $this->belongsTo(TaskField::class);
    }
}
