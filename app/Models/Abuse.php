<?php

namespace App\Models;

use App\Services\Cinema\RouteHandlers\RequestHandlerResolver;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Abuse extends Model
{
    protected $fillable = [
        'query',
        'service',
    ];

    protected $casts = [
        'handler_error' => 'boolean',
        'handled' => 'boolean',
        'challenged_on_google_at' => 'datetime',
        'reinstated_on_google_at' => 'datetime',
        'canceled_on_google_at' => 'datetime',
    ];

    protected static function booted()
    {
        static::creating(function ($abuse) {
            static::associateDomain($abuse);
        });
    }

    public static function associateDomain($abuse)
    {

        try {

            $request = Request::create('https://'.$abuse->query, 'GET');

            /* получаем домен из реквеста */
            $domain = Domain::find($request->getHost());

        } catch (\Exception $e) {
            return false;
        }

        if (! $domain) {
            return false;
        }

        $abuse->domain()->associate($domain);
    }

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    /* надо оставить один из этих методов */

    /* обрабатывает ссылку учитывая особенности писем */
    public static function formatUrl($url)
    {
        if (! $url) {
            throw new \Exception('Не передан URL');
        }
        $url = trim($url);
        $url = str_replace(['https://', 'http://'], '', $url);
        $url = urldecode($url);
        $url = urlencode($url);
        $url = str_replace('%2F', '/', $url);
        $url = str_replace('%7E', '~', $url);
        $url = str_replace('%2B', '+', $url);

        return $url;
    }

    /* собираем ссылку заново, учитывая что домен может быть написан по-русски и с пробелами и другие особенности url из писем */
    public static function formatUrlNew($url)
    {
        if (! $url) {
            throw new \Exception('Не передан URL');
        }

        $url = trim($url);

        /* декодируем на всякий случай (использую raw, т.к. он не декодирует плюсы в пробелы, но далее все равно идет замена пробелов на +, поэтому вроде как бы и нет смысла) */
        $url = rawurldecode($url);

        $parsed_url = parse_url($url);

        $path = isset($parsed_url['path']) ? str_replace(' ', '+', $parsed_url['path']) : null;

        /* если домен написан в idn формате - приводим к ascii */
        $host = idn_to_ascii($parsed_url['host']);

        $url = "{$parsed_url['scheme']}://{$host}{$path}";

        return $url;
    }

    /* существует ли ркн абуза на переданный адрес среди адресов актуальных доменов для ботов */
    public static function isRknUriOfActualCrawlerDomainsExists(Domain $domain, $urn)
    {
        return self::whereIn('query', $domain->actual_crawler_mirrors->map(function ($mirror) use ($urn) {
            return self::formatUrl($mirror->domain.$urn);
        }))->whereService('ркн')->first();
    }

    public function isCanonical(): Attribute
    {
        return Attribute::make(
            get: function () {
                $request = Request::create('https://'.$this->query, 'GET');
                try {
                    $result = (new RequestHandlerResolver)($request, \App\Services\Cinema\RouteHandlers\IsUrlCanonical::class);
                } catch (\Exception $e) {
                    return ['error' => $e->getMessage()];
                }

                return $result;
            }
        )->shouldCache();
    }
}
