<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class ClinicType extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;
    use QueryCacheable;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
    ];

    /* таблицу указываю только из-за doctor. */
    public function clinics()
    {
        return $this->belongsToMany(Clinic::class);
    }

    public function fillFromDoctu()
    {
        $this->proprietary = $this->data->proprietary;
        $this->name = $this->data->text;
        $this->doctu_id = $this->data->value;
    }
}
