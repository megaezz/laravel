<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DoctorSpecialty extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;
    use QueryCacheable;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
    ];

    public function doctors()
    {
        return $this->belongsToMany(Doctor::class);
    }

    public function fillFromDoctu()
    {

        $this->name = $this->data->text;
        $this->level = $this->data->level;
        $this->doctu_id = $this->data->value;

    }
}
