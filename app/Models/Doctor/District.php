<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class District extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
    ];

    public function fillFromDoctu()
    {
        $this->name = $this->data->text ?? $this->name;
        $this->city_id = City::where('doctu_id', $this->data->city)->first()->id;
        $this->doctu_id = $this->data->value;
    }
}
