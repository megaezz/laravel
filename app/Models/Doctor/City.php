<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class City extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;
    use QueryCacheable;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
    ];

    public function clinics()
    {
        return $this->hasMany(Clinic::class);
    }

    /* хуй его знает как через отношения сделать, заебался пробовать, всегда какая-то шляпа не юзабельная, в т.ч. hasManyDeep */
    public function doctors()
    {
        return Doctor::whereRelation('clinics', 'city_id', $this->id);
    }

    public function services()
    {
        return Service::whereRelation('clinics', 'city_id', $this->id);
    }

    public function fillFromDoctu()
    {

        $this->name = $this->data->text ?? $this->name;
        $this->abbr = $this->data->textAbbr ?? $this->abbr;
        $this->doctu_id = $this->data->value ?? $this->doctu_id;
    }
}
