<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Metro extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function fillFromDoctu()
    {

        $this->doctu_id = $this->data->value;
        $this->city()->associate(City::where('doctu_id', $this->data->city)->first());
        $this->name = $this->data->text;

    }
}
