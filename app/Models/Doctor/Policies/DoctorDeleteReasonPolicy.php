<?php

namespace App\Models\Doctor\Policies;

use App\Models\Doctor\DoctorDeleteReason;
use App\Models\User;

class DoctorDeleteReasonPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, DoctorDeleteReason $doctorDeleteReason): bool
    {
        return false;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, DoctorDeleteReason $doctorDeleteReason): bool
    {
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, DoctorDeleteReason $doctorDeleteReason): bool
    {
        return false;
    }

    public function deleteAny(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, DoctorDeleteReason $doctorDeleteReason): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, DoctorDeleteReason $doctorDeleteReason): bool
    {
        return false;
    }
}
