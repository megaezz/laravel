<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class MedflexPricelist extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
        'data_wide' => 'object',
    ];

    public function fillFromDoctu()
    {

        $this->clinic_name = $this->data->clinic_name;
        $this->clinic_address = $this->data->clinic_address;
        $this->clinic_city = $this->data->clinic_city;
        $this->status = $this->data->status;
        /* не все города находит, да и не к чему оно, тут задача определить клинику, посмотреть может в клиниках есть ссылка на соответствующий id medflex */
        // $this->city_id = City::where('name', $this->data->clinic_city)->first()?->id;
    }
}
