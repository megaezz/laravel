<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Doctor extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;
    use QueryCacheable;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
        'data_wide' => 'object',
    ];

    /* таблицу указываю только из-за doctor. */
    public function specialties()
    {
        return $this->belongsToMany(DoctorSpecialty::class);
    }

    /* таблицу указываю только из-за doctor. */
    public function clinics()
    {
        return $this->belongsToMany(Clinic::class);
    }

    public function fillFromDoctu()
    {

        $data = [
            'url' => 'url',
            'img' => 'img',
            'firstName' => 'nameFamily',
            'middleName' => 'nameFirst',
            'secondName' => 'nameFathers',
            'rank' => 'rank',
            'degree' => 'degree',
            'category' => 'category',
            'experienceStart' => 'experienceStart',
            'sex' => 'sex',
            'info' => 'info',
            'rating' => 'rating',
            'bestCityCharId' => 'bestCityCharId',
            'deleted_at' => 'deleted_at',
        ];

        foreach ($data as $internal => $external) {
            $this->$internal = ($this->data_wide->$external == '') ? null : $this->data_wide->$external;
        }

        /* синхронизируем специализации (удаляет лишние) */
        $this->specialties()->sync(DoctorSpecialty::whereIn('doctu_id', $this->data_wide->specialities_ids)->get());

        /* синхронизируем клиники (удаляет лишние) */
        $this->clinics()->sync(Clinic::whereIn('doctu_id', collect($this->data_wide->clinics)->pluck('id'))->get());

        // dump($this->id);

        /* прописать связи, одновременно создавая их если отсутствуют */
    }
}
