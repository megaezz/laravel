<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Review extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;
    use QueryCacheable;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
    ];

    public function fillFromDoctu() {}
}
