<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class ClinicService extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    /* получаем услуги (сервисы) которые относятся к данной услуге клиники, вместе с дополнительными полями связующей (pivot) таблицы clinic_service_service */
    public function services()
    {
        return $this->belongsToMany(Service::class)->withPivot('predict', 'match_type', 'status');
    }

    public function clinic()
    {
        return $this->belongsTo(Clinic::class);
    }
}
