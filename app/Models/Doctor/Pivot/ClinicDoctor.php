<?php

namespace App\Models\Doctor\Pivot;

use App\Models\Doctor\Clinic;
use App\Models\Doctor\Doctor;
use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class ClinicDoctor extends Pivot
{
    use HasDatabasePrefix;
    use LogsActivity;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    public function clinic()
    {
        return $this->belongsTo(Clinic::class);
    }

    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }
}
