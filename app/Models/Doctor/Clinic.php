<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Clinic extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;
    use QueryCacheable;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
        'data_wide' => 'object',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /* таблицу указываю только из-за doctor. */
    public function types()
    {
        return $this->belongsToMany(ClinicType::class);
    }

    public function services()
    {
        return $this->hasMany(ClinicService::class);
    }

    public function doctors()
    {
        return $this->belongsToMany(Doctor::class);
    }

    public function childrenClinics()
    {
        return $this->hasMany(self::class, 'parent_clinic_id');
    }

    public function parentClinic()
    {
        return $this->belongsTo(self::class, 'parent_clinic_id');
    }

    public function fillFromDoctu()
    {
        $this->name = $this->data_wide->clinic->name ?? $this->name;
        $this->name_official = $this->data_wide->clinic->nameOfficial ?? $this->name_official;
        $this->registry_phone = $this->data_wide->clinic->registryPhone ?? $this->registry_phone;
        $this->calltouch_phone = $this->data_wide->clinic->calltouchPhone ?? $this->calltouch_phone;
        $this->website = $this->data_wide->clinic->website ?? $this->website;
        $this->city_id = ($this->data_wide->clinic->city ? City::where('doctu_id', $this->data_wide->clinic->city)->first()?->id : null) ?? $this->city_id;
        $this->address = $this->data_wide->clinic->address ?? $this->address;
        /* дважды выполнять, чтобы все родители заполнились? не надо, т.к. doctu_id уже заоплнены, все ок */
        $this->parent_clinic_id = ($this->data_wide->parent?->id ? self::where('doctu_id', $this->data_wide->parent->id)->first()?->id : null) ?? $this->parent_clinic_id;

        $this->has_active_subscription = $this->data_wide->hasActiveSubscription;
        $this->email = $this->data_wide->clinic->email;
        $this->report_to = $this->data_wide->clinic->reportTo;

        /* синхронизируем типы (удаляет лишние) */
        $this->types()->sync(ClinicType::whereIn('doctu_id', $this->data_wide->clinic->types)->get());

        // dd($this->data_wide);
    }
}
