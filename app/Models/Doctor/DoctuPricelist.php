<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class DoctuPricelist extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
        'data_wide' => 'object',
    ];

    public function clinic()
    {
        return $this->belongsTo(Clinic::class);
    }

    public function fillFromDoctu()
    {

        $clinic = Clinic::where('doctu_id', $this->data->clinic->id)->first();

        $this->clinic()->associate($clinic);

        dump($this->id);

        foreach ($this->data_wide->data as $index => $data) {

            // dump($this->id, $index);

            /* есть ли уже такой ClinicService? */
            $clinicService = $clinic->services()->where('doctu_id', $data->id)->first();

            if (! $clinicService) {
                $clinicService = $clinic->services()->create();
            }

            $clinicService->doctu_id = $data->id;
            $clinicService->name = $data->name;
            $clinicService->description = $data->description;
            $clinicService->status = $data->status;
            $clinicService->code = $data->code;
            $clinicService->mixed_price = $data->mixedPrice;
            $clinicService->author_id = $data->author_id;
            $clinicService->save();

            /* теперь синхронизируем услуги для этой записи прайслиста */

            /* получаем докту услуги к которым относится данная позиция прайс-листа */
            $doctuServices = collect((array) $data->services);

            /* получаем соответствущие им наши услуги */
            $services = Service::whereIn('doctu_id', $doctuServices->pluck('id'))->get();

            /* синхронизируем услуги для клиники */
            $clinicService->services()->sync($services);

            /* проходим по всем образовавшимся услугам клиники и проставляем дополнительные данные */
            foreach ($clinicService->services as $service) {

                /* находим соответствующую докту услугу */
                $doctuService = $doctuServices->where('id', $service->doctu_id)->first();

                /* дополняем связующую таблицу данными */
                $service->pivot->predict = $doctuService->predict;
                $service->pivot->match_type = $doctuService->matchType;
                $service->pivot->status = $doctuService->status;
                $service->pivot->save();
            }
            // dump("категорий: " . count((array)$data->category));
        }
    }
}
