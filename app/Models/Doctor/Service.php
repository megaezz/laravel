<?php

namespace App\Models\Doctor;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Service extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;
    use QueryCacheable;

    // protected $database = 'doctor';
    protected $connection = 'doctor';

    protected $casts = [
        'data' => 'object',
    ];

    protected $hidden = ['data'];

    public function childrenServices()
    {
        return $this->hasMany(self::class, 'parent_service_id');
    }

    public function parentService()
    {
        return $this->belongsTo(self::class, 'parent_service_id');
    }

    public function clinicServices()
    {
        return $this->belongsToMany(ClinicService::class);
    }

    /* рекурсивно загружает все родительские услуги */
    public function parentServiceRecursive()
    {
        return $this->parentService()->with('parentServiceRecursive');
    }

    /* возвращает корневую услугу, либо саму себя если и так корневая */
    public function rootService(): Attribute
    {
        return Attribute::make(
            get: function () {
                $service = $this;

                while ($service->parentServiceRecursive) {
                    $service = $service->parentServiceRecursive;
                }

                return $service;
            }
        )->shouldCache();
    }

    public function fillFromDoctu()
    {

        if ($this->data) {
            /* заполняем doctu_id т.к. он не был заполнен при парсинге */
            $this->doctu_id = $this->data->id;
            $this->save();
        } else {
            /* если это не сервис высшего уровня, то он нам не нужен */
            return false;
        }

        /* замыкание, которое обрабатыввает любой сервис, в том числе и вложенные */
        $serviceHandler = function (?self $parent, object $doctuService) use (&$serviceHandler) {
            $service = self::where('doctu_id', $doctuService->id)->first();
            if ($service) {
                dump("{$service->id} уже существует");
            } else {
                $service = new self;
            }
            $service->doctu_id = $doctuService->id;
            $service->depth = $doctuService->depth ?? null;
            $service->name = $doctuService->name ?? null;
            /* ассоциируем родителя */
            if ($parent) {
                $service->parentService()->associate($parent);
            }
            $service->save();

            /* если есть дети, то обрабатываем их рекурсивно */
            if (isset($doctuService->children)) {
                foreach ($doctuService->children as $children) {
                    $serviceHandler($service, $children);
                }
            }
        };

        /* передаем сервис верхнего уровня в замыкание, где он будет рекурсивно обработан */
        $serviceHandler(null, $this->data);
    }
}
