<?php

namespace App\Models;

use Bavix\Wallet\Models\Transfer as TransferBavix;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transfer extends TransferBavix
{
    use HasFactory;
}
