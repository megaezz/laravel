<?php

namespace App\Models;

use App\Services\IsDuckDuckGoBot;
use Illuminate\Database\Eloquent\Model;

class Ip extends Model
{
    protected $primaryKey = 'ip';

    protected $keyType = 'string';

    protected $casts = [
        'is_real_bot' => 'boolean',
    ];

    private static $cached = [];

    public function updateData()
    {
        $ipInfo = self::getIpInfo($this->ip);
        /* если модель уже сохранена в БД и свойство изменилось - уведомляем */
        if ($this->exists and $ipInfo->is_real_bot !== $this->is_real_bot) {
            logger()->alert("IP {$this->ip} изменил свойство is_real_bot с ".json_encode($this->is_real_bot).' на '.json_encode($ipInfo->is_real_bot));
        }
        $this->hostname = $ipInfo->hostname;
        $this->is_real_bot = $ipInfo->is_real_bot;
        $this->crawler = $ipInfo->crawler;
    }

    /* возвращает ip из кеша, если уже был загружен, либо создает новую запись и проверяет сразу ip, избегая возможную коллизию при создании записи */
    public static function cachedOrCreate($ip)
    {
        /* если есть ли уже загруженная модель этого IP, используем ее */
        if (isset(self::$cached[$ip])) {
            $ipModel = self::$cached[$ip];
        } else {
            /* если нет, проверяем существует ли уже IP */
            $ipModel = self::find($ip);
        }

        /* если у нас нет модели, то создаем ее */
        if (! $ipModel) {
            /* создаем объект и проверяем IP */
            $newIp = new self;
            $newIp->ip = $ip;
            $newIp->updateData();
            /* еще раз проверяем наличие IP в БД, т.к. за время проверки кто-то мог записать уже */
            $ipModel = self::find($ip);
            if ($ipModel) {
                logger()->alert("Избежали коллизии при добавлении IP: {$ip}");
            } else {
                $ipModel = $newIp;
                /* пробуем сохранить, или сообщаем в телегу, что не получилось */
                try {
                    $ipModel->save();
                } catch (\Exception $e) {
                    logger()->alert("Не получилось добавить IP: {$e->getMessage()}");
                    /* ничего больше не делаем, т.к. у нас есть объект, можем работать далее, просто он не сохранился */
                }
            }
        }

        /* запоминаем и отдаем */
        self::$cached[$ip] = $ipModel;

        return $ipModel;
    }

    public static function getIpInfo($ip)
    {

        if (empty($ip)) {
            throw new \Exception('Не передан IP');
        }

        $result = [];

        /* сначала проверяем не принадлежит ли IP списку DuckDuckGo ботов, иначе по-другому никак не проверить */
        if ((new IsDuckDuckGoBot)($ip)) {
            $result['is_real_bot'] = true;
            $result['crawler'] = 'DuckDuckBot';
            /* не тратим пока что время на получение имени хоста для дакдак бота, возможно потом изменю это поведение */
            $result['hostname'] = null;

            return (object) $result;
        }

        /* сначала считаем не реальным ботом */
        $result['is_real_bot'] = false;

        /* TODO: реализовать определение названия бота */
        $result['crawler'] = null;

        /* Получаем имя хоста по ip и обратно */

        /* Сначала получаем имя хоста по IP */
        $host = gethostbyaddr($ip);
        $result['hostname'] = $host;

        /* Если имя хоста оканчивается на один из этих хостов с точкой вначале, то будем проверять дальше */
        if (preg_match('/^.+\.(googlebot\.com|google\.com|yandex\.ru|yandex\.net|yandex\.com|search\.msn\.com|mail\.ru|baidu\.com|baidu\.jp|yahoo\.com|yahoo\.net|petalsearch\.com|amazonbot.amazon)$/', $host)) {

            /* получаем обратно - IP по имени хоста */
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $dns = dns_get_record($host, DNS_A);
                $host_ip = $dns[0]['ip'] ?? false;
            } elseif (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
                $dns = dns_get_record($host, DNS_AAAA);
                $host_ip = $dns[0]['ipv6'] ?? false;
            } else {
                $host_ip = false;
                logger()->alert("Не удалось узнать тип IP адреса: {$ip}");
            }

            if ($host_ip) {
                /* если этот итоговый IP совпадает с искомым, то значит бот реальный
                inet_pton помогает сравнить ip, даже если они написаны по-разному */
                if (inet_pton($host_ip) == inet_pton($ip)) {
                    $result['is_real_bot'] = true;
                }
            } else {
                logger()->alert("Не удалось определить IP адрес по хосту для IP: {$ip}");
            }
        }

        return (object) $result;
    }
}
