<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YandexToken extends Model
{
    protected $primaryKey = 'email';

    protected $keyType = 'string';
}
