<?php

namespace App\Models;

use App\Contracts\DnsInterface;
use GuzzleHttp\Client;

class Technitium implements DnsInterface
{
    private Client $client;

    private $acceptedTypes = ['A', 'ANAME', 'TXT'];

    public function __construct(public DnsProvider $dnsProvider, private ?Domain $domain, private ?Domain $subdomain)
    {

        $this->client = new Client([
            'base_uri' => 'http://technitium:5380/api/',
        ]);
    }

    public function call(string $endpoint, ?array $parameters = null)
    {

        $mergedQueries = ['token' => $this->dnsProvider->api_key, 'zone' => $this->domain->domain];

        if ($parameters) {
            $mergedQueries = array_merge($mergedQueries, $parameters);
        }

        $response = json_decode($this->client->get($endpoint, ['query' => $mergedQueries])->getBody()->getContents());

        if ($response->status == 'error') {
            throw new \Exception($response->errorMessage);
        }

        return $response;
    }

    public function getZone()
    {
        try {
            $result = $this->call('zones/options/get');
        } catch (\Exception $e) {
            return false;
        }

        return $result;
    }

    public function getDnsRecords(?string $type = '', array|string|null $content = null, int $page = 1, int $perPage = 100000)
    {

        /* сначала получаем все записи, чтобы потом фильтровать на уровне php, ибо нативная - там пиздец */
        $response = $this->call('zones/records/get', ['domain' => $this->domain->domain, 'listZone' => 'true']);

        $records = collect($response->response->records);

        if ($type) {
            $records = $records->where('type', $type);
        }

        /* смотрим какой у нас сабдомен - родительский или нет, чтобы решить фильтровать по нему записи или нет */
        $records = $this->subdomain->is_parent_domain ? $records : $records->where('name', $this->subdomain->domain);

        if ($content) {
            $records = $records->filter(function ($record) use ($content) {
                return $record->rData == $content;
            });

        }

        return $records;
    }

    public function getContentByTypeAndRdata($type, $rData)
    {

        if (! in_array($type, $this->acceptedTypes)) {
            throw new \Exception("Тип {$type} не поддерживается");
        }

        if ($type == 'A') {
            $content = $rData->ipAddress;
        }

        if ($type == 'ANAME') {
            $content = $rData->aname;
        }

        if ($type == 'TXT') {
            $content = $rData->text;
        }

        return $content;
    }

    public function getRdataByTypeAndContent($type, $content)
    {

        $type = $this->convertRecordTypeToTechnitium($type);

        if (! in_array($type, $this->acceptedTypes)) {
            throw new \Exception("Тип {$type} не поддерживается");
        }

        $rData = new \stdClass;

        if ($type == 'A') {
            $rData->ipAddress = $content;
        }

        if ($type == 'ANAME') {
            $rData->aname = $content;
        }

        if ($type == 'TXT') {
            $rData->text = $content;
        }

        return $rData;
    }

    public function fillParametersByRecordType($parameters, $type, $content)
    {

        if (! in_array($type, $this->acceptedTypes)) {
            throw new \Exception("Тип {$type} не поддерживается");
        }

        $parameters['type'] = $type;

        /* если указан content, то указываем нужный параметр */
        if ($content) {
            if ($type == 'A') {
                $parameters['ipAddress'] = $content;
            }

            if ($type == 'ANAME') {
                $parameters['aname'] = $content;
            }

            if ($type == 'TXT') {
                $parameters['text'] = $content;
            }
        }

        return $parameters;

    }

    public function addDnsRecord(string $type, string $domain, string $content, bool $proxied = true, string $priority = '')
    {

        $parameters['domain'] = $domain;

        $parameters = $this->fillParametersByRecordType($parameters, $type, $content);

        $response = $this->call('zones/records/add', $parameters);

        return $response;
    }

    // public function getCnameRecord() {

    // }

    public function deleteDnsRecords(?string $type = '', ?string $domain = '', string $content = '')
    {

        /* получаем все записи с указанными type и content */
        $records = $this->getDnsRecords($type, $content);

        /* если указан домен, то оставляем записи только с этим доменом */
        if ($domain) {
            $records = $records->where('name', $domain);
        }

        /* теперь удаляем получившийся список */
        foreach ($records as $record) {
            /* если тип не поддерживается, то пропускаем */
            if (! in_array($record->type, $this->acceptedTypes)) {
                continue;
            }
            $parameters['domain'] = $record->name;
            $parameters = $this->fillParametersByRecordType($parameters, $record->type, $this->getContentByTypeAndRdata($record->type, $record->rData));
            // dd($parameters);
            $this->call('zones/records/delete', $parameters);
        }

        return true;
    }

    public function createZone()
    {
        return $this->call('zones/create', ['type' => 'primary']);
    }

    public function deleteZone()
    {
        return $this->call('zones/delete');
    }

    public function activationCheck()
    {
        return true;
    }

    public function applyDefaultSettings()
    {
        return true;
    }

    public function getNameservers()
    {
        return ['ns1.awmzone.net', 'ns2.awmzone.net'];
    }

    public function addVpsRecord()
    {
        $existedRecords = $this->getDnsRecords('ANAME');
        /* проверяем, потому что апи само не проверяет, в отличие от cloudflare */
        if ($existedRecords->count()) {
            throw new \Exception("ANAME запись уже существует: {$existedRecords->first()->rData->aname}");
        }

        return $this->addDnsRecord('ANAME', $this->subdomain->domain, Config::cached()->vps_hostname);
    }

    public function addGoogleRecord()
    {
        $existedRecords = $this->getDnsRecords('ANAME');
        /* проверяем, потому что апи само не проверяет, в отличие от cloudflare */
        if ($existedRecords->count()) {
            throw new \Exception("ANAME запись уже существует: {$existedRecords->first()->rData->aname}");
        }

        return $this->addDnsRecord('ANAME', $this->subdomain->domain, 'google.awmzone.net', false);
    }

    public function addMosopenRecord()
    {
        $existedRecords = $this->getDnsRecords('ANAME');
        /* проверяем, потому что апи само не проверяет, в отличие от cloudflare */
        if ($existedRecords->count()) {
            throw new \Exception("ANAME запись уже существует: {$existedRecords->first()->rData->aname}");
        }

        return $this->addDnsRecord('ANAME', $this->subdomain->domain, 'mosopen.awmzone.net', false);
    }

    public function deleteCnameRecord()
    {
        return $this->deleteDnsRecords('ANAME', $this->subdomain->domain);
    }

    public function saveRecordsFromProviderToDatabase()
    {

        $records = $this->getDnsRecords();

        $messages = [];

        foreach ($records as $record) {

            /* игнорим эти записи */
            if (in_array($record->type, ['NS', 'SOA'])) {
                continue;
            }

            $recordType = $this->convertRecordTypeFromTechnitium($record->type);
            $recordContent = $this->getContentByTypeAndRdata($record->type, $record->rData);

            $dnsRecord = $this->domain->parentDnsRecords
                ->where('name', $record->name)
                ->where('type', $recordType)
                ->where('content', $recordContent)
                ->first();

            if ($dnsRecord) {
                $messages[] = "{$recordType} {$record->name} {$recordContent} существует";
            } else {
                $dnsRecord = new DomainDnsRecord;
                $dnsRecord->domain = Domain::find($record->name)?->domain;
                $dnsRecord->name = $record->name;
                $dnsRecord->type = $recordType;
                $dnsRecord->content = $recordContent;
                $dnsRecord->priority = $record->priority ?? null;
                $this->domain->parentDnsRecords()->save($dnsRecord);
                $messages[] = "{$recordType} {$record->name} {$recordContent} добавлена";
            }

        }

        return $messages;
    }

    public function convertRecordTypeToTechnitium($type)
    {
        return ($type == 'CNAME') ? 'ANAME' : $type;
    }

    public function convertRecordTypeFromTechnitium($type)
    {
        return ($type == 'ANAME') ? 'CNAME' : $type;
    }

    public function saveRecordsFromDatabaseToProvider()
    {

        $messages = [];

        try {
            /* создаем новую доменную зону на новом аккаунте */
            if ($this->getZone()) {
                $messages[] = 'Зона уже существует';
            } else {
                $this->createZone();
            }
        } catch (\Exception $e) {
            throw new \Exception("Ошибка при добавлении зоны: {$e->getMessage()}");
        }

        /* применяем дефолтные настройки */
        $this->applyDefaultSettings();

        /* получаем все записи от провайдера */
        $existedDnsRecords = collect($this->getDnsRecords());

        foreach ($this->domain->parentDnsRecords as $record) {

            /* если такой записи нет, добавляем */
            $isRecordExists = $existedDnsRecords
                ->where('type', $this->convertRecordTypeToTechnitium($record->type))
                ->where('name', $record->name)
                ->where('rData', $this->getRdataByTypeAndContent($record->type, $record->content));

            /* скопировал из cloudflare, не знаю нужно тут такое или нет */
            if (isset($record->priority)) {
                $isRecordExists = $isRecordExists->where('priority', $record->priority);
            }

            $isRecordExists = $isRecordExists->isNotEmpty();

            if ($isRecordExists) {
                $messages[] = "{$record->type} {$record->name} {$record->content} существует";
            } else {
                $this->addDnsRecord(
                    $this->convertRecordTypeToTechnitium($record->type),
                    $record->name,
                    $record->content,
                    $record->proxied,
                    isset($record->priority) ? $record->priority : ''
                );
                $messages[] = "{$record->type} {$record->name} {$record->content} добавлена";
            }

        }

        /* выдаем какие NS нужно проставить */
        $messages[] = collect($this->getNameservers())->implode(',');

        $messages[] = 'Готово';

        return $messages;
    }

    public function setTls13(bool $b)
    {
        return true;
    }

    public function setEch(bool $b)
    {
        return true;
    }
}
