<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Process;

class Node extends Model
{
    protected $casts = [
        'load_average' => 'json',
    ];

    /* если uptime отстутсвует, то выполнить apt install procps */
    public function calcLoadAverage(): Attribute
    {
        return Attribute::make(
            get: function () {

                if ($this->local) {
                    return collect(sys_getloadavg())->transform(function ($value) {
                        return round($value, 2);
                    })->toArray();
                }
                $command = "{$this->ssh} uptime";
                exec($command, $output, $returnCode);
                preg_match('/load average: ([\d.]+), ([\d.]+), ([\d.]+)/', ($output[0] ?? ''), $matches);
                if (empty($matches)) {
                    return false;
                }

                return [
                    (float) $matches[1],
                    (float) $matches[2],
                    (float) $matches[3],
                ];
            }
        )->shouldCache();
    }

    public function calcFreeSpace(): Attribute
    {
        return Attribute::make(
            get: function () {

                /* TODO: соединить в одну строку */
                if ($this->local) {
                    $command = "df --output=pcent / | awk 'NR==2 {print 100 - substr($0, 1, length($0)-1)}'";
                } else {
                    $command = $this->ssh . ' "df --output=pcent / | awk \'NR==2 {print 100 - substr(\$0, 1, length(\$0)-1)}\'"';
                }
                exec($command, $output, $returnCode);
                if (! isset($output[0])) {
                    return false;
                }

                return $output[0];
            }
        )->shouldCache();
    }

    public function setData()
    {
        $this->load_average = $this->calc_load_average;
        $this->free_space = $this->calc_free_space;
        $this->nproc = $this->command('nproc');
        $this->save();
    }

    public function command($command, $dry = false)
    {

        if (! $this->local) {
            // экранируем одинарные кавычки, потому что далее обернем команду в одинарные кавычки
            // $command = str_replace("'", "\\'", $command);
            $command = escapeshellarg($command);
            $command = "{$this->ssh} {$command}";
        }

        if ($dry) {
            return $command;
        }

        $result = Process::run($command);

        if (! $result->successful()) {
            throw new \Exception($result->errorOutput());
        }

        return $result->output();
    }

    public static function secondary()
    {
        return self::where('type', 'secondary')->where('monitored', true)->first() ?? throw new \Exception('Secondary нода не найдена');
    }
}
