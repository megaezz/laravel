<?php

namespace App\Models;

use App\Services\ParseUrls;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParsedUrl extends Model
{
    use HasFactory;

    protected $casts = [
        'headers' => 'object',
        'content' => 'object',
    ];

    /* храним данные в сжатом виде */
    // function content(): Attribute {
    //     return Attribute::make(
    //         get: fn (string $value) => gzcompress($value),
    //         set: fn (string $value) => gzuncompress($value),
    //     );
    // }

    public static function parse()
    {
        return new ParseUrls;
    }
}
