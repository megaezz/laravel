<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cache extends Model
{
    protected $primaryKey = 'key';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $table = 'cache';

    protected $casts = [
        'expiration' => 'timestamp',
        'reset' => 'boolean',
    ];

    public function __construct(array $attributes = [])
    {

        parent::__construct($attributes);

        /* динамически изменяем connection модели исходя из конфига */
        $this->setConnection(config('cache.stores.database.connection'));

    }
}
