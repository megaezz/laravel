<?php

namespace App\Models\Cinema;

use App\Models\Domain;
use App\Traits\LogsActivity;
use cinema\app\models\eloquent\DomainMovie;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Article extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    const CREATED_AT = 'add_date';

    const UPDATED_AT = 'updated_at';

    protected $database = 'cinema';

    protected $casts = [
        'apply_date' => 'datetime',
    ];

    public function engineDomain()
    {
        return $this->belongsTo(Domain::class, 'domain');
    }

    public function acceptorDomain()
    {
        return $this->belongsTo(Domain::class, 'acceptor');
    }

    public function domainMovie()
    {
        return $this->belongsTo(DomainMovie::class);
    }
}
