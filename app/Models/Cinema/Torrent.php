<?php

namespace App\Models\Cinema;

use App\Models\Node;
use App\Models\User;
use App\Services\Cinema\Torrent\CreateMovieUploadsFromTorrent;
use App\Services\Cinema\Torrent\TorrentAria2Service;
use App\Traits\LogsActivity;
use cinema\app\models\eloquent\Dubbing;
use cinema\app\models\eloquent\Movie;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Torrent extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    protected $database = 'cinema';

    protected $casts = [
        'is_donwloaded' => 'boolean',
        'aria2_torrent_data' => 'json',
        'aria2_content_data' => 'json',
    ];

    protected static function booted()
    {
        static::saving(function ($torrent) {
            // пересчитываем hash, если меняется файл торрента
            if ($torrent->isDirty('src')) {
                $torrent->hash = md5(Storage::disk('public')->get($torrent->src));
            }
        });
    }

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function dubbing()
    {
        return $this->belongsTo(Dubbing::class);
    }

    public function uploads()
    {
        return $this->hasMany(MovieUpload::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }

    public function scopeRelatedTorrents()
    {
        return Torrent::query()
            ->where('movie_id', $this->movie_id)
            ->where('dubbing_id', $this->dubbing_id)
            ->where('id', '!=', $this->id);
    }

    // понятное имя для торрента
    public function name(): Attribute
    {
        return Attribute::make(
            get: function () {
                return "Торрент от {$this->created_at->translatedFormat('j F Y H:i')}: {$this->movie->title_ru} ({$this->movie->localed_type}, {$this->movie->year})";
            }
        )->shouldCache();
    }

    public function aria2(): Attribute
    {
        return Attribute::make(
            get: fn () => new TorrentAria2Service($this),
        )->shouldCache();
    }

    public function createMovieUploads()
    {
        return (new CreateMovieUploadsFromTorrent)($this);
    }

    public static function clearDownloads()
    {
        try {
            $result = Node::secondary()->command('rm -rf /home/laravel/storage/app/cinema/torrent-downloads/*');

            return 'Загрузки очищены';
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
