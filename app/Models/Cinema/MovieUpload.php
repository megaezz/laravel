<?php

namespace App\Models\Cinema;

use App\Models\Node;
use App\Services\Cinema\MovieUploadTranscodingCommand;
use App\Traits\LogsActivity;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Dubbing;
use cinema\app\models\eloquent\Movie;
use cinema\app\models\eloquent\MovieEpisode;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class MovieUpload extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    protected $database = 'cinema';

    protected static function booted()
    {
        static::saving(function ($movieUpload) {

            // проверка на соответствие movie_id и episode_id, чтобы эпизод всегда относился к фильму
            if ($movieUpload->movie_id and $movieUpload->movie_episode_id) {
                if ($movieUpload->movie_id !== $movieUpload->episode->movie_id) {
                    throw new \Exception("Эпизод {$movieUpload->movie_episode_id} не относится к фильму {$movieUpload->movie_id}");
                }
            }

            // заполняем автоматически качество исходя из того - какая команда для транскодирования используется, сейчас - 720p.
            if (! $movieUpload->quality) {
                $movieUpload->quality = '720';
            }

            // расчитываем amount
            if (! $movieUpload->amount) {
                $movieUpload->amount = $movieUpload->calcAmount();
            }

            // если hls существует, то значит транскодирования нет
            if ($movieUpload->hls_exists) {
                $movieUpload->is_transcoding = false;
            }
        });
    }

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function episode()
    {
        return $this->belongsTo(MovieEpisode::class, 'movie_episode_id');
    }

    public function torrent()
    {
        return $this->belongsTo(Torrent::class);
    }

    public function dubbing()
    {
        return $this->belongsTo(Dubbing::class);
    }

    public function hls(): Attribute
    {
        return Attribute::make(get: fn ($value) => $value ?? "cinema/turbo/{$this->id}/hls.m3u8")->shouldCache();
    }

    public function size(): Attribute
    {
        return Attribute::make(
            get: function ($value) {
                $size = 0;
                $files = Storage::disk('public')->allFiles(dirname($this->hls));

                foreach ($files as $file) {
                    $size += Storage::disk('public')->size($file);
                }

                return round($size / 1024 / 1024 / 1024, 1);
            })->shouldCache();
    }

    public function calcHlsExists(): Attribute
    {
        // return Attribute::make(get: fn ($value) => Storage::disk('public')->exists($this->hls))->shouldCache();
        return Attribute::make(get: function () {
            try {
                return Http::timeout(1)->head(Storage::disk('storage')->url($this->hls))->successful();
            } catch (\Exception $e) {
                return false;
            }
        })->shouldCache();
    }

    public function calcAmount()
    {
        if (! $this->movie) {
            return null;
        }
        if ($this->movie->type == 'serial') {
            return Config::cached()->turbo_tariff->episode;
        }
        if ($this->movie->type == 'movie') {
            return Config::cached()->turbo_tariff->movie;
        }

        return null;
    }

    public function transcodingCommand()
    {
        return (new MovieUploadTranscodingCommand)($this);
    }

    // отправляет команду на транскодирование
    public function transcode()
    {
        // на всякий случай проверяем, не транскодируется ли уже кто-то
        $transcodingCount = self::where('is_transcoding', true)->count();

        if ($transcodingCount) {
            throw new \Exception('Уже кто-то транскодируется');
        }

        $result = Node::secondary()->command($this->transcodingCommand());
        $this->is_transcoding = true;
        $this->save();

        return true;
    }
}
