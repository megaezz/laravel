<?php

namespace App\Models\Cinema;

use App\Models\Domain;
use cinema\app\models\eloquent\Dubbing;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class TurboDomain extends Model
{

    use HasDatabasePrefix;

    protected $primaryKey = 'domain_domain';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $database = 'cinema';

    function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    function dubbing()
    {
        return $this->belongsTo(Dubbing::class);
    }
}
