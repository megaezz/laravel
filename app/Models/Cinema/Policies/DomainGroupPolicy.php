<?php

namespace App\Models\Cinema\Policies;

use App\Models\Cinema\DomainGroup;
use App\Models\User;

class DomainGroupPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return $user->can('view cinema domain groups');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, DomainGroup $domainGroup): bool
    {
        return $user->can('view cinema domain groups');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $user->can('create cinema domain groups');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, DomainGroup $domainGroup): bool
    {
        return $user->can('update cinema domain groups');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, DomainGroup $domainGroup): bool
    {
        return false;
    }

    public function deleteAny(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, DomainGroup $domainGroup): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, DomainGroup $domainGroup): bool
    {
        return false;
    }
}
