<?php

namespace App\Models\Cinema\Policies;

use App\Models\Cinema\Torrent;
use App\Models\User;

class TorrentPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return $user->can('view cinema torrents');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Torrent $torrent): bool
    {
        return $user->can('view cinema torrents');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return $user->can('create cinema torrents');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Torrent $torrent): bool
    {
        if ($user->can('update own cinema torrents')) {
            return $user->id === $torrent->created_by_id;
        }

        return $user->can('update cinema torrents');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Torrent $torrent): bool
    {
        if ($user->can('delete own cinema torrents')) {
            return $user->id === $torrent->created_by_id;
        }

        return false;
    }

    public function deleteAny(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Torrent $torrent): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Torrent $torrent): bool
    {
        return false;
    }
}
