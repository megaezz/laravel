<?php

namespace App\Models\Cinema\Policies;

use App\Models\Cinema\DomainMovie;
use App\Models\User;

class DomainMoviePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        return $user->can('view cinema domain movies');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, DomainMovie $domainMovie): bool
    {
        return $user->can('view cinema domain movies');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, DomainMovie $domainMovie): bool
    {
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, DomainMovie $domainMovie): bool
    {
        return false;
    }

    public function deleteAny(User $user): bool
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, DomainMovie $domainMovie): bool
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, DomainMovie $domainMovie): bool
    {
        return false;
    }
}
