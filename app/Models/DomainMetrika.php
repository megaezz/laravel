<?php

namespace App\Models;

use engine\app\models\YandexApi;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

class DomainMetrika extends Model
{
    protected $primaryKey = 'domain';

    protected $keyType = 'string';

    protected $casts = [
        'data' => 'object',
    ];

    protected $fillable = [
        'metrika_id',
    ];

    protected static function boot()
    {

        parent::boot();

        static::saving(function ($domainMetrika) {
            // сохряняем значение дневной посещаемости в отдельном поле бд, для сортировки в filament
            $domainMetrika->daily_visits = $domainMetrika->visits;
        });
    }

    /* пришлось называть, так, т.к. есть свойство domain, надо его переименовать в domain_domain, тогда будет норм */
    public function engineDomain()
    {
        return $this->belongsTo(Domain::class, 'domain');
    }

    public function yandexToken()
    {
        return $this->belongsTo(YandexToken::class);
    }

    public function metrikaData(): Attribute
    {
        return Attribute::make(
            get: function () {
                $api = new YandexApi;
                if (! $this->resultedYandexToken) {
                    throw new \Exception("Отсутствует токен яндекс метрики для {$this->domain}");
                }
                $api->setToken($this->resultedYandexToken->token);

                /*
                preset=geo_country - было
                preset=search_engines - стало
                */
                return $api->request([
                    'url' => "https://api-metrika.yandex.net/stat/v1/data?date1=30daysAgo&date2=today&dimensions=ym:s:searchEngine,ym:s:regionCountry&metrics=ym:s:visits&filters=ym:s:trafficSource=='organic'+AND+ym:s:isRobot=='No'&id={$this->metrika_id}",

                ]);
            }
        )->shouldCache();
    }

    /* если есть отдельный токен для метрики - используем его, если нет - берем токен для сайта */
    public function resultedYandexToken(): Attribute
    {
        return Attribute::make(
            get: function () {
                /* если для метрики указан свой токен - используем его */
                if ($this->yandexToken) {
                    return $this->yandexToken;
                }
                /* если нет, то используем общий токен для сайта */
                if ($this->engineDomain->yandexToken) {
                    return $this->engineDomain->yandexToken;
                }

                return null;
            }
        )->shouldCache();
    }

    public function visits(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->data?->totals[0] ?? null;
            }
        )->shouldCache();
    }

    public function ruPercent(): Attribute
    {
        return Attribute::make(
            get: function () {
                if (! isset($this->data->data)) {
                    return null;
                }
                $sum = collect($this->data->data)->sum(function ($data) {
                    return (($data->dimensions[1]->iso_name ?? null) == 'RU') ? $data->metrics[0] : 0;
                });
                /* избегаем деления на ноль */
                if (empty($this->data->totals[0])) {
                    return null;
                }

                return ceil($sum / $this->data->totals[0] * 100);
            }
        )->shouldCache();
    }

    public function crawlerPercent($crawler)
    {
        $sum = $this->crawlerTraffic($crawler) ?? 0;
        /* избегаем деления на ноль */
        if (empty($this->data->totals[0])) {
            return null;
        }

        return round($sum / $this->data->totals[0] * 100, 1);
    }

    public function crawlerTraffic($crawler)
    {
        if (! isset($this->data->data)) {
            return null;
        }

        return collect($this->data->data)->sum(function ($data) use ($crawler) {
            return mb_strstr(($data->dimensions[0]->id ?? null), $crawler) ? $data->metrics[0] : 0;
        });
    }

    public function yandexPercent(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->crawlerPercent('yandex');
            }
        )->shouldCache();
    }

    public function googlePercent(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->crawlerPercent('google');
            }
        )->shouldCache();
    }

    public function yahooPercent(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->crawlerPercent('yahoo');
            }
        )->shouldCache();
    }

    public function bingPercent(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->crawlerPercent('bing');
            }
        )->shouldCache();
    }

    public function duckDuckGoPercent(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->crawlerPercent('duckduckgo');
            }
        )->shouldCache();
    }
}
