<?php

namespace App\Models\Blog;

use App\Models\Domain;
use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Article extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    const CREATED_AT = 'add_date';

    const UPDATED_AT = 'updated_at';

    protected $database = 'blog';

    protected $casts = [
        'apply_date' => 'datetime',
    ];

    public function engineDomain()
    {
        return $this->belongsTo(Domain::class, 'domain');
    }
}
