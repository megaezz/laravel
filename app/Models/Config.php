<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use \App\Traits\LogsActivity;

    protected $casts = [
        'monitored_countries' => 'json',
        'logLongQueries' => 'boolean',
        'switch' => 'boolean',
        'cacheUseDB' => 'boolean',
        'proxiesEnabled' => 'boolean',
        'debugByGet' => 'boolean',
        'debug' => 'boolean',
        'telegram_chats' => 'json',
    ];

    protected static ?Config $cached = null;

    public static function cached()
    {
        if (! static::$cached) {
            static::$cached = Config::first();
        }

        return static::$cached;
    }

    public function mailer()
    {
        return $this->belongsTo(Mailer::class);
    }
}
