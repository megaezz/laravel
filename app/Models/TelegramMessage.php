<?php

namespace App\Models;

use danog\MadelineProto\EventHandler\Message;
use danog\MadelineProto\EventHandler\SimpleFilter\Incoming;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Message as BotApiMessage;

class TelegramMessage extends Model
{
    protected $casts = [
        'data' => 'object',
        'message_id' => 'integer',
        'sender_id' => 'integer',
    ];

    // сообщение которое мы ретранслировали
    public function retranslationOf()
    {
        return $this->belongsTo(self::class, 'retranslation_of');
    }

    // сообщения, которые ретранслировали нас
    public function retranslations()
    {
        return $this->hasMany(self::class, 'retranslation_of');
    }

    public static function fillFromMadeline(Incoming&Message $message)
    {

        $telegramMessage = new TelegramMessage;
        $telegramMessage->message_id = $message->id;
        $telegramMessage->sender_id = $message->senderId;
        $telegramMessage->chat_id = $message->chatId;
        $telegramMessage->reply_of = $message->replyToMsgId;
        $telegramMessage->message = $message->message;
        $telegramMessage->data = $message;

        return $telegramMessage;

    }

    public static function fillFromBotApi(BotApi $bot, BotApiMessage $message, int $senderId)
    {
        $telegramMessage = new TelegramMessage;
        $telegramMessage->message_id = $message->getMessageId();
        $telegramMessage->chat_id = $message->getChat()->getId();
        $telegramMessage->sender_id = $senderId;
        $telegramMessage->reply_of = $message->getReplyToMessage()?->getMessageId();
        $telegramMessage->message = $message->getText();
        $telegramMessage->data = $message;

        return $telegramMessage;
    }

    // ищет сообщения на одно из которых реплаится данное
    public function replyOf()
    {
        return $this->hasMany(self::class, 'message_id', 'reply_of')
            ->where('chat_id', $this->chat_id);
    }

    // function replyOf(): Attribute {
    //     return Attribute::make(
    //         get: function() {

    //             if (!$this->reply_of) {
    //                 return null;
    //             }

    //             return self::query()
    //             ->where('chat_id', $this->chat_id)
    //             ->where('message_id', $this->reply_of);
    //         }
    //         )->shouldCache();
    // }
}
