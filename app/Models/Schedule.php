<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $casts = [
        'last_time' => 'datetime',
        'finished_at' => 'datetime',
    ];
}
