<?php

namespace App\Observers;

use App\Models\Domain;

class DomainObserver
{
    public function saving(Domain $domain)
    {
        // всегда пересчитываем domain_decoded на всякий случай. вдруг изменится имя домена.
        $domain->domain_decoded = idn_to_utf8($domain->domain);

        // наследуем приоритетную студию
        $this->inheritPriorityDubbing($domain);

        // выполняем действия на провайдерах при изменении свойств
        $this->tlsOrEchChanges($domain);
    }

    public function creating(Domain $domain)
    {
        $this->associateParentDomain($domain);
    }

    private static function associateParentDomain($domain)
    {
        /* если родительский домен просчитывается, но связи нет, то создаем ее */
        if ($domain->calculated_parent_domain and ! $domain->parentDomain) {
            /* если родительский домен не существует, то создаем его */
            $parentDomain = Domain::find($domain->calculated_parent_domain);
            if (! $parentDomain) {
                $parentDomain = new Domain;
                $parentDomain->domain = $domain->calculated_parent_domain;
                $parentDomain->type = 'trash';
                $parentDomain->save();
            }
            /* создаем связь с родительским доменом */
            $domain->parentDomain()->associate($parentDomain);
        }
    }

    private function tlsOrEchChanges(Domain $domain)
    {
        if ($domain->dns) {
            if ($domain->isDirty('tls_13')) {
                try {
                    $domain->dns->setTls13($domain->tls_13);
                } catch (\Exception $e) {
                    $domain->tls_13 = $domain->getOriginal('tls_13');
                    logger()->alert("{$domain->domain} - ошибка TLS 1.3: {$e->getMessage()}");
                }
            }
            if ($domain->isDirty('ech')) {
                try {
                    $domain->dns->setEch($domain->ech);
                } catch (\Exception $e) {
                    $domain->ech = $domain->getOriginal('ech');
                    logger()->alert("{$domain->domain} - ошибка ECH: {$e->getMessage()}");
                }
            }
        }
    }

    // наследуем приоритетную студию
    private function inheritPriorityDubbing(Domain $domain)
    {
        foreach (['redirect_to', 'yandex_redirect_to'] as $attribute) {

            // если не изменено или изменено на null – уходим
            if (!$domain->isDirty($attribute) or !$domain->{$attribute}) {
                continue;
            }

            // если redirectTo существовал, то наследуем его студию, а если нет, то студию текущего домена
            if ($domain->getOriginal($attribute)) {
                $copyFrom = Domain::findOrFail($domain->getOriginal($attribute));
            } else {
                $copyFrom = $domain;
            }

            $copyTo = Domain::findOrFail($domain->{$attribute});

            // наследуем приоритетную студию
            if ($copyFrom->turbo?->dubbing) {
                $copyTo->turbo()->updateOrCreate(
                    // я вообще не понял какого хрена, но нужно явно указывать поле связи (нафига, если оно прописано в отношении - я не понял)
                    ['domain_domain' => $copyTo->domain],
                    ['dubbing_id' => $copyFrom->turbo->dubbing->id],
                );
                logger()->alert("Наследовали turbo.dubbing для {$copyTo->domain} от {$copyFrom->domain}");
            }
        }
    }

    /**
     * Handle the Domain "created" event.
     */
    public function created(Domain $domain): void
    {
        //
    }

    /**
     * Handle the Domain "updated" event.
     */
    public function updated(Domain $domain): void
    {
        //
    }

    /**
     * Handle the Domain "deleted" event.
     */
    public function deleted(Domain $domain): void
    {
        //
    }

    /**
     * Handle the Domain "restored" event.
     */
    public function restored(Domain $domain): void
    {
        //
    }

    /**
     * Handle the Domain "force deleted" event.
     */
    public function forceDeleted(Domain $domain): void
    {
        //
    }
}
