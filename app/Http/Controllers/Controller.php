<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // контроллер, чтобы подсунуть ссылку кому-нибудь, возвращаем пустой ответ, просто чтобы посмотреть потом логи посететелей
    public function gotcha()
    {
        return response('', 200);
    }
}
