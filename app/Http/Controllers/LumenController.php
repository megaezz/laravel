<?php

namespace App\Http\Controllers;

use engine\app\controllers\admin\AdminController;
use engine\app\models\Engine;
use engine\app\models\F;

// use cinema\app\models\Movie;

class LumenController extends AdminController
{
    public function test()
    {
        $url = 'https://www.lumendatabase.org/notices/24584938?access_token=vzeoUTXalqAYvxIHyJzrUg';
        $curl = curl_init();
        $proxy = '1.pq.awmzone.net:3128';
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'Accept: application/json',
                'Content-type: application/json',
            ],
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
            // CURLOPT_PROXY => $proxy
        ]);
        $response = curl_exec($curl);
        F::dump($response);

    }

    public function parse()
    {
        $domain = $_GET['domain'] ?? F::error('Domain required');
        $page = $_GET['page'] ?? 1;
        $data = $this->getRequests($domain, $page);
        $redirect = null;
        // F::dump($data);
        if ($data->error == 'wait') {
            F::alert('Ожидание 5 мин. ('.(new \DateTime)->format('Y-m-d H:i:s').')'.F::jsRedirect('/?r=admin/Lumen/parse&domain='.$domain.'&page='.$page, 320));
        }
        if (! $data->found) {
            F::alert('Закончили');
        }
        $page++;
        F::alert('
			Найдено: '.$data->found.' записей<br>
			Добавлено '.$data->inserted.' записей<br>
			Далее: '.$page.
            F::jsRedirect('/?r=admin/Lumen/parse&domain='.$domain.'&page='.$page, 1)
        );
    }

    public function getRequests($domain = null, $page = 1)
    {
        Engine::setDisplayErrors(true);
        if (! $domain) {
            F::error('Domain required');
        }
        $url = 'https://www.lumendatabase.org/notices/search?term='.$domain.'&sort_by=date_received+desc&page='.$page;
        // $url = 'https://lumendatabase.org/notices/search.json?term='.$domain.'&sort_by=date_received+desc&page='.$page;
        // F::dump($url);
        /*
        $curl = curl_init();
        $proxy = '1.pq.awmzone.net:3128';
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            // CURLOPT_HTTPHEADER => [
            // 	'Accept: application/json',
            // 	'Content-type: application/json'
            // ],
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
            // CURLOPT_PROXY => $proxy
        ]);
        $response = curl_exec($curl);
        // F::dump($response);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            F::dump('cURL Error #:'.$err);
        }
        */
        $result = new \stdClass;
        $result->error = false;
        $result->inserted = 0;
        $document = new \DiDom\Document($url, true);
        $elements = $document->find('.results-list .notice');
        $result->found = count($elements);
        // F::dump($elements);
        /*
        if (strstr($response,'Oops, you were browsing a little faster')) {
            $result->error = 'wait';
            return $result;
        }
        $data = json_decode($response);
        */
        foreach ($elements as $v) {
            $id = str_replace('notice_', '', $v->getAttribute('id'));
            $arr = F::query_assoc('select id from '.F::typetab('lumen').' where id = \''.$id.'\';');
            if (! $arr) {
                F::query('insert into '.F::typetab('lumen').' set id = \''.$id.'\';');
                $result->inserted++;
            }
        }

        return $result;
    }
}
