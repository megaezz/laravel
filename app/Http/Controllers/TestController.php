<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Product;
use App\Models\Rate;
use App\Models\User;
use Bavix\Wallet\Objects\Cart;
use engine\app\models\F;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class TestController extends Controller
{
    public function testImap()
    {
        // dd('жопа');
        $imapClient = new \App\Mail\ImapClient;
        $inbox = $imapClient->getInbox();
        dd($inbox);
    }

    public function testGmail()
    {
        $credentials = Storage::path('imposing-ace-389110-a0853c2b3da0.json');
        // Подключение к API с использованием ваших учетных данных
        $client = new \Google_Client;
        $client->setAuthConfig($credentials);
        $client->addScope(\Google_Service_Gmail::GMAIL_READONLY);
        $service = new \Google_Service_Gmail($client);

        // Получение списка писем
        $results = $service->users_messages->listUsersMessages('me');
        $messages = $results->getMessages();
        dd($messages);
        // Получение содержимого письма
        foreach ($messages as $message) {
            $msg = $service->users_messages->get('me', $message->getId());
            $payload = $msg->getPayload();
            $headers = $payload->getHeaders();

            // Обработка заголовков письма и его содержимого
            // ...
        }
    }

    public function testSearch()
    {
        $arr = \cinema\app\models\eloquent\Movie::where(function ($query) {
            $query
                ->where('title_ru', 'like', '%паром%')
                ->orWhere('title_en', 'like', '%паром%')
                ->orWhere('kinopoisk_id', 'like', '%паром%');
        })
            ->whereDeleted(false)
            ->whereIn('type', ['movie', 'serial', 'trailer'])
            ->whereHas('domainMovies', function ($query) {
                $query
                    ->whereDomain('ru.zagonka-tv.com')
                    ->whereActive(true)
                    ->whereDeletedPage(false);
            })
            ->limit(28)
        // ->toSql();
            ->get();

        // dd($arr);
        return 'ok';
    }

    public function testDate()
    {
        $arr = \engine\app\models\F::query_assoc('select unix_timestamp(current_timestamp) as time;');
        // допустимая разница между временем mysql и php - 10 секунд
        dd($arr['time'], time());
    }

    public function testLog()
    {
        return \Log::info('Тест сообщение в лог');
    }

    public function laravel_wallet()
    {
        $user = User::find(1);
        // $product = Product::find(1);
        dd($user->deposit(100));
        // dd($product->getAmountProduct($user));
        // dd($user->pay($product));
        // $user2 = User::find(77);
        // dd($user->transfer($user2,10.5));
        dd($user->balance);
        // dd($user->wallet->refreshBalance());
        dd($user->deposit(100));
    }

    public function fill_transactions()
    {
        $payments = Payment::all();
        foreach ($payments as $payment) {
            $product = null;
            if ($payment->value == 75) {
                $product = Product::find(1);
            }
            if ($payment->value == 500) {
                $product = Product::find(2);
            }
            if (! $product) {
                continue;
            }

            $cart = app(Cart::class)
                ->addItems([$product])
                ->setMeta([
                    'txid' => $payment->txid,
                    'comment' => $payment->comment,
                ]);
            // dd($cart->getTotal($payment->user));
            // dd(User::find(91)->deposit(100));
            // dd($payment->user);
            // dd($payment->user->deposit(50));
            $payment->user->deposit($cart->getTotal($payment->user));
            // dd($cart);
            $transfers = $payment->user->payCart($cart);
        }
        dd('ok');
    }

    public function telegram()
    {
        // установил webhook
        // https://api.telegram.org/bot5204059549:AAEk1yWbqXHE69_Ug4zvuNFoIOE_O2P_F9Y/setwebhook?url=https://kripto.lol/tbot
        $botKey = '5204059549:AAEk1yWbqXHE69_Ug4zvuNFoIOE_O2P_F9Y';
        $update = json_decode(file_get_contents('php://input'), true);
        dd($update);

        $requestFactory = new Http\Factory\Guzzle\RequestFactory;
        $streamFactory = new Http\Factory\Guzzle\StreamFactory;
        $client = new Http\Adapter\Guzzle6\Client;

        $apiClient = new \TgBotApi\BotApiBase\ApiClient($requestFactory, $streamFactory, $client);
        $bot = new \TgBotApi\BotApiBase\BotApi($botKey, $apiClient, new \TgBotApi\BotApiBase\BotApiNormalizer);

        $userId = '<user id>';

        $bot->send(\TgBotApi\BotApiBase\Method\SendMessageMethod::create($userId, 'Hi'));
    }

    public function bibyt()
    {
        $response = Http::get('https://api.bybit.com/derivatives/v3/public/tickers?category=linear');
        $data = json_decode($response->body());
        $rate = new Rate;
        $rate->time = (new \DateTime)->setTimestamp(mb_substr($data->time, 0, 10));
        $rate->time_ms = mb_substr($data->time, -3);
        $rate->data = $data->result->list;
        $rate->save();

        return true;
    }

    public function deepl()
    {
        $sourceText = $_POST['sourceText'] ?? null;
        $translatedText = null;
        if ($sourceText) {
            $deepl = new \BabyMarkt\DeepL\DeepL;
            $translatedText = $deepl->translate($sourceText, 'RU', 'EN');
        }
        F::alertLite('
            <form action="/?r=admin/Test/deepl" method="post">
            <div class="form-group">
            <textarea name="sourceText" class="form-control"></textarea>
            <button class="btn btn-light">Перевести</button>
            </div>
            </form>
            <h2>Исходный текст:</h2>
            <div class="card">'.$sourceText.'</div>
            <h2>Перевод:</h2>
            <div class="card">'.$translatedText.'</div>
            ');
    }
}
