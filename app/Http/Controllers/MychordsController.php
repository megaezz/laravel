<?php

namespace App\Http\Controllers;

use engine\app\models\F;

class MychordsController
{
    public function parseSitemap()
    {
        ini_set('memory_limit', '900M');
        // F::query('truncate '.F::typetab('mychords').';');
        $files = [
            'https://mychords.net/uploads/sitemap1.xml',
            'https://mychords.net/uploads/sitemap2.xml',
            'https://mychords.net/uploads/sitemap3.xml',
        ];
        $i = 0;
        // f::dump('oo');
        // $filename = empty($_GET['filename'])?F::error('Require sitemap filename'):F::checkstr($_GET['filename']);
        // $filename = 'https://mychords.net/uploads/'.$filename;
        $files = [$filename];
        foreach ($files as $file) {
            // $file = F::cache('engine\app\models\F','file_get_contents',array('filename' => $file));
            $file = file_get_contents($file);
            // F::dump($file);
            $parser = xml_parser_create();
            xml_parse_into_struct($parser, $file, $arr);
            foreach ($arr as $v) {
                if ($v['tag'] == 'LOC') {
                    if (strstr($v['value'], '.html')) {
                        $arr = F::query_assoc('select 1 from '.F::typetab('mychords').' where url = \''.$v['value'].'\';');
                        if (! $arr) {
                            // F::dump($v['value']);
                            F::query('insert into '.F::typetab('mychords').' set url = \''.$v['value'].'\' on duplicate key update id = id;');
                            $i++;
                        }
                        // F::dump('test');
                    }
                }
            }
        }
        // $arr = F::query('select count(*) as q from '.F::typetab('mychords').';');
        // F::alert('Загружено '.$arr['q'].' адресов');
        F::alert('Загружено '.$i.' адресов');
    }

    public function compress()
    {
        $arr = F::query_arr('select id,html from '.F::typetab('mychords').' where not checked limit 100;');
        $rows = F::rows_without_limit();
        foreach ($arr as $v) {
            F::query('
				update '.F::typetab('mychords').' set
				compressed = \''.F::escape_string(gzdeflate($v['html'])).'\',
				checked = 1
				where id = \''.$v['id'].'\'
				;');
        }
        $arr = F::query_assoc('select count(*) as q from '.F::typetab('mychords').' where not checked limit 1;');
        F::alert('
			Всего: '.$arr['q'].'
			<script type="text/javascript">setTimeout("window.location.reload()",1000);</script>
			');
    }

    public function parsePages()
    {
        F::dump('ok');
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS url from '.F::typetab('mychords').' where not checked limit 100;');
        $rows = F::rows_without_limit();
        foreach ($arr as $v) {
            $page = file_get_contents($v['url']);
            // $notes = F::copybetwen('<pre class="w-words__text" itemprop="text">','</pre>',$page);
            // notes = \''.F::escape_string($notes).'\',
            F::query('
				update '.F::typetab('mychords').' set
				html = \''.F::escape_string(gzdeflate($page)).'\',
				checked = 1
				where url = \''.$v['url'].'\'
				;');
        }
        $arr = F::query_assoc('select count(*) as q from '.F::typetab('mychords').' where not checked limit 1;');
        F::alert('
			Всего: '.$arr['q'].'
			<script type="text/javascript">setTimeout("window.location.reload()",1000);</script>
			');
    }
}
