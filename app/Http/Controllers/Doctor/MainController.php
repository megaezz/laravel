<?php

namespace App\Http\Controllers\Doctor;

use App\Models\Config;
use App\Models\Doctor\City;
use App\Models\Domain;
use App\Models\Route;
use engine\app\models\Engine;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MainController
{
    public Domain $domain;

    public Domain $requestedDomain;

    public Route $route;

    public Config $engineConfig;

    public View $template;

    public function __construct()
    {
        $this->domain = Engine::getDomainEloquent();
        $this->requestedDomain = Engine::getRequestedDomainEloquent();
        $this->route = Engine::route();
        $this->engineConfig = Config::cached();

        if (! empty($this->route->template)) {
            $this->template = view($this->route->template)
                ->with('domain', $this->domain)
                ->with('requestedDomain', $this->requestedDomain);
        }
    }

    public function index(Request $request)
    {

        $headers = $this->route->getHeaders([
            'domain' => $this->domain,
            'requestedDomain' => $this->requestedDomain,
        ]);

        $cityId = $request->session()->get('cityId');

        if ($cityId) {
            $city = City::where('id', $cityId)->first();
            if (! $city) {
                throw new \Exception('Такого городка нет вообще-то');
            }
        } else {
            $city = City::where('name', 'Москва')->first();
        }

        /* ой хуй знает, из-за того что не получается нормально реализовать отношение doctors, приходится так извращаться */
        $popularCitiesWithDoctorsCount = City::query()
            ->select('cities.*')
            ->selectSub(function ($query) {
                $query
                    ->selectRaw('COUNT(DISTINCT doctors.id)')
                    ->from('doctors')
                    ->join('clinic_doctor', 'clinic_doctor.doctor_id', 'doctors.id')
                    ->join('clinics', 'clinics.id', 'clinic_doctor.clinic_id')
                    ->whereRaw('clinics.city_id = cities.id');
            }, 'doctors_count')
            ->where('is_popular', true)
        // ->ddrawsql()
            ->cacheFor(now()->addDays(1))
            ->get();

        // dd($popularCitiesWithDoctorsCount);

        return $this->template->with(compact('headers', 'city', 'popularCitiesWithDoctorsCount'));
    }

    public function cityIndex(Request $request, $cityDoctuId)
    {

        $city = City::where('doctu_id', $cityDoctuId)->firstOrFail();

        $clinicsCount = $city->clinics()->cacheFor(now()->addDay())->count();
        $doctorsCount = $city->doctors()->cacheFor(now()->addDay())->count();
        // $reviewsCount = $city->reviews()->cacheFor(now()->addDay())->count();
        $reviewsCount = '?';

        $headers = $this->route->getHeaders([
            'domain' => $this->domain,
            'requestedDomain' => $this->requestedDomain,
        ] + compact('city', 'clinicsCount', 'doctorsCount', 'reviewsCount'));

        return $this->template->with(compact('headers', 'city'));
    }

    public function cityClinics(Request $request, $cityDoctuId, $serviceDoctuId)
    {

        $headers = $this->route->getHeaders([
            'domain' => $this->domain,
            'requestedDomain' => $this->requestedDomain,
        ]);

        $city = City::where('doctu_id', $cityDoctuId)->firstOrFail();

        return $this->template->with(compact('headers', 'city'));
    }
}
