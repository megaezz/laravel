<?php

namespace App\Http\Controllers\Types\Engine;

use App\Models\Ip;
use engine\app\models\Engine;
use Illuminate\Http\Request;

class MainController
{
    public function monitoring(Request $request)
    {

        $data = [
            'up' => true,
            'ip' => request()->ip(),
            'ra' => $_SERVER['REMOTE_ADDR'],
            'time' => now()->toDateTimeString(),
            'crawler' => Engine::getCrawler(),
            'irb' => Ip::cachedOrCreate(ip: (request()->get('ip') ?? request()->ip()))->is_real_bot,
            'cc' => Engine::getClientCountry(),
            'scheme' => request()->getScheme(),
        ];

        return response()->json($data);
    }

    public function favicon(Request $request)
    {
        $favicon = Engine::getDomainEloquent()->favicon_src;

        if ($favicon) {
            return redirect($favicon);
        } else {
            abort(404, 'Favicon.ico не указан для сайта');
        }
    }
}
