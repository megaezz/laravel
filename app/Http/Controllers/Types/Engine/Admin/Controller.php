<?php

namespace App\Http\Controllers\Types\Engine\Admin;

use App\Models\Domain;

class Controller
{
    public function domainMenu()
    {
        $domain = Domain::findOrFail(request()->input('domain'));

        return view('types.engine.admin.domain-menu')->with('domain', $domain);
    }

    public function domainSettings()
    {
        $domain = Domain::with('metrika')->findOrFail(request()->input('domain'));

        return view('types.engine.admin.domain-settings')->with('domain', $domain);
    }

    public function domainRoutes()
    {
        $domain = Domain::findOrFail(request()->input('domain'));

        return view('types.engine.admin.domain-routes')->with('domain', $domain);
    }

    public function visits()
    {
        return view('types.engine.admin.visits');
    }

    public function bans()
    {
        return view('types.engine.admin.bans');
    }

    public function visitAnalysis()
    {
        $type = request()->get('type') ?? 'all';
        $service = request()->get('service');
        $days = request()->get('days') ?? 1;
        $limit = request()->get('limit') ?? 2000;
        $withBanInfo = is_null(request()->get('withBanInfo')) ? null : filter_var(request()->get('withBanInfo'), FILTER_VALIDATE_BOOLEAN);
        $withRealBots = is_null(request()->get('withRealBots')) ? null : filter_var(request()->get('withRealBots'), FILTER_VALIDATE_BOOLEAN);
        $q = request()->get('q') ?? 1;

        return view('types.engine.admin.visit-analysis', compact('type', 'service', 'days', 'limit', 'withBanInfo', 'withRealBots', 'q'));
    }
}
