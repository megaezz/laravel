<?php

namespace App\Http\Controllers\Types\Cinema;

use App\Models\Config as EngineConfig;
use cinema\app\models\eloquent\Config as CinemaConfig;
use cinema\app\models\eloquent\DomainMovie;
use cinema\app\models\Movies;
use engine\app\models\Engine;

class MainController
{
    private $domain;

    private $requestedDomain;

    private $route;

    private $template;

    private $engineConfig;

    private $cinemaConfig;

    public function __construct()
    {
        $this->domain = Engine::getDomainEloquent();
        $this->requestedDomain = Engine::getRequestedDomainEloquent();
        $this->domain->cinemaDomain ?? throw new \Exception('Домен Cinema не найден');
        $this->route = Engine::route();
        $this->engineConfig = EngineConfig::cached();
        $this->cinemaConfig = CinemaConfig::cached();

        if (! empty($this->route->template)) {
            /* указан type_template, то подставляем его в путь к шаблону */
            // $template = $this->domain->type_template ? "types.cinema.templates.{$this->domain->type_template}.{$this->route->template}" : $this->route->template;
            $this->template = view($this->route->template)
                ->with('domain', $this->domain)
                ->with('requestedDomain', $this->requestedDomain);
            // ->with('route', $this->route)
            // ->with('engineConfig', $this->engineConfig)
            // ->with('cinemaConfig', $this->cinemaConfig);
        }
    }

    public function index()
    {
        return $this->template
            ->with('headers', $this->route->getHeaders([
                'domain' => $this->domain,
                'requestedDomain' => $this->requestedDomain,
            ]));
    }

    public function footerScripts()
    {
        $blocks = $this->domain->domainBlocks()->whereBlock('footer')->whereActive(true)->get();
        $list = '';
        foreach ($blocks as $block) {
            if (! Engine::isRealBot()) {
                $list .= view($block->view)->with('domain', $this->domain->cinemaDomain);
            }
        }

        return $list;
    }

    public function movieView($id, $slug = null)
    {

        /* TODO выводить только активные */
        if ($this->domain->cinemaDomain->useMovieId) {
            $domainMovie = $this->domain->cinemaDomain->movies()
                ->with('movie.tags')
                ->whereRelation('movie', 'id', $id)
                ->firstOrFail();
        } else {
            $domainMovie = $this->domain->cinemaDomain->movies()
                ->with('movie.tags')
                ->whereId($id)
                ->firstOrFail();
        }

        $headers = $this->route->getHeaders([
            'domainMovie' => $domainMovie,
            // 'domain' => $this->domain,
            // 'requestedDomain' => $this->requestedDomain,
        ]);

        return $this->template
            ->with('headers', $headers)
            ->with('domainMovie', $domainMovie)
        // это для seasongo, придумтаь как лучше делать, может сделать контроллер SeasongoController и там наследовать этот и дописать метод
            ->with('footerScripts', $this->footerScripts())
            ->with('movieOrders', collect(Movies::getOrders())->where('showInSearchOptions', true))
            ->with('engineConfig', EngineConfig::cached())
            ->with('movieDescriptionOrDomainMovieDescriptionShort', $domainMovie->description ? mb_strimwidth((string) $domainMovie->description, 0, 160, '...') : mb_strimwidth((string) $domainMovie->movie->description, 0, 160, '...'));
    }

    public function sitemap()
    {

        $moviesLimit = CinemaConfig::cached()->sitemapMoviesLimit;

        $moviesCount = $this->domain->cinemaDomain
            ->movies()
            ->whereDeletedPage(false)
            ->whereActive(true)
            ->whereHas('movie', function ($query) {
                $query
                    ->whereDeleted(false);
            })
            ->cacheFor(now()->addDays(1))
            ->count();

        $pages_count = ceil($moviesCount / $moviesLimit);

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');

        for ($i = 1; $i <= $pages_count; $i++) {
            $sitemap = $xml->addChild('sitemap');
            $sitemap->addChild('loc', route('movie_sitemap', ['page' => $i]));
        }

        return response($xml->asXml())->header('Content-Type', 'text/xml');
    }

    public function movieSitemap(int $page)
    {

        $moviesLimit = CinemaConfig::cached()->sitemapMoviesLimit;

        /* не кэшируем связи, разницы почти никакой по производительности */
        /* сначала запрашиваем только ID, чтобы не переполнять БД кеша */
        $moviesShort = $this->domain->cinemaDomain
            ->movies()
            ->select('id')
            ->whereDeletedPage(false)
            ->whereActive(true)
            ->whereHas('movie', function ($query) {
                $query
                    ->whereDeleted(false);
            })
            ->take($moviesLimit)
            ->skip(($page - 1) * $moviesLimit)
            ->cacheFor(now()->addDays(1))
            ->get();

        /* далее подтягиваем все необходимое */
        $movies = $this->domain->cinemaDomain->movies()->with('movie');
        $movies = $movies->findMany($moviesShort->pluck('id'));

        /* creating object of SimpleXMLElement */
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');

        foreach ($movies as $movie) {
            $url = $xml->addChild('url');
            $url->addChild('loc', $this->requestedDomain->toRoute->absolute()->movieView($movie));
            $url->addChild('changefreq', 'daily');
            $url->addChild('priority', 0.8);
        }

        return response($xml->asXml())->header('Content-Type', 'text/xml');
    }

    public function groupView($id, $slug)
    {
        $group = $this->domain->cinemaDomain->mergedGroups()->findOrFail($id);
        $headers = $this->route->getHeaders(['group' => $group, 'domain' => $this->domain]);

        return $this->template
            ->with('headers', $headers)
            ->with('group', $group);
    }

    /* где то в роутах используется, создал пока чтобы не было ошибок */
    public function genre() {}

    public function embedPlayers(DomainMovie $domainMovie)
    {

        if ($domainMovie->domain != $this->domain->domain) {
            throw new \Exception('Видео не принадлежит домену');
        }

        return view('types.cinema.players.embed')->with('domainMovie', $domainMovie);
    }

    public function robots()
    {
        return response($this->template)->header('Content-Type', 'text/plain');
    }
}
