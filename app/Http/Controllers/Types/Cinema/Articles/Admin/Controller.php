<?php

namespace App\Http\Controllers\Types\Cinema\Articles\Admin;

use cinema\app\models\Article;
use cinema\app\models\Articles;
// use cinema\app\controllers\articles\admin\Admin;
use cinema\app\models\Domain;
use cinema\app\models\DomainMovie;
use cinema\app\models\Domains;
use cinema\app\models\Movies;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class Controller
{
    public function __construct()
    {
        Engine::setType('cinema');
    }

    public function index()
    {

        $articles = new Articles;
        $articles->setOrder('articles.add_date desc');
        $articles->setListTemplate('admin/articles/list');
        $articles->setLimit($articles->getRows());
        $articlesList = $articles->getList();

        return view('types.cinema.admin.articles.list')
            ->with('articlesList', $articlesList);
    }

    public function add()
    {
        $id = Article::add();

        return redirect()->route('cinema.articles.edit', ['id' => $id]);
    }

    public function editor($id)
    {

        $article = new Article($id);
        $t = new Template;
        //
        $images = $article->getImages();
        $listImg = '';
        $listOpt = '';
        foreach ($images as $v) {
            if ($article->getMainImage() === $v) {
                $active = 'active';
                $selected = 'selected';
            } else {
                $active = '';
                $selected = '';
            }
            $listImg .= '
			<img class="'.$active.'" src="'.$article->getImagesUrl().'/'.$v.'" data-file="'.$v.'">
			<!--
			<button class="btn btn-sm" style="position: relative; top: -35px; left: -15px;">
			<i class="fas fa-trash-alt"></i>
			</button>
			-->
			';
            $listOpt .= '<option '.$selected.'>'.$v.'</option>';
        }
        $t->v('imagesList', $listImg);
        $t->v('imagesOptions', $listOpt);
        //
        $t->v('title', 'Редактирование статьи #'.$article->getId());
        $t->v('articleId', $article->getId());
        $t->v('articleTitle', htmlspecialchars($article->getTitle()));
        $t->vf('articleText', htmlspecialchars($article->getText()));
        $t->vf('articleTextPreview', htmlspecialchars($article->getTextPreview()));
        $t->v('articleDomainMovieId', $article->getDomainMovieId());
        $articles = new Articles;
        $articles->setOrder('articles.add_date desc');
        $articles->setLimit(30);
        $articles->setPage(1);
        $articles->setActiveId($id);
        $articles->setListTemplate('admin/articles/list');
        $t->v('articlesList', $articles->getList());
        // получаем список доменов
        $domains = new Domains;
        $list = '';
        foreach ($domains->get() as $v) {
            $domainObj = new Domain($v['domain']);
            $lastDomain = $domainObj->getLastRedirectDomain();
            $selected = ($article->getDomain() == $v['domain']) ? 'selected' : '';
            $list .= '<option value="'.$v['domain'].'" '.$selected.'>'.$lastDomain.'</option>';
        }
        $t->v('domainsOptionList', $list);
        $list = '';
        foreach ($domains->get() as $v) {
            $domainObj = new Domain($v['domain']);
            $lastDomain = $domainObj->getLastRedirectDomain();
            $selected = ($article->getAcceptor() == $v['domain']) ? 'selected' : '';
            $list .= '<option value="'.$v['domain'].'" '.$selected.'>'.$lastDomain.'</option>';
        }
        $t->v('acceptorsOptionList', $list);
        // селекторы активации
        $list = '';
        $arr = ['Нет' => false, 'Да' => true];
        foreach ($arr as $txt => $v) {
            $selected = ($article->getActive() == $v) ? 'selected' : '';
            $list .= '<option value="'.($v ? 1 : 0).'" '.$selected.'>'.$txt.'</option>';
        }
        $t->v('activeOptionList', $list);
        // ссылка на видео
        if ($article->getDomainMovieId()) {
            $movie = new DomainMovie($article->getDomainMovieId());
            $articleDomainObj = new Domain($article->getDomain());
            $articleDomain = $articleDomainObj->getRedirectTo() ? $articleDomainObj->getRedirectTo() : $articleDomainObj->getDomain();
            $articleLink = $articleDomain.$articleDomainObj->getWatchLink($movie);
            $t->v('articleMovieWatchLink', '<a href="http://'.$articleLink.'">http://'.$articleLink.'</a>');
        } else {
            $t->v('articleMovieWatchLink', '');
        }

        // dd($t);

        return view('types.cinema.admin.articles.edit')
            ->with('imagesList', $t->getVar('imagesList'))
            ->with('imagesOptions', $t->getVar('imagesOptions'))
            ->with('articleId', $t->getVar('articleId'))
            ->with('articleTitle', $t->getVar('articleTitle'))
            ->with('articleText', $t->getVar('articleText'))
            ->with('articleTextPreview', $t->getVar('articleTextPreview'))
            ->with('articleDomainMovieId', $t->getVar('articleDomainMovieId'))
            ->with('articlesList', $t->getVar('articlesList'))
            ->with('domainsOptionList', $t->getVar('domainsOptionList'))
            ->with('acceptorsOptionList', $t->getVar('acceptorsOptionList'))
            ->with('activeOptionList', $t->getVar('activeOptionList'))
            ->with('articleMovieWatchLink', $t->getVar('articleMovieWatchLink'));
    }

    public function save()
    {
        $id = empty($_POST['id']) ? F::error('Article ID reuired') : $_POST['id'];
        $title = empty($_POST['title']) ? null : $_POST['title'];
        $mainImage = empty($_POST['mainImage']) ? null : $_POST['mainImage'];
        $text = empty($_POST['text']) ? null : $_POST['text'];
        $textPreview = empty($_POST['textPreview']) ? null : $_POST['textPreview'];
        $domainMovieId = empty($_POST['domainMovieId']) ? null : $_POST['domainMovieId'];
        $domain = empty($_POST['domain']) ? null : $_POST['domain'];
        $acceptor = empty($_POST['acceptor']) ? null : $_POST['acceptor'];
        $active = empty($_POST['active']) ? false : true;
        $article = new Article($id);
        // если заполнено DomainMovieId, то проверяем, если для домена стоит настройка UseMovieId = true, то значит передан movieId, получаем исходя из него DomainMovieId
        if ($article->getDomainMovieId() != $domainMovieId) {
            if ($domain) {
                $domainObj = new Domain($domain);
                if ($domainObj->getUseMovieId()) {
                    // получаем DomainMovieId из MovieId
                    $movies = new Movies;
                    $movies->setDomain($domainObj->getDomain());
                    $movies->setMovieId($domainMovieId);
                    $arr = $movies->get();
                    if ($arr) {
                        $domainMovieId = $arr[0]['domain_movies_id'];
                    } else {
                        $domainMovieId = null;
                    }
                }
            }
        }
        $article->setTitle($title);
        // убираем тэг редактора
        $text = str_replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>', '', $text);
        $text = str_replace('rel="noopener noreferrer"', '', $text);
        $article->setText($text);
        $article->setTextPreview($textPreview);
        $article->setMainImage($mainImage);
        $article->setDomainMovieId($domainMovieId);
        $article->setDomain($domain);
        $article->setAcceptor($acceptor);
        // если активируется то прописываем дату активации
        if ($article->getActive() === false and $active === true) {
            $date = new \DateTime;
            $article->setApplyDate($date->format('Y-m-d H:i:s'));
        }
        $article->setActive($active);
        $article->save();

        // F::alert('Article #'.$article->getId().' has been saved');
        return redirect()->back();
    }

    public function delete()
    {
        $id = empty($_POST['id']) ? F::error('Article ID reuired') : F::checkstr($_POST['id']);
        $article = new Article($id);
        $article->delete();

        return redirect()->route('cinema.articles.list');
    }
}
