<?php

namespace App\Http\Controllers\Types\Cinema;

use cinema\app\controllers\UserController;
use cinema\app\models\Cinema;
use cinema\app\models\DomainMovie;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\User as UserEloquent;
use cinema\app\models\Movie;
use cinema\app\models\Movies;
use cinema\app\models\User;
use cinema\app\models\Users;
use engine\app\models\Engine;
use engine\app\models\Enot;
use engine\app\models\F;
use engine\app\models\Template;
use engine\app\models\TextRu;

class CustomerController extends UserController
{
    protected $auth = null;

    protected $user = null;

    protected $userEloquent = null;

    public function __construct()
    {
        if (Config::cached()->kinocontent_maintenance) {
            F::alertLite(Config::cached()->kinocontent_maintenance_message);
        }

        /* сейчас в этом контроллере есть методы, доступные и без аутентификации, поэтому проверяем есть ли она */
        /* не проверяем  auth()->getType() == cinema, доверяем middleware на роутах */
        if (Engine::auth()) {
            $this->auth = Engine::auth() ?? throw new \Exception('Нет аутентификации');
            $this->userEloquent = UserEloquent::where('login', $this->auth->getLogin())->firstOrFail();
            $this->user = $this->userEloquent->megaweb;
        }
    }

    public function blackList()
    {
        $arr = F::query_arr('
			select worker from '.F::typetab('customer_blacklist').'
			where customer = \''.$this->user->getLogin().'\'
			;');
        $list = '';
        foreach ($arr as $v) {
            $worker = new User($v['worker']);
            $list .= '<li><i class="fas fa-user"></i>&nbsp;# '.$worker->getId().' <a class="btn btn-sm btn-light" href="/removeFromBlacklist?workerId='.$worker->getId().'" title="Удалить из черного списка"><i class="fas fa-times"></i></a> '.($worker->getExpressRewriter() ? ('<span class="badge badge-success">Хороший исполнитель, советуем удалить из ЧС</span>') : '').'</li>';
        }
        $t = new Template('admin/customer/blacklist');
        $t->v('list', $list ? $list : '<li>Никого нет</li>');
        $this->setMenuVars($t, 'done');

        return $t->get();
    }

    public function addToBlacklist()
    {
        $workerId = empty($_POST['workerId']) ? F::error('Worker ID required') : F::checkstr($_POST['workerId']);
        $users = new Users;
        $users->setLevels(['worker', 'moderator']);
        $users->setId($workerId);
        $arr = $users->get();
        if (! $arr) {
            F::error('Such rewriter ID was not found');
        }
        $worker = new User($arr[0]['login']);
        F::query('
			insert into '.F::typetab('customer_blacklist').'
			set customer = \''.$this->user->getLogin().'\', worker = \''.$worker->getLogin().'\'
			on duplicate key update customer = customer
			;');
        F::redirect('/blacklist');
    }

    public function removeFromBlacklist()
    {
        $workerId = empty($_GET['workerId']) ? F::error('Worker ID required') : F::checkstr($_GET['workerId']);
        $users = new Users;
        $users->setLevels(['worker', 'moderator']);
        $users->setId($workerId);
        $arr = $users->get();
        if (! $arr) {
            F::error('Such rewriter ID was not found');
        }
        $worker = new User($arr[0]['login']);
        F::query('
			delete from '.F::typetab('customer_blacklist').'
			where customer = \''.$this->user->getLogin().'\' and worker = \''.$worker->getLogin().'\'
			;');
        F::redirect('/blacklist');
    }

    public function statByDays()
    {
        $cinema = new Cinema;
        $arr = F::query_arr('
			select date(description_date) as day,count(*) as q,
			sum(char_length(description)) as symbols,
			sum(char_length(description)*customer_price_per_1k/1000) as paid
			from '.F::typetab('domain_movies').'
			where customer = \''.$this->user->getLogin().'\'
			and description is not null
			group by day
			order by day desc
			;');
        $list = '';
        $totalSum = 0;
        $totalQ = 0;
        $totalSymbols = 0;
        foreach ($arr as $v) {
            // $paid = $v['symbols']*$cinema->getCustomerPricePer1k()/1000;
            $list .= '<tr><td>'.$v['day'].'</td><td>'.$v['q'].'</td><td>'.$v['symbols'].'</td><td>'.round($v['paid'], 2).' руб.</td></tr>';
            $totalSum = $totalSum + $v['paid'];
            $totalQ = $totalQ + $v['q'];
            $totalSymbols = $totalSymbols + $v['symbols'];
        }
        $t = new Template('admin/customer/stat_by_days');
        $t->v('list', $list);
        $t->v('totalSum', round($totalSum, 2));
        $t->v('totalQ', $totalQ);
        $t->v('totalSymbols', $totalSymbols);
        $this->setMenuVars($t, 'stat');

        return $t->get();
    }

    public function index()
    {
        return view('types.cinema.admin.kinocontent.index')
            ->with([
                'user' => $this->userEloquent,
                'config' => Config::cached(),
                'customerPricePer1k' => $this->userEloquent->megaweb->getCustomerPricePer1k(),
            ]);
    }

    public function doneCsv()
    {
        $archived = empty($_GET['archived']) ? false : true;
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=kinocontent_done.csv');
        $t = new Template('admin/customer/done_csv/page');
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setLimit(700);
        $movies->setWithDomainDescription(true);
        $movies->setOrder('domain_movies.description_date desc');
        $movies->setDomainMovieShowToCustomer(true);
        $movies->setDomainMovieCustomerArchived($archived);
        // $movies->setListTemplate('admin/customer/done_csv/item');
        $list = '';
        foreach ($movies->get() as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            $l = new Template('admin/customer/done_csv/item');
            $l->v('domainMovieId', str_replace('"', '""', $movie->getDomainMovieId()));
            $l->v('domainMovieDescriptionDate', str_replace('"', '""', $movie->getDomainMovieDescriptionDate()));
            $l->v('movieTitleRu', str_replace('"', '""', $movie->getTitleRu()));
            $l->v('movieKinopoiskId', str_replace('"', '""', $movie->getKinopoiskId()));
            $l->v('domainMovieDescription', str_replace('"', '""', $movie->getDomainMovieDescription()));
            $list .= $l->get();
        }
        // $t->v('list',$movies->getList());
        $t->v('list', $list);

        return "\xEF\xBB\xBF".$t->get();
    }

    public function done()
    {
        $archived = empty($_GET['archived']) ? 0 : 1;
        $page = empty($_GET['p']) ? 1 : F::checkstr($_GET['p']);
        $showToCustomer = isset($_GET['moderated']) ? ($_GET['moderated'] ? true : false) : null;
        $domainMovieId = empty($_GET['id']) ? null : F::checkstr($_GET['id']);
        $t = new Template('admin/customer/done');
        // считаем кол-во элементов в архиве
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setDomainMovieCustomerArchived(true);
        $t->v('archivedRows', $movies->getRows());
        //
        // считаем кол-во элементов отмодерированное
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieCustomerArchived(false);
        $movies->setDomainMovieShowToCustomer(true);
        $t->v('moderatedRows', $movies->getRows());
        //
        // считаем кол-во элементов не отмодерированное
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieCustomerArchived(false);
        $movies->setDomainMovieShowToCustomer(false);
        $t->v('notModeratedRows', $movies->getRows());
        //
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setDomainMovieShowToCustomer($showToCustomer);
        $movies->setLimit(10);
        $movies->setWithDomainDescription(true);
        $movies->setOrder('domain_movies.description_date desc');
        $movies->setPage($page);
        $movies->setPaginationUrlPattern('/done?archived='.$archived.'&amp;'.(is_null($showToCustomer) ? '' : ('moderated='.$showToCustomer.'&amp;')).'p=(:num)');
        if (! $domainMovieId) {
            $movies->setDomainMovieCustomerArchived($archived);
        }
        $movies->setDomainMovieId($domainMovieId);
        // $movies->setDomainMovieShowToCustomer(true);
        $templates = [
            'movie' => [
                'archived' => 'admin/customer/items/movie_archived',
                'done' => 'admin/customer/items/movie_done',
                'done_rework' => 'admin/customer/items/movie_done_rework',
                'moderation' => 'admin/customer/items/movie_moderation',
            ],
            'game' => [
                'archived' => 'admin/customer/items/game_archived',
                'done' => 'admin/customer/items/game_done',
                'done_rework' => 'admin/customer/items/game_done_rework',
                'moderation' => 'admin/customer/items/game_moderation',
            ],
            'other' => [
                'archived' => 'admin/customer/items/other_done',
                'done' => 'admin/customer/items/other_done',
                'done_rework' => 'admin/customer/items/other_done',
                'moderation' => 'admin/customer/items/other_done',
            ],
        ];
        $list = '';
        foreach ($movies->get() as $v) {
            // F::dump($v);
            $domainMovie = new DomainMovie($v['domain_movies_id']);
            $taskType = $domainMovie->getTaskType();
            if ($domainMovie->getDomainMovieCustomerArchived()) {
                $l = new Template($templates[$taskType]['archived']);
            } else {
                if ($domainMovie->getDomainMovieRework()) {
                    $l = new Template($templates[$taskType]['done_rework']);
                } else {
                    $l = new Template($templates[$taskType]['done']);
                }
                if (! $domainMovie->getDomainMovieShowToCustomer()) {
                    $l = new Template($templates[$taskType]['moderation']);
                }
            }
            // F::dump($domainMovie);
            if ($domainMovie->getDomainMovieDescriptionWriter()) {
                $user = new User($domainMovie->getDomainMovieDescriptionWriter());
                $l->v('descriptionWriterId', $user->getId());
            } else {
                $l->v('descriptionWriterId', '');
            }
            if ($domainMovie->getUniqueId() == 'not_in_base') {
                $l->v('type', 'тип неизвестен');
                $l->v('year', 'год неизвестен');
            } else {
                $l->v('type', $domainMovie->getTypeRu(true));
                $l->v('year', $domainMovie->getYear());
            }
            $l->v('descriptionWriter', $domainMovie->getDomainMovieDescriptionWriter());
            $l->v('descriptionDate', $domainMovie->getDomainMovieDescriptionDate());
            if ($domainMovie->getDomainMovieShowToCustomer()) {
                $l->v('domainMovieDescription', nl2br($domainMovie->getDomainMovieDescription()));
                $textRu = $domainMovie->getDomainMovieTextRuResult();
                if ($textRu) {
                    $l->v('unique', $textRu->unique.'%');
                    $plagiatUrls = TextRu::getPlagiatUrls($textRu);
                    $l->v('plagiatUrls', $plagiatUrls ? '<ol>'.$plagiatUrls.'</ol>' : '');
                } else {
                    if ($domainMovie->getDomainMovieTextRuDescriptionId()) {
                        $l->v('unique', 'ожидание');
                    } else {
                        $l->v('unique', 'нет данных');
                    }
                    $l->v('plagiatUrls', '');
                }
                $l->v('domainMovieTextRuDescriptionId', $domainMovie->getDomainMovieTextRuDescriptionId());
            } else {
                $l->v('unique', '');
                $l->v('domainMovieDescription', '');
                $l->v('plagiatUrls', '');
                $l->v('domainMovieTextRuDescriptionId', '');
            }
            $l->v('description', nl2br(($domainMovie->getDescription() ?? '')));
            $l->v('titleRu', $domainMovie->getTitleRu());
            $l->v('domainMovieId', $domainMovie->getDomainMovieId());
            $l->v('poster', $domainMovie->getPosterOrPlaceholder());
            $l->v('domainMovieDescriptionCustomerPrice', $domainMovie->getDomainMovieDescriptionCustomerPrice());
            $l->v('domainMovieDescriptionLength', mb_strlen($domainMovie->getDomainMovieDescription()));
            $l->v('domainMovieDescriptionLengthWithoutSpaces', mb_strlen(str_replace(' ', '', $domainMovie->getDomainMovieDescription())));
            $l->v('domainMovieReworkComment', $domainMovie->getDomainMovieReworkComment());
            $l->v('domainMovieCustomerComment', nl2br(($domainMovie->getDomainMovieCustomerComment() ?? '')));
            $l->v('domainMoviePrivateComment', nl2br(($domainMovie->getDomainMoviePrivateComment() ?? '')));
            $symbolsRequired = $domainMovie->getDomainMovieSymbolsFrom() ? ('от '.$domainMovie->getDomainMovieSymbolsFrom()) : '';
            $symbolsRequired .= $domainMovie->getDomainMovieSymbolsTo() ? (' до '.$domainMovie->getDomainMovieSymbolsTo()) : '';
            $l->v('symbolsRequired', $symbolsRequired ? ('<b>Требуемое количество символов:</b> '.$symbolsRequired) : '');
            $l->v('kinopoisk_id', $domainMovie->getKinopoiskId());
            // f::dump($domainMovie->getDomainMovieDescription());
            $l->v('domainMovieCustomerLike', is_null($domainMovie->getDomainMovieCustomerLike()) ? '' : ($domainMovie->getDomainMovieCustomerLike() ? 1 : 0));
            $l->v('domainMovieRework', $domainMovie->getDomainMovieRework() ? 1 : 0);
            $l->v('domainMovieShowToCustomer', $domainMovie->getDomainMovieShowToCustomer() ? 1 : 0);
            $l->v('domainMovieCustomerArchived', $domainMovie->getDomainMovieCustomerArchived() ? 1 : 0);
            $list .= $l->get();
        }
        $t->v('moviesList', $list);
        $t->v('moviesPages', $movies->getPages());
        $t->v('moviesRows', $movies->getRows());
        $t->v('archivedActive', $archived ? 'active' : '');
        $t->v('jsonArchived', json_encode($archived));
        $t->v('moderatedActive', ($showToCustomer === true) ? 'active' : '');
        $t->v('notModeratedActive', ($showToCustomer === false) ? 'active' : '');
        $this->setMenuVars($t, 'done');

        return $t->get();
    }

    public function setMenuVars($t = null, $active = null)
    {
        $arr = ['index', 'done', 'stat'];
        foreach ($arr as $v) {
            $t->v('menu'.ucfirst($v).'Active', ($v == $active) ? 'active' : '');
        }
        $t->v('userLogin', $this->user->getLogin());
        $t->v('userRewriteBalance', $this->user->getRewriteBalance());
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieCustomerArchived(false);
        $t->v('doneRows', $movies->getRows());
        $t->v('adminLiteSection', in_array($this->user->getLevel(), ['admin', 'admin-lite']) ? '{admin/admin-lite/admin_lite_section.html}' : '');

        return true;
    }

    public function searchInputHelper()
    {
        Engine::setDebug(false);
        $query = empty($_GET['q']) ? null : trim(htmlspecialchars(F::checkstr($_GET['q'])));
        if (mb_strlen($query) < 2) {
            return;
        }
        // если передан URL кинопоиска, то подменяем query на ID кинопоиска
        preg_match('/kinopoisk\.ru\/(film|series)\/(\d+)/', $query, $found);
        if ($found) {
            $query = $found[2];
        }
        $movies = new Movies;
        // $movies->setDomain($this->domain->getDomain());
        $movies->setSearch($query);
        $movies->setLeftSearch(true);
        $movies->setLimit(10);
        $movies->setPage(1);
        $movies->setOrder('cinema.kinopoisk_rating desc');
        $movies->setListTemplate('admin/customer/items/list_search_helper_movies');
        $movies->setIncludeSerials(true);
        $movies->setIncludeMovies(true);
        $movies->setIncludeTrailers(true);
        // $movies->setIncludeNullType(true);
        $list_1 = $movies->getList();
        $list_2 = '';
        if (! $movies->getRows()) {
            $movies->setLeftSearch(false);
            $list_2 = $movies->getList();
        }
        // если нет ничего, то предлагаем "Нет в базе"
        $list_3 = '';
        if (empty($list_1.$list_2)) {
            // $movies->setSearch(null);
            // $movies->setUniqueId('not_in_base');
            // $list_3 = $movies->getList();
            $t = new Template('admin/customer/items/list_search_helper_not_found');
            $list_3 = $t->get();
        }

        return $list_1.$list_2.$list_3;
    }

    public function searchDoneMoviesHelper()
    {
        Engine::setDebug(false);
        $query = empty($_GET['q']) ? null : trim(htmlspecialchars(F::checkstr($_GET['q'])));
        if (mb_strlen($query) < 2) {
            return;
        }
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(true);
        $movies->setSearch($query);
        $movies->setSearchByCustomerComment(true);
        $movies->setLeftSearch(true);
        $movies->setLimit(10);
        $movies->setPage(1);
        $movies->setListTemplate('admin/customer/items/list_search_done_movies_helper');
        $movies->setIncludeSerials(true);
        $movies->setIncludeMovies(true);
        $movies->setIncludeTrailers(true);
        $list_1 = $movies->getList();
        $list_2 = '';
        if (! $movies->getRows()) {
            $movies->setLeftSearch(false);
            $list_2 = $movies->getList();
        }

        return $list_1.$list_2;
    }

    public function addMovieToQueue()
    {
        Engine::setDebug(false);
        $movieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        $maxQueue = 1000;
        // проверяем чтобы очередь была не более maxQueue
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(false);
        $movies->setLimit(1);
        $movies->setPage(1);
        if ($movies->getRows() >= $maxQueue) {
            F::error('Максимальная очередь &mdash; '.$maxQueue.' штук');
        }
        $this->addDomainMovie($movieId);

        return json_encode(true);
    }

    public function submitAddOtherTask()
    {
        $comment = empty($_POST['comment']) ? null : $_POST['comment'];
        $symbolsFrom = empty($_POST['symbolsFrom']) ? null : F::checkstr($_POST['symbolsFrom']);
        $symbolsTo = empty($_POST['symbolsTo']) ? null : F::checkstr($_POST['symbolsTo']);
        $privateComment = empty($_POST['privateComment']) ? null : F::checkstr($_POST['privateComment']);
        $adult = empty($_POST['adult']) ? false : ($_POST['adult'] ? true : false);
        $movieId = Movies::getNotInBaseId();
        $maxQueue = 1000;
        // проверяем чтобы очередь была не более maxQueue
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(false);
        $movies->setLimit(1);
        $movies->setPage(1);
        if ($movies->getRows() >= $maxQueue) {
            F::error('Максимальная очередь &mdash; '.$maxQueue.' штук');
        }
        $domainMovie = $this->addDomainMovie($movieId);
        $domainMovie->setDomainMovieSymbolsFrom($symbolsFrom);
        $domainMovie->setDomainMovieSymbolsTo($symbolsTo);
        $domainMovie->setDomainMovieCustomerComment($comment);
        $domainMovie->setDomainMoviePrivateComment($privateComment);
        $domainMovie->setDomainMovieAdult($adult);
        $domainMovie->save($this->user->getLogin());
        F::redirect('/');
    }

    public function checkIsMovieInQueue()
    {
        $movieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        Engine::setDebug(false);
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(false);
        $movies->setMovieId($movieId);

        return json_encode($movies->getRows());
    }

    public function checkIsMovieInDoneTasks()
    {
        $movieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        Engine::setDebug(false);
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(true);
        $movies->setMovieId($movieId);

        return json_encode($movies->getRows());
    }

    public function deleteMovieFromQueue()
    {
        Engine::setDebug(false);
        $domainMovieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        $domainMovie = new DomainMovie($domainMovieId);
        // F::dump($domainMovie->getDomainMovieCustomer().' vs '.$this->user->getLogin());
        if ($domainMovie->getDomainMovieCustomer() !== $this->user->getLogin()) {
            F::error('Не ваш фильм');
        }
        if ($domainMovie->getDomainMovieDescription()) {
            F::error('Нельзя удалить, т.к. описание уже было написано');
        }
        if ($domainMovie->getDomainMovieBookedForLogin()) {
            if ($domainMovie->getDomainMovieBookedForLogin() !== 'ozerin') {
                F::error('Нельзя удалить, фильм уже забронирован за рерайтером');
            }
        }
        $domainMovie->delete();

        return json_encode(true);
    }

    public function duplicateTask()
    {
        Engine::setDebug(false);
        $domainMovieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        $domainMovie = new DomainMovie($domainMovieId);
        // F::dump($domainMovie->getDomainMovieCustomer().' vs '.$this->user->getLogin());
        if ($domainMovie->getDomainMovieCustomer() !== $this->user->getLogin()) {
            F::error('Не ваше задание');
        }
        $created = $this->addDomainMovie($domainMovie->getId());
        $created->setDomainMovieSymbolsFrom($domainMovie->getDomainMovieSymbolsFrom());
        $created->setDomainMovieSymbolsTo($domainMovie->getDomainMovieSymbolsTo());
        $created->setDomainMovieCustomerComment($domainMovie->getDomainMovieCustomerComment());
        if ($domainMovie->getDomainMovieIsExpress()) {
            $cinema = new Cinema;
            $created->setDomainMovieDescriptionPricePer1k($cinema->getRewriteExpressPricePer1k());
            $created->setDomainMovieCustomerPricePer1k($this->user->getCustomerExpressPricePer1k());
        }
        $created->save();

        // F::dump($created);
        return json_encode(true);
    }

    public function confirmMovieFromQueue()
    {
        Engine::setDebug(false);
        $domainMovieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        $domainMovie = new DomainMovie($domainMovieId);
        // F::dump($domainMovie->getDomainMovieCustomer().' vs '.$this->user->getLogin());
        if ($domainMovie->getDomainMovieCustomer() !== $this->user->getLogin()) {
            F::error('Не ваше задание');
        }
        if ($domainMovie->getDomainMovieIsBooked()) {
            F::error('Запрещено. Задание уже забронировано');
        }
        if ($domainMovie->getDomainMovieIsConfirmed()) {
            $domainMovie->setDomainMovieBookedForLogin(null);
            $domainMovie->setDomainMovieCustomerConfirmDate(null);
        } else {
            $domainMovie->setDomainMovieBookedForLogin('ozerin');
            $domainMovie->setDomainMovieCustomerConfirmDate((new \DateTime)->format('Y-m-d H:i:s'));
        }
        $domainMovie->save($this->user->getLogin());

        return json_encode(true);
    }

    public function confirmAllMoviesFromQueue()
    {
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(false);
        $movies->setWithBookedForLogin(false);
        $movies->setLimit($movies->getRows());
        $arr = $movies->get();
        // F::dump($arr);
        foreach ($arr as $v) {
            $domainMovie = new DomainMovie($v['domain_movies_id']);
            $domainMovie->setDomainMovieBookedForLogin('ozerin');
            $domainMovie->setDomainMovieCustomerConfirmDate((new \DateTime)->format('Y-m-d H:i:s'));
            $domainMovie->save($this->user->getLogin());
        }
        F::redirect(F::referer_url());
    }

    public function deleteAllMoviesFromQueue()
    {
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(false);
        $movies->setWithBookedForLogin(false);
        $movies->setLimit($movies->getRows());
        $arr = $movies->get();
        // F::dump($arr);
        foreach ($arr as $v) {
            $domainMovie = new DomainMovie($v['domain_movies_id']);
            $domainMovie->delete();
        }
        F::redirect(F::referer_url());
    }

    public function moviesQueueList()
    {
        Engine::setDebug(false);
        $maxQueue = 1000;
        $t = new Template('admin/customer/queue_list');
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setOrder('domain_movies.add_date desc');
        $movies->setLimit($maxQueue);
        $movies->setPage(1);
        $movies->setWithDomainDescription(false);
        $arr = $movies->get();
        $list = '';
        $expectedPrice = 0;
        $q_not_booked = 0;
        $cinema = new Cinema;
        $templates = [
            'movie' => [
                'confirmed' => 'admin/customer/items/movie_queue_confirmed',
                'booked' => 'admin/customer/items/movie_queue_booked',
                'queue' => 'admin/customer/items/movie_queue',
            ],
            'game' => [
                'confirmed' => 'admin/customer/items/game_queue_confirmed',
                'booked' => 'admin/customer/items/game_queue_booked',
                'queue' => 'admin/customer/items/game_queue',
            ],
            'other' => [
                'confirmed' => 'admin/customer/items/other_queue_confirmed',
                'booked' => 'admin/customer/items/other_queue_booked',
                'queue' => 'admin/customer/items/other_queue',
            ],
        ];
        foreach ($arr as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            $taskType = $movie->getTaskType();
            if ($movie->getDomainMovieBookedForLogin()) {
                if ($movie->getDomainMovieBookedForLogin() === 'ozerin') {
                    $l = new Template($templates[$taskType]['confirmed']);
                } else {
                    $l = new Template($templates[$taskType]['booked']);
                }
            } else {
                $q_not_booked = $q_not_booked + 1;
                $l = new Template($templates[$taskType]['queue']);
            }
            $l->v('domainMovieId', $movie->getDomainMovieId());
            $l->v('titleRu', $movie->getTitleRu());
            $l->v('titleEn', $movie->getTitleEn());
            $l->v('type', $movie->getTypeRu(true));
            $l->v('year', $movie->getYear());
            $l->v('bookedByRewriterIcon', ($movie->getDomainMovieBookedForLogin() !== 'ozerin') ? '<i class="fas fa-user" title="Забронировано рерайтером"></i>' : '');
            $l->v('domainMovieCustomerComment', htmlspecialchars(($movie->getDomainMovieCustomerComment() ?? '')));
            $l->v('domainMovieCustomerCommentPreview', htmlspecialchars(mb_strimwidth(str_replace(PHP_EOL, ' ', ($movie->getDomainMovieCustomerComment() ?? '')), 0, 90, '…')));
            $l->v('domainMoviePrivateComment', htmlspecialchars(($movie->getDomainMoviePrivateComment() ?? '')));
            $l->v('domainMovieSymbolsFrom', $movie->getDomainMovieSymbolsFrom());
            $l->v('domainMovieSymbolsTo', $movie->getDomainMovieSymbolsTo());
            $l->v('domainMovieIsExpress', $movie->getDomainMovieIsExpress() ? 1 : 0);
            $l->v('customerExpressFare', $cinema->getCustomerExpressFare());
            $expectedPrice = $expectedPrice + $movie->getExpectedPrice();
            $list .= $l->get();
        }
        $t->v('moviesList', $list);
        $rows = $movies->getRows();
        $t->v('confirmAllMoviesFromQueueButton', $q_not_booked ? '<a class="btn btn-sm btn-secondary" href="/confirmAllMoviesFromQueue"><i class="fas fa-check"></i> Отправить все на выполнение</a>' : '');
        $t->v('deleteAllMoviesFromQueueButton', $q_not_booked ? '<a class="btn btn-sm btn-danger mr-2" href="/deleteAllMoviesFromQueue"><i class="far fa-trash-alt"></i> Очистить очередь</a>' : '');
        $t->v('moviesRows', $rows);
        $price1k = $this->user->getCustomerPricePer1k();
        $t->v('recommendedBalance', ceil($expectedPrice));

        return $t->get();
    }

    public function logout()
    {
        $this->auth->clearSID();
        F::redirect('/');
    }

    public function submitRework()
    {
        Engine::setDebug(false);
        $domainMovieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        // убрали checkstr т.к. иначе дважды экранируются символы (далее в domainMovie->save())
        $reworkComment = empty($_POST['rework_comment']) ? null : $_POST['rework_comment'];
        $domainMovie = new DomainMovie($domainMovieId);
        // F::dump($domainMovie->getDomainMovieCustomer().' vs '.$this->user->getLogin());
        if ($domainMovie->getDomainMovieCustomer() !== $this->user->getLogin()) {
            F::error('Не ваш фильм');
        }
        if (! $domainMovie->getDomainMovieDescription()) {
            F::error('Задание не было выполнено, нельзя отправить на доработку');
        }
        if (date_diff(new \DateTime, new \DateTime($domainMovie->getDomainMovieDescriptionDate()))->days > 14) {
            F::error('Нельзя отправить на доработку, прошло более 2-х недель.');
        }
        if (! $reworkComment) {
            F::error('Не указан комментарий доработки');
        }
        $domainMovie->setDomainMovieRework(true);
        $domainMovie->setDomainMovieReworkComment($reworkComment);
        $domainMovie->setDomainMovieReworkDate((new \DateTime)->format('Y-m-d H:i:s'));
        $domainMovie->save($this->user->getLogin());
        // return json_encode(true);
        F::redirect(F::referer_url());
    }

    public function moveToArchive()
    {
        $domainMovieId = empty($_GET['id']) ? F::error('Movie ID required') : F::checkstr($_GET['id']);
        $moveBack = empty($_GET['moveBack']) ? false : true;
        $domainMovie = new DomainMovie($domainMovieId);
        if ($domainMovie->getDomainMovieCustomer() !== $this->user->getLogin()) {
            F::error('Не ваш фильм');
        }
        if (! $domainMovie->getDomainMovieDescription()) {
            F::error('Задание не было выполнено, нельзя отправить в архив');
        }
        $domainMovie->setDomainMovieCustomerArchived($moveBack ? false : true);
        $domainMovie->save($this->user->getLogin());
        F::redirect(F::referer_url());
    }

    public function submitSetSymbols()
    {
        $comment = empty($_POST['comment']) ? null : $_POST['comment'];
        $symbolsFrom = empty($_POST['symbolsFrom']) ? null : F::checkstr($_POST['symbolsFrom']);
        $symbolsTo = empty($_POST['symbolsTo']) ? null : F::checkstr($_POST['symbolsTo']);
        $privateComment = empty($_POST['privateComment']) ? null : F::checkstr($_POST['privateComment']);
        $checkIsMovieInDoneTasks = empty($_POST['checkIsMovieInDoneTasks']) ? null : (($_POST['checkIsMovieInDoneTasks'] == 'on') ? true : false);
        $autoExpress = empty($_POST['autoExpress']) ? null : (($_POST['autoExpress'] == 'on') ? true : false);
        $this->user->setSymbolsFrom($symbolsFrom);
        $this->user->setSymbolsTo($symbolsTo);
        $this->user->setCustomerComment($comment);
        $this->user->setCustomerPrivateComment($privateComment);
        $this->user->setCheckIsMovieInDoneTasks($checkIsMovieInDoneTasks);
        $this->user->setCustomerAutoExpress($autoExpress);
        // F::dump($this->user);
        $this->user->save();
        F::redirect(F::referer_url());
    }

    public function deposit()
    {

        $usdRubRate = F::getUsdRubRateCached() ? (F::getUsdRubRateCached() * (1 - (new Cinema)->getUsdRubShave() / 100)) : null;
        $enotUserId = Enot::getTimeInt($this->user->getId());

        return view('types.cinema.admin.kinocontent.deposit.index')
            ->with([
                'user' => $this->userEloquent,
            ] + compact('usdRubRate', 'enotUserId'));
    }

    public function wmDepositFail()
    {
        return view('types.cinema.admin.kinocontent.deposit.fail')->with('user', $this->userEloquent);
    }

    public function wmDepositSuccess()
    {
        return view('types.cinema.admin.kinocontent.deposit.success')->with('user', $this->userEloquent);
    }

    public function userInfo()
    {
        $code_sent = empty($_GET['code_sent']) ? false : true;
        $t = new Template('admin/customer/user-info');
        $t->v('userId', $this->user->getId());
        $t->v('userEmail', $this->user->getEmail());
        $t->v('userPassword', $this->user->getPassword());
        $t->v('jsonEmailConfirmed', json_encode($this->user->getEmailConfirmed()));
        // рефералы
        $users = new Users;
        $users->setPartner($this->user->getLogin());
        $arr = $users->get();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<li>'.$v['login'].'</li>';
        }
        $t->v('userReferralsList', $list ? ('<ol>'.$list.'</ol>') : 'список пуст');
        $t->v('userApiKey', $this->user->getApiKey());
        $t->v('jsonCodeSent', json_encode($code_sent));
        F::setPageVars($t);
        $this->setMenuVars($t);

        return $t->get();
    }

    public function privateMessageForCustomer()
    {
        $message = null;
        if ($this->user->getLogin() === 'jumanji') {
            // $message = 'jumanji, насчет вашего вопроса - фильмы не ушли в работу, т.к. вы не нажали на галочку.';
        }
        $content = $message ? ('<div class="alert alert-info">'.$message.'</div>') : '';

        return $content;
    }

    public function editMovieSettings()
    {
        $domainMovieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        $editPrivateCommentOnly = empty($_POST['editPrivateCommentOnly']) ? false : true;
        $comment = empty($_POST['comment']) ? null : $_POST['comment'];
        $privateComment = empty($_POST['privateComment']) ? null : $_POST['privateComment'];
        $symbolsFrom = empty($_POST['symbolsFrom']) ? null : F::checkstr($_POST['symbolsFrom']);
        $symbolsTo = empty($_POST['symbolsTo']) ? null : F::checkstr($_POST['symbolsTo']);
        $domainMovie = new DomainMovie($domainMovieId);
        if ($domainMovie->getDomainMovieCustomer() !== $this->user->getLogin()) {
            F::error('Не ваш фильм');
        }
        if ($editPrivateCommentOnly) {
            $domainMovie->setDomainMoviePrivateComment($privateComment);
        } else {
            if ($domainMovie->getDomainMovieBookedForLogin()) {
                F::error('Нельзя редактировать, фильм уже забронирован за рерайтером');
            }
            $domainMovie->setDomainMoviePrivateComment($privateComment);
            $domainMovie->setDomainMovieCustomerComment($comment);
            $domainMovie->setDomainMovieSymbolsFrom($symbolsFrom);
            $domainMovie->setDomainMovieSymbolsTo($symbolsTo);
        }
        $domainMovie->save($this->user->getLogin());
        F::redirect(F::referer_url());
    }

    public function addMovieByKinopoiskId()
    {
        $kpId = empty($_GET['id']) ? F::error('Kinopoisk ID is required') : F::checkstr($_GET['id']);
        $movies = new Movies;
        $movies->setKinopoiskId($kpId);
        $movies->setIncludeNullType(true);
        $arr = $movies->get();
        // F::dump($arr);
        if ($arr) {
            $movie = new Movie($arr[0]['id']);

            return $movie->getTypeRu(true).' &laquo;'.$movie->getTitleRu().'&raquo; (Kinopoisk ID: '.$movie->getKinopoiskId().') присутствует в базе';
        }
        $url = 'http://apivideo.ru/api/kinopoisk.json?id='.$kpId.'&token=037313259a17be837be3bd04a51bf678';
        $result = json_decode(file_get_contents($url));
        if (isset($result->error)) {
            return $result->error->message;
        }
        // F::dump($result);
        $movie = new Movie(Movie::add());
        $movie->setKinopoiskId($result->id);
        $movie->setTitleRu($result->name_ru);
        $movie->setTitleEn($result->name_en);
        $movie->setYear($result->year);
        $movie->setDescription($result->description);
        $movie->save($this->user->getLogin());

        return 'Добавлено в базу: &laquo;'.$movie->getTitleRu().'&raquo; ('.$movie->getYear().') г.';
    }

    public function submitMassLoadMovies()
    {
        $list = empty($_POST['list']) ? F::error('List required') : $_POST['list'];
        $checkDouble = empty($_POST['checkDouble']) ? false : true;
        $checkAlreadyDone = empty($_POST['checkDone']) ? false : true;
        $checkIsAlreadyInQueue = empty($_POST['checkInQueue']) ? false : true;
        $arr_enter = explode("\r\n", $list);
        $arr_dots = explode(',', $list);
        if (count($arr_enter) > count($arr_dots)) {
            $arr = $arr_enter;
        } else {
            $arr = $arr_dots;
        }
        if (count($arr) > 200) {
            F::error('Максимум 200 элементов списка');
        }
        $arr_result['done'] = [];
        $arr_result['not_found'] = [];
        $arr_result['already_done'] = [];
        $arr_result['already_in_queue'] = [];
        $handledKpIds = [];
        foreach ($arr as $v) {
            $str = trim($v);
            // если пустая строка - пропускаем
            if (empty($str)) {
                continue;
            }
            preg_match('/kinopoisk\.ru\/(film|series)\/(\d+)/', $str, $found);
            if ($found) {
                $kpId = $found[2];
            } else {
                $kpId = $str;
            }
            // if (strstr($str,'kinopoisk')) {
            // 	$kpId = F::copybetwen('/film/','/',$str);
            // } else {
            // 	$kpId = $str;
            // }
            if ($checkDouble) {
                if (in_array($kpId, $handledKpIds)) {
                    continue;
                }
            }
            $kpId = F::checkstr($kpId);
            $handledKpIds[] = $kpId;
            // получаем movieId из ID кинопоиска
            $movies = new Movies;
            $movies->setKinopoiskId($kpId);
            $arr_movies = $movies->get();
            $movieId = isset($arr_movies[0]['id']) ? $arr_movies[0]['id'] : null;
            if ($movieId) {
                if ($checkAlreadyDone) {
                    $moviesDone = new Movies;
                    $moviesDone->setDomainMovieCustomer($this->user->getLogin());
                    $moviesDone->setWithDomainDescription(true);
                    $moviesDone->setMovieId($movieId);
                    // F::dump($moviesDone);
                    // F::dump($moviesDone->get());
                    if ($moviesDone->getRows()) {
                        $arr_result['already_done'][] = $str;

                        continue;
                    }
                }
                if ($checkIsAlreadyInQueue) {
                    $moviesQueue = new Movies;
                    $moviesQueue->setDomainMovieCustomer($this->user->getLogin());
                    $moviesQueue->setWithDomainDescription(null);
                    $moviesQueue->setMovieId($movieId);
                    // F::dump($moviesDone);
                    // F::dump($moviesDone->get());
                    if ($moviesQueue->getRows()) {
                        $arr_result['already_in_queue'][] = $str;

                        continue;
                    }
                }
                $arr_result['done'][] = $str;
                $this->addDomainMovie($movieId);
            } else {
                $arr_result['not_found'][] = $str;
            }
            // $arr_result[] = $kpId;
        }
        $doneList = '';
        $notFoundList = '';
        $alreadyDoneList = '';
        $alreadyInQueueList = '';
        foreach ($arr_result['already_done'] as $v) {
            $alreadyDoneList .= '<li>'.$v.'</li>';
        }
        foreach ($arr_result['already_in_queue'] as $v) {
            $alreadyInQueueList .= '<li>'.$v.'</li>';
        }
        foreach ($arr_result['done'] as $v) {
            $doneList .= '<li>'.$v.'</li>';
        }
        foreach ($arr_result['not_found'] as $v) {
            $notFoundList .= '<li>'.$v.'</li>';
        }
        $t = new Template('admin/customer/submit_mass_load_movies');
        $t->v('doneList', $doneList);
        $t->v('notFoundList', $notFoundList);
        $t->v('alreadyDoneList', $alreadyDoneList);
        $t->v('alreadyInQueueList', $alreadyInQueueList);
        $this->setMenuVars($t);

        return $t->get();
        // F::dump($arr_result);
        // F::dump($arr);
    }

    // создает задание и возвращает объект
    private function addDomainMovie($movieId = null)
    {
        if (empty($movieId)) {
            F::error('Movie ID required to use User::addDomainMovieByCustomer');
        }
        $domainMovieId = DomainMovie::add($movieId);
        $domainMovie = new DomainMovie($domainMovieId);
        $domainMovie->setCustomerSettings($this->user->getLogin());
        $domainMovie->save($this->user->getLogin());

        return $domainMovie;
    }

    public function submitLike()
    {
        Engine::setDebug(false);
        $domainMovieId = empty($_GET['id']) ? F::error('Movie ID required') : F::checkstr($_GET['id']);
        $like = empty($_GET['like']) ? false : true;
        $domainMovie = new DomainMovie($domainMovieId);
        if ($domainMovie->getDomainMovieCustomer() !== $this->user->getLogin()) {
            F::error('Ошибка доступа');
        }
        if (! $domainMovie->getDomainMovieDescription()) {
            F::error('Задание не было выполнено, нельзя поставить оценку');
        }
        // если ставится та же оценка что и была - то снимается лайк или дизлайк
        // F::dump($domainMovie->getDomainMovieCustomerLike());
        if ($like === $domainMovie->getDomainMovieCustomerLike()) {
            $like = null;
        }
        $domainMovie->setDomainMovieCustomerLike($like);
        $domainMovie->save($this->user->getLogin());

        return is_null($domainMovie->getDomainMovieCustomerLike()) ? '' : ($domainMovie->getDomainMovieCustomerLike() ? '1' : '0');
        // $t = new Template('admin/customer/items/like_block');
        // $t->v('domainMovieId',$domainMovie->getId());
        // $t->v('domainMovieCustomerLike',is_null($domainMovie->getDomainMovieCustomerLike())?'null':($domainMovie->getDomainMovieCustomerLike()?'true':'false'));
        // return $t->get();
        F::dump($domainMovie);
    }

    // массовое построчное добавление заданий
    public function addMovieToQueueMass()
    {
        Engine::setDebug(false);
        $list = empty($_POST['list']) ? F::error('List required') : $_POST['list'];
        // получаем ID пустого задания
        $movies = new Movies;
        $movies->setUniqueId('not_in_base');
        $arr = $movies->get();
        if (empty($arr)) {
            F::error('Невозможно добавить задания');
        }
        $movieId = $arr[0]['id'];
        $maxQueue = 1000;
        // проверяем чтобы очередь была не более maxQueue
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription(false);
        $movies->setLimit(1);
        $movies->setPage(1);
        // если после добавления очередь получится слишком большой, отбой
        if ($movies->getRows() + count($arr) >= $maxQueue) {
            F::error('Максимальная очередь заданий &mdash; '.$maxQueue.' штук');
        }
        $arr = explode("\r\n", $list);
        $cinema = new Cinema;
        foreach ($arr as $v) {
            $str = trim($v);
            // если пустая строка - пропускаем
            if (empty($str)) {
                continue;
            }
            // добавляем пустое задание
            $domainMovieId = DomainMovie::add($movieId);
            $domainMovie = new DomainMovie($domainMovieId);
            $domainMovie->setDomainMovieCustomer($this->user->getLogin());
            $domainMovie->setDomainMovieCustomerPricePer1k($this->user->getCustomerPricePer1k());
            $domainMovie->setDomainMovieSymbolsFrom($this->user->getSymbolsFrom());
            $domainMovie->setDomainMovieSymbolsTo($this->user->getSymbolsTo());
            $domainMovie->setDomainMovieCustomerComment($str."\r\n".$this->user->getCustomerComment());
            $domainMovie->setDomainMoviePrivateComment($this->user->getCustomerPrivateComment());
            if ($this->user->getCustomerAutoExpress()) {
                $domainMovie->setDomainMovieDescriptionPricePer1k($cinema->getRewriteExpressPricePer1k());
                $domainMovie->setDomainMovieCustomerPricePer1k($this->user->getCustomerExpressPricePer1k());
            } else {
                $domainMovie->setDomainMovieDescriptionPricePer1k($cinema->getRewritePricePer1k());
                $domainMovie->setDomainMovieCustomerPricePer1k($this->user->getCustomerPricePer1k());
            }
            $domainMovie->save($this->user->getLogin());
        }
        F::redirect('/');

        return json_encode(true);
    }

    public function setMovieExpress()
    {
        Engine::setDebug(false);
        $domainMovieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        $domainMovie = new DomainMovie($domainMovieId);
        if ($domainMovie->getDomainMovieCustomer() !== $this->user->getLogin()) {
            F::error('Не ваш фильм');
        }
        if ($domainMovie->getDomainMovieIsBooked()) {
            F::error('Запрещено. Задание уже забронировано.');
        }
        $cinema = new Cinema;
        // F::dump($domainMovie->getDomainMovieIsExpress());
        if ($domainMovie->getDomainMovieIsExpress()) {
            $domainMovie->setDomainMovieDescriptionPricePer1k($cinema->getRewritePricePer1k());
            $domainMovie->setDomainMovieCustomerPricePer1k($this->user->getCustomerPricePer1k());
        } else {
            $domainMovie->setDomainMovieDescriptionPricePer1k($cinema->getRewriteExpressPricePer1k());
            $domainMovie->setDomainMovieCustomerPricePer1k($this->user->getCustomerExpressPricePer1k());
        }
        $domainMovie->save($this->user->getLogin());

        return json_encode(true);
    }

    public function sendEmailCode()
    {
        parent::sendEmailCode();
        $email = new Template('admin/customer/emails/confirm_email');
        $email->v('emailCode', $this->user->getEmailCode());
        $email->v('login', $this->user->getLogin());
        $email->v('password', $this->user->getPassword());
        (new Cinema)->sendMail($this->user->getEmail(), 'Киноконтент', 'Подтверждение почты', $email->get(), 'kinocontent');
        F::redirect('/userInfo?code_sent=1');
        // F::alert('Вам на почту отправлена ссылка для подтверждения почтового адреса, перейдите по ней.');
    }

    public function sendPassword()
    {
        Engine::setDebug(false);
        if (empty($_POST['login'])) {
            return 'Укажите логин';
        } else {
            $login = F::checkstr($_POST['login']);
        }
        $users = new Users;
        $users->setLogin($login);
        $users->setLevels(['customer', 'admin', 'admin-lite']);
        $arr = $users->get();
        if (! $arr) {
            return 'Указанный пользователь не найден в системе';
        }
        $user = new User($arr[0]['login']);
        if (! $user->getEmail()) {
            return 'Для данного аккаунта не указан email адрес, мы не знаем куда отправить пароль. Для восстановления доступа напишите в поддержку.';
        }
        // отправляем письмо
        $email = new Template('admin/customer/emails/forgot_password');
        $email->v('login', $user->getLogin());
        $email->v('password', $user->getPassword());
        (new Cinema)->sendMail($user->getEmail(), 'Киноконтент', 'Восстановление пароля', $email->get(), 'kinocontent');

        return 'Данные для доступа отправлены на email указанный в аккаунте';
    }

    public function rules()
    {
        $t = new Template('rules');

        return $t->get();
    }

    public function join()
    {
        Engine::setDebug(false);
        $login = $_POST['login'] ?? null;
        $email = $_POST['email'] ?? null;
        $partnerId = empty($_COOKIE['mgw_partner_cinema']) ? null : F::checkstr($_COOKIE['mgw_partner_cinema']);
        if (! filter_var(
            $login,
            FILTER_VALIDATE_REGEXP,
            ['options' => ['regexp' => '/^([\w0-9.]){4,20}$/']]
        )) {
            return '
			<div class="mt-4 authErrorMsg">
			Некорректный логин. Можно использовать латиницу, точку, цифры. От 4 до 20 символов.
			</div>';
        }
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return '
			<div class="mt-4 authErrorMsg">
			Некорректный адрес почты
			</div>';
        }
        // проверяем существует ли такой логин
        $users = new Users;
        $users->setLogin($login);
        if ($users->get()) {
            return '
			<div class="mt-4 authErrorMsg">
			Такой пользователь уже существует
			</div>';
        }
        $generatedPasswords = F::generateRandomStr(8);
        // F::pre($generatedPasswords);
        User::add($login);
        $user = new User($login);
        $user->setLevel('customer');
        $user->setPassword($generatedPasswords);
        if ($partnerId) {
            $users = new Users;
            $users->setId($partnerId);
            $arr = $users->get();
            if ($arr) {
                $partner = new User($arr[0]['login']);
                $partnerLogin = $partner->getLogin();
                $user->setPartner($partnerLogin);
            }
        }
        $user->setEmail($email);
        // сразу генерим проверочный код т.к. дальше идет отправка
        $user->setEmailCode(F::generateRandomStr(40));
        $user->save();
        // отправляем письмо
        $email = new Template('admin/customer/emails/join');
        $email->v('emailCode', $user->getEmailCode());
        $email->v('login', $user->getLogin());
        $email->v('password', $user->getPassword());
        (new Cinema)->sendMail($user->getEmail(), 'Киноконтент', 'Регистрация в сервисе', $email->get(), 'kinocontent');

        return '
		<div class="mt-4">
		<h5>Вы успешно зарегистрировались</h5>
		<p>
		<b>Логин:</b> '.$user->getLogin().'<br>
		<b>Пароль:</b> '.$user->getPassword().'
		</p>
		<form method="post" action="/">
		<input type="hidden" name="login" value="'.$user->getLogin().'">
		<input type="hidden" name="pass" value="'.$user->getPassword().'">
		<input type="hidden" name="enter" value="">
		<input type="hidden" name="type" value="{type}">
		<input type="submit" value="Перейти в аккаунт" class="btn btn-warning btn-lg">
		<input type="hidden" name="_token" value="'.csrf_token().'">
		</form>
		</div>';
    }

    public function confirmEmail($redirect_url = null)
    {
        $code = $_GET['code'] ?? null;
        $login = $_GET['login'] ?? null;
        if (! $login) {
            F::error('Не передан логин');
        }
        if (! $code) {
            F::error('Не передан код подтверждения');
        }
        $user = new User($login);
        if ($code == $user->getEmailCode()) {
            $user->setEmailConfirmed(true);
            $user->save();
            F::alert('Адрес подтвержден');
            F::redirect('/userInfo');
        }
        F::alert('Адрес не подтвержден');
    }
}
