<?php

namespace App\Http\Controllers\Types\Cinema;

use App\Models\Abuse;
use App\Models\Domain;
use App\Services\Cinema\AddAbusesFromEmails;
use cinema\app\models\DomainMovie as DomainMovieMegaweb;
use engine\app\models\Engine;
use engine\app\models\F;
use Illuminate\Http\Request;

class AdminController
{
    public function dmcaSaver()
    {

        request()->validate(['text' => 'required']);
        $text = request()->input('text');

        return redirect()->back()->with('success', (new AddAbusesFromEmails)('dmca', $text));
    }

    // разбираем csv файл заблокированных страниц яндекса и проставляем Dmca где нужно
    public function memorandumSaver()
    {

        request()->validate(['csvUrl' => 'required']);
        $csvUrl = request()->input('csvUrl');

        return redirect()->back()->with('success', (new AddAbusesFromEmails)('memorandum', $csvUrl));
    }

    public function rknSaver()
    {

        request()->validate(['text' => 'required']);
        $text = request()->input('text');

        return redirect()->back()->with('success', (new AddAbusesFromEmails)('ркн', $text));
    }

    public function yandexRecrawl()
    {

        request()->validate(['text' => 'required']);
        $text = request()->input('text');

        $urls = collect(explode("\r\n", $text));

        $urls->transform(function ($url) {
            return Abuse::formatUrlNew($url);
        });

        $results = collect();

        foreach ($urls as $url) {
            $parsed_url = parse_url($url);
            $domain = Domain::findOrFail($parsed_url['host'])->primary_domain;
            $result = $domain->primary_domain->yandexWebmaster->recrawl([$url]);
            $results = $results->merge($result);
        }

        // $results = (new \engine\app\services\YandexWebmasterService)->recrawl($urls->toArray());

        return view('types.cinema.admin.yandex-recrawl')
            ->with('results', $results);
    }

    public function addExistedDomainMovieToQueue()
    {

        Engine::setType('cinema');
        $login = 'Monax';

        $domainMovieId = request('id');
        $domainMovie = new DomainMovieMegaweb($domainMovieId);
        if ($domainMovie->getDomainMovieCustomer()) {
            F::error('Для задания уже указан исполнитель: '.$domainMovie->getDomainMovieCustomer());
        }
        if ($domainMovie->getDomainMovieDescription()) {
            F::error('В задании уже существует текст');
        }
        if (! $domainMovie->getDomain()) {
            F::error('В задании не указан домен, ошибка в ID?');
        }
        $domainMovie->setCustomerSettings($login);
        $domainMovie->save($login);

        return redirect()->back()->with('success', '#'.$domainMovieId.' добавлен в очередь на киноконтент для пользователя '.$login);
    }

    public function abuseHandler()
    {

        request()->validate([
            'text' => 'required',
            'service' => 'required',
        ]);

        $text = request()->input('text');
        $service = request()->input('service');

        return redirect()->back()->with('success', (new AddAbusesFromEmails)($service, $text));
    }

    public function images()
    {

        $root = Engine::getRootPath().'/types/cinema/template/images';
        $folders = ['bg', 'logo', 'favicon'];

        foreach ($folders as $folder) {
            $paths = [];
            $path = $root.'/'.$folder;
            $iter = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::SELF_FIRST,
                /*при блоке прав чтения не отвалится
                Ignore "Permission denied" (>>на которую у него нет прав на чтение)*/
                \RecursiveIteratorIterator::CATCH_GET_CHILD
            );
            foreach ($iter as $path => $dir) {
                if ($dir->isFile()) {
                    $pathinfo = pathinfo($path);
                    if ($pathinfo['basename'] == '.DS_Store') {
                        continue;
                    }
                    $paths[] = str_replace(Engine::getRootPath(), '', $path);
                }
            }
            $arr[$folder] = $paths;
        }

        return view('types.cinema.admin.images')
            ->with('types', $arr);

        return json_encode($arr, JSON_UNESCAPED_UNICODE);
    }

    public function domainSettings(Request $request)
    {
        $domain = Domain::findOrFail($request->get('domain'));

        return view('types.cinema.admin.domain-settings')->with('domain', $domain->cinemaDomain);
    }
}
