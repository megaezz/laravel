<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'poster' => $this->poster,
            'title_en' => $this->title_en,
            'title' => $this->title,
            'year' => $this->year,
            'rating_sko' => $this->rating_sko,
            'poster_or_placeholder' => $this->poster_or_placeholder,
            'myshows' => new MyshowsResource($this->whenLoaded('myshows')),
        ];
    }
}
