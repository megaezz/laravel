<?php

namespace App\Http\Middleware\Engine;

use App\Models\Config;
use Barryvdh\Debugbar\Facades\Debugbar;
use Closure;
use engine\app\models\Engine as EngineModel;
use engine\app\models\F;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class Engine
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        Debugbar::startMeasure('Engine');

        /* в 10% случаев подменяем соединение для кэша, чтобы заполнился новый кэш */
        /*
        $lottery = Lottery::odds(1, 10)
        ->winner(function () use (&$cache) {config(['cache.stores.database.connection' => 'mysql']);})
        ->choose();
        */

        // EngineModel::setDisplayErrors(Config::cached()->displayErrors ? true : false);
        // EngineModel::setDebug(Config::cached()->debug ? true : false);

        $domain = EngineModel::getDomainEloquent();
        $requestedDomain = EngineModel::getRequestedDomainEloquent();
        $visit = EngineModel::visit();

        if (! Config::cached()->switch) {
            abort(503, 'Service is switched off');
        }

        if (! $domain) {
            /* указываем код 421, он хорошо описывает проблему и почему-то не записывается в лог, что на руку, не будет насрано от uptimekuma */
            abort(421);
            // abort(404, "Domain {$this->getDomain()} doesn't exist");
        }

        /* если мы не на локалке и днс провайдер запрошенного домена cloudflare, то проверяем есть ли заголовок HTTP_CF_CONNECTING_IP */
        if (
            $requestedDomain->parent_domain_or_self->dnsProvider?->service == 'cloudflare' and
            ! $request->headers->has('CF-Connecting-IP') and
            Config::cached()->deny_direct_access
        ) {
            logger()->alert("{$visit->ip} {$visit->query}: попытка прямого доступа");
            abort(421);
        }

        if (! $domain->on) {
            // не выводит лог, как выводить лог в abort? походу поведение зависит от статуса
            abort(503, 'Domain is switched off');
        }

        if (empty($domain->type)) {
            throw new \Exception('Domain type is not specified');
        }

        if ($domain->auth) {
            Gate::authorize('access to domains under auth');
        }

        Debugbar::stopMeasure('Engine');

        return $next($request);
    }

    /* срабатывает после вывода ответа пользователю, ДАЖЕ если запрос не дошел до этого middleware */
    public function terminate($request, $response)
    {

        /* дописываем в визит время генерации, если он есть и сохряняем его единожды */
        $visit = EngineModel::visit();
        $config = Config::cached();

        if ($visit) {
            $visit->time = F::runtime();

            // если можно сохранять визиты и НЕ "есть активный бан и нельзя записывать визиты банов", то сохраняем
            if ($config->save_visits and ! (EngineModel::getClientBan()?->active and ! $config->log_banned_visits)) {
                $visit->save();
            }
        }
    }
}
