<?php

namespace App\Http\Middleware\Engine;

use App\Models\Config;
use Barryvdh\Debugbar\Facades\Debugbar;
use Closure;
use engine\app\models\Db as MegawebDb;
use engine\app\models\Engine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class Debug
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next): Response
    {

        /* если дебаг отключен, но есть куки mmnas, то активируем debug режим */
        // if (!config('app.debug') and ((Config::cached()->debugByGet and request()->hasCookie('mmnas')) or Config::cached()->debug)) {
        //     config(['app.debug' => true]);
        // }

        // if (config('app.debug')) {
        // DB::enableQueryLog();
        // Debugbar::enable();
        // }

        Debugbar::startMeasure('Debug');
        Debugbar::stopMeasure('Debug');

        $response = $next($request);

        /* добавляем запросы megaweb.v2 в дебагбар */
        if (config('app.debug')) {
            if (MegawebDb::getList()) {
                Debugbar::info(MegawebDb::getList());
            }
            // dump(\engine\app\models\Db::getList());
        }

        /* устанавливаем куки для дебага на проде */
        if ($request->has('mmnas')) {
            $response = $response->withCookie('mmnas', 'mmnas');
        }

        /* устанавливаем куки для менеджера турбо */
        if (Engine::accessForTurbo()) {
            $response = $response->withCookie('access');
        }

        return $response;
    }
}
