<?php

namespace App\Http\Middleware\Engine;

use App\Models\Config;
use Barryvdh\Debugbar\Facades\Debugbar;
use Closure;
use Illuminate\Http\Request;

class CronSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        Debugbar::startMeasure('CronSwitcher');

        if (! Config::cached()->cronSwitch) {
            return response()->view('types.engine.error', ['text' => 'Redirecting...', 'refreshed' => true], 503);
        }

        Debugbar::stopMeasure('CronSwitcher');

        return $next($request);
    }
}
