<?php

namespace App\Http\Middleware\Engine;

use App\Models\Ban;
use Barryvdh\Debugbar\Facades\Debugbar;
use Closure;
use engine\app\models\Engine;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AutoBan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        Debugbar::startMeasure('AutoBan');

        // если уже есть активный бан для этого визита, то идем дальше
        $ban = Engine::getClientBan();

        if ($ban?->active) {
            Debugbar::stopMeasure('AutoBan');

            return $next($request);
        }

        $makeBan = null;

        // $request->headers->set('referer', 'https://rep2.isola-dynamics.com/');

        // если ip запалился с реферером исолы - добавляем бан на сутки
        if ($request->header('referer')) {
            if (preg_match('/^https:\/\/rep2\.isola-dynamics\.com\/$/', $request->header('referer'))) {
                $makeBan = 'isola';
            }
        }

        if ($makeBan) {
            $ban = new Ban;
            $ban->ip = '^'.preg_quote($request->ip()).'$';
            $ban->active = true;
            $ban->description = "AutoBan: {$makeBan}";
            // пытаемся добавить бан, если не получается ругаемся и уходим
            try {
                $ban->save();
            } catch (\Exception $e) {
                logger()->alert("Не получилось добавить бан [{$makeBan}] для {$ban->ip}: {$e->getMessage()}");
                Debugbar::stopMeasure('AutoBan');

                return $next($request);
            }

            // если бан добавился, устанавливаем модель в getClientBan, во-первых этим экономим запроса, во-вторых - блокируем этим самым текущий запрос, т.к. изменяем уже закешированный бан
            Engine::setClientBan($ban);

            logger()->alert("Запалили бота [{$makeBan}] и добавили бан: #{$ban->id}");
        }

        Debugbar::stopMeasure('AutoBan');

        return $next($request);
    }
}
