<?php

namespace App\Http\Middleware\Engine;

use Barryvdh\Debugbar\Facades\Debugbar;
use Closure;
use engine\app\models\Engine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class Ban
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        Debugbar::startMeasure('Ban');

        $ban = Engine::getClientBan();

        // dd(DB::getQueryLog());

        if ($ban) {
            if ($ban->active) {
                $ban->q++;
                $ban->save();
                abort(503, "Бан {$ban->id}");
            }
            if ($ban->hide_player) {
                $ban->q_player++;
                $ban->save();
            }
        }

        Debugbar::stopMeasure('Ban');

        return $next($request);
    }
}
