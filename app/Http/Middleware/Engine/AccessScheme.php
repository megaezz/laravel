<?php

namespace App\Http\Middleware\Engine;

use Barryvdh\Debugbar\Facades\Debugbar;
use Closure;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use engine\app\services\DeviceDetector;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response as LaravelResponse;
use Symfony\Component\HttpFoundation\Response;

class AccessScheme
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        Debugbar::startMeasure('Access Scheme');

        /* возвращаем ответ от accessScheme, если есть */
        $domain = Engine::getDomainEloquent();
        $requestedDomain = Engine::getRequestedDomainEloquent();

        if ($request->getScheme() == 'http' and $domain->only_https) {
            return redirect("https://{$requestedDomain->domain}".request()->getRequestUri(), 301);
            // return redirect("https://{$requestedDomain->domain}" . $request->getRequestUri(), 301);
        }

        if ($request->getScheme() == 'https' and $domain->only_http) {
            return redirect("http://{$requestedDomain->domain}".request()->getRequestUri(), 301);
            // return redirect("http://{$requestedDomain->domain}" . $request->getRequestUri(), 301);
        }

        if ($domain->access_scheme) {
            $accessSchemeResponse = Engine::{"accessScheme{$domain->access_scheme}"}();
            // $accessSchemeResponse = $this->{"accessScheme{$domain->access_scheme}"}($request);
        }

        if ($accessSchemeResponse instanceof LaravelResponse or $accessSchemeResponse instanceof RedirectResponse) {
            return $accessSchemeResponse;
        }

        Debugbar::stopMeasure('Access Scheme');

        return $next($request);
    }

    public function accessSchemeV1(Request $request)
    {
        $domain = Engine::getDomainObject();
        $isAnyBot = Engine::isAcceptedBot();
        $redirectToDomain = $domain->getRedirectTo();
        $isNowRedirectToDomain = $request->host() == $redirectToDomain;
        $clientRedirectToDomain = $domain->getClientRedirectTo();
        $isNowClientRedirectToDomain = $request->host() == $clientRedirectToDomain;
        // если у домена есть актуальный домен и мы сейчас не на нем, то провеяем, надо ли редиректить на актуальный
        if ($domain->getRedirectTo() and ! $isNowRedirectToDomain) {
            $redirect = true;
            if ($isAnyBot) {
                if (in_array(Engine::getCrawler(), ['Yandex', 'bingbot', 'Mail.RU', 'Slurp', 'Baiduspider', 'PetalBot', 'DuckDuckBot', 'Amazonbot', 'BingPreview'])) {
                    $redirect = $domain->getYandexRedirect();
                }
                if (in_array(Engine::getCrawler(), ['Googlebot', 'Google-Read-Aloud', 'Google-InspectionTool', 'GoogleOther'])) {
                    $redirect = $domain->getGoogleRedirect();
                }
            } else {
                $redirect = $domain->getClientRedirect();
            }
            if ($redirect) {
                return redirect($request->getScheme().'://'.$redirectToDomain.$request->getRequestUri(), 301);
            }
        }
        // срабатывает когда: не бот, запрошен актуальный домен, (домен не явлется целью редиректа - можно убрать наверное этот пункт, т.к. если запрошен актуальный домен, то он никак уже не может быть целью редиректа клиента, но надо затестить)
        if (! $isAnyBot and $domain->getClientRedirectTo() and ($domain->getRedirectTo() ? $isNowRedirectToDomain : true) and ! $isNowClientRedirectToDomain) {
            return redirect($request->getScheme().'://'.$clientRedirectToDomain.$request->getRequestUri());
        }
    }

    public function accessSchemeV2(Request $request)
    {
        $domain = Engine::getDomainObject();
        $isAnyBot = Engine::isAcceptedBot();
        $isRealBot = $isAnyBot ? F::isRealBotCached($request->ip()) : false;
        $redirectToDomain = $domain->getRedirectTo() ? $domain->getRedirectTo() : false;
        $clientRedirectToDomain = $domain->getClientRedirectTo() ? $domain->getClientRedirectTo() : false;
        // если актуального зеркала нет, то считаем что мы на нем
        $isNowRedirectToDomain = $redirectToDomain ? ($request->host() == $redirectToDomain) : true;
        $isNowClientRedirectToDomain = $clientRedirectToDomain ? ($request->host() == $clientRedirectToDomain) : false;
        // если не реальный бот, то вместо роботс и сайтмэп - заглушка
        if (! $isRealBot) {
            $uri = parse_url($request->getRequestUri());
            if (isset($uri['path']) and in_array($uri['path'], ['/robots.txt', '/sitemap.xml'])) {
                // dd('показываем фейк заглушку для роботса и sitemap');
                return $this->fakeBrowserError($request);
            }
        }
        // если мы не на клиентском домене, не настоящий бот и не валидный реферер - показываем заглушку
        if ($isNowRedirectToDomain and ! $isRealBot and ! Engine::isValidReferer()) {
            // dd('показываем фейк заглушку для не бота');
            return $this->fakeBrowserError($request);
        }
        // если реальный бот и у домена есть актуальный домен и мы сейчас не на нем, то редирект на актуальный
        if ($isRealBot and $domain->getRedirectTo() and ! $isNowRedirectToDomain) {
            return redirect($request->getScheme().'://'.$redirectToDomain.$request->getRequestUri(), 301);
        }
        // срабатывает когда: не бот + существует клиентский домен + существует актуальный домен и мы на нем, либо не существует актуального + мы сейчас не на клиентском домене
        if (! $isRealBot and $domain->getClientRedirectTo() and $isNowRedirectToDomain) {
            return redirect($request->getScheme().'://'.$clientRedirectToDomain.$request->getRequestUri());
        }
    }

    public function accessSchemeV3(Request $request)
    {

        $domain = Engine::getDomainEloquent();
        $isRealBot = Engine::getAcceptedBotName() ? F::isRealBotCached($request->ip()) : false;

        /* находимся ли мы сейчас на каком-либо домене для бота? */
        $isNowAnyCrawlerDomain = in_array($request->host(), [
            $domain->redirect_to ? $domain->redirect_to : $domain->domain,
            $domain->yandex_redirect_to ? $domain->yandex_redirect_to : $domain->domain,
        ]);
        $isNowCorrectCrawlerDomain = $request->host() == Engine::getCrawlerDomain()->domain;

        /* если реальный бот, проверяем что за поисковик и редиректим на нужный домен при необходимости */
        if ($isRealBot) {
            if (! $isNowCorrectCrawlerDomain) {
                return redirect($request->getScheme().'://'.Engine::getCrawlerDomain()->domain.$request->getRequestUri(), 301);
            }
        } else {
            /* если не реальный бот, то вместо роботс и сайтмэп - заглушка */
            $uri = parse_url($request->getRequestUri());
            if (isset($uri['path']) and in_array($uri['path'], ['/robots.txt', '/sitemap.xml'])) {
                // dd('показываем фейк заглушку для роботса и sitemap');
                return $this->fakeBrowserError($request);
            }
            /* если не реальный бот и мы на актуальном домене для бота (или его нет) */
            if ($isNowAnyCrawlerDomain) {
                /* если валидный реферер или стоит натсройка разрешать невалидные рефереры */
                if (Engine::isValidReferer() or $domain->allow_not_valid_referers_for_clients) {
                    /* и есть клиентский домен - редиректим на него */
                    if ($domain->client_redirect_to) {
                        return redirect($request->getScheme().'://'.$domain->client_redirect_to.$request->getRequestUri());
                    }
                } else {
                    /* если не валидный реферер - показываем заглушку */
                    return $this->fakeBrowserError($request);
                }
            }
        }
        // dd('впускаем');
    }

    /* покывает кастомную страницу 404 в зависимости от браузера */
    public function fakeBrowserError(Request $request)
    {
        $result = new DeviceDetector($request->userAgent());
        $template = 'fake_error_pages/chrome';
        if (! empty($result->getClient('family'))) {
            if ($result->getClient('family') == 'Safari') {
                $template = 'fake_error_pages/safari';
            }
            if ($result->getClient('family') == 'Internet Explorer') {
                $template = 'fake_error_pages/edge';
            }
            if ($result->getClient('family') == 'Yandex Browser') {
                $template = 'fake_error_pages/yandex';
            }
            if ($result->getClient('family') == 'Firefox') {
                $template = 'fake_error_pages/firefox';
            }
            if ($result->getClient('family') == 'Opera') {
                $template = 'fake_error_pages/opera';
            }
            if ($result->getClient('family') == 'Chrome') {
                $template = 'fake_error_pages/chrome';
            }
        }
        // $template = 'fake_error_pages/firefox';
        // dd($result->browser->name);
        logger()->info("Показали фейковую заглушку {$template}");
        $t = new Template($template);
        $t->v('protocol', $request->getScheme().'://');
        $t->v('requestUri', $request->getRequestUri());
        $t->v('requestedDomain', Engine::getRequestedDomainObject()->getDomain());

        // header('HTTP/1.0 404 Not Found');
        // die($t->get());
        return response($t->get(), 404);
    }

    /* переделал схему v1, но побоялся применять пока что, оттестить и заменить ей существуюшую */
    public function accessSchemeV1New(Request $request)
    {
        $domain = Engine::getDomainEloquent();
        $isAnyBot = Engine::isAcceptedBot();
        $isNowCorrectCrawlerDomain = $request->getHost() == Engine::getCrawlerDomain()->domain;

        // если у домена есть хотя бы один домен для ботов и мы сейчас не на нем, то провеяем, надо ли редиректить на домен для ботов
        if (($domain->redirect_to or $domain->yandex_redirect_to) and ! $isNowCorrectCrawlerDomain) {

            // если мы не на домене для бота, то редиректим ботов и клиентов на верный домен для ботов, если нужно
            $redirect = true;

            if ($isAnyBot) {
                if (in_array(Engine::getCrawler(), ['Yandex', 'bingbot', 'Mail.RU', 'Slurp', 'Baiduspider', 'PetalBot'])) {
                    $redirect = $domain->yandex_redirect;
                }
                if (in_array(Engine::getCrawler(), ['Googlebot', 'Google-Read-Aloud', 'Google-InspectionTool', 'GoogleOther'])) {
                    $redirect = $domain->google_redirect;
                }
            } else {
                $redirect = $domain->client_redirect;
            }

            if ($redirect) {
                $crawlerDomain = Engine::getCrawlerDomain()->domain;

                return redirect($request->getScheme()."://{$crawlerDomain}".$request->getRequestUri(), 301);
            }
        }

        // срабатывает когда: не бот + существует клиентский домен + существует домен для ботов и мы на нем
        if (! $isAnyBot and $isNowCorrectCrawlerDomain and $domain->client_redirect_to) {
            return redirect($request->getScheme()."://{$domain->client_redirect_to}".$request->getRequestUri());
        }
        // срабатывает когда: не бот, запрошен актуальный домен, (домен не явлется целью редиректа - можно убрать наверное этот пункт, т.к. если запрошен актуальный домен, то он никак уже не может быть целью редиректа клиента, но надо затестить)
        // срабатывает когда: не бот, есть клиентский домен, сейчас не на клиентском домене, есть домен для ботов и мы на нем, либо просто нет домена для ботов
    }
}
