<?php

namespace App\Http\Middleware\Engine;

use Barryvdh\Debugbar\Facades\Debugbar;
use Closure;
use engine\app\models\Engine;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ModifyHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        Debugbar::startMeasure('ModifyHeaders');

        // если домен реквеста отличается от расчитанного в Engine - передаем в Engine новый реквест (для тестов)
        // if ($request->host() !== Engine::getRequestedDomainEloquent()?->domain) {
        //     Engine::setRequest($request);
        //     Engine::getRequestedDomainEloquent()?->registerRoutes(withoutPrefix: true);
        // }

        // получаем реальный IP от Cloudflare
        if ($request->server->has('HTTP_CF_CONNECTING_IP')) {
            $request->server->set('REMOTE_ADDR', $request->server->get('HTTP_CF_CONNECTING_IP'));
        }

        // получаем реальный протокол от Cloudflare
        if (json_decode($request->header('CF-Visitor', ''))?->scheme === 'https') {
            $request->server->set('HTTPS', 'on');
        }

        /* меняем userAgent внутри request(), т.к. если есть например символы ��, то были ошибки в запросах, в том числе нативные (сессии) */
        if ($request->userAgent()) {
            $request->headers->set('User-Agent', mb_convert_encoding($request->userAgent(), 'UTF-8'));
        }

        /* убираем всратый заголовок, с условием, что заголовки еще не были отправлены (бывает при дебаге), иначе ошибка */
        if (! headers_sent()) {
            header_remove('X-Powered-By');
        }

        Debugbar::stopMeasure('ModifyHeaders');

        return $next($request);
    }
}
