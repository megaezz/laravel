<?php

namespace App\Http\Middleware;

use Barryvdh\Debugbar\Facades\Debugbar;
use cinema\app\controllers\CinemaAuthorizationController;
use Closure;
use engine\app\controllers\EngineAuthorizationController;
use engine\app\models\Engine;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MegawebAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, $engineType, ...$levels): Response
    {

        Debugbar::startMeasure('MegawebAuthorization');

        if ($engineType == 'engine') {
            $auth = new EngineAuthorizationController($levels);
        }

        if ($engineType == 'cinema') {
            $auth = new CinemaAuthorizationController($levels);
        }

        if (! isset($auth)) {
            throw new \Exception('Неизвестный движок аутентификации');
        }

        Engine::setAuth($auth);

        Debugbar::stopMeasure('MegawebAuthorization');

        return $next($request);
    }
}
