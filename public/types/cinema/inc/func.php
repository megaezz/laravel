<?php

// get_group_intersect(array('group_id'=>1));
/*
function preparing_group_intersect($args=array()) {
    $args['group_id']=empty($args['group_id'])?error('�� �������� ������'):$args['group_id'];
    $args['order']=empty($args['order'])?false:$args['order'];
    $sql['order']=$args['order']?'order by '.$args['order']:'';
    $args['page']=empty($args['page'])?error('�� ������� ����� ��������'):$args['page'];
    $args['rows']=isset($args['rows'])?$args['rows']:error('�� �������� ���-�� ���������');
    $from=($args['page']-1)*$args['rows'];
    $sql['limit']='limit '.$from.','.$args['rows'];
    $tags=query_arr('select tag_id from '.typetab('groups_tags').' where group_id=\''.$args['group_id'].'\';');
    if (empty($tags)) {error('��� ����� ��� ������');};
    $query='';
    foreach ($tags as $k=>$v) {
        if ($k===0) {
            $query.='select sql_calc_found_rows cinema_id
            from '.typetab('cinema_tags').'
            join '.typetab('cinema').' on cinema.id=cinema_tags.cinema_id
            where tag_id=\''.$v['tag_id'].'\'';
        };
        if ($k!==0) {
            $query.='and cinema_id in (
            select cinema_id from '.typetab('cinema_tags').' where tag_id=\''.$v['tag_id'].'\'
            )';
        };
    };
    // pre($sql);
    $arr['items']=query_arr($query.' '.$sql['order'].' '. $sql['limit']);
    $arr['rows']=rows_without_limit();
    // pre($arr);
    return $arr;
}
*/

function player()
{
    $args['id'] = empty($_GET['cinema_id']) ? error('�� ������� ID �����') : checkstr($_GET['cinema_id']);
    $arr = preparing_cinema_info(['id' => $args['id']]);
    // pre($arr);
    temp_var('title_ru', $arr['title_ru']);
    temp_var('title_en', $arr['title_en']);
    temp_var('title_en_if_exist', empty($arr['title_en']) ? '' : '('.$arr['title_en'].')');
    temp_var('description', $arr['description']);
    temp_var('player', moonwalk_player(['id' => $args['id']]));
    $return = template('block_player');
    clear_temp_vars();

    return $return;
}

function preparing_list_groups()
{
    $arr = query_arr('select groups.id,groups.name
		from '.typetab('groups').'
		order by groups.name
		;');
    foreach ($arr as $k => $v) {
        $group_arr = cache('preparing_group_intersect', ['group_id' => $v['id'], 'page' => 1, 'rows' => 0]);
        $arr[$k]['rows'] = $group_arr['rows'];
    }

    return $arr;
}

function list_groups()
{
    $arr = cache('preparing_list_groups');
    // pre($arr);
    $group_id = empty($_GET['group_id']) ? false : checkstr($_GET['group_id']);
    $list = '';
    foreach ($arr as $v) {
        // $group_arr=cache('preparing_group_intersect',array('group_id'=>$v['id']),86400);
        temp_var('rows', $v['rows']);
        $active = '';
        if ($v['id'] == $group_id) {
            $active = 'active';
        }
        temp_var('active', $active);
        temp_var('id', $v['id']);
        temp_var('name', $v['name']);
        $list .= template('items/list_group');
        clear_temp_vars();
    }

    return $list;
}

function preparing_list_cinema($args = [])
{
    $args['group_id'] = empty($args['group_id']) ? false : $args['group_id'];
    $args['search'] = empty($args['search']) ? false : $args['search'];
    $args['order'] = empty($args['order']) ? false : $args['order'];
    $sql['order'] = $args['order'] ? 'order by '.$args['order'] : '';
    $args['page'] = empty($args['page']) ? error('�� ������� ����� ��������') : $args['page'];
    $args['rows'] = isset($args['rows']) ? $args['rows'] : error('�� �������� ���-�� ���������');
    $from = ($args['page'] - 1) * $args['rows'];
    $sql['limit'] = 'limit '.$from.','.$args['rows'];
    if ($args['group_id']) {
        $tags = query_arr('select tag_id from '.typetab('groups_tags').' where group_id=\''.$args['group_id'].'\';');
        if (empty($tags)) {
            error('��� ����� ��� ������');
        }
        $query = '';
        foreach ($tags as $k => $v) {
            if ($k === 0) {
                $query .= 'select sql_calc_found_rows cinema_id
				from '.typetab('cinema_tags').'
				join '.typetab('cinema').' on cinema.id=cinema_tags.cinema_id
				where tag_id=\''.$v['tag_id'].'\'';
            }
            if ($k !== 0) {
                $query .= 'and cinema_id in (
				select cinema_id from '.typetab('cinema_tags').' where tag_id=\''.$v['tag_id'].'\'
				)';
            }
        }
    }
    if ($args['search']) {
        // pre($args['search']);
        $query = 'select sql_calc_found_rows id as cinema_id
		from '.typetab('cinema').' 
		where title_ru like \'%'.$args['search'].'%\' or title_en like \'%'.$args['search'].'%\'';
    }
    if (empty($query)) {
        $query = 'select sql_calc_found_rows id as cinema_id
		from '.typetab('cinema');
    }
    // pre($args);
    $arr['items'] = query_arr($query.' '.$sql['order'].' '.$sql['limit']);
    $arr['rows'] = rows_without_limit();

    return $arr;
}

function preparing_cinema_info($args = [])
{
    $id = empty($args['id']) ? error('�� ������� ID ��������') : $args['id'];
    $arr = query_assoc('select cinema.id,cinema.title_en,cinema.title_ru,
		cinema.kinopoisk_id
		from '.typetab('cinema').' 
		where id=\''.$id.'\';');
    $arr['tags'] = query_arr('select tags.tag
		from '.typetab('cinema_tags').'
		join '.typetab('tags').' on tags.id=cinema_tags.tag_id
		where cinema_tags.cinema_id=\''.$id.'\';');
    $arr['getmovie'] = empty($arr['kinopoisk_id']) ? [] : getmovie_api(['kinopoisk_id' => $arr['kinopoisk_id']]);
    $arr['description'] = empty($arr['getmovie']['description']) ? '' : $arr['getmovie']['description'];
    $arr['poster_big'] = empty($arr['getmovie']['poster_big']) ? 'https://st.kp.yandex.net/images/movies/poster_none.png' : $arr['getmovie']['poster_big'];
    // pre($arr['getmovie']);
    if (empty($arr)) {
        error('������� �� ������');
    }

    return $arr;
}

function list_cinema($args = [])
{
    $args['template']['block'] = empty($args['template']['block']) ? 'block_list_cinema' : $args['template']['block'];
    $args['template']['item'] = empty($args['template']['item']) ? 'items/list_cinema' : $args['template']['item'];
    $args['page_link'] = empty($args['page_link']) ? '/?group_id='.getvar('group_id').'&amp;p=' : $args['page_link'];
    $group_id = empty($_GET['group_id']) ? false : $_GET['group_id'];
    $search = empty($_GET['search']) ? false : checkstr($_GET['search']);
    $cinema_id = empty($_GET['cinema_id']) ? false : checkstr($_GET['cinema_id']);
    $page = empty($_GET['p']) ? 1 : checkstr($_GET['p']);
    $per_page = 21;
    $order = empty($_GET['order']) ? 'cinema.title_ru' : checkstr($_GET['order']);
    $arr = cache('preparing_list_cinema', ['group_id' => $group_id, 'page' => $page, 'rows' => $per_page, 'order' => $order, 'search' => $search]);
    // pre($arr);
    $list = '';
    foreach ($arr['items'] as $v) {
        $cinema = cache('preparing_cinema_info', ['id' => $v['cinema_id']]);
        // pre($cinema);
        $active = '';
        if ($v['cinema_id'] == $cinema_id) {
            $active = 'active';
        }
        temp_var('active', $active);
        temp_var('id', $v['cinema_id']);
        temp_var('kinopoisk_id', $cinema['kinopoisk_id']);
        temp_var('title_ru', $cinema['title_ru']);
        temp_var('title_en', $cinema['title_en']);
        temp_var('description', $cinema['description']);
        temp_var('poster_big', $cinema['poster_big']);
        $list .= template($args['template']['item']);
        // pre($list);
        clear_temp_vars();
    }
    temp_var('list', $list);
    $pages = page_numbers_alt([
        'rows' => $arr['rows'],
        'per_page' => $per_page,
        'page' => $page,
        'link' => $args['page_link'],
        'class_current' => 'active',
        'a_inside_inactive' => 1,
    ]);
    temp_var('pages', $pages);
    temp_var('rows', $arr['rows']);
    $block = template($args['template']['block']);

    return $block;
}

function cinema_init()
{
    global $config;
    include $config['path']['server'].'types/'.$config['type'].'/inc/moonwalk/func.php';
    include $config['path']['server'].'types/'.$config['type'].'/inc/admin.php';
    if (empty($config['vars']['page'])) {
        cinema_rewrite_query();
    }
}

function cinema_rewrite_query() {}

function authorizate_cinema_admin()
{
    // authorizate('shop',tabname('shop','users'),array('admin'));
    authorizate_alt(['type' => 'cinema', 'levels' => ['admin']]);
}
