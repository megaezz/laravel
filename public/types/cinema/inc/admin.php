<?php

function submit_rewrite_cinema()
{
    $login = auth_login(['type' => 'cinema']);
    $description = empty($_POST['description']) ? error('�� �������� ��������') : checkstr($_POST['description']);
    $cinema_id = empty($_POST['cinema_id']) ? error('�� ������� ID ������') : checkstr($_POST['cinema_id']);
    query('
		insert into '.typetab('rewrites').' set
		cinema_id=\''.$cinema_id.'\',
		login=\''.$login.'\',
		description=\''.$description.'\'
		;');

    return true;
}

function list_cinema_rewrite()
{
    $args['template']['block'] = 'admin/rewrite/block';
    $args['template']['item'] = 'admin/rewrite/item';
    $args['page_link'] = '?page=admin/rewrite/index&amp;p=';

    return list_cinema($args);
}
