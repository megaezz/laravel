<?php

/*

http://moonwalk.cc/partners/login

*/

// http://getmovie.cc/
function getmovie_api($args = [])
{
    $args['kinopoisk_id'] = empty($args['kinopoisk_id']) ? error('�� ������� kinopoisk_id') : $args['kinopoisk_id'];
    // $url='http://getmovie.cc/api/kinopoisk.json?id='.$args['kinopoisk_id'].'&token=037313259a17be837be3bd04a51bf678';
    $url = 'http://getmovie.cc/api/videos.json?kinopoisk_id='.$args['kinopoisk_id'].'&api_token=037313259a17be837be3bd04a51bf678&r=json';
    // pre($url);
    $json = filegetcontents_alt($url, ['timeout' => 1, 'error' => 'no_error']);
    if ($json === 'error') {
        return false;
    }
    $object = json_decode($json);
    // pre(var_dump($object->response->items[0]));
    if (empty($object->response->items[0])) {
        return false;
    }
    foreach ($object->response->items[0] as $k => $v) {
        $arr[$k] = mb_convert_encoding($v, 'windows-1251', 'utf-8');
    }

    // pre($arr);
    return $arr;
}

function moonwalk_grab()
{
    $arr = query_arr('select id,kinopoisk_id 
		from '.typetab('cinema').' 
		where kinopoisk_id!=\'\' and not grabbed 
		limit 10
		;');
    $list = '';
    foreach ($arr as $v) {
        $info = getmovie_api(['kinopoisk_id' => $v['kinopoisk_id']]);
        if (! $info) {
            $list .= '<li>'.$v['kinopoisk_id'].' ������</li>';

            continue;
        }
        $list .= '<li>'.$v['description'].'</li>';
        // query('update '.typetab('cinema').' set grabbed=1 where id=\''.$v['id'].'\';');
    }
    $return = '<ol>'.$list.'</ol>';

    return $return;
}

function moonwalk_get_info($args = [])
{
    $id = empty($args['id']) ? error('�� ������� ID �������') : $args['id'];
    // �������� ����� �������� � ���� ���-��� ������� � ��������
    $arr = query_assoc('select moonwalk.token,moonwalk.type
		from '.typetab('moonwalk').'
		join '.typetab('cinema').' on cinema.mw_unique_id=moonwalk.unique_id
		where id=\''.$args['id'].'\'
		order by episodes_count desc,seasons_count desc
		limit 1
		;');
    if (empty($arr)) {
        error('������ �� ������');
    }

    return $arr;
}

function moonwalk_player($args = [])
{
    $args['id'] = empty($args['id']) ? error('�� ������� ID ��������') : $args['id'];
    $mw_info = moonwalk_get_info(['id' => $args['id']]);
    temp_var('token', $mw_info['token']);
    $type = $mw_info['type'];
    if ($type === 'movie') {
        $type = 'video';
    }
    temp_var('type', $type);

    return template('moonwalk/player');
}

// ������� ���������� �� cinema �� moonwalk
function moonwalk_to_cinema()
{
    query('truncate '.typetab('cinema').';');
    query('truncate '.typetab('cinema_tags').';');
    query('
		insert into '.typetab('cinema').' 
		(mw_unique_id,title_en,title_ru,kinopoisk_id)
		select unique_id,title_en,title_ru,kinopoisk_id
		from '.typetab('moonwalk').'
		group by unique_id
		;');
    // ����������� ��������� ��� ���������
    $arr = query_arr('select cinema.id,moonwalk.type,moonwalk.category,moonwalk.camrip
		from '.typetab('cinema').'
		join '.typetab('moonwalk').' on moonwalk.unique_id=cinema.mw_unique_id
		group by cinema.mw_unique_id
		;');
    foreach ($arr as $v) {
        $tags = [];
        $tags[] = $v['category'];
        $tags[] = $v['type'];
        if (! empty($v['camrip'])) {
            $tags[] = 'camrip';
        }
        foreach ($tags as $tag) {
            if (! empty($tag)) {
                query('insert into '.typetab('cinema_tags').'
					set cinema_id=\''.$v['id'].'\', tag_id=(select id from '.typetab('tags').' where tag=\''.$tag.'\')
					;');
            }
        }
    }
    alert('���� cinema � cinema_groups �����������');
}

// ������� �� moonwalk �� ���� ��������� ������ .json � ����������� ���������� unique_id
function moonwalk_json_to_db()
{
    ini_set('memory_limit', '200M');
    $path = $_SERVER['DOCUMENT_ROOT'].'/types/cinema/files/moonwalk/';
    $db['serials_foreign']['file'] = file_get_contents($path.'serials_foreign.json');
    $db['serials_foreign']['type'] = 'serials';
    $db['serials_russian']['file'] = file_get_contents($path.'serials_russian.json');
    $db['serials_russian']['type'] = 'serials';
    $db['serials_anime']['file'] = file_get_contents($path.'serials_anime.json');
    $db['serials_anime']['type'] = 'serials';
    $db['movies_anime']['file'] = file_get_contents($path.'movies_anime.json');
    $db['movies_anime']['type'] = 'movies';
    $db['movies_russian']['file'] = file_get_contents($path.'movies_russian.json');
    $db['movies_russian']['type'] = 'movies';
    $db['movies_foreign']['file'] = file_get_contents($path.'movies_foreign.json');
    $db['movies_foreign']['type'] = 'movies';
    $db['movies_camrip']['file'] = file_get_contents($path.'movies_camrip.json');
    $db['movies_camrip']['type'] = 'movies';
    query('truncate '.typetab('moonwalk_new').';');
    foreach ($db as $content) {
        $arr = json_decode($content['file']);
        foreach ($arr->report->$content['type'] as $v) {
            // die(var_dump(mb_detect_encoding($v->title_ru)));
            $title_ru = mysql_real_escape_string($v->title_ru);
            $title_ru = mb_convert_encoding($title_ru, 'windows-1251', 'utf-8');
            $title_en = mysql_real_escape_string($v->title_en);
            $title_en = mb_convert_encoding($title_en, 'windows-1251', 'utf-8');
            $translator = mysql_real_escape_string($v->translator);
            $translator = mb_convert_encoding($translator, 'windows-1251', 'utf-8');
            // �������� unique_id �� id ���������� ���� world_art, ���� ��� �� ���� �� ������� �� null � � ���������� ������ ���� ����������� �� �������
            $unique_id = empty($v->kinopoisk_id) ? (empty($v->world_art_id) ? 'null' : '\'w_'.$v->world_art_id.'\'') : '\'k_'.$v->kinopoisk_id.'\'';
            query('insert into '.typetab('moonwalk_new').' set
				title_ru=\''.$title_ru.'\',
				title_en=\''.$title_en.'\',
				token=\''.$v->token.'\',
				type=\''.(empty($v->type) ? 'movie' : $v->type).'\',
				translator=\''.$translator.'\',
				translator_id=\''.$v->translator_id.'\',
				category=\''.(empty($v->category) ? 'foreign' : $v->category).'\',
				seasons_count=\''.(isset($v->seasons_count) ? $v->seasons_count : '').'\',
				episodes_count=\''.(isset($v->episodes_count) ? $v->episodes_count : '').'\',
				kinopoisk_id=\''.(isset($v->kinopoisk_id) ? $v->kinopoisk_id : '').'\',
				world_art_id=\''.(isset($v->world_art_id) ? $v->world_art_id : '').'\',
				camrip='.(empty($v->camrip) ? 0 : $v->camrip).',
				added_at='.(empty($v->added_at) ? 'null' : '\''.$v->added_at.'\'').',
				pornolab_id=\''.(isset($v->pornolab_id) ? $v->pornolab_id : '').'\',
				unique_id='.$unique_id.'
				;');
        }
    }
    // ������������� ������� ��� world id � kinopoisk id
    // ������� ������ ���� �������� ��������������� �� �������� ��������� � ���������� ���� id ��� n_�/�
    $arr = query_arr('select title_ru from '.typetab('moonwalk_new').' where unique_id is null;');
    $q = 0;
    foreach ($arr as $v) {
        $q++;
        query('update '.typetab('moonwalk_new').' 
			set unique_id=\'n_'.$q.'\' 
			where unique_id is null and title_ru=\''.$v['title_ru'].'\';');
    }
    alert('���� moonwalk �����������');
}
