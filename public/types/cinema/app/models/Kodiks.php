<?php

namespace cinema\app\models;

use engine\app\models\F;

class Kodiks
{
    private $limit = null;

    private $order = null;

    private $rows = null;

    private $page = null;

    private $listTemplate = null;

    private $paginationUrlPattern = null;

    private $checked = null;

    private $unique_id = null;

    private $id = null;

    private $isUniqueId = null;

    public function __construct() {}

    public function get()
    {
        if (! $this->getLimit()) {
            F::error('setLimit first');
        }
        if (! $this->getPage()) {
            F::error('setPage first');
        }
        $sql['from'] = ($this->getPage() - 1) * $this->getLimit();
        $sql['having'] = '';
        $sql['q'] = '';
        $sql['order'] = $this->getOrder() ? 'order by '.$this->getOrder() : '';
        $sql['unique_id'] = $this->getUniqueId() ? 'unique_id = \''.$this->getUniqueId().'\'' : 1;
        if (is_null($this->getIsUniqueId())) {
            $sql['is_unique_id'] = 1;
        } else {
            $sql['is_unique_id'] = $this->getIsUniqueId() ? 'unique_id is not null' : 'unique_id is null';
        }
        if (is_null($this->getChecked())) {
            $sql['checked'] = 1;
        } else {
            $sql['checked'] = $this->getChecked() ? 'checked' : 'not checked';
        }
        $sql['id'] = $this->getId() ? ('id = \''.$this->getId().'\'') : 1;
        $arr = F::query_arr(
            'select SQL_CALC_FOUND_ROWS id,date(date) as date
			'.$sql['q'].'
			from '.F::typetab('kodik').'
			where
			'.$sql['is_unique_id'].' and
			'.$sql['checked'].' and
			'.$sql['unique_id'].' and
			'.$sql['id'].'

			'.$sql['having'].'
			'.$sql['order'].'
			limit '.$sql['from'].','.$this->getLimit().'
			;'
        );
        // dump($arr);
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getIsUniqueId()
    {
        return $this->isUniqueId;
    }

    public function setIsUniqueId($b = null)
    {
        $this->isUniqueId = $b;
    }

    public function getUniqueId()
    {
        return $this->unique_id;
    }

    public function setUniqueId($unique_id = null)
    {
        $this->unique_id = $unique_id;
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function setChecked($b = null)
    {
        return $this->checked = $b;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getPage()
    {
        return $this->page;
    }

    public static function resetCheckers()
    {
        F::query('update '.F::typetab('kodik').' set checked = 0;');

        return true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id = null)
    {
        $this->id = $id;
    }
}
