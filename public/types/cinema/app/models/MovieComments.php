<?php

namespace cinema\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class MovieComments
{
    private $domainMovieId = null;

    private $page = null;

    private $limit = null;

    private $order = 'asc';

    private $active = null;

    private $status = null;

    private $listTemplate = null;

    private $rows = null;

    private $domain = null;

    private $lang = null;

    public function get()
    {
        $orders['asc'] = 'order by date';
        $orders['desc'] = 'order by date desc';
        $sql['order'] = isset($orders[$this->order]) ? $orders[$this->order] : F::error('Order of videoComments was not recognized');
        $sql['active'] = is_null($this->active) ? '1' : ($this->active ? 'active = 1' : 'active = 0');
        $sql['status'] = is_null($this->status) ? '1' : 'status = \''.$this->status.'\'';
        if ($this->limit) {
            if (! $this->page) {
                F::error('Use setPage to use limit');
            }
            $from = ($this->page - 1) * $this->limit;
            $sql['limit'] = 'limit '.$from.','.$this->limit.'';
        } else {
            $sql['limit'] = '';
        }
        $sql['domainMovieId'] = $this->getDomainMovieId() ? ('domain_movie_id=\''.$this->getDomainMovieId().'\'') : 1;
        if ($this->getDomain()) {
            $sql['domain'] = 'domain_movies.domain = \''.$this->getDomain().'\'';
            $sql['joinDomainMovies'] = 'join '.F::typetab('domain_movies').' on domain_movies.id = movie_comments.domain_movie_id';
        } else {
            $sql['domain'] = 1;
            $sql['joinDomainMovies'] = '';
        }
        $arr = F::query_arr('
			select movie_comments.id
			from '.F::typetab('movie_comments').'
			'.$sql['joinDomainMovies'].'
			where
			'.$sql['domainMovieId'].'
			and '.$sql['domain'].'
			and '.$sql['active'].'
			and '.$sql['status'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getList()
    {
        if (! $this->listTemplate) {
            F::error('Use setListTemplate first to use VideoComments/getList');
        }
        if ($this->getDomain()) {
            $domain = new Domain($this->getDomain());
        }
        $arr_says = ['говорит', 'молвит', 'уверен, что', 'кричит', 'шепчет', 'стонет'];
        $arr = $this->get();
        $list = '';
        foreach ($arr as $v) {
            $comment = new MovieComment($v['id']);
            $movie = new DomainMovie($comment->getDomainMovieId());
            $t = new Template($this->listTemplate);
            $t->setLang($this->getLang());
            $t->v('movieTitle', [
                'ru' => $movie->getTitleRu(),
                'ua' => $movie->getTitleRu(),
                'en' => $movie->getTitleEnIfPossible(),
            ]);
            $t->v('movieTitleRu', $movie->getTitleRu());
            $t->v('movieType', [
                'ru' => $movie->getTypeRu(true),
                'ua' => $movie->getTypeUa(true),
                'en' => $movie->getTypeEn(true),
            ]);
            $t->v('domainMovieId', $comment->getDomainMovieId());
            $t->v('date', $comment->getDate());
            $t->vf('text', nl2br(htmlspecialchars($comment->getText())));
            $t->vf('username', htmlspecialchars($comment->getUsername()));
            $t->v('says', $arr_says[array_rand($arr_says)]);
            if ($this->getDomain()) {
                $t->v('watchLink', $domain->getWatchLink($movie));
            }
            $date = new \DateTime($comment->getDate());
            $local = 'ru';
            if ($this->getLang() == 'ua') {
                $local = 'uk';
            }
            if ($this->getLang() == 'en') {
                $local = 'en';
            }
            // выводим дату на нужном языке
            $formatter = new \IntlDateFormatter(
                $local.'_RU',
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::SHORT,
                'Europe/Moscow'
            );
            $t->v('ruDate', $formatter->format($date));
            $t->v('localDate', $formatter->format($date));
            $list .= $t->get();
        }

        return $list;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setStatus($status = null)
    {
        $this->status = $status;
    }

    public function setActive($active = null)
    {
        $this->active = $active;
    }

    public function getDomainMovieId()
    {
        return $this->domainMovieId;
    }

    public function setDomainMovieId($domainMovieId = null)
    {
        $this->domainMovieId = $domainMovieId;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang = null)
    {
        $this->lang = $lang;
    }
}
