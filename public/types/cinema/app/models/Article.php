<?php

namespace cinema\app\models;

use engine\app\models\F;

class Article extends \engine\app\models\Article
{
    protected const TYPE = 'cinema';

    protected const STATIC_PATH = 'types/cinema/articles';

    // остановился на том что http://local.coin.awmzone.net/?r=admin/Articles/add создает правильно новую запись в engine.articles хотя по идее должна выдавать ошибку, т.к. контанта переопределена а таблицы blog.articles не существует
    private $domainMovieId = null;

    private $acceptor = null;

    public function __construct($id = null)
    {
        parent::__construct($id);
        $arr = F::query_assoc('select acceptor,domain_movie_id from '.F::typetab('articles').' where id = \''.$id.'\';');
        $this->domainMovieId = $arr['domain_movie_id'];
        $this->acceptor = $arr['acceptor'] ? $arr['acceptor'] : null;
    }

    public function save()
    {
        parent::save();
        F::query('
			update '.F::typetab('articles').'
			set
			domain_movie_id = '.($this->getDomainMovieId() ? '\''.F::escape_string($this->getDomainMovieId()).'\'' : 'null').',
			acceptor = '.($this->getAcceptor() ? '\''.$this->getAcceptor().'\'' : 'null').'
			where id = \''.$this->getId().'\'
		');
    }

    public function getDomainMovieId()
    {
        return $this->domainMovieId;
    }

    public function setDomainMovieId($id = null)
    {
        $this->domainMovieId = $id;
    }

    public function getAcceptor()
    {
        return $this->acceptor;
    }

    public function setAcceptor($domain = null)
    {
        $this->acceptor = $domain;
    }
}
