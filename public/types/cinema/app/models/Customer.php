<?php

namespace cinema\app\models;

use engine\app\models\F;

class Customer extends User
{
    private $symbolsFrom = null;

    private $symbolsTo = null;

    public function __construct($login = null)
    {
        parent::__construct($login);
        if (! $login) {
            F::error('User ID required to create User Class');
        }
        if ($this->getLevel() !== 'customer') {
            F::error('Level should be a customer');
        }
        $this->symbolsFrom = $arr['symbols_from'];
        $this->symbolsTo = $arr['symbols_to'];
    }

    public function getIsEnoughtBalance()
    {
        return ($this->getRewriteBalance() >= $this->getCustomerPricePer1k() / 2) ? true : false;
    }

    public function getSymbolsFrom()
    {
        return $this->symbolsFrom;
    }

    public function setSymbolsFrom($q = null)
    {
        $this->symbolsFrom = $q;
    }

    public function getSymbolsTo()
    {
        return $this->symbolsTo;
    }

    public function setSymbolsTo($q = null)
    {
        $this->symbolsTo = $q;
    }

    public function save()
    {
        parent::save();
        F::query('update '.F::typetab('users').'
			set
			symbols_from = '.(is_null($this->getSymbolsFrom()) ? 'null' : ('\''.$this->getSymbolsFrom().'\'')).',
			symbols_to = '.(is_null($this->getSymbolsTo()) ? 'null' : ('\''.$this->getSymbolsTo().'\'')).'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }
}
