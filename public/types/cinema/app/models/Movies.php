<?php

namespace cinema\app\models;

use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\PaginatorCustom;
use engine\app\models\Template;

class Movies
{
    private $limit = null;

    private $order = null;

    private $tags = [];

    private $rows = null;

    private $page = null;

    private $listTemplate = null;

    private $paginationUrlPattern = null;

    private $search = null;

    private $withYear = null;

    private $deleted = null;

    private $unique_id = null;

    private $checked = null;

    private $groupBy = null;

    private $withPoster = null;

    private $tagsSearchStrict = null;

    private $year = null;

    private $includeMovies = true;

    private $includeSerials = true;

    private $includeTrailers = true;

    private $includeNullType = false;

    private $includeGames = false;

    private $includeYoutube = false;

    // искать слева (c начала названия фильма)
    private $leftSearch = null;

    // исключить видео из выдачи (для похожих видео)
    private $excludeMovie = null;

    private $domain = null;

    // private $withUniqueDescription = null;
    private $withDomainDescription = null;

    private $descriptionWriter = null;

    private $bookedForLogin = null;

    private $useDomainMovies = null;

    private $domainMovieActive = null;

    private $movieId = null;

    private $domainMovieId = null;

    private $withKinopoiskRatingSko = null;

    private $withKinopoiskId = null;

    private $withRatingSko = null;

    private $addedAtFrom = null;

    private $addedAtTo = null;

    private $domainIsNot = null;

    private $withBookedForLogin = null;

    private $domainMovieCustomer = null;

    // private $domainMovieCustomerTempBalanceIsMoreThan = null;
    private $domainMovieWithCustomerEnoughBalance = null;

    private $useDomainMovieCustomers = null;

    private $domainMovieRework = null;

    private $domainMovieSymbolsFrom = null;

    private $domainMovieSymbolsTo = null;

    private $domainMovieShowToCustomer = null;

    private $domainMovieCustomerArchived = null;

    private $domainMovieWriting = null;

    private $excludeDescriptionWriters = [];

    private $withDomainMovieCustomerComment = null;

    private $kinopoiskId = null;

    private $domainMovieDeleted = null;

    private $domainObject = null;

    private $withLegalPlayer = null;

    private $titleRu = null;

    private $genresListTemplate = null;

    private $domainMoviePageDeleted = null;

    // исключить строки выдачи для этого работника (если он в черном списке у кого-то)
    private $excludeDomainMoviesForWorker = null;

    private $descriptionDateFrom = null;

    private $descriptionDateTo = null;

    private $yearFrom = null;

    private $yearTo = null;

    private $addDateFrom = null;

    private $addDateTo = null;

    private $imdbId = null;

    private $domainMovieDescriptionLengthFrom = null;

    private $domainMovieDescriptionLengthTo = null;

    private $ratingFrom = null;

    private $ratingTo = null;

    private $domainMovieChecked = null;

    private $searchByCustomerComment = null;

    private $excludeExpressMovies = null;

    private $excludeAdult = null;

    // в каком языке запрашивается содержимое?
    private $lang = null;

    // private $withCustomer = null;
    // по умолчанию true, т.к. ранее не было этого свойства, можно поставить false после того как везде учесть это
    private $calcSeasonsEpisodesData = true;

    private $group = null;

    private $fulltextSearchByTitle = null;

    public function __construct() {}

    public function get()
    {
        if (! $this->getLimit()) {
            // F::error('setLimit first');
            $this->setLimit(1);
        }
        if (! $this->getPage()) {
            // F::error('setPage first');
            $this->setPage(1);
        }
        $sql['from'] = ($this->getPage() - 1) * $this->getLimit();
        $sql['joinTags'] = '';
        $sql['groupBy'] = $this->getGroupBy() ? ('group by '.$this->getGroupBy()) : '';
        $sql['having'] = '';
        $sql['q'] = '';
        $sql['order'] = $this->getOrder() ? 'order by '.$this->getOrder() : '';
        /*
        if ($this->getSearchFromLeft()) {
            $sql['search'] = $this->getSearch()?'concat(cinema.title_ru,cinema.title_en) like \''.($this->getSearchFromLeft()?'':'%').$this->getSearch().'%\'':1;
        } else {
            $sql['search'] = $this->getSearch()?'concat(cinema.title_ru,cinema.title_en) like \'%'.$this->getSearch().'%\'':1;
        }
        */
        if (is_null($this->getChecked())) {
            $sql['checked'] = 1;
        } else {
            $sql['checked'] = $this->getChecked() ? 'cinema.checked = 1' : 'cinema.checked = 0';
        }
        if (is_null($this->getWithYear())) {
            $sql['withYear'] = 1;
        } else {
            $sql['withYear'] = $this->getWithYear() ? 'cinema.year is not null' : 'cinema.year is null';
        }
        $sql['year'] = $this->getYear() ? ('cinema.year = \''.$this->getYear().'\'') : 1;
        if ($this->tags) {

            /* если указаны теги и excludeMovie значит запрашиваются похожие видео, подменяем запрос, чтобы он работал через related_movies */
            if ($this->getExcludeMovie()) {

                $sql['tags'] = 1;
                $sql['joinTags'] = 'join (
				select related_movie_id as cinema_id, q
				from '.F::typetab('related_movies').'
				where movie_id = '.$this->getExcludeMovie().'
				order by q desc limit 1000
				) ct on cinema_id = cinema.id';
                $sql['q'] = ',ct.q';

            } else {

                /* если установлено group, значит нужна выдача по группе, подменяем запрос на легкий, через group_movie */
                if ($this->group) {

                    $sql['tags'] = 1;
                    $sql['joinTags'] = 'join (
					select movie_id as cinema_id
					from '.F::typetab('group_movie').'
					where group_id = '.$this->group.'
					) ct on cinema_id = cinema.id';

                } else {
                    // dump($tagsQuoted);
                    // общее при любом значении getTagsSearchStrict
                    // оборачиваем тэги в кавычки

                    $tagsQuoted = $this->tags;
                    array_walk($tagsQuoted, function (&$v) {
                        $v = '\''.$v.'\'';
                    });
                    // $sql['tags'] = 'cinema_tags.tag_id in ('.implode(', ',$tagsQuoted).')';
                    // $sql['joinTags'] = 'join '.F::typetab('cinema_tags').' on cinema_tags.cinema_id=cinema.id';
                    $sql['tags'] = 1;
                    // если установлен строгий поиск по тегам (должны присутствовать все теги), то делаем группировку и выводим те записи, где q = количеству передаваемых тегов
                    if ($this->getTagsSearchStrict()) {
                        $sql['having'] = 'having q='.count($this->tags);
                    }
                    $sql['joinTags'] = 'join (
					select cinema_id, count(*) as q
					from '.F::typetab('cinema_tags').'
					where cinema_tags.tag_id in ('.implode(', ', $tagsQuoted).')
					group by cinema_id
					'.$sql['having'].'
					'.($this->getExcludeMovie() ? 'order by q desc limit 1000' : '').'
					) ct on cinema_id = cinema.id';
                    $sql['groupBy'] = empty($sql['groupBy']) ? '' : F::error('Group By was already set ('.$sql['groupBy'].')');
                    $sql['q'] = ',ct.q';
                }
            }

            /*
            $arrExistsTags = [];
            foreach ($this->tags as $tag) {
                $arrExistsTags[] = 'exists (
                select *
                from '.F::typetab('cinema_tags').'
                where cinema_tags.tag_id = '.$tag.'
                and cinema_tags.cinema_id = cinema.id
                )';
            }
            $sql['existsTags'] = '('.implode($this->getTagsSearchStrict()?' and ':' or ', $arrExistsTags).')';
            $sql['tags'] = 1;
            */
        } else {

            /* дублирование кода для категории, пофиг, этой моделе жить недолго осталось */
            if ($this->group) {
                $sql['tags'] = 1;
                $sql['joinTags'] = 'join (
				select movie_id as cinema_id
				from '.F::typetab('group_movie').'
				where group_id = '.$this->group.'
				) ct on cinema_id = cinema.id';
            } else {
                /* $sql['existsTags'] = 1; */
                $sql['tags'] = 1;
            }

        }
        if (is_null($this->getDeleted())) {
            $sql['deleted'] = 1;
        } else {
            // почему то здесь в 3 раза быстрее работает конструкция not, а не = 0
            // поменял обратно на 0, потому что потом стало работать быстрее с 0. хз)
            // и еще раз поменял обратно на = 0, теперь быстрее так
            $sql['deleted'] = $this->getDeleted() ? 'cinema.deleted = 1' : 'cinema.deleted = 0';
            // $sql['deleted'] = $this->getDeleted()?'cinema.deleted':'not cinema.deleted';
        }
        $sql['unique_id'] = $this->getUniqueId() ? ('cinema.mw_unique_id = \''.$this->getUniqueId().'\'') : 1;
        if (is_null($this->getWithPoster())) {
            $sql['withPoster'] = 1;
        } else {
            $sql['withPoster'] = $this->getWithPoster() ? 'cinema.poster = 1' : 1;
        }
        $sql['excludeMovie'] = $this->getExcludeMovie() ? ('cinema.id != \''.$this->getExcludeMovie().'\'') : 1;
        $sql['movieTypes'] = '('.($this->getIncludeMovies() ? 'cinema.type = \'movie\'' : 0).' or '.($this->getIncludeSerials() ? 'cinema.type = \'serial\'' : 0).' or '.($this->getIncludeTrailers() ? 'cinema.type = \'trailer\'' : 0).' or '.($this->getIncludeNullType() ? 'cinema.type is null' : 0).' or '.($this->getIncludeGames() ? 'cinema.type = \'game\'' : 0).' or '.($this->getIncludeYoutube() ? 'cinema.type = \'youtube\'' : 0).')';
        // $sql['movieTypes'] = '(cinema.type in ('.implode(', ',$movieTypes).') '.($this->getIncludeNullType()?'or type is null':'').')';
        if ($this->getWithLegalPlayer()) {
            $sql['withLegalPlayer'] = '
			(select 1 from '.F::typetab('movie_players').' where movie_id = cinema.id limit 1)
			';
        } else {
            $sql['withLegalPlayer'] = 1;
        }
        if ($this->getTitleRu()) {
            $sql['titleRu'] = 'cinema.title_ru = \''.$this->getTitleRu().'\'';
        } else {
            $sql['titleRu'] = 1;
        }
        $sql['yearFrom'] = $this->getYearFrom() ? ('cinema.year >= \''.$this->getYearFrom().'\'') : 1;
        $sql['yearTo'] = $this->getYearTo() ? ('cinema.year <= \''.$this->getYearTo().'\'') : 1;
        $sql['ratingFrom'] = $this->getRatingFrom() ? ('cinema.rating_sko >= \''.$this->getRatingFrom().'\'') : 1;
        $sql['ratingTo'] = $this->getRatingTo() ? ('cinema.rating_sko <= \''.$this->getRatingTo().'\'') : 1;
        if ($this->getUseDomainMovies()) {
            // $searchStr = $this->getLeftSearch() ? "{$this->getSearch()}*" : "*{$this->getSearch()}*";
            $searchStr = $this->getLeftSearch() ? "{$this->getSearch()}%" : "%{$this->getSearch()}%";
            $searchByCustomerComment = $this->getSearchByCustomerComment() ? ("domain_movies.customer_comment like '%{$this->getSearch()}%'") : 0;
            $searchByKinopiskId = is_int($this->getSearch()) ? "cinema.kinopoisk_id = {$this->getSearch()}" : 0;
            // match (cinema.title_ru,cinema.title_en) against ('{$searchStr}' in boolean mode) or
            $sql['search'] = $this->getSearch() ? (
                "(
				cinema.title_ru like '{$searchStr}' or
				cinema.title_en like '{$searchStr}' or
				{$searchByKinopiskId} or
				{$searchByCustomerComment}
				)"
            ) : 1;
            $sql['fulltextSearchByTitle'] = $this->getFulltextSearchByTitle() ? 'MATCH(cinema.title_ru, cinema.title_en) AGAINST(\''.$this->getFulltextSearchByTitle().'*\' IN BOOLEAN MODE)' : 1;
            if ($this->getDomain()) {
                $sql['joinDomainMovies'] = '
				join '.F::tabname('cinema', 'domain_movies').' on domain_movies.movie_id = cinema.id
				and domain_movies.domain = \''.$this->getDomain().'\'
				';
            } else {
                $sql['joinDomainMovies'] = 'join '.F::typetab('domain_movies').' on domain_movies.movie_id = cinema.id';
            }
            $sql['domain_movies_id'] = ',domain_movies.id as domain_movies_id';
            // $sql['uniqueDescription'] = 'domain_movies.domain = \''.$this->getDomain().'\'';
            if (is_null($this->getWithDomainDescription())) {
                $sql['withDomainDescription'] = 1;
            } else {
                // $sql['withDomainDescription'] = $this->getWithDomainDescription()?'(domain_movies.description_length > 0)':'(domain_movies.description_length = 0)';
                // не знаю почему, но запрос с description_length стал выполняться дольше чем через is null, пока что ставим эту версию
                $sql['withDomainDescription'] = $this->getWithDomainDescription() ? '(domain_movies.description is not null)' : '(domain_movies.description is null)';
            }
            // if (is_null($this->getWithCustomer())) {
            // 	$sql['withCustomer'] = 1;
            // } else {
            // 	$sql['withCustomer'] = $this->getWithCustomer()?'(domain_movies.customer is not null)':'(domain_movies.customer is null)';
            // }
            $sql['descriptionWriter'] = $this->getDescriptionWriter() ? 'domain_movies.description_writer = \''.$this->getDescriptionWriter().'\'' : 1;
            $sql['descriptionDateFrom'] = $this->getDescriptionDateFrom() ? ('domain_movies.description_date > \''.$this->getDescriptionDateFrom().'\'') : 1;
            $sql['descriptionDateTo'] = $this->getDescriptionDateTo() ? ('domain_movies.description_date < \''.$this->getDescriptionDateTo().'\'') : 1;
            $sql['bookedForLogin'] = $this->getBookedForLogin() ? 'domain_movies.booked_for_login = \''.$this->getBookedForLogin().'\'' : 1;
            if (is_null($this->getDomainMovieActive())) {
                $sql['domainMovieActive'] = 1;
            } else {
                // $sql['domainMovieActive'] = $this->getDomainMovieActive()?'domain_movies.active = 1':'domain_movies.active = 0';
                // хз почему но так рабтает все-таки быстрее в каких-то случаях
                $sql['domainMovieActive'] = $this->getDomainMovieActive() ? 'domain_movies.active' : 'not domain_movies.active';
            }
            $sql['domainMovieId'] = $this->getDomainMovieId() ? 'domain_movies.id = \''.$this->getDomainMovieId().'\'' : 1;
            $sql['withBookedForLogin'] = is_null($this->getWithBookedForLogin()) ? 1 : ($this->getWithBookedForLogin() ? 'domain_movies.booked_for_login is not null' : 'domain_movies.booked_for_login is null');
            $sql['domainMovieCustomer'] = $this->getDomainMovieCustomer() ? 'domain_movies.customer = \''.$this->getDomainMovieCustomer().'\'' : 1;
            // F::dump($this);
            if ($this->getUseDomainMovieCustomers()) {
                $sql['joinCustomers'] = 'left join '.F::typetab('users').' customers on customers.login = domain_movies.customer';
                // $sql['domainMovieCustomerTempBalanceIsMoreThan'] = is_null($this->getDomainMovieCustomerTempBalanceIsMoreThan())?1:('(domain_movies.customer is null or customers.temp_balance > '.$this->getDomainMovieCustomerTempBalanceIsMoreThan().')');
                $sql['domainMovieWithCustomerEnoughBalance'] = is_null($this->getDomainMovieWithCustomerEnoughBalance()) ? 1 : ($this->getDomainMovieWithCustomerEnoughBalance() ? ('(domain_movies.customer is null or customers.temp_balance >= domain_movies.expected_price)') : ('(customers.temp_balance < domain_movies.expected_price)'));
                $sql['domainMovieSymbolsFrom'] = $this->getDomainMovieSymbolsFrom() ? ('domain_movies.symbols_from >= '.$this->getDomainMovieSymbolsFrom()) : 1;
                $sql['domainMovieSymbolsTo'] = $this->getDomainMovieSymbolsTo() ? ('domain_movies.symbols_to <= '.$this->getDomainMovieSymbolsTo()) : 1;
            } else {
                $sql['joinCustomers'] = '';
                // $sql['domainMovieCustomerTempBalanceIsMoreThan'] = 1;
                $sql['domainMovieWithCustomerEnoughBalance'] = 1;
                $sql['domainMovieSymbolsFrom'] = 1;
                $sql['domainMovieSymbolsTo'] = 1;
            }
            $sql['domainMovieShowToCustomer'] = is_null($this->getDomainMovieShowToCustomer()) ? 1 : ($this->getDomainMovieShowToCustomer() ? 'domain_movies.show_to_customer = 1' : 'domain_movies.show_to_customer = 0');
            $sql['domainMovieRework'] = is_null($this->getDomainMovieRework()) ? 1 : ($this->getDomainMovieRework() ? 'domain_movies.rework = 1' : 'domain_movies.rework = 0');
            $sql['domainMovieCustomerArchived'] = is_null($this->getDomainMovieCustomerArchived()) ? 1 : ($this->getDomainMovieCustomerArchived() ? 'domain_movies.customer_archived = 1' : 'domain_movies.customer_archived = 0');
            $sql['domainMovieWriting'] = is_null($this->getDomainMovieWriting()) ? 1 : ($this->getDomainMovieWriting() ? 'domain_movies.writing = 1' : 'domain_movies.writing = 0');
            $sql['excludeDescriptionWriters'] = $this->getExcludeDescriptionWriters() ? ('domain_movies.description_writer not in (\''.implode('\',\'', $this->getExcludeDescriptionWriters()).'\')') : 1;
            $sql['withDomainMovieCustomerComment'] = is_null($this->getWithDomainMovieCustomerComment()) ? 1 : ($this->getWithDomainMovieCustomerComment() ? 'domain_movies.customer_comment is not null' : 'domain_movies.customer_comment is null');
            $sql['domainMovieDeleted'] = is_null($this->getDomainMovieDeleted()) ? 1 : ($this->getDomainMovieDeleted() ? 'domain_movies.deleted = 1' : 'domain_movies.deleted = 0');
            // $sql['domainMoviePageDeleted'] = is_null($this->getDomainMoviePageDeleted())?1:($this->getDomainMoviePageDeleted()?'domain_movies.deleted_page = 1':'domain_movies.deleted_page = 0');
            // хз почему но так работает быстрее в каких-то случаях
            $sql['domainMoviePageDeleted'] = is_null($this->getDomainMoviePageDeleted()) ? 1 : ($this->getDomainMoviePageDeleted() ? 'domain_movies.deleted_page' : 'not domain_movies.deleted_page');
            $sql['domainMovieDescriptionLengthFrom'] = is_null($this->getDomainMovieDescriptionLengthFrom()) ? 1 : ('domain_movies.description_length > '.$this->getDomainMovieDescriptionLengthFrom());
            $sql['domainMovieDescriptionLengthTo'] = is_null($this->getDomainMovieDescriptionLengthTo()) ? 1 : ('domain_movies.description_length < '.$this->getDomainMovieDescriptionLengthTo());
            if ($this->getExcludeDomainMoviesForWorker()) {
                // F::dump('ok');
                // $sql['joinCustomerBlacklistForWorker'] = 'left join '.F::typetab('customer_blacklist').' on customer_blacklist.customer = domain_movies.customer and customer_blacklist.worker = \''.$this->getExcludeDomainMoviesForWorker().'\'';
                // так быстрее
                $sql['joinCustomerBlacklistForWorker'] = 'left join (
				select customer,worker
				from '.F::typetab('customer_blacklist').'
				where customer_blacklist.worker = \''.$this->getExcludeDomainMoviesForWorker().'\'
				) as customer_blacklist on customer_blacklist.customer = domain_movies.customer';
                $sql['excludeDomainMoviesForWorker'] = 'customer_blacklist.worker is null';
            } else {
                $sql['joinCustomerBlacklistForWorker'] = '';
                $sql['excludeDomainMoviesForWorker'] = 1;
            }
            if (is_null($this->getDomainMovieChecked())) {
                $sql['domainMovieChecked'] = 1;
            } else {
                $sql['domainMovieChecked'] = $this->getDomainMovieChecked() ? 'domain_movies.checked = 1' : 'domain_movies.checked = 0';
            }
            $sql['excludeExpressMovies'] = $this->getExcludeExpressMovies() ? ('domain_movies.description_price_per_1k <= '.(new Cinema)->getRewritePricePer1k()) : 1;
            $sql['excludeAdult'] = $this->getExcludeAdult() ? 'not domain_movies.adult' : 1;
        } else {
            $sql['search'] = $this->getSearch() ? 'concat(ifnull(cinema.title_ru,\'\'),ifnull(cinema.title_en,\'\'),ifnull(cinema.kinopoisk_id,\'\')) like \''.($this->getLeftSearch() ? '' : '%').$this->getSearch().'%\'' : 1;
            $sql['fulltextSearchByTitle'] = $this->getFulltextSearchByTitle() ? 'MATCH(cinema.title_ru, cinema.title_en) AGAINST(\''.$this->getFulltextSearchByTitle().'*\' IN BOOLEAN MODE)' : 1;
            $sql['joinDomainMovies'] = '';
            $sql['domain_movies_id'] = '';
            $sql['withDomainDescription'] = 1;
            $sql['descriptionWriter'] = 1;
            $sql['descriptionDateFrom'] = 1;
            $sql['descriptionDateTo'] = 1;
            $sql['bookedForLogin'] = 1;
            $sql['domainMovieActive'] = 1;
            $sql['domainMovieId'] = 1;
            $sql['withBookedForLogin'] = 1;
            $sql['domainMovieCustomer'] = 1;
            // $sql['domainMovieCustomerTempBalanceIsMoreThan'] = 1;
            $sql['domainMovieWithCustomerEnoughBalance'] = 1;
            $sql['joinCustomers'] = '';
            $sql['domainMovieRework'] = 1;
            $sql['domainMovieShowToCustomer'] = 1;
            $sql['domainMovieSymbolsFrom'] = 1;
            $sql['domainMovieSymbolsTo'] = 1;
            $sql['domainMovieCustomerArchived'] = 1;
            $sql['domainMovieWriting'] = 1;
            $sql['excludeDescriptionWriters'] = 1;
            $sql['withDomainMovieCustomerComment'] = 1;
            $sql['domainMovieDeleted'] = 1;
            // $sql['uniqueDescription'] = 1;
            $sql['domainMoviePageDeleted'] = 1;
            $sql['joinCustomerBlacklistForWorker'] = '';
            $sql['excludeDomainMoviesForWorker'] = 1;
            $sql['domainMovieDescriptionLengthFrom'] = 1;
            $sql['domainMovieDescriptionLengthTo'] = 1;
            $sql['domainMovieChecked'] = 1;
            $sql['excludeExpressMovies'] = 1;
            $sql['excludeAdult'] = 1;
        }
        $sql['movieId'] = $this->getMovieId() ? 'cinema.id = \''.$this->getMovieId().'\'' : 1;
        $sql['withKinopoiskId'] = is_null($this->getWithKinopoiskId()) ? 1 : ($this->getWithKinopoiskId() ? 'cinema.kinopoisk_id is not null' : 'cinema.kinopoisk_id is null');
        $sql['withKinopoiskRatingSko'] = is_null($this->getWithKinopoiskRatingSko()) ? 1 : ($this->getWithKinopoiskRatingSko() ? 'cinema.kinopoisk_rating_sko is not null' : 'cinema.kinopoisk_rating_sko is null');
        $sql['withRatingSko'] = is_null($this->getWithRatingSko()) ? 1 : ($this->getWithRatingSko() ? 'cinema.rating_sko is not null' : 'cinema.rating_sko is null');
        $sql['addedAtFrom'] = $this->getAddedAtFrom() ? ('cinema.added_at > \''.$this->getAddedAtFrom().'\'') : 1;
        $sql['addedAtTo'] = $this->getAddedAtTo() ? ('cinema.added_at < \''.$this->getAddedAtTo().'\'') : 1;
        $sql['addDateFrom'] = $this->getAddDateFrom() ? ('cinema.add_date > \''.$this->getAddDateFrom().'\'') : 1;
        $sql['addDateTo'] = $this->getAddDateTo() ? ('cinema.add_date < \''.$this->getAddDateTo().'\'') : 1;
        // я не дурак, здесь все правильно, запрос именно такой, потому что ищем все cinema.id которых нет в domain_movies для какого то домена, без join domain_movies
        $sql['domainIsNot'] = $this->getDomainIsNot() ? ('not exists (select * from '.F::typetab('domain_movies').' where movie_id = cinema.id and domain = \''.$this->getDomainIsNot().'\')') : 1;
        $sql['kinopoiskId'] = $this->getKinopoiskId() ? 'cinema.kinopoisk_id = \''.$this->getKinopoiskId().'\'' : 1;
        $sql['imdbId'] = $this->getImdbId() ? 'cinema.imdb_id = \''.$this->getImdbId().'\'' : 1;
        // F::dump($this);
        $arr = F::query_arr(
            'select SQL_CALC_FOUND_ROWS cinema.id
			'.$sql['q'].'
			'.$sql['domain_movies_id'].'
			from '.F::tabname('cinema', 'cinema').'
			'.$sql['joinTags'].'
			'.$sql['joinDomainMovies'].'
			'.$sql['joinCustomers'].'
			'.$sql['joinCustomerBlacklistForWorker'].'
			where
			'.$sql['search'].' and
			'.$sql['fulltextSearchByTitle'].' and
			'.$sql['tags'].' and
			'.$sql['withYear'].' and
			'.$sql['deleted'].' and
			'.$sql['unique_id'].' and
			'.$sql['checked'].' and
			'.$sql['withPoster'].' and
			'.$sql['year'].' and
			'.$sql['excludeMovie'].' and
			'.$sql['movieTypes'].' and
			'.$sql['withDomainDescription'].' and
			'.$sql['descriptionWriter'].' and
			'.$sql['bookedForLogin'].' and
			'.$sql['domainMovieActive'].' and
			'.$sql['movieId'].' and
			'.$sql['domainMovieId'].' and
			'.$sql['withKinopoiskId'].' and
			'.$sql['withKinopoiskRatingSko'].' and
			'.$sql['withRatingSko'].' and
			'.$sql['addedAtFrom'].' and
			'.$sql['addedAtTo'].' and
			'.$sql['addDateFrom'].' and
			'.$sql['addDateTo'].' and
			'.$sql['domainIsNot'].' and
			'.$sql['withBookedForLogin'].' and
			'.$sql['domainMovieCustomer'].' and
			'.$sql['domainMovieWithCustomerEnoughBalance'].' and
			'.$sql['domainMovieRework'].' and
			'.$sql['domainMovieShowToCustomer'].' and
			'.$sql['domainMovieSymbolsFrom'].' and
			'.$sql['domainMovieSymbolsTo'].' and
			'.$sql['domainMovieCustomerArchived'].' and
			'.$sql['domainMovieWriting'].' and
			'.$sql['excludeDescriptionWriters'].' and
			'.$sql['withDomainMovieCustomerComment'].' and
			'.$sql['kinopoiskId'].' and
			'.$sql['imdbId'].' and
			'.$sql['domainMovieDeleted'].' and
			'.$sql['withLegalPlayer'].' and
			'.$sql['titleRu'].' and
			'.$sql['domainMoviePageDeleted'].' and
			'.$sql['excludeDomainMoviesForWorker'].' and
			'.$sql['descriptionDateFrom'].' and
			'.$sql['descriptionDateTo'].' and
			'.$sql['yearFrom'].' and
			'.$sql['yearTo'].' and
			'.$sql['domainMovieDescriptionLengthFrom'].' and
			'.$sql['domainMovieDescriptionLengthTo'].' and
			'.$sql['ratingFrom'].' and
			'.$sql['ratingTo'].' and
			'.$sql['domainMovieChecked'].' and
			'.$sql['excludeExpressMovies'].' and
			'.$sql['excludeAdult'].'

			'.$sql['groupBy'].'
			'.$sql['order'].'
			limit '.$sql['from'].','.$this->getLimit().'
			;'
        );
        // dump($arr);
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getWithPoster()
    {
        return $this->withPoster;
    }

    public function setWithPoster($b = null)
    {
        $this->withPoster = $b;
    }

    public function getGroupBy()
    {
        return $this->groupBy;
    }

    public function setGroupBy($groupBy = null)
    {
        $this->groupBy = $groupBy;
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function setChecked($b = null)
    {
        return $this->checked = $b;
    }

    public function setGroup($groupId = null)
    {

        /* проставляем группу, чтобы потом подменить запрос в get() для работы через group_movie */
        $this->group = $groupId;

        if (! $groupId) {
            return false;
        }
        $group = new Group($groupId);
        $tags = $group->getTags();
        $this->setTags($tags);
        $this->setTagsSearchStrict(true);
    }

    // добавляет к массиву тегов новый массив тегов
    public function setTags($tags = [])
    {
        $this->tags = is_array($tags) ? array_merge($this->tags, $tags) : F::error('setTags value should be an array');
    }

    public function setSearch($text = null)
    {
        $this->search = $text;
    }

    public function getSearch()
    {
        return $this->search;
    }

    public function getList()
    {
        if (! $this->getListTemplate()) {
            F::error('setListTemplate first');
        }
        $list = '';
        $arr = $this->get();
        // F::dump($arr);
        foreach ($arr as $v) {
            $t = new Template($this->getListTemplate());
            $t->setLang($this->getLang());
            if ($this->getUseDomainMovies()) {
                $movie = new DomainMovie($v['domain_movies_id']);
                $t->v('domainMovieId', $movie->getDomainMovieId());
                $t->v('domainMovieDescription', $movie->getDomainMovieDescription());
                $t->v('domainMovieDescriptionWriter', $movie->getDomainMovieDescriptionWriter());
                $t->v('domainMovieDescriptionDate', $movie->getDomainMovieDescriptionDate());
                $t->v('domain', $movie->getDomain());
                // F::dump($this->getDomainClass());
                $t->v('domainMovieCustomerComment', htmlspecialchars((string) $movie->getDomainMovieCustomerComment()));
                $t->v('domainMovieSymbolsFrom', $movie->getDomainMovieSymbolsFrom());
                $t->v('domainMovieSymbolsTo', $movie->getDomainMovieSymbolsTo());
                $t->v('domainMovieDescriptionShort', mb_strimwidth((string) $movie->getDomainMovieDescription(), 0, 160, '...'));
                $t->v('domainMovieDescriptionLength', $movie->getDomainMovieDescriptionLength());
            } else {
                $movie = new Movie($v['id']);
            }
            // F::dump($this);
            // F::dump($movie);
            $t->v('title', [
                'ru' => $movie->getTitleRu(),
                'ua' => $movie->getTitleRu(),
                'en' => $movie->getTitleEnIfPossible(),
            ]);
            $t->v('titleHtmlSpecialchars', htmlspecialchars($t->getVar('title')));
            $t->v('titleRu', $movie->getTitleRu());
            $t->v('titleEn', $movie->getTitleEn());
            $t->v('titleEnIfPossible', $movie->getTitleEnIfPossible());
            $t->v('description', $movie->getDescription());
            $t->v('descriptionShort', mb_strimwidth((string) $movie->getDescription(), 0, 160, '...'));
            $t->v('movieId', $movie->getId());
            $t->v('poster', $movie->getPosterOrPlaceholder());
            $t->v('posterWebP', $movie->getPosterWebPOrPlaceholder());
            $t->v('year', $movie->getYear());
            $t->v('type', [
                'ru' => $movie->getTypeRu(true),
                'ua' => $movie->getTypeUa(true),
                'en' => $movie->getTypeEn(true),
            ]);
            $t->v('typeVar', $movie->getType());
            $t->v('typeEn', $movie->getTypeEn(true));
            $t->v('titleRuUrl', $movie->getTitleRuUrl());
            $t->v('kinopoisk_id', $movie->getKinopoiskId());
            $t->v('kinopoisk_rating', $movie->getKinopoiskRating());
            $t->v('kinopoisk_votes', $movie->getKinopoiskVotes());
            $t->v('kinopoisk_rating_sko', $movie->getKinopoiskRatingSko());
            $t->v('imdb_rating', $movie->getImdbRating());
            $t->v('imdb_votes', $movie->getImdbVotes());
            $t->v('imdb_rating_sko', $movie->getImdbRatingSko());
            $t->v('ratingSko', round((int) $movie->getRatingSko(), 1));
            if ($movie->getType() === 'trailer') {
                $t->v('quality', ['ru' => 'Трейлер', 'ua' => 'Трейлер', 'en' => 'Trailer']);
            } else {
                $t->v('quality', ['ru' => 'Хорошее (HD)', 'ua' => 'Гарне (HD)', 'en' => 'Good (HD)']);
            }
            // F::dump($t);

            if ($this->getCalcSeasonsEpisodesData()) {
                $lastSeason = $movie->getLastSeason();
                if ($lastSeason) {
                    $t->v('movieLastSeason', $lastSeason);
                    $lastEpisodesOfSeasonArr = $movie->getLastEpisodesOfSeason($lastSeason, 3);
                    // f::dump($lastEpisodesOfSeasonArr);
                    $t->v('movieLastEpisodes', implode(', ', $lastEpisodesOfSeasonArr));
                    $t->v('movieLastEpisode', end($lastEpisodesOfSeasonArr));
                } else {
                    $t->v('movieLastSeason', 1);
                    $t->v('movieLastEpisodes', 1);
                    $t->v('movieLastEpisode', 1);
                }
                $t->v('movieEpisodesSum', $movie->getEpisodesSum());
            }
            // для ?seoLinks
            $t->v('httpCurrentDomain', 'http://'.Engine::getDomain());
            $t->v('httpsCurrentDomain', 'https://'.Engine::getDomain());
            $t->v('lastRedirectDomain', Engine::getDomainObject()->getLastRedirectDomain());
            $t->v('requestedDomain', Engine::getRequestedDomainObject()->getDomain());
            $t->v('http', 'http://');
            $t->v('https', 'https://');
            if ($this->getDomain()) {
                if ($this->getDomainObject()) {
                    $domain = $this->getDomainObject();
                } else {
                    $domain = new Domain($this->getDomain());
                    $this->setDomainObject($domain);
                }
                $domain->setLang($this->getLang());
                $t->v('watchLink', $domain->getWatchLink($movie));
                $t->v('watchLinkEn', $domain->getWatchLink($movie, $domain->getWatchLinkEnTemplate()));
                $http = $domain->getHttpsOnly() ? 'https://' : 'http://';
                $t->v('httpWatchLink', $http.$domain->getDomain().$domain->getWatchLink($movie));
                $t->v('domainProtocol', $domain->getProtocol().'://');
            } else {
                $t->v('watchLink', '');
                $t->v('watchLinkEn', '');
                $t->v('httpWatchLink', '');
                $t->v('domainProtocol', '');
            }
            // если указан шаблон для списка категорий, то выводим его тоже
            if ($this->getGenresListTemplate()) {
                $movieTags = new MovieTags($movie->getId());
                $movieTags->setLang($this->getLang());
                $movieTags->setListTemplate($this->getGenresListTemplate());
                $t->v('genresList', $movieTags->getGenres());
            }
            if ($movie->getYoutubeData()) {
                $youtube = $movie->getYoutubeData();
                $t->v('movieYoutubeDuration', (new \DateInterval($youtube->getData()->contentDetails->duration))->format('%h:%I:%S'));
                $t->v('movieYoutubeThumbnail', $youtube->getData()->snippet->thumbnails->medium->url);
            }
            $list .= $t->get();
        }

        return $list;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function setGenresListTemplate($template = null)
    {
        $this->genresListTemplate = $template;
    }

    public function getGenresListTemplate()
    {
        return $this->genresListTemplate;
    }

    public function setTag($tag = null)
    {
        if ($tag) {
            $this->tags[] = $tag;
        }
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setPage($page = null)
    {
        $this->page = (int) $page;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getPages()
    {
        if (! $this->getPaginationUrlPattern()) {
            F::error('setPaginationUrlPattern first');
        }
        $Paginator = new PaginatorCustom;
        $Paginator->setLang($this->getLang());
        $Paginator->setUrlPattern($this->getPaginationUrlPattern());
        $Paginator->setTemplateList('pagination/list');
        $Paginator->setTemplatePrev('pagination/prev');
        $Paginator->setTemplateNext('pagination/next');
        $Paginator->setRows($this->getRows());
        $Paginator->setLimit($this->getLimit());
        $Paginator->setPage($this->getPage());
        $Paginator->setMaxPagesToShow(4);

        return $Paginator->get();
        // return new Paginator($this->getRows(),$this->getLimit(),$this->getPage(),$this->getPaginationUrlPattern());
    }

    public function getPaginationUrlPattern()
    {
        return $this->paginationUrlPattern;
    }

    public function setPaginationUrlPattern($v = null)
    {
        $this->paginationUrlPattern = $v;
    }

    public function setWithYear($b = null)
    {
        $this->withYear = $b;
    }

    public function getWithYear()
    {
        return $this->withYear;
    }

    public function getDeleted()
    {
        return $this->deleted;
    }

    public function setDeleted($b = null)
    {
        $this->deleted = $b;
    }

    public function setUniqueId($id = null)
    {
        $this->unique_id = $id;
    }

    public function getUniqueId()
    {
        return $this->unique_id;
    }

    public static function resetCheckers()
    {
        F::query('update '.F::typetab('cinema').' set checked = 0 where type != \'youtube\';');

        return true;
    }

    public static function getYearsList($args = [])
    {
        $template = isset($args['listTemplate']) ? $args['listTemplate'] : null;
        $active = isset($args['active']) ? $args['active'] : null;
        $arr = F::query_arr('select year
			from '.F::typetab('cinema').'
			where year
			group by year
			order by year desc;');
        $list = '';
        foreach ($arr as $v) {
            $t = new Template($template);
            if ($v['year'] === $active) {
                $t->v('active', 'active');
                $t->v('selected', 'selected');
            } else {
                $t->v('active', '');
                $t->v('selected', '');
            }
            $t->v('year', $v['year']);
            $list .= $t->get();
        }

        return $list;
    }

    public static function getOrders()
    {
        $arr = [];
        $arr[] = [
            'name' => 'topRated',
            'orderBy' => 'cinema.rating_sko desc',
            'text' => 'По рейтингу',
            'showInSearchOptions' => true,
        ];
        $arr[] = [
            'name' => 'worstRated',
            'orderBy' => 'cinema.rating_sko',
            'text' => 'По рейтингу (худшее)',
            'showInSearchOptions' => false,
        ];
        $arr[] = [
            'name' => 'mostRecent',
            'orderBy' => 'cinema.year desc, cinema.added_at desc',
            'text' => 'По дате',
            'showInSearchOptions' => true,
        ];
        $arr[] = [
            'name' => 'recentAndTopRated',
            'orderBy' => 'cinema.year desc, cinema.rating_sko desc',
            'text' => 'Популярные новинки',
            'showInSearchOptions' => true,
        ];
        $arr[] = [
            'name' => 'mostRecentAdded',
            'orderBy' => 'domain_movies.add_date desc',
            'text' => 'Последние добавленные',
            'showInSearchOptions' => false,
        ];

        // $arr = array(
        // 	'topRated' => 'cinema.kinopoisk_rating desc',
        // 	'mostRecent' => 'cinema.added_at desc'
        // );
        return $arr;
    }

    public static function getOrderByName($name = null)
    {
        $arr = static::getOrders();
        foreach ($arr as $v) {
            if ($name === $v['name']) {
                return $v;
            }
        }

        // return isset($arr[$name])?$arr[$name]:false;
        return false;
    }

    public static function getOrdersList($args = [])
    {
        $template = isset($args['listTemplate']) ? $args['listTemplate'] : null;
        $active = isset($args['active']) ? $args['active'] : null;
        $arr = static::getOrders();
        $list = '';
        foreach ($arr as $v) {
            if (! $v['showInSearchOptions']) {
                continue;
            }
            $t = new Template($template);
            if ($v['name'] === $active) {
                $t->v('active', 'active');
                $t->v('selected', 'selected');
            } else {
                $t->v('active', '');
                $t->v('selected', '');
            }
            $t->v('name', $v['name']);
            $t->v('text', $v['text']);
            $list .= $t->get();
        }

        return $list;
    }

    public function setTagsSearchStrict($b = null)
    {
        $this->tagsSearchStrict = $b;
    }

    public function getTagsSearchStrict()
    {
        return $this->tagsSearchStrict;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year = null)
    {
        $this->year = $year;
    }

    public function setLeftSearch($b = null)
    {
        $this->leftSearch = $b;
    }

    public function getLeftSearch()
    {
        return $this->leftSearch;
    }

    public function setExcludeMovie($id = null)
    {
        $this->excludeMovie = $id;
    }

    public function getExcludeMovie()
    {
        return $this->excludeMovie;
    }

    public function getIncludeMovies()
    {
        return $this->includeMovies;
    }

    public function getIncludeSerials()
    {
        return $this->includeSerials;
    }

    public function getIncludeTrailers()
    {
        return $this->includeTrailers;
    }

    public function getIncludeNullType()
    {
        return $this->includeNullType;
    }

    public function setIncludeNullType($b = null)
    {
        $this->includeNullType = $b;
    }

    public function setIncludeMovies($b = null)
    {
        $this->includeMovies = $b;
    }

    public function setIncludeSerials($b = null)
    {
        $this->includeSerials = $b;
    }

    public function setIncludeTrailers($b = null)
    {
        $this->includeTrailers = $b;
    }

    public function getIncludeGames()
    {
        return $this->includeGames;
    }

    public function setIncludeGames($b = null)
    {
        $this->includeGames = $b;
    }

    public function getIncludeYoutube()
    {
        return $this->includeYoutube;
    }

    public function setIncludeYoutube($b = null)
    {
        $this->includeYoutube = $b;
    }

    // function getWithUniqueDescription() {
    // 	return $this->withUniqueDescription;
    // }

    // function setWithUniqueDescription($b = null) {
    // 	$this->withUniqueDescription = $b;
    // }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        if (! is_null($domain)) {
            $this->setUseDomainMovies(true);
        }
        $this->domain = $domain;
    }

    public function setWithDomainDescription($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->withDomainDescription = $b;
    }

    public function getWithDomainDescription()
    {
        return $this->withDomainDescription;
    }

    public function setDescriptionWriter($login = null)
    {
        if (! is_null($login)) {
            $this->setUseDomainMovies(true);
        }
        $this->descriptionWriter = $login;
    }

    public function getDescriptionWriter()
    {
        return $this->descriptionWriter;
    }

    public function setDescriptionDateFrom($date = null)
    {
        if (! is_null($date)) {
            $this->setUseDomainMovies(true);
        }
        $this->descriptionDateFrom = $date;
    }

    public function getDescriptionDateFrom()
    {
        return $this->descriptionDateFrom;
    }

    public function getDescriptionDateTo()
    {
        return $this->descriptionDateTo;
    }

    public function setDescriptionDateTo($date = null)
    {
        if (! is_null($date)) {
            $this->setUseDomainMovies(true);
        }
        $this->descriptionDateTo = $date;
    }

    public function setBookedForLogin($login = null)
    {
        if (! is_null($login)) {
            $this->setUseDomainMovies(true);
        }
        $this->bookedForLogin = $login;
    }

    public function getBookedForLogin()
    {
        return $this->bookedForLogin;
    }

    public function getUseDomainMovies()
    {
        return $this->useDomainMovies;
    }

    public function setUseDomainMovies($b = null)
    {
        $this->useDomainMovies = $b;
    }

    // оставлена для совместимости. заменить везде setActive на setDomainMovieActive
    public function setActive($b = null)
    {
        $this->setDomainMovieActive($b);
    }

    // оставлена для совместимости. заменить везде getActive на getDomainMovieActive
    public function getActive()
    {
        return $this->getDomainMovieActive();
    }

    public function setMovieId($movieId = null)
    {
        $this->movieId = $movieId;
    }

    public function getMovieId()
    {
        return $this->movieId;
    }

    public function getWithKinopoiskId()
    {
        return $this->withKinopoiskId;
    }

    public function setWithKinopoiskId($b = null)
    {
        $this->withKinopoiskId = $b;
    }

    public function getWithKinopoiskRatingSko()
    {
        return $this->withKinopoiskRatingSko;
    }

    public function setWithKinopoiskRatingSko($b = null)
    {
        $this->withKinopoiskRatingSko = $b;
    }

    public function getWithRatingSko()
    {
        return $this->withRatingSko;
    }

    public function setWithRatingSko($b = null)
    {
        $this->withRatingSko = $b;
    }

    public function getAddedAtFrom()
    {
        return $this->addedAtFrom;
    }

    public function setAddedAtFrom($date = null)
    {
        $this->addedAtFrom = $date;
    }

    public function getAddedAtTo()
    {
        return $this->addedAtTo;
    }

    public function setAddedAtTo($date = null)
    {
        $this->addedAtTo = $date;
    }

    // неверное название метода, оставляем для совместимости
    public function getDomainClass()
    {
        return $this->getDomainObject();
    }

    // неверное название метода, оставляем для совместимости
    public function setDomainClass($object = null)
    {
        $this->setDomainObject($object);
    }

    public function getDomainObject()
    {
        // logger()->info('MOVIES::getDomainObject requested');
        return $this->domainObject;
    }

    public function setDomainObject($object = null)
    {
        // logger()->info('MOVIES::setDomainObject requested');
        $this->domainObject = $object;
    }

    public function setDomainMovieId($movieId = null)
    {
        if (! is_null($movieId)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieId = $movieId;
    }

    public function getDomainMovieId()
    {
        return $this->domainMovieId;
    }

    public function getDomainIsNot()
    {
        return $this->domainIsNot;
    }

    public function setDomainIsNot($domain = null)
    {
        $this->domainIsNot = $domain;
    }

    public function getWithBookedForLogin()
    {
        return $this->withBookedForLogin;
    }

    public function setWithBookedForLogin($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->withBookedForLogin = $b;
    }

    public function getDomainMovieActive()
    {
        return $this->domainMovieActive;
    }

    public function setDomainMovieActive($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieActive = $b;
    }

    public function getDomainMovieCustomer()
    {
        return $this->domainMovieCustomer;
    }

    public function setDomainMovieCustomer($login = null)
    {
        if (! is_null($login)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieCustomer = $login;
    }

    // function setDomainMovieCustomerTempBalanceIsMoreThan($amount = null) {
    // 	if (!is_null($amount)) {
    // 		$this->setUseDomainMovieCustomers(true);
    // 	}
    // 	$this->domainMovieCustomerTempBalanceIsMoreThan = $amount;
    // }

    // function getDomainMovieCustomerTempBalanceIsMoreThan() {
    // 	return $this->domainMovieCustomerTempBalanceIsMoreThan;
    // }

    public function setDomainMovieWithCustomerEnoughBalance($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovieCustomers(true);
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieWithCustomerEnoughBalance = $b;
    }

    public function getDomainMovieWithCustomerEnoughBalance()
    {
        return $this->domainMovieWithCustomerEnoughBalance;
    }

    public function getUseDomainMovieCustomers()
    {
        return $this->useDomainMovieCustomers;
    }

    public function setUseDomainMovieCustomers($b = null)
    {
        $this->useDomainMovieCustomers = $b;
    }

    public function setDomainMovieRework($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieRework = $b;
    }

    public function getDomainMovieRework()
    {
        return $this->domainMovieRework;
    }

    public function setDomainMovieSymbolsFrom($length = null)
    {
        if (! is_null($length)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieSymbolsFrom = $length;
    }

    public function getDomainMovieSymbolsFrom()
    {
        return $this->domainMovieSymbolsFrom;
    }

    public function setDomainMovieSymbolsTo($length = null)
    {
        if (! is_null($length)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieSymbolsTo = $length;
    }

    public function getDomainMovieSymbolsTo()
    {
        return $this->domainMovieSymbolsTo;
    }

    public function setDomainMovieShowToCustomer($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieShowToCustomer = $b;
    }

    public function getDomainMovieShowToCustomer()
    {
        return $this->domainMovieShowToCustomer;
    }

    public function getDomainMovieCustomerArchived()
    {
        return $this->domainMovieCustomerArchived;
    }

    public function setDomainMovieCustomerArchived($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieCustomerArchived = $b;
    }

    public function getDomainMovieWriting()
    {
        return $this->domainMovieWriting;
    }

    public function setDomainMovieWriting($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieWriting = $b;
    }

    public function getExcludeDescriptionWriters()
    {
        return $this->excludeDescriptionWriters;
    }

    public function setExcludeDescriptionWriters($descriptionWriters = [])
    {
        $this->excludeDescriptionWriters = $descriptionWriters;
    }

    public function setWithDomainMovieCustomerComment($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->withDomainMovieCustomerComment = $b;
    }

    public function getWithDomainMovieCustomerComment()
    {
        return $this->withDomainMovieCustomerComment;
    }

    public function getKinopoiskId()
    {
        return $this->kinopoiskId;
    }

    public function setKinopoiskId($id = null)
    {
        $this->kinopoiskId = $id;
    }

    public function getDomainMovieDeleted()
    {
        return $this->domainMovieDeleted;
    }

    public function setDomainMovieDeleted($b = null)
    {
        $this->domainMovieDeleted = $b;
    }

    public function getWithLegalPlayer()
    {
        return $this->withLegalPlayer;
    }

    public function setWithLegalPlayer($b = null)
    {
        $this->withLegalPlayer = $b;
    }

    public function getTitleRu()
    {
        return $this->titleRu;
    }

    public function setTitleRu($titleRu = null)
    {
        $this->titleRu = $titleRu;
    }

    public function getDomainMoviePageDeleted()
    {
        return $this->domainMoviePageDeleted;
    }

    public function setDomainMoviePageDeleted($b = null)
    {
        return $this->domainMoviePageDeleted = $b;
    }

    public function getExcludeDomainMoviesForWorker()
    {
        return $this->excludeDomainMoviesForWorker;
    }

    public function setExcludeDomainMoviesForWorker($login = null)
    {
        $this->excludeDomainMoviesForWorker = $login;
    }

    public function getYearFrom()
    {
        return $this->yearFrom;
    }

    public function getYearTo()
    {
        return $this->yearTo;
    }

    public function setYearTo($year = null)
    {
        $this->yearTo = $year;
    }

    public function setYearFrom($year = null)
    {
        $this->yearFrom = $year;
    }

    public function getAddDateFrom()
    {
        return $this->addDateFrom;
    }

    public function setAddDateFrom($date = null)
    {
        $this->addDateFrom = $date;
    }

    public function getAddDateTo()
    {
        return $this->addDateTo;
    }

    public function setAddDateTo($date = null)
    {
        $this->addDateTo = $date;
    }

    public function getImdbId()
    {
        return $this->imdbId;
    }

    public function setImdbId($id = null)
    {
        $this->imdbId = $id;
    }

    public function setDomainMovieDescriptionLengthTo($length = null)
    {
        $this->domainMovieDescriptionLengthTo = $length;
    }

    public function getDomainMovieDescriptionLengthTo()
    {
        return $this->domainMovieDescriptionLengthTo;
    }

    public function setDomainMovieDescriptionLengthFrom($length = null)
    {
        $this->domainMovieDescriptionLengthFrom = $length;
    }

    public function getDomainMovieDescriptionLengthFrom()
    {
        return $this->domainMovieDescriptionLengthFrom;
    }

    public function getMinYear()
    {
        $arr = F::query_assoc('select min(year) as year from '.F::typetab('cinema').';');

        return $arr['year'];
    }

    public function getMaxYear()
    {
        $arr = F::query_assoc('select max(year) as year from '.F::typetab('cinema').';');

        return $arr['year'];
    }

    public function setRatingFrom($rating = null)
    {
        $this->ratingFrom = $rating;
    }

    public function setRatingTo($rating = null)
    {
        $this->ratingTo = $rating;
    }

    public function getRatingFrom()
    {
        return $this->ratingFrom;
    }

    public function getRatingTo()
    {
        return $this->ratingTo;
    }

    public function setDomainMovieChecked($b = null)
    {
        if (! is_null($b)) {
            $this->setUseDomainMovies(true);
        }
        $this->domainMovieChecked = $b;
    }

    public function getDomainMovieChecked()
    {
        return $this->domainMovieChecked;
    }

    public function getSearchByCustomerComment()
    {
        return $this->searchByCustomerComment;
    }

    public function setSearchByCustomerComment($b = null)
    {
        $this->searchByCustomerComment = $b;
    }

    public function getExcludeExpressMovies()
    {
        return $this->excludeExpressMovies;
    }

    public function setExcludeExpressMovies($b = null)
    {
        $this->excludeExpressMovies = $b;
    }

    public static function getNotInBaseId()
    {
        $movies = new Movies;
        $movies->setUniqueId('not_in_base');
        $arr = $movies->get();
        if (isset($arr[0]['id'])) {
            $movieId = $arr[0]['id'];
        } else {
            $movieId = null;
        }

        return $movieId;
    }

    public function getExcludeAdult()
    {
        return $this->excludeAdult;
    }

    public function setExcludeAdult($b = null)
    {
        $this->excludeAdult = $b;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang = null)
    {
        $this->lang = $lang;
    }

    public function getCalcSeasonsEpisodesData()
    {
        return $this->calcSeasonsEpisodesData;
    }

    public function setCalcSeasonsEpisodesData($b = null)
    {
        $this->calcSeasonsEpisodesData = $b;
    }

    public function setFulltextSearchByTitle($query = null)
    {
        if ($query) {
            /* удаляем спец символы, иначе ошибка, баг в mysql с boolean mode */
            $query = preg_replace('/[^\p{L}\p{N}\s]/u', '', trim($query));
        }
        $this->fulltextSearchByTitle = $query;
    }

    public function getFulltextSearchByTitle()
    {
        return $this->fulltextSearchByTitle;
    }
}
