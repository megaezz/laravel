<?php

namespace cinema\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class MovieSeasons extends \engine\app\models\Lists
{
    private $movie = null;

    private $seasonsListTemplate = null;

    public function getSeasonsEpisodes()
    {
        $sql = $this->getSql();
        $sql['movie_id'] = $this->getMovie()->getId() ? ('movie_episodes_data.movie_id = \''.F::checkstr($this->getMovie()->getId()).'\'') : F::error('setMovieId to use getEpisodes');
        $sql['season'] = $this->getSeason() ? ('movie_episodes_data.season = \''.F::checkstr($this->getSeason()).'\'') : 1;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS movie_episodes_data.season, movie_episodes_data.episode
			from '.F::typetab('movie_episodes_data').'
			where
			'.$sql['season'].' and
			'.$sql['movie_id'].'
			'.$sql['order'].'
			'.$sql['limit'].'
		;');
        $this->setRows(F::rows_without_limit());

        return $arr;
    }

    public function getSeasons()
    {
        $sql = $this->getSql();
        $sql['movie_id'] = $this->getMovie()->getId() ? ('movie_episodes_data.movie_id = \''.F::checkstr($this->getMovie()->getId()).'\'') : F::error('setMovieId to use getSeasons');
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS movie_episodes_data.season
			from '.F::typetab('movie_episodes_data').'
			where
			'.$sql['movie_id'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			group by movie_episodes_data.season
		;');
        $this->setRows(F::rows_without_limit());

        return $arr;
    }

    public function getSeasonsList()
    {
        $arr = $this->getSeasons();
        $list = '';
        if ($this->getMovie()->getDomain()) {
            $domain = new Domain($this->getMovie()->getDomain());
            $t->v('movieWatchLink', $domain->getWatchLink($this->getMovie()));
        }
        foreach ($arr as $v) {
            $t = new Template($this->getSeasonsListTemplate());
            $t->v('season', $v['season']);
            $t->v('movieTitleRu', $this->getMovie()->getTitleRu());
            $t->v('movieTitleEn', $this->getMovie()->getTitleEn());
            $t->v('watchLink', $this->domain->getWatchLink($movie));
            $t->v('currentSeason', ($this->getActiveSeason() == $v['season']) ? true : false);
            $list .= $t->get();
        }

        return $arr;
    }

    public function getMovie()
    {
        return $this->movie;
    }

    public function setMovie($movie = null)
    {
        $this->movie = $movie;
    }

    public function getSeason()
    {
        return $this->season;
    }

    public function setSeason($sesaon = null)
    {
        $this->season = $season;
    }

    public function getSeasonsListTemplate()
    {
        return $this->seasonsListTemplate;
    }

    public function setSeasonsListTemplate($template = null)
    {
        $this->seasonsListTemplate = $template;
    }
}
