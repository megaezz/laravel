<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Task extends DomainMovie
{
    protected $table = 'domain_movies';

    protected static function booted()
    {
        static::addGlobalScope('withCustomer', function (Builder $builder) {
            $builder->whereNotNull('customer');
        });
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'customer', 'login');
    }

    public function taskType(): Attribute
    {
        return Attribute::make(
            get: function () {
                if ($this->movie->mw_unique_id == 'not_in_base') {
                    return 'other';
                }
                if (in_array($this->movie->type, ['movie', 'serial', 'trailer', 'game'])) {
                    return 'movie';
                }

                return $this->movie->type;
            }
        )->shouldCache();
    }

    public function scopeExpress($query = null)
    {
        return $query
            ->where('description_price_per_1k', '>', Config::cached()->rewrite_price_per_1k)
            ->where('description_price_per_1k', '>', Config::cached()->high_rewrite_price_per_1k);
    }

    public function isExpress(): Attribute
    {
        return Attribute::make(
            get: function () {
                return ($this->description_price_per_1k > Config::cached()->rewrite_price_per_1k) and ($this->description_price_per_1k > Config::cached()->high_rewrite_price_per_1k);
            }
        )->shouldCache();
    }

    public function setExpress(bool $bool)
    {
        if ($bool) {
            $this->description_price_per_1k = Config::cached()->rewrite_price_per_1k + Config::cached()->rewriteExpressFare;
            /* TODO заюзал megaweb из-за лени */
            $this->customer_price_per_1k = $this->createdBy->megaweb->getCustomerExpressPricePer1k();
        } else {
            $this->description_price_per_1k = Config::cached()->rewrite_price_per_1k;
            /* TODO заюзал megaweb из-за лени */
            $this->customer_price_per_1k = $this->createdBy->megaweb->getCustomerPricePer1k();
        }
    }

    public function isBooked(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->booked_for_login and ($this->booked_for_login !== 'ozerin');
            }
        )->shouldCache();
    }

    public function scopeBooked($query)
    {
        return $query->confirmed()->where('booked_for_login', '!=', 'ozerin');
    }

    public function scopeNotBooked($query)
    {
        return $query->confirmed()->where('booked_for_login', 'ozerin');
    }

    public function setConfirm(bool $bool)
    {
        if ($bool) {
            $this->customer_confirm_date = now();
            $this->booked_for_login = 'ozerin';
        } else {
            $this->customer_confirm_date = null;
            $this->booked_for_login = null;
        }
    }

    public function isConfirmed(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->booked_for_login ? true : false;
            }
        )->shouldCache();
    }

    public function scopeConfirmed($query)
    {
        return $query->whereNotNull('booked_for_login');
    }

    public function scopeNotConfirmed($query)
    {
        return $query->whereNull('booked_for_login');
    }

    public function isDone(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->description ? true : false;
            }
        )->shouldCache();
    }

    public function scopeDone($query = null)
    {
        return $query->whereNotNull('description');
    }

    public function scopeNotDone($query = null)
    {
        return $query->whereNull('description');
    }

    public function duplicate()
    {

        $task = $this->createdBy->addTask($this->movie);
        $task->symbols_from = $this->symbols_from;
        $task->symbols_to = $this->symbols_to;
        $task->customer_comment = $this->customer_comment;
        $task->setExpress($this->is_express);
        $task->save();

        return $task;
    }

    /* TODO: лень */

    // function expectedPrice(): Attribute {
    // 	return Attribute::make(
    //         get: function() {
    //         	return round($this->getExpectedSymbols() / 1000 * $this->getDomainMovieCustomerPricePer1k(), 1);
    //         }
    //     )->shouldCache();
    // }

    // function expectedSymbols() {
    // 	$from = $this->getDomainMovieSymbolsFrom();
    // 	$to = $this->getDomainMovieSymbolsTo();
    // 	// F::dump('ok');
    // 	return Cinema::getExpectedSymbols($from,$to);
    // }
}
