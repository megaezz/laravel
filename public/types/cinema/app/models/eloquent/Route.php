<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Route extends Model
{
    use HasDatabasePrefix;

    protected $primaryKey = 'pattern';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $database = 'cinema';
}
