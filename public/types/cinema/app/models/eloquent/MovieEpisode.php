<?php

namespace cinema\app\models\eloquent;

use App\Models\Cinema\MovieUpload;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class MovieEpisode extends Model
{
    use HasDatabasePrefix;
    use QueryCacheable;

    protected $database = 'cinema';

    protected $table = 'movie_episodes_data';

    protected $casts = [
        'air_date' => 'datetime',
    ];

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function videodbs()
    {
        return $this->hasMany(Videodb::class);
    }

    public function uploads()
    {
        return $this->hasMany(MovieUpload::class);
    }

    /* берем первый соответствующий эпизод из videodb */
    public function videodb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->videodbs()->first();
            }
        )->shouldCache();
    }

    public function previous()
    {
        return $this->hasOne(MovieEpisode::class, 'movie_id', 'movie_id')
            ->where('id', '!=', $this->id)
            ->where('season', '<=', $this->season)
            ->where('episode', '<=', $this->episode)
            ->orderByDesc('season')
            ->orderByDesc('episode')
            ->limit(1);
    }

    public function next()
    {
        return $this->hasOne(MovieEpisode::class, 'movie_id', 'movie_id')
            ->where('id', '!=', $this->id)
            ->where('season', '>=', $this->season)
            ->where('episode', '>=', $this->episode)
            ->orderBy('season')
            ->orderBy('episode')
            ->limit(1);
    }

    public function viewRoute(): Attribute
    {
        return Attribute::make(
            get: function () {
                /* оборачиваю в urlencode, т.к. без этого laravel сам оборачивает во что-то похожее, но случаются 2 процента подряд %% и ссылка не валидная */
                return route('episode_view', ['id' => $this->id, 'slug' => urlencode($this->movie->title_ru)], false);
            }
        )->shouldCache();
    }

    public function viewRouteAbsolute(): Attribute
    {
        return Attribute::make(
            get: function () {
                /* оборачиваю в urlencode, т.к. без этого laravel сам оборачивает во что-то похожее, но случаются 2 процента подряд %% и ссылка не валидная */
                return route('episode_view', ['id' => $this->id, 'slug' => urlencode($this->movie->title_ru)], true);
            }
        )->shouldCache();
    }

    public function seasonEpisode(): Attribute
    {
        return Attribute::make(
            get: function () {
                return ($this->season ? ($this->season . ' сезон ') : '') . $this->episode . ' серия';
            }
        )->shouldCache();
    }

    public function se(): Attribute
    {
        return Attribute::make(
            get: function () {
                return "S{$this->season}E{$this->episode}";
            }
        )->shouldCache();
    }

    public function myshows(): Attribute
    {
        return Attribute::make(
            get: function () {
                return collect($this->movie->myshows->data->episodes)->where('seasonNumber', $this->season)->where('episodeNumber', $this->episode)->first();
            }
        )->shouldCache();
    }
}
