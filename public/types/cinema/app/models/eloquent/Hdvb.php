<?php

namespace cinema\app\models\eloquent;

use cinema\app\models\Videodb;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

/* Здесь какого-то черта Hdvb::updateOrCreate записывал пустой token в случае create (со старым vendor, с новым норм). Вылечилось тем, что заменил свой Model на дефолтный. Каким-то образом оно влияет. */

class Hdvb extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $database = 'cinema';

    protected $primaryKey = 'token';

    protected $keyType = 'string';

    protected $casts = [
        'data' => 'object',
    ];

    protected $fillable = [
        'token',
        'movie_id',
        'data',
    ];

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    // function getData() {
    // 	return json_decode($this->data);
    // }

    public function getPlayer($country = null)
    {
        $blocked = in_array($country, empty($this->data->player->block) ? [] : $this->data->player->block);
        if ($blocked) {
            return null;
        } else {
            /* подмена домена, т.к. эти придурки до сих пор отдают по апи нерабочий домен */
            $this->data->player->iframe_url = str_replace(['vb24132nightdwellers.com', 'azure133sitsarl.com'], 'clement134quo.com', $this->data->player->iframe_url ?? null);

            return empty($this->data->player->iframe_url) ? null : ("{$this->data->player->iframe_url}?d=awmzone1.pro&p=https://".Videodb::getIframeDomain().'/types/cinema/template/images/preview.jpg');
        }
    }
}
