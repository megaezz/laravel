<?php

namespace cinema\app\models\eloquent;

use cinema\app\models\User as UserMegaweb;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class User extends Model
{
    use HasDatabasePrefix;

    protected $database = 'cinema';

    public $timestamps = false;

    protected $casts = [
        'checkIsMovieInDoneTasks' => 'boolean',
        'customerAutoExpress' => 'boolean',
        'symbols_from' => 'integer',
        'symbols_to' => 'integer',
    ];

    public function transfers()
    {
        return $this->hasMany(Transfer::class, 'login', 'login');
    }

    public function wallets(): Attribute
    {
        return Attribute::make(
            get: function () {
                $wallets[] = [
                    /*'unitpayType' => 'webmoney',
                    'fkWallet' => false,*/
                    'wallet' => $this->wmr,
                    'currency' => 'RUB',
                    'link' => 'https://light.webmoney.ru/v3#/v3/WMTrans',
                    'serviceName' => 'Webmoney WMR',
                    'type' => 'wmr',
                    'available' => false,
                ];
                $wallets[] = [
                    /*'unitpayType' => 'webmoney',
                    'fkWallet' => false,*/
                    /*'fkWallet' => [
                     'id' => 2,
                     'minRub' => 10,
                     'fee' => 7
                    ],*/
                    'wallet' => $this->wmz,
                    'currency' => 'USD',
                    'link' => 'https://light.webmoney.ru/v3#/v3/WMTrans',
                    'serviceName' => 'Webmoney WMZ',
                    'type' => 'wmz',
                    'available' => true,
                ];
                $wallets[] = [
                    /*'unitpayType' => 'yandex',
                    'fkWallet' => false,*/
                    /*'fkWallet' => [
                     'id' => 45,
                     'minRub' => 10,
                     'fee' => 3
                    ],*/
                    'wallet' => $this->yandex,
                    'currency' => 'RUB',
                    'link' => 'https://money.yandex.ru/transfer',
                    'serviceName' => 'ЮMoney',
                    'type' => 'yandex',
                    'available' => true,
                ];
                $wallets[] = [
                    /*'unitpayType' => null,
                    'fkWallet' => false,*/
                    /*'fkWallet' => [
                     'id' => 150,
                     'minRub' => 10,
                     'fee' => 5
                    ],*/
                    'wallet' => $this->advcash,
                    'currency' => 'RUB',
                    'link' => 'https://wallet.advcash.com/pages/transfer/wallet',
                    'serviceName' => 'Volet (Advcash) RUB',
                    'type' => 'advcash',
                    'available' => true,
                ];
                $wallets[] = [
                    /*'unitpayType' => null,
                    'fkWallet' => false,*/
                    'wallet' => $this->sberbank,
                    'currency' => 'RUB',
                    'link' => 'https://node2.online.sberbank.ru/PhizIC/private/payments/payment.do?form=RurPayment&receiverSubType=ourCard',
                    'serviceName' => 'Сбербанк',
                    'type' => 'sberbank',
                    'available' => false,
                ];
                $wallets[] = [
                    /*'unitpayType' => 'sbp',
                    'fkWallet' => false,*/
                    /*'fkWallet' => [
                     'id' => 63,
                     'minRub' => 50,
                     'fee' => 4
                    ],*/
                    'wallet' => $this->sbp,
                    'currency' => 'RUB',
                    'link' => null,
                    'serviceName' => 'СБП (номер телефона с +7)',
                    'type' => 'sbp',
                    'available' => true,
                ];
                $wallets[] = [
                    'wallet' => $this->usdtbep20,
                    'currency' => 'USD',
                    'link' => null,
                    'serviceName' => 'USDT BEP20',
                    'type' => 'usdtbep20',
                    'available' => true,
                ];

                return collect($wallets);
            }
        );
    }

    public function wallet(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->wallets->where('available', true)->whereNotNull('wallet')->first();
            }
        )->shouldCache();
    }

    public function megaweb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new UserMegaweb(null, $this);
            }
        )->shouldCache();
    }

    public function createdTasks()
    {
        return $this->hasMany(Task::class, 'customer', 'login');
    }

    public function rewritedTasks()
    {
        return $this->hasMany(Task::class, 'description_writer', 'login');
    }

    public function bookedTasks()
    {
        return $this->hasMany(Task::class, 'booked_for_login', 'login');
    }

    public function addTask(Movie $movie)
    {
        $max = 1000;

        if ($this->createdTasks()->notDone()->count() >= $max) {
            throw new \Exception("Максимум {$max} заданий в очереди");
        }

        $task = new Task;
        $task->movie_id = $movie->id;
        /* домен тут нужен для того чтобы избежать ошибки при сохранении, которая возникает в событии saving и связана с присутствием одноименного отношения domain() */
        $task->domain = null;
        $task->customer = $this->login;
        $task->save();
        /* refresh для megaweb */
        $task->refresh();
        /* TODO: лень было */
        $task->megaweb->setCustomerSettings($this->login);
        $task->megaweb->save();
        $task->refresh();

        return $task;
    }
}
