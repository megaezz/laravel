<?php

namespace cinema\app\models\eloquent;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class DomainGroup extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;

    protected $database = 'cinema';

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function cinemaDomain()
    {
        return $this->belongsTo(Domain::class, 'domain');
    }
}
