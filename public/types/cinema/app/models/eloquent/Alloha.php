<?php

namespace cinema\app\models\eloquent;

use App\View\Components\types\cinema\players\Alloha as AllohaPlayer;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Alloha extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $database = 'cinema';

    protected $primaryKey = 'token_movie';

    protected $keyType = 'string';

    protected $table = 'alloha';

    protected $casts = [
        'data' => 'object',
    ];

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function player(): Attribute
    {
        return Attribute::make(
            get: function () {
                return (new AllohaPlayer($this))->render();
            }
        )->shouldCache();
    }
}
