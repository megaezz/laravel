<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Builder;

class Moderator extends User
{
    protected $table = 'users';

    protected static function booted()
    {
        static::addGlobalScope('moderator', function (Builder $builder) {
            $builder->whereIn('level', ['moderator', 'admin']);
        });
    }
}
