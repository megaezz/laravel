<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Builder;

class Moderator extends User
{
    protected $table = 'users';

    protected static function booted()
    {
        static::addGlobalScope('customer', function (Builder $builder) {
            $builder->whereIn('level', ['customer', 'admin-lite', 'admin']);
        });
    }
}
