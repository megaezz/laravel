<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Builder;

class Moderator extends User
{
    protected $table = 'users';

    protected static function booted()
    {
        static::addGlobalScope('worker', function (Builder $builder) {
            $builder->whereIn('level', ['worker', 'moderator', 'admin']);
        });
    }

    public function availableTasks()
    {
        // считаем сколько заданий в базе
        $movies = new Movies;
        $movies->setUseDomainMovies(true);
        $movies->setBookedForLogin('ozerin');
        $movies->setWithDomainDescription(false);
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        $movies->setUseDomainMovieCustomers(true);
        $movies->setExcludeDomainMoviesForWorker($this->user->getLogin());
        $movies->setExcludeExpressMovies(! $this->user->getExpressRewriter());
        $movies->setExcludeAdult(! $this->user->getAdult());

        return Task::notBooked();
    }
}
