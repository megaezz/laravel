<?php

namespace cinema\app\models\eloquent;

use cinema\app\models\Videodb as VideodbMegaweb;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Videodb extends Model
{
    use HasDatabasePrefix;

    public $incrementing = false;

    protected $database = 'cinema';

    protected $table = 'videodb';

    protected $casts = [
        'data' => 'object',
        'checked' => 'boolean',
        'is_exists_on_date' => 'datetime',
    ];

    const CREATED_AT = 'date';

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    /* все записи videodb для данного эпизода */
    public function videodbs()
    {
        return $this->hasMany(Videodb::class, 'movie_id', 'movie_id')
            ->where('season', $this->season)
            ->where('episode', $this->episode);
    }

    public function movieEpisode()
    {
        return $this->belongsTo(MovieEpisode::class);
    }

    /* дубляж в таблице translations */
    public function dubbing()
    {
        return $this->belongsTo(Dubbing::class, 'translation', 'videodb_name');
    }

    /* проверяет есть ли человеческое название дубляжа, если нет то берет из videodb */
    public function dubbingName(): Attribute
    {
        return Attribute::make(
            get: function () {
                return empty($this->dubbing?->name)
                    ? ($this->translation ?? 'Неизвестный')
                    : $this->dubbing->name;
            }
        )->shouldCache();
    }

    /* TODO: можно наверное его перенести сразу в dubbings, вряд ли этот метод отдельно будет использоваться */
    /* уникальные дубляжи выборки */
    public function scopeDubbings($query)
    {
        return $query->select('translation')->distinct()->with('dubbing');
    }

    /* уникальные дубляжи данного эпизода */
    public function dubbings(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->videodbs()->dubbings()->get();
            }
        )->shouldCache();
    }

    public function megaweb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new VideodbMegaweb(null, $this);
            }
        )->shouldCache();
    }

    public function dataKinopoiskId(): Attribute
    {
        return Attribute::make(
            get: function () {
                $series_id = $this->data->content_object->tv_series->kinopoisk_id ?? null;
                $episode_id = $this->data->content_object->kinopoisk_id ?? null;
                $kinopoisk_id = $series_id ? $series_id : $episode_id;

                return $kinopoisk_id;
            }
        )->shouldCache();
    }

    public function dataImdbId(): Attribute
    {
        return Attribute::make(
            get: function () {
                $series_id = $this->data->content_object->tv_series->imdb_id ?? null;
                $episode_id = $this->data->content_object->imdb_id ?? null;
                $imdb_id = $series_id ? $series_id : $episode_id;

                return str_replace('tt', '', ($imdb_id ?? ''));
            }
        )->shouldCache();
    }

    public function dataTitleRu(): Attribute
    {
        return Attribute::make(
            get: function () {
                $series_title = $this->data->content_object->tv_series->ru_title ?? null;
                $episode_title = $this->data->content_object->ru_title ?? null;
                $title = $series_title ? $series_title : $episode_title;

                return $title;
            }
        )->shouldCache();
    }

    public function dataTitleEn(): Attribute
    {
        return Attribute::make(
            get: function () {
                $series_title = $this->data->content_object->tv_series->orig_title ?? null;
                $episode_title = $this->data->content_object->orig_title ?? null;
                $title = $series_title ? $series_title : $episode_title;

                return $title;
            }
        )->shouldCache();
    }

    public function type(): Attribute
    {
        return Attribute::make(
            get: function () {
                return isset($this->data->content_object->tv_series) ? 'serial' : 'movie';
            }
        )->shouldCache();
    }

    // TODO: этот метод из megaweb модели Videodb переписать тут под eloquent Videodb и заменить в playerJsData
    public function playlist(): Attribute
    {
        return Attribute::make(
            get: function () {
                if (! $this->movie_id) {
                    return false;
                }

                // получаем все эпизоды и озвучки
                $dubs = Videodb::query()
                    ->selectRaw('*, cast(season as unsigned) as season_int, cast(episode as unsigned) as episode_int')
                    ->leftJoin('cinema.translations', 'translations.videodb_name', 'videodb.translation')
                    ->where('movie_id', $this->movie_id)
                    ->orderBy('season_int')
                    ->orderBy('episode_int')
                    ->orderByDesc('translations.order')
                    ->orderBy('videodb.translation')
                    ->get();

                // dd($dubs);

                // подтягиваем загрузки если есть
                $movieUploads = $this->movie
                    ->uploads()
                    ->with('episode')
                    ->with('dubbing')
                    ->where('hls_exists', true)
                    ->get();

                $seasons = [];
                foreach ($dubs as $dub) {
                    $seasons[$dub->season_int][$dub->episode_int][$dub->translation] = $dub->data;
                }

                $seasonsArr = [];
                foreach ($seasons as $season => $episodes) {
                    $seasonObj = new \stdClass;
                    $seasonObj->title = "Сезон {$season}";
                    $episodesArr = [];
                    foreach ($episodes as $episode => $translations) {
                        $episodeObj = new \stdClass;
                        $episodeObj->title = "Серия {$episode}";
                        // текущий сезон и эпизод, чтобы получать в плеере и формировать название
                        $episodeObj->id = new \stdClass;
                        $episodeObj->id->season = $season;
                        $episodeObj->id->episode = $episode;
                        $sourcesList = '';
                        foreach ($translations as $translation => $data) {
                            // $sourcesList .= '{'.$translation.'}'."{$this->megaweb->getSignedUrl($data->path, 'ip')}hls.m3u8;";
                            $signedUrl = Config::cached()->zerocdnService->getSignedUrl($data->hls, 'ip');
                            $sourcesList .= "{{$translation}}{$signedUrl};";
                            // dd($sourcesList);
                        }

                        if ($season === '' and $episode === '') {
                            $foundUploads = $movieUploads->whereNull('episode')->whereNull('season');
                        } else {
                            $foundUploads = $movieUploads->where('episode.season', $season)->where('episode.episode', $episode);
                        }

                        // TODO: временный колхозинг для турбо
                        if (in_array($this->movie_id, [281933])) {
                            $foundUploads = $foundUploads->sortByDesc(fn ($movieUpload) => $movieUpload->dubbing->turbo_order);
                        }

                        // если для эпизода есть загрузки, то добавляем их в начало плейлиста-глиста
                        $sourcesList = $foundUploads->map(function ($movieUpload) {
                            $url = Storage::disk('storage')->url($movieUpload->hls);

                            return "{{$movieUpload->dubbing->short_or_full_name}}{$url};";
                        })->implode('').$sourcesList;

                        $episodeObj->file = VideodbMegaweb::pjsBase64Encrypt($sourcesList);
                        $episodesArr[] = $episodeObj;
                    }
                    $seasonObj->folder = $episodesArr;
                    $seasonsArr[] = $seasonObj;
                }

                return $seasonsArr;
            }
        )->shouldCache();
    }

    public function playerJsData(): Attribute
    {
        return Attribute::make(
            get: function () {
                $data = new \stdClass;
                $data->movieTitle = $this->movie->title;
                $data->movieYear = $this->movie->year;
                $data->movieType = $this->movie->localed_type;
                // $data->playlist = $this->megaweb->getPlaylist();
                $data->playlist = $this->playlist;

                return $data;
            }
        )->shouldCache();
    }

    /*  дописать плеер */
    // function player(): Attribute {
    //     return Attribute::make(
    //         get: function() {
    //             return view('');
    //         }
    //     )->shouldCache();

    // }
}
