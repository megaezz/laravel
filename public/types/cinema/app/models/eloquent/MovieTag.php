<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class MovieTag extends Model
{
    use HasDatabasePrefix;

    public $incrementing = false;

    protected $database = 'cinema';

    protected $table = 'cinema_tags';

    public function movie()
    {
        return $this->belongsTo(Movie::class, 'cinema_id');
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
