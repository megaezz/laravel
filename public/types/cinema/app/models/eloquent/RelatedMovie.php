<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class RelatedMovie extends Model
{
    use HasDatabasePrefix;
    use QueryCacheable;

    /* чтобы upsert в calc-related-movies не обновлял updated_at при каждом расчете похожих, иначе все жутко тормозит */
    const UPDATED_AT = null;

    protected $database = 'cinema';

    protected $fillable = [
        'related_movie_id',
        'q',
    ];

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function relatedMovie()
    {
        return $this->belongsTo(Movie::class);
    }
}
