<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Kinopoisk extends Model
{
    use HasDatabasePrefix;

    protected $database = 'cinema';

    protected $table = 'kinopoisk';

    protected $casts = [
        'data' => 'json',
        'videos' => 'object',
        'facts' => 'object',
    ];

    protected $fillable = ['id'];

    const UPDATED_AT = 'update_time';

    public function mainTrailer(): Attribute
    {
        return Attribute::make(
            get: function () {
                return collect($this->videos?->items)->where('site', 'YOUTUBE')->first();
            }
        )->shouldCache();
    }

    public function changeYoutubeUrlToEmbed($url)
    {
        $toChange = [
            'https://www.youtube.com/v/',
            'http://www.youtube.com/v/',
            'https://youtu.be/',
            'http://youtu.be/',
            'https://www.youtube.com/watch?v=',
            'http://www.youtube.com/watch?v=',
        ];

        return str_replace($toChange, 'https://www.youtube.com/embed/', $url ?? '');
    }

    public function premiereWorld(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->data['premiereWorld'] ? Carbon::parse($this->data['premiereWorld']) : null;
            }
        )->shouldCache();
    }
}
