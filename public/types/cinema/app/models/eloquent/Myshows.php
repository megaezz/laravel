<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Megaezz\LaravelEloquentLocalizable\Localizable;

class Myshows extends Model
{
    use HasDatabasePrefix;
    use Localizable;

    protected $database = 'cinema';

    protected $casts = [
        'data' => 'object',
        'data_en' => 'object',
        'data_ua' => 'object',
        'data_ru' => 'object',
    ];

    protected $localizable = ['data'];

    public function getEpisodesOfSeason($season_number = null)
    {
        return array_filter($this->data->episodes, function ($episode) use ($season_number) {
            return $episode->seasonNumber == $season_number;
        });
    }

    public function getDataRuAttribute()
    {
        return json_decode($this->attributes['data']);
    }

    public function setDataRuAttribute($data_ru = null)
    {
        $this->data = $data_ru;
    }
}
