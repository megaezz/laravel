<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Tvmaze extends Model
{
    use HasDatabasePrefix;

    protected $database = 'cinema';

    protected $casts = [
        'data' => 'object',
    ];
}
