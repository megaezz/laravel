<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class MoviePlayer extends Model
{
    use HasDatabasePrefix;

    public $incrementing = false;

    protected $database = 'cinema';

    public $timestamps = false;

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }
}
