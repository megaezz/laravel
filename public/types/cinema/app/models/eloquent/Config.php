<?php

namespace cinema\app\models\eloquent;

use App\Services\Cinema\Zerocdn;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Config extends Model
{
    use HasDatabasePrefix;

    protected $database = 'cinema';

    protected $casts = [
        'tvmaze_api_credentials' => 'object',
        'myshows_api_credentials' => 'object',
        'zerocdn_credentials' => 'object',
        'videodb_credentials' => 'object',
        'turbo_tariff' => 'object',
    ];

    protected static ?Config $cached = null;

    public static function cached()
    {
        if (! static::$cached) {
            static::$cached = Config::first();
        }

        return static::$cached;
    }

    public function priorityDubbingOverride()
    {
        return $this->belongsTo(Dubbing::class, 'priority_dubbing_override_id');
    }

    public function fallbackPriorityDubbing()
    {
        return $this->belongsTo(Dubbing::class, 'fallback_priority_dubbing_id');
    }

    public function zerocdnService(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new Zerocdn(
                    login: $this->zerocdn_credentials->login,
                    apiSecret: $this->zerocdn_credentials->apiSecret,
                    cdnDomain: $this->zerocdn_credentials->cdnDomain
                );
            }
        )->shouldCache();
    }
}
