<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Cron extends Model
{
    use HasDatabasePrefix;

    protected $primaryKey = 'action';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $database = 'cinema';

    protected $casts = [
        'finished_at' => 'datetime',
    ];
}
