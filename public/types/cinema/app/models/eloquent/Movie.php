<?php

namespace cinema\app\models\eloquent;

use App\Models\Cinema\MovieUpload;
use App\Models\Cinema\Torrent;
use App\Traits\LogsActivity;
use cinema\app\models\Movie as MovieMegaweb;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Megaezz\LaravelEloquentLocalizable\Localizable;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Movie extends Model
{
    use HasDatabasePrefix;
    use Localizable;
    use LogsActivity;
    use QueryCacheable;

    protected $database = 'cinema';

    protected $table = 'cinema';

    protected $localizable = ['title', 'localed_type', 'localed_status'];

    protected $casts = [
        'videodb' => 'boolean',
        'checked' => 'boolean',
        'started' => 'datetime',
        'ended' => 'datetime',
    ];

    const CREATED_AT = 'add_date';

    public static function getByKinopoiskId($kinopoisk_id = null)
    {
        return $kinopoisk_id ? static::where('kinopoisk_id', $kinopoisk_id)->first() : null;
    }

    public function collapses()
    {
        return $this->hasMany(Collaps::class);
    }

    /* оставить только hdvbs */
    public function hdvb()
    {
        return $this->hasMany(Hdvb::class);
    }

    public function hdvbs()
    {
        return $this->hasMany(Hdvb::class);
    }

    public function videodbs()
    {
        return $this->hasMany(Videodb::class);
    }

    /* метод переименован в videodbs, оставлен для совместимости */
    public function videodb()
    {
        return $this->videodbs();
    }

    /* оставить только allohas */
    public function alloha()
    {
        return $this->hasMany(Alloha::class);
    }

    public function allohas()
    {
        return $this->hasMany(Alloha::class);
    }

    /* все-таки здесь должно быть отношение не hasManyThrough, а belongsToMany - многие ко многим. многие movies ко многим tags, через промежуточную cinema_tags */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'cinema.cinema_tags', 'cinema_id');
        // return $this->hasManyThrough(Tag::class,MovieTag::class,'cinema_id','id',null,'tag_id');
    }

    public function movieTags()
    {
        return $this->hasMany(MovieTag::class, 'cinema_id');
    }

    public function domainMovies()
    {
        return $this->hasMany(DomainMovie::class);
    }

    public function myshows()
    {
        return $this->belongsTo(Myshows::class);
    }

    public function kinopoisk()
    {
        return $this->belongsTo(Kinopoisk::class);
    }

    public function players()
    {
        return $this->hasMany(MoviePlayer::class);
    }

    public function episodes()
    {
        return $this->hasMany(MovieEpisode::class);
    }

    public function torrents()
    {
        return $this->hasMany(Torrent::class);
    }

    public function relatedMovies()
    {
        return $this->hasMany(RelatedMovie::class);
    }

    /* приходится прописывать связующую таблицу вручную, т.к. laravel автоматически обращается к laravel.group_movie */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'cinema.group_movie');
    }

    public function uploads()
    {
        return $this->hasMany(MovieUpload::class);
    }

    /* TODO: если нигде не используется, то удалить нахер */
    public function scopeStrictRelated($query, $tags = [])
    {
        if (empty($tags)) {
            return $query;
        }
        foreach ($tags as $tag) {
            $query->whereRelation('tags', 'tag_id', '=', $tag);
        }
        $query
            ->orderByDesc('cinema.year')
            ->orderByDesc('cinema.rating_sko');

        return $query;
    }

    /* TODO: если нигде не используется, то удалить нахер */
    public function scopeRelated($query, $tags = [])
    {
        if (empty($tags)) {
            return $query;
        }

        return $query
            ->joinSub(
                MovieTag::selectRaw('count(*) as q, cinema_id')
                    ->whereIn('tag_id', $tags)
                    ->groupBy('cinema_id'),
                'ct', 'ct.cinema_id', '=', 'cinema.id'
            )
            ->where('cinema.deleted', false)
            ->where('cinema.poster', true)
            ->whereIn('cinema.type', ['movie', 'serial', 'trailer'])
            ->orderByDesc('ct.q')
            ->orderByDesc('cinema.year')
            ->orderByDesc('cinema.rating_sko');
    }

    /* TODO: наверное удалить, т.к. не используется, в кроне похожих видео запрос прописан полностью */
    /* тяжелый запрос для нахождения похожих видео, нужен только того, чтобы расчитывать их по крону */
    public function scopeRelatedMoviesCalculate($query)
    {
        return $query
            ->joinSub(
                MovieTag::selectRaw('count(*) as q, cinema_id')
                    ->whereIn('tag_id', $this->tags->modelKeys())
                    ->groupBy('cinema_id')
                    ->having('q', '>', 4),
                'ct', 'ct.cinema_id', '=', 'cinema.id'
            )
            ->whereIn('cinema.type', ['movie', 'serial', 'trailer'])
            ->where('id', '!=', $this->id);
    }

    public function getPosterOrPlaceholderAttribute()
    {
        return $this->poster ? '/static/types/cinema/posters/'.$this->id.'.jpg' : '/types/cinema/template/images/poster_none.png';
    }

    public function getPosterOrPlaceholderWebpAttribute()
    {
        return $this->poster ? '/static/types/cinema/posters-webp/'.$this->id.'.webp' : '/types/cinema/template/images/poster_none.webp';
    }

    public function scopeSetTypesFromDomain($query, $domain = null)
    {
        $domain = Domain::findOrFail($domain);
        $types = [];
        if ($domain->include_movies) {
            $types[] = 'movie';
        }
        if ($domain->include_serials) {
            $types[] = 'serial';
        }
        if ($domain->include_youtube) {
            $types[] = 'youtube';
        }
        if ($domain->include_trailers) {
            $types[] = 'trailer';
        }

        return $query->whereIn('type', $types);
    }

    /* TODO: перенес этот метод из DomainMovie, но пока не знаю зачем оно вообще и работает ли */
    public function getRelatedGroupsAttribute()
    {
        return Group::related($this->tags->modelKeys())->get();
    }

    public function megaweb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new MovieMegaweb(null, $this);
            }
        )->shouldCache();
    }

    public function isSng(): Attribute
    {
        return Attribute::make(
            get: function () {
                /* использую аттрибут ->tags вместо запроса ->tags(), чтобы использовались ранее предзагруженные теги */
                return $this->tags->contains('sng', true);
            }
        )->shouldCache();
    }

    /* первый эпизод */
    /* по-хорошему надо это засунуть в MovieEpisode как scopeFirstEpisode, но в этом случае нет кеширования */
    public function firstEpisode(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->episodes()->orderBy('season')->orderBy('episode')->first();
            }
        )->shouldCache();
    }

    /* последний эпизод */
    /* по-хорошему надо это засунуть в MovieEpisode как scopeLastEpisode, но в этом случае нет кеширования */
    public function lastEpisode(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->episodes()->orderByDesc('season')->orderByDesc('episode')->first();
            }
        )->shouldCache();
    }

    /* добавляем/удаляем теги foreign/sng. Если есть СНГ страны, то ставим тег СНГ, если нет, то foreign. И удаляем дубли. */
    public function applyForeignOrSngTag(?Collection $tags = null)
    {

        $changed = false;

        /* можно передать коллекцию, содержащую 2 тега: снг и foreign, чтобы экономить на запросах */
        if ($tags) {
            $foreignTag = $tags->where('tag', 'foreign')->first() ?? throw new \Exception('В коллекции не найден тег foreign');
            $sngTag = $tags->where('tag', 'СНГ')->first() ?? throw new \Exception('В коллекции не найден тег СНГ');
        } else {
            $foreignTag = Tag::getForeignTag();
            $sngTag = Tag::getSngTag();
        }
        // dump('Были теги', $this->tags->pluck('tag'));
        // dd($this->tags->refresh());
        if ($this->is_sng) {
            // dump('Фильм является СНГ');
            if ($this->tags->contains($foreignTag)) {
                // dump('Удаляем foreign');
                $this->tags()->detach($foreignTag);
                $changed = true;
            }
            if (! $this->tags->contains($sngTag)) {
                // dump('Добавляем СНГ');
                $this->tags()->attach($sngTag);
                $changed = true;
            }
        } else {
            // dump('Фильм не является СНГ');
            if (! $this->tags->contains($foreignTag)) {
                // dump('Добавляем foreign');
                $this->tags()->attach($foreignTag);
                $changed = true;
            }
            if ($this->tags->contains($sngTag)) {
                // dump('Удаляем СНГ');
                $this->tags()->detach($sngTag);
                $changed = true;
            }
        }
        if ($changed) {
            $this->load('tags');
        }

        return $changed;
        // dump('Стали теги', $this->tags->pluck('tag'));
    }

    /* локализация */

    public function statusTranslations($status, $locale)
    {
        $statuses = [
            'Canceled/Ended' => [
                'ru' => 'Отменен/Завершен',
            ],
            'Returning Series' => [
                'ru' => 'Снимается',
            ],
            'TBD/On The Bubble' => [
                'ru' => 'Под вопросом',
            ],
            'Pilot Rejected' => [
                'ru' => 'Пилот отклонен',
            ],
        ];

        return $statuses[$status][$locale] ?? $status;
    }

    public function localedStatusRu(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->statusTranslations($this->status, 'ru');
            }
        )->shouldCache();
    }

    public function localedStatusEn(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->statusTranslations($this->status, 'en');
            }
        )->shouldCache();
    }

    public function localedStatusUa(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->statusTranslations($this->status, 'ru');
            }
        )->shouldCache();
    }

    public function typeTranslations($type, $locale)
    {
        $types = [
            'movie' => [
                'en' => 'Movie',
                'ua' => 'Фiльм',
                'ru' => 'Фильм',
            ],
            'serial' => [
                'en' => 'Series',
                'ua' => 'Серiал',
                'ru' => 'Сериал',
            ],
            'trailer' => [
                'en' => 'Trailer',
                'ua' => 'Трейлер',
                'ru' => 'Трейлер',
            ],
            'youtube' => [
                'en' => 'Youtube',
                'ua' => 'Youtube',
                'ru' => 'Youtube',
            ],
        ];

        return $types[$type][$locale] ?? throw new \Exception('Отсутствует перевод для типа контента');
    }

    public function localedTypeEn(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->typeTranslations($this->type, 'en');
            }
        )->shouldCache();
    }

    public function localedTypeUa(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->typeTranslations($this->type, 'ua');
            }
        )->shouldCache();
    }

    public function localedTypeRu(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->typeTranslations($this->type, 'ru');
            }
        )->shouldCache();
    }

    /* конец локализации */
}
