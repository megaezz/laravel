<?php

namespace cinema\app\models\eloquent;

use App\Traits\LogsActivity;
use cinema\app\models\Group as GroupMegaweb;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Megaezz\LaravelEloquentLocalizable\Localizable;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Group extends Model
{
    use HasDatabasePrefix;
    use Localizable;
    use LogsActivity;
    use QueryCacheable;

    protected $database = 'cinema';

    protected $localizable = ['name'];

    /* тут отношение правильное, но выполняется супер долго, т.к. в запросе к tags джоинится groups_tags */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'cinema.groups_tags');
    }

    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'cinema.group_movie');
    }

    /* возвращает фильмы, относящиеся к данной категории, используется для заполнения связующей таблицы */
    public function calculateMovies()
    {
        $query = Movie::query();

        /* если тегов нет, значит и фильмов нет */
        if ($this->tags->isEmpty()) {
            return $query->whereRaw('1 = 0');
        }

        foreach ($this->tags as $tag) {
            $query->whereRelation('tags', 'tag_id', '=', $tag->id);
        }

        return $query;
    }

    /* это группы похожие на данную группу? ну хз, может пригодится */
    public function scopeRelated($query, $tags = [])
    {
        return $query
            ->selectRaw('groups.*')
            ->joinSub(
                GroupTag::selectRaw('count(*) as q, group_id')
                    ->whereIn('tag_id', $tags)
                    ->groupBy('group_id'),
                'gt', 'gt.group_id', '=', 'groups.id'
            )
            ->orderByDesc('gt.q');
    }

    public function megaweb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new GroupMegaweb(null, $this);
            }
        )->shouldCache();
    }

    // из-за того, что в ДБ русское название не name_ru, а name и стоит трейт Localizable
    public function getNameRuAttribute()
    {
        return $this->attributes['name'];
    }

    // из-за того, что в ДБ русское название не name_ru, а name и стоит трейт Localizable
    public function setNameRuAttribute($name_ru = null)
    {
        $this->name = $name_ru;
    }

    public function viewRoute(): Attribute
    {
        return Attribute::make(
            get: function () {
                return route('group_view', ['id' => $this->id, 'slug' => $this->name_ru], false);
            }
        )->shouldCache();
    }

    public function viewRouteTranslit(): Attribute
    {
        return Attribute::make(
            get: function () {
                return route('group_view', ['id' => $this->id, 'slug' => Str::slug($this->name_ru)], false);
            }
        )->shouldCache();
    }
}
