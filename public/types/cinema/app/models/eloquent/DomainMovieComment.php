<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DomainMovieComment extends Model
{
    use HasDatabasePrefix;
    use QueryCacheable;

    const CREATED_AT = 'date';

    const UPDATED_AT = null;

    protected $database = 'cinema';

    protected $table = 'movie_comments';

    public function domainMovie()
    {
        return $this->belongsTo(DomainMovie::class);
    }
}
