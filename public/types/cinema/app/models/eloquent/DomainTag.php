<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class DomainTag extends Model
{
    use HasDatabasePrefix;

    protected $database = 'cinema';

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public function cinemaDomain()
    {
        return $this->belongsTo(Domain::class, 'domain');
    }
}
