<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Collaps extends Model
{
    use HasDatabasePrefix;

    public $incrementing = false;

    protected $database = 'cinema';

    protected $casts = [
        'data' => 'object',
    ];

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }
}
