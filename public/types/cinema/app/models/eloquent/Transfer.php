<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Transfer extends Model
{
    use HasDatabasePrefix;

    protected $database = 'cinema';

    const CREATED_AT = 'date';

    const UPDATED_AT = null;
}
