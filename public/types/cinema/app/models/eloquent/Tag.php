<?php

namespace cinema\app\models\eloquent;

use cinema\app\models\Tag as TagMegaweb;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Megaezz\LaravelEloquentLocalizable\Localizable;
use Rennokki\QueryCache\Traits\QueryCacheable;

class Tag extends Model
{
    use HasDatabasePrefix;
    use Localizable;
    use QueryCacheable;

    protected $database = 'cinema';

    protected $localizable = ['tag'];

    protected static Tag $foreignTag;

    protected static Tag $sngTag;

    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'cinema.cinema_tags', null, 'cinema_id');
    }

    public function getTagRuAttribute()
    {
        return $this->attributes['tag'];
    }

    public function setTagRuAttribute($tag_ru = null)
    {
        $this->tag = $tag_ru;
    }

    /* выводит тег foreign и кеширует его в моделе */
    public static function getForeignTag()
    {
        static::$foreignTag = static::$foreignTag ?? static::whereTag('foreign')->first();
        if (! static::$foreignTag) {
            throw new \Exception('Не найден тег foreign');
        }

        return static::$foreignTag;
    }

    /* выводит тег tag и кеширует его в моделе */
    public static function getSngTag()
    {
        static::$sngTag = static::$sngTag ?? static::whereTag('СНГ')->first();
        if (! static::$sngTag) {
            throw new \Exception('Не найден тег СНГ');
        }

        return static::$sngTag;
    }

    public function megaweb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new TagMegaweb(null, $this);
            }
        )->shouldCache();
    }
}
