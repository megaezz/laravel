<?php

namespace cinema\app\models\eloquent;

use App\Models\Domain as EngineDomain;
use App\Services\Cinema\MovieAllowed\IsMovieAllowedForDomain;
use cinema\app\models\Domain as DomainMegaweb;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Domain extends Model
{
    use HasDatabasePrefix;

    protected $primaryKey = 'domain';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $database = 'cinema';

    protected $casts = [
        'useInRedirectToAnyMovie' => 'boolean',
        'include_serials' => 'boolean',
        'include_movies' => 'boolean',
        'include_trailers' => 'boolean',
        'include_games' => 'boolean',
        'include_youtube' => 'boolean',
        'movies_with_unique_description' => 'boolean',
        'useMovieId' => 'boolean',
        'useTagIdForGenres' => 'boolean',
        'legalMovies' => 'boolean',
        'watchCalcSameMovies' => 'boolean',
        'calcThumbGenresList' => 'boolean',
        'dmcaQuestion' => 'boolean',
        'useAltDescriptions' => 'boolean',
        'allowRussianMovies' => 'boolean',
        'rknForRuOnly' => 'boolean',
        'moviesWithPoster' => 'boolean',
        'allowRiskyStudios' => 'boolean',
        'useOnlyChoosenGroups' => 'boolean',
        'rknBanned' => 'boolean',
        'episodically' => 'boolean',
        'hide_canonical' => 'boolean',
        'index_search' => 'boolean',
        'ai_descriptions' => 'boolean',
        'index_movies_in_yandex' => 'boolean',
        'show_trailer_for_non_sng' => 'boolean',

    ];

    public function engineDomain()
    {
        return $this->belongsTo(EngineDomain::class, 'domain');
    }

    public function megaweb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new DomainMegaweb(null, $this);
            }
        )->shouldCache();
    }

    public function groups()
    {
        return $this->hasMany(DomainGroup::class, 'domain');
    }

    public function tags()
    {
        return $this->hasMany(DomainTag::class, 'domain');
    }

    public function mergedGroups()
    {
        return Group::select('groups.*', 'domain_groups.*', 'groups.id as id')
            ->leftJoin('cinema.domain_groups', function ($leftJoin) {
                $leftJoin
                    ->on('groups.id', '=', 'domain_groups.group_id')
                    ->whereDomain($this->domain);
            });
    }

    public function mergedTags()
    {
        return Tag::select('tags.*', 'domain_tags.*', 'tags.id as id')
            ->leftJoin('cinema.domain_tags', function ($leftJoin) {
                $leftJoin
                    ->on('tags.id', '=', 'domain_tags.tag_id')
                    ->whereDomain($this->domain);
            });
    }

    public function movies()
    {
        return $this->hasMany(DomainMovie::class, 'domain');
    }

    /* TODO: пробовал через hasManyThrough - было супер медленно, пришлось так, попробовать переписать полностью через модели */
    public function episodes()
    {
        return MovieEpisode::whereIn('movie_id', function ($query) {
            $query
                ->select('movie_id')
                ->from('cinema.domain_movies')
                ->where('domain', $this->domain);
        });
    }

    public function movieComments()
    {
        return $this->hasManyThrough(DomainMovieComment::class, DomainMovie::class, 'domain');
    }

    /* подходит ли movie для этого домена? */
    public function isMovieAllowed(int|Movie $movie)
    {
        return (new IsMovieAllowedForDomain)($this, $movie);
    }

    public function allowedTypes(): Attribute
    {
        return Attribute::make(
            get: function () {
                $types = [];
                if ($this->include_serials) {
                    $types[] = 'serial';
                }
                if ($this->include_trailers) {
                    $types[] = 'trailer';
                }
                if ($this->include_movies) {
                    $types[] = 'movie';
                }
                if ($this->include_games) {
                    $types[] = 'game';
                }
                if ($this->include_youtube) {
                    $types[] = 'youtube';
                }

                return $types;
            }
        )->shouldCache();
    }

    /* выдает каталоги фильмов и сериалов, учитывая языки, делал для роботс disallow */
    public function getWatchDirs()
    {

        $attributes = ['watchLink', 'watchLinkEn', 'watchMovieLink', 'watchSerialLink'];
        $locales = ['ru', 'ua', 'en'];
        $linkTemplates = [];

        foreach ($attributes as $attribute) {
            if ($this->{$attribute}) {
                /* прогоняем linkTemplate через megaweb шаблон, чтобы учесть языки */
                foreach ($locales as $locale) {
                    $megawebTemplate = new \engine\app\models\Template;
                    $megawebTemplate->setContent($this->{$attribute});
                    $megawebTemplate->v('domainMovieId', null);
                    $megawebTemplate->v('movieTitleRuUrl', null);
                    $megawebTemplate->v('movieTitleRuTranslitUrl', null);
                    $megawebTemplate->v('dmca', null);
                    $megawebTemplate->setLang($locale);
                    $linkTemplate = $megawebTemplate->get();
                    /* убираем окончания */
                    $linkTemplate = str_replace(['/~', '/-'], '', $linkTemplate);
                    if (! $linkTemplate) {
                        throw new \Exception('Не удалось определить каталог для linkTemplate: ' . $this->{$attribute});
                    }
                    // preg_match('/\/([^\/]+)/', $linkTemplate, $matches);
                    // $linkTemplate = $matches[0] ?? throw new \Exception('Не удалось определить каталог для linkTemplate: ' . $this->{$attribute});
                    $linkTemplates[] = $linkTemplate;
                }
            }
        }

        return collect($linkTemplates)->unique();
    }

    public function indexRoute($args = [])
    {

        /* можно передать имя роута, используется в dmcahandler, когда имя роута генерится с доменом в названии */
        $routename = $args['routename'] ?? 'index';

        return route($routename, [
            'locale' => $args['locale'] ?? null,
            /* используем routes, а не routes() - чтобы избежать дублированных запросов при множестве вызовов этого метода */
            'dmca' => $this->engineDomain->routes->where('name', 'index')->firstOrFail()->slug,
        ], false);
    }

    public function top50Route($args = [])
    {

        /* можно передать имя роута, используется в dmcahandler, когда имя роута генерится с доменом в названии */
        $routename = $args['routename'] ?? 'top50';

        return route($routename, [
            /* используем routes, а не routes() - чтобы избежать дублированных запросов при множестве вызовов этого метода */
            'dmca' => $this->engineDomain->routes->where('name', 'top50')->firstOrFail()->slug,
        ], false);
    }
}
