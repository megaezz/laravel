<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class GroupTag extends Model
{
    use HasDatabasePrefix;

    public $incrementing = false;

    protected $database = 'cinema';

    protected $table = 'groups_tags';

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
