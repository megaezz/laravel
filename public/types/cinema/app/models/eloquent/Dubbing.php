<?php

namespace cinema\app\models\eloquent;

use App\Models\Cinema\MovieUpload;
use App\Models\Cinema\TurboDomain;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Dubbing extends Model
{
    use HasDatabasePrefix;

    protected $database = 'cinema';

    protected $table = 'translations';

    public function movieUploads()
    {
        return $this->hasMany(MovieUpload::class);
    }

    public function torrents()
    {
        return $this->hasMany(MovieUpload::class);
    }

    public function shortOrFullName(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->name ?: $this->videodb_name;
            }
        )->shouldCache();
    }

    function turboDomains()
    {
        return $this->hasMany(TurboDomain::class);
    }
}
