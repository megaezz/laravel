<?php

namespace cinema\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class DomainMovieChanges extends Model
{
    use HasDatabasePrefix;

    protected $database = 'cinema';

    public $incrementing = false;
}
