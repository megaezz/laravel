<?php

namespace cinema\app\models\eloquent;

use App\Models\Cinema\Article;
use App\Models\Domain as EngineDomain;
use App\Traits\LogsActivity;
use cinema\app\models\DomainMovie as DomainMovieMegaweb;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;
use Rennokki\QueryCache\Traits\QueryCacheable;

class DomainMovie extends Model
{
    use HasDatabasePrefix;
    use LogsActivity;
    use QueryCacheable;

    protected $database = 'cinema';

    const CREATED_AT = 'add_date';

    const UPDATED_AT = null;

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($domainMovie) {
            $domainMovie->description_length = mb_strlen((string) $domainMovie->description);

            /* если есть домен и не проставлен slug - проставляем */
            if ($domainMovie->domain and ! $domainMovie->slug) {
                $domainMovie->slug = $domainMovie->movie->title_ru ?? $domainMovie->movie->title_en;
            }
        });
    }

    /* используется например в CreateDescriptionsByChatgpt */
    public function domainMovies()
    {
        return $this->belongsTo(DomainMovie::class, 'movie_id', 'movie_id');
    }

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    // function domain() {
    // 	return $this->belongsTo(Domain::class,'domain');
    // }

    public function engineDomain()
    {
        return $this->belongsTo(EngineDomain::class, 'domain');
    }

    public function cinemaDomain()
    {
        return $this->belongsTo(Domain::class, 'domain');
    }

    public function descriptionWriter()
    {
        return $this->belongsTo(User::class, 'description_writer');
    }

    public function bookedFor()
    {
        return $this->belongsTo(User::class, 'booked_for_login');
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer');
    }

    public function moderator()
    {
        return $this->belongsTo(User::class, 'moderator');
    }

    /* отношение напрямую к Videodb, минуя movie, писал для использования в turkru шаблоне, вроде бы работает как надо, последить как покажет себя в дальнейшем */
    public function videodbs()
    {
        return $this->hasMany(Videodb::class, 'movie_id', 'movie_id');
    }

    /* отношение напрямую к MovieEpisode, минуя movie, писал для использования в turkru шаблоне, вроде бы работает как надо, последить как покажет себя в дальнейшем */
    public function episodes()
    {
        return $this->hasMany(MovieEpisode::class, 'movie_id', 'movie_id');
    }

    public function comments()
    {
        return $this->hasMany(DomainMovieComment::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'cinema.group_movie', 'movie_id', 'group_id', 'movie_id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    /* возвращает похожие фильмы, сделано специально не через eloquent, чтобы быстрее работало */
    public function scopeRelatedMovies($query)
    {
        return $query->select('domain_movies.*')
            ->from('cinema.related_movies')
        // RelatedMovie::select('domain_movies.*')
            ->join('cinema.cinema', 'cinema.id', '=', 'related_movies.related_movie_id')
            ->join('cinema.domain_movies', 'domain_movies.movie_id', '=', 'related_movies.related_movie_id')
            ->where('domain_movies.domain', $this->domain)
            ->where('related_movies.movie_id', $this->movie_id)
            ->orderByDesc('related_movies.q');
    }

    /* TODO: туповато, но по-другому хз как. добавляет в выборку relatedMovies условия, чтобы выдавались только активные и неудаленные фильмы */
    public function scopeActiveRelatedMovies($query)
    {
        return $query
            ->where('domain_movies.deleted', false)
            ->where('domain_movies.active', true)
            ->where('cinema.deleted', false);
    }

    /* TODO: если нигде не используется, то удалить нахер */
    public function scopeRelated($query, $tags = [])
    {
        $query
            ->selectRaw('domain_movies.*')
            ->join('cinema.cinema', 'cinema.id', '=', 'domain_movies.movie_id')
            ->joinSub(
                MovieTag::selectRaw('count(*) as q, cinema_id')
                    ->whereIn('tag_id', $tags)
                    ->groupBy('cinema_id'),
                'ct', 'ct.cinema_id', '=', 'cinema.id'
            )
        /* убрать строку ниже, предусмотреть != в Movie (cinema.id != текущему movie.id) */
            ->where('domain_movies.id', '!=', $this->id)
            ->where('cinema.deleted', false)
            ->where('cinema.poster', true)
            ->whereIn('cinema.type', ['movie', 'serial', 'trailer'])
            ->orderByDesc('ct.q')
            ->orderByDesc('cinema.year')
            ->orderByDesc('cinema.rating_sko');

        return $query;
    }

    /* TODO: если нигде не используется, то удалить нахер */
    public function scopeStrictRelated($query, $tags = [])
    {
        $query
            ->selectRaw('domain_movies.*')
            ->join('cinema.cinema', 'cinema.id', '=', 'domain_movies.movie_id')
            ->where('cinema.deleted', false)
            ->where('cinema.poster', true)
            ->orderByDesc('cinema.year')
            ->orderByDesc('cinema.rating_sko');
        $query->whereHas('movie', function ($query) use ($tags) {
            foreach ($tags as $tag) {
                $query->whereRelation('tags', 'tag_id', '=', $tag);
            }
        });

        return $query;
    }

    /* можно передать как id группы, так и объект группы */
    public function scopeGroup($query, Group|int $group)
    {
        if ($group instanceof Group) {
            return $query->whereRelation('groups', 'group_id', '=', $group->id);
        } else {
            return $query->whereRelation('groups', 'group_id', '=', $group);
        }
    }

    // 10 сек на mgw6, разобраться
    /*
    function getRelatedMoviesAttribute() {
        return DomainMovie
        ::related($this->movie->tags->modelKeys())
        ->whereDomain($this->domain)
        ->whereActive(true)
        ->whereDeletedPage(false);
    }
    */

    /* TODO: перенес этот метод в movie, там оно логичнее, но здесь пока не удаляю, разобраться зачем оно вообще */
    public function getRelatedGroupsAttribute()
    {
        return Group::related($this->movie->tags->modelKeys());
    }

    public function megaweb(): Attribute
    {
        return Attribute::make(
            get: function () {
                return new DomainMovieMegaweb(null, $this);
            }
        )->shouldCache();
    }

    public function getWatchLinkAttribute()
    {
        $domain = new \cinema\app\models\Domain($this->domain);

        return $domain->getWatchLink($this->megaweb);
    }

    public function viewRoute(): Attribute
    {
        return Attribute::make(
            get: function () {
                /* оборачиваю в urlencode, т.к. без этого laravel сам оборачивает во что-то похожее, но случаются 2 процента подряд %% и ссылка не валидная */
                return route('movie_view', ['id' => $this->id, 'slug' => urlencode($this->movie->title_ru)], false);
            }
        )->shouldCache();
    }

    public function viewRouteTranslit(): Attribute
    {
        return Attribute::make(
            get: function () {
                return route('movie_view', ['id' => $this->id, 'slug' => Str::slug($this->movie->title_ru)], false);
            }
        )->shouldCache();
    }

    public function viewRouteAbsolute(): Attribute
    {
        return Attribute::make(
            get: function () {
                /* оборачиваю в urlencode, т.к. без этого laravel сам оборачивает во что-то похожее, но случаются 2 процента подряд %% и ссылка не валидная */
                return route('movie_view', ['id' => $this->id, 'slug' => urlencode($this->movie->title_ru)], true);
            }
        )->shouldCache();
    }

    /* TODO: то же самое что viewRoute, удалить */
    public function movieViewRoute($routename = null)
    {
        $routename = $routename ?? 'movie_view';

        return route($routename, ['id' => $this->id, 'slug' => urlencode($this->movie->title_ru)], false);
    }

    public function episodeViewRoute(MovieEpisode $episode, $routename = null)
    {
        $routename = $routename ?? 'episode_view';

        return route($routename, [
            'id' => $episode->id,
            'dmca' => $this->dmca,
        ], false);
    }

    /* TODO: наверное удалить */
    public function scopeAlternativeDescriptions($query)
    {
        return $query
            ->whereNotNull('description')
            ->whereMovieId($this->movie_id)
            ->whereShowToCustomer(true)
            ->where('description_date', '<', now()->subWeeks(2))
            ->whereNotIn('description_writer', ['olga']);
    }

    /* TODO: наверное удалить */
    public function randomAlternativeDescription(): Attribute
    {
        return Attribute::make(
            get: function () {
                $movies = $this->alternativeDescriptions()->get();
                if (! $movies->count()) {
                    return false;
                }

                return $movies->random()->description;
            }
        )->shouldCache();
    }

    /* получаем массив всех возможных дат, связанных с фильмом и выбираем наибольшую */
    public function lastModified(): Attribute
    {
        return Attribute::make(
            get: function () {
                $dates['comments'] = now()::parse($this->comments()->max('date'));
                $dates['videodb'] = now()::parse($this->videodbs()->max('date'));
                $dates['add_date'] = $this->add_date;
                $dates['updated_at'] = $this->updated_at;
                $dates['description_date'] = $this->description_date;
                $max_date = collect($dates)->max();
                /* меняем локаль только для определенного объекта, вместо ->setLocal('en'), иначе меняется глобально */
                $max_date->locale('en');

                return $max_date;
            }
        )->shouldCache();
    }

    public function descriptionOrCopypaste(): Attribute
    {
        return Attribute::make(
            get: function () {
                return $this->description ?? $this->movie->description;
            }
        )->shouldCache();
    }

    public function generateDmca()
    {
        return '~'.substr(bin2hex(openssl_random_pseudo_bytes(4)), 0, 2).'-'.date('d-m');
    }

    public function scopeAvailable($query)
    {
        return $query
            ->whereDeletedPage(false)
            ->whereActive(true);
    }
}
