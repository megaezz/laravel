<?php

namespace cinema\app\models;

use cinema\app\models\eloquent\User as UserEloquent;
use engine\app\models\Engine;
use engine\app\models\F;

class User
{
    private $login = null;

    private $level = null;

    private $id = null;

    private $rewritePricePer1k = null;

    private $wmr = null;

    private $yandex = null;

    private $advCash = null;

    private $tempBalance = null;

    private $symbolsFrom = null;

    private $symbolsTo = null;

    private $premoderation = null;

    private $activeDescriptionWriter = null;

    private $comments = null;

    private $timezone = null;

    private $password = null;

    private $email = null;

    private $customerComment = null;

    private $customerTemp1TxtPrice = null;

    private $sberbank = null;

    // опустить вниз в очереди
    private $customerLowerDown = null;

    private $customerPrivateComment = null;

    private $customerActiveTasks = null;

    private $checkIsMovieInDoneTasks = null;

    private $partner = null;

    private $wmz = null;

    private $sbp = null;

    private $usdtBep20 = null;

    private $customerPricePer1k = null;

    private $uniqueAutoCheck = null;

    private $expressRewriter = null;

    private $customerAutoExpress = null;

    private $apiKey = null;

    private $adult = null;

    private $emailConfirmed = null;

    private $emailCode = null;

    private $tempBalanceDate = null;

    private $restrictEndDate = null;

    private $testPassed = null;

    private $testStartDate = null;

    private $checked = null;

    public function __construct($login = null, ?UserEloquent $userEloquent = null)
    {
        if ($userEloquent) {
            $arr = $userEloquent->getAttributes();
        } else {
            if (! $login) {
                F::error('User ID required to create User Class');
            }
            $arr = F::query_assoc('
				select login,id,level,rewrite_price_per_1k,wmr,yandex,advcash,temp_balance,
				symbols_from, symbols_to, premoderation, active_rewriter,comments, timezone,
				pass, email, customer_comment, customerTemp1TxtPrice, sberbank, lower_down,
				customer_private_comment, customer_active_tasks, checkIsMovieInDoneTasks,
				partner, wmz, sbp, usdtbep20, customer_price_per_1k, unique_auto_check, expressRewriter,
				customerAutoExpress, api_key, adult, emailConfirmed, emailCode, temp_balance_date,
				restrict_end_date, test_passed, test_start_date, checked
				from '.F::typetab('users').'
				where login = \''.$login.'\'
				;');
        }
        if (! $arr) {
            F::error('User was not found');
        }
        $this->id = $arr['id'];
        $this->login = $arr['login'];
        $this->level = $arr['level'];
        $this->rewritePricePer1k = $arr['rewrite_price_per_1k'];
        $this->customerPricePer1k = $arr['customer_price_per_1k'];
        $this->wmr = $arr['wmr'];
        $this->yandex = $arr['yandex'];
        $this->advCash = $arr['advcash'];
        $this->tempBalance = $arr['temp_balance'];
        $this->symbolsFrom = $arr['symbols_from'];
        $this->symbolsTo = $arr['symbols_to'];
        $this->premoderation = $arr['premoderation'];
        $this->activeDescriptionWriter = $arr['active_rewriter'];
        $this->comments = $arr['comments'];
        $this->timezone = $arr['timezone'];
        $this->password = $arr['pass'];
        $this->email = $arr['email'];
        $this->customerComment = $arr['customer_comment'];
        $this->customerTemp1TxtPrice = $arr['customerTemp1TxtPrice'];
        $this->sberbank = $arr['sberbank'];
        $this->customerLowerDown = $arr['lower_down'] ? true : false;
        $this->customerPrivateComment = $arr['customer_private_comment'];
        $this->customerActiveTasks = $arr['customer_active_tasks'];
        $this->checkIsMovieInDoneTasks = $arr['checkIsMovieInDoneTasks'];
        $this->partner = $arr['partner'];
        $this->wmz = $arr['wmz'];
        $this->sbp = $arr['sbp'];
        $this->usdtBep20 = $arr['usdtbep20'];
        $this->uniqueAutoCheck = $arr['unique_auto_check'] ? true : false;
        $this->expressRewriter = $arr['expressRewriter'] ? true : false;
        $this->customerAutoExpress = $arr['customerAutoExpress'] ? true : false;
        $this->apiKey = $arr['api_key'];
        $this->adult = $arr['adult'] ? true : false;
        $this->emailConfirmed = $arr['emailConfirmed'] ? true : false;
        $this->emailCode = $arr['emailCode'];
        $this->tempBalanceDate = $arr['temp_balance_date'];
        $this->restrictEndDate = $arr['restrict_end_date'];
        $this->testPassed = $arr['test_passed'] ? true : false;
        $this->testStartDate = $arr['test_start_date'];
        $this->setChecked($arr['checked'] ? true : false);
    }

    public function getRewriteBalance()
    {
        // $cinema = new Cinema;
        // $cost1k = $cinema->getRewritePrice1k();
        $arr = F::query_assoc('
			select sum(description_length*description_price_per_1k/1000) as balance
			from '.F::typetab('domain_movies').'
			where description_writer = \''.$this->getLogin().'\'
			/*group by description_writer*/
			');
        $rewritedPlus = $arr['balance'];
        $arr = F::query_assoc('
			select sum(description_length*customer_price_per_1k/1000) as balance
			from '.F::typetab('domain_movies').'
			where customer = \''.$this->getLogin().'\'
			');
        $customerMinus = $arr['balance'];
        // определить логины тех, кого этот пользователь привел на сайт, затем умножить их баланс (без учета реферальских прибавок) на 10% и прибавить.
        $balance = round($rewritedPlus - $customerMinus + $this->getModerationBalance() + $this->getTransfers(), 2);

        return $balance;
    }

    public function getModerationBalance()
    {
        $arr = F::query_assoc('
			select sum(description_length*moderation_price_per_1k/1000) as balance
			from '.F::typetab('domain_movies').'
			where moderator = \''.$this->getLogin().'\'
			');

        return $arr['balance'];
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel($level = null)
    {
        $this->level = $level;
    }

    public function getTransfers()
    {
        $arr = F::query_assoc('select sum(sum) as sum
			from '.F::typetab('transfers').'
			where login = \''.$this->getLogin().'\'
			;');

        return $arr['sum'] ? $arr['sum'] : 0;
    }

    public function getTransfersArr()
    {
        $arr = F::query_arr('select date,sum,comment
			from '.F::typetab('transfers').'
			where login = \''.$this->getLogin().'\'
			order by date desc
			;');

        return $arr;
    }

    public function addTransfer($sum = 0, $comment = '', $payout = null)
    {
        if (! $sum) {
            F::error('Sum required to use User::addTransfer');
        }
        F::query('insert into '.F::typetab('transfers').'
			set login = \''.$this->getLogin().'\',
			comment = '.($comment ? '\''.$comment.'\'' : 'null').',
			sum = \''.$sum.'\',
			payout = '.($payout ? 1 : 0).'
			;');
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));
        $this->calcTempBalance();

        return $id;
    }

    public function getRewritePricePer1k()
    {
        if (is_null($this->rewritePricePer1k)) {
            return (new Cinema)->getRewritePricePer1k();
        }

        return $this->rewritePricePer1k;
    }

    public function getCustomerPricePer1k()
    {
        // если стоит авто проверка уникальности - добавляем рублик
        $uniqueAutoCheckPrice = $this->getUniqueAutoCheck() ? 1 : 0;
        if (is_null($this->customerPricePer1k)) {
            return (new Cinema)->getCustomerPricePer1k() + $uniqueAutoCheckPrice;
        }

        return $this->customerPricePer1k + $uniqueAutoCheckPrice;
    }

    public function getModerationPricePer1k()
    {
        // if (is_null($this->moderationPricePer1k)) {
        return (new Cinema)->getModerationPricePer1k();
        // }
        // return $this->moderationPricePer1k;
    }

    public function getCustomerExpressPricePer1k()
    {
        // фиксим, чтобы бесплатная цена оставалась бесплатной
        if ($this->getCustomerPricePer1k() === 0) {
            return $this->getCustomerPricePer1k();
        }

        return $this->getCustomerPricePer1k() + (new Cinema)->getCustomerExpressFare();
    }

    public function getAvgTextUnique()
    {
        $arr = F::query_assoc('
			select avg(text_ru_unique) as avg
			from '.F::typetab('domain_movies').'
			where description_writer = \''.$this->getLogin().'\'
			;');

        return $arr['avg'];
    }

    public function getWmr()
    {
        return $this->wmr;
    }

    public function getYandex()
    {
        return $this->yandex;
    }

    public function getAdvCash()
    {
        return $this->advCash;
    }

    public function save()
    {
        F::query('update '.F::typetab('users').'
			set
			temp_balance = '.(is_null($this->getTempBalance()) ? 'null' : ('\''.F::escape_string($this->getTempBalance()).'\'')).',
			symbols_from = '.(is_null($this->getSymbolsFrom()) ? 'null' : ('\''.$this->getSymbolsFrom().'\'')).',
			symbols_to = '.(is_null($this->getSymbolsTo()) ? 'null' : ('\''.$this->getSymbolsTo().'\'')).',
			pass = '.($this->getPassword() ? ('\''.F::escape_string($this->getPassword()).'\'') : 'null').',
			level = '.($this->getLevel() ? ('\''.F::escape_string($this->getLevel()).'\'') : 'null').',
			email = '.($this->getEmail() ? ('\''.F::escape_string($this->getEmail()).'\'') : 'null').',
			customer_comment = '.($this->getCustomerComment() ? ('\''.F::escape_string($this->getCustomerComment()).'\'') : 'null').',
			customerTemp1TxtPrice = '.(is_null($this->getCustomerTemp1TxtPrice()) ? 'null' : ('\''.F::escape_string($this->getCustomerTemp1TxtPrice()).'\'')).',
			customer_private_comment = '.($this->getCustomerPrivateComment() ? ('\''.F::escape_string($this->getCustomerPrivateComment()).'\'') : 'null').',
			wmr = '.($this->getWmr() ? ('\''.F::escape_string($this->getWmr()).'\'') : 'null').',
			yandex = '.($this->getYandex() ? ('\''.F::escape_string($this->getYandex()).'\'') : 'null').',
			advcash = '.($this->getAdvCash() ? ('\''.F::escape_string($this->getAdvCash()).'\'') : 'null').',
			sberbank = '.($this->getSberbank() ? ('\''.F::escape_string($this->getSberbank()).'\'') : 'null').',
			customer_active_tasks = '.($this->getCustomerActiveTasks() ? ('\''.F::escape_string($this->getCustomerActiveTasks()).'\'') : 'null').',
			checkIsMovieInDoneTasks = '.($this->getCheckIsMovieInDoneTasks() ? 1 : 0).',
			partner = '.($this->getPartner() ? ('\''.F::escape_string($this->getPartner()).'\'') : 'null').',
			wmz = '.($this->getWmz() ? ('\''.F::escape_string($this->getWmz()).'\'') : 'null').',
			sbp = '.($this->getSbp() ? ('\''.F::escape_string($this->getSbp()).'\'') : 'null').',
			usdtbep20 = '.($this->getUsdtBep20() ? ('\''.F::escape_string($this->getUsdtBep20()).'\'') : 'null').',
			unique_auto_check = '.($this->getUniqueAutoCheck() ? 1 : 0).',
			customerAutoExpress = '.($this->getCustomerAutoExpress() ? 1 : 0).',
			api_key = '.($this->getApiKey() ? ('\''.F::escape_string($this->getApiKey()).'\'') : 'null').',
			adult = '.($this->getAdult() ? 1 : 0).',
			emailConfirmed = '.($this->getEmailConfirmed() ? 1 : 0).',
			emailCode = '.($this->getEmailCode() ? ('\''.F::escape_string($this->getEmailCode()).'\'') : 'null').',
			temp_balance_date = '.($this->getTempBalanceDate() ? ('\''.F::escape_string($this->getTempBalanceDate()).'\'') : 'null').',
			restrict_end_date = '.($this->getRestrictEndDate() ? ('\''.F::escape_string($this->getRestrictEndDate()).'\'') : 'null').',
			test_passed = '.($this->getTestPassed() ? 1 : 0).',
			test_start_date = '.($this->getTestStartDate() ? ('\''.F::escape_string($this->getTestStartDate()).'\'') : 'null').',
			checked = '.($this->getChecked() ? 1 : 0).'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }

    public function calcTempBalance()
    {
        $this->setTempBalance($this->getRewriteBalance());
        $this->setTempBalanceDate((new \DateTime)->format('Y-m-d H:i:s'));
        $this->save();
    }

    public function calcCustomerTemp1TxtPrice()
    {
        // $this->setCustomerTemp1TxtPrice($this->getCustomerExpectedSymbols()*(new Cinema)->getCustomerPricePer1k()/1000);
        $this->setCustomerTemp1TxtPrice($this->getCustomerExpectedSymbols() * $this->getCustomerPricePer1k() / 1000);
        $this->save();
    }

    public function setCustomerTemp1TxtPrice($price = null)
    {
        $this->customerTemp1TxtPrice = $price;
    }

    public function getCustomerTemp1TxtPrice()
    {
        return $this->customerTemp1TxtPrice;
    }

    public function getCustomer1TxtPrice()
    {
        // return $this->getCustomerExpectedSymbols()*(new Cinema)->getCustomerPricePer1k()/1000;
        return $this->getCustomerExpectedSymbols() * $this->getCustomerPricePer1k() / 1000;
    }

    public function setTempBalance($amount = null)
    {
        $this->tempBalance = $amount;
    }

    public function getTempBalance()
    {
        return $this->tempBalance;
    }

    // хватит ли заказчику баланса хотя бы для 1 описания
    public function getCustomerIsEnoughBalance()
    {
        // обновляем баланс
        $this->calcTempBalance();
        $balance = $this->getTempBalance();
        $movies = new Movies;
        $movies->setLimit(1);
        $movies->setWithDomainDescription(false);
        $movies->setWithBookedForLogin(true);
        $movies->setDomainMovieCustomer($this->getLogin());
        // если у заказчика нет заданий в очереди, то считаем его активным, если баланс больше 0
        if (! $movies->get()) {
            return ($balance > 0) ? true : false;
        } else {
            // если задания в очереди есть, значит смотрим, есть ли хотя бы одно задание, на которое у заказчика хватает баланса, если да, его статус - активен.
            $movies = new Movies;
            $movies->setLimit(1);
            $movies->setWithDomainDescription(false);
            $movies->setWithBookedForLogin(true);
            $movies->setDomainMovieWithCustomerEnoughBalance(true);
            $movies->setDomainMovieCustomer($this->getLogin());

            return $movies->get() ? true : false;
        }
        // $text_price = $this->getCustomerExpectedSymbols()*$this->getCustomerPricePer1k()/1000;
        // return ($this->getRewriteBalance() >= $text_price)?true:false;
    }

    public function getSymbolsFrom()
    {
        return $this->symbolsFrom;
    }

    public function setSymbolsFrom($q = null)
    {
        $this->symbolsFrom = $q;
    }

    public function getSymbolsTo()
    {
        return $this->symbolsTo;
    }

    public function setSymbolsTo($q = null)
    {
        $this->symbolsTo = $q;
    }

    public function getPremoderation()
    {
        return $this->premoderation;
    }

    public function getActiveDescriptionWriter()
    {
        return $this->activeDescriptionWriter;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function getTimezone()
    {
        return $this->timezone;
    }

    public static function add($login = null)
    {
        F::query('insert into '.F::typetab('users').' set login = \''.F::escape_string($login).'\';');
        $user = new User($login);

        return $user->getId();
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password = null)
    {
        $this->password = $password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email = null)
    {
        $this->email = $email;
    }

    public function getCustomerComment()
    {
        return $this->customerComment;
    }

    public function setCustomerComment($comment = null)
    {
        $this->customerComment = $comment;
    }

    public function getCustomerExpectedSymbols()
    {
        $from = $this->getSymbolsFrom();
        $to = $this->getSymbolsTo();

        return Cinema::getExpectedSymbols($from, $to);
    }

    public function getSberbank()
    {
        return $this->sberbank;
    }

    public function getCustomerLowerDown()
    {
        return $this->customerLowerDown;
    }

    public function setCustomerPrivateComment($comment = null)
    {
        $this->customerPrivateComment = $comment;
    }

    public function getCustomerPrivateComment()
    {
        return $this->customerPrivateComment;
    }

    public function setWmr($wallet = null)
    {
        $this->wmr = $wallet;
    }

    public function setYandex($wallet = null)
    {
        $this->yandex = $wallet;
    }

    public function setSberbank($wallet = null)
    {
        $this->sberbank = $wallet;
    }

    public function setAdvCash($wallet = null)
    {
        $this->advCash = $wallet;
    }

    public function setCustomerActiveTasks($rows = null)
    {
        $this->customerActiveTasks = $rows;
    }

    public function getCustomerActiveTasks()
    {
        return $this->customerActiveTasks;
    }

    public function getCheckIsMovieInDoneTasks()
    {
        return $this->checkIsMovieInDoneTasks;
    }

    public function setCheckIsMovieInDoneTasks($b = null)
    {
        $this->checkIsMovieInDoneTasks = $b;
    }

    public function getPartner()
    {
        return $this->partner;
    }

    public function setPartner($login = null)
    {
        $this->partner = $login;
    }

    public function getWmz()
    {
        return $this->wmz;
    }

    public function setWmz($wallet = null)
    {
        $this->wmz = $wallet;
    }

    public function getSbp()
    {
        return $this->sbp;
    }

    public function setSbp($wallet = null)
    {
        $this->sbp = $wallet;
    }

    public function getUsdtBep20()
    {
        return $this->usdtBep20;
    }

    public function setUsdtBep20($wallet = null)
    {
        $this->usdtBep20 = $wallet;
    }

    public function getLikeCounter()
    {
        $arr = F::query_assoc('select count(*) as q
			from '.F::typetab('domain_movies').'
			where customer_like = 1 and description_writer = \''.$this->getLogin().'\'
			;');

        return $arr['q'];
    }

    public function getDislikeCounter()
    {
        $arr = F::query_assoc('select count(*) as q
			from '.F::typetab('domain_movies').'
			where customer_like = 0 and description_writer = \''.$this->getLogin().'\'
			;');

        return $arr['q'];
    }

    public function getBlacklistCounter()
    {
        $arr = F::query_assoc('select count(*) as q
			from '.F::typetab('customer_blacklist').'
			where worker = \''.$this->getLogin().'\'
			;');

        return $arr['q'];
    }

    public function getUniqueAutoCheck()
    {
        return $this->uniqueAutoCheck;
    }

    public function setUniqueAutoCheck($b = null)
    {
        $this->uniqueAutoCheck = $b;
    }

    public function getExpressRewriter()
    {
        return $this->expressRewriter;
    }

    public function getWallet()
    {
        $object = new \stdClass;
        $object->wallet = null;
        $object->type = null;
        $object->unitpayType = null;
        $object->currency = null;
        $object->link = null;
        $wallets[] = [
            'unitpayType' => 'webmoney',
            'fkWallet' => false,
            'wallet' => $this->getWmr(),
            'currency' => 'RUB',
            'link' => 'https://light.webmoney.ru/v3#/v3/WMTrans',
            'serviceName' => 'Webmoney WMR',
            'type' => 'Wmr',
        ];
        $wallets[] = [
            'unitpayType' => 'webmoney',
            'fkWallet' => false,
            // 'fkWallet' => [
            // 	'id' => 2,
            // 	'minRub' => 10,
            // 	'fee' => 7
            // ],
            'wallet' => $this->getWmz(),
            'currency' => 'USD',
            'link' => 'https://light.webmoney.ru/v3#/v3/WMTrans',
            'serviceName' => 'Webmoney WMZ',
            'type' => 'Wmz',
        ];
        $wallets[] = [
            'unitpayType' => 'yandex',
            'fkWallet' => false,
            // 'fkWallet' => [
            // 	'id' => 45,
            // 	'minRub' => 10,
            // 	'fee' => 3
            // ],
            'wallet' => $this->getYandex(),
            'currency' => 'RUB',
            'link' => 'https://money.yandex.ru/transfer',
            'serviceName' => 'Yandex',
            'type' => 'Yandex',
        ];
        $wallets[] = [
            'unitpayType' => null,
            'fkWallet' => false,
            // 'fkWallet' => [
            // 	'id' => 150,
            // 	'minRub' => 10,
            // 	'fee' => 5
            // ],
            'wallet' => $this->getAdvCash(),
            'currency' => 'RUB',
            'link' => 'https://wallet.advcash.com/pages/transfer/wallet',
            'serviceName' => 'Advcash',
            'type' => 'Advcash',
        ];
        $wallets[] = [
            'unitpayType' => null,
            'fkWallet' => false,
            'wallet' => $this->getSberbank(),
            'currency' => 'RUB',
            'link' => 'https://node2.online.sberbank.ru/PhizIC/private/payments/payment.do?form=RurPayment&receiverSubType=ourCard',
            'serviceName' => 'Сбербанк',
            'type' => 'Sberbank',
        ];
        $wallets[] = [
            'unitpayType' => 'sbp',
            'fkWallet' => false,
            // 'fkWallet' => [
            // 	'id' => 63,
            // 	'minRub' => 50,
            // 	'fee' => 4
            // ],
            'wallet' => $this->getSbp(),
            'currency' => 'RUB',
            'link' => '',
            'serviceName' => 'СБП',
            'type' => 'SBP',
        ];
        $wallets[] = [
            'wallet' => $this->getUsdtBep20(),
            'currency' => 'USD',
            'link' => null,
            'serviceName' => 'USDT BEP20',
            'type' => 'usdtbep20',
            'available' => true,
        ];
        foreach ($wallets as $v) {
            if ($v['wallet']) {
                $object->wallet = $v['wallet'];
                $object->type = $v['type'];
                $object->unitpayType = $v['unitpayType'] ?? null;
                $object->fkWallet = $v['fkWallet'] ?? null;
                $object->currency = $v['currency'];
                $object->link = $v['link'];
                $object->serviceName = $v['serviceName'];
                break;
            }
        }

        return $object;
    }

    public function getCustomerAutoExpress()
    {
        return $this->customerAutoExpress;
    }

    public function setCustomerAutoExpress($b = null)
    {
        $this->customerAutoExpress = $b;
    }

    public function getApiKey()
    {
        // если нет ключа, то генерируем его
        if (! $this->apiKey) {
            do {
                $key = F::generateRandomStr(40);
                // проверяем вдруг в базе уже есть такой key
                $users = new Users;
                $users->setApiKey($key);
            } while (! empty($users->getRows()));
            $this->apiKey = $key;
            $this->save();
        }

        return $this->apiKey;
    }

    public function setApiKey($apiKey = null)
    {
        $this->apiKey = $apiKey;
    }

    public function getAdult()
    {
        return $this->adult;
    }

    public function setAdult($b = null)
    {
        $this->adult = $b;
    }

    public function getEmailConfirmed()
    {
        return $this->emailConfirmed;
    }

    public function setEmailConfirmed($b = null)
    {
        $this->emailConfirmed = $b;
    }

    public function getEmailCode()
    {
        return $this->emailCode;
    }

    public function setEmailCode($code = null)
    {
        $this->emailCode = $code;
    }

    public function getTempBalanceDate()
    {
        return $this->tempBalanceDate;
    }

    public function setTempBalanceDate($date = null)
    {
        $this->tempBalanceDate = $date;
    }

    public function getRestrictEndDate()
    {
        return $this->restrictEndDate;
    }

    public function setRestrictEndDate($date = null)
    {
        $this->restrictEndDate = $date;
    }

    public function getRestricted()
    {
        $nowDate = new \DateTime;
        $endDate = $this->getRestrictEndDate() ? (new \DateTime($this->getRestrictEndDate())) : null;
        if ($nowDate > $endDate) {
            $this->setRestrictEndDate(null);
            $this->save();
        }

        return empty($this->restrictEndDate) ? false : true;
    }

    public function getTestPassed()
    {
        return $this->testPassed;
    }

    public function setTestPassed($b = null)
    {
        $this->testPassed = $b;
    }

    public function getTestStartDate()
    {
        return $this->testStartDate;
    }

    public function setTestStartDate($date = null)
    {
        $this->testStartDate = $date;
    }

    public function setChecked($b = null)
    {
        $this->checked = $b;
    }

    public function getChecked()
    {
        return $this->checked;
    }
}
