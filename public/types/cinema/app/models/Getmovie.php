<?php

// http://getmovie.cc/
function getmovie_api($args = [])
{
    global $config;
    $args['kinopoisk_id'] = empty($args['kinopoisk_id']) ? error('Не передан kinopoisk_id') : $args['kinopoisk_id'];
    mysql_set_charset('utf8');
    $arr = F::query_assoc('
		select kinopoisk_id,timestampdiff(minute,update_date,current_timestamp) as minutes,kinopoisk,getmovie
		from '.F::typetab('getmovie').'
		where kinopoisk_id=\''.$args['kinopoisk_id'].'\'
		;');
    // pre(var_dump(json_decode($arr['kinopoisk'])));
    // если не прошли сутки то выводим информацию из бд, если прошли, то запрашиваем через api.
    if ($arr['kinopoisk_id']) {
        if ($arr['minutes'] < 86400) {
            // pre(var_dump(json_decode($arr['kinopoisk'])));
            // if (empty($arr['kinopoisk'])) {pre('пусто');};
            $return = new stdClass;
            $return->kinopoisk = json_decode($arr['kinopoisk']);
            $return->getmovie = json_decode($arr['getmovie']);

            // pre('из бд:<br>'.var_dump($return));
            return $return;
        }
    }
    $url_kinopoisk = 'http://getmovie.cc/api/kinopoisk.json?id='.$args['kinopoisk_id'].'&token=037313259a17be837be3bd04a51bf678';
    $url_getmovie = 'http://getmovie.cc/api/videos.json?kinopoisk_id='.$args['kinopoisk_id'].'&api_token=037313259a17be837be3bd04a51bf678&r=json';
    // pre($url_kinopoisk);
    // $json_kinopoisk=filegetcontents_alt($url_kinopoisk,array('timeout'=>1,'error'=>'no_error'));
    // $json_getmovie=filegetcontents_alt($url_getmovie,array('timeout'=>1,'error'=>'no_error'));
    $json_kinopoisk = @file_get_contents($url_kinopoisk);
    $json_getmovie = @file_get_contents($url_getmovie);
    // pre($url_getmovie);
    // pre(var_dump($json_kinopoisk));
    // pre(var_dump($json_getmovie));
    if ($json_kinopoisk === false or $json_getmovie === false) {
        // error('Ошибка чтения данных');
        return false;
    }
    // mysql_set_charset('utf8');
    // меняем юникод на кириллицу в json кинопоиска
    $object_kinopoisk = json_decode($json_kinopoisk);
    $json_kinopoisk = json_encode($object_kinopoisk, JSON_UNESCAPED_UNICODE);
    // pre($json_getmovie);
    F::query('
		insert into '.F::typetab('getmovie').'
		set
		kinopoisk_id=\''.mysql_real_escape_string($args['kinopoisk_id']).'\',
		kinopoisk=\''.mysql_real_escape_string($json_kinopoisk).'\',
		getmovie=\''.mysql_real_escape_string($json_getmovie).'\',
		update_date=current_timestamp
		on duplicate key update
		kinopoisk=\''.mysql_real_escape_string($json_kinopoisk).'\',
		getmovie=\''.mysql_real_escape_string($json_getmovie).'\',
		update_date=current_timestamp
		;');
    $return = new stdClass;
    $return->kinopoisk = json_decode($json_kinopoisk);
    $return->getmovie = json_decode($json_getmovie);

    // pre('из api:<br>'.var_dump($return));
    return $return;
    // if (empty($object->response->items[0])) {return false;};
    // foreach ($object->response->items[0] as $k=>$v) {
    // 	$arr[$k]=mb_convert_encoding($v,'windows-1251','utf-8');
    // };
    // pre($arr);
    mysql_set_charset($config['mysql_charset']);

    return $arr;
}
