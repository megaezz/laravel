<?php

/*

http://moonwalk.cc/partners/login

*/

namespace cinema\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class Moonwalk
{
    const apiToken = '3bb4510087038631f510bba0ca7c3f24';

    private $data = null;

    private $token = null;

    private $checked = null;

    private $date = null;

    private $unique_id = null;

    public function __construct($token = null, $date = null)
    {
        if (! $token) {
            F::error('Не передан Token объекта');
        }
        if ($date) {
            $sql['date'] = 'date(date) = \''.$date.'\'';
            $sql['table'] = 'moonwalk_updates';
        } else {
            $sql['date'] = 1;
            $sql['table'] = 'moonwalk';
        }
        $arr = F::query_assoc('select unique_id,data,checked,token,date(date) as date
			from '.F::typetab($sql['table']).'
			where
			token=\''.$token.'\' and
			'.$sql['date'].'
			limit 1
			;');
        if (empty($arr)) {
            F::error('Объект Mnwlk не найден');
        }
        $this->token = $arr['token'];
        $this->unique_id = $arr['unique_id'];
        $this->data = json_decode($arr['data']);
        $this->checked = $arr['checked'];
        $this->date = $arr['date'];
        // dump($this->data);
    }

    public static function getApiToken()
    {
        return static::apiToken;
    }

    // возвратить токен по unique_id, имеющий макс. кол-во эпизодов
    public static function getBestTokenByUniqueId($unique_id = null)
    {
        if (! $unique_id) {
            return null;
        }
        $moonwalks = new Moonwalks;
        $moonwalks->setUniqueId($unique_id);
        $moonwalks->setLimit(20);
        $moonwalks->setPage(1);
        // $moonwalks->setOrder('episodes_count desc');
        $arr = $moonwalks->get();
        if (! $arr) {
            return null;
        }
        $bestObj = null;
        foreach ($arr as $v) {
            $m = new Moonwalk($v['token']);
            if (is_null($bestObj)) {
                $bestObj = $m;
            }
            if ($m->getEpisodesCount() > $bestObj->getEpisodesCount()) {
                $bestObj = $m;
            }
            if ($m->getType() != 'trailer' and $bestObj->getType() == 'trailer') {
                $bestObj = $m;
            }
        }

        return $bestObj->getToken();
    }

    public function getData()
    {
        return $this->data;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getUniqueId()
    {
        return $this->unique_id;
    }

    public function getTitleEn()
    {
        if (! isset($this->data->title_en)) {
            return null;
        }

        return $this->data->title_en;
    }

    public function getTitleRu()
    {
        if (! isset($this->data->title_ru)) {
            return null;
        }

        return $this->data->title_ru;
    }

    public function getYear()
    {
        if (! isset($this->data->year)) {
            return null;
        }

        return $this->data->year;
    }

    public function getAddedAt()
    {
        if (! isset($this->data->added_at)) {
            return null;
        }

        return isset($this->data->added_at) ? $this->data->added_at : null;
    }

    public function getType()
    {
        if (! isset($this->data->type)) {
            return null;
        }

        return $this->data->type;
    }

    public function getCategory()
    {
        if (! isset($this->data->category)) {
            return 'foreign';
        }

        return $this->data->category ? $this->data->category : 'foreign';
    }

    public function getCamrip()
    {
        return isset($this->data->camrip) ? $this->data->camrip : null;
    }

    public function getKinopoiskId()
    {
        if (! isset($this->data->kinopoisk_id)) {
            return null;
        }

        return $this->data->kinopoisk_id;
    }

    public function getWorldArtId()
    {
        if (! isset($this->data->world_art_id)) {
            return null;
        }

        return $this->data->world_art_id;
    }

    public function getIframe()
    {
        if (! isset($this->data->iframe_url)) {
            return null;
        }

        return $this->data->iframe_url;
    }

    public function getSeasonsCount()
    {
        if (! isset($this->data->seasons_count)) {
            return null;
        }

        return $this->data->seasons_count;
    }

    public function getEpisodesCount()
    {
        if (! isset($this->data->episodes_count)) {
            return null;
        }

        return $this->data->episodes_count;
    }

    public function getSeasonEpisodesCount()
    {
        if (! isset($this->data->season_episodes_count)) {
            return null;
        }

        return $this->data->season_episodes_count;
    }

    public function getBlockUa()
    {
        if (! isset($this->getData()->block->block_ua)) {
            return null;
        }

        return $this->getData()->block->block_ua;
    }

    public function getBlockRu()
    {
        if (! isset($this->getData()->block->block_ru)) {
            return null;
        }

        return $this->getData()->block->block_ru;
    }

    public function getTranslator()
    {
        if (! isset($this->data->translator)) {
            return null;
        }

        return $this->data->translator;
    }

    public function getTranslatorId()
    {
        if (! isset($this->data->translator_id)) {
            return null;
        }

        return $this->data->translator_id;
    }

    public function getDescription()
    {
        return isset($this->data->material_data->description) ? $this->data->material_data->description : false;
    }

    public function getKinopoiskRating()
    {
        return isset($this->data->material_data->kinopoisk_rating) ? $this->data->material_data->kinopoisk_rating : false;
    }

    public function getKinopoiskVotes()
    {
        return isset($this->data->material_data->kinopoisk_votes) ? $this->data->material_data->kinopoisk_votes : false;
    }

    public function getImdbRating()
    {
        return isset($this->data->material_data->imdb_rating) ? $this->data->material_data->imdb_rating : false;
    }

    public function getImdbVotes()
    {
        return isset($this->data->material_data->imdb_votes) ? $this->data->material_data->imdb_votes : false;
    }

    public function getTrailerIframe()
    {
        if (! isset($this->data->trailer_iframe_url)) {
            return null;
        }

        return $this->data->trailer_iframe_url;
    }

    public function getThumbnails()
    {
        return false;
        $json = @file_get_contents('http://moonwalk.cc/api/thumbnails.json?token='.$this->getToken().'&api_token='.static::apiToken);
        $thumbnails = json_decode($json);

        return isset($thumbnails->thumbnails) ? $thumbnails->thumbnails : false;
    }

    public function getGenres()
    {
        return isset($this->data->material_data->genres) ? $this->data->material_data->genres : false;
    }

    public function getCountries()
    {
        return isset($this->data->material_data->countries) ? $this->data->material_data->countries : false;
    }

    public function getActors()
    {
        return isset($this->data->material_data->actors) ? $this->data->material_data->actors : false;
    }

    public function getDirectors()
    {
        return isset($this->data->material_data->directors) ? $this->data->material_data->directors : false;
    }

    public function getStudios()
    {
        return isset($this->data->material_data->studios) ? $this->data->material_data->studios : false;
    }

    public function getPlayer()
    {
        $t = new Template('players/moonwalk');
        $type = $this->getType();
        if ($type === 'movie' or $type === 'trailer') {
            $type = 'video';
        }
        $t->v('token', $this->getToken());
        $t->v('type', $type);

        return $t->get();
    }

    public function getTrailerPlayer()
    {
        $t = new Template('players/moonwalk_trailer');
        $t->v('url', $this->getTrailerIframe());

        return $t->get();
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function setChecked($b = null)
    {
        $this->checked = $b;
    }

    public function save()
    {
        F::query('update '.F::typetab('moonwalk').'
			set
			checked = '.($this->getChecked() ? 1 : 0).'
			where token = \''.$this->getToken().'\'
			;');

        return true;
    }

    // выдает последние сохраненные данные
    public function getLastMoonwalk()
    {
        // должен выводить предыдущую от текущей запись архива
        // /*
        $moonwalks = new Moonwalks;
        $moonwalks->setToken($this->getToken());
        $moonwalks->setLimit(1);
        $moonwalks->setPage(1);
        $moonwalks->setOrder('date desc');
        $moonwalks->setMaxDate($this->getDate());
        $arr = $moonwalks->get();
        if ($arr) {
            $m = new Moonwalk($arr[0]['token'], $arr[0]['date']);

            return $m;
        } else {
            return false;
        }
        // */
        /*
        $movieId = Movie::getIdByUniqueId($this->getUniqueId());
        $arr = F::query_assoc('
            select data
            from '.F::typetab('moonwalk_updates').'
            where cinema_id = \''.$movieId.'\' and token = \''.$this->getToken().'\'
            order by date desc
            limit 1
            ;');
        return json_decode($arr['data']);
        */
    }

    // обновляем данные в movies_updates если нужно
    public function addMoonwalkEpisodesUpdate()
    {
        if ($this->getType() !== 'serial') {
            return false;
        }
        $last = $this->getLastMoonwalk();
        if (! $last) {
            return false;
        }
        if ($last->getSeasonEpisodesCount() == $this->getSeasonEpisodesCount()) {
            // dump($last);
            // dump($last->getSeasonEpisodesCount());
            // dump($this->getSeasonEpisodesCount());
            return false;
        }
        $movieId = Movie::getIdByUniqueId($this->getUniqueId());
        F::query('
			insert into '.F::typetab('moonwalk_updates').'
			set
			cinema_id = \''.$movieId.'\',
			token = \''.$this->getToken().'\',
			data = \''.F::escape_string(json_encode($this->data, JSON_UNESCAPED_UNICODE)).'\',
			unique_id = \''.$this->getUniqueId().'\'
			;');

        return true;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getUpdatedAt()
    {
        return isset($this->data->material_data->updated_at) ? $this->data->material_data->updated_at : false;
    }

    public function getAltYear()
    {
        return isset($this->data->material_data->year) ? $this->data->material_data->year : null;
    }

    public function getSourceType()
    {
        return isset($this->data->source_type) ? $this->data->source_type : null;
    }

    public function getAge()
    {
        return isset($this->data->material_data->age) ? $this->data->material_data->age : null;
    }

    public function getTagline()
    {
        return isset($this->data->material_data->tagline) ? $this->data->material_data->tagline : null;
    }
}
