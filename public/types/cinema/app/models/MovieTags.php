<?php

namespace cinema\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class MovieTags
{
    private $movieId = null;

    private $type = null;

    private $rows = null;

    private $limit = null;

    private $page = null;

    private $order = null;

    private $listTemplate = null;

    private $domain = null;

    private $domainObject = null;

    private $lang = null;

    private $active = null;

    public function __construct($movieId = null)
    {
        if (! $movieId) {
            F::error('movieId required to create MoviesTag class');
        }
        $this->movieId = $movieId;
    }

    public function get()
    {
        if (! $this->getLimit()) {
            // F::error('setLimit first');
        }
        if (! $this->getPage()) {
            // F::error('setPage first');
            $this->setPage(1);
        }
        $sql['limit'] = $this->getLimit() ? ('limit '.$sql['from'].','.$this->getLimit()) : '';
        $sql['from'] = ($this->getPage() - 1) * $this->getLimit();
        $sql['order'] = $this->getOrder() ? 'order by '.$this->getOrder() : '';
        $do['joinTags'] = false;
        $sql['joinTags'] = '';
        if ($this->getType()) {
            $do['joinTags'] = true;
            $sql['type'] = 'tags.type = \''.$this->getType().'\'';
        } else {
            $sql['type'] = 1;
        }
        if (is_null($this->getActive())) {
            $sql['active'] = 1;
        } else {
            $do['joinTags'] = true;
            $sql['active'] = $this->getActive() ? 'tags.active = 1' : 'not tags.active';
        }
        if ($do['joinTags']) {
            $sql['joinTags'] = 'join '.F::typetab('tags').' on tags.id = cinema_tags.tag_id';
        }
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS cinema_tags.tag_id as id
			from '.F::typetab('cinema_tags').'
			'.$sql['joinTags'].'
			where
			cinema_tags.cinema_id = \''.$this->getMovieId().'\' and
			'.$sql['type'].' and
			'.$sql['active'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getMovieId()
    {
        return $this->movieId;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function getGenres()
    {
        $this->setType('genre');

        return $this->getList();
    }

    public function getCountries()
    {
        $this->setType('country');

        return $this->getList();
    }

    public function getDirectors()
    {
        $this->setType('director');

        return $this->getList();
    }

    public function getActors()
    {
        $this->setType('actor');

        return $this->getList();
    }

    public function getStudios()
    {
        $this->setType('studio');

        return $this->getList();
    }

    public function getList($args = [])
    {
        if (! $this->getListTemplate()) {
            F::error('setListTemplate first');
        }
        $list = '';
        if (isset($args['arr'])) {
            $arr = $args['arr'];
        } else {
            $arr = $this->get();
        }
        foreach ($arr as $v) {
            if (isset($args['arr'])) {
                $tag = $v->megaweb;
            } else {
                $tag = new Tag($v['id']);
            }
            $t = new Template($this->getListTemplate());
            $t->setLang($this->getLang());
            $t->v('name', [
                'ru' => $tag->getName(),
                'ua' => $tag->getNameUa(),
                'en' => $tag->getNameEn(),
            ]);
            $t->v('tagId', $tag->getId());
            $t->v('type', $tag->getType());
            $t->v('movieId', $this->getMovieId());
            $t->v('tagEmoji', $tag->getEmoji());
            $t->v('nameUrl', $tag->getNameUrl());
            if ($this->getDomainObject()) {
                $tag->setDomain($this->getDomainObject()->getDomain());
                $t->v('tagLink', $this->getDomainObject()->getTagLink($tag));
                $t->v('genreLink', $this->getDomainObject()->getGenreLink($tag));
            } else {
                $t->v('tagLink', '');
                $t->v('genreLink', '');
            }
            $list .= $t->get();
        }

        return $list;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function getDomainObject()
    {
        if (! $this->domainObject) {
            if ($this->getDomain()) {
                $this->setDomainObject(new Domain($this->getDomain()));
            }
        }

        return $this->domainObject;
    }

    public function setDomainObject($domainObject = null)
    {
        $this->domainObject = $domainObject;
        $this->setDomain($this->getDomainObject()->getDomain());
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang = null)
    {
        $this->lang = $lang;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }
}
