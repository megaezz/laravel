<?php

namespace cinema\app\models;

use engine\app\models\Domains as EngineDomains;
use engine\app\models\F;

class Domains extends EngineDomains
{
    private $moviesWithUniqueDescription = null;

    private $useInRedirectToAnyMovie = null;

    public function get()
    {
        if ($this->getLimit()) {
            if (! $this->getPage()) {
                $this->setPage(1);
            }
            $sql['limit'] = 'limit '.($this->getPage() - 1) * $this->getLimit().','.$this->getLimit();
        } else {
            $sql['limit'] = '';
        }
        $sql['order'] = $this->getOrder() ? ('order by '.$this->getOrder()) : '';
        $sql['type'] = $this->getType() ? 'engine_domains.type=\''.$this->getType().'\'' : 1;
        $sql['reborn'] = is_null($this->getReborn()) ? 1 : ($this->getReborn() ? 'engine_domains.reborn' : 'not engine_domains.reborn');
        $sql['showMirrorsFor'] = $this->getShowMirrorsFor() ? ('engine_domains.mirror_of = \''.$this->getShowMirrorsFor().'\'') : 1;
        $sql['ruBlock'] = is_null($this->getRuBlock()) ? 1 : ($this->getRuBlock() ? 'engine_domains.ru_block' : 'not engine_domains.ru_block');
        $sql['movies_with_unique_description'] = is_null($this->getMoviesWithUniqueDescription()) ? 1 : ($this->getMoviesWithUniqueDescription() ? 'domains.movies_with_unique_description' : 'not domains.movies_with_unique_description');
        $sql['useInRedirectToAnyMovie'] = is_null($this->getUseInRedirectToAnyMovie()) ? 1 : ($this->getUseInRedirectToAnyMovie() ? 'domains.useInRedirectToAnyMovie' : 'not domains.useInRedirectToAnyMovie');
        // F::dump($this->getUseInRedirectToAnyMovie());
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS engine_domains.domain
			from '.F::tabname('engine', 'domains').' engine_domains
			join '.F::typetab('domains').' on domains.domain = engine_domains.domain
			where
			'.$sql['type'].' and
			'.$sql['reborn'].' and
			'.$sql['showMirrorsFor'].' and
			'.$sql['ruBlock'].' and
			'.$sql['movies_with_unique_description'].' and
			'.$sql['useInRedirectToAnyMovie'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getMoviesWithUniqueDescription()
    {
        return $this->moviesWithUniqueDescription;
    }

    public function setMoviesWithUniqueDescription($b = null)
    {
        $this->moviesWithUniqueDescription = $b;
    }

    public function setUseInRedirectToAnyMovie($b = null)
    {
        $this->useInRedirectToAnyMovie = $b;
    }

    public function getUseInRedirectToAnyMovie()
    {
        return $this->useInRedirectToAnyMovie;
    }
}
