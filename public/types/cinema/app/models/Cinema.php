<?php

namespace cinema\app\models;

use App\Models\Mailer;
use engine\app\models\F;

class Cinema
{
    private $rewritePricePer1k = null;

    private $customerPricePer1k = null;

    private $serviceLoad = null;

    private $bookMinutes = null;

    private $highRewritePricePer1k = null;

    private $regularRewritePricePer1k = null;

    private $tasks100percent = null;

    private $highPricePercent = null;

    // private $refPercent = null;
    private $refWorker = null;

    private $refCustomer = null;

    private $videocdnDomain = null;

    private $minWorkerPayment = null;

    // сколько шейвим рублей при оплате в долларах
    private $usdRubShave = null;

    private $rewriteExpressFare = null;

    private $customerExpressFare = null;

    private $moderationPricePer1k = null;

    private $descriptionWaitTime = null;

    private $sitemapMoviesLimit = null;

    private $playerjsVersion = null;

    public function __construct()
    {
        $arr = F::query_assoc('
			select rewrite_price_per_1k, customer_price_per_1k, service_load, book_minutes, high_rewrite_price_per_1k, regular_rewrite_price_per_1k, tasks100percent, highPricePercent, videocdnDomain, refWorker, refCustomer, minWorkerPayment, usdRubShave, rewriteExpressFare, customerExpressFare, moderationPricePer1k,
			descriptionWaitTime, sitemapMoviesLimit, playerjsVersion
			from '.F::tabname('cinema', 'configs').'
			;');
        $this->rewritePricePer1k = $arr['rewrite_price_per_1k'];
        $this->customerPricePer1k = $arr['customer_price_per_1k'];
        $this->serviceLoad = $arr['service_load'];
        $this->bookMinutes = $arr['book_minutes'];
        $this->highRewritePricePer1k = $arr['high_rewrite_price_per_1k'];
        $this->regularRewritePricePer1k = $arr['regular_rewrite_price_per_1k'];
        $this->tasks100percent = $arr['tasks100percent'];
        $this->highPricePercent = $arr['highPricePercent'];
        // $this->refPercent = $arr['refPercent'];
        $this->videocdnDomain = $arr['videocdnDomain'];
        $this->refWorker = $arr['refWorker'];
        $this->refCustomer = $arr['refCustomer'];
        $this->minWorkerPayment = $arr['minWorkerPayment'];
        $this->usdRubShave = $arr['usdRubShave'];
        $this->rewriteExpressFare = $arr['rewriteExpressFare'];
        $this->customerExpressFare = $arr['customerExpressFare'];
        $this->moderationPricePer1k = $arr['moderationPricePer1k'];
        $this->descriptionWaitTime = $arr['descriptionWaitTime'];
        $this->sitemapMoviesLimit = empty($arr['sitemapMoviesLimit']) ? F::error('sitemapMoviesLimit required') : $arr['sitemapMoviesLimit'];
        $this->playerjsVersion = $arr['playerjsVersion'];
    }

    public function getRewritePricePer1k()
    {
        return $this->rewritePricePer1k;
    }

    public function setRewritePricePer1k($price = null)
    {
        $this->rewritePricePer1k = $price;
    }

    public function getCustomerPricePer1k()
    {
        return $this->customerPricePer1k;
    }

    // минимально возможный баланс пользователя при котором его задания показываются исполнителям
    public function getCustomerMinimalBalance()
    {
        return $this->getCustomerPricePer1k() / 2;
    }

    public function getServiceLoad()
    {
        // return 10;
        return $this->serviceLoad;
    }

    public function setServiceLoad($percent = null)
    {
        $this->serviceLoad = $percent;
    }

    public function save()
    {
        F::query('update '.F::typetab('configs').' set
			service_load = '.($this->getServiceLoad() ? $this->getServiceLoad() : 0).',
			rewrite_price_per_1k = '.($this->getRewritePricePer1k() ? $this->getRewritePricePer1k() : 0).',
			descriptionWaitTime = '.($this->getDescriptionWaitTime() ? $this->getDescriptionWaitTime() : 0).'
			;');

        return true;
    }

    public function isRewritePriceIncreased()
    {
        if ($this->getServiceLoad() > $this->getHighPricePercent()) {
            return true;
        } else {
            return false;
        }
    }

    // сколько времени есть у рерайтера для выполнения задания
    public function getBookMinutes()
    {
        return $this->bookMinutes;
    }

    public function getHighRewriterPricePer1k()
    {
        return $this->highRewritePricePer1k;
    }

    public function getRegularRewriterPricePer1k()
    {
        return $this->regularRewritePricePer1k;
    }

    public function getTasks100percent()
    {
        return $this->tasks100percent;
    }

    public function setTasks100Percent($q = null)
    {
        $this->tasks100percent = $q;
    }

    public function getHighPricePercent()
    {
        return $this->highPricePercent;
    }

    public function setHighPricePercent($percent = null)
    {
        $this->highPricePercent = $percent;
    }

    // function getRefPercent() {
    // 	return $this->refPercent;
    // }

    public function getRefWorker()
    {
        return $this->refWorker;
    }

    public function getRefCustomer()
    {
        return $this->refCustomer;
    }

    public function getVideocdnDomain()
    {
        return $this->videocdnDomain;
    }

    public static function getExpectedSymbols($from = null, $to = null)
    {
        $def_from = 300;
        $def_to = 1000;
        // $from = null;
        // $to = 200;
        if (is_null($from)) {
            if ($to > $def_from) {
                $from = $def_from;
            } else {
                if (is_null($to)) {
                    $from = $def_from;
                } else {
                    $from = $to;
                }
            }
        }
        if (is_null($to)) {
            if ($from < $def_to) {
                $to = $def_to;
            } else {
                if (is_null($from)) {
                    $to = $def_to;
                } else {
                    $to = $from;
                }
            }
        }
        // $from = is_null($from)?300:$from;
        // $to = is_null($to)?1000:$to;
        if ($to < $from) {
            $from = $to;
        }
        if ($from > $to) {
            $to = $from;
        }

        // f::dump($from);
        return round(($to + $from) / 2);
    }

    public function getMinWorkerPayment()
    {
        return $this->minWorkerPayment;
    }

    public function getUsdRubShave()
    {
        return $this->usdRubShave;
    }

    public function getRewriteExpressPricePer1k()
    {
        return $this->getRewritePricePer1k() + $this->getRewriteExpressFare();
    }

    public function getRewriteExpressFare()
    {
        return $this->rewriteExpressFare;
    }

    public function getCustomerExpressFare()
    {
        return $this->customerExpressFare;
    }

    public function getModerationPricePer1k()
    {
        return $this->moderationPricePer1k;
    }

    public function getDescriptionWaitTime()
    {
        return $this->descriptionWaitTime;
    }

    public function setDescriptionWaitTime($time = null)
    {
        $this->descriptionWaitTime = $time;
    }

    public function sendMail($email = null, $name = null, $subject = null, $body = null, $type = null)
    {
        $mailer = Mailer::where('transport', 'smtp')->where('active', true)->firstOrFail();
        $unsubscribe_link = 'https://cinema.awmzone.net/api/unsubscribe?email='.urlencode($email);

        return F::sendMail($email, $name, $subject, $body, $unsubscribe_link, $mailer);
    }

    public function getSitemapMoviesLimit()
    {
        return $this->sitemapMoviesLimit;
    }

    public function setSitemapMoviesLimit($limit = null)
    {
        $this->sitemapMoviesLimit = $limit;
    }

    public function getPlayerJsVersion()
    {
        return $this->playerjsVersion;
    }
}
