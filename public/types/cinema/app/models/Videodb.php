<?php

namespace cinema\app\models;

use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Videodb as VideodbEloquent;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class Videodb
{
    const iframeDomain = 'vdb9.awmzone1.pro';

    const pjsBase64Separator = '//';

    const pjsBase64Keys = ['bibaiboba', 'dvadolboeba', 'pososikloun'];

    private $id = null;

    private $data = null;

    private $movieId = null;

    private $checked = null;

    private $date = null;

    private $playerTemplate = 'players/videodb/page';

    private $playerjsVersion = null;

    private $isExistsOnDate = null;

    public function __construct($id = null, ?VideodbEloquent $videodbEloquent = null)
    {
        if ($videodbEloquent) {
            $arr = $videodbEloquent->getAttributes();
        } else {
            if (empty($id)) {
                F::error('ID required to create Videodb object');
            }
            $arr = F::query_assoc('
				select id, data, movie_id, checked, date, is_exists_on_date
				from '.F::typetab('videodb').'
				where id = \''.$id.'\'
				;');
        }
        if (! $arr) {
            F::error('Videodb doesn\'t exist');
        }
        $this->id = $arr['id'];
        /* если объект формируется из eloquent модели, то data уже будет декодирована, поэтому проверяем */
        $this->data = is_string($arr['data']) ? json_decode($arr['data']) : $arr['data'];
        $this->movieId = $arr['movie_id'];
        $this->checked = $arr['checked'] ? true : false;
        $this->date = $arr['date'];
        $this->isExistsOnDate = $arr['is_exists_on_date'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getMovieId()
    {
        return $this->movieId;
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function getDate()
    {
        return $this->date;
    }

    public static function getCdnDomain()
    {
        return Config::cached()->videodb_domain;
    }

    public static function getIframeDomain()
    {
        return static::iframeDomain;
    }

    public function getTranslation()
    {
        return $this->getData()->translation->title ?? null;
    }

    public function getPlayer()
    {
        // Engine::setDebug(false);
        if (! $this->getMovieId()) {
            F::error('movieId required to use Videodb::getPlayer');
        }
        $movie = new Movie($this->getMovieId());
        $data = new \stdClass;
        $data->movieTitle = $movie->getTitleRu();
        $data->movieYear = $movie->getYear();
        $data->movieType = $movie->getTypeRu(true);
        $data->playlist = $this->getPlaylist();
        // все что ниже - для старого плеера
        $data->movieId = $movie->getId();
        $data->videodbId = $this->getId();
        $data->cdnPath = Config::cached()->zerocdnService->getSignedUrl($this->getData()->path, 'ip');
        $data->translations = $this->getTranslations();
        $data->seasonsEpisodes = $this->getSeasonsEpisodes();
        $data->season = $this->getSeason();
        $data->episode = $this->getEpisode();
        $t = new Template($this->getPlayerTemplate());
        // $t->v('jsonData',json_encode($data,JSON_UNESCAPED_UNICODE));
        $t->v('base64JsonData', base64_encode(json_encode($data)));
        $t->v('jsonRedirect', json_encode(false));
        $t->v('movieId', $movie->getId());
        $t->v('jsonStrip', json_encode(empty($_GET['strip']) ? false : true));
        $t->v('webWapScript', '');
        $t->v('playerjsVersion', $this->getPlayerjsVersion());
        F::setPageVars($t);

        return $t->get();
    }

    public function getTranslations()
    {
        if (! $this->getMovieId()) {
            return false;
        }
        $arr = F::query_arr('
			select distinct translation
			from '.F::typetab('videodb').'
			where movie_id = \''.$this->getMovieId().'\'
			/* order by season desc, episode desc */
			;');
        //  если 0 или 1 элемент, не показываем меню выбора
        if (count($arr) < 2) {
            return false;
        }
        $list = '';
        foreach ($arr as $v) {
            // $videodb = new Videodb($v['id']);
            $t = new Template('players/videodb/items/translation');
            // $t->v('videodbId',$v['id']);
            $t->v('translation', $v['translation']);
            $selected = ($this->getTranslation() == $v['translation']) ? 'selected' : '';
            $t->v('selected', $selected);
            $list .= $t->get();
        }

        return $list;
    }

    public function getSeasonsEpisodes()
    {
        if (! $this->getMovieId()) {
            return false;
        }
        $arr = F::query_arr('
			select id, cast(season as unsigned) as season_int, cast(episode as unsigned) as episode_int
			from '.F::typetab('videodb').'
			where movie_id = \''.$this->getMovieId().'\'
			and translation = \''.F::checkstr($this->getTranslation()).'\'
			order by season_int desc, episode_int desc
			;');
        //  если 0 или 1 элемент, не показываем меню выбора
        if (count($arr) < 2) {
            return false;
        }
        $list = '';
        foreach ($arr as $v) {
            // $videodb = new Videodb($v['id']);
            $t = new Template('players/videodb/items/season_episode');
            $t->v('id', $v['id']);
            $t->v('season', $v['season_int']);
            $t->v('episode', $v['episode_int']);
            $selected = ($this->getId() == $v['id']) ? 'selected' : '';
            $t->v('selected', $selected);
            $list .= $t->get();
        }

        return $list;
    }

    public function getPlaylist()
    {
        if (! $this->getMovieId()) {
            return false;
        }
        // получаем все
        $arr = F::query_arr('
			select translation, cast(season as unsigned) as season_int, cast(episode as unsigned) as episode_int, data
			from '.F::typetab('videodb').'
			left join '.F::typetab('translations').' on translations.videodb_name = videodb.translation
			where videodb.movie_id = \''.$this->getMovieId().'\'
			/* order by season_int,episode_int,translation */
			order by season_int,episode_int,translations.order desc,translation
			;');
        $seasons = [];
        foreach ($arr as $v) {
            $seasons[$v['season_int']][$v['episode_int']][$v['translation']] = json_decode($v['data']);
        }
        $seasonsArr = [];
        foreach ($seasons as $season => $episodes) {
            $seasonObj = new \stdClass;
            $seasonObj->title = 'Сезон '.$season;
            $episodesArr = [];
            foreach ($episodes as $episode => $translations) {
                $episodeObj = new \stdClass;
                $episodeObj->title = 'Серия '.$episode;
                // текущий сезон и эпизод, чтобы получать в плеере и формировать название
                $episodeObj->id = new \stdClass;
                $episodeObj->id->season = $season;
                $episodeObj->id->episode = $episode;
                $sourcesList = '';
                foreach ($translations as $translation => $data) {
                    $sourcesList .= '{'.$translation.'}'.Config::cached()->zerocdnService->getSignedUrl($data->path, 'ip').'hls.m3u8;';
                }
                $episodeObj->file = static::pjsBase64Encrypt($sourcesList);
                $episodesArr[] = $episodeObj;
            }
            $seasonObj->folder = $episodesArr;
            $seasonsArr[] = $seasonObj;
        }

        return $seasonsArr;
    }

    public function getMaxQuality()
    {
        return $this->getData()->max_quality ?? null;
    }

    public function getSeason()
    {
        return $this->getData()->content_object->season->num ?? null;
    }

    public function getEpisode()
    {
        return $this->getData()->content_object->num ?? null;
    }

    public function getShowId()
    {
        return $this->getData()->content_object->tv_series_id ?? null;
    }

    // function getTitleRu() {
    // 	return $this->getData()->content_object->tv_series_id??null;
    // }

    public function getKinopoiskId()
    {
        $series_id = $this->getData()->content_object->tv_series->kinopoisk_id ?? null;
        $episode_id = $this->getData()->content_object->kinopoisk_id ?? null;
        $kinopoisk_id = $series_id ? $series_id : $episode_id;

        return $kinopoisk_id;
    }

    public function getImdbId()
    {
        $series_id = $this->getData()->content_object->tv_series->imdb_id ?? null;
        $episode_id = $this->getData()->content_object->imdb_id ?? null;
        $imdb_id = $series_id ? $series_id : $episode_id;

        return str_replace('tt', '', ($imdb_id ?? ''));
    }

    public function getTitleRu()
    {
        $series_title = $this->getData()->content_object->tv_series->ru_title ?? null;
        $episode_title = $this->getData()->content_object->ru_title ?? null;
        $title = $series_title ? $series_title : $episode_title;

        return $title;
    }

    public function getTitleEn()
    {
        $series_title = $this->getData()->content_object->tv_series->orig_title ?? null;
        $episode_title = $this->getData()->content_object->orig_title ?? null;
        $title = $series_title ? $series_title : $episode_title;

        return $title;
    }

    public function getType()
    {
        return isset($this->getData()->content_object->tv_series) ? 'serial' : 'movie';
    }

    public static function getUpdatesOnDay($date = null)
    {
        if (! $date) {
            F::error('getUpdatedOnDay required date');
        }
        $from = new \DateTime($date);
        $to = clone $from;
        $to->modify('+1 day');
        $arr = F::query_arr('
			select movie_id,max(season) as season, max(episode) as episode
			from '.F::typetab('videodb').'
			where
			date between \''.$from->format('Y-m-d').'\' and \''.$to->format('Y-m-d').'\'
			and movie_id is not null
			and season is not null and episode is not null
			group by movie_id
			;');

        return $arr;
    }

    public function getPlayerTemplate()
    {
        return $this->playerTemplate;
    }

    public function setPlayerTemplate($template = null)
    {
        $this->playerTemplate = $template;
    }

    public function setChecked($b = null)
    {
        $this->checked = $b;
    }

    public function setMovieId($movieId = null)
    {
        $this->movieId = $movieId;
    }

    public function save()
    {
        F::query('
			update '.F::typetab('videodb').'
			set
			checked = '.($this->getChecked() ? 1 : 0).',
			movie_id = '.($this->getMovieId() ? $this->getMovieId() : 'null').',
			max_quality = '.($this->getMaxQuality() ? '\''.$this->getMaxQuality().'\'' : 'null').',
			translation = '.($this->getTranslation() ? '\''.F::escape_string($this->getTranslation()).'\'' : 'null').',
			season = '.($this->getSeason() ? '\''.$this->getSeason().'\'' : 'null').',
			episode = '.($this->getEpisode() ? '\''.$this->getEpisode().'\'' : 'null').',
			videodb_show_id = '.($this->getShowId() ? '\''.$this->getShowId().'\'' : 'null').',
			kinopoisk_id = '.($this->getKinopoiskId() ? '\''.$this->getKinopoiskId().'\'' : 'null').'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }

    public static function isExists($id = null)
    {
        if (! $id) {
            F::error('Id required to Videodb::isExists');
        }
        $arr = F::query_assoc('select 1 from '.F::typetab('videodb').' where id = \''.$id.'\';');

        return $arr ? true : false;
    }

    public static function add($videodb = null)
    {
        $id = $videodb->id ?? F::error('Videodb->id required to use Videodb:add');
        F::query('
			insert into '.F::typetab('videodb').' set
			id = \''.$id.'\',
			data = \''.F::escape_string(json_encode($videodb, JSON_UNESCAPED_UNICODE)).'\'
			;');

        return $id;
    }

    public function getPlayerjsVersion()
    {
        if (! $this->playerjsVersion) {
            $this->setPlayerjsVersion((new Cinema)->getPlayerjsVersion());
        }

        return $this->playerjsVersion;
    }

    public function setPlayerjsVersion($version = null)
    {
        $this->playerjsVersion = $version;
    }

    public static function getIframeSign($movieId = null)
    {
        return md5(request()->ip().Config::cached()->zerocdn_credentials->apiSecret.$movieId);
    }

    public static function pjsBase64Encrypt($string)
    {
        $pjsBase64Separator = static::pjsBase64Separator;
        $pjsBase64Keys = static::pjsBase64Keys;
        $result = base64_encode($string);
        if (is_array($pjsBase64Keys)) {
            for ($i = 0; $i < count($pjsBase64Keys); $i++) {
                if ($pjsBase64Keys[$i] != '') {
                    $tmp = rand(0, strlen($result));
                    $result = substr($result, 0, $tmp).$pjsBase64Separator.base64_encode($pjsBase64Keys[$i]).substr($result, $tmp);
                }
            }
        }

        return '#2'.$result;
    }

    public static function pjsBase64Decrypt($string)
    {
        $pjsBase64Separator = static::pjsBase64Separator;
        $pjsBase64Keys = static::pjsBase64Keys;
        $result = $string;
        if (is_array($pjsBase64Keys)) {
            for ($i = count($pjsBase64Keys); $i >= 0; $i--) {
                if ($pjsBase64Keys[$i] != '') {
                    $result = str_replace($pjsBase64Separator.base64_encode($pjsBase64Keys[$i]), '', $result);
                }
            }
        }
        $result = base64_decode(substr($result, 2));

        return $result;
    }

    public function setIsExistsOnDate($b = null)
    {
        if ($b) {
            F::query('
				update '.F::typetab('videodb').'
				set is_exists_on_date = current_timestamp
				where id = \''.$this->getId().'\''
            );
        }
    }

    public function getIsExistsOnDate()
    {
        return $this->isExistsOnDate;
    }
}
