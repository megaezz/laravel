<?php

namespace cinema\app\models;

use cinema\app\models\eloquent\Hdvb;
use cinema\app\models\eloquent\Movie as MovieEloquent;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class Movie
{
    private $id = null;

    private $title_en = null;

    private $title_ru = null;

    private $kinopoisk_id = null;

    private $unique_id = null;

    private $poster = null;

    private $year = null;

    private $deleted = null;

    private $add_date = null;

    private $checked = null;

    private $added_at = null;

    private $tags = null;

    private $mw_deleted = null;

    private $kinopoisk_rating = null;

    private $kinopoisk_votes = null;

    private $kinopoisk_rating_sko = null;

    private $imdb_rating = null;

    private $imdb_votes = null;

    private $imdb_rating_sko = null;

    private $type = null;

    private $description = null;

    private $poster_url = null;

    private $rkn_block = null;

    private $embed = null;

    private $embedTrailer = null;

    private $copyPosterUrl = null;

    private $directLink = null;

    private $doUpdate = null;

    private $rating_sko = null;

    private $myshows_id = null;

    private $status = null;

    private $started = null;

    private $ended = null;

    private $episodesDataListTemplate = null;

    private $age = null;

    private $sourceType = null;

    private $tagline = null;

    private $hdvb_4k = null;

    private $mainPlayer = null;

    private $blockStr = null;

    private $collaps = null;

    private $videocdn = null;

    private $videodb = null;

    private $imdb_id = null;

    private $lordDescription = null;

    private $youtube_id = null;
    // private $genres = array();

    public function __construct($id = null, ?MovieEloquent $movieEloquent = null)
    {
        if ($movieEloquent) {
            $arr = $movieEloquent->getAttributes();
        } else {
            if (! $id) {
                F::error('Movie ID is required to create Movie class');
            }
            // получаем токен элемента с макс кол-вом сезонов и эпизодов
            $arr = F::query_assoc('select id,title_en,title_ru,kinopoisk_id,imdb_id,
				mw_unique_id,poster,year,deleted, add_date, added_at, type,
				checked, mw_deleted, kinopoisk_rating, kinopoisk_votes, imdb_rating, imdb_votes,
				description,kinopoisk_rating_sko,imdb_rating_sko,rkn_block,embed,embed_trailer,
				copy_poster_url,direct,do_update,rating_sko,myshows_id,status,started,ended,age,
				source_type,tagline, lordDescription, hdvb_4k, mainPlayer, blockStr, collaps, videocdn, videodb,
				youtube_id
				from '.F::tabname('cinema', 'cinema').'
				where id=\''.$id.'\'
				;');
        }
        if (empty($arr)) {
            F::error('Фильм не найден');
        }
        $this->id = $arr['id'];
        $this->title_en = $arr['title_en'];
        $this->title_ru = $arr['title_ru'];
        $this->kinopoisk_id = $arr['kinopoisk_id'];
        $this->unique_id = $arr['mw_unique_id'];
        $this->poster = $arr['poster'] ? true : false;
        $this->year = $arr['year'];
        $this->deleted = $arr['deleted'] ? true : false;
        $this->add_date = $arr['add_date'];
        $this->added_at = $arr['added_at'];
        $this->checked = $arr['checked'] ? true : false;
        $this->mw_deleted = $arr['mw_deleted'] ? true : false;
        $this->kinopoisk_rating = $arr['kinopoisk_rating'];
        $this->kinopoisk_votes = $arr['kinopoisk_votes'];
        $this->imdb_rating = $arr['imdb_rating'];
        $this->imdb_votes = $arr['imdb_votes'];
        $this->type = $arr['type'];
        $this->poster_url = Engine::getStaticUrl().'/types/cinema/posters/'.$this->id.'.jpg';
        $this->description = $arr['description'];
        $this->rkn_block = $arr['rkn_block'] ? true : false;
        $this->embed = $arr['embed'];
        $this->embedTrailer = $arr['embed_trailer'];
        $this->copyPosterUrl = $arr['copy_poster_url'];
        $this->directLink = $arr['direct'];
        $this->kinopoisk_rating_sko = $arr['kinopoisk_rating_sko'];
        $this->imdb_rating_sko = $arr['imdb_rating_sko'];
        $this->doUpdate = $arr['do_update'];
        $this->rating_sko = $arr['rating_sko'];
        $this->myshows_id = $arr['myshows_id'];
        $this->status = $arr['status'];
        $this->started = $arr['started'];
        $this->ended = $arr['ended'];
        $this->age = $arr['age'];
        $this->sourceType = $arr['source_type'];
        $this->tagline = $arr['tagline'];
        $this->lordDescription = $arr['lordDescription'];
        $this->hdvb_4k = $arr['hdvb_4k'] ? true : false;
        $this->mainPlayer = $arr['mainPlayer'];
        $this->blockStr = $arr['blockStr'];
        $this->collaps = $arr['collaps'] ? true : false;
        $this->videocdn = $arr['videocdn'] ? true : false;
        $this->videodb = $arr['videodb'] ? true : false;
        $this->imdb_id = $arr['imdb_id'];
        $this->youtube_id = $arr['youtube_id'];
        // dump($this);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTypeRu($upper = null)
    {
        if ($upper) {
            $type_arr = ['serial' => 'Сериал', 'movie' => 'Фильм', 'trailer' => 'Трейлер', 'youtube' => 'Youtube'];
        } else {
            $type_arr = ['serial' => 'сериал', 'movie' => 'фильм', 'trailer' => 'трейлер', 'youtube' => 'youtube'];
        }
        $typeRu = isset($type_arr[$this->getType()]) ? $type_arr[$this->getType()] : '';

        return $typeRu;
    }

    public function getTypeUa($upper = null)
    {
        if ($upper) {
            $type_arr = ['serial' => 'Серiал', 'movie' => 'Фiльм', 'trailer' => 'Трейлер', 'youtube' => 'Youtube'];
        } else {
            $type_arr = ['serial' => 'серiал', 'movie' => 'фiльм', 'trailer' => 'трейлер', 'youtube' => 'youtube'];
        }
        $typeRu = isset($type_arr[$this->getType()]) ? $type_arr[$this->getType()] : '';

        return $typeRu;
    }

    public function getTypeEn($upper = null)
    {
        if ($upper) {
            $type_arr = ['serial' => 'Series', 'movie' => 'Movie', 'trailer' => 'Trailer', 'youtube' => 'Youtube'];
        } else {
            $type_arr = ['serial' => 'Series', 'movie' => 'Movie', 'trailer' => 'Trailer', 'youtube' => 'youtube'];
        }
        $typeEn = isset($type_arr[$this->getType()]) ? $type_arr[$this->getType()] : '';

        return $typeEn;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getAddDate()
    {
        return $this->add_date;
    }

    public function getAddedAt()
    {
        return $this->added_at;
    }

    public function getTitleEn()
    {
        return $this->title_en;
    }

    public function getTitleRu()
    {
        return $this->title_ru;
    }

    public function getKinopoiskId()
    {
        return $this->kinopoisk_id;
    }

    // function getPlayer() {
    // 	$m = new Moonwalk($this->getMwId());
    // 	return $m->getPlayer();
    // }

    public function getId()
    {
        return $this->id;
    }

    public function getPoster()
    {
        return $this->poster;
    }

    public function getPosterOrPlaceholder()
    {
        return $this->poster ? $this->getPosterUrl() : '/types/cinema/template/images/poster_none.png';
    }

    public function getPosterWebPOrPlaceholder()
    {
        return $this->poster ? $this->getPosterWebPUrl() : '/types/cinema/template/images/poster_none.webp';
    }

    public function getPosterWebPUrl()
    {
        return Engine::getStaticUrl().'/types/cinema/posters-webp/'.$this->id.'.webp';
    }

    public function getKinopoiskPoster()
    {
        $poster = null;
        if ($this->getKinopoiskId()) {
            // $url = 'https://st.kp.yandex.net/images/film_big/'.$this->getKinopoiskId().'.jpg';
            $url = 'https://st.kp.yandex.net/images/film_iphone/iphone360_'.$this->getKinopoiskId().'.jpg';
            // не следуем по редиректу, чтобы не попадать на заглушку для отсутствующего постера
            // $context = stream_context_create(
            // 	array (
            // 		'http' => array (
            // 			'follow_location' => false
            // 		)
            // 	)
            // );
            // dump($url);
            // $file = @file_get_contents($url,false,$context);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_NOBODY, true); // do not include response body
            curl_setopt($ch, CURLOPT_TIMEOUT, 2);
            curl_exec($ch);
            $effectiveUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
            // F::dump($effectiveUrl);
            // F::pre($file);
            // если файл меньше 200, значит файл содержит "302 found", что значит что должен был произойти редирект на заглушку, значит постер отсутствует (больше нельзя исползовать этот способ потому что теперь не тольок заглушки редиректят но еще и сами постеры и пришлось отказаться от follow_location => false который использовался для проверки)
            // $poster = (strlen($file)>200)?$url:null;
            // новый спосо: если файл содержит W5M0MpCehiHzreSzNTczkc9d значит отдается заглушка, корявый способ, сделать потом по другому
            $poster = ($effectiveUrl == 'https://st.kp.yandex.net/images/no-poster.gif') ? null : $effectiveUrl;
            // f::dump($poster);
        }

        return $poster;
    }

    public function setPoster($poster = null)
    {
        $this->poster = $poster;
    }

    public function setYear($year = null)
    {
        $this->year = $year;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setDeleted($b = null)
    {
        $this->deleted = $b;
    }

    public function getDeleted()
    {
        return $this->deleted;
    }

    // function addGenre($genre = null) {
    // 	$this->genres[] = $genre;
    // }

    public function save()
    {
        $this->calcSkoRating();
        F::query('update '.F::typetab('cinema').'
			set
			title_en = '.($this->getTitleEn() ? ('\''.F::escape_string($this->getTitleEn()).'\'') : 'null').',
			title_ru = '.($this->getTitleRu() ? ('\''.F::escape_string($this->getTitleRu()).'\'') : 'null').',
			kinopoisk_id = '.($this->getKinopoiskId() ? ('\''.$this->getKinopoiskId().'\'') : 'null').',
			imdb_id = '.($this->getImdbId() ? ('\''.F::escape_string($this->getImdbId()).'\'') : 'null').',
			poster = '.($this->getPoster() ? 1 : 0).',
			year = '.($this->getYear() ? ('\''.$this->getYear().'\'') : 'null').',
			deleted = '.($this->getDeleted() ? 1 : 0).',
			checked = '.($this->getChecked() ? 1 : 0).',
			mw_unique_id = '.($this->getUniqueId() ? ('\''.$this->getUniqueId().'\'') : 'null').',
			added_at = '.($this->getAddedAt() ? ('\''.$this->getAddedAt().'\'') : 'null').',
			mw_deleted = '.($this->getMwDeleted() ? 1 : 0).',
			kinopoisk_rating = '.($this->getKinopoiskRating() ? ('\''.$this->getKinopoiskRating().'\'') : 'null').',
			kinopoisk_votes = '.($this->getKinopoiskVotes() ? ('\''.$this->getKinopoiskVotes().'\'') : 'null').',
			kinopoisk_rating_sko = '.($this->getKinopoiskRatingSko() ? ('\''.$this->getKinopoiskRatingSko().'\'') : 'null').',
			imdb_rating = '.($this->getImdbRating() ? ('\''.$this->getImdbRating().'\'') : 'null').',
			imdb_votes = '.($this->getImdbVotes() ? ('\''.$this->getImdbVotes().'\'') : 'null').',
			imdb_rating_sko = '.($this->getImdbRatingSko() ? ('\''.$this->getImdbRatingSko().'\'') : 'null').',
			type = '.($this->getType() ? ('\''.$this->getType().'\'') : 'null').',
			description = '.($this->getDescription() ? ('\''.F::escape_string($this->getDescription()).'\'') : 'null').',
			copy_poster_url = '.($this->getCopyPosterUrl() ? ('\''.F::escape_string($this->getCopyPosterUrl()).'\'') : 'null').',
			rating_sko = '.($this->getRatingSko() ? ('\''.$this->getRatingSko().'\'') : 'null').',
			myshows_id = '.($this->getMyshowsId() ? ('\''.$this->getMyshowsId().'\'') : 'null').',
			status = '.($this->getStatus() ? ('\''.$this->getStatus().'\'') : 'null').',
			started = '.($this->getStarted() ? ('\''.$this->getStarted().'\'') : 'null').',
			ended = '.($this->getEnded() ? ('\''.$this->getEnded().'\'') : 'null').',
			age = '.($this->getAge() ? ('\''.$this->getAge().'\'') : 'null').',
			source_type = '.($this->getSourceType() ? ('\''.$this->getSourceType().'\'') : 'null').',
			tagline = '.($this->getTagline() ? ('\''.F::escape_string($this->getTagline()).'\'') : 'null').',
			lordDescription = '.($this->getLordDescription() ? ('\''.F::escape_string($this->getLordDescription()).'\'') : 'null').',
			hdvb_4k = '.($this->getHdvb4k() ? 1 : 0).',
			mainPlayer = '.($this->getMainPlayer() ? ('\''.$this->getMainPlayer().'\'') : 'null').',
			collaps = '.($this->getCollaps() ? 1 : 0).',
			videocdn = '.($this->getVideocdn() ? 1 : 0).',
			videodb = '.($this->getVideodb() ? 1 : 0).',
			youtube_id = '.($this->getYoutubeId() ? ('\''.F::escape_string($this->getYoutubeId()).'\'') : 'null').'
			where id = \''.$this->getId().'\'
			;');
        // сохраняем тэги
        // узнаем какие тэги уже есть в базе для этого создаем новый объект
        $movie = new Movie($this->getId());
        $arr = array_diff($this->getTags(), $movie->getTags());
        // F::dump($arr);
        foreach ($arr as $v) {
            F::query('insert into '.F::typetab('cinema_tags').'
				set cinema_id = \''.$this->getId().'\',
				tag_id = \''.$v.'\'
				;');
            // on duplicate key update tag_id = tag_id
        }
        // удаляем теги ненужные
        $arr = array_diff($movie->getTags(), $this->getTags());
        foreach ($arr as $v) {
            F::query('
				delete from '.F::typetab('cinema_tags').'
				where cinema_id = \''.$this->getId().'\' and tag_id = \''.$v.'\'
				;');
            // on duplicate key update tag_id = tag_id
        }

        return true;
    }

    public static function getIdByUniqueId($unique_id = null)
    {
        if (! $unique_id) {
            return null;
        }
        $movies = new Movies;
        $movies->setLimit(1);
        $movies->setPage(1);
        $movies->setUniqueId($unique_id);
        $movies->setIncludeNullType(true);
        $movies_arr = $movies->get();
        $movie_id = isset($movies_arr[0]['id']) ? $movies_arr[0]['id'] : null;

        return $movie_id;
    }

    public static function getIdByKinopoiskId($kinopoiskId = null)
    {
        if (! $kinopoiskId) {
            return false;
        }
        $movies = new Movies;
        $movies->setLimit(1);
        $movies->setPage(1);
        $movies->setKinopoiskId($kinopoiskId);
        $movies->setIncludeNullType(true);
        $movies_arr = $movies->get();
        $movie_id = isset($movies_arr[0]['id']) ? $movies_arr[0]['id'] : null;

        return $movie_id;
    }

    public static function getIdByImdbId($imdbId = null)
    {
        if (! $imdbId) {
            return false;
        }
        $movies = new Movies;
        $movies->setLimit(1);
        $movies->setPage(1);
        $movies->setImdbId($imdbId);
        $movies->setIncludeNullType(true);
        $movies_arr = $movies->get();
        $movie_id = isset($movies_arr[0]['id']) ? $movies_arr[0]['id'] : null;

        return $movie_id;
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function setChecked($b = null)
    {
        $this->checked = $b;
    }

    // добавляем новый тэг если он еще не проставлен
    public function addTag($tagId = null)
    {
        // проверяем существует ли такой тэг в массиве, заодно подгружая тэги в переменную $this->tags используя $this->getTags();
        if ($tagId and ! in_array($tagId, $this->getTags())) {
            // добавляем новый тэг
            $this->tags[] = $tagId;

            return true;
        }

        return false;
    }

    public function addTagByName($tagName = null)
    {
        $tagId = Tag::getIdByName($tagName);

        return $this->addTag($tagId);
    }

    // удаляем тэг если он существует
    public function removeTag($tagId = null)
    {
        // проверяем существует ли такой тэг в массиве, заодно подгружая тэги в переменную $this->tags используя $this->getTags();
        if (in_array($tagId, $this->getTags())) {
            // удаляем тэг
            foreach ($this->getTags() as $k => $tag) {
                if ($tag == $tagId) {
                    unset($this->tags[$k]);
                }
            }

            return true;
        }

        return false;
    }

    public function getTags()
    {
        if (! is_null($this->tags)) {
            return $this->tags;
        }
        $mt = new MovieTags($this->getId());
        $tags = [];
        foreach ($mt->get() as $v) {
            $tags[] = $v['id'];
        }
        $this->tags = $tags;

        return $this->tags;
    }

    public function getTagsNames()
    {
        $arr = $this->getTags();
        $tags = [];
        foreach ($arr as $v) {
            $tag = new Tag($v['id']);
            $tags[] = $tag->getName();
        }

        return $tags;
    }

    public function setUniqueId($id = null)
    {
        $this->unique_id = $id;
    }

    public function getUniqueId()
    {
        return $this->unique_id;
    }

    public static function add()
    {
        F::query('insert into '.F::typetab('cinema').' set id = null;');
        // $id = Engine::getSqlConnection()->insert_id;
        // пришлось так ухищриться, т.к. на сервере часто возвращался нулевой insert_id не понятно почему
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));

        return $id;
    }

    public function delete()
    {
        F::query('delete from '.F::typetab('cinema').' where id = \''.$this->getId().'\';');

        return true;
    }

    public function setTitleRu($title = null)
    {
        $this->title_ru = $title;
    }

    public function setTitleEn($title = null)
    {
        $this->title_en = $title;
    }

    public function setAddedAt($date = null)
    {
        $this->added_at = $date;
    }

    public function setKinopoiskId($id = null)
    {
        $this->kinopoisk_id = $id;
    }

    public function setMwDeleted($b = null)
    {
        $this->mw_deleted = $b;
    }

    public function getMwDeleted()
    {
        return $this->mw_deleted;
    }

    public function getKinopoiskRating()
    {
        return $this->kinopoisk_rating;
    }

    public function setKinopoiskRating($v = null)
    {
        $this->kinopoisk_rating = $v;
    }

    public function getKinopoiskRatingSko()
    {
        return $this->kinopoisk_rating_sko;
    }

    public function setKinopoiskRatingSko($v = null)
    {
        $this->kinopoisk_rating_sko = $v;
    }

    public function getKinopoiskVotes()
    {
        return $this->kinopoisk_votes;
    }

    public function setKinopoiskVotes($v = null)
    {
        $this->kinopoisk_votes = $v;
    }

    public function getImdbRating()
    {
        return $this->imdb_rating;
    }

    public function setImdbRating($v = null)
    {
        $this->imdb_rating = $v;
    }

    public function getImdbRatingSko()
    {
        return $this->imdb_rating_sko;
    }

    public function setImdbRatingSko($v = null)
    {
        $this->imdb_rating_sko = $v;
    }

    public function getRatingSko()
    {
        return $this->rating_sko;
    }

    public function setRatingSko($v = null)
    {
        $this->rating_sko = $v;
    }

    public function getImdbVotes()
    {
        return $this->imdb_votes;
    }

    public function setImdbVotes($v = null)
    {
        $this->imdb_votes = $v;
    }

    public function getPosterUrl()
    {
        return $this->poster_url;
    }

    public function getTitleRuUrl()
    {
        $str = str_replace([' '], '+', $this->getTitleRu());
        $str = str_replace(['&', '%', '?', '#', ':', '/', '"', '\'', '>', '<'], '', $str);

        return trim($str);
    }

    public function getTitleRuTranslitUrl()
    {
        return F::translitRu($this->getTitleRuUrl());
    }

    public function getTitleEnUrl()
    {
        $str = str_replace([' '], '+', (string) $this->getTitleEn());
        $str = str_replace(['&', '%', '?', '#', ':', '/', '"', '\'', '>', '<'], '', (string) $str);

        return trim($str);
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description = null)
    {
        $this->description = $description;
    }

    public function addEpisode($season = null, $episode = null, $translator = null)
    {
        if (is_null($episode)) {
            F::error('Specify episode');
            // F::dump($episode_name);
        }
        if (is_null($season)) {
            F::error('Specify season');
        }
        F::query('
			insert into '.F::typetab('movie_episodes').'
			set
			movie_id = \''.$this->getId().'\',
			season = \''.$season.'\',
			episode = \''.$episode.'\',
			translator = \''.$translator.'\'
			on duplicate key update movie_id = movie_id
			;');

        return true;
    }

    public function deleteEpisodes($translator = null)
    {
        if (is_null($translator)) {
            F::error('Specify translator for deleteEpisodes');
        }
        F::query('
			delete from '.F::typetab('movie_episodes').'
			where movie_id = \''.$this->getId().'\' and translator = \''.$translator.'\'
			;');

        return true;
    }

    public function truncateMovieEpisodes()
    {
        F::query('truncate '.F::typetab('movie_episodes').';');

        return true;
    }

    public function getLastEpisodeAirDate()
    {
        $arr = F::query_assoc('
			select air_date from '.F::typetab('movie_episodes_data').'
			where movie_id = '.$this->getId().' and air_date <= current_date
			order by air_date desc
			limit 1
			;');

        return $arr['air_date'] ?? null;
    }

    public function getLastEpisodesOfSeason($season = null, $count = null)
    {
        if (! $count) {
            F::error('Specify last episodes count');
        }
        if (is_null($season)) {
            F::error('Specify season number for getLastEpisodesOfSeason');
        }
        $arr = F::query_arr('
			select episode from '.F::typetab('movie_episodes_data').'
			where movie_id = '.$this->getId().' and season = \''.$season.'\' and air_date <= date_add(current_date, interval 7 day)
			order by air_date desc, episode desc
			limit 3
			;');
        $arr = array_reverse($arr);
        $last3 = [];
        foreach ($arr as $v) {
            $last3[] = $v['episode'];
        }

        /*
        // переворачиваем массив эпизодов сезона
        $arr = array_reverse($this->getEpisodesOfSeason($season));
        $i = 0;
        $last3 = array();
        // получаем последних 3 эпизода
        foreach ($arr as $v) {
            $last3[] = $v;
            $i++;
            if ($i == $count) {
                break;
            }
        }
        // еще раз переворачиваем обратно и возвращаем эпизоды
        return array_reverse($last3);
        */
        return $last3;
    }

    public function getEpisodesOfSeason($season = null)
    {
        $arr = F::query_arr('
			select distinct episode from '.F::typetab('movie_episodes_data').'
			where movie_id = \''.$this->getId().'\' and season = \''.$season.'\'
			;');
        $episodes = [];
        foreach ($arr as $v) {
            $episodes[] = $v['episode'];
        }

        return $episodes;
    }

    public function getEpisodesSum()
    {
        $arr = F::query_arr('
			select count(*) from '.F::typetab('movie_episodes_data').'
			where movie_id = \''.$this->getId().'\'
			;');

        return count($arr);
    }

    public function getLastSeason()
    {
        $arr = F::query_assoc('
			select max(season) as last_season from '.F::typetab('movie_episodes_data').'
			where movie_id = \''.$this->getId().'\'
			;');

        return $arr['last_season'];
    }

    // реализовать через теги
    public function getTranslators()
    {
        $arr = F::query_arr('
			select distinct translator from '.F::typetab('movie_episodes').'
			where movie_id = \''.$this->getId().'\'
			;');
        $translators = [];
        foreach ($arr as $v) {
            $translators[] = $v['translator'];
        }

        return $translators;
    }

    public function getRknBlock()
    {
        return $this->rkn_block;
    }

    public function setRknBlock($b = null)
    {
        $this->rkn_block = $b;
    }

    public function getSeasons()
    {
        $arr = F::query_arr('
			select distinct season from '.F::typetab('movie_episodes_data').'
			where movie_id = \''.$this->getId().'\'
			order by season
			;');
        $seasons = [];
        foreach ($arr as $v) {
            $seasons[] = $v['season'];
        }

        return $seasons;
    }

    public function getEpisodesList()
    {
        $seasons = $this->getSeasons();
        $seasons = array_reverse($seasons);
        $list = '';
        foreach ($seasons as $season) {
            $episodes = $this->getEpisodesOfSeason($season);
            $episodes = array_reverse($episodes);
            foreach ($episodes as $episode) {
                $list .= '<li>'.$season.' сезон '.$episode.' серия</li>';
            }
        }

        return $list;
    }

    public function getEmbed()
    {
        return $this->embed;
    }

    public function setEmbed($embed = null)
    {
        $this->embed = null;
    }

    public function getEmbedTrailer()
    {
        return $this->embedTrailer;
    }

    public function setEmbedTrailer($embed = null)
    {
        return $this->embedTrailer = $embed;
    }

    public function getCopyPosterUrl()
    {
        return $this->copyPosterUrl;
    }

    public function setCopyPosterUrl($url = null)
    {
        $this->copyPosterUrl = $url;
    }

    public function getDirectLink()
    {
        return $this->directLink;
    }

    public function setDirectLink($directLink = null)
    {
        $this->directLink = $directLink;
    }

    public function getDoUpdate()
    {
        return $this->doUpdate;
    }

    public function hasLegalPlayer()
    {
        $legalMovies = new Movies;
        $legalMovies->setMovieId($movie->getId());
        $legalMovies->setWithLegalPlayer(true);

        return $legalMovies->getRows() ? true : false;
    }

    public function getLegalPlayer()
    {
        $arr = F::query_arr('select season,episode,player from '.F::typetab('movie_players').'
			where movie_id = \''.$this->getId().'\' order by season, episode
			;');
        if (! $arr) {
            return false;
        }
        $listOptions = '';
        foreach ($arr as $v) {
            $listOptions .= '<option value="'.$v['player'].'">Сезон: '.$v['season'].', серия: '.$v['episode'].'</option>';
        }
        $t = new Template('players/legal_alt');
        $t->v('options', $listOptions);
        $t->v('firstPlayer', $arr[0]['player']);

        return $t->get();
    }

    public function getMyshowsId()
    {
        return $this->myshows_id;
    }

    public function setMyshowsId($id = null)
    {
        $this->myshows_id = $id;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status = null)
    {
        $this->status = $status;
    }

    public function getStarted()
    {
        if ($this->started == '0000-00-00 00:00:00') {
            return null;
        }

        return $this->started;
    }

    public function setStarted($date = null)
    {
        $this->started = $date;
    }

    public function getEnded()
    {
        if ($this->ended == '0000-00-00 00:00:00') {
            return null;
        }

        return $this->ended;
    }

    public function setEnded($date = null)
    {
        $this->ended = $date;
    }

    public function getStatusRu()
    {
        $arr = [
            'Canceled/Ended' => 'Отменен/Завершен',
            'Returning Series' => 'Снимается',
            'TBD/On The Bubble' => 'Под вопросом',
            'Pilot Rejected' => 'Пилот отклонен',
        ];

        return isset($arr[$this->getStatus()]) ? $arr[$this->getStatus()] : $this->getStatus();
    }

    public function getEpisodesDataList()
    {
        if (! $this->getEpisodesDataListTemplate()) {
            F::error('use setEpisodesDataListTemplate first to use getEpisodesDataList');
        }
        $arr = F::query_arr('select id,season,episode,title,short_name,air_date
			from '.F::typetab('movie_episodes_data').'
			where movie_id = \''.$this->getId().'\'
			order by season desc,episode desc
			;');
        $list = '';
        $i = 0;
        foreach ($arr as $v) {
            $num = count($arr) - $i;
            $i++;
            $t = new Template($this->getEpisodesDataListTemplate());
            $t->v('season', $v['season']);
            $t->v('episode', $v['episode']);
            $t->v('episode_id', $v['id']);
            $t->v('title', $v['title']);
            $t->v('shortName', $v['short_name']);
            // выводим дату на русском
            $formatter = new \IntlDateFormatter(
                'ru_RU',
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::NONE,
                'Europe/Moscow'
            );
            if ($v['air_date']) {
                $airDate = new \DateTime($v['air_date']);
                $t->v('airDate', $formatter->format($airDate->getTimestamp()));
                $currentDate = new \DateTime;
                $t->v('daysToRelease', ($airDate->diff($currentDate)->invert ? (+1) : (-1)) * $airDate->diff($currentDate)->days + 1);
            } else {
                $t->v('airDate', null);
                $t->v('daysToRelease', null);
            }
            // F::dump($formatter->format($airDate->getTimestamp()));
            // $t->v('released',($airDate > $currentDate)?0:1);
            // F::dump(s$airDate->diff($currentDate));
            // if ($airDate > $currentDate) {
            // 	$t->v('daysToRelease',$airDate->diff($currentDate)->days + 1);
            // } else {
            // 	$t->v('daysToRelease',false);
            // }
            $t->v('num', $num);
            $list .= $t->get();
        }

        return $list;
    }

    public function getEpisodesDataListTemplate()
    {
        return $this->episodesDataListTemplate;
    }

    public function setEpisodesDataListTemplate($template = null)
    {
        $this->episodesDataListTemplate = $template;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age = null)
    {
        $this->age = $age;
    }

    public function getSourceType()
    {
        return $this->sourceType;
    }

    public function setSourceType($sourceType = null)
    {
        $this->sourceType = $sourceType;
    }

    public function getTagline()
    {
        return $this->tagline;
    }

    public function setTagline($tagline = null)
    {
        $this->tagline = $tagline;
    }

    public function getLordDescription()
    {
        return $this->lordDescription;
    }

    public function setLordDescription($description = null)
    {
        $this->lordDescription = $description;
    }

    public function getHdvb4k()
    {
        return $this->hdvb_4k;
    }

    public function setHdvb4k($b = null)
    {
        $this->hdvb_4k = $b;
    }

    public function calcMainPlayer()
    {
        if (! $this->getKinopoiskId()) {
            $result = 'mnw';
        } else {
            $vcdn = file_get_contents('https://videocdn.tv/api/short?api_token=CXbCNEUHap9B5JomINoyo97osUbqXW0I&kinopoisk_id='.$this->getKinopoiskId());
            $vcdnObj = json_decode($vcdn);
            if (empty($vcdnObj->data)) {
                // $mnw = file_get_contents('http://moonwalk.cc/api/videos.json?kinopoisk_id='.$movie->getKinopoiskId().'&api_token=3bb4510087038631f510bba0ca7c3f24');
                // $mnwObj = json_decode($mnw);
                $result = 'mnw';
            } else {
                $result = 'videocdn';
            }
            // return $result;
            // F::dump($vcdnObj);
            // F::dump($mnwObj);
        }
        $this->setMainPlayer($result);
        $this->save();

        return $result;
    }

    public function getMainPlayer()
    {
        return $this->mainPlayer;
    }

    public function setMainPlayer($mainPlayer = null)
    {
        $this->mainPlayer = $mainPlayer;
    }

    public function getHdvb()
    {
        $country = Engine::getClientCountry();
        if ($country and ! in_array($country, ['RU', 'UA', 'KZ', 'BY'])) {
            $country = 'EU';
        }
        // если есть hdvb и плеер не в блоке
        $hdvb = Hdvb::where('movie_id', $this->getId())->first();

        return $hdvb and $hdvb->getPlayer($country);
    }

    public function getRndPlayer()
    {
        if ($this->getHdvb()) {
            return 'hdvb';
        }
        /*
        if ($this->getVideodb()) {
            // return 'videocdn';
            return 'videodb';
        }
        */
        if ($this->getAlloha()) {
            return 'allohatv';
        }
        // если ничего из этого нет, то выполняем код ниже, который в итоге выводет videodb фейковый
        // если есть видеодб, то уже решаем, что показать, его или видеосдн
        $country = Engine::getClientCountry();
        $date = new \DateTime;
        $hour = $date->format('G');
        // $hour = 0;
        // $country = 'RU';
        // получаем процент показа плеера videodb в данное время
        $arr = F::query_assoc('
			select ru,ua,other
			from '.F::typetab('videodb_rnd').'
			where hour = \''.$hour.'\'
			;');
        if (in_array($country, ['RU', 'UA'])) {
            $countrySm = mb_strtolower($country);
            $p = empty($arr[$countrySm]) ? 0 : $arr[$countrySm];
        } else {
            $p = empty($arr['other']) ? 0 : $arr['other'];
        }

        // F::dump($p);
        return F::random_event(['videodb' => $p, 'videocdn' => 100 - $p]);
    }

    // строка блокировки (используется например в VideodbController::getPlayer - при несовпадении этой строки плеер не отображается)
    public function getBlockStr()
    {
        return $this->blockStr;
    }

    public function getCollaps()
    {
        return $this->collaps;
    }

    public function getAlloha()
    {
        $arr = F::query_assoc('select 1 from '.F::typetab('alloha').' where movie_id = \''.$this->getId().'\';');

        return $arr ? true : false;
    }

    public function setCollaps($b = null)
    {
        $this->collaps = $b;
    }

    public function getVideocdn()
    {
        return $this->videocdn;
    }

    public function setVideocdn($b = null)
    {
        $this->videocdn = $b;
    }

    public function getVideodb()
    {
        return $this->videodb;
    }

    public function setVideodb($b = null)
    {
        $this->videodb = $b;
    }

    public function getImdbId()
    {
        return $this->imdb_id;
    }

    public function setImdbId($id = null)
    {
        $this->imdb_id = $id;
    }

    public function getTitleEnIfPossible()
    {
        return $this->getTitleEn() ? $this->getTitleEn() : $this->getTitleRu();
    }

    public function calcSkoRating()
    {
        if (! $this->getKinopoiskRating()) {
            $this->setKinopoiskRatingSko(null);
        } else {
            $this->setKinopoiskRatingSko($this->getKinopoiskVotes() ? ($this->getKinopoiskRating() * (1 - (0.5 / sqrt(($this->getKinopoiskVotes()))))) : null);
        }
        if (! $this->getImdbRating()) {
            $this->setImdbRatingSko(null);
        } else {
            $this->setImdbRatingSko($this->getImdbVotes() ? ($this->getImdbRating() * (1 - (0.5 / sqrt(($this->getImdbVotes()))))) : null);
        }
        $this->setRatingSko(((is_null($this->getKinopoiskRatingSko()) ? $this->getImdbRatingSko() : $this->getKinopoiskRatingSko()) + (is_null($this->getImdbRatingSko()) ? $this->getKinopoiskRatingSko() : $this->getImdbRatingSko())) / 2);
    }

    public function setYoutubeId($id = null)
    {
        $this->youtube_id = $id;
    }

    public function getYoutubeId()
    {
        return $this->youtube_id;
    }

    public function getYoutubeData()
    {
        if (! $this->getYoutubeId()) {
            // F::error('Youtube ID required to use getYoutubeData');
            return null;
        }

        return new YoutubeVideo($this->getYoutubeId());
    }

    public function getYoutubePlayer()
    {
        $youtube = $this->getYoutubeData();
        if (! $youtube) {
            return null;
        }
        if (! $this->getYoutubeId()) {
            return null;
        }
        $t = new Template('players/youtube');
        $t->v('movieYoutubeId', $this->getYoutubeId());
        $t->v('movieYoutubePoster', $youtube->getData()->snippet->thumbnails->maxres->url ?? $youtube->getData()->snippet->thumbnails->high->url);

        return $t->get();
    }

    public function getHdvbPlayer()
    {
        $t = new Template('players/hdvb');
        $hdvb = Hdvb::where('movie_id', $this->getId())->first();
        $t->v('iframeUrl', $hdvb ? $hdvb->getPlayer() : null);

        return $t->get();
    }
}
