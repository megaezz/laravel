<?php

namespace cinema\app\models;

use engine\app\models\F;

class YoutubeVideo
{
    private $id = null;

    private $data = null;

    private $date = null;

    public function __construct($id = null)
    {
        $id = $id ?? F::error('ID required to create Youtube class');
        $arr = F::query_assoc('
			select id,data,date
			from '.F::typetab('youtube_videos').'
			where id = \''.F::escape_string($id).'\'
			;');
        if (! $arr) {
            F::error('Video is not exist');
        }
        $this->id = $arr['id'];
        $this->data = json_decode($arr['data']);
        $this->date = $arr['date'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        // F::dump($this->data);
        return $this->data;
    }

    public function getDate()
    {
        return $this->date;
    }

    public static function add($video = null)
    {
        if (empty($video)) {
            F::error('Video data required to add Youtube');
        }
        F::query('
			insert into '.F::typetab('youtube_videos').'
			set id = \''.F::escape_string($video->id).'\',
			data = \''.F::escape_string(json_encode($video, JSON_UNESCAPED_UNICODE)).'\'
			on duplicate key update
			data = \''.F::escape_string(json_encode($video, JSON_UNESCAPED_UNICODE)).'\'
			;');

        return $video->id;
    }

    public function getDescriptionModified()
    {
        // F::dump(preg_replace('/<a.+>.+<\/a>/', '', $this->getData()->snippet->description));
        return preg_replace('/https?\:\/\/([\w.\/]+)/', '', $this->getData()->snippet->description);
    }
}
