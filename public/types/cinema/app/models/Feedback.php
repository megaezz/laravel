<?php

namespace cinema\app\models;

use engine\app\models\Engine;
use engine\app\models\F;

class Feedback
{
    private $id = null;

    private $date = null;

    private $text = null;

    private $theme = null;

    private $email = null;

    private $domain = null;

    private $ip = null;

    public function __construct($id = null)
    {
        if (! $id) {
            F::error('ID required to create Feedback object');
        }
        // F::error('Specify Id first to creste videoComment class');
        $this->id = $id;
        $arr = F::query_assoc('
			select date,text,theme,email,domain,ip
			from '.F::typetab('feedback').'
			where id=\''.$this->id.'\'
			;');
        $this->date = $arr['date'];
        $this->text = $arr['text'];
        $this->theme = $arr['theme'];
        $this->email = $arr['email'];
        $this->domain = $arr['domain'];
        $this->ip = $arr['ip'];
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function getTheme()
    {
        return $this->theme;
    }

    public function setTheme($theme = null)
    {
        $this->theme = $theme;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email = null)
    {
        $this->email = $email;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip = null)
    {
        $this->ip = $ip;
    }

    public static function add()
    {
        F::query('insert into '.F::typetab('feedback').' set text = null;');
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));
        $feedback = new self($id);
        $feedback->setIp(request()->ip());
        $feedback->setDomain(Engine::getDomain(Engine::getDomain()));
        $feedback->save();

        return $id;
    }

    public function save()
    {
        F::query('update '.F::typetab('feedback').'
			set
			theme = '.($this->getTheme() ? ('\''.F::escape_string($this->getTheme()).'\'') : 'null').',
			email = '.($this->getEmail() ? ('\''.F::escape_string($this->getEmail()).'\'') : 'null').',
			domain = '.($this->getDomain() ? ('\''.F::escape_string($this->getDomain()).'\'') : 'null').',
			ip = '.($this->getIp() ? ('\''.F::escape_string($this->getIp()).'\'') : 'null').',
			text = '.($this->getText() ? ('\''.F::escape_string($this->getText()).'\'') : 'null').'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }
}
