<?php

namespace cinema\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class Tags implements \JsonSerializable
{
    private $type = null;

    private $rows = null;

    private $limit = null;

    private $page = null;

    private $order = null;

    private $listTemplate = null;

    private $activeId = null;

    // для проверки сколько есть видео с данным тегом для этого домена
    private $domain = null;

    // только теги для которых есть фильмы на указанном домене
    private $withDomainMovies = null;

    // объект домена (для генерации ссылок например)
    private $domainObject = null;

    private $lang = null;

    private $active = null;

    private $risky = null;

    public function __construct() {}

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function get()
    {
        if (! $this->getPage()) {
            $this->setPage(1);
        }
        if ($this->getLimit()) {
            $sql['limit'] = $this->getLimit() ? ('limit '.($this->getPage() - 1) * $this->getLimit().','.$this->getLimit()) : '';
        } else {
            $sql['limit'] = '';
        }
        $sql['order'] = $this->getOrder() ? 'order by '.$this->getOrder() : '';
        $sql['type'] = $this->getType() ? ('tags.type = \''.$this->getType().'\'') : 1;
        $sql['risky'] = is_null($this->getRisky()) ? 1 : ($this->getRisky() ? 'tags.risky = 1' : 'not tags.risky');
        $sql['active'] = is_null($this->getActive()) ? 1 : ($this->getActive() ? 'tags.active = 1' : 'not tags.active');
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS tags.id
			from '.F::typetab('tags').'
			where
			'.$sql['type'].' and
			'.$sql['active'].' and
			'.$sql['risky'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        // чтобы вывести только те тэги, к которым есть активные видео на сайте
        if ($this->getWithDomainMovies()) {
            $arrWithDomainMovies = [];
            if (! $this->getDomain()) {
                F::error('use Tags::setDomain first to use Tags::withDomainMovies');
            }
            foreach ($arr as $v) {
                $movies = new Movies;
                $movies->setActive(true);
                $movies->setDeleted(false);
                $movies->setDomain($this->getDomain());
                $movies->setTag($v['id']);
                if (! $movies->getRows()) {
                    continue;
                }
                $arrWithDomainMovies[] = $v;
            }
            $arr = $arrWithDomainMovies;
            $this->rows = count($arr);
        } else {
            $this->rows = F::rows_without_limit();
        }

        return $arr;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function getGenres()
    {
        $this->setType('genre');

        return $this->getList();
    }

    public function getChannels()
    {
        $this->setType('channel');

        return $this->getList();
    }

    public function getCountries()
    {
        $this->setType('country');

        return $this->getList();
    }

    public function getDirectors()
    {
        $this->setType('director');

        return $this->getList();
    }

    public function getActors()
    {
        $this->setType('actor');

        return $this->getList();
    }

    public function getStudios()
    {
        $this->setType('studio');

        return $this->getList();
    }

    // function getTypes() {
    // 	$this->setType('type');
    // 	return $this->getList();
    // }

    public function getList($args = [])
    {
        if (! $this->getListTemplate()) {
            F::error('setListTemplate first');
        }
        $list = '';
        if (isset($args['arr'])) {
            $arr = $args['arr'];
        } else {
            $arr = $this->get();
        }
        foreach ($arr as $v) {
            if (isset($args['arr'])) {
                $tag = $v->megaweb;
            } else {
                $tag = new Tag($v['id']);
            }
            $t = new Template($this->getListTemplate());
            $t->setLang($this->getLang());
            if ($v['id'] === $this->getActiveId()) {
                $t->v('active', 'active');
                $t->v('selected', 'selected');
            } else {
                $t->v('active', '');
                $t->v('selected', '');
            }
            $t->v('name', [
                'ru' => $tag->getName(),
                'ua' => $tag->getNameUa(),
                'en' => $tag->getNameEn(),
            ]);
            $t->v('nameUcFirst', [
                'ru' => F::mb_ucfirst($tag->getName()),
                'ua' => F::mb_ucfirst($tag->getNameUa()),
                'en' => F::mb_ucfirst($tag->getNameEn()),
            ]);
            $t->v('tagId', $tag->getId());
            $t->v('type', $tag->getType());
            $t->v('nameUrl', $tag->getNameUrl());
            $t->v('tagEmoji', $tag->getEmoji());
            if ($this->getDomainObject()) {
                $tag->setDomain($this->getDomainObject()->getDomain());
                $t->v('tagLink', $this->getDomainObject()->getTagLink($tag));
                $t->v('genreLink', $this->getDomainObject()->getGenreLink($tag));
                // $http = $this->getDomainClass()->getHttpsOnly()?'https://':'http://';
                // $t->v('httpWatchLink',$http.$this->getDomainClass()->getDomain().$this->getDomainClass()->getWatchLink($movie));
            } else {
                $t->v('tagLink', '');
                $t->v('genreLink', '');
            }
            $list .= $t->get();
        }

        return $list;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    // для отображение active и selected классов css в getList
    public function setActiveId($id = null)
    {
        $this->activeId = $id;
    }

    public function getActiveId()
    {
        return $this->activeId;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setWithDomainMovies($b = null)
    {
        $this->withDomainMovies = $b;
    }

    public function getWithDomainMovies()
    {
        return $this->withDomainMovies;
    }

    public function getDomainObject()
    {
        if (! $this->domainObject) {
            if ($this->getDomain()) {
                $this->setDomainObject(new Domain($this->getDomain()));
            }
        }

        return $this->domainObject;
    }

    public function setDomainObject($domainObject = null)
    {
        $this->domainObject = $domainObject;
        $this->setDomain($this->getDomainObject()->getDomain());
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang = null)
    {
        $this->lang = $lang;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }

    public function getRisky()
    {
        return $this->risky;
    }

    public function setRisky($b = null)
    {
        $this->risky = $b;
    }
}
