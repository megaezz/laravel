<?php

namespace cinema\app\models;

use App\Models\Domain as EngineDomainEloquent;
use cinema\app\models\eloquent\Domain as CinemaDomainEloquent;
use engine\app\models\Domain as EngineDomain;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class Domain extends EngineDomain
{
    private $show_movies = null;

    private $show_serials = null;

    private $moviesWithUniqueDescription = null;

    private $topMoviesLimit = null;

    private $watchLink = null;

    private $watchLinkEn = null;

    private $groupLink = null;

    private $genreLink = null;

    // не null потому что из базы не подтягивается и всегда null и возникает ошибка шаблонизатора
    private $tagLink = '';

    private $useMovieId = null;

    private $moviesLimit = null;

    private $useInRedirectToAnyMovie = null;

    private $moviesPerDay = null;

    private $moviesAddedPerDay = null;

    private $includeMovies = null;

    private $includeSerials = null;

    private $includeTrailers = null;

    private $includeGames = null;

    private $includeYoutube = null;

    private $legalMovies = null;

    // просчитывать ли sameMovies в контроллере Main::watch
    private $watchCalcSameMovies = null;

    // просчитывать ли жанры для видео тумб
    private $calcThumbGenresList = null;

    private $watchSerialLink = null;

    private $watchMovieLink = null;

    private $dmcaQuestion = null;

    // наполнять сайт альтернативными описаниями
    private $useAltDescriptions = null;

    private $recaptchaVersion = null;

    private $allowRussianMovies = null;

    private $rknForRuOnly = null;

    private $moviesWithPoster = null;

    private $allowRiskyStudios = null;

    private $sitemapMoviesPaginationLink = null;

    private $useOnlyChoosenGroups = null;

    private $moviesMinYear = null;

    public function __construct($domain = null, ?CinemaDomainEloquent $cinemaDomainEloquent = null)
    {
        if ($cinemaDomainEloquent) {
            parent::__construct(null, $cinemaDomainEloquent->engineDomain);
            $arr = $cinemaDomainEloquent->getAttributes();
        } else {
            parent::__construct($domain);
            $arr = $this->getCinemaDomainInfo($domain);
        }
        if (! $arr) {
            F::error('Domain '.$domain.' doesn\'t exist for cinema type');
        }
        $this->includeMovies = $arr['include_movies'] ? true : false;
        $this->includeSerials = $arr['include_serials'] ? true : false;
        $this->includeTrailers = $arr['include_trailers'] ? true : false;
        $this->includeGames = $arr['include_games'] ? true : false;
        $this->includeYoutube = $arr['include_youtube'] ? true : false;
        $this->moviesWithUniqueDescription = $arr['movies_with_unique_description'] ? true : false;
        $this->topMoviesLimit = $arr['topMoviesLimit'];
        $this->watchLink = $arr['watchLink'];
        $this->watchLinkEn = $arr['watchLinkEn'];
        $this->groupLink = $arr['groupLink'];
        $this->genreLink = $arr['genreLink'];
        $this->useMovieId = $arr['useMovieId'] ? true : false;
        $this->moviesLimit = $arr['moviesLimit'];
        $this->useInRedirectToAnyMovie = $arr['useInRedirectToAnyMovie'] ? true : false;
        $this->moviesPerDay = $arr['moviesPerDay'];
        $this->moviesAddedPerDay = $arr['moviesAddedPerDay'];
        $this->legalMovies = $arr['legalMovies'] ? true : false;
        $this->watchCalcSameMovies = $arr['watchCalcSameMovies'] ? true : false;
        $this->calcThumbGenresList = $arr['calcThumbGenresList'] ? true : false;
        $this->watchSerialLink = $arr['watchSerialLink'];
        $this->watchMovieLink = $arr['watchMovieLink'];
        $this->dmcaQuestion = $arr['dmcaQuestion'] ? true : false;
        $this->useAltDescriptions = $arr['useAltDescriptions'] ? true : false;
        $this->recaptchaVersion = $arr['recaptchaVersion'];
        $this->allowRussianMovies = $arr['allowRussianMovies'] ? true : false;
        $this->setRknForRuOnly($arr['rknForRuOnly'] ? true : false);
        $this->moviesWithPoster = $arr['moviesWithPoster'] ? true : false;
        $this->allowRiskyStudios = $arr['allowRiskyStudios'] ? true : false;
        $this->sitemapMoviesPaginationLink = $arr['sitemapMoviesPaginationLink'];
        $this->useOnlyChoosenGroups = $arr['useOnlyChoosenGroups'] ? true : false;
        $this->moviesMinYear = $arr['moviesMinYear'];
    }

    public function getIncludeMovies()
    {
        return $this->includeMovies;
    }

    public function setIncludeMovies($b = null)
    {
        $this->includeMovies = $b;
    }

    public function getIncludeSerials()
    {
        return $this->includeSerials;
    }

    public function setIncludeSerials($b = null)
    {
        $this->includeSerials = $b;
    }

    public function getIncludeGames()
    {
        return $this->includeGames;
    }

    public function setIncludeGames($b = null)
    {
        $this->includeGames = $b;
    }

    public function getIncludeYoutube()
    {
        return $this->includeYoutube;
    }

    public function setIncludeYoutube($b = null)
    {
        $this->includeYoutube = $b;
    }

    public function getIncludeTrailers()
    {
        return $this->includeTrailers;
    }

    public function setIncludeTrailers($b = null)
    {
        $this->includeTrailers = $b;
    }

    public function getMoviesWithUniqueDescription()
    {
        return $this->moviesWithUniqueDescription;
    }

    public function setMoviesWithUniqueDescription($b = null)
    {
        $this->moviesWithUniqueDescription = $b;
    }

    public function getTopMoviesLimit()
    {
        return $this->topMoviesLimit;
    }

    public function setTopMoviesLimit($q = null)
    {
        $this->topMoviesLimit = $q;
    }

    public function getWatchLinkTemplate()
    {
        return $this->watchLink;
    }

    public function setWatchLinkTemplate($template = null)
    {
        $this->watchLink = $template;
    }

    public function getWatchSerialLinkTemplate()
    {
        return $this->watchSerialLink;
    }

    public function setWatchSerialLinkTemplate($template = null)
    {
        $this->watchSerialLink = $template;
    }

    public function getWatchMovieLinkTemplate()
    {
        return $this->watchMovieLink;
    }

    public function setWatchMovieLinkTemplate($template = null)
    {
        $this->watchMovieLink = $template;
    }

    public function getWatchLinkEnTemplate()
    {
        return $this->watchLinkEn;
    }

    public function setWatchLinkEnTemplate($template = null)
    {
        $this->watchLinkEn = $template;
    }

    public function getGroupLinkTemplate()
    {
        return $this->groupLink;
    }

    public function setGroupLinkTemplate($template = null)
    {
        $this->groupLink = $template;
    }

    public function getTagLinkTemplate()
    {
        return $this->tagLink;
    }

    public function setTagLinkTemplate($template = null)
    {
        $this->tagLink = $template;
    }

    public function getGenreLinkTemplate()
    {
        return $this->genreLink;
    }

    public function setGenreLinkTemplate($template = null)
    {
        $this->genreLink = $template;
    }

    /*
    function getTagLink($tagObject = null) {
        if (!$tagObject) {
            F::error('TagObject required to Domain::getTagLink');
        }
        // $domain = new Domain($this->getDomain());
        // если пустая строка шаблона ссылки, то выводим пустую строку
        if (!$this->getTagLinkTemplate()) {
            return '';
        }
        $t = new Template;
        $t->setContent($this->getTagLinkTemplate());
        $t->v('tagId',$tagObject->getId());
        // $t->v('domainMovieId',method_exists($movieClass,'getDomainMovieId')?$movieClass->getDomainMovieId():null);
        // $t->v('domainMovieId',$movieClass->getDomainMovieId());
        $t->v('tagNameUrl',$tagObject->getNameUrl());
        // $t->v('movieTitleEnUrl',$movieClass->getTitleEnUrl());
        // $t->v('dmca',method_exists($movieClass,'getDomainMovieDmca')?$movieClass->getDomainMovieDmca():null);
        // $t->v('dmca',$movieClass->getDomainMovieDmca());
        return $t->get();
    }
    */

    public function getGenreLink($tagObj = null)
    {
        // /genres/{tagId}~{nameUrl}
        if (! $tagObj) {
            F::error('TagObj required to Domain::getGenreLink');
        }
        $t = new Template;
        $t->setLang($this->getLang());
        // if (empty($this->getGenreLinkTemplate())) {
        // 	F::dump($this);
        // }
        // F::dump($this);
        // echo 'Шаблон: '.$tagObj->getId().' '.$this->getGenreLinkTemplate().'<br>';
        $t->setContent($this->getGenreLinkTemplate());
        $t->v('tagId', $tagObj->getId());
        $t->v('tagNameUrl', $tagObj->getNameUrl());
        $t->v('tagNameTranslitUrl', $tagObj->getNameTranslitUrl());
        $t->v('tagDmca', $tagObj->getDomainDmca());
        $t->v('reload', Engine::getReload());

        return $t->get();
    }

    public function getTagLink($tagObj = null)
    {
        if (! $tagObj) {
            F::error('TagObj required to Domain::getTagLink');
        }
        $t = new Template;
        $t->setLang($this->getLang());
        $t->setContent($this->getTagLinkTemplate());
        $t->v('tagId', $tagObj->getId());
        $t->v('tagNameUrl', $tagObj->getNameUrl());
        $t->v('tagNameTranslitUrl', $tagObj->getNameTranslitUrl());
        $t->v('tagDmca', $tagObj->getDomainDmca());

        return $t->get();
    }

    public function getGroupLink($groupObject = null)
    {
        if (! $groupObject) {
            F::error('groupObject required to Domain::getGroupLink');
        }
        // если пустая строка шаблона ссылки, то выводим пустую строку
        if (! $this->getGroupLinkTemplate()) {
            return '';
        }
        $t = new Template;
        $t->setLang($this->getLang());
        $t->setContent($this->getGroupLinkTemplate());
        $t->v('groupId', $groupObject->getId());
        $t->v('groupNameUrl', $groupObject->getNameUrl());
        $t->v('groupNameTranslitUrl', $groupObject->getNameTranslitUrl());
        $domainGroupId = DomainGroup::getDomainGroupIdByGroupIdAndDomain($groupObject->getId(), $this->getDomain());
        // F::dump($domainGroupId);
        if ($domainGroupId) {
            $domainGroup = new DomainGroup($domainGroupId);
            $t->v('groupDmca', $domainGroup->getDomainGroupDmca());
        } else {
            $t->v('groupDmca', '');
        }
        $t->v('reload', Engine::getReload());

        return $t->get();
    }

    public function getWatchLink($movieClass = null, $watchLinkTemplate = null)
    {
        if (! $movieClass) {
            F::error('DomainMovie Class required to Domain::getWatchLink');
        }
        // F::dump($watchLinkTemplate);
        // $domain = new Domain($this->getDomain());
        $t = new Template;
        $t->setLang($this->getLang());
        if ($watchLinkTemplate) {
            $t->setContent($watchLinkTemplate);
        } else {
            if ($movieClass->getType() == 'serial') {
                $t->setContent($this->getWatchSerialLinkTemplate() ? $this->getWatchSerialLinkTemplate() : $this->getWatchLinkTemplate());
            } else {
                $t->setContent($this->getWatchMovieLinkTemplate() ? $this->getWatchMovieLinkTemplate() : $this->getWatchLinkTemplate());
            }
        }
        // $t->v('lang',$t->getLang());
        $t->v('movieId', $movieClass->getId());
        // $t->v('domainMovieId',method_exists($movieClass,'getDomainMovieId')?$movieClass->getDomainMovieId():null);
        $t->v('domainMovieId', $movieClass->getDomainMovieId());
        $t->v('movieTitleRuUrl', $movieClass->getTitleRuUrl());
        $t->v('movieTitleEnUrl', $movieClass->getTitleEnUrl());
        $t->v('movieTitleRuTranslitUrl', $movieClass->getTitleRuTranslitUrl());
        $t->v('movieTitleRuUrlencoded', urlencode($movieClass->getTitleRu()));
        // $t->v('dmca',method_exists($movieClass,'getDomainMovieDmca')?$movieClass->getDomainMovieDmca():null);
        $t->v('dmca', $movieClass->getDomainMovieDmca() ? (($this->getDmcaQuestion() ? '?' : '').$movieClass->getDomainMovieDmca()) : '');
        $t->v('reload', Engine::getReload());

        // F::dump($t->get());
        return $t->get();
    }

    // function getGroupLink() {
    // 	$domain = new Domain($this->domain);
    // 	$t = new Template;
    // 	$t->setContent($domain->getGroupLink());
    // 	$t->v('groupId',$this->getId());
    // 	$t->v('name',$this->getName());
    // 	return $t->get();
    // }

    public function getUseMovieId()
    {
        return $this->useMovieId;
    }

    public function getMoviesLimit()
    {
        return $this->moviesLimit;
    }

    public function setMoviesLimit($q = null)
    {
        $this->moviesLimit = $q;
    }

    public function getUseInRedirectToAnyMovie()
    {
        return $this->useInRedirectToAnyMovie;
    }

    public function setMoviesAddedPerDay($q = null)
    {
        $this->moviesAddedPerDay = $q;
    }

    public function getMoviesAddedPerDay()
    {
        return $this->moviesAddedPerDay;
    }

    public function getMoviesPerDay()
    {
        return $this->moviesPerDay;
    }

    public function setMoviesPerDay($q = null)
    {
        $this->moviesPerDay = $q;
    }

    public function getLegalMovies()
    {
        return $this->legalMovies;
    }

    public function setLegalMovies($b = null)
    {
        $this->legalMovies = $b;
    }

    public function save()
    {
        F::query('update '.F::tabname('cinema', 'domains').'
			set
			moviesAddedPerDay = '.(is_null($this->getMoviesAddedPerDay()) ? 'null' : ('\''.F::escape_string($this->getMoviesAddedPerDay()).'\'')).',
			include_serials = '.($this->getIncludeSerials() ? 1 : 0).',
			include_movies = '.($this->getIncludeMovies() ? 1 : 0).',
			include_trailers = '.($this->getIncludeTrailers() ? 1 : 0).',
			include_games = '.($this->getIncludeGames() ? 1 : 0).',
			include_youtube = '.($this->getIncludeYoutube() ? 1 : 0).',
			movies_with_unique_description = '.($this->getMoviesWithUniqueDescription() ? 1 : 0).',
			topMoviesLimit = '.(is_null($this->getTopMoviesLimit()) ? 'null' : ('\''.F::escape_string($this->getTopMoviesLimit()).'\'')).',
			watchLink = '.(is_null($this->getWatchLinkTemplate()) ? 'null' : ('\''.F::escape_string($this->getWatchLinkTemplate()).'\'')).',
			groupLink = '.(is_null($this->getGroupLinkTemplate()) ? 'null' : ('\''.F::escape_string($this->getGroupLinkTemplate()).'\'')).',
			genreLink = '.(is_null($this->getGenreLinkTemplate()) ? 'null' : ('\''.F::escape_string($this->getGenreLinkTemplate()).'\'')).',
			moviesLimit = '.(is_null($this->getMoviesLimit()) ? 'null' : ('\''.F::escape_string($this->getMoviesLimit()).'\'')).',
			moviesPerDay = '.(is_null($this->getMoviesPerDay()) ? 'null' : ('\''.F::escape_string($this->getMoviesPerDay()).'\'')).',
			legalMovies = '.($this->getLegalMovies() ? 1 : 0).',
			watchCalcSameMovies = '.($this->getWatchCalcSameMovies() ? 1 : 0).',
			watchLinkEn = '.(is_null($this->getWatchLinkEnTemplate()) ? 'null' : ('\''.F::escape_string($this->getWatchLinkEnTemplate()).'\'')).',
			calcThumbGenresList = '.($this->getCalcThumbGenresList() ? 1 : 0).',
			watchMovieLink = '.(is_null($this->getWatchMovieLinkTemplate()) ? 'null' : ('\''.F::escape_string($this->getWatchMovieLinkTemplate()).'\'')).',
			watchSerialLink = '.(is_null($this->getWatchSerialLinkTemplate()) ? 'null' : ('\''.F::escape_string($this->getWatchSerialLinkTemplate()).'\'')).',
			useAltDescriptions = '.($this->getUseAltDescriptions() ? 1 : 0).',
			recaptchaVersion = \''.F::escape_string($this->getRecaptchaVersion()).'\',
			allowRussianMovies = '.($this->getAllowRussianMovies() ? 1 : 0).',
			allowRiskyStudios = '.($this->getAllowRiskyStudios() ? 1 : 0).',
			moviesWithPoster = '.($this->getMoviesWithPoster() ? 1 : 0).'
			where domain = \''.$this->getDomain().'\'
			;');

        return true;
    }

    public function getWatchCalcSameMovies()
    {
        return $this->watchCalcSameMovies;
    }

    public function setWatchCalcSameMovies($b = null)
    {
        $this->watchCalcSameMovies = $b;
    }

    public function getCalcThumbGenresList()
    {
        return $this->calcThumbGenresList;
    }

    public function setCalcThumbGenresList($b = null)
    {
        $this->calcThumbGenresList = $b;
    }

    public function getDmcaQuestion()
    {
        return $this->dmcaQuestion;
    }

    public static function addCinemaDomain($domain = null)
    {
        if (! $domain) {
            F::error('Set domain to use Domain::add');
        }
        if (! self::isExist($domain)) {
            F::error('Engine Domain doesn\'t exist');
        }
        if (self::isCinemaDomainExist($domain)) {
            F::error('Cinema Domain is already exist');
        }
        F::query('
			insert into '.F::tabname('cinema', 'domains').' set
			domain = \''.$domain.'\'
			');

        return true;
    }

    public static function isCinemaDomainExist($domain = null)
    {
        if (! $domain) {
            F::error('Specify domain first to use Domain::isExist');
        }
        $arr = self::getCinemaDomainInfo($domain);

        return $arr ? true : false;
    }

    public static function getCinemaDomainInfo($domain = null)
    {
        if (! $domain) {
            F::error('Specify domain first');
        }
        $arr = F::query_assoc('
			select domain,include_movies,include_serials,include_trailers,include_games,include_youtube,
			movies_with_unique_description,
			topMoviesLimit, watchLink, groupLink, genreLink, useMovieId, moviesLimit, useInRedirectToAnyMovie,
			moviesPerDay, moviesAddedPerDay, legalMovies, watchCalcSameMovies, watchLinkEn, calcThumbGenresList, watchSerialLink, watchMovieLink, dmcaQuestion, useAltDescriptions, recaptchaVersion, allowRussianMovies,
			rknForRuOnly, moviesWithPoster, allowRiskyStudios, sitemapMoviesPaginationLink, useOnlyChoosenGroups,
			moviesMinYear
			from '.F::tabname('cinema', 'domains').'
			where domain=\''.$domain.'\';
			');

        return $arr;
    }

    public function getUseAltDescriptions()
    {
        return $this->useAltDescriptions;
    }

    public function setUseAltDescriptions($b = null)
    {
        $this->useAltDescriptions = $b;
    }

    public function getRecaptchaVersion()
    {
        return $this->recaptchaVersion;
    }

    public function getRecaptchaKeys()
    {
        /* TODO: колхоз, но работает. рекапча не зависит от данной модели, а зависит от запрошенного домена, поэтому этого метода здесь быть не должно, но сделал так, чтобы не переписывать старый код */
        $domain = EngineDomainEloquent::find(EngineDomainEloquent::cleanDomain(request()->getHost()));

        return $domain->parent_domain_or_self?->recaptchaKey;
    }

    public function setRecaptchaVersion($recaptchaVersion = null)
    {
        $this->recaptchaVersion = $recaptchaVersion;
    }

    public function getRecaptchaClientKey()
    {
        return $this->getRecaptchaKeys()?->public;
    }

    public function getRecaptchaServerKey()
    {
        return $this->getRecaptchaKeys()?->private;
    }

    public function getAllowRussianMovies()
    {
        return $this->allowRussianMovies;
    }

    public function setAllowRussianMovies($b = null)
    {
        $this->allowRussianMovies = $b;
    }

    public function getRknForRuOnly()
    {
        return $this->rknForRuOnly;
    }

    public function setRknForRuOnly($b = null)
    {
        $this->rknForRuOnly = $b;
    }

    public function getMoviesWithPoster()
    {
        return $this->moviesWithPoster;
    }

    public function setMoviesWithPoster($b = null)
    {
        $this->moviesWithPoster = $b;
    }

    public function getFirstDomainMovieAddDate()
    {
        $movies = new Movies;
        $movies->setDomain($this->getDomain());
        $movies->setOrder('domain_movies.add_date');
        $movies->setLimit(1);
        $arr = $movies->get();
        if (! $arr) {
            return false;
        }
        $movie = new DomainMovie($arr[0]['domain_movies_id']);

        return $movie->getDomainMovieAddDate();
    }

    public function jsonSerialize()
    {
        $domain = parent::jsonSerialize();
        $domain->allowRussianMovies = $this->getAllowRussianMovies();
        $domain->allowRiskyStudios = $this->getAllowRiskyStudios();
        // $domain->firstDomainMovieAddDate = (new \DateTime(F::cache($domain,'getFirstDomainMovieAddDate')))->format('Y-m-d');
        $domain->useAltDescriptions = $this->getUseAltDescriptions();
        $domain->rknForRuOnly = $this->getRknForRuOnly();
        $domain->includeMovies = $this->getIncludeMovies();
        $domain->includeSerials = $this->getIncludeSerials();
        $domain->includeYoutube = $this->getIncludeYoutube();
        $domain->moviesOnly = ($this->getIncludeMovies() and ! $this->getIncludeSerials());
        $domain->serialsOnly = (! $this->getIncludeMovies() and $this->getIncludeSerials());

        return $domain;
    }

    public function getAllowRiskyStudios()
    {
        return $this->allowRiskyStudios;
    }

    public function setAllowRiskyStudios($b = null)
    {
        $this->allowRiskyStudios = $b;
    }

    public function getSitemapMoviesPaginationLinkTemplate()
    {
        return $this->sitemapMoviesPaginationLink;
    }

    public function getUseOnlyChoosenGroups()
    {
        return $this->useOnlyChoosenGroups;
    }

    // подходит ли movie для этого домена?
    public function getIsMovieAllowed($movie_id = null)
    {
        $movie = new Movie($movie_id);
        // если контент этого типа не нужен, то пропускаем
        if ($movie->getType() == 'serial' and ! $this->getIncludeSerials()) {
            return ['result' => false, 'message' => 'Сериалы запрещены'];
        }
        if ($movie->getType() == 'trailer' and ! $this->getIncludeTrailers()) {
            return ['result' => false, 'message' => 'Трейлеры запрещены'];
        }
        if ($movie->getType() == 'movie' and ! $this->getIncludeMovies()) {
            return ['result' => false, 'message' => 'Фильмы запрещены'];
        }
        if ($movie->getType() == 'game' and ! $this->getIncludeGames()) {
            return ['result' => false, 'message' => 'Игры запрещены'];
        }
        if ($movie->getType() == 'youtube' and ! $this->getIncludeYoutube()) {
            return ['result' => false, 'message' => 'Youtube ролики запрещены'];
        }
        /* в идеале должно быть так, но пока что $movie->deleted используется только для скрытия плеера, продумать */
        /*if ($movie->getDeleted()) {
            return array('result' => false, 'message' => 'Фильм удален');
        }*/

        // если указан минимальный год, то добавляем только фильмы не старше этого года
        if ($this->getMoviesMinYear()) {
            if ($movie->getYear()) {
                if ($movie->getYear() < $this->getMoviesMinYear()) {
                    return ['result' => false, 'message' => 'Разрешены только фильмы с '.$this->getMoviesMinYear().' года'];
                }
            }
        }
        // если нужно добавлять только определенные категории, то сопоставляем фильм с выбранными категориями
        if ($this->getUseOnlyChoosenGroups()) {
            $isGroupAllowed = false;
            $groups = new Groups;
            $groups->setMovieId($movie->getId());
            $movieGroups = $groups->get();
            $groups = new Groups;
            $groups->setDomain($this->getDomain());
            $groups->setUseDomainGroup(true);
            $allowedGroups = [];
            foreach (F::cache($groups, 'get', null, 600) as $v) {
                $allowedGroups[] = $v['id'];
            }
            foreach ($movieGroups as $v) {
                if (in_array($v['id'], $allowedGroups)) {
                    $isGroupAllowed = true;
                    break;
                }
            }
            if (! $isGroupAllowed) {
                return ['result' => false, 'message' => 'Не относится к разрешенным категориям. Разрешены: '.collect($allowedGroups)->implode(', ').', фильм в '.collect($movieGroups)->pluck('id')->implode(', ')];
            }
        }
        // если можно добавлять только легальные плееры, проверяем
        if ($this->getLegalMovies()) {
            if (! $movie->hasLegalPlayer()) {
                return ['result' => false, 'message' => 'Нет легального плеера'];
            }
        }
        // если нельзя добавлять русские фильмы, но они ранее существовали на сайте, то проверяем и деактивируем такие видео
        if (! $this->getAllowRussianMovies()) {
            // получаем ID тега Россия чтобы использовать в цикле
            $russianTagId = Tag::getIdByName('Россия');
            if (in_array($russianTagId, $movie->getTags())) {
                return ['result' => false, 'message' => 'Русские фильмы запрещены'];
            }
        }
        // если нельзя добавлять рискованные студии, но они ранее сущ., то проверяем и деактивируем такие видео
        if (! $this->getAllowRiskyStudios()) {
            // получаем список рискованных тегов
            $tags = new Tags;
            $tags->setRisky(true);
            $riskyTags = F::cache($tags, 'get', null, 600);
            // получаем список тегов видео
            $movieTags = $movie->getTags();
            // сопоставляем
            foreach ($riskyTags as $riskyTag) {
                if (in_array($riskyTag['id'], $movieTags)) {
                    return ['result' => false, 'message' => 'Рискованные теги запрещены'];
                }
            }
        }

        return ['result' => true];
    }

    public function getMoviesMinYear()
    {
        return $this->moviesMinYear;
    }
}
