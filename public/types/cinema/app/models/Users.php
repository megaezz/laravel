<?php

namespace cinema\app\models;

use engine\app\models\F;

class Users
{
    private $limit = null;

    private $order = null;

    private $rows = null;

    private $page = null;

    private $level = null;

    private $activeDescriptionWriter = null;

    private $levels = null;

    private $tempBalanceIs = null;

    // чтобы перед регистрацией нового пользователя проверять есть ли такой в базе
    private $login = null;

    // поиск по ID
    private $id = null;

    private $partner = null;

    private $enoughBalance = null;

    private $apiKey = null;

    private $payoutToday = null;

    private $tempBalanceDateIs = null;

    private $email = null;

    private $emailConfirmed = null;

    private $withEmail = null;

    private $checked = null;

    private $lastActivityIs = null;

    public function get()
    {
        $sql['order'] = $this->getOrder() ? 'order by '.$this->getOrder() : '';
        $sql['level'] = $this->getLevel() ? 'level = \''.F::escape_string($this->getLevel()).'\'' : 1;
        $sql['activeDescriptionWriter'] = is_null($this->getActiveDescriptionWriter()) ? 1 : ($this->getActiveDescriptionWriter() ? 'active_rewriter = 1' : 'active_rewriter = 0');
        // if ($this->getLevels()) {f::dump($this->getLevels());}
        $sql['levels'] = $this->getLevels() ? ('level in (\''.implode('\',\'', $this->getLevels()).'\')') : 1;
        // if ($sql['levels'] and $sql['levels']!=1) {f::dump($sql['levels']);}
        $sql['login'] = $this->getLogin() ? 'login = \''.F::escape_string($this->getLogin()).'\'' : 1;
        $sql['email'] = $this->getEmail() ? 'email = \''.F::escape_string($this->getEmail()).'\'' : 1;
        $sql['withEmail'] = is_null($this->getWithEmail()) ? 1 : ($this->getWithEmail() ? 'email is not null' : 'email is null');
        $sql['emailConfirmed'] = is_null($this->getEmailConfirmed()) ? 1 : ('emailConfirmed = '.($this->getEmailConfirmed() ? 1 : 0));
        $sql['checked'] = is_null($this->getChecked()) ? 1 : ('checked = '.($this->getChecked() ? 1 : 0));
        $sql['id'] = $this->getId() ? 'id = \''.F::escape_string($this->getId()).'\'' : 1;
        if ($this->getLimit()) {
            if (! $this->getPage()) {
                $this->setPage(1);
            }
            $sql['limit'] = 'limit '.F::escape_string(($this->getPage() - 1) * $this->getLimit()).','.F::escape_string($this->getLimit());
        } else {
            $sql['limit'] = '';
        }
        $sql['tempBalanceIs'] = $this->getTempBalanceIs() ? ('(users.temp_balance '.$this->getTempBalanceIs().')') : 1;
        $sql['tempBalanceDateIs'] = $this->getTempBalanceDateIs() ? ('(users.temp_balance_date '.$this->getTempBalanceDateIs().')') : 1;
        $sql['partner'] = $this->getPartner() ? ('users.partner = \''.F::escape_string($this->getPartner()).'\'') : 1;
        $sql['enoughBalance'] = is_null($this->getEnoughBalance()) ? 1 : ($this->getEnoughBalance() ? ('(users.temp_balance >= users.customerTemp1TxtPrice)') : ('(users.temp_balance < users.customerTemp1TxtPrice)'));
        $sql['apiKey'] = $this->getApiKey() ? ('users.api_key = \''.F::escape_string($this->getApiKey()).'\'') : 1;
        $sql['payoutToday'] = is_null($this->getPayoutToday()) ? 1 : ('(select count(*) from '.F::typetab('transfers').' where login = users.login and date(date) = current_date and payout = 1 limit 1) = '.($this->getPayoutToday() ? 1 : 0));
        $sql['lastActivityIs'] = $this->getLastActivityIs() ? ('(select last_activity from '.F::typetab('user_sessions').' where login = users.login order by last_activity desc limit 1) '.$this->getLastActivityIs()) : 1;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS
			id,login
			from '.F::typetab('users').'
			where
			'.$sql['level'].' and
			'.$sql['levels'].' and
			'.$sql['activeDescriptionWriter'].' and
			'.$sql['tempBalanceIs'].' and
			'.$sql['tempBalanceDateIs'].' and
			'.$sql['login'].' and
			'.$sql['id'].' and
			'.$sql['partner'].' and
			'.$sql['enoughBalance'].' and
			'.$sql['apiKey'].' and
			'.$sql['payoutToday'].' and
			'.$sql['email'].' and
			'.$sql['emailConfirmed'].' and
			'.$sql['checked'].' and
			'.$sql['withEmail'].' and
			'.$sql['lastActivityIs'].'

			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel($level = null)
    {
        $this->level = $level;
    }

    public function setActiveDescriptionWriter($b = null)
    {
        $this->activeDescriptionWriter = $b;
    }

    public function getActiveDescriptionWriter()
    {
        return $this->activeDescriptionWriter;
    }

    public function getLevels()
    {
        return $this->levels;
    }

    public function setLevels($levels = null)
    {
        $this->levels = $levels;
    }

    // > < = != x
    public function setTempBalanceIs($str = null)
    {
        $this->tempBalanceIs = $str;
    }

    public function getTempBalanceIs()
    {
        return $this->tempBalanceIs;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login = null)
    {
        $this->login = $login;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id = null)
    {
        $this->id = $id;
    }

    public function getPartner()
    {
        return $this->partner;
    }

    public function setPartner($login = null)
    {
        $this->partner = $login;
    }

    public function setEnoughBalance($b = null)
    {
        $this->enoughBalance = $b;
    }

    public function getEnoughBalance()
    {
        return $this->enoughBalance;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function setApiKey($apiKey = null)
    {
        $this->apiKey = $apiKey;
    }

    public function getPayoutToday()
    {
        return $this->payoutToday;
    }

    public function setPayoutToday($b = null)
    {
        $this->payoutToday = $b;
    }

    public function getTempBalanceDateIs()
    {
        return $this->tempBalanceDateIs;
    }

    // > < = != x
    public function setTempBalanceDateIs($str = null)
    {
        $this->tempBalanceDateIs = $str;
    }

    public function setEmail($email = null)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getEmailConfirmed()
    {
        return $this->emailConfirmed;
    }

    public function setEmailConfirmed($b = null)
    {
        $this->emailConfirmed = $b;
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function setChecked($b = null)
    {
        $this->checked = $b;
    }

    public function getWithEmail()
    {
        return $this->withEmail;
    }

    public function setWithEmail($b = null)
    {
        $this->withEmail = $b;
    }

    public function getLastActivityIs()
    {
        return $this->lastActivityIs;
    }

    public function setLastActivityIs($str = null)
    {
        $this->lastActivityIs = $str;
    }
}
