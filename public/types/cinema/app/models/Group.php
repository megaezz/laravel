<?php

namespace cinema\app\models;

use cinema\app\models\eloquent\Group as GroupEloquent;
use engine\app\models\Engine;
use engine\app\models\F;

class Group
{
    private $id = null;

    private $tags = null;

    private $name = null;

    private $active = null;

    private $thumb = null;

    private $name_ua = null;

    private $name_en = null;

    private $type = null;

    private $eloquent = null;

    private $emoji = null;

    public function __construct($id = null, ?GroupEloquent $groupEloquent = null)
    {

        if ($groupEloquent) {

            $arr = $groupEloquent->getAttributes();

        } else {

            if (! $id) {
                F::error('Specify group ID to create Group class');
            }

            $arr = F::query_assoc('select id,name,emoji,active,thumb,name_ua,name_en,type
			from '.F::typetab('groups').'
			where id=\''.$id.'\'
			;');
        }
        if (empty($arr)) {
            F::error('Group is not exist');
        }
        $this->id = $arr['id'];
        $this->name = $arr['name'];
        $this->emoji = $arr['emoji'];
        $this->active = $arr['active'];
        $this->thumb = $arr['thumb'];
        $this->name_ua = $arr['name_ua'];
        $this->name_en = $arr['name_en'];
        $this->type = $arr['type'];
        // dump($this);
    }

    public function getTags()
    {
        if (! is_null($this->tags)) {
            return $this->tags;
        }
        $arr = F::query_arr('
				select tag_id
				from '.F::typetab('groups_tags').'
				where
				group_id=\''.$this->getId().'\'
				');
        $tags = [];
        foreach ($arr as $v) {
            $tags[] = $v['tag_id'];
        }

        return $tags;
    }

    public function addTag($tag_id = null)
    {
        if (! in_array($tag_id, $this->getTags())) {
            F::query('
				insert into '.F::typetab('groups_tags').'
				set group_id = \''.$this->getId().'\',
				tag_id = \''.$tag_id.'\'
				;');

            return true;
        }

        return false;
    }

    public function addTagByNameAndType($name = null, $type = null)
    {
        if (! $name) {
            F::error('Name required to addTagByName');
        }
        if (! $type) {
            F::error('Type required to addTagByNameAndType');
        }
        $tag_id = Tag::getIdByName($name);
        if (! $tag_id) {
            $tag = new Tag(Tag::add());
            $tag->setType($type);
            $tag->setName($name);
            $tag->save();
            $tag_id = $tag->getId();
        }

        return $this->addTag($tag_id);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name = null)
    {
        $this->name = $name;
    }

    public function getNameUrl()
    {
        return str_replace(' ', '+', $this->getName());
    }

    public function getNameTranslitUrl()
    {
        return F::translitRu($this->getNameUrl());
    }

    public function getEmoji()
    {
        return $this->emoji;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getThumb()
    {
        return $this->thumb;
    }

    public function getNameUa()
    {
        return $this->name_ua;
    }

    public function getNameEn()
    {
        return $this->name_en;
    }

    public static function add()
    {
        F::query('insert into '.F::typetab('groups').' set id = null;');
        // $id = Engine::getSqlConnection()->insert_id;
        // пришлось так ухищриться, т.к. на сервере часто возвращался нулевой insert_id не понятно почему
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));

        return $id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function save()
    {
        F::query('
			update '.F::typetab('groups').'
			set
			name = '.($this->getName() ? '\''.F::escape_string($this->getName()).'\'' : 'null').',
			type = '.($this->getType() ? '\''.F::escape_string($this->getType()).'\'' : 'null').'
			where id = \''.F::escape_string($this->getId()).'\'
			;');
    }

    public function getEloquent()
    {
        if (is_null($this->eloquent)) {
            $this->eloquent = GroupEloquent::find($this->getId());
        }

        return $this->eloquent;
    }
}
