<?php

/*

http://moonwalk.cc/partners/login

*/

namespace cinema\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class Kodik
{
    // const apiToken = '3bb4510087038631f510bba0ca7c3f24';
    private $data = null;

    private $id = null;

    private $checked = null;

    public function __construct($id = null)
    {
        if (! $id) {
            F::error('Не передан ID объекта');
        }
        $sql['table'] = 'kodik';
        $arr = F::query_assoc('select unique_id,data,checked,id,date(date) as date
			from '.F::typetab($sql['table']).'
			where
			id = \''.$id.'\'
			limit 1
			;');
        if (empty($arr)) {
            F::error('Объект Kodik не найден');
        }
        $this->id = $arr['id'];
        $this->unique_id = $arr['unique_id'];
        $this->data = json_decode($arr['data']);
        $this->checked = $arr['checked'];
        $this->date = $arr['date'];
        // dump($this->data);
    }

    // возвратить id по unique_id, имеющий макс. кол-во эпизодов
    public static function getBestIdByUniqueId($unique_id = null)
    {
        if (! $unique_id) {
            return null;
        }
        $kodiks = new Kodiks;
        $kodiks->setUniqueId($unique_id);
        $kodiks->setLimit(20);
        $kodiks->setPage(1);
        // $moonwalks->setOrder('episodes_count desc');
        $arr = $kodiks->get();
        // F::dump($arr);
        if (! $arr) {
            return null;
        }
        $bestObj = null;
        foreach ($arr as $v) {
            $m = new Kodik($v['id']);
            if (is_null($bestObj)) {
                $bestObj = $m;
            }
            if ($m->getEpisodesCount() > $bestObj->getEpisodesCount()) {
                $bestObj = $m;
            }
            // if ($m->getType() != 'trailer' and $bestObj->getType() == 'trailer') {
            // 	$bestObj = $m;
            // }
        }

        return $bestObj->getId();
    }

    public function getData()
    {
        return $this->data;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUniqueId()
    {
        return $this->unique_id;
    }

    public function getTitleEn()
    {
        if (! isset($this->data->title_orig)) {
            return null;
        }

        return $this->data->title_orig;
    }

    public function getTitleRu()
    {
        if (! isset($this->data->title)) {
            return null;
        }

        return $this->data->title;
    }

    public function getYear()
    {
        if (! isset($this->data->year)) {
            return null;
        }

        return $this->data->year;
    }

    public function getAddedAt()
    {
        if (! isset($this->data->created_at)) {
            return null;
        }

        return isset($this->data->created_at) ? $this->data->created_at : null;
    }

    public function getType()
    {
        if (! isset($this->data->type)) {
            return null;
        }

        return $this->data->type;
    }

    public function getCategory()
    {
        if (! isset($this->data->category)) {
            return 'foreign';
        }

        return $this->data->category ? $this->data->category : 'foreign';
    }

    public function getCamrip()
    {
        return isset($this->data->camrip) ? $this->data->camrip : null;
    }

    public function getKinopoiskId()
    {
        if (! isset($this->data->kinopoisk_id)) {
            return null;
        }

        return $this->data->kinopoisk_id;
    }

    public function getWorldArtId()
    {
        if (! isset($this->data->world_art_id)) {
            return null;
        }

        return $this->data->world_art_id;
    }

    public function getIframe()
    {
        if (! isset($this->data->player_link)) {
            return null;
        }

        return $this->data->player_link;
    }

    public function getSeasonsCount()
    {
        if (! isset($this->data->last_season)) {
            return null;
        }

        return $this->data->last_season;
    }

    public function getEpisodesCount()
    {
        if (! isset($this->data->episodes_count)) {
            return null;
        }

        return $this->data->episodes_count;
    }

    // нет такого в кодике
    public function getSeasonEpisodesCount()
    {
        return null;
    }

    public function getBlockUa()
    {
        $blocked_countries = isset($this->getData()->blocked_countries) ? $this->getData()->blocked_countries : [];
        foreach ($blocked_countries as $v) {
            if ($v == 'UA') {
                return true;
            }
        }

        return false;
    }

    public function getBlockRu()
    {
        $blocked_countries = isset($this->getData()->blocked_countries) ? $this->getData()->blocked_countries : [];
        foreach ($blocked_countries as $v) {
            if ($v == 'RU') {
                return true;
            }
        }

        return false;
    }

    public function getTranslator()
    {
        if (! isset($this->data->translate)) {
            return null;
        }

        return $this->data->translate;
    }

    // такого нет в кодике
    public function getTranslatorId()
    {
        return null;
    }

    public function getDescription()
    {
        return isset($this->data->material_data->description) ? $this->data->material_data->description : false;
    }

    public function getKinopoiskRating()
    {
        return isset($this->data->material_data->kinopoisk_rating) ? $this->data->material_data->kinopoisk_rating : false;
    }

    public function getKinopoiskVotes()
    {
        return isset($this->data->material_data->kinopoisk_votes) ? $this->data->material_data->kinopoisk_votes : false;
    }

    public function getImdbRating()
    {
        return isset($this->data->material_data->imdb_rating) ? $this->data->material_data->imdb_rating : false;
    }

    public function getImdbVotes()
    {
        return isset($this->data->material_data->imdb_votes) ? $this->data->material_data->imdb_votes : false;
    }

    // нет такого в кодике
    public function getTrailerIframe()
    {
        return null;
    }

    // нет такого в кодике
    public function getThumbnails()
    {
        return false;
    }

    public function getGenres()
    {
        return isset($this->data->material_data->genres) ? $this->data->material_data->genres : false;
    }

    public function getCountries()
    {
        return isset($this->data->material_data->countries) ? $this->data->material_data->countries : false;
    }

    public function getActors()
    {
        return isset($this->data->material_data->actors) ? $this->data->material_data->actors : false;
    }

    public function getDirectors()
    {
        return isset($this->data->material_data->directors) ? $this->data->material_data->directors : false;
    }

    public function getStudios()
    {
        return isset($this->data->material_data->studios) ? $this->data->material_data->studios : false;
    }

    public function getPlayer()
    {
        $t = new Template('players/kodik');
        $t->v('movieKinopoiskId', $this->getKinopoiskId());

        return $t->get();
    }

    // нет такого в кодике
    public function getTrailerPlayer()
    {
        return null;
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function setChecked($b = null)
    {
        $this->checked = $b;
    }

    public function save()
    {
        F::query('update '.F::typetab('kodik').'
			set
			checked = '.($this->getChecked() ? 1 : 0).'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getUpdatedAt()
    {
        return isset($this->data->updated_at) ? $this->data->updated_at : false;
    }

    public function getAltYear()
    {
        return isset($this->data->material_data->year) ? $this->data->material_data->year : null;
    }

    // нет такого в кодике
    public function getSourceType()
    {
        return isset($this->data->source_type) ? $this->data->source_type : null;
    }

    public function getAge()
    {
        return isset($this->data->material_data->minimal_age) ? $this->data->material_data->minimal_age : null;
    }

    public function getTagline()
    {
        return isset($this->data->material_data->tagline) ? $this->data->material_data->tagline : null;
    }
}
