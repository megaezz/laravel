<?php

namespace cinema\app\models;

use App\Models\Proxy;
use engine\app\models\F;

class MyShowsMe
{
    public function __construct() {}

    public static function getShowData($id = null)
    {
        $request = self::getShowDataRequest($id);
        $result = self::apiRequest($request);
        if (isset($result->result)) {
            return $result->result;
        } else {
            return false;
        }
        // F::dump(self::apiRequest($request));
    }

    public static function getShowDataRequest($id = null)
    {
        if (! $id) {
            F::error('myShowsId required');
        }
        $request = new \stdClass;
        $request->jsonrpc = '2.0';
        $request->method = 'shows.GetById';
        $request->params = new \stdClass;
        $request->params->showId = $id;
        $request->params->withEpisodes = true;
        $request->id = 1;

        return $request;
    }

    public static function getShowDataByKinopoiskId($kinopoiskId = null)
    {
        if (! $kinopoiskId) {
            F::error('kinopoiskId required');
        }
        $request = new \stdClass;
        $request->jsonrpc = '2.0';
        $request->method = 'shows.getByExternalId';
        $request->params = new \stdClass;
        $request->params->source = 'kinopoisk';
        $request->params->id = $kinopoiskId;
        $request->id = 1;
        $result = self::apiRequest($request);
        if (isset($result->result)) {
            // F::dump(self::getShowData($result->result->id));
            // return $result->result;
            return self::getShowData($result->result->id);
        } else {
            return false;
        }
        // F::dump(self::apiRequest($request));
    }

    public static function apiRequest($request = null)
    {
        return json_decode(curl_exec(self::getCurl($request)));
    }

    public static function getCurl($request = null, $locale = 'ru')
    {
        $proxy = Proxy::where('active', true)->where('myshows', true)->inRandomOrder()->first();
        if (! $proxy) {
            throw new \Exception('Нет активного прокси для myshows');
        }
        // Разбор прокси на составляющие
        $parsedUrl = parse_url($proxy->proxy);
        $proxyHost = $parsedUrl['host'].':'.$parsedUrl['port'];
        $proxyUser = $parsedUrl['user'];
        $proxyPassword = $parsedUrl['pass'];

        $headers = [
            'Content-type: application/json',
            'Authorization: Bearer 859ea2450228a5d655e3a8c2f9a5aedc30b591df',
            'Accept-Language: '.$locale,
        ];
        $ch = curl_init();
        // Установка прокси
        curl_setopt($ch, CURLOPT_PROXY, $proxyHost);
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyUser.':'.$proxyPassword);
        //
        curl_setopt($ch, CURLOPT_URL, 'https://api.myshows.me/v2/rpc/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        return $ch;
    }

    // передача аргументов массивом, для кеша
    public static function getShowDataForCache($arr = [])
    {
        if (! isset($arr['id'])) {
            F::error('myShowsId required');
        }

        return self::getShowData($arr['id']);
    }
}
