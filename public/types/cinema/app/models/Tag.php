<?php

namespace cinema\app\models;

use cinema\app\models\eloquent\Tag as TagEloquent;
use engine\app\models\Engine;
use engine\app\models\F;

class Tag
{
    private $id = null;

    private $name = null;

    private $type = null;

    private $domain = null;

    private $text = null;

    private $pluralName = null;

    private $emoji = null;

    private $domainDmca = null;

    private $name_ua = null;

    private $name_en = null;

    public function __construct($id = null, ?TagEloquent $tagEloquent = null)
    {
        if ($tagEloquent) {

            $arr = $tagEloquent->getAttributes();

        } else {

            if (! $id) {
                F::error('ID required to create Tag class');
            }
            $arr = F::query_assoc('
				select id,tag,type,plural,emoji,tag_ua,tag_en
				from '.F::typetab('tags').'
				where id = \''.$id.'\'
				;');

        }

        if (! $arr) {
            F::error('Tag doesn\'t exist', 'Tag '.$id.' doesn\'t exist');
        }
        $this->id = $arr['id'];
        $this->name = $arr['tag'];
        $this->type = $arr['type'];
        $this->pluralName = $arr['plural'];
        $this->emoji = $arr['emoji'];
        $this->name_en = $arr['tag_en'];
        $this->name_ua = $arr['tag_ua'];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getNameUrl()
    {
        return str_replace(' ', '+', F::mb_ucfirst($this->getName()));
    }

    public function getNameTranslitUrl()
    {
        return F::translitRu($this->getNameUrl());
    }

    public function setName($name = null)
    {
        $this->name = $name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function add()
    {
        F::query('insert into '.F::typetab('tags').' set id = null;');
        // пришлось так ухищриться, т.к. на сервере часто возвращался нулевой insert_id не понятно почему
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));

        // $id = Engine::getSqlConnection()->insert_id;
        return $id;
    }

    public function save()
    {
        F::query('update '.F::typetab('tags').'
			set
			tag = \''.F::escape_string($this->getName()).'\',
			type = \''.F::escape_string($this->getType()).'\',
			plural = '.($this->getPluralName() ? ('\''.F::escape_string($this->getPluralName()).'\'') : 'null').',
			emoji = '.($this->getEmoji() ? ('\''.F::escape_string($this->getEmoji()).'\'') : 'null').'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }

    public static function getIdByName($name = null)
    {
        if (! $name) {
            F::error('Tag name required to Tag::getIdByName');
        }
        $arr = F::query_assoc('select id from '.F::typetab('tags').' where tag = \''.F::escape_string($name).'\' ;');

        return $arr ? $arr['id'] : false;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
        $arr = F::query_assoc('
			select text,dmca from '.F::typetab('domain_tags').'
			where
			tag_id = \''.$this->getId().'\' and
			domain = \''.$this->getDomain().'\'
			;');
        if ($arr) {
            $this->setText($arr['text']);
            $this->setDomainDmca($arr['dmca'] ? $arr['dmca'] : null);
        }
    }

    public function getDomainDmca()
    {
        return $this->domainDmca;
    }

    public function setDomainDmca($dmca = null)
    {
        $this->domainDmca = $dmca;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function getPluralName()
    {
        return $this->pluralName;
    }

    public function setPluralName($pluralName = null)
    {
        $this->pluralName = $pluralName;
    }

    public function getEmoji()
    {
        return $this->emoji;
    }

    public function setEmoji($emoji = null)
    {
        $this->emoji = $emoji;
    }

    public function getNameEn()
    {
        return $this->name_en;
    }

    public function getNameUa()
    {
        return $this->name_ua;
    }
}
