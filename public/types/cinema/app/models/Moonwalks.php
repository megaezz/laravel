<?php

namespace cinema\app\models;

use engine\app\models\F;

class Moonwalks
{
    private $limit = null;

    private $order = null;

    private $rows = null;

    private $page = null;

    private $listTemplate = null;

    private $paginationUrlPattern = null;

    private $checked = null;

    private $unique_id = null;

    private $token = null;

    private $isUniqueId = null;

    private $date = null;

    private $useUpdates = null;

    private $maxDate = null;

    public function __construct() {}

    public function get()
    {
        if (! $this->getLimit()) {
            F::error('setLimit first');
        }
        if (! $this->getPage()) {
            F::error('setPage first');
        }
        $sql['from'] = ($this->getPage() - 1) * $this->getLimit();
        $sql['having'] = '';
        $sql['q'] = '';
        $sql['order'] = $this->getOrder() ? 'order by '.$this->getOrder() : '';
        $sql['unique_id'] = $this->getUniqueId() ? 'unique_id = \''.$this->getUniqueId().'\'' : 1;
        if (is_null($this->getIsUniqueId())) {
            $sql['is_unique_id'] = 1;
        } else {
            $sql['is_unique_id'] = $this->getIsUniqueId() ? 'unique_id is not null' : 'unique_id is null';
        }
        if (is_null($this->getChecked())) {
            $sql['checked'] = 1;
        } else {
            $sql['checked'] = $this->getChecked() ? 'checked' : 'not checked';
        }
        if ($this->getDate()) {
            $this->setUseUpdates(true);
            $sql['date'] = 'date between \''.$this->getDate().' 00:00:00\' and \''.$this->getDate().' 23:59:59\'';
        } else {
            $sql['date'] = 1;
        }
        if ($this->getMaxDate()) {
            $this->setUseUpdates(true);
            $sql['maxDate'] = 'date < \''.$this->getMaxDate().'\'';
        } else {
            $sql['maxDate'] = 1;
        }
        $sql['table'] = $this->getUseUpdates() ? 'moonwalk_updates' : 'moonwalk';
        $sql['token'] = $this->getToken() ? ('token = \''.$this->getToken().'\'') : 1;
        $arr = F::query_arr(
            'select SQL_CALC_FOUND_ROWS token,date(date) as date
			'.$sql['q'].'
			from '.F::typetab($sql['table']).'
			where
			'.$sql['is_unique_id'].' and
			'.$sql['checked'].' and
			'.$sql['unique_id'].' and
			'.$sql['date'].' and
			'.$sql['token'].' and
			'.$sql['maxDate'].'

			'.$sql['having'].'
			'.$sql['order'].'
			limit '.$sql['from'].','.$this->getLimit().'
			;'
        );
        // dump($arr);
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getIsUniqueId()
    {
        return $this->isUniqueId;
    }

    public function setIsUniqueId($b = null)
    {
        $this->isUniqueId = $b;
    }

    public function getUniqueId()
    {
        return $this->unique_id;
    }

    public function setUniqueId($unique_id = null)
    {
        $this->unique_id = $unique_id;
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function setChecked($b = null)
    {
        return $this->checked = $b;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getPage()
    {
        return $this->page;
    }

    public static function resetCheckers()
    {
        F::query('update '.F::typetab('moonwalk').' set checked = 0;');

        return true;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date = null)
    {
        $this->date = $date;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token = null)
    {
        $this->token = $token;
    }

    public function setUseUpdates($b = null)
    {
        $this->useUpdates = $b;
    }

    public function getUseUpdates()
    {
        return $this->useUpdates;
    }

    public function setMaxDate($date = null)
    {
        $this->maxDate = $date;
    }

    public function getMaxDate()
    {
        return $this->maxDate;
    }
}
