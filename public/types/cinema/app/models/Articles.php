<?php

namespace cinema\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class Articles extends \engine\app\models\Articles
{
    private $domainMovieId = null;

    protected const TYPE = 'cinema';

    protected const CLASS_ARTICLE = 'cinema\app\models\Article';

    public function get()
    {
        if (! $this->getLimit()) {
            $this->setLimit(1);
        }
        if (! $this->getPage()) {
            $this->setPage(1);
        }
        $sql['from'] = ($this->getPage() - 1) * $this->getLimit();
        $sql['domain'] = $this->getDomain() ? ('domain = \''.$this->getDomain().'\'') : 1;
        // $sql['bookedDomain'] = $this->bookedDomain?('booked_domain = \''.$this->bookedDomain.'\''):1;
        $sql['order'] = $this->getOrder() ? ('order by '.$this->getOrder()) : '';
        $sql['domainMovieId'] = $this->getDomainMovieId() ? ('domain_movie_id = \''.$this->getDomainMovieId().'\'') : 1;
        if (is_null($this->getActive())) {
            $sql['active'] = 1;
        } else {
            $sql['active'] = $this->getActive() ? 'active' : 'not active';
        }
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS id
			from '.F::tabname(static::TYPE, 'articles').'
			where
			'.$sql['domain'].'
			and '.$sql['active'].'
			and '.$sql['domainMovieId'].'

			'.$sql['order'].'
			limit '.$sql['from'].','.$this->getLimit().'
			;');
        $this->setRows(F::rows_without_limit());

        return $arr;
    }

    public function getList()
    {
        if (! $this->getListTemplate()) {
            F::error('use setListTemplate');
        }
        $list = '';
        $arr = $this->get();
        foreach ($arr as $v) {
            $t = new Template($this->getListTemplate());
            $a = (new \ReflectionClass(static::CLASS_ARTICLE))->newInstance($v['id']);
            $t->v('title', empty($a->getTitle()) ? 'Без названия' : $a->getTitle());
            // $t->v('text',$a->getText());
            $t->v('id', $a->getId());
            $t->v('classActive', ($a->getId() === $this->getActiveId()) ? 'active' : '');
            $t->v('mainImageUrl', $a->getMainImageUrl());
            $t->v('textPreview', $a->getTextPreview());
            $t->v('domain', $a->getDomain() ? $a->getDomain() : '');
            $t->v('acceptor', $a->getAcceptor() ? $a->getAcceptor() : '');
            if ($a->getDomain()) {
                $domainObj = new Domain($a->getDomain());
                $t->v('lastRedirectDomain', $domainObj->getLastRedirectDomain() ? $domainObj->getLastRedirectDomain() : '');
            } else {
                $t->v('lastRedirectDomain', '');
            }
            if ($a->getAcceptor()) {
                $domainObj = new Domain($a->getAcceptor());
                $t->v('lastRedirectAcceptor', $domainObj->getLastRedirectDomain() ? $domainObj->getLastRedirectDomain() : '');
            } else {
                $t->v('lastRedirectAcceptor', '');
            }
            $t->v('active', $a->getActive() ? 1 : 0);
            $addDate = new \DateTime($a->getApplyDate());
            $formatter = new \IntlDateFormatter(
                'ru_RU',
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::NONE,
                'Europe/Moscow'
            );
            $t->v('applyDate', $formatter->format($addDate));
            $list .= $t->get();
        }

        return $list;
    }

    public function setDomainMovieId($id = null)
    {
        $this->domainMovieId = $id;
    }

    public function getDomainMovieId()
    {
        return $this->domainMovieId;
    }
}
