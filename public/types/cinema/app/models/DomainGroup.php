<?php

namespace cinema\app\models;

use engine\app\models\F;

class DomainGroup extends Group
{
    private $domain = null;

    private $domainGroupH1 = null;

    private $domainGroupTitle = null;

    private $domainGroupText = null;

    private $domainGroupActive = null;

    private $domainGroupImg = null;

    private $domainGroupDmca = null;

    private $domainGroupId = null;

    public function __construct($id = null)
    {
        $arr = F::query_assoc('
			select id,domain,group_id,h1,title,text,active,img,dmca
			from '.F::typetab('domain_groups').'
			where
			id = \''.$id.'\'
			;');
        if (! $arr) {
            // F::error('Group doesn\'t exist for this domain');
            throw new \Exception('Group doesn\'t exist for this domain');
        }
        $this->domain = $arr['domain'];
        $this->domainGroupH1 = $arr['h1'];
        $this->domainGroupTitle = $arr['title'];
        $this->domainGroupText = $arr['text'];
        $this->domainGroupActive = $arr['active'];
        $this->domainGroupId = $arr['id'];
        $this->domainGroupImg = $arr['img'];
        $this->domainGroupDmca = $arr['dmca'];
        parent::__construct($arr['group_id']);
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getDomainGroupH1()
    {
        return $this->h1;
    }

    public function getDomainGroupTitle()
    {
        return $this->title;
    }

    public function getDomainGroupText()
    {
        return $this->domainGroupText;
    }

    public function getDomainGroupActive()
    {
        return $this->domainGroupActive;
    }

    public function getDomainGroupImg()
    {
        return $this->domainGroupImg;
    }

    public function setDomainGroupImg($src = null)
    {
        return $this->domainGroupImg;
    }

    public static function getDomainGroupIdByGroupIdAndDomain($groupId = null, $domain = null)
    {
        $groups = new Groups;
        $groups->setLimit(1);
        $groups->setDomain($domain);
        $groups->setPage(1);
        $groups->setGroupId($groupId);
        $groups_arr = $groups->get();
        $domain_group_id = isset($groups_arr[0]['domain_group_id']) ? $groups_arr[0]['domain_group_id'] : null;

        return $domain_group_id;
    }

    public function getDomainGroupDmca()
    {
        return $this->domainGroupDmca;
    }

    public function setDomainGroupDmca($dmca = null)
    {
        $this->domainGroupDmca = $dmca;
    }

    public function getIdByDomainAndGroupId($domain = null, $groupId = null)
    {
        $groups = new Groups;
        $groups->setDomain($domain);
        $groups->setGroupId($groupId);
        F::dump($groups->get());
    }
}
