<?php

// не сделан
class MovieTag
{
    private $movieId = null;

    private $tagId = null;

    private $tagClass = null;

    public function __construct($movieId = null, $tagId = null)
    {
        if (! $movieId) {
            F::error('movieId required to create MovieTag class');
        }
        if (! $tagId) {
            F::error('tagId required to create MovieTag class');
        }
        $arr = F::query_assoc('
			select cinema_id,tag_id
			'.F::typetab('cinema_tags').'
			where cinema_id = \''.$movieId.'\' and tag_id = \''.$tagId.'\'
			;');
        if (! $arr) {
            F::error('Tag doesn\'t exist', 'Tag '.$tagId.' doesn\'t exist in movie '.$movieId);
        }
        $this->tagId = $arr['tag_id'];
        $this->movieId = $arr['cinema_id'];
    }

    public function getTagClass()
    {
        if ($tagClass) {
            return $tagClass;
        }
        $this->tagClass = new Tag($this->getTagId());

        return $this->tagClass;
    }

    public function getType()
    {
        return $this->getTagClass()->getType();
    }

    public function getName()
    {
        return $this->getTagClass()->getTag();
    }

    public function getMovieId()
    {
        return $this->movieId;
    }

    public function getTagId()
    {
        return $this->tagId;
    }
}
