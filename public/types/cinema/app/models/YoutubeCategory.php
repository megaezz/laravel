<?php

namespace cinema\app\models;

use engine\app\models\F;

class YoutubeCategory
{
    private $id = null;

    private $data = null;

    public function __construct($id = null)
    {
        $id = $id ?? F::error('ID required');
        $arr = F::query_assoc('
			select id,data
			from '.F::typetab('youtube_categories').'
			where id = \''.$id.'\'
			;');
        if (! $arr) {
            F::error('Category '.$id.' is not exist');
        }
        $this->id = $arr['id'];
        $this->data = json_decode($arr['data']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public static function isIdExists($id = null)
    {
        $arr = F::query_assoc('
			select id from '.F::typetab('youtube_categories').'
			where
			id = \''.F::escape_string($id).'\'
			;');

        return $arr ? true : false;
    }
}
