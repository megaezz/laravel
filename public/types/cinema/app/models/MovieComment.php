<?php

namespace cinema\app\models;

use engine\app\models\Engine;
use engine\app\models\F;

class MovieComment
{
    private $id = null;

    private $date = null;

    private $text = null;

    private $username = null;

    private $domainMovieId = null;

    public function __construct($id = null)
    {
        if ($id) {
            // F::error('Specify Id first to creste videoComment class');
            $this->id = $id;
            $arr = F::query_assoc('
				select domain_movie_id,date,username,text
				from '.F::typetab('movie_comments').'
				where id=\''.$this->id.'\'
				;');
            $this->domainMovieId = $arr['domain_movie_id'];
            $this->date = $arr['date'];
            $this->username = $arr['username'];
            $this->text = $arr['text'];
        }
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getDomainMovieId()
    {
        return $this->domainMovieId;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUsername($username = null)
    {
        $this->username = $username;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public static function add($domainMovieId = null)
    {
        // пробуем создать класс, чтобы проверить есть ли такой domanMovieId
        $movie = new DomainMovie($domainMovieId);
        F::query('insert into '.F::typetab('movie_comments').' set
			domain_movie_id=\''.$movie->getDomainMovieId().'\';');
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));

        return $id;
    }

    public function save()
    {
        F::query('update '.F::typetab('movie_comments').'
			set
			username = '.($this->getUsername() ? ('\''.F::escape_string($this->getUsername()).'\'') : 'null').',
			text = '.($this->getText() ? ('\''.F::escape_string($this->getText()).'\'') : 'null').'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }
}
