<?php

namespace cinema\app\models;

use cinema\app\models\eloquent\DomainMovie as DomainMovieEloquent;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class DomainMovie extends Movie
{
    private $domain = null;

    private $domainMovieDescription = null;

    private $domainMovieDescriptionWriter = null;

    private $domainMovieDescriptionPricePer1k = null;

    private $domainMovieAddDate = null;

    private $domainMovieId = null;

    private $domainMovieTextRuDescriptionId = null;

    private $domainMovieDescriptionDate = null;

    private $domainMovieTextRuResult = null;

    private $domainMovieTextRuWideResult = null;

    private $domainMovieTextRuUnique = null;

    private $domainMovieActive = null;

    private $domainMovieDmca = null;

    private $domainMovieDeleted = null;

    private $domainMovieBookedForLogin = null;

    private $domainMovieCustomer = null;

    private $domainMovieCustomerPricePer1k = null;

    private $domainMovieRework = null;

    private $domainMovieReworkComment = null;

    private $domainMovieShowToCustomer = null;

    private $domainMovieCustomerArchived = null;

    private $domainMovieMovedToTop = null;

    private $domainMovieWriting = null;

    private $domainMovieCustomerComment = null;

    private $domainMovieSymbolsFrom = null;

    private $domainMovieSymbolsTo = null;

    private $domainMovieBookDate = null;

    private $domainMoviePrivateComment = null;

    private $domainMovieMovedToBottom = null;

    private $domainMovieDeletedPage = null;

    private $domainMovieDoRedirect = null;

    private $domainMovieRkn = null;

    private $domainMovieBlockForeigners = null;

    private $domainMovieCustomerLike = null;

    private $domainMovieChecked = null;

    private $domainMovieModerator = null;

    private $domainMovieReworkDate = null;

    private $domainMovieModerationPricePer1k = null;

    private $domainMovieCustomerConfirmDate = null;

    private $domainMovieAdult = null;

    public $eloquent = null;
    // private $domainMovieDescriptionUnique = null;

    public function __construct($id = null, ?DomainMovieEloquent $domainMovieEloquent = null)
    {
        if ($domainMovieEloquent) {
            // dd($domainMovieEloquent);
            $arr = $domainMovieEloquent->getAttributes();
        } else {
            $arr = F::query_assoc('
			select id, description, domain, add_date, movie_id, description_writer,
			description_price_per_1k, text_ru_description_id, description_date,
			text_ru_result, text_ru_unique, active, dmca, deleted, booked_for_login,
			customer, customer_price_per_1k, rework, rework_comment, show_to_customer,
			customer_archived, to_top, writing, customer_comment, symbols_from, symbols_to,
			book_date, private_comment, to_bottom, deleted_page, do_redirect, rkn, block_foreigners,
			customer_like, checked, rework_date, moderator, moderation_price_per_1k,customer_confirm_date,
			text_ru_wide_result, adult
			from '.F::tabname('cinema', 'domain_movies').'
			where
			id = \''.$id.'\'
			;');
        }
        if (! $arr) {
            F::error('DomainMovie doesn\'t exist', 'DomainMovie '.$id.' doesn\'t exist');
        }
        // parent::__construct($arr['movie_id']);
        $this->domain = $arr['domain'];
        // F::dump($this);
        $this->domainMovieId = (int) $arr['id'];
        // F::dump($this);
        $this->domainMovieDescription = $arr['description'];
        $this->domainMovieDescriptionDate = $arr['description_date'];
        $this->domainMovieDescriptionWriter = $arr['description_writer'];
        $this->domainMovieDescriptionPricePer1k = $arr['description_price_per_1k'];
        $this->domainMovieAddDate = $arr['add_date'];
        $this->domainMovieTextRuDescriptionId = $arr['text_ru_description_id'];
        $this->domainMovieTextRuResult = json_decode((string) $arr['text_ru_result']);
        $this->domainMovieTextRuUnique = $arr['text_ru_unique'];
        $this->domainMovieActive = $arr['active'] ? true : false;
        $this->domainMovieDmca = $arr['dmca'];
        $this->domainMovieDeleted = $arr['deleted'] ? true : false;
        $this->domainMovieBookedForLogin = $arr['booked_for_login'];
        $this->domainMovieCustomer = $arr['customer'];
        $this->domainMovieCustomerPricePer1k = $arr['customer_price_per_1k'];
        $this->domainMovieRework = $arr['rework'] ? true : false;
        $this->domainMovieReworkComment = $arr['rework_comment'];
        $this->domainMovieShowToCustomer = $arr['show_to_customer'] ? true : false;
        $this->domainMovieCustomerArchived = $arr['customer_archived'] ? true : false;
        $this->domainMovieMovedToTop = $arr['to_top'] ? true : false;
        $this->domainMovieWriting = $arr['writing'] ? true : false;
        $this->domainMovieCustomerComment = $arr['customer_comment'];
        $this->domainMovieSymbolsFrom = $arr['symbols_from'];
        $this->domainMovieSymbolsTo = $arr['symbols_to'];
        $this->domainMovieBookDate = $arr['book_date'];
        $this->domainMoviePrivateComment = $arr['private_comment'];
        $this->domainMovieMovedToBottom = $arr['to_bottom'] ? true : false;
        $this->domainMovieDeletedPage = $arr['deleted_page'] ? true : false;
        $this->domainMovieDoRedirect = $arr['do_redirect'] ? true : false;
        $this->domainMovieRkn = $arr['rkn'] ? true : false;
        $this->domainMovieBlockForeigners = $arr['block_foreigners'] ? true : false;
        $this->domainMovieCustomerLike = is_null($arr['customer_like']) ? null : ($arr['customer_like'] ? true : false);
        $this->domainMovieChecked = $arr['checked'] ? true : false;
        $this->domainMovieReworkDate = $arr['rework_date'];
        $this->domainMovieModerator = $arr['moderator'];
        $this->domainMovieModerationPricePer1k = $arr['moderation_price_per_1k'];
        $this->domainMovieCustomerConfirmDate = $arr['customer_confirm_date'];
        $this->domainMovieTextRuWideResult = json_decode((string) $arr['text_ru_wide_result']);
        $this->domainMovieAdult = $arr['adult'] ? true : false;
        // F::dump($arr);
        if ($domainMovieEloquent) {
            parent::__construct(null, $domainMovieEloquent->movie);
        } else {
            parent::__construct($arr['movie_id']);
        }
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function save($login = null)
    {
        if ($login) {
            $domainMovie = new DomainMovie($this->getDomainMovieId());
            // передаем не объекты, а get_object_vars, чтобы избежать проблемы видимости приватных свойств
            $diff = F::getDiffProperties(get_object_vars($domainMovie), get_object_vars($this));
            foreach ($diff as $property => $value) {
                F::query('
					insert into '.F::typetab('domain_movie_changes').'
					set
					id = \''.$this->getDomainMovieId().'\',
					property = \''.F::escape_string($property).'\',
					old_value = \''.F::escape_string(json_encode($value['old'], JSON_UNESCAPED_UNICODE)).'\',
					new_value = \''.F::escape_string(json_encode($value['new'], JSON_UNESCAPED_UNICODE)).'\',
					login = \''.F::escape_string($login).'\'
					;');
            }
        }
        F::query('update '.F::typetab('domain_movies').'
			set
			movie_id = '.($this->getId() ? ('\''.F::escape_string($this->getId()).'\'') : 'null').',
			domain = '.($this->getDomain() ? ('\''.F::escape_string($this->getDomain()).'\'') : 'null').',
			description_writer = '.($this->getDomainMovieDescriptionWriter() ? ('\''.F::escape_string($this->getDomainMovieDescriptionWriter()).'\'') : 'null').',
			description = '.($this->getDomainMovieDescription() ? ('\''.F::escape_string($this->getDomainMovieDescription()).'\'') : 'null').',
			description_date = '.($this->getDomainMovieDescriptionDate() ? ('\''.F::escape_string($this->getDomainMovieDescriptionDate()).'\'') : 'null').',
			description_price_per_1k = '.($this->getDomainMovieDescriptionPricePer1k() ? ('\''.F::escape_string($this->getDomainMovieDescriptionPricePer1k()).'\'') : 'null').',
			text_ru_description_id = '.($this->getDomainMovieTextRuDescriptionId() ? ('\''.F::escape_string($this->getDomainMovieTextRuDescriptionId()).'\'') : 'null').',
			text_ru_result = '.($this->getDomainMovieTextRuResult() ? ('\''.F::escape_string(json_encode($this->getDomainMovieTextRuResult())).'\'') : 'null').',
			text_ru_unique = '.(is_null($this->getDomainMovieTextRuUnique()) ? 'null' : ('\''.F::escape_string($this->getDomainMovieTextRuUnique()).'\'')).',
			active = '.($this->getDomainMovieActive() ? 1 : 0).',
			booked_for_login = '.($this->getDomainMovieBookedForLogin() ? ('\''.F::escape_string($this->getDomainMovieBookedForLogin()).'\'') : 'null').',
			customer = '.($this->getDomainMovieCustomer() ? ('\''.F::escape_string($this->getDomainMovieCustomer()).'\'') : 'null').',
			customer_price_per_1k = '.(is_null($this->getDomainMovieCustomerPricePer1k()) ? 'null' : ('\''.F::escape_string($this->getDomainMovieCustomerPricePer1k()).'\'')).',
			rework = '.($this->getDomainMovieRework() ? 1 : 0).',
			rework_comment = '.($this->getDomainMovieReworkComment() ? ('\''.F::escape_string($this->getDomainMovieReworkComment()).'\'') : 'null').',
			show_to_customer = '.($this->getDomainMovieShowToCustomer() ? 1 : 0).',
			customer_archived = '.($this->getDomainMovieCustomerArchived() ? 1 : 0).',
			to_top = '.($this->getDomainMovieMovedToTop() ? 1 : 0).',
			writing = '.($this->getDomainMovieWriting() ? 1 : 0).',
			customer_comment = '.($this->getDomainMovieCustomerComment() ? ('\''.F::escape_string($this->getDomainMovieCustomerComment()).'\'') : 'null').',
			symbols_from = '.($this->getDomainMovieSymbolsFrom() ? ('\''.F::escape_string($this->getDomainMovieSymbolsFrom()).'\'') : 'null').',
			symbols_to = '.($this->getDomainMovieSymbolsTo() ? ('\''.F::escape_string($this->getDomainMovieSymbolsTo()).'\'') : 'null').',
			dmca = '.($this->getDomainMovieDmca() ? ('\''.F::escape_string($this->getDomainMovieDmca()).'\'') : 'null').',
			book_date = '.($this->getDomainMovieBookDate() ? ('\''.F::escape_string($this->getDomainMovieBookDate()).'\'') : 'null').',
			private_comment = '.($this->getDomainMoviePrivateComment() ? ('\''.F::escape_string($this->getDomainMoviePrivateComment()).'\'') : 'null').',
			do_redirect = '.($this->getDomainMovieDoRedirect() ? 1 : 0).',
			deleted = '.($this->getDomainMovieDeleted() ? 1 : 0).',
			rkn = '.($this->getDomainMovieRkn() ? 1 : 0).',
			customer_like = '.(is_null($this->getDomainMovieCustomerLike()) ? 'null' : ($this->getDomainMovieCustomerLike() ? 1 : 0)).',
			to_bottom = '.($this->getDomainMovieMovedToBottom() ? 1 : 0).',
			checked = '.($this->getDomainMovieChecked() ? 1 : 0).',
			expected_price = \''.($this->getExpectedPrice()).'\',
			moderator = '.($this->getDomainMovieModerator() ? ('\''.F::escape_string($this->getDomainMovieModerator()).'\'') : 'null').',
			rework_date = '.($this->getDomainMovieReworkDate() ? ('\''.F::escape_string($this->getDomainMovieReworkDate()).'\'') : 'null').',
			moderation_price_per_1k = \''.((float) F::escape_string($this->getDomainMovieModerationPricePer1k())).'\',
			customer_confirm_date = '.($this->getDomainMovieCustomerConfirmDate() ? ('\''.F::escape_string($this->getDomainMovieCustomerConfirmDate()).'\'') : 'null').',
			text_ru_wide_result = '.($this->getDomainMovieTextRuWideResult() ? ('\''.F::escape_string(json_encode($this->getDomainMovieTextRuWideResult(), JSON_UNESCAPED_UNICODE)).'\'') : 'null').',
			adult = '.($this->getDomainMovieAdult() ? 1 : 0).',
			description_length = '.($this->getDomainMovieDescriptionLength()).'
			where id = \''.$this->getDomainMovieId().'\'
			;');

        return true;
    }

    public static function add($movieId = null)
    {
        if (! $movieId) {
            F:error('movieId is required to add new DomainMovie');
        }
        F::query('insert into '.F::typetab('domain_movies').' set movie_id = \''.$movieId.'\';');
        // $id = Engine::getSqlConnection()->insert_id;
        // пришлось так ухищриться, т.к. на сервере часто возвращался нулевой insert_id не понятно почему
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));

        return $id;
    }

    public function delete()
    {
        F::query('delete from '.F::typetab('domain_movies').' where id = \''.$this->getDomainMovieId().'\';');

        return true;
    }

    public function setDomainMovieId($id = null)
    {
        $this->domainMovieId = $id;
    }

    public function getDomainMovieId()
    {
        return $this->domainMovieId;
    }

    public function getDomainMovieDescription()
    {
        return $this->domainMovieDescription;
    }

    public function setDomainMovieDescription($description = null)
    {
        $this->domainMovieDescription = $description;
    }

    public function setDomainMovieDescriptionWriter($login = null)
    {
        $this->domainMovieDescriptionWriter = $login;
    }

    public function getDomainMovieDescriptionWriter()
    {
        return $this->domainMovieDescriptionWriter;
    }

    public function getDomainMovieDescriptionDate()
    {
        return $this->domainMovieDescriptionDate;
    }

    public function setDomainMovieDescriptionDate($date = null)
    {
        $this->domainMovieDescriptionDate = $date;
    }

    public function setDomainMovieDescriptionPricePer1k($price = null)
    {
        $this->domainMovieDescriptionPricePer1k = $price;
    }

    public function getDomainMovieDescriptionPricePer1k()
    {
        return $this->domainMovieDescriptionPricePer1k;
    }

    public function setDomainMovieTextRuDescriptionId($id = null)
    {
        $this->domainMovieTextRuDescriptionId = $id;
    }

    public function getDomainMovieTextRuDescriptionId()
    {
        return $this->domainMovieTextRuDescriptionId;
    }

    public static function getDomainMovieIdByUniqueIdAndDomain($unique_id = null, $domain = null)
    {
        $movies = new Movies;
        $movies->setLimit(1);
        $movies->setDomain($domain);
        $movies->setPage(1);
        $movies->setUniqueId($unique_id);
        $movies->setIncludeNullType(true);
        $movies_arr = $movies->get();
        $movie_id = isset($movies_arr[0]['domain_movies_id']) ? $movies_arr[0]['domain_movies_id'] : null;

        return $movie_id;
    }

    public static function getDomainMovieIdByMovieIdAndDomain($movieId = null, $domain = null)
    {
        $movies = new Movies;
        $movies->setLimit(1);
        $movies->setDomain($domain);
        $movies->setPage(1);
        $movies->setMovieId($movieId);
        $movies->setIncludeNullType(true);
        $movies->setIncludeYoutube(true);
        $movies_arr = $movies->get();
        $movie_id = isset($movies_arr[0]['domain_movies_id']) ? $movies_arr[0]['domain_movies_id'] : null;

        return $movie_id;
    }

    public function getDomainMovieTextRuResult()
    {
        return $this->domainMovieTextRuResult;
    }

    public function setDomainMovieTextRuResult($result = null)
    {
        $this->domainMovieTextRuResult = $result;
    }

    public function setDomainMovieTextRuUnique($unique = null)
    {
        $this->domainMovieTextRuUnique = $unique;
    }

    public function getDomainMovieTextRuUnique()
    {
        return $this->domainMovieTextRuUnique;
    }

    public function getDomainMovieActive()
    {
        return $this->domainMovieActive;
    }

    public function setDomainMovieActive($b = null)
    {
        $this->domainMovieActive = $b;
    }

    public function getDomainMovieDmca()
    {
        return $this->domainMovieDmca;
    }

    public function setDomainMovieDmca($dmca = null)
    {
        $this->domainMovieDmca = $dmca;
    }

    public function getDomainMovieDeleted()
    {
        return $this->domainMovieDeleted;
    }

    public function setDomainMovieDeleted($b = null)
    {
        $this->domainMovieDeleted = $b;
    }

    public function getDomainMovieRkn()
    {
        return $this->domainMovieRkn;
    }

    public function setDomainMovieRkn($b = null)
    {
        $this->domainMovieRkn = $b;
    }

    public function getDomainMovieBookedForLogin()
    {
        return $this->domainMovieBookedForLogin;
    }

    public function setDomainMovieBookedForLogin($login = null)
    {
        $this->domainMovieBookedForLogin = $login;
    }

    public function getDomainMovieCustomer()
    {
        return $this->domainMovieCustomer;
    }

    public function setDomainMovieCustomer($login = null)
    {
        $this->domainMovieCustomer = $login;
    }

    public function setDomainMovieCustomerPricePer1k($price = null)
    {
        $this->domainMovieCustomerPricePer1k = $price;
    }

    public function getDomainMovieCustomerPricePer1k()
    {
        return $this->domainMovieCustomerPricePer1k;
    }

    public function getDomainMovieDescriptionCustomerPrice()
    {
        return round(mb_strlen($this->getDomainMovieDescription()) / 1000 * $this->getDomainMovieCustomerPricePer1k(), 1);
    }

    public function getDomainMovieDescriptionWorkerPrice()
    {
        return round(mb_strlen($this->getDomainMovieDescription()) / 1000 * $this->getDomainMovieDescriptionPricePer1k(), 1);
    }

    public function getDomainMovieRework()
    {
        return $this->domainMovieRework;
    }

    public function setDomainMovieRework($b = null)
    {
        $this->domainMovieRework = $b;
    }

    public function getDomainMovieReworkComment()
    {
        return $this->domainMovieReworkComment;
    }

    public function setDomainMovieReworkComment($comment = null)
    {
        $this->domainMovieReworkComment = $comment;
    }

    public function setDomainMovieShowToCustomer($b = null)
    {
        $this->domainMovieShowToCustomer = $b;
    }

    public function getDomainMovieShowToCustomer()
    {
        return $this->domainMovieShowToCustomer;
    }

    public function getDomainMovieAddDate()
    {
        return $this->domainMovieAddDate;
    }

    public function getDomainMovieCustomerArchived()
    {
        return $this->domainMovieCustomerArchived;
    }

    public function setDomainMovieCustomerArchived($b = null)
    {
        $this->domainMovieCustomerArchived = $b;
    }

    public function setDomainMovieMovedToTop($b = null)
    {
        $this->domainMovieMovedToTop = $b;
    }

    public function getDomainMovieMovedToTop()
    {
        return $this->domainMovieMovedToTop;
    }

    public function setDomainMovieWriting($b = null)
    {
        $this->domainMovieWriting = $b;
    }

    public function getDomainMovieWriting()
    {
        return $this->domainMovieWriting;
    }

    public function getRandAltDescription()
    {
        $altMovies = new Movies;
        $altMovies->setLimit(1);
        $altMovies->setPage(1);
        $altMovies->setOrder('rand()');
        $altMovies->setWithDomainDescription(true);
        $altMovies->setMovieId($this->getId());
        // исключаем говноописания
        $altMovies->setExcludeDescriptionWriters(['olga']);
        $altMoviesArr = $altMovies->get();
        if (! $altMoviesArr) {
            return false;
        }
        $altMovie = new DomainMovie($altMoviesArr[0]['domain_movies_id']);

        return $altMovie->getDomainMovieDescription();
    }

    public function getDomainMovieAltDescriptions()
    {
        logger()->info('Alt для '.$this->getId());
        $altMovies = new Movies;
        $altMovies->setLimit(1);
        $altMovies->setPage(1);
        // $altMovies->setOrder('rand()');
        $altMovies->setWithDomainDescription(true);
        $altMovies->setMovieId($this->getId());
        $altMovies->setDomainMovieShowToCustomer(true);
        $date = new \DateTime;
        $date->modify('-2 weeks');
        // F::dump($date->format('Y-m-d'));
        // выводим только описания, написанные более 2х недель назад
        $altMovies->setDescriptionDateTo($date->format('Y-m-d'));
        // F::dump($altMovies);
        // исключаем говноописания
        $altMovies->setExcludeDescriptionWriters(['olga']);
        // не высчитываем эпизоды, сезоны в getList
        $altMovies->setCalcSeasonsEpisodesData(false);

        return $altMovies;
        // $altMoviesArr = $altMovies->get();
    }

    public function getDomainMovieCustomerComment()
    {
        return $this->domainMovieCustomerComment;
    }

    public function setDomainMovieCustomerComment($comment = null)
    {
        $this->domainMovieCustomerComment = $comment;
    }

    public function getDomainMovieSymbolsFrom()
    {
        return $this->domainMovieSymbolsFrom;
    }

    public function setDomainMovieSymbolsFrom($symbols = null)
    {
        $this->domainMovieSymbolsFrom = $symbols;
    }

    public function getDomainMovieSymbolsTo()
    {
        return $this->domainMovieSymbolsTo;
    }

    public function setDomainMovieSymbolsTo($symbols = null)
    {
        $this->domainMovieSymbolsTo = $symbols;
    }

    public function getDomainMovieBookDate()
    {
        return $this->domainMovieBookDate;
    }

    public function setDomainMovieBookDate($date = null)
    {
        $this->domainMovieBookDate = $date;
    }

    public function getDomainMoviePrivateComment()
    {
        return $this->domainMoviePrivateComment;
    }

    public function setDomainMoviePrivateComment($comment = null)
    {
        $this->domainMoviePrivateComment = $comment;
    }

    public function setDomainMovieMovedToBottom($b = null)
    {
        $this->domainMovieMovedToBottom = $b;
    }

    public function getDomainMovieMovedToBottom()
    {
        return $this->domainMovieMovedToBottom;
    }

    public function getDomainMovieDeletedPage()
    {
        return $this->domainMovieDeletedPage;
    }

    public function getDomainMovieDoRedirect()
    {
        return $this->domainMovieDoRedirect;
    }

    public function setDomainMovieDoRedirect($b = null)
    {
        $this->domainMovieDoRedirect = $b;
    }

    public function getDomainMovieBlockForeigners()
    {
        return $this->domainMovieBlockForeigners;
    }

    public function getOtherSiteLink()
    {
        $altMovies = new Movies;
        $altMovies->setUseDomainMovies(true);
        $altMovies->setMovieId($this->getId());
        $altMovies->setDeleted(false);
        $altMovies->setDomainMovieDeleted(false);
        $altMovies->setDomainMovieActive(true);
        $altMovies->setLimit(100);
        $altMoviesArr = $altMovies->get();
        // F::dump($altMoviesArr);
        if (! $altMoviesArr) {
            return false;
        }
        $altMovieId = $altMoviesArr[array_rand($altMoviesArr)]['domain_movies_id'];
        $altMovie = new DomainMovie($altMovieId);
        $altDomain = new Domain($altMovie->getDomain());
        $link = 'http://'.($altDomain->getRedirectTo() ? $altDomain->getRedirectTo() : $altDomain->getDomain()).$altDomain->getWatchLink($altMovie);

        return $link;
    }

    public function getDomainMovieArticlesList()
    {
        $articles = new Articles;
        $articles->setDomain($this->getDomain());
        $articles->setDomainMovieId($this->getDomainMovieId());
        $articles->setListTemplate('items/list_article');

        return $articles->getList();
    }

    public function generateDmca()
    {
        $str = '~'.substr(bin2hex(openssl_random_pseudo_bytes(4)), 0, 2).'-'.date('d-m');
        if (! $this->getDomainMovieDoRedirect()) {
            $str = $str.'-nr';
        }
        $this->setDomainMovieDmca($str);
    }

    public function getDomainMovieCustomerLike()
    {
        return $this->domainMovieCustomerLike;
    }

    public function setDomainMovieCustomerLike($b = null)
    {
        $this->domainMovieCustomerLike = $b;
    }

    public function getDomainMovieDescriptionLength()
    {
        return mb_strlen((string) $this->getDomainMovieDescription());
    }

    // ожидаемое кол-во символов этого задания
    public function getExpectedSymbols()
    {
        $from = $this->getDomainMovieSymbolsFrom();
        $to = $this->getDomainMovieSymbolsTo();

        // F::dump('ok');
        return Cinema::getExpectedSymbols($from, $to);
    }

    public function getExpectedPrice()
    {
        return round($this->getExpectedSymbols() / 1000 * $this->getDomainMovieCustomerPricePer1k(), 1);
    }

    public function getDomainMovieChecked()
    {
        return $this->domainMovieChecked;
    }

    public function setDomainMovieChecked($b = null)
    {
        $this->domainMovieChecked = $b;
    }

    // возвращает уникальность описания (сверяет с альтернативными описаниями)
    public function getDomainMovieDescriptionUnique()
    {
        // если нет описания, то null
        if (! $this->getDomainMovieDescription()) {
            return null;
        }
        // не выгружаем альтернативные для нет в базе
        if ($this->getUniqueId() == 'not_in_base') {
            return null;
        }
        $altMovies = $this->getDomainMovieAltDescriptions();
        // если слишком много проверять, то не надо.
        if ($altMovies->getRows() > 100) {
            return null;
        }
        $altMovies->setLimit($altMovies->getRows());
        $arr = $altMovies->get();
        $arrSimilar = [];
        // сверяем с самим собой для теста
        // similar_text($this->getDomainMovieDescription(),$this->getDomainMovieDescription(),$similar);
        // $arrSimilar[] = array('text' => $this->getDomainMovieDescription(),'similar' => $similar);
        // $max_similar = $similar;
        // сначала сверяем с описанием кинопоиска
        similar_text(($this->getDomainMovieDescription() ?? ''), ($this->getDescription() ?? ''), $similar);
        $arrSimilar[] = ['text' => $this->getDescription(), 'similar' => $similar];
        // нам нужна максимальная схожесть среди всех описаний, чтобы получить потом уникальность, как 100 минус схожесть
        $max_similar = $similar;
        // затем сверяем с альтернативными описаниями
        foreach ($arr as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            similar_text(($this->getDomainMovieDescription() ?? ''), ($movie->getDomainMovieDescription() ?? ''), $similar);
            $arrSimilar[] = ['text' => $movie->getDomainMovieDescription(), 'similar' => $similar];
            if ($similar > $max_similar) {
                $max_similar = $similar;
            }
        }

        // return F::dump($arrSimilar);
        return 100 - $max_similar;
    }

    public function getDomainMovieIsExpress()
    {
        $cinema = new Cinema;
        if (
            $this->getDomainMovieDescriptionPricePer1k() > $cinema->getRewritePricePer1k()
            and
            $this->getDomainMovieDescriptionPricePer1k() > $cinema->getHighRewriterPricePer1k()
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function getDomainMovieIsBooked()
    {
        if (! $this->getDomainMovieBookedForLogin() or $this->getDomainMovieBookedForLogin() === 'ozerin') {
            return false;
        } else {
            return true;
        }
    }

    public function getDomainMovieIsConfirmed()
    {
        if ($this->getDomainMovieBookedForLogin()) {
            return true;
        } else {
            return false;
        }
    }

    public function getDomainMovieReworkDate()
    {
        return $this->domainMovieReworkDate;
    }

    public function setDomainMovieReworkDate($date = null)
    {
        $this->domainMovieReworkDate = $date;
    }

    public function getDomainMovieModerator()
    {
        return $this->domainMovieModerator;
    }

    public function setDomainMovieModerator($moderator = null)
    {
        $this->domainMovieModerator = $moderator;
    }

    public function getDomainMovieModerationPricePer1k()
    {
        return $this->domainMovieModerationPricePer1k;
    }

    public function setDomainMovieModerationPricePer1k($price = null)
    {
        $this->domainMovieModerationPricePer1k = $price;
    }

    public function getDomainMovieCustomerConfirmDate()
    {
        return $this->domainMovieCustomerConfirmDate;
    }

    public function setDomainMovieCustomerConfirmDate($date = null)
    {
        $this->domainMovieCustomerConfirmDate = $date;
    }

    public function getDomainMovieTextRuWideResult()
    {
        return $this->domainMovieTextRuWideResult;
    }

    public function setDomainMovieTextRuWideResult($result = null)
    {
        $this->domainMovieTextRuWideResult = $result;
    }

    public function getMinutesFromConfirm()
    {
        if (! $this->getDomainMovieCustomerConfirmDate()) {
            return false;
        }
        $confirmDate = (new \DateTime($this->getDomainMovieCustomerConfirmDate()))->getTimestamp();

        return ceil(((new \DateTime)->getTimestamp() - $confirmDate) / 60);
    }

    public function getReworkMinutes()
    {
        if (! $this->getDomainMovieReworkDate()) {
            return false;
        }
        $reworkDate = (new \DateTime($this->getDomainMovieReworkDate()))->getTimestamp();

        return ceil(((new \DateTime)->getTimestamp() - $reworkDate) / 60);
    }

    public function getExecutionMinutes()
    {
        if (! $this->getDomainMovieCustomerConfirmDate()) {
            return false;
        }
        $descriptionDate = (new \DateTime($this->getDomainMovieDescriptionDate() ? $this->getDomainMovieDescriptionDate() : null))->getTimestamp();
        $confirmDate = (new \DateTime($this->getDomainMovieCustomerConfirmDate()))->getTimestamp();

        return ceil(($descriptionDate - $confirmDate) / 60);
    }

    // заполняем объект настройками заказчика
    public function setCustomerSettings($login = null)
    {
        $user = new User($login);
        $this->setDomainMovieCustomer($user->getLogin());
        // создаем цены только здесь (плюс заказчик может делать экспресс и по крону может меняться цена для исполнителя в пределах rewritePrice и highRewritePrice)
        $this->setDomainMovieDescriptionPricePer1k((new Cinema)->getRewritePricePer1k());
        $this->setDomainMovieCustomerPricePer1k($user->getCustomerPricePer1k());
        $this->setDomainMovieSymbolsFrom($user->getSymbolsFrom());
        $this->setDomainMovieSymbolsTo($user->getSymbolsTo());
        $this->setDomainMovieCustomerComment($user->getCustomerComment());
        $this->setDomainMoviePrivateComment($user->getCustomerPrivateComment());
        if ($user->getCustomerAutoExpress()) {
            $this->setDomainMovieDescriptionPricePer1k((new Cinema)->getRewriteExpressPricePer1k());
            $this->setDomainMovieCustomerPricePer1k($user->getCustomerExpressPricePer1k());
        }
    }

    public function getHistory()
    {
        $arr = F::query_arr('
			select id,date,login,property,new_value,old_value
			from '.F::typetab('domain_movie_changes').'
			where id = \''.$this->getDomainMovieId().'\'
			order by date
			;');
        foreach ($arr as $k => $v) {
            $v['old_value'] = json_decode($v['old_value']);
            $v['new_value'] = json_decode($v['new_value']);
            $arr[$k] = $v;
        }

        return $arr;
    }

    public function getLastBookingData($login = null)
    {
        if (! $login) {
            F::error('Login required to use getLastBookingData');
        }
        $arr = F::query_assoc('
			select date,login,property,new_value,old_value
			from '.F::typetab('domain_movie_changes').'
			where id = \''.$this->getDomainMovieId().'\' and property = \'domainMovieBookedForLogin\'
			and new_value = \''.json_encode($login).'\'
			order by date desc
			limit 1
			;');
        if (! $arr) {
            return null;
        }
        $arr['old_value'] = json_decode($arr['old_value']);
        $arr['new_value'] = json_decode($arr['new_value']);

        return $arr;
    }

    public function clearTask()
    {
        $this->setDomainMovieDescription(null);
        $this->setDomainMovieDescriptionWriter(null);
        $this->setDomainMovieBookedForLogin('ozerin');
        $this->setDomainMovieDescriptionDate(null);
        $this->setDomainMovieRework(false);
        $this->setDomainMovieReworkDate(null);
        $this->setDomainMovieReworkComment(null);
        $this->setDomainMovieShowToCustomer(false);
        $this->setDomainMovieTextRuUnique(null);
        $this->setDomainMovieTextRuResult(null);
        $this->setDomainMovieTextRuDescriptionId(null);
        $this->setDomainMovieTextRuWideResult(null);
        $this->setDomainMovieBookDate(null);
        $this->setDomainMovieModerator(null);
        $this->setDomainMovieModerationPricePer1k(null);
        $this->setDomainMovieCustomerLike(null);
    }

    public function getTaskType()
    {
        if ($this->getUniqueId() == 'not_in_base') {
            return 'other';
        }
        if (in_array($this->getType(), ['movie', 'serial', 'trailer', 'game'])) {
            return 'movie';
        }

        return $this->getType();
    }

    public function getDomainMovieAdult()
    {
        return $this->domainMovieAdult;
    }

    public function setDomainMovieAdult($b = null)
    {
        $this->domainMovieAdult = $b;
    }

    public function getEloquent()
    {
        if (is_null($this->eloquent)) {
            $this->eloquent = DomainMovieEloquent::find($this->getId());
        }

        return $this->eloquent;
    }

    public function getDomainMovieReworkAllowed()
    {
        if ($this->getDomainMovieDescription()) {
            $allowed = true;
            if ($this->getDomainMovieRework()) {
                $allowed = false;
            }
            if ($this->getDomainMovieCustomerArchived()) {
                $allowed = false;
            }
            if (! $this->getDomainMovieShowToCustomer()) {
                $allowed = false;
            }
        } else {
            $allowed = false;
        }
        if ($allowed) {
            if (date_diff(new \DateTime, new \DateTime($this->getDomainMovieDescriptionDate()))->days > 14) {
                $allowed = false;
            }
        }

        return $allowed;
    }

    public function getEpisodesDataList()
    {
        if (! $this->getEpisodesDataListTemplate()) {
            F::error('use setEpisodesDataListTemplate first to use getEpisodesDataList');
        }
        $movie = DomainMovieEloquent::find($this->getDomainMovieId());
        $episodes = $movie->episodes()->orderByDesc('season')->orderByDesc('episode')->get();
        $list = '';
        $i = 0;
        foreach ($episodes as $v) {
            $num = count($episodes) - $i;
            $i++;
            $t = new Template($this->getEpisodesDataListTemplate());
            $t->v('season', $v->season);
            $t->v('episode', $v->episode);
            $t->v('episode_id', $v->id);
            $t->v('episodeLink', $movie->episodeViewRoute($v));
            $t->v('title', $v->title);
            $t->v('shortName', $v->short_name);
            // выводим дату на русском
            $formatter = new \IntlDateFormatter(
                'ru_RU',
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::NONE,
                'Europe/Moscow'
            );
            if ($v->air_date) {
                $airDate = new \DateTime($v->air_date);
                $t->v('airDate', $formatter->format($airDate->getTimestamp()));
                $currentDate = new \DateTime;
                $t->v('daysToRelease', ($airDate->diff($currentDate)->invert ? (+1) : (-1)) * $airDate->diff($currentDate)->days + 1);
            } else {
                $t->v('airDate', null);
                $t->v('daysToRelease', null);
            }
            $t->v('num', $num);
            $list .= $t->get();
        }

        return $list;
    }
}
