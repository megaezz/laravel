<?php

namespace cinema\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class Groups
{
    private $rows = null;

    private $limit = null;

    private $page = null;

    private $order = null;

    private $listTemplate = null;

    private $movieId = null;

    private $active = null;

    private $selected = null;

    private $type = null;

    private $useDomainGroup = null;

    private $groupId = null;

    private $domain = null;

    // объект домена (для генерации ссылок например)
    private $domainObject = null;

    // только категории для которых есть фильмы на указанном домене
    private $withDomainMovies = null;

    private $lang = null;

    public function __construct() {}

    public function get()
    {
        if (! $this->getPage()) {
            $this->setPage(1);
        }
        if ($this->getLimit()) {
            $sql['limit'] = $this->getLimit() ? ('limit '.($this->getPage() - 1) * $this->getLimit().','.$this->getLimit()) : '';
        } else {
            $sql['limit'] = '';
        }
        $sql['order'] = $this->getOrder() ? 'order by '.$this->getOrder() : '';
        $sql['active'] = is_null($this->getActive()) ? 1 : ($this->getActive() ? 'groups.active = 1' : 'groups.active = 0');
        $sql['type'] = $this->getType() ? 'groups.type = \''.$this->getType().'\'' : 1;
        if ($this->getMovieId()) {
            $sql['joinGroupsTags'] = 'left join '.F::typetab('groups_tags').' on groups_tags.group_id = groups.id';
            $sql['groupBy'] = 'group by groups.id';
            $sql['having'] = 'having sum(if (groups_tags.tag_id in (select tag_id from '.F::typetab('cinema_tags').' where cinema_id = \''.$this->getMovieId().'\'),1,0)) = count(*)';
        } else {
            $sql['joinGroupsTags'] = '';
            $sql['groupBy'] = '';
            $sql['having'] = '';
        }
        if ($this->getUseDomainGroup()) {
            $sql['joinDomainGroups'] = 'join '.F::typetab('domain_groups').' on domain_groups.group_id = groups.id';
            $sql['domain_group_id'] = ',domain_groups.id as domain_group_id';
            $sql['domain'] = 'domain_groups.domain = \''.$this->getDomain().'\'';
        } else {
            $sql['joinDomainGroups'] = '';
            $sql['domain_group_id'] = '';
            $sql['domain'] = 1;
        }
        $sql['groupId'] = $this->getGroupId() ? ('groups.id = \''.$this->getGroupId().'\'') : 1;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS groups.id
			'.$sql['domain_group_id'].'
			from '.F::typetab('groups').'
			'.$sql['joinDomainGroups'].'
			'.$sql['joinGroupsTags'].'
			where
			'.$sql['active'].'
			and '.$sql['type'].'
			and '.$sql['groupId'].'
			and '.$sql['domain'].'
			'.$sql['groupBy'].'
			'.$sql['having'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        if ($this->getWithDomainMovies()) {
            $arrWithDomainMovies = [];
            if (! $this->getDomain()) {
                F::error('use Groups::setDomain first to use Groups::withDomainMovies');
            }
            foreach ($arr as $v) {
                $movies = new Movies;
                $movies->setActive(true);
                $movies->setDeleted(false);
                $movies->setDomain($this->getDomain());
                $movies->setGroup($v['id']);
                // F::dump($movies);
                if (! $movies->getRows()) {
                    continue;
                }
                $arrWithDomainMovies[] = $v;
            }
            $arr = $arrWithDomainMovies;
            $this->rows = count($arr);
        } else {
            $this->rows = F::rows_without_limit();
        }

        // $this->rows = F::rows_without_limit();
        return $arr;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function getList($args = [])
    {
        if (! $this->getListTemplate()) {
            F::error('setListTemplate first');
        }
        $list = '';
        if (isset($args['arr'])) {
            $arr = $args['arr'];
        } else {
            $arr = $this->get();
        }
        foreach ($arr as $v) {
            if (isset($args['arr'])) {
                $group = $v->megaweb;
            } else {
                $group = new Group($v['id']);
            }
            $t = new Template($this->getListTemplate());
            $t->setLang($this->getLang());
            if ($v['id'] === $this->getSelected()) {
                $t->v('groupActive', 'active');
                $t->v('groupSelected', 'selected');
            } else {
                $t->v('groupActive', '');
                $t->v('groupSelected', '');
            }
            // $t->v('groupName',$group->getName());
            $t->v('name', [
                'ru' => $group->getName(),
                'ua' => $group->getNameUa(),
                'en' => $group->getNameEn(),
            ]);
            $t->v('groupId', $group->getId());
            $t->v('groupNameUrl', $group->getNameUrl());
            $t->v('groupEmoji', $group->getEmoji());
            $t->v('groupThumb', $group->getThumb());
            // $t->v('nameUrl',$group->getNameUrl());
            // $t->v('tagEmoji',$tag->getEmoji());
            if ($this->getDomainObject()) {
                $t->v('groupLink', $this->getDomainObject()->getGroupLink($group));
            } else {
                $t->v('groupLink', '');
            }
            $list .= $t->get();
        }

        return $list;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function setMovieId($movieId = null)
    {
        $this->movieId = $movieId;
    }

    public function getMovieId()
    {
        return $this->movieId;
    }

    // для отображение active и selected классов css в getList
    public function setSelected($id = null)
    {
        $this->selected = $id;
    }

    public function getSelected()
    {
        return $this->selected;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getUseDomainGroup()
    {
        return $this->useDomainGroup;
    }

    public function setUseDomainGroup($b = null)
    {
        $this->useDomainGroup = $b;
    }

    public function setGroupId($groupId = null)
    {
        $this->groupId = $groupId;
    }

    public function getGroupId()
    {
        return $this->groupId;
    }

    public function setDomain($domain = null)
    {
        if (! is_null($domain)) {
            $this->setUseDomainGroup(true);
        }
        $this->domain = $domain;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getDomainObject()
    {
        if (! $this->domainObject) {
            if ($this->getDomain()) {
                $this->setDomainObject(new Domain($this->getDomain()));
            }
        }

        return $this->domainObject;
    }

    public function setDomainObject($domainObject = null)
    {
        $this->domainObject = $domainObject;
        $this->setDomain($this->getDomainObject()->getDomain());
    }

    public function setWithDomainMovies($b = null)
    {
        $this->withDomainMovies = $b;
    }

    public function getWithDomainMovies()
    {
        return $this->withDomainMovies;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang = null)
    {
        $this->lang = $lang;
    }
}
