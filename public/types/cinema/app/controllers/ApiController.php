<?php

namespace cinema\app\controllers;

use cinema\app\models\Domain;
use cinema\app\models\DomainMovie;
use cinema\app\models\Feedback;
use cinema\app\models\Moonwalk;
use cinema\app\models\Movie;
use cinema\app\models\Movies;
use cinema\app\models\MovieTags;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class ApiController
{
    private $domain;

    private $domainEloquent;

    public function __construct()
    {
        $this->domainEloquent = Engine::getDomainEloquent()->cinemaDomain ?? F::error('Domain was not found');
        $this->domain = $this->domainEloquent->megaweb;
    }

    public function handleRequest($method)
    {

        return method_exists($this, $method) ? $this->{$method}() : throw new \Exception('Method not found');

    }

    public function unsubscribe()
    {
        $email = $_GET['email'] ?? F::error('Не передана почта');
        // $code = $_GET['code']??F::error('Не передан проверочный код');
        $code = 'test';
        logger()->info('Запрошена отписка от рассылки. Почта: '.$email.' код: '.$code);

        return 'ok';
    }

    public function savedMovieInfo()
    {
        $movieId = empty($_GET['movieId']) ? F::error('movieId required to Api::savedMovieInfo') : F::checkstr($_GET['movieId']);
        // $movie = new DomainMovie($movieId);
        // if ($movie->getDomain() !== $this->domain->getDomain()) {
        // 	return 'Видео не найдено';
        // }
        // return '<li><a href="'.$this->domain->getWatchLink($movie).'">'.$movie->getTitleRu().'</a></li>';
        $movies = new Movies;
        $movies->setDomainMovieId($movieId);
        $movies->setDomain($this->domain->getDomain());
        $movies->setListTemplate('items/movie');
        $movies->setDeleted(false);
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setActive(true);

        // $movies->setDomainClass($this->domain);
        // F::dump($movies);
        // $movies->setWithUniqueDescription($this->domain->getMoviesWithUniqueDescription());
        return $movies->getList();
    }

    public function lucky()
    {
        $movies = new Movies;
        $movies->setDomain($this->domain->getDomain());
        $movies->setLimit(250);
        $movies->setPage(1);
        $movies->setDeleted(false);
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setActive(true);
        // $movies->setDomainClass($this->domain);
        $movies->setOrder('cinema.rating_sko desc');
        $arr = F::cache($movies, 'get');
        $rand = array_rand($arr);
        $movie = new DomainMovie($arr[$rand]['domain_movies_id']);
        F::redirect($this->domain->getWatchLink($movie));
        // F::dump($this->domain->getWatchLink($movie));
    }

    public function searchInputHelperMegaweb()
    {
        Engine::setDebug(false);
        $query = empty($_GET['q']) ? null : trim(htmlspecialchars(F::checkstr($_GET['q'])));
        if (mb_strlen($query ?? '') < 2) {
            return;
        }
        $lang = empty($_GET['lang']) ? null : $_GET['lang'];
        $movies = new Movies;
        $movies->setLang($lang);
        // if ($this->domain->getMoviesWithUniqueDescription()) {
        $movies->setDomain($this->domain->getDomain());
        // }
        $movies->setSearch($query);
        $movies->setLeftSearch(true);
        $movies->setLimit(10);
        $movies->setPage(1);
        $movies->setOrder('cinema.kinopoisk_rating desc');
        $movies->setListTemplate('items/list_search_helper_movies');
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setDomainMoviePageDeleted(false);
        $movies->setActive(true);
        // $movies->setDomainClass($this->domain);
        // $movies->setWithUniqueDescription($this->domain->getMoviesWithUniqueDescription());
        $list_1 = $movies->getList();
        $list_2 = '';
        $list_3 = '';
        if (! $movies->getRows()) {
            $movies->setLeftSearch(false);
            $list_2 = $movies->getList();
        }
        if (! $movies->getRows()) {
            $movies->setLeftSearch(true);
            $movies->setSearch(F::fixLayoutToRus($query));
            $list_3 = $movies->getList();
        }

        return $list_1.$list_2.$list_3;
    }

    public function searchInputHelper()
    {

        $searchQuery = $_GET['q'] ?? null;

        $locale = $_GET['lang'] ?? null;

        if (empty($searchQuery)) {
            return null;
        }

        /* удаляем спец символы, иначе ошибка, баг в mysql с boolean mode */
        $searchQuery = preg_replace('/[^\p{L}\p{N}\s]/u', '', trim($searchQuery));

        if (mb_strlen($searchQuery) < 2) {
            return null;
        }

        if ($locale) {
            app()->setLocale($locale);
        }

        $variants = [$searchQuery, F::fixLayoutToRus($searchQuery)];

        foreach ($variants as $searchQuery) {
            $movies = $this->domainEloquent
                ->movies()
                ->with('movie')
                ->available()
                ->whereHas('movie', function ($query) use ($searchQuery) {
                    // $query->whereRaw("MATCH(title_ru, title_en) AGAINST(? IN BOOLEAN MODE)", [$searchQuery . '*']);
                    $query->whereRaw('MATCH(title_ru) AGAINST(?)', [$searchQuery]);
                    // $query
                    // ->where('title_ru', 'like', "%$searchQuery%")
                    // ->orWhere('title_en', 'like', "%$searchQuery%");
                })
                ->limit(10)
            // ->dumprawsql()
            // ->orderByDesc('kinopoisk_rating')
                ->get();

            if ($movies->count()) {
                break;
            }
        }

        // dd($movies);

        return F::view('search-input-helper')
            ->with('movies', $movies)
            ->with('domain', $this->domainEloquent);
    }

    public function getThumbnails()
    {
        Engine::setDebug(false);
        $movieId = empty($_GET['id']) ? F::error('Movie ID required to getThumnails') : F::checkstr($_GET['id']);
        $movie = new Movie($movieId);
        $moonwalk = new Moonwalk(Moonwalk::getBestTokenByUniqueId($movie->getUniqueId()));
        $thumbnails = F::cache($moonwalk, 'getThumbnails');

        return json_encode($thumbnails ? $thumbnails : []);
    }

    public function showMovieShortInfo()
    {
        Engine::setDebug(false);
        $domainMovieId = empty($_GET['id']) ? F::error('Specify video ID') : F::checkstr($_GET['id']);
        $t = new Template('items/movie_short_info');
        // if ($this->domain->getMoviesWithUniqueDescription()) {
        $movie = new DomainMovie($domainMovieId);
        if ($movie->getDomain() != $this->domain->getDomain() or ! $movie->getDomainMovieActive()) {
            F::error('Movie not found');
        }
        $t->v('domainMovieDescription', mb_strimwidth(($movie->getDomainMovieDescription() ?? ''), 0, 500, '...'));
        $t->v('domainMovieId', $movie->getDomainMovieId());
        // } else {
        // $movie = new Movie($id);
        // }
        $t->v('movieId', $movie->getId());
        $t->v('movieTitleRu', $movie->getTitleRu());
        $t->v('movieTitleRuUrl', $movie->getTitleRuUrl());
        $t->v('movieTitleEn', $movie->getTitleEn());
        $t->v('movieTitleEnIfExist', empty($movie->getTitleEn()) ? '' : '/ '.$movie->getTitleEn());
        $t->v('moviePoster', $movie->getPosterOrPlaceholder());
        if ($movie->getType() == 'trailer') {
            $t->v('movieYear', date('Y'));
            $t->v('movieType', 'Фильм');
        } else {
            $t->v('movieYear', $movie->getYear());
            $t->v('movieType', $movie->getTypeRu(true));
        }
        $t->v('movieImdbRating', $movie->getImdbRating() ? $movie->getImdbRating() : '?');
        $t->v('movieKinopoiskRating', $movie->getKinopoiskRating() ? $movie->getKinopoiskRating() : '?');
        $t->v('movieKinopoiskVotes', $movie->getKinopoiskVotes() ? $movie->getKinopoiskVotes() : '?');
        $t->v('movieDescription', mb_strimwidth(($movie->getDescription() ?? ''), 0, 500, '...'));
        $t->v('movieKinopoiskId', $movie->getKinopoiskId());
        // dump(Engine::getRunningTime());
        $mt = new MovieTags($movie->getId());
        $mt->setListTemplate('items/list_tag');
        $t->v('movieGenres', F::cache($mt, 'getGenres'));
        $t->v('movieCountries', F::cache($mt, 'getCountries'));
        $t->v('movieActors', F::cache($mt, 'getActors'));
        $t->v('movieStudios', F::cache($mt, 'getStudios'));
        $t->v('movieDirectors', F::cache($mt, 'getDirectors'));
        $mt->setListTemplate('items/list_tag_text');
        $t->v('movieCountriesText', F::cache($mt, 'getCountries'));
        $t->v('movieDirectorsText', F::cache($mt, 'getDirectors'));
        $addDate = new \DateTime($movie->getDomainMovieAddDate());
        $t->v('movieAddDate', $addDate->format('d.m.Y'));
        // foreach (F::cache($movie,'getTranslators') as $translator) {
        // 	$tTranslator = new Template('items/list_tag');
        // 	$tTranslator->v('name',$);
        // }
        $t->v('movieTranslators', implode(', ', F::cache($movie, 'getTranslators')));

        return $t->get();
    }

    public function addComment()
    {
        $recaptchaResponse = empty($_POST['g-recaptcha-response']) ? F::error('Recaptcha response is required') : F::checkstr($_POST['g-recaptcha-response']);
        if (! Engine::checkRecaptcha($recaptchaResponse, $this->domain->getRecaptchaServerKey())) {
            F::alert('Sorry, you didn\'t pass a robot check ('.$this->domain->getRecaptchaVersion().'). Please try again one more time.');
        }
        $name = trim($_POST['name']);
        $text = trim($_POST['message']);
        if (empty($name)) {
            F::error('Name is required to add comment');
        }
        if (empty($text)) {
            F::error('Message is required to add comment');
        }
        $domainMovie = $this->domainEloquent->movies()->findOrFail($_POST['domain_movie_id']);
        $banned_words = ['KRAKEN', 'HYDRA', '2krn', 'a href=', 'url=', 'https', 'http'];
        foreach ($banned_words as $string) {
            if (strpos($text, $string)) {
                logger()->info('Забанили комментарий: '.$text);
                // молча не добавляем
                F::redirect(F::referer_url());
            }
        }

        $comment = $domainMovie->comments()->make();
        $comment->username = $name;
        $comment->text = $text;
        $comment->save();
        F::redirect(F::referer_url());
    }

    public function submitFeedback()
    {
        $recaptchaResponse = empty($_POST['g-recaptcha-response']) ? F::error('Recaptcha response is required') : F::checkstr($_POST['g-recaptcha-response']);
        if (! Engine::checkRecaptcha($recaptchaResponse, $this->domain->getRecaptchaServerKey())) {
            F::alert('Sorry, you didn\'t pass a robot check. Please try again one more time.');
        }
        $theme = empty($_POST['theme']) ? '' : trim($_POST['theme']);
        $email = empty($_POST['email']) ? '' : trim($_POST['email']);
        $text = empty($_POST['message']) ? F::error('Message is required to add comment') : trim($_POST['message']);
        // F::dump($text);
        $createdFeedbackId = Feedback::add();
        $feedback = new Feedback($createdFeedbackId);
        $feedback->setTheme($theme);
        $feedback->setEmail($email);
        $feedback->setText($text);
        $feedback->setDomain($this->domain->getDomain());
        $feedback->save();
        F::alert('Спасибо, ваше обращение принято к рассмотрению.');
    }

    // плеер сервиса hdgo.cc
    public function getHdgoIframe()
    {
        Engine::setDebug(false);
        $kinopoisk_id = empty($_GET['kp_id']) ? F::error('Kinopoisk ID required') : F::checkstr($_GET['kp_id']);
        $answer = file_get_contents('https://vio.to/api/video.json?token=uatzdwa1a2xxhl5w03yn1kve&kinopoisk_id='.$kinopoisk_id);
        $arr = json_decode($answer);
        // если вместо массива объект то значит ошибка
        if (is_object($arr)) {
            return '';
        }

        // F::dump($arr);
        return isset($arr[0]->iframe_url) ? F::redirect($arr[0]->iframe_url) : '';
    }

    // плеер сервиса hdgo.cc
    public function getIframeVideoIframe()
    {
        Engine::setDebug(false);
        $kinopoisk_id = empty($_GET['kp_id']) ? F::error('Kinopoisk ID required') : F::checkstr($_GET['kp_id']);
        $url = 'https://iframe.video/api/v2/search?kp='.$kinopoisk_id;
        $session = curl_init($url);
        curl_setopt($session, CURLOPT_HTTPHEADER, ['X-TOKEN: d17ce1aeabf14425ec1ccf74acb9ec25']);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($session, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($session, CURLOPT_USERAGENT, 'API v2 requester, v1.0');
        curl_setopt($session, CURLOPT_TIMEOUT, 2);
        $answer = curl_exec($session);
        $code = curl_getinfo($session, CURLINFO_HTTP_CODE);
        if ($code != 200) {
            return false;
        }
        $result = json_decode($answer);
        $results = $result->results;

        return isset($results[0]->path) ? F::redirect($results[0]->path) : '';
    }

    // плеер сервиса hdvb.cc
    public function getHdvbIframe()
    {
        Engine::setDebug(false);
        $kinopoisk_id = empty($_GET['kp_id']) ? F::error('Kinopoisk ID required') : F::checkstr($_GET['kp_id']);
        // узнаем что за фильм (нужно для 4к)
        $movies = new Movies;
        $movies->setKinopoiskId($kinopoisk_id);
        $arr = $movies->get();
        if (! $arr) {
            return '';
        }
        $movie = new Movie($arr[0]['id']);
        //
        // запрашиваем данные hdvb
        $answer = file_get_contents('https://my-serials.info/api/movie.json?token=6721e3457628a64855fc0991f1b271af&id_kp='.$kinopoisk_id);
        $arr = json_decode($answer);
        // если вместо массива объект то значит ошибка
        if (is_object($arr)) {
            return '';
        }
        // если есть объект с качеством 4к, то прописываем в бд
        // F::dump($arr[2]->quality);
        if (! $movie->getHdvb4k()) {
            foreach ($arr as $v) {
                if ($v->quality === '4К') {
                    $movie->setHdvb4k(true);
                    $movie->save();
                }
            }
        }

        // F::dump($arr);
        return isset($arr[0]->iframe_url) ? F::redirect(str_replace('https://tehranvd.pw', 'https://vid'.time().'.farsihd.pw', $arr[0]->iframe_url)) : '';
    }

    // плеер сервиса kodik.info
    public function getKodikIframe($args = [])
    {
        Engine::setDebug(false);
        $lang = empty($_GET['lang']) ? 'ru' : F::checkstr($_GET['lang']);
        $kinopoisk_id = empty($_GET['kp_id']) ? F::error('Kinopoisk ID required') : F::checkstr($_GET['kp_id']);
        $translation_str = '';
        if ($lang == 'en') {
            $translation_str = '&translation_id=882';
        }
        $answer = file_get_contents('https://kodikapi.com/search?token=60b2be925c7d5e6b2eceef9988044a73&kinopoisk_id='.$kinopoisk_id.$translation_str);
        $arr = json_decode($answer);

        // F::dump($arr);
        // дописываем в конце ссылки ?uid=2nWm1R, чтобы подтягивались настройки для аккаунта и не выводилась нелг реклама
        return empty($arr->results) ? '' : F::redirect($arr->results[0]->link.'?uid=2nWm1R');
    }
}
