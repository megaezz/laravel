<?php

namespace cinema\app\controllers;

use cinema\app\models\User;
use engine\app\models\F;

class SeosprintController extends WorkerController
{
    public function __construct()
    {
        $this->user = new User('seosprint');
    }

    public function rewrited($args = [])
    {
        $domainMovieId = $_GET['id'] ?? null;
        $rework = $_GET['rework'] ?? null;
        if (! $domainMovieId) {
            // F::error('Не указан ID');
        }
        if (! $rework) {
            F::error('Разрешена только доработка');
        }

        return parent::rewrited([
            'template' => 'admin/rewrite/seosprint/rewrited',
            'item_rework_template' => 'admin/rewrite/seosprint/item_rework',
            'other_rework_template' => 'admin/rewrite/seosprint/other_rework',
        ]);
    }

    public function submitRewrite($args = [])
    {
        return parent::submitRewrite([
            'return_function' => function () {
                F::alertLite('<h1>Спасибо. Задание отправлено на проверку.</h1>');
            },
        ]);
    }
}
