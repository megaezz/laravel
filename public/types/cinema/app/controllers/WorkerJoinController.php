<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\User;
use cinema\app\models\Users;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class WorkerJoinController
{
    public function sendPassword()
    {
        Engine::setDebug(false);
        if (empty($_POST['login'])) {
            return 'Укажите логин';
        } else {
            $login = F::checkstr($_POST['login']);
        }
        $users = new Users;
        $users->setLogin($login);
        $users->setLevels(['worker', 'admin']);
        $arr = $users->get();
        if (! $arr) {
            return 'Указанный пользователь не найден в системе';
        }
        $user = new User($arr[0]['login']);
        if (! $user->getEmail()) {
            return 'Для данного аккаунта не указан email адрес, мы не знаем куда отправить пароль. Для восстановления доступа напишите в поддержку.';
        }
        // отправляем письмо
        $email = new Template('admin/rewrite/emails/forgot_password');
        $email->v('login', $user->getLogin());
        $email->v('password', $user->getPassword());
        (new Cinema)->sendMail($user->getEmail(), 'Кинотекст', 'Восстановление пароля', $email->get(), 'kinotext');

        return 'Данные для доступа отправлены на email указанный в аккаунте';
    }

    public function join()
    {
        Engine::setDebug(false);
        $login = $_POST['login'] ?? null;
        $email = $_POST['email'] ?? null;
        // $password = $_POST['pass']??null;
        $partnerId = empty($_COOKIE['mgw_partner_cinema']) ? null : F::checkstr($_COOKIE['mgw_partner_cinema']);
        if (! filter_var(
            $login,
            FILTER_VALIDATE_REGEXP,
            ['options' => ['regexp' => '/^([\w0-9.]){4,20}$/']]
        )) {
            return '
			<div class="mt-4 authErrorMsg">
			Некорректный логин. Он должен быть на английском, от 4 до 20 символов.
			</div>';
        }
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return '
			<div class="mt-4 authErrorMsg">
			Некорректный адрес почты
			</div>';
        }
        // проверяем существует ли такой логин
        $users = new Users;
        $users->setLogin($login);
        if ($users->get()) {
            return '
			<div class="mt-4 authErrorMsg">
			Такой логин уже существует, попробуйте другой
			</div>';
        }
        // проверяем существует ли такой email
        $users = new Users;
        $users->setEmail($email);
        $users->setEmailConfirmed(true);
        if ($users->get()) {
            return '
			<div class="mt-4 authErrorMsg">
			Пользователь с такой почтой уже существует, воспользуйтесь восстановлением пароля.
			</div>';
        }
        $generatedPasswords = F::generateRandomStr(8);
        // F::pre($generatedPasswords);
        User::add($login);
        $user = new User($login);
        $user->setLevel('worker');
        $user->setPassword($generatedPasswords);
        if ($partnerId) {
            $users = new Users;
            $users->setId($partnerId);
            $arr = $users->get();
            if ($arr) {
                $partner = new User($arr[0]['login']);
                $partnerLogin = $partner->getLogin();
                $user->setPartner($partnerLogin);
                // если пришел с сеоспринта, то можно не сдавать тест
                if ($user->getPartner() == 'seosprint') {
                    $user->setTestPassed(true);
                }
            }
        }
        $user->setEmail($email);
        // сразу генерим проверочный код т.к. дальше идет отправка
        $user->setEmailCode(F::generateRandomStr(40));
        $user->save();
        // отправляем письмо
        $email = new Template('admin/rewrite/emails/join');
        $email->v('emailCode', $user->getEmailCode());
        $email->v('login', $user->getLogin());
        $email->v('password', $user->getPassword());
        (new Cinema)->sendMail($user->getEmail(), 'Кинотекст', 'Регистрация в сервисе', $email->get(), 'kinotext');

        return '
		<div class="mt-4">
		<h5>Вы успешно зарегистрировались</h5>
		<p>
		<b>Логин:</b> '.$user->getLogin().'<br>
		<b>Пароль:</b> '.$user->getPassword().'
		</p>
		<form method="post" action="/">
		<input type="hidden" name="login" value="'.$user->getLogin().'">
		<input type="hidden" name="pass" value="'.$user->getPassword().'">
		<input type="hidden" name="enter" value="">
		<input type="hidden" name="type" value="{type}">
		<input type="submit" value="Перейти в аккаунт" class="btn btn-warning btn-lg">
		</form>
		</div>';
    }

    public function confirmEmail()
    {
        $code = $_GET['code'] ?? null;
        $login = $_GET['login'] ?? null;
        if (! $login) {
            F::error('Не передан логин');
        }
        if (! $code) {
            F::error('Не передан код подтверждения');
        }
        $user = new User($login);
        if ($code == $user->getEmailCode()) {
            $user->setEmailConfirmed(true);
            $user->save();
            F::alert('Адрес подтвержден');
            F::redirect('/?r=Worker/userInfo');
        }
        F::alert('Адрес не подтвержден');
    }
}
