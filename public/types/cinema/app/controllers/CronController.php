<?php

namespace cinema\app\controllers;

use Carbon\Carbon;
use cinema\app\models\Cinema;
use cinema\app\models\DomainMovie;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Domain;
use cinema\app\models\eloquent\DomainMovieComment;
use cinema\app\models\eloquent\User as UserEloquent;
use cinema\app\models\eloquent\Videodb as VideodbEloquent;
use cinema\app\models\Movies;
use cinema\app\models\User;
use cinema\app\models\Users;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\TextRu;

class CronController
{
    public static function syncStaticFolderOnMgw6()
    {
        return 'Защита от выполнения';
        $command = 'rsync -az megaweb@tunnel_mgw5:~/www/megaweb.v2/static /mnt';
        exec($command, $output, $returnCode);

        return $output;
    }

    public static function checkZerocdn()
    {
        $result = file_get_contents('https://mng.zerocdn.com/api/v2/users/info/?username='.Config::cached()->zerocdn_credentials->login.'&api_key='.Config::cached()->zerocdn_credentials->apiSecret);
        $result = json_decode($result);
        // если делить 3 раза на 1024 получается число меньше чем отбражается в ЛК, а если 1000 то такое же.
        $gbit = $result->speed * 8 / 1000 / 1000 / 1000;
        $return = 'Zerocdn speed: '.round($gbit, 2).' gbit/s';
        if ($gbit > Config::first()->zerocdn_max_speed) {
            // logger()->alert($return);
        }
        logger()->info($return);

        return $return;
    }

    // отправляем описания на проверку на текст.ру
    public static function textRuSend()
    {
        Engine::setType('cinema');
        // получаем задания заказчиков, у которых стоит автопроверка, не присвоен textRuId, написано описание и они не находятся на модерации (чтобы лишний раз не тратить деньги на копипастеров)
        // добавил show_to_customer = 0, чтоб проверялось все, что модерации, т.к. много новых исполнителей.
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS domain_movies.id,
			(select count(*) from '.F::typetab('domain_movies').' dm
			where dm.description_writer = domain_movies.description_writer
			and dm.show_to_customer = 0
			and dm.text_ru_description_id is not null
			and dm.description_length > 0) as q_already_sent
			from '.F::typetab('domain_movies').'
			join '.F::typetab('users').' on users.login = domain_movies.customer
			where
			(
			/* (users.unique_auto_check and
			domain_movies.show_to_customer) or */
			domain_movies.show_to_customer = 0
			) and
			domain_movies.text_ru_description_id is null and
			domain_movies.description_length > 0
			having q_already_sent < 4
			limit 4
			;');
        $rows = F::rows_without_limit();
        // F::dump($rows);
        $textRu = new TextRu;
        $textRu->setApiKey('d6c6c0b10e560da9dd8c1a56122d31c2');
        $textRu->setVisible('vis_on');
        $i = 0;
        foreach ($arr as $v) {
            $movie = new DomainMovie($v['id']);
            $textRu->setText($movie->getDomainMovieDescription());
            $textRuId = $textRu->addPost();
            if ($textRuId) {
                $movie->setDomainMovieTextRuDescriptionId($textRuId);
                $movie->setDomainMovieTextRuResult(null);
                $movie->setDomainMovieTextRuUnique(null);
                $movie->save();
                // logger()->info('Text.ru: Добавлен на проверку #'.$movie->getDomainMovieId());
                $i++;
            } else {
                logger()->info('Text.ru: Ошибка при добавлении на проверку #'.$movie->getDomainMovieId());
            }
        }
        $return = 'Text.ru: Добавлено на проверку: '.$i.' из '.$rows;
        logger()->info($return);

        return $return;
        // domain_movies.text_ru_result is null and
    }

    // получаем проверенные описания с текст.ру
    public static function textRuGet()
    {
        Engine::setType('cinema');
        // получаем задания для которых указан textRuId но нет результата
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS domain_movies.id
			from '.F::typetab('domain_movies').'
			where
			domain_movies.text_ru_description_id is not null and
			/* domain_movies.text_ru_result is null */
			domain_movies.add_date > \'2022-05-01\' and
			domain_movies.text_ru_wide_result is null
			limit 4
			;');
        $rows = F::rows_without_limit();
        // F::dump($arr);
        $textRu = new TextRu;
        $textRu->setApiKey('d6c6c0b10e560da9dd8c1a56122d31c2');
        $i = 0;
        foreach ($arr as $v) {
            $movie = new DomainMovie($v['id']);
            $textRu->setTextId($movie->getDomainMovieTextRuDescriptionId());
            $result = $textRu->getResultPost();
            // F::dump($result->seo_check == 'null');
            if ($result) {
                $movie->setDomainMovieTextRuResult(json_decode($result->result_json));
                if (! empty($result->seo_check) and $result->seo_check != 'null') {
                    $movie->setDomainMovieTextRuWideResult($result);
                }
                $movie->setDomainMovieTextRuUnique($result->unique);
                $movie->save();
                // logger()->info('Text.ru: Получен результат для #'.$movie->getDomainMovieId());
                $i++;
                // logger()->info('Данные обновлены');
            }
        }
        $return = 'Text.ru: Получен результат проверки: '.$i.' из '.$rows;
        logger()->info($return);

        return $return;
    }

    public static function setCustomersActiveTasks()
    {
        // получаем список активных заказчиков
        $users = new Users;
        $users->setLevel('customer');
        $users->setOrder('users.temp_balance desc');
        $users->setTempBalanceIs('>0');
        // F::dump($users->get());
        $arr = $users->get();
        foreach ($arr as $v) {
            // считаем сколько заданий в очереди у этого заказчика
            $movies = new Movies;
            $movies->setDomainMovieCustomer($v['login']);
            $movies->setUseDomainMovies(true);
            $movies->setWithDomainDescription(false);
            // отображаем задания, тех пользователей у которых достаточный баланс
            $movies->setDomainMovieWithCustomerEnoughBalance(true);
            $rows = $movies->getRows();
            $user = new User($v['login']);
            // записываем кол-во активных заданий
            $user->setCustomerActiveTasks($rows);
            $user->save();
        }
    }

    public static function calcTempBalances()
    {
        Engine::setType('cinema');
        $users = UserEloquent::query()
            ->where('temp_balance_date', '<', now()->subMinutes(3))
            ->limit(10000)
            ->orderBy('temp_balance_date')
            ->get();
        foreach ($users as $user) {
            $user->megaweb->calcTempBalance();
            $user->megaweb->calcCustomerTemp1TxtPrice();
        }

        return 'Обработано '.$users->count().' пользователей';
    }

    public static function calcServiceLoad()
    {
        Engine::setType('cinema');
        $cinema = new Cinema;
        $movies = new Movies;
        $movies->setUseDomainMovies(true);
        $movies->setBookedForLogin('ozerin');
        $movies->setWithDomainDescription(false);
        // отображаем задания, тех пользователей у которых достаточный баланс
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        $rows = $movies->getRows();
        $load = ceil($rows / $cinema->getTasks100Percent() * 100);
        // $load = ($rows > 100)?100:$rows;
        $cinema->setServiceLoad($load);
        // F::dump($cinema);
        $cinema->save();

        return $load;
    }

    public static function setRewritePricePer1k()
    {
        $cinema = new Cinema;
        if ($cinema->isRewritePriceIncreased()) {
            $cinema->setRewritePricePer1k($cinema->getHighRewriterPricePer1k());
        } else {
            $cinema->setRewritePricePer1k($cinema->getRegularRewriterPricePer1k());
        }
        F::query('
			update '.F::typetab('domain_movies').' set
			description_price_per_1k = '.$cinema->getRewritePricePer1k().'
			where
			customer is not null and
			description_length = 0 and
			description_writer is null and
			(
			description_price_per_1k <= '.$cinema->getHighRewriterPricePer1k().' or
			description_price_per_1k is null
			)
			;');
        $cinema->save();
    }

    // освободить фильмы от брони, чей рерайтер не появляется больше 6 часов
    public static function releaseMoviesFromLateRewriters()
    {
        Engine::setType('cinema');
        F::query('update '.F::typetab('domain_movies').' set
			booked_for_login = \'ozerin\',
			book_date = null,
			writing = 0
			where
			domain_movies.description_length = 0 and
			domain_movies.booked_for_login is not null
			and domain_movies.booked_for_login not in (\'ozerin\',\'seo-fast\',\'seo-vip\')
			and timestampdiff(minute,book_date,current_timestamp) >= '.(new Cinema)->getBookMinutes().'
			;');
        $return = 'Освобождено от брони заданий: '.Engine::getSqlConnection()->affected_rows;
        logger()->info($return);

        return $return;
    }

    public static function calcDescriptionWaitTime()
    {
        Engine::setType('cinema');
        $arr = F::query_assoc('
			select round(avg((unix_timestamp(description_date) - unix_timestamp(customer_confirm_date))/60/60),1) as hours
			from '.F::typetab('domain_movies').'
			where description_date between date_sub(current_timestamp,interval 24 hour) and current_timestamp
			;');
        $cinema = new Cinema;
        $cinema->setDescriptionWaitTime($arr['hours']);
        $cinema->save();

        return 'Время ожидания: '.$arr['hours'].' часов';
    }

    public static function updateTranslations()
    {
        Engine::setType('cinema');
        F::query('insert into '.F::typetab('translations').' (videodb_name,q)
			(select translation,count(*) as q from '.F::typetab('videodb').' group by translation)
			on duplicate key update translations.q = values(q)');
        $return = 'Таблица translations обновлена';
        logger()->info($return);

        return $return;
    }

    public static function checkMovieComments()
    {
        Engine::setType('cinema');
        $twoDaysAgo = Carbon::now()->subDays(1)->toDateString();
        $comments = DomainMovieComment::where('date', '>', $twoDaysAgo)->count();
        if ($comments > Config::first()->movie_comments_max) {
            return logger()->alert('За последние 2 дня '.$comments.' комментариев, чекнуть спам.');
        }

        return 'За последние 2 дня '.$comments.' комментариев';
    }

    public static function clearVideodb()
    {
        // разрешенный процент удаляемых строк от общего количества строк
        $allowed_percent = 1;
        // сколько дней записи должны отсутствовать при обновлении, чтобы они считались удаленными
        $diff_between_min_max_days = 3;
        $videodb_rows = VideodbEloquent::count();
        $videodb_min_max_days = VideodbEloquent::selectRaw('date(min(is_exists_on_date)) as min_day,date(max(is_exists_on_date)) as max_day, datediff(max(is_exists_on_date),min(is_exists_on_date)) as diff')->first();
        $log = 'Без действий';
        if ($videodb_min_max_days->diff < $diff_between_min_max_days) {
            $log = 'Videodb: минимальная дата ('.$videodb_min_max_days->min_day.') меньше максимальной ('.$videodb_min_max_days->max_day.') менее чем на '.$diff_between_min_max_days.' дней';
            logger()->info($log);

            return $log;
        }
        $videodb_min_day_rows = VideodbEloquent::whereRaw('date(is_exists_on_date) = ?', [$videodb_min_max_days->min_day])->count();
        // если к удалению больше allowed_percent, то не трогаем
        $percent = round($videodb_min_day_rows / $videodb_rows * 100, 2);
        if ($percent > $allowed_percent) {
            $log = 'Videodb: Не удаляем '.$videodb_min_day_rows.' строк, т.к. это больше '.$allowed_percent.'%';
            logger()->alert($log);

            return $log;
        } else {
            VideodbEloquent::whereRaw('date(is_exists_on_date) = ?', [$videodb_min_max_days->min_day])->delete();
            $log = 'Videodb: Удалили '.$videodb_min_day_rows.' строк, '.$percent.'%';
            logger()->alert($log);

            return $log;
        }

        return $log;
    }

    public static function updateMovies()
    {
        (new \cinema\app\services\cinema_update\UpdateStagesService)->run();
    }
}
