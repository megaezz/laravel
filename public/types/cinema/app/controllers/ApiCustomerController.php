<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\DomainMovie;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\User;
use cinema\app\models\Movie;
use cinema\app\models\Movies;

class ApiCustomerController
{
    private $user = null;

    private $format = 'json';

    public function __construct()
    {

        if (Config::cached()->kinocontent_maintenance) {
            $this->error(Config::cached()->kinocontent_maintenance_message, 503);
        }

        $apiKey = request()->header('X-API-KEY', null);

        if (request()->input('apiKey')) {
            $apiKey = request()->input('apiKey');
            $this->format = 'html';
        }

        if (! $apiKey) {
            $this->error('Не передан X-API-KEY', 401);
        }

        $this->user = User::where('api_key', $apiKey)->first()?->megaweb;

        if (! $this->user) {
            $this->error('Такой API ключ не существует');
        }
    }

    public function route($method)
    {
        if (empty($method)) {
            $this->error('Method not specified');
        }
        // если запрос OPTIONS, т.е. браузер спрашивает сервер, разрешено ли использовать такие-то заголовки, то нужно ответить статусом 200, иначе в Swagger'е вылетает ошибка "Preflight response..."
        if (request()->header('Origin') and request()->method() === 'OPTIONS') {
            return $this->makeResponse('Allowed');
        }
        $prefixes = ['GET' => 'get', 'POST' => 'add', 'PUT' => 'edit', 'DELETE' => 'delete'];
        $prefix = $prefixes[request()->method()] ?? null;
        $method_name = $prefix.ucfirst($method);
        if (! method_exists($this, $method_name)) {
            $this->error('Method doesn\'t exist');
        }

        return $this->makeResponse(call_user_func([$this, $method_name]));
    }

    public function makeResponse($data, $code = null)
    {

        if ($this->format == 'json') {
            $response = response()->json($data, ($code ?? 200));
        } else {
            $response = response($data, ($code ?? 200));
        }

        // разрешаем запускаться по CORS в swaggerhub и других приложениях
        if (request()->header('Origin')) {
            $response
                ->header('Access-Control-Allow-Origin', request()->header('Origin'))
                ->header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
                ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, X-API-KEY')
                ->header('Access-Control-Allow-Credentials', 'true');
        }

        return $response;
    }

    public function error($text = null, $code = null)
    {
        abort($this->makeResponse(['message' => ($text ?? 'Error text not specified')], ($code ?? 400)));
    }

    private function getTaskObject($id = null)
    {
        if (! $id) {
            $this->error('Не передан id для getTaskObject');
        }
        $movie = new DomainMovie($id);
        // F::dump($movie);
        $result = new \stdClass;
        $result->id = $movie->getDomainMovieId();
        $result->kinopoisk_id = empty($movie->getKinopoiskId()) ? null : (int) $movie->getKinopoiskId();
        if ($movie->getDomainMovieShowToCustomer()) {
            $result->text = $movie->getDomainMovieDescription();
            $result->text_length = $movie->getDomainMovieDescriptionLength();
            $result->unique = $movie->getDomainMovieTextRuUnique();
            $result->rework = $movie->getDomainMovieRework();
            if ($movie->getDomainMovieRework()) {
                $result->rework_comment = $movie->getDomainMovieReworkComment();
            } else {
                $result->rework_comment = null;
            }
        } else {
            $result->text = null;
            $result->text_length = null;
            $result->unique = null;
            $result->rework = false;
            $result->rework_comment = null;
        }
        $result->symbols_from = is_null($movie->getDomainMovieSymbolsFrom()) ? null : (int) $movie->getDomainMovieSymbolsFrom();
        $result->symbols_to = is_null($movie->getDomainMovieSymbolsTo()) ? null : (int) $movie->getDomainMovieSymbolsTo();
        $result->comment = $movie->getDomainMovieCustomerComment();
        $result->private_comment = $movie->getDomainMoviePrivateComment();
        $result->confirmed = $movie->getDomainMovieIsConfirmed();
        $result->express = $movie->getDomainMovieIsExpress();
        $result->archived = $movie->getDomainMovieCustomerArchived();
        $result->moderated = $movie->getDomainMovieShowToCustomer();
        $result->rework_allowed = $movie->getDomainMovieReworkAllowed();
        $result->booked = $movie->getDomainMovieIsBooked();

        return $result;
    }

    private function getUserObject()
    {
        $result = new \stdClass;
        $result->login = $this->user->getLogin();
        $result->balance = $this->user->getRewriteBalance();
        $result->is_enough_balance = $this->user->getCustomerIsEnoughBalance();
        $result->symbols_from = $this->user->getSymbolsFrom();
        $result->symbols_to = $this->user->getSymbolsTo();
        $result->comment = $this->user->getCustomerComment();
        $result->private_comment = $this->user->getCustomerPrivateComment();
        $result->express = $this->user->getcustomerAutoExpress();

        // F::dump($result);
        return $result;
    }

    public function getTasks()
    {
        $done = isset($_GET['done']) ? (filter_var($_GET['done'], FILTER_VALIDATE_BOOLEAN) ? true : false) : null;
        // $rework = isset($_GET['rework'])?(filter_var($_GET['rework'], FILTER_VALIDATE_BOOLEAN)?true:false):null;
        $archived = isset($_GET['archived']) ? (filter_var($_GET['archived'], FILTER_VALIDATE_BOOLEAN) ? true : false) : null;
        // $booked = isset($_GET['booked'])?($_GET['booked']?true:false):null;
        $confirmed = isset($_GET['confirmed']) ? (filter_var($_GET['confirmed'], FILTER_VALIDATE_BOOLEAN) ? true : false) : null;
        $include_data = isset($_GET['include_data']) ? ((filter_var($_GET['include_data'], FILTER_VALIDATE_BOOLEAN)) ? true : false) : true;
        // F::dump($_GET);
        // F::dump($include_data);
        $limit = isset($_GET['limit']) ? (int) $_GET['limit'] : 10;
        if ($limit > 100) {
            $limit = 100;
        }
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setWithDomainDescription($done);
        // $movies->setDomainMovieRework($rework);
        $movies->setWithBookedForLogin($confirmed);
        $movies->setDomainMovieCustomerArchived($archived);
        $movies->setOrder('domain_movies.add_date');
        $movies->setLimit($limit);
        $movies->setPage($page);
        $arr = $movies->get();
        $arr_items = [];
        foreach ($arr as $v) {
            if ($include_data) {
                $task = $this->getTaskObject($v['domain_movies_id']);
            } else {
                $task = new \stdClass;
                $task->id = $v['domain_movies_id'];
            }
            $arr_items[] = $task;
        }
        $result = new \stdClass;
        $result->rows = $movies->getRows();
        $result->limit = $movies->getLimit();
        $result->page = $movies->getPage();
        $result->items = $arr_items;

        return $result;
    }

    public function getTask()
    {
        $id = empty($_GET['id']) ? $this->error('Не передан ID задания') : $_GET['id'];
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setDomainMovieId($id);
        $arr = $movies->get();
        if (! $arr) {
            $this->error('Задание не найдено');
        }

        return $this->getTaskObject($arr[0]['domain_movies_id']);
    }

    public function addTask()
    {
        $kinopoisk_id = empty($_GET['kinopoisk_id']) ? null : $_GET['kinopoisk_id'];
        // $symbols_from = empty($_GET['symbols_from'])?null:$_GET['symbols_from'];
        // $symbols_to = empty($_GET['symbols_to'])?null:$_GET['symbols_to'];
        // $customer_comment = empty($_GET['comment'])?null:$_GET['comment'];
        // $private_comment = empty($_GET['private_comment']):$_GET['private_comment'];
        $movieId = Movie::getIdByKinopoiskId($kinopoisk_id);
        $addedEmptyTask = false;
        // если видео не найдено в базе, ищем not_in_base элемент
        if (! $movieId) {
            $notInBaseId = Movies::getNotInBaseId();
            if ($notInBaseId) {
                $movieId = $notInBaseId;
                $addedEmptyTask = true;
            }
        }
        // если movieId все еще нет то ошибка
        if (! $movieId) {
            $this->error('Невозможно добавить задание');
        }
        $domainMovieId = DomainMovie::add($movieId);
        $domainMovie = new DomainMovie($domainMovieId);
        $domainMovie->setCustomerSettings($this->user->getLogin());
        if ($addedEmptyTask) {
            $domainMovie->setDomainMovieCustomerComment('Ссылка на необходимый материал: https://www.kinopoisk.ru/film/'.$kinopoisk_id.'/'.PHP_EOL.$domainMovie->getDomainMovieCustomerComment());
        }
        $domainMovie->save($this->user->getLogin());

        // $result = new \stdClass;
        // $result->created_task_id = $domainMovie->getDomainMovieId();
        return $this->getTaskObject($domainMovie->getDomainMovieId());
    }

    public function deleteTask()
    {
        $id = empty($_GET['id']) ? $this->error('Не передан ID задания') : $_GET['id'];
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setDomainMovieId($id);
        $arr = $movies->get();
        if (! $arr) {
            $this->error('Задание не найдено');
        }
        $movie = new DomainMovie($arr[0]['domain_movies_id']);
        if ($movie->getDomainMovieDescription()) {
            $this->error('Нельзя удалить, так как задание уже выполнено');
        }
        if ($movie->getDomainMovieIsBooked()) {
            $this->error('Нельзя удалить, задание уже забронировано за рерайтером');
        }
        $movie->delete();

        return true;
    }

    public function editTask()
    {
        // $this->error('Метод еще не реализован');
        $id = empty($_GET['id']) ? $this->error('Не передан ID задания') : $_GET['id'];
        $movies = new Movies;
        $movies->setDomainMovieCustomer($this->user->getLogin());
        $movies->setDomainMovieId($id);
        $arr = $movies->get();
        if (! $arr) {
            $this->error('Задание не найдено');
        }
        $movie = new DomainMovie($arr[0]['domain_movies_id']);
        // можем менять все что угодно, пока задание не подтверждено и не забронировано
        // когда забронировано можем менять private_comment, archived

        // когда задание выполнено
        if ($movie->getDomainMovieDescription()) {
            $allowed = ['archived', 'private_comment', 'rework', 'rework_comment'];
            $stage = 'задание выполнено';
            if ($movie->getDomainMovieRework()) {
                $allowed = ['private_comment'];
                $stage = 'задание на доработке';
            }
            if ($movie->getDomainMovieCustomerArchived()) {
                $allowed = ['archived', 'private_comment'];
                $stage = 'задание в архиве';
            }
            if (! $movie->getDomainMovieShowToCustomer()) {
                $allowed = ['private_comment'];
                $stage = 'задание на модерации';
            }
        } else {
            // если задание не выполнено
            if ($movie->getDomainMovieIsConfirmed()) {
                // задание отправлено в работу
                if ($movie->getDomainMovieIsBooked()) {
                    // задание забронировано
                    $allowed = ['private_comment'];
                    $stage = 'задание забронировано';
                } else {
                    // задание не забронировано
                    $allowed = ['symbols_from', 'symbols_to', 'comment', 'private_comment', 'confirmed', 'express'];
                    $stage = 'задание не забронировано';
                }
            } else {
                // если не отправлено в работу
                $allowed = ['symbols_from', 'symbols_to', 'comment', 'private_comment', 'confirmed', 'express'];
                $stage = 'задание не отправлено в работу';
            }
        }
        if (isset($_GET['symbols_from'])) {
            if (in_array('symbols_from', $allowed)) {
                $movie->setDomainMovieSymbolsFrom($_GET['symbols_from']);
            } else {
                $this->error('Нельзя изменить количество символов, так как '.$stage);
            }
        }
        if (isset($_GET['symbols_to'])) {
            if (in_array('symbols_to', $allowed)) {
                $movie->setDomainMovieSymbolsTo($_GET['symbols_to']);
            } else {
                $this->error('Нельзя изменить количество символов, так как '.$stage);
            }
        }
        if (isset($_GET['comment'])) {
            if (in_array('comment', $allowed)) {
                $movie->setDomainMovieCustomerComment($_GET['comment']);
            } else {
                $this->error('Нельзя изменить комментарий, так как '.$stage);
            }
        }
        if (isset($_GET['private_comment'])) {
            if (in_array('private_comment', $allowed)) {
                $movie->setDomainMoviePrivateComment($_GET['private_comment']);
            } else {
                $this->error('Нельзя изменить приватный комментарий, так как '.$stage);
            }
        }
        if (isset($_GET['confirmed'])) {
            if (in_array('confirmed', $allowed)) {
                $confirmed = filter_var($_GET['confirmed'], FILTER_VALIDATE_BOOLEAN);
                if ($confirmed) {
                    $movie->setDomainMovieBookedForLogin('ozerin');
                    $movie->setDomainMovieCustomerConfirmDate((new \DateTime)->format('Y-m-d H:i:s'));
                } else {
                    $movie->setDomainMovieBookedForLogin(null);
                    $movie->setDomainMovieCustomerConfirmDate(null);
                }
            } else {
                $this->error('Нельзя изменить статус подтверждения задания, так как '.$stage);
            }
        }
        if (isset($_GET['express'])) {
            if (in_array('express', $allowed)) {
                $express = filter_var($_GET['express'], FILTER_VALIDATE_BOOLEAN);
                $cinema = new Cinema;
                if ($express) {
                    $movie->setDomainMovieDescriptionPricePer1k($cinema->getRewriteExpressPricePer1k());
                    $movie->setDomainMovieCustomerPricePer1k($this->user->getCustomerExpressPricePer1k());
                } else {
                    $movie->setDomainMovieDescriptionPricePer1k($cinema->getRewritePricePer1k());
                    $movie->setDomainMovieCustomerPricePer1k($this->user->getCustomerPricePer1k());
                }
            } else {
                $this->error('Нельзя изменить экспресс статус, так как '.$stage);
            }
        }
        if (isset($_GET['archived'])) {
            if (in_array('archived', $allowed)) {
                $archived = filter_var($_GET['archived'], FILTER_VALIDATE_BOOLEAN);
                $movie->setDomainMovieCustomerArchived($archived);
            } else {
                $this->error('Нельзя изменить архивный статус, так как '.$stage);
            }
        }
        if (isset($_GET['rework'])) {
            if (in_array('rework', $allowed)) {
                $rework = filter_var($_GET['rework'], FILTER_VALIDATE_BOOLEAN);
                if (date_diff(new \DateTime, new \DateTime($movie->getDomainMovieDescriptionDate()))->days > 14) {
                    $this->error('Нельзя отправить на доработку, прошло более 2-х недель');
                } else {
                    $movie->setDomainMovieRework($rework);
                }
            } else {
                $this->error('Нельзя изменить статус доработки, так как '.$stage);
            }
        }
        if (isset($_GET['rework_comment'])) {
            if (in_array('rework_comment', $allowed)) {
                $movie->setDomainMovieReworkComment($_GET['rework_comment']);
            } else {
                $this->error('Нельзя изменить комментарий доработки, так как '.$stage);
            }
        }
        $movie->save($this->user->getLogin());

        return $this->getTaskObject($movie->getDomainMovieId());
    }

    public function getUser()
    {
        return $this->getUserObject();
    }

    public function editUser()
    {
        if (isset($_GET['symbols_from'])) {
            $this->user->setSymbolsFrom($_GET['symbols_from']);
        }
        if (isset($_GET['symbols_to'])) {
            $this->user->setSymbolsTo($_GET['symbols_to']);
        }
        if (isset($_GET['comment'])) {
            $this->user->setCustomerComment($_GET['comment']);
        }
        if (isset($_GET['private_comment'])) {
            $this->user->setCustomerPrivateComment($_GET['private_comment']);
        }
        if (isset($_GET['express'])) {
            $this->user->setCustomerAutoExpress(filter_var($_GET['express'], FILTER_VALIDATE_BOOLEAN));
        }
        $this->user->save();

        return $this->getUserObject();
    }
}
