<?php

namespace cinema\app\controllers;

use engine\app\models\F;
use YandexMoney\API;

class YandexMoneyController
{
    private const APP_ID = 'A128CCE7ABF490F16206A38937C2FB109649D03E33C9AA96D11FB33B78B4BA69';

    // private const REDIRECT_URI = 'https://kinocontent.club/?r=YandexMoney/checkPayment';
    private const REDIRECT_URI = 'https://kinocontent.club/wmDepositSuccess';

    public function test()
    {
        // $auth_url = API::buildObtainTokenUrl($client_id, $redirect_uri, $scope);
        $auth_url = API::buildObtainTokenUrl(self::APP_ID, self::REDIRECT_URI, ['payment']);
        // F::redirect($auth_usrl);
        f::dump($auth_url);
    }

    public function checkPayment()
    {
        f::dump('check payment');
    }
}
