<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\Domain;
use cinema\app\models\eloquent\MovieEpisode;
use cinema\app\models\Movie;
use cinema\app\models\Movies;
use cinema\app\models\Videodb;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class VideodbController
{
    public function getPlaylist()
    {
        $videodbId = empty($_GET['id']) ? null : F::checkstr($_GET['id']);
        $videodb = new Videodb($videodbId);

        // dd($videodb->getPlaylist());
        return $videodb->getPlaylist();
    }

    public function getPlayer()
    {
        // просто закоментировать, если нужен вывод плеера через рекаптчу
        // return $this->getPlayerStep2();
        //
        $domain = new Domain(Engine::getDomain());
        $recaptcha_response = empty($_GET['token']) ? null : $_GET['token'];
        if ($recaptcha_response) {
            if (! Engine::checkRecaptcha($recaptcha_response, $domain->getRecaptchaServerKey())) {
                F::error('Wrong recaptcha', null, false);
            }

            return $this->getPlayerStep2();
        }
        $t = new Template('players/videodb/recaptcha');
        $t->v('request_uri', request()->getRequestUri());
        $t->v('recaptchaClientKey', $domain->getRecaptchaClientKey());

        return $t->get();
    }

    public function getPlayerStep2()
    {
        // \Debugbar::disable();
        // header('X_FRAME_OPTIONS','ALLOWALL');
        // Engine::setDisplayErrors(true);
        $ban = (Engine::getClientBan() and Engine::getClientBan()->hide_player) ? true : false;
        if ($ban) {
            F::error('Offline', 'Player banned for client', false);
        }
        $movieId = empty($_GET['id']) ? null : F::checkstr($_GET['id']);
        // $episodeId = empty($_GET['episode_id']) ? null : F::checkstr($_GET['episode_id']);
        $kinopoiskId = null;
        // $kinopoiskId = empty($_GET['kp_id'])?null:F::checkstr($_GET['kp_id']);
        // dd(Videodb::getIframeSign());
        $key = $_GET['k'] ?? null;
        if ($key != Videodb::getIframeSign($movieId) and $key != 'allow') {
            F::alertLite('Offline');
        }

        $blockStr = empty($_GET['b']) ? null : F::checkstr($_GET['b']);
        $playerjsVersion = empty($_GET['v']) ? ((new Cinema)->getPlayerjsVersion()) : F::checkstr($_GET['v']);
        if ($movieId) {
            $movie = new Movie($movieId);
        } else {
            if ($kinopoiskId) {
                $movies = new Movies;
                $movies->setKinopoiskId($kinopoiskId);
                $arr = $movies->get();
                if ($arr) {
                    $movie = new Movie($arr[0]['id']);
                }
            }
        }
        // F::dump($movie);
        if (! isset($movie)) {
            F::error('Материал не найден');
        }
        // если для фильма указана строка блокировки, то без этой строки не показываем плеер
        if ($movie->getBlockStr()) {
            if ($movie->getBlockStr() !== $blockStr) {
                F::error('Материал не найден');
            }
        }
        $videodbId = empty($_GET['videodbId']) ? null : F::checkstr($_GET['videodbId']);
        /* пробуем найти именно указанный эпизод */
        /* что-то тут не получилось, чтобы найти именно указанный эпизод, отложил на потом */
        // if ($episodeId) {
        // 	$episode = MovieEpisode::find($episodeId);
        // 	if ($episode and $episode->videodb) {
        // 		$videodbId = $episode->videodb->id;
        // 	}
        // }

        $translation = empty($_GET['translation']) ? null : F::checkstr($_GET['translation']);
        $sql['movieId'] = $movie->getId() ? ('movie_id = \''.$movie->getId().'\'') : 1;
        $sql['videodbId'] = $videodbId ? ('id = \''.$videodbId.'\'') : 1;
        // если указан точный ID то игнорируем озвучку
        if ($videodbId) {
            $sql['translation'] = 1;
        } else {
            $sql['translation'] = $translation ? ('translation = \''.F::checkstr($_GET['translation']).'\'') : 1;
        }
        $arr = F::query_assoc('
			select id
			from '.F::typetab('videodb').'
			where
			'.$sql['movieId'].'
			and '.$sql['translation'].'
			and '.$sql['videodbId'].'
			order by cast(season as unsigned) desc, cast(episode as unsigned) desc, max_quality desc
			limit 1
			;');
        if (! $arr) {
            $t = new Template('players/videodb/page');
            // $t = new Template('players/videodb/page_dreamcash');
            $data = new \stdClass;
            $data->movieTitle = $movie->getTitleRu();
            $data->movieYear = $movie->getYear();
            $data->movieType = '- '.$movie->getTypeRu(true);
            //
            $seasonObj = new \stdClass;
            $seasonObj->title = 'Сезон 1';
            $episodeObj = new \stdClass;
            $episodeObj->title = 'Серия 1';
            // текущий сезон и эпизод, чтобы получать в плеере и формировать название
            $episodeObj->id = new \stdClass;
            $episodeObj->id->season = 1;
            $episodeObj->id->episode = 1;
            $episodeObj->file = '{Оригинал}/types/cinema/template/players/videodb/video_unavailable.mp4;';
            // $episodeObj->file = Videodb::pjsBase64Encrypt('{Оригинал}/types/cinema/template/players/videodb/video_unavailable.mp4;');
            $episodesArr = [$episodeObj];
            $seasonObj->folder = $episodesArr;
            $seasonsArr = [$seasonObj];
            //
            $data->playlist = $seasonsArr;
            $t->v('base64JsonData', base64_encode(json_encode($data)));
            $redirect = true;
            $strip = empty($_GET['strip']) ? false : true;
            $t->v('jsonRedirect', json_encode($redirect));
            $t->v('movieId', $movieId);
            $t->v('jsonStrip', json_encode($strip));
            $t->v('webWapScript', ($redirect and $strip) ? ((new Template('players/videodb/webwap_script'))->get()) : '');
            $t->v('movieTitle', $movie->getTitleRu());
            $t->v('movieYear', $movie->getYear());
            $t->v('playerjsVersion', $playerjsVersion);
            F::setPageVars($t);

            return $t->get();
        }
        $videodb = new Videodb($arr['id']);
        $videodb->setPlayerjsVersion($playerjsVersion);

        return $videodb->getPlayer();
    }

    public function getVast()
    {
        Engine::setDebug(false);
        $country = empty($_GET['country']) ? (empty(Engine::getClientCountry()) ? 'RU' : Engine::getClientCountry()) : $_GET['country'];
        $id = empty($_GET['id']) ? null : $_GET['id'];
        $arr = F::query_assoc('
			select code
			from '.F::typetab('vast').'
			where
			'.($id ? 1 : 'active').' and
			'.($id ? 'id = \''.F::escape_string($id).'\'' : 1).'
			and (country = \''.F::escape_string($country).'\' or country is null)
			order by rand()
			;');
        // header('Content-Type: text/xml;charset=UTF-8');
        header('Content-Type: text/xml');

        return $arr['code'] ?? '<?xml version="1.0" encoding="utf-8"?>
		<VAST xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance" version="3.0" xsi:noNamespaceSchemaLocation="vast.xsd">
		<Error/>
		</VAST>';
    }

    public function vpaid()
    {
        // $type = $_GET['type']??null;
        // $id = $_GET['id']??F::error('ID required');
        return 'ok';
    }

    public function getHdvb() {}
}
