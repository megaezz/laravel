<?php

namespace cinema\app\controllers\templates\_24seriala;

use cinema\app\models\DomainMovie;
use cinema\app\models\Movies;
use cinema\app\models\Videodb;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class MainController extends \cinema\app\controllers\MainController
{
    public function index($args = [])
    {
        // Engine::setDebug(false);
        $t = $this->template;
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setDomain($this->domain->getDomain());
        $movies->setOrder('cinema.title_ru');
        $movies->setListTemplate('items/movie_index');
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster(true);
        $movies->setIncludeSerials(true);
        $movies->setIncludeMovies(false);
        $movies->setIncludeTrailers(false);
        $movies->setActive(true);
        $movies->setCalcSeasonsEpisodesData(false);
        // $movies->setLimit(F::cache($movies,'getRows'));
        $movies->setLimit(2000);
        $t->v('cinemaList', F::cache($movies, 'getList'));
        $t->v('topCinemaList', $this->topCinemaList());
        $t->v('updatesList', F::cache($this, 'getUpdatesList'));
        F::setPageVars($t);

        return $t->get();
    }

    public function getUpdatesList()
    {
        $date = new \DateTime;
        $date->modify('-7 days');
        $list = '';
        for ($i = 1; $i <= 5; $i++) {
            $date->modify('-1 day');
            $arr = Videodb::getUpdatesOnDay($date->format('Y-m-d'));
            $listUpd = '';
            foreach ($arr as $v) {
                $movies = new Movies;
                $movies->setDomain($this->domain->getDomain());
                $movies->setMovieId($v['movie_id']);
                $movies->setDeleted(false);
                $movies->setDomainMoviePageDeleted(false);
                $movies->setCalcSeasonsEpisodesData(false);
                $movies->setLimit(1);
                $arrDM = $movies->get();
                if (! $arrDM) {
                    continue;
                }
                $movie = new DomainMovie($arrDM[0]['domain_movies_id']);
                $tUpd = new Template('items/list_updates');
                $tUpd->v('title', $movie->getTitleRu());
                $tUpd->v('watchLink', $this->domain->getWatchLink($movie));
                $tUpd->v('season', $v['season']);
                $tUpd->v('episode', $v['episode']);
                $listUpd .= $tUpd->get();
            }
            $tDay = new Template('items/day_updates');
            $tDay->v('list', $listUpd);
            $tDay->v('date', $date->format('Y-m-d'));
            $list .= $tDay->get();
        }

        return $list;
    }
}
