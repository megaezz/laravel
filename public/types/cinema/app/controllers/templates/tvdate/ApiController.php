<?php

namespace cinema\app\controllers\templates\tvdate;

use App\Facades\F;
use App\Http\Resources\DomainMovieResource;
use cinema\app\models\eloquent\Domain;
use cinema\app\models\eloquent\DomainMovie;
use engine\app\models\Engine;

class ApiController
{
    private $domain = null;

    public function construct()
    {
        $this->domain = Domain::findOrFail(Engine::getDomain());
    }

    public function routing($args = [])
    {
        $this->construct();
        if (! method_exists($this, $args['method'])) {
            F::megaweb()::error('Method is not exist');
        }

        return $this->{$args['method']}();
    }

    public function getMovies()
    {
        $this->construct();
        $movies = DomainMovie::whereDomain($this->domain->domain)
            ->whereRelation('movie', 'type', 'serial')
            ->with('movie')
            ->with('movie.myshows')
            ->get();

        return DomainMovieResource::collection($movies);
    }
}
