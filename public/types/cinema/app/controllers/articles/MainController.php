<?php

namespace cinema\app\controllers\articles;

use cinema\app\models\Article;
use cinema\app\models\Articles;
use cinema\app\models\DomainMovie;
use engine\app\models\Engine;
use engine\app\models\F;

class MainController extends \cinema\app\controllers\MainController
{
    public function articlesIndex()
    {
        // Engine::setDisplayErrors(true);
        $t = $this->template;
        $this->setMenuVars($t);
        $t->v('showSearchBlock', 'false');
        F::setPageVars($t);
        $this->setPageVars($t);
        $a = new Articles;
        $a->setDomain($this->domain->getDomain());
        $a->setLimit(10);
        $a->setPage(1);
        $a->setActive(true);
        $a->setOrder('articles.apply_date desc');
        $a->setListTemplate('articles/list-article');
        // dump($a->getList());
        // $t->v('listArticles',F::cache($a,'getList');
        $t->v('listArticles', $a->getList());

        return $t->get();
    }

    public function articlesView($args = [])
    {
        if (Engine::route()) {
            $id = request()->route()->parameter('id');
        } else {
            $id = empty($args['id']) ? F::error('Article ID required') : F::checkstr($args['id']);
        }
        $t = $this->template;
        $this->setMenuVars($t);
        $t->v('showSearchBlock', 'false');
        F::setPageVars($t);
        $this->setPageVars($t);
        $a = new Article($id);
        if ($a->getDomain() != $this->domain->getDomain()) {
            F::error('Article not found');
        }
        $t->v('articleText', $a->getText());
        $t->v('articleTextPreview', $a->getTextPreview());
        $t->v('articleTitle', $a->getTitle());
        $addDate = new \DateTime($a->getApplyDate());
        $formatter = new \IntlDateFormatter(
            'ru_RU',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE,
            'Europe/Moscow'
        );
        $t->v('articleApplyDate', $formatter->format($addDate));
        if ($a->getDomainMovieId()) {
            // F::dump($a->getDomainMovieId());
            $movie = new DomainMovie($a->getDomainMovieId());
            $t->v('articleMovieWatchLink', '
				<p>Вы можете посмотреть <a href="'.$this->domain->getWatchLink($movie).'"><strong>'.$movie->getTypeRu().' '.$movie->getTitleRu().'</strong></a> на нашем сайте.</p>
				');
        } else {
            $t->v('articleMovieWatchLink', '');
        }

        return $t->get();
    }

    // для обычной текстовой страницы
    public function text()
    {
        return $this->template->get();
    }
}
