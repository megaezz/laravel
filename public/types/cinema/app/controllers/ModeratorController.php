<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\DomainMovie;
use cinema\app\models\Movies;
use cinema\app\models\User;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use engine\app\models\TextRu;

class ModeratorController extends WorkerController
{
    public function __construct()
    {
        parent::__construct();
        $this->auth = new CinemaAuthorizationController(['admin', 'moderator']);
    }

    public function rewrited($args = [])
    {
        $page = empty($_GET['p']) ? 1 : F::checkstr($_GET['p']);
        $movieId = empty($_GET['movieId']) ? null : F::checkstr($_GET['movieId']);
        $domainMovieId = empty($_GET['domainMovieId']) ? null : F::checkstr($_GET['domainMovieId']);
        $descriptionWriter = empty($_GET['descriptionWriter']) ? null : F::checkstr($_GET['descriptionWriter']);
        $customer = empty($_GET['customer']) ? null : F::checkstr($_GET['customer']);
        $showToCustomer = isset($_GET['showToCustomer']) ? (($_GET['showToCustomer'] == '') ? null : ($_GET['showToCustomer'] ? true : false)) : null;
        $withDomainDescription = isset($_GET['withDomainDescription']) ? (($_GET['withDomainDescription'] == '') ? null : $_GET['withDomainDescription']) : null;
        $active = isset($_GET['active']) ? (($_GET['active'] == '') ? null : $_GET['active']) : null;
        $rework = isset($_GET['rework']) ? (($_GET['rework'] == '') ? null : $_GET['rework']) : null;
        $bookedForLogin = isset($_GET['bookedForLogin']) ? (($_GET['bookedForLogin'] == '') ? null : $_GET['bookedForLogin']) : null;
        $customerIsEnoughBalance = isset($_GET['customerIsEnoughBalance']) ? (($_GET['customerIsEnoughBalance'] == '') ? null : $_GET['customerIsEnoughBalance']) : null;
        $btnActive = empty($_GET['btnActive']) ? null : $_GET['btnActive'];
        $writing = isset($_GET['writing']) ? (($_GET['writing'] == '') ? null : $_GET['writing']) : null;
        $order = isset($_GET['order']) ? (($_GET['order'] == '') ? null : $_GET['order']) : null;
        $withDomainMovieCustomerComment = isset($_GET['withDomainMovieCustomerComment']) ? (($_GET['withDomainMovieCustomerComment'] == '') ? null : $_GET['withDomainMovieCustomerComment']) : null;
        $orders = [
            'addDateDesc' => 'domain_movies.add_date desc',
            'descriptionDateDesc' => 'domain_movies.description_date desc',
            'descriptionDate' => 'domain_movies.description_date',
            'customerFirstAndAddDate' => 'domain_movies.description_price_per_1k desc,domain_movies.to_top desc, isnull(domain_movies.customer),
			domain_movies.to_bottom,
			rand(),
			domain_movies.add_date',
        ];
        $orderStr = $order ? (isset($orders[$order]) ? $orders[$order] : F::error('Order name doesn\'t exist')) : null;
        // F::dump($withDomainDescription);
        $t = new Template('admin/moderator/rewrited');
        $t->v('btnPremoderationActive', ($btnActive === 'premoderation') ? 'active' : '');
        $t->v('btnReworkActive', ($btnActive === 'rework') ? 'active' : '');
        $t->v('btnBaseQueueActive', ($btnActive === 'baseQueue') ? 'active' : '');
        $t->v('btnWritingActive', ($btnActive === 'writing') ? 'active' : '');
        $t->v('btnCustomerCommentActive', ($btnActive === 'customerComment') ? 'active' : '');
        // чтобы узнать кол-во премодераций
        $movies = new Movies;
        $movies->setDomainMovieShowToCustomer(false);
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieRework(false);
        $t->v('hidden_from_customer_rows', $movies->getRows());
        // $t->v('hidden_from_customer_rows',F::cache($movies,'getRows',null,30));
        // чтобы узнать кол-во на доработку
        $movies = new Movies;
        $movies->setDomainMovieRework(true);
        $t->v('rework_rows', $movies->getRows());
        // $t->v('rework_rows',F::cache($movies,'getRows',null,30));
        // чтобы узнать кол-во фильмов в очереди на рерайт
        $movies = new Movies;
        $movies->setBookedForLogin('ozerin');
        $movies->setWithDomainDescription(false);
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        $t->v('base_rows', $movies->getRows());
        // $t->v('base_rows',F::cache($movies,'getRows',null,30));
        // чтобы узнать кол-во фильмов которые пишут
        $movies = new Movies;
        $movies->setDomainMovieWriting(true);
        $t->v('writing_rows', $movies->getRows());
        // $t->v('writing_rows',F::cache($movies,'getRows',null,30));
        // чтобы узнать кол-во фильмов с комментариями заказчика
        // $movies = new Movies;
        // $movies->setWithDomainMovieCustomerComment(true);
        // $t->v('customer_comment_rows',$movies->getRows());
        $t->v('customer_comment_rows', '?');
        // основная выдача
        $movies = new Movies;
        $movies->setUseDomainMovies(true);
        $movies->setLimit(10);
        $movies->setPage($page);
        $movies->setWithDomainDescription($withDomainDescription);
        $movies->setDomainMovieActive($active);
        $movies->setDomainMovieRework($rework);
        $movies->setDomainMovieShowToCustomer($showToCustomer);
        $movies->setOrder($orderStr);
        $movies->setDescriptionWriter($descriptionWriter);
        $movies->setDomainMovieCustomer($customer);
        $movies->setBookedForLogin($bookedForLogin);
        $movies->setDomainMovieWithCustomerEnoughBalance($customerIsEnoughBalance);
        $movies->setDomainMovieWriting($writing);
        $movies->setWithDomainMovieCustomerComment($withDomainMovieCustomerComment);
        if ($domainMovieId) {
            $movies->setDomainMovieId($domainMovieId);
        }
        // F::dump($movies);
        // $movies->setWithPoster(true);
        // $movies->setIncludeTrailers(false);
        // $movies->setListTemplate('admin/item_rewrited');
        $movies->setPaginationUrlPattern('/?r=Moderator/rewrited&amp;p=(:num)&amp;descriptionWriter='.$descriptionWriter.'&amp;withDomainDescription='.$withDomainDescription.'&amp;showToCustomer='.(is_null($showToCustomer) ? '' : ($showToCustomer ? 1 : 0)).'&amp;active='.$active.'&amp;rework='.$rework.'&amp;bookedForLogin='.$bookedForLogin.'&amp;customerIsEnoughBalance='.$customerIsEnoughBalance.'&amp;order='.$order.'&amp;btnActive='.$btnActive.'&amp;writing='.$writing.'&amp;withDomainMovieCustomerComment='.$withDomainMovieCustomerComment.'&amp;customer='.$customer);
        $maxBookMinutes = (new Cinema)->getBookMinutes();
        $list = '';
        foreach ($movies->get() as $v) {
            $l = new Template('admin/moderator/item_rewrited');
            // F::dump($v);
            $domainMovie = new DomainMovie($v['domain_movies_id']);
            $textRu = $domainMovie->getDomainMovieTextRuResult();
            $textRuWide = $domainMovie->getDomainMovieTextRuWideResult();
            if ($textRu) {
                if (isset($textRuWide->seo_check)) {
                    $seo_check = json_decode($textRuWide->seo_check);
                    // F::dump($seo_check->spam_percent);
                    $water = isset($seo_check->water_percent) ? $seo_check->water_percent : null;
                    $spam = isset($seo_check->spam_percent) ? $seo_check->spam_percent : null;
                } else {
                    $water = null;
                    $spam = null;
                }
                $l->v('unique', $textRu->unique.'%'.(isset($water) ? (', вода: '.$water.'%') : '').(isset($spam) ? (', спам: '.$spam.'%') : ''));
                $plagiatUrls = TextRu::getPlagiatUrls($textRu);
                $l->vf('plagiatUrls', $plagiatUrls ? '<ol>'.$plagiatUrls.'</ol>' : '');
            } else {
                $l->v('plagiatUrls', '');
                if ($domainMovie->getDomainMovieTextRuDescriptionId()) {
                    $l->v('unique', 'ожидание');
                } else {
                    $l->v('unique', 'нет данных');
                }
            }
            $customerComment = '';
            $symbolsComment = '';
            $symbolsComment = $domainMovie->getDomainMovieSymbolsFrom() ? ('от '.$domainMovie->getDomainMovieSymbolsFrom()) : '';
            $symbolsComment .= $domainMovie->getDomainMovieSymbolsTo() ? (' до '.$domainMovie->getDomainMovieSymbolsTo()) : '';
            if ($symbolsComment) {
                $customerComment = '<b>Требуемое количество символов:</b> '.$symbolsComment;
            }
            $l->v('customerComment', $customerComment);
            $l->v('domainMovieCustomer', $domainMovie->getDomainMovieCustomer());
            $l->v('domainMovieDescriptionWorkerPrice', $domainMovie->getDomainMovieDescriptionWorkerPrice());
            $l->v('domainMovieBookedForLogin', $domainMovie->getDomainMovieBookedForLogin());
            $l->v('domainMovieDescriptionWriter', $domainMovie->getDomainMovieDescriptionWriter());
            $l->v('domainMovieDescriptionDate', $domainMovie->getDomainMovieDescriptionDate());
            $l->v('domainMovieAddDate', $domainMovie->getDomainMovieAddDate());
            $l->v('domainMovieDescription', $domainMovie->getDomainMovieDescription());
            $l->v('description', $domainMovie->getDescription());
            // $l->v('description',$domainMovie->getDescription()?$domainMovie->getDescription():$domainMovie->getRandAltDescription());
            $l->v('titleRu', $domainMovie->getTitleRu());
            $l->v('domainMovieId', $domainMovie->getDomainMovieId());
            $l->v('poster', $domainMovie->getPosterOrPlaceholder());
            $l->v('domain', $domainMovie->getDomain());
            $l->v('kinopoisk_id', $domainMovie->getKinopoiskId());
            $l->v('classActiveIfHiddenFromCustomer', $domainMovie->getDomainMovieShowToCustomer() ? '' : 'active');
            $l->v('domainMovieReworkComment', $domainMovie->getDomainMovieRework() ? ('<b>Требуется доработка:</b> '.nl2br($domainMovie->getDomainMovieReworkComment())) : ($domainMovie->getDomainMovieReworkComment() ? ('<b>Требовалась доработка:</b> '.nl2br($domainMovie->getDomainMovieReworkComment())) : ''));
            $l->v('classActiveIfMovedToTop', $domainMovie->getDomainMovieMovedToTop() ? 'active' : '');
            $l->v('classActiveIfWriting', $domainMovie->getDomainMovieWriting() ? 'active' : '');
            $l->v('classActiveIfRework', $domainMovie->getDomainMovieRework() ? 'active' : '');
            // $l->v('domainMovieReworkComment',$domainMovie->getDomainMovieRework()?('<b>Комментарий заказчика:</b> '.$domainMovie->getDomainMovieReworkComment()):'');
            if ($domainMovie->getUniqueId() == 'not_in_base') {
                $l->v('altDescriptionsLink', '');
                $l->v('altDescriptions', '');
                $l->v('type', 'тип неизвестен');
                $l->v('year', 'год неизвестен');
            } else {
                $altMovies = $domainMovie->getDomainMovieAltDescriptions();
                $altMovies->setLimit(F::cache($altMovies, 'getRows'));
                $altMovies->setListTemplate('admin/rewrite/alt_description_item');
                $l->v('altDescriptionsLink', F::cache($altMovies, 'getRows') ? ('Есть альтернативные описания: '.F::cache($altMovies, 'getRows').' шт.') : '');
                $l->v('altDescriptions', F::cache($altMovies, 'getList'));
                $l->v('type', $domainMovie->getTypeRu(true));
                $l->v('year', $domainMovie->getYear());
            }
            $l->v('domainMovieCustomerComment', $domainMovie->getDomainMovieCustomerComment() ? ('<p><b>Комментарий для исполнителя:</b> '.nl2br($domainMovie->getDomainMovieCustomerComment()).'</p>') : '');
            $l->v('domainMoviePrivateComment', $domainMovie->getDomainMoviePrivateComment() ? ('<p><b>Комментарий для себя:</b> '.nl2br($domainMovie->getDomainMoviePrivateComment()).'</p>') : '');
            $l->v('domainMovieDmca', $domainMovie->getDomainMovieDmca());
            // если нет описания, то выводим информацию о том когда будет снята бронь
            if (! $domainMovie->getDomainMovieDescription()) {
                $bookDiff = ceil(((new \DateTime)->getTimestamp() - (new \DateTime($domainMovie->getDomainMovieBookDate()))->getTimestamp()) / 60);
                $l->v('bookTimeAlert', (($maxBookMinutes - $bookDiff) < 60) ? ('<span style="color: red;">Осталось '.($maxBookMinutes - $bookDiff).' минут на выполнение этого задания</span>') : '');
            } else {
                $l->v('bookTimeAlert', '');
            }
            $l->v('classActiveIfDoRedirect', $domainMovie->getDomainMovieDoRedirect() ? 'active' : '');
            $l->v('classActiveIfPlayerDeleted', $domainMovie->getDomainMovieDeleted() ? 'active' : '');
            // $domainMovieDescriptionUnique = F::cache($domainMovie,'getDomainMovieDescriptionUnique');
            // большие тексты виснут
            if ($domainMovie->getDomainMovieDescriptionLength() < 10000) {
                $domainMovieDescriptionUnique = F::cache($domainMovie, 'getDomainMovieDescriptionUnique', null, 600);
            } else {
                $domainMovieDescriptionUnique = null;
            }
            $l->v('domainMovieDescriptionUnique', is_null($domainMovieDescriptionUnique) ? 'нет данных' : (floor($domainMovieDescriptionUnique).'%'));
            $l->v('domainMovieTextRuDescriptionId', $domainMovie->getDomainMovieTextRuDescriptionId());
            $l->v('domainMovieDescriptionPricePer1k', $domainMovie->getDomainMovieDescriptionPricePer1k());
            $l->v('domainMovieIsExpress', $domainMovie->getDomainMovieIsExpress() ? 1 : 0);
            $l->v('domainMovieCustomerPricePer1k', $domainMovie->getDomainMovieCustomerPricePer1k());
            // F::dump($showToCustomer);
            if ($showToCustomer === false) {
                // если это страница модерации, узнаем, сколько еще на модерации текстов у этого исполнителя
                $rewriterMovies = new Movies;
                $rewriterMovies->setDomainMovieShowToCustomer(false);
                $rewriterMovies->setDomainMovieRework(false);
                $rewriterMovies->setWithDomainDescription(true);
                $rewriterMovies->setDescriptionWriter($domainMovie->getDomainMovieDescriptionWriter());
                $l->v('rewriterModerationRows', F::cache($rewriterMovies, 'getRows', null, 0).' шт.');
                // $l->v('rewriterModerationRows',$rewriterMovies->getRows().' шт.');
            } else {
                $l->v('rewriterModerationRows', '');
            }
            $reworkMinutes = $domainMovie->getReworkMinutes();
            $executionMinutes = $domainMovie->getExecutionMinutes();
            $fromConfirmMinutes = $domainMovie->getMinutesFromConfirm();
            $l->v('reworkHours', $reworkMinutes ? (ceil($reworkMinutes / 60).' ч.') : 'нет данных');
            $l->v('fromConfirmHours', $fromConfirmMinutes ? (ceil($fromConfirmMinutes / 60).' ч.') : 'нет данных');
            $l->v('executionHours', $executionMinutes ? (ceil($executionMinutes / 60).' ч.') : 'нет данных');
            if ($domainMovie->getDomainMovieDescriptionWriter()) {
                $descriptionWriter = new User($domainMovie->getDomainMovieDescriptionWriter());
                $l->v('jsonUserRestricted', json_encode($descriptionWriter->getRestricted()));
                $l->v('jsonUserExpress', json_encode($descriptionWriter->getExpressRewriter()));
            } else {
                $l->v('jsonUserRestricted', '');
                $l->v('jsonUserExpress', '');
            }
            $list .= $l->get();
        }
        // $t->v('cinemaList',$movies->getList());
        $t->v('cinemaList', $list);
        $t->v('cinemaPages', $movies->getPages());
        $t->v('cinemaRows', $movies->getRows());
        $t->v('rewriteBalance', $this->user->getRewriteBalance());
        $t->v('getMovieId', empty($_GET['movieId']) ? '' : F::checkstr($_GET['movieId']));
        $t->v('getDomainMovieId', empty($_GET['domainMovieId']) ? '' : F::checkstr($_GET['domainMovieId']));
        $t->v('isReworkPage', $rework ? 1 : 0);
        $this->setMenuVars($t, 'moderation');
        F::setPageVars($t);

        return $t->get();
    }

    public function submitShowToCustomer()
    {
        $domainMovieId = empty($_POST['domainMovieId']) ? null : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($domainMovieId);
        $movie->setDomainMovieShowToCustomer($movie->getDomainMovieShowToCustomer() ? false : true);
        $movie->setDomainMovieModerator($this->user->getLogin());
        $movie->setDomainMovieModerationPricePer1k($this->user->getModerationPricePer1k());
        $movie->save($this->user->getLogin());
        // F::dump($movie);
        F::redirect(F::referer_url());
    }

    public function submitOnRework()
    {
        $domainMovieId = empty($_POST['domainMovieId']) ? null : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($domainMovieId);
        $movie->setDomainMovieRework($movie->getDomainMovieRework() ? false : true);
        $movie->setDomainMovieShowToCustomer(false);
        $movie->setDomainMovieModerator($this->user->getLogin());
        $movie->setDomainMovieModerationPricePer1k(0);
        $movie->setDomainMovieReworkDate((new \DateTime)->format('Y-m-d H:i:s'));
        $movie->save($this->user->getLogin());
        // F::dump($movie);
        F::redirect(F::referer_url());
    }

    public function submitRework()
    {
        // Engine::setDebug(false);
        $domainMovieId = empty($_POST['id']) ? F::error('Movie ID required') : F::checkstr($_POST['id']);
        // убрали checkstr т.к. иначе дважды экранируются символы (далее в domainMovie->save())
        $reworkComment = empty($_POST['rework_comment']) ? null : $_POST['rework_comment'];
        $domainMovie = new DomainMovie($domainMovieId);
        // F::dump($domainMovie->getDomainMovieCustomer().' vs '.$this->user->getLogin());
        $domainMovie->setDomainMovieRework(true);
        $domainMovie->setDomainMovieReworkComment($reworkComment);
        $domainMovie->setDomainMovieReworkDate((new \DateTime)->format('Y-m-d H:i:s'));
        $domainMovie->setDomainMovieShowToCustomer(false);
        $domainMovie->setDomainMovieModerator($this->user->getLogin());
        $domainMovie->setDomainMovieModerationPricePer1k(0);
        $domainMovie->save($this->user->getLogin());
        if ($domainMovie->getDomainMovieDescriptionWriter()) {
            $user = new User($domainMovie->getDomainMovieDescriptionWriter());
            if ($user->getEmail() and $user->getEmailConfirmed()) {
                $email = new Template('admin/rewrite/emails/rework');
                $email->v('login', $user->getLogin());
                $email->v('domainMovieId', $domainMovie->getDomainMovieId());
                $email->v('reworkComment', nl2br($domainMovie->getDomainMovieReworkComment()));
                (new Cinema)->sendMail($user->getEmail(), 'Кинотекст', 'Доработка задания', $email->get(), 'kinotext');
            }
        }
        // return json_encode(true);
        F::redirect(F::referer_url());
    }

    public function returnAllMoviesOfRewriterToRewriteBase()
    {
        // F::dump('Сделано');
        $descriptionWriter = empty($_POST['descriptionWriter']) ? F::error('DescriptionWriter required') : F::checkstr($_POST['descriptionWriter']);
        $movies = new Movies;
        $movies->setDescriptionWriter($descriptionWriter);
        $movies->setDomainMovieShowToCustomer(false);
        $movies->setDomainMovieRework(false);
        $movies->setLimit(1000);
        $arr = $movies->get();
        foreach ($arr as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            if ($movie->getDomainMovieShowToCustomer()) {
                F::error('Сначала нужно поместить задание #'.$movie->getDomainMovieId().' на модерацию');
            }
            $movie->clearTask();
            $movie->save($this->user->getLogin());
        }
        F::alert('Все описания рерайтера '.$descriptionWriter.', находящиеся на модерации и не на доработке, очищены и возвращены в общую базу. Всего: '.$movies->getRows().' шт.');
    }

    public function acceptAllMoviesOfRewriter()
    {
        // F::dump('Сделано');
        $descriptionWriter = empty($_POST['descriptionWriter']) ? F::error('DescriptionWriter required') : F::checkstr($_POST['descriptionWriter']);
        $movies = new Movies;
        $movies->setDescriptionWriter($descriptionWriter);
        $movies->setDomainMovieShowToCustomer(false);
        $movies->setDomainMovieRework(false);
        $movies->setLimit(1000);
        $arr = $movies->get();
        foreach ($arr as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            $movie->setDomainMovieShowToCustomer(true);
            $movie->setDomainMovieModerator($this->user->getLogin());
            $movie->setDomainMovieModerationPricePer1k($this->user->getModerationPricePer1k());
            $movie->save($this->user->getLogin());
        }
        F::alert('Все описания рерайтера '.$descriptionWriter.', находящиеся на модерации и не на доработке, приняты. Всего: '.$movies->getRows().' шт.');
    }

    public function returnMovieToRewriteBase()
    {
        $id = empty($_POST['domainMovieId']) ? F::error('Movie ID required') : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($id);
        if ($movie->getDomainMovieShowToCustomer()) {
            F::error('Сначала нужно поместить задание на модерацию');
        }
        $movie->clearTask();
        $movie->save($this->user->getLogin());
        F::redirect(F::referer_url());
    }

    public function descriptionTextRuCheck()
    {
        $domain_movie_id = empty($_GET['movie_id']) ? F::error('Не передан ID фильма') : $_GET['movie_id'];
        $reCheck = isset($_GET['recheck']) ? filter_var($_GET['recheck'], FILTER_VALIDATE_BOOLEAN) : false;
        // F::dump($reCheck);
        $movie = new DomainMovie($domain_movie_id);
        $textRu = new TextRu;
        $textRu->setApiKey('d6c6c0b10e560da9dd8c1a56122d31c2');
        $textRu->setVisible('vis_on');
        if ($reCheck) {
            $textRu->setText($movie->getDomainMovieDescription());
            // $textRu->setCallbackUrl();
            $textRu->setExceptDomain($movie->getDomain());
            $textRuId = $textRu->addPost();
            if ($textRuId) {
                $movie->setDomainMovieTextRuDescriptionId($textRuId);
                $movie->setDomainMovieTextRuResult(null);
                $movie->setDomainMovieTextRuWideResult(null);
                $movie->setDomainMovieTextRuUnique(null);
                $movie->save($this->user->getLogin());

                return 'Добавлен на перепроверку';
            } else {
                return 'Ошибка при добавлении на перепроверку';
            }
        } else {
            if ($movie->getDomainMovieTextRuDescriptionId()) {
                $textRu->setTextId($movie->getDomainMovieTextRuDescriptionId());
                $result = $textRu->getResultPost();
                // F::dump(json_decode($unique));
                if ($result) {
                    $movie->setDomainMovieTextRuResult(json_decode($result->result_json));
                    $movie->setDomainMovieTextRuWideResult($result);
                    // F::dump($result->unique);
                    $movie->setDomainMovieTextRuUnique($result->unique);
                    $movie->save($this->user->getLogin());

                    return 'Данные обновлены';
                } else {
                    return 'Нет данных';
                }
            } else {
                $textRu->setText($movie->getDomainMovieDescription());
                // $textRu->setCallbackUrl();
                $textRu->setExceptDomain($movie->getDomain());
                $textRuId = $textRu->addPost();
                if ($textRuId) {
                    $movie->setDomainMovieTextRuDescriptionId($textRuId);
                    $movie->save($this->user->getLogin());

                    return 'Добавлен на проверку';
                } else {
                    return 'Ошибка при добавлении на проверку';
                }
            }
        }

        return $textRuId;
    }

    public function submitRewrite($args = [])
    {
        $description = empty($_POST['description']) ? null : $_POST['description'];
        $dmca = empty($_POST['dmca']) ? null : $_POST['dmca'];
        $domain_movie_id = empty($_POST['cinema_id']) ? F::error('Не передан ID фильма') : $_POST['cinema_id'];
        // $cinema = new Cinema;
        // F::dump($dmca);
        $movie = new DomainMovie($domain_movie_id);
        $movie->setDomainMovieDescription($description);
        $movie->setDomainMovieDmca($dmca);
        $movie->setDomainMovieModerator($this->user->getLogin());
        $movie->setDomainMovieModerationPricePer1k(0);
        $movie->save($this->user->getLogin());
        F::redirect(F::referer_url());
    }

    public function submitCopypastPenalty()
    {
        $domainMovieId = empty($_POST['domainMovieId']) ? null : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($domainMovieId);
        $user = new User($movie->getDomainMovieDescriptionWriter());
        $user->addTransfer(-20, 'Штраф за копипаст, задание #'.$movie->getDomainMovieId());
        $movie->setDomainMovieRework(true);
        $movie->setDomainMovieReworkComment('Копипаст, штраф 20руб.');
        $movie->setDomainMovieShowToCustomer(false);
        $movie->setDomainMovieModerator($this->user->getLogin());
        $movie->setDomainMovieModerationPricePer1k(0);
        $movie->save($this->user->getLogin());
        // F::dump($movie);
        F::redirect(F::referer_url());
    }

    public function getModerationRows()
    {
        Engine::setDebug(false);
        // считаем сколько заданий на модерации
        $movies = new Movies;
        $movies->setDomainMovieShowToCustomer(false);
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieRework(false);

        return $movies->getRows();
    }

    public function getTaskHistory()
    {
        $id = empty($_GET['id']) ? null : F::checkstr($_GET['id']);
        $movie = new DomainMovie($id);
        $arr = $movie->getHistory();
        $list = '';
        $allowedProperties = [
            'domainMovieDescription' => 'Текст',
            'domainMovieShowToCustomer' => 'Отмодерировано',
            'domainMovieBookedForLogin' => 'Бронь',
            'domainMovieRework' => 'На доработке',
            'domainMovieReworkComment' => 'Комментарий доработки',
            'domainMovieTextRuUnique' => 'Уникальность',
            'domainMovieModerator' => 'Модератор',
        ];
        foreach ($arr as $v) {
            if (! isset($allowedProperties[$v['property']])) {
                continue;
            }
            if (in_array($v['property'], [
                'domainMovieShowToCustomer',
                'domainMovieRework',
            ])) {
                $v['old_value'] = $v['old_value'] ? 'Да' : 'Нет';
                $v['new_value'] = $v['new_value'] ? 'Да' : 'Нет';
            }
            if ($v['property'] == 'domainMovieBookedForLogin') {
                if ($v['old_value'] == 'ozerin') {
                    $v['old_value'] = 'Общий список';
                }
                if ($v['new_value'] == 'ozerin') {
                    $v['new_value'] = 'Общий список';
                }
            }
            if ($v['property'] == 'domainMovieModerator') {
                if ($v['old_value'] == 'ozerin') {
                    $v['old_value'] = 'Администратор';
                }
                if ($v['new_value'] == 'ozerin') {
                    $v['new_value'] = 'Администратор';
                }
            }
            $user = new User($v['login']);
            if ($user->getLevel() == 'customer') {
                $v['login'] = 'Заказчик';
            }
            if ($user->getLevel() == 'admin') {
                $v['login'] = 'Администратор';
            }
            $tList = new Template('admin/moderator/item_task_history');
            $tList->v('date', $v['date']);
            $tList->v('login', $v['login']);
            $tList->v('old_value', $v['old_value']);
            $tList->v('new_value', $v['new_value']);
            $tList->v('property', $allowedProperties[$v['property']]);
            $tList->v('id', $v['id']);
            $list .= $tList->get();
        }
        $t = new Template('admin/moderator/task_history');
        $t->v('domainMovieId', $movie->getDomainMovieId());
        $t->v('list', $list);
        $this->setMenuVars($t);

        return $t->get();
    }

    public function restrictWorker()
    {
        $descriptionWriter = empty($_POST['descriptionWriter']) ? F::error('descriptionWriter required to use restrictWorker') : F::checkstr($_POST['descriptionWriter']);
        $days = empty($_POST['days']) ? 2 : F::checkstr($_POST['days']);
        // F::dump((new \DateTime)->modify('+'.$days.' days')->format('Y-m-d H:i:s'));
        $user = new User($descriptionWriter);
        if ($user->getRestricted()) {
            $user->setRestrictEndDate(null);
        } else {
            $user->setRestrictEndDate((new \DateTime)->modify('+'.$days.' days')->format('Y-m-d H:i:s'));
        }
        $user->save();
        F::redirect(F::referer_url());
    }

    public function workersQuality()
    {
        $arr = F::cache('engine\app\models\F', 'query_arr_arg', ['query' => '
			select users.login,
			count(*) as q,
			sum(if (rework_comment is null,0,1)) as reworks,
			ceil(100-sum(if (rework_comment is null,0,1))/count(*)*100) as good_percent,
			expressRewriter
			from '.F::typetab('users').'
			join '.F::typetab('domain_movies').' dm on dm.description_writer = users.login and description_date between date_sub(current_timestamp, interval 30 day) and current_timestamp
			where
			users.level in (\'worker\',\'moderator\')
			-- and not expressRewriter
			group by users.login
			order by q desc,good_percent desc
			;'], 600);
        $list = '';
        foreach ($arr as $v) {
            $list .= '<tr class="'.($v['expressRewriter'] ? 'table-success' : '').'">
			<td><a title="Посмотреть выполненные задания" href="/?r=Moderator/rewrited&amp;descriptionWriter='.$v['login'].'&amp;order=descriptionDateDesc">'.$v['login'].'</a></td>
			<td>'.$v['q'].'</td>
			<td>'.$v['good_percent'].'%</td>
			</tr>';
        }
        $result = '<h2>Качество выполнения заданий за последний месяц</h2>
		<table class="table table-stripped">
		<tr style="font-weight: bold;"><td>Логин</td><td>Выполнено</td><td>Без доработок</td></tr>
		'.$list.'
		</table>';
        F::alertLite($result);
    }
}
