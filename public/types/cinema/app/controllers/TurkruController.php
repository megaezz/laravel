<?php

namespace cinema\app\controllers;

use App\Models\Config as EngineConfig;
use cinema\app\models\eloquent\Domain;
use engine\app\models\Engine;
use Illuminate\Support\Facades\Route;

class TurkruController
{
    private $domain;

    private $route;

    private $template;

    public function __construct()
    {
        $this->domain = Domain::findOrFail(Engine::getDomain());
        $this->route = $this->domain->engineDomain->routes()->wherePatternLaravel(Route::current()->uri)->firstOrFail();
        if ($this->route->template) {
            $this->template = view($this->route->template)
                ->with('domain', $this->domain)
                ->with('route', $this->route)
                ->with('engineConfig', EngineConfig::first());
        }
    }

    public function episode_view($id, $slug = null)
    {
        $movie = $this->domain->movies()
            ->whereRelation('episodes', 'id', '=', $id)
            ->first();

        if (! $movie) {
            throw new \Exception('Movie episode is not found');
        }

        $headers = $this->route->getHeaders(['movie' => $movie]);

        return $this->template
            ->with('headers', $headers)
            ->with('movie', $movie)
            ->with('episode', $movie->movie->episodes()->find($id));
    }
}
