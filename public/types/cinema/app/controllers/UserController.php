<?php

namespace cinema\app\controllers;

use cinema\app\models\User;
use engine\app\models\F;

class UserController
{
    protected $auth = null;

    protected $user = null;

    public function __construct()
    {
        // F::error('Есть технические проблемы, ждем чуть-чуть.');
        $this->auth = new CinemaAuthorizationController(['worker', 'admin', 'moderator', 'admin-lite', 'customer']);
        $this->user = new User($this->auth->getLogin());
    }

    public function confirmEmail($redirect_url = null)
    {
        if (empty($redirect_url)) {
            F::error('Не передан redirect url');
        }
        $code = empty($_GET['code']) ? null : F::checkstr($_GET['code']);
        if (! $code) {
            F::error('Не передан код подтверждения');
        }
        if ($code == $this->user->getEmailCode()) {
            $this->user->setEmailConfirmed(true);
            $this->user->save();
            F::redirect($redirect_url);
        }
        F::alert('Адрес не подтвержден');
    }

    public function emailAlertBlock()
    {
        if (! $this->user->getEmail()) {
            return '<div class="alert alert-warning">Необходимо заполнить адрес электронной почты в <a href="/?r=Worker/userInfo">настройках профиля</a></div>';
        }
        if (! $this->user->getEmailConfirmed()) {
            return '<div class="alert alert-info">Адрес электронной почты указан, но не подтвержден. Пройдите проверку в <a href="/?r=Worker/userInfo">настройках профиля</a></div>';
        }
        // return 'Email подтвержден';
    }

    public function sendEmailCode()
    {
        if (! $this->user->getEmailCode()) {
            $this->user->setEmailCode(F::generateRandomStr(40));
            $this->user->save();
        }
        if (! filter_var($this->user->getEmail(), FILTER_VALIDATE_EMAIL)) {
            F::error('Указан некорректный email');
        }
    }

    public function submitChangeUserInfo()
    {
        $password = empty($_POST['password']) ? F::error('Не передан пароль') : $_POST['password'];
        $email = empty($_POST['email']) ? null : $_POST['email'];
        if ($email and ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            F::error('Указан некорректный email');
        }
        $this->user->setPassword($password);
        if (! $this->user->getEmailConfirmed()) {
            $this->user->setEmail($email);
        }
        $this->user->save();
        F::redirect(F::referer_url());
    }
}
