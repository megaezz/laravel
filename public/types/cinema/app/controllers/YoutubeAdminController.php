<?php

namespace cinema\app\controllers;

use cinema\app\models\Group;
use cinema\app\models\Groups;
use cinema\app\models\Movie;
use cinema\app\models\Movies;
use cinema\app\models\YoutubeCategory;
use cinema\app\models\YoutubeVideo;
use engine\app\models\Engine;
use engine\app\models\F;

class YoutubeAdminController extends AdminController
{
    public function getYoutube()
    {
        $api_key = 'AIzaSyCrYyY2xbmSoBMS0Ug1QoBVfrOF6YZdcq4';
        $client = new \Google\Client;
        $client->setDeveloperKey($api_key);
        $youtube = new \Google_Service_YouTube($client);

        return $youtube;
    }

    public function add_popular_videos()
    {
        // ini_set('xdebug.var_display_max_depth', 99);
        $youtube = $this->getYoutube();
        $arr = $youtube->videos->listVideos('snippet, statistics, contentDetails', [
            'chart' => 'mostPopular',
            'maxResults' => 50,
            'regionCode' => 'RU',
        ]);
        foreach ($arr['items'] as $v) {
            YoutubeVideo::add($v);
        }
        F::alert('Добавлено '.count($arr['items']).' видео');
    }

    public function add_videos_from_youtube_sitemap()
    {
        $limit = 50;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS youtube_sitemap.youtube_id
			from '.F::typetab('youtube_sitemap').'
			/* left join '.F::typetab('youtube_videos').' on youtube_videos.id = youtube_sitemap.youtube_id */
			where
			youtube_sitemap.youtube_videos_id is null and
			youtube_sitemap.youtube_deleted = 0 and
			youtube_sitemap.youtube_id is not null
			limit '.$limit.'
			;');
        // F::dump($arr);
        if (! $arr) {
            F::alert(
                'Обновление завершено'.
                F::jsRedirect(F::buildQuery(['r' => 'YoutubeAdmin/add_movies_from_youtube_videos'], null, false))
            );
        }
        $rows = F::rows_without_limit();
        $youtube_ids = [];
        foreach ($arr as $v) {
            $youtube_ids[$v['youtube_id']] = $v['youtube_id'];
        }
        $youtube_ids_str = implode(',', $youtube_ids);
        // F::dump($youtube_ids_str);
        $arr = $this->getYoutube()->videos->listVideos('snippet, statistics, contentDetails', [
            'id' => $youtube_ids_str,
            'maxResults' => 50,
            'regionCode' => 'RU',
        ]);
        // F::dump($arr);
        foreach ($arr['items'] as $v) {
            unset($youtube_ids[$v->id]);
            YoutubeVideo::add($v);
            F::query('update '.F::typetab('youtube_sitemap').' set youtube_videos_id = \''.$v->id.'\' where youtube_id = \''.$v->id.'\';');
        }
        // помечаем оставшиеся ID как удаленные
        foreach ($youtube_ids as $v) {
            F::query('
        		update '.F::typetab('youtube_sitemap').'
        		set youtube_deleted = 1
        		where youtube_id = \''.$v.'\'
        		;');
        }
        F::alert('
			Добавлено '.count($arr['items']).' видео. Осталось: '.$rows.
            F::jsRedirect(F::buildQuery(['r' => 'YoutubeAdmin/add_videos_from_youtube_sitemap'], null, false))
        );
    }

    public function add_movies_from_youtube_videos()
    {
        $limit = 1000;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS youtube_videos.id
			from '.F::typetab('youtube_videos').'
			left join '.F::typetab('cinema').' on cinema.youtube_id = youtube_videos.id
			where cinema.youtube_id is null
			limit '.$limit.'
			;');
        $rows = F::rows_without_limit();
        if (! $arr) {
            F::alert('
				Добавление youtube_videos в cinema завершено
				'.F::jsRedirect(F::buildQuery(['r' => 'YoutubeAdmin/updateCinemaFromYoutube'], null, false)).'
				');
        }
        foreach ($arr as $v) {
            $movie = new Movie(Movie::add());
            $movie->setYoutubeId($v['id']);
            $movie->setType('youtube');
            $movie->save();
        }
        F::alert(
            'Осталось: '.$rows.' видео'.
            F::jsRedirect(F::buildQuery(['r' => 'YoutubeAdmin/add_movies_from_youtube_videos'], null, false))
        );
    }

    // добавляем категории ютуба в базу
    public function add_categories()
    {
        $arr = $this->getYoutube()->videoCategories->listVideoCategories('snippet', ['hl' => 'RU', 'regionCode' => 'US']);
        $groups = new Groups;
        $groups->setType('youtube');
        $arr_groups = $groups->get();
        $arr_names = [];
        foreach ($arr_groups as $v) {
            $group = new Group($v['id']);
            $arr_names[$group->getName()] = true;
        }
        foreach ($arr['items'] as $v) {
            if (! isset($arr_names[$v->snippet->title])) {
                $group = new Group(Group::add());
                $group->setName($v->snippet->title);
                $group->setType('youtube');
                $group->save();
                $group->addTagByNameAndType($group->getName(), 'category');
            }
            F::query('
				insert into '.F::typetab('youtube_categories').'
				set id = \''.$v->id.'\',
				data = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				on duplicate key update
				data = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				;');
        }
        F::alert('Добавлено '.count($arr['items']).' категорий');
    }

    public function parseKosmosn()
    {
        $page = $_GET['page'] ?? 1;
        // $url = 'https://kosmosn.ru/newvideos.html?page='.$page;
        $url = 'https://srostova.ru/video/newvideos.php?&page=2'.$page;
        $proxy = Engine::getProxy();
        // загружаем страницу
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        // curl_setopt($ch, CURLOPT_INTERFACE, '135.181.182.179');
        $content = curl_exec($ch);
        // F::dump($content);
        // F::pre($content);
        //
        $document = new \DiDom\Document($content);
        // $document = new \DiDom\Document($url, true);
        $links = $document->find('.pm-ul-browse-videos .thumbnail .caption h3 a');
        // F::dump($links);
        if (! $links) {
            F::alert('Обновление завершено');
        }
        foreach ($links as $v) {
            $link = $v->getAttribute('href');
            $arr = F::query_assoc('
				select url from '.F::typetab('youtube_sitemap').'
				where url = \''.F::escape_string($link).'\'
				;');
            if ($arr) {
                sleep(5);
                F::alert($link.' существует, спим и уходим'.F::jsRedirect(F::buildQuery(['r' => 'YoutubeAdmin/parseKosmosnIds'], null, false)));
            } else {
                F::query('
					insert into '.F::typetab('youtube_sitemap').'
					set url = \''.F::escape_string($link).'\'
					;');
            }
        }
        $page++;

        return F::alert('Далее: '.$page.F::jsRedirect(F::buildQuery(['r' => 'YoutubeAdmin/parseKosmosn', 'page' => $page], null, false)));
    }

    public function parseKosmosnIds()
    {
        // F::dump('ok');
        $limit = 50;
        // $offset = $_GET['offset']??0;
        $arr = F::query_arr('
				select SQL_CALC_FOUND_ROWS url
				from '.F::typetab('youtube_sitemap').'
				where youtube_id is null
				limit '.$limit.'
				;');
        $rows = F::rows_without_limit();
        if (! $arr) {
            // F::dump('stop');
            F::alert(
                'Определение youtube_id в youtube_sitemap завершено'.
                F::jsRedirect(F::buildQuery(['r' => 'YoutubeAdmin/add_videos_from_youtube_sitemap'], null, false))
            );
        }
        $multiCurlArr = [];
        foreach ($arr as $v) {
            $object = new \stdClass;
            $object->url = $v['url'];
            $object->curl = new \stdClass;
            $object->curl->proxy = Engine::getProxy();
            $multiCurlArr[] = $object;
        }
        F::multiCurl($multiCurlArr);
        // F::dump($multiCurlArr);
        foreach ($multiCurlArr as $v) {
            $youtube_id = 'deleted';
            $publish_date = null;
            if (! empty($v->result)) {
                preg_match('/\"https:\/\/www\.youtube\.com\/watch\?v=(.+?)\"/', $v->result, $matches);
                if (isset($matches[1])) {
                    $youtube_id = $matches[1];
                } else {
                    if (isset($matches[1])) {
                        preg_match('/youtube\.com\/embed\/(.+?)\?/', $v->result, $matches);
                        $youtube_id = $matches[1];
                    }
                }
                // publish_date_timestamp: 1615228217,
                preg_match('/publish_date_timestamp\: ([0-9]+?)\,/', $v->result, $matches);
                if (isset($matches[1])) {
                    $publish_date = $matches[1];
                }
            }
            $youtube_id = empty($youtube_id) ? null : $youtube_id;
            F::query('
				update '.F::typetab('youtube_sitemap').'
				set
				youtube_id = '.($youtube_id ? '\''.F::escape_string($youtube_id).'\'' : 'null').',
				publish_date = '.($publish_date ? 'from_unixtime('.F::escape_string($publish_date).')' : 'null').'
				where url = \''.F::escape_string($v->url).'\'
				;');
        }

        // $offset = $offset + $limit;
        return F::alert('Осталось '.$rows.F::jsRedirect(F::buildQuery(['r' => 'YoutubeAdmin/parseKosmosnIds'], null, false)));
    }

    public function updateCinemaFromYoutube()
    {
        $limit = empty($_GET['limit']) ? 1000 : $_GET['limit'];
        $resetCheckers = $_GET['resetCheckers'] ?? null;
        if (! is_null($resetCheckers)) {
            Movies::resetCheckers();
        }
        $movies = new Movies;
        $movies->setIncludeMovies(false);
        $movies->setIncludeSerials(false);
        $movies->setIncludeTrailers(false);
        $movies->setIncludeYoutube(true);
        $movies->setLimit($limit);
        $movies->setPage(1);
        $movies->setChecked(false);
        $arrMovies = $movies->get();
        $list = '';
        foreach ($arrMovies as $v) {
            $movie = new Movie($v['id']);
            $movie->setChecked(true);
            $youtube = $movie->getYoutubeData()->getData();
            // F::dump($youtube);
            if (YoutubeCategory::isIdExists($youtube->snippet->categoryId)) {
                // F::dump('сущ');
                $category = (new YoutubeCategory($youtube->snippet->categoryId))->getData();
                // добавляем тэг категории
                $movie->addTagByName($category->snippet->title);
            }
            // foreach ($youtube->snippet->tags as $tag) {
            // 	$movie->addTagByName($tag);
            // }
            $movie->setTitleRu($youtube->snippet->title);
            $movie->setYear((new \DateTime($youtube->snippet->publishedAt))->format('Y'));
            $movie->setAddedAt((new \DateTime($youtube->snippet->publishedAt))->format('Y-m-d H:i:s'));
            $movie->save();
            // dump($movie);
            $list .= '
			<li>Видео #'.$movie->getId().' обновлено</li>';
        }
        F::alert(
            (! is_null($resetCheckers) ? '<p>Чеккеры Cinema сброшены</p>' : '').'
			<p>Всего: '.$movies->getRows().'</p>
			<p>Лимит: '.$limit.'</p>
			<ol>'.$list.'</ol>
			'.($movies->getRows() ? F::jsRedirect('/?r=YoutubeAdmin/updateCinemaFromYoutube') : F::alert('Обновление завершено'))
        );
    }
}
