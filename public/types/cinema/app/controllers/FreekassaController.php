<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\User;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Freekassa;
use engine\app\models\Template;

class FreekassaController
{
    private const SECRET_1 = ',I8C1/E4l94W^G]';

    private const SECRET_2 = '<n@F@PY9}FhX?M*';

    private const MERCHANT_ID = 2360;

    public function getSignature()
    {
        Engine::setDebug(false);
        $sum = $_POST['oa'] ?? F::error('Не передана сумма');
        $order_id = $_POST['o'] ?? F::error('Не передан заказ');
        $str = self::MERCHANT_ID.':'.$sum.':'.self::SECRET_1.':RUB:'.$order_id;
        // F::dump($str);
        // return $str;
        $sign = md5($str);

        return $sign;
    }

    public function handler()
    {
        $sum = $_REQUEST['AMOUNT'] ?? F::error('Не передана сумма');
        $order_id = $_REQUEST['MERCHANT_ORDER_ID'] ?? F::error('Не передан ID заказа');
        $income_sign = $_REQUEST['SIGN'] ?? F::error('Не передана подпись');
        $str = self::MERCHANT_ID.':'.$sum.':'.self::SECRET_2.':'.$order_id;
        // F::dump($str);
        $sign = md5($str);
        // F::dump($sign);
        // F::dump($income_sign);
        if (! Freekassa::checkIp(request()->ip())) {
            exit('wrong ip');
        }
        if ($sign != $income_sign) {
            exit('wrong sign');
        }
        // F::dump('YES');
        // если все ок, зачисляем sum
        $user = new User($order_id);
        $user->addTransfer($sum, 'Депозит Freekassa');
        $user->save();
        if ($user->getPartner()) {
            $partner = new User($user->getPartner());
            $partner->addTransfer($sum * (new Cinema)->getRefCustomer() / 100, 'Начисление за реферала '.$user->getLogin());
        }
        // отправляем письмо
        if ($user->getEmail() and $user->getEmailConfirmed()) {
            $email = new Template('admin/customer/emails/deposit');
            $email->v('login', $user->getLogin());
            $email->v('amount', $sum);
            (new Cinema)->sendMail($user->getEmail(), 'Киноконтент', 'Депозит', $email->get(), 'kinocontent');
        }
        logger()->info('Freekassa: Оплата '.($sum).' руб. зачтена для аккаунта '.$user->getLogin());

        return 'YES';
    }
}
