<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\DomainMovie;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\User as UserEloquent;
use cinema\app\models\Moonwalk;
use cinema\app\models\Movies;
use cinema\app\models\User;
use cinema\app\models\Users;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use engine\app\models\TextRu;

class WorkerController extends UserController
{
    protected $auth = null;

    protected $user = null;

    public function __construct()
    {
        if (Config::cached()->kinocontent_maintenance) {
            F::alertLite(Config::cached()->kinocontent_maintenance_message);
        }

        if (Engine::auth()) {
            $this->auth = Engine::auth();
            if (! in_array($this->auth->getLevel(), ['worker', 'admin', 'moderator'])) {
                throw new \Exception('Not allowed');
            }
        } else {
            $this->auth = new CinemaAuthorizationController(['worker', 'admin', 'moderator']);
        }
        // F::error('Есть технические проблемы, ждем чуть-чуть.');
        // $this->auth = new CinemaAuthorizationController(array('worker','admin','moderator'));
        $this->user = new User($this->auth->getLogin());
        if (! $this->user->getActiveDescriptionWriter()) {
            F::error('Ваш аккаунт был деактивирован, для восстановления доступа напишите в телеграм @megaezz');
        }
        // временно, пока не сделаю полноценный крон
        // CronController::calcTempBalances();
        // CronController::calcServiceLoad();
        // CronController::setRewritePricePer1k();
        // CronController::releaseMoviesFromLateRewriters();
    }

    public function getUserRatingArr()
    {
        return F::query_arr('
			select description_writer, count(*) as q, sum(char_length(description)) as sum_length,
			sum(if ((rework or not show_to_customer) and description_writer = domain_movies.description_writer,1,0)) as q_not_checked,
			sum(if (not rework and show_to_customer and description_writer = domain_movies.description_writer,1,0)) as q_checked
			from '.F::typetab('domain_movies').'
			where
			date(description_date) = current_date
			and description_writer is not null
			group by description_writer
			order by q_checked desc, sum_length desc
			;');
    }

    public function dayRating()
    {
        $currentDate = new \DateTime;
        $date = empty($_GET['date']) ? $currentDate->format('Y-m-d') : F::checkstr($_GET['date']);
        $arr = F::query_arr('
			select description_writer, count(*) as q, sum(char_length(description)) as sum_length,
			sum(if ((rework or not show_to_customer) and description_writer = domain_movies.description_writer,1,0)) as q_not_checked,
			sum(if (not rework and show_to_customer and description_writer = domain_movies.description_writer,1,0)) as q_checked
			from '.F::typetab('domain_movies').'
			where
			description_date between \''.$date.' 00:00:00\' and \''.$date.' 22:00:00\'
			and description_writer is not null
			group by description_writer
			order by q_checked desc, sum_length desc
			;');
        // $arr2 = F::query_assoc('select '.$date.' as date;');
        $date = new \DateTime($date);
        $list = '';
        $i = 0;
        foreach ($arr as $v) {
            $i++;
            $list .= '
			<tr '.(($v['description_writer'] == $this->user->getLogin()) ? 'style="font-weight: bold;"' : '').'>
			<td>'.$i.'</td>
			<td>'.$v['description_writer'].'</td>
			<td>'.$v['q_checked'].' '.($v['q_not_checked'] ? '<span style="color: #666">(+'.$v['q_not_checked'].' на проверке)</span>' : '').'</td>
			<td>'.$v['sum_length'].'</td>
			</tr>';
        }
        if (empty($list)) {
            $list = '<tr><td colspan="3">Сегодня еще никто ничего не писал</td></tr>';
        }
        $t = new Template('admin/rewrite/dayRating');
        $this->setMenuVars($t, 'rewrited');
        $t->v('dayRatingList', $list);
        $formatter = new \IntlDateFormatter(
            'ru_RU',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE,
            'Europe/Moscow'
        );

        $t->v('rusDate', $formatter->format($date));

        return $t->get();
    }

    public function index()
    {
        if (in_array($this->user->getLevel(), ['worker', 'moderator'])) {
            return $this->base();
        } else {
            return (new AdminController)->index();
        }
    }

    public function rewrited($args = [])
    {
        $page = empty($_GET['p']) ? 1 : F::checkstr($_GET['p']);
        $rework = empty($_GET['rework']) ? 0 : 1;
        $domainMovieId = empty($_GET['id']) ? null : F::checkstr($_GET['id']);
        $t = new Template($args['template'] ?? 'admin/rewrite/rewrited');
        $movies = new Movies;
        $movies->setLimit(10);
        $movies->setUseDomainMovies(true);
        // $movies->setDomain('seasongo.net');
        $movies->setWithDomainDescription(true);
        $movies->setOrder('domain_movies.description_date desc');
        $movies->setDescriptionWriter($this->user->getLogin());
        // $movies->setWithPoster(true);
        // $movies->setIncludeTrailers(false);
        $movies->setPage($page);
        $movies->setPaginationUrlPattern('/?r=Worker/rewrited&amp;p=(:num)&amp;rework='.$rework);
        $movies->setDomainMovieRework($rework);
        // для возможности выборочного исправления заданий сеоспринта
        if ($rework and $domainMovieId) {
            $movies->setDomainMovieId($domainMovieId);
        }
        $list = '';
        foreach ($movies->get() as $v) {
            $domainMovie = new DomainMovie($v['domain_movies_id']);
            if ($domainMovie->getTaskType() == 'movie') {
                if ($page > 5) {
                    $itemTemplate = 'admin/rewrite/item_rewrited_hidden';
                } else {
                    $itemTemplate = 'admin/rewrite/item_rewrited';
                }
                if ($rework) {
                    $itemTemplate = $args['item_rework_template'] ?? 'admin/rewrite/item_rework';
                }
            } else {
                if ($page > 5) {
                    $itemTemplate = 'admin/rewrite/items/other_rewrited_hidden';
                } else {
                    $itemTemplate = 'admin/rewrite/items/other_rewrited';
                }
                if ($rework) {
                    $itemTemplate = $args['other_rework_template'] ?? 'admin/rewrite/items/other_rework';
                }
            }
            $l = new Template($itemTemplate);
            // F::dump($domainMovie);
            $textRu = $domainMovie->getDomainMovieTextRuResult();
            if ($textRu and $textRu->unique != 100) {
                $l->v('unique', $textRu->unique.'%');
                $plagiatUrls = TextRu::getPlagiatUrls($textRu);
                $l->v('plagiatUrls', $plagiatUrls ? '<ol>'.$plagiatUrls.'</ol>' : '');
                $l->v('uniqueInfo', '<code>Уникальность: {unique} {plagiatUrls}</code>');
            } else {
                $l->v('unique', 'нет данных');
                $l->v('plagiatUrls', '');
                $l->v('uniqueInfo', '');
            }
            // если это доработка, то нужно выводить альт. опис.
            if ($rework) {
                if ($domainMovie->getUniqueId() == 'not_in_base') {
                    $l->v('altDescriptionsLink', '');
                    $l->v('altDescriptions', '');
                } else {
                    $altMovies = $domainMovie->getDomainMovieAltDescriptions();
                    $altMovies->setLimit(F::cache($altMovies, 'getRows'));
                    $altMovies->setListTemplate('admin/rewrite/alt_description_item');
                    $l->v('altDescriptionsLink', F::cache($altMovies, 'getRows') ? ('Есть альтернативные описания: '.F::cache($altMovies, 'getRows').' шт.') : '');
                    $l->v('altDescriptions', F::cache($altMovies, 'getList'));
                }
                $l->v('kinopoisk_id', $domainMovie->getKinopoiskId());
                $l->v('titleRuUrl', $domainMovie->getTitleRuUrl());
            }
            $customerComment = '';
            $symbolsComment = '';
            $symbolsComment = $domainMovie->getDomainMovieSymbolsFrom() ? ('от '.$domainMovie->getDomainMovieSymbolsFrom()) : '';
            $symbolsComment .= $domainMovie->getDomainMovieSymbolsTo() ? (' до '.$domainMovie->getDomainMovieSymbolsTo()) : '';
            if ($symbolsComment) {
                $customerComment = '<b>Требуемое количество символов:</b> '.$symbolsComment;
            }
            // if ($domainMovie->getDomainMovieCustomer()) {
            // 	$user = new User($domainMovie->getDomainMovieCustomer());
            // 	if ($user->getCustomerComment()) {
            // 		$customerComment.= '
            // 		<br><b>Комментарий заказчика:</b>
            // 		<div class="alert alert-secondary">
            // 		'.$user->getCustomerComment().'
            // 		</div>
            // 		';
            // 	}
            // }
            if ($domainMovie->getUniqueId() == 'not_in_base') {
                $l->v('type', 'тип неизвестен');
                $l->v('year', 'год неизвестен');
            } else {
                $l->v('year', $domainMovie->getYear());
                $l->v('type', $domainMovie->getTypeRu(true));
            }
            $l->v('customerComment', $customerComment);
            $l->v('descriptionWriter', $domainMovie->getDomainMovieDescriptionWriter());
            $l->v('descriptionDate', $domainMovie->getDomainMovieDescriptionDate());
            $l->v('domainMovieDescription', $domainMovie->getDomainMovieDescription());
            $l->v('description', $domainMovie->getDescription());
            $l->v('titleRu', $domainMovie->getTitleRu());
            $l->v('domainMovieId', $domainMovie->getDomainMovieId());
            $l->v('domainMovieDescriptionWorkerPrice', $domainMovie->getDomainMovieDescriptionWorkerPrice());
            $l->v('poster', $domainMovie->getPosterOrPlaceholder());
            $l->v('reworkComment', $domainMovie->getDomainMovieRework() ? ('<b>Комментарий к доработке: </b>'.nl2br($domainMovie->getDomainMovieReworkComment())) : '');
            $l->v('domainMovieCustomerComment', $domainMovie->getDomainMovieCustomerComment() ? nl2br(htmlspecialchars($domainMovie->getDomainMovieCustomerComment())) : 'не указан');
            // $l->v('reworkComment','');
            // $t->v('isNotInBase',($domainMovie->getUniqueId() == 'not_in_base')?1:0);
            $l->v('domainMovieCustomerLike', is_null($domainMovie->getDomainMovieCustomerLike()) ? '' : ($domainMovie->getDomainMovieCustomerLike() ? 1 : 0));
            $l->v('domainMovieExpressPriceInfo', $domainMovie->getDomainMovieIsExpress() ? '<i class="far fa-arrow-alt-circle-up"></i>&nbsp;'.ceil($domainMovie->getDomainMovieDescriptionPricePer1k() * 1.2).' руб. за 1000 сбп.' : '');
            // считаем сколько минут прошло после написания текста, чтобы поставить холд 30 минут на отображение статуса "проверен модератором"
            $currentDate = new \DateTime;
            $descriptionDate = new \DateTime($domainMovie->getDomainMovieDescriptionDate());
            $descriptionDateDiffSec = ($currentDate->getTimestamp() - $descriptionDate->getTimestamp());
            $isModereted = ($domainMovie->getDomainMovieShowToCustomer() and $descriptionDateDiffSec > 1800) ? true : false;
            // F::dump($descriptionDateDiffSec);
            // F::dump($descriptionDateDiff->format('%R%s'));
            $l->v('moderationStatus', $isModereted ? '<span class="badge badge-success"><i class="far fa-check-circle"></i>&nbsp;Проверено модератором</span>' : '<span class="badge badge-secondary"><i class="far fa-clock"></i>&nbsp;Проверяется модератором</span>');
            $list .= $l->get();
        }
        $t->v('cinemaList', $list);
        $t->v('cinemaPages', $movies->getPages());
        $t->v('cinemaRows', $movies->getRows());
        $this->setMenuVars($t, 'rewrited');

        return $t->get();
    }

    public function rewrite()
    {
        $page = empty($_GET['p']) ? 1 : F::checkstr($_GET['p']);
        $t = new Template('admin/rewrite/index');
        $movies = new Movies;
        $movies->setLimit(10);
        $movies->setUseDomainMovies(true);
        $movies->setBookedForLogin($this->user->getLogin());
        $movies->setWithDomainDescription(false);
        // отображаем задания, тех пользователей у которых достаточный баланс
        // временно убрал, чтобы не пропадали задания, если закончился баланс
        // $movies->setDomainMovieWithCustomerEnoughBalance(true);
        // сначала задания от покупателей, потом мои
        $movies->setOrder('isnull(domain_movies.customer), domain_movies.add_date');
        $movies->setPage($page);
        // $movies->setListTemplate('admin/rewrite/item');
        $movies->setPaginationUrlPattern('/?r=Worker/rewrite&p=(:num)');
        $list = '';
        $maxBookMinutes = (new Cinema)->getBookMinutes();
        foreach ($movies->get() as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            if ($movie->getTaskType() == 'movie') {
                $m = new Template('admin/rewrite/item');
            } else {
                $m = new Template('admin/rewrite/items/other_rewrite');
            }
            $customerComment = '';
            $symbolsComment = '';
            $symbolsComment = $movie->getDomainMovieSymbolsFrom() ? ('от '.$movie->getDomainMovieSymbolsFrom()) : '';
            $symbolsComment .= $movie->getDomainMovieSymbolsTo() ? (' до '.$movie->getDomainMovieSymbolsTo()) : '';
            if ($symbolsComment) {
                $customerComment = '<b>Требуемое количество символов:</b> '.$symbolsComment;
            }
            // if ($movie->getDomainMovieCustomer()) {
            // 	$user = new User($movie->getDomainMovieCustomer());
            // 	if ($user->getCustomerComment()) {
            // 		$customerComment.= '
            // 		<br><b>Комментарий заказчика:</b>
            // 		<div class="alert alert-secondary">
            // 		'.$user->getCustomerComment().'
            // 		</div>
            // 		';
            // 	}
            // }
            $m->v('customerComment', $customerComment);
            $m->v('domainMovieId', $movie->getDomainMovieId());
            $m->v('titleRu', $movie->getTitleRu());
            $m->v('titleRuUrl', $movie->getTitleRuUrl());
            $m->v('description', $movie->getDescription());
            // $m->v('description',$movie->getDescription()?$movie->getDescription():$movie->getRandAltDescription());
            $m->v('movieId', $movie->getId());
            $m->v('poster', $movie->getPosterOrPlaceholder());
            $m->v('kinopoisk_id', $movie->getKinopoiskId());
            if ($movie->getUniqueId() == 'not_in_base') {
                $m->v('altDescriptionsLink', '');
                $m->v('altDescriptions', '');
                $m->v('type', 'тип неизвестен');
                $m->v('year', 'год неизвестен');
            } else {
                $altMovies = $movie->getDomainMovieAltDescriptions();
                $altMovies->setLimit(F::cache($altMovies, 'getRows'));
                $altMovies->setListTemplate('admin/rewrite/alt_description_item');
                $m->v('altDescriptionsLink', F::cache($altMovies, 'getRows') ? ('Есть альтернативные описания: '.F::cache($altMovies, 'getRows').' шт.') : '');
                $m->v('altDescriptions', F::cache($altMovies, 'getList'));
                $m->v('year', $movie->getYear());
                $m->v('type', $movie->getTypeRu(true));
                // $m->v('type',($movie->getType() === 'trailer')?'Фильм':$movie->getTypeRu(true));
            }
            $m->v('domainMovieCustomerComment', $movie->getDomainMovieCustomerComment() ? nl2br(htmlspecialchars($movie->getDomainMovieCustomerComment())) : 'не указан');
            $bookDiff = ceil(((new \DateTime)->getTimestamp() - (new \DateTime($movie->getDomainMovieBookDate()))->getTimestamp()) / 60);
            // F::dump($bookDiff);
            $m->v('bookTimeAlert', (($maxBookMinutes - $bookDiff) < 60) ? ('<span style="color: red;">Осталось '.($maxBookMinutes - $bookDiff).' минут на выполнение этого задания</span>') : '');
            $m->v('domainMovieExpressPriceInfo', $movie->getDomainMovieIsExpress() ? '<i class="far fa-arrow-alt-circle-up"></i>&nbsp;'.ceil($movie->getDomainMovieDescriptionPricePer1k() * 1.2).' руб. за 1000 сбп.' : '');
            $list .= $m->get();
        }
        $t->v('cinemaList', $list);
        // $t->v('cinemaList',$movies->getList());
        $t->v('cinemaPages', $movies->getPages());
        $t->v('cinemaRows', $movies->getRows());
        $t->v('rewriterPricePer1k', $this->user->getRewritePricePer1k());
        $t->v('privateMessageForWorker', $this->privateMessageForWorker());
        $this->setMenuVars($t, 'rewrite');

        return $t->get();
    }

    public function test2()
    {
        $q = F::query_assoc('select current_timestamp;');
        $d = new \DateTime;

        return 'mysql: '.$q['current_timestamp'].' php: '.$d->format('Y-m-d H:i:s');
    }

    private function getSmartBaseQueue($limit = null, $lengthFrom = null, $lengthTo = null)
    {
        if (empty($limit)) {
            F::error('set limit to use getBaseQueue');
        }
        // сортируем массив заданий в нужной последовательности
        // сначала получаем весь список заданий
        $movies = new Movies;
        $movies->setUseDomainMovies(true);
        $movies->setBookedForLogin('ozerin');
        $movies->setWithDomainDescription(false);
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        $movies->setExcludeDomainMoviesForWorker($this->user->getLogin());
        $movies->setExcludeExpressMovies(! $this->user->getExpressRewriter());
        $movies->setExcludeAdult(! $this->user->getAdult());
        // сначала задания от покупателей в случайном порядке, потом мои
        $movies->setOrder('domain_movies.description_price_per_1k desc, domain_movies.to_top desc, isnull(domain_movies.customer),
			domain_movies.to_bottom,
			rand(),
			domain_movies.add_date');
        // обязательно последней строкой, иначе лимит будет неверный
        $movies->setLimit($movies->getRows());
        $arr = $movies->get();
        // результирующий массив
        $resultArr = [];
        // массив всех объектов DomainMovie
        $movieObjects = [];
        // оставляем только задания, которые удовлетворяют фильтру по символам и заодно создаем массив объектов DomainMovie
        foreach ($arr as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            $movieObjects[$movie->getDomainMovieId()] = $movie;
            // если указаны lengthFrom или lengthTo, проверяем удовлетворяет ли задание этой длинне
            if (! is_null($lengthFrom) or ! is_null($lengthTo)) {
                if (! $this->isExpectedMovieDescriptionLengthInSymbolRange($movie, $lengthFrom, $lengthTo)) {
                    unset($movieObjects[$movie->getDomainMovieId()]);

                    continue;
                }
            }
        }
        if (count($movieObjects) < $limit) {
            $limit = count($movieObjects);
        }
        // пока количество элементов результирующего массива меньше лимита, выполняем
        while (count($resultArr) < $limit) {
            // массив добавленных заказчиков (обнуляется каждый раз, так надо, чтобы добрать неоходимое до лимита количество заданий)
            $usedCustomers = [];
            foreach ($movieObjects as $movie) {
                // проверяем, если этот заказчик в списке уже добавленных, то не добавляем
                if (in_array($movie->getDomainMovieCustomer(), $usedCustomers)) {
                    continue;
                }
                $resultArr[] = ['domain_movies_id' => $movie->getDomainMovieId()];
                $usedCustomers[] = $movie->getDomainMovieCustomer();
                // если добавили это задание в очередь, то удаляем этот объект из массива объектов, чтобы это задание не появилось еще раз
                unset($movieObjects[$movie->getDomainMovieId()]);
                // если уже набрали $limit элементов, то завершаем цикл
                if (count($resultArr) == $limit) {
                    break;
                }
            }
        }

        // F::dump(in_array(null,array('123',null)));
        // $list = '';
        // foreach ($resultArr as $v) {
        // 	$movie = new DomainMovie($v['domain_movies_id']);
        // 	$list.= '<li>#'.$movie->getDomainMovieId().' &mdash; '.$movie->getDomainMovieCustomer().'</li>';
        // }
        // F::alert('<ol>'.$list.'</ol>');
        return $resultArr;
    }

    // принимает объект DomainMovie и проверяет, находится ли expectedSymbols в диапазоне от symbolsFrom до symbolsTo
    private function isExpectedMovieDescriptionLengthInSymbolRange($movie = null, $lengthFrom = null, $lengthTo = null)
    {
        $expectedSymbols = $movie->getExpectedSymbols();
        if (! is_null($lengthFrom)) {
            if ($expectedSymbols >= $lengthFrom) {
                $matchFrom = true;
            } else {
                $matchFrom = false;
            }
        } else {
            $matchFrom = true;
        }
        if (! is_null($lengthTo)) {
            if ($expectedSymbols < $lengthTo) {
                $matchTo = true;
            } else {
                $matchTo = false;
            }
        } else {
            $matchTo = true;
        }
        if ($matchFrom and $matchTo) {
            return true;
        }

        return false;
    }

    // полная база доступных видео (бронь ozerin)
    // не отображаем страницы, чтобы брали по порядку
    public function base()
    {
        // $page = empty($_GET['p'])?1:F::checkstr($_GET['p']);
        // $lengthFrom = isset($_GET['lengthFrom'])?F::checkstr($_GET['lengthFrom']):null;
        // $lengthTo = isset($_GET['lengthTo'])?F::checkstr($_GET['lengthTo']):null;
        $limit = 20;
        $lengthFrom = null;
        $lengthTo = null;
        $filter = empty($_GET['filter']) ? '' : F::checkstr($_GET['filter']);
        if ($filter == 'filterLessThen500') {
            $lengthTo = 500;
        }
        if ($filter == 'filterFrom500to1000') {
            $lengthFrom = 500;
            $lengthTo = 1000;
        }
        if ($filter == 'filterMoreThen1000') {
            $lengthFrom = 1000;
        }
        if ($filter == 'filterFrom500to550') {
            $lengthFrom = 500;
            $lengthTo = 550;
        }
        if ($filter == 'filterFrom550to1000') {
            $lengthFrom = 550;
            $lengthTo = 1000;
        }
        if ($filter == 'filterFrom500to501') {
            $lengthFrom = 500;
            $lengthTo = 501;
        }
        if ($filter == 'filterFrom501to1000') {
            $lengthFrom = 501;
            $lengthTo = 1000;
        }
        $page = 1;
        $t = new Template('admin/rewrite/base');
        $arr = $this->getSmartBaseQueue($limit, $lengthFrom, $lengthTo);
        /*
        $movies = new Movies;
        $movies->setLimit($limit);
        $movies->setUseDomainMovies(true);
        $movies->setBookedForLogin('ozerin');
        $movies->setWithDomainDescription(false);
        // отображаем задания, тех пользователей у которых достаточный баланс
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        // подключаем таблицу юзеров по полю заказчика для того чтобы использовать в сортировке customers.cutomer_active_tasks (хотя она уже и так подключается автоматиечски из-за setDomainMovieWithCustomerEnoughBalance)
        $movies->setUseDomainMovieCustomers(true);
        $movies->setExcludeDomainMoviesForWorker($this->user->getLogin());
        // сначала задания от покупателей в случайном порядке, потом мои
        $movies->setOrder('domain_movies.to_top desc, isnull(domain_movies.customer),
            domain_movies.to_bottom,
            rand(),
            domain_movies.add_date');
        // если указаны lengthFrom или lengthTo то нам нужно сделать постфильтрацию выборки, поэтому ставим лимит 2000 и прогоняем цикл
        if (!is_null($lengthFrom) or !is_null($lengthTo)) {
            $filteredArr = array();
            $movies->setLimit(2000);
            $arr = $movies->get();
            foreach ($arr as $k => $v) {
                $movie = new DomainMovie($v['domain_movies_id']);
                if ($this->isExpectedMovieDescriptionLengthInSymbolRange($movie,$lengthFrom,$lengthTo)) {
                    $filteredArr[] = $v;
                }
                // оставляем не более $limit элементов
                if (count($filteredArr) == $limit) {
                    break;
                }
            }
            $arr = $filteredArr;
        }
        if (!isset($arr)) {
            $arr = $movies->get();
        }
        */
        // получаем счетчики для меню фильтров
        $filterMovies = new Movies;
        $filterMovies->setUseDomainMovies(true);
        $filterMovies->setBookedForLogin('ozerin');
        $filterMovies->setWithDomainDescription(false);
        $filterMovies->setDomainMovieWithCustomerEnoughBalance(true);
        $filterMovies->setUseDomainMovieCustomers(true);
        $filterMovies->setExcludeDomainMoviesForWorker($this->user->getLogin());
        $filterMovies->setExcludeExpressMovies(! $this->user->getExpressRewriter());
        $filterMovies->setExcludeAdult(! $this->user->getAdult());
        $filterMovies->setLimit($filterMovies->getRows());
        $counters['filterLessThen500Rows'] = 0;
        $counters['filterFrom500to1000Rows'] = 0;
        $counters['filterFrom500to550Rows'] = 0;
        $counters['filterFrom550to1000Rows'] = 0;
        $counters['filterMoreThen1000Rows'] = 0;
        $counters['filterFrom500to501Rows'] = 0;
        $counters['filterFrom501to1000Rows'] = 0;
        foreach ($filterMovies->get() as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            $expectedSymbols = $movie->getExpectedSymbols();
            if ($expectedSymbols < 500) {
                $counters['filterLessThen500Rows']++;
            }
            if ($expectedSymbols >= 500 and $expectedSymbols < 1000) {
                $counters['filterFrom500to1000Rows']++;
            }
            if ($expectedSymbols >= 500 and $expectedSymbols < 550) {
                $counters['filterFrom500to550Rows']++;
            }
            if ($expectedSymbols >= 550 and $expectedSymbols < 1000) {
                $counters['filterFrom550to1000Rows']++;
            }
            if ($expectedSymbols >= 1000) {
                $counters['filterMoreThen1000Rows']++;
            }
            if ($expectedSymbols >= 500 and $expectedSymbols < 501) {
                $counters['filterFrom500to501Rows']++;
            }
            if ($expectedSymbols >= 501 and $expectedSymbols < 1000) {
                $counters['filterFrom501to1000Rows']++;
            }
        }
        $t->v('filterLessThen500Rows', $counters['filterLessThen500Rows']);
        $t->v('filterFrom500to1000Rows', $counters['filterFrom500to1000Rows']);
        $t->v('filterFrom500to550Rows', $counters['filterFrom500to550Rows']);
        $t->v('filterFrom550to1000Rows', $counters['filterFrom550to1000Rows']);
        $t->v('filterMoreThen1000Rows', $counters['filterMoreThen1000Rows']);
        $t->v('filterFrom500to501Rows', $counters['filterFrom500to501Rows']);
        $t->v('filterFrom501to1000Rows', $counters['filterFrom501to1000Rows']);
        $t->v('filterLessThen500Active', ($filter == 'filterLessThen500') ? 'active' : '');
        $t->v('filterFrom500to1000Active', ($filter == 'filterFrom500to1000') ? 'active' : '');
        $t->v('filterFrom500to550Active', ($filter == 'filterFrom500to550') ? 'active' : '');
        $t->v('filterFrom550to1000Active', ($filter == 'filterFrom550to1000') ? 'active' : '');
        $t->v('filterMoreThen1000Active', ($filter == 'filterMoreThen1000') ? 'active' : '');
        $t->v('filterFrom500to501Active', ($filter == 'filterFrom500to501') ? 'active' : '');
        $t->v('filterFrom501to1000Active', ($filter == 'filterFrom501to1000') ? 'active' : '');
        // $t->v('filterLessThen500Rows',$filterMovies->getRows());
        // $filterMovies->setDomainMovieDescriptionLengthFrom(500);
        // $filterMovies->setDomainMovieDescriptionLengthTo(1000);
        // $t->v('filterFrom500to1000Rows',$filterMovies->getRows());
        // $filterMovies->setDomainMovieDescriptionLengthFrom(1000);
        // $filterMovies->setDomainMovieDescriptionLengthTo(null);
        // $t->v('filterMoreThen1000Rows',$filterMovies->getRows());
        //
        $list = '';
        foreach ($arr as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            if ($movie->getTaskType() == 'movie') {
                $m = new Template('admin/rewrite/base_item');
            } else {
                $m = new Template('admin/rewrite/items/other_base');
            }
            $customerComment = '';
            $symbolsComment = '';
            $symbolsComment = $movie->getDomainMovieSymbolsFrom() ? ('от '.$movie->getDomainMovieSymbolsFrom()) : '';
            $symbolsComment .= $movie->getDomainMovieSymbolsTo() ? (' до '.$movie->getDomainMovieSymbolsTo()) : '';
            if ($symbolsComment) {
                $customerComment = '<b>Требуемое количество символов:</b> '.$symbolsComment;
            }
            // if ($movie->getDomainMovieCustomer()) {
            // 	$user = new User($movie->getDomainMovieCustomer());
            // 	if ($user->getCustomerComment()) {
            // 		$customerComment.= '
            // 		<br><b>Комментарий заказчика:</b>
            // 		<div class="alert alert-secondary">
            // 		'.$user->getCustomerComment().'
            // 		</div>
            // 		';
            // 	}
            // }
            $m->v('customerComment', $customerComment);
            // $m->v('description',$movie->getDescription()?$movie->getDescription():$movie->getRandAltDescription());
            $m->v('description', $movie->getDescription());
            $m->v('domainMovieId', $movie->getDomainMovieId());
            $m->v('titleRu', $movie->getTitleRu());
            $m->v('titleRuUrl', $movie->getTitleRuUrl());
            $m->v('movieId', $movie->getId());
            $m->v('poster', $movie->getPosterOrPlaceholder());
            $fromConfirmMinutes = $movie->getMinutesFromConfirm();
            $m->v('fromConfirmHours', $fromConfirmMinutes ? (ceil($fromConfirmMinutes / 60).' ч.') : 'нет данных');
            // $m->v('year',$movie->getYear());
            // $m->v('type',$movie->getTypeRu(true));
            $m->v('kinopoisk_id', $movie->getKinopoiskId());
            if ($movie->getUniqueId() == 'not_in_base') {
                $m->v('altDescriptionsLink', '');
                $m->v('altDescriptions', '');
                $m->v('type', 'тип неизвестен');
                $m->v('year', 'год неизвестен');
            } else {
                $altMovies = $movie->getDomainMovieAltDescriptions();
                $altMovies->setLimit(F::cache($altMovies, 'getRows'));
                $altMovies->setListTemplate('admin/rewrite/alt_description_item');
                $m->v('altDescriptionsLink', $altMovies->getRows() ? ('Есть альтернативные описания: '.F::cache($altMovies, 'getRows').' шт.') : '');
                $m->v('altDescriptions', F::cache($altMovies, 'getList'));
                $m->v('type', $movie->getTypeRu(true));
                $m->v('year', $movie->getYear());
            }
            $m->v('domainMovieCustomerComment', $movie->getDomainMovieCustomerComment() ? nl2br(htmlspecialchars($movie->getDomainMovieCustomerComment())) : 'не указан');
            $m->v('domainMovieExpressPriceInfo', $movie->getDomainMovieIsExpress() ? '<i class="far fa-arrow-alt-circle-up"></i>&nbsp;'.ceil($movie->getDomainMovieDescriptionPricePer1k() * 1.2).' руб. за 1000 сбп.' : '');
            $list .= $m->get();
        }
        $t->v('cinemaList', $list ? $list : '<h4 class="text-center mt-5">Упс, задания закончились... 🤷‍♂️</h4>
		<h4 class="text-center">Скоро должны появиться.</h4>');
        $formatter = new \IntlDateFormatter(
            'ru_RU',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE,
            'Europe/Moscow'
        );
        $t->v('currentDateRus', $formatter->format(new \DateTime));
        // $t->v('cinemaList',$movies->getList());
        // $t->v('cinemaPages',$movies->getPages());
        // $t->v('cinemaRows',$movies->getRows());
        $this->setMenuVars($t, 'base');
        // для алерт блока с партнеркой
        $t->v('userId', $this->user->getId());

        return $t->get();
    }

    // полная база доступных видео (бронь ozerin)
    // не отображаем страницы, чтобы брали по порядку
    public function baseOld()
    {
        // $page = empty($_GET['p'])?1:F::checkstr($_GET['p']);
        // $lengthFrom = isset($_GET['lengthFrom'])?F::checkstr($_GET['lengthFrom']):null;
        // $lengthTo = isset($_GET['lengthTo'])?F::checkstr($_GET['lengthTo']):null;
        $limit = 20;
        $lengthFrom = null;
        $lengthTo = null;
        $filter = empty($_GET['filter']) ? '' : F::checkstr($_GET['filter']);
        if ($filter == 'filterLessThen500') {
            $lengthTo = 500;
        }
        if ($filter == 'filterFrom500to1000') {
            $lengthFrom = 500;
            $lengthTo = 1000;
        }
        if ($filter == 'filterMoreThen1000') {
            $lengthFrom = 1000;
        }
        if ($filter == 'filterFrom500to550') {
            $lengthFrom = 500;
            $lengthTo = 550;
        }
        if ($filter == 'filterFrom550to1000') {
            $lengthFrom = 550;
            $lengthTo = 1000;
        }
        if ($filter == 'filterFrom500to501') {
            $lengthFrom = 500;
            $lengthTo = 501;
        }
        if ($filter == 'filterFrom501to1000') {
            $lengthFrom = 501;
            $lengthTo = 1000;
        }
        $page = 1;
        $t = new Template('admin/rewrite/base');
        $movies = new Movies;
        $movies->setLimit($limit);
        $movies->setUseDomainMovies(true);
        $movies->setBookedForLogin('ozerin');
        $movies->setWithDomainDescription(false);
        // отображаем задания, тех пользователей у которых достаточный баланс
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        // подключаем таблицу юзеров по полю заказчика для того чтобы использовать в сортировке customers.cutomer_active_tasks (хотя она уже и так подключается автоматиечски из-за setDomainMovieWithCustomerEnoughBalance)
        $movies->setUseDomainMovieCustomers(true);
        $movies->setExcludeDomainMoviesForWorker($this->user->getLogin());
        // сначала задания от покупателей в случайном порядке, потом мои
        $movies->setOrder('domain_movies.to_top desc, isnull(domain_movies.customer),
			domain_movies.to_bottom,
			rand(),
			domain_movies.add_date');
        // если указаны lengthFrom или lengthTo то нам нужно сделать постфильтрацию выборки, поэтому ставим лимит 2000 и прогоняем цикл
        if (! is_null($lengthFrom) or ! is_null($lengthTo)) {
            $filteredArr = [];
            $movies->setLimit(2000);
            $arr = $movies->get();
            foreach ($arr as $k => $v) {
                $movie = new DomainMovie($v['domain_movies_id']);
                if ($this->isExpectedMovieDescriptionLengthInSymbolRange($movie, $lengthFrom, $lengthTo)) {
                    $filteredArr[] = $v;
                }
                // оставляем не более $limit элементов
                if (count($filteredArr) == $limit) {
                    break;
                }
            }
            $arr = $filteredArr;
        }
        if (! isset($arr)) {
            $arr = $movies->get();
        }
        // получаем счетчики для меню фильтров
        $filterMovies = new Movies;
        $filterMovies->setUseDomainMovies(true);
        $filterMovies->setBookedForLogin('ozerin');
        $filterMovies->setWithDomainDescription(false);
        $filterMovies->setDomainMovieWithCustomerEnoughBalance(true);
        $filterMovies->setUseDomainMovieCustomers(true);
        $filterMovies->setExcludeDomainMoviesForWorker($this->user->getLogin());
        $filterMovies->setLimit(2000);
        $counters['filterLessThen500Rows'] = 0;
        $counters['filterFrom500to1000Rows'] = 0;
        $counters['filterFrom500to550Rows'] = 0;
        $counters['filterFrom550to1000Rows'] = 0;
        $counters['filterMoreThen1000Rows'] = 0;
        $counters['filterFrom500to501Rows'] = 0;
        $counters['filterFrom501to1000Rows'] = 0;
        foreach ($filterMovies->get() as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            $expectedSymbols = $movie->getExpectedSymbols();
            if ($expectedSymbols < 500) {
                $counters['filterLessThen500Rows']++;
            }
            if ($expectedSymbols >= 500 and $expectedSymbols < 1000) {
                $counters['filterFrom500to1000Rows']++;
            }
            if ($expectedSymbols >= 500 and $expectedSymbols < 550) {
                $counters['filterFrom500to550Rows']++;
            }
            if ($expectedSymbols >= 550 and $expectedSymbols < 1000) {
                $counters['filterFrom550to1000Rows']++;
            }
            if ($expectedSymbols >= 1000) {
                $counters['filterMoreThen1000Rows']++;
            }
            if ($expectedSymbols >= 500 and $expectedSymbols < 501) {
                $counters['filterFrom500to501Rows']++;
            }
            if ($expectedSymbols >= 501 and $expectedSymbols < 1000) {
                $counters['filterFrom501to1000Rows']++;
            }
        }
        $t->v('filterLessThen500Rows', $counters['filterLessThen500Rows']);
        $t->v('filterFrom500to1000Rows', $counters['filterFrom500to1000Rows']);
        $t->v('filterFrom500to550Rows', $counters['filterFrom500to550Rows']);
        $t->v('filterFrom550to1000Rows', $counters['filterFrom550to1000Rows']);
        $t->v('filterMoreThen1000Rows', $counters['filterMoreThen1000Rows']);
        $t->v('filterFrom500to501Rows', $counters['filterFrom500to501Rows']);
        $t->v('filterFrom501to1000Rows', $counters['filterFrom501to1000Rows']);
        $t->v('filterLessThen500Active', ($filter == 'filterLessThen500') ? 'active' : '');
        $t->v('filterFrom500to1000Active', ($filter == 'filterFrom500to1000') ? 'active' : '');
        $t->v('filterFrom500to550Active', ($filter == 'filterFrom500to550') ? 'active' : '');
        $t->v('filterFrom550to1000Active', ($filter == 'filterFrom550to1000') ? 'active' : '');
        $t->v('filterMoreThen1000Active', ($filter == 'filterMoreThen1000') ? 'active' : '');
        $t->v('filterFrom500to501Active', ($filter == 'filterFrom500to501') ? 'active' : '');
        $t->v('filterFrom501to1000Active', ($filter == 'filterFrom501to1000') ? 'active' : '');
        // $t->v('filterLessThen500Rows',$filterMovies->getRows());
        // $filterMovies->setDomainMovieDescriptionLengthFrom(500);
        // $filterMovies->setDomainMovieDescriptionLengthTo(1000);
        // $t->v('filterFrom500to1000Rows',$filterMovies->getRows());
        // $filterMovies->setDomainMovieDescriptionLengthFrom(1000);
        // $filterMovies->setDomainMovieDescriptionLengthTo(null);
        // $t->v('filterMoreThen1000Rows',$filterMovies->getRows());
        //
        $list = '';
        foreach ($arr as $v) {
            $m = new Template('admin/rewrite/base_item');
            $movie = new DomainMovie($v['domain_movies_id']);
            $customerComment = '';
            $symbolsComment = '';
            $symbolsComment = $movie->getDomainMovieSymbolsFrom() ? ('от '.$movie->getDomainMovieSymbolsFrom()) : '';
            $symbolsComment .= $movie->getDomainMovieSymbolsTo() ? (' до '.$movie->getDomainMovieSymbolsTo()) : '';
            if ($symbolsComment) {
                $customerComment = '<b>Требуемое количество символов:</b> '.$symbolsComment;
            }
            // if ($movie->getDomainMovieCustomer()) {
            // 	$user = new User($movie->getDomainMovieCustomer());
            // 	if ($user->getCustomerComment()) {
            // 		$customerComment.= '
            // 		<br><b>Комментарий заказчика:</b>
            // 		<div class="alert alert-secondary">
            // 		'.$user->getCustomerComment().'
            // 		</div>
            // 		';
            // 	}
            // }
            $m->v('customerComment', $customerComment);
            // $m->v('description',$movie->getDescription()?$movie->getDescription():$movie->getRandAltDescription());
            $m->v('description', $movie->getDescription());
            $m->v('domainMovieId', $movie->getDomainMovieId());
            $m->v('titleRu', $movie->getTitleRu());
            $m->v('titleRuUrl', $movie->getTitleRuUrl());
            $m->v('movieId', $movie->getId());
            $m->v('poster', $movie->getPosterOrPlaceholder());
            // $m->v('year',$movie->getYear());
            // $m->v('type',$movie->getTypeRu(true));
            $m->v('kinopoisk_id', $movie->getKinopoiskId());
            if ($movie->getUniqueId() == 'not_in_base') {
                $m->v('altDescriptionsLink', '');
                $m->v('altDescriptions', '');
                $m->v('type', 'тип неизвестен');
                $m->v('year', 'год неизвестен');
            } else {
                $altMovies = $movie->getDomainMovieAltDescriptions();
                $altMovies->setLimit($altMovies->getRows());
                $altMovies->setListTemplate('admin/rewrite/alt_description_item');
                $m->v('altDescriptionsLink', $altMovies->getRows() ? ('Есть альтернативные описания: '.$altMovies->getRows().' шт.') : '');
                $m->v('altDescriptions', $altMovies->getList());
                $m->v('type', $movie->getTypeRu(true));
                $m->v('year', $movie->getYear());
            }
            $m->v('domainMovieCustomerComment', $movie->getDomainMovieCustomerComment() ? nl2br(htmlspecialchars($movie->getDomainMovieCustomerComment())) : 'не указан');
            $list .= $m->get();
        }
        $t->v('cinemaList', $list);
        // $t->v('cinemaList',$movies->getList());
        // $t->v('cinemaPages',$movies->getPages());
        // $t->v('cinemaRows',$movies->getRows());
        $this->setMenuVars($t, 'base');
        // для алерт блока с партнеркой
        $t->v('userId', $this->user->getId());

        return $t->get();
    }

    public function bookMovie()
    {
        $domain_movie_id = empty($_POST['cinema_id']) ? F::error('Не передан ID фильма') : $_POST['cinema_id'];
        $user = $this->user;
        // если тест не пройден - предлагаем пройти
        if (! $this->user->getTestPassed()) {
            F::alert('Чтобы начать выполнение заданий, вам нужно пройти небольшой тест из 15 вопросов. На выполнение дается 10 минут. <a href="/?r=Worker/russianTest">Приступить к выполнению</a>.');
        }
        // если у юзера больше 3 видео в брони, то нельзя больше бронировать
        $movies = new Movies;
        $movies->setBookedForLogin($user->getLogin());
        $movies->setWithDomainDescription(false);
        // $movies->setDomainMovieWithCustomerEnoughBalance(true);
        if ($user->getRestricted()) {
            F::error('Модератор ограничил вам доступ к выполнению заданий до: '.$user->getRestrictEndDate().' (по московскому времени). Работайте над качеством текста, чтобы не получать ограничения вновь.');
        }
        if ($movies->getRows() >= 3) {
            F::error('У вас уже забронировано '.$movies->getRows().' заданий. Сначала выполните их и потом сможете брать новые.');
        }
        // проверяем есть ли у рерайтера заказы на доработке, если да, то запрещаем бронировать новые
        $movies = new Movies;
        $movies->setBookedForLogin($user->getLogin());
        $movies->setDomainMovieRework(true);
        if ($movies->getRows()) {
            F::alert('У вас есть задания, <a href="/?r=Worker/rewrited&amp;rework=1">требующие доработки: '.$movies->getRows().' шт.</a>. Вы не можете брать новые, пока не исправите их.');
        }
        $movie = new DomainMovie($domain_movie_id);
        if ($movie->getDomainMovieBookedForLogin() !== 'ozerin') {
            F::error('Это задание уже кто-то забрал, вернитесь назад и возьмите другое');
        }
        if ($movie->getDomainMovieDescription()) {
            F::error('Это задание уже выполнено');
        }
        // F::dump($movie);
        // F::dump($movie->getLastBookingData());
        $lastBookingData = $movie->getLastBookingData($this->user->getLogin());
        // F::dump($lastBookingData);
        if ($lastBookingData) {
            if ($lastBookingData['new_value'] === $this->user->getLogin()) {
                $cinema = new Cinema;
                // F::dump($cinema->getBookMinutes());
                $nowDate = new \DateTime;
                $lastBookingDate = new \DateTime($lastBookingData['date']);
                $accessDate = (new \DateTime($lastBookingData['date']))->modify('+ '.($cinema->getBookMinutes() + 60).' minutes');
                // F::dump(['now'=>$nowDate->format('Y-m-d H:i:s'),'last'=>$lastBookingDate->format('Y-m-d H:i:s'),'access'=>$accessDate->format('Y-m-d H:i:s')]);
                if ($nowDate < $accessDate) {
                    F::error('Вы сможете взять это задание вновь через '.$nowDate->diff($accessDate)->format('%h часов %i минут'));
                }
            }
        }
        $movie->setDomainMovieBookedForLogin($user->getLogin());
        $movie->setDomainMovieBookDate((new \DateTime)->format('Y-m-d H:i:s'));
        // F::dump($movie);
        $movie->save($this->user->getLogin());
        // F::redirect(F::referer_url());
        // return 'ok';
        F::redirect('/?r=Worker/rewrite');
    }

    public function submitRewrite($args = [])
    {
        $login = $this->user->getLogin();
        $description = empty($_POST['description']) ? F::error('Не передано описание') : $_POST['description'];
        $domain_movie_id = empty($_POST['cinema_id']) ? F::error('Не передан ID фильма') : $_POST['cinema_id'];
        // удаляем сразу лишние пробелы
        $description = preg_replace('/ {2,}/', ' ', $description);
        // $cinema = new Cinema;
        $user = new User($login);
        // $domain = empty($_POST['domain'])?F::error('Не передан домен'):F::checkstr($_POST['domain']);
        // $id = DomainMovie::add();
        $movie = new DomainMovie($domain_movie_id);
        if ($movie->getDomainMovieBookedForLogin() != $this->user->getLogin()) {
            F::alert('Это задание больше не забронировано за вами. На выполнение задания дается 6 часов.');
        }
        // проверяем, входит ли кол-во символов в заданные рамки
        if ($movie->getDomainMovieSymbolsFrom() and mb_strlen($description) < $movie->getDomainMovieSymbolsFrom() * 0.9) {
            F::alert('Не хватает символов. По заданию требуется от '.$movie->getDomainMovieSymbolsFrom().' символов. <a href="#" onclick="history.go(-1); return false;">Вернуться к редактированию.</a>');
        }
        if ($movie->getDomainMovieSymbolsTo() and mb_strlen($description) > $movie->getDomainMovieSymbolsTo() * 1.1) {
            F::alert('Слишком длинный текст. По заданию требуется до '.$movie->getDomainMovieSymbolsTo().' символов. <a href="#" onclick="history.go(-1); return false;">Вернуться к редактированию.</a>');
        }
        // если описание уже было написано
        if ($movie->getDomainMovieDescription()) {
            // если описание было написано текущим рерайтером, значит он хочет отредактировать ошибки
            if ($movie->getDomainMovieDescriptionWriter() == $login) {
                // если это доработка, то не проверяем дату, позволяем отредактировать
                if ($movie->getDomainMovieRework()) {
                    // снимаем метку доработки
                    $movie->setDomainMovieRework(false);
                    $movie->setDomainMovieDescription($description);
                } else {
                    // проверяем дату, если меньше 1 дня то можно отредактировать
                    if ($movie->getDomainMovieDescriptionDate()) {
                        $descriptionDate = new \DateTime($movie->getDomainMovieDescriptionDate());
                    } else {
                        // если дата по какой то причине не была установлена ранее, проставляем текущую
                        $descriptionDate = new \DateTime('now');
                    }
                    $nowDate = new \DateTime('now');
                    $diff = $descriptionDate->diff($nowDate);
                    if ($diff->format('%a') > 1) {
                        F::error('Нельзя отредактировать. Прошло больше 1 дня.');
                    } else {
                        // если все ок, то редактируем (только текст)
                        $movie->setDomainMovieDescription($description);
                    }
                }
                // обнуляем уникальность
                $movie->setDomainMovieTextRuDescriptionId(null);
                $movie->setDomainMovieTextRuResult(null);
                $movie->setDomainMovieTextRuUnique(null);
                $movie->setDomainMovieTextRuWideResult(null);
                // отправляем на модерацию, чтобы посмотреть, что он там наредактировал
                $movie->setDomainMovieShowToCustomer(false);
            } else {
                // если описание было написано другим рерайтером, то создаем новый объект и заполняем все необходимое
                $id = DomainMovie::add($movie->getId());
                $movie = new DomainMovie($id);
                $movie->setDomainMovieDescription($description);
                $movie->setDomainMovieDescriptionWriter($login);
                // $movie->setDomainMovieDescriptionPricePer1k($user->getRewritePricePer1k());
                $movie->setDomainMovieDescriptionDate((new \DateTime)->format('Y-m-d H:i:s'));
            }
        } else {
            // F::dump($user);
            // если мы здесь, значит описание не было написано ранее, проставляем все необходимое
            // F::dump(preg_replace('/ {2,}/', ' ', $description));
            $movie->setDomainMovieDescription($description);
            $movie->setDomainMovieDescriptionWriter($login);
            // $movie->setDomainMovieDescriptionPricePer1k($user->getRewritePricePer1k());
            $movie->setDomainMovieDescriptionDate((new \DateTime)->format('Y-m-d H:i:s'));
        }
        // если для пользователя проставлена премодерация, то ставим отметку не показывать фильм (описание) для покупателя
        if ($user->getPremoderation()) {
            $movie->setDomainMovieShowToCustomer(false);
        }
        // сразу проверяем уник биржи и пишем предупреждение, если скопировано
        // F::dump($movie->getDomainMovieDescriptionUnique());
        // if ($movie->getDomainMovieDescriptionUnique() < 20) {
        // 	F::alert('Похоже, что в скопировали текст, у нас нужно писать уникальные описания, вернитесь назад и напишите текст самостоятельно. <a href="#" onclick="history.go(-1); return false;">Вернуться к редактированию.</a>');
        // }
        $movie->setDomainMovieMovedToBottom(false);
        $movie->save($this->user->getLogin());
        if (empty($args['return_function'])) {
            F::redirect(F::referer_url());
        } else {
            $args['return_function']();
        }
    }

    public function setMenuVars($t = null, $active = null)
    {
        $arr = ['rewrite', 'rewrited', 'payment', 'base', 'rules', 'moderation'];
        foreach ($arr as $v) {
            $t->v('menu'.ucfirst($v).'Active', ($v == $active) ? 'active' : '');
        }
        $t->v('userLogin', $this->user->getLogin());
        $t->v('userRewriteBalance', $this->user->getRewriteBalance());
        //
        $cinema = new Cinema;
        $t->v('messageIfRewritePriceIncreased', $cinema->isRewritePriceIncreased() ? '<div class="alert alert-success" role="alert">
			<b>Внимание!</b> В данный момент оплата повышена до '.ceil($cinema->getRewritePricePer1k() * 1.2).' руб. за 1000 символов. Акция действует ограниченное время.
		</div>' : '');
        $t->v('rewritePricePer1k', $cinema->getRewritePricePer1k());
        $t->v('rewritePricePer1kNoSpaces', ceil($cinema->getRewritePricePer1k() * 1.2));
        $t->v('baseRows', $this->getBaseRows());
        //
        // считаем мои задания
        $movies = new Movies;
        $movies->setUseDomainMovies(true);
        $movies->setBookedForLogin($this->user->getLogin());
        $movies->setWithDomainDescription(false);
        // $movies->setDomainMovieWithCustomerEnoughBalance(true);
        $t->v('myRows', $movies->getRows());
        // считаем сколько одобренных заданий (чтобы решить, показывать ссылку на чат или нет)
        $movies = new Movies;
        $movies->setDescriptionWriter($this->user->getLogin());
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieShowToCustomer(true);
        $t->v('moderatedRows', F::cache($movies, 'getRows', null, 3600));
        if (in_array($this->user->getLevel(), ['admin', 'moderator'])) {
            // узнаем кол-во премодераций
            $movies = new Movies;
            $movies->setDomainMovieShowToCustomer(false);
            $movies->setWithDomainDescription(true);
            $movies->setDomainMovieRework(false);
            $t->v('moderationRows', $movies->getRows());
            $t->v('moderationButton', '{admin/moderator/menu_button_moderation.html}');
        } else {
            $t->v('moderationRows', '');
            $t->v('moderationButton', '');
        }
        // считаем сколько заданий было выполнено сегодня
        $today = new \DateTime;
        $yesterday = (new \DateTime)->modify('-1 day');
        $tomorrow = (new \DateTime)->modify('+1 day');
        // F::dump($beforeYesterday->format('Y-m-d'));
        $movies = new Movies;
        $movies->setWithDomainDescription(true);
        $movies->setWithBookedForLogin(true);
        $movies->setDescriptionDateFrom($today->format('Y-m-d'));
        $movies->setDescriptionDateTo($tomorrow->format('Y-m-d'));
        $t->v('todayDoneRows', F::cache($movies, 'getRows', null, 300));
        // считаем сколько заданий было выполнено вчера
        $movies = new Movies;
        $movies->setWithDomainDescription(true);
        $movies->setWithBookedForLogin(true);
        $movies->setDescriptionDateFrom($yesterday->format('Y-m-d'));
        $movies->setDescriptionDateTo($today->format('Y-m-d'));
        $t->v('yesterdayDoneRows', F::cache($movies, 'getRows', null, 300));
        $t->v('jsonShowAdult', json_encode($this->user->getAdult()));
        // блок с подтверждением почты
        $t->v('emailAlertBlock', $this->emailAlertBlock());
        $t->v('reworkAlertBlock', $this->reworkAlertBlock());
        // топ пользователей сегодня
        $arr = F::cache($this, 'getUserRatingArr', null, 120);
        $pos = count($arr);
        $top_limit = 10;
        $list = '';
        foreach ($arr as $k => $v) {
            if ($k < $top_limit) {
                if ($v['description_writer'] == $this->user->getLogin()) {
                    $style = 'style="font-weight: bold;"';
                } else {
                    $style = '';
                }
                $list .= '<li '.$style.'>'.($k + 1).'.&nbsp;'.$v['description_writer'].'</li>';
            }
            if ($v['description_writer'] == $this->user->getLogin()) {
                $pos = $k + 1;
            }
        }
        if ($pos > ($top_limit + 1)) {
            $list .= '<li>...</li>';

        }
        if ($pos > $top_limit) {
            $list .= '<li style="font-weight: bold;">'.$pos.'.&nbsp;'.$this->user->getLogin().'</li>';
        }
        if (empty($arr)) {
            $list = 'Никого нет.';
        }
        $t->v('userRatingList', $list);

        return true;
    }

    public function logout()
    {
        $this->auth->clearSID();
        F::redirect('/');
    }

    public function payment()
    {

        $user = UserEloquent::whereLogin($this->auth->getLogin())->firstOrFail();

        $transfers = $user->transfers()->orderByDesc('date')->simplePaginate(10);

        return view('types.cinema.admin.rewrite.payment')
            ->with('user', $user)
            ->with('transfers', $transfers);
    }

    public function setPaymentWallet()
    {

        request()->validate([
            'type' => 'required',
            'wallet' => 'nullable|string',
        ]);

        $type = request()->post('type');
        $wallet = request()->post('wallet');

        $user = UserEloquent::whereLogin($this->auth->getLogin())->firstOrFail();

        if (! $user->wallets->where('available', true)->where('type', $type)->first()) {
            throw new \Exception('Такого кошелька не существует');
        }

        foreach ($user->wallets as $w) {
            $user->{$w['type']} = null;
        }

        $user->{$type} = $wallet;
        $user->save();

        return redirect()->back();
    }

    public function rules()
    {
        $t = new Template('admin/rewrite/rules');
        $this->setMenuVars($t, 'rules');

        return $t->get();
    }

    public function privateMessageForWorker()
    {
        $message = null;
        if ($this->user->getLogin() === 'igor') {
            // $message = 'Игорь, не могу с вами связаться в телеграме. Обратите внимание, что у вас есть <a href="/?r=Worker/rewrited&rework=1">задание на доработке</a>.';
        }
        if ($this->user->getLogin() === 'olga.ch') {
            // $message = 'Ольга здравствуйте, не могу с вами связаться вконтакте. Часто приходится редактировать ваши описания. Пожалуйста, перечитывайте их перед публикацией и убирайте ошибки и "воду" (много повторений слов "очень", "является" и т.д.).';
        }
        if ($this->user->getLogin() === 'Paha90') {
            // $message = 'Paha90, пожалуйста проверьте свой Яндекс кошелек для оплаты. Не получается перевести вам деньги, пишет, что кошелек не существует.';
        }
        if ($this->user->getLogin() === 'viploiq') {
            // $message = 'Пожалуйста проверьте свои реквизиты для оплаты. Выбран сбербанк, но кошелек больше похож на вебмани.';
        }
        $content = $message ? ('<div class="alert alert-info">'.$message.'</div>') : '';

        return $content;
    }

    public function userInfo()
    {
        $code_sent = empty($_GET['code_sent']) ? false : true;
        $t = new Template('admin/rewrite/user-info');
        $t->v('userId', $this->user->getId());
        $t->v('userEmail', $this->user->getEmail());
        $t->v('userPassword', $this->user->getPassword());
        $t->v('jsonEmailConfirmed', json_encode($this->user->getEmailConfirmed()));
        // рефералы
        $orders = ['mr' => 'users.id desc', 'mp' => 'users.temp_balance desc'];
        $order = $_GET['order'] ?? 'mp';
        $users = new Users;
        $users->setPartner($this->user->getLogin());
        $users->setOrder($orders[$order]);
        $arr = $users->get();
        $list = '';
        foreach ($arr as $v) {
            $referal = new User($v['login']);
            $amount = round($referal->getTempBalance() * (new Cinema)->getRefWorker() / 100, 1);
            $list .= '<li>'.$v['login'].' <code>'.(($amount < 0) ? 0 : $amount).' руб.</code></li>';
        }
        $t->v('userReferralsList', $list ? ('<ol>'.$list.'</ol>') : 'список пуст');
        $t->v('userReferralsOrder', $order);
        $t->v('jsonCodeSent', json_encode($code_sent));
        F::setPageVars($t);
        $this->setMenuVars($t);

        return $t->get();
    }

    public function watch($args = [])
    {
        $domainMovieId = empty($_GET['id']) ? F::error('Specify domainMovieId') : F::checkstr($_GET['id']);
        $t = new Template('admin/rewrite/trailer');
        F::setPageVars($t);
        $this->setMenuVars($t);
        $movie = new DomainMovie($domainMovieId);
        $moonwalkId = Moonwalk::getBestTokenByUniqueId($movie->getUniqueId());
        if (! $moonwalkId) {
            F::alert('Просмотр недоступен');
        }
        $moonwalk = new Moonwalk($moonwalkId);
        $t->v('movieTitleRu', $moonwalk->getTitleRu());
        $t->v('moviePlayerMnwTrailer', $moonwalk->getTrailerPlayer());

        return $t->get();
    }

    public function returnMovieToRewriteBase()
    {
        $id = empty($_POST['domainMovieId']) ? F::error('Movie ID required') : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($id);
        if ($movie->getDomainMovieBookedForLogin() !== $this->user->getLogin()) {
            F::error('Это не ваше задание');
        }
        $movie->clearTask();
        $movie->save($this->user->getLogin());
        F::redirect(F::referer_url());
    }

    public function getBaseRows()
    {
        Engine::setDebug(false);
        // считаем сколько заданий в базе
        $movies = new Movies;
        $movies->setUseDomainMovies(true);
        $movies->setBookedForLogin('ozerin');
        $movies->setWithDomainDescription(false);
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        $movies->setUseDomainMovieCustomers(true);
        $movies->setExcludeDomainMoviesForWorker($this->user->getLogin());
        $movies->setExcludeExpressMovies(! $this->user->getExpressRewriter());
        $movies->setExcludeAdult(! $this->user->getAdult());

        return $movies->getRows();
    }

    public function submitShowAdult()
    {
        // F::dump($_POST);
        $showAdult = empty($_POST['showAdult']) ? false : ($_POST['showAdult'] ? true : false);
        $this->user->setAdult($showAdult);
        $this->user->save();
        F::redirect(F::referer_url());
    }

    public function confirmEmail($redirect_url = null)
    {
        parent::confirmEmail('/?r=Worker/userInfo');
    }

    public function sendEmailCode()
    {
        parent::sendEmailCode();
        $email = new Template('admin/rewrite/emails/confirm_email');
        $email->v('emailCode', $this->user->getEmailCode());
        $email->v('login', $this->user->getLogin());
        $email->v('password', $this->user->getPassword());
        (new Cinema)->sendMail($this->user->getEmail(), 'Кинотекст', 'Подтверждение почты', $email->get(), 'kinotext');
        F::redirect('/?r=Worker/userInfo&code_sent=1');
        // F::alert('Вам на почту отправлена ссылка для подтверждения почтового адреса, перейдите по ней.');
    }

    public function reworkAlertBlock()
    {
        // считаем кол-во доработок
        $movies = new Movies;
        $movies->setLimit(1);
        $movies->setPage(1);
        $movies->setDescriptionWriter($this->user->getLogin());
        $movies->setDomainMovieRework(true);
        if (! $movies->getRows()) {
            return;
        }
        $t = new Template('admin/rewrite/rework_alert_block');
        $t->v('reworkCount', $movies->getRows());

        return $t->get();
    }

    public function russianTestCheckTime()
    {
        $maxMinutes = 10;
        $nextTryTimeoutDays = 1;
        $currentDate = new \DateTime;
        if ($this->user->getTestPassed()) {
            F::alert('Вы уже прошли тест');
        }
        if (! $this->user->getTestStartDate()) {
            $this->user->setTestStartDate($currentDate->format('Y-m-d H:i:s'));
            $this->user->save();
        }
        $testStartDate = new \DateTime($this->user->getTestStartDate());
        $nextTryDate = (clone $testStartDate)->modify('+'.$nextTryTimeoutDays.' day');
        $testEndDate = (clone $testStartDate)->modify('+'.$maxMinutes.' minutes');
        // если установлена дата начала теста, то проверяем если не прошло maxMinutes, то ничего не делаем, если прошло maxMinutes и прошел таймаут, то ставим новую дату, если нет, то выводим ошибку
        if ($currentDate > $testEndDate) {
            // если дата начала теста больше чем nextTryDate, то обновляем дату и позволяем начать тест
            if ($currentDate > $nextTryDate) {
                $this->user->setTestStartDate($currentDate->format('Y-m-d H:i:s'));
                $this->user->save();
            } else {
                // отображаем уведомление, что следующая попытка возможна в такую-то дату
                $interval = $currentDate->diff($nextTryDate);
                F::alert('К сожалению вы не прошли тест. Вы сможете пройти его повторно через: '.$interval->h.' ч. '.$interval->i.' мин.');
            }
        }
    }

    public function russianTest()
    {
        $this->russianTestCheckTime();
        $arr = F::query_arr('
			select id, question
			from '.F::typetab('rus_test_questions').'
			where active
			order by rand()
			limit 15
			;');
        $list = '';
        foreach ($arr as $v) {
            $answers = F::query_arr('
				select id, answer
				from '.F::typetab('rus_test_answers').'
				where test_id = \''.$v['id'].'\'
				order by rand()
				;');
            $listAnswers = '';
            foreach ($answers as $answer) {
                // $listAnswers.= '<li>'.$answer['answer'].'</li>';
                $listAnswers .= '
				<input type="radio" id="answer'.$answer['id'].'" name="question['.$v['id'].']" value="'.$answer['id'].'">
				<label for="answer'.$answer['id'].'">'.$answer['answer'].'</label>
				<br>';
            }
            // $list.= '<li><p>'.$v['question'].'</p><ol>'.$listAnswers.'</ol></li>';
            $list .= '<li style="margin-bottom: 20px;">
			<div class="card">
			<h5 class="card-header">'.$v['question'].'</h5>
			<div class="card-body">
			<ol>'.$listAnswers.'</ol>
			</div>
			</div>
			</li>
			';
        }
        $t = new Template('admin/rewrite/russian_test');
        $t->v('testList', '<ol>'.$list.'</ol>');
        $testStartDate = new \DateTime($this->user->getTestStartDate());
        $maxMinutes = 10;
        $testEndDate = (clone $testStartDate)->modify('+'.$maxMinutes.' minutes');
        $currentDate = new \DateTime;
        $interval = $currentDate->diff($testEndDate);
        if ($interval->invert) {
            $t->v('testEndIntervalSeconds', 0);
        } else {
            $t->v('testEndIntervalSeconds', $interval->i * 60 + $interval->s);
        }
        // F::dump($testEndDate->format('Y-m-d H:i:s'));
        F::setPageVars($t);
        $this->setMenuVars($t);

        return $t->get();
    }

    public function submitRussianTest()
    {
        $this->russianTestCheckTime();
        $questions = empty($_POST['question']) ? F::error('Ответы не переданы') : $_POST['question'];
        $correctCount = 0;
        $limit = 15;
        foreach ($questions as $v) {
            $correct = F::query_assoc('
				select answer,correct
				from '.F::typetab('rus_test_answers').'
				where id = \''.F::escape_string($v).'\'
				;');
            if ($correct['correct']) {
                $correctCount++;
            }
        }
        $correctPercent = $correctCount / $limit * 100;
        if ($correctPercent > 70) {
            $this->user->setTestPassed(true);
            $this->user->save();
            F::alert('Поздравляем, вы прошли тест. Вы можете <a href="/?r=Worker/base">перейти к выполнению заданий</a>.');
        } else {
            // меняем дату начала теста на более раннюю, чтобы не было возможности использовать оставшееся время и вернуться к выполнению
            $nextTryTimeoutDays = 1;
            $maxMinutes = 10;
            $currentDate = new \DateTime;
            $testStartDate = (clone $currentDate)->modify('-'.$maxMinutes.' minutes');
            $this->user->setTestStartDate($testStartDate->format('Y-m-d H:i:s'));
            $this->user->save();
            F::alert('К сожалению вы не прошли тестирование. Вы можете повторить попытку через '.$nextTryTimeoutDays.' день.');
        }
        // F::dump($correctPercent);
    }

    public function getUser()
    {
        return $this->user;
    }
}
