<?php

namespace cinema\app\controllers;

use cinema\app\models\DomainMovie;
use cinema\app\models\Movies;
use engine\app\models\F;
use engine\app\models\PaginatorCustom;
use engine\app\models\Template;

class YoutubeController extends MainController
{
    public function getVideos($vars = [])
    {
        $movies = new Movies;
        $movies->setDomain($this->domain->getDomain());
        $movies->setIncludeMovies(false);
        $movies->setIncludeSerials(false);
        $movies->setIncludeTrailers(false);
        $movies->setIncludeGames(false);
        $movies->setIncludeYoutube(true);
        $movies->setLimit(12);
        $movies->setOrder('cinema.add_date desc');
        $arr = $movies->get();
        $list = '';
        foreach ($arr as $v) {
            $video = new DomainMovie($v['domain_movies_id']);
            $youtube = $video->getYoutubeData();
            // F::dump($data);
            $t = new Template($vars['template']);
            $t->v('video_id', $video->getDomainMovieId());
            $t->v('youtube_id', $youtube->getId());
            $t->v('video_thumbnail', $youtube->getData()->snippet->thumbnails->medium->url);
            $t->v('video_title', $youtube->getData()->snippet->title);
            // F::dump($video->data->contentDetails->duration);
            $t->v('video_duration', (new \DateInterval($youtube->getData()->contentDetails->duration))->format('%h:%I:%S'));
            $t->v('video_link', htmlspecialchars($this->getVideoLink($video)));
            $list .= $t->get();
        }

        // F::pre($list);
        return $list;
    }

    public function getCategories($vars = [])
    {
        $vars['template'] = $vars['template'] ?? F::error('Template required');
        $arr = F::query_arr('select id
			from '.F::typetab('youtube_categories').'
			;');
        $list = '';
        foreach ($arr as $v) {
            $category = $this->getCategory($v['id']);
            // F::dump($data);
            $t = new Template($vars['template']);
            $t->v('category_id', $category->id);
            $t->v('category_title', $category->data->snippet->title);
            $t->v('category_link', htmlspecialchars($category->link));
            $list .= $t->get();
        }

        // F::pre($list);
        return $list;
    }

    public function categories()
    {
        $t = new Template('categories');
        $t->v('categories', $this->getCategories(['template' => 'items/thumb_category']));

        return $t->get();
    }

    public function watch($args = [])
    {
        // F::dump($args);
        $domainMovieId = empty($args['domainMovieId']) ? F::error('DomainMovieId required') : F::checkstr($args['domainMovieId']);
        $video = new DomainMovie($domainMovieId);
        if ($video->getDomain() !== $this->domain->getDomain()) {
            F::error('Video is not found');
        }
        // F::dump($youtube);
        $t = $this->template;
        $t->v('video_id', $video->getDomainMovieId());
        $t->v('video_watch_link', htmlspecialchars($this->getVideoLink($video)));
        $category = $this->getCategory($youtube->getData()->snippet->categoryId);
        $t->v('video_category_id', $category->id);
        $t->v('video_category_title', $category->data->snippet->title);
        $t->v('video_category_link', $category->link);
        $t->v('sameVideos', $this->getVideos(['template' => 'items/thumb']));
        $t->v('nextVideos', $this->getVideos(['template' => 'items/next_thumb']));
        $t->v('categories', $this->getCategories(['template' => 'items/menu_category']));
        $t->v('canonical_link', '{https}{currentDomain}{video_watch_link}');
        F::setPageVars($t);
        $this->setPageVars($t);

        return $t->get();
    }

    public function category()
    {
        $limit = 200;
        $category_id = empty($_GET['category_id']) ? F::error('Category ID required') : F::escape_string($category_id);
        $page = empty($_GET['page']) ? 1 : F::escape_string($page);
        $offset = ($page - 1) * $limit;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS id
			from '.F::typetab('youtube_videos').'
			where
			category_id = \''.$category_id.'\'
			order by date desc
			limit '.$offset.','.$limit.'
			;');
        foreach ($arr as $v) {
            $video = $this->getVideo($v['id']);
            // $
        }
        $Paginator = new PaginatorCustom;
        // $Paginator->setLang($this->getLang());
        $Paginator->setUrlPattern('/?r=Youtube/category&id='.$category_id.'&page='.$page);
        $Paginator->setTemplateList('items/pagination/list');
        $Paginator->setTemplatePrev('items/pagination/prev');
        $Paginator->setTemplateNext('items/pagination/next');
        $Paginator->setRows($rows);
        $Paginator->setLimit($limit);
        $Paginator->setPage($page);
        $Paginator->setMaxPagesToShow(7);
        $t->v('categoryPages', $Paginator->get());
    }
}
