<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use engine\app\controllers\AuthorizationController;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class CinemaAuthorizationController extends AuthorizationController
{
    public function __construct($levels = [])
    {
        parent::__construct('cinema', $levels);
    }

    public function showForm($t = null)
    {
        if (! $t) {
            $t = new Template('authorization_form');
        }
        $partner = empty($_GET['p']) ? null : F::checkstr($_GET['p']);
        if ($partner) {
            // устанавливаем куки с ID партнера на неделю
            setcookie('mgw_partner_'.Engine::getType(), $partner, time() + 3600 * 24 * 7);
        }
        $cinema = new Cinema;
        $t->v('customerPricePer1k', $cinema->getCustomerPricePer1k());
        $t->v('rewritePricePer1kNoSpaces', ceil($cinema->getRewritePricePer1k() * 1.2));
        parent::showForm($t);
    }
}
