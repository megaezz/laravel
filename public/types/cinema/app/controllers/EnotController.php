<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\eloquent\User as UserEloquent;
use cinema\app\models\User;
use engine\app\models\Engine;
use engine\app\models\Enot;
use engine\app\models\F;
use engine\app\models\Template;

class EnotController
{
    private const SECRET_1 = 'MPBM8RbzPloRVGCVrenqsFXnjOp8D5DC';

    private const SECRET_2 = 'OfmCkv1gMO5M6Lhtv7SC4T8wfSEWk81i';

    private const MERCHANT_ID = 50929;

    public function getSignature()
    {
        Engine::setDebug(false);
        $sum = $_POST['oa'] ?? F::error('Не передана сумма');
        $order_id = $_POST['o'] ?? F::error('Не передан заказ');
        $str = self::MERCHANT_ID.':'.$sum.':'.self::SECRET_1.':'.$order_id;
        // F::dump($str);
        $sign = md5($str);

        return $sign;
    }

    public function handler()
    {
        $sum = $_REQUEST['amount'] ?? F::error('Не передана сумма');
        $order_id = $_REQUEST['merchant_id'] ?? F::error('Не передан ID заказа');
        $merchant_id = $_REQUEST['merchant'] ?? F::error('Не передан ID магазина');
        $income_sign = $_REQUEST['sign_2'] ?? F::error('Не передана подпись');
        $str = self::MERCHANT_ID.':'.$sum.':'.self::SECRET_2.':'.$order_id;
        $sign = md5($str);
        if ($sign != $income_sign) {
            logger()->info('wrong sign');
            exit('wrong sign');
        }
        // F::dump('YES');
        // если все ок, зачисляем sum
        $user_id = Enot::getFromTimeInt($order_id);
        $userEloquent = UserEloquent::find($user_id);
        if ($userEloquent) {
            $user = new User($userEloquent->login);
        } else {
            F::error('Пользователь не найден');
        }
        $user->addTransfer($sum, 'Депозит Enot');
        $user->save();
        if ($user->getPartner()) {
            $partner = new User($user->getPartner());
            $partner->addTransfer($sum * (new Cinema)->getRefCustomer() / 100, 'Начисление за реферала '.$user->getLogin());
        }
        // отправляем письмо
        if ($user->getEmail() and $user->getEmailConfirmed()) {
            $email = new Template('admin/customer/emails/deposit');
            $email->v('login', $user->getLogin());
            $email->v('amount', $sum);
            (new Cinema)->sendMail($user->getEmail(), 'Киноконтент', 'Депозит', $email->get(), 'kinocontent');
        }
        logger()->info('Enot: Оплата '.($sum).' руб. зачтена для аккаунта '.$user->getLogin());

        return 'YES';
    }
}
