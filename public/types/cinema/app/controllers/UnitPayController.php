<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\User;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class UnitPayController
{
    // private const PROJECT_ID = 163001;
    // private const SECRET_KEY = '44ccc262e6385aea04e0ffa325b14d33';
    // private const PUBLIC_ID = '163001-0aaef';
    private const PROJECT_ID = 431981;

    private const SECRET_KEY = '53de18afece5c505781ae98492181c8a';

    private const PUBLIC_ID = '431981-75529';

    public function test()
    {
        // Project Data
        $secretKey = '9e977d0c0e1bc8f5cc9775a8cc8744f1';
        $publicId = '15155-ae12d';

        // My item Info
        $itemName = 'Iphone 6 Skin Cover';

        // My Order Data
        $orderId = 'a183f94-1434-1e44';
        $orderSum = 900;
        $orderDesc = 'Payment for item "'.$itemName.'"';
        $orderCurrency = 'RUB';

        $unitPay = new \UnitPay('unitpay.ru', $secretKey);

        $unitPay
            ->setBackUrl('http://domain.com')
            ->setCustomerEmail('customer@domain.com')
            ->setCustomerPhone('79001235555')
            ->setCashItems([
                new \CashItem($itemName, 1, $orderSum),
            ]);

        $redirectUrl = $unitPay->form(
            $publicId,
            $orderSum,
            $orderId,
            $orderDesc,
            $orderCurrency
        );

        header('Location: '.$redirectUrl);

        return '';
    }

    public function handler()
    {
        $unitPay = new \UnitPay('unitpay.ru', self::SECRET_KEY);
        try {
            // Validate request (check ip address, signature and etc)
            $unitPay->checkHandlerRequest();
            [$method, $params] = [$_GET['method'], $_GET['params']];

            // Very important! Validate request with your order data, before complete order
            if (
                $params['projectId'] != self::PROJECT_ID or
                $params['orderCurrency'] != 'RUB'
            ) {
                // logging data and throw exception
                throw new InvalidArgumentException('Order validation Error!');
            }

            switch ($method) {
                // Just check order (check server status, check order in DB and etc)
                case 'check':
                    print $unitPay->getSuccessHandlerResponse('Check Success. Ready to pay.');
                    break;
                    // Method Pay means that the money received
                case 'pay':
                    // Please complete order
                    // если все ок, зачисляем orderSum
                    $sum = $params['orderSum'];
                    $user = new User($params['account']);
                    // $user->setDeposited($user->getDeposited() + $sum);
                    $user->addTransfer($sum, 'Депозит UnitPay');
                    $user->save();
                    if ($user->getPartner()) {
                        $partner = new User($user->getPartner());
                        $partner->addTransfer($sum * (new Cinema)->getRefCustomer() / 100, 'Начисление за реферала '.$user->getLogin());
                    }
                    // отправляем письмо
                    if ($user->getEmail() and $user->getEmailConfirmed()) {
                        $email = new Template('admin/customer/emails/deposit');
                        $email->v('login', $user->getLogin());
                        $email->v('amount', $sum);
                        (new Cinema)->sendMail($user->getEmail(), 'Киноконтент', 'Депозит', $email->get(), 'kinocontent');
                    }
                    logger()->info('UnitPay: Оплата '.($sum).' руб. зачтена для аккаунта '.$params['account']);
                    echo $unitPay->getSuccessHandlerResponse('Pay Success');
                    break;
                    // Method Error means that an error has occurred.
                case 'error':
                    // Please log error text.
                    print $unitPay->getSuccessHandlerResponse('Error logged');
                    break;
                    // Method Refund means that the money returned to the client
                case 'refund':
                    // Please cancel the order
                    print $unitPay->getSuccessHandlerResponse('Order canceled');
                    break;
            }
            // Oops! Something went wrong.
        } catch (Exception $e) {
            echo $unitPay->getErrorHandlerResponse($e->getMessage());
        }

        return '';
    }

    public function getSignature()
    {
        Engine::setDebug(false);
        $account = $_POST['account'] ?? F::error('Не передан аккаунт');
        $desc = $_POST['desc'] ?? F::error('Не передано описание');
        $sum = $_POST['sum'] ?? F::error('Не передана сумма');

        return hash('sha256', $account.'{up}'.$desc.'{up}'.$sum.'{up}'.self::SECRET_KEY);
    }
}
