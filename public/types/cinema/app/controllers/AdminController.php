<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\Domain;
use cinema\app\models\DomainMovie;
use cinema\app\models\Domains;
use cinema\app\models\Group;
use cinema\app\models\Groups;
use cinema\app\models\Kodiks;
use cinema\app\models\Moonwalk;
use cinema\app\models\Moonwalks;
use cinema\app\models\Movies;
use cinema\app\models\Tag;
use cinema\app\models\User;
use cinema\app\models\Users;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use engine\app\models\TextRu;

class AdminController extends ModeratorController
{
    // private $auth = null;
    // private $user = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth = new CinemaAuthorizationController(['admin']);
        // $this->user = new User($this->auth->getLogin());
        // выставляем временную зону если нужно
        if ($this->user->getTimezone()) {
            // F::query('SET time_zone = \''.$this->user->getTimeZone().'\';');
            // закоментировал т.к. не могу преобразовать +01:00 в +1
            // date_default_timezone_set('Etc/GMT+1');
        }
    }

    public function updateRawg()
    {
        $limit = empty($_GET['limit']) ? 100 : F::checkstr($_GET['limit']);
        $api = new \Swagger\Client\Api\GamesApi(new \GuzzleHttp\Client);
        $arr = F::query_arr('
				select SQL_CALC_FOUND_ROWS id,json
				from '.F::typetab('rawg').'
				where checked = 0
				limit '.$limit.'
				;');
        $rows = F::rows_without_limit();
        $list = '';
        foreach ($arr as $v) {
            $game = $api->gamesRead($v['id']);
            // F::dump($game);
            F::query('
				update '.F::typetab('rawg').' set
				data_single = \''.F::escape_string($game).'\',
				checked = 1
				where id = \''.$v['id'].'\'
				;');
            $list .= '<li>#'.$v['id'].' обновлено</li>';
        }
        F::alert('
			<p>Всего: '.$rows.'</p>
			<p>Лимит: '.$limit.'</p>
			<ol>'.$list.'</ol>
			<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>
			');
    }

    public function testRawg()
    {
        $page = empty($_GET['p']) ? 1 : F::checkstr($_GET['p']);
        $api = new \Swagger\Client\Api\GamesApi(new \GuzzleHttp\Client);
        $limit = 40;
        // $arr = $api->gamesRead(3498);
        // F::dump($arr);
        $arr = $api->gamesList($page, $limit);
        foreach ($arr['results'] as $v) {
            // F::dump($v->getId());
            F::query('
				insert into '.F::typetab('rawg').'
				set id = \''.$v->getId().'\',
				json = \''.F::escape_string($v).'\'
				on duplicate key update
				json = \''.F::escape_string($v).'\'
				;');
        }
        F::alert($page.' из '.ceil($arr['count'] / $limit).'
			<script>
			setTimeout(
			window.location.replace(\'/?r=Admin/testRawg&p='.($page + 1).'\'),
			5000);
			</script>
			');
        F::dump($arr);
        F::dump($api);
    }

    public function fixTagNames()
    {
        // $str = 'РђР»РµРєСЃР°РЅРґСЂ РџРµС‚СЂРѕРІ';
        $arr = F::query_arr('
			select id
			from '.F::typetab('tags').'
			where tag like \'%µ%\'
			limit 500
			');
        foreach ($arr as $v) {
            $tag = new Tag($v['id']);
            // F::dump($tag);
            $fixed = mb_convert_encoding($tag->getName(), 'windows-1251');
            F::dump($tag->getId().' '.$fixed);
            $existed = Tag::getIdByName($fixed);
            if ($existed) {
                F::query('update IGNORE '.F::typetab('cinema_tags').'
					set tag_id = \''.$existed.'\'
					where tag_id = \''.$tag->getId().'\'
					;');
                F::query('delete from
					'.F::typetab('cinema_tags').'
					where tag_id = \''.$tag->getId().'\'
					;');
                F::query('delete from '.F::typetab('tags').'
					where id = \''.$tag->getId().'\'
					;');
            } else {
                $tag->setName($fixed);
                $tag->save();
            }
        }
        F::alert('done '.count($arr));
        F::dump($str);
        F::dump(mb_convert_encoding($str, 'windows-1251'));
    }

    public function groups()
    {
        $groups = new Groups;
        $arr = $groups->get();
        $list = '';
        foreach ($arr as $v) {
            $group = new Group($v['id']);
            $arrTags = $group->getTags();
            // asort($arrTags);
            $listTags = '';
            foreach ($arrTags as $vTag) {
                $tag = new Tag($vTag);
                $listTags .= '<li>'.$tag->getName().' #'.$tag->getId().' ('.$tag->getType().')</li>';
            }
            $tGroupList = new Template('admin/groups/list-group');
            $tGroupList->v('groupId', $group->getId());
            $tGroupList->v('groupName', $group->getName());
            $tGroupList->v('groupTagsList', $listTags);
            $list .= $tGroupList->get();
        }
        $t = new Template('admin/groups/index');
        $t->v('groupsList', $list);
        $this->setMenuVars($t);
        F::setPageVars($t);

        return $t->get();
    }

    public function rewrited($args = [])
    {
        $page = empty($_GET['p']) ? 1 : F::checkstr($_GET['p']);
        $movieId = empty($_GET['movieId']) ? null : F::checkstr($_GET['movieId']);
        $domainMovieId = empty($_GET['domainMovieId']) ? null : F::checkstr($_GET['domainMovieId']);
        $descriptionWriter = empty($_GET['descriptionWriter']) ? null : F::checkstr($_GET['descriptionWriter']);
        $customer = empty($_GET['customer']) ? null : F::checkstr($_GET['customer']);
        $showToCustomer = isset($_GET['showToCustomer']) ? (($_GET['showToCustomer'] == '') ? null : ($_GET['showToCustomer'] ? true : false)) : null;
        $withDomainDescription = isset($_GET['withDomainDescription']) ? (($_GET['withDomainDescription'] == '') ? null : $_GET['withDomainDescription']) : null;
        $active = isset($_GET['active']) ? (($_GET['active'] == '') ? null : $_GET['active']) : null;
        $rework = isset($_GET['rework']) ? (($_GET['rework'] == '') ? null : $_GET['rework']) : null;
        $bookedForLogin = isset($_GET['bookedForLogin']) ? (($_GET['bookedForLogin'] == '') ? null : $_GET['bookedForLogin']) : null;
        $customerIsEnoughBalance = isset($_GET['customerIsEnoughBalance']) ? (($_GET['customerIsEnoughBalance'] == '') ? null : $_GET['customerIsEnoughBalance']) : null;
        $btnActive = empty($_GET['btnActive']) ? null : $_GET['btnActive'];
        $writing = isset($_GET['writing']) ? (($_GET['writing'] == '') ? null : $_GET['writing']) : null;
        $order = isset($_GET['order']) ? (($_GET['order'] == '') ? null : $_GET['order']) : null;
        $withDomainMovieCustomerComment = isset($_GET['withDomainMovieCustomerComment']) ? (($_GET['withDomainMovieCustomerComment'] == '') ? null : $_GET['withDomainMovieCustomerComment']) : null;
        $orders = [
            'addDateDesc' => 'domain_movies.add_date desc',
            'descriptionDateDesc' => 'domain_movies.description_date desc',
            'descriptionDate' => 'domain_movies.description_date',
            'customerFirstAndAddDate' => 'domain_movies.description_price_per_1k desc,domain_movies.to_top desc, isnull(domain_movies.customer),
			domain_movies.to_bottom,
			rand(),
			domain_movies.add_date',
        ];
        $orderStr = $order ? (isset($orders[$order]) ? $orders[$order] : F::error('Order name doesn\'t exist')) : null;
        // F::dump($withDomainDescription);
        $t = new Template('admin/rewrited');
        $t->v('btnPremoderationActive', ($btnActive === 'premoderation') ? 'active' : '');
        $t->v('btnReworkActive', ($btnActive === 'rework') ? 'active' : '');
        $t->v('btnBaseQueueActive', ($btnActive === 'baseQueue') ? 'active' : '');
        $t->v('btnWritingActive', ($btnActive === 'writing') ? 'active' : '');
        $t->v('btnCustomerCommentActive', ($btnActive === 'customerComment') ? 'active' : '');
        $t->v('btnModeratedActive', ($btnActive === 'moderated') ? 'active' : '');
        // чтобы узнать кол-во премодераций
        $movies = new Movies;
        $movies->setDomainMovieShowToCustomer(false);
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieRework(false);
        $t->v('hidden_from_customer_rows', $movies->getRows());
        // $t->v('hidden_from_customer_rows',F::cache($movies,'getRows',null,30));
        // чтобы узнать кол-во на доработку
        $movies = new Movies;
        $movies->setDomainMovieRework(true);
        $t->v('rework_rows', $movies->getRows());
        // $t->v('rework_rows',F::cache($movies,'getRows',null,30));
        // чтобы узнать кол-во фильмов в очереди на рерайт
        $movies = new Movies;
        $movies->setBookedForLogin('ozerin');
        $movies->setWithDomainDescription(false);
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        $t->v('base_rows', $movies->getRows());
        // $t->v('base_rows',F::cache($movies,'getRows',null,30));
        // чтобы узнать кол-во фильмов которые пишут
        $movies = new Movies;
        $movies->setDomainMovieWriting(true);
        $t->v('writing_rows', $movies->getRows());
        // $t->v('writing_rows',F::cache($movies,'getRows',null,30));
        // чтобы узнать кол-во фильмов с комментариями заказчика
        // $movies = new Movies;
        // $movies->setWithDomainMovieCustomerComment(true);
        // $t->v('customer_comment_rows',$movies->getRows());
        $t->v('customer_comment_rows', '?');
        // основная выдача
        $movies = new Movies;
        $movies->setUseDomainMovies(true);
        $movies->setLimit(10);
        $movies->setPage($page);
        $movies->setWithDomainDescription($withDomainDescription);
        $movies->setDomainMovieActive($active);
        $movies->setDomainMovieRework($rework);
        $movies->setDomainMovieShowToCustomer($showToCustomer);
        $movies->setOrder($orderStr);
        $movies->setDescriptionWriter($descriptionWriter);
        $movies->setDomainMovieCustomer($customer);
        $movies->setBookedForLogin($bookedForLogin);
        $movies->setDomainMovieWithCustomerEnoughBalance($customerIsEnoughBalance);
        $movies->setDomainMovieWriting($writing);
        $movies->setWithDomainMovieCustomerComment($withDomainMovieCustomerComment);
        if ($movieId) {
            $movies->setDomain('turboserial.com');
            $movies->setMovieId($movieId);
        }
        if ($domainMovieId) {
            $movies->setDomainMovieId($domainMovieId);
        }
        // F::dump($movies);
        // $movies->setWithPoster(true);
        // $movies->setIncludeTrailers(false);
        // $movies->setListTemplate('admin/item_rewrited');
        $movies->setPaginationUrlPattern('/?r=Admin/rewrited&amp;p=(:num)&amp;descriptionWriter='.$descriptionWriter.'&amp;withDomainDescription='.$withDomainDescription.'&amp;showToCustomer='.(is_null($showToCustomer) ? '' : ($showToCustomer ? 1 : 0)).'&amp;active='.$active.'&amp;rework='.$rework.'&amp;bookedForLogin='.$bookedForLogin.'&amp;customerIsEnoughBalance='.$customerIsEnoughBalance.'&amp;order='.$order.'&amp;btnActive='.$btnActive.'&amp;writing='.$writing.'&amp;withDomainMovieCustomerComment='.$withDomainMovieCustomerComment.'&amp;customer='.$customer);
        $maxBookMinutes = (new Cinema)->getBookMinutes();
        $list = '';
        foreach ($movies->get() as $v) {
            $l = new Template('admin/item_rewrited');
            // F::dump($v);
            $domainMovie = new DomainMovie($v['domain_movies_id']);
            $textRu = $domainMovie->getDomainMovieTextRuResult();
            $textRuWide = $domainMovie->getDomainMovieTextRuWideResult();
            if ($textRu) {
                if (isset($textRuWide->seo_check)) {
                    $seo_check = json_decode($textRuWide->seo_check);
                    // F::dump($seo_check->spam_percent);
                    $water = isset($seo_check->water_percent) ? $seo_check->water_percent : null;
                    $spam = isset($seo_check->spam_percent) ? $seo_check->spam_percent : null;
                } else {
                    $water = null;
                    $spam = null;
                }
                $l->v('unique', $textRu->unique.'%'.(isset($water) ? (', вода: '.$water.'%') : '').(isset($spam) ? (', спам: '.$spam.'%') : ''));
                $plagiatUrls = TextRu::getPlagiatUrls($textRu);
                $l->vf('plagiatUrls', $plagiatUrls ? '<ol>'.$plagiatUrls.'</ol>' : '');
            } else {
                $l->v('plagiatUrls', '');
                if ($domainMovie->getDomainMovieTextRuDescriptionId()) {
                    $l->v('unique', 'ожидание');
                } else {
                    $l->v('unique', 'нет данных');
                }
            }
            $customerComment = '';
            $symbolsComment = '';
            $symbolsComment = $domainMovie->getDomainMovieSymbolsFrom() ? ('от '.$domainMovie->getDomainMovieSymbolsFrom()) : '';
            $symbolsComment .= $domainMovie->getDomainMovieSymbolsTo() ? (' до '.$domainMovie->getDomainMovieSymbolsTo()) : '';
            if ($symbolsComment) {
                $customerComment = '<b>Требуемое количество символов:</b> '.$symbolsComment;
            }
            $l->v('customerComment', $customerComment);
            $l->v('domainMovieCustomer', $domainMovie->getDomainMovieCustomer());
            $l->v('domainMovieDescriptionWorkerPrice', $domainMovie->getDomainMovieDescriptionWorkerPrice());
            $l->v('domainMovieBookedForLogin', $domainMovie->getDomainMovieBookedForLogin());
            $l->v('domainMovieDescriptionWriter', $domainMovie->getDomainMovieDescriptionWriter());
            $l->v('domainMovieDescriptionDate', $domainMovie->getDomainMovieDescriptionDate());
            $l->v('domainMovieAddDate', $domainMovie->getDomainMovieAddDate());
            $l->v('domainMovieDescription', $domainMovie->getDomainMovieDescription());
            $l->v('description', $domainMovie->getDescription());
            // $l->v('description',$domainMovie->getDescription()?$domainMovie->getDescription():$domainMovie->getRandAltDescription());
            $l->v('titleRu', $domainMovie->getTitleRu());
            $l->v('domainMovieId', $domainMovie->getDomainMovieId());
            $l->v('poster', $domainMovie->getPosterOrPlaceholder());
            $l->v('domain', $domainMovie->getDomain());
            $l->v('classActiveIfHiddenFromCustomer', $domainMovie->getDomainMovieShowToCustomer() ? '' : 'active');
            $l->v('domainMovieReworkComment', $domainMovie->getDomainMovieRework() ? ('<b>Требуется доработка:</b> '.nl2br($domainMovie->getDomainMovieReworkComment())) : ($domainMovie->getDomainMovieReworkComment() ? ('<b>Требовалась доработка:</b> '.nl2br($domainMovie->getDomainMovieReworkComment())) : ''));
            $l->v('classActiveIfMovedToTop', $domainMovie->getDomainMovieMovedToTop() ? 'active' : '');
            $l->v('classActiveIfWriting', $domainMovie->getDomainMovieWriting() ? 'active' : '');
            $l->v('classActiveIfRework', $domainMovie->getDomainMovieRework() ? 'active' : '');
            // $l->v('domainMovieReworkComment',$domainMovie->getDomainMovieRework()?('<b>Комментарий заказчика:</b> '.$domainMovie->getDomainMovieReworkComment()):'');
            if ($domainMovie->getUniqueId() == 'not_in_base') {
                $l->v('altDescriptionsLink', '');
                $l->v('altDescriptions', '');
                $l->v('type', 'тип неизвестен');
                $l->v('year', 'год неизвестен');
            } else {
                $altMovies = $domainMovie->getDomainMovieAltDescriptions();
                $altMovies->setLimit($altMovies->getRows());
                $altMovies->setListTemplate('admin/rewrite/alt_description_item');
                $l->v('altDescriptionsLink', F::cache($altMovies, 'getRows') ? ('Есть альтернативные описания: '.F::cache($altMovies, 'getRows').' шт.') : '');
                $l->v('altDescriptions', F::cache($altMovies, 'getList'));
                $l->v('type', $domainMovie->getTypeRu(true));
                $l->v('year', $domainMovie->getYear());
            }
            $l->v('domainMovieCustomerComment', $domainMovie->getDomainMovieCustomerComment() ? ('<p><b>Комментарий для исполнителя:</b> '.nl2br(htmlspecialchars($domainMovie->getDomainMovieCustomerComment())).'</p>') : '');
            $l->v('domainMoviePrivateComment', $domainMovie->getDomainMoviePrivateComment() ? ('<p><b>Комментарий для себя:</b> '.nl2br($domainMovie->getDomainMoviePrivateComment()).'</p>') : '');
            $l->v('domainMovieDmca', $domainMovie->getDomainMovieDmca());
            // если нет описания, то выводим информацию о том когда будет снята бронь
            if (! $domainMovie->getDomainMovieDescription()) {
                $bookDiff = ceil(((new \DateTime)->getTimestamp() - (new \DateTime($domainMovie->getDomainMovieBookDate()))->getTimestamp()) / 60);
                $l->v('bookTimeAlert', (($maxBookMinutes - $bookDiff) < 60) ? ('<span style="color: red;">Осталось '.($maxBookMinutes - $bookDiff).' минут на выполнение этого задания</span>') : '');
            } else {
                $l->v('bookTimeAlert', '');
            }
            $l->v('classActiveIfDoRedirect', $domainMovie->getDomainMovieDoRedirect() ? 'active' : '');
            $l->v('classActiveIfPlayerDeleted', $domainMovie->getDomainMovieDeleted() ? 'active' : '');
            // $domainMovieDescriptionUnique = F::cache($domainMovie,'getDomainMovieDescriptionUnique');
            $domainMovieDescriptionUnique = F::cache($domainMovie, 'getDomainMovieDescriptionUnique', null, 600);
            $l->v('domainMovieDescriptionUnique', is_null($domainMovieDescriptionUnique) ? 'нет данных' : (floor($domainMovieDescriptionUnique).'%'));
            $l->v('domainMovieTextRuDescriptionId', $domainMovie->getDomainMovieTextRuDescriptionId());
            $l->v('domainMovieDescriptionPricePer1k', $domainMovie->getDomainMovieDescriptionPricePer1k());
            $l->v('domainMovieCustomerPricePer1k', $domainMovie->getDomainMovieCustomerPricePer1k());
            $l->v('domainMovieIsExpress', $domainMovie->getDomainMovieIsExpress() ? 1 : 0);
            if ($showToCustomer === false) {
                // если это страница модерации, узнаем, сколько еще на модерации текстов у этого исполнителя
                $rewriterMovies = new Movies;
                $rewriterMovies->setDomainMovieShowToCustomer(false);
                $rewriterMovies->setDomainMovieRework(false);
                $rewriterMovies->setWithDomainDescription(true);
                $rewriterMovies->setDescriptionWriter($domainMovie->getDomainMovieDescriptionWriter());
                $l->v('rewriterModerationRows', F::cache($rewriterMovies, 'getRows', null, 0).' шт.');
            } else {
                $l->v('rewriterModerationRows', '');
            }
            $reworkMinutes = $domainMovie->getReworkMinutes();
            $executionMinutes = $domainMovie->getExecutionMinutes();
            $fromConfirmMinutes = $domainMovie->getMinutesFromConfirm();
            $l->v('reworkHours', $reworkMinutes ? (ceil($reworkMinutes / 60).' ч.') : 'нет данных');
            $l->v('fromConfirmHours', $fromConfirmMinutes ? (ceil($fromConfirmMinutes / 60).' ч.') : 'нет данных');
            $l->v('executionHours', $executionMinutes ? (ceil($executionMinutes / 60).' ч.') : 'нет данных');
            if ($domainMovie->getDomainMovieDescriptionWriter()) {
                $descriptionWriter = new User($domainMovie->getDomainMovieDescriptionWriter());
                $l->v('jsonUserRestricted', json_encode($descriptionWriter->getRestricted()));
            } else {
                $l->v('jsonUserRestricted', '');
            }
            // F::dump(ceil($domainMovie->getExecutionMinutes() / 60));
            $list .= $l->get();
        }
        // $t->v('cinemaList',$movies->getList());
        $t->v('cinemaList', $list);
        $t->v('cinemaPages', $movies->getPages());
        $t->v('cinemaRows', $movies->getRows());
        $t->v('rewriteBalance', $this->user->getRewriteBalance());
        $t->v('getMovieId', empty($_GET['movieId']) ? '' : F::checkstr($_GET['movieId']));
        $t->v('getDomainMovieId', empty($_GET['domainMovieId']) ? '' : F::checkstr($_GET['domainMovieId']));
        $this->setMenuVars($t, 'rewrited');
        F::setPageVars($t);

        return $t->get();
    }

    public function submitMoveToTop()
    {
        $domainMovieId = empty($_POST['domainMovieId']) ? null : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($domainMovieId);
        $movie->setDomainMovieMovedToTop($movie->getDomainMovieMovedToTop() ? false : true);
        $movie->save($this->user->getLogin());
        // F::dump($movie);
        F::redirect(F::referer_url());
    }

    public function submitDoRedirect()
    {
        $domainMovieId = empty($_POST['domainMovieId']) ? null : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($domainMovieId);
        $movie->setDomainMovieDoRedirect($movie->getDomainMovieDoRedirect() ? false : true);
        $movie->save($this->user->getLogin());
        F::redirect(F::referer_url());
    }

    // скрываем плеер и ставим метку ркн
    public function submitDeletePlayer()
    {
        $domainMovieId = empty($_POST['domainMovieId']) ? null : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($domainMovieId);
        if ($movie->getDomainMovieRkn()) {
            F::error('Can not undo deletion, because Rkn flag is active');
        }
        if ($movie->getDomainMovieDeleted()) {
            $movie->setDomainMovieDeleted(false);
            $movie->setDomainMovieRkn(false);
        } else {
            $movie->setDomainMovieDeleted(true);
            $movie->setDomainMovieRkn(true);
        }
        $movie->save($this->user->getLogin());
        F::redirect(F::referer_url());
    }

    public function submitWriting()
    {
        $domainMovieId = empty($_POST['domainMovieId']) ? null : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($domainMovieId);
        $movie->setDomainMovieWriting($movie->getDomainMovieWriting() ? false : true);
        $movie->save($this->user->getLogin());
        // F::dump($movie);
        F::redirect(F::referer_url());
    }

    public function submitGenerateDmca()
    {
        $domainMovieId = empty($_POST['domainMovieId']) ? null : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($domainMovieId);
        $movie->generateDmca();
        $movie->save($this->user->getLogin());
        // return 'test';
        // F::redirect(F::referer_url().'&mmnas');
        F::redirect(F::referer_url());
    }

    public function index()
    {
        $isCsv = empty($_GET['csv']) ? false : (F::checkstr($_GET['csv']) ? true : false);
        if ($isCsv) {
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename=mass_payment.csv');
            Engine::setDebug(false);
        }
        $templatePage = empty($_GET['templatePage']) ? 'admin/index' : F::checkstr($_GET['templatePage']);
        $showWorkersWithoutWallet = isset($_GET['showWorkersWithoutWallet']) ? (F::checkstr($_GET['showWorkersWithoutWallet']) ? true : false) : true;
        $showWorkersWithoutUnitpayWallet = isset($_GET['showWorkersWithoutUnitpayWallet']) ? (F::checkstr($_GET['showWorkersWithoutUnitpayWallet']) ? true : false) : true;
        $showEnoughBalanceWithoutModeration = isset($_GET['showEnoughBalanceWithoutModeration']) ? (F::checkstr($_GET['showEnoughBalanceWithoutModeration']) ? true : false) : false;
        $t = new Template($templatePage);
        $cinema = new Cinema;
        $t->v('serviceLoad', $cinema->getServiceLoad());
        $timer['active_rewriters']['start'] = F::microtime_float();
        // список активных рерайтеров и статистика по ним
        $users = new Users;
        $users->setLevels(['worker', 'moderator']);
        $users->setActiveDescriptionWriter(true);
        $users->setOrder('isnull(users.yandex), isnull(users.wmr), isnull(users.wmz), isnull(users.advcash), isnull(users.sberbank), isnull(users.sbp), isnull(users.usdtbep20)');
        $users->setTempBalanceIs(' >= '.$cinema->getMinWorkerPayment());
        $users->setPayoutToday(false);
        $arr = $users->get();
        $list = '';
        foreach ($arr as $v) {
            $user = new User($v['login']);
            // сколько заданий видит этот исполнитель
            $moviesExcluded = new Movies;
            $moviesExcluded->setBookedForLogin('ozerin');
            $moviesExcluded->setWithDomainDescription(false);
            $moviesExcluded->setDomainMovieWithCustomerEnoughBalance(true);
            // $moviesExcluded->setUseDomainMovieCustomers(true);
            $moviesExcluded->setExcludeDomainMoviesForWorker($user->getLogin());
            // на доработках
            $moviesRework = new Movies;
            $moviesRework->setDescriptionWriter($user->getLogin());
            $moviesRework->setDomainMovieRework(true);
            $moviesRework->setLimit($moviesRework->getRows());
            $moviesReworkArr = $moviesRework->get();
            $sumRework = 0;
            foreach ($moviesReworkArr as $mrv) {
                $movieRework = new DomainMovie($mrv['domain_movies_id']);
                $sumRework = $sumRework + $movieRework->getDomainMovieDescriptionWorkerPrice();
            }
            // на модерации
            $moviesModer = new Movies;
            $moviesModer->setDescriptionWriter($user->getLogin());
            $moviesModer->setDomainMovieShowToCustomer(false);
            $moviesModer->setLimit($moviesModer->getRows());
            $moviesModerArr = $moviesModer->get();
            $sumModer = 0;
            foreach ($moviesModerArr as $mmv) {
                $movieModer = new DomainMovie($mmv['domain_movies_id']);
                $sumModer = $sumModer + $movieModer->getDomainMovieDescriptionWorkerPrice();
            }
            $avgTextUnique = $user->getAvgTextUnique();
            $walletObj = $user->getWallet();
            $wallet = $walletObj->wallet ? '<a href="'.$walletObj->link.'">'.$walletObj->serviceName.':</a> <span id="wallet'.$user->getId().'">'.$walletObj->wallet.'</span>' : 'нет кошелька';
            // для массовых выплат: если кошелек не указан - пропускаем
            if (! $showWorkersWithoutWallet) {
                if (! $userWallet['wallet']) {
                    continue;
                }
            }
            if (! $showWorkersWithoutUnitpayWallet) {
                if (! $userWallet['unitpayType']) {
                    continue;
                }
            }
            $balanceWithoutModeration = round($user->getTempBalance() - $sumModer, 2);
            if ($showEnoughBalanceWithoutModeration) {
                if ($balanceWithoutModeration < $cinema->getMinWorkerPayment()) {
                    continue;
                }
            }
            $templateWorkerItem = empty($_GET['templateWorkerItem']) ? 'admin/index/item_rewriter' : F::checkstr($_GET['templateWorkerItem']);
            $tList = new Template($templateWorkerItem);
            $tList->v('userLogin', $user->getLogin());
            $tList->v('balanceWithoutModeration', $balanceWithoutModeration);
            $tList->v('balanceWithoutModerationFloor', floor($balanceWithoutModeration));
            $tList->v('balanceWithoutModerationUSD', round(floor($balanceWithoutModeration) / (F::getUsdRubRateCached() + 0.5), 2));
            $tList->v('avgTextUnique', is_null($avgTextUnique) ? '?' : round($avgTextUnique));
            $tList->v('rewritePricePer1k', $user->getRewritePricePer1k());
            $tList->v('moviesReworkRows', $moviesRework->getRows());
            $tList->v('wallet', $wallet);
            $tList->v('userId', $user->getId());
            $tList->v('userComments', $user->getComments());
            $tList->v('userLikeCounter', $user->getLikeCounter());
            $tList->v('userDislikeCounter', $user->getDislikeCounter());
            $tList->v('userBlacklistCounter', $user->getBlacklistCounter());
            $tList->v('moviesExcludedRows', F::cache($moviesExcluded, 'getRows'), null, 3600);
            $tList->v('userWallet', $walletObj->wallet);
            $tList->v('userWalletType', $walletObj->type);
            $tList->v('userWalletUnitpayType', $walletObj->unitpayType);
            // $tList->v('',);
            $list .= $tList->get();
        }
        $timer['active_rewriters']['end'] = F::microtime_float();
        $t->v('userBalancesList', $list);
        $timer['active_customers']['start'] = F::microtime_float();
        $t->v('customerStatList', F::cache($this, 'getActiveCustomersList', null, 300));
        $timer['active_customers']['end'] = F::microtime_float();
        $timer['moviesOnDomains']['start'] = F::microtime_float();
        // кол-во фильмов на сайтах, и сколько неактивных
        $arr = F::cache('engine\app\models\F', 'query_arr_arg', ['query' => '
			select domain,count(domain) as q,
			sum(active = 0) as q_not_active,
			sum(active = 0 and description_length > 0) as q_not_active_descripted,
			sum(description_length > 0) as q_descripted
			from '.F::typetab('domain_movies').'
			group by domain;
			']);
        $list = '';
        foreach ($arr as $v) {
            // $domain = new Domain($v['domain']);
            // F::dump($domain);
            $tList = new Template('admin/index/item_movies_on_domain');
            $tList->v('domain', $v['domain']);
            $tList->v('q', $v['q']);
            $tList->v('q_descripted', $v['q_descripted']);
            $tList->v('q_not_active', $v['q_not_active']);
            $tList->v('q_not_active_descripted', $v['q_not_active_descripted']);
            // $tList->v('',);
            $list .= $tList->get();
        }
        $timer['moviesOnDomains']['end'] = F::microtime_float();
        $t->v('moviesOnDomainList', $list);
        // общая активность за последние дни, только по заданиям с закачиками
        $timer['commonActivity']['start'] = F::microtime_float();
        $arr = F::query_arr('
			select date(description_date) as day,
			ceil(sum(description_length*customer_price_per_1k/1000)) as customer_price,
			ceil(sum(description_length*description_price_per_1k/1000)) as rewriter_price,
			ceil(sum(description_length*moderation_price_per_1k/1000)) as moderation_price,
			count(*) as q
			from '.F::typetab('domain_movies').'
			where description_length > 0 and description_date between (current_date - interval 21 day) and (current_date + interval 1 day)
			and customer is not null
			group by day desc
			;');
        $list = '';
        foreach ($arr as $v) {
            $tList = new Template('admin/index/item_common_activity');
            $tList->v('day', $v['day']);
            $tList->v('q', $v['q']);
            $tList->v('customerPrice', $v['customer_price']);
            $tList->v('rewriterPrice', $v['rewriter_price']);
            $tList->v('moderationPrice', $v['moderation_price']);
            $tList->v('profit', $v['customer_price'] - $v['rewriter_price'] - $v['moderation_price']);
            $list .= $tList->get();
        }
        $timer['commonActivity']['end'] = F::microtime_float();
        $t->v('commonActivityList', $list);
        // активность рерайтеров за последние 4 дня
        $timer['rewritersActivity']['start'] = F::microtime_float();
        $arr = F::query_arr('
			select description_writer, date(description_date) as day, count(*) as q
			from '.F::typetab('domain_movies').'
			where description_length > 0 and description_date between (current_date - interval 3 day) and (current_date + interval 1 day)
			group by day desc,description_writer
			;');
        $list = '';
        foreach ($arr as $v) {
            $list .= '<tr><td>'.$v['day'].'</td><td>'.$v['description_writer'].'</td><td>'.$v['q'].'</td></tr>';
        }
        $timer['rewritersActivity']['end'] = F::microtime_float();
        $t->v('rewritersActivityList', $list);
        // детектор битой базы moonwalk
        $timer['badMoonwalkDetector']['start'] = F::microtime_float();
        $arr = F::cache('engine\app\models\F', 'query_arr_arg', ['query' => '
			select description,count(*) as q
			from '.F::typetab('cinema').'
			group by description
			order by q desc
			limit 10
			;']);
        $list = '';
        foreach ($arr as $v) {
            $list .= '<tr><td>'.mb_strimwidth(($v['description'] ?? ''), 0, 100, '...').'</td><td>'.$v['q'].'</td></tr>';
        }
        $timer['badMoonwalkDetector']['end'] = F::microtime_float();
        $t->v('badMoonwalkDetectorList', $list);
        $this->setMenuVars($t, 'index');
        $list = '';
        foreach ($timer as $k => $v) {
            $list .= '<li>'.$k.': '.(round($v['end'] - $v['start'], 3)).' сек.</li>';
        }
        $t->v('timerList', $list);
        F::setPageVars($t);

        return $t->get();
    }

    public function getActiveCustomersList()
    {
        // список активных заказчиков и статистика по ним
        $users = new Users;
        $users->setLevel('customer');
        $users->setOrder('users.temp_balance desc');
        $users->setTempBalanceIs('!=0');
        $arr_active_customers = $users->get();
        // $arr_active_customers = F::cache($users,'get',300);
        // admin-light тоже нужен
        $users = new Users;
        $users->setLevel('admin-lite');
        $users->setOrder('users.temp_balance desc');
        $users->setTempBalanceIs('!=0');
        $arr_active_admins_light = $users->get();
        // $arr_active_admins_light = F::cache($users,'get',300);
        // новые заказчики
        $users = new Users;
        $users->setLimit(8);
        $users->setOrder('users.id desc');
        $users->setLevel('customer');
        $arr_new_customers = $users->get();
        // $arr_new_customers = F::cache($users,'get',300);
        // создаем результирующий массив
        $arr_result_customers = $arr_active_customers;
        foreach ($arr_active_admins_light as $v) {
            $arr_result_customers[] = $v;
        }
        foreach ($arr_new_customers as $v) {
            $arr_result_customers[] = $v;
        }
        $i = 0;
        $list = '';
        // $arr_result_customers = array();
        foreach ($arr_result_customers as $v) {
            $user = new User($v['login']);
            // сколько заданий в очереди у этого покупателя
            $movies = new Movies;
            $movies->setDomainMovieCustomer($user->getLogin());
            $movies->setWithDomainDescription(false);
            $movies->setWithBookedForLogin(true);
            $withoutDescriptionRows = $movies->getRows();
            // сколько выполненных заданий в очереди у этого покупателя
            $movies = new Movies;
            $movies->setDomainMovieCustomer($user->getLogin());
            $movies->setWithDomainDescription(true);
            // $withDescriptionRows = $movies->getRows();
            $withDescriptionRows = '?';
            $i++;
            $tList = new Template('admin/index/item_customer');
            $tList->v('userLogin', $user->getLogin());
            $tList->v('classNotEnoughBalance', $user->getCustomerIsEnoughBalance() ? '' : 'notEnoughBalance');
            // отделяем полоской активных от новеньких
            $tList->v('borderActiveCustomers', ($i == count($arr_active_customers)) ? 'border-bottom: 2px solid green;' : '');
            $tList->v('userTempBalance', round($user->getTempBalance(), 2));
            $tList->v('userTransfers', $user->getTransfers());
            $tList->v('withoutDescriptionRows', $withoutDescriptionRows);
            $tList->v('withDescriptionRows', $withDescriptionRows);
            $tList->v('customerLowerDownIcon', $user->getCustomerLowerDown() ? '<i class="fas fa-arrow-down" style="color: red;"></i>' : '');
            // $tList->v('',);
            $list .= $tList->get();
        }

        return $list;
    }

    public function actions()
    {
        $t = new Template('admin/actions');
        $movies = new Movies;
        $movies->setIncludeNullType(true);
        $movies->setChecked(true);
        $movies->setLimit(1);
        $movies->setPage(1);
        // F::dump($movies->getRows());
        $t->v('moviesCheckersCount', $movies->getRows());
        // F::dump($movies->getRows());
        $m = new Moonwalks;
        $m->setChecked(true);
        $m->setLimit(1);
        $m->setPage(1);
        $t->v('moonwalksCheckersCount', $m->getRows());
        $k = new Kodiks;
        $k->setChecked(true);
        $k->setLimit(1);
        $k->setPage(1);
        $t->v('kodiksCheckersCount', $k->getRows());
        $arr = F::query_assoc('select count(*) as q from '.F::typetab('collaps').' where checked;');
        $t->v('collapsCheckersCount', $arr['q']);
        $arr = F::query_assoc('select count(*) as q from '.F::typetab('myshows').' where checked;');
        $t->v('myShowsMeCheckersCount', $arr['q']);
        $arr = F::query_assoc('select count(*) as q from '.F::typetab('rserial.com').' where checked;');
        $t->v('rserialCheckersCount', $arr['q']);
        $arr = F::query_assoc('select count(*) as q from '.F::typetab('lordsfilms.tv').' where checked;');
        $t->v('lordFilmCheckersCount', $arr['q']);
        $arr = F::query_assoc('select count(*) as q from '.F::typetab('videodb').' where checked;');
        $t->v('videodbCheckersCount', $arr['q']);
        $this->setMenuVars($t, 'actions');

        return $t->get();
    }

    public function setMenuVars($t = null, $active = null)
    {
        $arr = ['index', 'actions', 'rewrite', 'rewrited', 'movies'];
        foreach ($arr as $v) {
            $t->v('menu'.ucfirst($v).'Active', ($v == $active) ? 'active' : '');
        }
        $t->v('userLogin', $this->user->getLogin());
        $t->v('userRewriteBalance', $this->user->getRewriteBalance());
        // узнаем кол-во премодераций
        $movies = new Movies;
        $movies->setDomainMovieShowToCustomer(false);
        $movies->setWithDomainDescription(true);
        $movies->setDomainMovieRework(false);
        $t->v('reworkRows', $movies->getRows());

        return true;
    }

    public function logout()
    {
        $this->auth->clearSID();
        F::redirect('/');
    }

    public function activateDescriptedDomainMovies()
    {
        $domain = empty($_GET['domain']) ? F::error('Domain required to run activateDescriptedDomainMovies') : F::checkstr($_GET['domain']);
        $limit = empty($_GET['limit']) ? F::error('Limit required to run activateDescriptedDomainMovies') : F::checkstr($_GET['limit']);
        $movies = new Movies;
        $movies->setDomain($domain);
        $movies->setActive(false);
        $movies->setWithDomainDescription(true);
        $movies->setLimit($limit);
        $movies->setPage(1);
        // F::dump($movies->get());
        foreach ($movies->get() as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            $movie->setDomainMovieActive(true);
            $movie->save($this->user->getLogin());
        }
        F::redirect(F::referer_url());
    }

    public function topRatedNotOnDomain()
    {
        $movies = new Movies;
        $movies->setDomainIsNot('seasongo.net');
        $movies->setOrder('cinema.kinopoisk_votes desc');
        $movies->setLimit(1);
        $movies->setPage(1);
        $movies->setYear(2018);
        F::dump($movies->getRows());
    }

    public function movies()
    {
        $t = new Template('admin/movies/vue');
        $this->setMenuVars($t, 'movies');

        return $t->get();
    }

    public function domain_movies()
    {
        $t = new Template('admin/movies/domain_movies_vue');
        $this->setMenuVars($t, 'movies');

        return $t->get();
    }

    public function moviesList()
    {
        $domain = empty($_POST['domain']) ? null : $_POST['domain'];
        $domainIsNot = empty($_POST['domainIsNot']) ? null : $_POST['domainIsNot'];
        $order = empty($_POST['order']) ? null : $_POST['order'];
        $limit = empty($_POST['limit']) ? 10 : $_POST['limit'];
        $page = empty($_POST['page']) ? 1 : $_POST['page'];
        $year = empty($_POST['year']) ? null : $_POST['year'];
        $bookedForLogin = empty($_POST['bookedForLogin']) ? null : $_POST['bookedForLogin'];
        $withDomainDescription = isset($_POST['withDomainDescription']) ? $_POST['withDomainDescription'] : '';
        $withBookedForLogin = isset($_POST['withBookedForLogin']) ? $_POST['withBookedForLogin'] : '';
        $includeMovies = isset($_POST['includeMovies']) ? (($_POST['includeMovies'] === 'on') ? true : false) : false;
        $includeSerials = isset($_POST['includeSerials']) ? (($_POST['includeSerials'] === 'on') ? true : false) : false;
        $includeTrailers = isset($_POST['includeTrailers']) ? (($_POST['includeTrailers'] === 'on') ? true : false) : false;
        $domainMovieActive = isset($_POST['domainMovieActive']) ? $_POST['domainMovieActive'] : '';
        $descriptionWriter = empty($_POST['descriptionWriter']) ? null : $_POST['descriptionWriter'];
        $groupId = empty($_POST['groupId']) ? null : $_POST['groupId'];
        // f::dump($domainMovieActive);
        $movies = new Movies;
        $movies->setDomain($domain);
        $movies->setDomainIsNot($domainIsNot);
        $movies->setOrder($order);
        $movies->setLimit($limit);
        $movies->setPage($page);
        $movies->setYear($year);
        $movies->setBookedForLogin($bookedForLogin);
        $movies->setWithDomainDescription(($withDomainDescription === '') ? null : ($withDomainDescription ? true : false));
        $movies->setWithBookedForLogin(($withBookedForLogin === '') ? null : ($withBookedForLogin ? true : false));
        $movies->setIncludeMovies($includeMovies);
        $movies->setIncludeSerials($includeSerials);
        $movies->setIncludeTrailers($includeTrailers);
        $movies->setDomainMovieActive(($domainMovieActive === '') ? null : ($domainMovieActive ? true : false));
        $movies->setDescriptionWriter($descriptionWriter);
        $movies->setGroup($groupId);
        // f::dump($movies->getDomainMovieActive());
        if ($movies->getUseDomainMovies()) {
            $movies->setListTemplate('admin/movies/list_domain_movie');
        } else {
            $movies->setListTemplate('admin/movies/list');
        }
        $t = new Template('admin/movies/index');
        // F::dump($movies);
        $t->v('moviesList', $movies->getList());
        // rows
        $t->v('rows', $movies->getRows());
        // limit
        $t->v('limit', $limit);
        $t->v('includeMoviesChecked', $includeMovies ? 'checked' : '');
        $t->v('includeSerialsChecked', $includeSerials ? 'checked' : '');
        $t->v('includeTrailersChecked', $includeTrailers ? 'checked' : '');
        // domain
        $domains = new Domains;
        $list = '';
        foreach ($domains->get() as $v) {
            $selected = ($domain == $v['domain']) ? 'selected' : '';
            $list .= '<option value="'.$v['domain'].'" '.$selected.'>'.$v['domain'].'</option>';
        }
        $t->v('domainOptions', $list);
        // domainIsNot
        $list = '';
        foreach ($domains->get() as $v) {
            $selected = ($domainIsNot == $v['domain']) ? 'selected' : '';
            $list .= '<option value="'.$v['domain'].'" '.$selected.'>'.$v['domain'].'</option>';
        }
        $t->v('domainIsNotOptions', $list);
        $t->v('domainIsNot', $domainIsNot);
        // bookedForLoginOptions
        $users = new Users;
        $users->setLevels(['worker', 'admin', 'moderator']);
        $list = '';
        foreach ($users->get() as $v) {
            $selected = ($bookedForLogin == $v['login']) ? 'selected' : '';
            $list .= '<option value="'.$v['login'].'" '.$selected.'>'.$v['login'].'</option>';
        }
        $t->v('bookedForLoginOptions', $list);
        // withDomainDescription
        $withDomainDescriptionOptions = ['Есть' => '1', 'Нет' => '0'];
        $list = '';
        foreach ($withDomainDescriptionOptions as $name => $value) {
            $selected = ($withDomainDescription === $value) ? 'selected' : '';
            $list .= '<option value="'.$value.'" '.$selected.'>'.$name.'</option>';
        }
        $t->v('withDomainDescriptionOptions', $list);
        // withBookedForLogin
        $withBookedForLoginOptions = ['Да' => '1', 'Нет' => '0'];
        $list = '';
        // f::dump($withBookedForLogin);
        foreach ($withBookedForLoginOptions as $name => $value) {
            $selected = ($withBookedForLogin === $value) ? 'selected' : '';
            $list .= '<option value="'.$value.'" '.$selected.'>'.$name.'</option>';
        }
        $t->v('withBookedForLoginOptions', $list);
        // order
        $orderOptions = [
            'Голоса Кинопоиск' => 'cinema.kinopoisk_votes desc',
            'Год и голоса Кинопоиск' => 'cinema.year desc, cinema.kinopoisk_votes desc',
            'Голоса IMDb' => 'cinema.imdb_votes desc',
            'Год и голоса IMDb' => 'cinema.year desc, cinema.imdb_votes desc',
        ];
        $list = '';
        foreach ($orderOptions as $name => $value) {
            $selected = ($order === $value) ? 'selected' : '';
            $list .= '<option value="'.$value.'" '.$selected.'>'.$name.'</option>';
        }
        $t->v('orderOptions', $list);
        // domainMovieActive
        $domainMovieActiveOptions = ['Да' => '1', 'Нет' => '0'];
        $list = '';
        foreach ($domainMovieActiveOptions as $name => $value) {
            $selected = ($domainMovieActive === $value) ? 'selected' : '';
            $list .= '<option value="'.$value.'" '.$selected.'>'.$name.'</option>';
        }
        $t->v('domainMovieActiveOptions', $list);
        // descriptionWriter
        $users = new Users;
        $users->setLevels(['worker', 'moderator']);
        $list = '';
        foreach ($users->get() as $v) {
            $selected = ($descriptionWriter == $v['login']) ? 'selected' : '';
            $list .= '<option value="'.$v['login'].'" '.$selected.'>'.$v['login'].'</option>';
        }
        $t->v('descriptionWriterOptions', $list);
        // setBookedForLogin (только активные рерайтеры)
        $users = new Users;
        $users->setLevels(['worker', 'admin', 'moderator']);
        $users->setActiveDescriptionWriter(true);
        $list = '';
        // f::dump($users->get());
        foreach ($users->get() as $v) {
            $selected = ($descriptionWriter == $v['login']) ? 'selected' : '';
            $list .= '<option value="'.$v['login'].'" '.$selected.'>'.$v['login'].'</option>';
        }
        $t->v('setBookedForLoginOptions', $list);
        // group
        $groups = new Groups;
        $list = '';
        foreach ($groups->get() as $v) {
            $group = new Group($v['id']);
            $selected = ($groupId == $group->getId()) ? 'selected' : '';
            $list .= '<option value="'.$group->getId().'" '.$selected.'>'.$group->getName().'</option>';
        }
        $t->v('groupOptions', $list);
        $this->setMenuVars($t, 'movies');

        return $t->get();
    }

    public function submitMoviesAction()
    {
        $action = empty($_POST['action']) ? null : $_POST['action'];
        $domainIsNot = empty($_POST['domainIsNot']) ? null : $_POST['domainIsNot'];
        $movies = empty($_POST['movies']) ? [] : $_POST['movies'];
        $bookedForLogin = empty($_POST['bookedForLogin']) ? null : $_POST['bookedForLogin'];
        if ($action === 'addToDomainIsNot') {
            foreach ($movies as $movie => $checked) {
                $domainMovieId = DomainMovie::add($movie);
                $domainMovie = new DomainMovie($domainMovieId);
                $domainMovie->setDomain($domainIsNot);
                $domainMovie->save();
            }
        }
        if ($action === 'setBookedForLogin') {
            foreach ($movies as $movie => $checked) {
                $domainMovie = new DomainMovie($movie);
                $domainMovie->setDomainMovieBookedForLogin($bookedForLogin);
                $domainMovie->save();
            }
        }
        F::redirect('/?r=Admin/moviesList');
    }

    // обновляем значение domain_movies.text_ru_unique из text_ru_result, т.к. был косяк, когда значения были разные
    public function changeTextRuUniqueForAllMovies()
    {
        $movies = new Movies;
        $movies->setLimit(3000);
        $movies->setPage(1);
        $movies->setWithDomainDescription(true);
        // f::dump($movies->get());
        // f::dump($movies->getRows());
        foreach ($movies->get() as $v) {
            $movie = new DomainMovie($v['domain_movies_id']);
            if ($movie->getDomainMovieTextRuResult()) {
                // F::dump($movie->getDomainMovieTextRuResult()->unique);
                $movie->setDomainMovieTextRuUnique($movie->getDomainMovieTextRuResult()->unique);
                $movie->save();
            }
        }
        f::alert('Done');
    }

    public function submitPay()
    {
        $login = empty($_POST['login']) ? F::error('Login required') : F::checkstr($_POST['login']);
        $amount = empty($_POST['amount']) ? F::error('Amount required') : F::checkstr($_POST['amount']);
        $amountUsd = empty($_POST['amountUsd']) ? null : F::checkstr($_POST['amountUsd']);
        $user = new User($login);
        if ($user->getWallet()->wallet == 'Wmz') {
            if (! $amountUsd) {
                F::error('Amount USD required');
            }
        }
        // $user->setPaid($user->getPaid() + $amount);
        // F::dump($result);
        if (! empty($user->getWallet()->fkWallet['minRub']) and $amount < $user->getWallet()->fkWallet['minRub']) {
            F::error('Минималка по '.$user->getWallet()->type.': '.$user->getWallet()->fkWallet['minRub'].' руб.');
        }
        $transferId = $user->addTransfer(-$amount, 'Выплата', true);
        if (empty($transferId)) {
            F::error('TransferId can\'t be empty');
        }

        // если есть unitpayType, то производим автоматическу выплату, если нет - просто фиксируем выплату
        if ($user->getWallet()->fkWallet) {
            // if ($user->getWallet()->unitpayType) {
            if (! $user->getWallet()->wallet) {
                F::error('Wallet required to use submitPay auto payment');
            }
            // $result = $this->unitPayMassPayment($amount,$user->getWallet()->wallet,$user->getWallet()->unitpayType,$transferId);
            $fk = new \engine\app\models\FkWallet;
            $fk->setApiKey('DED044F9C4592DADAE0A60A7E79FFBE4');
            $fk->setWalletId('F111271148');
            if ($user->getWallet()->type == 'Wmz') {
                $fk->setAmount($amountUsd);
            } else {
                $fk->setAmount($amount);
            }
            $fk->setPurse($user->getWallet()->wallet);
            $fk->setDesc($transferId);
            $fk->setCurrency($user->getWallet()->fkWallet['id']);
            // F::dump($user->getWallet());
            // F::dump($fk);
            $result = $fk->cashout();

            if ($result['status'] == 'error') {
                // if (!isset($result->result)) {
                // $errorMessage = isset($result->error)?$result->error->message:'Неизвестная ошибка';
                $errorMessage = isset($result['desc']) ? $result['desc'] : 'Неизвестная ошибка';
                F::query('update '.F::typetab('transfers').'
					set comment = \''.$errorMessage.'\',
					sum = 0,
					unitpay = \''.F::escape_string(json_encode($result)).'\'
					where id = \''.$transferId.'\'
					;');
                F::error('Выплата '.$transferId.' пользователю '.$user->getLogin().' не произведена: '.$errorMessage);
            }
            F::query('update '.F::typetab('transfers').'
				set unitpay = \''.F::escape_string(json_encode($result)).'\'
				where id = \''.$transferId.'\'
				;');
        }

        // $user->save();
        if ($user->getPartner()) {
            $partner = new User($user->getPartner());
            $partner->addTransfer($amount * (new Cinema)->getRefWorker() / 100, 'Начисление за реферала '.$user->getLogin());
        }
        if ($user->getEmail() and $user->getEmailConfirmed()) {
            $email = new Template('admin/rewrite/emails/payment');
            $email->v('login', $user->getLogin());
            $email->v('amount', $amount);
            (new Cinema)->sendMail($user->getEmail(), 'Кинотекст', 'Выплата', $email->get(), 'kinotext');
        }
        logger()->info('Оплачено '.$user->getLogin().' '.$amount.' руб.');
        F::redirect(F::referer_url());
    }

    public function unitPayMassPayment($sum = null, $purse = null, $purseType = null, $transactionId = null)
    {
        $secretKey = 'C81C60B3188-3C4F6AE35B6-DF9905C4E3';
        $projectId = 163001;
        if (! $sum) {
            F::error('Sum required to use massPayment');
        }
        if (! $purse) {
            F::error('Purse required to use massPayment');
        }
        if (! $purseType) {
            F::error('PurseType required to use massPayment');
        }
        if (! $transactionId) {
            F::error('TransactionId required to use massPayment');
        }
        $email = 'megaezh@me.com';
        $url = 'https://unitpay.ru/api?method=massPayment&params[sum]='.$sum.'&params[purse]='.str_replace(' ', '', $purse).'&params[login]='.$email.'&params[transactionId]='.$transactionId.'&params[secretKey]='.$secretKey.'&params[paymentType]='.$purseType.'&projectId='.$projectId;
        // F::dump($url);
        $result = file_get_contents($url);

        return json_decode($result);
    }

    public function deleteMovie()
    {
        $id = empty($_POST['domainMovieId']) ? F::error('Movie ID required') : F::checkstr($_POST['domainMovieId']);
        $movie = new DomainMovie($id);
        $movie->delete();
        F::redirect(F::referer_url());
    }

    public function getEmailsFromWhois()
    {
        $arr = F::query_arr('select domain from '.F::typetab('kinosites').' where email is null limit 1;');
        $list = '';
        foreach ($arr as $v) {
            $shell = shell_exec('whois '.$v['domain']);
            // $email = F::copybetwen('Registrant Email:',"\r\n",$shell);
            preg_match_all("/Registrant Email: [-a-z0-9!#$%&'*_`{|}~]+[-a-z0-9!#$%&'*_`{|}~\.=?]*@[a-zA-Z0-9_-]+[a-zA-Z0-9\._-]+/i", $shell, $emails);
            $email = empty($emails) ? null : (empty($emails[0][0]) ? null : $emails[0][0]);
            // F::dump($emails);
            F::query('update '.F::typetab('kinosites').' set email = \''.$email.'\' where domain = \''.$v['domain'].'\';');
            $list .= '<li>'.$v['domain'].' &mdash; '.$email.'</li>';
        }

        return '<ol>'.$list.'</ol>';
    }

    public function domainSettings()
    {
        $domain = empty($_GET['domain']) ? F::error('Specify domain to domainSettings') : F::checkstr($_GET['domain']);
        // если нет настроек для этого домена, то создаем их
        if (! Domain::isCinemaDomainExist($domain)) {
            Domain::addCinemaDomain($domain);
        }
        $domainObj = new Domain($domain);
        $t = new Template('admin/domain/settings');
        // $t = $this->setMenuVars($t);
        $t->v('domain', $domainObj->getDomain());
        $t->v('includeSerials', $domainObj->getIncludeSerials() ? 1 : 0);
        $t->v('includeMovies', $domainObj->getIncludeMovies() ? 1 : 0);
        $t->v('includeTrailers', $domainObj->getIncludeTrailers() ? 1 : 0);
        $t->v('moviesWithUniqueDescription', $domainObj->getMoviesWithUniqueDescription() ? 1 : 0);
        $t->v('topMoviesLimit', $domainObj->getTopMoviesLimit());
        $t->vf('watchLink', $domainObj->getWatchLinkTemplate());
        $t->vf('groupLink', $domainObj->getGroupLinkTemplate());
        $t->vf('genreLink', $domainObj->getGenreLinkTemplate());
        $t->v('moviesLimit', $domainObj->getMoviesLimit());
        $t->v('moviesPerDay', $domainObj->getMoviesPerDay());
        $t->v('legalMovies', $domainObj->getLegalMovies() ? 1 : 0);
        $t->v('watchCalcSameMovies', $domainObj->getWatchCalcSameMovies() ? 1 : 0);
        $t->vf('watchLinkEn', $domainObj->getWatchLinkEnTemplate());
        $t->v('calcThumbGenresList', $domainObj->getCalcThumbGenresList() ? 1 : 0);
        $t->vf('watchMovieLink', $domainObj->getWatchMovieLinkTemplate());
        $t->vf('watchSerialLink', $domainObj->getWatchSerialLinkTemplate());
        $t->v('useAltDescriptions', $domainObj->getUseAltDescriptions() ? 1 : 0);
        $t->v('allowRussianMovies', $domainObj->getAllowRussianMovies() ? 1 : 0);

        // f::dump($t);
        return $t->get();
    }

    public function domainSettingsVue()
    {
        $t = new Template('admin/domain/settings_vue');

        return $t->get();

    }

    public function submitDomainSettings()
    {
        if (empty($_POST)) {
            F::error('Не передан POST запрос');
        }
        $arr = $_POST;
        foreach ($arr as $k => $v) {
            $arr[$k] = trim($v);
        }
        // f::dump($arr);
        $domain = empty($_POST['domain']) ? F::error('Specify domain to domainSettings') : F::checkstr($_POST['domain']);
        $domainObj = new Domain($domain);
        if (isset($arr['includeSerials'])) {
            $domainObj->setIncludeSerials($arr['includeSerials']);
        }
        if (isset($arr['includeMovies'])) {
            $domainObj->setIncludeMovies($arr['includeMovies']);
        }
        if (isset($arr['includeTrailers'])) {
            $domainObj->setIncludeTrailers($arr['includeTrailers']);
        }
        if (isset($arr['moviesWithUniqueDescription'])) {
            $domainObj->setMoviesWithUniqueDescription($arr['moviesWithUniqueDescription']);
        }
        if (isset($arr['topMoviesLimit'])) {
            $domainObj->setTopMoviesLimit($arr['topMoviesLimit']);
        }
        if (isset($arr['watchLink'])) {
            $domainObj->setWatchLinkTemplate(($arr['watchLink'] == '') ? null : $arr['watchLink']);
        }
        if (isset($arr['groupLink'])) {
            $domainObj->setGroupLinkTemplate(($arr['groupLink'] == '') ? null : $arr['groupLink']);
        }
        if (isset($arr['genreLink'])) {
            $domainObj->setGenreLinkTemplate(($arr['genreLink'] == '') ? null : $arr['genreLink']);
        }
        if (isset($arr['moviesLimit'])) {
            $domainObj->setMoviesLimit($arr['moviesLimit']);
        }
        if (isset($arr['moviesPerDay'])) {
            $domainObj->setMoviesPerDay(($arr['moviesPerDay'] == '') ? null : $arr['moviesPerDay']);
        }
        if (isset($arr['legalMovies'])) {
            $domainObj->setLegalMovies($arr['legalMovies']);
        }
        if (isset($arr['watchCalcSameMovies'])) {
            $domainObj->setWatchCalcSameMovies($arr['watchCalcSameMovies']);
        }
        if (isset($arr['watchLinkEn'])) {
            $domainObj->setWatchLinkEnTemplate(($arr['watchLinkEn'] == '') ? null : $arr['watchLinkEn']);
        }
        if (isset($arr['calcThumbGenresList'])) {
            $domainObj->setCalcThumbGenresList($arr['calcThumbGenresList']);
        }
        if (isset($arr['watchMovieLink'])) {
            $domainObj->setWatchMovieLinkTemplate(($arr['watchMovieLink'] == '') ? null : $arr['watchMovieLink']);
        }
        if (isset($arr['watchSerialLink'])) {
            $domainObj->setWatchSerialLinkTemplate(($arr['watchSerialLink'] == '') ? null : $arr['watchSerialLink']);
        }
        if (isset($arr['useAltDescriptions'])) {
            $domainObj->setUseAltDescriptions($arr['useAltDescriptions']);
        }
        if (isset($arr['allowRussianMovies'])) {
            $domainObj->setAllowRussianMovies($arr['allowRussianMovies']);
        }
        // F::dump($domainObj);
        $domainObj->save();
        F::redirect('/?r=Admin/domainSettings&domain='.$domainObj->getDomain());
    }

    public function usersMassPayment()
    {
        $cinema = new Cinema;
        // список активных рерайтеров и статистика по ним
        $users = new Users;
        $users->setLevels(['worker', 'moderator']);
        $users->setActiveDescriptionWriter(true);
        $users->setOrder('isnull(users.yandex), isnull(users.wmr), isnull(users.wmz), isnull(users.advcash), isnull(users.sberbank), isnull(users.sbp), isnull(users.usdtbep20)');
        $users->setTempBalanceIs(' >= '.$cinema->getMinWorkerPayment());
        $arr = $users->get();
        $list = '';
        foreach ($arr as $v) {
            $user = new User($v['login']);
            // сколько заданий видит этот исполнитель
            $moviesExcluded = new Movies;
            $moviesExcluded->setBookedForLogin('ozerin');
            $moviesExcluded->setWithDomainDescription(false);
            $moviesExcluded->setDomainMovieWithCustomerEnoughBalance(true);
            // $moviesExcluded->setUseDomainMovieCustomers(true);
            $moviesExcluded->setExcludeDomainMoviesForWorker($user->getLogin());
            // на доработках
            $moviesRework = new Movies;
            $moviesRework->setDescriptionWriter($user->getLogin());
            $moviesRework->setDomainMovieRework(true);
            $moviesRework->setLimit(1000);
            $moviesReworkArr = $moviesRework->get();
            $sumRework = 0;
            foreach ($moviesReworkArr as $mrv) {
                $movieRework = new DomainMovie($mrv['domain_movies_id']);
                $sumRework = $sumRework + $movieRework->getDomainMovieDescriptionWorkerPrice();
            }
            // на модерации
            $moviesModer = new Movies;
            $moviesModer->setDescriptionWriter($user->getLogin());
            $moviesModer->setDomainMovieShowToCustomer(false);
            $moviesModer->setLimit(1000);
            $moviesModerArr = $moviesModer->get();
            $sumModer = 0;
            foreach ($moviesModerArr as $mmv) {
                $movieModer = new DomainMovie($mmv['domain_movies_id']);
                $sumModer = $sumModer + $movieModer->getDomainMovieDescriptionWorkerPrice();
            }
            $wallet['WMR'] = $user->getWmr();
            $wallet['WMZ'] = $user->getWmz();
            $wallet['Yandex'] = $user->getYandex();
            $wallet['AdvCash'] = $user->getAdvCash();
            $wallet['Sberbank'] = $user->getSberbank();
            $wallet['Sbp'] = $user->getSbp();
            $wallet['usdtbep20'] = $user->getUsdtBep20();
            $userWallet = ['wallet' => null, 'type' => null];
            foreach ($wallet as $t => $w) {
                if ($w) {
                    $userWallet = ['wallet' => $w, 'type' => $t];
                    break;
                }
            }
            // если кошелек не указан - пропускаем
            if (! $userWallet['wallet']) {
                continue;
            }
            $balanceWithoutModeration = round($user->getTempBalance() - $sumModer, 2);
            // если реальный (проверенный) баланс меньше минималки, то пропускаем
            if ($balanceWithoutModeration < $cinema->getMinWorkerPayment()) {
                continue;
            }
            $tList = new Template('admin/mass_payment/item');
            $tList->v('userLogin', $user->getLogin());
            $tList->v('balanceWithoutModeration', $balanceWithoutModeration);
            $tList->v('balanceWithoutModerationFloor', floor($balanceWithoutModeration));
            $tList->v('balanceWithoutModerationUSD', round(floor($balanceWithoutModeration) / (F::getUsdRubRateCached() + 0.5), 2));
            $tList->v('rewritePricePer1k', $user->getRewritePricePer1k());
            $tList->v('moviesReworkRows', $moviesRework->getRows());
            $tList->v('userWallet', $userWallet['wallet']);
            $tList->v('userWalletType', $userWallet['type']);
            $tList->v('userId', $user->getId());
            $tList->v('userComments', $user->getComments());
            $tList->v('userLikeCounter', $user->getLikeCounter());
            $tList->v('userDislikeCounter', $user->getDislikeCounter());
            $tList->v('userBlacklistCounter', $user->getBlacklistCounter());
            $tList->v('moviesExcludedRows', F::cache($moviesExcluded, 'getRows'), null, 3600);
            // $tList->v('',);
            $list .= $tList->get();
        }
        F::pre($list);
    }
}
