<?php

namespace cinema\app\controllers;

use App\Models\Abuse;
use App\Models\Config as EngineConfig;
use cinema\app\models\Cinema;
use cinema\app\models\Domain;
use cinema\app\models\DomainGroup;
use cinema\app\models\DomainMovie;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\DomainMovie as DomainMovieEloquent;
use cinema\app\models\eloquent\Group as GroupEloquent;
use cinema\app\models\eloquent\Tag as TagEloquent;
use cinema\app\models\Group;
use cinema\app\models\Groups;
use cinema\app\models\Moonwalk;
use cinema\app\models\Moonwalks;
use cinema\app\models\Movie;
use cinema\app\models\MovieComments;
use cinema\app\models\Movies;
use cinema\app\models\MovieSeasons;
use cinema\app\models\MovieTags;
use cinema\app\models\Tag;
use cinema\app\models\Tags;
use cinema\app\models\Videodb;
use cinema\app\services\FirstPlayer;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use Jaybizzle\CrawlerDetect\CrawlerDetect;

class MainController
{
    protected $domain = null;

    protected $domainEloquent = null;

    protected $template = null;

    protected $lang = null;

    public function __construct()
    {
        // dd(app()->getLoadedProviders())
        // dump(F::runtime('Botting'));
        // dd(\Illuminate\Support\Facades\Route::current());
        // $this->domainEloquent = Engine::getDomainEloquent()->cinemaDomain ?? F::error('Domain was not found');
        $this->domainEloquent = Engine::getDomainEloquent()->cinemaDomain;
        $this->domain = $this->domainEloquent->megaweb;
        $this->template = Engine::template();
        /* если задан шаблон в роутере megaweb, то получаем объект шаблона, если нет, то пробуем получить его через роутер laravel */
        /*
        if (Engine::router()->getTemplate()) {
            $this->template = Engine::template();
        } else {
            $route = \app\Facades\F::getRouteSettings(\Route::current());
            if ($route) {
                $this->template = new Template;
                $this->template->setPath($route->template);
            }
        }
        */
        // подтягиваем нужный videoroll_id для requested domain
        /*
        $videoroll_domain = Engine::getRequestedDomainObject()->getParentDomain()?Engine::getRequestedDomainObject()->getParentDomain()->getDomain():Engine::getRequestedDomainObject()->getDomain();
        $arr = F::query_assoc('
            select id
            from '.F::tabname('engine','videoroll_ids').'
            where domain = \''.$videoroll_domain.'\'
            limit 1
            ;');
        if (empty($arr['id'])) {
            $this->template->v('videoroll_id','');
            // logger()->info('Не найден videoroll_id для домена '.Engine::getRequestedDomainObject()->getDomain().' -> '.$videoroll_domain);
        } else {
            $this->template->v('videoroll_id',$arr['id']);
        }
        */
        // подтягиваем нужный videoroll_id для requested domain
        $videoroll_domain = Engine::getRequestedDomainObject()->getParentDomain() ? Engine::getRequestedDomainObject()->getParentDomainObject() : Engine::getRequestedDomainObject();
        $this->template->v('videoroll_id', $videoroll_domain->getVideorollId() ? $videoroll_domain->getVideorollId() : '');
        if (preg_match('/^(ua|en)\.([\w\-.]+)$/', Engine::getRequestedDomainObject()->getDomain(), $matches)) {
            $this->setLang($matches[1]);
        }
        $this->template->v('movieads_id', $this->domain->getMovieadsId() ? $this->domain->getMovieadsId() : 'efedbb4ef36caca12c6b3dfb21ebb54c');
        $this->template->v('footer_scripts', $this->footerScripts());
    }

    public function footerScripts()
    {
        $arr = Engine::getDomainEloquent()->domainBlocks()->whereBlock('footer')->whereActive(true)->get();
        $list = '';
        foreach ($arr as $v) {
            if (! Engine::isRealBot()) {
                $list .= '{'.$v->template.'}'.PHP_EOL;
            }
        }

        return $list;
    }

    public function top50()
    {
        /* если определен Engine::route(), значит используется laravel роут, получаем необходимые переменные, учитывая это */
        if (Engine::route()) {

            $canonical = $this->domainEloquent->top50Route();

            if (request()->getRequestUri() !== $canonical) {
                return redirect($canonical, 301);
            }
        }
        //
        $t = $this->template;
        $this->setMenuVars($t);
        F::setPageVars($t);
        $this->setPageVars($t);
        $t->v('showSearchBlock', 'false');
        //
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setLimit(50);
        $movies->setOrder('cinema.rating_sko desc');
        $movies->setDomain($this->domain->getDomain());
        // $movies->setDomainObject($this->domain);
        $movies->setActive(true);
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setDomainMovieDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setListTemplate('items/movie');
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        $t->v('cinemaList', F::cache($movies, 'getList'));

        // $t->v('cinemaList',$movies->getList());
        return $t->get();
    }

    // раздел обновлений
    public function updatedCinema($args = [])
    {
        if (Engine::route()) {
            $date = request()->route()->parameter('date');
        } else {
            $date = empty($args['date']) ? null : F::checkstr($args['date']);
        }

        if (! $date) {
            F::error('Set date for updatedCinema');
        }
        if (! \DateTime::createFromFormat('Y-m-d', $date)) {
            F::error('Wrong date');
        }
        $minDate = new \DateTime('2018-10-24');
        $maxDate = new \DateTime('now');
        $activeDate = new \DateTime($date);
        $prevDate = new \DateTime($date);
        $nextDate = new \DateTime($date);
        $prevDate->modify('-1 day');
        $nextDate->modify('+1 day');
        // dump($nextDateObj);
        if ($activeDate > $maxDate or $activeDate < $minDate) {
            F::error('Date out of range');
        }
        $t = $this->template;
        $this->setMenuVars($t);
        $t->v('showSearchBlock', 'false');
        $updates = F::cache($this, 'updatesList', ['date' => $date]);
        $t->v('updates', $updates ? $updates : '<p class="no-updates">Обновлений не было</p>');
        $t->v('nextDate', ($nextDate > $maxDate) ? '' : $nextDate->format('d.m.Y'));
        $t->v('prevDate', ($prevDate < $minDate) ? '' : $prevDate->format('d.m.Y'));
        $t->v('nextDateUrl', ($nextDate > $maxDate) ? '' : $nextDate->format('Y-m-d'));
        $t->v('prevDateUrl', ($prevDate < $minDate) ? '' : $prevDate->format('Y-m-d'));
        // $t->v('activeDate',$activeDate->format('d.m.Y'));
        // выводим текущую дату на русском
        $formatter = new \IntlDateFormatter(
            'ru_RU',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE,
            'Europe/Moscow'
        );
        $t->v('activeLocalDate', $formatter->format($activeDate));
        F::setPageVars($this->template);
        $this->setPageVars($this->template);

        return $t->get();
    }

    // главная страница и постраничный вывод видео
    public function index($args = [])
    {
        /* если определен Engine::route(), значит используется laravel роут, получаем необходимые переменные, учитывая это */
        if (Engine::route()) {
            // $dmca = request()->route()->parameter('dmca');
            $locale = request()->route()->parameter('locale');
            $page = empty(request()->route()->parameter('page')) ? 1 : request()->route()->parameter('page');

            /* locale без слэша */
            $locale = str_replace('/', '', $locale ?? '') ?? null;
            $locale = empty($locale) ? null : $locale;
            $lang = $locale;

            // if ($locale) {
            // 	app()->setLocale($locale);
            // }

            // dd($locale, $main_dmca);
            if ($page == 1) {
                $canonical = $this->domainEloquent->indexRoute(['locale' => $locale]);

                if (request()->getRequestUri() !== $canonical) {
                    // dd($canonical);
                    return redirect($canonical, 301);
                }
            }
        } else {
            $lang = empty($args['lang']) ? null : $args['lang'];
            $page = empty($args['page']) ? 1 : F::checkstr($args['page']);
        }

        if ($lang) {
            $this->setLang($lang);
        }
        $order = 'mostRecentAdded';
        $t = $this->template;
        // $t->setLang('ua');
        if (isset($_GET['seoLinks'])) {
            // F::dump('ok');
            $movieItem = 'items/seo_movie';
        } else {
            if (empty($args['page'])) {
                $movieItem = 'items/movie_index';
            } else {
                $movieItem = 'items/movie';
            }
        }
        $this->setMenuVars($t);
        $t->v('showSearchBlock', 'false');
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setDomain($this->domain->getDomain());
        // $orderArr = Movies::getOrderByName('RecentAndTopRated');
        // $movies->setOrder('domain_movies.add_date desc');
        $orderArr = Movies::getOrderByName($order);
        if ($orderArr) {
            // F::dump($orderArr['orderBy']);
            $movies->setOrder($orderArr['orderBy']);
        }
        $t->v('moviesOrder', $order);
        // F::dump($order);
        // $movies->setOrder($orderArr['orderBy']);
        $movies->setLimit($this->domain->getMoviesLimit());
        $movies->setPage($page);
        $movies->setListTemplate($movieItem);
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        // $movies->setDomainClass($this->domain);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        // F::dump($movies);
        // $movies->setWithUniqueDescription($this->domain->getMoviesWithUniqueDescription());
        $t->v('cinemaList', F::cache($movies, 'getList'));
        // F::dump($movies);
        // $t->v('cinemaList',$movies->getList());
        $movies->setPaginationUrlPattern('/movies/(:num)');
        // $t->v('cinemaPages',$movies->getPages());
        $t->v('cinemaPages', F::cache($movies, 'getPages'));
        // $movies->setWithUniqueDescription($this->domain->getMoviesWithUniqueDescription());
        $t->v('topCinemaList', $this->topCinemaList());
        $t->v('page', $page);
        // $t->v('topCinemaList20',F::cache($movies,'getList'));
        // dump($this->template);
        $date = new \DateTime('now');
        /*
        $i = 0;
        // если сегодня обновлений нет, показываем последние доступные
        do {
            if ($i) {
                $date->modify('-1 day');
            }
            $updates = F::cache($this,'updatesList',array('date'=>$date->format('Y-m-d')),3600);
            $i++;
            // максимальное кол-во итераций - 10, чтобы в случае чего цикл не ушел в бексонечность
            if ($i>15) {
                break;
            }
        } while (
            empty($updates)
        );
        */
        $updates = [];
        $t->v('updates', $updates ? $updates : '<p class="no-updates">{ru=[Обновлений сегодня еще не было]ua=[Оновлень сьогодні ще не було]en=[No updates today]}</p>');
        // $t->v('updatesCinemaList',F::cache($this,'updatesList',array('date'=>$date->format('Y-m-d'),'listTemplate'=>'items/movie_updates'),3600));
        // $t->v('updates',$this->todayUpdates());
        $t->v('todayDate', date('d.m.Y'));
        $t->v('todayDateUrl', date('Y-m-d'));
        // выводим текущую дату на русском
        $local = 'ru';
        if ($this->getLang() == 'ua') {
            $local = 'uk';
        }
        if ($this->getLang() == 'en') {
            $local = 'en';
        }
        $formatter = new \IntlDateFormatter(
            $local.'_RU',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE,
            'Europe/Moscow'
        );
        $t->v('localDate', $formatter->format($date));
        // готовим блок популярного в текущем месяце
        $datetime = new \DateTime('first day of this month');
        // F::dump($datetime->format('Y-m-d'));
        $movies = new Movies;
        $movies->setLang($this->getLang());
        // если это январь - выдаем предыдущий год
        if ($datetime->format('m') == '01') {
            $lastYear = new \DateTime('last year');
            // F::dump($lastYear);
            $movies->setYear($lastYear->format('Y'));
        } else {
            $movies->setYear($datetime->format('Y'));
        }
        $movies->setAddedAtFrom($datetime->format('Y-m-d'));
        $months = F::getMonths();
        $t->v('thisMonth', $months[$datetime->format('m')]['genitive']);
        $t->v('thisMonthNominative', $months[$datetime->format('m')]['nominative']);
        $datetime->modify('+1 month');
        $movies->setAddedAtTo($datetime->format('Y-m-d'));
        // if ($this->domain->getMoviesWithUniqueDescription()) {
        $movies->setDomain($this->domain->getDomain());
        // }
        $movies->setOrder('cinema.rating_sko desc');
        $movies->setLimit($this->domain->getTopMoviesLimit());
        $movies->setPage(1);
        $movies->setListTemplate('items/movie_top');
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setDomainMovieDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers(false);
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        // $movies->setDomainClass($this->domain);
        // F::dump($movies);
        $rows = F::cache($movies, 'getRows');
        // если недостаточно строк то выдаем предыдущий месяц
        if ($rows < 10) {
            $datetime->modify('-2 month');
            $movies->setAddedAtFrom($datetime->format('Y-m-d'));
            $datetime->modify('+1 month');
            $movies->setAddedAtTo($datetime->format('Y-m-d'));
            // F::dump($movies);
        }
        $t->v('topOfThisMonthCinemaList', F::cache($movies, 'getList'));
        F::setPageVars($this->template);
        $this->setPageVars($this->template);
        // выводим последние фильмы, мультфильмы, сериалы и мультсериалы для главной
        // группа 7
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setDomain($this->domain->getDomain());
        $movies->setOrder('cinema.year desc, cinema.rating_sko desc, cinema.added_at desc');
        $movies->setLimit(12);
        $movies->setPage(1);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setListTemplate($movieItem);
        $movies->setDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        // $movies->setDomainClass($this->domain);
        $movies->setGroup(7);
        // F::dump($movies);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        $t->v('group7List', F::cache($movies, 'getList'));
        // группа 12
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setDomain($this->domain->getDomain());
        $movies->setOrder('cinema.year desc, cinema.rating_sko desc, cinema.added_at desc');
        $movies->setLimit(12);
        $movies->setPage(1);
        $movies->setListTemplate($movieItem);
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        // $movies->setDomainClass($this->domain);
        $movies->setGroup(12);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        $t->v('group12List', F::cache($movies, 'getList'));
        // группа 6
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setDomain($this->domain->getDomain());
        $movies->setOrder('cinema.year desc, cinema.rating_sko desc, cinema.added_at desc');
        $movies->setLimit(12);
        $movies->setPage(1);
        $movies->setListTemplate($movieItem);
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        // $movies->setDomainClass($this->domain);
        $movies->setGroup(6);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        $t->v('group6List', F::cache($movies, 'getList'));
        // группа 246
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setDomain($this->domain->getDomain());
        $movies->setOrder('cinema.year desc, cinema.rating_sko desc, cinema.added_at desc');
        $movies->setLimit(12);
        $movies->setPage(1);
        $movies->setListTemplate($movieItem);
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        // $movies->setDomainClass($this->domain);
        $movies->setGroup(246);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        // F::dump($movies);
        $t->v('group246List', F::cache($movies, 'getList'));
        // рандомный ютуб ролик из популярных
        $movies = new Movies;
        $movies->setDomain($this->domain->getDomain());
        $movies->setLimit(250);
        $movies->setPage(1);
        $movies->setDeleted(false);
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        $movies->setOrder('cinema.rating_sko desc');
        $arr = F::cache($movies, 'get');
        if ($arr) {
            $rand = array_rand($arr);
            $movie = new DomainMovie($arr[$rand]['domain_movies_id']);
            $t->v('randomMovieYoutubePlayer', $movie->getYoutubePlayer());
            $t->v('randomMovieWatchLink', $this->domain->getWatchLink($movie));
            $t->v('randomMovieTitle', [
                'ru' => $movie->getTitleRu(),
                'ua' => $movie->getTitleRu(),
                'en' => $movie->getTitleEnIfPossible(),
            ]);
        } else {
            $t->v('randomMovieYoutubePlayer', '');
            $t->v('randomMovieWatchLink', '');
            $t->v('randomMovieTitle', '');
        }
        $t->v('canonicalIndexLink', view('types.cinema.canonical-index', [
            'domain' => $this->domainEloquent,
        ]));

        return $t->get();
    }

    public function compilations()
    {
        $groups = new Groups;
        $groups->setType('compilation');
        $groups->setActive(true);
        $groups->setListTemplate('items/list_compilation');
        // устанавливаем объект домена для генерации ссылок в Groups::getList
        $groups->setDomain($this->domain->getDomain());
        // только группы с фильмами
        $groups->setWithDomainMovies(true);
        $groups->setUseDomainGroup(false);
        $t = $this->template;
        $t->v('compilationsList', F::cache($groups, 'getList'));
        F::setPageVars($t);
        $this->setMenuVars($t);
        $this->setPageVars($t);
        $t->v('showSearchBlock', 'false');

        return $t->get();
        // F::dump($groups->get());
    }

    // вывод видео для группы
    public function groupCinema($args = [])
    {
        $groupID = empty($args['groupID']) ? F::error('Enter groupID') : F::checkstr($args['groupID']);
        $page = empty($args['page']) ? 1 : F::checkstr($args['page']);
        $order = empty($_GET['order']) ? 'recentAndTopRated' : F::checkstr($_GET['order']);
        $t = $this->template;
        // $time['before setMenuVars'] = Engine::getRunningTime();
        $this->setMenuVars($t, $args);
        // $time['after setMenuVars'] = Engine::getRunningTime();
        $t->v('showSearchBlock', 'false');
        $domainGroupId = DomainGroup::getDomainGroupIdByGroupIdAndDomain($groupID, $this->domain->getDomain());
        // если существует domainGroup то формируем блок текста для категории
        if ($domainGroupId) {
            $textBlock = new Template('block_group_text');
            $domainGroup = new DomainGroup($domainGroupId);
            if ($domainGroup->getDomainGroupText()) {
                $textBlock->v('domainGroupText', $domainGroup->getDomainGroupText());
                $textBlock->v('domainGroupTextBr', nl2br($domainGroup->getDomainGroupText()));
                $textBlock->v('domainGroupImg', $domainGroup->getDomainGroupImg());
                $textBlock->v('groupThumb', $domainGroup->getThumb());
                $textBlock->v('groupName', $domainGroup->getName());
                $t->v('groupTextBlock', $textBlock->get());
            } else {
                $t->v('groupTextBlock', '');
            }
        } else {
            $t->v('groupTextBlock', '');
        }
        $group = new Group($groupID);
        // редирект на правильную ссылку пока что не делаем, используем канонический адрес
        $rightUrl = $this->domain->getGroupLink($group);
        // если заходим на домен который не LastRedirectDomain, то в watchLink пишем не правильный адрес, а текущий адрес на новом домене, а уже на той странице будет прописан каноничный адрес на новом домене, это сделано, чтобы на странице вида http://site1.ru/watch/123~Test1 не был прописан каноничный адрес вида: http://site2.ru/watch/123~Test2, т.к. яндекс в статье говорит о том, что они с этим не умеют работать, и каноничные страницы должны указывать на другой домен с таким же адресом страницы.
        $t->v('groupLink', $rightUrl);
        $t->v('canonicalGroupLink', view('types.cinema.canonical-group', [
            'domain' => $this->domainEloquent,
            'groupLink' => $rightUrl,
        ]));

        $thisUrl = rawurldecode(request()->server('REDIRECT_URL', request()->getRequestUri()));

        /* редиректит при указании ?order= , учитываем это */
        if ($rightUrl !== $thisUrl and $page == 1 and ! isset($_GET['order'])) {
            F::redirect_301($rightUrl);
        }
        //
        $movies = new Movies;
        $movies->setLang($this->getLang());
        // if ($this->domain->getMoviesWithUniqueDescription()) {
        $movies->setDomain($this->domain->getDomain());
        // }
        // $movies->setOrder('cinema.year desc, cinema.added_at desc');
        $orderArr = Movies::getOrderByName($order);
        if ($orderArr) {
            $movies->setOrder($orderArr['orderBy']);
        }
        $movies->setLimit($this->domain->getMoviesLimit());
        $movies->setPage($page);
        $movies->setGroup($groupID);
        $movies->setListTemplate('items/movie');
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        $movies->setPaginationUrlPattern('/group/'.$group->getId().'/(:num)?order='.$order);
        // F::dump($movies);
        // $time['before getList'] = Engine::getRunningTime();
        $t->v('cinemaList', F::cache($movies, 'getList'));
        // $time['after getList'] = Engine::getRunningTime();
        $t->v('groupName', [
            'ru' => $group->getName(),
            'ua' => $group->getNameUa(),
            'en' => $group->getNameEn(),
        ]);
        $t->v('groupId', $group->getId());
        $t->v('groupEmoji', $group->getEmoji());
        // $time['before getPages'] = Engine::getRunningTime();
        $t->v('cinemaPages', F::cache($movies, 'getPages'));
        // $time['after getPages'] = Engine::getRunningTime();
        $t->v('btnOrderByDateActive', ($order === 'mostRecent') ? 'active' : '');
        $t->v('btnOrderByRatingActive', ($order === 'topRated') ? 'active' : '');
        $t->v('btnOrderByRecentAndTopRatedActive', ($order === 'recentAndTopRated') ? 'active' : '');
        $t->v('moviesOrder', $order);
        $t->v('page', $page);
        // популярные новинки
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setGroup($groupID);
        $movies->setDomain($this->domain->getDomain());
        $movies->setOrder('cinema.year desc, cinema.rating_sko /* del */ desc, cinema.added_at desc');
        $movies->setLimit($this->domain->getTopMoviesLimit());
        $movies->setPage(1);
        $movies->setListTemplate('items/movie_top');
        $movies->setDeleted(false);
        $movies->setDomainMovieDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        // $time['before top getList'] = Engine::getRunningTime();
        $t->v('topCinemaList', F::cache($movies, 'getList'));
        // $time['after top getList'] = Engine::getRunningTime();
        F::setPageVars($this->template);
        // $time['before setPageVars'] = Engine::getRunningTime();
        $this->setPageVars($this->template);

        // $time['after setPageVars'] = Engine::getRunningTime();
        // F::dump($time);
        return $t->get();
    }

    // вывод видео по параметрам поиска
    public function search()
    {
        $lang = empty($_REQUEST['lang']) ? 'ru' : F::checkstr($_REQUEST['lang']);
        if ($lang) {
            $this->setLang($lang);
        }
        // dump($movies->getList());
        $query = empty($_REQUEST['q']) ? null : trim(htmlspecialchars(F::checkstr($_REQUEST['q'])));
        $page = empty($_REQUEST['p']) ? 1 : F::checkstr($_REQUEST['p']);
        $type = empty($_REQUEST['type']) ? null : F::checkstr($_REQUEST['type']);
        $year = empty($_REQUEST['year']) ? null : F::checkstr($_REQUEST['year']);
        $genre = empty($_REQUEST['genre']) ? null : F::checkstr($_REQUEST['genre']);
        $country = empty($_REQUEST['country']) ? null : F::checkstr($_REQUEST['country']);
        $order = empty($_REQUEST['order']) ? 'mostRecent' : F::checkstr($_REQUEST['order']);
        $group = empty($_REQUEST['group']) ? null : F::checkstr($_REQUEST['group']);
        $leftSearch = empty($_REQUEST['leftSearch']) ? 0 : 1;
        $sameMovieId = empty($_REQUEST['sameMovieId']) ? null : F::checkstr($_REQUEST['sameMovieId']);
        $listTemplate = empty($_REQUEST['listTemplate']) ? 'movie' : (in_array($_REQUEST['listTemplate'], ['movie', 'movie2', 'movie_search']) ? $_REQUEST['listTemplate'] : 'movie');
        $yearFrom = empty($_REQUEST['yearFrom']) ? null : F::checkstr($_REQUEST['yearFrom']);
        $yearTo = empty($_REQUEST['yearTo']) ? null : F::checkstr($_REQUEST['yearTo']);
        $ratingFrom = empty($_REQUEST['ratingFrom']) ? null : F::checkstr($_REQUEST['ratingFrom']);
        $ratingTo = empty($_REQUEST['ratingTo']) ? null : F::checkstr($_REQUEST['ratingTo']);
        $typeMovie = empty($_REQUEST['typeMovie']) ? null : F::checkstr($_REQUEST['typeMovie']);
        $typeSerial = empty($_REQUEST['typeSerial']) ? null : F::checkstr($_REQUEST['typeSerial']);
        $limit = null;
        if (! empty($_REQUEST['limit']) and $_REQUEST['limit'] > 1 and $_REQUEST['limit'] < 100) {
            $limit = (int) $_REQUEST['limit'];
        }
        // F::dump($limit);
        if ($typeMovie and $typeSerial) {
            $type = null;
        } else {
            if ($typeMovie) {
                $type = $typeMovie;
            }
            if ($typeSerial) {
                $type = $typeSerial;
            }
        }
        // любой тэг
        $tag = empty($_REQUEST['tag']) ? null : F::checkstr($_REQUEST['tag']);
        $t = $this->template;
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $this->setMenuVars($t);
        $t->v('showSearchBlock', 'true');
        //
        // если сайт без фильмов то тип - сериалы
        // if (!$this->domain->getIncludeMovies()) {
        // 	$type = 1;
        // }
        // если сайт без сериалов то тип - фильмы
        // if (!$this->domain->getIncludeSerials()) {
        // 	$type = 2;
        // }
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setDomainMoviePageDeleted(false);
        // формируем строку поиска
        // пример: Фильмы и сериалы 2018 года в жанре Детектив, страна Россия
        $str['query'] = '';
        if ($query) {
            if ($leftSearch) {
                $str['query'] = 'на букву '.F::mb_ucfirst($query);
            } else {
                $str['query'] = 'по запросу: &laquo;'.$query.'&raquo;';
            }
        }
        if ($this->domain->getIncludeMovies()) {
            if ($this->domain->getIncludeSerials()) {
                $str['type'] = 'фильмы и сериалы';
            } else {
                $str['type'] = 'фильмы';
            }
        } else {
            if ($this->domain->getIncludeSerials()) {
                $str['type'] = 'сериалы';
            } else {
                $str['type'] = '';
            }
        }
        // $str['type'] = 'фильмы и сериалы';
        if ($type == 2) {
            $str['type'] = 'фильмы';
        }
        if ($type == 1) {
            $str['type'] = 'сериалы';
        }
        $str['genre'] = '';
        if ($genre) {
            $tagObj = new Tag($genre);
            $str['genre'] = 'в жанре '.$tagObj->getName();
        }
        $str['year'] = '';
        if ($year) {
            $str['year'] = $year.' года';
        }
        $str['country'] = '';
        if ($country) {
            $tagObj = new Tag($country);
            $str['country'] = 'производства '.$tagObj->getName();
        }
        $str['tag'] = '';
        if ($tag) {
            $tagObj = new Tag($tag);
            if ($tagObj->getType() == 'director') {
                $str['tag'] = 'с режиссёром '.$tagObj->getName();
            }
            if ($tagObj->getType() == 'actor') {
                $str['tag'] = 'с актёром '.$tagObj->getName();
            }
            if ($tagObj->getType() == 'studio') {
                $str['tag'] = 'студии '.$tagObj->getName();
            }
            if ($tagObj->getType() == 'country') {
                // $str['tag'] = 'производства '.$tagObj->getName();
                $str['tag'] = $tagObj->getName();
            }
            if ($tagObj->getType() == 'genre') {
                $str['tag'] = 'в жанре '.$tagObj->getName();
            }
            if ($tagObj->getType() == 'channel') {
                $str['tag'] = 'с канала '.$tagObj->getName();
            }
            // F::dump($tagObj);
            // F::dump($str);
        }
        $str['order'] = 'Все';
        if ($order == 'mostRecent') {
            // $str['order'] = 'Последние';
            $str['order'] = 'Все';
        }
        if ($order == 'topRated') {
            $str['order'] = 'Лучшие';
        }
        if ($order == 'worstRated') {
            $str['order'] = 'Худшие';
            $movies->setWithRatingSko(true);
            $movies->setWithPoster($this->domain->getMoviesWithPoster());
        }
        if ($order == 'recentAndTopRated') {
            $str['order'] = 'Популярные новинки';
        }
        $str['group'] = '';
        if ($group) {
            $groupClass = new Group($group);
            // F::dump($groupClass->getName());
            $str['group'] = $groupClass->getName();
        }
        //
        // if ($this->domain->getMoviesWithUniqueDescription()) {
        $movies->setDomain($this->domain->getDomain());
        // }
        $movies->setLimit($this->domain->getMoviesLimit());
        // F::dump($movies);
        $movies->setPage($page);
        $movies->setListTemplate('items/'.$listTemplate);
        $movies->setFulltextSearchByTitle($query);
        // $movies->setSearch($query);
        // $movies->setLeftSearch($leftSearch?true:false);
        $movies->setYear($year);
        $movies->setTag($type);
        $movies->setTag($genre);
        $movies->setTag($country);
        $movies->setTag($tag);
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        $movies->setYearFrom($yearFrom);
        $movies->setYearTo($yearTo);
        $movies->setRatingFrom($ratingFrom);
        $movies->setRatingTo($ratingTo);
        // F::dump($_REQUEST);
        // $movies->setDomainClass($this->domain);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        // $movies->setWithUniqueDescription($this->domain->getMoviesWithUniqueDescription());
        // $movies->setDomain($this->domain->getDomain());
        $orderArr = Movies::getOrderByName($order);
        // F::dump($orderArr);
        if ($orderArr) {
            $movies->setOrder($orderArr['orderBy']);
        }
        // F::dump($movies);
        // строгий поиск по тегам
        $movies->setTagsSearchStrict(true);
        $movies->setGroup($group);
        $movies->setDeleted(false);
        // $movies->setWithPoster(true);
        // похожие фильмы
        $str['same'] = '';
        if ($sameMovieId) {
            // die('');
            $movie = new DomainMovie($sameMovieId);
            // калькулируем главный плеер здесь, чтобы не было лишней нагрузки при генерации страницы просмотра (создаем другой объект, чтобы сохранялось, иначе не работает сохранение Movie через объект DomainMovie)
            // $movie2 = new Movie($movie->getId());
            // if (!$movie2->getMainPlayer()) {
            // 	$movie2->calcMainPlayer();
            // }

            // $CrawlerDetect = new CrawlerDetect;
            // $CrawlerDetect->isCrawler();
            // $isAnyBot = in_array($CrawlerDetect->getMatches(),array('Yandex','Mail.RU','StackRambler','Googlebot'))?true:false;
            // $isYandexBot = in_array($CrawlerDetect->getMatches(),array('Yandex'))?true:false;
            // if ($isAnyBot) {
            // if ($isYandexBot) {
            // $movies->setOrder('cinema.rating_sko desc');
            // } else {
            /* тут есть проблема, если теги у фильма есть, но похожие еще не расчитаны, то топ не показывается, просто пустота */
            $tags = F::cache($movie, 'getTags');
            if ($tags) {
                // если есть теги - отдаем похожие
                $movies->setExcludeMovie($movie->getId());
                $movies->setTags($tags);
                // $movies->setOrder('ct.q desc, cinema.rating_sko desc');
                // убираем сортировку по rating_sko, т.к. существенно замедляет запрос
                $movies->setOrder('ct.q desc');
                $movies->setTagsSearchStrict(false);
            } else {
                // если нет тегов - отдаем топ
                $movies->setOrder('cinema.rating_sko desc');
            }
            // }

            //
            // $movies->setTags(F::cache($movie,'getTags'));
            // $movies->setOrder('q desc, cinema.rating_sko desc');
            //
            $movies->setLimit($limit ?? 12);
            if ($sameMovieId) {
                $str['order'] = '';
                $str['same'] = 'похожие на '.$movie->getTitleRu();
            }
        }
        $t->v('searchString', F::mb_ucfirst(trim($str['order'].' '.($str['group'] ? $str['group'] : $str['type']).' '.$str['year'].' '.$str['genre'].' '.$str['country'].' '.$str['tag'].' '.$str['query'].' '.$str['same'])));
        // F::dump($movies);
        // $t->v('cinemaList',cache($movies,'getList'));
        $searchLeftStr = $leftSearch ? '&amp;leftSearch='.$leftSearch : '';
        $movies->setPaginationUrlPattern('/search?q='.$query.'&amp;group='.$group.'&amp;year='.$year.'&amp;genre='.$genre.'&amp;type='.$type.'&amp;country='.$country.'&amp;tag='.$tag.'&amp;order='.$order.$searchLeftStr.'&amp;yearFrom='.$yearFrom.'&amp;yearTo='.$yearTo.'&amp;ratingFrom='.$ratingFrom.'&amp;ratingTo='.$ratingTo.'&amp;sameMovieId='.$sameMovieId.'&amp;p=(:num)');
        $list = F::cache($movies, 'getList');
        // $list = $movies->getList();
        $t->v('cinemaList', $list ? $list : '<p class="nothing-found">Ничего не найдено</p>');
        $t->v('searchLeftStr', $searchLeftStr);
        // if ($sameMovieId and $isYandexBot) {
        // 	$movies->setPaginationUrlPattern('/');
        // } else {

        // }
        // $movies->setPaginationUrlPattern('/search?q='.$query.'&p=(:num)');
        $t->v('cinemaPages', F::cache($movies, 'getPages'));
        // $t->v('cinemaPages',$movies->getPages());
        $t->v('searchType', $type);
        $t->v('searchYear', $year);
        $t->v('searchGenre', $genre);
        $t->v('searchCountry', $country);
        $t->v('searchOrder', $order);
        $t->v('searchGroup', $group);
        $t->v('searchTag', $tag);
        $t->v('searchSameMovieId', $sameMovieId);
        $t->v('btnOrderByDateActive', ($order === 'mostRecent') ? 'active' : '');
        $t->v('btnOrderByRatingActive', ($order === 'topRated') ? 'active' : '');
        $t->v('btnOrderByRecentAndTopRatedActive', ($order === 'recentAndTopRated') ? 'active' : '');
        $t->v('page', $page);
        // $searchText['']
        F::setPageVars($this->template);
        $this->setPageVars($this->template);

        return $t->get();
    }

    // страница просмотра видео
    public function watch($args = [])
    {
        /* костыль: эпизоды смотрят в этот же контроллер, через laravel роутер, поэтому $args - будет являться ID эпизода */
        $episode_id = is_numeric($args) ? $args : null;
        // $episode_id = empty($args['episode_id']) ? null : F::checkstr($args['episode_id']);
        $lang = empty($args['lang']) ? null : $args['lang'];
        $season = empty($args['season']) ? null : $args['season'];
        if ($lang) {
            $this->setLang($lang);
        }
        // F::dump($args);
        $t = $this->template;

        if ($episode_id) {
            $episode = $this->domainEloquent->episodes()->find($episode_id);
            if (! $episode) {
                F::error('Episode doesn\'t exists for this domain');
            }
            $movieEloquent = $this->domainEloquent
                ->movies()
                ->whereMovieId($episode->movie->id)
                ->first();
            $movie = $movieEloquent->megaweb;
        } else {
            $id = empty($args['movieID']) ? F::error('Specify video ID') : F::checkstr($args['movieID']);
            if ($this->domain->getUseMovieId()) {
                $domainMovieId = DomainMovieEloquent::whereDomain($this->domain->getDomain())->whereMovieId($id)->firstOrFail()->id;
            } else {
                $domainMovieId = $id;
            }
            $movieEloquent = DomainMovieEloquent::findOrFail($domainMovieId);
            $movie = $movieEloquent->megaweb;

        }

        /* необходимые переменные для эпизода */
        $t->v('episodeNumber', $episode->episode ?? null);
        $t->v('seasonNumber', $episode->season ?? null);
        $t->v('episodeId', $episode->id ?? null);

        if (isset($episode)) {
            $t->v('episodeLink', $movieEloquent->episodeViewRoute($episode));
            $t->v('canonicalEpisodeLink', view('types.cinema.canonical-episode', [
                'domain' => $this->domainEloquent,
                'movie' => $movieEloquent,
                'episode' => $episode,
            ]));
        } else {
            $t->v('episodeLink', '');
            $t->v('canonicalEpisodeLink', '');
        }

        $this->setMenuVars($t);
        $t->v('showSearchBlock', 'false');
        // если не правильный домен или видео не активно - уходим
        if ($movie->getDomain() != $this->domain->getDomain()) {
            F::error('Movie not found');
        }
        if (! $movie->getDomainMovieActive()) {
            logger()->info('Запрошено удаленное видео, редиректим на главную.');

            return redirect('/');
        }
        if ($movie->getDeleted()) {
            // просто скрываем плеер
            // F::error('Movie has been deleted');
        }
        // редирект на правильный URL
        // if ($lang == 'en') {
        // 	$rightUrl = $this->domain->getWatchLink($movie,$this->domain->getWatchLinkEnTemplate());
        // } else {

        $rightUrl = $this->domain->getWatchLink($movie);

        // }
        // F::dump($rightUrl);
        // если заходим на домен который не LastRedirectDomain, то в watchLink пишем не правильный адрес, а текущий адрес на новом домене, а уже на той странице будет прописан каноничный адрес на новом домене, это сделано, чтобы на странице вида http://site1.ru/watch/123~Test1 не был прописан каноничный адрес вида: http://site2.ru/watch/123~Test2, т.к. яндекс в статье говорит о том, что они с этим не умеют работать, и каноничные страницы должны указывать на другой домен с таким же адресом страницы.
        // $t->v('watchLinkEn',$this->domain->getWatchLink($movie,$this->domain->getWatchLinkEnTemplate()));
        // $t->v('watchLinkRu',$this->domain->getWatchLink($movie));
        $t->v('watchLink', $rightUrl);
        $t->v('canonicalWatchLink', view('types.cinema.canonical-watch', [
            'domain' => $this->domainEloquent,
            'watchLink' => $rightUrl,
        ]));

        $thisUrl = rawurldecode(request()->server('REDIRECT_URL', request()->getRequestUri()));

        if (! isset($episode) and $lang == null and $rightUrl !== $thisUrl and Engine::isRealBot()) {
            logger()->info('Скрытый редирект на '.$rightUrl);

            return redirect($rightUrl, 301);
        }

        $cloneDomain = clone $this->domain;
        $cloneDomain->setLang('ru');
        $t->v('watchLinkRu', $cloneDomain->getWatchLink($movie));
        $cloneDomain->setLang('ua');
        $t->v('watchLinkUa', $cloneDomain->getWatchLink($movie));
        $cloneDomain->setLang('en');
        $t->v('watchLinkEn', $cloneDomain->getWatchLink($movie));

        /* если можно показывать (отмодерирован), то показываем, иначе не показываем */
        if ($movie->getDomainMovieDescription() and $movie->getDomainMovieShowToCustomer()) {
            $domainMovieDescription = $movie->getDomainMovieDescription();
        } else {
            $domainMovieDescription = '';
        }
        $t->v('domainMovieDescription', $domainMovieDescription);
        $t->v('domainMovieId', $movie->getDomainMovieId());
        $t->v('domainMovieDescriptionShort', mb_strimwidth((string) $domainMovieDescription, 0, 160, '...'));
        $t->v('domainMovieDescriptionExtraShort', mb_strimwidth((string) $domainMovieDescription, 0, 100, '...'));
        $t->v('domainMovieDescriptionWriter', $movie->getDomainMovieDescriptionWriter());
        $t->v('movieDescriptionOrDomainMovieDescription', $domainMovieDescription ? $domainMovieDescription : $movie->getDescription());
        $t->v('movieDescriptionOrDomainMovieDescriptionBr', nl2br((string) ($domainMovieDescription ? $domainMovieDescription : $movie->getDescription())));
        $t->v('movieDescriptionOrDomainMovieDescriptionJson', json_encode($domainMovieDescription ? $domainMovieDescription : $movie->getDescription()));
        $t->v('movieDescriptionOrDomainMovieDescriptionShort', $domainMovieDescription ? mb_strimwidth((string) $domainMovieDescription, 0, 160, '...') : mb_strimwidth((string) $movie->getDescription(), 0, 160, '...'));
        $t->v('movieDescriptionOrDomainMovieDescriptionExtraShort', $domainMovieDescription ? mb_strimwidth((string) $domainMovieDescription, 0, 100, '...') : mb_strimwidth((string) $movie->getDescription(), 0, 100, '...'));
        $domainOrLordOrMovieDescription = $domainMovieDescription ? $domainMovieDescription : ($movie->getLordDescription() ? $movie->getLordDescription() : $movie->getDescription());
        $t->v('domainOrLordOrMovieDescription', $domainOrLordOrMovieDescription);
        $t->v('domainOrLordOrMovieDescriptionBr', nl2br((string) $domainOrLordOrMovieDescription));
        $t->v('domainOrLordOrMovieDescriptionExtraShort', mb_strimwidth((string) $domainOrLordOrMovieDescription, 0, 100, '...'));
        // {lordOrDomainOrMovieDescription}
        // если стоит rknForRuOnly то блочим все кроме Украины. Если нет, то всех
        $rknGeoBlock = $this->domain->getRknForRuOnly() ? (in_array(Engine::getClientCountry(), ['UA']) ? false : true) : true;
        // $rknIpBlock = Engine::isClientRkn();
        $rknIpBlock = (Engine::getClientBan() and Engine::getClientBan()->hide_player) ? true : false;
        // F::dump($rknIpBlock);
        $t->v('domainMovieDeleted', (($movie->getDomainMovieDeleted() and $rknGeoBlock) or $rknIpBlock) ? 1 : 0);
        $t->v('movieMainPlayer', $movie->getMainPlayer());
        // $t->v('movieRndPlayer',$movie->getRndPlayer());
        $t->v('movieRndPlayer', (new FirstPlayer(
            $movieEloquent->movie,
            /* если для фильма указан mainPlayer - используем его, если нет, то main_player сайта, если нет, то main_player общий */
            $movieEloquent->movie->mainPlayer ?? ($this->domainEloquent->main_player ?? Config::cached()->main_player),
            Engine::getClientCountry(),
            $this->domainEloquent->show_trailer_for_non_sng
        ))->get());
        // if (Engine::getClientCountry() == 'RU' and $moonwalk->getBlockRu()) {
        // раскомментировать когда начнутся массовые запросы на удаление.
        // $player = 'Видео было удалено для России по запросу правообладателей';
        // }
        // if (Engine::getClientCountry() == 'UA' and $moonwalk->getBlockUa()) {
        // $player = 'Видео было удалено для Украины по запросу правообладателей';
        // }
        $lastEpisodeAirDate = $movie->getLastEpisodeAirDate();
        if ($lastEpisodeAirDate) {
            $t->v('movieUpdatedAt', (new \DateTime($lastEpisodeAirDate))->format('d.m.Y'));
        } else {
            $t->v('movieUpdatedAt', '?');
        }
        $addDate = new \DateTime($movie->getDomainMovieAddDate());
        $t->v('movieAddDate', $addDate->format('d.m.Y'));
        // для структурированных данных Movie
        $t->v('movieAddDate2', $addDate->format('Y-m-dTH:i:s'));
        if ($movie->getEmbed()) {
            $playerLegal = '{players/legal.html}';
        } else {
            $playerLegal = '';
        }
        if ($movie->getType() == 'trailer' and $movie->getDirectLink()) {
            $playerDirect = '{players/direct.html}';
        } else {
            $playerDirect = '';
        }
        // if ($movie->getType() == 'trailer' and $movie->getEmbedTrailer()) {
        // 	$playerTrailer = '{players/trailer.html}';
        // } else {
        // 	$playerTrailer = '';
        // }
        // объявляем переменные плееров
        $playerMnw = '';
        $playerHdg = '';
        $playerHdb = '';
        $playerKodik = '';
        $playerKodikEn = '';
        $playerVideoCdn = '';
        $playerVideoCdnEn = '';
        $playerCollaps = '';
        $playerHdvb = '';
        $playerLegalAlt = '';
        $playerMnwTrailer = '';
        $playerYohoho = '';
        $playerVideodb = '';
        $playerIframeVideo = '';
        $playerVideospider = '';
        $playerAllohatv = '';
        $playerYoutube = '';
        $playerTrailer = '';
        // тут проверяем если домен содержит опцию "удалять плеер только на странице с жалобой": проверяем есть ли текущая ссылка в abuses, если есть, не выводим плеер, если нет, то выводим.
        $deletedBecauseRknAbuseOnPage = false;
        if ($this->domainEloquent->rkn_scheme == 'delete_player_only_if_abuse_on_page') {
            $deletedBecauseRknAbuseOnPage = Abuse::isRknUriOfActualCrawlerDomainsExists($this->domainEloquent->engineDomain, request()->getRequestUri()) ? true : false;
        }
        if ($this->domainEloquent->rkn_scheme == 'delete_player_if_ru_and_abuse_on_page') {
            if (Engine::getClientCountry() == 'RU' and Abuse::isRknUriOfActualCrawlerDomainsExists($this->domainEloquent->engineDomain, request()->getRequestUri())) {
                $deletedBecauseRknAbuseOnPage = true;
            }
        }
        $deletedBecauseRuAndNotCanonical = false;
        if ($this->domainEloquent->rkn_scheme == 'delete_player_when_ru_and_not_canonical') {
            /* если РФ и запрошена не каноническая страница, то не показываем плеер */
            if (Engine::getClientCountry() == 'RU' and (Abuse::formatUrl(request()->getRequestUri()) != Abuse::formatUrl($rightUrl))) {
                // dd('не показываем плеер. ру и не каноникал');
                $deletedBecauseRuAndNotCanonical = true;
            }
        }
        // Не отображаем плеер, (если стоит общее удаление и сайт не забанен, либо стоит удаление локальное) + не указано обратное в geo и ip блоках
        if (((($movie->getDeleted() and ! $this->domainEloquent->engineDomain->every_actual_crawler_mirror_in_known_ru_block) or ($movie->getDomainMovieDeleted() or $deletedBecauseRknAbuseOnPage or $deletedBecauseRuAndNotCanonical)) and $rknGeoBlock) or $rknIpBlock) {
            $t->v('movieBlockedMessage', [
                'ru' => 'Видео было удалено по запросу правообладателей 😟',
                'ua' => 'Відео було видалено через запитом правовласників 😟',
                'en' => 'The video was removed due to a request from the copyright holders 😟',
            ]);
        } else {
            if ($movie->getDomainMovieBlockForeigners() and ! in_array(Engine::getClientCountry(), ['RU', 'UA', 'KZ', 'BY'])) {
                $t->v('movieBlockedMessage', 'Видео недоступно 😟');
            } else {
                $playerMnw = '';
                $playerMnwTrailer = '';
                $playerHdg = '{players/hdgo.html}';
                $playerHdb = '{players/hdbaza.html}';
                $playerKodik = '{players/kodik.html}';
                $playerKodikEn = '{en/players/kodik.html}';
                $playerVideoCdn = '{players/videocdn.html}';
                $playerVideoCdnEn = '{en/players/videocdn.html}';
                $playerCollaps = '{players/collaps.html}';
                $playerYohoho = '{players/yohoho.html}';
                $playerVideodb = '{players/videodb/iframe.html}';
                $playerIframeVideo = '{players/iframe_video.html}';
                $playerVideospider = '{players/videospider.html}';
                $playerAllohatv = '{players/allohatv.html}';
                // $playerYoutube = '{players/youtube.html}';
                $playerYoutube = $movie->getYoutubePlayer();
                $playerHdvb = $movie->getHdvbPlayer();
                $playerLegalAlt = F::cache($movie, 'getLegalPlayer');
                $playerTrailer = view('types.cinema.players.trailer')->with('movie', $movieEloquent);
                $t->v('movieBlockedMessage', '');
            }
        }
        // посмотреть на другом сайте
        if ($movie->getDomainMovieDeleted()) {
            $altLink = F::cache($movie, 'getOtherSiteLink');
            // F::dump($altLink);
            $t->v('altMovieLink', $altLink ? $altLink : '');
        } else {
            $t->v('altMovieLink', '');
        }
        if ($movie->getRknBlock() or $movie->getDomainMovieDeletedPage()) {
            F::error('Страница была удалена из-за жалоб правообладателя');
        }
        $t->v('movieDirectLink', $movie->getDirectLink());
        $t->v('moviePlayerDirect', $playerDirect);
        $t->v('movieEmbed', $movie->getEmbed());
        $t->v('moviePlayerLegal', $playerLegal);
        $t->v('moviePlayerLegalAlt', $playerLegalAlt);
        $t->v('movieEmbedTrailer', $movie->getEmbedTrailer());
        $t->v('moviePlayerTrailer', $playerTrailer);
        $t->v('moviePlayerMnw', $playerMnw);
        $t->v('moviePlayerMnwTrailer', $playerMnwTrailer);
        $t->v('moviePlayerHdg', $playerHdg);
        $t->v('moviePlayerHdb', $playerHdb);
        $t->v('moviePlayerKodik', $playerKodik);
        $t->v('moviePlayerKodikEn', $playerKodikEn);
        $t->v('moviePlayerVideoCdn', $playerVideoCdn);
        $t->v('moviePlayerVideoCdnEn', $playerVideoCdnEn);
        $t->v('moviePlayerCollaps', $playerCollaps);
        $t->v('moviePlayerHdvb', $playerHdvb);
        $t->v('moviePlayerYohoho', $playerYohoho);
        // чтобы появлялся только фейковый плеер
        // $t->v('moviePlayerVideodb',($movie->getVideodb()?'':$playerVideodb));
        // чтобы videodb появлялся только тогда, как нет hdvb и alloha
        // $t->v('moviePlayerVideodb',($movie->getAlloha() or $movie->getHdvb())?'':$playerVideodb);
        $t->v('moviePlayerVideodb', $playerVideodb);
        // чтобы videodb появлялся всегда
        // $t->v('moviePlayerVideodb',$playerVideodb);
        $t->v('moviePlayerIframeVideo', $playerIframeVideo);
        $t->v('moviePlayerVideospider', $playerVideospider);
        $t->v('moviePlayerAllohatv', $playerAllohatv);
        $t->v('moviePlayerYoutube', $playerYoutube);
        $t->v('movieHdvb4k', $movie->getHdvb4k() ? 'true' : 'false');
        // dump($moonwalk->getData());
        $t->v('movieId', $movie->getId());
        $t->v('movieTitle', [
            'ru' => $movie->getTitleRu(),
            'ua' => $movie->getTitleRu(),
            'en' => $movie->getTitleEnIfPossible(),
        ]);
        $t->v('movieTitleRu', $movie->getTitleRu());
        // для блока movie-data
        // F::dump(json_encode($movie->getTitleRu()));
        $t->v('movieTitleRuJson', json_encode($movie->getTitleRu(), JSON_UNESCAPED_UNICODE));
        $t->v('movieTitleJson', json_encode($t->getVar('movieTitle'), JSON_UNESCAPED_UNICODE));
        $t->v('movieTitleRuUrl', $movie->getTitleRuUrl());
        $t->v('movieTitleEnUrl', $movie->getTitleEnUrl());
        $t->v('movieTitleEn', $movie->getTitleEn());
        $t->v('movieTitleEnIfPossible', $movie->getTitleEnIfPossible());
        $t->v('movieTitleEnIfExist', empty($movie->getTitleEn()) ? '' : '/ '.$movie->getTitleEn());
        $t->v('moviePoster', $movie->getPosterOrPlaceholder());
        $t->v('moviePosterWebP', $movie->getPosterWebPOrPlaceholder());
        $t->v('movieTypeVar', $movie->getType());
        if ($movie->getType() == 'trailer') {
            $year = $movie->getYear() ? $movie->getYear() : date('Y');
            $t->v('movieYear', $year);
            $t->v('movieType', ['ru' => 'Фильм', 'ua' => 'Фiльм', 'en' => 'Movie']);
            $t->v('movieTypeEn', 'Movie');
            $t->v('movieLastYearsOrNothing', in_array($year, [date('Y'), date('Y') - 1]) ? $year : '');
        } else {
            $t->v('movieYear', $movie->getYear());
            $t->v('movieType', [
                'ru' => $movie->getTypeRu(true),
                'ua' => $movie->getTypeUa(true),
                'en' => $movie->getTypeEn(true),
            ]);
            $t->v('movieTypeEn', $movie->getTypeEn(true));
            $t->v('movieLastYearsOrNothing', in_array($movie->getYear(), [date('Y'), date('Y') - 1]) ? $movie->getYear() : '');
        }
        $t->v('movieImdbRating', $movie->getImdbRating() ? $movie->getImdbRating() : 0);
        $t->v('movieImdbVotes', $movie->getImdbVotes() ? $movie->getImdbVotes() : 0);
        $t->v('movieKinopoiskRating', $movie->getKinopoiskRating() ? $movie->getKinopoiskRating() : 0);
        $t->v('movieKinopoiskVotes', $movie->getKinopoiskVotes() ? $movie->getKinopoiskVotes() : 0);
        $t->v('movieRatingSko', $movie->getRatingSko() ? round($movie->getRatingSko(), 1) : 0);
        $t->v('movieRatingVotes', $movie->getKinopoiskVotes() + $movie->getImdbVotes());
        $t->v('movieDescription', $movie->getDescription());
        $t->v('movieKinopoiskId', $movie->getKinopoiskId());
        $t->v('movieImdbId', $movie->getImdbId());
        $t->v('movieStatus', $movie->getStatusRu());
        $t->v('movieStarted', $movie->getStarted() ? ((new \DateTime($movie->getStarted()))->format('d.m.Y')) : '');
        $t->v('movieEnded', $movie->getEnded() ? ((new \DateTime($movie->getEnded()))->format('d.m.Y')) : '');
        $t->v('movieAge', $movie->getAge() ? ($movie->getAge().'+') : '');
        if ($this->domainEloquent->episodically) {
            $movie->setEpisodesDataListTemplate('items/list_episodes_data_with_links');
            $t->v('movieEpisodesDataList', F::cache($movie, 'getEpisodesDataList'));
        } else {
            /* если нужен список серий без ссылки на домен, то нужно вызывать getEpisodesDataList из объекта Movie а не DomainMovie */
            $simpleMovie = new Movie($movie->getId());
            $simpleMovie->setEpisodesDataListTemplate('items/list_episodes_data');
            $t->v('movieEpisodesDataList', F::cache($simpleMovie, 'getEpisodesDataList'));
        }
        $t->v('movieEpisodesSum', F::cache($movie, 'getEpisodesSum'));
        $t->v('movieBlockStr', $movie->getBlockStr());
        $t->v('movieTagline', $movie->getTagline());
        // собираем блок статей к фильму
        $articlesList = F::cache($movie, 'getDomainMovieArticlesList');
        // $articlesList = $movie->getDomainMovieArticlesList();
        if ($articlesList) {
            $articlesBlockTemplate = new Template('block-movie-articles');
            $articlesBlockTemplate->v('list', $articlesList);
            $articlesBlockTemplate->v('movieType', $movie->getTypeRu());
            $articlesBlockTemplate->v('movieTitleRu', $movie->getTitleRu());
            $t->v('movieArticlesBlock', $articlesBlockTemplate->get());
        } else {
            $t->v('movieArticlesBlock', '');
        }
        // dump(Engine::getRunningTime());
        $mt = new MovieTags($movie->getId());
        $mt->setLang($this->getLang());
        $mt->setDomain($this->domain->getDomain());
        $mt->setListTemplate('items/list_tag_genre');
        // $t->v('movieGenres',F::cache($mt,'getGenres'));
        $t->v('movieGenres', F::cache($mt, 'getList', ['arr' => $movieEloquent->movie->tags->where('type', 'genre')]));
        //
        $mt = new MovieTags($movie->getId());
        $mt->setLang($this->getLang());
        $mt->setDomain($this->domain->getDomain());
        $mt->setListTemplate('items/list_tag_actor');
        // $t->v('movieActors',F::cache($mt,'getActors'));
        $t->v('movieActors', F::cache($mt, 'getList', ['arr' => $movieEloquent->movie->tags->where('type', 'actor')]));
        //
        $mt = new MovieTags($movie->getId());
        $mt->setLang($this->getLang());
        $mt->setDomain($this->domain->getDomain());
        $mt->setListTemplate('items/list_tag');
        // $t->v('movieStudios',F::cache($mt,'getStudios'));
        $t->v('movieStudios', F::cache($mt, 'getList', ['arr' => $movieEloquent->movie->tags->where('type', 'studio')]));
        //
        $mt = new MovieTags($movie->getId());
        $mt->setLang($this->getLang());
        $mt->setDomain($this->domain->getDomain());
        $mt->setListTemplate('items/list_tag');
        // $t->v('movieDirectors',F::cache($mt,'getDirectors'));
        $t->v('movieDirectors', F::cache($mt, 'getList', ['arr' => $movieEloquent->movie->tags->where('type', 'director')]));
        //
        $mt = new MovieTags($movie->getId());
        $mt->setLang($this->getLang());
        $mt->setDomain($this->domain->getDomain());
        $mt->setListTemplate('items/list_tag_country');
        $mt->setActive(true);
        // $t->v('movieCountries',F::cache($mt,'getCountries'));
        $t->v('movieCountries', F::cache($mt, 'getList', ['arr' => $movieEloquent->movie->tags->where('active', true)->where('type', 'country')]));
        //
        $mt = new MovieTags($movie->getId());
        $mt->setLang($this->getLang());
        $mt->setDomain($this->domain->getDomain());
        $mt->setListTemplate('items/list_tag_text');
        // $t->v('movieCountriesText',F::cache($mt,'getCountries'));
        $t->v('movieCountriesText', F::cache($mt, 'getList', ['arr' => $movieEloquent->movie->tags->where('type', 'country')]));
        //
        $mt = new MovieTags($movie->getId());
        $mt->setLang($this->getLang());
        $mt->setDomain($this->domain->getDomain());
        $mt->setListTemplate('items/list_tag_text');
        // $t->v('movieDirectorsText',F::cache($mt,'getDirectors'));
        // $t->v('movieDirectorsTextJson',json_encode(F::cache($mt,'getDirectors'),JSON_UNESCAPED_UNICODE));
        $t->v('movieDirectorsText', F::cache($mt, 'getList', ['arr' => $movieEloquent->movie->tags->where('type', 'director')]));
        $t->v('movieDirectorsTextJson', json_encode(F::cache($mt, 'getList', ['arr' => $movieEloquent->movie->tags->where('type', 'director')]), JSON_UNESCAPED_UNICODE));
        $groups = new Groups;
        $groups->setLang($this->getLang());
        $groups->setMovieId($movie->getId());
        $groups->setActive(true);
        $groups->setListTemplate('items/list_group');
        $groups->setDomain($this->domain->getDomain());
        $groups->setUseDomainGroup(false);
        $t->v('movieGroups', F::cache($groups, 'getList', ['arr' => $movieEloquent->groups->where('active', true)]));
        // получаем список групп - жанров
        $groups = new Groups;
        $groups->setLang($this->getLang());
        $groups->setMovieId($movie->getId());
        $groups->setActive(true);
        $groups->setListTemplate('items/list_group');
        $groups->setType('genre');
        $groups->setDomain($this->domain->getDomain());
        $groups->setUseDomainGroup(false);
        // $t->v('movieGroupsGenres',F::cache($groups,'getList'));
        $t->v('movieGroupsGenres', F::cache($groups, 'getList', ['arr' => $movieEloquent->groups->where('active', true)->where('type', 'genre')]));
        // получаем список групп - озвучек
        $groups = new Groups;
        $groups->setLang($this->getLang());
        $groups->setMovieId($movie->getId());
        // $groups->setActive(true);
        $groups->setListTemplate('items/list_group');
        $groups->setType('translator');
        $groups->setDomain($this->domain->getDomain());
        $groups->setUseDomainGroup(false);
        // $t->v('movieGroupsTranslators',F::cache($groups,'getList'));
        $t->v('movieGroupsTranslators', F::cache($groups, 'getList', ['arr' => $movieEloquent->groups->where('type', 'translator')]));
        // foreach (F::cache($movie,'getTranslators') as $translator) {
        // 	$tTranslator = new Template('items/list_tag');
        // 	$tTranslator->v('name',$);
        // }
        $t->v('movieTranslators', implode(', ', F::cache($movie, 'getTranslators')));
        $t->v('screenshots', ($movie->getType() === 'movie') ? '{block_screenshots.html}' : '');
        // находим похожие фильмы и сериалы если нужно
        if ($this->domain->getWatchCalcSameMovies()) {
            $movieTags = F::cache($movie, 'getTags');
            // если тегов нет, то похожие видео не просчитываем т.к. возникает ошибка из-за сортировки q desc
            if ($movieTags) {
                $movies = new Movies;
                $movies->setLang($this->getLang());
                // if ($this->domain->getMoviesWithUniqueDescription()) {
                $movies->setDomain($this->domain->getDomain());
                // }
                $movies->setTags($movieTags);
                // чтобы не выдавался этот же ролик
                $movies->setExcludeMovie($movie->getId());
                $movies->setLimit(12);
                $movies->setPage(1);
                $movies->setListTemplate('items/movie');
                $movies->setOrder('q desc, cinema.rating_sko desc');
                $movies->setWithPoster($this->domain->getMoviesWithPoster());
                $movies->setIncludeSerials($this->domain->getIncludeSerials());
                $movies->setIncludeMovies($this->domain->getIncludeMovies());
                $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
                $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
                $movies->setActive(true);
                $movies->setDomainMovieDeleted(false);
                $movies->setDomainMoviePageDeleted(false);
                // $movies->setDomainClass($this->domain);
                if ($this->domain->getCalcThumbGenresList()) {
                    $movies->setGenresListTemplate('items/movie_genre');
                }
                // F::dump($movies);
                // $movies->setWithUniqueDescription($this->domain->getMoviesWithUniqueDescription());
                // $movies->setDomain($this->domain->getDomain());
                $t->v('sameMoviesList', F::cache($movies, 'getList', null, 604800));
            } else {
                $t->v('sameMoviesList', '<p class="nothing-found">Ничего не найдено</p>');
            }
        }
        // если это сериал, получаем последний сезон и последние эпизоды
        if ($movie->getType() == 'serial') {
            $lastSeason = $movie->getLastSeason();
            if ($lastSeason) {
                $t->v('movieLastSeason', $lastSeason);
                $t->v('movieLastEpisodes', implode(', ', $movie->getLastEpisodesOfSeason($lastSeason, 3)));
            } else {
                $t->v('movieLastSeason', 1);
                $t->v('movieLastEpisodes', 1);
            }
            $t->v('movieEpisodes', F::cache($movie, 'getEpisodesList'));
        } else {
            $t->v('movieLastSeason', '');
            $t->v('movieLastEpisodes', '');
            $t->v('movieEpisodes', '');
        }
        // собираем блок комментариев
        $comments = new MovieComments;
        $comments->setLang($this->getLang());
        $comments->setDomainMovieId($movie->getDomainMovieId());
        $comments->setListTemplate('items/movie_comment');
        $t->v('commentsList', $comments->getList());
        $t->v('commentsCount', $comments->getRows());
        // $t->v('movieType',mb_convert_case($movie->getTypeRu(), MB_CASE_TITLE, 'UTF-8'));
        $t->v('topCinemaList', $this->topCinemaList());
        //
        // $seasons = new MovieSeasons;
        // $seasons->setMovie($movie);
        // $t->v('seasonsList',$seasons->getSeasonsList());
        //
        F::setPageVars($this->template);
        $this->setPageVars($this->template);
        //
        // рекламный блок в зависимости от региона
        // F::dump(Engine::getClientCountry());
        $t->v('shakes_ru_teasernet_other', (Engine::getClientCountry() === 'RU') ? '{promo/shakes.pro/player-right.html}' : '{promo/teasernet.com/player-right.html}');
        $t->v('videodbIframeDomain', Videodb::getIframeDomain());
        $t->v('videodbIframeSign', Videodb::getIframeSign($movie->getId()));
        $t->v('videocdnDomain', (new Cinema)->getVideocdnDomain());
        $t->v('recaptchaClientKey', $this->domain->getRecaptchaClientKey());
        $t->v('currentSeason', $season);
        if ($movie->getYoutubeData()) {
            $youtube = $movie->getYoutubeData();
            $t->v('movieYoutubeId', $youtube->getId());
            $t->v('movieYoutubeThumbnail', $youtube->getData()->snippet->thumbnails->medium->url);
            $t->v('movieYoutubePoster', $youtube->getData()->snippet->thumbnails->maxres->url ?? $youtube->getData()->snippet->thumbnails->high->url);
            // $t->v('video_title',$youtube->getData()->snippet->title);
            // $t->v('video_title_escaped',F::escape_string($youtube->getData()->snippet->title));
            $t->v('movieYoutubeDescription', nl2br($youtube->getDescriptionModified()));
            $t->v('movieYoutubeDescriptionShortOrTitle', str_replace(["\r", "\n"], '', mb_strimwidth($youtube->getDescriptionModified() ?? $youtube->getData()->snippet->title, 0, 150, '...')));
            $t->v('movieYoutubeDurationIso8601', $youtube->getData()->contentDetails->duration);
            $t->v('movieYoutubePublishedAtIso8601', $youtube->getData()->snippet->publishedAt);
            // F::dump($category);
        }
        // F::dump(Engine::getRequestedDomainObject()->getDomain());
        // $t->v('hidePlayerOnTurboserialCom',(Engine::getRequestedDomainObject()->getDomain() == 'turboserial.com')?'<style>.playerBlock {display: none;}</style>':'');
        // F::dump($t);
        // разметка видео
        $ldJsonVideoObject = new \stdClass;
        $ldJsonVideoObject->{'@context'} = 'https://schema.org';
        $ldJsonVideoObject->{'@type'} = 'VideoObject';
        $ldJsonVideoObject->name = $t->getVar('movieTitle');
        $ldJsonVideoObject->description = $t->getVar('movieDescriptionOrDomainMovieDescription') ? $t->getVar('movieDescriptionOrDomainMovieDescription') : $t->getVar('movieTitle');
        $ldJsonVideoObject->thumbnailUrl = [Engine::getDomainObject()->getProtocol().'://'.Engine::getDomainObject()->getLastRedirectDomain().$movie->getPosterOrPlaceholder()];
        $ldJsonVideoObject->uploadDate = $t->getVar('movieAddDate2');
        $ldJsonVideoObject->embedUrl = 'https://'.Videodb::getIframeDomain().'/?r=Videodb/getPlayer&id='.$movie->getId().'&b='.$movie->getBlockStr();
        $ldJsonMovie = new \stdClass;
        $ldJsonMovie->{'@context'} = 'https://schema.org';
        $ldJsonMovie->{'@type'} = 'Movie';
        $ldJsonMovie->name = $t->getVar('movieTitle');
        $ldJsonMovie->image = Engine::getDomainObject()->getProtocol().'://'.Engine::getDomainObject()->getLastRedirectDomain().$movie->getPosterOrPlaceholder();
        $ldJsonMovie->dateCreated = $t->getVar('movieAddDate2');
        $ldJsonMovie->url = Engine::getDomainObject()->getProtocol().'://'.Engine::getDomainObject()->getLastRedirectDomain().$t->getVar('watchLink');
        if ($t->getVar('movieDirectorsText')) {
            $ldJsonMovie->director = new \stdClass;
            $ldJsonMovie->director->{'@type'} = 'Person';
            $ldJsonMovie->director->name = $t->getVar('movieDirectorsText');
        }
        if ($t->getVar('movieRatingVotes')) {
            $ldJsonMovie->aggregateRating = new \stdClass;
            $ldJsonMovie->aggregateRating->{'@type'} = 'AggregateRating';
            $ldJsonMovie->aggregateRating->bestRating = 10;
            $ldJsonMovie->aggregateRating->worstRating = 0;
            $ldJsonMovie->aggregateRating->ratingValue = $t->getVar('movieRatingSko');
            $ldJsonMovie->aggregateRating->ratingCount = $t->getVar('movieRatingVotes');
            // $ldJsonMovie->aggregateRating->reviewCount = 10;
        }
        // F::dump($ldJsonMovie);
        $t->v('ldJsonMovie', json_encode($ldJsonMovie, JSON_UNESCAPED_UNICODE));
        $t->v('ldJsonVideoObject', json_encode($ldJsonVideoObject, JSON_UNESCAPED_UNICODE));
        $t->v('hreflang', $this->getHreflang($this->domain, $rightUrl));

        return $t->get(data: ['domainMovie' => $movieEloquent]);
    }

    // вывод видео для тега жанра
    public function genreCinema($args = [])
    {
        $tagId = empty($args['tagId']) ? F::error('Enter tagId') : F::checkstr($args['tagId']);
        if ($tagId == 3614) {
            F::error('Раздел не существует');
        }
        $lang = empty($args['lang']) ? null : $args['lang'];
        if ($lang) {
            $this->setLang($lang);
        }
        $page = empty($args['page']) ? 1 : F::checkstr($args['page']);
        $order = empty($_GET['order']) ? 'recentAndTopRated' : F::checkstr($_GET['order']);
        $t = $this->template;
        // $time['before setMenuVars'] = Engine::getRunningTime();
        $this->setMenuVars($t, $args);
        // $time['after setMenuVars'] = Engine::getRunningTime();
        $t->v('showSearchBlock', 'false');
        $tag = new Tag($tagId);
        $tag->setDomain($this->domain->getDomain());
        // редирект на правильную ссылку
        $rightUrl = $this->domain->getGenreLink($tag);
        $t->v('genreLink', $rightUrl);
        $t->v('canonicalGenreLink', view('types.cinema.canonical-genre', [
            'domain' => $this->domainEloquent,
            'genreLink' => $rightUrl,
        ]));
        // f::dump($rightUrl);
        $thisUrl = rawurldecode(request()->server('REDIRECT_URL', request()->getRequestUri()));
        if ($rightUrl !== $thisUrl and $page == 1) {
            F::redirect_301($rightUrl);
        }
        //
        $movies = new Movies;
        $movies->setLang($this->getLang());
        $movies->setDomain($this->domain->getDomain());
        $orderArr = Movies::getOrderByName($order);
        if (! $orderArr) {
            F::error('Order doesn\'t exist');
        }
        $movies->setOrder($orderArr['orderBy']);
        // $movies->setOrder('cinema.year desc, cinema.added_at desc');
        $movies->setLimit($this->domain->getMoviesLimit());
        $movies->setPage($page);
        $movies->setTag($tagId);
        $movies->setListTemplate('items/movie');
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        $movies->setPaginationUrlPattern('/genres/'.$tag->getId().'/(:num)');
        // $movies->setDomainClass($this->domain);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }
        // F::dump($movies->get());
        // $movies->setWithUniqueDescription($this->domain->getMoviesWithUniqueDescription());
        // $movies->setDomain($this->domain->getDomain());
        // $t->v('cinemaList',cache($movies,'getList'));
        // $time['before getList'] = Engine::getRunningTime();
        // dd($movies->getRows());

        /*
        $movies->getRows();
        $moviesEloquent = $this->domainEloquent
        ->movies()
        ->orderByRaw($orderArr['orderBy'])
        // ->limit($this->domain->getMoviesLimit())
        // ->offset(($page - 1) * $this->domain->getMoviesLimit())
        ->whereActive(true)
        ->whereDeletedPage(false)
        ->whereHas('movie',function($query) use ($tagId) {
            $query
            ->whereDeleted(false)
            ->wherePoster($this->domain->getMoviesWithPoster())
            ->whereIn('type',['movie','trailer'])
            ->whereHas('movieTags',function($query) use ($tagId) {
                $query
                ->whereTagId($tagId);
            });
        })
        ->ddRawSql()
        ->count();

        dd($moviesEloquent);
            return '';
        */
        $t->v('cinemaList', F::cache($movies, 'getList'));
        // $time['after getList'] = Engine::getRunningTime();
        $t->v('tagName', [
            'ru' => F::mb_ucfirst($tag->getName()),
            'ua' => F::mb_ucfirst($tag->getNameUa()),
            'en' => F::mb_ucfirst($tag->getNameEn()),
        ]);
        $t->v('tagPluralName', [
            'ru' => $tag->getPluralName() ? F::mb_ucfirst($tag->getPluralName()) : F::mb_ucfirst($tag->getName()),
            'ua' => F::mb_ucfirst($tag->getNameUa()),
            'en' => F::mb_ucfirst($tag->getNameEn()),
        ]);
        $t->v('tagId', $tag->getId());
        $t->v('tagText', $tag->getText());
        $t->v('tagTextBr', nl2br((string) $tag->getText()));
        $t->v('tagEmoji', $tag->getEmoji());
        // $time['before getPages'] = Engine::getRunningTime();
        $t->v('cinemaPages', F::cache($movies, 'getPages'));
        // $time['after getPages'] = Engine::getRunningTime();
        $t->v('btnOrderByDateActive', ($order === 'mostRecent') ? 'active' : '');
        $t->v('btnOrderByRatingActive', ($order === 'topRated') ? 'active' : '');
        $t->v('btnOrderByRecentAndTopRatedActive', ($order === 'recentAndTopRated') ? 'active' : '');
        $t->v('moviesOrder', $order);
        $t->v('page', $page);
        F::setPageVars($this->template);
        // $time['before setPageVars'] = Engine::getRunningTime();
        $this->setPageVars($this->template);

        // $time['after setPageVars'] = Engine::getRunningTime();
        // F::dump($time);
        return $t->get();
    }

    // контроллер вывода robots.txt
    public function robots()
    {
        Engine::setDebug(false);
        header('Content-Type: text/plain');
        F::setPageVars($this->template);
        $this->setPageVars($this->template);
        $this->template->v('yandexDisallowed', ($this->domain->getYandexAllowed() ? '' : 'User-agent: Yandex'.PHP_EOL.'Disallow: /'));
        $this->template->v('indexSearch', ($this->domainEloquent->index_search) ? 'Allow: /search*&index' : '');
        if (Engine::router()->topText) {
            $this->template->setContent(Engine::router()->topText);
        }

        return $this->template->get();
    }

    // контроллер вывода страницы без допольнительного контента
    public function simple($args = null)
    {
        $t = $this->template;
        $t->v('showSearchBlock', 'false');
        $t->v('recaptchaClientKey', $this->domain->getRecaptchaClientKey());
        $this->setMenuVars($t, $args);
        $this->setPageVars($t);
        F::setPageVars($t);

        return $t->get();
    }

    // контроллер для вывода JS шаблона
    public function getJs()
    {
        $t = $this->template;
        header('Content-Type: text/javascript; charset=UTF-8');

        return $t->get();
    }

    // вывод карты сайта
    public function sitemap_old()
    {
        // Engine::setDebug(false);
        set_time_limit(120);
        ini_set('memory_limit', '600M');
        header('Content-Type: text/xml; charset=UTF-8');
        $movies = new Movies;
        // if ($this->domain->getMoviesWithUniqueDescription()) {
        $movies->setDomain($this->domain->getDomain());
        // }
        $movies->setOrder('cinema.added_at desc');
        $movies->setLimit(1);
        $movies->setPage(1);
        $movies->setListTemplate('items/movie_sitemap');
        $movies->setDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        // $movies->setDomainClass($this->domain);
        $movies->setLimit($movies->getRows());
        // чтобы не кешировался одинаковый результат для всех доменов, нужны разные выдачи из-за использования requested domain в sitemap.xml
        $list = F::cache($movies, 'getList', [Engine::getRequestedDomainObject()->getDomain()], 10800);
        // $list = $movies->getList();
        $t = $this->template;
        F::setPageVars($t);
        $this->setPageVars($t);
        $t->v('list', $list);

        return $t->get();
    }

    // вывод карты сайта (разбивка по 10к)
    public function sitemap()
    {
        Engine::setDebug(false);

        $cinema = Config::first();
        $movies = DomainMovieEloquent::query()
            ->whereDomain($this->domain->getDomain())
            ->whereDeletedPage(false)
            ->whereActive(true)
            ->whereHas('movie', function ($query) {
                $query
                    ->whereDeleted(false);
            })
            ->cacheFor(now()->addDays(1))
            ->count();

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');

        $full_url_to_domain = $this->domain->getProtocol().'://'.Engine::getRequestedDomainObject()->getDomain();

        $pages = ceil($movies / $cinema->sitemapMoviesLimit);

        $list = '';

        /* cсылка на сайтмеп групп */
        $sitemap = $xml->addChild('sitemap');
        $sitemap->addChild('loc', $full_url_to_domain.'/sitemap_groups.xml');

        /* cсылка на сайтмеп жанров */
        $sitemap = $xml->addChild('sitemap');
        $sitemap->addChild('loc', $full_url_to_domain.'/sitemap_genres.xml');

        for ($i = 1; $i <= $pages; $i++) {
            $sitemap_link = $full_url_to_domain.str_replace('(:num)', $i, $this->domain->getSitemapMoviesPaginationLinkTemplate());
            $sitemap = $xml->addChild('sitemap');
            $sitemap->addChild('loc', $sitemap_link);
        }

        return response($xml->asXml())->header('Content-Type', 'text/xml');
    }

    // вывод карты сайты фильмов
    public function sitemap_movies($args = [])
    {

        $cache_lifetime = now()->addHours(12);

        Engine::setDebug(false);

        $page = empty($args['page']) ? 1 : F::checkstr($args['page']);

        /* не кэшируем связи, разницы почти никакой по производительности */
        /* сначала запрашиваем только ID, чтобы не переполнять БД кеша */
        $moviesShort = DomainMovieEloquent::query()
            ->select('id')
            ->whereDomain($this->domain->getDomain())
            ->whereDeletedPage(false)
            ->whereActive(true)
            ->whereHas('movie', function ($query) {
                $query
                    ->whereDeleted(false);
            })
            ->orderBy('add_date')
            ->orderBy('id')
            ->limit(Config::cached()->sitemapMoviesLimit)
            ->offset(Config::cached()->sitemapMoviesLimit * ($page - 1))
            ->cacheFor($cache_lifetime)
            ->get();

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');

        /* так памяти меньше расходуется, не знаю надо не надо оно */
        // $movies->chunk(100)->each(function($movies) use (&$xml) {});

        /* далее подтягиваем все необходимое */
        $movies = DomainMovieEloquent::with('movie');

        /* если посерийка, то подтягиваем эпизоды */
        if ($this->domainEloquent->episodically) {
            $movies->with('movie.episodes');
        }

        $movies = $movies->findMany($moviesShort->pluck('id'));

        foreach ($movies as $movie) {
            $watch_link = $this->domain->getWatchLink($movie->megaweb);
            $url = $xml->addChild('url');
            $url->addChild('loc', $this->domain->getProtocol().'://'.Engine::getRequestedDomainObject()->getDomain().$watch_link);
            $url->addChild('priority', 1);

            /* если посерийка, то генерим ссылки на эпизоды */
            if ($this->domainEloquent->episodically) {
                foreach ($movie->movie->episodes as $episode) {
                    $episode_link = $movie->episodeViewRoute($episode);
                    $url = $xml->addChild('url');
                    $url->addChild('loc', $this->domain->getProtocol().'://'.Engine::getRequestedDomainObject()->getDomain().$episode_link);
                    // $url->addChild('loc', route('episode', ['id' => $episode->id]));
                    $url->addChild('priority', 1);
                }
            }
        }

        // return $xml->asXml();

        return response($xml->asXml())->header('Content-Type', 'text/xml');
    }

    public function sitemap_groups()
    {

        $groups = GroupEloquent::query()
            ->whereActive(true)
            ->where('type', '!=', 'youtube')
            ->get();

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');

        foreach ($groups as $group) {
            $group_link = $this->domain->getGroupLink($group->megaweb);
            $url = $xml->addChild('url');
            $url->addChild('loc', $this->domain->getProtocol().'://'.Engine::getRequestedDomainObject()->getDomain().$group_link);
            $url->addChild('priority', 1);
        }

        return response($xml->asXml())->header('Content-Type', 'text/xml');
    }

    public function sitemap_genres()
    {

        $genres = TagEloquent::query()
            ->whereType('genre')
            ->get();

        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');

        foreach ($genres as $genre) {
            $genre_link = $this->domain->getGenreLink($genre->megaweb);
            $url = $xml->addChild('url');
            $url->addChild('loc', $this->domain->getProtocol().'://'.Engine::getRequestedDomainObject()->getDomain().$genre_link);
            $url->addChild('priority', 1);
        }

        return response($xml->asXml())->header('Content-Type', 'text/xml');
    }

    protected function topCinemaList($args = [])
    {
        // $lang = empty($args['lang'])?'ru':$args['lang'];
        // популярные новинки
        $movies = new Movies;
        $movies->setLang($this->getLang());
        // F::dump((new \DateTime)->modify('+1 year')->format('Y'));
        // следующий год не берем, т.к. появляется всякая фигня из будущего
        $movies->setYearTo((new \DateTime)->format('Y'));
        // if ($this->domain->getMoviesWithUniqueDescription()) {
        $movies->setDomain($this->domain->getDomain());
        // }
        $movies->setOrder('cinema.year desc, rating_sko desc, cinema.added_at desc');
        $movies->setLimit($this->domain->getTopMoviesLimit());
        $movies->setPage(1);
        // if ($lang == 'en') {
        // $movies->setListTemplate('en/items/movie_top');
        // }
        // if ($lang == 'ru') {
        $movies->setListTemplate('items/movie_top');
        // }
        $movies->setDeleted(false);
        $movies->setDomainMovieDeleted(false);
        $movies->setDomainMoviePageDeleted(false);
        $movies->setWithPoster($this->domain->getMoviesWithPoster());
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers(false);
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        if ($this->domain->getCalcThumbGenresList()) {
            $movies->setGenresListTemplate('items/movie_genre');
        }

        // F::dump($movies);
        // убираем нахрен аниме из топа
        // $movies->setExcludeGroup(267);
        // $movies->setDomainClass($this->domain);
        return F::cache($movies, 'getList');
    }

    protected function setMenuVars($t = null, $args = [])
    {
        //
        $query = empty($_REQUEST['q']) ? null : trim(htmlspecialchars(F::checkstr($_REQUEST['q'])));
        $type = empty($_REQUEST['type']) ? null : F::checkstr($_REQUEST['type']);
        $year = empty($_REQUEST['year']) ? null : F::checkstr($_REQUEST['year']);
        $genre = empty($_REQUEST['genre']) ? null : F::checkstr($_REQUEST['genre']);
        $country = empty($_REQUEST['country']) ? null : F::checkstr($_REQUEST['country']);
        $order = empty($_REQUEST['order']) ? null : F::checkstr($_REQUEST['order']);
        $genreArg = empty($args['tagId']) ? null : F::checkstr($args['tagId']);
        $group = empty($args['groupID']) ? null : F::checkstr($args['groupID']);
        $yearFrom = empty($_REQUEST['yearFrom']) ? null : F::checkstr($_REQUEST['yearFrom']);
        $yearTo = empty($_REQUEST['yearTo']) ? null : F::checkstr($_REQUEST['yearTo']);
        $ratingFrom = empty($_REQUEST['ratingFrom']) ? null : F::checkstr($_REQUEST['ratingFrom']);
        $ratingTo = empty($_REQUEST['ratingTo']) ? null : F::checkstr($_REQUEST['ratingTo']);
        $typeMovie = empty($_REQUEST['typeMovie']) ? null : F::checkstr($_REQUEST['typeMovie']);
        $typeSerial = empty($_REQUEST['typeSerial']) ? null : F::checkstr($_REQUEST['typeSerial']);
        $t->v('ratingFrom', $ratingFrom);
        $t->v('ratingTo', $ratingTo);
        $t->v('yearFrom', $yearFrom);
        $t->v('yearTo', $yearTo);
        $t->v('typeMovie', $typeMovie);
        $t->v('typeSerial', $typeSerial);
        //
        $t->v('yearsOptionList', F::cache('cinema\app\models\Movies', 'getYearsList', ['listTemplate' => 'items/option_year', 'active' => $year]));
        // $t->v('yearsOptionList','');
        // жанры для поиска
        $tags = new Tags;
        $tags->setLang($this->getLang());
        $tags->setOrder('tags.tag');
        $tags->setListTemplate('items/option_tag');
        /* убираем acitveId т.к. сильно грузит */
        // $tags->setActiveId($genre);
        // $t->v('genresOptionList',F::cache($tags,'getGenres'));
        $t->v('genresOptionList', F::cache($tags, 'getList', ['arr' => TagEloquent::whereType('genre')->get()]));
        // жанры для меню
        $tags = new Tags;
        $tags->setLang($this->getLang());
        $tags->setOrder('tags.tag');
        $tags->setListTemplate('items/menu_tag');
        /* убираем acitveId т.к. сильно грузит */
        // $tags->setActiveId($genreArg);
        // $tags->setDomain($this->domain->getDomain());
        $tags->setWithDomainMovies(true);
        // устанавливаем объект домена для генерации ссылок в Tags::getList
        $tags->setDomain($this->domain->getDomain());
        $tags->setActive(true);
        // $t->v('genresMenuList',dd(F::cache($tags,'getGenres')));
        /* получаем жанры и каналы с фильмами, чтобы далее использовать */
        $tags_having_movies = TagEloquent::query()
            ->whereIn('type', ['genre', 'channel'])
            ->whereActive(true)
            ->whereHas('movies', function ($query) {
                $query
                    ->whereActive(true)
                    ->whereDeleted(false)
                    ->whereHas('domainMovies', function ($query) {
                        $query->whereDomain($this->domain->getDomain());
                    });
            })
            ->orderBy('tag')
            ->cacheFor(EngineConfig::cached()->cacheSeconds)
            ->get();
        $t->v('genresMenuList', F::cache($tags, 'getList', ['arr' => $tags_having_movies->where('type', 'genre')]));
        // каналы для меню
        $tags = new Tags;
        $tags->setLang($this->getLang());
        $tags->setOrder('tags.tag');
        /* убираем acitveId т.к. сильно грузит */
        // $tags->setActiveId($genre);
        $tags->setDomain($this->domain->getDomain());
        $tags->setListTemplate('items/menu_tag');
        // $tags->setActive($genreArg);
        $tags->setWithDomainMovies(true);
        // устанавливаем объект домена для генерации ссылок в Tags::getList
        $tags->setDomain($this->domain->getDomain());
        // $t->v('channelsMenuList',F::cache($tags,'getChannels'));
        $t->v('channelsMenuList', F::cache($tags, 'getList', ['arr' => $tags_having_movies->where('type', 'channel')]));

        /* получаем подборки и озвучки с фильмами, чтобы далее использовать */
        $groups_having_movies = GroupEloquent::query()
            ->whereIn('type', ['compilation', 'translator'])
            ->whereActive(true)
            ->whereHas('movies', function ($query) {
                $query
                    ->whereActive(true)
                    ->whereDeleted(false)
                    ->whereHas('domainMovies', function ($query) {
                        $query->whereDomain($this->domain->getDomain());
                    });
            })
            ->cacheFor(EngineConfig::cached()->cacheSeconds)
            ->get();

        // подборки
        $groups = new Groups;
        $groups->setLang($this->getLang());
        $groups->setType('compilation');
        $groups->setActive(true);
        $groups->setListTemplate('items/menu_compilation');
        // устанавливаем объект домена для генерации ссылок в Groups::getList
        $groups->setDomain($this->domain->getDomain());
        // только группы с фильмами
        $groups->setWithDomainMovies(true);
        $groups->setUseDomainGroup(false);
        // $t->v('compilationsMenuList',F::cache($groups,'getList'));
        $t->v('compilationsMenuList', F::cache($groups, 'getList', ['arr' => $groups_having_movies->where('type', 'compilation')]));
        // только ютуб группы с видео
        $groups = new Groups;
        $groups->setLang($this->getLang());
        $groups->setType('youtube');
        $groups->setActive(true);
        $groups->setListTemplate('items/menu_compilation');
        // устанавливаем объект домена для генерации ссылок в Groups::getList
        $groups->setDomain($this->domain->getDomain());
        // $groups->setWithDomainMovies(true);
        $groups->setUseDomainGroup(false);
        // F::dump($groups->getList());
        // $t->v('youtubeMenuList',F::cache($groups,'getList'));
        $t->v('youtubeMenuList', F::cache($groups, 'getList', ['arr' => GroupEloquent::whereActive(true)->where('type', 'youtube')->get()]));
        // озвучки для меню
        $groups = new Groups;
        $groups->setLang($this->getLang());
        $groups->setType('translator');
        // $groups->setActive(true);
        $groups->setListTemplate('items/menu_group');
        // устанавливаем объект домена для генерации ссылок в Groups::getList
        $groups->setDomain($this->domain->getDomain());
        // только группы с фильмами
        $groups->setWithDomainMovies(true);
        $groups->setUseDomainGroup(false);
        // $t->v('translatorsMenuList',F::cache($groups,'getList'));
        $t->v('translatorsMenuList', F::cache($groups, 'getList', ['arr' => $groups_having_movies->where('type', 'translator')]));
        // $groups = new Groups;
        // $groups->setOrder('groups.name');
        // $groups->setListTemplate('items/menu_group');
        // $groups->setType('genre');
        // $t->v('genresMenuList',$groups->getList());

        // F::dump(json_encode($tags));
        // F::dump(F::cache($tags,'getGenres'));
        // $t->v('genresMenuList',$tags->getGenres());

        // пересоздаем объект, т.к. были проблемы с кешем при использовании одного и того же объекта для разных выводов. получалось что до кеширования объект getCountries имел type=genre (от предыдущего использования), а при повторной загрузке странице - объект уже имеет type=null, т.к. getGenres подгружаются из кеща, а type=genre присваивается в методе getGenres который еще не был выполнен. вот такой сложный косяк.
        $tags = new Tags;
        $tags->setLang($this->getLang());
        $tags->setOrder('tags.tag');
        $tags->setListTemplate('items/option_tag');
        /* убираем acitveId т.к. сильно грузит */
        // $tags->setActiveId($country);
        $tags->setActive(true);
        // $t->v('countriesOptionList',F::cache($tags,'getCountries'));
        $t->v('countriesOptionList', F::cache($tags, 'getList', ['arr' => TagEloquent::whereType('country')->whereActive(true)->orderBy('tag')->cacheFor(EngineConfig::cached()->cacheSeconds)->get()]));

        // нужны id тегов фильмов и сериалов, т.к. набрано вручную
        $movieTagId = TagEloquent::whereTag('movie')->firstOrFail()->id;
        $serialTagId = TagEloquent::whereTag('serial')->firstOrFail()->id;
        $t->v('movieTagId', $movieTagId);
        $t->v('serialTagId', $serialTagId);
        $t->v('movieTypeSelected', ($type === $movieTagId) ? 'selected' : '');
        $t->v('serialTypeSelected', ($type === $serialTagId) ? 'selected' : '');
        // $t->v('typesOptionList',$tags->getTypes());
        $t->v('ordersOptionList', Movies::getOrdersList(['listTemplate' => 'items/option_order', 'active' => $order]));
        $t->v('searchQuery', $query);
        $t->v('activeType', $type);
        // подсветка активных элементов меню
        $t->v('activeGroup1', ($group == 1) ? 'active' : '');
        $t->v('activeGroup2', ($group == 2) ? 'active' : '');
        $t->v('activeGroup3', ($group == 3) ? 'active' : '');
        $t->v('activeGroup4', ($group == 4) ? 'active' : '');
        $t->v('activeGroup6', ($group == 6) ? 'active' : '');
        $t->v('activeGroup7', ($group == 7) ? 'active' : '');
        $t->v('activeFilmGroups', in_array($group, [3, 4, 7]) ? 'active' : '');
        $t->v('activeSerialGroups', in_array($group, [1, 2, 6]) ? 'active' : '');
        $t->v('activeAnimeGroup', ($group == 5) ? 'active' : '');
        $t->v('activeGenreGroups', $genreArg ? 'active' : '');
        // ссылки на группы
        // $t->v('group4Link',F::dump($this->domain->getGroupLink(new Group(4))));

        $movies = new Movies;
        $t->v('moviesMinYear', F::cache($movies, 'getMinYear'));
        $t->v('moviesMaxYear', F::cache($movies, 'getMaxYear'));
    }

    protected function setPageVars($t = null)
    {
        // вычисляем общее кол-во фильмов и сериалов на сайте
        $movies = new Movies;
        $movies->setDomain($this->domain->getDomain());
        $movies->setDeleted(false);
        $movies->setIncludeSerials($this->domain->getIncludeSerials());
        $movies->setIncludeMovies($this->domain->getIncludeMovies());
        $movies->setIncludeTrailers($this->domain->getIncludeTrailers());
        $movies->setIncludeYoutube($this->domain->getIncludeYoutube());
        $movies->setActive(true);
        $t->v('allMoviesRows', F::cache($movies, 'getRows'));
        // получаем последние комментарии
        $comments = new MovieComments;
        $comments->setLang($this->getLang());
        $comments->setDomain($this->domain->getDomain());
        $comments->setListTemplate('items/movie_comment_full');
        $comments->setLimit(10);
        $comments->setPage(1);
        $comments->setOrder('desc');
        $t->v('lastComments', F::cache($comments, 'getList', null, 900));
        $t->v('lang', $this->getLang());
    }

    public function updatesList($args = [])
    {
        $date = empty($args['date']) ? null : F::checkstr($args['date']);
        $listTemplate = empty($args['listTemplate']) ? 'items/list_updates' : $args['listTemplate'];
        if (! $date) {
            F::error('Set date for updatesList');
        }
        $moonwalks = new Moonwalks;
        $moonwalks->setLimit(300);
        $moonwalks->setPage(1);
        $moonwalks->setDate($date);
        // $moonwalks->setOrder('title_ru');
        $arr = $moonwalks->get();
        // dump($arr);
        $list = '';
        foreach ($arr as $v) {
            $t = new Template($listTemplate);
            $m = new Moonwalk($v['token'], $v['date']);
            // if ($this->domain->getUseMovieId()) {
            $domainMovieId = DomainMovie::getDomainMovieIdByUniqueIdAndDomain($m->getUniqueId(), $this->domain->getDomain());
            if (! $domainMovieId) {
                continue;
            }
            $movie = new DomainMovie($domainMovieId);
            $t->v('domainMovieId', $movie->getDomainMovieId());
            // } else {
            // $movie = new Movie(Movie::getIdByUniqueId($m->getUniqueId()));
            // }
            $l = $m->getLastMoonwalk();
            $mse = $m->getSeasonEpisodesCount();
            // dump($lse);
            // dump(array_diff($mse, $lse));
            $episodesList = '';
            if (! empty($l)) {
                $lse = $l->getSeasonEpisodesCount();
                foreach ($mse as $sk => $s) {
                    $seasonWritten = false;
                    $episodeWritten = false;
                    foreach ($s->episodes as $ek => $e) {
                        if (! isset($lse[$sk]->episodes[$ek])) {
                            if (! $seasonWritten) {
                                $episodesList .= $s->season_number.' сезон, ';
                                $seasonWritten = true;
                            }
                            if (! $episodeWritten) {
                                $episodesList .= ' серия ';
                                $episodeWritten = true;
                            }
                            $episodesList .= $e;
                            if ($ek != (count($s->episodes) - 1)) {
                                $episodesList .= ', ';
                            }
                        }
                    }
                }
            } else {
                $episodesList .= 'сериал целиком';
            }
            $t->v('movieId', $movie->getId());
            $t->v('titleRuUrl', $movie->getTitleRuUrl());
            $t->v('poster', $movie->getPoster());
            $t->v('titleRu', $movie->getTitleRu());
            $t->v('translator', $m->getTranslator() ? $m->getTranslator().',' : '');
            $t->v('episodesList', $episodesList);
            $t->v('watchLink', $this->domain->getWatchLink($movie));
            $list .= $t->get();
            // $list.= '<li><a href="/watch/'.$movie->getId().'/'.$movie->getTitleRuUrl().'">'.$m->getTitleRu().'</a> '.($m->getTranslator()?'('.$m->getTranslator().')':'').' '.$episodesList.'</li>';
        }

        return $list;
    }

    protected function setLang($lang = null)
    {
        $this->lang = $lang;
        $this->template->setLang($lang);
        $this->domain->setLang($lang);
    }

    protected function getLang()
    {
        return $this->lang;
    }

    public function getHreflang($domain, $uri)
    {
        if (! $domain->getRuDomain()) {
            return false;
        }
        $result = '';
        if ($domain->getRuDomain()) {
            $result .= '
			<link rel="alternate" href="'.request()->getScheme().'://'.$domain->getRuDomain().$uri.'" hreflang="ru-ru" />
			<link rel="alternate" href="'.request()->getScheme().'://'.$domain->getRuDomain().$uri.'" hreflang="ru" />
			';
        }
        if ($domain->getNextRuDomain()) {
            $result .= '
			<link rel="alternate" href="'.request()->getScheme().'://'.$domain->getNextRuDomain().$uri.'" hreflang="ru-kz" />
			<link rel="alternate" href="'.request()->getScheme().'://'.$domain->getNextRuDomain().$uri.'" hreflang="kz" />
			';
        }
        $result .= '<link rel="alternate" href="'.request()->getScheme().'://'.$domain->getLastRedirectDomain().$uri.'" hreflang="x-default" />';

        return $result;
    }
}
