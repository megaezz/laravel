<?php

namespace cinema\app\controllers;

use cinema\app\models\Cinema;
use cinema\app\models\User;
use engine\app\models\F;
use engine\app\models\Template;

// сервисный контроллер, нет проверки на домен cinema.domain
class WebmoneyController
{
    public function depositResult()
    {
        // F::dump($_REQUEST);
        // если не передан POST значит вебмани проверяет доступность сервера
        if (empty($_POST)) {
            return 'I\'m ok';
        }
        // logger()->info(F::pre_str($_POST));
        $amount = empty($_POST['LMI_PAYMENT_AMOUNT']) ? F::error('Не передана сумма') : $_POST['LMI_PAYMENT_AMOUNT'];
        $payee_purse = empty($_POST['LMI_PAYEE_PURSE']) ? F::error('Не передан кошелек') : $_POST['LMI_PAYEE_PURSE'];
        $secret_key = empty($_POST['LMI_SECRET_KEY']) ? F::error('Не передан секретный ключ') : $_POST['LMI_SECRET_KEY'];
        $mode = ! isset($_POST['LMI_MODE']) ? F::error('Не передан режим') : $_POST['LMI_MODE'];
        $login = empty($_POST['login']) ? F::error('Не передан логин') : $_POST['login'];
        if (! in_array($payee_purse, ['R085008526052', 'Z703935021742'])) {
            F::error('Неверный кошелек');
        }
        if ($secret_key !== '9879F5B8-ECD1-4D59-A404-3641729A5E94') {
            F::error('Неверный ключ');
        }
        if ($mode) {
            F::error('Кошелек в тестовом режиме, прием платежей невозможен');
        }
        // если все ок, зачисляем amount
        $user = new User($login);
        // $user->setDeposited($user->getDeposited() + $amount);
        if (substr($payee_purse, 0, 1) == 'Z') {
            $usdRubRate = F::getUsdRubRateCached() ? (F::getUsdRubRateCached() * (1 - (new Cinema)->getUsdRubShave() / 100)) : null;
            $amountWmz = $amount;
            $amount = $usdRubRate * $amount;
            $user->addTransfer($amount, 'Депозит Webmoney '.$amountWmz.' WMZ');
        } else {
            $user->addTransfer($amount, 'Депозит Webmoney WMR');
        }
        // отправляем письмо
        if ($user->getEmail() and $user->getEmailConfirmed()) {
            $email = new Template('admin/customer/emails/deposit');
            $email->v('login', $user->getLogin());
            $email->v('amount', $amount);
            (new Cinema)->sendMail($user->getEmail(), 'Киноконтент', 'Депозит', $email->get(), 'kinocontent');
        }
        // $user->save();
        if ($user->getPartner()) {
            $partner = new User($user->getPartner());
            $partner->addTransfer($amount * (new Cinema)->getRefCustomer() / 100, 'Начисление за реферала '.$user->getLogin());
        }

        // logger()->info('Оплата '.$amount.' руб. зачтена');
        return 'ok';
    }
}
