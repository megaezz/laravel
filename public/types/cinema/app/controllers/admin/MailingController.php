<?php

namespace cinema\app\controllers\admin;

use cinema\app\controllers\AdminController;
use cinema\app\models\Cinema;
use cinema\app\models\User;
use cinema\app\models\Users;
use engine\app\models\F;
use engine\app\models\Template;

class MailingController extends AdminController
{
    // рассылка по пользователям с проверенной почтой, которые не заходили более 10 дней.
    public function come_back()
    {
        $limit = 10;
        $cinema = new Cinema;
        $users = new Users;
        $users->setLastActivityIs('< date_sub(current_date,interval 20 day)');
        $users->setLevels(['admin', 'worker', 'moderator', 'customer']);
        $users->setWithEmail(true);
        $users->setEmailConfirmed(true);
        $users->setChecked(false);
        $users->setLimit($limit);
        $arr = $users->get();
        foreach ($arr as $v) {
            $user = new User($v['login']);
            if (in_array($user->getLevel(), ['admin', 'worker', 'moderator'])) {
                $t = new Template('admin/rewrite/emails/come_back');
                $t->v('login', $user->getLogin());
                $t->v('password', $user->getPassword());
                $t->v('rewritePricePer1kNoSpaces', ceil($cinema->getRewritePricePer1k() * 1.2));
                $cinema->sendMail($user->getEmail(), 'Кинотекст', 'Возвращайтесь', $t->get(), 'kinotext');
            }
            if (in_array($user->getLevel(), ['customer'])) {
                $t = new Template('admin/customer/emails/come_back');
                $t->v('login', $user->getLogin());
                $t->v('password', $user->getPassword());
                $t->v('customerPricePer1k', $cinema->getCustomerPricePer1k());
                $cinema->sendMail($user->getEmail(), 'Киноконтент', 'Возвращайтесь', $t->get(), 'kinocontent');
            }
            $user->setChecked(true);
            $user->save();
        }
        F::alert('Всего: '.$users->getRows().'. Отправлено: '.count($arr));
    }

    // рассылка по исполнителям
    public function workers_increased_rate()
    {
        $limit = 25;
        $cinema = new Cinema;
        $users = new Users;
        $users->setLevels(['admin', 'worker', 'moderator']);
        $users->setWithEmail(true);
        $users->setEmailConfirmed(true);
        $users->setChecked(false);
        $users->setLimit($limit);
        $arr = $users->get();
        foreach ($arr as $v) {
            $user = new User($v['login']);
            $t = new Template('admin/rewrite/emails/workers_increased_rate');
            $t->v('login', $user->getLogin());
            $t->v('password', $user->getPassword());
            $t->v('rewritePricePer1kNoSpaces', ceil($cinema->getRewritePricePer1k() * 1.2));
            $cinema->sendMail($user->getEmail(), 'Кинотекст', 'Повышение ставки', $t->get(), 'kinotext');
            $user->setChecked(true);
            $user->save();
        }
        F::alert('Всего: '.$users->getRows().'. Отправлено: '.count($arr).F::jsRedirect('/?r=admin/Mailing/workers_increased_rate'));
    }

    // рассылка по пользователям с указанной, но не проверенной почтой
    public function confirm_email()
    {
        $limit = 10;
        $cinema = new Cinema;
        $users = new Users;
        $users->setLevels(['admin', 'worker', 'moderator', 'customer']);
        $users->setWithEmail(true);
        $users->setEmailConfirmed(false);
        $users->setChecked(false);
        $users->setLimit($limit);
        $arr = $users->get();
        foreach ($arr as $v) {
            $user = new User($v['login']);
            if (! $user->getEmailCode()) {
                $user->setEmailCode(F::generateRandomStr(40));
                $user->save();
            }
            if (in_array($user->getLevel(), ['admin', 'worker', 'moderator'])) {
                $t = new Template('admin/rewrite/emails/mailing_confirm_email');
                $t->v('login', $user->getLogin());
                $t->v('emailCode', $user->getEmailCode());
                $cinema->sendMail($user->getEmail(), 'Кинотекст', 'Подтверждение почты', $t->get(), 'kinotext');
            }
            if (in_array($user->getLevel(), ['customer'])) {
                $t = new Template('admin/customer/emails/mailing_confirm_email');
                $t->v('login', $user->getLogin());
                $t->v('emailCode', $user->getEmailCode());
                $cinema->sendMail($user->getEmail(), 'Киноконтент', 'Подтверждение почты', $t->get(), 'kinocontent');
            }
            $user->setChecked(true);
            $user->save();
        }
        F::alert('Всего: '.$users->getRows().'. Отправлено: '.count($arr));
    }
}
