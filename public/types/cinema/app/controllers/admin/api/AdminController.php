<?php

namespace cinema\app\controllers\admin\api;

use App\Models\Domain as EngineDomain;
use cinema\app\controllers\CinemaAuthorizationController;
use cinema\app\models\eloquent\Domain;
use cinema\app\models\eloquent\DomainMovie;
use cinema\app\models\eloquent\Movie;
use engine\app\models\Engine;
use engine\app\models\F;

class AdminController
{
    public function __construct()
    {
        $this->auth = new CinemaAuthorizationController(['admin', 'admin-lite']);
        header('Content-Type: application/json');
    }

    public function getMovies()
    {
        $filter = json_decode(file_get_contents('php://input'));
        $movies = Movie::with('tags');
        $movies->limit(10);
        $movies->orderByDesc('id');
        if (isset($filter->limit)) {
            $movies->limit($filter->limit);
        }
        if (isset($filter->id)) {
            $movies->whereId($filter->id);
        }

        return json_encode($movies->get(), JSON_UNESCAPED_UNICODE);
    }

    public function getDomainMovies()
    {
        $filter = json_decode(file_get_contents('php://input'));
        $movies = DomainMovie::with('movie', 'movie.tags');
        $movies->limit(10);
        $movies->orderByDesc('id');
        if (isset($filter->domain)) {
            $movies->whereDomain($filter->domain);
        }
        if (isset($filter->limit)) {
            $movies->limit($filter->limit);
        }
        if (isset($filter->active)) {
            $movies->whereActive($filter->active);
        }
        if (isset($filter->deleted)) {
            $movies->whereDeleted($filter->deleted);
        }
        if (isset($filter->rkn)) {
            $movies->whereRkn($filter->rkn);
        }
        if (isset($filter->id)) {
            $movies->whereId($filter->id);
        }
        if (isset($filter->movie_id)) {
            $movies->whereMovieId($filter->movie_id);
        }

        return json_encode($movies->get(), JSON_UNESCAPED_UNICODE);
    }

    public function getDomain()
    {
        $input = file_get_contents('php://input');
        $domain = $input ?? F::error('Domain required');
        // если домен не создан для типа cinema, создаем
        if (! Domain::find($domain)) {
            $engineDomain = EngineDomain::find($domain);
            if ($engineDomain and $engineDomain->type == 'cinema') {
                $cinemaDomain = new Domain;
                $cinemaDomain->domain = $domain;
                $cinemaDomain->save();
            }
        }

        return json_encode(Domain::with('engineDomain')->findOrFail($domain), JSON_UNESCAPED_UNICODE);
    }

    public function getDomains()
    {
        $filter = json_decode(file_get_contents('php://input'));
        $domains = Domain::query();

        // if (isset($filter->engine_domain->type)) {
        // 	$domains->whereRelation('engineDomain','type',$filter->engine_domain->type);
        // }
        return json_encode($domains->get(), JSON_UNESCAPED_UNICODE);
    }

    public function updateDomain()
    {
        if ($this->auth->getLevel() != 'admin') {
            F::error('Denied');
        }
        $input = json_decode(file_get_contents('php://input'));
        $newDomain = $input ?? F::error('Domain object required');
        $domain = Domain::findOrFail($newDomain->domain);
        foreach ($newDomain as $property => $value) {
            if (! is_object($value)) {
                $domain->{$property} = $value;
            }
        }
        $domain->save();

        return json_encode(true);
    }
}
