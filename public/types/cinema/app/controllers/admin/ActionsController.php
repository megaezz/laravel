<?php

namespace cinema\app\controllers\admin;

use cinema\app\controllers\AdminController;
use cinema\app\models\Cinema;
use cinema\app\models\Domain;
use cinema\app\models\Domains;
use cinema\app\models\eloquent\Config;
use cinema\app\models\eloquent\Kinopoisk;
use cinema\app\models\eloquent\Myshows;
use cinema\app\models\eloquent\VideorollStat;
use cinema\app\models\Kodik;
use cinema\app\models\Kodiks;
use cinema\app\models\Moonwalk;
use cinema\app\models\Moonwalks;
use cinema\app\models\Movie;
// use engine\app\models\Kinopoisk;
use cinema\app\models\Movies;
use cinema\app\models\MovieTags;
use cinema\app\models\MyShowsMe;
use cinema\app\models\Tag;
use cinema\app\models\Videodb;
use engine\app\models\Domain as EngineDomain;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class ActionsController extends AdminController
{
    public function submitVideorollStat()
    {
        $date = $_POST['day'] ?? F::error('Day require');
        if (VideorollStat::where('date', $date)->first()) {
            F::error('Данные за '.$date.' уже были загружены');
        }
        $file_path = empty($_FILES['csv']['tmp_name']) ? F::error('Не передан файл') : $_FILES['csv']['tmp_name'];
        $csv = fopen($file_path, 'r');
        $str = 0;
        while (($data = fgetcsv($csv, null, ';')) !== false) {
            $str++;
            if ($str < 3) {
                continue;
            }
            $url = $data[0];
            $parse_url = parse_url($url);
            $domain = $parse_url['host'];
            $videorollStat = VideorollStat::where('date', $date)->where('domain', $domain)->first();
            if (! $videorollStat) {
                $videorollStat = new VideorollStat;
            }
            $videorollStat->date = $date;
            $videorollStat->url = $url;
            $videorollStat->domain = $domain;
            // dd($data);
            $videorollStat->pc_views = $data[1];
            $videorollStat->pc_income = $data[2];
            $videorollStat->mob_views = $data[3];
            $videorollStat->mob_income = $data[4];
            $videorollStat->save();
        }
        F::alert('Загружено');
    }

    public function updateKinopoiskUnofficialAdditional()
    {
        $limit = 10;
        $token = Config::first()->kinopoiskApiUnofficialToken;
        $items = Kinopoisk::orderBy('id')->cursorPaginate($limit);
        $items->withPath('/?r=admin/Actions/updateKinopoiskUnofficialAdditional');
        foreach ($items as $item) {
            // трейлеры
            $object = new \stdClass;
            $object->id = $item->id;
            $object->url = 'https://kinopoiskapiunofficial.tech/api/v2.2/films/'.$item->id.'/videos';
            $object->curl = new \stdClass;
            $object->curl->httpheader = ['access-token: '.$token];
            $objects[] = $object;
            // факты
            $object2 = new \stdClass;
            $object2->id = $item->id;
            $object2->url = 'https://kinopoiskapiunofficial.tech/api/v2.2/films/'.$item->id.'/facts';
            $object2->curl = new \stdClass;
            $object2->curl->httpheader = ['access-token: '.$token];
            $objects2[] = $object2;
        }
        $videos = F::multiCurl($objects);
        $facts = F::multiCurl($objects2);
        foreach ($items as $item) {
            $found_video = null;
            $found_fact = null;
            foreach ($videos as $video) {
                if ($video->id == $item->id) {
                    $found_video = $video;
                    break;
                }
            }
            if (! $found_video) {
                F::error("Не найден ответ с трейлерами ID #{$item->id}");
            }
            foreach ($facts as $fact) {
                if ($fact->id == $item->id) {
                    $found_fact = $fact;
                    break;
                }
            }
            if (! $found_video) {
                F::error("Не найден ответ с фактами ID #{$item->id}");
            }
            if (isset($found_video->result)) {
                $item->videos = json_decode($found_video->result);
            }
            if (isset($found_fact->result)) {
                $item->facts = json_decode($found_fact->result);
            }
            $item->save();
        }
        if ($items->hasMorePages()) {
            F::alert("Обновлено {$limit} записей кинопоиска. Курсор: {$items->last()->id}".F::jsRedirect($items->nextPageUrl()));
        } else {
            F::alert("Завершено обновление данных кинопоиска. Курсор: {$items->last()->id}");
        }
    }

    // API кинопоиска http://kinopoiskapiunofficial.tech
    public function updateCinemaFromKinopoiskApiUnofficial()
    {
        Engine::setDebug(false);
        Engine::setDisplayErrors(true);
        // повышаем время исполнения скрипта на 60 сек с текущего момента
        set_time_limit(60);
        $limit = empty($_GET['limit']) ? 500 : $_GET['limit'];
        $resetCheckers = $_GET['resetCheckers'] ?? null;
        if (! is_null($resetCheckers)) {
            Movies::resetCheckers();
        }
        $movies = new Movies;
        $movies->setLimit($limit);
        $movies->setPage(1);
        $movies->setChecked(false);
        $movies->setWithKinopoiskId(true);
        // $movies->setOrder('cinema.add_date desc');
        // выводим хаотично чтобы распределить нагрузку (например постеры чтобы массово не загружались для последних добавленных материалов)
        $movies->setOrder('rand()');
        $movies->setIncludeNullType(true);
        $arrMovies = $movies->get();
        $objects = [];
        // сначала прогружаем все ответы апи через мультикурл
        $i = 0;
        $p = 0;
        $kinopoiskIds = [];
        $list = '';
        foreach ($arrMovies as $v) {
            $i++;
            if ($i > 100) {
                $i = 1;
                $p++;
            }
            $movie = new Movie($v['id']);
            $kinopoiskIds[$p][] = $movie->getKinopoiskId();
        }
        // F::alert(Engine::debug());
        // F::dump($kinopoiskIds);
        // тест с kinopoiskIds
        foreach ($kinopoiskIds as $v) {
            $url = 'https://kinopoiskapiunofficial.tech/api/v3/films/'.implode(',', $v);
            $object = new \stdClass;
            $object->url = $url;
            $object->curl = new \stdClass;
            $object->curl->httpheader = ['access-token: EPn08FovZRmOpDYO050iHSxixM8SF22e'];
            // $object->movieId = $v['id'];
            $objects[] = $object;
        }
        // конец теста с kinopoiskIds
        $objects = F::multiCurl($objects);
        // F::dump($objects);
        // выполняем задачи
        foreach ($objects as $obj) {
            $result = json_decode($obj->result);
            // F::dump($result);
            if (isset($result->error)) {
                // $movie->setChecked(true);
                // $movie->save();
                $list .= '<li style="color: red;">Ошибка запроса '.$obj->url.' ('.$result->error.')</li>';

                continue;
            }
            foreach ($result->films as $data) {
                // F::dump($data);
                $movie = new Movie(Movie::getIdByKinopoiskId($data->filmId));
                $movie->setChecked(true);
                $info = $this->setMovieFromKinopoiskData($movie, $data);
                $listTags = $info['tags'];
                // скачиваем постер если его нет
                $listPoster = $this->addPosterToMovie($movie);
                // убираем ID обработанного фильма из списка, чтобы потом знать какие остались не обработанным и отметить чеккеры
                foreach ($arrMovies as $k => $v) {
                    if ($v['id'] === $movie->getId()) {
                        unset($arrMovies[$k]);
                    }
                }
                // F::dump($movie);
                $movie->save();
                // dump($movie);
                $list .= '
				<li>Фильм #'.$movie->getId().' обновлен
				'.($listPoster ? ('<br>Постер:<ul><li>'.$listPoster.'</li></ul>') : '').'
				'.($listTags ? ('<br>Добавили тэги: <ol>'.$listTags.'</ol>') : '').'
				</li>';
            }
        }
        // F::dump($arrMovies);
        foreach ($arrMovies as $v) {
            $movie = new Movie($v['id']);
            $movie->setChecked(true);
            $movie->save();
            $list .= '<li style="color: red;">Фильм #'.$movie->getId().' не найден в базе API</li>';
        }
        // смотрим какие
        $script = $movies->getRows() ? ('
			<script>
			setTimeout(window.location.replace(\'/?r=admin/Actions/updateCinemaFromKinopoiskApiUnofficial\'),5000);
			</script>
			') : ('
			<script>
			setTimeout(window.location.replace(\'/?r=admin/Actions/addMoviesToDomainMovies&resetCheckers\'),5000);
			</script>
			');
        $t = new Template('admin/text');
        $t->v('text', '
			'.($resetCheckers ? '<p>Чеккеры Cinema сброшены</p>' : '').'
			<p>Всего: '.$movies->getRows().'</p>
			<p>Лимит: '.$limit.'</p>
			<ol>'.$list.'</ol>
			'.$script.'
			');
        $this->setMenuVars($t, 'actions');

        return $t->get();
        // return '<ol>Всего: '.$movies->getRows().' '.$list.'</ol>';
    }

    public function setMovieFromKinopoiskData($movie = null, $data = null)
    {
        $yearExplode = explode('-', $data->year);
        if (count($yearExplode) > 1) {
            $year = $yearExplode[0];
        } else {
            $year = $data->year;
        }
        // F::dump($year);
        // if (!$movie->getYear()) {
        if ($year) {
            $movie->setYear($year);
        }
        // if (!$movie->getTitleRu()) {
        if ($data->nameRu) {
            $movie->setTitleRu($data->nameRu);
        }
        // if (!$movie->getTitleEn()) {
        if ($data->nameEn) {
            $movie->setTitleEn($data->nameEn);
        }
        // if (!$movie->getDescription()) {
        if ($data->description) {
            $movie->setDescription($data->description);
        }
        if ($data->rating) {
            $movie->setKinopoiskRating($data->rating);
        }
        if ($data->ratingVoteCount) {
            $movie->setKinopoiskVotes($data->ratingVoteCount);
        }
        if ($data->ratingImdb) {
            $movie->setImdbRating($data->ratingImdb);
        }
        if ($data->ratingImdbVoteCount) {
            $movie->setImdbVotes($data->ratingImdbVoteCount);
        }
        if (! $movie->getTagline() and $data->slogan) {
            $movie->setTagline($data->slogan);
        }
        if ($data->ratingAgeLimits) {
            $movie->setAge($data->ratingAgeLimits);
        }
        $movieType = $data->type ?? null;
        if ($movieType == 'TV_SHOW') {
            $movie->setType('serial');
        }
        if ($movieType == 'FILM') {
            $movie->setType('movie');
        }
        $listTags = '';
        // добавляем тэг "тип фильма"
        if ($movie->getType()) {
            // удаляем теги
            $movie->removeTag(Tag::getIdByName('serial'));
            $movie->removeTag(Tag::getIdByName('movie'));
            // пишем тег
            $movie->addTagByName($movie->getType());

            $listTags .= '<li>добавлен тэг: '.$movie->getType().'</li>';
        }
        // F::dump($data);
        $actors = [];
        // if (isset($data->creators->actor)) {
        // 	foreach ($data->creators->actor as $v2) {
        // 		$actors[] = $v2->name_person_ru;
        // 	}
        // }
        $directors = [];
        // if (isset($data->creators->director)) {
        // 	foreach ($data->creators->director as $v2) {
        // 		$directors[] = $v2->name_person_ru;
        // 	}
        // }
        $genres = [];
        if (isset($data->genres)) {
            foreach ($data->genres as $v2) {
                $genres[] = $v2->genre;
            }
        }
        $countries = [];
        if (isset($data->countries)) {
            foreach ($data->countries as $v2) {
                $countries[] = $v2->country;
            }
        }
        // F::dump($data);
        // добавляем жанры и страны
        $tags_arr = [
            'genre' => $genres,
            'country' => $countries,
            'actor' => $actors,
            'director' => $directors,
            'studio' => [],
            'year' => [isset($year) ? $year : null],
        ];
        foreach ($tags_arr as $type => $arr) {
            // если массива тэгов нет то идем дальше
            if (! $arr) {
                continue;
            }
            foreach ($arr as $v) {
                // если передается пустой тег, то пропускаем, иначе пробуем получить ID тега
                if (empty($v)) {
                    continue;
                } else {
                    $tagId = Tag::getIdByName($v);
                }
                // флаг нужен для отчета
                $created = false;
                // если такого тэга нет, то создаем его с типом type
                if (! $tagId) {
                    // создаем тэг одновременно создавая класс
                    $tag = new Tag(Tag::add());
                    $tag->setType($type);
                    $tag->setName($v);
                    $tag->save();
                    $tagId = $tag->getId();
                    $created = true;
                }
                // если тип тэга - год, то сначала удаляем все теги данного типа
                $tag = new Tag($tagId);
                // F::dump($tag);
                if ($tag->getType() == 'year') {
                    // получаем список тегов с годом для фильма
                    $movieTags = new MovieTags($movie->getId());
                    $movieTags->setType('year');
                    $arrMovieTags = $movieTags->get();
                    // если больше 1 тега года, то значит сносим годики и пишем текущий тег года
                    if (count($arrMovieTags) > 1) {
                        // удаляем эти теги
                        foreach ($arrMovieTags as $yearMovieTag) {
                            $movie->removeTag($yearMovieTag['id']);
                        }
                    }
                    // F::dump($movie->getTags());
                }
                // если такого тэга нет в списке тэгов для данного фильма, то добавляем его
                if ($movie->addTag($tagId)) {
                    $listTags .= '<li>'.($created ? 'создан и ' : '').'добавлен тэг: '.$v.' ('.$type.')</li>';
                }
            }
        }
        $info['tags'] = $listTags;

        return $info;
    }

    public function updateCinemaFromKinopoisk()
    {
        // Engine::setDebug(false);
        Engine::setDisplayErrors(true);
        // повышаем время исполнения скрипта на 60 сек с текущего момента
        set_time_limit(60);
        $limit = empty($_GET['limit']) ? 1000 : $_GET['limit'];
        $resetCheckers = $_GET['resetCheckers'] ?? null;
        if (! is_null($resetCheckers)) {
            Movies::resetCheckers();
        }
        $movies = new Movies;
        $movies->setLimit($limit);
        $movies->setPage(1);
        $movies->setChecked(false);
        $movies->setWithKinopoiskId(true);
        // $movies->setOrder('cinema.add_date desc');
        // выводим хаотично чтобы распределить нагрузку (например постеры чтобы массово не загружались для последних добавленных материалов)
        $movies->setOrder('rand()');
        $movies->setIncludeNullType(true);
        $arrMovies = $movies->get();
        $list = '';
        foreach ($arrMovies as $v) {
            $movie = new Movie($v['id']);
            $movie->setChecked(true);
            $arr = F::query_assoc('select data from '.F::typetab('kinopoisk').' where id = \''.$movie->getKinopoiskId().'\';');
            if (! $arr) {
                $list .= '<li style="color: red;">Фильм #'.$movie->getId().' не найден в базе API</li>';
                $movie->save();

                continue;
            }
            $data = json_decode($arr['data']);
            $info = $this->setMovieFromKinopoiskData($movie, $data);
            $listTags = $info['tags'];
            // скачиваем постер если его нет
            $listPoster = $this->addPosterToMovie($movie);
            $movie->save();
            // dump($movie);
            $list .= '
			<li>Фильм #'.$movie->getId().' обновлен
			'.($listPoster ? ('<br>Постер: '.$listPoster) : '').'
			'.($listTags ? ('<br>Добавили тэги: <ol>'.$listTags.'</ol>') : '').'
			</li>';
        }
        // смотрим какие
        $script = $movies->getRows() ? ('
			<script>
			setTimeout(window.location.replace(\'/?r=admin/Actions/updateCinemaFromKinopoisk&limit='.$limit.'\'),5000);
			</script>
			') : ('
			<script>
			setTimeout(window.location.replace(\'/?r=admin/Actions/addMoviesToDomainMovies&resetCheckers\'),5000);
			</script>
			');
        $t = new Template('admin/text');
        $t->v('text', '
			'.($resetCheckers ? '<p>Чеккеры Cinema сброшены</p>' : '').'
			<p>Всего: '.$movies->getRows().'</p>
			<p>Лимит: '.$limit.'</p>
			<ol>'.$list.'</ol>
			'.$script.'
			');
        $this->setMenuVars($t, 'actions');

        return $t->get();
        // return '<ol>Всего: '.$movies->getRows().' '.$list.'</ol>';
    }

    // загружает постер для требуемого фильма если его нет
    // входные параметры - объект Movie
    public function addPosterToMovie($movie = null)
    {
        // скачиваем постер если его нет
        if ($movie->getPoster()) {
            return false;
        }
        $postersDir = Engine::getStaticPath().'/types/cinema/posters';
        $postersWebPDir = Engine::getStaticPath().'/types/cinema/posters-webp';
        // создаем каталог постеров если его нет
        if (! is_dir($postersDir)) {
            mkdir($postersDir);
        }
        // создаем каталог webp постеров если его нет
        if (! is_dir($postersWebPDir)) {
            mkdir($postersWebPDir);
        }
        // путь создаваемого файла
        $path = $postersDir.'/'.$movie->getId().'.jpg';
        $pathWebP = $postersWebPDir.'/'.$movie->getId().'.webp';
        // проверяем, мало ли есть уже файл
        if (file_exists($path)) {
            if (file_exists($pathWebP)) {
                $movie->setPoster(true);

                return 'Постер уже был сохранен, проставили метку';
            } else {
                // webP отсутствует, значит выполняем дальше, чтобы сохранился
            }
        }
        if ($movie->getDoUpdate()) {
            $poster = $movie->getKinopoiskPoster();
        } else {
            $poster = null;
        }
        // если указан путь для копирования постера - копируем оттуда и сразу затираем
        if ($movie->getCopyPosterUrl()) {
            $poster = $movie->getCopyPosterUrl();
            $movie->setCopyPosterUrl(null);
        }
        if ($poster) {
            $file = file_get_contents($poster);
            if (file_put_contents($path, $file)) {
                // проверяем, в правильном ли формате находится картинка
                $format = mime_content_type($path);
                if ($format == 'image/png') {
                    $posterImg = imagecreatefrompng($path);
                    // пересохраняем в верном формате
                    imagejpeg($posterImg, $path);
                }
                // создаем WebP версию картинки
                if (function_exists('imagewebp')) {
                    $posterImg = imagecreatefromjpeg($path);
                    if ($posterImg) {
                        if (imagewebp($posterImg, $pathWebP)) {
                            $movie->setPoster(true);

                            return 'Сохранили постер';
                        } else {
                            return '<span style="color: red;">Не удалось создать WebP версию</span>';
                        }
                    } else {
                        return 'Не jpg файл ('.$poster.')';
                    }
                }
            }
        }

        return 'Постер отсутствует';
    }

    // создаем бд kodik из всех имеющихся файлов .json и проставляем уникальные unique_id
    public function createCollapsFromApi()
    {
        $startTime = F::microtime_float();
        Engine::setDebug(false);
        Engine::setDisplayErrors(true);
        // F::alert('Защита от выполнения');
        ini_set('memory_limit', '900M');
        // повышаем время исполнения скрипта на 60 сек с текущего момента
        set_time_limit(180);
        $mwtab = 'collaps';
        $limit = 250;
        $apiUrl = 'https://apicollaps.cc/list?token=56d54e93de7819ef9b8f8b0100aef673&sort=activate_time&limit='.$limit.'&page=';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $apiUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // определим сколько страниц апи нужно парсить
        $arr = json_decode(curl_exec($curl));
        // $arr = json_decode(file_get_contents($apiUrl));
        // F::dump($arr);
        $totalPages = ceil($arr->total / $limit);
        $page = empty($_GET['page']) ? 1 : F::checkstr($_GET['page']);
        $limitPages = empty($_GET['limitPages']) ? 1 : F::checkstr($_GET['limitPages']);
        $pageTo = $page + $limitPages - 1;
        // составляем список страниц для и выполняем мультикурлом
        $objects = [];
        for ($i = $page; $i <= $pageTo; $i++) {
            $object = new \stdClass;
            $object->url = $apiUrl.$i;
            $objects[] = $object;
        }
        $objects = F::multiCurl($objects);
        // F::dump($objects);
        $countMovies = 0;
        $listAll = '';
        foreach ($objects as $object) {
            // если что-то пошло не так и нет объекта, то прекращаем работу
            if (empty($object->result)) {
                F::error('Нет содержимого '.$object->url.'
					<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>
					');
            }
            $arr = json_decode($object->result);
            // F::dump($arr);
            if (! isset($arr->results)) {
                F::error('results не существует '.$object->url.'
					<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>
					');
            }
            $totalMovies = $arr->total;
            $nextApiPage = $arr->next_page;
            foreach ($arr->results as $data) {
                $list = '';
                $countMovies++;
                // addNewMoviesFromCollapsToCinema
                if (! isset($data->kinopoisk_id) or ! $data->kinopoisk_id) {
                    continue;
                }
                $movie_id = Movie::getIdByKinopoiskId($data->kinopoisk_id);
                // F::dump($movie_id);
                if ($movie_id) {
                    $list .= '<li>kp: '.$data->kinopoisk_id.' существует, время: '.round(F::microtime_float() - $startTime, 4).'с</li>';
                    // создаем для использования в дальнейшем для записи эпизодов
                    $movie = new Movie($movie_id);
                } else {
                    $createdMovieId = Movie::add();
                    $movie = new Movie($createdMovieId);
                    $list .= '<li>kp: '.$data->kinopoisk_id.' -> отсутствует, добавили cinema #'.$movie->getId().' время: '.round(F::microtime_float() - $startTime, 4).'с</li>';
                }
                // updateCinemaFromCollaps
                $movie->setCollaps(true);
                $movie->setKinopoiskId($data->kinopoisk_id);
                if (! $movie->getImdbId()) {
                    $movie->setImdbId($data->imdb_id);
                }
                $movie->setYear($data->year);
                $movie->setTitleRu($data->name);
                $movie->setTitleEn($data->origin_name);
                $movie->setAddedAt($data->activate_time);
                $movie->setType(isset($data->seasons) ? 'serial' : 'movie');
                $listTags = '';
                // проставляем тег фильма или сериала
                if ($movie->addTagByName($movie->getType())) {
                    $listTags .= '<li>добавлен тэг: '.$movie->getType().'</li>';
                }
                // если на фильме стоит флаг не обновлять или отсутствует ID кинопоиска то не обновляем некоторую информацию
                if ($movie->getDoUpdate() and $movie->getKinopoiskId()) {
                    // добавляем жанры и страны
                    $tags_arr = [
                        'year' => [$data->year],
                    ];
                    foreach ($tags_arr as $type => $arr) {
                        // если массива тэгов нет то идем дальше
                        if (! $arr) {
                            continue;
                        }
                        foreach ($arr as $v) {
                            if (empty($v)) {
                                continue;
                            } else {
                                $tagId = Tag::getIdByName($v);
                            }
                            // флаг нужен для отчета
                            $created = false;
                            // если такого тэга нет, то создаем его с типом type
                            if (! $tagId) {
                                // создаем тэг одновременно создавая класс
                                $tag = new Tag(Tag::add());
                                $tag->setType($type);
                                $tag->setName($v);
                                $tag->save();
                                $tagId = $tag->getId();
                                $created = true;
                            }
                            // если тип тэга - год, то сначала удаляем все теги данного типа
                            $tag = new Tag($tagId);
                            // F::dump($tag);
                            if ($tag->getType() == 'year') {
                                // получаем список тегов с годом для фильма
                                $movieTags = new MovieTags($movie->getId());
                                $movieTags->setType('year');
                                $arrMovieTags = $movieTags->get();
                                // если больше 1 тега года, то значит сносим годики и пишем текущий тег года
                                if (count($arrMovieTags) > 1) {
                                    // удаляем эти теги
                                    foreach ($arrMovieTags as $yearMovieTag) {
                                        $movie->removeTag($yearMovieTag['id']);
                                    }
                                }
                                // F::dump($movie->getTags());
                            }
                            // если такого тэга нет в списке тэгов для данного фильма, то добавляем его
                            if ($movie->addTag($tagId)) {
                                $listTags .= '<li>'.($created ? 'создан и ' : '').'добавлен тэг: '.$v.' ('.$type.')</li>';
                            }
                        }
                    }
                }
                // $movie->setChecked(true);
                // F::dump($movie);
                $movie->save();
                // dump($movie);
                $list .= '
				<li>Фильм #'.$movie->getId().' обновлен
				'.($listTags ? ('<br>Добавили тэги: <ol>'.$listTags.'</ol>') : '').'
				</li>';
                $listAll .= '<li><ul>'.$list.'</ul></li>';
            }
        }
        $nextPage = $pageTo + 1;
        $nextPageUrl = '/?r=admin/Actions/createCollapsFromApi&page='.$nextPage;
        F::alert('Обработано: '.$countMovies.' элементов. <a href="'.$nextPageUrl.'">Далее, со страницы '.($nextPage).' лимит '.$limitPages.'</a>. Осталось: '.($totalMovies - $pageTo * $limit).'. Выполнено за: '.round(F::microtime_float() - $startTime, 4).' сек.
			<ol>'.$listAll.'</ol>
			<script type="text/javascript">
			setTimeout(window.location.replace(\''.($nextApiPage ? $nextPageUrl : '/?r=admin/Alloha/createFromApi').'\'),5000);
			</script>
			');
    }

    public function findCinemaCollapsMatching()
    {
        $arr = F::query_arr('select id from '.F::typetab('collaps').' where movie_id is null;');
        $list = '';
        foreach ($arr as $v) {
            $arr2 = F::query_assoc('select data from '.F::typetab('collaps').' where id = \''.$v['id'].'\';');
            $data = json_decode($arr2['data']);
            // F::dump($data);
            $movieId = null;
            $foundType = null;
            if (isset($data->kinopoisk_id) and $data->kinopoisk_id) {
                $movies = new Movies;
                $movies->setKinopoiskId($data->kinopoisk_id);
                $arrFound = $movies->get();
                if ($arrFound) {
                    $movieId = $arrFound[0]['id'];
                    $foundType = 'kinopoisk';
                }
            }
            if (isset($data->imdb_id) and $data->imdb_id and ! $movieId) {
                $movies = new Movies;
                $movies->setImdbId($data->imdb_id);
                $arrFound = $movies->get();
                if ($arrFound) {
                    $movieId = $arrFound[0]['id'];
                    $foundType = 'imdb';
                }
            }
            if ($movieId) {
                F::query('update '.F::typetab('collaps').' set movie_id = '.($movieId ? '\''.$movieId.'\'' : 'null').' where id = \''.$v['id'].'\';');
                $list .= '<li>Collaps #'.$v['id'].' -> Cinema #'.$movieId.' (по '.$foundType.')</li>';
            } else {
                $list .= '<li>Collaps #'.$v['id'].' -> не найдено</li>';
            }
        }
        F::alert('<ol>'.$list.'</ol>');
    }

    // сбросить check на всей таблице Collaps
    public function resetCollapsCheckers()
    {
        F::query('update '.F::typetab('collaps').' set checked = 0;');
        F::redirect('/?r=Admin/actions');
    }

    public function convertPostersToWebP()
    {
        Engine::setDisplayErrors(true);
        $limit = empty($_GET['limit']) ? 300 : $_GET['limit'];
        $movies = new Movies;
        $movies->setWithPoster(true);
        $movies->setChecked(false);
        $movies->setPage(1);
        $movies->setLimit($limit);
        $arr = $movies->get();
        $postersWebPDir = Engine::getRootPath().'/static/types/cinema/posters-webp';
        if (! is_dir($postersWebPDir)) {
            mkdir($postersWebPDir);
        }
        // F::dump($arr);
        $list = '';
        foreach ($arr as $v) {
            $movie = new Movie($v['id']);
            $posterWebP = $postersWebPDir.'/'.$movie->getId().'.webp';
            if (file_exists($posterWebP)) {
                $list .= '<li>'.$posterWebP.' существует</li>';
                $movie->setChecked(true);
                $movie->save();

                continue;
            }
            $poster = Engine::getRootPath().$movie->getPosterUrl();
            $posterImg = imagecreatefromjpeg($poster);
            imagewebp($posterImg, $posterWebP);
            $movie->setChecked(true);
            $movie->save();
            $list .= '<li style="color: green;">'.$posterWebP.' сохранили</li>';
        }
        F::alert('Сгенерировано '.$limit.' из '.$movies->getRows().'
			<ul>'.$list.'</ul>
			<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>');
    }

    // создаем бд moonwalk из всех имеющихся файлов .json и проставляем уникальные unique_id
    public function createMoonwalkFromFiles()
    {
        Engine::setDebug(false);
        Engine::setDisplayErrors(true);
        // F::alert('Защита от выполнения');
        ini_set('memory_limit', '900M');
        // повышаем время исполнения скрипта на 60 сек с текущего момента
        set_time_limit(60);
        $files = [
            'serials_foreign.json',
            'serials_russian.json',
            'serials_anime.json',
            'movies_anime.json',
            'movies_russian.json',
            'movies_foreign.json',
            'movies_camrip.json',
            'trailers_all.json',
        ];
        $mwtab = 'moonwalk';
        F::query('truncate '.F::typetab($mwtab).';');
        foreach ($files as $file) {
            // $path = Engine::getRootPath().'/types/cinema/files/moonwalk/'.$file;
            $url = 'http://moonwalk.cc/api/'.$file.'?api_token='.Moonwalk::getApiToken();
            [$type] = explode('_', $file);
            $arr = json_decode(file_get_contents($url));
            // dump($arr);
            if (! isset($arr->report->$type)) {
                F::dump($arr);
            }
            foreach ($arr->report->$type as $v) {
                // собираем unique_id по id кинопоиска либо world_art, если нет ни того ни другого то null и в дальнейшем пустые поля проставятся по порядку на основании русских заголовков
                $unique_id = empty($v->kinopoisk_id) ? (empty($v->world_art_id) ? 'null' : ('\'w_'.$v->world_art_id.'\'')) : ('\'k_'.$v->kinopoisk_id.'\'');
                F::query('insert into '.F::typetab($mwtab).' set
					token=\''.$v->token.'\',
					unique_id='.$unique_id.',
					data=\''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
					;');
            }
        }
        // уникализируем остатки без world id и kinopoisk id
        // получем список всех остатков сгруппированных по русскому заголовку и записываем уник id как n_п/п
        // $arr=F::query_arr('select title_ru from '.F::typetab($mwtab).' where unique_id is null;');
        // $i=0;
        // foreach ($arr as $v) {
        // 	$i++;
        // 	F::query('update '.F::typetab($mwtab).'
        // 		set unique_id=\'n_'.$i.'\'
        // 		where unique_id is null and title_ru=\''.$v['title_ru'].'\';');
        // };
        $this->checkIfMoonwalkIsDamaged();
        F::alert('База '.$mwtab.' пересоздана
			<script>
			setTimeout(
			window.location.replace(\'/?r=admin/Actions/addNewMoviesFromMoonwalkToCinema\'),
			5000);
			</script>
			');
    }

    // создаем бд kodik из всех имеющихся файлов .json и проставляем уникальные unique_id
    public function createKodikFromFiles()
    {
        Engine::setDebug(false);
        Engine::setDisplayErrors(true);
        // F::alert('Защита от выполнения');
        ini_set('memory_limit', '900M');
        // повышаем время исполнения скрипта на 60 сек с текущего момента
        set_time_limit(180);
        $files = [
            'films.json',
            'serials.json',
        ];
        $mwtab = 'kodik';
        F::query('truncate '.F::typetab($mwtab).';');
        foreach ($files as $file) {
            // $path = Engine::getRootPath().'/types/cinema/files/moonwalk/'.$file;
            $url = 'http://dumps.kodik.biz/'.$file;
            [$type] = explode('_', $file);
            $arr = json_decode(file_get_contents($url));
            foreach ($arr as $v) {
                // собираем unique_id по id кинопоиска либо world_art, если нет ни того ни другого то null и в дальнейшем пустые поля проставятся по порядку на основании русских заголовков
                $unique_id = empty($v->kinopoisk_id) ? (empty($v->imdb_id) ? 'null' : ('\'i_'.$v->imdb_id.'\'')) : ('\'k_'.$v->kinopoisk_id.'\'');
                F::query('insert into '.F::typetab($mwtab).' set
					id=\''.$v->id.'\',
					unique_id='.$unique_id.',
					data=\''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
					;');
            }
        }
        // уникализируем остатки без world id и kinopoisk id
        // получем список всех остатков сгруппированных по русскому заголовку и записываем уник id как n_п/п
        // $arr=F::query_arr('select title_ru from '.F::typetab($mwtab).' where unique_id is null;');
        // $i=0;
        // foreach ($arr as $v) {
        // 	$i++;
        // 	F::query('update '.F::typetab($mwtab).'
        // 		set unique_id=\'n_'.$i.'\'
        // 		where unique_id is null and title_ru=\''.$v['title_ru'].'\';');
        // };
        $this->checkIfKodikIsDamaged();
        F::alert('База '.$mwtab.' пересоздана
			<script>
			/*
			setTimeout(
			window.location.replace(\'/?r=admin/Actions/addNewMoviesFromMoonwalkToCinema\'),
			5000);
			*/
			</script>
			');
    }

    public function updateCinemaFromKodik()
    {
        Engine::setDebug(false);
        Engine::setDisplayErrors(true);
        // повышаем время исполнения скрипта на 60 сек с текущего момента
        set_time_limit(60);
        $limit = empty($_GET['limit']) ? 250 : $_GET['limit'];
        $movies = new Movies;
        $movies->setLimit($limit);
        $movies->setPage(1);
        $movies->setChecked(false);
        $movies->setOrder('cinema.add_date desc');
        $movies->setIncludeNullType(true);
        // $movies->setMovieId(32064);
        // F::dump($movies->get());
        // $movies->setWithPoster(false);
        // dump();
        $arr = $movies->get();
        // dump($arr);
        $list = '';
        foreach ($arr as $v) {
            $movie = new Movie($v['id']);
            $mwToken = Kodik::getBestIdByUniqueId($movie->getUniqueId());
            if (! $mwToken) {
                $movie->setChecked(true);
                // $movie->setMwDeleted(true);
                $movie->save();
                $list .= '<li>Фильм #'.$movie->getId().' удален из базы Kodik</li>';
            } else {
                $moonwalk = new Kodik($mwToken);
                // dump($moonwalk);
                // dump($movie->getTags());
                $listTags = '';
                // добавляем тэг "тип фильма"
                if (! empty($moonwalk->getType())) {
                    $tagId = Tag::getIdByName($moonwalk->getType());
                    if ($tagId and ! in_array($tagId, $movie->getTags())) {
                        $movie->addTag($tagId);
                        // F::dump($movie);
                        $listTags .= '<li>добавлен тэг: '.$moonwalk->getType().'</li>';
                    }
                }
                // добавляем тэг "категория", если пусто, значит foreign
                if (! empty($moonwalk->getCategory())) {
                    $tagId = Tag::getIdByName($moonwalk->getCategory());
                    if ($tagId and ! in_array($tagId, $movie->getTags())) {
                        $movie->addTag($tagId);
                        $listTags .= '<li>добавлен тэг: '.$moonwalk->getCategory().'</li>';
                    }
                }
                // добавляем тэг "камрип"
                if ($moonwalk->getCamrip()) {
                    $tagId = Tag::getIdByName('camrip');
                    if ($tagId and ! in_array($tagId, $movie->getTags())) {
                        $movie->addTag($tagId);
                        $listTags .= '<li>добавлен тэг: camrip</li>';
                    }
                }
                // if (strstr($moonwalk->getDescription(),'Прелестные сестры Лалы, Нана и Момо переводятся в ту же школу, что и Рито.')) {
                // 	F::dump($movie);
                // }
                // если на фильме стоит флаг не обновлять или отсутствует ID кинопоиска то не обновляем некоторую информацию
                if (
                    $movie->getDoUpdate() and
                    $moonwalk->getKinopoiskId() and
                    ! strstr($moonwalk->getDescription(), 'Прелестные сестры Лалы, Нана и Момо переводятся в ту же школу, что и Рито.')
                ) {
                    // определяем год
                    $year = $moonwalk->getYear() ? $moonwalk->getYear() : $moonwalk->getAltYear();
                    // добавляем жанры и страны
                    $tags_arr = [
                        'genre' => $moonwalk->getGenres(),
                        'country' => $moonwalk->getCountries(),
                        'actor' => $moonwalk->getActors(),
                        'director' => $moonwalk->getDirectors(),
                        'getStudios' => $moonwalk->getStudios(),
                        'year' => [$year],
                        'translator' => $movie->getTranslators(),
                    ];
                    foreach ($tags_arr as $type => $arr) {
                        // если массива тэгов нет то идем дальше
                        if (! $arr) {
                            continue;
                        }
                        foreach ($arr as $v) {
                            if (empty($v)) {
                                continue;
                            } else {
                                $tagId = Tag::getIdByName($v);
                            }
                            // флаг нужен для отчета
                            $created = false;
                            // если такого тэга нет, то создаем его с типом type
                            if (! $tagId) {
                                // создаем тэг одновременно создавая класс
                                $tag = new Tag(Tag::add());
                                $tag->setType($type);
                                $tag->setName($v);
                                $tag->save();
                                $tagId = $tag->getId();
                                $created = true;
                            }
                            // если тип тэга - год, то сначала удаляем все теги данного типа
                            $tag = new Tag($tagId);
                            // F::dump($tag);
                            if ($tag->getType() == 'year') {
                                // получаем список тегов с годом для фильма
                                $movieTags = new MovieTags($movie->getId());
                                $movieTags->setType('year');
                                $arrMovieTags = $movieTags->get();
                                // если больше 1 тега года, то значит сносим годики и пишем текущий тег года
                                if (count($arrMovieTags) > 1) {
                                    // удаляем эти теги
                                    foreach ($arrMovieTags as $yearMovieTag) {
                                        $movie->removeTag($yearMovieTag['id']);
                                    }
                                }
                                // F::dump($movie->getTags());
                            }
                            // если такого тэга нет в списке тэгов для данного фильма, то добавляем его
                            if (! in_array($tagId, $movie->getTags())) {
                                $movie->addTag($tagId);
                                $listTags .= '<li>'.($created ? 'создан и ' : '').'добавлен тэг: '.$v.' ('.$type.')</li>';
                            }
                        }
                    }
                    // if ($movie->getId() == 40032) {
                    // 	F::dump($moonwalk->getKinopoiskRating());
                    // 	F::dump($movie);
                    // }
                    $movie->setDescription($moonwalk->getDescription());
                    $movie->setYear($year);
                    $movie->setKinopoiskRating($moonwalk->getKinopoiskRating() ? $moonwalk->getKinopoiskRating() : null);
                    $movie->setKinopoiskVotes($moonwalk->getKinopoiskVotes());
                    $movie->setImdbRating($moonwalk->getImdbRating() ? $moonwalk->getImdbRating() : null);
                    $movie->setImdbVotes($moonwalk->getImdbVotes());
                    $movie->setKinopoiskId($moonwalk->getKinopoiskId());
                    $movie->setAge($moonwalk->getAge());
                    $movie->setTagline($moonwalk->getTagline());
                }
                $movie->setTitleRu($moonwalk->getTitleRu());
                $movie->setTitleEn($moonwalk->getTitleEn());
                $movie->setAddedAt($moonwalk->getAddedAt());
                $movie->setType(strstr($moonwalk->getId(), 'serial') ? 'serial' : 'movie');
                $movie->setSourceType($moonwalk->getSourceType());
                // скачиваем постер если его нет
                $listPoster = '';
                if (! $movie->getPoster()) {
                    $postersDir = Engine::getStaticPath().'/types/cinema/posters';
                    $postersWebPDir = Engine::getStaticPath().'/types/cinema/posters-webp';
                    // создаем каталог постеров если его нет
                    if (! is_dir($postersDir)) {
                        mkdir($postersDir);
                    }
                    // создаем каталог webp постеров если его нет
                    if (! is_dir($postersWebPDir)) {
                        mkdir($postersWebPDir);
                    }
                    // путь создаваемого файла
                    $path = $postersDir.'/'.$movie->getId().'.jpg';
                    $pathWebP = $postersWebPDir.'/'.$movie->getId().'.webp';
                    // проверяем, мало ли есть уже файл
                    if (file_exists($path)) {
                        $movie->setPoster(true);
                        $listPoster .= '<li>#'.$movie->getId().' постер уже был сохранен, проставили метку</li>';
                    } else {
                        if ($movie->getDoUpdate()) {
                            $poster = $movie->getKinopoiskPoster();
                        } else {
                            $poster = null;
                        }
                        // если указан путь для копирования постера - копируем оттуда и сразу затираем
                        if ($movie->getCopyPosterUrl()) {
                            $poster = $movie->getCopyPosterUrl();
                            $movie->setCopyPosterUrl(null);
                        }
                        if ($poster) {
                            $file = file_get_contents($poster);
                            if (file_put_contents($path, $file)) {
                                // создаем WebP версию картинки
                                if (function_exists('imagewebp')) {
                                    $posterImg = imagecreatefromjpeg($path);
                                    imagewebp($posterImg, $pathWebP);
                                }
                                $movie->setPoster(true);
                            }
                        }
                        $listPoster .= '<li>#'.$movie->getId().' '.($poster ? 'Сохранили постер' : 'Постер отсутствует').'</li>';
                    }
                }
                // конец скачивания постера
                $movie->setChecked(true);
                // F::dump($movie);
                $movie->save();
                // dump($movie);
                $list .= '
				<li>Фильм #'.$movie->getId().' обновлен
				'.($listPoster ? ('<br>Постер:<ul>'.$listPoster.'</ul>') : '').'
				'.($listTags ? ('<br>Добавили тэги: <ol>'.$listTags.'</ol>') : '').'
				</li>';
            }
        }
        $script = $movies->getRows() ? ('
			<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>
			') : '';
        // обнулить cinema чекеры перед редиректом на addMoviesToDomainMovies
        // ('
        // 	<script>
        // 	setTimeout(
        // 	window.location.replace(\'/?r=Admin/addMoviesToDomainMovies\'),
        // 	5000);
        // 	</script>
        // 	')
        $t = new Template('admin/text');
        $t->v('text', '
			'.$script.'
			<p>Всего: '.$movies->getRows().'</p>
			<p>Лимит: '.$limit.'</p>
			<ol>'.$list.'</ol>
			');
        $this->setMenuVars($t, 'actions');

        return $t->get();
        // return '<ol>Всего: '.$movies->getRows().' '.$list.'</ol>';
    }

    public function updateCinemaPosters()
    {
        F::alert('Защита от выполнения');
        $limit = empty($_GET['limit']) ? 100 : $_GET['limit'];
        $movies = new Movies;
        $movies->setLimit($limit);
        $movies->setPage(1);
        $movies->setChecked(false);
        $movies->setOrder('cinema.add_date');
        $movies->setWithPoster(false);
        $postersDir = Engine::getStaticPath().'/types/cinema/posters';
        if (! is_dir($postersDir)) {
            mkdir($postersDir);
        }
        $arr = $movies->get();
        $list = '';
        foreach ($arr as $v) {
            $movie = new Movie($v['id']);
            $path = $postersDir.'/'.$movie->getId().'.jpg';
            if (file_exists($path)) {
                $list .= '<li>#'.$movie->getId().' уже был сохранен</li>';
            } else {
                $poster = $movie->getKinopoiskPoster();
                if ($poster) {
                    $file = file_get_contents($poster);
                    file_put_contents($path, $file);
                    $movie->setPoster(true);
                }
                $list .= '<li>#'.$movie->getId().' '.($poster ? 'Сохранено' : 'null').'</li>';
            }
            $movie->setChecked(true);
            $movie->save();
        }
        $t = new Template('admin/text');
        $t->v('text', '
			<script type="text/javascript">setTimeout("window.location.reload()",5000);</script>
			<p>Всего: '.$movies->getRows().'</p>
			<p>Лимит: '.$limit.'</p>
			<ol>'.$list.'</ol>
			');
        $this->setMenuVars($t, 'actions');

        return $t->get();
    }

    // обновляем постеры, даже если они существуют (обновляем)
    public function updateAllCinemaPosters()
    {
        // F::alert('Защита от выполнения');
        $limit = empty($_GET['limit']) ? 100 : $_GET['limit'];
        $movies = new Movies;
        $movies->setLimit($limit);
        $movies->setPage(1);
        $movies->setChecked(false);
        $movies->setOrder('cinema.add_date');
        $postersDir = Engine::getStaticPath().'/types/cinema/posters';
        if (! is_dir($postersDir)) {
            mkdir($postersDir);
        }
        $arr = $movies->get();
        $list = '';
        foreach ($arr as $v) {
            $movie = new Movie($v['id']);
            $path = $postersDir.'/'.$movie->getId().'.jpg';
            $poster = $movie->getKinopoiskPoster();
            if ($poster) {
                $file = file_get_contents($poster);
                file_put_contents($path, $file);
                $movie->setPoster(true);
            }
            $list .= '<li>#'.$movie->getId().' '.($poster ? 'Сохранено' : 'Не постера').'</li>';
            $movie->setChecked(true);
            $movie->save();
        }
        $t = new Template('admin/text');
        $t->v('text', '
			<script type="text/javascript">setTimeout("window.location.reload()",5000);</script>
			<p>Всего: '.$movies->getRows().'</p>
			<p>Лимит: '.$limit.'</p>
			<ol>'.$list.'</ol>
			');
        $this->setMenuVars($t, 'actions');

        return $t->get();
    }

    // сбросить check на всей таблице Kodik
    public function resetKodikCheckers()
    {
        Kodiks::resetCheckers();
        // F::alert('Чекеры сброшены в таблице Moonwalk');
        F::redirect('/?r=Admin/actions');
    }

    // ничего страшного что выборка из Moonwalk будет не уникальная с повторяющимися unique_id, т.к. при добавлении в базу нового фильма, следующая неуникальная запись будет пропущена, т.к. уже существует.
    public function addNewMoviesFromKodikToCinema()
    {
        Engine::setDebug(false);
        Engine::setDisplayErrors(true);
        // повышаем время исполнения скрипта на 60 сек с текущего момента
        set_time_limit(60);
        $limit = empty($_GET['limit']) ? 1000 : $_GET['limit'];
        // Engine::setDebug(true);
        $moonwalks = new Kodiks;
        // видео без unique_id нам не нужны
        $moonwalks->setIsUniqueId(true);
        $moonwalks->setLimit($limit);
        $moonwalks->setPage(1);
        $moonwalks->setChecked(false);
        $arr = $moonwalks->get();
        $list = '';
        foreach ($arr as $v) {
            $startTime = F::microtime_float();
            $moonwalk = new Kodik($v['id']);
            $moonwalk->setChecked(true);
            // F::dump($moonwalk);
            $moonwalk->save();
            // видео без kinopoisk_id нам не нужны, т.к. там косячные описания и остальные данные
            if (! $moonwalk->getKinopoiskId()) {
                continue;
            }
            $movie_id = Movie::getIdByUniqueId($moonwalk->getUniqueId());
            // F::dump($movie_id);
            if ($movie_id) {
                $list .= '<li>#'.$moonwalk->getUniqueId().' существует, время: '.round(F::microtime_float() - $startTime, 4).'с</li>';
                // создаем для использования в дальнейшем для записи эпизодов
                $movie = new Movie($movie_id);
            } else {
                // if ($moonwalk->getTitleRu() == '1001 ночь / Тысяча и одна ночь') {
                // 	F::dump('Пытаемся создать 1001 ночь из unique_id = '.$moonwalk->getUniqueId());
                // }
                $createdMovieId = Movie::add();
                $movie = new Movie($createdMovieId);
                $movie->setUniqueId($moonwalk->getUniqueId());
                $movie->save();
                $list .= '<li>#'.$moonwalk->getUniqueId().' ('.$moonwalk->getId().') -> '.($movie_id ? $movie_id : 'отсутствует, добавили cinema #'.$movie->getId()).' время: '.round(F::microtime_float() - $startTime, 4).'с</li>';
            }
            // записываем апдейт серий если нужно
            // $moonwalk->addMoonwalkEpisodesUpdate();
            // записываем эпизоды в Movie_Episodes
            if ($moonwalk->getSeasonEpisodesCount()) {
                // сначала удаляем все эпизоды этого переводчика
                $movie->deleteEpisodes($moonwalk->getTranslator() ? $moonwalk->getTranslator() : '');
                foreach ($moonwalk->getSeasonEpisodesCount() as $season) {
                    foreach ($season->episodes as $episode) {
                        $movie->addEpisode($season->season_number, $episode, $moonwalk->getTranslator());
                    }
                }
            }
        }
        // если закончили процесс добавления, переходим к следующей странице
        $redirect = $moonwalks->getRows() ? ('
			<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>
			') : ('
			<script>
			setTimeout(
			window.location.replace(\'/?r=admin/Actions/updateCinemaFromKodik\'),
			//window.location.replace(\'/?r=admin/Actions/checkIfMoonwalkIsDamaged\'),
			5000);
			</script>
			');
        $t = new Template('admin/text');
        $t->v('text', '
			<p>Всего: '.$moonwalks->getRows().'</p>
			<p>Лимит: '.$limit.'</p>
			<ol>'.$list.'</ol>
			'.$redirect.'
			');
        $this->setMenuVars($t, 'actions');

        return $t->get();
    }

    // пробегаем по всем видео и добавляем их если они еще не добавлены к доменам, поддерживающим автодобавление
    public function addMoviesToDomainMovies()
    {
        return (new \cinema\app\services\cinema_update\CinemaService)->addMovies();
    }

    // проверяем битая ли база kodik
    public function checkIfKodikIsDamaged()
    {
        // повышаем время исполнения скрипта на 60 сек с текущего момента
        set_time_limit(60);
        // ini_set('memory_limit','800M');
        // ini_set("memory_limit", "64M");
        // F::dump(ini_get_all());
        $descriptions = [];
        $arr = F::query_arr('select id,data from '.F::typetab('kodik').';');
        foreach ($arr as $v) {
            $data = json_decode($v['data']);
            if (empty($data->kinopoisk_id)) {
                continue;
            }
            if (isset($data->material_data->description)) {
                $description = $data->material_data->description;
                $descriptions[$description][] = $data->id;
            }
            // if (isset($descriptions[$description])) {
            // 	$
            // }
            // F::dump($data);
        }
        // без описания нас не интересует
        unset($descriptions['']);
        array_multisort(array_map('count', $descriptions), SORT_DESC, $descriptions);
        $descriptions_limited = [];
        $i = 0;
        foreach ($descriptions as $k => $v) {
            $descriptions_limited[$k] = $v;
            $i++;
            if ($i === 20) {
                break;
            }
        }
        F::dump($descriptions_limited);
    }

    // парсит sitemap сайта lorsfilms.tv и записывает все url фильмов в таблиу lordsfilms.tv
    public function parseLordFilmSitemap()
    {
        Engine::setDisplayErrors(true);
        ini_set('memory_limit', '900M');
        $sitemapUrl = 'http://f3.lordfilm7.tv/sitemap.xml';
        // $sitemapUrl = 'http://lordfilm6.tv/sitemap.xml';
        // $sitemapUrl = 'http://x2.lordfilm6.tv/sitemap.xml';
        // $sitemapPage = file_get_contents($sitemapUrl);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $sitemapUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot');
        // curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $sitemapPage = curl_exec($curl);
        F::pre($sitemapPage);
        $items = new \SimpleXMLElement($sitemapPage);
        $list = '';
        // F::pre($items);
        foreach ($items as $item) {
            // F::dump($item);
            if (preg_match('/^(.+)\/([\d]+)-(.+).html$/', $item->loc)) {
                $arr = F::query_arr('select 1 from '.F::typetab('lordsfilms.tv').' where url = \''.$item->loc.'\';');
                if ($arr) {
                    $list .= '<li style="color: gray;">'.$item->loc.'</li>';
                } else {
                    $list .= '<li style="color: green;">'.$item->loc.'</li>';
                    F::query('insert into '.F::typetab('lordsfilms.tv').' set url = \''.$item->loc.'\';');
                }
            } else {
                $list .= '<li style="color: red;">'.$item->loc.'</li>';
            }
        }
        // сбрасываем чекеры
        F::query('update '.F::typetab('lordsfilms.tv').' set checked = 0;');

        return '<ol>'.$list.'</ol>';
    }

    // проходит по всем ссылками в базе lordsfilms.tv, проставляет movie_id и неоходимые данные
    public function updateLordFilmMovies()
    {
        Engine::setDisplayErrors(true);
        $limit = 100;
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS url from '.F::typetab('lordsfilms.tv').' where
			/* movie_id is null and */
			not checked limit '.$limit.';');
        $rows = F::rows_without_limit();
        $list = '';
        foreach ($arr as $v) {
            $url = $v['url'];
            F::query('update '.F::typetab('lordsfilms.tv').' set checked = 1 where url = \''.$url.'\';');
            // $page = file_get_contents($url);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot');
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            $page = curl_exec($curl);
            $kpId = trim(F::copybetwen('<script>var kp_id = ', ';</script>', $page));
            if (empty($kpId)) {
                $list .= '<li style="color: red;">'.$url.' нет kpId</li>';

                continue;
            }
            // F::pre($page);
            // if (!preg_match('/(.*)http:\/\/streamtojupiter\.me\/([\w]+)\/([\w\d]+)\/iframe(.*)/',$page,$args)) {
            // 	$list.= '<li style="color: red;">'.$url.' нет токена</li>';
            // 	continue;
            // }
            // $token = $args[3];
            // $moonwalks = new Moonwalks;
            // $moonwalks->setToken($token);
            // $moonwalks->setLimit(1);
            // $moonwalks->setPage(1);
            // $arrMoonwalks = $moonwalks->get();
            // F::dump($arrMoonwalks);
            // if (!$arrMoonwalks) {
            // 	$list.= '<li style="color: red;">'.$url.' нет такого токена</li>';
            // 	continue;
            // }
            // $moonwalk = new Moonwalk($arrMoonwalks[0]['token']);
            // F::dump($moonwalk);
            // $movieId = Movie::getIdByUniqueId($moonwalk->getUniqueId());
            $movieId = Movie::getIdByKinopoiskId($kpId);
            if (! $movieId) {
                $list .= '<li style="color: red;">'.$url.' нет такого фильма в базе</li>';

                continue;
            } else {
                $list .= '<li style="color: green;">'.$url.'</li>';
            }
            $movie = new Movie($movieId);
            // F::dump($movie);
            $description = F::copybetwen('<div class="fdesc clearfix slice-this" id="s-desc" itemprop="description">', '</div>', $page);
            // F::dump($description);
            $movie->setLordDescription(str_replace('<br>', "\n", $description));
            $movie->save();
            F::query('update '.F::typetab('lordsfilms.tv').' set
				movie_id = \''.$movie->getId().'\'
				where url = \''.$url.'\'
				;');
        }
        F::alert('Завершено '.$limit.' из '.$rows.'
			 <ol>'.$list.'</ol>
			'.($rows ? '<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>' : '').'
			');
    }

    public function clearLordFilmCheckers()
    {
        F::query('update '.F::typetab('lordsfilms.tv').' set checked = 0;');
        F::alert('Сбросили чеккеры lordsfilms.tv');
    }

    // парсит sitemap сайты rserial.com и добавляет отсутствующие страницы в таблицу rserial.com
    public function parseRserialSitemap()
    {
        $foundMovies = [];
        $sitemapUrl = 'https://rserial.com/sitemap.xml';
        $sitemapPage = file_get_contents($sitemapUrl);
        $items = new \SimpleXMLElement($sitemapPage);
        $maxItems = 15;
        // $itemNum = 1;
        $list = '';
        // F::dump($items);
        foreach ($items as $item) {
            // if ($itemNum > $maxItems) {
            // 	break;
            // }
            // F::dump($item);
            if (preg_match('/^(.+)\/(.+)\/([\d]+)-(.+).html$/', $item->loc)) {
                $arr = F::query_arr('select 1 from '.F::typetab('rserial.com').' where url = \''.$item->loc.'\';');
                if ($arr) {
                    $list .= '<li style="color: gray;">'.$item->loc.'</li>';
                } else {
                    $list .= '<li style="color: green;">'.$item->loc.'</li>';
                    F::query('insert into '.F::typetab('rserial.com').' set url = \''.$item->loc.'\';');
                }
            } else {
                $list .= '<li style="color: red;">'.$item->loc.'</li>';
            }
        }
        // сбрасываем чекеры
        F::query('update '.F::typetab('rserial.com').' set checked = 0;');

        // F::dump($foundMovies);
        // F::dump('совп не найдено');
        return '<ol>'.$list.'</ol>';
    }

    // проходит по всем ссылками в базе rserial.com, проставляет movie_id, добавляет плееры в movie_players
    public function updateRserialMovies()
    {
        $limit = 100;
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS url from '.F::typetab('rserial.com').' where
			/* movie_id is null and */
			not checked limit '.$limit.';');
        $rows = F::rows_without_limit();
        foreach ($arr as $v) {
            $url = $v['url'];
            F::query('update '.F::typetab('rserial.com').' set checked = 1 where url = \''.$url.'\';');
            $page = file_get_contents($url);
            $titleRu = F::copybetwen('<meta property="og:title" content="', '" />', $page);
            $blockYear = F::copybetwen('<meta name="keywords" content="', '" />', $page);
            $year = null;
            if (preg_match('/(\d+)/', $blockYear, $yearFound)) {
                // F::dump($year);
                $year = $yearFound[0];
            }
            // F::dump($blockYear);
            // поиск фильма в базе
            $movies = new Movies;
            // F::dump($titleRu);
            // $movies->setSearch($titleRu);
            // $movies->setLeftSearch(true);
            $movies->setTitleRu($titleRu);
            $movies->setYear($year);
            F::query('update '.F::typetab('rserial.com').' set
				title = \''.$titleRu.'\',
				year = \''.$year.'\'
				where url = \''.$url.'\'
				;');
            if ($movies->getRows()) {
                // echo $movies->getRows().'<br>';
                $moviesArr = $movies->get();
                $movie = new Movie($moviesArr[0]['id']);
                // F::dump($item);
                F::query('update '.F::typetab('rserial.com').' set
					movie_id = \''.$movie->getId().'\'
					where url = \''.$url.'\'
					;');
                $players = $this->parseRserialPlayers($page);
                // F::dump($players);
                foreach ($players as $season => $episodes) {
                    foreach ($episodes as $episode) {
                        F::query('
							insert into '.F::typetab('movie_players').' set
							movie_id = \''.$movie->getId().'\',
							season = \''.$season.'\',
							episode = \''.$episode['episode'].'\',
							player = \''.$episode['player'].'\'
							on duplicate key update
							player = \''.$episode['player'].'\'
							;');
                    }
                }
                // F::dump($moviesArr);
                // F::dump($movie);
            }
        }
        F::alert('Завершено '.$limit.' из '.$rows.'
			'.($rows ? '<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>' : '').'
			');
    }

    // парсит плееры с переданной страницы сайта rserial.com и возвращает массив с плеерами
    public function parseRserialPlayers($page = '')
    {
        $players = [];
        // $url = 'https://rserial.com/rossiiskie-serialy/1528-smersh.html';
        // $url = 'https://rserial.com/rossiiskie-serialy/1612-zateryannye-v-lesah.html';
        // $url = 'https://rserial.com/rossiiskie-serialy/810-boec.html';
        // $page = file_get_contents($url);
        // F::query('update '.F::typetab('rserial.com').' set checked = 1 where url = \''.$url.'\';');
        $result = [];
        //
        $blockEpisodes = F::copybetwen('<div class="series">', '</div>', $page);
        // если все ок и у нас есть блок с эпизодами
        if ($blockEpisodes) {
            $dom = new \DOMDocument;
            $dom->loadHTML(mb_convert_encoding($blockEpisodes, 'HTML-ENTITIES', 'UTF-8'));
            // F::dump($dom);
            // $select = $dom->getElementsByTagName('select');
            // F::dump($select);
            $optgroups = $dom->getElementsByTagName('optgroup');
            // если есть элементы optgroup, значит разбито на сезоны
            if ($optgroups->length) {
                foreach ($optgroups as $optgroup) {
                    $seasonName = $optgroup->getAttribute('label');
                    $options = $optgroup->getElementsByTagName('option');
                    foreach ($options as $option) {
                        $player = $option->getAttribute('value');
                        $episodeName = $option->nodeValue;
                        $result[$seasonName][] = ['episode' => $episodeName, 'player' => $player];
                    }
                }
                // если нет optgroup, то всегда 1 сезон
            } else {
                $seasonName = 1;
                $options = $dom->getElementsByTagName('option');
                foreach ($options as $option) {
                    $player = $option->getAttribute('value');
                    $episodeName = $option->nodeValue;
                    $result[$seasonName][] = ['episode' => $episodeName, 'player' => $player];
                }
            }
            // если нет блока с сезонами, значит у нас стоит единый плеер на весь сериал/фильм
        } else {
            $blockPlayer = F::copybetwen('<div class="content-filmplay">', '</div>', $page);
            // F::dump($blockPlayer);
            $dom = new \DOMDocument;
            $dom->loadHTML(mb_convert_encoding($blockPlayer, 'HTML-ENTITIES', 'UTF-8'));
            $iframes = $dom->getElementsByTagName('iframe');
            // F::dump($iframes);
            foreach ($iframes as $iframe) {
                $player = $iframe->getAttribute('src');
                // F::dump($player);
                $result[1][] = ['episode' => 'Все серии', 'player' => $player];
            }
        }

        // f::dump($result);
        return $result;
    }

    public function clearRserialCheckers()
    {
        F::query('update '.F::typetab('rserial.com').' set checked = 0;');
        F::alert('Сбросили чеккеры rserial.com');
    }

    // добавляет новые ID сервиса myshows.me в базу myshows.me
    public function addMyshowsNewIds()
    {
        $arr = F::query_assoc('select max(id) as max_id from '.F::typetab('myshows').';');
        $max_id = $arr['max_id'] ? $arr['max_id'] : 0;
        $request = new \stdClass;
        $request->jsonrpc = '2.0';
        $request->method = 'shows.Ids';
        $request->params = new \stdClass;
        $request->params->fromId = $max_id;
        $request->params->count = 1000;
        $request->id = 1;
        $answer = MyShowsMe::apiRequest($request);
        foreach ($answer->result as $id) {
            F::query('insert into '.F::typetab('myshows').' set id = \''.$id.'\';');
        }
        F::alert('Добавлено '.count($answer->result).' строк');
    }

    // прогон по базе MyShowsMeows.me, обновление data и нахождение соответствия с movie_id
    public function updateMyshowsInfo()
    {
        Engine::setDisplayErrors(true);
        $limit = 15;
        $myshowses = Myshows::select('id')->whereChecked(false)->limit($limit);
        $rows = $myshowses->count();
        $myshowses = $myshowses->get();
        // dd($myshowses);
        $locales = ['en', 'ua', 'ru'];
        $objects = [];
        // ID 2340 создает проблемы, там куча данных
        ini_set('memory_limit', '512M');
        foreach ($myshowses as $myshows) {
            foreach ($locales as $locale) {
                $object = new \stdClass;
                $object->locale = $locale;
                $object->myshows_id = $myshows->id;
                $object->curl = new \stdClass;
                $object->curl->ch = MyShowsMe::getCurl(MyShowsMe::getShowDataRequest($myshows->id), $locale);
                $objects[] = $object;
            }
        }
        $result = F::multiCurl($objects);
        // dd($result);
        $list = '';
        foreach ($result as $object) {
            // $data = MyShowsMe::getShowData($v['id']);
            $result = json_decode($object->result);
            // $data = isset($result->result)?$result->result:F::dump($object);
            if (isset($result->result)) {
                $data = $result->result;
                // dd($data);
            } else {
                // dd($object);
                if (isset($result->error)) {
                    $list .= '<li>'.$result->id.': '.$result->error->data.'</li>';
                } else {
                    // dd($object);
                    exit(var_dump($object));
                    $list .= '<li>'.json_encode($object).'</li>';
                }

                continue;
            }
            $myshows = Myshows::find($object->myshows_id);
            $movie_id = null;
            if (! empty($data->kinopoiskId) and $object->locale == 'ru') {
                // ищем соответствие в cinema.movies
                $movies = new Movies;
                $movies->setKinopoiskId($data->kinopoiskId);
                $arr_movies = $movies->get();
                if ($arr_movies) {
                    // F::dump($data);
                    $movie_id = $arr_movies[0]['id'];
                    $myshows->movie_id = $movie_id;
                    $movie = new Movie($movie_id);
                    $movie->setMyshowsId($data->id);
                    if ($data->started) {
                        $started = null;
                        if (preg_match('/^([\w]+)\/([\d]+)\/([\d]+)$/', $data->started)) {
                            $started = \DateTime::createFromFormat('M/d/Y', $data->started);
                        }
                        if (preg_match('/^([\w]+)\/([\d]+)$/', $data->started)) {
                            $started = \DateTime::createFromFormat('M/Y', $data->started);
                        }
                        if (preg_match('/^([\d]+)$/', $data->started)) {
                            $started = \DateTime::createFromFormat('Y', $data->started);
                        }
                        if ($started) {
                            $movie->setStarted($started->format('Y-m-d'));
                        }
                    }
                    if ($data->ended) {
                        if (preg_match('/^([\w]+)\/([\d]+)\/([\d]+)$/', $data->ended)) {
                            $ended = \DateTime::createFromFormat('M/d/Y', $data->ended);
                        }
                        if (preg_match('/^([\w]+)\/([\d]+)$/', $data->ended)) {
                            $ended = \DateTime::createFromFormat('M/Y', $data->ended);
                        }
                        if (preg_match('/^([\d]+)$/', $data->ended)) {
                            $ended = \DateTime::createFromFormat('Y', $data->ended);
                        }
                        if ($ended) {
                            $movie->setEnded($ended->format('Y-m-d'));
                        }
                    }
                    if (isset($data->network->title)) {
                        $tagId = Tag::getIdByName($data->network->title);
                        // если такого тега нет, создаем его
                        if (! $tagId) {
                            $tag = new Tag(Tag::add());
                            $tag->setType('studio');
                            $tag->setName($data->network->title);
                            $tag->save();
                            $tagId = $tag->getId();
                        }
                        // добавляем тег к фильму, если он не существует
                        $movie->addTag($tagId);
                        // F::dump($data);
                    }
                    // F::dump($data);
                    // записываем эпизоды
                    foreach ($data->episodes as $e) {
                        // F::dump($data);
                        $airDate = new \DateTime($e->airDateUTC);
                        // F::dump($airDate->getTimestamp());
                        F::query('
							insert into '.F::typetab('movie_episodes_data').'
							set movie_id = \''.$movie->getId().'\',
							season = \''.$e->seasonNumber.'\',
							episode = \''.$e->episodeNumber.'\',
							air_date = from_unixtime('.$airDate->getTimestamp().'),
							title = \''.F::escape_string($e->title).'\',
							short_name = \''.F::escape_string($e->shortName).'\'
							on duplicate key update
							air_date = from_unixtime('.$airDate->getTimestamp().'),
							title = \''.F::escape_string($e->title).'\',
							short_name = \''.F::escape_string($e->shortName).'\'
							;');
                        // F::dump('ok');
                    }
                    $movie->setStatus($data->status);
                    // F::dump($movie);
                    $movie->save();
                }
            }
            if ($object->locale == 'ru') {
                $myshows->data = $data;
                // отмечаем строку, потому что локаль ru - последняя в цикле
                $myshows->checked = true;
            } else {
                $myshows->{'data_'.$object->locale} = $data;
            }
            $myshows->save();
        }
        $secondsPer1row = 5 / $limit;
        $minutesToEnd = $rows * $secondsPer1row / 60;
        if ($rows) {
            F::alert('Обновлено '.count($myshowses).' строк из '.$rows.'<br>Осталось '.round($minutesToEnd, 2).' мин.
			'.$list.'
			<script type="text/javascript">setTimeout("window.location.reload()");</script>
			');
        } else {
            F::alert('База myshows обновлена');
        }
    }

    public function clearMyShowsCheckers()
    {
        F::query('update '.F::typetab('myshows').' set checked = 0;');
        F::alert('Сбросили чеккеры myshows');
    }

    // проходит по всем видео содержащим kinopoisk и обновляет информацию с hdvb
    public function updateHdvbInfo()
    {
        $limit = empty($_GET['limit']) ? 250 : $_GET['limit'];
        $movies = new Movies;
        $movies->setChecked(false);
        $movies->setLimit($limit);
        $arr = $movies->get();
        $tagId4k = Tag::getIdByName('4k');
        foreach ($arr as $v) {
            $movie = new Movie($v['id']);
            if (! $movie->getKinopoiskId()) {
                $movie->setChecked(true);
                $movie->save();

                continue;
            }
            $answer = file_get_contents('https://tehranvd.pw/api/movie.json?token=6721e3457628a64855fc0991f1b271af&id_kp='.$movie->getKinopoiskId());
            $movieOpts = json_decode($answer);
            if (is_object($movieOpts)) {
                $movie->setChecked(true);
                $movie->save();

                continue;
            }
            // F::dump($movieOpts);
            foreach ($movieOpts as $m) {
                if ($m->quality === '4К') {
                    $movie->setHdvb4k(true);
                    $movie->addTag($tagId4k);
                    $movie->save();
                }
            }
            $movie->setChecked(true);
            $movie->save();
        }
        F::alert('Завершено '.count($arr).' из '.$movies->getRows().'
			'.($movies->getRows() ? '<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>' : '').'
			');
    }

    // парсит sitemap сайта lorsfilms.tv и записывает все url фильмов в таблиу lordsfilms.tv
    public function parseKinogoSitemap()
    {
        $sitemapUrl = 'https://kinogo.by/sitemap.xml';
        $sitemapPage = file_get_contents($sitemapUrl);
        $items = new \SimpleXMLElement($sitemapPage);
        $list = '';
        // F::dump($items);
        foreach ($items as $item) {
            // F::dump($item);
            if (preg_match('/^(.+)\/([\d]+)-(.+).html$/', $item->loc)) {
                $arr = F::query_arr('select 1 from '.F::typetab('lordsfilms.tv').' where url = \''.$item->loc.'\';');
                if ($arr) {
                    $list .= '<li style="color: gray;">'.$item->loc.'</li>';
                } else {
                    $list .= '<li style="color: green;">'.$item->loc.'</li>';
                    F::query('insert into '.F::typetab('lordsfilms.tv').' set url = \''.$item->loc.'\';');
                }
            } else {
                $list .= '<li style="color: red;">'.$item->loc.'</li>';
            }
        }
        // сбрасываем чекеры
        F::query('update '.F::typetab('lordsfilms.tv').' set checked = 0;');

        return '<ol>'.$list.'</ol>';
    }

    // проходит по всем ссылками в базе lordsfilms.tv, проставляет movie_id и неоходимые данные
    public function updateKinogoMovies()
    {
        $limit = 100;
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS url from '.F::typetab('lordsfilms.tv').' where
			/* movie_id is null and */
			not checked limit '.$limit.';');
        $rows = F::rows_without_limit();
        $list = '';
        foreach ($arr as $v) {
            $url = $v['url'];
            F::query('update '.F::typetab('lordsfilms.tv').' set checked = 1 where url = \''.$url.'\';');
            $page = file_get_contents($url);
            if (! preg_match('/(.*)http:\/\/streamtojupiter\.me\/([\w]+)\/([\w\d]+)\/iframe(.*)/', $page, $args)) {
                $list .= '<li style="color: red;">'.$url.' нет токена</li>';

                continue;
            }
            $token = $args[3];
            $moonwalks = new Moonwalks;
            $moonwalks->setToken($token);
            $moonwalks->setLimit(1);
            $moonwalks->setPage(1);
            $arrMoonwalks = $moonwalks->get();
            // F::dump($arrMoonwalks);
            if (! $arrMoonwalks) {
                $list .= '<li style="color: red;">'.$url.' нет такого токена</li>';

                continue;
            }
            $moonwalk = new Moonwalk($arrMoonwalks[0]['token']);
            // F::dump($moonwalk);
            $movieId = Movie::getIdByUniqueId($moonwalk->getUniqueId());
            if (! $movieId) {
                $list .= '<li style="color: red;">'.$url.' нет такого фильма в базе</li>';

                continue;
            } else {
                $list .= '<li style="color: green;">'.$url.'</li>';
            }
            $movie = new Movie($movieId);
            $description = F::copybetwen('<div class="fdesc clearfix slice-this" id="s-desc" itemprop="description">', '</div>', $page);
            // F::dump($description);
            $movie->setLordDescription(str_replace('<br>', "\n", $description));
            $movie->save();
            F::query('update '.F::typetab('lordsfilms.tv').' set
				movie_id = \''.$movie->getId().'\'
				where url = \''.$url.'\'
				;');
        }
        F::alert('Завершено '.$limit.' из '.$rows.'
			 <ol>'.$list.'</ol>
			'.($rows ? '<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>' : '').'
			');
    }

    public function createVideodb()
    {
        Engine::setDisplayErrors(true);
        $startTime = F::microtime_float();
        // $apiUrl = empty($_GET['apiUrl'])?'https://videodb.win/api/v1/medias/?limit=100':F::checkstr($_GET['apiUrl']);
        $limit = 100;
        $q = empty($_GET['q']) ? 10 : $_GET['q'];
        $offset = empty($_GET['offset']) ? 0 : $_GET['offset'];
        $objects = [];
        for ($i = 0; $i < $q; $i++) {
            $object = new \stdClass;
            $object->url = 'https://videodb.win/api/v1/medias/?limit='.$limit.'&offset='.$offset;
            $object->curl = new \stdClass;
            $object->curl->userpwd = Config::cached()->videodb_credentials->login.':'.Config::cached()->videodb_credentials->password;
            $objects[] = $object;
            // F::dump($object);
            $offset = $offset + $limit;
        }
        // F::dump($objects);
        $objects = F::multiCurl($objects);
        // dd($objects);
        $time['after_multicurl'] = round(F::microtime_float() - $startTime, 4);
        $list = '';
        foreach ($objects as $object) {
            $result = json_decode($object->result);
            // dd($result);
            foreach ($result->results as $v) {
                if (! Videodb::isExists($v->id)) {
                    Videodb::add($v);
                }
                // обновляем нужные данные в таблице
                $videodb = new Videodb($v->id);
                // ставим временную метку, что запись существует в апи на данный момент
                $videodb->setIsExistsOnDate(true);
                $movie_id = null;
                if ($videodb->getKinopoiskId()) {
                    $movie_id = Movie::getIdByKinopoiskId($videodb->getKinopoiskId());
                }
                if (! $movie_id and $videodb->getImdbId()) {
                    $movie_id = Movie::getIdByImdbId($videodb->getImdbId());
                }
                $listMovieAdded = null;
                if ($movie_id) {
                    $movie = new Movie($movie_id);
                    $movie->setVideodb(true);
                    $movie->save();
                } else {
                    if ($videodb->getKinopoiskId()) {
                        // если фильм не найден в Cinema и есть ID кинопоиска, то нужно создать
                        $movie_id = Movie::add();
                        $movie = new Movie($movie_id);
                        $movie->setType($videodb->getType());
                        $movie->setKinopoiskId($videodb->getKinopoiskId());
                        $movie->setImdbId($videodb->getImdbId());
                        $movie->setTitleRu($videodb->getTitleRu());
                        $movie->setTitleEn($videodb->getTitleEn());
                        $movie->setVideodb(true);
                        $movie->save();
                        $listMovieAdded = '<span class="badge bg-success">добавлен movie: '.$movie->getId().'</span>';
                    } else {
                        $listMovieAdded = '<span class="badge bg-warning">не добавлен movie, т.к. нет ID кинопоиска</span>';
                    }
                }
                $videodb->setChecked(true);
                $videodb->setMovieId($movie_id);
                $videodb->save();
                $list .= '<li>#'.$videodb->getId().' (kp: '.$videodb->getKinopoiskId().', imdb: '.$videodb->getImdbId().') -> '.$movie_id.'
				'.$listMovieAdded.'
				</li>';
            }
        }
        // $nextPage = empty($result->next)?null:$result->next;
        F::alert((count($result->results) ? ('
			<p>
			Выполнено за: '.round(F::microtime_float() - $startTime, 4).' сек.<br>
			Multicurl: '.$time['after_multicurl'].' сек.
			</p>
			<p>Далее '.$result->next.'</p>
			<ol>
			'.$list.'
			</ol>
			<script>
			setTimeout(window.location.replace(\'/?r=admin/Actions/createVideodb&offset='.$offset.'\'),5000);
			</script>
			') : '
		<script type="text/javascript">
		setTimeout(window.location.replace(\'/?r=admin/Actions/updateCinemaFromKinopoisk&resetCheckers=1\'),5000);
		</script>
		'));
    }

    // сбросить чекеры Videodb
    public function resetVideodbCheckers()
    {
        F::query('update '.F::typetab('videodb').' set checked = 0;');
        F::redirect('/?r=Admin/actions');
    }

    public function setMovieExistingPlayers($movie = null)
    {
        $arr = F::query_assoc('select 1 from '.F::typetab('videodb').' where movie_id = \''.$movie->getId().'\' limit 1;');
        $movie->setVideodb($arr ? true : false);
        $movie->setVideocdn($arr ? true : false);
        $arr = F::query_assoc('select 1 from '.F::typetab('collaps').' where movie_id = \''.$movie->getId().'\' limit 1;');
        $movie->setCollaps($arr ? true : false);
    }

    // найти аккаунты на одном ip
    public function findSameIpAccounts()
    {
        $arr = F::query_arr('
			select us.ip,us.login,users.partner
			from '.F::typetab('user_sessions').' us
			join '.F::typetab('users').' on users.login = us.login
			where us.ip in (
			select ip
			from '.F::typetab('user_sessions').'
			group by ip
			having count(distinct login) > 1
			)
			group by us.ip,us.login
			order by us.ip,us.login
			;');
        $list = '';
        foreach ($arr as $v) {
            $ipLogins[$v['ip']][$v['login']] = true;
            $warningStyle = '';
            if (isset($ipLogins[$v['ip']][$v['partner']])) {
                $warningStyle = 'color: red;';
            }
            $list .= '<tr style="'.$warningStyle.'"><td>'.$v['ip'].'</td><td>'.$v['login'].'</td><td>'.$v['partner'].'</td></tr>';
        }
        $result = '<table class="table table-sm">'.$list.'</table>';
        F::alert($result);
    }

    public function calcSameMovies()
    {
        $limit = empty($_GET['limit']) ? 10 : F::checkstr($_GET['limit']);
        $start = F::microtime_float();
        $movies = new Movies;
        $movies->setLimit($limit);
        $movies->setChecked(false);
        $arr = $movies->get();
        foreach ($arr as $v) {
            F::query('delete from '.F::typetab('movies_same').'
				where movie_id = \''.$v['id'].'\'
					;');
            $movie = new Movie($v['id']);
            // если нет тегов - пропускаем, иначе будет ошибка в запросе mysql из-за отсутствия переменной q
            $arrTags = $movie->getTags();
            if (! $arrTags) {
                continue;
            }
            $moviesSame = new Movies;
            $moviesSame->setTags($arrTags);
            $moviesSame->setExcludeMovie($movie->getId());
            $moviesSame->setOrder('q desc, cinema.rating_sko desc');
            $moviesSame->setTagsSearchStrict(false);
            $moviesSame->setLimit(100);
            $arrSame = $moviesSame->get();
            foreach ($arrSame as $vSame) {
                F::query('insert into '.F::typetab('movies_same').' set
					movie_id = \''.$v['id'].'\', same_movie_id = \''.$vSame['id'].'\', q = \''.$vSame['q'].'\'
					;');
            }
            $movie->setChecked(true);
            $movie->save();
        }
        $end = F::microtime_float();
        $time = $end - $start;
        F::alert($limit.' из '.$movies->getRows().'. Время: '.$time.' сек
			<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>
			');
    }

    public function domainsAnalytics()
    {
        $t = new Template('admin/domains-analytics/index');
        $domains = new Domains;
        $domains->setType('cinema');
        $domains->setOrder('domains.allowRussianMovies, engine_domains.redirect_to, domains.domain');
        $domains->setIsMirror(false);
        $arr = $domains->get();
        $list = '';
        foreach ($arr as $v) {
            $tList = new Template('admin/domains-analytics/list');
            $domain = new Domain($v['domain']);
            $tList->v('domain', $domain->getDomainDecoded());
            $tList->v('allowRussianMovies', $domain->getAllowRussianMovies() ? 'с РУ' : 'без РУ');
            if ($domain->getRedirectTo()) {
                $redirectToDomain = new EngineDomain($domain->getRedirectTo());
                $tList->v('redirectToDomain', $redirectToDomain->getDomain());
                $tList->v('redirectToDomainAddDate', $redirectToDomain->getAddDate());
                $interval = (new \DateTime)->diff(new \DateTime($redirectToDomain->getAddDate()));
                $tList->v('redirectToDomainAddDateInterval', $interval->format('%a дней назад'));
            } else {
                $tList->v('redirectToDomain', '');
                $tList->v('redirectToDomainAddDate', '');
                $tList->v('redirectToDomainAddDateInterval', '');
            }
            if ($domain->getClientRedirectTo()) {
                $clientRedirectToDomain = new EngineDomain($domain->getClientRedirectTo());
                $tList->v('clientRedirectToDomain', $clientRedirectToDomain->getDomain());
                $tList->v('clientRedirectToDomainAddDate', $clientRedirectToDomain->getAddDate());
                $interval = (new \DateTime)->diff(new \DateTime($clientRedirectToDomain->getAddDate()));
                $tList->v('clientRedirectToDomainAddDateInterval', $interval->format('%a дней назад'));
            } else {
                $tList->v('clientRedirectToDomain', '');
                $tList->v('clientRedirectToDomainAddDate', '');
                $tList->v('clientRedirectToDomainAddDateInterval', '');
            }
            $list .= $tList->get();
        }
        $t->v('list', $list);

        return $t->get();
    }
}
