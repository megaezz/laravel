<?php

namespace cinema\app\controllers;

use App\Http\Controllers\Types\Cinema\CustomerController;
use App\Models\Ip;
use cinema\app\models\Domain;
use cinema\app\models\DomainMovie;
use cinema\app\models\Movie;
use cinema\app\models\MovieTags;
use cinema\app\models\User;
use engine\app\models\Domain as EngineDomain;
use engine\app\models\F;
use engine\app\models\Template;

class AdminLiteController extends CustomerController
{
    public function __construct()
    {
        $this->auth = new CinemaAuthorizationController(['admin', 'admin-lite']);
        $this->user = new User($this->auth->getLogin());
    }

    public function checkIsMovieAllowed()
    {
        $domain = empty($_GET['domain']) ? null : F::checkstr($_GET['domain']);
        $movie_id = empty($_GET['movie_id']) ? null : F::checkstr($_GET['movie_id']);
        $domain_movie_id = empty($_GET['domain_movie_id']) ? null : F::checkstr($_GET['domain_movie_id']);
        if ($domain_movie_id) {
            $domain_movie = new DomainMovie($domain_movie_id);
            $domain = $domain_movie->getDomain();
            $movie_id = $domain_movie->getId();
        }
        if (! $domain) {
            F::error('Не указан domain');
        }
        if (! $movie_id) {
            F::error('Не указан movie_id');
        }
        $domain = new EngineDomain($domain);
        if ($domain->getMirrorOf()) {
            $domain = new Domain($domain->getMirrorOf());
        } else {
            $domain = new Domain($domain->getDomain());
        }
        dd($domain->getIsMovieAllowed($movie_id));
    }

    public function checkIsRealBot()
    {
        $ip = empty($_GET['ip']) ? F::error('Укажите ip') : F::escape_string($_GET['ip']);
        dd(Ip::getIpInfo($ip)->is_real_bot);
    }

    public function sendTelegramByKpId()
    {
        $kp_id = $_POST['kp_id'] ?? null;
        $movie_id = Movie::getIdByKinopoiskId($kp_id);
        if (! $movie_id) {
            F::alert('Материал не найден');
        }
        if ($this->sendPostToTelegramChannel($movie_id)) {
            F::alert('Успешно');
        } else {
            F::alert('Ошибка');
        }
    }

    public function sendPostToTelegramChannel($movie_id = null)
    {
        $apiKey = '1445623561:AAG91ZhvZpDRRL9fyzd1tV0i2mi_Fz5_VcA';
        $chatIds = ['@cinemawatch', '@findwatch', '@directedrobert'];
        // $chatIds = ['@topcinemaonline'];
        $bot = new \TelegramBot\Api\BotApi($apiKey);
        if (! $movie_id) {
            return false;
        }
        $movie = new Movie($movie_id);
        $movieTags = new MovieTags($movie->getId());
        $movieTags->setListTemplate('admin/telegram/item_genre');
        $movieGenres = $movieTags->getGenres();
        // F::dump($movieGenres);
        $post = new Template('admin/telegram/item_post');
        $post->v('year', $movie->getYear());
        $post->v('title', $movie->getTitleRu());
        $post->v('description', $movie->getDescription());
        $post->v('genres', $movieGenres);
        $post->v('type', $movie->getTypeRu(true));
        // $media = new \TelegramBot\Api\Types\InputMedia\ArrayOfInputMedia();
        // $media->addItem(new \TelegramBot\Api\Types\InputMedia\InputMediaPhoto('https://vdb6.awmzone1.pro'.$movie->getPosterUrl()));
        $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
            [
                [
                    [
                        'text' => '▶️ Смотреть онлайн',
                        'url' => 'https://vdb7.awmzone1.pro/getPlayer?kp_id='.$movie->getKinopoiskId(),
                    ],
                ],
            ]
        );
        foreach ($chatIds as $chatId) {
            $bot->sendMessage($chatId, $post->get(), 'html', false, null, $keyboard);
        }

        // $respone = $bot->sendMediaGroup($chatId, $media);
        // $response = $bot->sendMessage($chatId, $post);
        // F::dump($post);
        return true;
    }

    public function domainImages()
    {
        $t = new Template('admin/domain/images_vue');

        return $t->get();
    }
}
