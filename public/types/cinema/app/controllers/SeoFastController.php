<?php

namespace cinema\app\controllers;

use cinema\app\models\Domain;
use cinema\app\models\DomainMovie;
use cinema\app\models\Domains;
use cinema\app\models\Group;
use cinema\app\models\Groups;
use cinema\app\models\Movie;
use cinema\app\models\Movies;
use cinema\app\models\User;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class SeoFastController
{
    // вывод задания для рерайта
    public function rewriteOneRand()
    {
        // Engine::setDebug(false);
        $big = empty($_GET['big']) ? false : true;
        $login = 'seosprint';
        $t = new Template('admin/rewrite/seosprint/one_rand');
        $movies = new Movies;
        if ($big) {
            $movies->setDomainMovieSymbolsFrom(700);
            $movies->setDomainMovieSymbolsTo(1000);
        } else {
            $movies->setDomainMovieSymbolsFrom(250);
            $movies->setDomainMovieSymbolsTo(500);
        }
        // сначала пробуем показать уже забронированные
        $movies->setBookedForLogin($login);
        $movies->setExcludeMovie(Movie::getIdByUniqueId('not_in_base'));
        $movies->setDomainMovieWithCustomerEnoughBalance(true);
        $movies->setExcludeDomainMoviesForWorker($login);
        $movies->setExcludeExpressMovies(true);
        $movies->setExcludeAdult(true);
        $movies->setLimit(1);
        $movies->setWithDomainDescription(false);
        $movies->setOrder('rand()');
        $movies->setPage(1);
        // отображать только те задания которые никто не пишет
        $movies->setDomainMovieWriting(false);
        // если мало забронированных, то берем из общего списка и бронируем его далее
        if ($movies->getRows() < 3) {
            $movies->setDomainMovieWriting(null);
            $movies->setBookedForLogin(null);
        }
        // $movies->setListTemplate('admin/rewrite/item_one_rand');
        // F::dump($movies->getRows());
        // $t->v('cinemaList',$movies->getList()?$movies->getList():'<h1>Задания кончились, зайдите позже</h1>');
        $list = '';
        foreach ($movies->get() as $v) {
            $m = new Template('admin/rewrite/seosprint/item_one_rand');
            $m->v('login', $login);
            $movie = new DomainMovie($v['domain_movies_id']);
            // бронируем это задание
            $movie->setDomainMovieBookedForLogin($login);
            $movie->setDomainMovieBookDate((new \DateTime)->format('Y-m-d H:i:s'));
            $movie->save();
            //
            $customerComment = '';
            if ($movie->getDomainMovieCustomer()) {
                $symbolsComment = '';
                $user = new User($movie->getDomainMovieCustomer());
                $symbolsComment = $movie->getDomainMovieSymbolsFrom() ? ('от '.$movie->getDomainMovieSymbolsFrom()) : '';
                $symbolsComment .= $movie->getDomainMovieSymbolsTo() ? (' до '.$movie->getDomainMovieSymbolsTo()) : '';
                if ($symbolsComment) {
                    $customerComment = '<b>Требуемое количество символов:</b> '.$symbolsComment;
                }
                $m->v('customerComment', $customerComment);
            } else {
                $m->v('customerComment', '');
            }
            if ($movie->getUniqueId() == 'not_in_base') {
                $m->v('type', 'тип неизвестен');
                $m->v('year', 'год неизвестен');
            } else {
                $m->v('type', $movie->getTypeRu(true));
                $m->v('year', $movie->getYear());
            }
            $m->v('domainMovieCustomerComment', $movie->getDomainMovieCustomerComment() ? ('<p><b>Комментарий к заданию:</b> '.nl2br($movie->getDomainMovieCustomerComment()).'</p>') : '');
            $m->v('description', $movie->getDescription() ? $movie->getDescription() : $movie->getRandAltDescription());
            $m->v('domainMovieId', $movie->getDomainMovieId());
            $m->v('titleRu', $movie->getTitleRu());
            $m->v('titleRuUrl', $movie->getTitleRuUrl());
            $m->v('movieId', $movie->getId());
            $m->v('poster', $movie->getPosterOrPlaceholder());
            $m->v('kinopoisk_id', $movie->getKinopoiskId());
            $list .= $m->get();
        }
        $t->v('cinemaList', $list ? $list : '<h1>Задания кончились, зайдите позже</h1>');

        return $t->get();
    }

    // сохранение рерайта и вывод ID фильма
    public function submitRewrite()
    {
        // $login = 'seo-fast';
        $login = empty($_POST['login']) ? F::error('Не передан логин') : F::checkstr($_POST['login']);
        $description = empty($_POST['description']) ? F::error('Не передано описание') : $_POST['description'];
        $domain_movie_id = empty($_POST['cinema_id']) ? F::error('Не передан ID фильма') : $_POST['cinema_id'];
        // $cinema = new Cinema;
        if (! in_array($login, ['seo-fast', 'seo-vip', 'seosprint'])) {
            F::error('Неверный логин');
        }
        $user = new User($login);
        // $domain = empty($_POST['domain'])?F::error('Не передан домен'):F::checkstr($_POST['domain']);
        // $id = DomainMovie::add();
        $movie = new DomainMovie($domain_movie_id);
        // если описание уже было написано
        if ($movie->getDomainMovieDescription()) {
            // если описание было написано другим рерайтером, то создаем новый объект
            $id = DomainMovie::add($movie->getId());
            $movie = new DomainMovie($id);
        }
        $movie->setDomainMovieDescription($description);
        $movie->setDomainMovieDescriptionWriter($login);
        // $movie->setDomainMovieDescriptionPricePer1k($user->getRewritePricePer1k());
        $movie->setDomainMovieDescriptionDate((new \DateTime)->format('Y-m-d H:i:s'));
        // снимаем флаг "пишут"
        $movie->setDomainMovieWriting(false);
        // если для пользователя проставлена премодерация, то ставим отметку не показывать фильм (описание) для покупателя
        if ($user->getPremoderation()) {
            $movie->setDomainMovieShowToCustomer(false);
        }
        $movie->save();

        return '<h1>Спасибо за выполнение задания. Вставьте в отчет число: '.$movie->getDomainMovieId().'</h1>';
    }

    // формирование задания для клика на сайт из поиска
    public function goToSearch()
    {
        $templateName = empty($_GET['tn']) ? null : F::checkstr($_GET['tn']);
        $template = null;
        if ($templateName == 'se_and_social') {
            $template = 'seo-clicks/'.$templateName;
        }
        if (! $template) {
            $template = 'seo-clicks/index';
        }
        $arr = F::query_assoc('
			select domain,se,query,position
			from '.F::typetab('seo-clicks').'
			where active
			order by rand() limit 1;
			');
        $t = new Template($template);
        $t->v('seUrl', $arr['se']);
        $t->v('searchQuery', $arr['query']);
        if ($arr['se'] == 'https://yandex.ru') {
            $t->v('seUrlFull', 'https://yandex.ru/search/?text='.htmlspecialchars($arr['query']));
        }
        if ($arr['se'] == 'https://google.com') {
            $t->v('seUrlFull', 'https://www.google.com/search?q='.htmlspecialchars($arr['query']));
        }
        $t->v('searchQueryUrl', htmlspecialchars($arr['query']));
        $t->v('siteName', str_replace('.', ' ', $arr['domain']));
        $t->v('searchPosition', $arr['position']);

        return $t->get();
    }

    // возвращает любой фильм, сериал, трейлер из активных на сайте, либо группу или главную
    public function getRandSitePage()
    {
        $type = empty($_GET['type']) ? 'compilation' : F::checkstr($_GET['type']);
        $url = null;
        $domains = new Domains;
        $domains->setUseInRedirectToAnyMovie(true);
        $domains->setOrder('rand()');
        $domains->setLimit(1);
        $domain = empty($domains->get()[0]['domain']) ? F::error('No domain to redirect') : $domains->get()[0]['domain'];
        $domainClass = new Domain($domain);
        if ($type == 'compilation') {
            $groups = new Groups;
            $groups->setType('compilation');
            $groups->setActive(true);
            $groups->setDomain($domain);
            $groups->setWithDomainMovies(true);
            // $groups->setOrder('rand()');
            $groups->setLimit(100);
            $arr = F::cache($groups, 'get');
            // F::dump($arr);
            $group = new Group($arr[array_rand($arr)]['id']);
            $url = 'http://'.$domainClass->getLastRedirectDomain().$domainClass->getGroupLink($group);
        }
        if ($type == 'movie') {
            $movies = new Movies;
            $movies->setDomain($domainClass->getDomain());
            $movies->setLimit(1);
            $movies->setPage(1);
            $movies->setYearFrom(2018);
            $movies->setOrder('rand()');
            $movies->setIncludeSerials($domainClass->getIncludeSerials());
            $movies->setIncludeMovies($domainClass->getIncludeMovies());
            $movies->setIncludeTrailers($domainClass->getIncludeTrailers());
            $movies->setActive(true);
            $movies->setDomainClass($domainClass);
            $arr = $movies->get();
            $movie = new DomainMovie($arr[0]['domain_movies_id']);
            $url = 'http://'.($domainClass->getRedirectTo() ? $domainClass->getRedirectTo() : $domainClass->getDomain()).$domainClass->getWatchLink($movie);
        }

        return $url;
    }

    public function randSites()
    {
        $type = empty($_GET['type']) ? 'compilation' : F::checkstr($_GET['type']);
        $domains = new Domains;
        $domains->setUseInRedirectToAnyMovie(true);
        $domains->setOrder('rand()');
        $domains->setLimit(5);
        $arr = $domains->get();
        if (empty($arr)) {
            F::error('Empty list of domains');
        }
        $list = '';
        if ($type == 'compilation') {
            foreach ($arr as $v) {
                $domain = new Domain($v['domain']);
                $groups = new Groups;
                $groups->setType('compilation');
                $groups->setActive(true);
                // $groups->setDomain($domain->getDomain());
                // $groups->setWithDomainMovies(true);
                $groups->setOrder('rand()');
                $groups->setLimit(1);
                $arr_groups = $groups->get();
                if (empty($arr_groups[0]['id'])) {
                    F::error('No groups for domain '.$domain->getDomain());
                }
                $group = new Group($arr_groups[0]['id']);
                $url = 'https://'.$domain->getLastRedirectDomain().$domain->getGroupLink($group);
                $list .= '<li><a href="'.$url.'" target="_blank">'.$url.'</a></li>';
            }
        }
        if (empty($list)) {
            F::error('No content');
        }
        F::alertLite('<h2 class="mb-3">Поделиться ссылками в соц. сетях</h2><ol>'.$list.'</ol>');
    }

    // редиректит на рандомную страницу сайта (тип страницы зависит от настроек)
    public function redirectToAnyMovie()
    {
        $url = $this->getRandSitePage();
        F::redirect($url);
    }

    // открывает iframe с рандомной страницей сайта (тип страницы зависит от настроек)
    public function randPage()
    {
        // $t = new Template('seosprint/iframe');
        // F::dump($this->getRandSitePage());
        // $t->v('url',$this->getRandSitePage());
        // return $t->get();
        // делаем редирект вместо айфрейма, т.к. http сайты не открывались в айфрейме
        F::redirect($this->getRandSitePage());
    }

    public function setMovieWriting()
    {
        Engine::setDebug(false);
        $domain_movie_id = empty($_GET['movie_id']) ? F::error('Не передан ID фильма') : $_GET['movie_id'];
        $movie = new DomainMovie($domain_movie_id);
        // если бронь не для сеоспринта, то запрещаем
        if (! in_array($movie->getDomainMovieBookedForLogin(), ['seo-fast', 'seo-vip', 'seosprint'])) {
            F::error('Запрещено');
        }
        $movie->setDomainMovieWriting(true);
        $movie->save();

        return '';
    }

    public function seoLinks()
    {
        $domain = empty($_GET['domain']) ? F::error('use ?domain=') : F::checkstr($_GET['domain']);
        $template = empty($_GET['template']) ? 'list' : F::checkstr($_GET['template']);
        $domainClass = new Domain($domain);
        $movies = new Movies;
        $movies->setDomain($domain);
        // для формирования ссылок
        // $movies->setDomainClass($domainClass);
        $movies->setIncludeMovies(true);
        $movies->setIncludeTrailers(false);
        $movies->setIncludeSerials(true);
        $movies->setYear(2019);
        $movies->setOrder('cinema.kinopoisk_votes desc');
        $movies->setListTemplate('admin/seo-links/'.$template);
        $movies->setLimit(100);
        $t = new Template('admin/seo-links/index');
        $t->v('domain', $domain);
        $t->v('top2019MoviesList', $movies->getList());
        $movies->setYear(2018);
        $t->v('top2018MoviesList', $movies->getList());
        $movies->setYear(null);
        $t->v('topMoviesList', $movies->getList());

        // $this->setMenuVars($t,'actions');
        return $t->get();
    }
}
