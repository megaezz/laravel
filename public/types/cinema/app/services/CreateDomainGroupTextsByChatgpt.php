<?php

namespace cinema\app\services;

use App\Models\Config;
use cinema\app\models\eloquent\DomainGroup;

class CreateDomainGroupTextsByChatgpt
{
    public function run($args = [])
    {

        config(['openai.api_key' => Config::cached()->chatgpt_api_key]);
        config(['openai.request_timeout' => 50]);

        /* просто выбираем доменную группа без описания */
        $domainGroup = DomainGroup::query()
            ->whereNull('text')
            ->first();

        if (! $domainGroup) {
            return 'Нет заданий';
        }

        $prompts = [];

        $promptVariants = [
            0 => "Напиши сео текст, 2-3 абзаца, объемом 1500 символов для категории киносайта. Название категории: \"{$domainGroup->group->name}\".",
            1 => "Напиши сео текст, 2-3 абзаца, объемом 1500 символов для раздела киносайта. Название раздела: {$domainGroup->group->name}.",
        ];

        $prompts[] = [
            'role' => 'user',
            'content' => $promptVariants[1],
        ];

        try {
            $result = \OpenAI\Laravel\Facades\OpenAI::chat()->create([
                // 'model' => 'gpt-3.5-turbo',
                'model' => 'gpt-4o-mini',
                'messages' => $prompts,
            ]);
        } catch (\Exception $e) {
            logger()->alert($e->getMessage());
        }

        if (isset($result->choices[0]->message->content)) {
            $domainGroup->text = $result->choices[0]->message->content;
            $domainGroup->save();

            $return = [
                'id' => $domainGroup->id,
                'source' => $prompts[0]['content'],
                'rewrited' => $domainGroup->text,
            ];

            logger()->info($return);

            return $return;
        }
    }
}
