<?php

namespace cinema\app\services;

use App\Models\Config;
use cinema\app\models\eloquent\DomainMovie;

class CreateDescriptionsByChatgpt
{
    public $sourceDescriptionMinLength = 300;

    public $sourceDescriptionMaxLength = 2000;

    public function run($args = [])
    {
        $args['count'] = isset($args['count']) ? $args['count'] : false;

        config(['openai.api_key' => Config::cached()->chatgpt_api_key]);
        config(['openai.request_timeout' => 50]);
        $movies = DomainMovie::query()
            ->whereRelation('cinemaDomain', 'ai_descriptions', true)
            ->whereActive(true)
            ->whereNull('description')
            ->whereChecked(false)
        /* если для фильма есть описание кинопоиска или с киноконтента удовлетворяющей длины */
            ->where(function ($query) {
                $query
                    ->whereHas('movie', function ($query) {
                        $query
                            ->whereRaw("CHAR_LENGTH(description) > {$this->sourceDescriptionMinLength}");
                    })
                    ->orWhereHas('domainMovies', function ($query) {
                        $query
                            ->where('description_length', '>', $this->sourceDescriptionMinLength)
                            ->where('description_length', '<', $this->sourceDescriptionMaxLength)
                            ->whereNotNull('customer')
                            ->where('show_to_customer', true);
                    });
            });

        // $movies->ddRawSql();

        if ($args['count']) {
            return $movies->count();
        }

        $movies = $movies
        /* пока не поддерживается больше 1 */
            ->limit(1)
        // ->ddRawSql()
            ->get();

        if (! $movies->count()) {
            return 'Нет заданий';
        }

        $prompts = [];

        foreach ($movies as $movie) {

            /* обновляем модель, чтобы получить свежий чеккер после долгого запроса вначале */
            $movie->refresh();

            /* если материал уже отмечен, значит идет выполнение, пропускаем */
            if ($movie->checked) {
                $errorText = "Описание к {$movie->id} уже генерируется, пропускаем.";
                logger()->alert($errorText);

                return $errorText;
            }

            $movie->checked = true;
            $movie->save();

            /* создаем массив с удовлетворящими описаниями */
            $descriptions = collect();

            /* если описание кинопоиска достаточной длины, добавляем его в массив */
            if ($movie->movie->description and mb_strlen($movie->movie->description) > $this->sourceDescriptionMinLength) {
                $descriptions->push($movie->movie->description);
            }

            /* ищем еще описания на киноконтенте и добавляем в массив */
            $movie->movie
                ->domainMovies()
                ->where('description_length', '>', $this->sourceDescriptionMinLength)
                ->where('description_length', '<', $this->sourceDescriptionMaxLength)
                ->whereNotNull('customer')
                ->where('show_to_customer', true)
                ->each(function ($movie) use ($descriptions) {
                    $descriptions->push($movie->description);
                });

            /* берем рандомное описание из массива и на основе него пишем новое */
            $sourceDescription = $descriptions->random();

            $promptVariants = [
                0 => 'Перепиши описание фильма или сериала, сохраняя смысл. Не заменяй имена собственные, имена персонажей, названия мест. Если в тексте указано название фильма или сериала, то не изменяй его. Не пиши "серия" вместо "сериал". Пиши на русском языке. Количество символов должно быть примерно равно оригиналу, но не менее 700 и не более 1500 без учета пробелов:',
                1 => 'Представь что ты професисональный копирайтер. Перепиши описание фильма или сериала, сохраняя смысл. Не изменяй имена собственные, имена персонажей, названия мест и прочие названия. Не пиши "серия" вместо "сериал". Пиши на русском языке. Количество символов должно быть примерно равно оригиналу, но не менее 700 и не более 1500. Мой текст:',
                2 => 'Прошу тебя выступить в роли профессионального копирайтера и переосмыслить предоставленный текст, используя синонимы, чтобы создать уникальную версию, в которой используется естественный стиль. Пожалуйста, сохрани исходное форматирование и смысл текста, придерживаясь красивого литературного языка и избегая стилистических и грамматических ошибок. Не изменяй имена собственные и различные названия. Обрати внимание, что общий объем текста не должен превышать исходный более чем на 10%. Предоставленный текст будет разделен на части, однако помни, что он образует единую целостность. Пожалуйста, обеспечь высокую сложность и разнообразие контента, чтобы избежать повторов и успешно пройти проверку на уникальность. Мой текст:',
                3 => 'Сделай рерайт описания:',
                4 => 'Сделай рерайт описания, чтобы добиться его уникальности для поисковых систем:',
                5 => 'Представь что ты профессиональный копирайтер. Сделай рерайт описания к '.(($movie->movie->type == 'serial') ? 'сериалу' : 'фильму')." \"{$movie->movie->title_ru}\" на русском языке и упомяни его название в тексте. Пиши только сюжет, не нужно писать обращение к зрителю:",
                6 => 'Напиши описание к '.(($movie->movie->type == 'serial') ? 'сериалу' : 'фильму')." \"{$movie->movie->title_ru}\" на русском языке и упомяни его название в тексте. Не нужно использовать обращение к зрителю, и писать текст, не относящийся к сюжету. Используй для вдохновения это описание",
                7 => 'Переосмысли текст и сделай рерайт на русском языке, не изменяй название:',
                8 => 'Переформулируйте описание данного сериала, учитывая его сюжетные особенности и характер главных персонажей, чтобы создать уникальное описание.',
            ];

            $prompts[] = [
                'role' => 'user',
                'content' => $promptVariants[7].PHP_EOL.PHP_EOL.$sourceDescription,
            ];
        }

        try {
            $result = \OpenAI\Laravel\Facades\OpenAI::chat()->create([
                // 'model' => 'gpt-3.5-turbo',
                'model' => 'gpt-4o-mini',
                'messages' => $prompts,
            ]);
        } catch (\Exception $e) {
            $movie->checked = false;
            $movie->save();
            logger()->alert(__METHOD__.' '.$e->getMessage());
        }

        if (isset($result->choices[0]->message->content)) {
            $movie->description = $result->choices[0]->message->content;
            $movie->checked = false;
            $movie->save();

            $return = [
                'id' => $movie->id,
                'source' => $prompts[0]['content'],
                'rewrited' => $movie->description,
            ];

            logger()->info($return);

            return $return;
        }
    }
}
