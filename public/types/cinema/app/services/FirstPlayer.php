<?php

namespace cinema\app\services;

use cinema\app\models\eloquent\Movie;

class FirstPlayer
{
    public Movie $movie;

    public ?string $mainPlayer;

    public ?string $country;

    public ?bool $show_trailer_for_non_sng;

    public array $players = [
        'allohatv',
        'videodb',
        'hdvb',
        'trailer',
        /* добавляю еще раз videodb, чтобы отобразился фейковый плеер в случае когда ничего нет */
        /* по идее надо изменить эту логику, сделать чтобы getVideodb всегда возвращал true, тогда всегда будет фейковый плеер, но зато не будет трейлеров никогда, подумать над этим */
        'videodb',
    ];

    public function __construct(Movie $movie, ?string $mainPlayer, ?string $country, ?bool $show_trailer_for_non_sng)
    {
        $this->movie = $movie;
        $this->mainPlayer = $mainPlayer;
        $this->country = $country;
        $this->show_trailer_for_non_sng = $show_trailer_for_non_sng;
    }

    public function get()
    {

        /* если есть настройка показывать трейлер для не снг, то проверяем страну и меняем main_player на trailer и оставляем в массиве плееров только trailer, если нужно */
        if ($this->show_trailer_for_non_sng and ! in_array($this->country, ['RU', 'UA', 'KZ', 'BY'])) {
            $this->mainPlayer = 'trailer';
            $this->players = ['trailer'];
        }

        $players = collect($this->players);

        if (! $this->mainPlayer) {
            throw new \Exception('Не указан mainPlayer');
        }

        if (! $players->contains($this->mainPlayer)) {
            throw new \Exception("{$this->mainPlayer} отсутствует в активных плеерах");
        }

        /* поднимаем mainPlayer наверх */
        $players = $players->sortBy(function ($player, $key) {
            return $player === $this->mainPlayer ? 0 : 1;
        });

        foreach ($players as $player) {
            $existMethod = 'get'.ucfirst($player);
            if (! method_exists($this, $existMethod)) {
                throw new \Exception("Отсутствует метод {$existMethod}");
            }
            if ($this->{$existMethod}()) {
                // dump(\DB::getQueryLog());
                return $player;
            }
        }

        /* если ничего так и не было выведено, то выводим последнее значение плеера */
        return $player;
    }

    public function getTrailer()
    {
        return $this->movie->kinopoisk?->mainTrailer ? true : false;
    }

    public function getAllohatv()
    {
        return $this->movie->allohas()->first() ? true : false;
    }

    public function getVideodb()
    {
        return $this->movie->videodbs()->first() ? true : false;
    }

    public function getHdvb()
    {
        $country = $this->country;
        if ($country and ! in_array($country, ['RU', 'UA', 'KZ', 'BY'])) {
            $country = 'EU';
        }

        return $this->movie->hdvbs()->first()?->getPlayer($country) ? true : false;
    }
}
