var VpaidVideoPlayer = function() {
    this.slot_ = null,
    this.top_frame = 0,
    this.AdImpressionSend = 0,
    this.startFrame = 0,
    this.statusPlay = 0,
    this.startAdSlot = 0,
    this.slot2_ = null,
    this.slot2_frame = null,
    this.new_logging_frame = null,
    this.videoSlot_ = null,
    this.eventsCallbacks_ = {},
    this.DocUrl = window.location.href,
    this.Origins = null,
    this.hr = 0;
    try {
        0 < window.location.ancestorOrigins.length && (this.DocUrl = window.location.ancestorOrigins[window.location.ancestorOrigins.length - 1])
    } catch (t) {}
    try {
        0 < window.location.ancestorOrigins.length ? (this.hr = 0, this.Origins = JSON.stringify(window.location.ancestorOrigins)) : (this.hr = 0, this.Origins = "{}")
    } catch (t) {
        this.hr = 1,
        this.Origins = "{}"
    }
    window == window.top ? this.top_frame = 1 : this.top_frame = 0,
    this.attributes_ = {
        companions: "",
        desiredBitrate: 256,
        duration: 10,
        expanded: !1,
        height: 0,
        icons: "",
        linear: !0,
        remainingTime: 10,
        skippableState: !1,
        viewMode: "normal",
        width: 0,
        volume: 1
    },
    this.quartileEvents_ = [{
        event: "AdImpression",
        value: 0
    }, {
        event: "AdVideoStart",
        value: 0
    }, {
        event: "AdVideoFirstQuartile",
        value: 25
    }, {
        event: "AdVideoMidpoint",
        value: 50
    }, {
        event: "AdVideoThirdQuartile",
        value: 75
    }, {
        event: "AdVideoComplete",
        value: 100
    }],
    this.nextQuartileIndex_ = 0,
    this.parameters_ = {}
};
VpaidVideoPlayer.prototype.handshakeVersion = function(t) {
    return "2.0"
},
VpaidVideoPlayer.prototype.initAd = function(t, e, i, s, a, n) {
    this.attributes_.width = t,
    this.attributes_.height = e,
    this.attributes_.viewMode = i,
    this.attributes_.desiredBitrate = s,
    this.slot_ = n.slot;
    try {
        this.slot_.classList.contains("jw-vpaid-wrapper") ? this.playerJS = "JW" : this.slot_.classList.contains("dmp_VideoView-ad-slot") ? this.playerJS = "dmp" : this.GetOwerIfEl(this.GetOwnerWindow(this.slot_)).parentElement.parentElement.parentElement.classList.contains("video-js") ? this.playerJS = "videojs" : this.playerJS = "default"
    } catch (t) {
        this.playerJS = "default"
    }
    if ("default" == this.playerJS)
        try {
            this.GetOwnerWindow(this.slot_).document.getElementsByClassName("copyright")[0].innerHTML.indexOf("Clickaine.com") + 1 && (this.playerJS = "vp-rotate")
        } catch (t) {
            this.playerJS = "default"
        }
    if ("default" == this.playerJS)
        try {
            this.GetOwnerWindow(this.slot_).document.getElementsByClassName("copy_right")[0].innerHTML.indexOf("Clickaine.com") + 1 && (this.playerJS = "vp-rotate")
        } catch (t) {
            this.playerJS = "default"
        }
    this.videoSlot_ = n.videoSlot,
    this.parameters_ = JSON.parse(a.AdParameters),
    this.updateVideoSlot_(),
    this.videoSlot_.addEventListener("timeupdate", this.timeUpdateHandler_.bind(this), !1),
    this.videoSlot_.addEventListener("loadedmetadata", this.loadedMetadata_.bind(this), !1),
    this.videoSlot_.addEventListener("ended", this.stopAd.bind(this), !1),
    this.slot_.addEventListener("click", this.clickAd_.bind(this), !1),
    setTimeout(function() {
        this.callEvent_("AdLoaded"),
        this.sendRequest_("GET", this.parameters_.vpaid_loaded_url)
    }.bind(this), 300)
},
VpaidVideoPlayer.prototype.clickAd_ = function() {
    0 in this.eventsCallbacks_
},
VpaidVideoPlayer.prototype.loadedMetadata_ = function() {
    this.attributes_.duration = this.videoSlot_.duration,
    this.callEvent_("AdDurationChange")
},
VpaidVideoPlayer.prototype.timeUpdateHandler_ = function() {
    var t;
    this.nextQuartileIndex_ >= this.quartileEvents_.length || (100 * this.videoSlot_.currentTime / this.videoSlot_.duration >= this.quartileEvents_[this.nextQuartileIndex_].value && (t = this.quartileEvents_[this.nextQuartileIndex_].event, this.eventsCallbacks_[t](), this.nextQuartileIndex_ += 1), 0 < this.videoSlot_.duration && (this.attributes_.remainingTime = this.videoSlot_.duration - this.videoSlot_.currentTime))
},
VpaidVideoPlayer.prototype.updateVideoSlot_ = function() {
    this.updateVideoPlayerSize_()
},
VpaidVideoPlayer.prototype.updateVideoPlayerSize_ = function() {
    this.videoSlot_.setAttribute("width", this.attributes_.width),
    this.videoSlot_.setAttribute("height", this.attributes_.height)
},
VpaidVideoPlayer.prototype.GetOwerIfEl = function(t) {
    for (var e = 0; e < t.parent.document.getElementsByTagName("iframe").length; e++)
        if (t.parent.document.getElementsByTagName("iframe")[e].contentWindow == t)
            return t.parent.document.getElementsByTagName("iframe")[e]
},
VpaidVideoPlayer.prototype.GetOwnerWindow = function(t) {
    return t.ownerDocument.defaultView || t.ownerDocument.parentWindow
},
VpaidVideoPlayer.prototype.send_YT = function(t, e) {
    this.frameSend.YtSendPostMessage('{"event":"command","func":"' + t + '","args":[' + e + '],"id":1,"channel":"widget"}', "*"),
    this.frameSend.YtSendPostMessage('{"event":"listening","id":1,"channel":"widget"}', "*")
},
VpaidVideoPlayer.prototype.getTemplate = function() {
    this.current_time_ar = [],
    this.skipSec = !1,
    this.skipFin = !1,
    this.ClassE2 = "";
    var e = "Tb_" + Math.random().toString().replace("0.", "");
    this.ClassE2 = e,
    this.div = document.createElement("div"),
    this.div.setAttribute("id", "div_" + e),
    this.div.setAttribute("class", e),
    this.btn_link = document.createElement("a"),
    this.btn_link.setAttribute("id", "link_" + e),
    this.btn_link.setAttribute("target", "blank__" + e);
    var i = function() {
        1 == this.skipSec && 0 == this.skipFin && (this.send_YT("mute", 1), "vp-rotate" == this.playerJS && clearInterval(this.temp3), "JW" == this.playerJS || "vj-codik" == this.playerJS || "videojs" == this.playerJS || "vp-rotate" == this.playerJS || "dmp" == this.playerJS || this.playerJS, this.div.setAttribute("style", "overflow:hidden;height:0px;width:0px;border:0px;position:absolute;top:0px;left:0px;z-index: 1000018;"), setTimeout(function() {
            this.callEvent_("AdVideoComplete"),
            this.callEvent_("AdSkipped"),
            this.sendRequest_("GET", this.parameters_.vpaid_skip_url),
            setTimeout(function() {
                this.callEvent_("AdStopped")
            }.bind(this), 300)
        }.bind(this), 300))
    };
    this.styelDiv = document.createElement("style");
    var s = ".{{tbClass}}{width:100%;height:100%;position:relative;z-index:1000010;}.{{tbClass}} button{cursor:pointer}#irame_{{tbClass}}{width:100%;height:100%;border:0px;}#skeep_{{tbClass}} {position: absolute;right: 0;top: 10%;background: rgba(0, 0, 0, 0.8);color: #fff;font-size: 14px;text-align: center;padding: 10px 30px;overflow: hidden;border: none; }#skeep_{{tbClass}}:before {content: '';position: absolute;left: 0;top: 0;width: 10px;height: 100%;background: rgb(18, 123, 5); }#link_{{tbClass}} {position: absolute;left: 0;top: 10%;background: rgba(0, 0, 0, 0.8);color: #fff;font-size: 14px;text-align: center;padding: 10px 30px;overflow: hidden;text-decoration:none;border: none; }#skeep_{{tbClass}} span,#link_{{tbClass}} span{display:block;position:relative;left:0px;top:0px;width:100%;height:100%;text-align:center;}#link_{{tbClass}}:hover:before,#skeep_{{tbClass}}:hover:before{width: 100%;}#link_{{tbClass}}:hover{text-decoration:none;}#link_{{tbClass}}:before,#skeep_{{tbClass}}:before {content: '';position: absolute;right: 0;top: 0;width: 10px;transition: 0.5s ease-in-out;height: 100%;background: #4B367C; } #copy_{{tbClass}} div{padding:2px 4px;text-align:right;color:#fff;font-size:8px;} #copy_{{tbClass}}{position:absolute;top:0px;left:0px;width:100%;background:rgba(0,0,0,0.5);} #loader_ajax_{{tbClass}}{z-index:13;background:#000;width:100%;height:100%;position:absolute;top:0px;left:0px;text-align:center;vertical-align: middle;}#loader_ajax_{{tbClass}} .lds-ripple {    left: 50%;    top: 50%;    margin-top: -40px;    margin-left: -40px;  display: inline-block;  position: absolute;  width: 80px;  height: 80px;} #loader_ajax_{{tbClass}} .lds-ripple div {  position: absolute;  border: 4px solid #fff;  opacity: 1;  border-radius: 50%;  animation: lds-ripple_{{tbClass}} 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;}#loader_ajax_{{tbClass}} .lds-ripple div:nth-child(2) {  animation-delay: -0.5s;}@keyframes lds-ripple_{{tbClass}} {  0% {    top: 36px;    left: 36px;    width: 0;    height: 0;    opacity: 1;  }  100% {    top: 0px;    left: 0px;    width: 72px;    height: 72px;    opacity: 0;  }}#skeep_{{tbClass}},#link_{{tbClass}},#copy_{{tbClass}}{z-index:10;text-indent: 0px;}@media only screen and (max-width: 480px) {#skeep_{{tbClass}},#link_{{tbClass}}{font-size:12px;padding: 8px 15px;}}".replace(/\{\{tbClass\}\}/g, e);
    this.styelDiv.type = "text/css",
    this.styelDiv.styleSheet ? this.styelDiv.styleSheet.cssText = s : this.styelDiv.appendChild(document.createTextNode(s)),
    this.btn_skip = document.createElement("button"),
    this.btn_skip.setAttribute("id", "skeep_" + e),
    this.loader_ajax1 = document.createElement("div"),
    this.loader_ajax1.innerHTML = '<div class="lds-ripple"><div></div><div></div></div>',
    this.loader_ajax1.setAttribute("id", "loader_ajax_" + e),
    this.btn_skip.addEventListener ? (this.btn_skip.addEventListener("click", i.bind(this)), this.btn_skip.addEventListener("touchstart", i.bind(this))) : (this.btn_skip.attachEvent("onclick", i.bind(this)), this.btn_skip.attachEvent("ontouchstart", i.bind(this))),
    this.div.appendChild(this.btn_skip),
    this.div.appendChild(this.btn_link),
    this.div.appendChild(this.loader_ajax1),
    this.div.appendChild(this.styelDiv),
    this.copy = document.createElement("div"),
    this.copy.setAttribute("id", "copy_" + e),
    this.div.appendChild(this.copy),
    console.log("player", this.playerJS),
    "JW" == this.playerJS ? (this.div.setAttribute("style", "height:100%;width:100%;border:0px;position:absolute;top:0px;left:0px;z-index: 1000018;"), this.GetOwerIfEl(this.GetOwnerWindow(this.slot_)).parentElement.parentElement.appendChild(this.div), this.slot2_ = window) : "dmp" == this.playerJS ? (this.div.setAttribute("style", "height:100%;width:100%;border:0px;position:absolute;top:0px;left:0px;z-index: 1000018;"), this.GetOwerIfEl(this.GetOwnerWindow(this.slot_)).parentElement.parentElement.parentElement.appendChild(this.div), this.slot2_ = this.GetOwnerWindow(this.slot_)) : ("videojs" == this.playerJS ? (this.div.setAttribute("style", "height:100%;width:100%;border:0px;position:absolute;top:0px;left:0px;z-index: 1000018;"), this.GetOwerIfEl(this.GetOwnerWindow(this.slot_)).parentElement.parentElement.parentElement.appendChild(this.div)) : "vj-codik" == this.playerJS ? this.GetOwnerWindow(this.slot_).frameElement.parentElement.parentElement.parentElement.appendChild(this.div) : "vp-rotate-3045" == this.playerJS ? (this.div.setAttribute("style", "height:100%;width:100%;border:0px;position:absolute;top:0px;left:0px;z-index: 999999999999;"), this.GetOwerIfEl(this.GetOwnerWindow(this.slot_)).parentElement.parentElement.parentElement.setAttribute("style", "display:none;"), this.GetOwerIfEl(this.GetOwnerWindow(this.slot_)).parentElement.parentElement.parentElement.parentElement.appendChild(this.div)) : "vp-rotate" == this.playerJS ? (this.callEvent_("AdStarted"), this.div.setAttribute("style", "height:100%;width:100%;border:0px;position:absolute;top:0px;left:0px;z-index: 1000018;"), this.GetOwnerWindow(this.slot_).frameElement.parentElement.appendChild(this.div), this.temp2 = function() {
        t = this.GetOwnerWindow(this.slot_).frameElement,
        "none" == t.style.display ? (console.log("Display is none"), this.stopAd(), clearInterval(this.temp3)) : this.div.setAttribute("style", t.getAttribute("style"))
    }.bind(this), this.temp3 = setInterval(this.temp2, 100)) : this.slot_.appendChild(this.div), this.slot2_ = window),
    this.GetOwnerWindow(this.div) == window.top ? this.top_frame = 1 : this.top_frame = 0;
    i = document.createElement("IFRAME");
    i.src = "about:blank",
    this.div.appendChild(i),
    i.setAttribute("style", "position:fixed;top:-999px;width:0px;height:0px;margin:0px;padding:0px;"),
    i.contentWindow.document.open(),
    i.contentWindow.document.write('<html><head><script>var YtTbFrame=document.createElement("iframe");window.parent.document.getElementById(\'div_' + e + "').appendChild(YtTbFrame);var ChannelYt=new MessageChannel();var YtSend = function(a,b){f=window.parent.document.getElementById(\"irame_" + e + '");f.contentWindow.postMessage(\'{"event":"command","func":"\'+a+\'","args":[\'+b+\'],"id":1,"channel":"widget"}\', "*");f.contentWindow.postMessage(\'{"event":"listening","id":1,"channel":"widget"}\',"*");};var YtSendPostMessage = function(a,b){f=window.parent.document.getElementById("irame_' + e + '");f.contentWindow.postMessage(a, b);};var Listers=function(e){try{ListersVpaid(e);}catch(e){}};var ListersVpaid=function(){};if (window.addEventListener) {window.addEventListener("message", Listers);} else {window.attachEvent("onmessage",  Listers);\t}<\/script></head><body></body></html>'),
    i.contentWindow.document.close(),
    this.frameSend = i.contentWindow,
    this.slot2_frame = this.frameSend.YtTbFrame,
    this.slot2_frame.setAttribute("id", "irame_" + e),
    this.slot2_frame.setAttribute("allow", "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"),
    this.slot2_frame.setAttribute("allowfullscreen", ""),
    this.slot2_frame.src = "javascript:false",
    this.slot2_frame.onload = function() {
        try {
            clearInterval(this.listening_yt)
        } catch (t) {}
        this.frameSend.YtSendPostMessage('{"event":"listening","id":1,"channel":"widget"}', "*"),
        this.listening_yt = setInterval(function() {
            this.frameSend.YtSendPostMessage('{"event":"listening","id":1,"channel":"widget"}', "*")
        }.bind(this), 300)
    }.bind(this),
    "dmp" == this.playerJS && this.callEvent_("AdVideoStart"),
    setTimeout(function() {
        this.GetAjaxAd()
    }.bind(this), 1),
    this.skipTimer = 0
},
VpaidVideoPlayer.prototype.eventFuns = function() {
    var t;
    "" !== this.parameters_.externalSkipTime ? (this.clock = +this.parameters_.externalSkipTime, this.clock2 = +this.parameters_.externalSkipTime) : (this.clock = +this.parameters_.skipTime, this.clock2 = +this.parameters_.skipTime),
    this.btn_skip.innerHTML = "<span>" + this.parameters_.buttonText.skipTime.replace("{timer}", this.clock) + "</span>",
    "site" == this.parameters_.ref_type ? (t = this.GetOwnerWindow(this.slot2_frame), this.slot2_frame.src = this.parameters_.embed_url + "&widget_referrer=" + encodeURIComponent(t.location.href.substring(0, 256)) + "&origin=" + encodeURIComponent(t.location.protocol + "//" + t.location.host)) : this.slot2_frame.src = this.parameters_.embed_url,
    this.btn_link.href = "https://www.youtube.com/watch?v=" + this.parameters_.video_id + "&feature=emb_rel_end",
    this.btn_link.innerHTML = "<span>Перейти на ролик</span>",
    0 == this.startAdSlot && (this.startAdSlot = 1, w = this.slot2_, this.frameSend.ListersVpaid = this.lister.bind(this))
},
VpaidVideoPlayer.prototype.AdImperssion = function(t) {
    console.log("AdImperssion"),
    0 == this.AdImpressionSend && (this.AdImpressionSend = 1, "JW" == this.playerJS ? this.callEvent_("AdStarted") : this.callEvent_("AdImpression"), this.sendRequest_("GET", this.parameters_.vpaid_impression_url))
},
VpaidVideoPlayer.prototype.lister = function(a) {
    try {
        if ("https://www.youtube.com" == a.origin || "https://.." == a.origin || "https://duckling-bureaucrat.ru" == a.origin)
            if (this.loadAdListen = 1, m = a.data, j = JSON.parse(m), "onReady" == j.event) {
                this.startAdFrame = 1,
                console.log("onReady"),
                this.send_YT("addEventListener", '"onError"'),
                this.send_YT("setPlaybackQualityRange", '"small"'),
                this.send_YT("setPlaybackQualityRange", '"tiny"');
                try {
                    this.loader_ajax1.setAttribute("style", "display:none;"),
                    this.loader_ajax1.style.display = "none"
                } catch (e) {}
            } else if ("onError" == j.event) {
                this.sendRequest_("GET", this.parameters_.vpaid_error_url),
                console.log("On error executed"),
                this.stopAd();
                try {
                    clearTimeout(this.timeoutStart)
                } catch (e) {}
                try {
                    this.loader_ajax1.setAttribute("style", "display:block;"),
                    this.loader_ajax1.style.display = "block"
                } catch (e) {}
            } else if ("infoDelivery" == j.event || "infoDelivery" == j.event || "initialDelivery" == j.event) {
                try {
                    null == j.info.videoStats.debug_error && 0 != j.info.duration || (this.sendRequest_("GET", this.parameters_.vpaid_error_url), this.stopAd())
                } catch (e) {}
                try {
                    null != j.info.playbackQuality && "unknown" != j.info.playbackQuality && eval(this.parameters_.jsIMA)
                } catch (e) {}
                if (null != j.info.playerState && "unknown" != j.info.playerState) {
                    var seR2;
                    this.statusPlay = j.info.playerState,
                    0 == this.startFrame && 1 == this.startAdFrame && (this.startFrame = 1, seR2 = function() {
                        0 == this.statusPlay && (this.clock2 = 0),
                        this.clock2 < 0 || 0 == this.clock2 ? 0 == this.skipSec && (console.log("Timer", this.skipTimer), clearInterval(this.setIntevalVpaid), this.skipSec = !0, this.btn_skip.innerHTML = "<span>" + this.parameters_.buttonText.skipText + "</span>", this.sendRequest_("GET", this.parameters_.vpaid_view_url), this.AdImperssion(), this.skipAdNotify()) : (1 == this.statusPlay && this.AdImperssion(), 1 == this.statusPlay && (this.skipTimer = this.skipTimer + 20, this.clock2 = this.clock - (this.current_time_ar[this.current_time_ar.length - 1] - this.current_time_ar[0]), this.clock = this.clock - .25, this.clock2 = this.clock, this.btn_skip.innerHTML = "<span>" + this.parameters_.buttonText.skipTime.replace(/{timer}/i, Math.ceil(this.clock2) < 1 ? 1 : Math.ceil(this.clock2)) + "</span>"))
                    }, clearInterval(this.setIntevalVpaid), this.setIntevalVpaid = setInterval(seR2.bind(this), 400));
                    try {
                        if (1 != j.info.playerState && this.startAdFrame, 1 == j.info.playerState && 1 != this.startAdFrame) {
                            try {
                                clearTimeout(this.timeoutStart)
                            } catch (e) {}
                            this.timeoutStart = setTimeout(function() {
                                this.send_YT("cueVideoById", '{"videoId":"' + this.parameters_.video_id + '"}'),
                                this.startAdFrame = 1;
                                try {
                                    this.loader_ajax1.setAttribute("style", "display:none;"),
                                    this.loader_ajax1.style.display = "none"
                                } catch (t) {}
                                this.startAdFrame = 0
                            }.bind(this), 1500)
                        }
                    } catch (e) {}
                }
                null != j.info.playbackQuality && j.info.playbackQuality,
                null != j.info.currentTimeLastUpdated_ && 1 == this.startFrame && this.current_time_ar.push(j.info.currentTimeLastUpdated_)
            }
    } catch (e) {}
},
VpaidVideoPlayer.prototype.GetAjaxAd = function() {
    this.loadAdListen = 0,
    this.callEvent_("AdPaused"),
    "dmp" == this.playerJS && this.callEvent_("AdVideoStart"),
    this.callEvent_("AdPlaying"),
    this.callEvent_("AdPaused"),
    "default" != this.playerJS && "vj-codik" != this.playerJS && "videojs" != this.playerJS && "vp-rotate" != this.playerJS && "dmp" != this.playerJS && "vp-rotate-3045" != this.playerJS || (console.log("Calling Start Ad"), this.callEvent_("AdStarted")),
    window.location.href.indexOf("https://www.dailymotion.com/"),
    this.loadAdIndex = 0,
    this.GetAjaxAdLoad()
},
VpaidVideoPlayer.prototype.GetAjaxAdLoad = function() {
    2 < this.loadAdIndex ? (console.log("Add Error Called 10"), this.callEvent_("AdError"), this.stopAd()) : (this.loadAdIndex = this.loadAdIndex + 1, this.eventFuns())
},
VpaidVideoPlayer.prototype.startAd = function() {
    this.log("Starting ad"),
    this.getTemplate()
},
VpaidVideoPlayer.prototype.stopAd = function() {
    console.trace(),
    console.log("stopAd"),
    "JW" != this.playerJS && "vj-codik" != this.playerJS && "videojs" != this.playerJS && "vp-rotate" != this.playerJS && "dmp" != this.playerJS && "vp-rotate-3045" != this.playerJS || ("vp-rotate" == this.playerJS && clearInterval(this.temp3), this.div.setAttribute("style", "overflow:hidden;height:0px;width:0px;border:0px;position:absolute;top:0px;left:0px;z-index: 1000018;")),
    this.log("Stopping ad");
    var t = this.callEvent_.bind(this);
    setTimeout(t, 75, ["AdStopped"])
},
VpaidVideoPlayer.prototype.resizeAd = function(t, e, i) {
    this.log("resizeAd " + t + "x" + e + " " + i),
    this.attributes_.width = t,
    this.attributes_.height = e,
    this.attributes_.viewMode = i,
    this.updateVideoPlayerSize_(),
    this.callEvent_("AdSizeChange")
},
VpaidVideoPlayer.prototype.pauseAd = function() {
    this.log("pauseAd"),
    this.videoSlot_.pause(),
    this.callEvent_("AdPaused")
},
VpaidVideoPlayer.prototype.resumeAd = function() {
    this.log("resumeAd"),
    this.videoSlot_.play(),
    this.callEvent_("AdPlaying")
},
VpaidVideoPlayer.prototype.expandAd = function() {
    this.log("expandAd"),
    this.attributes_.expanded = !0,
    this.callEvent_("AdExpanded")
},
VpaidVideoPlayer.prototype.collapseAd = function() {
    this.log("collapseAd"),
    this.attributes_.expanded = !1
},
VpaidVideoPlayer.prototype.skipAd = function() {
    this.log("skipAd"),
    this.attributes_.skippableState && this.callEvent_("AdSkipped")
},
VpaidVideoPlayer.prototype.subscribe = function(t, e, i) {
    i = t.bind(i);
    this.eventsCallbacks_[e] = i
},
VpaidVideoPlayer.prototype.unsubscribe = function(t) {
    this.eventsCallbacks_[t] = null
},
VpaidVideoPlayer.prototype.getAdLinear = function() {
    return this.attributes_.linear
},
VpaidVideoPlayer.prototype.getAdWidth = function() {
    return this.attributes_.width
},
VpaidVideoPlayer.prototype.getAdHeight = function() {
    return this.attributes_.height
},
VpaidVideoPlayer.prototype.getAdExpanded = function() {
    return this.log("getAdExpanded"), this.attributes_.expanded
},
VpaidVideoPlayer.prototype.getAdSkippableState = function() {
    return this.log("getAdSkippableState"), this.attributes_.skippableState
},
VpaidVideoPlayer.prototype.getAdRemainingTime = function() {
    return this.attributes_.remainingTime
},
VpaidVideoPlayer.prototype.getAdDuration = function() {
    return this.attributes_.duration
},
VpaidVideoPlayer.prototype.getAdVolume = function() {
    return this.log("getAdVolume"), this.attributes_.volume
},
VpaidVideoPlayer.prototype.setAdVolume = function(t) {
    this.attributes_.volume = t,
    this.log("setAdVolume " + t),
    this.callEvent_("AdVolumeChange")
},
VpaidVideoPlayer.prototype.getAdCompanions = function() {
    return this.attributes_.companions
},
VpaidVideoPlayer.prototype.getAdIcons = function() {
    return this.attributes_.icons
},
VpaidVideoPlayer.prototype.log = function(t) {
    console.log(t)
},
VpaidVideoPlayer.prototype.callEvent_ = function(t) {
    console.log(t),
    t in this.eventsCallbacks_ && this.eventsCallbacks_[t]()
},
VpaidVideoPlayer.prototype.skipAdNotify = function() {
    try {
        if (void 0 === this.counter && (this.counter = 0), this.counter++, 1 < this.counter)
            return;
        if (!0 === this.parameters_.useJson)
            for (var t = this.parameters_.vast.skip, e = 0; e < t.length; e++)
                console.log(t[e])
    } catch (t) {}
},
VpaidVideoPlayer.prototype.sendRequest_ = function(t, e, i) {
    var s = new XMLHttpRequest;
    s.onreadystatechange = function() {
        s.readyState == XMLHttpRequest.DONE && void 0 !== i && i()
    },
    s.open(t, e, !0),
    s.send()
};
var getVPAIDAd = function() {
    return new VpaidVideoPlayer
};

