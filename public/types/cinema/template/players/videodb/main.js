function initPlayer(data) {
	data = JSON.parse(atob(data));
	// document.title = data.movieTitle;
	var playerTitle = data.movieType + ' &laquo;' + data.movieTitle + '&raquo; (' + data.movieYear + ' г.) ';
	var isMovie = false;
	if (data.playlist.length == 1) {
		/* если только 1 эпизод, то не выводим меню эпизодов */
		if (data.playlist[0].folder.length == 1) {
			var playlist = data.playlist[0].folder[0].file;
			/* а еще значит это фильм */
			isMovie = true;
		} else {
			var playlist = data.playlist[0].folder;
		}
	} else {
		var playlist = data.playlist;
	}

	var player = new Playerjs({
		id: 'player',
		title: playerTitle,
		file: playlist,
		poster: '/types/cinema/template/images/preview.jpg',
		default_quality: '360p',
		// plstart: data.videodbId
	});
}

function PlayerjsEvents(event, id, info){
	if (event == "play") {
		$('.settings').hide();
	}
	if (event == "pause" || event == "stop") {
		$('.settings').show();
	}
	if (event == 'new' || event == 'init') {
		/* при инициализации плеера или смене файла
		получаем ID текущего плейлиста, который является объектом информации об эпизоде и сезоне */
		var episodeData = player.api('playlist_id');
		if (!isMovie) {
			/* формируем название сериала */
			player.api('title', playerTitle + ' Сезон ' + episodeData.season + ', Серия ' + episodeData.episode);
		}
	}
}