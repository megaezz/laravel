<?php

@error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);
@ini_set('display_errors', true);
@ini_set('html_errors', true);
@ini_set('error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE);
define('DATALIFEENGINE', true);
define('ROOT_DIR', substr(dirname(__FILE__), 0, -12));
define('ENGINE_DIR', ROOT_DIR.'/engine');
include ENGINE_DIR.'/data/config.php';
include ENGINE_DIR.'/data/moonserials_options.php'; // конфиг модуля
if ($config['http_home_url'] == '') {
    $config['http_home_url'] = explode('engine/ajax/newms.php', $_SERVER['PHP_SELF']);
    $config['http_home_url'] = reset($config['http_home_url']);
    $config['http_home_url'] = 'http://'.$_SERVER['HTTP_HOST'].$config['http_home_url'];
}
require_once ENGINE_DIR.'/classes/mysql.php';
require_once ENGINE_DIR.'/data/dbconfig.php';
require_once ENGINE_DIR.'/modules/functions.php';
require_once ENGINE_DIR.'/classes/newms.class.php';

if (function_exists('dle_session')) {
    dle_session();
}

$utf = (strtolower($config['charset']) == 'utf-8') ? true : false;

if ($utf) {
    header('Content-Type: text/html; charset=utf-8');
} else {
    header('Content-Type: text/html; charset=windows-1251');
}

if (isset($_POST['kpid'])) {
    $kpid = $_POST['kpid'];
}
if (isset($_POST['season'])) {
    $season_in = $_POST['season'];
}
if (isset($_POST['series'])) {
    $series_in = $_POST['series'];
}
if (isset($_POST['action'])) {
    $action = $_POST['action'];
}
if (isset($_POST['trid'])) {
    $trid = $_POST['trid'];
}
if (isset($_POST['seasid'])) {
    $seasid = $_POST['seasid'];
}
if (isset($_POST['cook'])) {
    $cook = $_POST['cook'];
}
if (isset($_POST['newsid'])) {
    $newsid = $_POST['newsid'];
}
if (isset($_POST['showseason'])) {
    $showseason = $_POST['showseason'];
}

if ($kpid && $action == 'pageload') {

    $dbxfields = $db->super_query('SELECT `xfields` FROM   '.PREFIX."_post  WHERE `id` = '{$newsid}'");

    $xfieldsdatanms = xfieldsdataload($dbxfields['xfields']);
    $xfields_n = $xfieldsdatanms;
    // поточные сезон серия
    if ($xfieldsdatanms[$moonserials_options['field_season']] and $xfieldsdatanms[$moonserials_options['field_series']] and $xfieldsdatanms[$moonserials_options['field_season']] !== $moonserials_options['if_series_ower']) {
        $str = strpos($xfieldsdatanms[$moonserials_options['field_season']], ' ');
        $seasonTemp = substr($xfieldsdatanms[$moonserials_options['field_season']], 0, $str);
        $str = strpos($xfieldsdatanms[$moonserials_options['field_series']], ' ');
        $seriesTemp = substr($xfieldsdatanms[$moonserials_options['field_series']], 0, $str);
    } else {
        $seasonTemp = false;
        $seriesTemp = false;
    }
    // определение количества серий в сезоне
    if ($xfieldsdatanms[$moonserials_options['field_series-max']] and $moonserials_options['allow_module_new_series_max'] > 0 and $xfieldsdatanms[$moonserials_options['field_season_iframe']]) {
        $seriesMax = $xfieldsdatanms[$moonserials_options['field_series-max']];
    }
    // если сериал закончен
    if ($xfieldsdatanms[$moonserials_options['field_status_name']] == $moonserials_options['field_status']) {
        if ($moonserials_options['allow_fields_spy']) {
            // поле сезон добавляем значение "если сериал закончен"
            $xfields_n[$moonserials_options['field_series']] = $moonserials_options['if_series_ower'];
            // если поле серия не пустое, чистим его
            /*if ($xfieldsdatanms[$moonserials_options['field_season']])
            unset($xfields_n[$moonserials_options['field_season']]);*/
            // обрабатываем доп поля
            foreach ($xfields_n as $key => &$value) {
                $arr_field[] = $key.'|'.str_replace('|', '&#124;', $value);
            }
            $xfields_n = implode('||', $arr_field);
            unset($arr_field);
            $xfields_n = $db->safesql($xfields_n);
            // и в базу
            $db->query('UPDATE '.PREFIX."_post SET `xfields` = '$xfields_n' WHERE `id` = '{$newsid}'");
        }
    }
    if ($curl = curl_init()) {
        curl_setopt($curl, CURLOPT_URL, 'http://moonwalk.cc/api/videos.json?kinopoisk_id='.$kpid.'&api_token='.$moonserials_options['api_token'].'');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        $out = curl_exec($curl);
        $output_tr = json_decode($out, true);
        curl_close($curl);
    }
    if ($config['charset'] == 'windows-1251') {
        encoding($output_tr, 'UTF-8', 'WINDOWS-1251');
    }
    if (! $output_tr or $output_tr['error'] == 'videos_not_found') {
        $player = '<img src="/images/serial.jpg" width="100%" style="border: none !important" />';
    } else {
        foreach ($output_tr as $kk => $vv) {
            $seasonfound = 0;
            $seriesfound = 0;
            $seasonfoundms = 0;
            $seriesfoundms = 0;
            $seasons = false;
            if ($curl = curl_init()) {
                curl_setopt($curl, CURLOPT_URL, 'http://moonwalk.cc/api/serial_episodes.json?kinopoisk_id='.$kpid.'&api_token='.$moonserials_options['api_token'].'&translator_id='.$vv['translator_id'].'');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl, CURLOPT_TIMEOUT, 5);
                $out = curl_exec($curl);
                $output_sr = json_decode($out, true);
                curl_close($curl);
            }
            // если включено пропускать субтитры
            if ($moonserials_options['disable_sub'] > 0 and $vv['translator'] == 'Субтитры') {
                continue;
            }
            if ($config['charset'] == 'windows-1251') {
                encoding($output_sr, 'UTF-8', 'WINDOWS-1251');
            }
            $title_ru = $vv['title_ru'];
            if (! $vv['translator']) {
                $vv['translator'] = 'Неизвестно';
            } elseif ($vv['translator'] == 'двухголосый закадровый') {
                $vv['translator'] = 'Двухголосый';
            } elseif ($vv['translator'] == 'многоголосый закадровый') {
                $vv['translator'] = 'Многоголосый';
            } elseif ($vv['translator'] == 'одноголосый закадровый') {
                $vv['translator'] = 'Одноголосый';
            }

            if ($moonserials_options['field_studios_sp'] and $vv['translator'] !== 'Неизвестно') {
                $studios_sp = ! $studios_sp ? $vv['translator'] : $studios_sp.', '.$vv['translator'];
            }
            foreach ($output_sr['season_episodes_count'] as $kkk => $vvv) {
                $seasons[] = $vvv['season_number'];
                if ($season_in > 0) {
                    if ($vvv['season_number'] == $season_in) {
                        $season = $vvv['season_number'];
                        $seriesA = $vvv['episodes'];
                        $seriesC = $vvv['episodes_count'];
                    }
                } else {
                    if ($vvv['season_number'] > $season) {
                        $season = $vvv['season_number'];
                        $seriesA = $vvv['episodes'];
                        $seriesC = $vvv['episodes_count'];
                        $episodes = $vvv['episodes'];
                    }
                }
                if ($season_in > 0) {
                    if ($vvv['season_number'] == $season_in) {
                        $seasonfound = 1;
                        $episodes = $vvv['episodes'];
                        if ($series_in > 0) {
                            foreach ($vvv['episodes'] as $vvvvv) {
                                if ($vvvvv == $series_in) {
                                    $seriesfound = 1;
                                    break;
                                }
                            }
                        }
                    }
                }
                foreach ($seriesA as $vvvv) {
                    if ($vvvv == 0) {
                        $seriesT = -1;
                        break;
                    }
                }
                if ($seriesT == -1 and $seriesC == 1) {
                    $series = $moonserials_options['if_pilot_series'];
                } else {
                    $series = max($seriesA);
                }
                if ($xfieldsdatanms[$moonserials_options['field_season_iframe']]) {
                    if ($vvv['season_number'] == $xfieldsdatanms[$moonserials_options['field_season_iframe']]) {
                        $seasonms = $vvv['season_number'];
                        $seriesAms = $vvv['episodes'];
                        $seriesCms = $vvv['episodes_count'];
                        // break;
                    }
                } else {
                    if ($vvv['season_number'] > $seasonms) {
                        $seasonms = $vvv['season_number'];
                        $seriesAms = $vvv['episodes'];
                        $seriesCms = $vvv['episodes_count'];
                    }
                }
                // есть ли сезон, который надо вывести
                if ($xfieldsdatanms[$moonserials_options['field_season_iframe']]) {
                    if ($vvv['season_number'] == $xfieldsdatanms[$moonserials_options['field_season_iframe']]) {
                        $seasonfoundms = 1;
                        if ($xfieldsdatanms[$moonserials_options['field_series_iframe']]) {
                            foreach ((array) $vvv['episodes'] as $vvvvv) {
                                if ($vvvvv == $xfieldsdatanms[$moonserials_options['field_series_iframe']]) {
                                    $seriesfoundms = 1;
                                    break;
                                }
                            }
                        }
                    }
                }
                // может есть пилотная серия
                foreach ((array) $seriesAms as $vvvv) {
                    if ($vvvv == 0) {
                        $seriesTms = -1;
                    }
                }
                // если есть пилотная и серия только одна
                if ($seriesTms == -1 and $seriesCms == 1) {
                    $seriesms = $moonserials_options['if_pilot_series'];
                } else {
                    $seriesms = max($seriesAms);
                }
            }
            $tabs[] = ['translator' => $vv['translator'], 'trid' => $vv['translator_id'], 'iframe_url' => $vv['iframe_url'], 'season' => $season, 'series' => $series, 'seasonfound' => $seasonfound, 'seriesfound' => $seriesfound, 'episodes' => $episodes, 'seasons' => $seasons];
            $tabsms[] = ['translator' => $vv['translator'], 'iframe_url' => $vv['iframe_url'], 'season' => $seasonms, 'series' => $seriesms, 'seasonfound' => $seasonfoundms, 'seriesfound' => $seriesfoundms];
            unset($season);
            unset($series);
            unset($seasonms);
            unset($seriesms);
        }
        foreach ($tabs as $k => $v) {
            if ($v['series'] == $moonserials_options['if_pilot_series']) {
                $v['series'] = 0;
            }
            if ($v['translator'] == 'Субтитры') {
                $v['series'] = $v['series'] - 0.5;
            }
            $se[$k] = $v['season'];
            $ep[$k] = $v['series'];
        }
        array_multisort($se, SORT_NUMERIC, SORT_DESC, $ep, SORT_NUMERIC, SORT_DESC, $tabs);
        foreach ($tabsms as $k => $v) {
            if ($v['series'] == $moonserials_options['if_pilot_series']) {
                $v['series'] = 0;
            }
            if ($v['translator'] == 'Субтитры') {
                $v['series'] = $v['series'] - 0.5;
            }
            $se[$k] = $v['season'];
            $ep[$k] = $v['series'];
        }
        array_multisort($se, SORT_NUMERIC, SORT_DESC, $ep, SORT_NUMERIC, SORT_DESC, $tabsms);
        if ($trid < 1) {
            $active = ' active';
        }
        if ($tabs[0]['translator']) {
            $translator_id = $tabs[0]['trid'];
            foreach ($tabs as $tabskey => $tabsvalue) {
                if ($trid > 0) {
                    $active = $trid == $tabsvalue['trid'] ? ' active' : false;
                    $iframe_url = $trid == $tabsvalue['trid'] ? $tabsvalue['iframe_url'] : $tabs[0]['iframe_url'];
                    $translator_id = $trid;
                    if ($trid == $tabsvalue['trid']) {
                        $translator_name = ' в озвучке &laquo;'.$tabsvalue['translator'].'&raquo;';
                    }
                }
                if ($season_in and $series_in and $tabsvalue['seasonfound'] > 0 and $tabsvalue['seriesfound'] > 0) {
                    $translators .= '<li onclick="translates();" class="b-translator__item'.$active.'" data-trid="'.$tabsvalue['trid'].'">'.$tabsvalue['translator'].'</li>';
                } elseif ($season_in and ! $series_in and $tabsvalue['seasonfound'] > 0) {
                    $translators .= '<li onclick="translates();" class="b-translator__item'.$active.'" data-trid="'.$tabsvalue['trid'].'">'.$tabsvalue['translator'].'</li>';
                } elseif (! $season_in and ! $series_in) {
                    $translators .= '<li onclick="translates();" class="b-translator__item'.$active.'" data-trid="'.$tabsvalue['trid'].'">'.$tabsvalue['translator'].'</li>';
                }
                if ($trid < 1) {
                    $active = false;
                }
            }
            $translators = '<div class="b-translators__block"><div class="b-translators__title">В русской озвучке от:</div><ul id="translators-list" class="b-translators__list">'.$translators.'</ul></div>';
        }
        if ($season_in > 0 && $cook > 0) {
            $lastepisodeout = '<div class="b-post__lastepisodeout"><h2>'.$output_sr['serial']['title_ru'].'<span id="les">. Вы остановились на '.$season_in.' сезоне '.$series_in.' серии'.$translator_name.'</span><img src="/images/process-stop.png" onclick="del();" id="lesc" title="Удалить отметку" /></h2> </div>';
        } else {
            $lastepisodeout = '<div class="b-post__lastepisodeout"><h2>'.$output_sr['serial']['title_ru'].'<span id="ln"> '.$tabs[0]['season'].' сезон '.$tabs[0]['series'].' серия</span><span id="les"></span></h2></div>';
        }
        $player = '<div id="player" class="b-player" style="text-align: center;">';
        $seasons = '<ul id="simple-seasons-tabs" class="b-simple_seasons__list clearfix">';
        if (! $tabs[0]['trid']) {
            $tabs[0]['trid'] = '0';
        }
        asort($tabs[0]['seasons']);
        foreach ($tabs[0]['seasons'] as $se_num) {
            if ($season_in > 0) {
                $active = $se_num == $season_in ? ' active' : false;
            } else {
                $active = $se_num == $tabs[0]['season'] ? ' active' : false;
            }
            $seasons .= '<li onclick="seasons();" class="b-simple_season__item'.$active.'" data-seasid="'.$se_num.'" data-trid="'.$translator_id.'">Сезон '.$se_num.'</li>';
            $episodes = '<div id="simple-episodes-tabs">';
            $episodes .= '<ul id="simple-episodes-list" class="b-simple_episodes__list clearfix">';
            asort($tabs[0]['episodes']);
            $se_num_ep = $season_in ? $season_in : $se_num;
            foreach ($tabs[0]['episodes'] as $ep_num) {
                if ($series_in > 0) {
                    $active = $ep_num == $series_in ? ' active' : false;
                } else {
                    $active = $ep_num == $tabs[0]['series'] ? ' active' : false;
                }
                $episodes .= '<li onclick="episodes();" class="b-simple_episode__item'.$active.'" data-season_id="'.$se_num_ep.'" data-episode_id="'.$ep_num.'" data-trid="'.$translator_id.'">Серия '.$ep_num.'</li>';
            }
            $episodes .= '</ul>';
            $episodes .= '</div>';
        }
        $seasons .= '</ul>';
        $ssn = $season_in > 0 ? '&season='.$season_in : '&season='.$tabs[0]['season'];
        $srs = $series_in > 0 ? '&episode='.$series_in : '&episode='.$tabs[0]['series'];
        $iframe .= '<div id="ibox"><div id="player-loader-overlay"></div><div id="moon" style="height: 100%; margin: 0 auto; width: 100%;"><iframe src="'.$tabs[0]['iframe_url'].'?nocontrols=1'.$ssn.$srs.'" width="724" height="460" frameborder="0" allowfullscreen=""></iframe></div>';

        if ($showseason > 0) {
            $player = $player.$seasons.$iframe.$episodes;
        } else {
            $player = $player.$iframe.$episodes;
        }

        $player .= '</div></div>';
    }
    $player = $lastepisodeout.$translators.$player;
    if ($season_in > $tabs[0]['season'] || $season_in <= $tabs[0]['season'] && $series_in > $tabs[0]['series']) {
        $player = '<img src="/images/serial.jpg" width="100%" style="border: none !important" />';
    }
    create_cache('news', $player, $newsid, false);
    if (! $en) {
        echo $newms_en;
    } else {
        echo $player;
    }

    $season = $tabsms[0]['season'];
    $series = $tabsms[0]['series'];
    $studios = $tabsms[0]['translator'];

    // если конкретно указано сезон и серию
    if ($xfieldsdata[$moonserials_options['field_season_iframe']]) {
        $season = $xfieldsdata[$moonserials_options['field_season_iframe']];
    }

    if ($xfieldsdata[$moonserials_options['field_series_iframe']]) {
        $series = $xfieldsdata[$moonserials_options['field_series_iframe']];
    }

    // вышел, поля пустые
    if ($season and ! $seasonTemp and $series and ! $seriesTemp and $xfieldsdata[$moonserials_options['field_status_name']] !== $moonserials_options['field_status']) {
        $notdateup = true;
    } else {
        $notdateup = false;
    }

    // есть новая серия
    if ((($seasonTemp and $seriesTemp) and ((int) $season > (int) $seasonTemp or (int) $series > (int) $seriesTemp) and $xfieldsdata[$moonserials_options['field_status_name']] !== $moonserials_options['field_status']) or ($season and ! $seasonTemp and $series and ! $seriesTemp and $xfieldsdata[$moonserials_options['field_status_name']] !== $moonserials_options['field_status'])) {
        $season = $season.' сезон';
        $series = $series.' серия';

        // сезон
        $xfields_n[$moonserials_options['field_season']] = $season;
        // серия. обычная и +1
        if ($moonserials_options['add_series_one'] > 0) {
            $xfields_n[$moonserials_options['field_series']] = ($tabsms[0]['series'] + 1).' серия';
        } else {
            $xfields_n[$moonserials_options['field_series']] = $series;
        }
        // перевод
        if ($studios and $studios !== 'Неизвестно' and $moonserials_options['field_studios']) {
            $xfields_n[$moonserials_options['field_studios']] = $studios;
        }
        // русское название
        if (! $xfieldsdata[$moonserials_options['field_title_ru']] and $moonserials_options['field_title_ru'] and $title_ru) {
            $xfields_n[$moonserials_options['field_title_ru']] = $title_ru;
        }
        // все переводы
        if ($studios_sp and $moonserials_options['field_studios_sp']) {
            $xfields_n[$moonserials_options['field_studios_sp']] = $studios_sp;
        }
        // форматированные сезон и серия
        if ($moonserials_options['season_mod']) {
            $xfields_n[$moonserials_options['season_mod']] = seform($moonserials_options['field_season_form'], $season, '0');
        }
        if ($moonserials_options['series_mod']) {
            $xfields_n[$moonserials_options['series_mod']] = seform($moonserials_options['field_series_form'], $series, $moonserials_options['add_series_one']);
        }

        // макс серий
        if (! $xfieldsdata[$moonserials_options['field_series-max']] and $seriesMax and $moonserials_options['allow_module_new_series_max'] > 0) {
            $xfields_n[$moonserials_options['field_series-max']] = $seriesMax;
        }
        // статус
        if ($moonserials_options['allow_module_new_series_max'] > 0 and $seriesMax == $tabsms[0]['series'] and $xfieldsdata[$moonserials_options['field_status_name']] !== $moonserials_options['field_status']) {
            $xfields_n[$moonserials_options['field_status_name']] = $moonserials_options['field_status'];
        }
        // новая дата новости
        if ($moonserials_options['allow_news_update'] and ! $notdateup) {
            $myNewDate = ($moonserials_options['allow_news_update'] != 0) ? ", `date` = '".date('Y-m-d H:i:s')."'" : false;
        }
        // если включено обновлять тайтл
        if ($moonserials_options['allow_news_title_update']) {
            $ms_title_date = ! empty($moonserials_options['ms_title_date']) ? langdate($moonserials_options['ms_title_date']) : false;
            $ms_title_preffix = ! empty($moonserials_options['ms_title_preffix']) ? $db->safesql(trim(strip_tags(stripslashes($moonserials_options['ms_title_preffix'])))).' ' : false;
            $ms_title_year = ! empty($moonserials_options['ms_title_year']) ? trim(strip_tags(stripslashes($xfieldsdata[$moonserials_options['ms_title_year']]))).' ' : false;
            $ms_title_t1 = ! empty($moonserials_options['ms_title_t1']) ? $db->safesql(trim(strip_tags(stripslashes($moonserials_options['ms_title_t1'])))).' ' : false;
            $ms_title_t2 = ! empty($moonserials_options['ms_title_t2']) ? $db->safesql(trim(strip_tags(stripslashes($moonserials_options['ms_title_t2'])))).' ' : false;
            $ms_title_field = ! empty($moonserials_options['ms_title_field']) ? $db->safesql(trim(strip_tags(stripslashes($xfieldsdata[$moonserials_options['ms_title_field']])))).' ' : false;

            if ($moonserials_options['ms_title_season']) {
                if ($moonserials_options['ms_title_season_one'] and $season == '1 сезон') {
                    $ms_title_season = false;
                } else {
                    $ms_title_season = seform($moonserials_options['title_season_form'], $season, '0');
                }
            }

            if ($moonserials_options['ms_title_series']) {
                $ms_title_series = seform($moonserials_options['title_series_form'], $series, $moonserials_options['ms_title_series_add']);
            }

            if ($ms_title_series) {
                $ms_title_season = $ms_title_season.' ';
            }
            $myModule_title = $ms_title_season.$ms_title_series.' ';
            $ms_title_up = $ms_title_preffix.$title_ru.' '.$ms_title_year.$ms_title_t1.$myModule_title.$ms_title_t2.$ms_title_field.$ms_title_date;
            if (substr($ms_title_up, -1) == ' ') {
                $ms_title_up = substr($ms_title_up, 0, -1);
            }
            $ms_title_up = ", `metatitle`='".$ms_title_up."'";

        }
        // если включено обновлять ЧПУ
        if ($moonserials_options['allow_news_cpu_update']) {
            $ms_cpu_date = ! empty($moonserials_options['ms_cpu_date']) ? totranslit(langdate($moonserials_options['ms_cpu_date'])) : false;
            $ms_cpu_preffix = ! empty($moonserials_options['ms_cpu_preffix']) ? totranslit($moonserials_options['ms_cpu_preffix']).'-' : false;
            $ms_cpu_year = ! empty($moonserials_options['ms_cpu_year']) ? totranslit($xfieldsdata[$moonserials_options['ms_cpu_year']]).'-' : false;
            $ms_cpu_t1 = ! empty($moonserials_options['ms_cpu_t1']) ? totranslit($moonserials_options['ms_cpu_t1']).'-' : false;
            $ms_cpu_t2 = ! empty($moonserials_options['ms_cpu_t2']) ? totranslit($moonserials_options['ms_cpu_t2']).'-' : false;
            $ms_cpu_field = ! empty($moonserials_options['ms_cpu_field']) ? totranslit($xfieldsdata[$moonserials_options['ms_cpu_field']]).'_' : false;
            $title_ru_cpu = totranslit($title_ru).'-';
            if ($moonserials_options['ms_cpu_season']) {
                if ($moonserials_options['ms_cpu_season_one'] and $season == '1 сезон') {
                    $ms_cpu_season = false;
                } else {
                    $ms_cpu_season = seform($moonserials_options['cpu_season_form'], $season, '0');
                }
            }

            if ($moonserials_options['ms_cpu_series']) {
                $ms_cpu_series = seform($moonserials_options['cpu_series_form'], $series, $moonserials_options['ms_cpu_series_add']);
            }

            if ($ms_cpu_series) {
                $ms_cpu_season = $ms_cpu_season.' ';
            }
            $myModule_cpu = $ms_cpu_season.$ms_cpu_series;
            $myModule_cpu = str_replace(',', '-', $myModule_cpu);
            $myModule_cpu = totranslit($myModule_cpu);
            $myModule_cpu = $myModule_cpu.'-';
            $ms_cpu_up = $ms_cpu_preffix.$title_ru_cpu.$ms_cpu_year.$ms_cpu_t1.$myModule_cpu.$ms_cpu_t2.$ms_cpu_field.$ms_cpu_date;
            if (substr($ms_cpu_up, -1) == '-') {
                $ms_cpu_up = substr($ms_cpu_up, 0, -1);
            }

            $ms_cpu_up = ", `alt_name`='".$ms_cpu_up."'";
        }
        // если включено обновлять заголовок
        if ($moonserials_options['allow_news_title2_update']) {
            $ms_title2_date = ! empty($moonserials_options['ms_title2_date']) ? langdate($moonserials_options['ms_title2_date']).' ' : false;
            $ms_title2_preffix = ! empty($moonserials_options['ms_title2_preffix']) ? $db->safesql(trim(strip_tags(stripslashes($moonserials_options['ms_title2_preffix'])))).' ' : false;
            $ms_title2_year = ! empty($moonserials_options['ms_title2_year']) ? trim(strip_tags(stripslashes($xfieldsdata[$moonserials_options['ms_title2_year']]))).' ' : false;
            $ms_title2_t1 = ! empty($moonserials_options['ms_title2_t1']) ? $db->safesql(trim(strip_tags(stripslashes($moonserials_options['ms_title2_t1'])))).' ' : false;
            $ms_title2_t2 = ! empty($moonserials_options['ms_title2_t2']) ? $db->safesql(trim(strip_tags(stripslashes($moonserials_options['ms_title2_t2'])))).' ' : false;
            $ms_title2_field = ! empty($moonserials_options['ms_title2_field']) ? $db->safesql(trim(strip_tags(stripslashes($xfieldsdata[$moonserials_options['ms_title2_field']])))).' ' : false;

            if ($moonserials_options['ms_title2_season']) {
                if ($moonserials_options['ms_title2_season_one'] and $season == '1 сезон') {
                    $ms_title2_season = false;
                } else {
                    $ms_title2_season = seform($moonserials_options['title2_season_form'], $season, '0');
                }
            }

            if ($moonserials_options['ms_title2_series']) {
                $ms_cpu_series = seform($moonserials_options['title2_series_form'], $series, $moonserials_options['ms_title2_series_add']);
            }

            if ($ms_title2_series) {
                $ms_title2_season = $ms_title2_season.' ';
            }
            $myModule_title2 = $ms_title2_season.$ms_title2_series.' ';
            $ms_title2_up = $ms_title2_preffix.$title_ru.' '.$ms_title2_year.$ms_title2_t1.$myModule_title2.$ms_title2_t2.$ms_title2_field.$ms_title2_date;
            if (substr($ms_title2_up, -1) == ' ') {
                $ms_title2_up = substr($ms_title2_up, 0, -1);
            }
            $ms_title2_up = ", `title`='".$ms_title2_up."'";
        }

        // обрабатываем доп поля
        foreach ($xfields_n as $key => &$value) {
            $arr_field[] = $key.'|'.str_replace('|', '&#124;', $value);
        }
        $xfields_n = implode('||', $arr_field);
        unset($arr_field);
        $xfields_n = $db->safesql($xfields_n);

        // вносим все это счастье в базу
        $db->query('UPDATE '.PREFIX."_post SET `xfields` = '$xfields_n' {$myNewDate} {$ms_title_up} {$ms_title2_up} {$ms_cpu_up} WHERE `id` = '{$newsid}'");
        // если включено отправлять ЛС
        if ($moonserials_options['sendpm'] and ! $xfieldsdata[$moonserials_options['field_season_iframe']]) {
            $user_id = '1';
            $user_id = (int) $user_id;
            $now = time();
            $subject = 'Вышла '.$series.' сериала '.$title_ru.'';
            $subject = $db->safesql($subject);
            $from = 'MoonSerials';
            $from = $db->safesql($from);
            $text = '<h3>Вышла '.$series.' сериала '.$title_ru.'</h3>';
            $text .= '<p><b>Теперь можно:</b></p>';
            $text .= '<ul><li><a href="'.$config['http_home_url'].'index.php?newsid='.$post_id.'" target="_blank">Открыть новость на сайте</a></li>';
            $text .= '<li><a href="'.$config['admin_path'].'?mod=editnews&action=editnews&id='.$post_id.'" target="_blank">Редактировать в админпанели</a></li>';
            $text .= '<li><a href="'.$config['admin_path'].'?mod=addnews&action=addnews" target="_blank">Добавить новую новость в админпанели</a></li></ul>';
            $text = $db->safesql($text);
            $db->query('INSERT into '.PREFIX."_pm (subj, text, user, user_from, date, pm_read, folder) VALUES ('$subject', '$text', '$user_id', '$from', '$now', '0', 'inbox')");
            $db->query('UPDATE '.USERPREFIX."_users set `pm_unread` = pm_unread + 1, `pm_all` = pm_all+1  where `user_id` = '{$user_id}'");
        }
    }
}
if ($kpid && $action == 'translates') {
    if ($curl = curl_init()) {
        curl_setopt($curl, CURLOPT_URL, 'http://moonwalk.cc/api/serial_episodes.json?kinopoisk_id='.$kpid.'&api_token='.$moonserials_options['api_token'].'&translator_id='.$trid.'');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        $out = curl_exec($curl);
        $output_sr = json_decode($out, true);
        curl_close($curl);
    }
    // ищем самую самую последнюю серию
    foreach ($output_sr['season_episodes_count'] as $kkk => $vvv) {
        $seasons[] = $vvv['season_number'];
        if ($season_in > 0) {
            if ($vvv['season_number'] == $season_in) {
                $season = $vvv['season_number'];
                $seriesA = $vvv['episodes'];
                $seriesC = $vvv['episodes_count'];
                $episodes = $vvv['episodes'];
            }
        } else {
            if ($vvv['season_number'] > $season) {
                $season = $vvv['season_number'];
                $seriesA = $vvv['episodes'];
                $seriesC = $vvv['episodes_count'];
                $episodes = $vvv['episodes'];
            }
        }
        foreach ($seriesA as $vvvv) {
            if ($vvvv == 0) {
                $seriesT = -1;
                break;
            }
        }
        if ($seriesT == -1 and $seriesC == 1) {
            $series = $moonserials_options['if_pilot_series'];
        } else {
            $series = max($seriesA);
        }
    }
    $tabs[] = ['iframe_url' => $output_sr['serial']['iframe_url'], 'season' => $season, 'series' => $series, 'episodes' => $episodes, 'seasons' => $seasons];
    foreach ($tabs as $k => $v) {
        if ($v['series'] == $moonserials_options['if_pilot_series']) {
            $v['series'] = 0;
        }
        if ($v['translator'] == 'Субтитры') {
            $v['series'] = $v['series'] - 0.5;
        }
        $se[$k] = $v['season'];
        $ep[$k] = $v['series'];
    }
    array_multisort($se, SORT_NUMERIC, SORT_DESC, $ep, SORT_NUMERIC, SORT_DESC, $tabs);
    $seasons = '<ul id="simple-seasons-tabs" class="b-simple_seasons__list clearfix">';
    asort($tabs[0]['seasons']);
    foreach ($tabs[0]['seasons'] as $se_num) {
        $active = $se_num == $tabs[0]['season'] ? ' active' : false;
        $seasons .= '<li onclick="seasons();" class="b-simple_season__item'.$active.'" data-seasid="'.$se_num.'" data-trid="'.$trid.'">Сезон '.$se_num.'</li>';
        $episodes = '<div id="simple-episodes-tabs">';
        $episodes .= '<ul id="simple-episodes-list" class="b-simple_episodes__list clearfix">';
        asort($tabs[0]['episodes']);
        foreach ($tabs[0]['episodes'] as $ep_num) {
            $active = $ep_num == $tabs[0]['series'] ? ' active' : false;
            $episodes .= '<li onclick="episodes();" class="b-simple_episode__item'.$active.'" data-season_id="'.$se_num.'" data-episode_id="'.$ep_num.'" data-trid="'.$trid.'">Серия '.$ep_num.'</li>';
        }
        $episodes .= '</ul>';
        $episodes .= '</div></div>';
    }
    $seasons .= '</ul>';
    $ssn = '&season='.$tabs[0]['season'];
    $srs = '&episode='.$tabs[0]['series'];
    $iframe .= '<div id="ibox"><div id="player-loader-overlay"></div><div id="moon" style="height: 100%; margin: 0 auto; width: 100%;"><iframe src="'.$tabs[0]['iframe_url'].'?nocontrols=1'.$ssn.$srs.'" width="724" height="460" frameborder="0" allowfullscreen=""></iframe></div>';

    if ($showseason > 0) {
        $player = $seasons.$iframe.$episodes;
    } else {
        $player = $iframe.$episodes;
    }

    if (! $en) {
        echo $newms_en;
    } else {
        echo $player;
    }

    /*    echo "<pre>";
        print_r($tabs[0]['seasons']);
        echo "</pre>";*/
}
if ($kpid && $action == 'seasons') {
    if ($trid > 0) {
        $tran = '&translator_id='.$trid;
    }
    if ($curl = curl_init()) {
        curl_setopt($curl, CURLOPT_URL, 'http://moonwalk.cc/api/serial_episodes.json?kinopoisk_id='.$kpid.'&api_token='.$moonserials_options['api_token'].$tran.'');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        $out = curl_exec($curl);
        $output_sr = json_decode($out, true);
        curl_close($curl);
    }
    // ищем самую самую последнюю серию
    foreach ($output_sr['season_episodes_count'] as $kkk => $vvv) {
        if ($vvv['season_number'] == $seasid) {
            $seriesA = $vvv['episodes'];
            break;
        }
    }
    $episodes = '<div id="simple-episodes-tabs">';
    $episodes .= '<ul id="simple-episodes-list" class="b-simple_episodes__list clearfix">';
    asort($seriesA);
    $series = max($seriesA);
    foreach ($seriesA as $ep_num) {
        $active = $ep_num == $series ? ' active' : false;
        $episodes .= '<li onclick="episodes();" class="b-simple_episode__item'.$active.'" data-season_id="'.$seasid.'" data-episode_id="'.$ep_num.'" data-trid="'.$trid.'">Серия '.$ep_num.'</li>';
    }
    $episodes .= '</ul>';
    $episodes .= '</div></div>';
    $ssn = '&season='.$seasid;
    $srs = '&episode='.$series;
    $iframe .= '<div id="ibox"><div id="player-loader-overlay"></div><div id="moon" style="height: 100%; margin: 0 auto; width: 100%;"><iframe src="'.$output_sr['serial']['iframe_url'].'?nocontrols=1'.$ssn.$srs.'" width="724" height="460" frameborder="0" allowfullscreen=""></iframe></div>';
    $player = $iframe.$episodes;
    if (! $en) {
        echo $newms_en;
    } else {
        echo $player;
    }
}
if ($action == 'cache') {
    $cache_n = 'news_'.md5($newsid).'.tmp';
    clear_cache($cache_n);
}
