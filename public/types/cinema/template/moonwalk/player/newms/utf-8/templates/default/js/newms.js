$(document).ready(function(){

    var cashe = $("#videoplayer719").attr("data-cashe");
    if ( cashe < 1 )
    {
        var kpid = $("#videoplayer719").attr("data-kpid");
        var season = $("#videoplayer719").attr("data-season");
        var series = $("#videoplayer719").attr("data-series");
        var newsid = $("#videoplayer719").attr("data-newsid");
        var showseason = $("#videoplayer719").attr("data-showseason");
        var action = 'pageload';
        var trid = '0';
        var cook = '0';
        var wlp = newsid;
        //console.log(wlp);
            if ( $.cookie('les_' + wlp) != null && $.cookie('les_' + wlp) != undefined && $.cookie('les_' + wlp) != 'null' )
            {
                arr = $.cookie('les_' + wlp).split(',');
                trid = arr[0];
                season = arr[1];
                series = arr[2];
                cook = 1;
            }
        $.ajax({
            url: "/engine/ajax/newms.php",
            type: "POST",
            dataType: "html",
            data: {kpid:kpid,trid:trid,season:season,series:series,action:action,cook:cook,newsid:newsid,showseason:showseason},
            success: function(data) {
                $("#videoplayer719").html(data);
                            if (typeof moonplayerReplace !== 'undefined') {
                                moonplayerReplace(
                                    document.getElementsByTagName('iframe'),
                                    'src'
                                );
                            }
            }
        });
    }
});

function translates(){
    $('#translators-list').on('click','.b-translator__item',function(){
        var _self = $(this);
        if(!_self.hasClass('active'))
        {
                $('.b-translator__item').removeClass('active');
                _self.addClass('active');
                    $('#simple-seasons-tabs').animate({opacity: "hide"}, 450);
                    $('#simple-episodes-tabs').animate({height: "hide"}, 450);
                    $('#player-loader-overlay').animate({opacity: "show"}, 450);
                $('#player').find('iframe').remove();
                /*$('#player-loader-overlay').css("display","block");*/
                var kpid = $("#videoplayer719").attr("data-kpid");
                var season = $("#videoplayer719").attr("data-season");
                var series = $("#videoplayer719").attr("data-series");
                var trid = _self.attr("data-trid");
                var showseason = $("#videoplayer719").attr("data-showseason");
                var action = 'translates';
                $.ajax({
                    url: "/engine/ajax/newms.php",
                    type: "POST",
                    dataType: "html",
                    data: {kpid:kpid,season:season,series:series,trid:trid,action:action,showseason:showseason},
                    success: function(data) {
                        $('#player').html(data);
                        $('#simple-seasons-tabs').animate({opacity: "show"}, 600);
                        $('#simple-episodes-tabs').animate({height: "show"}, 600);
                        $('#player-loader-overlay').animate({opacity: "hide"}, 600);
                            if (typeof moonplayerReplace !== 'undefined') {
                                moonplayerReplace(
                                    document.getElementsByTagName('iframe'),
                                    'src'
                                );
                            }
                    }
                });
        }
    });
}

function seasons(){
    $('#simple-seasons-tabs').on('click','.b-simple_season__item',function(){
        var _self = $(this);
        if(!_self.hasClass('active'))
        {
                $('.b-simple_season__item').removeClass('active');
                _self.addClass('active');
               // $('#simple-episodes-tabs').remove();
                //$('#player').find('iframe').remove();

                var kpid = $("#videoplayer719").attr("data-kpid");
                var seasid = _self.attr("data-seasid");
                var trid = _self.attr("data-trid");
                var action = 'seasons';
                $.ajax({
                    url: "/engine/ajax/newms.php",
                    type: "POST",
                    dataType: "html",
                    data: {kpid:kpid,seasid:seasid,action:action,trid:trid},
                    success: function(data) {

                        $('#player-loader-overlay').animate({opacity: "show"}, 50);
                        $('#simple-episodes-tabs').animate({height: "hide"}, 250);

                          $('#simple-episodes-tabs').animate({height: "show"}, 400, function() {
                            $('#ibox').html(data);
                                if (typeof moonplayerReplace !== 'undefined') {
                                    moonplayerReplace(
                                        document.getElementsByTagName('iframe'),
                                        'src'
                                    );
                                }
                            $('#player-loader-overlay').animate({opacity: "hide"}, 600);
                          });

                    }
                });
        }
    });
}

function episodes(){
    $('#simple-episodes-list').on('click','.b-simple_episode__item',function(){
        var _self = $(this);
        var seasid = _self.attr("data-season_id");
        var trid = '0';
        var trid = _self.attr("data-trid");
        var trna = $('#translators-list li.active').text();
        var episode = _self.attr("data-episode_id");
        var _iframe_block = $('#player').find('iframe').clone();
        var crop = _iframe_block.attr("src").indexOf("episode=");
        var src = _iframe_block.attr("src").slice(0,crop + 8);
        var newsid = $("#videoplayer719").attr("data-newsid");

        if(!_self.hasClass('active'))
        {
            var cookie_value = trid + ',' + seasid + ',' + episode;
            var wlp = newsid;
            $.cookie('les_' + wlp, cookie_value, {
                expires: 14
            });

            if ( trna != '' )
            {
                $('#les').html('. Вы остановились на ' + seasid + ' сезоне ' + episode + ' серии' + ' в озвучке &laquo;' + trna + '&raquo;');
                if ( $('#lesc').html() != '' ) $('#les').append('<img src="/images/process-stop.png" onclick="del();" id="lesc" title="Удалить отметку" />');
            }
            else
            {
                $('#les').html('. Вы остановились на ' + seasid + ' сезоне ' + episode + ' серии');
                if ( $('#lesc').html() != '' ) $('#les').append('<img src="/images/process-stop.png" onclick="del();" id="lesc" title="Удалить отметку" />');
            }

                var action = 'cache';
                var newsid = $("#videoplayer719").attr("data-newsid");
                $.ajax({
                    url: "/engine/ajax/newms.php",
                    type: "POST",
                    dataType: "html",
                    data: {action:action,newsid:newsid},
                });

            $('#ln').html('');
            $('.b-simple_episode__item').removeClass('active');
            _self.addClass('active');
            $('#player-loader-overlay').animate({opacity: "show"}, 50);
            $('#player').find('iframe').attr('src', src + episode);
            $('#player-loader-overlay').animate({opacity: "hide"}, 600);
                            if (typeof moonplayerReplace !== 'undefined') {
                                moonplayerReplace(
                                    document.getElementsByTagName('iframe'),
                                    'src'
                                );
                            }
        }
    });
}
function del(){
        var newsid = $("#videoplayer719").attr("data-newsid");
             $('#les').html('');
             $('#ln').html('');
             $('#lesc').remove();
             var wlp = newsid;
             $.cookie('les_' + wlp, null, {
                expires: 14
            });
                var action = 'cache';
                var newsid = $("#videoplayer719").attr("data-newsid");
                $.ajax({
                    url: "/engine/ajax/newms.php",
                    type: "POST",
                    dataType: "html",
                    data: {action:action,newsid:newsid},
                });
}