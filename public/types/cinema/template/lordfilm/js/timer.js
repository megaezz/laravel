function timer(timestamp){
    if (timestamp < 0) timestamp = 0;
 
    var week = Math.floor( ((timestamp/60/60) / 168));
    var day = Math.floor( ((timestamp/60/60) / 24));
    var lday = Math.floor( ((timestamp/60/60) / 24) - (7*week));
    var hour = Math.floor(timestamp/60/60);
    var mins = Math.floor((timestamp - hour*60*60)/60);
    var secs = Math.floor(timestamp - hour*60*60 - mins*60); 
    var lhour = Math.floor( (timestamp - day*24*60*60) / 60 / 60 );
  
	if(String(week).length == 1){week="0" + week;}
	$('.pretimer .weeks .value').text(week);
  
	if(String(day).length == 1){day="0" + day;}
	$('.pretimer .days .value').text(day);
  
	if(String(lhour).length == 1){lhour="0" + lhour;}
	$('.pretimer .hours .value').text(lhour);
 
	if(String(mins).length == 1){mins="0" + mins;}
	$('.pretimer .minutes .value').text(mins);

	if(String(secs).length == 1){secs="0" + secs;}
	$('.pretimer .seconds .value').text(secs);
  
	$('.pretimer .weeks .unit').text(numpf(week, "неделя", "недели", "недель"));
	$('.pretimer .days .unit').text(numpf(day, "день", "дня", "дней"));
	$('.pretimer .hours .unit').text(numpf(lhour, "час", "часа", "часов"));
	$('.pretimer .minutes .unit').text(numpf(mins, "минута", "минуты", "минут"));
	$('.pretimer .seconds .unit').text(numpf(secs, "секунда", "секунды", "секунд")); 
}
 
$(document).ready(function(){
	if ( typeof timer_timestamp !=="undefined" && timer_timestamp > 0 ){
		setInterval(function(){
			timer_timestamp = timer_timestamp - 1;
			timer(timer_timestamp);
		}, 1000);
	}
});

function numpf(t, e, i, n) {
    var o = t % 10;
    return 1 == o && (1 == t || t > 20) ? e : o > 1 && 5 > o && (t > 20 || 10 > t) ? i : n;
}

function open_popup(box, width, height, yt) {
  if(!$(box+' iframe').length && yt) {
   $(box).append('<iframe id="yt" width="'+width+'" height="'+height+'" src="//www.youtube.com/embed/'+yt+'?rel=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>');
 }
 $(box+' .popup-close').html('<div class="popup-l1"></div><div class="popup-l2"></div>');
  if(width) $(box).css({'width': width, 'margin-left': -(width/2)});
 if(height) $(box).css({'height': height, 'margin-top': -(height/2)});
  else $(box).css('margin-top', -($(box).height()/2));
  $(box).show();
}
function close_popup(box) {
 $(box).hide();
  $(box+' #yt').remove();
}
// moonserials
(function($){
    jQuery.fn.lightTabs = function(options){

        var createTabs = function(){
            tabsms = this;
            i = 0;

            showPage = function(i){
                $(tabsms).children("div").children("div").hide().text('');
                $(tabsms).children("div").children("div").eq(i).show();
                $(tabsms).children("ul").children("li").removeClass("active");
                $(tabsms).children("ul").children("li").eq(i).addClass("active");
            }

            showPage(0);

            $(tabsms).children("ul").children("li").each(function(index, element){
                $(element).attr("data-page", i);
                i++;
            });

            $(tabsms).children("ul").children("li").click(function(){
                showPage(parseInt($(this).attr("data-page")));
                var urlll0 = $(".ms-content").eq(parseInt($(this).attr("data-page"))).attr("data-show_player");
                var ifrm0 = '<iframe src=\"' + urlll0 + '\" frameborder=\"0\" scrolling=\"no\" width=\"607\" height=\"360\" allowfullscreen=\"\"></iframe';
                var studios = $(".ms-title").eq(parseInt($(this).attr("data-page"))).html();
                $(".ms-content").eq(parseInt($(this).attr("data-page"))).html(ifrm0);
                $("#studios").html('');
                $("#studios").html(studios);
            });
        };
        return this.each(createTabs);
    };
})(jQuery);

$(document).ready(function(){
    $(".tabsms").lightTabs();
    var urlll = $(".ms-content").first().attr("data-show_player");
    var ifrm = '<iframe src=\"' + urlll + '\" frameborder=\"0\" scrolling=\"no\" width=\"607\" height=\"360\" allowfullscreen=\"\"></iframe>';
    $(".ms-content").first().html(ifrm);
});
// moonserials