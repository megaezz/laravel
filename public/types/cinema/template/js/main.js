// краткая всплывающая инфа по фильму (в т.ч. для новых элементов (ajax))
$(document).bind('DOMNodeInserted', function(e) {
	fastInfoOpened = false;
	$('.showFastInfo').on('mouseleave',function(event){
		fastInfoOpened = false;
		$('.fastInfo').hide();
	});
	$('.showFastInfo').on('mouseenter',function(event){
		if (!fastInfoOpened) {
			if (!$('.mainContainer').has('.fastInfo').length) {
				$('.mainContainer').append('<div class="fastInfo"></div>');
			}
			fastInfoOpened = true;
			$.ajax({
				method: "GET",
				url: "/api/showMovieShortInfo",
				data: { id: $(this).data('id') }
			})
			.done(function( msg ) {
				// если ко времени загрузки уже убрали мышку с кнопки информации, то не открываем окно
				infoWidth = 430;
				if (event.clientX+infoWidth < $(window).width()) {
					left = event.clientX + 10;
				} else {
					left = event.clientX + 10 - infoWidth;
				}
				// left = event.clientX+
				if (fastInfoOpened) {
					// alert('Ширина экрана: '+$(window).width()+' Клик: '+event.clientX);
					$('.fastInfo').css({
						top: (event.clientY)+'px',
						left: left+'px'
					});
					$('.fastInfo').show();
					$('.fastInfo').html(msg);
				}
			})
			.fail(function( jqXHR, textStatus ) {
				$('.fastInfo').show();
				$('.fastInfo').html(textStatus);
			})
		}
	});
});
// продолжить просмотр
//нужна подключенная библиотека jquery.cookie
function saveMovie(movieId = null) {
	// 5 лет =)
	var expDays = 365;
	$.cookie.json = true;
	moviesObj = $.cookie('savedMovies');
	if (moviesObj) {
		// удаляем такой ID, если есть, чтобы он был только в конце
		moviesObj.movies.forEach(function(arrMovieId,index) {
			if (arrMovieId == movieId) {
				// alert(arrMovieId+' удаляем');
                // удаляем этот элемент массива
                moviesObj.movies.splice(index,1);
            } else {
            	// alert(arrMovieId+' не удаляем');
            }
        });
		moviesObj.movies.push(movieId);
	} else {
		moviesObj = {movies: [movieId]};
	}
	$.cookie('savedMovies',moviesObj,{expires: 365, path: '/'});
	// alert('Сохранили');
}

function showSavedMovies(blockId = null) {
	$.cookie.json = true;
	// чистим блок
	$('#'+blockId).html('');
	if ($.cookie('savedMovies')) {
		moviesObj = $.cookie('savedMovies');
		reversed = moviesObj.movies.reverse();
		reversed.forEach(function(movieId) {
			$.ajax({
				method: "GET",
				url: "/api/savedMovieInfo?movieId="+movieId
			})
			.done(function( msg ) {
				$('#'+blockId).append(msg);
			})
			.fail(function( jqXHR, textStatus ) {
				alert(textStatus);
			})
		});
	} else {
		$('#'+blockId).html('<h5>Вы пока что ничего не смотрели</h5>');
	}
}

function isMovieSaved(movieId = null) {
	$.cookie.json = true;
	if (!$.cookie('savedMovies')) {
		return false;
	}
	moviesObj = $.cookie('savedMovies');
	// alert(moviesObj.movies);
	isSaved = false;
	moviesObj.movies.forEach(function(arrMovieId) {
		if (arrMovieId == movieId) {
			// alert(arrMovieId+' == '+movieId);
			isSaved = true;
		} else {
			// alert(arrMovieId+' != '+movieId);
		}
	});
	return isSaved;
}

// подгружаем контент по ссылке в указанный элемент
function loadContent(element = null, url = null) {
	$.ajax({
		method: "GET",
		url: url
	})
	.done(function( msg ) {
		element.append(msg);
	})
	.fail(function( jqXHR, textStatus ) {
		element.append(textStatus);
	})
}

// ленивая загрузка изображений (в т.ч. для новых элементов (ajax))
// $(document).bind('DOMNodeInserted', function(e) {
// 	[].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
// 		img.setAttribute('src', img.getAttribute('data-src'));
// 		img.onload = function() {
// 			img.removeAttribute('data-src');
// 		};
// 	});
// });