document.addEventListener("DOMContentLoaded", function() {
	// если фокусировка на фрейме то сохраняем фильм
	isMovieSaved = false;
	$(window).blur(function () {
		if (!isMovieSaved) {
			// check focus
			// alert('focused');
			if ($('#player iframe').is(':focus')) {
				isMovieSaved = true;
				saveMovie(domainMovieId);
			}             
		}
	});

	// действия с плеером
	sorryText = 'Привет! На данный момент это лучший вариант фильма, который есть в интернете. Как только появится что-то лучше, мы моментально обновим! Добавляйте нас в закладки или сохраняйте в соц. сети через кнопки под плеером и будем вместе ждать выхода полной версии.';
	if (blockMessage) {
		$('.player-message').html(blockMessage);
	}
	// отображаем блок с главным плеером
	$('#player-'+mainPlayer).show();
	$('.show-player-btn[data-player='+mainPlayer+']').addClass('active');
	// при нажатии на кнопку выбора плеера скрываем все блоки плеера, 
	// убираем активный класс с кнопок, добавляем активный класс на нажатую кнопку, 
	// активируем блок к которой относится нажатая кнопка, убираем текст сообщения, 
	// проверяем если нет iframe, то выводим "плеер отсутствует"
	$('.show-player-btn').on('click',function(){
		$('.player-block').hide();
		$('.show-player-btn').removeClass('active');
		$(this).addClass('active');
		// alert('#player-'+$(this).data('player'));
		$('#player-'+$(this).data('player')).show();
		$('.player-message').html('');
		if (!$('#player-'+$(this).data('player')+' iframe').length) {
			$('.player-message').html('Плеер отсутствует');
		}
	});
	// ресайз hdgo плеера
	$('#hdgoplayer').on('DOMNodeInserted', 'iframe', function () {
		$('#hdgoplayer iframe').width('');
		$('#hdgoplayer iframe').height('');
	});
	// когда жмем на кнопку videocdn - удаляем src в айфрейме и применяем обратно, иначе плеер глючно отображается
	$('.show-player-btn[data-player=videocdn]').on('click',function(){
		// обновляем путь в айфрейме чтобы плеер корректно отобразился
		videocdnUrl = $('#videocdn_iframe').attr('src');
		$('#videocdn_iframe').attr('src','');
		$('#videocdn_iframe').attr('src',videocdnUrl);
		// alert($('#player-hdg iframe').length);
	});
	// если легальный плеер не пустой то скрываем все блоки с плеером и все кнопки и показываем его
	/*
	if ($.trim($('#player-legal').html()).length) {
		$('.player-block').hide();
		$('#player-legal').show();
		$('.show-player-btn').hide();
	}
	*/
	// если плеер с трейлером не пустой то скрываем все блоки с плеером и все кнопки и показываем его
	/*
	if ($.trim($('#player-trailer').html()).length) {
		$('.player-block').hide();
		$('#player-trailer').show();
		$('.show-player-btn').hide();
		if (!blockMessage) {
			$('.player-message').html(sorryText);
		}
	}
	*/
	// если плеер с прямой ссылкой не пустой то скрываем все блоки с плеером и все кнопки и показываем его
	/*
	if ($.trim($('#player-direct').html()).length) {
		$('.player-block').hide();
		$('.show-player-btn').hide();
		$('#player-direct').show();
	}
	*/
	// если тип фильма - трейлер, то извиняемся
	/*
	if (movieType == 'trailer') {
		if (!blockMessage) {
			$('.player-message').html(sorryText);
		}
	}
	*/
	// если фильм удален, то скрываем все кнопки и плееры
	if (movieDeleted) {
		$('.show-player-btn').hide();
		$('.player-block').hide();
	}
});