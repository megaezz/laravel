var theme = (typeof localStorage.theme !== 'undefined')?localStorage.theme:'light';
var lightBackgroundImage = document.body.style.backgroundImage;

function changeTheme(theme) {
	if (theme == 'dark') {
		document.querySelectorAll('nav.navbar').forEach(function(item){
			item.classList.remove('bg-dark');
			item.classList.add('bg-light');
		});
		link = document.createElement('link');
		link.setAttribute('rel','stylesheet');
		link.setAttribute('href','/types/cinema/template/admin/customer/css/darkly/bootstrap.min.css');
		document.head.appendChild(link);
		document.body.style.backgroundImage = 'none';
		document.querySelector('.themeToggle > i').className = 'fas fa-sun';
	}
	if (theme == 'light') {
		document.querySelectorAll('nav.navbar').forEach(function(item){
			item.classList.remove('bg-light');
			item.classList.add('bg-dark');
		});
		if (typeof link !== 'undefined') {
			link.remove();
		}
		document.body.style.backgroundImage = lightBackgroundImage;
		document.querySelector('.themeToggle > i').className = 'fas fa-moon';
	}
	localStorage.theme = theme;
}

changeTheme(theme);

document.querySelector('.themeToggle').onclick = function() {
	theme = (theme == 'light')?'dark':'light';
	changeTheme(theme);
}