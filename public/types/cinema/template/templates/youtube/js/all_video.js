  $(document).ready(function() {

        $('.pm-search-form').focusin(function() {
            $(this).children('.input-group').addClass("focused");
        });
        $('.pm-search-form').focusout(function() {
            $(this).children('.input-group').removeClass("focused");
        });


        $(function() {
            $('.ripple').on('click', function(event) {
                //event.preventDefault();

                var $div = $('<div/>'),
                    btnOffset = $(this).offset(),
                    xPos = event.pageX - btnOffset.left,
                    yPos = event.pageY - btnOffset.top;

                $div.addClass('ripple-effect');
                var $ripple = $(".ripple-effect");

                $ripple.css("height", $(this).height());
                $ripple.css("width", $(this).height());
                $div
                .css({
                    top: yPos - ($ripple.height() / 2),
                    left: xPos - ($ripple.width() / 2),
                    background: $(this).data("ripple-color")
                })
                .appendTo($(this));

                window.setTimeout(function() {
                    $div.remove();
                }, 2000);
            });
        });
    });


    $(document).ready(function() {
        echo.init({
            offset: 600,
            throttle: 200,
            unload: false,
        });


        if ($('ul.pagination').length) {
            // $('ul.pagination.pagination-arrows li a').first().html("<i class='fa fa-arrow-left'></i>");
            // $('ul.pagination.pagination-arrows li a').last().html("<i class='fa fa-arrow-right'></i>");
            $('ul.pagination.pagination-arrows li a').first().html("<i class='fa fa-arrow-left'></i>");
            $('ul.pagination.pagination-arrows li a').last().html("<i class='fa fa-arrow-right'></i>");
        }

    });

    // In-button Feedback (Icon)
    $(document).ready(function() {
        $('.btn-with-loader').on('click', function() {
            var $this = $(this);
            $this.button('Loading').prepend('<i class="btn-loader"></i>');
            setTimeout(function() {
                $this.button('reset');
            }, 2000);
        });
    });
    // Page Loading Feedback (Red Stripe)
    $(document).ready(function() {
        $(".pm-section-highlighted, .pm-ul-browse-videos").animsition({
            linkElement: '.animsition',
            loading: false,
            timeout: true,
            timeoutCountdown: 5000,
            browser: ['animation-duration', '-webkit-animation-duration'],
            overlay: false,
            overlayClass: '',
            overlayParentElement: 'body',
            transition: function(url) {
                $('header').append('<div class="pm-horizontal-loader"></div>');
                window.location.href = url;
            }
        });
    });

    // Global settings for notifications
    $(document).ready(function() {
        $.notifyDefaults({
            // settings
            element: 'body',
            position: null,
            type: "info",
            allow_dismiss: true,
            newest_on_top: true,
            showProgressbar: false,
            placement: {
                from: "top",
                // top, bottom
                align: "right" // left, right, center
            },
            offset: {
                x: 20,
                y: 100
            },
            spacing: 10,
            z_index: 1031,
            delay: 10000,
            timer: 1000,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInDown',
                //'animated fadeIn',
                exit: 'animated fadeOutUpBig',
            },
            //'animated fadeOut'
            onShow: null,
            onShown: null,
            onClose: null,
            onClosed: null,
            template: '<div data-notify="container" class="growl alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">&times;</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>',
            PM_exitAnimationTimeout: 0 // PHP Melody custom settings
        });
    });

$(document).ready(function() {
        $('#nav-link-comments-native').click(function() {
            $.cookie('pm_comment_view', 'native', {
                expires: 180,
                path: '/'
            });
        });
        $('#nav-link-comments-facebook').click(function() {
            $.cookie('pm_comment_view', 'facebook', {
                expires: 180,
                path: '/'
            });
            FB.XFBML.parse(document.getElementById("comments-facebook"));
        });
        $('#nav-link-comments-disqus').click(function() {
            $.cookie('pm_comment_view', 'disqus', {
                expires: 180,
                path: '/'
            });
        });
    });

 $(document).ready(function() {
        $('textarea').autosize();
        //$('.fileinput').fileinput();
        $(".ellipsis").dotdotdot({
            ellipsis: '...',
            wrap: 'word',
            watch: true,
            height: 40
        });
    });

$(document).ready(function() {
        // live search
        $('#pm-search').typeWatch({
            callback: function() {
                $.ajax({
                    type: "POST",
                    url: MELODYURL2 + "/ajax-search.php",
                    data: {
                        "queryString": $('#pm-search').val()
                    },
                    dataType: "html",
                    success: function(b) {
                        if (b.length > 0) {
                            $(".pm-search-suggestions-list").html(b);

                            $(".pm-search-suggestions").show(function() {

                                var $listItems = $('.pm-search-suggestions-list').find('li');

                                $('#pm-search').keydown(function(e)
                                {

                                    var key = e.keyCode,
                                        $selected = $listItems.filter('.selected'),
                                        $current;

                                    if (key === 27) {
                                        // ESC
                                        $(".pm-search-suggestions").hide();
                                    }

                                    if (key == 38 || key == 40) {
                                        // UP/DOWN
                                        e.preventDefault();
                                    }

                                    if (key != 38 && key != 40)
                                        return;

                                    $listItems.removeClass('selected');

                                    if (key == 40)// Down key
                                    {
                                        if (!$selected.length || $selected.is(':last-child')) {
                                            $current = $listItems.eq(0);
                                        }
                                        else {
                                            $current = $selected.next();
                                        }
                                    }
                                    else if (key == 38)// Up key
                                    {
                                        if (!$selected.length || $selected.is(':first-child')) {
                                            $current = $listItems.last();
                                        }
                                        else {
                                            $current = $selected.prev();
                                        }
                                    }

                                    $current.addClass('selected');
                                    $selected_url = $current.find('a').attr('href');

                                    $selected_id = $current.attr('data-video-id');

                                    ($('#pm-video-id').val($selected_id));

                                });

                                $(document).on('click', (function(e) {
                                    if (e.target.className !== "pm-search-suggestions" && e.target.id !== "pm-search") {
                                        $(".pm-search-suggestions").hide();
                                    }
                                }));
                            });

                        } else {
                            $(".pm-search-suggestions").hide();
                        }
                    }
                });
            },
            wait: 400,
            highlight: true,
            captureLength: 3
        });
    });

$(window).load(function() {

        var pm_elastic_player = $.cookie('pm_elastic_player');
        if (pm_elastic_player == null) {
            $.cookie('pm_elastic_player', 'normal');
        }
        else if (pm_elastic_player == 'wide') {
            $('.pm-video-watch-sidebar').slideDown('slow');
            $('#player_extend').find('i').removeClass('fa-arrows-h').addClass('fa-compress');
            $('#player').addClass('watch-large');
        } else {
            $('#player').removeClass('watch-large');
        }

        $("#player_extend").click(function() {

            if ($(this).find('i').hasClass("fa-arrows-h")) {
                $(this).find('i').removeClass("fa-arrows-h").addClass("fa-compress");
            } else {
                $(this).find('i').removeClass("fa-compress").addClass("fa-arrows-h");
                $('.pm-video-watch-sidebar').slideDown(300);
            }

            $('#player').animate({
            }, 500, "linear", function() {
                $('#player').toggleClass("watch-large");
            });
            if ($.cookie('pm_elastic_player') == 'normal') {
                $.cookie('pm_elastic_player', 'wide');
                $('#player_extend').find('i').removeClass('fa-arrows-h').addClass('fa-compress');
            } else {
                $.cookie('pm_elastic_player', 'normal');
                $('#player_extend').find('i').removeClass('fa-compress').addClass('fa-arrows-h');
            }
            return false;
        });

        $('.pm-video-description').readmore({
            speed: 50,
            maxHeight: 100,
            moreLink: '<a href="#">' + pm_lang.show_more + '</a>',
            lessLink: '<a href="#">' + pm_lang.show_less + '</a>',
        }).autolink({
            urls: true,
            target: "_blank"
        });

    });

$(function() {
        var cropit_avatar_notify = null;
        var cropit_cover_notify = null;
        var cropit_notify_type = 'info';

        // Avatar
        $('.pm-profile-avatar-pic').cropit({
            smallImage: 'allow',
            // width: 180, 
            // height: 180,
            width: 120,
            height: 120,
            preview: '.pm-profile-avatar-preview',
            onImageLoading: function() {
                cropit_avatar_notify = $.notify({
                    message: pm_lang.please_wait
                }, {
                    type: cropit_notify_type
                });
            },
            onImageLoaded: function() {
                cropit_avatar_notify.close();
            },
            onImageError: function() {
                cropit_avatar_notify.close();
            }
        });

        $('#btn-edit-avatar').click(function() {
            $('#cropit-avatar-input').click();
            $('#cropit-avatar-form').css('visibility', 'visible');
            $('.cropit-image-preview').removeClass('animated fadeIn');
        });

        $('.btn-cancel-avatar').click(function() {
            $('.pm-profile-avatar-pic .cropit-image-preview').removeClass('cropit-image-loaded').addClass('animated fadeIn');
            $('#cropit-avatar-form').css('visibility', 'hidden');
            return false;
        });

        $('form#cropit-avatar-form').submit(function() {

            var image_data = $('.pm-profile-avatar-pic').cropit('export', {
                type: 'image/jpeg',
                quality: .9,
                fillBg: '#333'
            });

            // Move cropped image data to hidden input
            $('.hidden-avatar-data-img').val(image_data);

            $.ajax({
                url: MELODYURL2 + "/ajax.php",
                type: "POST",
                dataType: "json",
                data: $('#cropit-avatar-form').serialize(),
                beforeSend: function(jqXHR, settings) {
                    // clean error message container
                    //cropit_avatar_notify.close();
                    $.notifyClose();
                    cropit_avatar_notify = $.notify({
                        message: pm_lang.swfupload_status_uploading
                    }, {
                        type: cropit_notify_type
                    });
                },
            })
            .done(function(data) {
                cropit_avatar_notify.close();
                if (data.success) {
                    // hide form action buttons 
                    $('#cropit-avatar-form').css('visibility', 'hidden');

                    // reset background with uploaded image 
                    $('.pm-profile-avatar-pic .cropit-image-preview img').attr('src', data.file_url);

                    // stop image movement ability
                    $('.pm-profile-avatar-pic .cropit-image-preview').addClass('animated fadeIn');
                    // timeout required to allow time for the uploaded image to load before removing the current image obj (and avoid a image-swapping 'glitch')
                    setTimeout(function() {
                        $('.pm-profile-avatar-pic .cropit-image-preview').removeClass('cropit-image-loaded')
                    }, 700);

                    // unload selected image to let the user re-select the same image
                    $('.pm-profile-avatar-pic input.cropit-image-input')[0].value = null;
                }
                cropit_avatar_notify = $.notify({
                    message: data.msg
                }, {
                    type: data.alert_type
                });
            });

            return false;
        });

        var cropit_cover_height = parseInt($('.pm-profile-cover-preview').attr('data-cropit-height'));
        if (!cropit_cover_height) {
            cropit_cover_height = 200;
        }

        // Cover
        $('.pm-profile-cover-preview').cropit({
            smallImage: 'allow',
            height: cropit_cover_height,
            onImageLoading: function() {
                cropit_cover_notify = $.notify({
                    message: pm_lang.please_wait
                }, {
                    type: cropit_notify_type
                });
            },
            onImageLoaded: function() {
                cropit_cover_notify.close();
            },
            onImageError: function() {
                cropit_cover_notify.close();
            }
        });

        $('#btn-edit-cover').click(function() {
            $('#cropit-cover-input').click();
            $('#cropit-cover-form').css('visibility', 'visible');
            $('.cropit-image-preview').removeClass('animated fadeIn');
        });

        $('.btn-cancel').click(function() {
            $('.pm-profile-cover-preview .cropit-image-preview').removeClass('cropit-image-loaded').addClass('animated fadeIn');
            $('#cropit-cover-form').css('visibility', 'hidden');
            return false;
        });

        $('form#cropit-cover-form').submit(function() {

            var image_data = $('.pm-profile-cover-preview').cropit('export', {
                type: 'image/jpeg',
                quality: .9,
                fillBg: '#333'
            });

            // Move cropped image data to hidden input
            $('.hidden-cover-data-img').val(image_data);

            $.ajax({
                url: MELODYURL2 + "/ajax.php",
                type: "POST",
                dataType: "json",
                data: $('#cropit-cover-form').serialize(),
                beforeSend: function(jqXHR, settings) {
                    // clean error message container
                    //cropit_cover_notify.close();
                    $.notifyClose();
                    cropit_cover_notify = $.notify({
                        message: pm_lang.swfupload_status_uploading
                    }, {
                        type: cropit_notify_type
                    });
                },
            })
            .done(function(data) {
                cropit_cover_notify.close();
                if (data.success) {
                    // hide form action buttons 
                    $('#cropit-cover-form').css('visibility', 'hidden');

                    // reset background with uploaded image 
                    $('.pm-profile-cover-preview .cropit-image-preview img').attr('src', data.file_url);

                    // stop image movement ability
                    $('.pm-profile-cover-preview .cropit-image-preview').addClass('animated fadeIn');
                    // timeout required to allow time for the uploaded image to load before removing the current image obj (and avoid a image-swapping 'glitch')
                    setTimeout(function() {
                        $('.pm-profile-cover-preview .cropit-image-preview').removeClass('cropit-image-loaded')
                    }, 700);

                    // unload selected image to let the user re-select the same image
                    $('.pm-profile-cover-preview input.cropit-image-input')[0].value = null;
                }
                cropit_cover_notify = $.notify({
                    message: data.msg
                }, {
                    type: data.alert_type
                });
            });

            return false;
        });
    });