class MyLists {

	constructor(mylists, config) {
		var _self = this;

		this.config = JSON.parse(config);

		if ( mylists === false ) {
			var matches = document.cookie.match(new RegExp("(?:^|; )mylists=([^;]*)"));

			mylists = matches ? decodeURIComponent(matches[1]) : '';
		}

		this.mylists = mylists;

		this.controls();

		$('body').on('click', 'ul.mylists-switch[data-id] li[data-id]', function() {
			if (dle_group == 5) {
				notyf.error("Доступно только после регистрации");
				return false;
			}
			var news_id = $(this).closest('ul').data('id'),
				status_id = $(this).data('id'),
				trigger = $(this).attr('class');

			$(this).closest('ul').find('li').removeClass('active');

			if ( trigger == 'active' ) {
				_self.set(news_id, 0);
			} else {
				_self.set(news_id, status_id);
				$(this).addClass('active');
			}
		});
    }

	get( news_id = false ) {
		var list = {};

		$.each(this.mylists.split('|'), function(index, value) {
			value = value.split('/');
			if ( $.isNumeric( value[0] ) && $.isNumeric( value[1] ) ) list[value[0]] = value[1];		
		});

		if ( news_id !== false ) return typeof list[news_id] != 'undefined' ? list[news_id] : "0";
		
		return list;
	}

	set( news_id, status_id = "0" ) {
		if ( dle_group == 5 ) {
			var list = this.get(), lst = [], message = "";

			if ( status_id == "0" ) {
				if ( typeof list[news_id] != 'undefined' ) {
					delete list[news_id];
				}
			} else {
				list[news_id] = status_id;
			}

			$.each(list, function(index, value) {
				lst.push(index + "/" + value);
			});

			lst = lst.join("|");

			this.mylists = lst;

			document.cookie = 'mylists=' + encodeURIComponent(lst) + '; path=/; expires=' + new Date(new Date().getTime() + (3600 * 1000 * 24 * 31 * 6));
		} else {
			$.ajax({
				url: '/mylists/',
				type: 'POST',
				dataType: 'JSON',
				data: {news_id: news_id, status_id: status_id},
				success: function(data) {
					if ( data.success !== false ) {
						notyf.success(data.message);
						return;
					}
				},
				error: function() {
					notyf.error("Произошла неизвестная ошибка: попробуйте обновить страницу");
				}
			});
		}
	}

	controls() {
		var _self = this;

		$('ul.mylists-switch[data-id]').each(function() {
			var news_id = $(this).data('id'),
				status_id = _self.get(news_id),
				buffer = "";

			$.each(_self.config, function(id, name) {
				id = id.replace(/_/g, '');
				
				if ( status_id == id ) {
					buffer += '<li class="active" data-id="' + id + '"><span>' + name[0] + '</span></li>';
				} else {
					buffer += '<li data-id="' + id + '"><span>' + name[0] + '</span></li>';
				}
			});

			$(this).html(buffer);
		});

		$('.mylists-count[data-id]').each(function() {
			var id = $(this).data('id'),
				count = 0;

			$.each(_self.get(), function(news_id, status_id) {
				if ( status_id == id || id == 0 ) count++;
			});

			$(this).html(count);
		});
	}

}