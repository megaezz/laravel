"use strict";
(self.webpackChunk = self.webpackChunk || []).push([[51], {
    86460: function(e, t, n) {
        n.d(t, {
            Z: function() {
                return r
            }
        });
        var i = n(9012),
            l = n(35500),
            o = {
                key: 0,
                class: "success-notify"
            },
            a = (0, i._)("i", {
                class: "icon-check-slim"
            }, null, -1);
        var s = {
            name: "FButton",
            props: ["successAvailable", "successText"]
        };
        var r = (0, n(20311).Z)(s, [["render", function(e, t, n, s, r, u) {
            return (0, i.wg)(), (0, i.iD)("button", {
                class: (0, l.normalizeClass)(["p-button", {
                    success: n.successText,
                    "success-available": n.successAvailable
                }])
            }, [n.successAvailable && n.successText ? ((0, i.wg)(), (0, i.iD)("span", o, [a, (0, i.Uk)((0, l.toDisplayString)(n.successText), 1)])) : (0, i.kq)("v-if", !0), (0, i._)("span", null, [(0, i.WI)(e.$slots, "default")])], 2)
        }]])
    },
    90353: function(e, t, n) {
        n.r(t),
        n.d(t, {
            default: function() {
                return Z
            }
        });
        var i = n(9012),
            l = n(35500),
            o = n(98993),
            a = n.p + "images/comments.png?e783be75af36b95bd2cb",
            s = {
                key: 0,
                class: "page-profile-comments"
            },
            r = {
                key: 0,
                class: "instruction"
            },
            u = [(0, i._)("h4", {
                class: "p-h4"
            }, "Как появляются комментарии?", -1), (0, i._)("p", null, "Под каждой серией есть возможность оставлять свои комментарии, картинка ниже Если ответят на ваш комментарий, вы увидите его тут ;)", -1), (0, i._)("img", {
                src: a
            }, null, -1)],
            c = {
                key: 1
            },
            p = {
                class: "comments-sort"
            },
            d = (0, i._)("span", {
                class: "label"
            }, "сортировка:", -1),
            f = {
                class: "comments-list lk-comments-list"
            },
            m = ["href"],
            v = {
                class: "lk-comment-list__item-serial_info"
            },
            h = ["title", "href"],
            g = ["title", "href"],
            _ = {
                key: 0,
                class: "pagination-wrap"
            };
        var k = n(33938),
            y = n(30222),
            b = n.n(y),
            w = n(37236),
            D = n(66336),
            C = n(85857),
            S = {
                name: "Comments",
                components: {
                    Header: n(67608).Z,
                    Comment: C.Z,
                    Pagination: D.Z
                },
                data: function() {
                    return {
                        items: [],
                        total: null,
                        page: 1,
                        limit: 10,
                        oldest: !1
                    }
                },
                mounted: function() {
                    this.fetch(),
                    this.removeHeaderNotify()
                },
                computed: {
                    pages: function() {
                        return this.total ? Math.ceil(this.total / this.limit) : 0
                    }
                },
                watch: {
                    oldest: function() {
                        this.fetch()
                    },
                    page: function() {
                        this.fetch()
                    }
                },
                methods: {
                    reply: function(e) {
                        window.location.href = e.url
                    },
                    fetch: function() {
                        var e = this;
                        return (0, k.Z)(b().mark((function t() {
                            var n,
                                i,
                                l;
                            return b().wrap((function(t) {
                                for (;;)
                                    switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2, w.Z.Replies({
                                            skip: e.page * e.limit - e.limit,
                                            take: e.limit,
                                            oldest: e.oldest
                                        });
                                    case 2:
                                        n = t.sent,
                                        i = n.items,
                                        l = n.total,
                                        e.items = i,
                                        e.total = l;
                                    case 7:
                                    case "end":
                                        return t.stop()
                                    }
                            }), t)
                        })))()
                    },
                    removeHeaderNotify: function() {
                        var e = document.querySelector(".header-profile .notify");
                        e && e.remove()
                    }
                }
            };
        var Z = (0, n(20311).Z)(S, [["render", function(e, t, n, a, k, y) {
            var b = (0, i.up)("Header"),
                w = (0, i.up)("Comment"),
                D = (0, i.up)("Pagination");
            return null !== e.total ? ((0, i.wg)(), (0, i.iD)("div", s, [(0, i.Wm)(b, {
                "sub-title": "Ответы на ваши комментарии"
            }, {
                default: (0, i.w5)((function() {
                    return [(0, i.Uk)(" Комментарии "), (0, i._)("span", null, "(" + (0, l.toDisplayString)(e.total) + ")", 1)]
                })),
                _: 1
            }), 0 === e.total ? ((0, i.wg)(), (0, i.iD)("div", r, u)) : ((0, i.wg)(), (0, i.iD)("div", c, [(0, i.Wm)(o.Transition, {
                name: "fade"
            }, {
                default: (0, i.w5)((function() {
                    return [(0, i._)("div", null, [(0, i._)("div", p, [d, (0, i._)("span", {
                        class: (0, l.normalizeClass)(["value", {
                            active: !e.oldest
                        }]),
                        onClick: t[0] || (t[0] = function(t) {
                            return e.oldest = !1
                        })
                    }, "новые", 2), (0, i._)("span", {
                        class: (0, l.normalizeClass)(["value", {
                            active: e.oldest
                        }]),
                        onClick: t[1] || (t[1] = function(t) {
                            return e.oldest = !0
                        })
                    }, "старые", 2)]), (0, i._)("div", f, [(0, i.Wm)(o.TransitionGroup, {
                        name: "fade"
                    }, {
                        default: (0, i.w5)((function() {
                            return [((0, i.wg)(!0), (0, i.iD)(i.HY, null, (0, i.Ko)(e.items, (function(n) {
                                return (0, i.wg)(), (0, i.iD)("div", {
                                    class: (0, l.normalizeClass)(["lk-comment-list__item", {
                                        unviewed: !n.viewed
                                    }]),
                                    key: n.id
                                }, [(0, i._)("a", {
                                    class: "back-link",
                                    title: "Перейти к комментарию",
                                    href: n.url
                                }, null, 8, m), (0, i.Wm)(w, {
                                    onReply: function(e) {
                                        return y.reply(n)
                                    },
                                    onViewed: t[2] || (t[2] = function(t) {
                                        return e.$emit("viewed")
                                    }),
                                    vote: n.vote,
                                    onVoted: function(e) {
                                        return n.vote = e.value
                                    },
                                    onComplained: function() {
                                        return n.complaint = !0
                                    },
                                    complaint: n.complaint,
                                    comment: n
                                }, {
                                    before_date: (0, i.w5)((function() {
                                        return [(0, i._)("div", v, [(0, i.Uk)(" ответил(а) на ваш комментарий "), null !== n.episode ? ((0, i.wg)(), (0, i.iD)(i.HY, {
                                            key: 0
                                        }, [(0, i.Uk)(" к сериалу «" + (0, l.toDisplayString)(n.cinema) + "», ", 1), (0, i._)("a", {
                                            title: n.episode,
                                            href: n.entity_url
                                        }, (0, l.toDisplayString)(n.episode), 9, h)], 64)) : ((0, i.wg)(), (0, i.iD)(i.HY, {
                                            key: 1
                                        }, [(0, i.Uk)(" к фильму «"), (0, i._)("a", {
                                            title: n.episode,
                                            href: n.entity_url
                                        }, (0, l.toDisplayString)(n.cinema), 9, g), (0, i.Uk)("» ")], 64))])]
                                    })),
                                    _: 2
                                }, 1032, ["onReply", "vote", "onVoted", "onComplained", "complaint", "comment"])], 2)
                            })), 128))]
                        })),
                        _: 1
                    })]), y.pages > 1 ? ((0, i.wg)(), (0, i.iD)("div", _, [(0, i.Wm)(D, {
                        modelValue: e.page,
                        "onUpdate:modelValue": t[3] || (t[3] = function(t) {
                            return e.page = t
                        }),
                        pages: y.pages
                    }, null, 8, ["modelValue", "pages"])])) : (0, i.kq)("v-if", !0)])]
                })),
                _: 1
            })]))])) : (0, i.kq)("v-if", !0)
        }]])
    },
    67608: function(e, t, n) {
        n.d(t, {
            Z: function() {
                return d
            }
        });
        var i = n(9012),
            l = n(35500),
            o = n(98993),
            a = {
                class: "page-profile-header"
            },
            s = {
                class: "left"
            },
            r = (0, i._)("div", {
                class: "line"
            }, null, -1),
            u = {
                key: 0,
                class: "right"
            },
            c = {
                key: 0,
                class: "profile-header__page-settings"
            };
        var p = {
            name: "Header",
            components: {
                FButton: n(86460).Z
            },
            props: {
                modelValue: {
                    type: Boolean,
                    default: !1
                },
                title: {
                    type: String,
                    default: null
                },
                subTitle: {
                    type: String,
                    default: null
                },
                allowSelection: {
                    type: Boolean,
                    default: !1
                },
                selectionText: {
                    type: String,
                    default: "Редактировать"
                }
            },
            computed: {
                edit: {
                    get: function() {
                        return this.modelValue
                    },
                    set: function(e) {
                        this.$emit("update:modelValue", e)
                    }
                }
            }
        };
        var d = (0, n(20311).Z)(p, [["render", function(e, t, n, p, d, f) {
            var m = (0, i.up)("FButton");
            return (0, i.wg)(), (0, i.iD)("div", a, [(0, i._)("div", s, [(0, i._)("h1", null, [(0, i.WI)(e.$slots, "default")]), (0, i._)("p", null, (0, l.toDisplayString)(n.subTitle), 1), r]), n.allowSelection ? ((0, i.wg)(), (0, i.iD)("div", u, [(0, i._)("button", {
                class: "p-button p-button-36 p-button_type_gray-border selection-toggle",
                onClick: t[0] || (t[0] = function(e) {
                    return f.edit = !0
                })
            }, [(0, i._)("span", null, (0, l.toDisplayString)(n.selectionText), 1)]), (0, i.Wm)(o.Transition, {
                name: "fade"
            }, {
                default: (0, i.w5)((function() {
                    return [f.edit ? ((0, i.wg)(), (0, i.iD)("div", c, [(0, i._)("button", {
                        class: "p-button p-button_type_link",
                        onClick: t[1] || (t[1] = function(t) {
                            return e.$emit("select-all")
                        })
                    }, "Выбрать все"), (0, i._)("button", {
                        class: "p-button p-button_type_link",
                        onClick: t[2] || (t[2] = function(t) {
                            return e.$emit("remove")
                        })
                    }, "Удалить выбранное"), (0, i.Wm)(m, {
                        class: "p-button_type_gray-border p-button-36",
                        onClick: t[3] || (t[3] = function(e) {
                            return f.edit = !1
                        })
                    }, {
                        default: (0, i.w5)((function() {
                            return [(0, i.Uk)("Отмена")]
                        })),
                        _: 1
                    })])) : (0, i.kq)("v-if", !0)]
                })),
                _: 1
            })])) : (0, i.kq)("v-if", !0)])
        }]])
    },
    66336: function(e, t, n) {
        n.d(t, {
            Z: function() {
                return b
            }
        });
        var i = n(9012),
            l = n(35500),
            o = {
                class: "pagination2"
            },
            a = [(0, i._)("span", null, [(0, i._)("span", {
                class: "icon-chevron-small-left"
            })], -1)],
            s = [(0, i._)("span", null, [(0, i._)("span", {
                class: "icon-chevron-small-right"
            })], -1)],
            r = [(0, i._)("span", null, "...", -1)],
            u = {
                key: 3,
                class: "item active"
            },
            c = ["onClick"];
        var p = n(39940),
            d = n.n(p),
            f = null,
            m = null;
        function v() {
            var e = f;
            return f = null, m = null, e
        }
        function h(e, t) {
            f.push({
                type: "page",
                number: e,
                active: e === m,
                hide_on_mobile: t,
                hideOnMobile: function() {
                    this.hide_on_mobile = !0
                },
                isActive: function() {
                    return this.active
                }
            })
        }
        function g() {
            return {
                type: "delimiter",
                hide_on_desktop: arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
                hide_on_mobile: arguments.length > 1 && void 0 !== arguments[1] && arguments[1]
            }
        }
        function _() {
            var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            f.unshift({
                type: "prev",
                hide_on_desktop: e
            })
        }
        function k() {
            var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
            f.push({
                type: "next",
                hide_on_desktop: e
            })
        }
        var y = {
            name: "Pagination",
            props: {
                modelValue: {
                    type: Number
                },
                pages: {
                    type: Number,
                    required: !0
                }
            },
            data: function() {
                return {
                    start: null,
                    end: null
                }
            },
            computed: {
                items: function() {
                    return function(e, t) {
                        if (e < 0 || e > t)
                            throw new Error("Page - out of range.");
                        if (f = [], m = e, t < 8) {
                            for (var n = 1; n <= t; n++)
                                h(n, !1);
                            return v()
                        }
                        if (t < 12) {
                            for (var i = 1; i <= t; i++)
                                h(i, !1);
                            if (e <= 4) {
                                for (var l = 6; l < t; l++)
                                    f[l - 1].hideOnMobile();
                                d()(f).call(f, t - 1, 0, g(!0))
                            } else if (e >= t - 3) {
                                for (var o = t - 5; o > 1; o--)
                                    f[o - 1].hideOnMobile();
                                d()(f).call(f, 1, 0, g(!0))
                            } else {
                                for (var a = 2, s = null; !(s = f[a - 1]).isActive();)
                                    s.hideOnMobile(),
                                    a++;
                                for (; a < t;)
                                    (s = f[a - 1]).hideOnMobile(),
                                    a++;
                                d()(f).call(f, 1, 0, g(!0)),
                                d()(f).call(f, t - 1, 0, g(!0)),
                                _(!0),
                                k(!0)
                            }
                            return v()
                        }
                        if (e <= 6) {
                            var r = null;
                            r = e > 4 ? function(t) {
                                return t > 1 && t !== e
                            } : function(e) {
                                return e > 5
                            };
                            for (var u = 1; u < 10; u++)
                                h(u, r(u));
                            f.push(g()),
                            h(t, !1),
                            e > 4 && (d()(f).call(f, 1, 0, g(!0)), _(!0), k(!0))
                        } else if (e >= t - 5) {
                            h(1, !1),
                            f.push(g());
                            var c = null;
                            c = e < t - 3 ? function(e) {
                                return e !== t
                            } : function(e) {
                                return e < t - 4
                            };
                            for (var p = t - 8; p <= t; p++)
                                h(p, c(p));
                            e < t - 3 && (d()(f).call(f, f.length - 1, 0, g(!0)), _(!0), k(!0))
                        } else {
                            h(1, !1),
                            f.push(g());
                            for (var y = e - 2; y <= e + 2; y++)
                                h(y, y !== e);
                            f.push(g()),
                            h(t, !1),
                            _(),
                            k()
                        }
                        return v()
                    }(this.page, this.pages)
                },
                page: {
                    get: function() {
                        return this.modelValue
                    },
                    set: function(e) {
                        this.$emit("update:modelValue", e)
                    }
                }
            },
            methods: {
                setPage: function(e) {
                    this.page !== e && (window.scrollTo(0, 0), this.page = e)
                }
            }
        };
        var b = (0, n(20311).Z)(y, [["render", function(e, t, n, p, d, f) {
            return (0, i.wg)(), (0, i.iD)("div", o, [((0, i.wg)(!0), (0, i.iD)(i.HY, null, (0, i.Ko)(f.items, (function(e) {
                return (0, i.wg)(), (0, i.iD)(i.HY, null, ["prev" === e.type ? ((0, i.wg)(), (0, i.iD)("a", {
                    key: 0,
                    class: (0, l.normalizeClass)([{
                        "hide-desktop": e.hide_on_desktop
                    }, "item prev-next"]),
                    href: "#",
                    onClick: t[0] || (t[0] = function(e) {
                        return f.setPage(f.page - 1)
                    })
                }, a, 2)) : "next" === e.type ? ((0, i.wg)(), (0, i.iD)("a", {
                    key: 1,
                    class: (0, l.normalizeClass)(["item prev-next", {
                        "hide-desktop": e.hide_on_desktop
                    }]),
                    href: "#",
                    onClick: t[1] || (t[1] = function(e) {
                        return f.setPage(f.page + 1)
                    })
                }, s, 2)) : "delimiter" === e.type ? ((0, i.wg)(), (0, i.iD)("div", {
                    key: 2,
                    class: (0, l.normalizeClass)([{
                        "hide-desktop": e.hide_on_desktop,
                        "hide-mobile": e.hide_on_mobile
                    }, "item delimiter"])
                }, r, 2)) : e.active ? ((0, i.wg)(), (0, i.iD)("div", u, [(0, i._)("span", null, (0, l.toDisplayString)(e.number), 1)])) : ((0, i.wg)(), (0, i.iD)("a", {
                    key: 4,
                    class: (0, l.normalizeClass)(["item", {
                        "hide-mobile": e.hide_on_mobile
                    }]),
                    onClick: function(t) {
                        return f.setPage(e.number)
                    }
                }, [(0, i._)("span", null, (0, l.toDisplayString)(e.number), 1)], 10, c))], 64)
            })), 256))])
        }]])
    }
}]);
