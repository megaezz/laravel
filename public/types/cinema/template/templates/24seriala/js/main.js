// helpers 
function leftpad (str, max) {
  str = str.toString();
  return str.length < max ? leftpad("0" + str, max) : str;
}
mirfunc = {
  'setPlaylist': function(e) {
    if (mark.playlist !== undefined) {
      delete player;
      $('[data-click="setPlaylist"]').removeClass('tabs-translate-element__active');
      if(e !== undefined) {
        mark.playlist = e.data('playlist');
        e.addClass('tabs-translate-element__active');
      } else {
        $('[data-playlist="'+mark.playlist+'"]').addClass('tabs-translate-element__active');
      }
      if(Cookies.get("ser24ad") > "3"){
        strAdv = '/uppod/s244.xml or /uppod/s245.xml';
      }
      player = new Playerjs({id:"pl",file:mark.playlist});
    }
  },
  'getSeries': function() { // тырим серию из плеера
    var seria =  mirfunc['api']("playlist_id");
    return seria;
    // return player.Get("comment").slice(0, player.Get("comment").indexOf(' '));
  },
  'getTime': function() { // тырим время из плеера
    var time = mirfunc['api']("time");
    if(-1 == time){
      time = 0;
    }
    return time;
  },
  'parseHash': function(hash) {
    if (hash == undefined) {
      var hash = window.location.hash;
    }
    var ret = {}
    if (-1 != hash.indexOf("rewind")) {
      ret['mark'] = hash.split("=")[1];
      ret['arMark'] = ret['mark'].split("_");
      ret['ser'] = ret['arMark'][0] + "_" + ret['arMark'][1];
      if (-1 != ret['mark'].indexOf("minute")) {
        ret['min'] = ret['arMark'][3];
      } else {
        ret['min'] = 0;
      }
      if (-1 != ret['mark'].indexOf("sekunde")) {
        ret['sec'] = ret['arMark'][5];
      } else {
        ret['sec'] = 0;
      }
    }
      return ret;
  },
  'api': function(action1,action2) {
    if(window.Playerjs !== undefined) {
      try {
        return player.api(action1,action2)
      } catch(e) {}
    } else {
      alert('ошибка плеера');
    }
  },
  'showFilter': function(t) {
    var $more = $('.filter-more');
    if ($more.hasClass('is-active')) {
      $more.removeClass('is-active');
      t.find('span').text('Прочие фильтры');
    } else {
      $more.addClass('is-active')
      t.find('span').text('Скрыть фильтры');
    }
  },
  'menuSH': function(t) {
    var $name = t.data('menubtn');
    if ($('[data-menures='+$name+']').hasClass('is-active')) {
      $('[data-menures='+$name+']').hide().removeClass('is-active');
      $('[data-menubtn='+$name+']').removeClass('is-active');
      if (t.data('menuafter')) {
        mirfunc[t.data('menuafter')](t);
      }
    } else {
      $('[data-menures='+$name+']').show().addClass('is-active');
      $('[data-menubtn='+$name+']').addClass('is-active');
      if (t.data('menubefore')) {
        mirfunc[t.data('menubefore')](t);
      }
    }
  },
  'load-showmarkpage': function() {
    $res = $('[data-menures="popup-marks-inned"]');
    $.ajax({
      url: "/?mod=pause",
      beforeSend: function() {
        $res.html('<div class="loader"></div>');
      },
      success: function(data) {
        $res.html(data);
      },
      error: function() {
        $res.html('<div class="msg">ошибка загрузки</div>');
      }
    });
  },
  'idSeason': function() {
    return $('[data-serialid]').data('serialid');
  },
  'playstart': function() {
    var tempSer = true;
    var pause = mirfunc['parseHash']();
    if (pause['mark'] == undefined) {return true;}
    var number = pause['arMark'][0];
    if (pause['min'] > 0 || pause['sec'] > 0) {
      var timeSeek = pause['min'] * 60 + parseInt(pause['sec']);
      mirfunc['api']("play","id:"+number+"[seek:"+timeSeek+"]");
      tempSer = false;
    } else{
      mirfunc['api']("play","id:"+number)
      tempSer = false;
      mirfunc['api']("next")
    }
    if(tempSer){
      mirfunc['api']("play","id:1");
    }
  }
};

mirfunc['set-mark'] = function(t) { // ставим закладку
  // 1 на серии, 2 на моменте, -1 хочу, -2 уже, -3 в блеклист
  var send = {
    'id': mirfunc['idSeason'](),
    'minute': 0,
    'second': 0,
    'tran': mark['trans'] == '' ? 0 : mark['trans'] 
  };
  send['seria'] =  t.data('set-mark');
  if (mark['trans'] == 68 && send['seria'] > 0 ) { // если трейлеры то хочу посмотреть
    send['seria'] = "-1";
  }
  if (send['seria'] == '-1'){ send['wanttosee'] = true;}
  if (send['seria'] == '-2'){ send['watched'] = true;}
  if (send['seria'] == '-3'){ send['notWatched'] = true;}

  if (send['seria'] == '2') { // если надо время
    var time = mirfunc['getTime']();
    send['minute'] = Math.floor(time / 60);
    send['second'] = parseInt(time - send['minute']*60);
  }
  if (send['seria'] > 0) {
    send['pauseadd'] = true;
    send['seria'] = mirfunc['getSeries']();
  }
  $.post("/jsonMark.php",
    send,
    function(obj) {
      if ("success" == obj.msg) {
        mirfunc['show-mark'](send['seria'], send['minute'], send['second']);
        // mirfunc.notify({
        //   txt: 'отметка установлена',
        //   type: 'msg'
        // });
        if(true !== obj.id && obj.id !== undefined && mark['trans'] !== 68){ // если есть закладка на предыдущем сезоне и тукущий перевод не трейлеры
          if(confirm("у вас есть отметка на последней серии предыдущего сезона, удалить ее?")){
            $.post("jsonMark.php", {delId: obj.id}, function(){
            }, "json");
          }
        }
      }
    },
    "json"
  );
}
mirfunc['show-mark'] = function(seria, minute, sekunde) { // показываем закладки
  var min = '';
  var series = '';
  var icon = '';
  var $result = $('[data-currmarks=statpause]');
  var dataclick = ' data-click="currentmarkGoTo"';
  if(0 != minute || 0 != sekunde){
    min = " (" + leftpad(minute,2) + ":"+ leftpad(sekunde,2) +")" ;
  }
  if(-1 == seria){
    series = 'Хочу посмотреть';
    dataclick = '';
  }
  else if(-2 == seria) {
    series = 'Посмотрел';
    dataclick = '';
  }
  else if(-3 == seria) {
    series = 'Не буду смотреть';
    dataclick = '';
  } else {
    series = seria +  ' серия' + min;
  }
  $result.html('<button'+dataclick+'>' + series + '</button><button class="button-remove button-remove__default" data-click="remove-marks" data-id="'+ mirfunc['idSeason']() +'">✕</button>');
};
mirfunc['currentmarkGoTo'] = function(t) { // переходим на закладку
  // TODO: переход по закладке, не учтен translate
  var data = t.text().trim().split(' ');
  var hash = 'rewind='+data[0]+'_seriya';
  if (data[2] !== undefined) {
    var min = data[2].replace('(','').replace(')','').split(':');
    hash = hash+'_na_'+min[0]+'_minute'+'_'+min[1]+'_sekunde';
  }
  if ('#'+hash == window.location.hash) {
    mirfunc['playstart']();
  } else {
    location.hash = hash;
    mirfunc['playstart']();
  }
}
mirfunc['remove-marks'] = function(t) {
  var id
  if(t !== undefined) {
    id = t.data('id');
  }
  if (id == undefined) {
    id = mirfunc['idSeason']();
  }
  var send = {'delId': id };
  $.post("jsonMark.php", send,
    function(obj){
      if("success" == obj.msg) {
        var $marks = $('[data-serial-remove-id='+id+']');
        if ($marks.length > 0){
          $marks.remove();
        } else {
          $('[data-currmarks=statpause]').html('');
        }
        if (id == mirfunc['idSeason']()) {
          $('[data-currmarks=statpause]').html('');
        } 
        // mirfunc.notify({
        //   txt: 'отметка удалена',
        //   type: 'msg'
        // });
      } else {
        // mirfunc.notify({
        //   txt: 'Ошибка удаления отметки, попробуйте удалить позже',
        //   type: 'error'
        // });
        return false;
      }
    },"json"
  );
}

mirfunc['ajax-load'] = function(t) {
  // history.replaceState({}, '', '/');
  $('.left-link').removeClass('act');
  t.addClass('act');
  var $res = $('.content');
  var $load = t.data('load');
  if ($load !== undefined) {
    var $url = "/ajax.php?mode=" + $load
  } else {
    var $url = t.attr('href');
  }
  $.ajax({
    url: $url,
    beforeSend: function() {
      $res.html('<div class="loader"></div>');
    },
    success: function(data) {
      $res.html(data);
      $(".lazy").unveil(600);
      if ($load == "pop") {
        $('[data-headload]').text('Популярные сезоны')
      }
      if ($load == "new") {
        $('[data-headload]').text('Новинки')
      }
    },
    error: function() {
      $res.html('<div class="msg">ошибка загрузки</div>');
    }
  });
}
mirfunc['ajax-load-genre'] = function(t) {
  var $res = $('.content');
  var $genre = t.data('genre');
  var $letter = t.data('letter');
  var $title = t.text();
  if ($genre !== undefined) {
    $genre = $genre;
    $('.left-link').removeClass('act');
  } else if ($('[data-genre].act') !== undefined) {
    $genre = $('[data-genre].act').data('genre') ;
    $title = $('[data-genre].act').text();
  } else {
    $genre  = 6;
  };
  if( $letter !== undefined ) {
    $letter = t.data('letter').replace('letter', '');
  } else {
    $letter = "#"
  }
  t.addClass('act');
  $.post("/ajax.php", {
      "letter": $letter,
      "genre": $genre,
      beforeSend: function() {
        $res.html('<div class="loader"></div>');
      }
    }, function (data) {
      $res.html(data);
      $('[data-headload]').text($title);
      $('[data-letter="letter'+$letter +'"]').addClass('act')
    }
  )
};


// автокомплит {
  function SvComplete() {
    this.click = '';
    this.select = function() {
      var cur = this.ul.find("li[aria-selected=true]");
      if (typeof(this.click) === 'function'){
        this.click(cur.data('complete-id'));
      } else {
        this.input.val(cur.text());
        this.ul.hide();
      }
      return false;
    };
    this.show = function(c) {
      var list = this.ul;
      var selected = list.find("li[aria-selected=true]");
      var li = list.find("li");
      if (c == 13) { // enter
        if (!!selected.length) {
          this.select();
        } else {
          // list.show();
          list.closest('form').submit();
        }
        return false;
      }
      if (c == 27) { // esc
        list.hide();
        return false;
      }
      li.attr('aria-selected',false)
      if (c == 38) { // up
        if( !selected.length ) {
          selected = li.first();
        }
        if (selected.prev().length == 0) {
          selected.siblings().last().attr('aria-selected',true)
        } else {
          selected.prev().attr('aria-selected',true);
        }
      } else if (c == 40) { // down
        if( !selected.length ) {
          selected = li.last();
        }
        if (selected.next().length == 0) {
            selected.siblings().first().attr('aria-selected',true);
        } else {
            selected.next().attr('aria-selected',true);
        }
      }
      if( !list.find("li[aria-selected=true]").length ) {
        selected = li.first().attr('aria-selected',true);
      }
      if (selected.length) {
        selected = li.first();
        var sto = list.find("li[aria-selected=true]").position().top;
        var to = list.scrollTop() + sto;
        if ( sto < 0 || sto > list.height() ) {
          list.scrollTop(to);
        }
      }
    };
    this.res = [];
    this.input = '';
    this.ul = '';
    this.config = function(e) {
      var proto = this;
      var ul = $('<ul class="awesomplete-list" data-complite=list></ul>');
      e.input.attr('autocomplete','off').wrap('<div class="awesomplete"></div>');
      e.input.after(ul);
      this.ul = ul;
      this.input = e.input;
      this.click = e.click;
      e.input.on('focus', function() {
        ul.show();
      });
      e.input.on('focusout', function(e){
        var timer = setTimeout(function () {
          ul.hide();
        }, 1000);
      });
      ul.on('click', 'li', function() {
        ul.find("li").attr('aria-selected',false);
        $(this).attr('aria-selected', true);
        proto.select();
      });
    };
  }
// }
// /автокмплит




















;(function($) { // lazyload
  $.fn.unveil = function(threshold, callback, element) {
    var $w = $(element || window),
        th = threshold || 0,
        attrib = "data-src",
        images = this,
        loaded;
    this.one("unveil", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        this.setAttribute("src", source);
        this.removeAttribute("data-src");
        this.onload = function() {
          this.classList.remove("lazy");
        }
        if (typeof callback === "function") callback.call(this);
      }
    });
    function unveil() {
      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.is(":hidden")) return;
        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();
        return eb >= wt - th && et <= wb + th;
      });
      loaded = inview.trigger("unveil");
      images = images.not(loaded);
    }
    $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);
    unveil();
    return this;
  };
})(window.jQuery);



var $body = $('body');
$(function() { 
  $(".lazy").unveil(600);
  $body.on('click','[data-click]', function(e)  {
    e.preventDefault();
    var funcname = window[$(this).data('click')];
    var dataclick = mirfunc[$(this).data('click')];
    if (typeof(dataclick) === 'function'){
      dataclick($(this));
    } else if (typeof(funcname) === 'function') {
      funcname($(this));
    }
     else {
      console.log('Что-то ты не то кликаешь ибо сие не работает аки функция. Толи недописана, толи кто-то накосячил :: '+$(this).data('click'))
    }
  });
  $body.on('click','[data-tabt]', function(e)  {
    e.preventDefault();
    var $tabname = $(this).data('tabt'); // имя таба
    var $tabgroupname = $(this).data('tabg'); // имя группы табов
    $('[data-tabg="'+$tabgroupname+'"]').removeClass('is-active'); // группа табов
    $('[data-tabgr="'+$tabgroupname+'"]').removeClass('is-active');
    var $restab = $('[data-tabr="'+$tabname+'"]').addClass('is-active'); // результирующий таб
    $(this).addClass('is-active') // текущий элимент

  });

  // для страници плеера
  if($('[data-currmarks="statpause"]').length > 0) {
    var marksCurrent = mark['href'].split("#")[1];
    if(marksCurrent !== undefined) {
      var send = mirfunc['parseHash'](marksCurrent);
      send['ser'] = send['ser'].split('_')[0];
      mirfunc['show-mark'](send['ser'], send['min'], send['sec']);
    }
    mirfunc['playstart']();
  }

  // вешаем события  на фильтры
  var $filter = $('.filter [multiple="multiple"], .filter [data-single="true"]');
  if ($filter.length > 0) {
    $filter.multipleSelect();
    $filter.on('change', function(e)  {
      mirfunc['filter-index']($(this),e);
      // console.log($(this))
    });
  }




  var $searhbox = $('input[data-input=search]');
  var searchBox = new SvComplete();
  searchBox.config({
    'input': $searhbox,
    'click': function(k) {
      if (searchBox.res[k]['url'] == '' || searchBox.res[k]['url'] == undefined) {
        searchBox.input.val('');
      } else {
        searchBox.input.val(searchBox.res[k]['name'].replace(/<.*>/g,"").replace(/\(.*сезон\)/,""));
        location.href = '/' + searchBox.res[k]['url'];
      };
    }
  });
  var dateClick = Date.now();
  var $searchClear = $('.search-btn');
  $searhbox.on("keyup", function(e){
    if((Date.now() - dateClick)>200) {
      var str = this.value;
      var c = e.keyCode;
      if (c == 38 || c == 40 || c == 13 || c == 39 || c == 37 || c == 27) { // Down/Up arrow LEFT: 37, UP: 38, RIGHT: 39, DOWN: 40, ENTER: 13, ESC: 27
        e.preventDefault();
        searchBox.show(c);
        return false;
      } else {
        searchBox.res = [];
        var $ul = $(this).next('[data-complite=list]').addClass('pgs-list');
        $ul.find('li').remove();
        if (str.length > 2) {
          $searchClear.addClass('js-show')
          $.ajax({
            url: '/autocomplete.php?query=' + str,
            type: 'GET',
            dataType: 'json'
          })
          .done(function (data) {
            if (!!data.id) {
              $.each(data.suggestions.valu, function (key, value) {
                searchBox.res.push({
                    'name': value,
                    'url': data.data[key],
                    'id': data.id[key]
                });
                // $ul.append('<li data-complete-id='+key+'>'+value.replace(str,'<mark>'+str+'</mark>')+'</li>');
                $ul.append('<li data-complete-id=' + key + '>' + value.replace(new RegExp(str, 'i'), '<mark>$&</mark>') + '</li>');
                // $ul.find('li').first().attr('aria-selected',true);
              });
            }
          });
        } else {
          $searchClear.removeClass('js-show')
        }
      }
      dateClick = Date.now();
    }
  });
    
});