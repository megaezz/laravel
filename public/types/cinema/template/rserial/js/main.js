document.addEventListener("DOMContentLoaded", function() {
	$('.second-menu ul li a').on('click',function(){
		$('.second-menu-content').hide();
		$('.'+$(this).data('block')).fadeIn();
		$('.second-menu ul li a').removeClass('active');
		$(this).addClass('active');
	});
	$('.btnCompilations').on('click',function(){
		$('.second-menu-content').hide();
		$('.'+$(this).data('block')).fadeIn();
		$('.second-menu ul li a').removeClass('active');
	});
});