<?php

function uaPixel()
{
    $ua = $_SERVER['HTTP_USER_AGENT'];
    if (stripos($ua, 'ipad') !== false ||
    stripos($ua, 'windows nt') !== false ||
    stripos($ua, 'macintosh') !== false ||
    stripos($ua, 'wow') !== false ||
    stripos($ua, 'x11') !== false) {
        return false;
    }

    return true;
}

function uaTouch()
{
    $ua = $_SERVER['HTTP_USER_AGENT'];
    if (stripos($ua, 'opera') !== false) {
        return false;
    }
    if (
        stripos($ua, 'Nokia6303iclassic') !== false ||
        stripos($ua, 'iphone') !== false ||
        stripos($ua, 'ipad') !== false ||
        stripos($ua, 'ipod') !== false ||
        stripos($ua, 'blackberry') !== false ||
        stripos($ua, 'android') !== false
    ) {
        return true;
    }

    return false;
}

function isMegafon()
{
    $ips = [];
    if ($_SERVER['HTTP_X_REAL_IP']) {
        $ip_arr_x_real = explode(',', $_SERVER['HTTP_X_REAL_IP']);
        $ips = $ips + $ip_arr_x_real;
    }
    if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
        $ip_arr_x_forwarded = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $ips = $ips + $ip_arr_x_forwarded;
    }
    if (request()->ip()) {
        $ip_arr_x_remote = explode(',', request()->ip());
        $ips = $ips + $ip_arr_x_remote;
    }

    $IP1 = [622680064, 786794496, 786795520, 786795776, 786796032, 1046069248, 1310280192, 1310281728, 1402273792, 1402275840, 1402275840, 1402278912, 1402279936, 1402281984, 1402283008, 1402284032, 1402285056, 1402286080, 1402287104, 1427809280, 1427813376, 1427814400, 1427817984, 1427824640, 1427826432, 1427826688, 1427828736, 3251233792];
    $IP2 = [622681087, 786794751, 786795775, 786796031, 786796287, 1046085631, 1310280447, 1310283775, 1402275839, 1402276863, 1402276351, 1402279935, 1402280959, 1402283007, 1402284031, 1402285055, 1402286079, 1402287103, 1402288127, 1427810303, 1427814399, 1427815423, 1427818111, 1427825663, 1427826559, 1427827711, 1427829759, 3251234815];

    foreach ($ips as $ip) {
        $ip_parts = split("\.", "$ip");
        @$ipnum = $ip_parts[3] + $ip_parts[2] * 256 + $ip_parts[1] * 256 * 256 + $ip_parts[0] * 256 * 256 * 256;
        if ($ipnum == 0) {
            return false;
        }
        for ($ix = 0; $ix < count($IP1); $ix++) {
            if ($ipnum >= $IP1[$ix] && $ipnum <= $IP2[$ix]) {
                return $ip;
            }
        }
    }

    return false;
}

function isMTS()
{
    $ips = [];
    if ($_SERVER['HTTP_X_REAL_IP']) {
        $ip_arr_x_real = explode(',', $_SERVER['HTTP_X_REAL_IP']);
        $ips = $ips + $ip_arr_x_real;
    }
    if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
        $ip_arr_x_forwarded = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $ips = $ips + $ip_arr_x_forwarded;
    }
    if (request()->ip()) {
        $ip_arr_x_remote = explode(',', request()->ip());
        $ips = $ips + $ip_arr_x_remote;
    }

    $IP1 = [1347126528, 1347674112, 1347677184, 1360933376, 1372794624, 1410457600, 1410465280, 1500315648, 1535627776, 1603903488, 1603907584, 1603926016, 3258356736, 3273026560, 3276428288, 3277188608, 3277989440, 3281465344, 3287259392, 3559689216, 3562834688, 3562834880, 3577445376, 3578831872, 3579248640, 3579259392, 3579267072, 3579268096, 3579269120, 3579273216, 3579273728, 3579277824, 3579278336, 3579279360, 3579279872, 3579280384, 3579281408, 3579283456, 3579297792, 3579310080, 3641237504, 3641240576, 3641283968, 3645018112, 3645020160, 3645566976, 3645569280, 3645569536, 3645569792, 3645570048, 3647698432, 3648478720];
    $IP2 = [1347127167, 1347674623, 1347678207, 1360933887, 1372794879, 1410459647, 1410465791, 1500332031, 1535628031, 1603907583, 1603911679, 1603928063, 3258357759, 3273027583, 3276428543, 3277188863, 3277989503, 3281465599, 3287259647, 3559689471, 3562834879, 3562834943, 3577445631, 3578832127, 3579258879, 3579265535, 3579267935, 3579268607, 3579273215, 3579273727, 3579274239, 3579278335, 3579278847, 3579279871, 3579280383, 3579280895, 3579283455, 3579285503, 3579299839, 3579311103, 3641240575, 3641241599, 3641284031, 3645019135, 3645022207, 3645569023, 3645569535, 3645569791, 3645570047, 3645570303, 3647698687, 3648479231];

    foreach ($ips as $ip) {
        $ip_parts = split("\.", "$ip");
        @$ipnum = $ip_parts[3] + $ip_parts[2] * 256 + $ip_parts[1] * 256 * 256 + $ip_parts[0] * 256 * 256 * 256;
        if ($ipnum == 0) {
            return false;
        }
        for ($ix = 0; $ix < count($IP1); $ix++) {
            if ($ipnum >= $IP1[$ix] && $ipnum <= $IP2[$ix]) {
                return $ip;
            }
        }
    }

    return false;
}
