<?php

function form_domains_redirects()
{
    $data = query('select id,domain,link 
		from '.tabname('go', 'domains_redirects').' 
		order by domain;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr>
		<td>'.$v['domain'].'</td>
		<td>
			<input type="hidden" name="links['.$v['id'].'][domain]" value="'.$v['domain'].'"/>
			<input type="text" name="links['.$v['id'].'][link]" value="'.$v['link'].'"/>
		</td>
		</tr>';
    }

    return $list;
}

function submit_domains_redirects()
{
    $affected_domains = '';
    if (! empty($_POST['links'])) {
        $arr = $_POST['links'];
    }
    if (empty($arr)) {
        error('�� ������� ������ ������');
    }
    // pre($arr);
    foreach ($arr as $id => $v) {
        query('update '.tabname('go', 'domains_redirects').'
			set link=\''.$v['link'].'\' 
			where id='.$id.';');
        if (mysql_affected_rows() != 0) {
            if (! empty($affected_domains)) {
                $affected_domains .= ', ';
            }
            $affected_domains .= $v['domain'];
        }
    }
    if (empty($affected_domains)) {
        alert('������ �� ��������');
    }
    alert('��������� ���������. ���������� ������: '.$affected_domains);
}

function link_stat_ruscams()
{
    $data = query('select
		    date(date) as dateday,
		    link,
		    count(*) as q,
		    count(distinct ip) as q_uniq
		    from '.tabname('go', 'clicks').'
		    where link=\'http://gloryadult.com/tds/in.cgi?12\'
		    group by dateday,link
		    order by dateday desc,q desc
		    ');
    $arr = readdata($data, 'nokey');
    $data = query('select payed_clicks from '.tabname('go', 'users').' where login=\'ruscams\'');
    $arr2 = mysql_fetch_assoc($data);
    $list = '';
    $q_uniq_all = 0;
    foreach ($arr as $v) {
        $q_uniq_all = $q_uniq_all + $v['q_uniq'];
        $list .= '<tr><td>'.$v['dateday'].'</td><td>'.$v['link'].'</td><td>'.$v['q'].'</td><td>'.$v['q_uniq'].'</td></tr>';
    }
    $content = '<table class="admin_table">
	<tr style="font-weight: bold; text-align: center;"><td>����</td><td>������</td><td>���������</td><td>����������</td></tr>
	'.$list.'
	</table>
	<br/>
	�������� ���������� ���������: '.$arr2['payed_clicks'].'<br/>
	���������: '.$q_uniq_all.'<br/>
	��������: '.($arr2['payed_clicks'] - $q_uniq_all).'<br/>
	';

    return $content;
}

function last_clicks_compare()
{
    $data = query('select
		    date,block_id,teaser_id,inet_ntoa(ip) as ip_addr
		    from megaweb_teaser.clicks
		    order by date desc
		    limit 100
		    ');
    $teaser_arr = readdata($data, 'nokey');
    $data = query('select
		    date,domain,dest,geo,link,inet_ntoa(ip) as ip_addr
		    from megaweb_go.clicks
		    order by date desc
		    limit 100
		    ');
    $go_arr = readdata($data, 'nokey');
    $teaser_list = '';
    foreach ($teaser_arr as $v) {
        $teaser_list .= '<tr><td>'.$v['date'].'</td><td>'.$v['block_id'].'</td><td>'.$v['teaser_id'].'</td><td>'.$v['ip_addr'].'</td></tr>';
    }
    $go_list = '';
    foreach ($go_arr as $v) {
        $go_list .= '<tr><td>'.$v['ip_addr'].'</td><td>'.$v['date'].'</td><td>'.$v['domain'].'</td><td>'.$v['dest'].'</td><td>'.$v['geo'].'</td><td>'.$v['link'].'</td></tr>';
    }
    $content = '<table class="admin_table">
	<tr>
	<td>
	<h3>������� �� �����:</h3>
	<table class="admin_table">
	<tr style="font-weight: bold; text-align: center;"><td>���� ����� �� �����</td><td>����</td><td>�����</td><td>IP</td></tr>
	'.$teaser_list.'
	</table>
	</td>
	<td>
	<h3>��������:</h3>
	<table class="admin_table">
	<tr style="font-weight: bold; text-align: center;"><td>IP</td><td>���� ��������</td><td>�����</td><td>�����������</td><td>������</td><td>������</td></tr>
	'.$go_list.'
	</table>
	</td>
	</tr>
	</table>';

    return $content;
}

function last_clicks()
{
    $data = query('select
		    '.tabname('teaser', 'clicks').'.date as teaser_date,
		    '.tabname('teaser', 'clicks').'.block_id as block_id,
		    '.tabname('teaser', 'clicks').'.teaser_id as teaser_id,
		    inet_ntoa('.tabname('teaser', 'clicks').'.ip) as ip,
		    max('.tabname('go', 'clicks').'.date) as go_date,
		    '.tabname('go', 'clicks').'.domain as go_domain,
		    '.tabname('go', 'clicks').'.dest as go_dest,
		    '.tabname('go', 'clicks').'.geo as go_geo,
		    '.tabname('go', 'clicks').'.link as go_link
		    from '.tabname('teaser', 'clicks').'
		    left join '.tabname('go', 'clicks').' on '.tabname('go', 'clicks').'.ip='.tabname('teaser', 'clicks').'.ip
		    group by '.tabname('teaser', 'clicks').'.ip
		    order by '.tabname('teaser', 'clicks').'.date desc
		    limit 100
		    ');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        if (empty($v['go_link']) == 1) {
            $v['go_link'] = 'WAP';
        }
        $list .= '<tr><td>'.$v['teaser_date'].'</td><td>'.$v['block_id'].'</td><td>'.$v['teaser_id'].'</td><td>'.$v['ip'].'</td><td>'.$v['go_date'].'</td><td>'.$v['go_domain'].'</td><td>'.$v['go_dest'].'</td><td>'.$v['go_geo'].'</td><td>'.$v['go_link'].'</td></tr>';
    }
    $content = '<table class="admin_table">
	<tr style="font-weight: bold; text-align: center;"><td>���� ����� �� �����</td><td>����</td><td>�����</td><td>IP</td><td>���� ��������</td><td>�����</td><td>�����������</td><td>������</td><td>������</td></tr>
	'.$list.'
	</table>';

    return $content;
}

function site_stat()
{
    $data = query('select
		    date(date) as dateday,
		    domain,
		    count(*) as q
		    from '.tabname('go', 'clicks').'
		    where date(date)=current_date
		    group by dateday,domain
		    order by dateday desc,q desc'
    );
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['dateday'].'</td><td>'.$v['domain'].'</td><td>'.$v['q'].'</td></tr>';
    }
    $content = '<table class="admin_table">
	<tr style="font-weight: bold; text-align: center;"><td>����</td><td>�����</td><td>���������</td></tr>
	'.$list.'
	</table>';

    return $content;
}

function link_stat()
{
    $data = query('select
		    date(date) as dateday,
		    link,
		    count(*) as q,
		    count(distinct ip) as q_uniq
		    from '.tabname('go', 'clicks').'
		    where date(date)=current_date
		    group by dateday,link
		    order by dateday desc,q desc
		    ');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['dateday'].'</td><td>'.$v['link'].'</td><td>'.$v['q'].'</td><td>'.$v['q_uniq'].'</td></tr>';
    }
    $content = '<table class="admin_table">
	<tr style="font-weight: bold; text-align: center;"><td>����</td><td>������</td><td>���������</td><td>����������</td></tr>
	'.$list.'
	</table>';

    return $content;
}

function geo_stat()
{
    $data = query('
		    select
		    date(date) as dateday,geo,count(*) as q_geo_clicks,
		    round(count(geo)/(select count(*) from '.tabname('go', 'clicks').' where date(date)=current_date)*100,2) as p
		    from '.tabname('go', 'clicks').'
		    where date(date)=current_date
		    group by dateday,geo
   		    order by dateday desc,q_geo_clicks desc;
		    ');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['dateday'].'</td><td>'.$v['geo'].'</td><td>'.$v['q_geo_clicks'].'</td><td>'.$v['p'].'%</td></tr>';
    }
    $content = '<table class="admin_table">
	<tr style="font-weight: bold; text-align: center;"><td>����</td><td>������</td><td>���������</td><td>����</td></tr>
	'.$list.'
	</table>';

    return $content;
}
