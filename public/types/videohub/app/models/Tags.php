<?php

namespace videohub\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class Tags
{
    private $rows = null;

    private $listTemplate = null;

    private $domain = null;

    private $active = null;

    private $withVideos = null;

    private $paginationUrlPattern = null;

    private $videoId = null;

    private $order = null;

    private $group = null;

    private $limit = null;

    private $page = null;

    private $tagEn = null;

    private $calcVideosRows = null;

    public function get()
    {
        $sql['limit'] = '';
        $sql['order'] = '';
        $sql['group'] = '';
        if ($this->getLimit()) {
            if (! $this->getPage()) {
                $this->setPage(1);
            }
            $sql['limit'] = 'limit '.(($this->page - 1) * $this->getLimit()).','.$this->getLimit();
        }
        $do['joinVideosDomain'] = false;
        $do['joinTags'] = false;
        if ($this->getDomain()) {
            $do['joinVideosDomain'] = true;
            $do['joinTags'] = true;
            $this->setGroup('tags_translation.tag_ru');
        }
        if ($this->getGroup()) {
            $sql['group'] = 'group by '.$this->getGroup();
        }
        if ($this->getOrder()) {
            $sql['order'] = 'order by '.$this->getOrder();
        }
        $sql['joinTags'] = $do['joinTags'] ? ('join '.F::typetab('tags').' on tags.tag_en = tags_translation.tag_en') : '';
        $sql['joinVideosDomain'] = $do['joinVideosDomain'] ? ('join '.F::typetab('videos').' on videos.id = tags.video_id and domain = \''.F::checkstr($this->getDomain()).'\'') : '';
        $sql['tagEn'] = $this->getTagEn() ? ('tags_translation.tag_en = \''.F::checkstr($this->getTagEn()).'\'') : 1;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS tags_translation.tag_ru
			from '.F::typetab('tags_translation').'
			'.$sql['joinTags'].'
			'.$sql['joinVideosDomain'].'
			where 
			'.$sql['tagEn'].'
			'.$sql['group'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');

        return $arr;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function getList()
    {
        if (! $this->getListTemplate()) {
            exit('Set template for list of categories first');
        }
        $arr = $this->get();
        // F::pre($arr);
        $list = '';
        foreach ($arr as $v) {
            // F::dump($v);
            $tag = new Tag($v['tag_ru']);
            // F::dump($tag);
            $t = new Template($this->getListTemplate());
            $t->v('tag_ru', $tag->getNameRu());
            $t->v('tag_en', $tag->getNameEn());
            $t->v('tag_ru_url', F::withoutSpaces($tag->getNameRu()));
            $t->v('tag_en_url', F::withoutSpaces($tag->getNameEn()));
            $t->v('tag_ru_urlencode', urlencode($tag->getNameRu()));
            if ($this->getCalcVideosRows()) {
                if (! $this->getDomain()) {
                    F::error('Domain required to calcVideosRows');
                }
                $videos = new Videos;
                $videos->setDomain($this->getDomain());
                $videos->setTag($tag->getNameRu());
                // F::dump($videos);
                $rows = $videos->getRows();
                $t->v('videosRows', $rows);
                $t->v('q_tag_ru', $rows);
                $t->v('font_size', 14);
            }
            // $t->v('class_active',($category->getId() == $this->getActiveCategory())?'class="active"':'');
            $list .= $t->get();
        }

        return $list;
    }

    public function setListTemplate($path = null)
    {
        $this->listTemplate = $path;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getWithVideos()
    {
        return $this->withVideos;
    }

    public function setWithVideos($b = null)
    {
        $this->withVideos = $b;
    }

    public function setVideoId($id = null)
    {
        $this->videoId = $id;
    }

    public function getVideoId()
    {
        return $this->videoId;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function getGroup()
    {
        return $this->group;
    }

    public function setGroup($group = null)
    {
        $this->group = $group;
    }

    public function setTagEn($tagEn = null)
    {
        $this->tagEn = $tagEn;
    }

    public function getTagEn()
    {
        return $this->tagEn;
    }

    public function getCalcVideosRows()
    {
        return $this->calcVideosRows;
    }

    public function setCalcVideosRows($b = null)
    {
        $this->calcVideosRows = $b;
    }
}
