<?php

namespace videohub\app\models;

use engine\app\models\Engine;
use engine\app\models\F;

class Article extends \engine\app\models\Article
{
    protected const TYPE = 'videohub';

    protected const STATIC_PATH = 'types/videohub/articles';

    public function __construct($id = null)
    {
        if (! $id) {
            F::error('Specify ID to create Article class');
        }
        $arr = F::query_assoc('
			select id,local_id,title,text,add_date,apply_date,domain,title_translit,booked_domain,
			views,rating,votes
			from '.F::tabname(static::TYPE, 'stories').'
			left join '.F::tabname(static::TYPE, 'stories_views').' on stories_views.story_id = stories.id
			where id = \''.F::checkstr($id).'\'
			;');
        if (! $arr) {
            F::error('Article is not exist');
        }
        $this->setId($arr['id']);
        $this->setTitle($arr['title']);
        $this->setText($arr['text']);
        // $this->setTextPreview($arr['text_preview']);
        $this->setAddDate($arr['add_date']);
        $this->setApplyDate($arr['apply_date']);
        // $this->url = $arr['url'];
        $this->setDomain($arr['domain']);
        // $this->mainImage = $arr['main_image'];
        $this->setStaticPath(Engine::getStaticPath().'/'.static::STATIC_PATH.'/'.$this->getId());
        $this->setImagesPath($this->getStaticPath().'/images');
        $this->setImagesUrl(Engine::getStaticUrl().'/'.static::STATIC_PATH.'/'.$this->getId().'/images');
        // $this->active = $arr['active']?true:false;
        // $this->chpu = $arr['chpu'];
        $this->setLocalId($arr['local_id']);
        $this->setTitleTranslit($arr['title_translit']);
        $this->setBookedDomain($arr['booked_domain']);
        $this->setViews($arr['views']);
        $this->setRating($arr['rating']);
        $this->setVotes($arr['votes']);
    }

    public function save()
    {
        parent::save();
        // F::query('
        // 	update '.F::tabname(static::TYPE,'articles').'
        // 	set
        // 	domain_movie_id = '.($this->getDomainMovieId()?'\''.F::escape_string($this->getDomainMovieId()).'\'':'null').',
        // 	acceptor = '.($this->getAcceptor()?'\''.$this->getAcceptor().'\'':'null').'
        // 	where id = \''.$this->getId().'\'
        // ');
    }

    public function getViews()
    {
        return $this->views;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function getVotes()
    {
        return $this->votes;
    }

    public function setLocalId($id = null)
    {
        $this->localId = $id;
    }

    public function setTitleTranslit($titleTranslit = null)
    {
        $this->titleTranslit = $titleTranslit;
    }

    public function setBookedDomain($domain = null)
    {
        $this->bookedDomain = $domain;
    }

    public function setViews($views = null)
    {
        $this->views = $views;
    }

    public function setRating($rating = null)
    {
        $this->rating = $rating;
    }

    public function setVotes($votes = null)
    {
        $this->votes = $votes;
    }

    public function getTitleTranslit()
    {
        return $this->titleTranslit;
    }
}
