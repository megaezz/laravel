<?php

namespace videohub\app\models;

use engine\app\models\Engine;
use engine\app\models\F;

class VideoComment
{
    private $id = null;

    private $date = null;

    private $text = null;

    private $username = null;

    private $videoId = null;

    private $ip = null;

    private $active = null;

    private $status = null;

    public function __construct($id = null)
    {
        $arr = F::query_assoc('
			select id,video_id,date,guest_name,text,status,ip,active
			from '.F::typetab('comments').'
			where id = \''.F::checkstr($id).'\'
			;');
        $this->id = $arr['id'];
        $this->videoId = $arr['video_id'];
        $this->date = $arr['date'];
        $this->username = $arr['guest_name'];
        $this->text = $arr['text'];
        $this->status = $arr['status'];
        $this->ip = $arr['ip'];
        $this->active = (int) $arr['active'] ? true : false;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date = null)
    {
        $this->date = $date;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username = null)
    {
        $this->username = $username;
    }

    public function getVideoId()
    {
        return $this->videoId;
    }

    public static function add($videoId = null)
    {
        $video = new Video($videoId);
        F::query('insert into '.F::typetab('comments').' set video_id = \''.F::checkstr($video->getId()).'\';');
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));

        return $id;
    }

    public function save()
    {
        F::query('update '.F::typetab('comments').' set 
			video_id = \''.F::checkstr($this->getVideoId()).'\', 
			guest_name = \''.F::checkstr($this->getUsername()).'\',
			text = \''.F::checkstr($this->getText()).'\',
			ip = \''.F::checkstr($this->getIp()).'\',
			date = \''.F::checkstr($this->getDate()).'\',
			status = \''.F::checkstr($this->getStatus()).'\',
			active = \''.F::checkstr($this->getActive() ? 1 : 0).'\'
			where id = \''.F::checkstr($this->getId()).'\'
			;');

        return true;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status = null)
    {
        $this->status = $status;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip = null)
    {
        $this->ip = $ip;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }
}
