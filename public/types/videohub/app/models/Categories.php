<?php

namespace videohub\app\models;

use engine\app\models\F;
use engine\app\models\Template;

// use stdClass;

class Categories
{
    private $rows = null;

    private $listTemplate = null;

    private $domain = null;

    private $domainActive = null;

    private $type = null;

    private $withVideos = null;

    private $activeCategory = null;

    private $nameUrl = null;

    private $nameTranslit = null;

    private $paginationUrlPattern = null;

    private $videoId = null;

    private $calcVideoThumb = null;

    public function get()
    {
        $sql['type'] = is_null($this->getType()) ? 1 : 'categories_groups.type=\''.F::checkstr($this->getType()).'\'';
        $sql['joinCategoriesDomains'] = '';
        $sql['nameUrl'] = $this->getNameUrl() ? ('categories_groups.name_url = \''.F::checkstr($this->getNameUrl()).'\'') : 1;
        $sql['nameTranslit'] = $this->getNameTranslit() ? ('categories_groups.name_translit = \''.F::checkstr($this->getNameTranslit()).'\'') : 1;
        if ($this->getDomain()) {
            $sql['domain'] = 'categories_domains.domain = \''.F::checkstr($this->getDomain()).'\'';
            $sql['domainActive'] = is_null($this->getDomainActive()) ? 1 : ('categories_domains.active = '.($this->getDomainActive() ? '\'1\'' : '\'0\''));
            $sql['joinCategoriesDomains'] = '
			join '.F::typetab('categories_domains').' on 
			categories_domains.category_id = categories_groups.id and
			'.$sql['domain'].' and
			'.$sql['domainActive'].'
			';
        }
        if ($this->getVideoId()) {
            $sql['joinGroupsTags'] = 'left join '.F::typetab('catgroups_tags').' on catgroups_tags.group_id = categories_groups.group_id';
            $sql['groupBy'] = 'group by categories_groups.id';
            $sql['having'] = 'having sum(if (catgroups_tags.tag_ru in (select tag_ru from '.F::typetab('tags').' where video_id = \''.$this->getVideoId().'\'),1,0)) > 0';
        } else {
            $sql['joinGroupsTags'] = '';
            $sql['groupBy'] = '';
            $sql['having'] = '';
        }
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS categories_groups.id
			from '.F::typetab('categories_groups').'
			'.$sql['joinCategoriesDomains'].'
			'.$sql['joinGroupsTags'].'
			where
			'.$sql['type'].' and
			'.$sql['nameUrl'].' and
			'.$sql['nameTranslit'].'
			'.$sql['groupBy'].'
			'.$sql['having'].'
			order by categories_groups.type, categories_groups.name
			;');
        $rowsMinus = 0;
        if ($this->getDomain() and ! is_null($this->getWithVideos())) {
            foreach ($arr as $k => $v) {
                $domainCategory = new DomainCategory($v['id'], $this->getDomain());
                $videosRows = F::cache($domainCategory, 'getVideosRows');
                if ($this->getWithVideos()) {
                    if (! $videosRows) {
                        $rowsMinus++;
                        unset($arr[$k]);
                    }
                } else {
                    if ($videosRows) {
                        $rowsMinus++;
                        unset($arr[$k]);
                    }
                }
            }
        }
        $this->rows = F::rows_without_limit() - $rowsMinus;

        // F::dump($this);
        return $arr;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function getList()
    {
        if (! $this->getListTemplate()) {
            exit('Set template for list of categories first');
        }
        $arr = $this->get();
        // pre($arr);
        $usedThumbVideos = [];
        $list = '';
        foreach ($arr as $v) {
            if ($this->getDomain()) {
                $category = new DomainCategory($v['id'], $this->getDomain());
            } else {
                $category = new Category($v['id']);
            }
            $t = new Template($this->getListTemplate());
            $t->v('name', $category->getName());
            $t->v('category_name', $category->getName());
            $t->v('id', $category->getId());
            $t->v('category_id', $category->getId());
            $t->v('nameTranslit', $category->getNameTranslit());
            $t->v('name_translit', $category->getNameTranslit());
            $t->v('category_name_translit', $category->getNameTranslit());
            $t->v('name_url', $category->getNameUrl());
            $t->v('category_name_url', $category->getNameUrl());
            if ($this->getDomain()) {
                $t->v('videosRows', F::cache($category, 'getVideosRows'));
                $t->v('rows', F::cache($category, 'getVideosRows'));
                if ($this->getCalcVideoThumb()) {
                    $video = $category->getThumbVideo($usedThumbVideos);
                    if ($video) {
                        $usedThumbVideos[] = $video->getId();
                        $t->v('videoThumb', $video->getThumb());
                        $t->v('pic_video_id', $video->getId());
                    } else {
                        $t->v('videoThumb', '');
                        $t->v('pic_video_id', $video->getId());
                    }
                }
                // $t->v('videosRows',$domainCategory->getVideosRows());
                // $t->v('rows',$domainCategory->getVideosRows());
            }
            $t->v('class_active', ($category->getId() == $this->getActiveCategory()) ? 'class="active"' : '');
            $list .= $t->get();
        }

        return $list;
    }

    public function getSelectList()
    {
        $this->setTemplate('admin/videos/categories_option');

        return $this->get();
    }

    public function setListTemplate($path = null)
    {
        $this->listTemplate = $path;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getDomainActive()
    {
        return $this->domainActive;
    }

    public function setDomainActive($b = null)
    {
        $this->domainActive = $b;
    }

    public function getWithVideos()
    {
        return $this->withVideos;
    }

    public function setWithVideos($b = null)
    {
        $this->withVideos = $b;
    }

    public function getActiveCategory()
    {
        return $this->activeCategory;
    }

    public function setActiveCategory($b = null)
    {
        $this->activeCategory = $b;
    }

    public function setNameUrl($nameUrl = null)
    {
        $this->nameUrl = $nameUrl;
    }

    public function getNameUrl()
    {
        return $this->nameUrl;
    }

    public function setNameTranslit($nameTranslit = null)
    {
        $this->nameTranslit = $nameTranslit;
    }

    public function getNameTranslit()
    {
        return $this->nameTranslit;
    }

    public function setVideoId($id = null)
    {
        $this->videoId = $id;
    }

    public function getVideoId()
    {
        return $this->videoId;
    }

    public function getCalcVideoThumb()
    {
        return $this->calcVideoThumb;
    }

    public function setCalcVideoThumb($b = null)
    {
        $this->calcVideoThumb = $b;
    }
}
