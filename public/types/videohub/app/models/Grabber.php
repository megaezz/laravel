<?php

namespace videohub\app\models;

use engine\app\models\F;

class Grabber
{
    public function __construct($args = [])
    {
        $url = empty($args['url']) ? F::error('URL is required') : $args['url'];
        $this->url = $url;
        $this->page = F::filegetcontents($url);
        // pre($this);
    }

    public static function setUrl($args = [])
    {
        $url = empty($args['url']) ? F::error('URL is required') : $args['url'];
        $parse_url = parse_url($url);
        if (isset($parse_url['host']) == 0) {
            F::error('URL wasn\'t recognized');
        }
        $site = str_replace('www.', '', $parse_url['host']);
        if ($site == 'xvideos.com') {
            $grabber = new xvideos(['url' => $url]);
        }
        if ($site == 'pornhub.com') {
            $grabber = new pornhub(['url' => $url]);
        }
        if (empty($grabber)) {
            F::error('Link wasn\'t recognized');
        }

        return $grabber;
    }
}
