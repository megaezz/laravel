<?php

namespace videohub\app\models;

use engine\app\models\F;
use engine\app\models\Paginator;
use engine\app\models\PaginatorCustom;
use engine\app\models\Template;

/*

методы:

getVideos()
getRows() - кол-во строк результата без лимита
setWithTitle(bool) - видео с заголовками/без boolean
setWithDescription(bool) - видео с описанием и без boolean
setWithDomain(bool) - с доменом или без boolean
setWithBookedDomain(bool) - с бронью или без boolean
setBookedDomain(домен) - домен брони
setPage(1) - номер страницы выдачи
setLimit(100) - лимит выдачи
setDomain(домен) - домен
setOrder(mv) - сортировка mv,mc,tr,ms,mr
setExist(bool) - видео с прямой ссылкой
setTags(массив русских тегов) - уст. теги по которым искать видео
setCategory(ID категории) - уст. категорию
setSameVideo(ID видео) - уст. видео для которого искать похожие
getOrders() - массив доступных сортировок (прим. mr=>most recent)
getPage() - возвр. номер страницы выдачи
setGroupByBooking() - уст. выдачу с группировкой по брони
getBookings() - возвр. список видео сгруппированные по брони
setID(ID) - уст. ID видео, котрое нужно найти
getID() - возвр ID видео по которому идет поиск
getDomain() - возвр домен по которому идет поиск
setExclude() - уст ID видео который исключить из поиска (для похожих)
*/

class Videos
{
    private $listTemplate = null;

    // шаблоы элементов пагинатора
    private $paginationTemplateList = null;

    private $paginationTemplateNext = null;

    private $paginationTemplatePrev = null;

    // паттерн ссылки N- (:num)
    private $paginationUrlPattern = null;

    private $paginationMaxPagesToShow = 10;

    // null - все, true - с заголовком, false - без заголовка
    private $withTitle = null;

    // null - все, true - с описанием, false - без описания
    private $withDescription = null;

    // null - все, true - с доменом, false - без домена
    private $withDomain = null;

    // null - все, true - с бронью, false - без брони
    private $withBookedDomain = null;

    private $bookedDomain = null;

    // номер страницы выдачи
    private $page = null;

    // лимит выдачи
    private $limit = null;

    private $domain = null;

    // сортировка (mr,mv,mc,tr,ms)
    private $order = null;

    // pre(var_dump($this));
    // только видео для которых есть прямая ссылка
    private $exist = null;

    // тэги по которым искать видео
    private $tags = [];

    // группировать по брони
    private $groupByBooking = null;

    // вывести видео с определенным ID
    private $id = null;

    // исключить определенный ID
    private $excludeId = null;

    private $excludeIds = [];

    // только видео с тегами звезд
    private $withStarTags = null;

    // сохраняет кол-во строк из результата getVideos
    private $rows = null;

    private $localId = null;

    private $search = null;

    private $titleTranslit = null;

    public function get()
    {
        if (! $this->getLimit()) {
            $this->setLimit(1);
        }
        if (! $this->getPage()) {
            $this->setPage(1);
        }
        $joins = [
            'videos_views' => 'left join '.F::typetab('videos_views').' on tags.video_id = videos_views.video_id',
            'tags' => 'join '.F::typetab('tags').' on videos.id = tags.video_id',
        ];
        $do['join_videos_views'] = false;
        $do['join_tags'] = false;
        $do['select_views'] = false;
        $do['select_votes'] = false;
        $do['select_rating'] = false;
        $do['select_same_tags_q'] = false;
        $do['select_rating_sko'] = false;
        $do['select_q_comments'] = false;
        $do['select_count'] = false;
        $do['group_by_booking'] = false;
        $do['select_booking'] = false;
        $do['select_video_id'] = true;
        $do['group_by_id'] = false;
        $do['join_tags_translation'] = false;
        $orders = [
            'mv' => [
                'order_by' => 'views desc',
                'joins' => ['videos_views'],
                'selects' => ['views'],
            ],
            'mr' => [
                'order_by' => 'videos.apply_date desc',
                'joins' => [],
                'selects' => [],
            ],
            'tr' => [
                'order_by' => 'rating desc, votes desc',
                'joins' => ['videos_views'],
                'selects' => ['rating'],
            ],
            'ms' => [
                'order_by' => 'same_tags_q desc, rating_sko desc',
                'joins' => ['tags', 'videos_views'],
                'selects' => ['same_tags_q', 'rating_sko'],
            ],
            'mc' => [
                'order_by' => 'q_comments desc',
                'joins' => [],
                'selects' => ['q_comments'],
            ],
            'grabber date' => [
                'order_by' => 'videos.add_date desc',
                'joins' => [],
                'selects' => [],
            ],
            'title date' => [
                'order_by' => 'videos.translate_date desc',
                'joins' => [],
                'selects' => [],
            ],
            'description date' => [
                'order_by' => 'videos.description_translate_date desc',
                'joins' => [],
                'selects' => [],
            ],
        ];
        if ($this->getOrder()) {
            if (! isset($orders[$this->getOrder()])) {
                F::error('This order is unavailable');
            }
            foreach ($orders[$this->getOrder()]['joins'] as $v) {
                $do['join_'.$v] = true;
            }
            foreach ($orders[$this->getOrder()]['selects'] as $v) {
                $do['select_'.$v] = true;
            }
        }
        if ($this->getTags()) {
            $do['join_tags'] = true;
            $do['group_by_id'] = true;
        }
        if ($this->groupByBooking) {
            $do['select_count'] = true;
            $do['group_by_booking'] = true;
            $do['select_booking'] = true;
            $do['select_video_id'] = false;
        }
        if (isset($this->withStarTags)) {
            $do['join_tags'] = true;
            $do['join_tags_translation'] = true;
            $do['group_by_id'] = true;
        }
        $sql['order'] = $this->getOrder() ? 'order by '.$orders[$this->getOrder()]['order_by'] : '';
        // $sql['withStarTags']=is_null($this->withStarTags)?1:($this->withStarTags?'tags_translation.star':'not tags_translation.star');
        // придумать как находить видео без звезд. если писать not tags_translation.star то все равно выводятся видео со свездами
        // т.к. они содержат и другие теги
        $sql['withStarTags'] = $this->withStarTags ? 'tags_translation.star' : 1;
        if ($this->getTags()) {
            $tagsQuoted = array_map(function ($v) {
                return '\''.$v.'\'';
            }, $this->getTags());
            $sql['tags'] = 'tags.tag_ru in ('.implode(', ', $tagsQuoted).')';
        } else {
            $sql['tags'] = 1;
        }
        $sql['group_by_id'] = $do['group_by_id'] ? 'group by videos.id' : '';
        $sql['group_by_booking'] = $do['group_by_booking'] ? 'group by booked_domain' : '';
        $sql['join_videos_views'] = $do['join_videos_views'] ? 'left join '.F::typetab('videos_views').' on videos.id=videos_views.video_id' : '';
        $sql['join_tags'] = $do['join_tags'] ? 'join '.F::typetab('tags').' on videos.id=tags.video_id' : '';
        $sql['join_tags_translation'] = $do['join_tags_translation'] ? 'join '.F::typetab('tags_translation').' on tags_translation.tag_en=tags.tag_en' : '';
        $sql['select_video_id'] = $do['select_video_id'] ? 'videos.id' : '';
        $sql['select_booking'] = $do['select_booking'] ? ', videos.booked_domain' : '';
        $sql['select_count'] = $do['select_count'] ? 'count(*) as q' : '';
        $sql['select_views'] = $do['select_views'] ? ', ifnull(videos_views.views,0) as views' : '';
        $sql['select_votes'] = $do['select_votes'] ? ', ifnull(votes,0) as votes' : '';
        $sql['select_rating'] = $do['select_rating'] ? ', ifnull(rating,0) as rating' : '';
        $sql['select_same_tags_q'] = $do['select_same_tags_q'] ? ', count(tags.video_id) as same_tags_q' : '';
        $sql['select_rating_sko'] = $do['select_rating_sko'] ? ', ((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko' : '';
        $sql['select_q_comments'] = $do['select_q_comments'] ? ', (select count(*) from '.F::typetab('comments').' where video_id=videos.id) as q_comments' : '';
        //
        // $sql['order']=($this->order)?('order by '.$orders[$this->order]):'';
        $sql['withTitle'] = is_null($this->withTitle) ? '1' : (($this->withTitle) ? '(videos.title_ru is not null and videos.title_ru!=\'\')' : '(videos.title_ru is null or videos.title_ru=\'\')');
        $sql['withDescription'] = is_null($this->withDescription) ? '1' : (($this->withDescription) ? '(videos.description is not null and videos.description!=\'\')' : '(videos.description is null or videos.description=\'\')');
        $sql['domain'] = is_null($this->domain) ? '1' : 'videos.domain=\''.$this->domain.'\'';
        $sql['id'] = is_null($this->id) ? '1' : 'videos.id=\''.$this->id.'\'';
        $sql['localId'] = is_null($this->getLocalId()) ? '1' : 'videos.local_id=\''.$this->getLocalId().'\'';
        // pre(var_dump($this));
        $sql['bookedDomain'] = is_null($this->bookedDomain) ? '1' : 'videos.booked_domain=\''.$this->bookedDomain.'\'';
        $sql['withDomain'] = is_null($this->withDomain) ? '1' : ($this->withDomain ? 'videos.domain is not null' : 'videos.domain is null');
        $sql['withBookedDomain'] = is_null($this->withBookedDomain) ? '1' : ($this->withBookedDomain ? 'videos.booked_domain is not null' : 'videos.booked_domain is null');
        $sql['from'] = ($this->page - 1) * $this->limit;
        $sql['exist'] = is_null($this->exist) ? '1' : ($this->exist ? 'videos.exist' : 'not videos.exist');
        $sql['excludeId'] = $this->getExcludeId() ? 'videos.id!=\''.$this->getExcludeId().'\'' : 1;
        $sql['excludeIds'] = $this->getExcludeIds() ? ('videos.id not in ('.implode(',', $this->getExcludeIds()).')') : 1;
        $sql['search'] = $this->getSearch() ? ('concat(ifnull(videos.title_ru,\'\'),ifnull(videos.description,\'\')) like \'%'.F::checkstr($this->getSearch()).'%\'') : 1;
        $sql['titleTranslit'] = $this->getTitleTranslit() ? ('videos.title_ru_translit = \''.F::checkstr($this->getTitleTranslit()).'\'') : 1;
        // не забыть использовать exist
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS
			'.$sql['select_video_id'].'
			'.$sql['select_count'].'
			'.$sql['select_booking'].'
			'.$sql['select_views'].'
			'.$sql['select_votes'].'
			'.$sql['select_rating'].'
			'.$sql['select_same_tags_q'].'
			'.$sql['select_rating_sko'].'
			'.$sql['select_q_comments'].'
			from '.F::typetab('videos').'
			'.$sql['join_videos_views'].'
			'.$sql['join_tags'].'
			'.$sql['join_tags_translation'].'
			where
			'.$sql['id'].' and
			'.$sql['localId'].' and
			'.$sql['titleTranslit'].' and
			'.$sql['withTitle'].' and
			'.$sql['withDescription'].' and
			'.$sql['withDomain'].' and
			'.$sql['withBookedDomain'].' and
			'.$sql['domain'].' and
			'.$sql['bookedDomain'].' and
			'.$sql['exist'].' and
			'.$sql['tags'].' and
			'.$sql['withStarTags'].' and
			'.$sql['excludeId'].' and
			'.$sql['excludeIds'].' and
			'.$sql['search'].'
			'.$sql['group_by_id'].'
			'.$sql['group_by_booking'].'
			'.$sql['order'].'
			limit '.$sql['from'].','.$this->limit.'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            // если не установлено свойство rows то просто выполняем getVideos
            if (! $this->getPage()) {
                $this->setPage(1);
            }
            if (! $this->getLimit()) {
                $this->setLimit(1);
            }
            $this->get();
        }

        return $this->rows;
    }

    public function getMaxPages()
    {
        if (! $this->limit) {
            F::error('Use setLimit first to use getPages');
        }

        return ceil($this->getRows() / $this->limit);
    }

    public function setWithTitle($b = null)
    {
        $this->withTitle = $b;
    }

    public function setWithDescription($b = null)
    {
        $this->withDescription = $b;
    }

    public function setWithDomain($b = null)
    {
        $this->withDomain = $b;
    }

    public function setWithBookedDomain($b = null)
    {
        $this->withBookedDomain = $b;
    }

    public function setBookedDomain($domain = null)
    {
        $this->bookedDomain = $domain;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setExist($b = null)
    {
        $this->exist = $b;
    }

    public function setTags($tags = [])
    {
        $this->tags = is_array($tags) ? $tags : F::error('setTags value should be an array');
    }

    public function setCategory($categoryID = null)
    {
        if (! $categoryID) {
            return false;
        }
        // if (empty($categoryID)) {F::error('Category ID required');};
        $category = new Category($categoryID);
        $this->setTags($category->getTags());
    }

    public function setSameVideo($videoID = null)
    {
        $video = new video($videoID);
        $this->setTags($video->getTags());
        $this->setOrder('ms');
        $this->setExcludeId($videoID);
    }

    public static function getOrders()
    {
        $arr = [
            'mr' => 'most recent',
            'mv' => 'most viewed',
            'tr' => 'top rated',
            'ms' => 'most same',
            'mc' => 'most commented',
            'grabber date' => 'by grabber date',
            'title date' => 'by title date',
            'description date' => 'by description date',
        ];

        return $arr;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setGroupByBooking($b = null)
    {
        $this->groupByBooking = $b;
    }

    public function getBookings()
    {
        $this->setGroupByBooking(true);
        $obj = $this->get();
        // foreach ($obj->array as $v) {};
        // pre($obj->array);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id = null)
    {
        $this->id = $id;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setWithStarTags($b = null)
    {
        $this->withStarTags = $b;
    }

    // обновляет local_id для установленного домена в талице videos
    public function updateLocalIds()
    {
        if (! $this->getDomain()) {
            F::error('use setDomain first to updateLocalIds');
        }
        F::updateLocalIds([
            'table' => F::typetab('videos'),
            'where_clause' => 'domain=\''.$this->getDomain().'\'',
        ]);
    }

    public function setPaginationMaxPagesToShow($v = null)
    {
        $this->paginationMaxPagesToShow = $v;
    }

    public function getPagesButtons()
    {
        $Paginator = new PaginatorCustom;
        $Paginator->setUrlPattern($this->paginationUrlPattern);
        $Paginator->setTemplateList('pagination/bootstrap');
        $Paginator->setTemplatePrev('pagination/bootstrap_prev');
        $Paginator->setTemplateNext('pagination/bootstrap_next');
        $Paginator->setRows($this->getRows());
        $Paginator->setLimit($this->getLimit());
        $Paginator->setPage($this->getPage());

        return $Paginator->get();
    }

    public function getPagesSimple()
    {
        if (! $this->paginationUrlPattern) {
            F::error('Use setPaginationUrlPattern first');
        }
        // if (!$this->urlPattern) {F::error('UrlPattern required');};
        $rows = $this->get()['rows'];
        $result = new Paginator($rows, $this->limit, $this->page, $this->paginationUrlPattern);

        /*
        $result=page_numbers_alt(array(
            'rows'=>$videos->rows,
            'per_page'=>$this->limit,
            'page'=>$this->page,
            'link'=>$this->pagesLink
        ));
        */
        return $result;
    }

    public function getList()
    {
        if (! $this->getListTemplate()) {
            F::error('listTemplate is required');
        }
        // pre(var_dump($this));
        // $getVideos = $this->getVideos();
        // $arr = $getVideos->array;
        // $rows = $getVideos->rows;
        $arr = $this->get();
        $list = '';
        foreach ($arr as $v) {
            $video = new Video($v['id']);
            $t = new Template($this->getListTemplate());
            // F::dump($t->get());
            // F::dump($t);
            $t->v('thumb', $video->getThumb());
            $t->v('id', $video->getID());
            $t->v('title', $video->getTitle());
            $t->v('title_ru', $t->getVar('title'));
            $t->v('titleEn', $video->getTitleEn());
            $t->v('titleTranslit', $video->getTitleTranslit());
            $t->v('title_ru_translit', $video->getTitleTranslit());
            $t->v('views', $video->getViews());
            $t->v('likes', $video->getLikes());
            $t->v('addDate', $video->getAddDate());
            $t->v('applyDate', $video->getApplyDate());
            $t->v('tags', $video->getRuTagsStr());
            // $t->v('player',$video->getEmbedPlayer());
            $t->v('domain', empty($video->getDomain()) ? '' : $video->getDomain());
            $t->v('bookedDomain', empty($video->getBookedDomain()) ? '' : $video->getBookedDomain());
            $t->v('hub', $video->getHub());
            $t->v('cardBg', $video->getDomain() ? 'light' : ($video->getTitle() ? 'success' : 'warning'));
            $t->v('shortDescription', $video->getShortDescription());
            $t->v('description_short', $video->getShortDescription());
            $t->v('localId', $video->getLocalId());
            $t->v('local_id', $t->getVar('localId'));
            $t->v('likes_votes', $video->getLikesSum());
            $t->v('likes_sum', $video->getLikes() - $video->getDislikes());
            if ($video->getHub() == 'xvideos.com' and $video->getPhViewkey() != 'no') {
                $xv_img_path = '/video-images-xv/videos/thumbs/'.substr($video->getPhViewkey(), 0, 2).'/'.substr($video->getPhViewkey(), 2, 2).'/'.substr($video->getPhViewkey(), 4, 2).'/'.$video->getPhViewkey().'/'.$video->getPhViewkey().'.';
                $t->v('img_scroll', 'onmouseover="startThumbSlide(\''.$video->getId().'\', \''.$xv_img_path.'\');" onmouseout="stopThumbSlide();"');
            } else {
                $t->v('img_scroll', '');
            }
            $formatter = new \IntlDateFormatter(
                'RU_RU',
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::NONE,
                'Europe/Moscow'
            );
            $t->v('date', $formatter->format(new \DateTime($video->getApplyDate())));
            $list .= $t->get();
        }

        return $list;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function setLocalId($localId = null)
    {
        $this->localId = $localId;
    }

    public function getLocalId()
    {
        return $this->localId;
    }

    public function getPages()
    {
        if (! $this->getPaginationUrlPattern()) {
            F::error('setPaginationUrlPattern first');
        }
        $Paginator = new PaginatorCustom;
        // $Paginator->setLang($this->getLang());
        $Paginator->setUrlPattern($this->getPaginationUrlPattern());
        $Paginator->setTemplateList('pagination/list');
        $Paginator->setTemplatePrev('pagination/prev');
        $Paginator->setTemplateNext('pagination/next');
        $Paginator->setRows($this->getRows());
        $Paginator->setLimit($this->getLimit());
        $Paginator->setPage($this->getPage());
        $Paginator->setMaxPagesToShow(4);

        return $Paginator->get();
        // return new Paginator($this->getRows(),$this->getLimit(),$this->getPage(),$this->getPaginationUrlPattern());
    }

    public function getPaginationUrlPattern()
    {
        return $this->paginationUrlPattern;
    }

    public function setPaginationUrlPattern($v = null)
    {
        $this->paginationUrlPattern = $v;
    }

    public function getExcludeId()
    {
        return $this->excludeId;
    }

    public function setExcludeId($b = null)
    {
        $this->excludeId = $b;
    }

    public function getExcludeIds()
    {
        return $this->excludeIds;
    }

    public function setExcludeIds($arr = null)
    {
        if (! is_array($arr)) {
            F::error('Array needed to use setExcludeIds');
        }
        $this->excludeIds = $arr;
    }

    public function addExcludeId($id = null)
    {
        $this->excludeIds[] = $id;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getSearch()
    {
        return $this->search;
    }

    public function setSearch($query = null)
    {
        $this->search = $query;
    }

    public function setTag($tag = null)
    {
        foreach ($this->tags as $v) {
            if ($v == $tag) {
                return false;
            }
        }
        $this->tags[] = $tag;

        return true;
    }

    public function setTitleTranslit($titleTranslit = null)
    {
        $this->titleTranslit = $titleTranslit;
    }

    public function getTitleTranslit()
    {
        return $this->titleTranslit;
    }
}
