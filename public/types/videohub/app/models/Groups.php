<?php

namespace videohub\app\models;

use engine\app\models\F;

class Groups
{
    public function __construct()
    {
        // $this->domain=null;
    }

    public function get()
    {
        $arr = F::query_arr('select distinct catgroups_tags.group_id
			from '.F::typetab('catgroups_tags').'
			;');
        $rows = F::rows_without_limit();
        $object = new stdClass;
        $object->rows = $rows;
        $object->array = $arr;

        return $object;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }
}
