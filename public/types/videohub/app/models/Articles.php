<?php

namespace videohub\app\models;

use engine\app\models\F;

class Articles extends \engine\app\models\Articles
{
    protected const TYPE = 'videohub';

    protected const CLASS_ARTICLE = 'videohub\app\models\Article';

    private $titleTranslit = null;

    public function get()
    {
        if (! $this->getLimit()) {
            $this->setLimit(1);
        }
        if (! $this->getPage()) {
            $this->setPage(1);
        }
        $sql['from'] = ($this->getPage() - 1) * $this->getLimit();
        $sql['domain'] = $this->getDomain() ? ('domain = \''.$this->getDomain().'\'') : 1;
        $sql['titleTranslit'] = $this->getTitleTranslit() ? ('title_translit = \''.F::checkstr($this->getTitleTranslit()).'\'') : 1;
        $sql['order'] = $this->getOrder() ? ('order by '.$this->getOrder()) : '';
        if (is_null($this->getActive())) {
            $sql['active'] = 1;
        } else {
            $sql['active'] = $this->getActive() ? 'active' : 'not active';
        }
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS id
			from '.F::tabname(static::TYPE, 'stories').'
			where
			'.$sql['domain'].' and
			'.$sql['titleTranslit'].'

			'.$sql['order'].'
			limit '.$sql['from'].','.$this->getLimit().'
			;');
        $this->setRows(F::rows_without_limit());

        return $arr;
    }

    public function setListVars($t = null, $a = null)
    {
        parent::setListVars($t, $a);
        $t->v('date_slashed', (new \DateTime($a->getApplyDate()))->format('Y/m/d'));
        $t->v('title_translit', $a->getTitleTranslit());
        $t->v('short_text', mb_substr(strip_tags($a->getText()), 0, 600));
        $t->v('views', $a->getViews());
    }

    public function setTitleTranslit($titleTranslit = null)
    {
        $this->titleTranslit = $titleTranslit;
    }

    public function getTitleTranslit()
    {
        return $this->titleTranslit;
    }
}
