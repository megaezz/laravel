<?php

// выносим в отдельный класс, т.к. иначе кэш срабатывает заново из за того что статистика изменяется постоянно

namespace videohub\app\models;

use engine\app\models\F;

class VideoStat
{
    private $id = null;

    public function __construct($id = null)
    {
        if (! $id) {
            F::error('Video ID is required to create VideoStat class');
        }
        $arr = F::query_assoc('
			select videos_views.views,videos_views.rating,videos_views.votes,
			videos_views.loads,videos_views.likes,videos_views.dislikes
			from '.F::typetab('videos_views').'
			where videos_views.video_id = \''.$id.'\'
			;');
        $this->id = $arr['id'];
        $this->views = $arr['views'];
        $this->rating = $arr['rating'];
        $this->votes = $arr['votes'];
        $this->loads = $arr['loads'];
        $this->likes = $arr['likes'];
        $this->dislikes = $arr['dislikes'];
    }
}
