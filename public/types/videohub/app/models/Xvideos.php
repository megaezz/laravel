<?php

namespace videohub\app\models;

use engine\app\models\F;

class Xvideos extends Grabber
{
    public function __construct($args = [])
    {
        parent::__construct($args);
        $this->hub = 'xvideos.com';
    }

    public function getVideos()
    {
        $boxes = explode('<div id="video_', $this->page);
        unset($boxes[0]);
        $videos = [];
        foreach ($boxes as $box) {
            $info['id_hub'] = F::copybetwen('prepareVideo(', ');</script>', $box);
            if (is_video_in_base($this->hub, $info['id_hub'])) {
                continue;
            }
            $info['img_src'] = F::copybetwen('data-src="', '" data-idcdn', $box);
            if (strstr($info['img_src'], 'THUMBNUM')) {
                $info['img_src'] = str_replace('THUMBNUM', '1', $info['img_src']);
            }
            $step1 = parse_url($info['img_src']);
            $step2 = explode('/', $step1['path']);
            if (empty($step2[6])) {
                F::error('Не найден viewkey');
            }
            $info['viewkey'] = $step2[6];
            $info['title_en'] = trim(htmlspecialchars(F::copybetwen('" title="', '">', $box)));
            // теги
            $viewpage_url = F::copybetwen('<a href="', '">', $box);
            $viewpage = F::filegetcontents('https://www.xvideos.com'.$viewpage_url);
            $info['title_en'] = trim(htmlspecialchars(F::copybetwen('<h2 class="page-title">', '<span class="', $viewpage)));
            $tags_str = trim(strip_tags(F::copybetwen('<div class="video-metadata video-tags-list ordered-label-list cropped">', '</div>', $viewpage)));
            $tags_str = strip_tags($tags_str);
            $tags_str = str_replace('+', '', $tags_str);
            $tags_str = str_replace("\n\n", ' ', $tags_str);
            $tags_str = trim($tags_str);
            $tags = explode(' ', $tags_str);
            unset($tags[count($tags) - 1]);
            foreach ($tags as $k => $v) {
                $tags[$k] = strtolower(trim($v));
                if (empty($tags[$k])) {
                    unset($tags[$k]);
                }
            }
            $info['tags'] = $tags;
            $videos[] = $info;
        }

        return $videos;
    }

    public static function getDirectLink($args = [])
    {
        $id_hub = empty($args['id_hub']) ? F::error('ID hub is required') : $args['id_hub'];
        $link = 'https://www.xvideos.com/video'.$id_hub.'/0/';
        $page = F::filegetcontents($link, 'no_error', 'iphone', 'default_interface');
        if (strpos($page, 'This video has been deleted')) {
            return false;
        }
        $mp4 = F::copybetwen('html5player.setVideoUrlHigh(\'', '\');', $page);
        if (! $mp4) {
            $mp4 = F::copybetwen('html5player.setVideoUrlLow(\'', '\');', $page);
        }
        if (! filter_var($mp4, FILTER_VALIDATE_URL)) {
            return false;
        }

        return $mp4;
    }
}
