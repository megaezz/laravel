<?php

namespace videohub\app\models;

use engine\app\models\F;

class PageComments extends \engine\app\models\Lists
{
    private $pageName = null;

    private $active = null;

    private $status = null;

    private $domain = null;

    public function __construct()
    {
        $this->setItemClass('\videohub\app\models\PageComment');
    }

    public function get()
    {
        $sql = $this->getSql();
        $orders['asc'] = 'order by page_comments.date';
        $orders['desc'] = 'order by page_comments.date desc';
        $sql['order'] = isset($orders[$this->getOrder()]) ? $orders[$this->getOrder()] : F::error('Order of pageComments was not recognized');
        $sql['active'] = is_null($this->getActive()) ? 1 : ($this->getActive() ? 'page_comments.active = \'1\'' : 'page_comments.active = \'0\'');
        $sql['status'] = is_null($this->getStatus()) ? 1 : 'page_comments.status=\''.$this->getStatus().'\'';
        $sql['pagePath'] = $this->getPagePath() ? ('page_comments.page = \''.F::checkstr($this->getPagePath()).'\'') : 1;
        $sql['domain'] = $this->getDomain() ? ('page_comments.domain = \''.F::checkstr($this->getDomain()).'\'') : 1;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS page_comments.id
			from '.F::typetab('page_comments').'
			where
			'.$sql['pagePath'].' and
			'.$sql['domain'].' and
			'.$sql['active'].' and
			'.$sql['status'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    protected function setListVars($t = null, $comment = null)
    {
        $arr_says = ['говорит', 'молвит', 'уверен, что', 'кричит', 'шепчет', 'стонет'];
        $t->v('date', $comment->getDate());
        $t->v('text', $comment->getText());
        $t->v('username', $comment->getName());
        $t->v('guest_name', $comment->getName());
        $t->v('user', $comment->getName());
        $t->v('says', $arr_says[array_rand($arr_says)]);
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status = null)
    {
        $this->status = $status;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active = null)
    {
        $this->active = $active;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getPagePath()
    {
        return $this->pagePath;
    }

    public function setPagePath($pagePath = null)
    {
        $this->pagePath = $pagePath;
    }
}
