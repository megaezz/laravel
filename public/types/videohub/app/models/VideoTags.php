<?php

namespace videohub\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class VideoTags
{
    private $tagEN = null;

    private $tagRU = null;

    private $star = null;

    private $limit = null;

    private $order = null;

    private $videoId = null;

    private $domain = null;

    private $page = null;

    private $rows = null;

    private $groupBy = null;

    private $listTemplate = null;

    public function __construct() {}

    public function get()
    {
        $sql['limit'] = '';
        if ($this->getLimit()) {
            if (! $this->getPage()) {
                $this->setPage(1);
            }
            $sql['limit'] = 'limit '.(($this->page - 1) * $this->getLimit()).','.$this->getLimit();
        }
        $sql['from'] = ($this->getPage() - 1) * $this->getLimit();
        if ($this->getVideoId()) {
            $sql['join_videos'] = true;
            $sql['videoId'] = 'videos.id = \''.$this->getVideoId().'\'';
        }
        if ($this->domain) {
            $sql['join_videos'] = true;
            $sql['domain'] = 'videos.domain = \''.$this->domain.'\'';
        }
        if ($this->order) {
            $orders = ['tagEN', 'tagRU', 'videosCount'];
            if (! in_array($this->order, $orders)) {
                F::error('Tags order ('.$this->order.') was not found');
            }
            if ($this->order == 'tagEN') {
                $sql['order'] = 'order by tags.tag_en';
            }
            if ($this->order == 'tagRU') {
                // $sql['join_tags_translation'] = true;
                $sql['order'] = 'order by tags_translation.tag_ru';
            }
            if ($this->order == 'videosCount') {
                $sql['select_videosCount'] = true;
                $sql['join_videos'] = true;
                $sql['order'] = 'order by videosCount desc';
            }
            if ($this->order == 'videosCount') {
                $sql['select_videosCount'] = true;
                $sql['join_videos'] = true;
                // $sql['join_tags_translation'] = true;
                $sql['order'] = 'order by videosCount desc';
            }
        }
        if ($this->groupBy) {
            if ($this->groupBy == 'tagEN') {
                $sql['group'] = 'group by tags.tag_en';
            }
            if ($this->groupBy == 'tagRU') {
                $sql['group'] = 'group by tags_translation.tag_ru';
            }
        }
        $sql['join_videos'] = isset($sql['join_videos']) ? 'join '.F::typetab('videos').' on videos.id = tags.video_id' : '';
        $sql['select_videosCount'] = isset($sql['select_videosCount']) ? ', count(distinct videos.id) as videosCount' : '';
        $sql['domain'] = isset($sql['domain']) ? $sql['domain'] : '1';
        $sql['videoId'] = isset($sql['videoId']) ? $sql['videoId'] : '1';
        $sql['star'] = isset($sql['star']) ? $sql['star'] : '1';
        $sql['group'] = isset($sql['group']) ? $sql['group'] : '';
        $sql['order'] = isset($sql['order']) ? $sql['order'] : '';
        $sql['limit'] = isset($sql['limit']) ? $sql['limit'] : '';
        // $sql['join_tags_translation'] = $sql['join_tags_translation']?'join '.F::tabname('videohub','tags_translation').' on tags.tag_en = tags_translation.tag_en':'';
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS tags.tag_en, tags_translation.tag_ru
			'.$sql['select_videosCount'].' 
			from '.F::typetab('tags').'
			'.$sql['join_videos'].'
			join '.F::typetab('tags_translation').' on tags.tag_en = tags_translation.tag_en
			where
			'.$sql['domain'].' and
			'.$sql['videoId'].' and
			'.$sql['star'].'
			'.$sql['group'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
        // $data=F::query('
        //     select DISTINCT tags.tag_en, tags_translation.tag_ru,count(distinct videos.id) as q_tag_ru
        //     FROM '.F::tabname('videohub','tags').'
        //     INNER JOIN '.F::tabname('videohub','tags_translation').' ON tags.tag_en = tags_translation.tag_en
        //     INNER JOIN '.F::tabname('videohub','videos').' ON videos.id = tags.video_id
        //     WHERE videos.domain=\''.$config['domain'].'\'
        //     group by tag_ru
        //     order by q_tag_ru desc
        //     ');
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setGroup($groupBy = null)
    {
        $this->groupBy = $groupBy;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function setVideoId($videoId = null)
    {
        $this->videoId = $videoId;
    }

    public function getVideoId($videoId = null)
    {
        return $this->videoId;
    }

    public function setStar($b = null)
    {
        $this->star = $b;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getRows()
    {
        if (! is_null($this->rows)) {
            return $rows;
        }
        $this->get();

        return $this->rows;
    }

    public function getList()
    {
        if (! $this->listTemplate) {
            F::error('Use setListTemplate first to VideoTags/getList');
        }
        $arr = $this->get();
        // dump($arr);
        // if (isset($arr[0]['videosCount'])) {
        // 	$maxVideosCount = 0;
        // 	// $minVideosCount = null;
        // 	foreach ($arr as $v) {
        // 		$maxVideosCount = ($v['videosCount']>$maxVideosCount)?$v['videosCount']:$maxVideosCount;
        // 		// $minVideosCount = ($v['videosCount']<$minVideosCount or is_null($minVideosCount))?$v['videosCount']:$minVideosCount;
        // 	};
        // };
        $list = '';
        foreach ($arr as $v) {
            $t = new Template($this->listTemplate);
            $t->v('tagEn', $v['tag_en']);
            $t->v('tagRu', $v['tag_ru']);
            $t->v('tag_ru', $v['tag_ru']);
            $t->v('tagRuUrl', urlencode($v['tag_ru']));
            $t->v('tag_ru_url', urlencode($v['tag_ru']));
            $t->v('comma', ',');
            $t->v('tag_ru_urlencode', urlencode($v['tag_ru']));
            if (isset($v['videosCount'])) {
                $t->v('videosCount', $v['videosCount']);
                // отношение кол-ва видео данного тега к кол-ву видео максимального тега
                // $t->v('ratio',$maxVideosCount?($v['videosCount']/$maxVideosCount):null);
            }
            $list .= $t->get();
        }

        return $list;
    }
}
