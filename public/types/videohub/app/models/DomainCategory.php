<?php

namespace videohub\app\models;

use engine\app\models\F;

class DomainCategory extends Category
{
    private $img = null;

    private $text = null;

    private $title = null;

    public $h2;

    public $active;

    public $titleTranslit;

    public $photoImg;

    public $photoText;

    public $photoH1;

    public $photoDescription;

    public $photoTitle;

    public $description;

    public $keywords;

    public $h1;

    public $text2;

    public $domain;

    public function __construct($id = null, $domain = null)
    {
        parent::__construct($id);
        $this->domain = $domain ? $domain : F::error('Domain required to create domainCategory class');
        $arr = F::query_assoc('
			select id,domain,category_id,text,text_2,title,h1,img,keywords,description,
			photo_title,photo_description,photo_h1,photo_text,photo_img,title_translit,active,h2
			from '.F::typetab('categories_domains').' 
			where category_id=\''.$this->id.'\' and domain=\''.$this->domain.'\'
			;');
        if (! $arr) {
            F::error('Category doesn\'t exist');
        }
        $this->text = $arr['text'];
        $this->text2 = $arr['text_2'];
        $this->title = $arr['title'];
        $this->h1 = $arr['h1'];
        $this->img = $arr['img'];
        $this->keywords = $arr['keywords'];
        $this->description = $arr['description'];
        $this->photoTitle = $arr['photo_title'];
        $this->photoDescription = $arr['photo_description'];
        $this->photoH1 = $arr['photo_h1'];
        $this->photoText = $arr['photo_text'];
        $this->photoImg = $arr['photo_img'];
        $this->titleTranslit = $arr['title_translit'];
        $this->active = $arr['active'];
        $this->h2 = $arr['h2'];
    }

    public function getVideosRows($b = null)
    {
        $videos = new Videos;
        $videos->setCategory($this->id);
        $videos->setDomain($this->domain);

        return $videos->getRows();
    }

    public static function getIdByDomainAndNameTranslit($domain, $nameTranslit = null)
    {
        $categories = new Categories;
        $categories->setDomain($domain);
        $categories->setNameTranslit($nameTranslit);
        $arr = $categories->get();
        if (! $arr) {
            return false;
        }

        return $arr[0]['id'];
    }

    public static function getIdByDomainAndNameUrl($domain, $nameUrl = null)
    {
        $categories = new Categories;
        $categories->setDomain($domain);
        $categories->setNameUrl($nameUrl);
        $arr = $categories->get();
        if (! $arr) {
            return false;
        }

        return $arr[0]['id'];
    }

    public function getText()
    {
        return $this->text;
    }

    public function getImg()
    {
        return $this->img;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getThumbVideo($excludeVideos = [])
    {
        $videos = new Videos;
        $videos->setDomain($this->getDomain());
        $videos->setCategory($this->getId());
        $videos->setOrder('mv');
        $videos->setExcludeIds($excludeVideos);
        $videos->setLimit(1);
        $arr = $videos->get();
        if (! $arr) {
            return null;
        }
        $video = new Video($arr[0]['id']);

        return $video;
    }
}
