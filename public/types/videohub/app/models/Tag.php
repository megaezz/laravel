<?php

namespace videohub\app\models;

use engine\app\models\F;

class Tag
{
    private $nameRu = null;

    private $nameEn = null;

    public function __construct($name_ru = null)
    {
        if (! $name_ru) {
            F::error('NameRu required to create Tag object');
        }
        $this->setNameRu($name_ru);
        $arr = F::query_assoc('
			select tag_en,tag_ru
			from '.F::typetab('tags_translation').' 
			where tag_ru = \''.$this->getNameRu().'\'
			;');
        // F::dump($arr);
        if (! $arr) {
            F::error('Tag \''.$this->getNameRu().'\' doesn\'t exist');
        }
        $this->setNameRu($arr['tag_ru']);
        $this->setNameEn($arr['tag_en']);
    }

    public function getNameRu()
    {
        return $this->nameRu;
    }

    public function setNameRu($nameRu = null)
    {
        $this->nameRu = $nameRu;
    }

    public function getNameEn()
    {
        return $this->nameEn;
    }

    public function setNameEn($nameEn = null)
    {
        $this->nameEn = $nameEn;
    }

    public static function getTagRuByTagEn($tagEn = null)
    {
        $tags = new Tags;
        $tags->setTagEn($tagEn);
        $arr = $tags->get();
        // F::dump($arr);
        if (! $arr) {
            return null;
        }

        return $arr[0]['tag_ru'];
    }
}
