<?php

namespace videohub\app\models;

use engine\app\models\F;

class Galleries extends \engine\app\models\Lists
{
    private $domain = null;

    private $titleTranslit = null;

    public function __construct($id = null)
    {
        $this->setItemClass('\videohub\app\models\Gallery');
    }

    public function get()
    {
        $sql = $this->getSql();
        $sql['domain'] = $this->getDomain() ? ('galleries.domain = \''.F::checkstr($this->getDomain()).'\'') : 1;
        $sql['titleTranslit'] = $this->getTitleTranslit() ? ('galleries.title_ru_translit = \''.F::checkstr($this->getTitleTranslit()).'\'') : 1;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS galleries.id
			from '.F::typetab('galleries').'
			where
			'.$sql['domain'].' and
			'.$sql['titleTranslit'].'

			'.$sql['order'].'
			'.$sql['limit'].'
		;');
        $this->setRows(F::rows_without_limit());

        return $arr;
    }

    protected function setListVars($t = null, $gallery = null)
    {
        $t->v('date_slashed', (new \DateTime($gallery->getApplyDate()))->format('Y/m/d'));
        $t->v('title_ru_translit', $gallery->getTitleTranslit());
        $t->v('title_ru', $gallery->getTitle());
        $t->v('id', $gallery->getId());
        $t->v('ext', $gallery->getExt());
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getTitleTranslit()
    {
        return $this->titleTranslit;
    }

    public function setTitleTranslit($titleTranslit = null)
    {
        $this->titleTranslit = $titleTranslit;
    }
}
