<?php

namespace videohub\app\models;

use engine\app\models\F;

/*
Страница видео

list() - список видео
getDomainsSelect() - список доменов под select
getBookedDomainsSelect() - список доменов брони под select
getOrdersSelect() - список сортировок под select
page() - страница

*/

class VideosPage extends VideosWriter
{
    public function __construct()
    {
        // pre(var_dump($_POST));
        parent::__construct();
        // паттерн пагинатора true т.к. не требуются ссылки
        $this->setPaginationURLPattern(true);
        $this->setPage((! empty($_POST['p']) and is_numeric($_POST['p'])) ? F::checkstr($_POST['p']) : 1);
        $this->setLimit(empty($_POST['limit']) ? 100 : F::checkstr($_POST['limit']));
        $this->setDomain(empty($_POST['domain']) ? null : F::checkstr($_POST['domain']));
        $this->setBookedDomain(empty($_POST['bookedDomain']) ? null : F::checkstr($_POST['bookedDomain']));
        $this->setOrder(empty($_POST['order']) ? null : F::checkstr($_POST['order']));
        $this->setID(empty($_POST['id']) ? null : F::checkstr($_POST['id']));
        $this->setCategory(empty($_POST['category']) ? null : F::checkstr($_POST['category']));
        if (isset($_POST['withDomain'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withDomain']])) {
                $this->setWithDomain($arr[$_POST['withDomain']]);
            }
        }
        if (isset($_POST['withBookedDomain'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withBookedDomain']])) {
                $this->setWithBookedDomain($arr[$_POST['withBookedDomain']]);
            }
        }
        if (isset($_POST['withTitle'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withTitle']])) {
                $this->setWithTitle($arr[$_POST['withTitle']]);
            }
        }
        if (isset($_POST['withDescription'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withDescription']])) {
                $this->setWithDescription($arr[$_POST['withDescription']]);
            }
        }
        if (isset($_POST['exist'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['exist']])) {
                $this->setExist($arr[$_POST['exist']]);
            }
        }
        if (isset($_POST['withStarTags'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withStarTags']])) {
                $this->setWithStarTags($arr[$_POST['withStarTags']]);
            }
        }
        // $this->setGroupByBooking(true);
        $this->listTemplate = null;
    }
}
