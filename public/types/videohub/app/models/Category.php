<?php

namespace videohub\app\models;

use engine\app\models\F;

class Category
{
    protected $id = null;

    protected $name = null;

    protected $nameTranslit = null;

    protected $groupId = null;

    protected $nameUrl = null;

    protected $type = null;

    protected $isStar = null;

    protected $isSerial = null;

    public function __construct($id = null)
    {
        $this->id = $id ? $id : F::error('ID required to create Category object');
        // $arr=cacheObject($this,'getInfo',null,0);
        $arr = F::query_assoc('
			select id,name,name_translit,group_id,name_url,star,type
			from '.F::typetab('categories_groups').' 
			where id=\''.$this->id.'\'
			;');
        if (! $arr) {
            F::error('Category doesn\'t exist');
        }
        $this->name = $arr['name'];
        $this->nameTranslit = $arr['name_translit'];
        $this->groupId = $arr['group_id'];
        $this->nameUrl = $arr['name_url'];
        $this->type = $arr['type'];
        $this->isStar = ($arr['type'] == 'star') ? true : false;
        $this->isSerial = ($arr['type'] == 'serial') ? true : false;
    }

    public function getTags()
    {
        $tags = F::query_arr('
				select catgroups_tags.tag_ru
				from '.F::typetab('categories_groups').'
				join  '.F::typetab('catgroups_tags').' on catgroups_tags.group_id = categories_groups.group_id
				where categories_groups.id=\''.$this->id.'\'
				');
        $video_tags = [];
        foreach ($tags as $v) {
            $video_tags[] = $v['tag_ru'];
        }

        return $video_tags;
    }

    public function getKeywords()
    {
        $category_name_arr = explode(' ', strtolower($this->getName()));
        $merge = array_merge_recursive($category_name_arr, $this->getTags());
        $result = array_unique($merge);
        $list = '';
        $i = 0;
        foreach ($result as $v) {
            if ($i != 0) {
                $list .= ', ';
            }
            $i = 1;
            $list .= $v;
        }

        return $list;
    }

    public function getID()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getNameTranslit()
    {
        return $this->nameTranslit;
    }

    public function getNameUrl()
    {
        return $this->nameUrl ?? F::withoutSpaces($this->getName());
    }

    public function getType()
    {
        return $this->type;
    }

    public function getGroupId()
    {
        return $this->groupId;
    }
}
