<?php

namespace videohub\app\models;

use engine\app\models\Domains;
use engine\app\models\F;

class Videohub
{
    private $videosLimit = null;

    private $sameVideosLimit = null;

    private $videosPerUpdate = null;

    private $pagesLength = null;

    private $videokeyCurrent = null;

    private $videokeyLast = null;

    public function __construct()
    {
        $arr = F::query_assoc('
			select videosLimit, sameVideosLimit, videosPerUpdate, pagesLength
			from '.F::typetab('default_config').'
			limit 1
			');
        $this->videosLimit = $arr['videosLimit'];
        $this->sameVideosLimit = $arr['sameVideosLimit'];
        $this->videosPerUpdate = $arr['videosPerUpdate'];
        $this->pagesLength = $arr['pagesLength'];
        $arr2 = F::query_arr('
			select property,value
			from '.F::typetab('config').';
			', 'property');
        $this->videokeyCurrent = $arr2['videokey_current']['value'];
        $this->videokeyLast = $arr2['videokey_last']['value'];
    }

    public function getDomains()
    {
        $domains = new Domains;
        $domains->setType('videohub');

        return $domains->get();
    }

    public function getVideosLimit()
    {
        return $this->videosLimit;
    }

    public function getSameVideosLimit()
    {
        return $this->sameVideosLimit;
    }

    public function getVideosPerUpdate()
    {
        return $this->videosPerUpdate;
    }

    public function getPagesLength()
    {
        return $this->pagesLength;
    }

    public function getVideokeyCurrent()
    {
        return $this->videokeyCurrent;
    }

    public function getVideokeyLast()
    {
        return $this->videokeyLast;
    }
}
