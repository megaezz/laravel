<?php

namespace videohub\app\models;

use engine\app\models\Engine;
use engine\app\models\F;

class PageComment
{
    private $id = null;

    private $date = null;

    private $text = null;

    private $name = null;

    private $domain = null;

    private $ip = null;

    private $active = null;

    private $pagePath = null;

    private $marked = null;

    private $status = null;

    public function __construct($id = null)
    {
        $arr = F::query_assoc('
			select id,date,text,guest_name,domain,ip,active,page,marked,status
			from '.F::typetab('page_comments').'
			where id = \''.F::checkstr($id).'\'
			;');
        $this->id = $arr['id'];
        $this->setDate($arr['date']);
        $this->setText($arr['text']);
        $this->setName($arr['guest_name']);
        $this->setDomain($arr['domain']);
        $this->setIp($arr['ip']);
        $this->setActive((int) $arr['active'] ? true : false);
        $this->setPagePath($arr['page']);
        $this->setMarked((int) $arr['marked'] ? true : false);
        $this->setStatus($arr['status']);
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date = null)
    {
        $this->date = $date;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name = null)
    {
        $this->name = $name;
    }

    public static function add($domain = null, $page = null)
    {
        if (! $domain) {
            F::error('Domain required to use PageComment::add()');
        }
        if (! $page) {
            F::error('Page required to use PageComment::add()');
        }
        F::query('
			insert into '.F::typetab('page_comments').' set 
			domain = \''.F::checkstr($domain).'\',
			page = \''.F::checkstr($page).'\'
			;');
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));

        return $id;
    }

    public function save()
    {
        F::query('update '.F::typetab('page_comments').' set 
			guest_name = \''.F::checkstr($this->getName()).'\',
			text = \''.F::checkstr($this->getText()).'\',
			ip = \''.F::checkstr($this->getIp()).'\',
			date = \''.F::checkstr($this->getDate()).'\',
			domain = \''.F::checkstr($this->getDomain()).'\',
			status = \''.F::checkstr($this->getStatus()).'\',
			active = \''.F::checkstr($this->getActive() ? 1 : 0).'\',
			marked = \''.F::checkstr($this->getMarked() ? 1 : 0).'\',
			page = \''.F::checkstr($this->getPagePath()).'\'
			where id = \''.F::checkstr($this->getId()).'\'
			;');

        return true;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status = null)
    {
        $this->status = $status;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip = null)
    {
        $this->ip = $ip;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }

    public function getMarked()
    {
        return $this->marked;
    }

    public function setMarked($b = null)
    {
        $this->marked = $b;
    }

    public function getPagePath()
    {
        return $this->pagePath;
    }

    public function setPagePath($page = null)
    {
        $this->pagePath = $page;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }
}
