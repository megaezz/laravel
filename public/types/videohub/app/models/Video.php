<?php

namespace videohub\app\models;

use engine\app\models\F;
use engine\app\models\Template;

/*

getInfo() - заполняет объект данными о видео из базы
checkHub() - проверяет существует ли хаб указанный в объекте (не понятно зачем нужно, проверить где используется)
getCategories() - возвр категории видео
getCategoriesCached() - кэш
getStars() - возвращает звезд видео
getStarsCached() - кэш
getTags() - возвр список тегов для видео
getTagsCached() - кэш
getSame(array()) - возвращает список похожих видео
getSameCached($args=array()) - кэш
setVar(поле,значение) - менчет значение в таблице
addTag($tag_en) - добавляет тэг для видео в таблицу
setThumb($url) - устанавлиает тумбу для видео
addVideosFromGrabber($grabber) - записывает данные переданные граббером
getDirectLink() - получает прямую ссылку
getDirectLinkCached() - кэш
getDirectLinkSame() - получает прямую ссылку, если ее нет, то похожее видео из 10
getDirectLinkSameCached() - кэш
checkConfigDomain() - проверяет относится ли видео к домену из конфига
getObjectByDomainAndTitleTranslit($domain=null,$titleTranslit=null) - получает ID видео по домену и транслиту
setID($id=null) - уст. ID видео в объекте
setExist($b=null) - уст. в базе значение exist

*/

class Video
{
    private $tags = null;

    private $tagsListTemplate = null;

    private $thumb = null;

    public $dislikes;

    public $likes;

    public $loads;

    public $votes;

    public $rating;

    public $views;

    public $deleted_from_hub;

    public $local_id;

    public $source_url;

    public $is_deleted;

    public $bookedDomain;

    public $ph_date;

    public $ph_viewkey;

    public $description_date;

    public $description_writer;

    public $description;

    public $title_date;

    public $title_writer;

    public $title_translit;

    public $title_alt;

    public $title;

    public $title_ru;

    public $title_en;

    public $apply_date;

    public $add_date;

    public $id_hub;

    public $hub;

    public $domain;

    public $id;

    public function __construct($id = null)
    {
        if ($id) {
            $this->setID($id);
        }
        if (empty($this->id)) {
            F::error('ID required to create video class');
        }
        $this->thumb = '/static/types/videohub/images/'.$this->getId().'.jpg';
        // $arr=cache($this,'getInfo',null,0);
        $arr = $this->getInfo();
        if (! $arr) {
            F::error('Video doesn\'t exist');
        }
        $this->id = $arr['id'];
        $this->domain = $arr['domain'];
        $this->hub = $arr['hub'];
        $this->checkHub();
        $this->id_hub = $arr['id_hub'];
        $this->add_date = $arr['add_date'];
        $this->apply_date = $arr['apply_date'];
        $this->title_en = $arr['title_en'];
        $this->title_ru = $arr['title_ru'];
        $this->title = $arr['title_ru'];
        $this->title_alt = $arr['title_ru_alt'];
        $this->title_translit = $arr['title_ru_translit'];
        $this->title_writer = $arr['translater'];
        $this->title_date = $arr['translate_date'];
        $this->description = $arr['description'];
        $this->description_writer = $arr['description_translater'];
        $this->description_date = $arr['description_translate_date'];
        $this->ph_viewkey = $arr['ph_viewkey'];
        $this->ph_date = $arr['ph_date'];
        $this->bookedDomain = $arr['booked_domain'];
        $this->is_deleted = $arr['deleted'];
        $this->source_url = $arr['source_url'];
        $this->local_id = $arr['local_id'];
        $this->deleted_from_hub = $arr['deleted_from_hub'];
        $this->views = $arr['views'];
        $this->rating = $arr['rating'];
        $this->votes = $arr['votes'];
        $this->loads = $arr['loads'];
        $this->likes = $arr['likes'];
        $this->dislikes = $arr['dislikes'];
        // $this-> = $arr[''];

    }

    public function getInfo()
    {
        $arr = F::query_assoc('select videos.id,videos.domain,videos.hub,videos.id_hub,videos.add_date,videos.apply_date,videos.title_en,
			videos.title_ru,videos.title_ru_alt,videos.title_ru_translit,videos.translater,videos.translate_date,videos.description,
			videos.description_translater,videos.description_translate_date,videos.ph_viewkey,videos.ph_date,videos.booked_domain,
			videos.deleted,videos.source_url,videos.local_id,videos.deleted_from_hub,
			videos_views.views,videos_views.rating,videos_views.votes,videos_views.loads,videos_views.likes,videos_views.dislikes
		 from '.F::typetab('videos').'
		 left join '.F::typetab('videos_views').' on videos_views.video_id = videos.id
		 where id=\''.$this->id.'\';');

        return $arr;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getTitleComma()
    {
        $clean = str_replace([',', '?', '!', '.'], '', strtolower($this->getTitle()));
        $commas = str_replace(' ', ', ', $clean);

        return $commas;
    }

    public function checkHub()
    {
        if (! in_array($this->hub, ['pornhub.com', 'xvideos.com', 'hardsextube.com', 'paradisehill.tv'])) {
            F::error('Video #'.$this->id.'. Hub '.$this->hub.' does not exist.');
        }
    }

    public function getCategories()
    {
        return preparing_categories_of_video(['video_id' => $this->id, 'stars' => 0]);
    }

    public function getCategoriesCached()
    {
        return F::cache($this, 'getCategories');
    }

    public function getStars()
    {
        return preparing_categories_of_video(['video_id' => $this->id, 'stars' => 1]);
    }

    public function getStarsCached()
    {
        return F::cache($this, 'getStars');
    }

    public function getTags()
    {
        if (! is_null($this->tags)) {
            return $this->tags;
        }
        // $tags=preparing_video_tags_list_array(array('id'=>$this->id));
        // if (empty($args['id'])) {F::error('Не передан ID видео');};
        $arr = F::query_arr('
			select tag_ru
			FROM '.F::typetab('tags').'
			WHERE tags.video_id = \''.$this->id.'\' and tag_ru is not null
			group by tag_ru
			;');
        $video_tags = [];
        foreach ($arr as $v) {
            $video_tags[] = $v['tag_ru'];
        }
        $this->tags = $video_tags;

        return $this->tags;
    }

    public function setTagsListTemplate($template = null)
    {
        $this->tagsListTemplate = $template;
    }

    public function getTagsList()
    {
        if (! $this->tagsListTemplate) {
            F::error('Use setTagsListTemplate first to use getTagsList');
        }
        $arr = [];
        foreach ($this->getTags() as $v) {
            $t = new Template($this->tagsListTemplate);
            $t->v('tag', $v);
            $t->v('tagURL', urlencode($v));
            $arr[] = $t->get();
        }

        return implode(', ', $arr);
    }

    public function getViews()
    {
        return $this->views;
    }

    public function getLoads()
    {
        return $this->loads;
    }

    public function viewsCounter()
    {
        $this->views++;
        F::query('insert into '.F::typetab('videos_views').' 
			set video_id=\''.$this->id.'\', views=1 
			on duplicate key update views=\''.$this->views.'\'
			;');
    }

    public function loadsCounter()
    {
        $this->loads++;
        F::query('insert into '.F::typetab('videos_views').' 
			set video_id=\''.$this->id.'\', loads=1 
			on duplicate key update loads=\''.$this->loads.'\'
			;');
    }

    public function likeCounter()
    {
        $this->likes++;
        F::query('insert into '.F::typetab('videos_views').' 
			set video_id=\''.$this->id.'\', likes=1 
			on duplicate key update likes=\''.$this->likes.'\'
			;');
    }

    public function dislikeCounter()
    {
        $this->dislikes++;
        F::query('insert into '.F::typetab('videos_views').' 
			set video_id=\''.$this->id.'\', dislikes=1 
			on duplicate key update dislikes=\''.$this->dislikes.'\'
			;');
    }

    public function addRating($rating = null)
    {
        if ($rating < 1 or $rating > 5 or ! is_int($rating)) {
            F::error('Rating should be integer in a range from 1 to 5');
        }
        $this->votes++;
        $this->rating = ($this->rating * $this->votes) / ($this->votes + 1);
        F::query('insert into '.F::typetab('videos_views').' 
			set video_id=\''.$this->id.'\', rating=\''.$rating.'\', votes=1
			on duplicate key update rating=\''.$this->rating.'\', votes=\''.$this->votes.'\'
			;');
    }

    public function getID()
    {
        return $this->id;
    }

    public function getLikes()
    {
        return $this->likes;
    }

    public function getDislikes()
    {
        return $this->dislikes;
    }

    public function getLikesSum()
    {
        return $this->likes - $this->dislikes;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function getTagsComma()
    {
        return implode(', ', $this->getTags());
    }

    public function getApplyDate()
    {
        return $this->apply_date;
    }

    public function getTagsCached()
    {
        return F::cache($this, 'getTags');
    }

    public function getSame($args = [])
    {
        $limit = empty($args['limit']) ? 10 : $args['limit'];
        $page = empty($args['page']) ? 1 : $args['page'];
        $videos = new Videos;
        $videos->setDomain($this->domain);
        $videos->setPage($page);
        $videos->setLimit($limit);
        $videos->setExist(true);
        $videos->setSameVideo($this->id);

        return $videos->get();
    }

    public function getSameCached($args = [])
    {
        return F::cache($this, 'getSame', $args, 2592000);
    }

    public function setVar($field, $value)
    {
        $sql_value = is_null($value) ? 'null' : '\''.$value.'\'';
        F::query('update '.F::typetab('videos').' set '.$field.'='.$sql_value.' where id=\''.$this->id.'\';');

        return true;
    }

    public function addTag($tag_en)
    {
        F::query('insert '.F::typetab('tags').' set tag_en=\''.$tag_en.'\', video_id=\''.$this->id.'\';');

        return true;
    }

    public function setThumb($url)
    {
        global $config;
        copytoserv($url, $config['videohub']['path']['images'].$this->id.'.jpg');
    }

    public static function addVideosFromGrabber($grabber)
    {
        // pre($args);
        $hub = empty($grabber->hub) ? F::error('Hub is required') : $grabber->hub;
        $source_url = empty($grabber->url) ? F::error('Source url is required') : $grabber->url;
        // pre($grabber->getVideos());
        foreach ($grabber->get() as $args) {
            $tags = empty($args['tags']) ? F::error('Tags are required') : $args['tags'];
            $img_src = empty($args['img_src']) ? F::error('IMG src is required') : $args['img_src'];
            $id_hub = empty($args['id_hub']) ? F::error('Hub ID is required') : $args['id_hub'];
            $title_en = empty($args['title_en']) ? F::error('Title EN is required') : $args['title_en'];
            $booked_domain = empty($args['booked_domain']) ? null : $args['booked_domain'];
            $viewkey = empty($args['viewkey']) ? null : $args['viewkey'];
            $ph_date = empty($args['ph_date']) ? null : $args['ph_date'];
            F::query('insert into '.F::typetab('videos').' () values ()');
            $video_id = mysql_insert_id();
            $video = new video(['id' => $video_id]);
            $video->setVar('hub', $hub);
            $video->setVar('id_hub', $id_hub);
            $video->setVar('title_en', F::checkstr($title_en));
            $video->setVar('source_url', $source_url);
            $video->setVar('booked_domain', $booked_domain);
            $video->setVar('ph_viewkey', $viewkey);
            $mysql_tags_values = '';
            foreach ($tags as $tag) {
                $video->addTag(F::checkstr($tag));
            }
            $video->setThumb($img_src);
        }
    }

    public function getDirectLink()
    {
        $link = false;
        if ($this->hub == 'pornhub.com') {
            $link = Pornhub::getDirectLink(['ph_viewkey' => $this->ph_viewkey]);
        }
        if ($this->hub == 'xvideos.com') {
            $link = Xvideos::getDirectLink(['id_hub' => $this->id_hub]);
        }
        // if ($this->hub=='hardsextube.com') {$link=hardsextube::getDirectLink(array('id_hub'=>$this->id_hub));};
        $this->setExist($link ? true : false);

        return $link;
    }

    public function getDirectLinkCached()
    {
        return F::cache($this, 'getDirectLink', null, 1200);
    }

    public function getDirectLinkSame()
    {
        $link = $this->getDirectLinkCached();
        if ($link) {
            return $link;
        }
        // pre(var_dump($link));
        // $video=new videoCached($this->id);
        // $same_arr=$video->getSame(array('sort'=>'ms','limit'=>10))->array;
        // $same_arr=$this->getSame(array('sort'=>'ms','limit'=>10))->array;
        $same_arr = $this->getSameCached(['sort' => 'ms', 'limit' => 5]);
        // pre($same_arr);
        foreach ($same_arr as $v) {
            $video = new video($v['id']);
            $link = $video->getDirectLinkCached();
            if ($link) {
                return $link;
            }
        }

        // pre(var_dump($link));
        return $link;
    }

    public function getDirectLinkSameCached()
    {
        return F::cache($this, 'getDirectLinkSame', null, 1200);
    }

    public function checkConfigDomain()
    {
        global $config;
        if ($this->domain != $config['domain']) {
            F::error('Wrong domain');
        }
    }

    // function setIDbyTitleTranslit($translit = null) {
    // 	if (!$translit) {F::error('Specify title translit for setIDbyTitleTranslit');};
    // 	if (!$this->domain) {F::error('Use setDomain first to setIDbyTitleTranslit');};
    // 	$arr=F::query_assoc('select id from '.F::typetab('videos').'
    // 		where domain=\''.$domain.'\' and title_ru_translit=\''.$titleTranslit.'\'
    // 		;');
    // 	if (!$arr) {F::error('Video was not found');};
    // 	$this->setID($arr['id']);
    // }

    public static function getIDbyDomainAndTitleTranslit($domain = null, $titleTranslit = null)
    {
        if (empty($domain)) {
            F::error('Specify domain');
        }
        if (empty($titleTranslit)) {
            F::error('Specify titleTranslit');
        }
        $arr = F::query_assoc('select id from '.F::typetab('videos').' 
			where domain=\''.$domain.'\' and title_ru_translit=\''.$titleTranslit.'\'
			;');
        if (! $arr) {
            F::error('Video was not found');
        }

        // return new self($arr['id']);
        return $arr['id'];
    }

    public function getTitleTranslit()
    {
        return $this->title_translit;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getThumb()
    {
        return $this->thumb;
    }

    public function getTitleEn()
    {
        return $this->title_en;
    }

    public function getAddDate()
    {
        return $this->add_date;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getBookedDomain()
    {
        return $this->bookedDomain;
    }

    public function getHub()
    {
        return $this->hub;
    }

    public function getShortDescription()
    {
        $short = mb_substr(($this->description ?? ''), 0, 100);
        $short = ($short == $this->description) ? $this->description : ($short.'&hellip;');

        return $short;
    }

    public function setID($id = null)
    {
        $this->id = $id;
    }

    public function setExist($b = null)
    {
        $this->setVar('exist', $b ? 1 : 0);
    }

    public function setApplyDate()
    {
        F::query('update '.F::typetab('videos').' set apply_date=current_timestamp where id=\''.$this->id.'\';');
    }

    public function setDomain($domain = null)
    {
        // если домен установлен и передан домен не null то не выполняем задачу, для предотвращения случайной смены домена
        if ($this->domain and $domain) {
            return false;
        }
        // if (!$domain) {die('Domain is required');};
        // обнуляем local_id
        $this->setVar('local_id', null);
        // устанавливаем дату добавления на сайт
        $this->setApplyDate();
        // меняем домен
        $this->setVar('domain', $domain);
        // обновляем local_id если домен не null
        if ($domain) {
            $videos = new videos;
            $videos->setDomain($domain);
            $videos->updateLocalIds();
        }

        return true;
    }

    public function setBookedDomain($domain = null)
    {
        $this->setVar('booked_domain', $domain);
    }

    public function redirectToDirectLink()
    {
        // $this->checkConfigDomain();
        $link = $this->getDirectLinkSameCached();
        if (! $link) {
            return 'Unable to get direct link';
        }
        header('Location: '.$link);
        exit();
    }

    public function getLocalId()
    {
        return $this->local_id;
    }

    public function getPlayer()
    {
        if ($this->getHub() == 'pornhub.com') {
            return $this->getEmbedPlayer();
        }
        if ($this->getHub() == 'xvideos.com') {
            return $this->getInternalPlayer();
        }

        return $this->getInternalPlayer();
    }

    public function getEmbedPlayer()
    {
        $t = new Template('player/'.$this->getHub().'/iframe.htm');
        $t->v('id_hub', $this->getIdHub());
        $t->v('ph_viewkey', $this->getPhViewkey());

        return $t->get();
    }

    public function getInternalPlayer()
    {
        $t = new Template('player/videojs.com/code.htm');
        $t->v('video_id', $this->getId());
        $t->v('thumb', $this->getThumb());

        return $t->get();
    }

    public function getIdHub()
    {
        return $this->id_hub;
    }

    public function getPhViewkey()
    {
        return $this->ph_viewkey;
    }

    public function getRuTagsStr()
    {
        $arr = $this->getTags();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<li>'.$v.'</li>';
        }

        return $list;
    }

    public function getLikesPercent()
    {
        $sum = $this->getLikes() + $this->getDislikes();
        if ($sum == 0) {
            return 0;
        }
        $percent = ceil(($this->getLikes() / ($sum)) * 100);

        return $percent;
    }

    public function getDislikesPercent()
    {
        $sum = $this->getLikes() + $this->getDislikes();
        if ($sum == 0) {
            return 0;
        }
        $percent = ceil(($this->getDislikes() / ($sum)) * 100);

        return $percent;
    }

    public function getVideoSaveLink()
    {
        $t = new Template('li/video_save_link.htm');
        $videohub = new Videohub;
        $t->v('videokey', $videohub->getVideokeyCurrent());
        $t->v('local_id', $this->getLocalId());
        $t->v('id', $this->getId());
        $t->v('title_ru_translit', $this->getTitleTranslit());

        return $t->get();
    }
}
