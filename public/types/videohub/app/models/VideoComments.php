<?php

namespace videohub\app\models;

use engine\app\models\F;
use engine\app\models\Template;

class VideoComments
{
    private $videoId = null;

    private $page = null;

    private $limit = null;

    private $order = 'asc';

    private $active = null;

    private $status = null;

    private $listTemplate = null;

    private $rows = null;

    private $domain = null;

    public function __construct($videoId = null)
    {
        $this->setVideoId($videoId);
    }

    public function get()
    {
        $orders['asc'] = 'order by comments.date';
        $orders['desc'] = 'order by comments.date desc';
        $sql['order'] = isset($orders[$this->order]) ? $orders[$this->order] : F::error('Order of videoComments was not recognized');
        $sql['active'] = is_null($this->active) ? '1' : ($this->active ? 'comments.active=\'1\'' : 'comments.active=\'0\'');
        $sql['status'] = is_null($this->status) ? '1' : 'comments.status=\''.$this->status.'\'';
        $sql['videoId'] = $this->getVideoId() ? ('comments.video_id = \''.F::checkstr($this->getVideoId()).'\'') : 1;
        if ($this->getDomain()) {
            $sql['domain'] = 'videos.domain = \''.F::checkstr($this->getDomain()).'\'';
            $sql['joinVideos'] = 'join '.F::typetab('videos').' on videos.id = comments.video_id';
        } else {
            $sql['domain'] = 1;
            $sql['joinVideos'] = '';
        }
        if ($this->getLimit()) {
            if (! $this->getPage()) {
                $this->setPage(1);
            }
            $from = ($this->getPage() - 1) * $this->getLimit();
            $sql['limit'] = 'limit '.$from.','.$this->getLimit();
        } else {
            $sql['limit'] = '';
        }
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS comments.id
			from '.F::typetab('comments').'
			'.$sql['joinVideos'].'
			where
			'.$sql['videoId'].' and
			'.$sql['domain'].' and
			'.$sql['active'].' and
			'.$sql['status'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getList()
    {
        if (! $this->listTemplate) {
            F::error('Use setListTemplate first to use VideoComments/getList');
        }
        $arr_says = ['говорит', 'молвит', 'уверен, что', 'кричит', 'шепчет', 'стонет'];
        $arr = $this->get();
        $list = '';
        foreach ($arr as $v) {
            $comment = new VideoComment($v['id']);
            $t = new Template($this->listTemplate);
            $t->v('videoId', $comment->getVideoId());
            $t->v('date', $comment->getDate());
            $t->v('text', $comment->getText());
            $t->v('username', $comment->getUsername());
            $t->v('user', $comment->getUsername());
            $t->v('says', $arr_says[array_rand($arr_says)]);
            $list .= $t->get();
        }

        return $list;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setStatus($status = null)
    {
        $this->status = $status;
    }

    public function setActive($active = null)
    {
        $this->active = $active;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getVideoId()
    {
        return $this->videoId;
    }

    public function setVideoId($videoId = null)
    {
        $this->videoId = $videoId;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getLimit()
    {
        return $this->limit;
    }
}
