<?php

namespace videohub\app\models;

use engine\app\models\F;

class Star extends Category
{
    private $name_en = null;

    private $name_ru = null;

    private $aka_en = null;

    private $gender_en = null;

    private $age = null;

    private $country_en = null;

    private $region_en = null;

    private $city_en = null;

    private $ethnicity_en = null;

    private $height = null;

    private $weight = null;

    private $hair_en = null;

    public function __construct($id = null)
    {
        parent::__construct($id);
        $arr = F::query_assoc('
		select stars.id,stars.name,stars.name_ru,stars.aka,stars.pic,stars.age,stars.country,stars.region,
		stars.city,stars.height,stars.weight,stars.gender as gender_en,stars.ethnicity as ethnicity_en,
		stars.hair as hair_en,stars.country_en,stars.region_en,stars.city_en,
		case stars.gender when \'woman\' then \'женский\' end as gender,
		case stars.ethnicity
		when \'white\' then \'белокожая\'
		when \'indian\' then \'индианка\'
		when \'asian\' then \'азиатка\'
		when \'mixed\' then \'смешанная\'
		when \'latina\' then \'латина\'
		when \'black\' then \'темнокожая\'
		when \'arab\' then \'арабка\'
		end as ethnicity,
		case stars.hair
		when \'brown\' then \'коричневые\'
		when \'black\' then \'черные\'
		when \'blonde\' then \'светлые\'
		when \'red\' then \'рыжие\'
		when \'white\' then \'белые\'
		end as hair
		from '.F::typetab('stars').'
		where group_id = \''.F::checkstr($this->getGroupId()).'\'
		;');
        if (! $arr) {
            F::error('Star not found');
        }
        $this->name_en = $arr['name'];
        $this->name_ru = $arr['name_ru'];
        $this->aka_en = $arr['aka'];
        $this->gender_en = $arr['gender_en'];
        $this->age = $arr['age'];
        $this->country_en = $arr['country_en'];
        $this->region_en = $arr['region_en'];
        $this->city_en = $arr['city_en'];
        $this->ethnicity_en = $arr['ethnicity_en'];
        $this->height = $arr['height'];
        $this->weight = $arr['weight'];
        $this->hair_en = $arr['hair_en'];
    }

    public function getNameEn()
    {
        return $this->name_en;
    }

    public function getNameRu()
    {
        return $this->name_ru;
    }

    public function getAkaEn()
    {
        return $this->aka_en;
    }

    public function getGenderEn()
    {
        return $this->gender_en;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getCountryEn()
    {
        return $this->country_en;
    }

    public function getRegionEn()
    {
        return $this->region_en;
    }

    public function getCityEn()
    {
        return $this->city_en;
    }

    public function getEthnicityEn()
    {
        return $this->ethnicity_en;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getHairEn()
    {
        return $this->hair_en;
    }
}
