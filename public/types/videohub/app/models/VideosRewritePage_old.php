<?php

namespace videohub\app\models;

class videosRewritePage extends VideosPage
{
    public function __construct()
    {
        $this->setPage(empty($_GET['p']) ? 1 : F::checkstr($_GET['p']));
        $this->setBookedDomain(empty($_GET['domain']) ? null : F::checkstr($_GET['domain']));
        // pre('test');
    }

    public function loadConfig()
    {
        // if (empty($this->BookedDomain)) {F::error('Set Booked Domain First');};
        $this->perPage = value_from_config('videohub', 'translate_titles_per_page');
        $this->videoTitleAlt = value_from_config_for_domain('videohub', 'video_title_alt', $this->bookedDomain);
        $this->titleLimit = value_from_config_for_domain('videohub', 'video_title_limit', $this->bookedDomain);
        $this->titleAltLimit = value_from_config_for_domain('videohub', 'video_title_alt_limit', $this->bookedDomain);
        $this->descriptionLimit = value_from_config_for_domain('videohub', 'video_description_limit', $this->bookedDomain);
        $this->translateToEng = value_from_config_for_domain('videohub', 'translate_to_eng', $this->bookedDomain);
        if ($this->translateToEng == '1') {
            authorizate_videohub_translater_eng();
        }
    }

    public function list()
    {
        if (empty($this->pageNum)) {
            F::error('Set page number first');
        }
        if (empty($this->bookedDomain)) {
            return;
        }
        $this->loadConfig();
        $from = ($this->pageNum - 1) * $this->perPage;
        $videos = new videos;
        $videos->setBookedDomain($this->bookedDomain);
        $videos->setPage($this->pageNum);
        $videos->setLimit($this->perPage);
        $videos->setWithTitle(false);
        $videos->setWithDescription(false);
        // $videos->setDomain($this->domain);
        $videos->setOrder('videos.add_date desc');
        $object = $videos->getVideos();
        $arr = $object->array;
        $rows = $object->rows;
        $list = '';
        $q = 0;
        foreach ($arr as $k => $v) {
            $video = new video($v['id']);
            temp_var('video_id', $video->id);
            temp_var('title_alt_limit', $this->titleAltLimit);
            $code_title_ru_alt = ($this->videoTitleAlt == 1) ? template('admin/translate/titles_descriptions/code_title_ru_alt') : '';
            clear_temp_vars();
            temp_var('code_title_alt', $code_title_ru_alt);
            temp_var('video_tags', video_tags_list_simple($video->id));
            temp_var('title_limit', $this->titleLimit);
            temp_var('description_limit', $this->descriptionLimit);
            temp_var('video_id', $video->id);
            temp_var('img_scroll', img_scroll($v));
            temp_var('title_en', $video->title_en);
            $list .= template('admin/translate/titles_descriptions/list');
            clear_temp_vars();
        }
        temp_var('page_numbers', $videos->getPages(['link' => '/?page=admin/translate/titles_descriptions&amp;domain='.$videos->bookedDomain.'&amp;p=']));
        temp_var('rows', $rows);
        temp_var('list', $list);
        temp_var('make_title_ru_alt', ($this->videoTitleAlt == 1) ? template('admin/translate/titles_description/make_title_ru_alt') : '');

        return template('admin/translate/titles_descriptions/block');
    }

    public function setPage($pageNum)
    {
        // $this->pageNum=empty($pageNum)?F::error('Page number is empty'):$pageNum;
        $this->pageNum = $pageNum;
    }

    public function setBookedDomain($domain)
    {
        // $this->BookedDomain=empty($domain)?F::error('Booked domain is empty'):$domain;
        $this->bookedDomain = $domain;
    }
}
