<?php

namespace videohub\app\models;

use engine\app\models\Domain as EngineDomain;
use engine\app\models\F;
use engine\app\models\Template;

class Domain extends EngineDomain
{
    // private $domain = null;
    // private static $configs = null;
    private $videosLimit = null;

    private $sameVideosLimit = null;

    private $videosPerUpdate = null;

    private $pagesLength = null;

    private $starVideosPagination = null;

    private $categoryVideosPagination = null;

    private $searchVideosPagination = null;

    private $tagVideosPagination = null;

    private $videosPagination = null;

    private $videoLoadLinkTemplate = null;

    private $videoWatchLinkTemplate = null;

    private $galleriesPagination = null;

    private $galleriesLimit = null;

    public function __construct($domain = null)
    {
        // if (!$domain) {
        // 	F::error('Domain is required to create Domain class');
        // };
        parent::__construct($domain);
        $videohub = new Videohub;
        $arr = F::query_assoc('
			select domain,videosLimit, sameVideosLimit, videosPerUpdate, pagesLength, 
			star_videos_pagination, category_videos_pagination, search_videos_pagination,
			tag_videos_pagination, videos_pagination, video_load_link, video_watch_link,
			galleries_pagination, galleries_per_page
			from '.F::typetab('domains').'
			where domain=\''.$domain.'\';
			');
        $this->videosLimit = $arr['videosLimit'] ?? $videohub->getVideosLimit();
        $this->sameVideosLimit = $arr['sameVideosLimit'] ?? $videohub->getSameVideosLimit();
        $this->videosPerUpdate = $arr['videosPerUpdate'] ?? $videohub->getVideosPerUpdate();
        $this->pagesLength = $arr['pagesLength'] ?? $videohub->getPagesLength();
        $this->starVideosPagination = $arr['star_videos_pagination'];
        $this->categoryVideosPagination = $arr['category_videos_pagination'];
        $this->searchVideosPagination = $arr['search_videos_pagination'];
        $this->tagVideosPagination = $arr['tag_videos_pagination'];
        $this->videosPagination = $arr['videos_pagination'];
        $this->videoLoadLinkTemplate = $arr['video_load_link'];
        $this->videoWatchLinkTemplate = $arr['video_watch_link'];
        $this->galleriesPagination = $arr['galleries_pagination'];
        $this->galleriesLimit = $arr['galleries_per_page'];
    }

    public function getVideosLimit()
    {
        return $this->videosLimit;
    }

    public function getSameVideosLimit()
    {
        return $this->sameVideosLimit;
    }

    public function getVideosPerUpdate()
    {
        return $this->videosPerUpdate;
    }

    public function getPagesLength()
    {
        return $this->pagesLength;
    }

    public function videoByTitleTranslit($titleTranslit = null)
    {
        if (empty($titleTranslit)) {
            F::error('Specify titleTranslit');
        }
        $id = videoWriter::getIDbyDomainAndTitleTranslit($this->getDomain(), $titleTranslit);

        return new videoWriter($id);
    }

    public function getCategoryByNameTranslit($nameTranslit = null)
    {
        if (empty($nameTranslit)) {
            F::error('Specify nameTranslit');
        }
        $id = DomainCategory::getIdByDomainAndNameTranslit($this->getDomain(), $nameTranslit);
        if (! $id) {
            F::error('Category not found by nameTranslit');
        }

        return new DomainCategory($id, $this->getDomain());
    }

    public function getCategoryByNameUrl($nameUrl = null)
    {
        if (empty($nameUrl)) {
            F::error('Specify nameUrl');
        }
        $id = DomainCategory::getIdByDomainAndNameUrl($this->getDomain(), $nameUrl);
        if (! $id) {
            F::error('Category not found by nameUrl');
        }

        return new DomainCategory($id, $this->getDomain());
    }

    public function getCategoryById($id = null)
    {
        if (empty($id)) {
            F::error('Specify categoryId');
        }

        return new DomainCategory($id, $this->getDomain());
    }

    public function videos()
    {
        $videos = new Videos;
        $videos->setDomain($this->getDomain());

        return $videos;
    }

    public function categories()
    {
        $categories = new Categories;
        $categories->setDomain($this->getDomain());

        return $categories;
    }

    public function tags()
    {
        $tags = new VideoTags;
        $tags->setDomain($this->getDomain());

        return $tags;
    }

    public function lastVideoUpdateDate()
    {
        $videos = new Videos;
        $videos->setDomain($this->getDomain());
        $videos->setLimit(1);
        $videos->setPage(1);
        $videos->setOrder('mr');
        $arr = $videos->get();
        if (empty($arr)) {
            return false;
        }
        $video = new Video($arr[0]['id']);

        return $video->getApplyDate();
    }

    public function getVideoById($id = null)
    {
        if (! $id) {
            F::error('Id required to use getVideoById');
        }
        $videos = new Videos;
        $videos->setId($id);
        $videos->setDomain($this->getDomain());
        $arr = $videos->get();
        if (! $arr) {
            return null;
        }
        $video = new Video($arr[0]['id']);

        return $video;
    }

    public function getVideoByLocalId($localId = null)
    {
        if (! $localId) {
            F::error('LocalId required to use getIdByLocalId');
        }
        $videos = new Videos;
        $videos->setLocalId($localId);
        $videos->setDomain($this->getDomain());
        $arr = $videos->get();
        if (! $arr) {
            return null;
        }
        $video = new Video($arr[0]['id']);

        return $video;
    }

    public function getVideoByTitleTranslit($titleTranslit = null)
    {
        if (! $titleTranslit) {
            F::error('titleTranslit required to use getIdByLocalId');
        }
        $videos = new Videos;
        $videos->setTitleTranslit($titleTranslit);
        $videos->setDomain($this->getDomain());
        $arr = $videos->get();
        if (! $arr) {
            return null;
        }
        $video = new Video($arr[0]['id']);

        return $video;
    }

    public function getStarVideosPagination()
    {
        return $this->starVideosPagination;
    }

    public function getCategoryVideosPagination()
    {
        return $this->categoryVideosPagination;
    }

    public function getSearchVideosPagination()
    {
        return $this->searchVideosPagination;
    }

    public function getTagVideosPagination()
    {
        return $this->tagVideosPagination;
    }

    public function getVideosPagination()
    {
        return $this->videosPagination;
    }

    public function getVideoLoadLink($video = null)
    {
        $t = new Template;
        if (! $this->getVideoLoadLinkTemplate()) {
            F::error('set videoLoadLinkTemplate');
        }
        $t->setContent($this->getVideoLoadLinkTemplate());
        $t->v('id', $video->getId());
        $t->v('titleTranslit', $video->getTitleTranslit());

        return $t->get();
    }

    public function getVideoLoadLinkTemplate()
    {
        return $this->videoLoadLinkTemplate;
    }

    public function getVideoWatchLink($video = null)
    {
        $t = new Template;
        if (! $this->getVideoWatchLinkTemplate()) {
            F::error('set videoWatchLinkTemplate');
        }
        $t->setContent($this->getVideoWatchLinkTemplate());
        $t->v('id', $video->getId());
        $t->v('titleTranslit', $video->getTitleTranslit());

        return $t->get();
    }

    public function getVideoWatchLinkTemplate()
    {
        return $this->videoWatchLinkTemplate;
    }

    public function getGalleriesPagination()
    {
        return $this->galleriesPagination;
    }

    public function getGalleriesLimit()
    {
        return $this->galleriesLimit;
    }
}
