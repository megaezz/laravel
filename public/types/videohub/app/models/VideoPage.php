<?php

namespace videohub\app\models;

use engine\app\models\F;

class VideoPage extends VideoWriter
{
    public function __construct()
    {
        $this->setID(empty($_GET['id']) ? F::error('Specify video ID') : F::checkstr($_GET['id']));
        parent::__construct();
        // pre($this);
    }
}
