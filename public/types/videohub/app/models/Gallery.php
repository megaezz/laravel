<?php

namespace videohub\app\models;

use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class Gallery
{
    private $id = null;

    private $local_id = null;

    private $domain = null;

    private $apply_date = null;

    private $translate_date = null;

    private $title = null;

    private $description = null;

    private $title_translit = null;

    private $ext = null;

    private $pics_count = null;

    const STATIC_PATH = 'static/types/videohub/galleries';

    private $files_list_template = null;

    public function __construct($id = null)
    {
        if (empty($id)) {
            F::error('Id required to create Gallery object');
        }
        $arr = F::query_assoc('
			select id,local_id,domain,apply_date,translate_date,title_ru,description,title_ru_translit,ext,q_pics,
			gv.views, gv.rating, gv.votes
			from '.F::typetab('galleries').'
			left join '.F::typetab('galleries_views').' gv on gallery_id = id
			where id = \''.F::checkstr($id).'\'
			;');
        $this->id = $arr['id'];
        $this->local_id = $arr['local_id'];
        $this->domain = $arr['domain'];
        $this->apply_date = $arr['apply_date'];
        $this->translate_date = $arr['translate_date'];
        $this->title = $arr['title_ru'];
        $this->description = $arr['description'];
        $this->title_translit = $arr['title_ru_translit'];
        $this->ext = $arr['ext'];
        $this->pics_count = $arr['q_pics'];
        // $this-> = $arr[''];
    }

    public function getApplyDate()
    {
        return $this->apply_date;
    }

    public function getTitleTranslit()
    {
        return $this->title_translit;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getExt()
    {
        return $this->ext;
    }

    public function getFilesList()
    {
        if (! $this->getFilesListTemplate()) {
            F::error('Use setFilesListTemplate first');
        }
        $arr = $this->getFiles();
        $list = '';
        foreach ($arr as $v) {
            $t = new Template($this->getFilesListTemplate());
            $t->v('gallery_id', $this->getId());
            $t->v('filename', $v->name);
            $list .= $t->get();
        }

        return $list;
    }

    public function getFiles()
    {
        $arr = F::readDirectory(Engine::getRootPath().'/'.$this->getPath());
        // natsort($arr);
        foreach ($arr as $k => $file) {
            if ($file->name == 'preview.'.$this->getExt()) {
                unset($arr[$k]);
            }
        }

        return $arr;
    }

    public function getPath()
    {
        return static::STATIC_PATH.'/'.$this->getId();
    }

    public function getPreview()
    {
        return 'preview.'.$this->getExt();
    }

    public function getFilesListTemplate()
    {
        return $this->files_list_template;
    }

    public function setFilesListTemplate($template = null)
    {
        $this->files_list_template = $template;
    }
}
