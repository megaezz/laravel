<?php

namespace videohub\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;
use videohub\app\models\Domain;

class IncludesController
{
    private $domain = null;

    private static $headers = null;

    public function __construct()
    {
        // $this->domain = new Domain(Engine::getDomain());
        // чтобы сайты не были пустыми
        $this->domain = new Domain('xrest.net');
    }

    public function categoriesList()
    {
        // собираем блок с категориями
        $categories = $this->domain->categories();
        $categories->setListTemplate('lists/category');

        // $categories->setCalcVideosRows(true);
        return F::cache($categories, 'getList', null, 172800);
    }

    public function tagsList()
    {
        $tags = $this->domain->tags();
        $tags->setLimit(100);
        $tags->setPage(1);
        // $tags->setVideo(000154);
        $tags->setListTemplate('lists/tagAllTags');
        $tags->setGroup('tagRU');
        $tags->setOrder('videosCount');

        return $tags->getList();
    }
}
