<?php

namespace videohub\app\controllers\sites;

use engine\app\models\F;
use videohub\app\controllers\MainController;
use videohub\app\models\Tags;

class XrestNetController extends MainController
{
    public function indexPhp()
    {
        $getAction = $_GET['action'] ?? null;
        if ($getAction == 'tags') {
            $args['tagRuUrl'] = $_GET['tag'] ?? null;

            return $this->tag($args);
        }

        return $this->fixEncoding($this->indexTemplate($args)->get());
    }

    public function redirectFromVideoCategory($args = [])
    {
        $titleTranslit = $args['titleTranslit'] ?? null;
        if (! $titleTranslit) {
            F::error('Required titleTranslit');
        }
        $video = $this->getDomain()->getVideoByTitleTranslit($titleTranslit);
        F::redirect_301($this->getDomain()->getVideoWatchLink($video));
    }

    public function redirectFromLoadCategory($args = [])
    {
        $titleTranslit = $args['titleTranslit'] ?? null;
        if (! $titleTranslit) {
            F::error('Required titleTranslit');
        }
        $video = $this->getDomain()->getVideoByTitleTranslit($titleTranslit);
        F::redirect_301($this->getDomain()->getVideoLoadLink($video));
    }

    protected function indexHandler($args = [])
    {
        parent::indexHandler($args);
        $page = $args['page'] ?? 1;
        $order = 'mr';
        $t = $this->getTemplate();
        // получаем 100 новых видео
        $videos = $this->getDomain()->videos();
        $videos->setListTemplate('li/thumbs.htm');
        $videos->setOrder('mr');
        $videos->setLimit(100);
        $t->v('xrest_videos_list_new', F::cache($videos, 'getList'));
        // получаем новые видео с 6й страницы
        $videos = $this->domain->videos();
        $videos->setListTemplate('li/thumbs.htm');
        $videos->setOrder($order);
        $videos->setPage(6);
        $videos->setLimit($this->getDomain()->getVideosLimit());
        $videos->setPaginationUrlPattern($this->getDomain()->getVideosPagination());
        $t->v('xrest_videos_list_all', F::cache($videos, 'getList'));
        $t->v('xrest_pages_videos_list_all', F::cache($videos, 'getPages'));
        /*
        формируем тексты самых популярных роликов и самых просматриваемых
        */
        $videos = $this->getDomain()->videos();
        $videos->setOrder('tr');
        $videos->setLimit(5);
        $videos->setPage(1);
        $videos->setListTemplate('li/thumbs_text_tr.htm');
        $t->v('cache(xrest_text_popular_videos)', F::cache($videos, 'getList', null, 3600));
        //
        $videos = $this->getDomain()->videos();
        $videos->setOrder('mv');
        $videos->setLimit(5);
        $videos->setPage(1);
        $videos->setListTemplate('li/thumbs_text_mv.htm');
        $t->v('cache(xrest_last_viewed_videos)', F::cache($videos, 'getList', null, 3600));

        return $t;
    }

    protected function setTags($t = null)
    {
        $tags = new Tags;
        $tags->setDomain($this->getDomain()->getDomain());
        $tags->setOrder('count(distinct videos.id) desc');
        $tags->setListTemplate('li/item_tags_block.htm');
        $tags->setCalcVideosRows(true);
        $tags->setLimit(205);
        $t->v('cached_xrest_tags_list', F::cache($tags, 'getList'));
    }
}
