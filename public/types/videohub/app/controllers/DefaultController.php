<?php

namespace videohub\app\controllers;

// use engine\app\models\Template;
use DateTime;
use engine\app\models\Engine;
// use engine\app\models\Router;
// use DateTime;
// use videohub\app\models\VideoComments;
// use \ReCaptcha\ReCaptcha;
// use videohub\app\models\VideoTags;
use engine\app\models\F;
use videohub\app\models\Domain;
use videohub\app\models\VideoComments;
use videohub\app\models\VideoWriter;

class DefaultController
{
    private $domain = null;

    private $template = null;

    public function __construct()
    {
        // parent::__construct();
        $this->domain = new Domain(Engine::getDomain());
        // чтобы сайты не были пустыми
        $this->domain = new Domain('xrest.net');
        $this->template = Engine::template();
        F::setPageVars($this->template);
        $this->setMenuVars($this->template);
    }

    // редирект на прямую ссылку
    public function videoDirectLink($args = null)
    {
        $id = empty($args['videoID']) ? F::error('VideoID required for videoDirectLink') : F::checkstr($args['videoID']);
        $video = new VideoWriter($id);
        $link = F::cacheKey("Video:{$video->getID()}", $video, 'getDirectLinkSame', null, 1200);
        F::redirect($link);
    }

    // вывод списка видео
    public function videos($args = null)
    {
        $t = $this->template;
        $page = empty($args['page']) ? 1 : F::checkstr($args['page']);
        $limit = $this->domain->getVideosLimit();
        $pagesLength = $this->domain->getPagesLength();
        $order = empty($args['order']) ? 'mr' : F::checkstr($args['order']);

        $t->v('orderMrActive', ($order === 'mr') ? 'active' : '');
        $t->v('orderTrActive', ($order === 'tr') ? 'active' : '');
        $t->v('orderMvActive', ($order === 'mv') ? 'active' : '');

        $videos = $this->domain->videos();
        $videos->setListTemplate('lists/thumb');
        $videos->setOrder($order);
        $videos->setPage($page);
        $videos->setLimit($limit);
        $videos->setPaginationMaxPagesToShow($pagesLength);
        $videos->setPaginationURLPattern("/$order/(:num)");

        if (! empty($args['categoryNameTranslit'])) {
            $category = $this->domain->categoryByNameTranslit($args['categoryNameTranslit']);
            $videos->setCategory($category->getID());
            $videos->setPaginationURLPattern("/{$category->getNameTranslit()}/$order/(:num)");
            $t->v('categoryText', $category->getText());
            $t->v('categoryName', $category->getName());
            $t->v('categoryNameTranslit', $category->getNameTranslit());
            $t->v('categoryID', $category->getID());
        }

        if (! empty($args['tagRU'])) {
            $t->v('tagRU', urldecode($args['tagRU']));
            $videos->setPaginationURLPattern("/tag/$order/{$args['tagRU']}/(:num)");
            $videos->setTags([urldecode($tag)]);
        }

        // заполняем видео в шаблон
        $t->v('videosList', F::cache($videos, 'list'));
        $t->v('videosPages', F::cache($videos, 'getPages'));
        $t->v('videosMaxPages', F::cache($videos, 'getMaxPages'));
        $t->v('videosOrder', $order);

        return $t->get();
    }

    // для обычной текстовой страницы
    public function text()
    {
        return $this->template->get();
    }

    // обратная связь
    public function feedback()
    {
        $text = empty($_POST['text']) ? null : F::checkstr($_POST['text']);
        $t = $this->template;
        $t->v('submitAnswer', null);
        if ($text) {
            // Feedback::add($text);
            $t->v('submitAnswer', 'Ваше сообщение успешно отправлено.');
        }

        return $t->get();
    }

    // список видео для категории
    public function categoryVideos($args = null)
    {
        return $this->videos($args);
    }

    // о нас (отдельный контроллер создан для подсвечивания элемента меню)
    public function aboutUs()
    {
        return $this->template->get();
    }

    // проставляет 'active' элементам меню
    public function setMenuVars($t = null)
    {
        $arr['Index'] = ['Default\\videos'];
        $arr['Category'] = ['Default\\categoryVideos'];
        $arr['Feedback'] = ['Default\\feedback'];
        $arr['AboutUs'] = ['Default\\aboutUs'];
        $arr['Stars'] = ['Default\\stars'];
        $arr['Articles'] = ['Default\\articles'];

        return F::setMenuActiveVars($t, Engine::router(), $arr);
    }

    // список видео категорий
    public function categories()
    {
        $t = $this->template;
        $categories = $this->domain->categories();
        $categories->setTemplate('lists/categoryBig');
        // $categories->setCalcVideosRows(true);
        $t->v('categoriesList', F::cache($categories, 'get', null, 172800));

        return $t->get();
    }

    // страница просмотра видео
    public function showVideo($args = [])
    {
        // Собираем блок плеера
        $translit = empty($args['titleTranslit']) ? F::error('Translit required for main/showVideo') : F::checkstr($args['titleTranslit']);
        $t = $this->template;
        $video = $this->domain->videoByTitleTranslit($translit);
        $video->viewsCounter();
        $video->setTagsListTemplate('lists/tagsList');
        $t->v('videoID', $video->getID());
        $t->v('videoTitle', $video->getTitle());
        // используем cacheKey, т.к. иначе из за того что статистика видео меняется - пересчитывается каждый раз
        $t->v('videoTagsKeywords', F::cacheKey("Video:{$video->getID()}", $video, 'getTagsComma'));
        $t->v('videoTitleKeywords', $video->getTitleComma());
        $t->v('videoPlayer', $video->getInternalPlayer());
        $t->v('videoTitleTranslit', $video->getTitleTranslit());
        $t->v('videoDescription', $video->getDescription());
        $t->v('videoTagsList', $video->getTagsList());
        $t->v('videoApplyDate', (new DateTime($video->getApplyDate()))->format('d.m.Y в H:i'));
        $t->v('videoViews', $video->getViews());
        $t->v('videoLoads', $video->getLoads());
        $t->v('videoLikesSum', $video->getLikesSum());
        // собираем блок комментариев
        $comments = new VideoComments($video->getID());
        $comments->setListTemplate('lists/comment');
        $t->v('commentsList', F::cacheKey("VideoComments:{$video->getID()}", $comments, 'getList'));
        $t->v('commentsNum', F::cacheKey("VideoComments:{$video->getID()}", $comments, 'getRows'));
        // собираем блок похожего видео
        $sameLimit = $this->domain->getSameVideosLimit();
        $videos = $this->domain->videos();
        $videos->setPage(1);
        $videos->setLimit($sameLimit);
        $videos->setExist(true);
        $videos->setSameVideo($video->getID());
        $videos->setListTemplate('lists/thumb');
        $t->v('sameVideosList', F::cache($videos, 'list', null, 604800));

        return $t->get();
    }
}
