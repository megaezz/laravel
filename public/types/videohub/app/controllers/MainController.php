<?php

namespace videohub\app\controllers;

use engine\app\models\Cache;
use engine\app\models\Db;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use ReCaptcha\ReCaptcha;
use videohub\app\models\Article;
use videohub\app\models\Articles;
use videohub\app\models\Domain;
use videohub\app\models\Galleries;
use videohub\app\models\Gallery;
use videohub\app\models\PageComment;
use videohub\app\models\PageComments;
use videohub\app\models\Star;
use videohub\app\models\Tag;
use videohub\app\models\Tags;
use videohub\app\models\Video;
use videohub\app\models\VideoComment;
use videohub\app\models\VideoComments;
use videohub\app\models\Videos;
use videohub\app\models\VideoTags;

class MainController
{
    protected $domain = null;

    protected $active_category = null;

    protected $encoding = null;

    protected $template = null;

    public function __construct()
    {
        $this->setDomain(new Domain(engine::getDomain()));
        $template = Engine::router()->getTemplate();
        if ($template) {
            $this->setTemplate(new Template($template));
        }
        $this->setEncoding('windows-1251');
    }

    public function index($args = [])
    {
        // обработчик страниц вида /?page=comment/submit
        $getPage = $_GET['page'] ?? null;
        if ($getPage) {
            return $this->getPageHandlers($getPage);
        }

        return $this->fixEncoding($this->indexHandler($args)->get());
    }

    public function video($args = [])
    {
        $videoId = $args['videoId'] ?? null;
        $localId = $args['localId'] ?? null;
        $titleTranslit = $args['titleTranslit'] ?? null;
        // $categoryNameTranslit = $args['categoryNameTranslit']??null;
        if ($videoId) {
            $video = $this->domain->getVideoById($videoId);
        }
        if (! isset($video) and $localId) {
            $video = $this->domain->getVideoByLocalId($localId);
        }
        if (! isset($video) and $titleTranslit) {
            $video = $this->domain->getVideoByTitleTranslit($titleTranslit);
        }
        if (! isset($video)) {
            F::error('Видео не найдено');
        }
        $t = $this->template;
        /*
        if ($categoryNameTranslit) {
            $category = $this->getDomain()->getCategoryByNameTranslit($categoryNameTranslit);
            $t->v('get_category_name_translit',$category->getNameTranslit());
            $t->v('category_name',$category->getName());
            // получаем видео для категории
            $videos = $this->domain->videos();
            $videos->setCategory($category->getId());
            $videos->setListTemplate('li/thumbs.htm');
            $videos->setOrder('mv');
            $videos->setLimit($this->domain->getVideosLimit());
            $videos->setPaginationMaxPagesToShow($this->domain->getPagesLength());
            $pagination = new Template;
            if (!$this->getDomain()->getCategoryVideosPagination()) {
                F::error('set categoryVideosPagination');
            }
            $pagination->setContent($this->getDomain()->getCategoryVideosPagination());
            $pagination->v('nameTranslit',$category->getNameTranslit());
            $pagination->v('nameUrl',$category->getNameUrl());
            $pagination->v('id',$category->getId());
            $pagination->v('order',$videos->getOrder());
            $videos->setPaginationUrlPattern($pagination->get());
            $t->v('xrest_pages_videos_list_category',F::cache($videos,'getPages'));
            $t->v('videos_list_category',F::cache($videos,'getList'));
            $t->v('ajax_get_sort',$videos->getOrder());
            $t->v('max_pages_videos_list_category',ceil(F::cache($videos,'getRows')/$videos->getLimit()));
            $t->v('category_id',$category->getId());
        }
        */
        $video->viewsCounter();
        $t->v('videoId', $video->getId());
        $t->v('video_id', $video->getId());
        $t->v('video_local_id', $video->getLocalId());
        $t->v('video_likes', $video->getLikes());
        $t->v('video_dislikes', $video->getDislikes());
        $t->v('video_likes_percent', $video->getLikesPercent());
        $t->v('video_likes_sum', $video->getLikesSum());
        $t->v('video_dislikes_percent', $video->getDislikesPercent());
        $t->v('videoTitle', $video->getTitle());
        $t->v('video_title', $video->getTitle());
        $t->v('videoDescription', $video->getDescription());
        $t->v('video_description', $video->getDescription());
        $t->v('video_views', $video->getViews());
        $player = $video->getPlayer();
        $t->v('video_embed_player', $player);
        $t->v('xrest_embed_or_video_player', $player);
        $t->v('video_save_link', $video->getVideoSaveLink());
        $t->v('video_loads', $video->getLoads());
        $t->v('video_tags_keywords', implode(', ', F::cache($video, 'getTags')));
        $t->v('video_title_keywords', str_replace(' ', ', ', strtolower($video->getTitle())));
        $t->v('video_load_link', $this->getDomain()->getVideoLoadLink($video));
        $months = F::getMonths();
        $addDate = new \DateTime($video->getAddDate());
        $formatter = new \IntlDateFormatter(
            'RU_RU',
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE,
            'Europe/Moscow'
        );
        $t->v('video_added_date', $formatter->format($addDate).' в '.$addDate->format('H:i'));
        $t->v('get_title_ru_translit', $video->getTitleTranslit());
        // получаем список тегов
        $videoTags = new VideoTags;
        $videoTags->setListTemplate('li/list_tags_of_video.htm');
        $videoTags->setVideoId($video->getId());
        $videoTags->setLimit(6);
        $t->v('video_tags_list', F::cache($videoTags, 'getList'));
        $t->v('xrest_video_tags_list', F::cache($videoTags, 'getList'));
        // получаем похожие видео
        // ПРОВЕРИТЬ ЕСЛИ НЕТ ТЕГОВ ТО ЧТО ВЫВОДИТ
        $videos = $this->domain->videos();
        $videos->setSameVideo($video->getId());
        $videos->setLimit(12);
        $videos->setPage(1);
        $videos->setListTemplate('li/thumbs.htm');
        $t->v('videos_list_same', F::cache($videos, 'getList'));
        // получаем список категорий для видео
        $categories = $this->domain->categories();
        $categories->setType('default');
        $categories->setVideoId($video->getId());
        $categories->setDomainActive(true);
        $categories->setListTemplate('li/list_categories_of_video.htm');
        // $t->v('list_categories_of_video_alt',F::cache($categories,'getList'));
        $t->v('list_categories_of_video_alt', F::cache($categories, 'getList'));
        // получаем список категорий для видео
        $categories = $this->domain->categories();
        $categories->setType('star');
        $categories->setVideoId($video->getId());
        $categories->setDomainActive(true);
        $categories->setListTemplate('li/item_stars_in_video.htm');
        // $t->v('list_stars_of_video',F::cache($categories,'getList'));
        $t->v('list_stars_of_video', F::cache($categories, 'getList'));
        // получаем комментарии
        $comments = new VideoComments($video->getId());
        $comments->setActive(true);
        $comments->setListTemplate('comments/li.htm');
        $commentsList = $comments->getList();
        $t->v('comments_list', $commentsList);
        $t->v('xrest_comments_list', $commentsList);
        $t->v('video_comments', $comments->getRows());
        // проверка выполняется, нет необходимости в этой переменной
        $t->v('check_video_id', '');
        // счетчик просмотров выполняется, не нужна эта переменная
        $t->v('views_count', '');
        $t->v('xrest_video_html_codes', '');
        $this->setMenuIsActive($t, 'video');
        $this->setTags($t);
        $this->setPageVars($t);
        $this->setCategories($t);

        return $this->fixEncoding($t->get());
    }

    public function submitContact()
    {
        $name = $_POST['name'] ?? F::error('Name required to use submitPageComment');
        $page = $_POST['page'] ?? F::error('Page required to use submitPageComment');
        $text = $_POST['text'] ?? F::error('Text required to use submitPageComment');
        $id = PageComment::add($this->getDomain()->getDomain(), $page);
        $pageComment = new PageComment($id);
        $pageComment->setName(htmlspecialchars($name));
        $pageComment->setPagePath($page);
        $pageComment->setText(nl2br(htmlspecialchars($text)));
        $pageComment->setIp(request()->ip());
        $pageComment->setActive(true);
        $pageComment->save();
        F::alert('Ваше сообщение успешно отправлено');
    }

    public function submitLike()
    {
        $videoId = $_GET['video_id'] ?? F::error('Не передан ID видео');
        $action = $_GET['action'] ?? F::error('Не передано действие');
        $video = new Video($videoId);
        $cookieName = 'like'.$videoId;
        $cookieValue = $_COOKIE[$cookieName] ?? null;
        if ($cookieValue) {
            if ($cookieValue == 'like') {
                return 'Вы уже поставили этому видео лайк';
            }
            if ($cookieValue == 'dislike') {
                return 'Вы уже поставили этому видео дизлайк';
            }

            return 'Вы уже оценили это видео';
        }
        if ($action == 'like') {
            setcookie($cookieName, 'like');
            $video->likeCounter();
        }
        if ($action == 'dislike') {
            setcookie($cookieName, 'dislike');
            $video->dislikeCounter();
        }

        // F::dump($video->getLikesSum());
        return (string) $video->getLikesPercent().'%';
    }

    public function submitAddComment()
    {
        // $recaptchaResponse = empty($_POST['g-recaptcha-response'])?F::error('Recaptcha response is required'):F::checkstr($_POST['g-recaptcha-response']);
        // if (!Engine::checkRecaptcha($recaptchaResponse)) {
        // 	F::alert('Sorry, you didn\'t pass a robot check. Please try again one more time.');
        // }
        $username = $_POST['name'] ?? F::error('Не передано имя пользователя');
        $text = $_POST['message'] ?? F::error('Не передан текст');
        $videoId = $_POST['video_id'] ?? F::error('Не передан ID видео');
        $id = VideoComment::add($videoId);
        $comment = new VideoComment($id);
        $comment->setUsername(htmlspecialchars($username));
        $comment->setText(nl2br(htmlspecialchars($text)));
        // $comment->setStatus('ok');
        $comment->setActive(true);
        $comment->setIp(request()->ip());
        $comment->save();
        F::redirect(F::referer_url());
    }

    public function load($args = [])
    {
        $videoId = $args['videoId'] ?? null;
        $localId = $args['localId'] ?? null;
        $titleTranslit = $args['titleTranslit'] ?? null;
        if ($videoId) {
            $video = $this->domain->getVideoById($videoId);
        }
        if (! isset($video) and $localId) {
            $video = $this->domain->getVideoByLocalId($localId);
        }
        if (! isset($video) and $titleTranslit) {
            $video = $this->domain->getVideoByTitleTranslit($titleTranslit);
        }
        if (! isset($video)) {
            F::error('Видео не найдено');
        }
        $t = $this->getTemplate();
        $video->loadsCounter();
        $t->v('video_id', $video->getId());
        $t->v('video_save_link', $video->getVideoSaveLink());
        $t->v('video_title', $video->getTitle());
        $t->v('video_description', $video->getDescription());
        $t->v('get_title_ru_translit', $video->getTitleTranslit());
        $t->v('check_video_id', '');
        $t->v('loads_count', '');
        $t->v('video_tags_keywords', implode(', ', F::cache($video, 'getTags')));
        $t->v('video_title_keywords', str_replace(' ', ', ', strtolower($video->getTitle())));
        $t->v('video_watch_link', $this->getDomain()->getVideoWatchLink($video));
        $t->v('video_image_link', $video->getThumb());
        $this->setMenuIsActive($t, 'load');
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setTags($t);

        return $this->fixEncoding($t->get());
    }

    public function category($args = [])
    {
        // F::dump($args);
        // F::dump(Engine::getRunningTime());
        $translit = empty($args['nameTranslit']) ? null : F::checkstr($args['nameTranslit']);
        $nameUrl = empty($args['nameUrl']) ? null : F::checkstr($args['nameUrl']);
        $categoryId = $args['categoryId'] ?? null;
        $order = empty($args['order']) ? 'mr' : $args['order'];
        if ($categoryId) {
            $category = $this->domain->getCategoryById($categoryId);
        }
        if (! isset($category) and $translit) {
            $category = $this->domain->getCategoryByNameTranslit($translit);
        }
        if (! isset($category) and $nameUrl) {
            $category = $this->domain->getCategoryByNameUrl(urldecode($nameUrl));
        }
        if (empty($category)) {
            F::error('Category not found');
        }
        $page = empty($args['page']) ? 1 : F::checkstr($args['page']);
        $limit = $this->domain->getVideosLimit();
        $pagesLength = $this->domain->getPagesLength();
        $t = $this->getTemplate();
        $t->v('categoryText', $category->getText());
        $t->v('category_text', $category->getText());
        $t->v('categoryName', $category->getName());
        $t->v('category_name', $category->getName());
        $t->v('categoryId', $category->getId());
        $t->v('category_id', $category->getId());
        $t->v('category_img', $category->getImg());
        $t->v('category_title', $category->getTitle());
        $t->v('category_description', $category->getDescription());
        $t->v('category_name_translit', $category->getNameTranslit());
        $t->v('categories_videos_description', trim(strip_tags($category->getText())));
        $t->v('categories_videos_keywords', F::cache($category, 'getKeywords'));
        $t->v('check_category_translit', $translit ? (($translit == $category->getNameTranslit()) ? '' : F::error('Wrong nameTranslit')) : '');
        // получаем видео для категории
        $videos = $this->domain->videos();
        $videos->setCategory($category->getId());
        $videos->setListTemplate('li/thumbs.htm');
        $videos->setOrder($order);
        $videos->setPage($page);
        $videos->setLimit($limit);
        $videos->setPaginationMaxPagesToShow($pagesLength);
        $pagination = new Template;
        if ($category->getType() == 'star') {
            $star = new Star($category->getId());
            // F::dump($star);
            $t->v('star_name', $star->getNameEn());
            $t->v('star_name_ru', $star->getNameRu());
            $t->v('star_aka', $star->getAkaEn() ? (' или '.$star->getAkaEn()) : '');
            $t->v('star_aka_en', $star->getAkaEn() ? (' aka '.$star->getAkaEn()) : '');
            $t->v('star_gender_en', $star->getGenderEn());
            $t->v('star_age', $star->getAge());
            $t->v('star_country_en', $star->getCountryEn());
            $t->v('star_region_en', $star->getRegionEn());
            $t->v('star_city_en', $star->getCityEn());
            $t->v('star_ethnicity_en', $star->getEthnicityEn());
            $t->v('star_height', $star->getHeight());
            $t->v('star_weight', $star->getWeight());
            $t->v('star_hair_en', $star->getHairEn());
            if (! $this->getDomain()->getStarVideosPagination()) {
                F::error('set starVideosPagination');
            }
            $pagination->setContent($this->getDomain()->getStarVideosPagination());
        } else {
            if (! $this->getDomain()->getCategoryVideosPagination()) {
                F::error('set categoryVideosPagination');
            }
            $pagination->setContent($this->getDomain()->getCategoryVideosPagination());
        }
        $pagination->v('nameTranslit', $category->getNameTranslit());
        $pagination->v('nameUrl', $category->getNameUrl());
        $pagination->v('id', $category->getId());
        $pagination->v('order', $videos->getOrder());
        $videos->setPaginationUrlPattern($pagination->get());
        // заполняем видео в шаблон
        $t->v('categoryVideosList', F::cache($videos, 'getList'));
        $t->v('videos_list_category', F::cache($videos, 'getList'));
        $t->v('pornstarshub_pages_videos_list_category_star', F::cache($videos, 'getPages'));
        $t->v('pornstarshub_pages_videos_list_category', F::cache($videos, 'getPages'));
        $t->v('pornozvezda_pages_videos_list_category_star', F::cache($videos, 'getPages'));
        $t->v('xrest_pages_videos_list_category', F::cache($videos, 'getPages'));
        $t->v('pages_categories_videos_alt', F::cache($videos, 'getPages'));
        $t->v('categoryVideosPages', F::cache($videos, 'getPages'));
        $t->v('categoryVideosMaxPages', F::cache($videos, 'getMaxPages'));
        $t->v('categoryVideosOrder', $order);
        $t->v('ajax_get_sort', $videos->getOrder());
        $t->v('sort_name', $this->getOrderNameRu($videos->getOrder()));
        $t->v('page_number', $videos->getPage());
        $t->v('max_pages_videos_list_category', ceil(F::cache($videos, 'getRows') / $videos->getLimit()));
        $this->setMenuIsActive($t, 'categories_videos');
        $this->setSortIsActive($t, $videos->getOrder());
        $this->setActiveCategory($category->getId());
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setTags($t);

        return $this->fixEncoding($t->get());
    }

    public function test()
    {
        // Cache::setDebug(true);
        // Cache::setKeepClonedObjects(false);
        $videos = $this->domain->videos();
        $videos->setListTemplate('li/thumbs.htm');
        $videos->setLimit(1);
        $videos->setPage(4);
        $cache = new Cache;
        $cache->setObject($videos);
        $cache->setMethod('getList');
        $cache->setSeconds(5);
        echo $cache->getHash().'<br>';
        $cache->result();
        echo $cache->getHash().'<br>';

        $cache->setMethod('getRows');
        echo $cache->getHash().'<br>';
        $cache->result();
        echo $cache->getHash().'<br>';

        // $videos = $cache->getObject();
        // F::dump(cache::getClonedObjects());
        // F::cache($videos,'getList');
        // F::cache($videos,'getMaxPages');
        // F::cache($videos,'getRows');
        return '';
    }

    public function simple()
    {
        $t = $this->getTemplate();
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setMenuIsActive($t, 'simple');

        return $this->fixEncoding($t->get());
    }

    public function tags()
    {
        $t = $this->getTemplate();
        $tags = new Tags;
        $tags->setDomain($this->getDomain()->getDomain());
        $tags->setOrder('count(distinct videos.id) desc');
        $tags->setListTemplate('li/item_all_tags_list.htm');
        $tags->setCalcVideosRows(true);
        $t->v('all_tags_list', F::cache($tags, 'getList'));
        $this->setMenuIsActive($t, 'tags');
        $this->setPageVars($t);
        $this->setCategories($t);

        return $this->fixEncoding($t->get());
    }

    public function search($args = [])
    {
        $page = $args['page'] ?? 1;
        $queryUrl = $args['queryUrl'] ?? null;
        if ($queryUrl) {
            $query = urldecode($queryUrl);
        }
        if (! empty($_POST['search_query'])) {
            $query = $_POST['search_query'];
        }
        if (! isset($query)) {
            F::error('Empty search query');
        }
        $t = $this->getTemplate();
        $videos = $this->getDomain()->videos();
        $videos->setSearch($query);
        $videos->setListTemplate('li/thumbs.htm');
        $videos->setOrder('tr');
        $videos->setPage($page);
        $videos->setLimit($this->getDomain()->getVideosLimit());
        $pagination = new Template;
        if (! $this->getDomain()->getSearchVideosPagination()) {
            F::error('set searchVideosPagination');
        }
        $pagination->setContent($this->getDomain()->getSearchVideosPagination());
        $pagination->v('queryUrl', urlencode($videos->getSearch()));
        $pagination->v('order', $videos->getOrder());
        $videos->setPaginationUrlPattern($pagination->get());
        $t->v('sort_name', $this->getOrderNameRu($videos->getOrder()));
        $t->v('page_number', F::cache($videos, 'getPage'));
        $t->v('videos_list_search', F::cache($videos, 'getList'));
        $t->v('pornstarshub_pages_videos_list_search', F::cache($videos, 'getPage'));
        $t->v('pornozvezda_pages_videos_list_search', F::cache($videos, 'getPage'));
        $t->v('pages_search_videos_alt', F::cache($videos, 'getPage'));
        $t->v('get_query', $videos->getSearch());
        $t->v('get_query_urlencode', urlencode($videos->getSearch()));
        $this->setSortIsActive($t, $videos->getOrder());
        $this->setMenuIsActive($t, 'search');
        $this->setPageVars($t);
        $this->setCategories($t);

        return $this->fixEncoding($t->get());
    }

    public function robots()
    {
        Engine::setDebug(false);
        header('Content-Type: text/plain');
        $this->setPageVars($this->template);

        return $this->template->get();
    }

    public function tag($args = [])
    {
        $order = empty($args['order']) ? 'tr' : $args['order'];
        $page = $args['page'] ?? 1;
        $tagEnWithoutSpaces = $args['tagEnWithoutSpaces'] ?? null;
        $tagRuWithoutSpaces = $args['tagRuWithoutSpaces'] ?? null;
        $tagRuUrl = $args['tagRuUrl'] ?? null;
        if ($tagRuWithoutSpaces) {
            $tagRu = F::withSpaces($tagRuWithoutSpaces);
        }
        if (! isset($tagRu) and $tagEnWithoutSpaces) {
            $tagEn = F::withSpaces($tagEnWithoutSpaces);
            $tagRu = Tag::getTagRuByTagEn($tagEn);
        }
        if (! isset($tagRu) and $tagRuUrl) {
            $tagRu = urldecode($tagRuUrl);
        }
        if (! isset($tagRu)) {
            F::error('Tag not found');
        }
        $tag = new Tag($tagRu);
        // F::dump($tagRu);
        $t = $this->getTemplate();
        // получаем видео для тега
        $videos = $this->getDomain()->videos();
        $videos->setTag($tag->getNameRu());
        $videos->setListTemplate('li/thumbs.htm');
        $videos->setOrder($order);
        $videos->setPage($page);
        $videos->setLimit($this->getDomain()->getVideosLimit());
        $pagination = new Template;
        if (! $this->getDomain()->getTagVideosPagination()) {
            F::error('set tagVideosPagination');
        }
        $pagination->setContent($this->getDomain()->getTagVideosPagination());
        $pagination->v('nameRuWithoutSpaces', F::withoutSpaces($tag->getNameRu()));
        $pagination->v('nameEnWithoutSpaces', F::withoutSpaces($tag->getNameEn()));
        $pagination->v('nameRuUrl', urlencode($tag->getNameRu()));
        $pagination->v('nameEnUrl', urlencode($tag->getNameEn()));
        $pagination->v('order', $videos->getOrder());
        $videos->setPaginationUrlPattern($pagination->get());
        $t->v('videos_list_tag_en', F::cache($videos, 'getList'));
        $t->v('videos_list_tag', F::cache($videos, 'getList'));
        $t->v('pornstarshub_pages_videos_list_tag', F::cache($videos, 'getPages'));
        $t->v('xrest_pages_videos_list_tag', F::cache($videos, 'getPages'));
        $t->v('pages_tags_videos_alt', F::cache($videos, 'getPages'));
        $t->v('tag_rows', F::cache($videos, 'getRows'));
        $t->v('tag_name', $tag->getNameRu());
        $t->v('tag_name_en', $tag->getNameEn());
        $t->v('sort_name', $this->getOrderNameRu($videos->getOrder()));
        $t->v('page_number', $videos->getPage());
        $t->v('tag_ru_urlencode', urlencode($tag->getNameRu()));
        $this->setSortIsActive($t, $videos->getOrder());
        $this->setMenuIsActive($t, 'tag');
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setTags($t);

        return $this->fixEncoding($t->get());
    }

    public function save($args = [])
    {
        $localId = $args['localId'] ?? null;
        $videoId = $args['videoId'] ?? null;
        if ($localId) {
            $videoId = $this->getDomain()->getVideoByLocalId($localId)->getId();
        }
        if (! $videoId) {
            $videoId = $args['videoId'] ?? F::error('VideoId required to use save');
        }
        $video = new Video($videoId);
        $link = F::cache($video, 'getDirectLink', null, 1200);
        if (! $link) {
            $videos = $this->getDomain()->videos();
            $videos->setSameVideo($video->getId());
            $videos->setLimit(10);
            // $videos->setExist(true);
            $arr = F::cache($videos, 'get');
            // F::dump($arr);
            foreach ($arr as $v) {
                $video = new Video($v['id']);
                $link = F::cache($video, 'getDirectLink', null, 1200);
                if ($link) {
                    break;
                }
            }
            if (! $link) {
                F::error('Download link is not available');
            }
            // F::dump($videos->get());
        }
        header('Location: '.$link);

        return '';
    }

    protected function indexHandler($args = [])
    {
        $page = $args['page'] ?? 1;
        $order = empty($args['order']) ? 'mr' : $args['order'];
        $t = $this->template;
        // получаем новое видео
        $videos = $this->domain->videos();
        $videos->setListTemplate('li/thumbs.htm');
        $videos->setOrder($order);
        $videos->setPage($page);
        $videos->setLimit($this->domain->getVideosLimit());
        $t->v('videos_list_all', F::cache($videos, 'getList'));
        $pagination = new Template;
        if (! $this->getDomain()->getVideosPagination()) {
            F::error('set videosPagination');
        }
        $pagination->setContent($this->getDomain()->getVideosPagination());
        $pagination->v('order', $videos->getOrder());
        $videos->setPaginationUrlPattern($pagination->get());
        $t->v('pornstarshub_pages_videos_list_all', F::cache($videos, 'getPages'));
        $t->v('pornozvezda_pages_videos_list_all', F::cache($videos, 'getPages'));
        $t->v('pages_video_collection_alt', F::cache($videos, 'getPages'));
        $orders = Videos::getOrders();
        $t->v('sort_name_en', $this->getOrderNameEn($videos->getOrder()));
        $t->v('page_number', $videos->getPage());
        $t->v('ajax_get_sort', $videos->getOrder());
        $t->v('sort_name', $this->getOrderNameRu($videos->getOrder()));
        $t->v('max_pages_videos_list_all', ceil(F::cache($videos, 'getRows') / $videos->getLimit()));
        $t->v('cache(last_video_comments)', F::cache($this, 'getLastComments'));
        $this->setMenuIsActive($t, 'index');
        $this->setSortIsActive($t, $videos->getOrder());
        $this->setPageVars($t);
        $this->setCategories($t);

        return $this->template;
    }

    public function getLastComments($t = null)
    {
        $videoComments = new VideoComments;
        $videoComments->setDomain($this->getDomain()->getDomain());
        $videoComments->setActive(true);
        $videoComments->setLimit(10);
        $videoComments->setOrder('desc');
        $arr = $videoComments->get();
        $videoCommentsList = '';
        foreach ($arr as $v) {
            $tList = new Template('comments/last.htm');
            $comment = new VideoComment($v['id']);
            $tList->v('user', $comment->getUsername());
            $tList->v('date', $comment->getDate());
            $tList->v('text', $comment->getText());
            $video = new Video($comment->getVideoId());
            $tList->v('id', $video->getId());
            $tList->v('title_ru_translit', $video->getTitleTranslit());
            $tList->v('title_ru', $video->getTitle());
            $videoCommentsList .= $tList->get();
        }

        return $videoCommentsList;
    }

    public function article($args = [])
    {
        $titleTranslit = $args['titleTranslit'] ?? null;
        $articleId = $args['articleId'] ?? null;
        if ($articleId) {
            $article = new Article($articleId);
        }
        if (! isset($article) and $titleTranslit) {
            $articles = new Articles;
            $articles->setDomain($this->getDomain()->getDomain());
            $articles->setTitleTranslit($titleTranslit);
            $arr = $articles->get();
            if ($arr) {
                $article = new Article($arr[0]['id']);
            }
        }
        if (empty($article)) {
            F::error('Article not found');
        }
        $t = $this->template;
        $t->v('story_title', $article->getTitle());
        $t->v('story_text', $article->getText());
        $ratingStars = new Template('stories/rating_stars.htm');
        $ratingStars->v('story_id', $article->getId());
        $ratingStars->v('story_rating', $article->getRating());
        $ratingStars->v('story_votes', $article->getVotes());
        $t->v('story_rating_stars', $ratingStars->get());
        $this->setMenuIsActive($t, 'article');
        $this->setPageVars($t);
        $this->setCategories($t);

        return $this->fixEncoding($t->get());
    }

    public function articles()
    {
        $t = $this->template;
        $articles = new Articles;
        $articles->setLimit(10);
        $articles->setDomain($this->getDomain()->getDomain());
        $articles->setOrder('stories.apply_date desc');
        $articles->setListTemplate('stories/item.htm');
        $articles->setPaginationUrlPattern('/articles/(:num)');
        $t->v('stories_list_all', F::cache($articles, 'getList'));
        $t->v('pornorubka_pages_stories_list_all', F::cache($articles, 'getPages'));
        $t->v('xrest_pages_stories_list_all', F::cache($articles, 'getPages'));
        $this->setMenuIsActive($t, 'articles');
        $this->setPageVars($t);
        $this->setCategories($t);

        return $this->fixEncoding($t->get());
    }

    public function categories()
    {
        $t = $this->template;
        // категории
        $categories = $this->domain->categories();
        $categories->setDomainActive(true);
        $categories->setType('default');
        $categories->setListTemplate('li/item_categories_list_with_pic.htm');
        $categories->setWithVideos(true);
        $categories->setCalcVideoThumb(true);
        $t->v('categories_list_with_pic', F::cache($categories, 'getList'));
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setMenuIsActive($t, 'categories_list');

        return $this->fixEncoding($t->get());
    }

    public function top()
    {
        $t = $this->template;
        $videos = $this->getDomain()->videos();
        $videos->setListTemplate('li/thumbs.htm');
        $videos->setOrder('mv');
        $videos->setLimit(100);
        $t->v('xrest_top100', F::cache($videos, 'getList'));
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setMenuIsActive($t, 'top');

        return $this->fixEncoding($t->get());
    }

    public function guestbook()
    {
        $t = $this->getTemplate();
        $pageComments = new PageComments;
        $pageComments->setDomain($this->getDomain()->getDomain());
        $pageComments->setPagePath('guestbook/page');
        $pageComments->setActive(true);
        $pageComments->setListTemplate('guestbook/post.htm');
        $pageComments->setOrder('asc');
        $t->v('xrest_guestbook_posts', $pageComments->getList());
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setMenuIsActive($t, 'guestbook');

        return $this->fixEncoding($t->get());
    }

    public function galleries($args = [])
    {
        $page = $args['page'] ?? 1;
        $t = $this->getTemplate();
        $galleries = new Galleries;
        $galleries->setOrder('galleries.apply_date desc');
        $galleries->setPage($page);
        $galleries->setDomain($this->getDomain()->getDomain());
        $galleries->setListTemplate('gallery/item.htm');
        $galleries->setLimit($this->getDomain()->getGalleriesLimit());
        $t->v('galleries_list_all', $galleries->getList());
        $pagination = new Template;
        if (! $this->getDomain()->getGalleriesPagination()) {
            F::error('set galleriesPagination');
        }
        $pagination->setContent($this->getDomain()->getGalleriesPagination());
        // $pagination->v('',$galleries->getOrder());
        $galleries->setPaginationUrlPattern($pagination->get());
        $t->v('xrest_pages_galleries_list_all', $galleries->getPages());
        $t->v('page_number', $galleries->getPage());
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setMenuIsActive($t, 'galleries');

        return $this->fixEncoding($t->get());
    }

    public function gallery($args = [])
    {
        $titleTranslit = $args['titleTranslit'] ?? null;
        $page = $args['page'] ?? 1;
        $t = $this->getTemplate();
        $galleries = new Galleries;
        $galleries->setDomain($this->getDomain()->getDomain());
        $galleries->setTitleTranslit($titleTranslit);
        $arr = $galleries->get();
        if (! $arr) {
            F::error('Gallery not found');
        }
        $gallery = new Gallery($arr[0]['id']);
        $gallery->setFilesListTemplate('gallery/li_pic.htm');
        $t->v('url_get(gallery_id)', $gallery->getId());
        $t->v('gallery_title', $gallery->getTitle());
        $t->v('gallery_description', $gallery->getDescription());
        $files = $gallery->getFiles();
        $t->v('gallery_first_file', empty($files[0]) ? '' : ($files[0]->name));
        $t->v('gallery_pics_files', $gallery->getFilesList());
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setMenuIsActive($t, 'gallery');

        return $this->fixEncoding($t->get());
    }

    public function news()
    {
        $t = $this->template;
        $arr = F::query_arr('
			select date,name,text
			from '.F::typetab('site_news').'
			where domain = \''.F::checkstr($this->getDomain()->getDomain()).'\'
			order by date desc
			;');
        $list = '';
        foreach ($arr as $v) {
            $tList = new Template('site_news/post.htm');
            $tList->v('date', $v['date']);
            $tList->v('text', $v['text']);
            $tList->v('name', $v['name']);
            $list .= $tList->get();
        }
        $t->v('site_news_posts', $list);
        $this->setPageVars($t);
        $this->setCategories($t);
        $this->setMenuIsActive($t, 'news');

        return $this->fixEncoding($t->get());
    }

    private function getPageHandlers($getPage = null)
    {
        // Engine::setDebug(false);
        if ($getPage == 'comments/submit') {
            return $this->submitAddComment();
        }
        // /?page=submit_likes&type=video&video_id='+video_id+'&action='+action
        if ($getPage == 'submit_likes') {
            return $this->submitLike();
        }
        if ($getPage == 'page_comments/submit') {
            return $this->submitContact();
        }
        if ($getPage == 'ajax/videos_list_all') {
            $args['page'] = $_GET['p'] ?? 1;
            $args['order'] = $_GET['sort'] ?? 'mr';
            $this->setTemplate(new Template('ajax/videos_list_all.htm'));

            return $this->fixEncoding($this->indexHandler($args)->get());
        }
        if ($getPage == 'ajax/videos_list_category') {
            $args['page'] = $_GET['p'] ?? 1;
            $args['order'] = $_GET['sort'] ?? 'mr';
            $args['categoryId'] = $_GET['category_id'] ?? null;
            $this->setTemplate(new Template('ajax/videos_list_category.htm'));

            return $this->category($args);
        }
    }

    protected function ajaxVideosList($args = [])
    {
        $template = $args['template'] ?? F::error('Template required to use ajaxVideosList');
        $categoryId = $_GET['category_id'] ?? null;
        $page = $_GET['p'] ?? 1;
        $order = $_GET['sort'] ?? 'mr';
        $t = new Template($template);

        return $t->get();
    }

    protected function setPageVars($t = null)
    {
        F::setPageVars($t);
        $t->v('http_current_domain', 'http://'.Engine::getRequestedDomainObject()->getDomain());
        $t->v('current_domain', $this->domain->getDomain());
        $t->v('config_page', str_replace('.htm', '', $t->getPath()));
        $t->v('text_top', $t->getVar('topText'));
        $t->v('text_bottom', $t->getVar('bottomText'));
        $t->v('DESCRIPTION', $t->getVar('description'));
        $t->v('csp', '');
        $t->v('rkn_block', '');
        $t->v('rkn_block_js', '');
        $t->v('server_time', date('H:i'));
        $t->v('runing_time', Engine::getRunningTime());
        $t->v('copyright_years(2008)', '2008-'.date('Y'));
        $t->v('copyright_years(2010)', '2010-'.date('Y'));
        $t->v('query_count', Db::getQueryCount());
        $t->v('bread', Engine::router()->getBread());
    }

    protected function getOrderNameRu($order = null)
    {
        $orders = [
            'mv' => 'Популярное',
            'mr' => 'Новое',
            'tr' => 'Топ рейтинга',
            'mc' => 'Обсуждаемое',
        ];
        if (empty($orders[$order])) {
            F::error('Сортировка не сущесвует');
        }

        return $orders[$order];
    }

    protected function getOrderNameEn($order = null)
    {
        $orders = [
            'mv' => 'Popular',
            'mr' => 'New',
            'tr' => 'Top rated',
            'mc' => 'Top rated',
        ];
        if (empty($orders[$order])) {
            F::error('Сортировка не сущесвует');
        }

        return $orders[$order];
    }

    protected function setMenuIsActive($t = null, $pageName = null)
    {
        $pages = ['index', 'categories_list', 'guestbook', 'about', 'rss'];
        foreach ($pages as $v) {
            $result = '';
            if ($v == $pageName) {
                $result = 'class="active"';
            }
            $t->v('menu_is_active('.$v.')', $result);
        }
    }

    protected function setSortIsActive($t = null, $order = null)
    {
        $orders = ['mr', 'mv', 'tr'];
        foreach ($orders as $v) {
            $result = '';
            if ($v == $order) {
                $result = 'class="active"';
            }
            $t->v('sort_is_active('.$v.')', $result);
        }
    }

    protected function setCategories($t = null)
    {
        // категории
        $categories = $this->domain->categories();
        $categories->setDomainActive(true);
        $categories->setType('default');
        $categories->setListTemplate('li/categories_list.htm');
        $categories->setWithVideos(true);
        $categories->setActiveCategory($this->active_category);
        $t->v('categories_list_new', F::cache($categories, 'getList'));
        $t->v('categories_list_without_empty', F::cache($categories, 'getList'));
        // звезды
        $categories = $this->domain->categories();
        $categories->setDomainActive(true);
        $categories->setType('star');
        $categories->setListTemplate('li/item_stars_list.htm');
        $categories->setWithVideos(true);
        $categories->setActiveCategory($this->active_category);
        $t->v('stars_list', F::cache($categories, 'getList'));
    }

    protected function setTags($t = null)
    {
        $tags = new Tags;
        $tags->setDomain($this->getDomain()->getDomain());
        $tags->setOrder('count(distinct videos.id) desc');
        $tags->setListTemplate('li/item_tags_block.htm');
        $tags->setCalcVideosRows(true);
        $tags->setLimit(50);
        $t->v('cache(tags_list)', F::cache($tags, 'getList'));
    }

    protected function fixEncoding($str = null)
    {
        if ($this->getEncoding() == 'windows-1251') {
            return $this->to1251($str);
        }

        return $str;
    }

    protected function to1251($t = null)
    {
        header('Content-Type: text/html; charset=windows-1251');

        return mb_convert_encoding($t, 'windows-1251');
    }

    protected function getDomain()
    {
        return $this->domain;
    }

    protected function getEncoding()
    {
        return $this->encoding;
    }

    protected function getActiveCategory()
    {
        return $this->active_category;
    }

    protected function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    protected function setEncoding($encoding = null)
    {
        $this->encoding = $encoding;
    }

    protected function setActiveCategory($active_category = null)
    {
        $this->active_category = $active_category;
    }

    protected function getTemplate()
    {
        return $this->template;
    }

    protected function setTemplate($template = null)
    {
        $this->template = $template;
    }
}
