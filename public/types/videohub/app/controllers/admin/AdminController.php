<?php

namespace videohub\app\controllers\admin;

use engine\app\models\Domains;
use engine\app\models\Engine;
// use engine\app\models\Cache;
use engine\app\models\F;
use engine\app\models\Template;
// use engine\app\models\ControllerMethod;
use videohub\app\controllers\VideohubAuthorizationController;
use videohub\app\models\Domain;
use videohub\app\models\Videohub;

class AdminController
{
    private $auth = null;

    public function __construct()
    {
        $this->auth = new VideohubAuthorizationController(['root', 'admin']);
    }

    public function index()
    {
        return $this->domains();
    }

    public function domains()
    {
        $videohub = new Videohub;
        $domain = $videohub->getDomains();
        $t = new Template('v2/admin/domains/page');
        $t = $this->setMenuVars($t);
        $list = '';
        foreach ($domain as $v) {
            $tBut = new Template('v2/admin/domains/domainButton');
            $tBut->v('domain', $v['domain']);
            $tBut->v('lastUpdateDate', '?');
            $list .= $tBut->get();
        }
        $t->v('domainList', $list);

        return $t->get();
    }

    public function setMenuVars($t = null)
    {
        $arr['Index'] = ['admin\\Admin\\index'];
        $arr['VideoBase'] = [
            'admin\\VideoBase\\index',
        ];

        return F::setMenuActiveVars($t, Engine::router(), $arr);
    }

    public function domainsSettings()
    {
        $domains = new Domains;
        $domains->setType('videohub');
        $domains->setReborn(false);
        $arr = $domains->get();
        // F::dump($domains);
        $list = '';
        $i = 0;
        foreach ($domains->get() as $v) {
            $domain = new Domain($v['domain']);
            $lastVideoUpdateDate = $domain->lastVideoUpdateDate();
            $t = new Template('v2/admin/domains/list_domain_settings');
            $t->v('lastVideoUpdateDate', $lastVideoUpdateDate ? $lastVideoUpdateDate : 'нет обновлений');
            $t->v('domain', $domain->getDomain());
            $t->v('urlDomain', 'http://'.$domain->getDomain());
            $t->v('videosPerUpdate', $domain->getVideosPerUpdate());
            $t->v('comment', nl2br($domain->getComment()));
            $t->v('commentEncoded', htmlspecialchars($domain->getComment()));
            $t->v('i', $i++);
            $t->v('ruBlock', $domain->getRuBlock() ? '<span class="ruBlock">ркн блок</span>' : '');
            $t->v('expired', $domain->getExpired() ? '<span class="ruBlock">удален</span>' : '');
            $mirrors = $domain->getMirrors();
            $mlist = '';
            foreach ($mirrors as $m) {
                $d = new Domain($m['domain']);
                $mlist .= '<a href="http://'.$m['domain'].'">'.$m['domain'].'</a> 
				'.($d->getRuBlock() ? '<span class="ruBlock">ркн блок</span>' : '').'
				'.($d->getExpired() ? '<span class="ruBlock">удален</span>' : '').'
				<br>';
            }
            $t->v('mirrors', $mlist);
            $list .= $t->get();
            // dump($domain);
        }
        $t = new Template('v2/admin/domains/domains_settings');
        $t = $this->setMenuVars($t);
        $t->v('list', $list);

        return $t->get();
    }

    public function editDomainComment()
    {
        $comment = empty($_POST['comment']) ? F::error('Comment is required') : F::checkstr($_POST['comment']);
        $domain = empty($_POST['domain']) ? F::error('Domain is required') : F::checkstr($_POST['domain']);
        $id = empty($_POST['id']) ? F::error('Id is required') : F::checkstr($_POST['id']);
        $d = new Domain($domain);
        $d->setComment($comment);
        $d->save();
        F::redirect('/?r=admin/Admin/domainsSettings#'.$id);
    }
}
