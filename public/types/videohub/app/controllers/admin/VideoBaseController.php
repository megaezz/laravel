<?php

namespace videohub\app\controllers\admin;

use engine\app\models\Domains;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use videohub\app\controllers\VideohubAuthorizationController;
use videohub\app\models\CategoriesWriter;
use videohub\app\models\DomainCategoriesWriter;
use videohub\app\models\Video;
use videohub\app\models\Videohub;
use videohub\app\models\VideoPage;
use videohub\app\models\Videos;
use videohub\app\models\VideosPage;

class VideoBaseController extends AdminController
{
    public function getDomainsSelect()
    {
        $arr = (new Videohub)->getDomains();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<option value="'.$v['domain'].'">'.$v['domain'].'</option>';
        }

        return $list;
    }

    public function getRebornDomainsSelect()
    {
        $domains = new Domains;
        $domains->setType('videohub');
        $domains->setReborn(true);
        $arr = $domains->get();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<option value="'.$v['domain'].'">'.$v['domain'].'</option>';
        }

        return $list;
    }

    public function getBookedDomainsSelect()
    {
        $arr = (new Videohub)->getDomains();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<option value="'.$v['domain'].'">'.$v['domain'].'</option>';
        }

        return $list;
    }

    public function videos()
    {
        $t = new Template('admin/videos/videos');
        $vp = new VideosPage;
        $vp->setlistTemplate('admin/videos/list');
        $t->v('getPagesBootstrap', $vp->getPagesButtons());
        $t->v('list', $vp->list());
        $t->v('getRows', $vp->getRows());

        return $t->get();
        // temp_var('getPagesBootstrap',$this->getPagesBootstrap());
        // temp_var('list',$this->list());
        // temp_var('getRows',$this->getRows());
        // return template('admin/videos/videos');
    }

    public function getCategoriesSelect()
    {
        $categories = new categoriesWriter;

        return $categories->getSelectList();
    }

    public function getDomainCategoriesSelect()
    {
        $domain = empty($_POST['domain']) ? null : F::checkstr($_POST['domain']);
        if (! $domain) {
            return false;
        }
        $categories = new DomainCategoriesWriter($domain);
        // $categories->setCalcVideosRows(false);
        $t = new Template('admin/videos/domain_categories');
        $t->v('getDomainCategoriesSelect', $categories->getSelectList());
        $t->v('getDomain', $domain);

        return $t->get();
    }

    public function getOrdersSelect()
    {
        $ru = [
            'mr' => 'Дате добавления на сайт',
            'mv' => 'Просмотрам',
            'mc' => 'Комметариям',
            'tr' => 'Рейтингу',
            'ms' => 'Схожести с выбранным видео',
            'grabber date' => 'Дате граббера',
            'title date' => 'Дате написания заголовка',
            'description date' => 'Дате написания описания',
        ];
        $arr = videos::getOrders();
        unset($arr['ms']);
        $list = '';
        foreach ($arr as $k => $v) {
            $list .= '<option value="'.$k.'">'.(empty($ru[$k]) ? $v : $ru[$k]).'</option>';
        }

        return $list;
    }

    public function index()
    {
        $vp = new VideosPage;
        $t = new Template('admin/videos/index');
        $t = $this->setMenuVars($t);
        $t->v('getDomainsSelect', $this->getDomainsSelect());
        $t->v('getRebornDomainsSelect', $this->getRebornDomainsSelect());
        $t->v('getBookedDomainsSelect', $this->getBookedDomainsSelect());
        $t->v('getOrdersSelect', $this->getOrdersSelect());
        $t->v('getCategoriesSelect', $this->getCategoriesSelect());
        $t->v('getID', $vp->getID());
        $t->v('getLimit', $vp->getLimit());
        $t->v('getPage', $vp->getPage());
        $t->v('reload', Engine::getReload());

        return $t->get();
    }

    public function changeDomainForm()
    {
        $t = new Template('admin/videos/change_domain_form');
        $t->v('getDomainsSelect', $this->getDomainsSelect());

        return $t->get();
    }

    public function changeBookedDomainForm()
    {
        $t = new Template('admin/videos/change_booked_domain_form');
        $t->v('getDomainsSelect', $this->getDomainsSelect());

        return $t->get();
    }

    public function submit_change_domain()
    {
        // это действие может выполнять только рут пользователь
        new VideohubAuthorizationController(['root']);
        if (! isset($_POST['changeDomain'])) {
            F::error('changeDomain is required to submit change domain');
        }
        $domain = empty($_POST['changeDomain']['domain']) ? null : F::checkstr($_POST['changeDomain']['domain']);
        $vp = new VideosPage;
        // dump($vp->getVideos());
        $arr = $vp->get();
        $i = 0;
        foreach ($arr as $v) {
            $video = new Video($v['id']);
            $b = $video->setDomain($domain);
            // если домен установлен и передан домен не null то выводим предупреждение, что сначала нужно обнулить домен. для предотвращения случайной смены домена
            if (! $b) {
                exit('Task is stopped on video #'.$v['id'].'. You can change domain only for videos with null domain for security reasons. Please first change domain to null.');
            }
            $i++;
        }
        exit('Changed domain of '.$i.' videos');
    }

    public function submit_change_booked_domain()
    {
        new VideohubAuthorization(['root']);
        if (! isset($_POST['changeBookedDomain'])) {
            F::error('changeBookedDomain is required to submit change domain');
        }
        $domain = empty($_POST['changeBookedDomain']['domain']) ? F::error('Domain for change required') : F::checkstr($_POST['changeBookedDomain']['domain']);
        $vp = new VideosPage;
        $arr = $vp->get()->array;
        $i = 0;
        foreach ($arr as $v) {
            $video = new video($v['id']);
            $video->setBookedDomain($domain);
            $i++;
        }
        exit('Changed booked domain of '.$i.' videos');
    }

    public function freeVideosStat()
    {
        $t = new Template('admin/videos/free_videos_stat');
        $videos = new Videos;
        $videos->setPage(1);
        $videos->setLimit(1);
        $videos->setWithDomain(false);
        $videos->setWithTitle(true);
        $videos->setWithDescription(false);
        $t->v('getFreeVideosWithTitleWithoutDescription', $videos->getRows());
        $videos = new Videos;
        $videos->setPage(1);
        $videos->setLimit(1);
        $videos->setWithDomain(false);
        $videos->setWithTitle(true);
        $videos->setWithDescription(true);
        $t->v('getFreeVideosWithTitleAndDescription', $videos->getRows());
        $videos = new Videos;
        $videos->setPage(1);
        $videos->setLimit(1);
        $videos->setWithDomain(false);
        $videos->setWithBookedDomain(false);
        $videos->setWithTitle(true);
        $videos->setWithDescription(false);
        $t->v('getFreeNotBookedVideosWithTitleWithoutDescription', $videos->getRows());
        $videos = new Videos;
        $videos->setPage(1);
        $videos->setLimit(1);
        $videos->setWithDomain(false);
        $videos->setWithBookedDomain(false);
        $videos->setWithTitle(true);
        $videos->setWithDescription(true);
        $t->v('getFreeNotBookedVideosWithTitleAndDescription', $videos->getRows());

        // return template('admin/videos/free_videos_stat');
        return $t->get();
    }

    public function getInternalPlayer()
    {
        $video = new VideoPage;

        return $video->getInternalPlayer();
    }

    public function getVideoInfo()
    {
        $video = new VideoPage;

        return '<pre>'.print_r($video).'</pre>';
    }
}
