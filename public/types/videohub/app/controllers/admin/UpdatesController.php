<?php

namespace videohub\app\controllers\admin;

use videohub\app\controllers\VideohubAuthorizationController;

class UpdatesController
{
    private $auth = null;

    public function __construct()
    {
        $this->auth = new VideohubAuthorizationController(['root', 'admin']);
    }
}
