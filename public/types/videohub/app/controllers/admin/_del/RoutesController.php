<?php

namespace videohub\app\controllers\admin;

use engine\app\models\ControllerMethod;
use engine\app\models\Domain;
use engine\app\models\SiteRoute;
use engine\app\models\Template;

class RoutesController extends AdminController
{
    public function route()
    {
        $domain = empty($_GET['domain']) ? F::error('Domain required') : $_GET['domain'];
        $pattern = empty($_GET['pattern']) ? null : $_GET['pattern'];
        $t = new Template('admin/routes/routes/page');
        $t = $this->setMenuVars($t);
        $t->v('domain', $domain);
        $domainObj = new Domain($domain);
        // dump($domainObj->routes());
        $list = '';
        foreach ($domainObj->routes()->get() as $v) {
            $active = ($pattern === $v['pattern']) ? 'active' : '';
            $list .= '
			<li class="nav-item mb-2">
			<a class="btn btn-light '.$active.'" href="/?r=admin/Routes/route&amp;domain='.$domain.'&amp;pattern='.urlencode($v['pattern']).'">'.$v['pattern'].'</a>
			</li>
			';
        }
        $t->v('patterns', $list);
        $t->v('forms', null);
        if ($pattern) {
            $tForms = new Template('admin/routes/routes/forms');
            $route = new SiteRoute($domain, $pattern);
            $tForms->v('domain', $route->getDomain());
            $tForms->v('pattern', $route->getPattern());
            $tForms->v('alias', $route->getAlias());
            $tForms->v('cache', $route->getCache());
            $tForms->v('controller', $route->getController());
            $tForms->v('var1', $route->getVar1());
            $tForms->v('var2', $route->getVar2());
            $tForms->v('var3', $route->getVar3());
            $tForms->v('var4', $route->getVar4());
            $tForms->v('method', $route->getMethod());
            $tForms->vf('title', $route->getTitle());
            $tForms->vf('description', htmlspecialchars($route->getDescription()));
            $tForms->vf('keywords', $route->getKeywords());
            $tForms->vf('h1', $route->getH1());
            $tForms->vf('topText', htmlspecialchars($route->getTopText()));
            $tForms->vf('bottomText', htmlspecialchars($route->getBottomText()));
            $tForms->vf('bread', htmlspecialchars($route->getBread()));
            $tForms->vf('text1', htmlspecialchars($route->getText1()));
            $tForms->vf('text2', htmlspecialchars($route->getText2()));
            $tForms->vf('h2', $route->getH2());
            $tForms->v('template', $route->getTemplate());
            $tForms->v('comment', htmlspecialchars($route->getComment()));

            if ($route->getMethod() and $route->getController()) {
                $methodID = ControllerMethod::getIDbyTypeControllerMethod(
                    'videohub',
                    $route->getController(),
                    $route->getMethod()
                );
                if ($methodID) {
                    $method = new ControllerMethod($methodID);
                    $tMethod = new Template('admin/routes/routes/methodDescription');
                    $tMethod->vf('text', nl2br($method->getText()));
                    $tMethod->v('controller', $method->getController());
                    $tMethod->v('method', $method->getMethod());
                    $tMethod->v('type', $method->getType());
                    $tForms->v('methodDescriptionButton', '
						<button class="btn" type="button" data-toggle="collapse" data-target="#methodDescription">
						Доступно описание метода
						</button>
						');
                    $tForms->v('methodDescriptionContent', $tMethod->get());
                }
            }

            if (! $tForms->isSet('methodDescriptionButton')) {
                $tForms->v('methodDescriptionButton', '');
                $tForms->v('methodDescriptionContent', '');
            }

            // $tForms->v('section',$route->getSection());
            // $t->v('',$route->get());
            $t->v('forms', $tForms->get());
        }

        return $t->get();
    }

    public function editRoute()
    {
        $patternID = empty($_POST['patternID']) ? F::error('PatternID required') : $_POST['patternID'];
        // dump($patternID);
        $domain = empty($_POST['domain']) ? F::error('Domain required') : $_POST['domain'];
        $r = new SiteRoute($domain, $patternID);
        if (isset($_POST['pattern'])) {
            $r->setPattern($_POST['pattern']);
        }
        if (isset($_POST['alias'])) {
            $r->setAlias($_POST['alias']);
        }
        if (isset($_POST['cache'])) {
            $r->setCache($_POST['cache']);
        }
        if (isset($_POST['controller'])) {
            $r->setController($_POST['controller']);
        }
        if (isset($_POST['method'])) {
            $r->setMethod($_POST['method']);
        }
        if (isset($_POST['title'])) {
            $r->setTitle($_POST['title']);
        }
        if (isset($_POST['description'])) {
            $r->setDescription($_POST['description']);
        }
        if (isset($_POST['keywords'])) {
            $r->setKeywords($_POST['keywords']);
        }
        if (isset($_POST['h1'])) {
            $r->setH1($_POST['h1']);
        }
        if (isset($_POST['topText'])) {
            $r->setTopText($_POST['topText']);
        }
        if (isset($_POST['bottomText'])) {
            $r->setBottomText($_POST['bottomText']);
        }
        if (isset($_POST['bread'])) {
            $r->setBread($_POST['bread']);
        }
        if (isset($_POST['text1'])) {
            $r->setText1($_POST['text1']);
        }
        if (isset($_POST['text2'])) {
            $r->setText2($_POST['text2']);
        }
        if (isset($_POST['h2'])) {
            $r->setH2($_POST['h2']);
        }
        if (isset($_POST['template'])) {
            $r->setTemplate($_POST['template']);
        }
        if (isset($_POST['var1'])) {
            $r->setVar1($_POST['var1']);
        }
        if (isset($_POST['var2'])) {
            $r->setVar2($_POST['var2']);
        }
        if (isset($_POST['var3'])) {
            $r->setVar3($_POST['var3']);
        }
        if (isset($_POST['var4'])) {
            $r->setVar4($_POST['var4']);
        }
        if (isset($_POST['comment'])) {
            $r->setComment($_POST['comment']);
        }
        // if (isset($_POST['section'])) {$r->setSection($_POST['section']);}
        // dump($r);
        $r->save();
        // dump($r->getPattern());
        F::redirect('/?r=admin/Routes/route&domain='.$r->getDomain().'&pattern='.urlencode($r->getPattern()));
        // dump($route);

        // dump($_POST);
    }

    public function addPattern()
    {
        $domain = empty($_POST['domain']) ? F::error('Domain required') : $_POST['domain'];
        $pattern = empty($_POST['pattern']) ? F::error('Pattern required') : $_POST['pattern'];
        SiteRoute::add($domain, $pattern);
        $r = new SiteRoute($domain, $pattern);
        F::redirect('/?r=admin/Routes/route&domain='.$r->getDomain().'&pattern='.urlencode($r->getPattern()));
    }

    public function deletePattern()
    {
        $domain = empty($_POST['domain']) ? F::error('Domain required') : $_POST['domain'];
        $pattern = empty($_POST['pattern']) ? F::error('Pattern required') : $_POST['pattern'];
        SiteRoute::delete($domain, $pattern);
        F::redirect('/?r=admin/Routes/route&domain='.$domain);
    }
}
