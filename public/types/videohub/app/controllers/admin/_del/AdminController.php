<?php

namespace videohub\app\controllers\admin;

use App\Models\Config;
use engine\app\models\Cache;
use engine\app\models\ControllerMethod;
use engine\app\models\Domain;
use engine\app\models\Engine;
use engine\app\models\Mirrors;
use engine\app\models\Template;
use videohub\app\controllers\VideohubAuthorizationController;
use videohub\app\models\Videohub;

class AdminController
{
    private $auth = null;

    public function __construct()
    {
        $this->auth = new VideohubAuthorizationController(['root', 'admin']);
    }

    public function engineSettings()
    {
        if (isset($_POST)) {

        }
        $t = new Template('admin/settings/page');
        $t = $this->setMenuVars($t);
        // $config = (new Videohub)->config();
        // dump($config);
        // $t->setVars($config);
        $t->v('sessionTimeout', Config::cached()->sessionTimeout);
        $t->v('switch', Config::cached()->switch);
        $t->v('reload', Engine::getReload());
        $t->v('displayErrors', Engine::getDisplayErrors());
        $t->v('logLongQueries', Config::cached()->logLongQueries);
        $t->v('cacheLifetime', Config::cached()->cacheLifetime);
        $t->v('cacheUseDB', Config::cached()->cacheUseDB);
        $t->v('routerCache', Config::cached()->routerCache);
        $t->v('debug', Engine::getDebug());
        $t->v('proxiesEnabled', Config::cached()->proxiesEnabled);

        // $t->v('',Engine::get());
        // $t->v('',Engine::get());
        // $t->v('',Engine::get());
        return $t->get();
    }

    public function mirrorsList()
    {
        $t = new Template('admin/mirrors/page');
        $t = $this->setMenuVars($t);
        $mirrors = new Mirrors;
        $arr = $mirrors->get();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<li class="list-group-item"><button class="btn btn-light">'.$v['from'].'</button> => <button class="btn btn-light">'.$v['to'].'</button></li>';
        }
        $t->v('mirrorsList', $list);

        return $t->get();
    }

    public function resetDomainCache()
    {
        $domain = empty($_GET['domain']) ? F::error('Specify domain to reset cache') : F::checkstr($_GET['domain']);
        $cache = new Cache;
        $cache->setDomain($domain);
        $cache->resetDomain();
        F::alert('Cache for domain '.$domain.' has been deleted');
    }

    public function domainSettings()
    {
        $domain = empty($_GET['domain']) ? F::error('Specify domain to domainSettings') : F::checkstr($_GET['domain']);
        $domainObj = new Domain($domain);
        $t = new Template('admin/domain/settings');
        $t = $this->setMenuVars($t);
        if (! empty($_POST)) {
            if (isset($_POST['type'])) {
                $domainObj->setType(F::checkstr($_POST['type']));
            }
            if (isset($_POST['on'])) {
                $domainObj->setIsOn(F::checkstr($_POST['on']) ? true : false);
            }
            if (isset($_POST['domain'])) {
                $domainObj->setDomain(F::checkstr($_POST['domain']));
            }
            if (isset($_POST['auth'])) {
                $domainObj->setAuth(F::checkstr($_POST['auth']) ? true : false);
            }
            if (isset($_POST['www'])) {
                $domainObj->setWww(F::checkstr($_POST['www']));
            }
            if (isset($_POST['httpsOnly'])) {
                $domainObj->setHttpsOnly(F::checkstr($_POST['httpsOnly']) ? true : false);
            }
            $domainObj->save();
            $t->v('submitAnswer', 'Изменения сохранены');
            F::redirect('/?r=admin/Admin/domainSettings&domain='.$domainObj->getDomain());
        }
        if (! $t->isset('submitAnswer')) {
            $t->v('submitAnswer', '');
        }
        $types = Engine::getTypes();
        $list = '';
        foreach ($types as $v) {
            $selected = ($v === $domainObj->getType()) ? 'selected' : '';
            $list .= '<option value="'.$v.'" '.$selected.'>'.$v.'</option>';
        }
        $t->v('typeOptionsList', $list);
        $arrOn = [true, false];
        $list = '';
        foreach ($arrOn as $v) {
            $selected = ($v === $domainObj->getIsOn()) ? 'selected' : '';
            $list .= '<option value="'.($v ? 1 : 0).'" '.$selected.'>'.($v ? 'Вкл' : 'Выкл').'</option>';
        }
        $t->v('onOptionsList', $list);
        $arrAuth = [true, false];
        $list = '';
        foreach ($arrAuth as $v) {
            $selected = ($v === $domainObj->getAuth()) ? 'selected' : '';
            $list .= '<option value="'.($v ? 1 : 0).'" '.$selected.'>'.($v ? 'Да' : 'Нет').'</option>';
        }
        $t->v('authOptionsList', $list);
        $arrWww = ['всегда www' => 'www_only', 'можно с www и без' => 'on', 'всегда без www' => 'off'];
        $list = '';
        foreach ($arrWww as $k => $v) {
            $selected = ($v === $domainObj->getWww()) ? 'selected' : '';
            $list .= '<option value="'.$v.'" '.$selected.'>'.$k.'</option>';
        }
        $t->v('wwwOptionsList', $list);
        $arrHttpsOnly = [true, false];
        $list = '';
        foreach ($arrHttpsOnly as $v) {
            $selected = ($v === $domainObj->getHttpsOnly()) ? 'selected' : '';
            $list .= '<option value="'.($v ? 1 : 0).'" '.$selected.'>'.($v ? 'Да' : 'Нет').'</option>';
        }
        $t->v('httpsOnlyOptionsList', $list);
        $t->v('domain', $domainObj->getDomain());

        return $t->get();
    }

    public function deleteDomain()
    {
        $domain = empty($_POST['domain']) ? F::error('Specify domain to addDomain') : F::checkstr($_POST['domain']);
        $object = new Domain($domain);
        $object->delete();
        F::redirect('/?r=admin/Admin/domains');
    }

    public function addDomain()
    {
        $domain = empty($_POST['domain']) ? F::error('Specify domain to addDomain') : F::checkstr($_POST['domain']);
        Domain::add($domain);
        $object = new Domain($domain);
        $object->setType('videohub');
        $object->save();
        F::redirect(F::referer_url());
    }

    public function index()
    {
        return $this->domains();
    }

    public function domains()
    {
        $videohub = new Videohub;
        $domain = $videohub->getDomains();
        $t = new Template('admin/domains/page');
        $t = $this->setMenuVars($t);
        $list = '';
        foreach ($domain as $v) {
            $tBut = new Template('admin/domains/domainButton');
            $tBut->v('domain', $v['domain']);
            $list .= $tBut->get();
        }
        $t->v('domainList', $list);

        return $t->get();
    }

    public function setMenuVars($t = null)
    {
        $arr['Index'] = ['admin\\Admin\\index'];
        $arr['Routes'] = [
            'admin\\Routes\\domains',
            'admin\\Routes\\route',
        ];
        $arr['VideoBase'] = [
            'admin\\VideoBase\\index',
        ];
        $arr['Methods'] = [
            'admin\\Admin\\controllerMethods',
        ];
        $arr['Settings'] = ['admin\\Admin\\engineSettings'];

        return F::setMenuActiveVars($t, Engine::router(), $arr);
    }

    public function controllerMethods()
    {
        $methodID = empty($_GET['methodID']) ? null : F::checkstr($_GET['methodID']);
        $t = new Template('admin/methods/page');
        $t = $this->setMenuVars($t);
        $list = '';
        foreach (ControllerMethod::getAll('videohub') as $v) {
            $method = new ControllerMethod($v['id']);
            $active = ($methodID === $v['id']) ? 'active' : '';
            $list .= '<li class="nav-item mb-2">
			<a class="btn btn-light '.$active.'" href="/?r=admin/Admin/controllerMethods&amp;methodID='.$method->getID().'">
			'.$method->getController().'::'.$method->getMethod().'
			</a>
			</li>';
        }
        $t->v('methodsList', $list);
        if ($methodID) {
            $method = new ControllerMethod($methodID);
            $t->vf('text', nl2br($method->getText()));
        } else {
            $t->v('text', 'Выберите метод');
        }

        return $t->get();
    }
}
