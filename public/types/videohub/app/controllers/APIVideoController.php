<?php

namespace videohub\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;
use videohub\app\models\Domain;

class APIVideoController
{
    public function getVideos()
    {
        $page = empty($_GET['p']) ? F::error('Page num required to main/getVideos') : F::checkstr($_GET['p']);
        $order = empty($_GET['order']) ? F::error('Order required to main/getVideos') : F::checkstr($_GET['order']);
        $categoryID = empty($_GET['categoryID']) ? null : F::checkstr($_GET['categoryID']);
        $tag = empty($_GET['tag']) ? null : F::checkstr($_GET['tag']);
        $domain = new Domain(Engine::getDomain());
        $limit = $domain->config()->videosLimit;
        $videos = $domain->videos();
        $videos->setListTemplate('thumb');
        $videos->setOrder($order);
        $videos->setPage($page);
        $videos->setLimit($limit);
        $videos->setCategory($categoryID);
        if ($tag) {
            $videos->setTags([$tag]);
        }

        // dump($videos->list());
        return F::cache($videos, 'list');
    }
}
