<?php

namespace videohub\app\controllers;

use engine\app\controllers\AuthorizationController;

class VideohubAuthorizationController extends AuthorizationController
{
    public function __construct($levels = [])
    {
        parent::__construct('videohub', $levels);
    }
}
