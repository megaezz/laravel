<?php

namespace MegawebV1;

function get_photo_id_from_local_id($local_id)
{
    global $config;
    $data = query('select id from '.tabname('videohub', 'galleries').' where domain=\''.$config['domain'].'\' and local_id=\''.$local_id.'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('������ �� ������');
    }

    return $arr['id'];
}

function gallery_edit_headers()
{
    if (empty($_POST['gallery_id'])) {
        error('�� ������� ID �������');
    }
    if (empty($_POST['title_ru'])) {
        error('�� ������� ���������');
    }
    if (! isset($_POST['description'])) {
        error('�� �������� ��������');
    }
    query('update '.tabname('videohub', 'galleries').' set title_ru=\''.mysql_escape_string($_POST['title_ru']).'\', title_ru_translit=\''.url_translit($_POST['title_ru']).'\', description=\''.mysql_escape_string($_POST['description']).'\' where id=\''.$_POST['gallery_id'].'\';');
    alert('�������� ��������� ��� ������� '.$_POST['gallery_id']);
}

function delete_gallery_get()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �������');
    }
    if (empty($_GET['confirm'])) {
        alert('������� ������� '.$_GET['id'].'? <a href="/?page=admin/gallery/delete_gallery&amp;id='.$_GET['id'].'&amp;confirm=yes">OK</a>');
    }
    if ($_GET['confirm'] == 'yes') {
        delete_gallery($_GET['id']);
        alert('������� ������� '.$_GET['id'].'. <a href="/?page=admin/gallery/uploaded_photos_moderate">���������</a>');
    }
}

function delete_gallery($id)
{
    global $config;
    if (empty($id)) {
        error('�� ������� ID �������');
    }
    $gallery_path = $config['videohub']['path']['galleries'].$id;
    $gallery_files = list_directory_files($gallery_path);
    foreach ($gallery_files as $v) {
        $file = $gallery_path.'/'.$v;
        $status = unlink($file);
        if (! $status) {
            error('������ �������� ����� �������: '.$file);
        }
    }
    $status = rmdir($gallery_path);
    if (! $status) {
        error('������ �������� �������� �������: '.$gallery_path);
    }
    query('delete from '.tabname('videohub', 'galleries').' where id=\''.$id.'\';');
    query('delete from '.tabname('videohub', 'galleries_tags').' where gallery_id=\''.$id.'\';');
    query('delete from '.tabname('videohub', 'galleries_views').' where gallery_id=\''.$id.'\';');
}

function uploaded_photos_moderate()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $per_page = 20;
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_en,title_ru,description,add_date,apply_date,moderated,ext from '.tabname('videohub', 'galleries').' where
		user_upload=\'1\' and merged=\'0\'
		order by add_date desc
			limit '.$from.','.$per_page);
    $rows = rows_without_limit(); // ����� �������
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $arr_gallery = preparing_view_gallery(['gallery_id' => $v['id'], 'check_domain' => 'no']);
        $q_pics = count($arr_gallery['files']);
        $status_text = '';
        if ($v['moderated'] == '0') {
            $status_text = '<a href="/?page=admin/gallery/moderate_status&amp;gallery_id='.$v['id'].'&amp;status=approve">�������� � �������� �� ����</a>';
        }
        if ($v['moderated'] == '1') {
            $status_text = $v['apply_date'].' <a href="/?page=admin/gallery/moderate_status&amp;gallery_id='.$v['id'].'&amp;status=reject">���������</a>';
        }
        $list .= '<li class="well well-large" style="height: 250px;">
		<table class="table"><tr>
		<td style="width: 160px;">#'.$v['id'].'<a href="/?page=admin/translate/gallery/view_gallery&amp;gallery_id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/galleries/'.$v['id'].'/preview.'.$v['ext'].'" alt="" /></a>
		<br/><a href="/?page=admin/gallery/delete_gallery&amp;id='.$v['id'].'" target="_blank">�������</a>
		</td>
		<td>
		<p>'.$v['title_en'].'</p>
		<p>'.gallery_tags_list_simple($v['id']).'</p>
		<form action="/?page=admin/gallery/edit_headers" method="post">
		<input type="hidden" name="gallery_id" value="'.$v['id'].'" />
		<input type="text" name="title_ru" value="'.$v['title_ru'].'" spellcheck="true" style="width: 90%;" />
		<br/><textarea name="description" spellcheck="true" style="width: 80%; height: 70px;">'.htmlspecialchars($v['description'], ENT_QUOTES, 'cp1251').'</textarea>
		<input type="submit" name="OK" value="OK" />
		</form>
		��������: '.$q_pics.'<br/>
		���������: '.$v['add_date'].'<br/>
		�� �����: '.$status_text.'<br/>
		<form action="/?page=admin/gallery/merge_galleries" method="post">
		<input type="hidden" name="from_id" value="'.$v['id'].'" />
		����������� �: <input type="text" name="to_id" style="width: 50px;" />
		<input type="submit" name="OK" value="OK" />
		</form>
		</td></tr>
		<tr><td colspan="2">

		</td></tr></table>
		</li>
		';
    }
    $content = '
	<ul class="desc">'.$list.'</ul>
	<div class="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/gallery/uploaded_photos_moderate&amp;p=').'</ul><hr/></div>
	';

    return $content;
}

function merge_galleries_post()
{
    if (! isset($_POST['from_id'],$_POST['to_id'])) {
        error('�� �������� ����������� ��������');
    }
    $log = merge_galleries($_POST['from_id'], $_POST['to_id']);
    // alert($log);
    if (! empty($_SERVER['HTTP_REFERER'])) {
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
}

function merge_galleries($from_id, $to_id)
{
    global $config;
    $arr_from = preparing_view_gallery(['gallery_id' => $from_id, 'check_domain' => 'no']);
    $log = '�����������:<br/>';
    foreach ($arr_from['files'] as $v) {
        $from_file = $config['videohub']['path']['galleries'].$from_id.'/'.$v;
        $to_file = $config['videohub']['path']['galleries'].$to_id.'/'.$from_id.$v;
        $status = copy($from_file, $to_file);
        if (! $status) {
            error('������ ����������� '.$from_file.' � '.$to_file);
        }
        $log .= $from_file.' � '.$to_file.'<br/>';
    }
    query('update '.tabname('videohub', 'galleries').' set merged=\'1\' where id=\''.$from_id.'\';');

    return $log;
}

function uploaded_photos_moderate_status()
{
    if (empty($_GET['gallery_id'])) {
        error('�� ������� ID');
    }
    if (empty($_GET['status'])) {
        error('�� ������� ������');
    }
    if ($_GET['status'] == 'approve') {
        $data = query('select id,domain,booked_domain,title_ru_translit from '.tabname('videohub', 'galleries').' where id=\''.$_GET['gallery_id'].'\';');
        $arr = mysql_fetch_assoc($data);
        $data2 = query('select id,title_ru_translit from '.tabname('videohub', 'galleries').' where title_ru_translit=\''.$arr['title_ru_translit'].'\' and merged=\'0\' and id!=\''.$_GET['gallery_id'].'\';');
        $arr2 = mysql_fetch_assoc($data2);
        if (! empty($arr2['title_ru_translit'])) {
            alert('�������� "'.$arr2['title_ru_translit'].'" ��� ���������� � ������� "'.$arr2['id'].'"');
        }
        if (empty($arr['booked_domain'])) {
            query('update '.tabname('videohub', 'galleries').' set moderated=\'1\' where id=\''.$_GET['gallery_id'].'\';');
            alert('������� '.$_GET['gallery_id'].' �������� � �� ��������� �� ����� �� �� ���������� �����');
        } else {
            query('update '.tabname('videohub', 'galleries').' set moderated=\'1\', domain=\''.$arr['booked_domain'].'\', apply_date=current_timestamp where id=\''.$_GET['gallery_id'].'\';');
            alert('������� '.$_GET['gallery_id'].' �������� � ��������� �� ����� '.$arr['booked_domain']);
        }
    }
    if ($_GET['status'] == 'reject') {
        query('update '.tabname('videohub', 'galleries').' set moderated=\'0\', domain=null where id=\''.$_GET['gallery_id'].'\';');
        alert('������� '.$_GET['gallery_id'].' ���������');
    }
}

// �������� ���������, ����� ��������� ���������� ���� � �������
function create_contest_preview()
{
    global $config;
    if (empty($_GET['id'])) {
        error('�� ������� ID �������');
    }
    $id = $_GET['id'];
    $img_name = $config['videohub']['path']['galleries'].$id.'/original.jpg';
    $img_name_preview = $config['videohub']['path']['galleries'].$id.'/preview.jpg';
    $arr = mysql_fetch_assoc(query('select title_ru from '.tabname('videohub', 'galleries').' where id=\''.$id.'\';'));
    query('update '.tabname('videohub', 'galleries').' set title_ru_translit=\''.url_translit($arr['title_ru']).'\' where id=\''.$id.'\';');
    // copytoserv(http_current_domain().'/types/engine/inc/timthumb/timthumb.php?src='.$img_name.'&w=200',$img_name_preview);
}

function submit_add_user_photo()
{
    global $config;
    // pre($_POST);
    // pre($_FILES);
    // if (!isset($_POST['name'],$_POST['mail'],$_FILES['image'])) {error('�� �������� ����������� ������');};
    if (empty($_POST['name']) or empty($_POST['mail']) or empty($_FILES['image'])) {
        alert('�� �������� ����������� ������');
    }
    // if (!isset($_POST['name'],$_POST['mail'],$_POST['img_title'],$_FILES['image'])) {error('�� �������� ����������� ������');};
    if ($_FILES['image']['size'] > (10 * 1024 * 1024)) {
        error('������� ������� ����');
    }
    $img_ext = check_ext(['filename' => $_FILES['image']['name'], 'whitelist' => ['jpg', 'jpeg', 'png']]);
    if (! $img_ext) {
        error('������ �� ��������������');
    }
    $name = checkstr($_POST['name']);
    $mail = checkstr($_POST['mail']);
    if (! empty($_POST['description'])) {
        $description = checkstr($_POST['description']);
    } else {
        $description = '';
    }
    if (! empty($_POST['phone'])) {
        $phone = checkstr($_POST['phone']);
    } else {
        $phone = '';
    }
    // $img_title=checkstr($_POST['img_title']);
    check_email($mail);
    query('insert into '.tabname('videohub', 'galleries').' set
		booked_domain=\''.$config['domain'].'\',
		add_date=current_timestamp,
		title_ru=\''.$name.'\',
		title_ru_translit=\''.url_translit($name).'\',
		mail=\''.$mail.'\',
		ext=\''.$img_ext.'\',
		phone=\''.$phone.'\',
		description=\''.$description.'\',
		user_upload=\'1\'
		;');
    $id = mysql_insert_id();
    $img_name = $config['videohub']['path']['galleries'].$id.'/original.'.$img_ext;
    $img_name_preview = $config['videohub']['path']['galleries'].$id.'/preview.'.$img_ext;
    // $img_name_preview=$config['path']['site'].'contest/photos/'.$id.'_preview.'.$img_ext;
    $dir_name = $config['videohub']['path']['galleries'].$id;
    mkdir($dir_name);
    chmod($dir_name, 0774);
    if (! move_uploaded_file($_FILES['image']['tmp_name'], $img_name)) {
        error('������ ��� �������� ����������.');
    }
    chmod($img_name, 0774);
    copytoserv(http_current_domain().'/types/engine/inc/timthumb/timthumb.php?src='.$img_name.'&w=200', $img_name_preview);
    $text = '<b>'.$name.'</b>, �������, ���� ���� ���������. � ������� ����� ��� ������� ��������� � �� ������� ���� �� �����.';
    $text_mail = '<b>'.$name.'</b>, �������, ���� ���� ���������.	� ������� ����� ��� ������� ��������� � �� ������� ���� �� �����.
	<br/><img src="http://rutrah.tv/photo-images/'.$id.'/preview.'.$img_ext.'" />';
    send_mail($mail, '�� Rutrah.tv', 'info@rutrah.tv', '���� ���� �� �������', $text_mail);
    // sendHtmlMail('info@rutrah.net',$mail,'������� �� ������.���',$text_mail);
    query('update '.tabname('videohub', 'galleries').' set completed=\'1\' where id=\''.$id.'\';');

    return $text;
}

function submit_contest_old()
{
    global $config;
    // pre($_POST);
    // pre($_FILES);
    if (! isset($_POST['name'],$_POST['mail'],$_FILES['image'])) {
        error('�� �������� ����������� ������');
    }
    // if (!isset($_POST['name'],$_POST['mail'],$_POST['img_title'],$_FILES['image'])) {error('�� �������� ����������� ������');};
    if ($_FILES['image']['size'] > (10 * 1024 * 1024)) {
        error('������� ������� ����');
    }
    $img_ext = check_ext(['filename' => $_FILES['image']['name'], 'whitelist' => ['jpg', 'jpeg', 'png']]);
    if (! $img_ext) {
        error('������ �� ��������������');
    }
    $name = checkstr($_POST['name']);
    $mail = checkstr($_POST['mail']);
    // $img_title=checkstr($_POST['img_title']);
    check_email($mail);
    query('insert into '.tabname('videohub', 'contest').' set
		domain=\''.$config['domain'].'\',
		name=\''.$name.'\',
		mail=\''.$mail.'\',
		img_ext=\''.$img_ext.'\',
		date=current_timestamp;');
    $id = mysql_insert_id();
    $img_name = $config['path']['site'].'contest/photos/'.$id.'.'.$img_ext;
    $img_name_preview = $config['path']['site'].'contest/photos/'.$id.'_preview.'.$img_ext;
    if (! move_uploaded_file($_FILES['image']['tmp_name'], $img_name)) {
        error('������ ��� �������� ����������.');
    }
    copytoserv(http_current_domain().'/types/engine/inc/timthumb/timthumb.php?src='.$img_name.'&w=200', $img_name_preview);
    $text = '<b>'.$name.'</b>, �������, ���� ���� ���������. �� ���� ����� ���������� ��������� �� ������� � ��������.
	� ������� ����� ���� ������ �� ������� ������� ��������� � �� ������� ���� �� �����.';
    $text_mail = '<b>'.$name.'</b>, �������, ���� ���� ���������.	� ������� ����� ���� ������ �� ������� � �������� ������� ��������� � �� ������� ���� �� �����.
	<br/><img src="'.http_current_domain().'/template/contest/photos/'.$id.'_preview.'.$img_ext.'" />';
    send_mail($mail, '�� Rutrah.net', 'info@rutrah.net', '������� �� �������', $text_mail);

    // sendHtmlMail('info@rutrah.net',$mail,'������� �� ������.���',$text_mail);
    return $text;
}

// �������� ������� �� ��� �����
function add_albums()
{
    $data = query('
		select domains.domain,value
		from '.tabname('engine', 'domains').'
		join '.tabname('videohub', 'config_for_domain').' on config_for_domain.domain=domains.domain
		where property=\'number_of_photos_per_update\';
	');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        add_photos_on_domain($v['domain'], $v['value']);
    }
}

function add_photos_on_domain($domain, $q)
{
    if (empty($q)) {
        logger()->info('���� ��� ������ '.$domain.' �� ���������.');

        return 0;
    }
    $booked = return_value_for_domain('videohub', 'only_booked_photos', $domain);
    $described = return_value_for_domain('videohub', 'only_described_photos', $domain);
    if ($booked == '0') {
        if ($described == '0') {
            $data = query('
				select id from '.tabname('videohub', 'galleries').'
				where (title_ru is not null and title_ru!=\'\') and (domain is null or domain=\'\')
				and ((booked_domain is null) or (booked_domain=\''.$domain.'\'))
				order by booked_domain desc, add_date desc
				limit 0,'.$q);
        }
        if ($described == '1') {
            $data = query('
				select id from '.tabname('videohub', 'galleries').'
				where (title_ru is not null and title_ru!=\'\') and (domain is null or domain=\'\')
				and ((booked_domain is null) or (booked_domain=\''.$domain.'\')) and (description is not null and description!=\'\')
				order by booked_domain desc, add_date desc
				limit 0,'.$q);
        }
    }
    if ($booked == '1') {
        if ($described == '0') {
            $data = query('
				select id from '.tabname('videohub', 'galleries').'
				where (title_ru is not null and title_ru!=\'\') and (domain is null or domain=\'\')
				and booked_domain=\''.$domain.'\'
				order by booked_domain desc, add_date desc
				limit 0,'.$q);
        }
        if ($described == '1') {
            $data = query('
				select id from '.tabname('videohub', 'galleries').'
				where (title_ru is not null and title_ru!=\'\') and (domain is null or domain=\'\')
				and booked_domain=\''.$domain.'\' and (description is not null and description!=\'\')
				order by booked_domain desc, add_date desc
				limit 0,'.$q);
        }
    }
    $arr = readdata($data, 'nokey');
    $q_photos = count($arr);
    if ($q_photos == 0) {
        logger()->info('��� ��������� ��������.');
    }
    // pre($arr);
    foreach ($arr as $v) {
        query('update '.tabname('videohub', 'galleries').' set domain=\''.$domain.'\', apply_date=current_timestamp where id=\''.$v['id'].'\';');
    }
    // ��������� local_id
    update_local_ids([
        'table' => tabname('videohub', 'galleries'),
        'where_clause' => 'domain=\''.$domain.'\'']
    );
    logger()->info('��������� '.$q_photos.' �������� ��� ������ '.$domain.'.');

    // temp_var('q',$q_videos);
    // temp_var('date',date('Y-m-d'));
    // add_news($domain,template('site_news/temp_add_videos',$domain));
    return $q_photos;
}

function get_gallery_id_from_translit($translit)
{
    global $config;
    $data = query('select id from '.tabname('videohub', 'galleries').' where domain=\''.$config['domain'].'\' and title_ru_translit=\''.$translit.'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('������� �� �������');
    }

    return $arr['id'];
}

function get_gallery_id_from_date_translit($date, $translit)
{
    global $config;
    if (! strtotime($date)) {
        error('������������ ������ ����');
    }
    $data = query('select id from '.tabname('videohub', 'galleries').'
		where domain=\''.$config['domain'].'\' and date(apply_date)=\''.$date.'\' and title_ru_translit=\''.$translit.'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('������� �� �������');
    }

    return $arr['id'];
}

function submit_gallery_rating()
{
    global $config;
    check_session_start();
    if (empty($_POST['gallery_id'])) {
        error('�� ������� ID �������');
    }
    if (empty($_POST['score'])) {
        error('�� �������� ������');
    }
    if (isset($_SESSION['videohub']['votes']['galleries'][$_POST['gallery_id']])) {
        $status = 'ERR';
        $answer = '�� ��� ������� ���� ������ �� '.$_SESSION['videohub']['votes']['galleries'][$_POST['gallery_id']].'.';
    } else {
        if (is_numeric($_POST['score']) == 0) {
            error('������� score, �� ���������� ������ ('.$_POST['score'].')');
        }
        if (($_POST['score'] < 0) or ($_POST['score'] > 5)) {
            error('������� score ��� ��������� ('.$_POST['score'].')');
        }
        query('update '.tabname('videohub', 'galleries_views').'
			join '.tabname('videohub', 'galleries').' on galleries.id=galleries_views.gallery_id
			set rating=(rating*votes+'.$_POST['score'].')/(votes+1), votes=votes+1
			where galleries_views.gallery_id=\''.$_POST['gallery_id'].'\' and galleries.domain=\''.$config['domain'].'\'');
        if (mysql_affected_rows() == 0) {
            $data = query('select * from '.tabname('videohub', 'galleries').' where id=\''.$_POST['gallery_id'].'\' and domain=\''.$config['domain'].'\'');
            if (mysql_affected_rows() == 0) {
                error('������ �� ������', '������ '.$_POST['gallery_id'].' �� ������ ��� ����� '.$config['domain'].' ������');
            }
            query('insert into '.tabname('videohub', 'galleries_views').' (gallery_id,votes,rating) values (\''.$_POST['gallery_id'].'\',\'1\',\''.$_POST['score'].'\')');
        }
        $_SESSION['videohub']['votes']['galleries'][$_POST['gallery_id']] = $_POST['score'];
        $status = 'OK';
        $answer = '��� ����� �����. �������.';
    }
    $arr['status'] = $status;
    $arr['msg'] = iconv('windows-1251', 'utf-8', $answer);

    return json_encode($arr);
}

function gallery_rating_stars()
{
    $args['gallery_id'] = $_GET['gallery_id'];
    $arr = cache('preparing_view_gallery', $args);
    // pre($arr);
    temp_var('gallery_id', $arr['id']);
    temp_var('gallery_votes', $arr['votes']);
    temp_var('gallery_rating', $arr['rating']);

    return template('gallery/rating_stars');
}

function is_gallery_in_base($hub, $id_hub)
{
    $data = query('select id from '.tabname('videohub', 'galleries').' where hub=\''.$hub.'\' and id_hub=\''.$id_hub.'\'');
    if (mysql_num_rows($data) != 0) {
        return true;
    } else {
        return false;
    }
}

function gallery_category_title()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['photo_title'];
}

function gallery_category_text()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['photo_text'];
}

function gallery_category_img()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['photo_img'];
}

function gallery_category_h1()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['photo_h1'];
}

function gallery_category_description()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['photo_description'];
}

function gallery_views_count()
{
    if (isset($_GET['gallery_id']) == 0) {
        error('�� ����� ID �������');
    }
    query('update '.tabname('videohub', 'galleries_views').' set views=(ifnull(views,0)+1) where gallery_id=\''.$_GET['gallery_id'].'\'');
    if (mysql_affected_rows() == 0) {
        $data = query('select * from '.tabname('videohub', 'galleries').' where id=\''.$_GET['gallery_id'].'\'');
        if (mysql_num_rows($data) != 0) {
            query('insert into '.tabname('videohub', 'galleries_views').' (gallery_id,views) values (\''.$_GET['gallery_id'].'\',\'1\')');
        }
    }
}

function galleries_categories_list()
{
    $args = [];
    $arr = cache('preparing_galleries_categories_list', $args);
    $list = '';
    foreach ($arr as $v) {
        // *���
        temp_var('id', $v['id']);
        temp_var('name_translit', $v['name_translit']);
        temp_var('name', $v['name']);
        if (isset($v['name_url'])) {
            temp_var('name_url', $v['name_url']);
        } else {
            temp_var('name_url', without_spaces($v['name']));
        }
        temp_var('rows', $v['rows']);
        temp_var('class_active', '');
        if (isset($_GET['category_id'])) {
            if ($_GET['category_id'] == $v['id']) {
                temp_var('class_active', 'class="active"');
            }
        }
        $list .= template('gallery/item_categories_list');
    }

    return $list;
}

function preparing_galleries_categories_list($args = [])
{
    global $config;
    $data = query('
		select categories_domains.category_id,categories_domains.id,categories_groups.name,categories_groups.name_translit,
		categories_groups.name_url
		from '.tabname('videohub', 'categories_domains').'
		inner join '.tabname('videohub', 'categories_groups').'  on categories_domains.category_id=categories_groups.id
		left join '.tabname('videohub', 'categories_groups_photos_excluded').' on categories_groups_photos_excluded.group_id=categories_groups.group_id
		left join '.tabname('videohub', 'categories_domains_photos_excluded').' on categories_domains_photos_excluded.category_id=categories_domains.id
		where domain=\''.$config['domain'].'\'
		and categories_groups_photos_excluded.group_id is null
		and categories_domains_photos_excluded.category_id is null
		order by name');
    $arr = readdata($data, 'nokey');
    $print = '';
    foreach ($arr as $v) {
        // ��� �� categories_videos
        // ������ �� ��������� ����� ����������� � ������ ���������
        $data = query('
			SELECT catgroups_tags.tag_ru
			FROM '.tabname('videohub', 'categories_domains').'
			INNER JOIN  '.tabname('videohub', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
			INNER JOIN  '.tabname('videohub', 'catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
			WHERE categories_domains.id=\''.$v['id'].'\' and categories_domains.domain=\''.$config['domain'].'\'
			');
        $arr2 = readdata($data, 'nokey');
        // ��������� ������ ������ �����
        $cat_tags = [];
        foreach ($arr2 as $v2) {
            $cat_tags[] = $v2['tag_ru'];
        }
        if (empty($cat_tags)) {
            error('��������� ����������', '��� ��������� '.$v['id'].' ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
        }
        // ������ �� ��������� ���������� ���� ���������� ID, ��������������� �������
        $data = query('
			SELECT count(distinct galleries_tags.gallery_id) as q_galleries
			FROM '.tabname('videohub', 'galleries_tags').'
			inner join '.tabname('videohub', 'tags_translation').' on galleries_tags.tag_en=tags_translation.tag_en
			inner join '.tabname('videohub', 'galleries').' on galleries.id=galleries_tags.gallery_id
			WHERE domain=\''.$config['domain'].'\' and tag_ru in ('.array_to_str($cat_tags).')
			');
        $arr2 = mysql_fetch_array($data);
        $v['rows'] = $arr2[0];
        $arr_answer[] = $v;
    }

    // pre($arr_answer);
    return $arr_answer;
}

function galleries_list_category()
{
    global $config;
    // $arr=preparing_galleries_list($config['domain'],'category',$_GET['category_id'],$_GET['p'],20,'mr');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'galleries_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_galleries_list', $args);

    return gallery_thumbs_from_array($arr['galleries']);
}

function galleries_list_all()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'galleries_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_galleries_list', $args);

    // $arr=preparing_galleries_list($config['domain'],'all','value',$_GET['p'],20,'mr');
    return gallery_thumbs_from_array($arr['galleries']);
}

function preparing_galleries_list($args)
{
    // $domain,$type='all',$value='',$page=1,$per_page=20,$sort='mr'
    global $config;
    if (empty($args)) {
        error('�� ������� ������ ����������');
    }
    $domain = $args['domain'];
    $type = $args['type'];
    $value = $args['value'];
    $page = $args['page'];
    $per_page = $args['per_page'];
    $sort = $args['sort'];
    if (empty($domain)) {
        error('�� ������� �����');
    }
    if (empty($value)) {
        error('�� �������� ����������� ����������');
    }
    $types = ['all' => '', 'category' => '', 'tag' => '', 'same' => ''];
    $sort_sql = ['mv' => 'views desc', 'mr' => 'apply_date desc', 'tr' => 'rating desc, votes desc'];
    if (isset($types[$type]) == 0) {
        error('��� ������ �� ����������');
    }
    if (isset($sort_sql[$sort]) == 0) {
        error('���������� �� ����������');
    }
    $from = ($page - 1) * $per_page;
    if ($type == 'category') {
        $category_id = $value;
        // ������ �� ��������� ����� ����������� � ������ ��������� � ����� ��������� �� ��������� ��� ���������
        $data = query('
			SELECT catgroups_tags.tag_ru, categories_groups.name_translit
			FROM '.tabname('videohub', 'categories_domains').'
			INNER JOIN  '.tabname('videohub', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
			INNER JOIN  '.tabname('videohub', 'catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
			left join '.tabname('videohub', 'categories_groups_photos_excluded').' on categories_groups_photos_excluded.group_id=categories_groups.group_id
			WHERE categories_domains.id=\''.$category_id.'\' and categories_domains.domain=\''.$domain.'\' and categories_groups_photos_excluded.group_id is null;
			');
        $arr = readdata($data, 'nokey');
        if (empty($arr)) {
            error('�������� ���������');
        }
        // �������� �������� ��������� �� ���������
        $content_array['category_name_translit'] = $arr[0]['name_translit'];
        // ��������� ������ ������ �����
        $cat_tags = [];
        foreach ($arr as $v) {
            $cat_tags[] = $v['tag_ru'];
        }
        if (empty($cat_tags)) {
            error('��������� ����������', '��� ��������� ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
        }
        // ������ �� ��������� ���� ���������� ����� �����, ��������������� �������
        $data = query('
			SELECT SQL_CALC_FOUND_ROWS galleries_tags.gallery_id as id,galleries_tags.tag_en,tags_translation.tag_ru, galleries.title_ru,
			galleries.title_ru_translit, galleries.description, galleries.ext,
			unix_timestamp(galleries.apply_date) as date,
			ifnull(galleries_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, galleries.id_hub,
			local_id
			FROM '.tabname('videohub', 'galleries_tags').'
			inner join '.tabname('videohub', 'tags_translation').' on galleries_tags.tag_en=tags_translation.tag_en
			inner join '.tabname('videohub', 'galleries').' on galleries.id=galleries_tags.gallery_id
			left join '.tabname('videohub', 'galleries_views').' on galleries_tags.gallery_id=galleries_views.gallery_id
			WHERE domain=\''.$domain.'\' and tag_ru in ('.array_to_str($cat_tags).')
			group by id
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$per_page
        );
    }
    if ($type == 'tag') {
        $tag_ru = $value;
        $data = query('
			SELECT SQL_CALC_FOUND_ROWS galleries_tags.gallery_id as id,galleries_tags.tag_en,tags_translation.tag_ru, galleries.title_ru,
			galleries.title_ru_translit, galleries.description, unix_timestamp(galleries.apply_date) as date,
			ifnull(galleries_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, galleries.id_hub, galleries.ext,
			local_id
			FROM '.tabname('videohub', 'galleries_tags').'
			inner join '.tabname('videohub', 'tags_translation').' on galleries_tags.tag_en=tags_translation.tag_en
			inner join '.tabname('videohub', 'galleries').' on galleries.id=galleries_tags.gallery_id
			left join '.tabname('videohub', 'galleries_views').' on galleries_tags.gallery_id=galleries_views.gallery_id
			WHERE domain=\''.$domain.'\' and tag_ru=\''.$tag_ru.'\'
			group by id
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$per_page
        );
    }
    if ($type == 'all') {
        $data = query('select SQL_CALC_FOUND_ROWS id,title_ru,title_ru_translit,description,unix_timestamp(apply_date) as date,
			ifnull(galleries_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, galleries.id_hub,
			galleries.ext, local_id
			from '.tabname('videohub', 'galleries').'
			left join '.tabname('videohub', 'galleries_views').' on galleries.id=galleries_views.gallery_id
			where domain=\''.$domain.'\'
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$per_page);
    }
    if ($type == 'same') {
        if (empty($value)) {
            error('�� ������� ID �������');
        }
        $gallery_id = $value;
        // ������ �� ��������� ����� �������� �����
        $data = query('
			SELECT tags_translation.tag_ru
			FROM '.tabname('videohub', 'galleries_tags').'
			INNER JOIN '.tabname('videohub', 'tags_translation').' ON galleries_tags.tag_en=tags_translation.tag_en
			WHERE galleries_tags.gallery_id=\''.$gallery_id.'\'
			');
        $arr = readdata($data, 'nokey');
        // ��������� ������ �����
        $gallery_tags = [];
        foreach ($arr as $v) {
            $gallery_tags[] = $v['tag_ru'];
        }
        $data = query('
			SELECT galleries_tags.gallery_id as id,galleries_tags.tag_en,tags_translation.tag_ru, galleries.title_ru, galleries.title_ru_translit, galleries.description,
			unix_timestamp(galleries.apply_date) as date, ifnull(galleries_views.views,0) as views, count(galleries_tags.gallery_id) as same_tags_q,
			ifnull(rating,0) as rating, ifnull(votes,0) as votes, galleries.id_hub, ((galleries_views.rating)*(1-(0.5/sqrt((galleries_views.votes))))) as rating_sko,
			galleries.ext, local_id
			FROM '.tabname('videohub', 'galleries_tags').'
			inner join '.tabname('videohub', 'tags_translation').' on galleries_tags.tag_en=tags_translation.tag_en
			inner join '.tabname('videohub', 'galleries').' on galleries.id=galleries_tags.gallery_id
			left join '.tabname('videohub', 'galleries_views').' on galleries_tags.gallery_id=galleries_views.gallery_id
			WHERE domain=\''.$domain.'\' and tag_ru in ('.array_to_str($gallery_tags).') and galleries_tags.gallery_id!='.$gallery_id.'
			group by id
			order by '.$sort_sql[$sort].'
			limit '.$per_page.'
			');
    }
    $arr = readdata($data, 'nokey');
    $content_array['galleries'] = $arr;
    $content_array['rows'] = rows_without_limit();

    // pre($content_array);
    return $content_array;
}

function gallery_thumbs_from_array($arr)
{
    $list = '';
    if (empty($arr)) {
        $list = '��� ����������� ��� �����������.';
        logger()->info('�� ������� �� ����� �������');
    }
    foreach ($arr as $v) {
        $date_arr = my_date($v['date']);
        $date = $date_arr['day'].' '.$date_arr['month'].' '.$date_arr['year'];
        if (date('d.m.Y', $v['date']) == date('d.m.Y')) {
            $date = '�������';
        }
        if (date('d.m.Y', $v['date']) == date('d.m.Y', time() - 86400)) {
            $date = '�����';
        }
        if (date('d.m.Y', $v['date']) == date('d.m.Y', time() - 86400 * 2)) {
            $date = '���������';
        }
        temp_var('date', $date);
        temp_var('date_slashed', date('Y/m/d', $v['date']));
        temp_var('hours_back', hours_back($v['date']));
        temp_var('id', $v['id']);
        temp_var('title_ru', $v['title_ru']);
        temp_var('title_ru_translit', $v['title_ru_translit']);
        temp_var('description', '');
        if (isset($v['description'])) {
            temp_var('description', $v['description']);
            $v['description_short'] = substr($v['description'], 0, 200);
            if (strlen($v['description']) > strlen($v['description_short'])) {
                $v['description_short'] .= '...';
            }
            temp_var('description_short', $v['description_short']);
        }
        temp_var('views', $v['views']);
        temp_var('rating', $v['rating']);
        temp_var('rating_percent', round(($v['rating'] / 5) * 100, 0));
        temp_var('votes', $v['votes']);
        temp_var('id_hub', $v['id_hub']);
        temp_var('ext', $v['ext']);
        if (isset($v['local_id'])) {
            temp_var('local_id', $v['local_id']);
        }
        $list .= template('gallery/item');
    }

    return $list;
}

function gallery_tags_list_array($id)
{
    if (empty($id)) {
        error('�� ������� ID �����');
    }
    $data = query('
		SELECT distinct(tags_translation.tag_ru)
		FROM '.tabname('videohub', 'galleries_tags').'
		INNER JOIN '.tabname('videohub', 'tags_translation').' ON galleries_tags.tag_en = tags_translation.tag_en
		WHERE galleries_tags.gallery_id = \''.$id.'\'
		');
    $arr = readdata($data, 'nokey');

    return $arr;
}

function gallery_tags_list_simple($id)
{
    $arr = gallery_tags_list_array($id);
    $list = '';
    $i = 0;
    foreach ($arr as $v) {
        if ($i != 0) {
            $list .= ', ';
        }	$i = 1;
        $list .= $v['tag_ru'];
    }

    return $list;
}

function gallery_pics_files()
{
    global $config;
    if (empty($_GET['gallery_id'])) {
        error('�� ������� ID �������');
    }
    $args['gallery_id'] = $_GET['gallery_id'];
    $arr = cache('preparing_view_gallery', $args);
    // pre($arr);
    $list = '';
    foreach ($arr['files'] as $filename) {
        temp_var('filename', $filename);
        temp_var('gallery_id', $args['gallery_id']);
        $list .= template('gallery/li_pic');
    }

    return $list;
}

function gallery_first_file()
{
    if (empty($_GET['gallery_id'])) {
        error('�� ������� ID �������');
    }
    $args['gallery_id'] = $_GET['gallery_id'];
    $arr = cache('preparing_view_gallery', $args);
    if (empty($arr)) {
        error('������ ������ ����������');
    }

    // return $arr['files'][0]; //������ ������� ������, �.�. �� ������� ��������� preview.jpg
    return current($arr['files']);
}

function gallery_title()
{
    global $config;
    if (empty($_GET['gallery_id'])) {
        error('�� ������� ID �������');
    }
    $args['gallery_id'] = $_GET['gallery_id'];
    $arr = cache('preparing_view_gallery', $args);

    return $arr['title_ru'];
}

function gallery_description()
{
    global $config;
    if (empty($_GET['gallery_id'])) {
        error('�� ������� ID �������');
    }
    $args['gallery_id'] = $_GET['gallery_id'];
    $arr = cache('preparing_view_gallery', $args);

    return $arr['description'];
}

function preparing_view_gallery($args)
{
    global $config;
    if (! isset($args['gallery_id'])) {
        error('�� �������� ����������� ���������');
    }
    if (empty($args['check_domain'])) {
        $args['check_domain'] = 'yes';
    }
    if ($args['check_domain'] == 'yes') {
        $sql_domain = 'and domain=\''.$config['domain'].'\'';
    }
    if ($args['check_domain'] == 'no') {
        $sql_domain = '';
    }
    // pre($sql_domain);
    $data = query('select id,title_ru,description,q_pics,views,votes,rating,ext
		from '.tabname('videohub', 'galleries').'
		left join '.tabname('videohub', 'galleries_views').' on galleries_views.gallery_id=galleries.id
		where id=\''.$args['gallery_id'].'\' '.$sql_domain.';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('������� �� �������');
    }
    $files = list_directory_files($config['videohub']['path']['galleries'].$args['gallery_id']);
    natsort($files);
    foreach ($files as $k => $filename) {
        if ($filename == 'preview.'.$arr['ext']) {
            unset($files[$k]);
        }
    }
    $arr['files'] = $files;

    // pre($arr);
    return $arr;
}
