<?php

namespace MegawebV1;

function translate_gallery_pics_files()
{
    global $config;
    if (empty($_GET['gallery_id'])) {
        error('�� ������� ID �������');
    }
    $args['gallery_id'] = $_GET['gallery_id'];
    $args['check_domain'] = 'no';
    $arr = preparing_view_gallery($args); // �� ���� �����������
    $list = '';
    foreach ($arr['files'] as $filename) {
        $list .= '<li>
		<a href="/types/videohub/galleries/'.$args['gallery_id'].'/'.$filename.'" rel="'.$args['gallery_id'].'">
		<img src="/types/videohub/galleries/'.$args['gallery_id'].'/'.$filename.'" class="img-polaroid"></a>
		<a href="/?page=admin/delete_file&amp;file='.$config['videohub']['path']['galleries'].$args['gallery_id'].'/'.$filename.'" target="_blank">�������</a>
		</li>';
    }

    return $list;
}

function translate_gallery_pics()
{
    if (empty($_GET['gallery_id'])) {
        error('�� ������� ID �������');
    }
    $args['gallery_id'] = $_GET['gallery_id'];
    $data = query('select id,title_ru,q_pics
		from '.tabname('videohub', 'galleries').'
		where id=\''.$args['gallery_id'].'\';');
    $arr = mysql_fetch_assoc($data);
    // pre($arr);
    if (empty($arr['id'])) {
        error('������� �� �������');
    }
    $list = '';
    for ($i = 0; $i <= ($arr['q_pics'] - 1); $i++) {
        // temp_var('i',$i);
        // temp_var('gallery_id',$args['gallery_id']);
        // $list.=template('gallery/li_pic');
        $list .= '<li>
		<a href="/types/videohub/galleries/'.$args['gallery_id'].'/'.$i.'.jpg" rel="'.$args['gallery_id'].'"><img src="/types/videohub/galleries/'.$args['gallery_id'].'/'.$i.'.jpg" class="img-polaroid"></a>
		</li>';
    }

    return $list;
}

function submit_translate_galleries_titles()
{
    if (empty($_POST['galleries']) == 1) {
        error('�� �������� ���������');
    }
    $q_titles = 0;
    $q_all = 0;
    $content = '';
    foreach ($_POST['galleries'] as $k => $v) {
        if (empty($v['title_ru']) == 0) {
            $data = query('select title_ru,title_en,translater from '.tabname('videohub', 'galleries').' where id=\''.$k.'\'');
            $arr = mysql_fetch_assoc($data);
            if (empty($arr['title_ru']) == 0) {
                $content .= '"'.$arr['title_en'].'" ��� ������� '.$arr['translater'].' ("'.$arr['title_ru'].'")<br/>';
            } else {
                $v['title_ru'] = trim($v['title_ru']);
                if (! empty($v['description'])) {
                    $v['description'] = trim($v['description']);
                }
                $data = query('select id,title_en,title_ru,translater,description from '.tabname('videohub', 'galleries').' where title_ru=\''.$v['title_ru'].'\'');
                if (mysql_num_rows($data) != 0) {
                    $content .= '��������� "'.$v['title_ru'].'" ��� ������������ � ����.<br/>';
                } else {
                    $q_titles++;
                    $q = strlen($v['title_ru']);
                    $q_all = $q_all + $q;
                    if (empty($v['description'])) {
                        query('update '.tabname('videohub', 'galleries').' set title_ru=\''.$v['title_ru'].'\', title_ru_translit=\''.url_translit($v['title_ru']).'\', translater=\''.auth_login(['type' => 'videohub']).'\', translate_date=current_timestamp where id=\''.$k.'\'');
                    }
                    if (! empty($v['description'])) {
                        query('update '.tabname('videohub', 'galleries').' set title_ru=\''.$v['title_ru'].'\', title_ru_translit=\''.url_translit($v['title_ru']).'\', description=\''.$v['description'].'\', translater=\''.auth_login(['type' => 'videohub']).'\', translate_date=current_timestamp, description_translater=\''.auth_login(['type' => 'videohub']).'\', description_translate_date=current_timestamp where id=\''.$k.'\'');
                    }

                }
            }
        }
    }
    $content .= '<b>���������:</b>
	<br/>���������� ����������: '.$q_titles.'
	<br/>��������: '.$q_all.'
	<br/><a href="/?page=admin/translate/gallery/galleries">��������� � ��������</a>';

    return $content;
}

function submit_translate_galleries_titles_descriptions()
{
    if (empty($_POST['galleries']) == 1) {
        error('�� �������� ���������');
    }
    $q_titles = 0;
    $q_all = 0;
    $content = '';
    foreach ($_POST['galleries'] as $k => $v) {
        if (empty($v['title_ru']) == 0) {
            $data = query('select title_ru,title_en,translater from '.tabname('videohub', 'galleries').' where id=\''.$k.'\'');
            $arr = mysql_fetch_assoc($data);
            if (empty($arr['title_ru']) == 0) {
                $content .= '"'.$arr['title_en'].'" ��� ������� '.$arr['translater'].' ("'.$arr['title_ru'].'")<br/>';
            } else {
                $v['title_ru'] = trim($v['title_ru']);
                if (! empty($v['description'])) {
                    $v['description'] = trim($v['description']);
                }
                $data = query('select id,title_en,title_ru,translater,description from '.tabname('videohub', 'galleries').' where title_ru=\''.$v['title_ru'].'\'');
                if (mysql_num_rows($data) != 0) {
                    $content .= '��������� "'.$v['title_ru'].'" ��� ������������ � ����.<br/>';
                } else {
                    $q_titles++;
                    $q = strlen($v['title_ru']);
                    $q_all = $q_all + $q;
                    if (empty($v['description'])) {
                        query('update '.tabname('videohub', 'galleries').' set title_ru=\''.$v['title_ru'].'\', title_ru_translit=\''.url_translit($v['title_ru']).'\', translater=\''.auth_login(['type' => 'videohub']).'\', translate_date=current_timestamp where id=\''.$k.'\'');
                    }
                    if (! empty($v['description'])) {
                        query('update '.tabname('videohub', 'galleries').' set title_ru=\''.$v['title_ru'].'\', title_ru_translit=\''.url_translit($v['title_ru']).'\', description=\''.$v['description'].'\', translater=\''.auth_login(['type' => 'videohub']).'\', translate_date=current_timestamp, description_translater=\''.auth_login(['type' => 'videohub']).'\', description_translate_date=current_timestamp where id=\''.$k.'\'');
                    }

                }
            }
        }
    }
    $content .= '<b>���������:</b>
	<br/>���������� ����������: '.$q_titles.'
	<br/>��������: '.$q_all.'
	<br/><a href="/?page=admin/translate/gallery/galleries_titles_descriptions">��������� � ��������</a>';

    return $content;
}

function translate_galleries_descriptions()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $per_page = 20;
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_en,title_ru from '.tabname('videohub', 'galleries').' where
		title_ru is not null and title_ru!=\'\' and (description is null or description=\'\') and user_upload=\'0\'
			limit '.$from.','.$per_page);
    $rows = rows_without_limit(); // ����� �������
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<li class="well well-large" style="height: 250px;">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/gallery/view_gallery&amp;gallery_id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/galleries/'.$v['id'].'/preview.jpg" alt="" /></a></td>
		<td>
		<p>'.$v['title_en'].'</p>
		<p>'.gallery_tags_list_simple($v['id']).'</p>
		<p>'.$v['title_ru'].'</p>
		<br/><textarea name="galleries['.$v['id'].'][description]" spellcheck="true" style="width: 90%; height: 70px;"></textarea>
		</td></tr></table>
		</li>
		';
    }
    $content = '<div class="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/gallery/descriptions&amp;p=').'</ul><hr/></div><hr/>
	<ul class="desc">'.$list.'</ul>';

    return $content;
}

function submit_translate_galleries_descriptions()
{
    if (empty($_POST['galleries']) == 1) {
        error('�� �������� �������');
    }
    $q_desc = 0;
    $q_all = 0;
    $content = '';
    foreach ($_POST['galleries'] as $k => $v) {
        if (empty($v['description']) == 0) {
            $data = query('select title_ru,description,translater from '.tabname('videohub', 'galleries').' where id=\''.$k.'\'');
            $arr = mysql_fetch_assoc($data);
            if (empty($arr['description']) == 0) {
                $content .= '"'.$arr['title_ru'].'" ��� ������� '.$arr['translater'].' ("'.$arr['description'].'")<br/>';
            } else {
                $v['description'] = trim($v['description']);
                $q_desc++;
                $q = strlen($v['description']);
                $q_all = $q_all + $q;
                if (! empty($v['description'])) {
                    query('update '.tabname('videohub', 'galleries').' set description=\''.$v['description'].'\', description_translater=\''.auth_login(['type' => 'videohub']).'\', description_translate_date=current_timestamp where id=\''.$k.'\'');
                }
            }
        }
    }
    $content .= '<b>���������:</b>
	<br/>���������� ��������: '.$q_desc.'
	<br/>��������: '.$q_all.'
	<br/><a href="/?page=admin/translate/gallery/descriptions">��������� � ��������</a>';

    return $content;
}

function translate_galleries_titles_descriptions()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $per_page = 20;
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_en from '.tabname('videohub', 'galleries').' where title_ru is null or title_ru=\'\' limit '.$from.','.$per_page);
    $rows = rows_without_limit(); // ����� �������
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<li class="well well-large" style="height: 250px;">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/gallery/view_gallery&amp;gallery_id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/galleries/'.$v['id'].'/preview.jpg" alt="" /></a></td>
		<td>
		<p>'.$v['title_en'].'</p>
		<p>'.gallery_tags_list_simple($v['id']).'</p>
		<input type="text" name="galleries['.$v['id'].'][title_ru]" spellcheck="true" style="width: 90%;" />
		<br/><textarea name="galleries['.$v['id'].'][description]" spellcheck="true" style="width: 90%; height: 70px;"></textarea>
		</td></tr></table>
		</li>
		';
    }
    $content = '<div class="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/gallery/galleries&amp;p=').'</ul><hr/></div><hr/>
	<ul class="desc">'.$list.'</ul>';

    return $content;
}

function translate_galleries_titles()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $per_page = 20;
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_en from '.tabname('videohub', 'galleries').' where title_ru is null or title_ru=\'\' limit '.$from.','.$per_page);
    $rows = rows_without_limit(); // ����� �������
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<li class="well well-large" style="height: 250px;">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/gallery/view_gallery&amp;gallery_id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/galleries/'.$v['id'].'/preview.jpg" alt="" /></a></td>
		<td>
		<p>'.$v['title_en'].'</p>
		<p>'.gallery_tags_list_simple($v['id']).'</p>
		<input type="text" name="galleries['.$v['id'].'][title_ru]" spellcheck="true" style="width: 90%;" />
		<br/><textarea name="galleries['.$v['id'].'][description]" spellcheck="true" style="width: 90%; height: 70px;"></textarea>
		</td></tr></table>
		</li>
		';
    }
    $content = '<div class="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/gallery/galleries&amp;p=').'</ul><hr/></div><hr/>
	<ul class="desc">'.$list.'</ul>';

    return $content;
}
