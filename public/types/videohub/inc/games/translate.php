<?php

namespace MegawebV1;

function translate_view_game()
{
    if (! isset($_GET['game_id'])) {
        error('�� �������� ����������� ���������');
    }
    $data = query('select id from '.tabname('videohub', 'games').' where id=\''.$_GET['game_id'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('���� �� �������');
    }
    temp_var('game_id', $arr['id']);
    $content = template('admin/translate/games/item_view_game');

    return $content;
}

function submit_translate_games()
{
    if (empty($_POST['games']) == 1) {
        error('�� �������� ���������');
    }
    $q_titles = 0;
    $q_all = 0;
    $content = '';
    foreach ($_POST['games'] as $k => $v) {
        if (empty($v['title']) == 0) {
            $data = query('select title,source_title,title_translater from '.tabname('videohub', 'games').' where id=\''.$k.'\'');
            $arr = mysql_fetch_assoc($data);
            if (empty($arr['title']) == 0) {
                $content .= '"'.$arr['source_title'].'" ��� ������� '.$arr['title_translater'].' ("'.$arr['title'].'")<br/>';
            } else {
                $v['title'] = trim($v['title']);
                if (! empty($v['descriptions'])) {
                    $v['description'] = trim($v['description']);
                }
                $data = query('select id,source_title,title,title_translater from '.tabname('videohub', 'games').' where title=\''.$v['title'].'\'');
                if (mysql_num_rows($data) != 0) {
                    $content .= '��������� "'.$v['title'].'" ��� ������������ � ����.<br/>';
                } else {
                    $q_titles++;
                    $q = strlen($v['title']);
                    $q_all = $q_all + $q;
                    if (empty($v['description'])) {
                        query('update '.tabname('videohub', 'games').' set title=\''.$v['title'].'\', title_translit=\''.url_translit($v['title']).'\', title_translater=\''.auth_login(['type' => 'videohub']).'\', title_translate_date=current_timestamp where id=\''.$k.'\'');
                    }
                    if (! empty($v['description'])) {
                        query('update '.tabname('videohub', 'games').' set title=\''.$v['title'].'\', title_translit=\''.url_translit($v['title']).'\', description=\''.$v['description'].'\', title_translater=\''.auth_login(['type' => 'videohub']).'\', title_translate_date=current_timestamp, description_translater=\''.auth_login(['type' => 'videohub']).'\', description_translate_date=current_timestamp where id=\''.$k.'\'');
                    }
                }
            }
        }
    }
    $content .= '<b>���������:</b>
	<br/>���������� ����������: '.$q_titles.'
	<br/>��������: '.$q_all.'
	<br/><a href="/?page=admin/translate/games/not_translated">��������� � ��������</a>';

    return $content;
}

function translate_games()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $per_page = 20;
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,source_title,source_description from '.tabname('videohub', 'games').' where title is null or title=\'\' limit '.$from.','.$per_page);
    $rows = rows_without_limit(); // ����� �������
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<li class="well well-large" style="height: 250px;">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/games/view&amp;game_id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/games/'.$v['id'].'/preview.jpg" alt="" /></a></td>
		<td>
		<p>'.$v['source_title'].'</p>
		<p>'.$v['source_description'].'</p>
		<input type="text" name="games['.$v['id'].'][title]" spellcheck="true" style="width: 90%;" />
		<br/><textarea name="games['.$v['id'].'][description]" spellcheck="true" style="width: 90%; height: 70px;"></textarea>
		</td></tr></table>
		</li>
		';
    }
    $content = '<div class="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/games/not_translated&amp;p=').'</ul><hr/></div><hr/>
	<ul class="desc">'.$list.'</ul>';

    return $content;
}
