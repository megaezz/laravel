<?php

namespace MegawebV1;

function get_game_id_from_translit($translit)
{
    global $config;
    $data = query('select id from '.tabname('videohub', 'games').' where title_translit=\''.$translit.'\' and domain=\''.$config['domain'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('���� �� �������');
    }

    return $arr['id'];
}

function submit_game_rating()
{
    global $config;
    check_session_start();
    if (empty($_POST['game_id'])) {
        error('�� ������� ID ����');
    }
    if (empty($_POST['score'])) {
        error('�� �������� ������');
    }
    if (isset($_SESSION['videohub']['votes']['games'][$_POST['game_id']])) {
        $status = 'ERR';
        $answer = '�� ��� ������� ��� ���� �� '.$_SESSION['videohub']['votes']['games'][$_POST['game_id']].'.';
    } else {
        if (is_numeric($_POST['score']) == 0) {
            error('������� score, �� ���������� ������ ('.$_POST['score'].')');
        }
        if (($_POST['score'] < 0) or ($_POST['score'] > 5)) {
            error('������� score ��� ��������� ('.$_POST['score'].')');
        }
        query('update '.tabname('videohub', 'games_views').'
			join '.tabname('videohub', 'games').' on games.id=games_views.game_id
			set rating=(rating*votes+'.$_POST['score'].')/(votes+1), votes=votes+1
			where games_views.game_id=\''.$_POST['game_id'].'\' and games.domain=\''.$config['domain'].'\'');
        if (mysql_affected_rows() == 0) {
            $data = query('select * from '.tabname('videohub', 'games').' where id=\''.$_POST['game_id'].'\' and domain=\''.$config['domain'].'\'');
            if (mysql_affected_rows() == 0) {
                error('���� �� �������', '���� '.$_POST['game_id'].' �� ������� ��� ����� '.$config['domain'].' ������');
            }
            query('insert into '.tabname('videohub', 'games_views').' (game_id,votes,rating) values (\''.$_POST['game_id'].'\',\'1\',\''.$_POST['score'].'\')');
        }
        $_SESSION['videohub']['votes']['games'][$_POST['game_id']] = $_POST['score'];
        $status = 'OK';
        $answer = '��� ����� �����. �������.';
    }
    $arr['status'] = $status;
    $arr['msg'] = iconv('windows-1251', 'utf-8', $answer);

    return json_encode($arr);
}

function game_rating_stars()
{
    $args['game_id'] = $_GET['game_id'];
    $arr = cache('preparing_view_game', $args);
    temp_var('game_id', $arr['id']);
    temp_var('game_votes', $arr['votes']);
    temp_var('game_rating', $arr['rating']);

    return template('games/rating_stars');
}

function game_id()
{
    global $config;
    if (empty($_GET['game_id'])) {
        error('�� ������� ID ����');
    }
    $args['game_id'] = $_GET['game_id'];
    $arr = cache('preparing_view_game', $args);

    return $arr['id'];
}

function game_title()
{
    global $config;
    if (empty($_GET['game_id'])) {
        error('�� ������� ID ����');
    }
    $args['game_id'] = $_GET['game_id'];
    $arr = cache('preparing_view_game', $args);

    return $arr['title'];
}

function game_description()
{
    global $config;
    if (empty($_GET['game_id'])) {
        error('�� ������� ID ����');
    }
    $args['game_id'] = $_GET['game_id'];
    $arr = cache('preparing_view_game', $args);

    return $arr['description'];
}

function games_views_count()
{
    if (isset($_GET['game_id']) == 0) {
        error('�� ����� ID ����');
    }
    query('update '.tabname('videohub', 'games_views').' set views=(ifnull(views,0)+1) where game_id=\''.$_GET['game_id'].'\'');
    if (mysql_affected_rows() == 0) {
        $data = query('select * from '.tabname('videohub', 'games').' where id=\''.$_GET['game_id'].'\'');
        if (mysql_num_rows($data) != 0) {
            query('insert into '.tabname('videohub', 'games_views').' (game_id,views) values (\''.$_GET['game_id'].'\',\'1\')');
        }
    }
}

function is_game_in_base($hub, $id_hub)
{
    $data = query('select id from '.tabname('videohub', 'games').' where hub=\''.$hub.'\' and id_hub=\''.$id_hub.'\'');
    if (mysql_num_rows($data) != 0) {
        return true;
    } else {
        return false;
    }
}

function games_list_all()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'games_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_games_list', $args);

    return games_thumbs_from_array($arr['games']);
}

function preparing_games_list($args)
{
    global $config;
    if (empty($args)) {
        error('�� ������� ������ ����������');
    }
    $domain = $args['domain'];
    $type = $args['type'];
    $value = $args['value'];
    $page = $args['page'];
    $per_page = $args['per_page'];
    $sort = $args['sort'];
    if (empty($domain)) {
        error('�� ������� �����');
    }
    if (empty($value)) {
        error('�� �������� ����������� ����������');
    }
    $types = ['all' => ''];
    $sort_sql = ['mv' => 'views desc', 'mr' => 'apply_date desc', 'tr' => 'rating desc, votes desc'];
    if (isset($types[$type]) == 0) {
        error('��� ������ �� ����������');
    }
    if (isset($sort_sql[$sort]) == 0) {
        error('���������� �� ����������');
    }
    $from = ($page - 1) * $per_page;
    if ($type == 'all') {
        $data = query('select SQL_CALC_FOUND_ROWS id,title,title_translit,description,unix_timestamp(apply_date) as date,
			ifnull(games_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, games.id_hub
			from '.tabname('videohub', 'games').'
			left join '.tabname('videohub', 'games_views').' on games.id=games_views.game_id
			where domain=\''.$domain.'\'
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$per_page);
    }
    $arr = readdata($data, 'nokey');
    $content_array['games'] = $arr;
    $content_array['rows'] = rows_without_limit();

    return $content_array;
}

function games_thumbs_from_array($arr)
{
    $list = '';
    if (empty($arr)) {
        $list = '��� ����������� ��� �����������.';
        logger()->info('�� ������� �� ����� �������');
    }
    foreach ($arr as $v) {
        $date_arr = my_date($v['date']);
        $date = $date_arr['day'].' '.$date_arr['month'].' '.$date_arr['year'];
        if (date('d.m.Y', $v['date']) == date('d.m.Y')) {
            $date = '�������';
        }
        if (date('d.m.Y', $v['date']) == date('d.m.Y', time() - 86400)) {
            $date = '�����';
        }
        if (date('d.m.Y', $v['date']) == date('d.m.Y', time() - 86400 * 2)) {
            $date = '���������';
        }
        temp_var('date', $date);
        temp_var('date_slashed', date('Y/m/d', $v['date']));
        temp_var('hours_back', hours_back($v['date']));
        temp_var('id', $v['id']);
        temp_var('title', $v['title']);
        temp_var('title_translit', $v['title_translit']);
        temp_var('description', '');
        if (isset($v['description'])) {
            temp_var('description', $v['description']);
            $v['description_short'] = substr($v['description'], 0, 200);
            if (strlen($v['description']) > strlen($v['description_short'])) {
                $v['description_short'] .= '...';
            }
            temp_var('description_short', $v['description_short']);
        }
        temp_var('views', $v['views']);
        temp_var('rating', $v['rating']);
        temp_var('rating_percent', round(($v['rating'] / 5) * 100, 0));
        temp_var('votes', $v['votes']);
        temp_var('id_hub', $v['id_hub']);
        $list .= template('games/item');
    }

    return $list;
}

function preparing_view_game($args)
{
    global $config;
    if (! isset($args['game_id'])) {
        error('�� �������� ����������� ���������');
    }
    $data = query('select id,title,description,views,votes,rating
		from '.tabname('videohub', 'games').'
		join '.tabname('videohub', 'games_views').' on games_views.game_id=games.id
		where id=\''.$args['game_id'].'\' and domain=\''.$config['domain'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('���� �� �������');
    }

    return $arr;
}
