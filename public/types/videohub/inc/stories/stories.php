<?php

namespace MegawebV1;

function submit_story_rating()
{
    global $config;
    check_session_start();
    if (empty($_POST['story_id'])) {
        error('�� ������� ID ��������');
    }
    if (empty($_POST['score'])) {
        error('�� �������� ������');
    }
    if (isset($_SESSION['videohub']['votes']['stories'][$_POST['story_id']])) {
        $status = 'ERR';
        $answer = '�� ��� ������� ���� ������� �� '.$_SESSION['videohub']['votes']['stories'][$_POST['story_id']].'.';
    } else {
        if (is_numeric($_POST['score']) == 0) {
            error('������� score, �� ���������� ������ ('.$_POST['score'].')');
        }
        if (($_POST['score'] < 0) or ($_POST['score'] > 5)) {
            error('������� score ��� ��������� ('.$_POST['score'].')');
        }
        query('update '.tabname('videohub', 'stories_views').'
			join '.tabname('videohub', 'stories').' on stories.id=stories_views.story_id
			set rating=(rating*votes+'.$_POST['score'].')/(votes+1), votes=votes+1
			where stories_views.story_id=\''.$_POST['story_id'].'\' and stories.domain=\''.$config['domain'].'\'');
        if (mysql_affected_rows() == 0) {
            $data = query('select * from '.tabname('videohub', 'stories').' where id=\''.$_POST['story_id'].'\' and domain=\''.$config['domain'].'\'');
            if (mysql_affected_rows() == 0) {
                error('������� �� ������', '������� '.$_POST['story_id'].' �� ������ ��� ����� '.$config['domain'].' ������');
            }
            query('insert into '.tabname('videohub', 'stories_views').' (story_id,votes,rating) values (\''.$_POST['story_id'].'\',\'1\',\''.$_POST['score'].'\')');
        }
        $_SESSION['videohub']['votes']['stories'][$_POST['story_id']] = $_POST['score'];
        $status = 'OK';
        $answer = '��� ����� �����. �������.';
    }
    $arr['status'] = $status;
    $arr['msg'] = iconv('windows-1251', 'utf-8', $answer);

    return json_encode($arr);
}

function story_rating_stars()
{
    $args['story_id'] = $_GET['story_id'];
    $arr = cache('preparing_view_story', $args);
    temp_var('story_id', $arr['id']);
    temp_var('story_votes', $arr['votes']);
    temp_var('story_rating', $arr['rating']);

    return template('stories/rating_stars');
}

function story_views_count()
{
    if (isset($_GET['story_id']) == 0) {
        error('�� ����� ID ��������');
    }
    query('update '.tabname('videohub', 'stories_views').' set views=(ifnull(views,0)+1) where story_id=\''.$_GET['story_id'].'\'');
    if (mysql_affected_rows() == 0) {
        $data = query('select * from '.tabname('videohub', 'stories').' where id=\''.$_GET['story_id'].'\'');
        if (mysql_num_rows($data) != 0) {
            query('insert into '.tabname('videohub', 'stories_views').' (story_id,views) values (\''.$_GET['story_id'].'\',\'1\')');
        }
    }
}

function preparing_view_story($args)
{
    global $config;
    if (! isset($args['story_id'])) {
        error('�� �������� ����������� ���������');
    }
    $data = query('select id,title,text,views,votes,rating
		from '.tabname('videohub', 'stories').'
		join '.tabname('videohub', 'stories_views').' on stories_views.story_id=stories.id
		where id=\''.$args['story_id'].'\' and domain=\''.$config['domain'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('������� �� �������');
    }

    return $arr;
}

function preparing_stories_list($args)
{
    // $domain,$type='all',$value='',$page=1,$per_page=10,$sort='mr'
    global $config;
    if (empty($args)) {
        error('�� �������� ���������');
    }
    $domain = $args['domain'];
    $type = $args['type'];
    $value = $args['value'];
    $page = $args['page'];
    $per_page = $args['per_page'];
    $sort = $args['sort'];
    if (empty($domain)) {
        error('�� ������� �����');
    }
    if (empty($value)) {
        error('�� �������� ����������� ����������');
    }
    // $types=array('all'=>'','category'=>'','tag'=>'','same'=>'');
    $types = ['all' => ''];
    $sort_sql = ['mv' => 'views desc', 'mr' => 'apply_date desc', 'tr' => 'rating desc, votes desc'];
    if (isset($types[$type]) == 0) {
        error('��� ������ �� ����������');
    }
    if (isset($sort_sql[$sort]) == 0) {
        error('���������� �� ����������');
    }
    $from = ($page - 1) * $per_page;
    if ($type == 'all') {
        $data = query('select SQL_CALC_FOUND_ROWS id,title,title_translit,text,unix_timestamp(apply_date) as date,
			ifnull(stories_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes,
			local_id
			from '.tabname('videohub', 'stories').'
			left join '.tabname('videohub', 'stories_views').' on stories.id=stories_views.story_id
			where domain=\''.$domain.'\'
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$per_page);
    }
    $arr = readdata($data, 'nokey');
    $content_array['stories'] = $arr;
    $content_array['rows'] = rows_without_limit();

    return $content_array;
}

function stories_list_all()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'stories_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_stories_list', $args);

    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],10,'mr');
    return story_thumbs_from_array($arr['stories']);
}

function story_thumbs_from_array($arr)
{
    $list = '';
    if (empty($arr)) {
        $list = '��� ����������� ��� �����������.';
        logger()->info('�� ������� �� ����� �������');
    }
    foreach ($arr as $v) {
        $date_arr = my_date($v['date']);
        $date = $date_arr['day'].' '.$date_arr['month'].' '.$date_arr['year'];
        if (date('d.m.Y', $v['date']) == date('d.m.Y')) {
            $date = '�������';
        }
        if (date('d.m.Y', $v['date']) == date('d.m.Y', time() - 86400)) {
            $date = '�����';
        }
        if (date('d.m.Y', $v['date']) == date('d.m.Y', time() - 86400 * 2)) {
            $date = '���������';
        }
        temp_var('date', $date);
        temp_var('date_slashed', date('Y/m/d', $v['date']));
        temp_var('hours_back', hours_back($v['date']));
        temp_var('id', $v['id']);
        temp_var('title', $v['title']);
        temp_var('short_text', substr(strip_tags($v['text']), 0, 600));
        temp_var('title_translit', $v['title_translit']);
        temp_var('views', $v['views']);
        temp_var('rating', $v['rating']);
        temp_var('rating_percent', round(($v['rating'] / 5) * 100, 0));
        temp_var('votes', $v['votes']);
        if (isset($v['local_id'])) {
            temp_var('local_id', $v['local_id']);
        }
        $list .= template('stories/item');
    }

    return $list;
}

function get_story_id_from_translit($translit)
{
    global $config;
    $data = query('select id from '.tabname('videohub', 'stories').' where title_translit=\''.$translit.'\' and domain=\''.$config['domain'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('������� �� ������');
    }

    return $arr['id'];
}

function get_story_id_from_local_id($local_id)
{
    global $config;
    $data = query('select id from '.tabname('videohub', 'stories').' where local_id=\''.$local_id.'\' and domain=\''.$config['domain'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('������� �� ������');
    }

    return $arr['id'];
}

function story_title()
{
    global $config;
    if (isset($_GET['story_id']) == 0) {
        error('�� ����� ID ��������');
    }
    $args['story_id'] = $_GET['story_id'];
    $arr = cache('preparing_view_story', $args);

    return $arr['title'];
}

function story_text()
{
    global $config;
    if (isset($_GET['story_id']) == 0) {
        error('�� ����� ID ��������');
    }
    $args['story_id'] = $_GET['story_id'];
    $arr = cache('preparing_view_story', $args);

    return $arr['text'];
}
