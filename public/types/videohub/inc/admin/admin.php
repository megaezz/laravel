<?php

namespace MegawebV1;

function preparing_news_info($args)
{
    if (empty($args['id'])) {
        error('�� ������� ID �������');
    }
    $arr = query_assoc('select id,login,date,name,text,domain
		from '.tabname('videohub', 'site_news').'
		where id=\''.$args['id'].'\';
		');
    if (! $arr) {
        error('������� �� ����������');
    }

    return $arr;
}

function admin_news_edit()
{
    $id = empty($_GET['id']) ? error('�� ������� ID �������') : check_input($_GET['id']);
    $arr = preparing_news_info(['id' => $id], 0);
    temp_var('name', $arr['name']);
    temp_var('id', $arr['id']);
    temp_var('text', $arr['text']);
    temp_var('domain', $arr['domain']);
    temp_var('login', $arr['login']);
    temp_var('date', $arr['date']);
    temp_var('select_list_domains_videohub', select_list_domains_videohub(['selected_value' => $arr['domain']]));

    return template('admin/news/block_edit');
}

function admin_news_list()
{
    $login_info = auth_login(['type' => 'videohub']);
    $arr = query_arr('select id,domain,date,name,text,login
		from '.tabname('videohub', 'site_news').'
		order by date desc;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '
		<tr>
		<td>'.$v['date'].'</td>
		<td>'.$v['domain'].'</td>
		<td>'.$v['name'].'</td>
		<td>'.$v['text'].'</td>
		<td>'.$v['login'].'</td>
		<td><a href="/?page=admin/news/edit&amp;id='.$v['id'].'">�������������</a></td>
		</tr>
		';
    }

    return $list;
}

function submit_edit_news()
{
    $p['id'] = empty($_POST['id']) ? error('�� ������� ID') : check_input($_POST['id']);
    $p['name'] = empty($_POST['name']) ? error('�� �������� ���') : check_input($_POST['name']);
    $p['text'] = empty($_POST['text']) ? error('�� ������� �����') : check_input($_POST['text']);
    $p['domain'] = ! isset($_POST['domain']) ? error('�� ������� �����') : check_input($_POST['domain']);
    $p = null_if_empty($p);
    $login = auth_login(['type' => 'videohub']);
    query('update '.typetab('site_news').' set
		name='.$p['name'].',
		text='.$p['text'].',
		login=\''.$login.'\',
		domain='.$p['domain'].'
		where id='.$p['id'].'
		;');
    alert('������� ��������');
}

function submit_add_news()
{
    $p['name'] = empty($_POST['name']) ? error('�� �������� ���') : check_input($_POST['name']);
    $p['text'] = empty($_POST['text']) ? error('�� ������� �����') : check_input($_POST['text']);
    $p['domain'] = ! isset($_POST['domain']) ? error('�� ������� �����') : check_input($_POST['domain']);
    $p = null_if_empty($p);
    $login = auth_login(['type' => 'videohub']);
    query('insert into '.typetab('site_news').' set
		date=current_timestamp,
		name='.$p['name'].',
		text='.$p['text'].',
		login=\''.$login.'\',
		domain='.$p['domain'].'
		;');
    alert('������� ���������');
}

// ���������� ��� ��������� ������
function admin_active_all_categories()
{
    $domain = empty($_GET['domain']) ? error('�� ������� �����') : checkstr($_GET['domain']);
    query('update '.typetab('categories_domains').' set active=\'1\' where domain=\''.$domain.'\';');
    alert('��� ��������� ��� ������ <b>'.$domain.'</b> ������������');
}

// ��������� �� �����-���� �� 20 ����� ������ ������. - ����������� �������
function admin_test_add_videos()
{
    $domain = 'pornostars-tube.com';
    $arr = query_arr('
		select videos.id,tag_ru from '.tabname('videohub', 'tags').'
		join '.tabname('videohub', 'videos').' on videos.id=tags.video_id
		join '.tabname('videohub', 'categories_groups').' on categories_groups.name=tag_ru
		where booked_domain=\''.$domain.'\' and star=\'1\' and description is not null
		;');
    foreach ($arr as $v) {
        $q[$v['tag_ru']] = isset($q[$v['tag_ru']]) ? ($q[$v['tag_ru']] + 1) : 0;
        if ($q[$v['tag_ru']] < 20) {
            add_video_on_domain(['domain' => $domain, 'video_id' => $v['id']]);
        }
    }
    update_local_ids([
        'table' => tabname('videohub', 'videos'),
        'where_clause' => 'domain=\''.$domain.'\'']
    );
    alert(pre_return($q));
}

function add_video_on_domain($args)
{
    $video_id = isset($args['video_id']) ? $args['video_id'] : error('�� ������� ID �����');
    $domain = isset($args['domain']) ? $args['domain'] : error('�� ������� �����');
    query('update '.tabname('videohub', 'videos').' set domain=\''.$domain.'\', apply_date=current_timestamp where id=\''.$video_id.'\';');
}

// �������� ����� � ��������� (��������� � ������� ��������)
function add_stars_into_categories()
{
    $data = query('select stars.id,stars.name_ru,stars.name from '.tabname('videohub', 'stars').'
		left join '.tabname('videohub', 'categories_groups').' on categories_groups.name=stars.name_ru
		where categories_groups.id is null;
		;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $arr_group_id = mysql_fetch_assoc(query('select (ifnull(max(group_id),0)+1) as next_group_id from '.tabname('videohub', 'categories_groups').';'));
        $group_id = $arr_group_id['next_group_id'];
        query('update '.tabname('videohub', 'stars').' set group_id=\''.$group_id.'\' where id=\''.$v['id'].'\';');
        $list .= query_add_category(['name' => $v['name_ru'], 'group_id' => $group_id, 'type' => 'star']).'<br/>';
        $list .= query_add_category(['name' => $v['name'], 'group_id' => $group_id, 'type' => 'star']).'<br/>';
        $list .= query_select_tags(['tags_ru' => [$v['name_ru']], 'group_id' => $group_id]);
    }

    return $list;
}

function arr_for_duplicate_videos()
{
    $data = query('select videos.id,videos.hub,videos.id_hub,videos.ph_viewkey as viewkey,videos.title_en
		from '.tabname('videohub', 'videos').'
		join '.tabname('videohub', 'tags').' on videos.id=tags.video_id
		join '.tabname('videohub', 'categories_groups').' on categories_groups.name=tags.tag_ru
		where booked_domain=\'pornstars-hub.com\' and hub=\'xvideos.com\' and ph_viewkey!=\'no\' and star=\'1\'
		and videos.id_hub not in (select id_hub from videos where booked_domain=\'pornostars-tube.com\'
		limit '.$from.','.$per_page.'
			;');
}

function duplicate_videos($page)
{
    $per_page = 30;
    $from = ($page - 1) * $per_page;
    // $where_clause='where booked_domain=\'rutrah.net\' and date(add_date)=current_date';
    // ����� ��� ����������� � 41 �������� ����������, �������� ��� ����������� ��
    // $where_clause='where videos.id in (select id from '.tabname('videohub','temp_videos').')';
    // and id_hub not in (select id_hub from videos where booked_domain='pornozvezda.net'
    $where_clause = 'where booked_domain=\'russiaporno.net\' and hub=\'xvideos.com\' and ph_viewkey!=\'no\'';
    // $where_clause='where booked_domain=\'rutrah.net\' and hub=\'xvideos.com\' and ph_viewkey!=\'no\'
    // and id_hub not in (select id_hub from '.tabname('videohub','videos').' where booked_domain=\'russiaporno.net\' and hub=\'xvideos.com\')';
    // $where_clause='where booked_domain=\'pornstars-hub.com\' and hub=\'xvideos.com\' and ph_viewkey!=\'no\'';
    // $where_clause='where booked_domain=\'pornoruso.net\' and hub=\'xvideos.com\' and id_hub not in (select id_hub from '.tabname('videohub','videos').' where booked_domain=\'rutrah.net\' and hub=\'xvideos.com\')';
    $data = query('select videos.id,videos.hub,videos.id_hub,videos.ph_viewkey as viewkey,videos.title_en
		from '.tabname('videohub', 'videos').'
		'.$where_clause.'
		limit '.$from.','.$per_page.'
		;');
    $arr = readdata($data, 'nokey');
    $data_tags = query('select id,tags.tag_en from '.tabname('videohub', 'videos').'
		join '.tabname('videohub', 'tags').' on tags.video_id=videos.id
		'.$where_clause.'
		;');
    $arr_tags = readdata($data_tags, 'nokey');
    foreach ($arr_tags as $v) {
        $arr_tags_grouped[$v['id']]['tags'][] = $v['tag_en'];
    }
    foreach ($arr as $k => $v) {
        if (isset($arr_tags_grouped[$v['id']]['tags'])) {
            $arr[$k]['tags'] = $arr_tags_grouped[$v['id']]['tags'];
        }
        $arr[$k]['img_src'] = 'http://mgw.awmengine.net/types/videohub/images/'.$v['id'].'.jpg';
    }

    // pre($arr);
    return $arr;
}

function stat_videohub()
{
    global $db;
    readdb(tabname('videohub', 'config'), 'property');
    $arr = $db['videohub.config']['property'];
    $content = '
	��������� ����� �� 5 �����: '.$arr['video_direct_link_errors_percent']['value'].'%<br/>
	�������� ������� �����: '.$arr['video_same_link_percent']['value'].'%<br/>
	������ ��������: '.($arr['pornhub_availability']['value'] ? '��������' : '�� ��������').'
	';

    // pre($content);
    return $content;
}

function save_all_stars_images()
{
    global $config;
    // pre($config);
    $data = query('select name,pic from '.tabname('videohub', 'stars').' where id>=47;');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        copytoserv($episode['url'], $config['path']['type'].'/static/stars/'.without_spaces($v['name']).'.jpg');
        // copytoserv($v['pic'],'/var/www/uer555/data/www/megaweb/sites/pornozvezda.net/template/stars-pics2/'.without_spaces($v['name']).'.jpg');
    }
}

function check_video_direct_links()
{
    $video['xvideos'] = '201216';
    $video['xvideos_3gp'] = '203342';
    $video['pornhub_720p'] = '172611';
    $video['pornhub_480p'] = '166327';
    $content = '<br/>
	xvideos mp4: '.preparing_video_direct_link(['video_id' => $video['xvideos'], 'ext' => 'mp4', 'check_domain' => 'no']).'<br/>
	xvideos flv: '.preparing_video_direct_link(['video_id' => $video['xvideos'], 'ext' => 'flv', 'check_domain' => 'no']).'<br/>
	xvideos 3gp: '.preparing_video_direct_link(['video_id' => $video['xvideos_3gp'], 'ext' => 'mp4', 'check_domain' => 'no']).'<br/>
	pornhub 720p mp4: '.preparing_video_direct_link(['video_id' => $video['pornhub_720p'], 'ext' => 'mp4', 'check_domain' => 'no']).'<br/>
	pornhub 480p mp4: '.preparing_video_direct_link(['video_id' => $video['pornhub_480p'], 'ext' => 'mp4', 'check_domain' => 'no']).'<br/>
	';

    return $content;
}

// ��������� ��� viewkey ��� ����� � xvideos
function grab_all_xvideos_viewkeys()
{
    $data = query('select SQL_CALC_FOUND_ROWS id,id_hub from '.tabname('videohub', 'videos').'
		where hub=\'xvideos.com\' and ph_viewkey is null order by apply_date desc;');
    $arr = readdata($data, 'nokey');
    $rows = rows_without_limit();
    $q = 0;
    $content = '
	<script type="text/javascript">
	setTimeout("window.location.reload()",3000)
	</script>
	�����: '.$rows.'<br/>';
    foreach ($arr as $v) {
        $viewkey = media_from_xvideos($v['id_hub'], 'viewkey');
        if (empty($viewkey)) {
            $viewkey = 'no';
        }
        query('update '.tabname('videohub', 'videos').' set ph_viewkey=\''.$viewkey.'\' where id=\''.$v['id'].'\';');
        $content .= $v['id'].': '.$viewkey.'<br/>';
        $q++;
        if ($q == 30) {
            break;
        }
    }

    return $content;
}

// ��������� local_id ��� ������ videos,galleries,games,stories (�� 1000 �� ��� �� �����)
function add_local_ids()
{
    $tables = ['videos', 'galleries', 'games', 'stories'];
    foreach ($tables as $table) {
        $data = query('select domain from '.tabname('videohub', $table).'
			where domain is not null and local_id is null group by domain limit 1;');
        $arr = mysql_fetch_assoc($data);
        if (! empty($arr['domain'])) {
            $answer = update_local_ids([
                'table' => tabname('videohub', $table),
                'where_clause' => 'domain=\''.$arr['domain'].'\'',
                'max_updates' => 1000,
            ]);
            alert('������: '.$table.'. �����: '.$arr['domain'].'. ��������� �����: '.$answer);
        }
    }
    alert('��� �����������');
}

function admin_feedback_list()
{
    $args['only_active'] = 'no';
    $args['check_domain'] = 'no';
    $args['page'] = 'contact';
    $args['template'] = 'admin/feedback/post';
    $args['per_page'] = '30';
    $args['order'] = 'desc';

    return page_comments_posts($args);
}

function admin_feedback_pages()
{
    $args['only_active'] = 'no';
    $args['check_domain'] = 'no';
    $args['page'] = 'contact';
    $args['link'] = '/?page=admin/feedback/feedback&amp;p=';
    $args['per_page'] = '5';
    $args['order'] = 'desc';

    return page_comments_pages($args);
}

// ������� ����� ��� ����� ������ ��� ���������
function admin_category_headers()
{
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    if (empty($_GET['category_id'])) {
        return;
    }
    $data = query('
				select categories_domains.category_id,categories_domains.title,categories_domains.title_translit,
				categories_domains.photo_title, categories_domains.h1, categories_domains.h2,categories_domains.photo_h1,
				categories_domains.text, categories_domains.photo_text, categories_domains.keywords,
				categories_domains.description, categories_domains.photo_description, categories_groups.name,
				categories_groups.name_translit,
				categories_domains.text_2,
				if(stars.url is null,\'\',concat_ws(\'\',\'http://www.xvideos.com/profiles/\',stars.url,\'/videos/pornstar/\')) as xvideos_stars_url
				from '.tabname('videohub', 'categories_domains').'
				inner join '.tabname('videohub', 'categories_groups').' on categories_domains.category_id=categories_groups.id
				left join '.typetab('stars').' on stars.group_id=categories_groups.group_id
				where domain=\''.$_GET['domain'].'\' and category_id=\''.$_GET['category_id'].'\'
				'
    );
    $arr = mysql_fetch_assoc($data);
    $v = $arr; // ����� �� ���������� ���������� ����
    $content = '
	<form action="/?page=admin/domain/submit_categories_headers_form_admin" method="post">
	<b>'.$v['name'].':</b> ('.$v['name_translit'].') '.$v['xvideos_stars_url'].'<br/>
	��������� ��� �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][title]" value="'.htmlspecialchars(braces2square($v['title']), ENT_QUOTES, 'cp1251').'"/><br/>
	�������� ��������� ��� �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][title_translit]" value="'.htmlspecialchars(braces2square($v['title_translit']), ENT_QUOTES, 'cp1251').'"/><br/>
	��������� ��� ����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][photo_title]" value="'.htmlspecialchars(braces2square($v['photo_title']), ENT_QUOTES, 'cp1251').'"/><br/>
	���� �������� �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][keywords]" value="'.htmlspecialchars(braces2square($v['keywords']), ENT_QUOTES, 'cp1251').'"/><br/>
	���� �������� ��� �����:<br/><input class="ckeditor" style="width: 750px;" name="category_id['.$v['category_id'].'][description]" value="'.htmlspecialchars(braces2square($v['description']), ENT_QUOTES, 'cp1251').'"/><br/>
	���� �������� ��� ����:<br/><input class="ckeditor" style="width: 750px;" name="category_id['.$v['category_id'].'][photo_description]" value="'.htmlspecialchars(braces2square($v['photo_description']), ENT_QUOTES, 'cp1251').'"/><br/>
	H1 ��� �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][h1]" value="'.htmlspecialchars(braces2square($v['h1']), ENT_QUOTES, 'cp1251').'" /><br/>
	H2 ��� �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][h2]" value="'.htmlspecialchars(braces2square($v['h2']), ENT_QUOTES, 'cp1251').'" /><br/>
	H1 ��� ����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][photo_h1]" value="'.htmlspecialchars(braces2square($v['photo_h1']), ENT_QUOTES, 'cp1251').'" /><br/>
	����� ��� �����:<br/><textarea class="ckeditor" name="category_id['.$v['category_id'].'][text]" style="width: 700px; height: 200px;" />'.htmlspecialchars(braces2square($v['text']), ENT_QUOTES, 'cp1251').'</textarea>
	����� ��� ����:<br/><textarea class="ckeditor" name="category_id['.$v['category_id'].'][photo_text]" style="width: 700px; height: 200px;" />'.htmlspecialchars(braces2square($v['photo_text']), ENT_QUOTES, 'cp1251').'</textarea>
	����� 2:<br/><textarea class="ckeditor" name="category_id['.$v['category_id'].'][text_2]" style="width: 700px; height: 200px;" />'.htmlspecialchars(braces2square($v['text_2']), ENT_QUOTES, 'cp1251').'</textarea>
	<input type="hidden" name="domain" value="'.$_GET['domain'].'"/>
	<input type="submit" name="submit" value="���������">
	</form>';

    return $content;
}

function admin_headers_categories_list()
{
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    $data = query('
				select categories_domains.category_id,categories_groups.name,
				categories_domains.title,categories_domains.text
				from '.tabname('videohub', 'categories_domains').'
				inner join '.tabname('videohub', 'categories_groups').' on categories_domains.category_id=categories_groups.id
				where domain=\''.$_GET['domain'].'\'
				order by categories_groups.type,name'
    );
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        if (! empty($_GET['category_id']) and $_GET['category_id'] == $v['category_id']) {
            $class_current = 'class="active"';
        } else {
            $class_current = '';
        }
        if (! empty($v['title']) or ! empty($v['text'])) {
            $exist_mark = '&#10004;';
        } else {
            $exist_mark = '';
        }
        $list .= '<a href="/?page=admin/domain/category_headers&amp;domain='.$_GET['domain'].'&amp;category_id='.$v['category_id'].'" '.$class_current.'><span class="exist_mark">'.$exist_mark.'</span>'.$v['name'].'</a>';
    }

    return $list;
}

function admin_page_headers()
{
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    if (! empty($_GET['page_name'])) {
        $data = query('select text from '.tabname('videohub', 'pages').' where page=\''.$_GET['page_name'].'\';');
        $arr = mysql_fetch_assoc($data);

        return '<p>'.$arr['text'].'<p/>'.headers_form_admin($_GET['page_name']);
    }
}

function admin_pages_list()
{
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    $data = query('select page,name from '.tabname('videohub', 'pages').' order by name;');
    $arr = readdata($data, 'nokey');
    $data2 = query('select page from '.tabname('videohub', 'headers').' where domain=\''.$_GET['domain'].'\';');
    $arr2 = readdata($data2, 'page');
    $list = '';
    foreach ($arr as $v) {
        if (! empty($_GET['page_name']) and $_GET['page_name'] == $v['page']) {
            $class_current = 'class="active"';
        } else {
            $class_current = '';
        }
        if (isset($arr2[$v['page']])) {
            $exist_mark = '&#10004;';
        } else {
            $exist_mark = '';
        }
        $list .= '<a href="/?page=admin/domain/headers&amp;domain='.$_GET['domain'].'&amp;page_name='.$v['page'].'" '.$class_current.'><span class="exist_mark">'.$exist_mark.'</span>'.$v['name'].'</a>';
    }
    // ������ ��� �������������� ���������
    if ($_GET['page'] == 'admin/domain/category_headers') {
        $class_current_category_headers = 'class="active"';
    } else {
        $class_current_category_headers = '';
    }
    $content = '<a href="/?page=admin/domain/category_headers&amp;domain='.$_GET['domain'].'" '.$class_current_category_headers.' style="margin-bottom: 5px;">��������� ��� ���������</a>
	'.$list;

    return $content;
}

function submit_comments_edit()
{
    // pre($_POST['comments']);
    if (! empty($_POST['comments'])) {
        foreach ($_POST['comments'] as $k => $v) {
            if (empty($v['delete'])) {
                query('update '.tabname('videohub', 'comments').' set
					guest_name=\''.checkstr($v['guest_name']).'\',
					text=\''.checkstr($v['text']).'\',
					status=\'ok\',
					active=\'1\'
					where id=\''.$k.'\';');
            } else {
                if ($v['delete'] == 'yes') {
                    query('delete from '.tabname('videohub', 'comments').' where id=\''.$k.'\';');
                }
            }
        }
    }
    if (! empty($_POST['page_comments'])) {
        foreach ($_POST['page_comments'] as $k => $v) {
            if (empty($v['delete'])) {
                query('update '.tabname('videohub', 'page_comments').' set
					guest_name=\''.checkstr($v['guest_name']).'\',
					text=\''.checkstr($v['text']).'\',
					status=\'ok\',
					active=\'1\'
					where id=\''.$k.'\';');
            } else {
                if ($v['delete'] == 'yes') {
                    query('delete from '.tabname('videohub', 'page_comments').' where id=\''.$k.'\';');
                }
            }
        }
    }
    header('Location: /?page=admin/comments/moderate');
    alert('���������');
}

function comments_edit_list()
{
    $data = query('select SQL_CALC_FOUND_ROWS comments.*,videos.domain,videos.id as video_id from '.tabname('videohub', 'comments').'
		join '.tabname('videohub', 'videos').' on videos.id=comments.video_id
		where active=\'0\' order by date limit 30;');
    $arr = readdata($data, 'nokey');
    $list = '����������� � ����� ('.rows_without_limit().')<br/>';
    foreach ($arr as $v) {
        $delete_checked = '';
        $alert_class = ' alert-success';
        if ($v['status'] == 'check') {
            $delete_checked = 'checked';
            $alert_class = '';
        }
        $list .= '
		<!--<label for="comments['.$v['id'].'][delete]">-->
		<div class="comment alert'.$alert_class.'" onclick="change_checkbox(\'comments['.$v['id'].'][delete]\');">
		<input onclick="change_checkbox(\'comments['.$v['id'].'][delete]\',true);" type="text" name="comments['.$v['id'].'][guest_name]" value="'.$v['guest_name'].'" />
		<textarea onclick="change_checkbox(\'comments['.$v['id'].'][delete]\',true);" name="comments['.$v['id'].'][text]">'.check_words($v['text']).'</textarea>
		<input type="checkbox" id="comments['.$v['id'].'][delete]" name="comments['.$v['id'].'][delete]" value="yes" '.$delete_checked.' /> Delete
		<a href="http://'.$v['domain'].'/?page=video&amp;id='.$v['video_id'].'" target="_blank">'.$v['domain'].' &bull; '.$v['video_id'].'</a>
		</div>
		<!--</label>-->
		';
    }
    //
    $data = query('select SQL_CALC_FOUND_ROWS * from '.tabname('videohub', 'page_comments').' where active=\'0\' order by date limit 30;');
    $arr = readdata($data, 'nokey');
    $list .= '����������� � ��������� ('.rows_without_limit().')<br/>';
    foreach ($arr as $v) {
        $delete_checked = '';
        $alert_class = ' alert-success';
        if ($v['status'] == 'check') {
            $delete_checked = 'checked';
            $alert_class = '';
        }
        if ($v['page'] == 'contact') {
            $alert_class = ' alert-info';
        }
        $list .= '
		<label for="page_comments['.$v['id'].'][delete]">
		<div class="comment alert'.$alert_class.'">
		<input type="text" name="page_comments['.$v['id'].'][guest_name]" value="'.$v['guest_name'].'" />
		<textarea name="page_comments['.$v['id'].'][text]">'.check_words($v['text']).'</textarea>
		<input type="checkbox" id="page_comments['.$v['id'].'][delete]" name="page_comments['.$v['id'].'][delete]" value="yes" '.$delete_checked.' /> Delete
		'.$v['domain'].' &bull; '.$v['page'].'
		</div>
		</label>
		';
    }
    $content = '
	<form action="/?page=admin/comments/submit_edit" method="post">
	'.$list.'
	<input type="submit" name="submit" value="OK" />
	</form>
	';

    return $content;
}

// �������� ������� �� ����, � ����������. �� ������� ���������� ������.
function submit_add_site_news($domain, $text, $name) {}

function submit_apply_story_to_domain()
{
    apply_story_to_domain($_POST['story_id'], $_POST['domain']);
}

function select_list_domains_videohub($args = [])
{
    $selected_value = isset($args['selected_value']) ? $args['selected_value'] : false;
    $domains = visible_domains_videohub(['sort' => 'domain']);
    $content = get_select_option_list(['array' => $domains, 'value' => 'domain', 'text' => 'domain', 'selected_value' => $selected_value, 'null_value' => '']);

    return $content;
}

function apply_story_to_domain($story_id, $domain)
{
    $data = query('select domain from '.tabname('engine', 'domains').' where domain=\''.$domain.'\' and type=\'videohub\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['domain'])) {
        error('����� �� ������');
    }
    // ���� ��� �� ����� � worker, �� ���������� ������ ��� ������
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'videohub']), 'type' => 'videohub'], 0);
    $where['login'] = (! in_array($login_info['level'], ['root', 'admin', 'worker'])) ? 'stories.login=\''.$login_info['login'].'\'' : '1';
    //
    $data = query('select id from '.tabname('videohub', 'stories').' where '.$where['login'].' and id=\''.$story_id.'\' and domain is null and (booked_domain is null or booked_domain=\''.$domain.'\');');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('����� �� ������, ���� �����, ���� ����� ����� �� ������ �����, ���� ��� ���� ������� � ����� ������');
    }
    query('update '.tabname('videohub', 'stories').' set domain=\''.$domain.'\', apply_date=current_timestamp where id=\''.$story_id.'\';');
    $affected = mysql_affected_rows();
    if (mysql_affected_rows() != 1) {
        error('��������� �����: '.$affected);
    }
    reset_cache('preparing_stories_list', 'all', $domain);
    // ��������� local_id
    update_local_ids([
        'table' => tabname('videohub', 'stories'),
        'where_clause' => 'domain=\''.$domain.'\'']
    );
    alert('����� <b>'.$story_id.'</b> �������� �� ����� <b>'.$domain.'</b>. <a href="/?page=admin/stories/list">������ �������</a>');
}

function admin_story_info($arr)
{
    if (! isset($arr['id'],$arr['value'])) {
        error('�� ������� ID ��� ������������ ��������');
    }
    // ���� ��� �� ����� � worker, �� ���������� ������ ��� ������
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'videohub']), 'type' => 'videohub'], 0);
    $where['login'] = (! in_array($login_info['level'], ['root', 'admin', 'worker'])) ? 'stories.login=\''.$login_info['login'].'\'' : '1';
    //
    $data = query('select id,title,text from '.tabname('videohub', 'stories').'
		where id=\''.$arr['id'].'\' and '.$where['login'].';
		');
    $v = mysql_fetch_assoc($data);
    if (! $v) {
        error('����� �� ������');
    }

    return $v[$arr['value']];
}

// ID ����������� ����� �������, �� ������ ���� ������� ������������ ID
function admin_story_id()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID ������');
    }

    return admin_story_info(['id' => $_GET['id'], 'value' => 'id']);
}

function admin_story_title()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID ������');
    }

    return admin_story_info(['id' => $_GET['id'], 'value' => 'title']);
}

function admin_story_text()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID ������');
    }

    return admin_story_info(['id' => $_GET['id'], 'value' => 'text']);
}

function admin_stories_domains_list()
{
    // ���� ��� �� ����� � worker, �� ���������� ������ ��� ������
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'videohub']), 'type' => 'videohub'], 0);
    $where['login'] = (! in_array($login_info['level'], ['root', 'admin', 'worker'])) ? 'stories.login=\''.$login_info['login'].'\'' : '1';
    //
    $data = query('select ifnull(domain,\'0\') as domain,count(*) as q
		from '.tabname('videohub', 'stories').'
		where '.$where['login'].'
		group by domain
		order by domain
		');
    $arr = readdata($data, 'domain');
    if (! $arr) {
        alert('��� ������');
    }
    // pre($arr);
    $all_domains = visible_domains_videohub(['sort' => 'domain']);
    if (! isset($_GET['domain'])) {
        $class_active = ' class="active"';
    } else {
        $class_active = '';
    }
    $list = '<li'.$class_active.'><a href="/?page=admin/stories/list">��������� ������ ('.$arr['0']['q'].')</a></li>';
    foreach ($all_domains as $v) {
        if ((isset($_GET['domain']) and $_GET['domain'] == $v['domain'])) {
            $class_active = ' class="active"';
        } else {
            $class_active = '';
        }
        if (isset($arr[$v['domain']]['q'])) {
            $q = $arr[$v['domain']]['q'];
        } else {
            $q = 0;
        }
        $list .= '<li'.$class_active.'><a href="/?page=admin/stories/list&amp;domain='.$v['domain'].'">'.$v['domain'].' ('.$q.')</a></li>';
    }

    return $list;
}

function admin_stories_list()
{
    // ���� ��� �� ����� � worker, �� ���������� ������ ��� ������
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'videohub']), 'type' => 'videohub'], 0);
    $where['login'] = (! in_array($login_info['level'], ['root', 'admin', 'worker'])) ? 'stories.login=\''.$login_info['login'].'\'' : '1';
    //
    $where['domain'] = (isset($_GET['domain'])) ? 'where domain=\''.$_GET['domain'].'\'' : 'where domain is null';
    $data = query('select id,domain,booked_domain,title,add_date,update_time,apply_date
		from '.tabname('videohub', 'stories').'
		'.$where['domain'].' and '.$where['login'].'
		order by apply_date desc,add_date desc;');
    $arr = readdata($data, 'nokey');
    $list = '';
    $i = 0;
    foreach ($arr as $v) {
        if ($v['domain'] == '') {
            $class = 'success';
        } else {
            $class = 'warning';
        }
        $i++;
        $list .= '
		<tr class="'.$class.'">
		<td>'.$i.'</td>
		<td>'.$v['add_date'].'</td>
		<td>'.$v['apply_date'].'</td>
		<td>'.$v['update_time'].'</td>
		<td><a href="/?page=admin/stories/edit&amp;id='.$v['id'].'">'.$v['title'].'</a></td>
		<td>'.$v['domain'].'</td>
		<td>'.$v['booked_domain'].'</td>
		<td><a href="/?page=admin/stories/apply_domain&amp;id='.$v['id'].'">���������</a></td>
		</tr>
		';
    }

    return $list;
}

function submit_edit_story()
{
    if (isset($_POST['id'],$_POST['title'],$_POST['text']) == 0) {
        error('���� �� ����� �� ���������');
    }
    // ���� ��� �� ����� � worker, �� ���������� ������ ��� ������
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'videohub']), 'type' => 'videohub'], 0);
    $where['login'] = (! in_array($login_info['level'], ['root', 'admin', 'worker'])) ? 'stories.login=\''.$login_info['login'].'\'' : '1';
    //
    query('update '.tabname('videohub', 'stories').' set
		title=\''.$_POST['title'].'\',
		text=\''.$_POST['text'].'\'
		where id=\''.$_POST['id'].'\' and '.$where['login'].'
		;');
    alert('����� �������');
}

function submit_add_story()
{
    if (isset($_POST['title'],$_POST['text']) == 0) {
        error('���� �� ����� �� ���������');
    }
    query('insert into '.tabname('videohub', 'stories').' set
		add_date=current_timestamp,
		title=\''.$_POST['title'].'\',
		text=\''.$_POST['text'].'\',
		title_translit=\''.url_translit($_POST['title']).'\',
		login=\''.auth_login(['type' => 'videohub']).'\'
		;');
    alert('������� ��������');
}

function get_missing_ph_viewkeys()
{
    if (empty($_GET['q'])) {
        alert('������� q (limit)');
    }
    if (empty($_GET['skip'])) {
        $skip = 0;
    } else {
        $skip = $_GET['skip'];
    }
    $data = query('select SQL_CALC_FOUND_ROWS id,id_hub from '.tabname('videohub', 'videos').' where hub=\'pornhub.com\' and (ph_viewkey is null or ph_viewkey=\'\') order by add_date desc limit '.$skip.','.$_GET['q'].';');
    $arr = readdata($data, 'nokey');
    $q = rows_without_limit();
    $list = '';
    foreach ($arr as $v) {
        $viewkey = media_from_pornhub($v['id_hub'], 'viewkey');
        if ($viewkey == '') {
            $viewkey = 'no';
        }
        query('update '.tabname('videohub', 'videos').' set ph_viewkey=\''.$viewkey.'\' where id=\''.$v['id'].'\';');
        $list .= '<li>'.$v['id'].' - '.$viewkey.'</li>';
        // usleep(300000);
    }
    $content = '����� ��� viewkey: '.$q.'<ol>'.$list.'</ol>';

    return $content;
}

function get_missing_ph_dates()
{
    if (empty($_GET['q'])) {
        alert('������� q (limit)');
    }
    if (empty($_GET['skip'])) {
        $skip = 0;
    } else {
        $skip = $_GET['skip'];
    }
    $data = query('select SQL_CALC_FOUND_ROWS id,id_hub,ph_viewkey
		from '.tabname('videohub', 'videos').'
		where hub=\'pornhub.com\' and (ph_date is null or ph_date=\'\' or ph_date=\'check\') order by add_date desc limit '.$skip.','.$_GET['q'].';');
    $arr = readdata($data, 'nokey');
    $q = rows_without_limit();
    $list = '';
    foreach ($arr as $v) {
        $ph_date = media_from_pornhub($v['id_hub'], 'ph_date', $v['ph_viewkey']);
        if ($ph_date == '') {
            $ph_date = 'no';
        }
        query('update '.tabname('videohub', 'videos').' set ph_date=\''.$ph_date.'\' where id=\''.$v['id'].'\';');
        $list .= '<li>'.$v['id'].' - '.$ph_date.'</li>';
        // usleep(300000);
    }
    $content = '����� ��� ph_date: '.$q.'<ol>'.$list.'</ol>';

    return $content;
}

function submit_booking()
{
    // echo '<pre>'; print_r($_POST); echo '</pre>';
    if (empty($_POST['domain'])) {
        error('�� ������� �����');
    }
    if (empty($_POST['video'])) {
        error('�� �������� �����');
    }
    $q = 0;
    foreach ($_POST['video'] as $k => $v) {
        if ($v['category_id'] != 'no') {
            $q++;
            query('update '.tabname('videohub', 'videos').'
				set booked_domain=\''.$_POST['domain'].'\', category_id=\''.$v['category_id'].'\'
				where id=\''.$k.'\';
			');
        }
    }
    alert('������������� '.$q.' ����� ��� ������ '.$_POST['domain']);
}

function booked_videos_quantity()
{
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    $data = query('select count(*) as q from '.tabname('videohub', 'videos').'
		where booked_domain=\''.$_GET['domain'].'\' and (domain is null or domain=\'\')
		;');
    $arr = mysql_fetch_assoc($data);

    return $arr['q'];
}

function domain_video_booking()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    if (isset($_GET['domain']) == 0) {
        error('�� ������ �����');
    }
    $data = query('select value from '.tabname('videohub', 'config').' where property=\'translate_titles_per_page\'');
    $arr = mysql_fetch_assoc($data);
    $per_page = $arr['value']; // ���������� �� ��������
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select categories_domains.id, categories_groups.name
		from '.tabname('videohub', 'categories_domains').'
		join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
		where domain=\''.$_GET['domain'].'\'
		order by name');
    $arr = readdata($data, 'nokey');
    $categories_option_list = '<option value="no">�������� ���������</option>';
    foreach ($arr as $v) {
        $categories_option_list .= '<option value="'.$v['id'].'">'.$v['name'].'</option>';
    }
    $data = query('select SQL_CALC_FOUND_ROWS id,title_ru, description
		from '.tabname('videohub', 'videos').'
		where (booked_domain is null or booked_domain=\'\') and (domain is null or domain=\'\') and (title_ru is not null and title_ru!=\'\')
		order by add_date desc
		limit '.$from.','.$per_page);
    $rows = rows_without_limit();
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    $q = 0;
    foreach ($arr as $k => $v) {
        if ($q % 2 == 0) {
            $list .= '<tr>';
        }
        $list .= '<li class="well well-large">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/view_video&amp;id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/images/'.$v['id'].'.jpg" alt="" /></a></td>
		<td>
		<p>'.$v['title_ru'].' [<a href="/?page=admin/translate/source&amp;id='.$v['id'].'" target="_blank">��������</a>] <a href="/?page=admin/delete_video&amp;id='.$v['id'].'" style="text-decoration: none;">[X]</a></p>
		<p>'.video_tags_list_simple($v['id']).'</p>
		<p>'.$v['description'].'</p>
		<p><select size="1" name="video['.$v['id'].'][category_id]">'.$categories_option_list.'</select></p>
		</td></tr></table>
		</li>
		';
        $q++;
        if ($q % 2 == 0) {
            $list .= '</tr>';
        }
    }
    if ($q % 2 != 0) {
        $list .= '<td colspan="'.(2 - ($q % 2)).'"></td></tr>';
    }
    $content = '<div class="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '?page=admin/domain/booking&domain='.$_GET['domain'].'&amp;p=').'</ul><hr/></div><hr/>
	<ul class="desc">'.$list.'</ul>';

    return $content;
}

function is_video_in_base($hub, $id_hub)
{
    $data = query('select id from '.tabname('videohub', 'videos').' where hub=\''.$hub.'\' and id_hub=\''.$id_hub.'\'');
    if (mysql_num_rows($data) != 0) {
        return true;
    } else {
        return false;
    }
}

function add_video_to_db($args)
{
    if (empty($args)) {
        error('�� �������� ���������');
    }
    $v = $args;
    if (is_video_in_base($v['hub'], $v['id_hub'])) {
        error('����� '.$v['hub'].' '.$v['id_hub'].' ��� ����������');
    }
    if (empty($v['tags'])) {
        error('����������� ���� ��� ����� '.$v['hub'].' '.$v['id_hub']);
    }
    query('insert into '.tabname('videohub', 'videos').' (hub,id_hub,title_en,source_url) values (\''.$v['hub'].'\',\''.$v['id_hub'].'\',\''.checkstr($v['title_en']).'\',\''.$url.'\');');
    $arr = mysql_fetch_array(query('select last_insert_id() as id'));
    $video_id = $arr[0];
    // ��� �������� ���������� � ������� viewkey � ph_date
    if ($v['hub'] == 'pornhub.com') {
        query('update '.tabname('videohub', 'videos').' set ph_viewkey=\''.$v['viewkey'].'\', ph_date=\''.$v['ph_date'].'\' where id=\''.$video_id.'\';');
    }
    $mysql_tags_values = '';
    foreach ($v['tags'] as $tag) {
        // query('insert into '.tabname('videohub','tags').' (video_id,tag_en) values (\''.$video_id.'\',\''.checkstr($tag).'\')');
        if (! empty($mysql_tags_values)) {
            $mysql_tags_values .= ', ';
        }
        $mysql_tags_values .= '(\''.$video_id.'\',\''.checkstr($tag).'\')';
    }
    if (! empty($mysql_tags_values)) {
        query('insert into '.tabname('videohub', 'tags').' (video_id,tag_en) values '.$mysql_tags_values.';');
    }
    copytoserv($v['img_src'], $config['videohub']['path']['images'].$video_id.'.jpg');
}

function list_booking_domains()
{
    $arr = preparing_booking_domains();
    $arr[]['domain'] = 'with_description';
    $arr[]['domain'] = 'home';
    $arr[]['domain'] = 'russian';
    $arr[]['domain'] = 'hard';
    $arr[]['domain'] = 'bisexual';
    $arr[]['domain'] = 'with_stars';
    $arr[]['domain'] = 'untitled';
    $arr[]['domain'] = 'only_titled';
    $booked_domain = empty($_GET['booked_domain']) ? '' : $_GET['booked_domain'];
    $list = '
	<select name="booked_domain">'.get_select_option_list(['array' => $arr, 'value' => 'domain', 'text' => 'domain', 'selected_value' => $booked_domain]).'</select>
	';

    return $list;
}

// ���������� ������ � �������� ��� ������� ����� ����������� �����
function preparing_booking_domains()
{
    $data = query('
		select domain
		from '.tabname('videohub', 'config_for_domain').'
		where property in (\'only_described_videos\',\'only_booked_videos\') and value=\'1\'
		group by domain
		order by domain
	');
    $arr = readdata($data, 'nokey');

    // pre($arr);
    return $arr;
}

function submit_grabber()
{
    global $config;
    // ini_set('max_input_vars', 3000);
    // pre($_POST);
    if (empty($_POST['videos'])) {
        error('����� �� ��������');
    }
    if (! isset($_POST['url_short'],$_POST['url_page_number'])) {
        error('�� �������� ��������� ������');
    }
    $url = $_POST['url_short'].$_POST['url_page_number'];
    if (isset($_POST['booked_domain']) and $_POST['booked_domain'] != 'null') {
        $booked_domain = $_POST['booked_domain'];
        $sql_booked_domain = '\''.$_POST['booked_domain'].'\'';
    } else {
        $booked_domain = 'null';
        $sql_booked_domain = 'null';
    }
    foreach ($_POST['videos'] as $v) {
        // if (is_video_in_base_for_booked_domain($v['hub'],$v['id_hub'],$booked_domain)) {error('����� '.$v['hub'].' '.$v['id_hub'].' ��� ���������� ��� ������������ ������');};
        if (empty($v['tags'])) {
            error('����������� ���� ��� ����� '.$v['hub'].' '.$v['id_hub']);
        }
        query('insert into '.tabname('videohub', 'videos').' (hub,id_hub,title_en,source_url,booked_domain) values (\''.$v['hub'].'\',\''.$v['id_hub'].'\',\''.checkstr($v['title_en']).'\',\''.$url.'\','.$sql_booked_domain.');');
        $arr = mysql_fetch_array(query('select last_insert_id() as id'));
        $video_id = $arr[0];
        // ��� �������� ���������� � ������� viewkey � ph_date
        if ($v['hub'] == 'pornhub.com') {
            query('update '.tabname('videohub', 'videos').' set ph_viewkey=\''.$v['viewkey'].'\', ph_date=\''.$v['ph_date'].'\' where id=\''.$video_id.'\';');
        }
        // ��� xvideos ���������� � ������� viewkey
        if ($v['hub'] == 'xvideos.com') {
            query('update '.tabname('videohub', 'videos').' set ph_viewkey=\''.$v['viewkey'].'\' where id=\''.$video_id.'\';');
        }
        $mysql_tags_values = '';
        foreach ($v['tags'] as $tag) {
            // query('insert into '.tabname('videohub','tags').' (video_id,tag_en) values (\''.$video_id.'\',\''.checkstr($tag).'\')');
            if (! empty($mysql_tags_values)) {
                $mysql_tags_values .= ', ';
            }
            $mysql_tags_values .= '(\''.$video_id.'\',\''.checkstr($tag).'\')';
        }
        if (! empty($mysql_tags_values)) {
            query('insert into '.tabname('videohub', 'tags').' (video_id,tag_en) values '.$mysql_tags_values.';');
        }
        // $curl['proxy']='rnd';
        // ��������� ua ������ ��� ���� ����� copytoserv ������� ����� filegetcontents � �� ����� file_get_contents (�������� xvideos ������ ��-�� �����)
        $curl['ua'] = 'default_ua';
        copytoserv($v['img_src'], $config['videohub']['path']['images'].$video_id.'.jpg', $curl);
    }
    alert('
		��������� '.count($_POST['videos']).' �����. ��� ������: '.$booked_domain.'<br/>
		<a href="/?page=admin/grabber/grabber&amp;booked_domain='.$booked_domain.'">�������</a><br/>
		<form action="?page=admin/grabber/grabber_confirm" method="post">
		������: '.$_POST['url_short'].($_POST['url_page_number'] + 1).'
		<input type="hidden" name="url_short" value="'.$_POST['url_short'].'" />
		<input type="hidden" name="url_page_number" value="'.($_POST['url_page_number'] + 1).'"/>
		<input type="hidden" name="booked_domain" value="'.$booked_domain.'"/>
		<br/><input style="margin-left: 25px;" type="submit" name="submit" value="��" />
		</form>
		');
}

function grabber_confirm()
{
    $url_page_number = empty($_POST['url_page_number']) ? 1 : $_POST['url_page_number'];
    $url_page_max = $url_page_number;
    for ($url_page_number; $url_page_number <= $url_page_max; $url_page_number++) {
        $url_short = empty($_POST['url_short']) ? error('�� ����� URL') : $_POST['url_short'];
        $booked_domain = isset($_POST['booked_domain']) ? $_POST['booked_domain'] : 'null';
        $url = $url_short.$url_page_number;
        $parse_url = parse_url($url);
        // pre($url);
        // pre($parse_url);
        if (isset($parse_url['host']) == 0) {
            error('URL �� ���������');
        }
        $site = str_replace('www.', '', $parse_url['host']);
        if ($site != 'duplicate_videos') {
            $page = filegetcontents($url);
        }
        if ($site == 'duplicate_videos') {
            $videos = duplicate_videos($url_page_number);
        }
        if ($site == 'pornhub.com') {
            $videos = grabber_pornhub($page);
        }
        if ($site == 'xvideos.com') {
            if (strstr($url, 'pornstar_videos')) {
                $videos = grabber_xvideos_pornstars($page);
                // pre($videos);
            } else {
                $videos = grabber_xvideos($page);
            }
        }
        if ($site == 'paradisehill.tv') {
            $videos = grabber_paradisehill($page);
        }
        if (empty($videos)) {
            alert('��� ����������� ��� �����������');
        }
        $list = empty($list) ? '' : $list;
        foreach ($videos as $k => $info) {
            if ($site != 'duplicate_videos') {
                if (is_video_in_base($info['hub'], $info['id_hub'])) {
                    $list .= '<div style="clear: both; margin: 10px; color: red;">'.$info['hub'].' '.$info['id_hub'].' ��� ����������</div>';

                    continue;
                }
            }
            if ($site == 'duplicate_videos') {
                $video_on_domain = query_assoc('select count(*) as q from '.tabname('videohub', 'videos').'
					where booked_domain=\''.$booked_domain.'\' and hub=\''.$info['hub'].'\' and id_hub=\''.$info['id_hub'].'\'; ');
                if ($video_on_domain['q'] != 0) {
                    $list .= '<div style="clear: both; margin: 10px; color: red;">
					'.$info['hub'].' '.$info['id_hub'].' ��� ���������� ��� ������ '.$booked_domain.'</div>';

                    continue;
                }
            }
            if (empty($info['tags'])) {
                $list .= '<div style="clear: both; margin: 10px; color: red;">'.$info['hub'].' '.$info['id_hub'].' ���� �����������</div>';

                continue;
            }
            $input_tags = '';
            foreach ($info['tags'] as $n => $tag) {
                $input_tags .= '<input type="hidden" name="videos['.$k.'][tags]['.$n.']" value="'.$tag.'" />';
            }
            if ($site != 'pornhub.com') {
                $info['ph_date'] = '';
            }
            if ($site != 'xvideos.com' and $site != 'pornhub.com' and empty($info['viewkey'])) {
                $info['viewkey'] = '';
            }
            $list .= '<div style="clear: both; margin: 10px;">
			<input type="hidden" name="videos['.$k.'][hub]" value="'.$info['hub'].'" />
			<input type="hidden" name="videos['.$k.'][id_hub]" value="'.$info['id_hub'].'" />
			<input type="hidden" name="videos['.$k.'][viewkey]" value="'.$info['viewkey'].'" />
			<input type="hidden" name="videos['.$k.'][ph_date]" value="'.$info['ph_date'].'" />
			<input type="hidden" name="videos['.$k.'][img_src]" value="'.$info['img_src'].'" />
			<input type="hidden" name="videos['.$k.'][title_en]" value="'.$info['title_en'].'" />
			'.$input_tags.'
			<img src="'.$info['img_src'].'" alt="" style="float: left; margin-right: 10px;" />
			<b>ID:</b> #'.$info['id_hub'].'<br/>
			<b>Viewkey:</b> #'.$info['viewkey'].'<br/>
			<b>Ph_date:</b> #'.$info['ph_date'].'<br/>
			<b>��������:</b> '.$info['title_en'].'<br/>
			<b>��������:</b> '.$info['img_src'].'<br/>
			<b>����:</b> '.array_to_str($info['tags']).'<br/>
			<div style="clear: both;"></div>
			</div>
			';
        }
    }
    $content = '
	��� ������: '.$booked_domain.'<br/>
	<form action="/?page=admin/grabber/submit_grabber" method="post">
	<input type="submit" name="submit" value="��������"/>
	'.$list.'
	<input type="hidden" name="url_short" value="'.$url_short.'"/>
	<input type="hidden" name="url_page_number" value="'.$url_page_number.'"/>
	<input type="hidden" name="booked_domain" value="'.$booked_domain.'"/>
	<input type="submit" name="submit" value="��������"/>
	</form>
	';

    return $content;
}

function redirect_to_video_watch_link_source()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $link = media_for_video($_GET['id'], 'video_watch_link_source');
    header('Location: '.$link);
}

function delete_video_get_param()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    delete_video($_GET['id']);
}

function delete_video($id)
{
    global $config;
    query('delete from '.tabname('videohub', 'videos').' where id=\''.$id.'\'');
    query('delete from '.tabname('videohub', 'videos_views').' where video_id=\''.$id.'\'');
    query('delete from '.tabname('videohub', 'tags').' where video_id=\''.$id.'\'');
    delete_file($config['videohub']['path']['images'].$id.'.jpg');
    alert('����� '.$id.' �������');
}

// ������ ����� � ��������� �� �����. ��� ��������� �� ��
function seo_links()
{
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    $domain = str_replace('www.', '', $_GET['domain']);
    $domain_origin = $_GET['domain'];
    $data = query('select id,title_ru,title_ru_translit,local_id,unix_timestamp(apply_date) as date
		from '.tabname('videohub', 'videos').'
		where domain=\''.$domain.'\' order by apply_date desc');
    $arr = readdata($data, 'nokey');
    $template_video_link = value_from_config_for_domain('videohub', 'template_video_link', $domain);
    $link_args = ['[id]', '[local_id]', '[title_translit]', '[date_slashed]'];
    $links['video_titles'] = '';
    $links['video_simple'] = '';
    $links['video_list'] = '';
    foreach ($arr as $v) {
        $link_values = [$v['id'], $v['local_id'], $v['title_ru_translit'], date('Y/m/d', $v['date'])];
        $video_link = 'http://'.$domain_origin.str_replace($link_args, $link_values, $template_video_link);
        $links['video_titles'] .= '<li>'.htmlspecialchars('<a href="'.$video_link.'">'.$v['title_ru'].'</a>', ENT_QUOTES, 'cp1251').'</li>';
        $links['video_simple'] .= '<li>'.htmlspecialchars('<a href="'.$video_link.'">'.$video_link.'</a>', ENT_QUOTES, 'cp1251').'</li>';
        $links['video_list'] .= '<li>'.$video_link.'</li>';
    }
    $data = query('
		select categories_domains.category_id,categories_domains.id,categories_domains.title,
		categories_domains.title_translit,categories_groups.name,categories_groups.name_translit,
		name_url
		from '.tabname('videohub', 'categories_domains').'
		inner join '.tabname('videohub', 'categories_groups').'  on categories_domains.category_id=categories_groups.id
		where domain=\''.$domain.'\' and type=\'default\' order by name');
    $arr = readdata($data, 'nokey');
    $template_category_link = value_from_config_for_domain('videohub', 'template_category_link', $domain);
    $link_args = ['[id]', '[title]', '[title_translit]', '[name]', '[name_translit]', '[name_url]'];
    $links['categories_list'] = '';
    $links['categories_titles'] = '';
    $links['categories_simple'] = '';
    foreach ($arr as $v) {
        $link_values = [$v['id'], $v['title'], $v['title_translit'], $v['name'], $v['name_translit'], $v['name_url']];
        $category_link = 'http://'.$domain_origin.str_replace($link_args, $link_values, $template_category_link);
        $links['categories_list'] .= '<li>'.$category_link.'</li>';
        $links['categories_titles'] .= '<li>'.htmlspecialchars('<a href="'.$category_link.'">'.$v['name'].'</a>', ENT_QUOTES, 'cp1251').'</li>';
        $links['categories_simple'] .= '<li>'.htmlspecialchars('<a href="'.$category_link.'">'.$category_link.'</a>', ENT_QUOTES, 'cp1251').'</li>';
    }
    $template_star_link = value_from_config_for_domain('videohub', 'template_star_link', $domain, false);
    // pre($template_star_link);
    $links['stars_list'] = '';
    $links['stars_titles'] = '';
    $links['stars_titles_en'] = '';
    $links['stars_simple'] = '';
    if ($template_star_link) {
        // $arr=preparing_stars_list(array('domain'=>$domain));
        // $arr=preparing_categories_list(array('domain'=>$domain,'mobile'=>'no','type'=>'star','show_empty'=>0));
        $arr = query_arr('
		select categories_domains.category_id,categories_domains.id,categories_domains.title,
		categories_domains.title_translit,categories_groups.name,categories_groups.name_translit,
		name_url
		from '.tabname('videohub', 'categories_domains').'
		inner join '.tabname('videohub', 'categories_groups').'  on categories_domains.category_id=categories_groups.id
		where domain=\''.$domain.'\' and type=\'star\' order by name');
        // pre($arr);
        // pre($arr);
        $link_args = ['[id]', '[title]', '[title_translit]', '[name]', '[name_translit]', '[name_url]'];
        foreach ($arr as $v) {
            $star_info = cache('preparing_star_info', ['category_id' => $v['id']]);
            // $link_values=array(without_spaces($v['name_url']));
            $link_values = [$v['id'], $v['title'], $v['title_translit'], $v['name'], $v['name_translit'], $v['name_url']];
            $star_link = 'http://'.$domain_origin.str_replace($link_args, $link_values, $template_star_link);
            $links['stars_list'] .= '<li>'.$star_link.'</li>';
            $links['stars_titles'] .= '<li>'.htmlspecialchars('<a href="'.$star_link.'">'.$v['name'].'</a>', ENT_QUOTES, 'cp1251').'</li>';
            $links['stars_titles_en'] .= '<li>'.$star_info['name'].'</li>';
            $links['stars_simple'] .= '<li>'.htmlspecialchars('<a href="'.$star_link.'">'.$star_link.'</a>', ENT_QUOTES, 'cp1251').'</li>';
        }
    }
    $content = '
	<ul style="position: fixed; width: 220px; top: 10px; right: 10px; background-color: #eee; list-style-type: none; padding: 3px;">
	<li><a href="/?page=admin/domain/seo_links&domain=www.'.$domain.'">� www</a> <a href="/?page=admin/domain/seo_links&domain='.$domain.'">��� www</a></li>
	<li>&nbsp;</li>
	<li><a href="#links_video_titles">������ �� ����� � ���������</a></li>
	<li><a href="#links_video_simple">������ �� ����� �������</a></li>
	<li><a href="#links_video_list">������ ������ �� ����� </a></li>
	<li>&nbsp;</li>
	<li><a href="#links_categories_titles">������ �� ��������� � ���������</a></li>
	<li><a href="#links_categories_simple">������ �� ��������� �������</a></li>
	<li><a href="#links_categories_list">������ ������ �� ���������</a></li>
	<li>&nbsp;</li>
	<li><a href="#links_stars_titles">������ �� ������ � ���������</a></li>
	<li><a href="#links_stars_titles_en">������ �� ������ � ��������� ENG</a></li>
	<li><a href="#links_stars_simple">������ �� ������ �������</a></li>
	<li><a href="#links_stars_list">������ ������ �� �����</a></li>
	</ul>
	<h2 id="links_video_titles">������ �� ����� � ���������</h2>
	<ol>'.$links['video_titles'].'</ol>
	<h2 id="links_video_simple">������ �� ����� �������</h2>
	<ol>'.$links['video_simple'].'</ol>
	<h2 id="links_video_list">������ ������ �� �����</h2>
	<ol>'.$links['video_list'].'</ol>
	<h2 id="links_categories_titles">������ �� ��������� � ��������</h2>
	<ol>'.$links['categories_titles'].'</ol>
	<h2 id="links_categories_simple">������ �� ��������� �������</h2>
	<ol>'.$links['categories_simple'].'</ol>
	<h2 id="links_categories_list">������ ������ �� ���������</h2>
	<ol>'.$links['categories_list'].'</ol>
	<h2 id="links_stars_titles">������ �� ������ � ��������</h2>
	<ol>'.$links['stars_titles'].'</ol>
	<h2 id="links_stars_titles_en">������ �� ������ � �������� ENG</h2>
	<ol>'.$links['stars_titles_en'].'</ol>
	<h2 id="links_stars_simple">������ �� ������ �������</h2>
	<ol>'.$links['stars_simple'].'</ol>
	<h2 id="links_stars_list">������ ������ �� �����</h2>
	<ol>'.$links['stars_list'].'</ol>
	';

    return $content;
}

// �������� ������������ ���������� ����� �� �����. ��������� �������, ������� �������.
// ���������� ���������� ����������� �����.
function add_videos_on_domain($domain, $q)
{
    // $q=1;
    // $domain='russiaporno.net';
    if (empty($q)) {
        logger()->info('����� ��� ������ '.$domain.' �� ���������.');

        return 0;
    }
    $booked = return_value_for_domain('videohub', 'only_booked_videos', $domain);
    $described = return_value_for_domain('videohub', 'only_described_videos', $domain);
    $only_titled = return_value_for_domain('videohub', 'only_titled_videos', $domain);
    $booking_type = return_value_for_domain('videohub', 'booking_type', $domain);
    // pre($only_titled);
    if ($booked == '0') {
        if ($described == '0') {
            if ($only_titled == '1') {
                $data = query('
					select id from '.tabname('videohub', 'videos').'
					where (title_ru is not null and title_ru!=\'\') and (domain is null or domain=\'\') and (saved=\'yes\' or saved=\'no\')
					and ((booked_domain is null) or (booked_domain in (\''.$booking_type.'\',\''.$domain.'\')))
					order by field(booked_domain,\''.$domain.'\'), add_date desc');
                // limit 0,'.$q);
            }
            if ($only_titled == '0') {
                $data = query('
					select id from '.tabname('videohub', 'videos').'
					where title_ru is null and (domain is null or domain=\'\') and (saved=\'yes\' or saved=\'no\')
					and ((booked_domain is null) or (booked_domain in (\''.$booking_type.'\',\''.$domain.'\')))
					order by field(booked_domain,\''.$domain.'\'), add_date desc');
                // limit 0,'.$q);
            }
        }
        if ($described == '1') {
            $data = query('
				select id from '.tabname('videohub', 'videos').'
				where (title_ru is not null and title_ru!=\'\') and (domain is null or domain=\'\') and (saved=\'yes\' or saved=\'no\')
				and ((booked_domain is null) or (booked_domain in (\''.$booking_type.'\',\''.$domain.'\'))) and (description is not null and description!=\'\')
				order by field(booked_domain,\''.$domain.'\',\''.$booking_type.'\'), add_date desc');
            // limit 0,'.$q);
        }
    }
    if ($booked == '1') {
        if ($described == '0') {
            $data = query('
				select id from '.tabname('videohub', 'videos').'
				where (title_ru is not null and title_ru!=\'\') and (domain is null or domain=\'\') and (saved=\'yes\' or saved=\'no\')
				and booked_domain in (\''.$booking_type.'\',\''.$domain.'\')
				order by booked_domain desc, add_date desc');
            // limit 0,'.$q);
        }
        if ($described == '1') {
            $data = query('
				select id from '.tabname('videohub', 'videos').'
				where (title_ru is not null and title_ru!=\'\') and (domain is null or domain=\'\') and (saved=\'yes\' or saved=\'no\')
				and (booked_domain in (\''.$booking_type.'\',\''.$domain.'\')) and (description is not null and description!=\'\')
				order by booked_domain desc, add_date desc');
            // limit 0,'.$q);
        }
    }
    // ��������� �������
    $arr_all = readdata($data, 'nokey');
    // pre($arr_all);
    shuffle($arr_all); // ������������ ������ �� ����� ���������� ����� (����� ����� �� array_rand ���, ��� ����������� �� ���������)
    $q_arr_all = count($arr_all);
    // pre($q_arr_all);
    if ($q > $q_arr_all) {
        $q = $q_arr_all;
    }
    $arr = [];
    if ($q != 0) {
        $arr_rnd_keys = array_rand($arr_all, $q);
        // pre($arr_rnd_keys);
        if (is_array($arr_rnd_keys)) {
            foreach ($arr_rnd_keys as $v) {
                $arr[] = $arr_all[$v];
            }
        } else {
            $arr[] = $arr_all[$arr_rnd_keys]; // ���� $q=1 �� array_rand ���������� ������ �����
        }
        // pre($arr);
    }
    $q_videos = count($arr);
    if ($q_videos == 0) {
        logger()->info('��� ��������� �����.');
    }
    // pre($arr);
    foreach ($arr as $v) {
        query('update '.tabname('videohub', 'videos').' set domain=\''.$domain.'\', apply_date=current_timestamp where id=\''.$v['id'].'\';');
    }
    // ��������� local_id
    update_local_ids([
        'table' => tabname('videohub', 'videos'),
        'where_clause' => 'domain=\''.$domain.'\'']
    );
    logger()->info('��������� '.$q_videos.' ����� ��� ������ '.$domain.'. Booking type: '.$booking_type);

    // temp_var('q',$q_videos);
    // temp_var('date',date('Y-m-d'));
    // add_news($domain,template('site_news/temp_add_videos',$domain));
    return $q_videos;
}

function submit_add_videos_on_site()
{
    if (empty($_POST['domain'])) {
        error('�� ������� �����');
    }
    if (empty($_POST['q'])) {
        error('�� �������� ���������� �����');
    }
    $q_added = add_videos_on_domain($_POST['domain'], $_POST['q']);
    alert('��������� '.$q_added.' ����� ��� ����� '.$_POST['domain']);
}

function query_select_tags($arr)
{
    if (empty($arr['tags_ru'])) {
        error('�� ������� ������ �����');
    }
    if (empty($arr['group_id'])) {
        error('�� ������� ID ������');
    }
    $list = '';
    foreach ($arr['tags_ru'] as $tag_ru) {
        query('insert into '.tabname('videohub', 'catgroups_tags').' (group_id,tag_ru)
			values (
				\''.$arr['group_id'].'\',
				\''.mysql_real_escape_string($tag_ru).'\'
				)');
        $list .= '\''.$tag_ru.'\' ';
    }

    return '��������� ����: '.$list.'" ��� ������ "'.$arr['group_id'].'"<br/>';
}

// ���������� ����� ������ ����� ��� �����
function submit_select_tags()
{
    if (empty($_POST['tag_ru'])) {
        error('�� ������� ������ �����');
    }
    query('delete from '.tabname('videohub', 'catgroups_tags').' where group_id=\''.$_POST['group_id'].'\'');
    $return = query_select_tags(['tags_ru' => $_POST['tag_ru'], 'group_id' => $_POST['group_id']]);
    alert($return);
}

// ����� ������ ����� ��� ������
function form_select_tags()
{
    // $data=query('select distinct tag_ru from '.tabname('videohub','tags_translation').' order by tag_ru');
    $data = query('select tag_ru,count(*) as q
		/*
		from '.tabname('videohub', 'tags_translation').'
		join '.tabname('videohub', 'tags').' on tags.tag_en=tags_translation.tag_en
		*/
		from '.tabname('videohub', 'tags').'
		where tag_ru is not null
		group by tag_ru
		having q>10
		order by q desc;
		');
    $arr = readdata($data, 'nokey');
    $list = '';
    $q = 0;
    foreach ($arr as $k => $v) {
        $q++;
        $checked_tags = query('select count(*) as q from '.tabname('videohub', 'catgroups_tags').' where group_id=\''.$_GET['group_id'].'\' and tag_ru=\''.$v['tag_ru'].'\'');
        $q_checked = mysql_fetch_assoc($checked_tags);
        if ($q_checked['q'] == 0) {
            $checked_atr = '';
        }
        if ($q_checked['q'] != 0) {
            $checked_atr = 'checked';
        }
        $list .= '<li><label class="checkbox"><input type="checkbox" name="tag_ru['.$q.']" value="'.$v['tag_ru'].'" '.$checked_atr.'/>'.$v['tag_ru'].'</label></li>
		';
    }

    return $list;
}

// ���������� ����� � ������������� ������
function submit_tags_translated()
{
    if (empty($_POST['tag'])) {
        error('�� ������� ������ �����');
    }
    foreach ($_POST['tag'] as $k => $v) {
        if (! empty($v)) {
            query('update '.tabname('videohub', 'tags_translation').' set tag_ru=\''.$v.'\' where tag_en=\''.$k.'\'');
        }
    }
    alert('���� ���������');
}

// ������� ����� � ������������� ������ � ������������ ��������������
function form_tags_translated()
{
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $q_tags_per_page = 100;
    $from = ($_GET['p'] - 1) * $q_tags_per_page;
    $data = query('select count(*) as q from '.tabname('videohub', 'tags_translation'));
    $arr = mysql_fetch_assoc($data);
    $q_tags = $arr['q'];
    $data = query('
				select tag_en,tag_ru from '.tabname('videohub', 'tags_translation').'  order by tag_en limit '.$from.','.$q_tags_per_page);
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<tr><td>'.$v['tag_en'].'</td>
		<td><input type="text" name="tag['.$v['tag_en'].']" value="'.$v['tag_ru'].'"/></td>
		<td><a href="/?page=admin/tags/delete_tags_translated&amp;tag='.$v['tag_en'].'">X</a></td></tr>
		';
    }
    $content = '
	<div class="pages"><ul>'.page_numbers($q_tags, $q_tags_per_page, $_GET['p'], '/?page=admin/tags/tags_translated&amp;p=').'</ul></div><hr/>
	<table class="table table-striped" style="width: 500px;">'.$list.'</table>
	<div class="pages"><ul>'.page_numbers($q_tags, $q_tags_per_page, $_GET['p'], '/?page=admin/tags/tags_translated&amp;p=').'</ul></div><hr/>
	';

    return $content;
}

// ������� ����� � ������������� ������ ��� ����������� ��������������
function form_tags_translated_lite()
{
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $q_tags_per_page = 100;
    $from = ($_GET['p'] - 1) * $q_tags_per_page;
    $data = query('select count(*) as q from '.tabname('videohub', 'tags_translation'));
    $arr = mysql_fetch_assoc($data);
    $q_tags = $arr['q'];
    $data = query('
		    select tag_en,tag_ru,translater,translate_date as date from '.tabname('videohub', 'tags_translation').'  order by date desc limit '.$from.','.$q_tags_per_page);
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<tr><td>'.$v['tag_en'].'</td>
		<td>'.$v['tag_ru'].'</td>
		<td>'.$v['translater'].'</td>
		<td>'.$v['date'].'</td></tr>
		';
    }
    $content = '<div class="pages"><ul>'.page_numbers($q_tags, $q_tags_per_page, $_GET['p'], '/?page=admin/tags/tags_translated_lite&amp;p=').'</ul></div><hr/>
	<table class="table table-striped" style="width: 600px;">'.$list.'</table>';

    return $content;
}

// ���������� ����� � ��������������� ������
function submit_tags_untranslated()
{
    if (empty($_POST['tag'])) {
        error('�� ������� ������ �����');
    }
    // echo '<pre>'; print_r($_POST); echo '</pre>';
    $i = 0;
    $content = '';
    foreach ($_POST['tag'] as $k => $v) {
        if (! empty($v)) {
            $data = query('select tag_ru, translater from '.tabname('videohub', 'tags_translation').' where tag_en=\''.$k.'\'');
            $arr = mysql_fetch_assoc($data);
            if (! empty($arr)) {
                $content .= '��� "'.$k.'" ��� ������� "'.$arr['translater'].'" ("'.$arr['tag_ru'].'")<br/>';
            } else {
                $i++;
                query('insert into '.tabname('videohub', 'tags_translation').' (tag_en,tag_ru,translater,translate_date) values (\''.$k.'\',\''.$v.'\',\''.auth_login(['type' => 'videohub']).'\',current_timestamp)');
            }
        }
    }
    $content .= '���������� �����: '.$i;

    return $content;
    // alert('���� ����������');
}

// �������� ��������������� ���� �� ���� �����
function delete_tags_untranslated()
{
    if (! isset($_GET['tag'])) {
        error('�� ������� ���');
    }
    query('delete from '.tabname('videohub', 'tags').' where tag_en=\''.$_GET['tag'].'\'');
    alert('��� '.$_GET['tag'].' ������');
}

// �������� ������������� ���� �� ���� ����� � �������� ��� ��������
function delete_tags_translated()
{
    if (! isset($_GET['tag'])) {
        error('�� ������� ���');
    }
    query('delete from '.tabname('videohub', 'tags').' where tag_en=\''.$_GET['tag'].'\'');
    query('delete from '.tabname('videohub', 'tags_translation').' where tag_en=\''.$_GET['tag'].'\'');
    alert('��� '.$_GET['tag'].' ������');
}

// ������� ����� � ��������������� ������
function form_tags_untranslated()
{
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    if (isset($_GET['stars']) and $_GET['stars'] == 1) {
        $only_stars = 1;
        $where_clause = 'and stars.name is not null';
        $join_clause = 'left join '.tabname('videohub', 'stars').'	on stars.name=tags.tag_en';
    } else {
        $where_clause = '';
        $join_clause = '';
        $only_stars = 0;
    }
    $q_tags_per_page = 100;
    $from = ($_GET['p'] - 1) * $q_tags_per_page;
    $data = query('
				select SQL_CALC_FOUND_ROWS tags.tag_en, count(tags.tag_en) as q from '.tabname('videohub', 'tags').'
				left join '.tabname('videohub', 'tags_translation').'
				on tags.tag_en=tags_translation.tag_en
				'.$join_clause.'
				where tags_translation.tag_en is null '.$where_clause.'
				group by tags.tag_en
				order by q desc
				limit '.$from.','.$q_tags_per_page.'
				');
    $arr = readdata($data, 'nokey');
    $q_tags = rows_without_limit();
    if ($q_tags == 0) {
        alert('��� �������������� �����');
    }
    if ($_GET['p'] > ceil($q_tags / $q_tags_per_page / 2)) {
        error('�������� ������ ������ �������� �������');
    }
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<tr><td>'.$v['tag_en'].' ('.$v['q'].') (<a href="https://translate.google.com/#en/ru/'.rawurlencode($v['tag_en']).'" target="_blank">G</a>)</td>
		<td><input type="text" spellcheck="true" name="tag['.$v['tag_en'].']"/></td>
		<td><a href="/?page=admin/tags/delete_tags_untranslated&amp;tag='.$v['tag_en'].'">X</a></td></tr>
		';
    }
    $content = '����� �� ����������: '.$q_tags.'<br/>��������: '.$q_tags_per_page.'<br/>
	<div class="pages"><ul>'.page_numbers($q_tags, $q_tags_per_page, $_GET['p'], '/?page=admin/tags/tags_untranslated&amp;p=').'</ul></div><hr/>
	<table class="table table-striped" style="width: 450px;">'.$list.'</table>';

    return $content;
}

// ���������� ����� ��� ����� ������ ��� ���������
function submit_categories_headers_form_admin()
{
    if (isset($_POST['category_id']) == 0) {
        error('������ �������� ��������� �� �������');
    }
    $translit_affected = '';
    foreach ($_POST['category_id'] as $k => $v) {
        query('update '.tabname('videohub', 'categories_domains').' set
			h1=\''.square2braces($v['h1']).'\',
			h2=\''.square2braces($v['h2']).'\',
			photo_h1=\''.square2braces($v['photo_h1']).'\',title=\''.square2braces($v['title']).'\',
			title_translit=\''.square2braces($v['title_translit']).'\',
			photo_title=\''.square2braces($v['photo_title']).'\',text=\''.square2braces($v['text']).'\',
			photo_text=\''.square2braces($v['photo_text']).'\',keywords=\''.square2braces($v['keywords']).'\',
			description=\''.square2braces($v['description']).'\',
			photo_description=\''.square2braces($v['photo_description']).'\',
			text_2=\''.square2braces($v['text_2']).'\'
			where domain=\''.$_POST['domain'].'\' and category_id=\''.$k.'\';');
        query('update '.tabname('videohub', 'categories_domains').' set title_translit=\''.url_translit(square2braces($v['title'])).'\'
			where domain=\''.$_POST['domain'].'\' and category_id=\''.$k.'\' and (title_translit is null or title_translit=\'\');');
        if (mysql_affected_rows() != 0) {
            $translit_affected .= '<br/>��� ��������� "'.square2braces($v['title']).'" ��� ������������� ������ ��������.';
        }
    }
    alert('������ ��� ��������� ������ '.$_POST['domain'].' ���������. '.$translit_affected);
}

// ������� ����� ��� ����� ������ ��� ���������
function categories_headers_form_admin()
{
    $data = query('
				select categories_domains.category_id,categories_domains.title,categories_domains.title_translit,
				categories_domains.photo_title, categories_domains.h1, categories_domains.h2, categories_domains.photo_h1,
				categories_domains.text, categories_domains.photo_text, categories_domains.keywords,
				categories_domains.description, categories_domains.photo_description, categories_groups.name,
				categories_domains.text_2
				from '.tabname('videohub', 'categories_domains').'
				inner join '.tabname('videohub', 'categories_groups').' on categories_domains.category_id=categories_groups.id
				where domain=\''.$_GET['domain'].'\'
				order by name'
    );
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<li>'.$v['name'].':<br/>
		��������� �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][title]" value="'.htmlspecialchars(braces2square($v['title']), ENT_QUOTES, 'cp1251').'"/><br/>
		�������� ��������� �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][title_translit]" value="'.htmlspecialchars(braces2square($v['title_translit']), ENT_QUOTES, 'cp1251').'"/><br/>
		��������� ����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][photo_title]" value="'.htmlspecialchars(braces2square($v['photo_title']), ENT_QUOTES, 'cp1251').'"/><br/>
		���� �������� �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][keywords]" value="'.htmlspecialchars(braces2square($v['keywords']), ENT_QUOTES, 'cp1251').'"/><br/>
		���� �������� �����:<br/><input class="ckeditor" style="width: 800px;" name="category_id['.$v['category_id'].'][description]" value="'.htmlspecialchars(braces2square($v['description']), ENT_QUOTES, 'cp1251').'"/><br/>
		���� �������� ����:<br/><input class="ckeditor" style="width: 800px;" name="category_id['.$v['category_id'].'][photo_description]" value="'.htmlspecialchars(braces2square($v['photo_description']), ENT_QUOTES, 'cp1251').'"/><br/>
		H1 �����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][h1]" value="'.htmlspecialchars(braces2square($v['h1']), ENT_QUOTES, 'cp1251').'" /><br/>
		H1 ����:<br/><input class="ckeditor" style="width: 400px;" name="category_id['.$v['category_id'].'][photo_h1]" value="'.htmlspecialchars(braces2square($v['photo_h1']), ENT_QUOTES, 'cp1251').'" /><br/>
		�����:<br/><textarea class="ckeditor" name="category_id['.$v['category_id'].'][text]" style="width: 700px; height: 200px;" />'.htmlspecialchars(braces2square($v['text']), ENT_QUOTES, 'cp1251').'</textarea>
		����� ����:<br/><textarea class="ckeditor" name="category_id['.$v['category_id'].'][photo_text]" style="width: 700px; height: 200px;" />'.htmlspecialchars(braces2square($v['photo_text']), ENT_QUOTES, 'cp1251').'</textarea>
		����� 2:<br/><textarea class="ckeditor" name="category_id['.$v['category_id'].'][text_2]" style="width: 700px; height: 200px;" />'.htmlspecialchars(braces2square($v['text_2']), ENT_QUOTES, 'cp1251').'</textarea>
		</li>';
    }
    $content = '
	<form action="/?page=admin/domain/submit_categories_headers_form_admin" method="post">
	<ul>
	'.$list.'
	</ul>
	<input type="hidden" name="domain" value="'.$_GET['domain'].'"/>
	<input type="submit" name="submit" value="���������">
	</form>
	';

    return $content;
}

// ���������� ����� ����� ���������� ��������
function submit_headers_form_admin()
{
    if (empty($_POST['domain']) or empty($_POST['page'])) {
        error('�� �������� POST ����������');
    }
    query('delete from '.tabname('videohub', 'headers').' where domain=\''.$_POST['domain'].'\' and page=\''.$_POST['page'].'\'');
    query('
		  insert into '.tabname('videohub', 'headers').'
		  (domain,page,title,h1,h2,bread,description,keywords,text_top,text_bottom,text_1,text_2)
		  values (\''.$_POST['domain'].'\',
							 \''.square2braces($_POST['page']).'\',
							 \''.square2braces($_POST['title']).'\',
							 \''.square2braces($_POST['h1']).'\',
							 \''.square2braces($_POST['h2']).'\',
							 \''.square2braces($_POST['bread']).'\',
							 \''.square2braces($_POST['description']).'\',
							 \''.square2braces($_POST['keywords']).'\',
							 \''.square2braces($_POST['text_top']).'\',
							 \''.square2braces($_POST['text_bottom']).'\',
							 \''.square2braces($_POST['text_1']).'\',
							 \''.square2braces($_POST['text_2']).'\'
							 )
		  ');
    alert('��������� ��� �������� '.$_POST['page'].' ������ '.$_POST['domain'].' ���������');
}

// ������� ����� ��� ����� ���������� ��������
function headers_form_admin($page)
{
    $autosite = 'pizdenka.net';
    $autofilled = 0;
    $content = '';
    $arr = return_headers($_GET['domain'], $page);
    // ���������������� ���� ����� ��������������
    // if (empty($arr)) {$autofilled=1; $arr=return_headers($autosite,$page);};
    if (empty($arr)) {
        $autofilled = 0;
        $arr = ['title' => '', 'h1' => '', 'h2' => '', 'description' => '', 'keywords' => '', 'text_top' => '', 'text_bottom' => '', 'bread' => '', 'text_1' => '', 'text_2' => ''];
    }
    if (! empty($arr)) {
        foreach ($arr as $k => $v) {
            $arr[$k] = braces2square($v);
        }
    }
    if ($autofilled == 1) {
        $content .= '<br/>��������� ������������� � <b>'.$autosite.'</b><br/>';
    }
    $content .= '
	<form action="/?page=admin/domain/submit_headers_form_admin" method="post" name="'.$page.'">
	<ul class="unstyled">
	<li>���������:<br/><input name="title" value="'.htmlspecialchars($arr['title'], ENT_QUOTES, 'cp1251').'" style="width: 700px;"/></li>
	<li>H1:<br/><input name="h1" value="'.htmlspecialchars($arr['h1'], ENT_QUOTES, 'cp1251').'" style="width: 700px;"/></li>
	<li>H2:<br/><input name="h2" value="'.htmlspecialchars($arr['h2'], ENT_QUOTES, 'cp1251').'" style="width: 700px;"/></li>
	<li>������� ������: <br/><input name="bread" value="'.htmlspecialchars($arr['bread'], ENT_QUOTES, 'cp1251').'" style="width: 700px;"/></li>
	<li>��������:<br/><input name="description" value="'.$arr['description'].'" style="width: 700px;"/></li>
	<li>�������� �����:<br/><input name="keywords" value="'.$arr['keywords'].'" style="width: 700px;"/></li>
	<li>����� �������� �������:<br/><textarea id="'.$page.'_text_top" class="ckeditor" name="text_top" style="width: 700px; height: 200px;">'.htmlspecialchars($arr['text_top'], ENT_QUOTES, 'cp1251').'</textarea></li>
	<li>����� �������� ������:<br/><textarea id="'.$page.'_text_bottom" class="ckeditor" name="text_bottom" style="width: 700px; height: 200px;">'.htmlspecialchars($arr['text_bottom'], ENT_QUOTES, 'cp1251').'</textarea></li>
	<li>����� 1:<br/><textarea id="'.$page.'_text_1" class="ckeditor" name="text_1" style="width: 700px; height: 200px;">'.htmlspecialchars($arr['text_1'], ENT_QUOTES, 'cp1251').'</textarea></li>
	<li>����� 2:<br/><textarea id="'.$page.'_text_2" class="ckeditor" name="text_2" style="width: 700px; height: 200px;">'.htmlspecialchars($arr['text_2'], ENT_QUOTES, 'cp1251').'</textarea></li>
	</ul>
	<input type="hidden" name="domain" value="'.$_GET['domain'].'"/>
	<input type="hidden" name="page" value="'.$page.'"/>
	<input type="submit" name="submit" value="���������">
	</form>
	';

    return $content;
}

function visible_domains_videohub($arr = [])
{
    if (empty($arr['sort'])) {
        $arr['sort'] = 'create_date,domain';
    }
    $data = query('select domain,status from '.tabname('engine', 'domains').'
		where type=\'videohub\' and ifnull(
			(
				select value from '.tabname('videohub', 'config_for_domain').'
				where domain=domains.domain and property=\'visible\'
				),1
	)!=0
	order by '.$arr['sort'].';
	');
    $arr = readdata($data, 'nokey');

    return $arr;
}

// ������� ������ ������� ���� videohub
function domains_videohub()
{
    $arr = visible_domains_videohub();
    $list = '';
    foreach ($arr as $v) {
        $domain_for_id = str_replace('.', '_', $v['domain']);
        if ($v['status'] == 'off' or $v['status'] == 'empty') {
            $status_class = 'text-error';
        } else {
            $status_class = 'text-success';
        }
        $list .= '
		<li class="dropdown">
		<a class="dropdown-toggle '.$status_class.'" data-toggle="dropdown" href="#">'.$v['domain'].' <b class="caret"></b></a>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
		<li><a href="/?page=admin/domain/headers&amp;domain='.$v['domain'].'">������ ��� �������</a></li>
		<li><a href="/?page=admin/domain/categories&amp;domain='.$v['domain'].'">������� ���������</a></li>
		<li><a href="/?page=admin/domain/add_videos&amp;domain='.$v['domain'].'">�������� �����</a></li>
		<li><a href="/?page=admin/domain/seo_links&amp;domain='.$v['domain'].'">������ ��� �� ���������</a></li>
		<li><a href="/?page=admin/domain/booking&amp;domain='.$v['domain'].'">������������</a></li>
		<li><a href="/?page=admin/domain/reset_cache&amp;domain='.$v['domain'].'">����� ����</a></li>
		<li><a href="/?page=admin/domain/functions&amp;domain='.$v['domain'].'">������</a></li>
		</ul>
		</li>';
    }

    return $list;
}

// ���������� ��� replace
// ���������� ����� ������ ��������� ��� ������
function submit_set_categories()
{
    if (isset($_POST['group']) == 0) {
        error('������ ����� �� �������');
    }
    // ������� ��� ���������� ������ ��� ������
    // query('delete from '.tabname('videohub','categories_domains').' where domain=\''.$_POST['domain'].'\'');
    foreach ($_POST['group'] as $k => $v) {
        $data = query('select count(*) as q,category_id from '.tabname('videohub', 'categories_domains').'
			inner join '.tabname('videohub', 'categories_groups').'
			on categories_domains.category_id=categories_groups.id
			where domain=\''.$_POST['domain'].'\' and group_id=\''.$k.'\'
			group by category_id
			');
        // select domain, category_id, group_id from categories_domains inner join categories_groups on category_id=categories_groups.id where group_id='4'
        $arr = mysql_fetch_assoc($data);
        // print_r($arr);
        if ($arr['q'] == 0) {
            query('insert into '.tabname('videohub', 'categories_domains').' (domain,category_id) values (\''.$_POST['domain'].'\',\''.$v.'\')');
        }
        if ($arr['q'] == 1) {
            query('update '.tabname('videohub', 'categories_domains').'  set category_id=\''.$v.'\' where domain=\''.$_POST['domain'].'\' and category_id=\''.$arr['category_id'].'\'');
        }
        if ($arr['q'] > 1) {
            error('� ������� '.tabname('videohub', 'categories_domains').' ���������� ������������� ������ � ������� '.$_POST['domain'].' � ����������� ������ '.$k.'.');
        }
    }
    alert('��������� ��� ������ '.$_POST['domain'].' �������');
}

// ����� ��������� ��� �����
function set_categories()
{
    if (empty($_GET['domain'])) {
        error('�� ����� �����');
    }
    // �������� ������ ���������� �� ��� �����
    $data = query('select distinct group_id from '.tabname('videohub', 'categories_groups').' where show_in_admin=\'1\' order by group_id;');
    $arr = readdata($data, 'nokey');
    // �������� ��� ���������� ��������� ��� ������� ������
    $data_cats = query('select category_id from '.tabname('videohub', 'categories_domains').' where domain=\''.$_GET['domain'].'\'');
    $arr_cats = readdata($data_cats, 'category_id');
    $list = '';
    $q = 0;
    foreach ($arr as $v) {
        $data2 = query('
					 SELECT categories_groups.id, categories_groups.name, COUNT( categories_domains.id ) AS onsites
					 FROM '.tabname('videohub', 'categories_groups').'
					 LEFT JOIN '.tabname('videohub', 'categories_domains').' ON categories_groups.id = categories_domains.category_id
					 WHERE categories_groups.group_id='.$v['group_id'].'
					 GROUP BY categories_groups.id
					 ;');
        $arr2 = readdata($data2, 'nokey');
        $name_list = '';
        foreach ($arr2 as $v2) {
            $checked = '';
            if (isset($arr_cats[$v2['id']]) == 1) {
                $checked = 'checked';
            }
            $name_list .= '
			<li><label class="radio"><input name="group['.$v['group_id'].']" type="radio" value="'.$v2['id'].'" '.$checked.' />'.$v2['name'].' ('.$v2['onsites'].')</label></li>';
        }
        if ($q % 5 == 0) {
            $list .= '<tr>';
        }
        $list .= '<td>������ #'.$v['group_id'].':<ul class="names">
		'.$name_list.'
		</ul></td>';
        $q++;
        if ($q % 5 == 0) {
            $list .= '</tr>';
        }
    }
    if ($q % 5 != 0) {
        $list .= '<td colspan="'.(5 - ($q % 5)).'"></td></tr>';
    }

    return $list;
}

// ������ ���� ��������� �� �������
function categories_groups()
{
    $data = query('select distinct group_id from '.tabname('videohub', 'categories_groups').' where show_in_admin=\'1\' order by group_id;');
    $arr = readdata($data, 'nokey');
    $list = '';
    $q = 0;
    foreach ($arr as $v) {
        $data_tags = query('select tag_ru from '.tabname('videohub', 'catgroups_tags').' where group_id='.$v['group_id']);
        $arr_tags = readdata($data_tags, 'nokey');
        $tags_list = '';
        foreach ($arr_tags as $v_tags) {
            $tags_list .= '<li>'.$v_tags['tag_ru'].'</li>';
        }
        $data2 = query('select name from '.tabname('videohub', 'categories_groups').' where group_id='.$v['group_id'].'');
        $arr2 = readdata($data2, 'nokey');
        $name_list = '';
        foreach ($arr2 as $v2) {
            $name_list .= '<li>'.$v2['name'].'</li>';
        }
        if ($q % 5 == 0) {
            $list .= '<tr>';
        }
        $list .= '<td>
		������ #'.$v['group_id'].':<ul class="names">'.$name_list.'</ul>
		����: <ul class="tags">'.$tags_list.'</ul>
		<a class="btn" type="button" href="/?page=admin/categories/select_tags&amp;group_id='.$v['group_id'].'">�������</a>
		</td>';
        $q++;
        if ($q % 5 == 0) {
            $list .= '</tr>';
        }
    }
    if ($q % 5 != 0) {
        $list .= '<td colspan="'.(5 - ($q % 5)).'"></td></tr>';
    }

    return $list;
}

function query_add_category($arr)
{
    if (empty($arr['name'])) {
        error('������ �������� ���������');
    }
    if (empty($arr['group_id'])) {
        error('������ ID ������ ���������');
    }
    if (empty($arr['type'])) {
        $arr['type'] = 'default';
    }
    query('insert into '.tabname('videohub', 'categories_groups').' (name,name_translit,group_id,name_url,type)
		values (
			\''.mysql_real_escape_string($arr['name']).'\',
			\''.mysql_real_escape_string(url_translit($arr['name'])).'\',
			\''.$arr['group_id'].'\',
			\''.mysql_real_escape_string(str_replace(' ', '_', $arr['name'])).'\',
			\''.$arr['type'].'\'
			)');

    return '��������� ��������� "'.$arr['name'].'" � ������ "'.$arr['group_id'].'". ���: '.$arr['type'].'.';
}

// ���������� ����� ���������� ���������
function submit_add_category()
{
    if (empty($_POST['name'])) {
        error('�� �������� �������� ���������');
    }
    if (empty($_POST['group_id'])) {
        error('�� ������� ����� ������');
    }
    $return = query_add_category(['name' => $_POST['name'], 'group_id' => $_POST['group_id']]);
    alert($return);
}

// ��������� ����� �����
function grab_empty_tags()
{
    if (empty($_GET['skip'])) {
        $skip = 0;
    } else {
        $skip = $_GET['skip'];
    }
    if (empty($_GET['q'])) {
        $q_videos = 50;
    } else {
        $q_videos = $_GET['q'];
    }
    $data = query('select count(*) as q_without from '.tabname('videohub', 'videos').' where id not in (select distinct video_id from '.tabname('videohub', 'tags').')');
    $arr = mysql_fetch_assoc($data);
    echo '����� ��� �����: '.$arr['q_without'].'<br/>';
    echo '��������� '.$skip.' �����. ����������� ���������� skip. �� ��������� 0.<br/>';
    echo '������� '.$q_videos.' �����. ����������� ���������� q. �� ��������� 50.<br/>';
    // $data=query('select videos.id from '.tabname('videohub','videos').' left join '.tabname('videohub','tags').' on videos.id=tags.video_id where tags.video_id is null and domain=\'pornorubka.net\'');
    $data = query('select videos.id from '.tabname('videohub', 'videos').' left join '.tabname('videohub', 'tags').' on videos.id=tags.video_id where tags.video_id is null limit '.$skip.','.$q_videos);
    // print_r(readdata($data,'nokey'));
    $arr = readdata($data, 'nokey');
    echo '<ol>';
    foreach ($arr as $k => $v) {
        echo '<li>';
        echo $v['id'].'<br/>';
        flush();
        $tags = media_for_video($v['id'], 'tags');
        if (empty($tags)) {
            echo '����������� ���� ��� ����� '.$v['id'].'<br/>';
        } else {
            foreach ($tags as $tag) {
                // echo('insert into '.tabname('videohub','tags').' (video_id,tag_en) values (\''.$v['id'].'\',\''.$tag.'\')<br/>');
                query('insert into '.tabname('videohub', 'tags').' (video_id,tag_en) values (\''.$v['id'].'\',\''.checkstr($tag).'\')');
                // sleep(2);
            }
        }
        echo '</li>';
        flush();
    }
    echo '</ol>';
}

// ������� ���� �����
function translate_tags()
{
    $data = query('select tag_en from '.tabname('videohub', 'tags').' group by tag_en');
    $tags = readdata($data, 'nokey');
    foreach ($tags as $k => $v) {
        $data = query('select tag_ru from '.tabname('videohub', 'tags_translation').' where tag_en=\''.$v['tag_en'].'\'');
        if (mysql_num_rows($data) == 0) {
            query('insert into '.tabname('videohub', 'tags_translation').' (tag_en) values (\''.$v['tag_en'].'\')');
        }
        $arr = mysql_fetch_assoc($data);
        query('update '.tabname('videohub', 'tags').' set tag_ru=\''.$arr['tag_ru'].'\' where tag_en=\''.$v['tag_en'].'\'');
    }
}

// ����������� �����
function video_player_admin()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID');
    }
    $data = query('select count(*) from '.tabname('videohub', 'videos').' where id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_array($data);
    if ($arr[0] == 0) {
        error('����� �� �������', '����� '.$_GET['id'].' �� �������', 1, 404);
    }

    return media_for_video($_GET['id'], 'embed_player');
}

// �������� ����������� �������� � �����
function copy_empty_images()
{
    global $config;
    // pre($config);
    $arr = readdata(query('select id,hub,id_hub,ph_date from '.tabname('videohub', 'videos').' where deleted=\'no\';'), 'nokey');
    $list = '';
    $i = 0;
    foreach ($arr as $k => $v) {
        $image_file = $config['videohub']['path']['images'].$v['id'].'.jpg';
        if (file_exists($image_file) == 0) {
            $image_url = media_for_video($v['id'], 'image');
            if (! empty($image_url)) {
                // copytoserv($image_url,$image_file);
            }
            $list .= '<li>#'.$v['id'].' #'.$v['id_hub'].' '.$v['hub'].' '.$image_url.'</li>'."\r\n";
            // pre(media_for_video($v['id'],'image').' ID: '.$v['id']);
            // copytoserv(media_for_video($v['id'],'image'),$image_file);
            // $list.='<li>#'.$v['id'].' <img src="/types/videohub/images/'.$v['id'].'.jpg" /></li>'."\r\n";
            // $list.='<li>#'.$v['id'].' '.media_for_video($v['id'],'image').'</li>';
            // $i++;
        }
        // if ($i==1) {break;};
    }
    $content = '<ol>'.$list.'</ol>';

    return $content;
}
