<?php

namespace MegawebV1;

/*
function videohub_booked_stat() {
    $arr['domains']=query_arr('
        select domains.domain, domain_config.*
        from '.tabname('engine','domains').'
        left join '.typetab('domain_config').' on domain_config.domain=engine.domains.domain
        where domains.type=\'videohub\' and domains.status=\'on\'
        order by domains.domain
        ;');
    // select domains.domain from '.tabname('engine','domains').' where type=\'videohub\'
    temp_var('domain',$v['domain']);
    temp_var('booking_type',$v['booking_type']);
    temp_var('translate_to_eng',$v['translate_to_eng']);
    temp_var('[template_star_link',$v['[template_star_link']);
    temp_var('template_category_link',$v['template_category_link']);
    temp_var('template_video_link',$v['template_video_link']);
    temp_var('video_description_limit',$v['video_description_limit']);
    temp_var('video_title_alt',$v['video_title_alt']);
    temp_var('only_titled_videos',$v['only_titled_videos']);
    temp_var('galleries_per_page',$v['galleries_per_page']);
    temp_var('number_of_photos_per_update',$v['number_of_photos_per_update']);
    temp_var('only_described_photos',$v['only_described_photos']);
    temp_var('only_booked_photos',$v['only_booked_photos']);
    temp_var('collection_videos_per_page',$v['collection_videos_per_page']);
    temp_var('number_of_same_video',$v['number_of_same_video']);
    temp_var('number_of_videos_per_update',$v['number_of_videos_per_update']);
    temp_var('pages_length',$v['pages_length']);
    temp_var('rewrite_function',$v['rewrite_function']);
    temp_var('visible',$v['visible']);
    temp_var('visible_translater',$v['visible_translater']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    temp_var('',$v['']);
    pre($arr);
}
*/

function stat_videos()
{
    $arr = mysql_fetch_row(query('select count(*) from '.tabname('videohub', 'videos')));
    $stat['all'] = $arr[0];
    // $arr=mysql_fetch_row(query('select count(*) from '.tabname('videohub','videos').' where title_ru is not null and title_ru!=\'\''));
    // $stat['translated']=$arr[0];
    $arr = mysql_fetch_row(query('select count(*) from '.tabname('videohub', 'videos').' where saved!=\'error\' and title_ru is not null and title_ru!=\'\' and (domain is null or domain=\'\') and booked_domain is null;'));
    $stat['free_with_title'] = $arr[0];
    $arr = mysql_fetch_row(query('select count(*) from '.tabname('videohub', 'videos').' where saved!=\'error\' and title_ru is not null and title_ru!=\'\' and (domain is null or domain=\'\') and description is not null and booked_domain=\'with_description\';'));
    $stat['free_with_description'] = $arr[0];
    $arr = mysql_fetch_row(query('select count(*) from '.tabname('videohub', 'videos').' where saved!=\'error\' and (title_ru is null or title_ru=\'\') and description is null and domain is null;'));
    $stat['nontranslated'] = $arr[0];
    $booked_domains = query_arr('select distinct value as booked_domain from '.typetab('config_for_domain').' where property=\'booking_type\';');
    $data = query('select booked_domain,count(*) as q from '.typetab('videos').' 
		where saved!=\'error\' and (title_ru is null or title_ru=\'\') and description is null 
		and domain is null and booked_domain is not null 
		and booked_domain in (select distinct value from '.typetab('config_for_domain').' where property=\'booking_type\')
		group by booked_domain
		;');
    // pre($arr);
    /*
    $booked_domains=query_arr('
        select distinct booked_domain,count(*) as q_videos
        from '.typetab('videos').'
        where booked_domain in (select distinct booking_type from '.typetab('config_for_domain').' where property=\'booking_type\')
        group by booked_domain
        order by booked_domain
        ;');
        */
    $free_bookings = readdata($data, 'booked_domain');
    $list = '';
    foreach ($booked_domains as $v) {
        $list .= '<tr><td>'.(empty($v['booked_domain']) ? '��� �����' : $v['booked_domain']).'</td><td>'.(isset($free_bookings[$v['booked_domain']]) ? $free_bookings[$v['booked_domain']]['q'] : '0').'</td></tr>';
    }
    // $arr=mysql_fetch_row(query('select count(*) from '.tabname('videohub','videos').' where domain is not null and domain!=\'\''));
    // $stat['used']=$arr[0];
    // $arr=readdata(query('select domain,count(*) as q from '.tabname('videohub','videos').' group by domain order by domain;'),'nokey');
    $content = '<table class="table table-striped" style="width: 200px;">
	<tr><td colspan="2" align="center"><b>����������:</b></td></tr>
	<tr><td>����� �����:</td><td>'.$stat['all'].'</td></tr>
	<tr><td>�� ����������:</td><td>'.$stat['nontranslated'].'</td></tr>
	<tr><td>�������� � ����������:</td><td>'.$stat['free_with_title'].'</td></tr>
	<tr><td>�������� � ��������� (�� ������ �����):</td><td>'.$stat['free_with_description'].'</td></tr>
	</table>
	<table class="table table-striped" style="width: 200px;">
	<tr><td colspan="2" align="center"><b>���������� �� �����:</b></td></tr>
	'.$list.'
	</table>
	';

    return $content;
}

function videohub_sites()
{
    $data = query('
	select domains.domain,status,
	case when status=\'off\' then \'off\' when status=\'on\' then ifnull((select value from '.tabname('videohub', 'config_for_domain').' where property=\'number_of_videos_per_update\' and domain=domains.domain),(select value from '.tabname('videohub', 'config').' where property=\'number_of_videos_per_update\')) end as videos_per_update,
	case ifnull((select value from '.tabname('videohub', 'config_for_domain').' where property=\'visible_translater\' and domain=domains.domain),0)	when 0 then \'no\' when 1 then \'yes\' end as visible_translater,
	(select count(*) from '.tabname('videohub', 'videos').' where domain=domains.domain) as q_videos,
	(select count(*) from '.tabname('videohub', 'videos').' where booked_domain=domains.domain and domain is null) as q_free_booked_videos,
	(select count(*) from '.tabname('videohub', 'videos').' where booked_domain=domains.domain and domain is null and title_ru is not null) as q_free_booked_videos_with_title_ru,
	(select count(*) from '.tabname('videohub', 'videos').' where booked_domain=domains.domain and domain is null and description is not null) as q_free_booked_videos_with_description,
	case ifnull((select value from '.tabname('videohub', 'config_for_domain').' where property=\'only_booked_videos\' and domain=domains.domain),(select value from '.tabname('videohub', 'config').' where property=\'only_booked_videos\')) when 0 then \'no\'	when 1 then \'yes\'	end as only_booked_videos,
	ifnull((select value from '.tabname('videohub', 'config_for_domain').' where domain=domains.domain and property=\'visible\'),1) as visible,
	updates.last_date
	from '.tabname('engine', 'domains').'
	left join (select domain,max(apply_date) as last_date from '.typetab('videos').' group by domain) updates on updates.domain=domains.domain
	where type=\'videohub\'
	order by field(visible,\'1\',\'0\'),field(status,\'on\',\'empty\',\'off\'),domain;
	');
    $arr = readdata($data, 'nokey');
    $list = '';
    $li_stat_sum = 0;
    foreach ($arr as $k => $v) {
        // $li_stat=li_stat($v['domain']);
        if (empty($li_stat['LI_day_vis'])) {
            $li_stat_link = 'no data';
        } else {
            $li_stat_link = '<a href="http://liveinternet.ru/stat/'.$v['domain'].'" target="_blank">'.$li_stat['LI_day_vis'].'</a>';
            $li_stat_sum = $li_stat_sum + $li_stat['LI_day_vis'];
        }
        $class = '';
        if ($v['status'] == 'off' or $v['status'] == 'empty') {
            $class = 'class="warning"';
        }
        if ($v['only_booked_videos'] == 'yes' and $v['q_free_booked_videos_with_title_ru'] < 50) {
            $class = 'class="warning"';
        }
        $list .= '<tr '.$class.'>
		<td>'.($k + 1).'.</td>
		<td><a href="http://'.$v['domain'].'/">'.$v['domain'].'</a></td>
		<td class="data">'.$v['q_videos'].'</td>
		<td class="data">'.$v['q_free_booked_videos'].'</td>
		<td class="data">'.$v['q_free_booked_videos_with_title_ru'].'</td>
		<td class="data">'.$v['q_free_booked_videos_with_description'].'</td>
		<td class="data">'.$v['only_booked_videos'].'</td>
		<td class="data">'.$v['videos_per_update'].'</td>
		<td class="data">'.$v['visible_translater'].'</td>
		<td class="data">'.$v['status'].'</td>
		<td class="data">'.$li_stat_link.'</td>
		<td class="data">'.$v['last_date'].'</td>
		';
    }
    $content = '<table class="table table-striped" style="width: 700px;">
	<tr class="head"><td>#</td><td>�����</td><td>�����</td><td>����� �� �����������</td><td>����� �� ����������� � ������������ ����������</td><td>����� �� ����������� � ���������</td><td>��������� ������ ���������������</td><td>��������� ��������� �����</td><td>���������� ��� ��������</td><td>������</td><td>������������</td>
	<td>����. ����������</td>
	</tr>
	'.$list.'
	<tr class="head"><td colspan="10"></td><td style="text-align: center;">'.$li_stat_sum.'</td></tr>
	</table>';

    return $content;
}
