<?php

namespace MegawebV1;

// �������� ������� � ��������� (��������� � ������� ��������)
function add_serials_into_categories()
{
    $data = query('select turbik_serials.id,turbik_serials.title_ru,turbik_serials.title_en from '.tabname('videohub', 'turbik_serials').'
		left join '.tabname('videohub', 'categories_groups').' on categories_groups.name=turbik_serials.title_ru
		where categories_groups.id is null;
		;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $arr_group_id = mysql_fetch_assoc(query('select (ifnull(max(group_id),0)+1) as next_group_id from '.tabname('videohub', 'categories_groups').';'));
        $group_id = $arr_group_id['next_group_id'];
        query('update '.tabname('videohub', 'turbik_serials').' set group_id=\''.$group_id.'\' where id=\''.$v['id'].'\';');
        $list .= query_add_category(['name' => checkstr($v['title_ru']), 'group_id' => $group_id, 'type' => 'serial']).'<br/>';
        $list .= query_add_category(['name' => checkstr($v['title_en']), 'group_id' => $group_id, 'type' => 'serial']);
        // $list.=query_select_tags(array('tags_ru'=>array($v['name_ru']),'group_id'=>$group_id));
    }

    return $list;
}

// �������� �������� ������ �� ����������� GET � �������������� COOKIE
function turbik_result_quality($type)
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);
    $qualities = ['sq' => '', 'hq' => ''];
    $types = ['main' => '', 'alt' => '', 'arr' => ''];
    if (! isset($types[$type])) {
        error('��� �� ����������');
    }
    if ($arr['hq'] == '0') {
        $quality['main'] = 'sq';
    }
    if ($arr['hq'] == '1') {
        if (isset($_COOKIE['quality'])) {
            $result = $_COOKIE['quality'];
        }
        if (isset($_GET['quality']) & $_GET['quality'] != 'default') {
            $result = $_GET['quality'];
        }
        if (empty($result)) {
            $result = 'hq';
        }
        if (! isset($qualities[$result])) {
            error('�������� �� ����������');
        }
        $quality['main'] = $result;
        if ($result == 'sq') {
            $quality['alt'] = 'hq';
        }
        if ($result == 'hq') {
            $quality['alt'] = 'sq';
        }
    }
    if ($type == 'arr') {
        return $quality;
    } else {
        return $quality[$type];
    }
}

// ��������� �������� ��� kt_player
function turbik_player_qualities()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);
    if ($arr['hq'] == '0') {
        $content = '
		video_url: \'/direct-sq/'.$args['id'].'.mp4\',
		video_url_text: \'SQ\'
		';
    }
    if ($arr['hq'] == '1') {
        $content = '
		video_url: \'/direct-hq/'.$args['id'].'.mp4\',
		video_url_text: \'HQ\',
		video_alt_url: \'/direct-sq/'.$args['id'].'.mp4\',
		video_alt_url_text: \'SQ\'
		';
    }

    return $content;
}

function preparing_episode_info($args)
{
    if (empty($args['id'])) {
        error('�� ������� ID');
    }
    $data = query('select turbik_episodes.id as episode_id,turbik_episodes.episode_number,
		turbik_episodes.title_ru as episode_title_ru,turbik_episodes.title_en as episode_title_en,
		turbik_episodes.description as episode_description,turbik_episodes.big_img as episode_big_img,
		turbik_episodes.hq,turbik_episodes.lang_ru,
		turbik_seasons.season_number,
		turbik_serials.id as serial_id,turbik_serials.title_ru as serial_title_ru,
		turbik_serials.title_en as serial_title_en,turbik_serials.translation_studio
		from '.tabname('videohub', 'turbik_episodes').'
		join '.tabname('videohub', 'turbik_seasons').' on turbik_seasons.id=turbik_episodes.season_id
		join '.tabname('videohub', 'turbik_serials').' on turbik_serials.id=turbik_seasons.serial_id
		where turbik_episodes.id=\''.$args['id'].'\';');
    $arr = mysql_fetch_assoc($data);

    return $arr;
}

function turbik_next_episode()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);
    // pre($arr);
    $data = query('
		select turbik_episodes.id
		from '.tabname('videohub', 'turbik_episodes').'
		join '.tabname('videohub', 'turbik_seasons').' on turbik_seasons.id=turbik_episodes.season_id
		join '.tabname('videohub', 'turbik_serials').' on turbik_serials.id=turbik_seasons.serial_id
		where turbik_serials.id=\''.$arr['serial_id'].'\'
		order by season_number,episode_number;
		');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $k => $v) {
        if ($v['id'] == $args['id']) {
            if (isset($arr[$k + 1])) {
                $arr2 = preparing_episode_info(['id' => $arr[$k + 1]['id']]);
                temp_var('episode_id', $arr2['episode_id']);
                temp_var('season_number', $arr2['season_number']);
                temp_var('episode_number', $arr2['episode_number']);
                $return = template('items/next_episode');
            } else {
                $return = '�� �������� ��������� �����';
            }
        }
    }

    return $return;
}

function turbik_get_quality()
{
    if (empty($_GET['quality'])) {
        error('�� �������� ��������');
    }

    return checkstr($_GET['quality']);
}

function turbik_episode_translation()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);
    if ($arr['lang_ru'] == '1') {
        if (empty($arr['translation_studio'])) {
            $translation = '����';
        } else {
            $translation = $arr['translation_studio'];
        }
    } else {
        $translation = '���';
    }

    return $translation;
}

function turbik_episode_number()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);

    return $arr['episode_number'];
}

function turbik_season_number_by_episode()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);

    return $arr['season_number'];
}

function turbik_serial_title_en_by_episode()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);

    return $arr['serial_title_en'];
}

function turbik_serial_title_ru_by_episode()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);

    return $arr['serial_title_ru'];
}

function turbik_episode_description()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);

    return $arr['episode_description'];
}

function turbik_episode_title_ru()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);

    return $arr['episode_title_ru'];
}

function turbik_episode_title_en()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);

    return $arr['episode_title_en'];
}

function turbik_episode_image_big()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_episode_info', $args);

    return $arr['episode_big_img'];
}

function turbik_direct_big_pic()
{
    exit(filegetcontents_alt(turbik_episode_image_big()));
}

function turbik_serials_list()
{
    $serials = readdata(query('select id,title_ru,title_en from '.tabname('videohub', 'turbik_serials').' order by title_ru;'), 'nokey');
    $serials_list = '';
    foreach ($serials as $serial) {
        $data = query('select id,season_number from '.tabname('videohub', 'turbik_seasons').' where serial_id=\''.$serial['id'].'\' order by season_number;');
        $seasons = readdata($data, 'nokey');
        $seasons_list = '';
        foreach ($seasons as $season) {
            $data = query('select id,episode_number,title_ru,title_en from '.tabname('videohub', 'turbik_episodes').' where season_id=\''.$season['id'].'\' order by episode_number;');
            $episodes = readdata($data, 'nokey');
            $episodes_list = '';
            foreach ($episodes as $episode) {
                // $episodes_list.='<li onclick="ajax_request(\'player\',\'/?page=turbik-test/iframe&amp;id='.$episode['id'].'\');">'.$episode['episode_number'].'</li>';
                temp_var('id', $episode['id']);
                temp_var('episode_number', $episode['episode_number']);
                temp_var('title_ru', $episode['title_ru']);
                temp_var('title_en', $episode['title_en']);
                $episodes_list .= template('items/episode');
                clear_temp_vars();
            }
            // $seasons_list.='<li>'.$season['season_number'].'<ul>'.$episodes_list.'</ul></li>';
            temp_var('id', $season['id']);
            temp_var('serial_id', $serial['id']);
            temp_var('season_number', $season['season_number']);
            temp_var('episodes_list', $episodes_list);
            $seasons_list .= template('items/season');
            clear_temp_vars();
        }
        // $serials_list.='<li>'.$serial['title_ru'].'<ul>'.$seasons_list.'</ul></li>';
        temp_var('id', $serial['id']);
        temp_var('title_ru', $serial['title_ru']);
        temp_var('title_en', $serial['title_en']);
        temp_var('seasons_list', $seasons_list);
        $serials_list .= template('items/serial');
        clear_temp_vars();
    }
    // $list='<ul>'.$serials_list.'</ul>';
    $list = $serials_list;

    return $list;
}

function turbik_direct()
{
    $sid = value_from_config('videohub', 'turbik_sid');
    $token = sha1($sid + mt_rand(1, 10000));
    $retry = '';
    // $retry='/r';
    // $url='http://cdn.turbik.tv/'.$_GET['lang'].'/'.$_GET['eid'].'/'.$_GET['source'].'/'.$_GET['start'].'/'.$sid.'/'.$_GET['token'].'/'.$_GET['sign'].'/j';
    // $url='http://cdn.turbik.tv/'.$_GET['lang'].'/'.$_GET['eid'].'/'.$_GET['source'].'/'.$_GET['start'].'/'.$sid.'/'.$token.'/'.$sign.$retry.'/j';
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    if (empty($_GET['start'])) {
        $start = 0;
    } else {
        $start = (int) $_GET['start'];
    }
    $id = (int) $_GET['id'];
    $data = query('select eid,source_default,source_hq,hq from '.tabname('videohub', 'turbik_episodes').' where id=\''.$id.'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['source_default'])) {
        error('�� ������ ��������� ��� �����');
    }
    $lang = 1;
    $sign = sha1($token.$arr['eid'].'A2DC51DE0F8BC1E9');
    $quality = turbik_get_quality();
    if ($quality == 'hq') {
        if ($arr['hq'] == '1') {
            $source = $arr['source_hq'];
        } else {
            error('��� HQ ��������.');
        }
    }
    if ($quality == 'sq') {
        $source = $arr['source_default'];
    }
    if (empty($source)) {
        error('�������� �� ������');
    }
    // pre($source);
    $url = 'http://cdn.turbik.tv/'.$lang.'/'.$arr['eid'].'/'.$source.'/'.$start.'/'.$sid.'/'.$token.'/'.$sign.$retry.'/j';
    // die($url);
    $json = file_get_contents($url);
    $json_decode = json_decode($json);
    if (! isset($json_decode->location)) {
        return false;
    }
    $link = $json_decode->location;
    // die($link);
    header('Location: '.$link);
    exit('');
    // return $link;
}

function turbik_metadata_decode()
{
    $data = query('select id,metadata from '.tabname('videohub', 'turbik_episodes').' where hq is null limit 200;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li><ul>
		<li>'.$v['id'].'</li>
		<li id="metadata'.$v['id'].'">'.$v['metadata'].'</li>
		<li><input type="text" id="metadata_decode'.$v['id'].'" name="video['.$v['id'].'][metadata_decode]" /></li>
		<script>
		mtd_decode=base64_decode(unescape(document.getElementById(\'metadata'.$v['id'].'\').innerHTML));
		mtd_decode_element=document.getElementById(\'metadata_decode'.$v['id'].'\');
		mtd_decode_element.value=mtd_decode;
		// alert(mtd_decode);
		</script>
		</ul></li>';
    }
    $return = '
	<ol>'.$list.'</ol>';

    return $return;
}

function turbik_metadata_decode_submit()
{
    if (empty($_POST['video'])) {
        error('�� ������� ������ video');
    }
    $q = 0;
    foreach ($_POST['video'] as $k => $v) {
        $arr = [];
        $arr['source_default'] = copybetwen('<default>', '</default>', copybetwen('<sources2>', '</sources2>', $v['metadata_decode']));
        $arr['source_hq'] = copybetwen('<hq>', '</hq>', copybetwen('<sources2>', '</sources2>', $v['metadata_decode']));
        $arr['big_img'] = 'http:'.copybetwen('<screen>', '</screen>', $v['metadata_decode']);
        $arr['lang_ru'] = copybetwen('<ru>', '</ru>', copybetwen('<langs>', '</langs>', $v['metadata_decode']));
        $arr['eid'] = copybetwen('<eid>', '</eid>', $v['metadata_decode']);
        $arr['hq'] = copybetwen('</duration>  <hq>', '</hq>  <eid>', $v['metadata_decode']);
        // pre($arr);
        query('update '.tabname('videohub', 'turbik_episodes').' set
			metadata_decode=\''.$v['metadata_decode'].'\',
			source_default=\''.$arr['source_default'].'\',
			source_hq=\''.$arr['source_hq'].'\',
			big_img=\''.$arr['big_img'].'\',
			lang_ru=\''.$arr['lang_ru'].'\',
			eid=\''.$arr['eid'].'\',
			hq=\''.$arr['hq'].'\'
			where id=\''.$k.'\';');
        $q++;
    }

    return '��������� '.$q.' �����';
}

// ���������� metadata ��� ���� �����
function grabber_turbik()
{
    $db['serials'] = readdata(query('select id,url,skip from '.tabname('videohub', 'turbik_serials').';'), 'url');
    $db['seasons'] = readdata(query('select id,url from '.tabname('videohub', 'turbik_seasons').';'), 'url');
    $db['episodes'] = readdata(query('select id,url from '.tabname('videohub', 'turbik_episodes').';'), 'url');
    // pre($db);
    $info['hub'] = 'turbik.tv';
    $curl['ua'] = 'iphone';
    $curl['cookies'] = ['IAS_ID' => value_from_config('videohub', 'turbik_sid')];
    $page = copybetwen('<div id="series">', '<div id="footer">', filegetcontents_alt('http://'.$info['hub'].'/Series', $curl));
    $page = mb_convert_encoding($page, 'windows-1251', 'utf-8');
    $serials = explode('<span class="serieslistboxcornerr"></span>', $page);
    unset($serials[count($serials) - 1]);
    // pre($serials);
    // $serials_arr=array();
    $break = 0;
    $inserted['serials'] = 0;
    $inserted['seasons'] = 0;
    $inserted['episodes'] = 0;
    $list = '';
    $serials_arr = [];
    foreach ($serials as $serial) {
        $arr = [];
        $arr['url'] = 'https://'.$info['hub'].'/Series/'.copybetwen('<a href="/Series/', '">', $serial);
        // copytoserv($arr['url'],$config['path']['type'].'/static/serials/serial/'.url_translit(without_spaces($v['name']).'.jpg'));
        // ���� ������ �� ������ ���� � ����, �� �� �������������� ���
        if (isset($db['serials'][$arr['url']])) {
            // ���� ���� ����� skip, ���������� ������
            if ($db['serials'][$arr['url']]['skip'] == '1') {
                $list .= '���������� '.$arr['url'].'<br/>';

                continue;
            }
            $arr['insert'] = 0;
        } else {
            $arr['insert'] = 1;
            $inserted['serials']++;
        }
        $arr['title_en'] = copybetwen('<span class="serieslistboxen">', '</span>', $serial);
        $arr['title_ru'] = copybetwen('<span class="serieslistboxru">', '</span>', $serial);
        $arr['img'] = 'http:'.copybetwen('<img src="', '" width="370" height="125"', $serial);
        $arr['genre'] = copybetwen('<span class="serieslistboxperstext">����: ', '</span>', $serial);
        $arr['date'] = copybetwen('<span class="serieslistboxperstext">�������� �������: ', '</span>', $serial);
        $serial_page = copybetwen('<div class="sseriestitle">', '<div id="footer">', filegetcontents_alt($arr['url'], $curl));
        $serial_page = mb_convert_encoding($serial_page, 'windows-1251', 'utf-8');
        $arr['description'] = trim(copybetwen('<span id="desccnt">', '</span>', $serial_page));
        $arr['translation'] = trim(strip_tags(copybetwen('<span class="sseriesrighttop">�������</span>', '</span>', $serial_page)));
        $seasons_block = copybetwen('<div class="seasonnum">', '</div>', $serial_page);
        $seasons = explode('</a>', $seasons_block);
        // $seasons=array();
        unset($seasons[count($seasons) - 1]);
        $seasons_arr = [];
        foreach ($seasons as $season) {
            $arr2 = [];
            $arr2['url'] = 'https://'.$info['hub'].copybetwen('<a href="', '">', $season);
            // ���� ����� ��� ���� � ����, �� ��������� � ����������
            if (isset($db['seasons'][$arr2['url']])) {
                continue;
            } else {
                $inserted['seasons']++;
            }
            $arr2['season_number'] = copybetwen('active">����� ', '</span>', $season);
            $season_page = filegetcontents_alt($arr2['url'], $curl);
            $season_page = mb_convert_encoding($season_page, 'windows-1251', 'utf-8');
            $episodes_block = copybetwen('<div class="sserieslistbox">', '<input type="hidden" id="sid"', $season_page);
            // die($episodes_block);
            $episodes = explode('<span class="sserieslistonebotcbg"></span>', $episodes_block);
            unset($episodes[count($episodes) - 1]);
            // pre($episodes);
            $episodes_arr = [];
            foreach ($episodes as $episode) {
                // die($episode);
                $arr3 = [];
                $arr3['url'] = 'https://'.$info['hub'].copybetwen('<a href="', '">', $episode);
                if (isset($db['episodes'][$arr3['url']])) {
                    continue;
                } else {
                    $inserted['episodes']++;
                }
                $arr3['episode_number'] = copybetwen('<span class="sserieslistonetxtep">������: ', '</span>', $episode);
                $arr3['small_img'] = 'http:'.copybetwen('<img src="', '" width="', $episode);
                $episode_page = filegetcontents_alt($arr3['url'], $curl);
                // $episode_page='';
                $episode_page = mb_convert_encoding($episode_page, 'windows-1251', 'utf-8');
                $arr3['metadata'] = copybetwen('id="metadata" value="', '" />', $episode_page);
                $arr3['title_en'] = copybetwen('<span class="comwatchinputlinec">', '</span>', $episode_page);
                $arr3['title_ru'] = copybetwen('id="runame" value="', '" /></span>', $episode_page);
                $arr3['description'] = copybetwen('<span class="textdesc">', '</span>', $episode_page);
                // die($episode_page);
                $episodes_arr[] = $arr3;
            }
            $arr2['episodes'] = $episodes_arr;
            $seasons_arr[] = $arr2;
            // ���� ��������� 2 ������, �� ������� �� �����
            if ($inserted['seasons'] >= 4) {
                $break = 1;
                break;
            }
        }
        $arr['seasons'] = $seasons_arr;
        $serials_arr[] = $arr;
        if ($break == 1) {
            break;
        }
        // pre($serials_arr);
    }
    // pre($serials_arr);
    foreach ($serials_arr as $serial) {
        if ($serial['insert'] == 1) {
            query('insert into '.tabname('videohub', 'turbik_serials').' set
			title_en=\''.checkstr($serial['title_en']).'\',
			title_ru=\''.checkstr($serial['title_ru']).'\',
			img=\''.$serial['img'].'\',
			url=\''.$serial['url'].'\',
			genre=\''.$serial['genre'].'\',
			premiere_date=\''.$serial['date'].'\',
			description=\''.checkstr($serial['description']).'\',
			translation_studio=\''.checkstr($serial['translation']).'\'
			;');
            $arr_last_id = mysql_fetch_assoc(query('select last_insert_id() as id;'));
            $serial['id'] = $arr_last_id['id'];
        } else {
            // ���� ��� ������� 0 �������, ������ ��� ������ ���������, ����� ��� ����������
            $serial['id'] = $db['serials'][$serial['url']]['id'];
            if (empty($serial['seasons'])) {
                query('update '.tabname('videohub', 'turbik_serials').' set skip=\'1\' where id=\''.$serial['id'].'\';');
            }
        }
        foreach ($serial['seasons'] as $season) {
            query('insert into '.tabname('videohub', 'turbik_seasons').' set
				serial_id=\''.$serial['id'].'\',
				season_number=\''.$season['season_number'].'\',
				url=\''.$season['url'].'\'
				;');
            $arr_last_id = mysql_fetch_assoc(query('select last_insert_id() as id;'));
            $season['id'] = $arr_last_id['id'];
            foreach ($season['episodes'] as $episode) {
                query('insert into '.tabname('videohub', 'turbik_episodes').' set
					season_id=\''.$season['id'].'\',
					title_en=\''.checkstr($episode['title_en']).'\',
					title_ru=\''.checkstr($episode['title_ru']).'\',
					episode_number=\''.$episode['episode_number'].'\',
					url=\''.$episode['url'].'\',
					small_img=\''.$episode['small_img'].'\',
					metadata=\''.$episode['metadata'].'\',
					description=\''.checkstr($episode['description']).'\'
					;');
                $list .= '<li>������ "'.$serial['title_ru'].'", ����� '.$season['season_number'].', ������ '.$episode['episode_number'].'.</li>';
            }
        }
    }

    return '<ol>'.$list.'</ol>'.pre_return($inserted).js_reload_page();
    // return pre_return($serials_arr).js_reload_page();
}

/*
function turbik_episode_qualities() {
    if (empty($_GET['id'])) {error('�� ������� ID');};
    $args['id']=$_GET['id'];
    $arr=cache('preparing_episode_info',$args);
    $list='';
    if ($arr['hq']=='0') {
        $return='SQ';
    } else {
        if (isset($_GET['quality']))  {
            $quality=$_GET['quality'];
        } else {
            if (isset($_COOKIE['quality'])) {
                $quality=$_COOKIE['quality'];
            } else {
                $quality='hq';
            };
        };
        if ($quality=='default') {$quality='hq';};
        $class=array('hq'=>'','sq'=>'');
        if (isset($class[$quality])) {$class[$quality]=' class="active"';} else {error('����� �������� �� ����������');};
        $return='
        <a data-role="quality" data-value="sq" data-episodeid="'.$args['id'].'"'.$class['sq'].'>SQ</a>
        <a data-role="quality" data-value="hq" data-episodeid="'.$args['id'].'"'.$class['hq'].'>HQ</a>
        ';
    };
    return $return;
}

function turbik_iframe_check_quality() {
    if (isset($_GET['quality']))  {
        if (empty($_GET['id'])) {error('�� ������� ID');};
        $quality=$_GET['quality'];
        $arr=preparing_episode_info(array('id'=>$_GET['id']));
        if ($arr['hq']==0) {
            if ($quality=='hq') {header('Location: /?page=iframe&id='.$_GET['id'].'&quality=default');};
        };
    };
}

*/
