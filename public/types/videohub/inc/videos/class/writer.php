<?php

namespace MegawebV1;

// оставляем эту функцию т.к. она много где используется в коде
function video_embed_player()
{
    $id = empty($_GET['id']) ? error('ID required') : checkstr($_GET['id']);
    $video = new videoWriter($id);
    $video->checkDomain();

    return $video->getPlayer();
}

class videohubTemplates extends engineTemplates
{
    public function video_embed_player()
    {
        $id = empty($_GET['id']) ? error('ID required') : checkstr($_GET['id']);
        $video = new videoWriter($id);
        $video->checkDomain();

        return $video->getPlayer();
    }

    public function redirect_to_direct_video()
    {
        $id = empty($_GET['id']) ? error('ID required') : checkstr($_GET['id']);
        $video = new videoCached($id);
        $video->checkDomain();
        // pre($video);
        // pre($video->getDirectLinkSame());
        header('Location: '.$video->getDirectLinkSame());
        exit();
    }
}
