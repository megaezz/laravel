<?php

namespace MegawebV1;

class videos
{
    public function __construct()
    {
        $this->withTitle = false;
        $this->withDescription = false;
        $this->bookedDomain = null;
        $this->page = null;
        $this->limit = null;
        $this->domain = null;
        $this->order = null;
    }

    public function getVideos()
    {
        if (! isset($this->page)) {
            error('Page number is required');
        }
        if (! isset($this->limit)) {
            error('Limit is required');
        }
        $sql['WithTitle'] = is_null($this->withTitle) ? '1' : (($this->withTitle) ? '(title_ru is not null and title_ru!=\'\')' : '(title_ru is null or title_ru=\'\')');
        $sql['WithDescription'] = is_null($this->withDescription) ? '1' : (($this->withDescription) ? '(description is not null and description!=\'\')' : '(description is null or description=\'\')');
        $sql['Domain'] = is_null($this->domain) ? 'domain is null' : (($this->domain) ? ('domain=\''.$this->domain.'\'') : '1');
        // pre(var_dump($this));
        $sql['BookedDomain'] = is_null($this->bookedDomain) ? '1' : 'booked_domain=\''.$this->bookedDomain.'\'';
        $sql['Order'] = ($this->order) ? ('order by '.$this->order) : '';
        $sql['From'] = ($this->page - 1) * $this->limit;
        $arr = query_arr('select SQL_CALC_FOUND_ROWS videos.id
			from '.typetab('videos').'
			where '.$sql['WithTitle'].' and '.$sql['WithDescription'].'
			and '.$sql['Domain'].'
			and '.$sql['BookedDomain'].'
			'.$sql['Order'].'
			limit '.$sql['From'].','.$this->limit.'
			;');
        $object = new stdClass;
        $object->rows = rows_without_limit();
        $object->array = $arr;

        return $object;

    }

    public function setWithTitle($b)
    {
        $this->withTitle = $b;
    }

    public function setWithDescription($b)
    {
        $this->withDescription = $b;
    }

    public function setBookedDomain($domain)
    {
        $this->bookedDomain = $domain;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function getPages($args = [])
    {
        $link = empty($args['link']) ? error('Link is required') : checkstr($args['link']);
        $videos = $this->getVideos();
        $rows = $videos->rows;

        return page_numbers($videos->rows, $this->limit, $this->page, $link);
    }
}

class video
{
    public function __construct($id = null)
    {
        $this->id = empty($id) ? error('ID required to create video class') : $id;
        $this->thumb = '/types/videohub/images/'.$this->id.'.jpg';
        $this->getInfo = false;
        $this->getInfo();
    }

    public function getInfo()
    {
        $this->getInfo = true;
        $arr = query_assoc('select id,domain,hub,id_hub,add_date,apply_date,title_en,title_ru,title_ru_alt,
			title_ru_translit,translater,translate_date,description,description_translater,description_translate_date,
			ph_viewkey,ph_date,booked_domain,deleted,source_url,local_id,deleted_from_hub
		 from '.typetab('videos').' where id=\''.$this->id.'\';');
        if (! $arr) {
            error('Video doesn\'t exist');
        }
        $this->id = $arr['id'];
        $this->domain = $arr['domain'];
        $this->hub = $arr['hub'];
        $this->id_hub = $arr['id_hub'];
        $this->add_date = $arr['add_date'];
        $this->apply_date = $arr['apply_date'];
        $this->title_en = $arr['title_en'];
        $this->title_ru = $arr['title_ru'];
        $this->title = $arr['title_ru'];
        $this->title_alt = $arr['title_ru_alt'];
        $this->title_translit = $arr['title_ru_translit'];
        $this->title_writer = $arr['translater'];
        $this->title_date = $arr['translate_date'];
        $this->description = $arr['description'];
        $this->description_writer = $arr['description_translater'];
        $this->description_date = $arr['description_translate_date'];
        $this->ph_viewkey = $arr['ph_viewkey'];
        $this->ph_date = $arr['ph_date'];
        $this->booked_domain = $arr['booked_domain'];
        $this->is_deleted = $arr['deleted'];
        $this->source_url = $arr['source_url'];
        $this->local_id = $arr['local_id'];
        $this->deleted_from_hub = $arr['deleted_from_hub'];
    }

    public function getCategories()
    {
        return preparing_categories_of_video(['video_id' => $this->id, 'stars' => 0]);
    }

    public function getStars()
    {
        return preparing_categories_of_video(['video_id' => $this->id, 'stars' => 1]);
    }

    public function getTags()
    {
        return preparing_video_tags_list_array(['id' => $this->id]);
    }

    public function getSame($args = [])
    {
        if (! $this->getInfo) {
            $this->getInfo();
            // cacheObject($this,'getInfo',null,0);
        }
        $sort = empty($args['sort']) ? 'mr' : $args['sort'];
        $limit = empty($args['limit']) ? 20 : $args['limit'];
        $sort_sql = [
            'mv' => 'views desc',
            'mr' => 'videos.apply_date desc',
            'tr' => 'rating desc, votes desc',
            'ms' => 'same_tags_q desc, rating_sko desc',
            'mc' => 'q_comments desc',
        ];
        $tags = query_arr('select tags_translation.tag_ru
			from '.typetab('tags').'
			join '.typetab('tags_translation').' ON tags.tag_en = tags_translation.tag_en
			where tags.video_id = \''.$this->id.'\'
			');
        // ��������� ������ �����
        $video_tags = [];
        foreach ($tags as $v) {
            $video_tags[] = $v['tag_ru'];
        }
        $arr = query_arr('select SQL_CALC_FOUND_ROWS videos.id,
			ifnull(videos_views.views,0) as views,
			count(tags.video_id) as same_tags_q, ifnull(rating,0) as rating, ifnull(votes,0) as votes,
			((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko,
			(select count(*) from '.typetab('comments').' where video_id=videos.id) as q_comments
			from '.typetab('tags').'
			join '.typetab('videos').' on videos.id=tags.video_id
			left join '.typetab('videos_views').' on tags.video_id=videos_views.video_id
			where domain=\''.$this->domain.'\' and tags.tag_ru in ('.array_to_str($video_tags).') and tags.video_id!=\''.$this->id.'\'
			and deleted_from_hub=\'no\'
			group by id
			order by '.$sort_sql[$sort].'
			limit '.$limit.'
			');
        $object = new stdClass;
        $object->rows = rows_without_limit();
        $object->array = $arr;

        return $object;
    }

    public function setVar($field, $value)
    {
        $sql_value = is_null($value) ? 'null' : '\''.$value.'\'';
        query('update '.typetab('videos').' set '.$field.'='.$sql_value.' where id=\''.$this->id.'\';');

        return true;
    }

    public function addTag($tag_en)
    {
        query('insert '.typetab('tags').' set tag_en=\''.$tag_en.'\', video_id=\''.$this->id.'\';');

        return true;
    }

    public function setThumb($url)
    {
        global $config;
        copytoserv($url, $config['videohub']['path']['images'].$this->id.'.jpg');
    }

    public static function addVideosFromGrabber($grabber)
    {
        // pre($args);
        $hub = empty($grabber->hub) ? error('Hub is required') : $grabber->hub;
        $source_url = empty($grabber->url) ? error('Source url is required') : $grabber->url;
        // pre($grabber->getVideos());
        foreach ($grabber->getVideos() as $args) {
            $tags = empty($args['tags']) ? error('Tags are required') : $args['tags'];
            $img_src = empty($args['img_src']) ? error('IMG src is required') : $args['img_src'];
            $id_hub = empty($args['id_hub']) ? error('Hub ID is required') : $args['id_hub'];
            $title_en = empty($args['title_en']) ? error('Title EN is required') : $args['title_en'];
            $booked_domain = empty($args['booked_domain']) ? null : $args['booked_domain'];
            $viewkey = empty($args['viewkey']) ? null : $args['viewkey'];
            $ph_date = empty($args['ph_date']) ? null : $args['ph_date'];
            query('insert into '.typetab('videos').' () values ()');
            $video_id = mysql_insert_id();
            $video = new video(['id' => $video_id]);
            $video->setVar('hub', $hub);
            $video->setVar('id_hub', $id_hub);
            $video->setVar('title_en', checkstr($title_en));
            $video->setVar('source_url', $source_url);
            $video->setVar('booked_domain', $booked_domain);
            $video->setVar('ph_viewkey', $viewkey);
            $mysql_tags_values = '';
            foreach ($tags as $tag) {
                $video->addTag(checkstr($tag));
            }
            $video->setThumb($img_src);
        }
    }

    public function getDirectLink()
    {
        $link = false;
        if ($this->hub == 'pornhub.com') {
            $link = pornhub::getDirectLink(['ph_viewkey' => $this->ph_viewkey]);
        }
        if ($this->hub == 'xvideos.com') {
            $link = xvideos::getDirectLink(['id_hub' => $this->id_hub]);
        }

        return $link;
    }

    public function getDirectLinkSame()
    {
        $link = $this->getDirectLink();
        if ($link) {
            return $link;
        }
        $same_arr = $this->getSame(['sort' => 'ms', 'limit' => 10])->array;
        // pre($same_arr);
        foreach ($same_arr as $v) {
            $video = new videoCached($v['id']);
            $link = $video->getDirectLink();
            if ($link) {
                return $link;
            }
        }

        return $link;
    }

    public function checkDomain()
    {
        global $config;
        if ($this->domain != $config['domain']) {
            error('Wrong domain');
        }
    }

    public static function getObjectFromDomainAndTitleTranslit($domain = null, $titleTranslit = null)
    {
        if (empty($domain)) {
            error('Specify domain');
        }
        if (empty($titleTranslit)) {
            error('Specify titleTranslit');
        }
        $arr = query_assoc('select id from '.typetab('videos').' 
			where domain=\''.$domain.'\' and title_ru_translit=\''.$titleTranslit.'\'
			;');
        if (! $arr) {
            error('Video was not found');
        }

        return new video($arr['id']);
    }
}

class videoWriter extends video
{
    public function getEmbedPlayer()
    {
        temp_var('id_hub', $this->id_hub);
        temp_var('ph_viewkey', $this->ph_viewkey);

        return template('player/'.$this->hub.'/iframe');
    }

    public function getInternalPlayer()
    {
        temp_var('video_id', $this->id);
        temp_var('thumb', $this->thumb);

        return template('player/kt_player/code');
    }

    public function getPlayer()
    {
        if ($this->hub == 'pornhub.com') {
            return $this->getEmbedPlayer();
        }
        if ($this->hub == 'xvideos.com') {
            return $this->getInternalPlayer();
        }
    }
}

class videoCached extends video
{
    public function getDirectLink()
    {
        $video = new video($this->id);

        return cacheObject($video, 'getDirectLink', null, 1200);
        // return objcache(array('class'=>'video','class_args'=>array('id'=>$this->id),'method'=>'getDirectLink','seconds'=>1200));
    }

    public function getDirectLinkSame()
    {
        $video = new video($this->id);

        return cacheObject($video, 'getDirectLinkSame', null, 1200);
        // $cache=new cache($this);
        // return $cache->method('getDirectLinkSame');
        // return objcache(array('class'=>'video','class_args'=>array('id'=>$this->id),'method'=>'getDirectLinkSame','seconds'=>1200));
    }

    public function getSame($args = [])
    {
        $video = new video($this->id);

        return cacheObject($video, 'getSame', $args);
        // return objcache(array('class'=>'video','class_args'=>array('id'=>$this->id),'method'=>'getSame','method_args'=>$args));
    }

    public function getCategories()
    {
        $video = new video($this->id);

        return cacheObject($video, 'getCategories');
        // return objcache(array('class'=>'video','class_args'=>array('id'=>$this->id),'method'=>'getCategories'));
    }

    public function getStars()
    {
        $video = new video($this->id);

        return cacheObject($video, 'getStars');
        // return objcache(array('class'=>'video','class_args'=>array('id'=>$this->id),'method'=>'getStars'));
    }

    public function getTags()
    {
        $video = new video($this->id);

        return cacheObject($video, 'getTags');
        // return objcache(array('class'=>'video','class_args'=>array('id'=>$this->id),'method'=>'getTags'));
    }
}
