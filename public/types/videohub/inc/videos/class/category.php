<?php

namespace MegawebV1;

class category
{
    public function __construct($args = [])
    {
        $id = empty($args['id']) ? error('ID required to create category class') : $args['id'];
        $domain = empty($args['domain']) ? error('Domain is required to create category class') : $args['domain'];
        $arr = query_assoc('select id,domain,category_id,text,text_2,title,h1,img,keywords,description,
			photo_title,photo_description,photo_h1,photo_text,photo_img,title_translit,active,h2
			from '.typetab('categories_domains').' where id=\''.$id.'\' and domain=\''.$domain.'\';');
        if (! $arr) {
            error('Category doesn\'t exist');
        }
        $this->id = $arr['id'];
        $this->domain = $arr['domain'];
        $this->category_id = $arr['category_id'];
        $this->text = $arr['text'];
        $this->text_2 = $arr['text_2'];
        $this->title = $arr['title'];
        $this->h1 = $arr['h1'];
        $this->img = $arr['img'];
        $this->keywords = $arr['keywords'];
        $this->description = $arr['description'];
        $this->photo_title = $arr['photo_title'];
        $this->photo_description = $arr['photo_description'];
        $this->photo_h1 = $arr['photo_h1'];
        $this->photo_text = $arr['photo_text'];
        $this->photo_img = $arr['photo_img'];
        $this->title_translit = $arr['title_translit'];
        $this->active = $arr['active'];
        $this->h2 = $arr['h2'];
    }

    public function getVideos($args = [])
    {
        $page = empty($args['page']) ? error('Page number is required') : $args['page'];
        $sort = empty($args['sort']) ? error('Sort is required') : $args['sort'];
        $limit = empty($args['limit']) ? error('Limit is required') : $args['limit'];
        $from = ($page - 1) * $limit;
        $sort_sql = [
            'mv' => 'views desc',
            'mr' => 'apply_date desc',
            'tr' => 'rating desc, votes desc',
            'ms' => 'same_tags_q desc, rating_sko desc',
            'mc' => 'q_comments desc',
        ];
        if (! isset($sort_sql[$sort])) {
            error('���������� �� ����������');
        }
        $arr = query_arr('
			select catgroups_tags.tag_ru, categories_groups.name_translit
			FROM '.typetab('categories_domains').'
			JOIN  '.typetab('categories_groups').' ON categories_groups.id = categories_domains.category_id
			JOIN  '.typetab('catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
			WHERE categories_domains.id=\''.$this->id.'\' and categories_domains.domain=\''.$this->domain.'\' and categories_domains.active=\'1\'
			');
        // ��������� ������ ������ �����
        $cat_tags = [];
        foreach ($arr as $v) {
            $cat_tags[] = $v['tag_ru'];
        }
        if (empty($cat_tags)) {
            error('��������� ����������', '��� ��������� ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
        }
        // ������ �� ��������� ���� ���������� ����� �����, ��������������� �������
        $arr = query_arr('
			select SQL_CALC_FOUND_ROWS videos.id
			FROM '.typetab('tags').'
			inner join '.typetab('videos').' on videos.id=tags.video_id
			left join '.typetab('videos_views').' on tags.video_id=videos_views.video_id
			WHERE domain=\''.$this->domain.'\' and tags.tag_ru in ('.array_to_str($cat_tags).')
			group by videos.id
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$limit
        );
        $object = new stdClass;
        $object->rows = rows_without_limit();
        $object->array = $arr;

        return $object;
    }
}
