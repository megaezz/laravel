<?php

namespace MegawebV1;

use engine\app\models\SapeRu;

function saperu_getlinks($q = null)
{
    // $q = array();
    // $q['force_show_code'] = true;
    return SapeRu::getLinks($q);
}

function video_title_ru_translit()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['title_ru_translit'];
}

function video_date_slashed()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['date_slashed'];
}

function video_title_alt()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['title_ru_alt'];
}

function star_hair_en()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['hair_en'];
}

function star_ethnicity_en()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['ethnicity_en'];
}

function star_gender_en()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['gender_en'];
}

function star_name_ru_url()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['name_ru_url'];
}

function star_name_url()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['name_url'];
}

function star_name()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['name'];
}

function star_name_ru()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['name_ru'];
}

function star_aka_en()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);
    if (! empty($arr['aka'])) {
        return ' aka '.$arr['aka'];
    }
}

function star_aka()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);
    if (! empty($arr['aka'])) {
        return ' ��� '.$arr['aka'];
    }
}

function star_pic()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['pic'];
}

function star_age()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['age'];
}

function star_country()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['country'];
}

function star_region()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['region'];
}

function star_city()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['city'];
}

function star_country_en()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['country_en'];
}

function star_region_en()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['region_en'];
}

function star_city_en()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['city_en'];
}

function star_height()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['height'];
}

function star_weight()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['weight'];
}

function star_gender()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['gender'];
}

function star_ethnicity()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['ethnicity'];
}

function star_hair()
{
    if (empty($_GET['category_id'])) {
        error('�� ������� ID');
    }
    $arr = cache('preparing_star_info', ['category_id' => $_GET['category_id']]);

    return $arr['hair'];
}

function max_pages_videos_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 1;
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);

    return $pages;
}

function max_pages_videos_list_category()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 1;
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);

    return $pages;
}

function max_pages_videos_list_tag()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'tag';
    $args['value'] = $_GET['tag_ru'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 1;
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);

    return $pages;
}

function video_likes_percent()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);
    $sum = $arr['likes'] + $arr['dislikes'];
    if ($sum == 0) {
        return 0;
    }
    $percent = ceil(($arr['likes'] / ($sum)) * 100);

    return $percent;
}

function video_dislikes_percent()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);
    $sum = $arr['likes'] + $arr['dislikes'];
    if ($sum == 0) {
        return 0;
    }
    $percent = ceil(($arr['dislikes'] / ($sum)) * 100);

    return $percent;
}

function video_likes_sum()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['likes_sum'];
}

function video_likes()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['likes'];
}

function video_dislikes()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['dislikes'];
}

function video_added_date()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['rus_date'];
}

function tag_rows()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'tag';
    $args['value'] = $_GET['tag_ru'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);

    return $arr['rows'];
}

function category_rows()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);

    return $arr['rows'];
}

function video_embed_player_mobile()
{
    global $config;
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);
    if ($arr['hub'] == 'pornhub.com') {
        return media_from_pornhub($arr['id_hub'], 'embed_player');
    }
    if ($arr['hub'] == 'hardsextube.com') {
        return media_from_hardsextube($arr['id_hub'], 'player');
    }
    if ($arr['hub'] == 'xvideos.com') {
        return media_from_xvideos($arr['id_hub'], 'player');
    }
    if ($arr['hub'] == 'paradisehill.tv') {
        return media_from_paradisehill($arr['id_hub'], 'player_mobile');
    }
    error('��� �����������');
}

function video_embed_player_old()
{
    global $config;
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);
    if ($arr['deleted'] == 'yes') {
        return '����� ���� ������� �� �������';
    }
    if ($arr['hub'] == 'pornhub.com') {
        // return media_from_pornhub($arr['id_hub'],'player',$arr['ph_viewkey']);
        return media_from_pornhub($arr['id_hub'], 'embed_player', $arr['ph_viewkey']);
    }
    if ($arr['hub'] == 'hardsextube.com') {
        return media_from_hardsextube($arr['id_hub'], 'player');
    }
    if ($arr['hub'] == 'xvideos.com') {
        return media_from_xvideos($arr['id_hub'], 'player');
        // return media_from_xvideos($arr['id_hub'],'embed_player');
    }
    if ($arr['hub'] == 'paradisehill.tv') {
        return media_from_paradisehill($arr['id_hub'], 'player');
    }
    error('��� �����������');
}

function category_name_url()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['name_url'];
}

function category_title_translit()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['title_translit'];
}

function category_img()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['img'];
}

function ajax_get_sort()
{
    if (empty($_GET['sort'])) {
        return 'mr';
    }

    return htmlspecialchars($_GET['sort']);
}

function sort_disabled($sort)
{
    if (empty($_GET['sort'])) {
        // error('�� �������� ����������');
        $_GET['sort'] = 'mr';
    }
    if (isset($_GET['sort'])) {
        if ($_GET['sort'] == $sort) {
            return ' disabled';
        }
    }

    return '';
}

function last_update_text()
{
    global $config;
    $data = query('
		select (select unix_timestamp(max(apply_date)) from '.tabname('videohub', 'videos').' where domain=\''.$config['domain'].'\') as max_unixtime, count(*) as q
		from '.tabname('videohub', 'videos').'
		where domain=\''.$config['domain'].'\' and date(apply_date)=(select max(date(apply_date)) from '.tabname('videohub', 'videos').' where domain=\''.$config['domain'].'\');
		');
    $arr = mysql_fetch_assoc($data);
    $date = my_date($arr['max_unixtime']);
    $content = $date['day'].' '.$date['month'].' '.$date['year'].' � '.$date['clock'].' (��������� '.$arr['q'].' �����)';

    return $content;
}

function get_id_hub()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['id_hub'];
}

function ph_viewkey()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    // $data=query('select ph_viewkey from '.tabname('videohub','videos').' where id=\''.$_GET['id'].'\';');
    // $arr=mysql_fetch_assoc($data);
    $args['video_id'] = $_GET['id'];
    $args['check_domain'] = 'no'; // ����� � ������� �� �������� �����
    $arr = cache('preparing_view_video', $args);
    if (empty($arr['ph_viewkey'])) {
        return false;
    }

    return $arr['ph_viewkey'];
}

// �������� ������ ����� �������
function video_title_keywords()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $content = str_replace(' ', ', ', strtolower(video_title()));

    return $content;
}

function sort_name_en()
{
    if ($_GET['sort'] == 'mv') {
        $content = 'Popular';
    }
    if ($_GET['sort'] == 'mr') {
        $content = 'New';
    }
    if ($_GET['sort'] == 'tr') {
        $content = 'Top rated';
    }
    if ($_GET['sort'] == 'mc') {
        $content = 'Discussed';
    }
    if (empty($content)) {
        error('���������� �� ���������');
    }

    return $content;
}

function sort_name()
{
    if ($_GET['sort'] == 'mv') {
        $content = '����������';
    }
    if ($_GET['sort'] == 'mr') {
        $content = '�����';
    }
    if ($_GET['sort'] == 'tr') {
        $content = '��� ��������';
    }
    if ($_GET['sort'] == 'mc') {
        $content = '�����������';
    }
    if (empty($content)) {
        error('���������� �� ���������');
    }

    return $content;
}

function categories_videos_description()
{
    return trim(strip_tags(category_text()));
}

function categories_videos_keywords()
{
    global $config;
    if (empty($_GET['category_id'])) {
        error('�� ������ ���������');
    }
    // ������ �� ��������� ����� ����������� � ������ ���������
    $data = query('
		select tag_ru from '.tabname('videohub', 'catgroups_tags').' where group_id=(
			select group_id from '.tabname('videohub', 'categories_groups').' where id=(
				select category_id from '.tabname('videohub', 'categories_domains').' where id=\''.$_GET['category_id'].'\' and domain=\''.$config['domain'].'\')
	);
	');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        $category_tags_arr[] = $v['tag_ru'];
    }
    $category_name_arr = explode(' ', strtolower(category_name()));
    $merge = array_merge_recursive($category_name_arr, $category_tags_arr);
    $result = array_unique($merge);
    $list = '';
    $i = 0;
    foreach ($arr as $v) {
        if ($i != 0) {
            $list .= ', ';
        }	$i = 1;
        $list .= $v['tag_ru'];
    }

    // die(implode(',',$result));
    return $list;
}

function video_watch_link_source()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }

    return media_for_video($_GET['id'], 'video_watch_link_source');
}

// ���������� id_hub �� id. ����� �� �����������, �.�. ����� � ������� �� �������� �����
function id_hub_from_get_id()
{
    global $config;
    if (empty($_GET['id'])) {
        error('�� ����� ID �����');
    }
    $data = query('select id_hub from '.tabname('videohub', 'videos').' where id=\''.$_GET['id'].'\' ');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id_hub'])) {
        error('�������� ����������');
    }

    return $arr['id_hub'];
}

function get_category_name_translit()
{
    if (empty($_GET['name_translit'])) {
        error('�� ������� �������� ���������');
    }

    return $_GET['name_translit'];
}

function video_id()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }

    return $_GET['id'];
}

// ������� �����
function video_rating()
{
    $data = query('select rating from '.tabname('videohub', 'videos_views').' where video_id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['rating'])) {
        $arr['rating'] = 0;
    }

    return $arr['rating'];
}

// ������� �� �����
function video_votes()
{
    $data = query('select votes from '.tabname('videohub', 'videos_views').' where video_id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['votes'])) {
        $arr['votes'] = 0;
    }

    return $arr['votes'];
}

// ���������� ������������
function video_comments()
{
    $data = query('select count(*) as q from '.tabname('videohub', 'comments').' where video_id=\''.$_GET['id'].'\' and active=\'1\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['q'])) {
        $arr['q'] = 0;
    }

    return $arr['q'];
}

// ���������� ���������� �����
function video_views()
{
    $data = query('select views from '.tabname('videohub', 'videos_views').' where video_id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_assoc($data);

    return $arr['views'];
}

// ���������� �������� �����
function video_loads()
{
    $data = query('select loads from '.tabname('videohub', 'videos_views').' where video_id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_assoc($data);

    return $arr['loads'];
}

function get_query()
{
    // pre($_GET);
    if (! isset($_REQUEST['search_query'])) {
        error('�� ����� ��������� ������');
    }

    return htmlspecialchars(checkstr($_REQUEST['search_query']), ENT_QUOTES, 'cp1251');
}

function get_query_urlencode()
{
    if (! isset($_REQUEST['search_query'])) {
        error('�� ����� ��������� ������');
    }

    return urlencode(checkstr(trim($_REQUEST['search_query'])));
}

function post_text()
{
    if (empty($_POST['text'])) {
        error('�� ������� �����');
    }

    return nl2br($_POST['text']);
}

function login_videohub()
{
    // return login('engine');
    return auth_login(['type' => 'videohub']);
}

// ����� ������ �� ����������� � �����
function video_image_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return media_for_video($_GET['id'], 'video_image_link');
}
function url_encode_video_image_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return urlencode(video_image_link());
}

// ����� ������ �� �������� �����
function video_watch_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return media_for_video($_GET['id'], 'video_watch_link');
}

// ����� ������ �� �������� �������� �����
function video_load_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return media_for_video($_GET['id'], 'video_load_link');
}

// ����� ������ �� �������� ����� (������)
function video_save_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return media_for_video($_GET['id'], 'video_save_link');
}
function url_encode_video_save_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return urlencode(video_save_link());
}

function url_encode_video_direct_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return urlencode(video_direct_link());
}

function category_title()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['title'];
}

function category_h1()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['h1'];
}

function category_h2()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['h2'];
}

function category_id()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['id'];
}

function category_name()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['name'];
}

function category_name_translit()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['name_translit'];
}

function category_text()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['text'];
}

function category_text_2()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['text_2'];
}

function category_description()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['description'];
}

function category_keywords()
{
    $arr = cache('preparing_category_info', ['category_id' => $_GET['category_id']]);

    return $arr['keywords'];
}

// �������� ��������� ����� �������
function category_name_keywords()
{
    return str_replace(' ', ', ', category_name());
}

/*
//title ���������
function category_title() {
    global $config;
    $data=query('select title from '.tabname('videohub','categories_domains').' where domain=\''.$config['domain'].'\' and id=\''.$_GET['category_id'].'\'');
    $arr=mysql_fetch_assoc($data);
    return $arr['title'];
}


//h1 ���������
function category_h1() {
    global $config;
    $data=query('select h1 from '.tabname('videohub','categories_domains').' where domain=\''.$config['domain'].'\' and id=\''.$_GET['category_id'].'\'');
    $arr=mysql_fetch_assoc($data);
    return $arr['h1'];
}

//ID ���������. ����������, �������� ��� ����� ���������� �� �������, �������� �����������
function category_id() {
    return $_GET['category_id'];
}

//�������� ���������
function category_name() {
    $data=query('select category_id, name from '.tabname('videohub','categories_domains').' inner join '.tabname('videohub','categories_groups').' on categories_domains.category_id=categories_groups.id where categories_domains.id='.$_GET['category_id'].' ');
    $arr=mysql_fetch_assoc($data);
    return $arr['name'];
}

function category_name_translit() {
    global $config;
    if (empty($_GET['category_id'])) {error('�� ������� ID ���������');};
    $data=query('
        SELECT categories_groups.name_translit
        FROM '.tabname('videohub','categories_domains').'
        JOIN  '.tabname('videohub','categories_groups').' ON categories_groups.id = categories_domains.category_id
        WHERE categories_domains.id=\''.$_GET['category_id'].'\' and categories_domains.domain=\''.$config['domain'].'\'
        ');
    $arr=mysql_fetch_assoc($data);
    if (empty($arr['name_translit'])) {error('�������� �� ����������','�� ������ �������� � ���������');};
    return $arr['name_translit'];
}


//����� �������� ���������
function category_text() {
    global $config;
    $data=query('select text from '.tabname('videohub','categories_domains').' where domain=\''.$config['domain'].'\' and id=\''.$_GET['category_id'].'\'');
    $arr=mysql_fetch_assoc($data);
    return $arr['text'];
}

//����� ���� �������� ���������
function category_description() {
    global $config;
    $data=query('select description from '.tabname('videohub','categories_domains').' where domain=\''.$config['domain'].'\' and id=\''.$_GET['category_id'].'\'');
    $arr=mysql_fetch_assoc($data);
    return $arr['description'];
}

//����� ���� �������� ���� ���������
function category_keywords() {
    global $config;
    $data=query('select keywords from '.tabname('videohub','categories_domains').' where domain=\''.$config['domain'].'\' and id=\''.$_GET['category_id'].'\'');
    $arr=mysql_fetch_assoc($data);
    return $arr['keywords'];
}

*/

// ������ ��������� �� id ������
function categories_of_group()
{
    $data = query('select name from '.tabname('videohub', 'categories_groups').' where group_id='.$_GET['group_id'].'');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li>'.$v['name'].'</li>';
    }

    return $list;
}

// �������� ����
function tag_name()
{
    return $_GET['tag_ru'];
}

// �������� ����
function tag_name_en()
{
    return $_GET['tag_en'];
}

function tag_ru_urlencode()
{
    return urlencode($_GET['tag_ru']);
}

function tag_ru_url()
{
    return without_spaces($_GET['tag_ru']);
}

// ���������� $_GET['group_id']
function get_group_id()
{
    if (isset($_GET['group_id']) == 1) {
        return $_GET['group_id'];
    }
}

// ���������� $_GET['p']
function page_number()
{
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }

    return $_GET['p'];
}

// ���� ������ ����� �������
function video_tags_keywords()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $tags = video_tags_list_simple($_GET['id']);
    $content = '';
    // $content=str_replace(' ',', ',video_title());
    if (! empty($tags)) {
        $content .= $tags;
    }

    return $content;
}

// ��� �����
function video_hub()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['hub'];
}

// ��������� ������
function video_title()
{
    // if (isset($_GET['id'])==0) {error('�� ����� ID �����');};
    // $data=query('select title_ru,title_ru_translit from '.tabname('videohub','videos').' where id='.$_GET['id']);
    // $arr=mysql_fetch_assoc($data);
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['title_ru'];
}

function video_local_id()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    return $arr['local_id'];
}

// ���������� $_GET['title_ru_translit']
function get_title_ru_translit()
{
    if (isset($_GET['title_ru_translit']) == 0) {
        error('�� ������ ����������� ���������', '�� ����� title_translit');
    }

    return $_GET['title_ru_translit'];
}

// �������� ������
function video_description()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);

    // $data=query('select description from '.tabname('videohub','videos').' where id='.$_GET['id']);
    // $arr=mysql_fetch_assoc($data);
    return $arr['description'];
}

// �������� ������ �����������
function video_description_short()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);
    $short = substr($v['description'], 0, 200);
    $short .= (strlen($v['description']) > strlen($short)) ? '...' : '';

    return $short;
}

// ���������� title ��������
function title()
{
    global $config;
    // pre($config);
    if (isset($config['headers']) == 0) {
        if (! isset($config['vars']['page'])) {
            error('�������� �� ����������');
        }
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['title']) == 1) {
        return $config['headers']['title'];
    }
}

// ���������� description ��������
function description()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['description']) == 1) {
        return $config['headers']['description'];
    }
}

// ���������� keywords ��������
function keywords()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['keywords']) == 1) {
        return $config['headers']['keywords'];
    }
}

// ���������� h1 ��������
function h1()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['h1']) == 1) {
        return $config['headers']['h1'];
    }
}

// ���������� h2 ��������
function h2()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['h2']) == 1) {
        return $config['headers']['h2'];
    }
}

// ���������� ������� ������ ��������
function bread()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['bread']) == 1) {
        return $config['headers']['bread'];
    }
}

// ���������� text_top ��������
function text_top()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['text_top']) == 1) {
        return $config['headers']['text_top'];
    }
}

// ���������� text_bottom ��������
function text_bottom()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['text_bottom']) == 1) {
        return $config['headers']['text_bottom'];
    }
}

function text_1()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['text_1']) == 1) {
        return $config['headers']['text_1'];
    }
}

function text_2()
{
    global $config;
    if (isset($config['headers']) == 0) {
        $config['headers'] = return_headers($config['domain'], $config['vars']['page']);
    }
    if (isset($config['headers']['text_2']) == 1) {
        return $config['headers']['text_2'];
    }
}
