<?php

namespace MegawebV1;

function admin_star_edit_form()
{
    $id = empty($_GET['id']) ? error('�� ������� ID') : (int) $_GET['id'];
    $arr = query_assoc('select * from '.tabname('videohub', 'stars').' where id=\''.$id.'\';');
    // pre($arr);
    $select['gender'] = [];
    $select['gender'][] = ['en' => 'man', 'ru' => '�������'];
    $select['gender'][] = ['en' => 'woman', 'ru' => '�������'];
    $select['ethnicity'] = [];
    $select['ethnicity'][] = ['en' => 'black', 'ru' => '����������'];
    $select['ethnicity'][] = ['en' => 'latina', 'ru' => '���������'];
    $select['ethnicity'][] = ['en' => 'mixed', 'ru' => '���������'];
    $select['ethnicity'][] = ['en' => 'white', 'ru' => '���������'];
    $select['ethnicity'][] = ['en' => 'asian', 'ru' => '���������'];
    $select['ethnicity'][] = ['en' => 'indian', 'ru' => '���������'];
    $select['ethnicity'][] = ['en' => 'arab', 'ru' => '��������'];
    $select['hair'] = [];
    $select['hair'][] = ['en' => 'blonde', 'ru' => '�������'];
    $select['hair'][] = ['en' => 'black', 'ru' => '������'];
    $select['hair'][] = ['en' => 'brown', 'ru' => '����������'];
    $select['hair'][] = ['en' => 'red', 'ru' => '�����'];
    $select['hair'][] = ['en' => 'white', 'ru' => '�����'];
    foreach ($arr as $k => $v) {
        if (isset($select[$k])) {
            temp_var('form_'.$k, get_select_option_list(['array' => $select[$k], 'value' => 'en', 'text' => 'ru', 'selected_value' => $v]));
        }
        temp_var($k, empty($v) ? '' : $v);
    }

    return template('admin/translate/stars/form_edit_star');
}

function admin_submit_star_add()
{
    // pre($_POST);
    $name = empty($_POST['name']) ? error('�� �������� ���') : trim($_POST['name']);
    $arr = query_arr('select id from '.typetab('stars').' where name=\''.$name.'\';');
    if ($arr) {
        alert($name.' ��� ����������');
    }
    query('insert into '.typetab('stars').' set name=\''.checkstr($name).'\';');
    alert('��������� ������ '.$name);
}

function admin_submit_star_edit()
{
    $id = empty($_POST['id']) ? error('�� ������� ID') : (int) $_POST['id'];
    $arr = query_assoc('select if(date>\'2015-10-23 05:03:59\',1,0) as is_allowed from '.typetab('stars').' where id=\''.$id.'\';');
    // pre($arr);
    if ($arr['is_allowed'] == 0) {
        alert('�� �� ������ ������������� ��� ������');
    }
    foreach ($_POST as $k => $v) {
        $p[$k] = trim(checkstr($v));
    }
    query('update '.typetab('stars').' set
		name=\''.$p['name'].'\',
		name_ru=\''.$p['name_ru'].'\',
		aka=\''.$p['aka'].'\',
		url=\''.$p['url'].'\',
		gender=\''.$p['gender'].'\',
		hair=\''.$p['hair'].'\',
		ethnicity=\''.$p['ethnicity'].'\',
		age=\''.$p['age'].'\',
		country=\''.$p['country'].'\',
		country_en=\''.$p['country_en'].'\',
		region=\''.$p['region'].'\',
		region_en=\''.$p['region_en'].'\',
		city=\''.$p['city'].'\',
		city_en=\''.$p['city_en'].'\',
		height=\''.$p['height'].'\',
		weight=\''.$p['weight'].'\'
		where id=\''.$id.'\'
		;');
    alert('��������� ���������');
}

function admin_stars_list()
{
    $arr = query_arr('select id,date,name,name_ru,gender,age,country,country_en,ethnicity,hair,url,
		(select count(distinct video_id) from '.typetab('tags').' where tag_en=stars.name) as q_videos
		from '.tabname('videohub', 'stars').'
		order by date,name;');
    $list = '';
    foreach ($arr as $v) {
        if (empty($v['name']) or empty($v['name_ru']) or empty($v['gender']) or empty($v['age'])
            or empty($v['country']) or empty($v['country_en']) or empty($v['ethnicity'])
            or empty($v['hair']) or empty($v['url'])) {
            $is_full = false;
        } else {
            $is_full = true;
        }
        // var_dump($is_full);
        $list .= '<li>
		<i class="icon-'.($is_full ? 'ok' : 'remove').'"></i>
		<a href="/?page=admin/translate/stars/edit&amp;id='.$v['id'].'">'.$v['name'].'</a>
		('.$v['q_videos'].' �����)
		</li>
		';
    }
    $content = '<ol>'.$list.'</ol>';

    return $content;
}

function correct_bad_texts()
{
    $page = isset($_GET['p']) ? $_GET['p'] : 1;
    $per_page = 20;
    $from = ($page - 1) * $per_page;
    $bad_words = '��������|�������|�����|���|������|������|�������|�����|����������������|����|����|�������|������';
    $arr = query_arr('select SQL_CALC_FOUND_ROWS id,title_ru,title_ru_alt,description
		from '.tabname('videohub', 'videos').'
		where
		concat_ws(\' \',title_ru,title_ru_alt,description) regexp \''.$bad_words.'\'
		limit '.$from.','.$per_page.'
		;');
    if (! $arr) {
        alert('��� ��������');
    }
    $rows = rows_without_limit();
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li class="well texts">
		<input class="title" type="text" name="videos['.$v['id'].'][title_ru]" value="'.$v['title_ru'].'"><br/>
		<input class="title" type="text" name="videos['.$v['id'].'][title_ru_alt]" value="'.$v['title_ru_alt'].'"><br/>
		<textarea class="description" name="videos['.$v['id'].'][description]">'.$v['description'].'</textarea>
		</li>';
    }
    $content = '
	<div class="all_count">�����: '.$rows.'</div>
	<div>���������� �����: '.$bad_words.'</div>
	<form method="post" action="?page=admin/translate/submit_correct_bad_texts">
	<ul class="desc">'.$list.'</ul>
	<input type="submit" name="submit" value="���������" />
	</form>
	<div class="pagination">
	<ul>'.page_numbers($rows, $per_page, $page, '/?page=admin/translate/correct_bad_texts&amp;p=', 5, '', '�����', '������', 'yes', 'active', 'disabled', 1).'</ul>
	</div>
	';

    return $content;
}

function submit_correct_bad_texts()
{
    // pre($_POST);
    if (empty($_POST['videos'])) {
        error('�� ������� ������ �������');
    }
    $videos = $_POST['videos'];
    foreach ($videos as $k => $v) {
        query('update '.tabname('videohub', 'videos').' set
			title_ru=\''.checkstr($v['title_ru']).'\',
			title_ru_alt=\''.checkstr($v['title_ru_alt']).'\',
			description=\''.checkstr($v['description']).'\'
			where id=\''.$k.'\'
			;');
    }
    alert('���������������. <a href="/?page=admin/translate/correct_bad_texts">����������</a>');
}

function get_text_ru_check_text()
{
    if (empty($_GET['video_id'])) {
        error('�� ������� video_id');
    }

    return text_ru_check_text($_GET['video_id']);
}

// ��������� �������� ������������ ��������� Q ������������� �����
function text_ru_check_video_descriptions()
{
    if (empty($_GET['q'])) {
        error('�� ������� q');
    }
    $sql['booked_domain'] = '';
    $sql['description_translater'] = '';
    if (! empty($_GET['description_translater']) and $_GET['description_translater'] != 'null') {
        $sql['description_translater'] = 'and description_translater=\''.$_GET['description_translater'].'\'';
    }
    if (! empty($_GET['booked_domain']) and $_GET['booked_domain'] != 'null') {
        $sql['booked_domain'] = 'and videos.booked_domain=\''.$_GET['booked_domain'].'\'';
    }
    $data = query('select sql_calc_found_rows id,description,video_text_unique.video_id, description_translater, description_translate_date
		from '.tabname('videohub', 'videos').'
		left join '.tabname('videohub', 'video_text_unique').' on video_text_unique.video_id=videos.id
		where description is not null and
		video_text_unique.video_id is null '.$sql['booked_domain'].' '.$sql['description_translater'].'
		order by description_translate_date desc
		limit '.$_GET['q'].';');
    $rows = rows_without_limit();
    $arr = readdata($data, 'nokey');
    $results = [];
    foreach ($arr as $v) {
        $results[] = text_ru_check_text($v['id']).' '.$v['description_translater'].' '.$v['description_translate_date'];
    }

    // pre($results);
    return '�����: '.$rows.'<br/><ul>'.li_from_array($results).'</ul>'.js_reload_page(3);
}

// ��������� ��������� �������� ������������ (Q �� ���)
function text_ru_get_pending_results()
{
    if (empty($_GET['q'])) {
        error('�� ������� q');
    }
    $data = query('select video_id,videos.description_translater
		from '.tabname('videohub', 'video_text_unique').'
		join '.tabname('videohub', 'videos').' on videos.id=video_text_unique.video_id
		where result is null
		limit '.$_GET['q'].';');
    $arr = readdata($data, 'nokey');
    $results = [];
    foreach ($arr as $v) {
        $result = text_ru_check_text($v['video_id']);
        if (is_object($result)) {
            $results[] = $result->unique.' '.$v['description_translater'];
        } else {
            $results[] = $result;
        }
    }

    // pre($results);
    return '<ul>'.li_from_array($results).'</ul>'.js_reload_page(3);
}

function text_ru_unique_info()
{
    if (empty($_GET['video_id'])) {
        error('�� ������� video_id');
    }
    $data = query('select domain,description,description_translater from '.tabname('videohub', 'videos').' where id=\''.$_GET['video_id'].'\';');
    $arr = mysql_fetch_assoc($data);
    $result = text_ru_check_text($_GET['video_id']);
    if (is_object($result)) {
        $links_list = '';
        // pre($result);
        foreach ($result->urls as $v) {
            $links_list .= '<li><a href="'.$v->url.'" target="_blank">'.$v->url.'</a> (������� '.$v->plagiat.')</li>';
        }
        $content = '<br/>
		<p>'.$arr['description'].'</p>
		����������: '.$arr['description_translater'].'<br/>
		���� ��������: '.$result->date_check.'<br/>
		����������� �����: '.$arr['domain'].' � �������<br/>
		������������: '.$result->unique.'%<br/>
		������-��������: <br/><ol>'.$links_list.'</ol><br/>
		';

        // pre($result);
        return $content;
    } else {
        return $result.js_reload_page(10);
    }
}

// ����� ���� ����� ��������� ������� result_short � �������. ������ � �������� �� �����
function set_all_short_results()
{
    $arr = readdata(query('select * from '.tabname('videohub', 'video_text_unique').' where result_short is null;'), 'nokey');
    foreach ($arr as $k => $v) {
        $result_decode = json_decode($v['result']);
        $result_short = $result_decode->unique;
        query('update '.tabname('videohub', 'video_text_unique').' set result_short=\''.$result_short.'\' where video_id=\''.$v['video_id'].'\';');
        // $arr[$k]['result_short']=$result_short;
    }
    // pre($arr);
}

function test_text_ru()
{
    pre(iconv('utf-8', 'windows-1251', text_ru_getResultPost('54c650810296f', 'd6c6c0b10e560da9dd8c1a56122d31c2')));
}

function text_ru_check_text($video_id)
{
    $userkey = value_from_config('videohub', 'text_ru_userkey');
    $except_domains = '';
    $data = query('select count(*) as q from '.tabname('videohub', 'video_text_unique').' where video_id=\''.$video_id.'\';');
    $arr = mysql_fetch_assoc($data);
    if ($arr['q'] == 0) {
        $data = query('select domain,description from '.tabname('videohub', 'videos').' where id=\''.$video_id.'\';');
        $arr = mysql_fetch_assoc($data);
        if (! empty($arr['domain'])) {
            $except_domains = $arr['domain'];
            $arr2 = query_arr('select domain from '.tabname('laravel', 'mirrors').' where mirrors.group=\''.$arr['domain'].'\';');
            foreach ($arr2 as $v2) {
                $except_domains .= ','.$v2['domain'];
            }
            // pre($except_domains);
        }
        $text = iconv('windows-1251', 'utf-8', $arr['description']);
        $time_start = show_time();
        $result = iconv('utf-8', 'windows-1251', text_ru_addPost($text, $userkey, $except_domains));
        $time = round(show_time() - $time_start, 4);
        query('insert into '.tabname('videohub', 'video_text_unique').' set video_id=\''.$video_id.'\',text_uid=\''.$result.'\';');

        return '�������� �������� ������. Uid: '.$result.'. ����� ����������: '.$time.' ���.';
    } else {
        $data = query('select text_uid,result from '.tabname('videohub', 'video_text_unique').' where video_id=\''.$video_id.'\';');
        $arr = mysql_fetch_assoc($data);
        if (! empty($arr['result'])) {
            $result = $arr['result'];
            $result_decode = json_decode($result);
        } else {
            if (empty($arr['text_uid'])) {
                error('����������� uid ������');
            }
            $result = iconv('utf-8', 'windows-1251', text_ru_getResultPost($arr['text_uid'], $userkey));
            // pre($result);
            $result_decode = json_decode($result);
            if (! $result_decode) {
                return $result;
            }
            $result_short = $result_decode->unique;
            query('update '.tabname('videohub', 'video_text_unique').' set result=\''.$result.'\',result_short=\''.$result_short.'\' where text_uid=\''.$arr['text_uid'].'\';');
        }

        // pre($result_decode->unique);
        return $result_decode;
    }
}

function translate_news()
{
    global $config;
    $content = '';
    $content .= '
		<div class="alert">
		������, ��������� ����� ��� �������� � ������� <a href="/?page=admin/translate/view">�������������� ����� (���������)</a>
		<!--
		� <a href="/?page=admin/translate/titles_descriptions&domain=sexhome.tv">�������������� ����� (��������� + ��������) ��� sexhome.tv</a>
		-->
		.
		</div>
		';
    // };
    // return $content;
}

// ������ ������������. ������ ��� � 10 ���
function get_translate_balance_cached()
{
    $arr = cache('preparing_get_translate_balance', $args = [], 600);

    return $arr;
}

// ������ ������������ � �������� �������
function get_translate_balance()
{
    $arr = preparing_get_translate_balance();

    return $arr;
}

function preparing_get_translate_balance()
{
    $cost['title'] = value_from_config('videohub', 'translate_title_one_symbol_cost');
    $cost['description'] = value_from_config('videohub', 'description_one_symbol_cost');
    // �������������� ����
    $data = query('
		select login,ifnull(title_cost,'.$cost['title'].') as titles,ifnull(description_cost,'.$cost['description'].') as descriptions
		from '.tabname('videohub', 'users').'
		group by login;
		');
    $user_cost = readdata($data, 'login');
    // pre($user_cost);
    $data = query('
		select login,paid_symbols as titles,paid_symbols_description as descriptions
		from '.tabname('videohub', 'users').'
		group by login;
		');
    $paid = readdata($data, 'login');
    $data = query('
		    SELECT translater as login,ifnull(sum(length(videos.title_ru)),0) as sum
		    FROM '.tabname('videohub', 'videos').'
		    group by login
		    having login is not null;
		');
    $arr['videos_titles'] = readdata($data, 'login');
    $data = query('
		    SELECT translater as login,ifnull(sum(length(videos.title_ru_alt)),0) as sum
		    FROM '.tabname('videohub', 'videos').'
		    group by login
		    having login is not null;
		');
    $arr['videos_titles_alt'] = readdata($data, 'login');
    $data = query('
		    SELECT description_translater as login,ifnull(sum(length(videos.description)),0) as sum
		    FROM '.tabname('videohub', 'videos').'
		    group by login
		    having login is not null;
		');
    $arr['videos_descriptions'] = readdata($data, 'login');
    $data = query('
		    SELECT translater as login,ifnull(sum(length(galleries.title_ru)),0) as sum
		    FROM '.tabname('videohub', 'galleries').'
		    group by login
		    having login is not null;
		');
    $arr['galleries_titles'] = readdata($data, 'login');
    $data = query('
		    SELECT description_translater as login,ifnull(sum(length(galleries.description)),0) as sum
		    FROM '.tabname('videohub', 'galleries').'
		    group by login
		    having login is not null;
		');
    $arr['galleries_descriptions'] = readdata($data, 'login');
    $data = query('
		    SELECT title_translater as login,ifnull(sum(length(games.title)),0) as sum
		    FROM '.tabname('videohub', 'games').'
		    group by login
		    having login is not null;
		');
    $arr['games_titles'] = readdata($data, 'login');
    $data = query('
		    SELECT description_translater as login,ifnull(sum(length(games.description)),0) as sum
		    FROM '.tabname('videohub', 'games').'
		    group by login
		    having login is not null;
		');
    $arr['games_descriptions'] = readdata($data, 'login');
    $data = query('select login from '.tabname('videohub', 'users').' where level in (\'root\',\'admin\',\'worker\',\'translater\',\'translater_eng\');');
    $users = readdata($data, 'login');
    foreach ($users as $v) {
        if (! isset($arr['videos_titles'][$v['login']]['sum'])) {
            $arr['videos_titles'][$v['login']]['sum'] = 0;
        }
        if (! isset($arr['videos_titles_alt'][$v['login']]['sum'])) {
            $arr['videos_titles_alt'][$v['login']]['sum'] = 0;
        }
        if (! isset($arr['galleries_titles'][$v['login']]['sum'])) {
            $arr['galleries_titles'][$v['login']]['sum'] = 0;
        }
        if (! isset($arr['games_titles'][$v['login']]['sum'])) {
            $arr['games_titles'][$v['login']]['sum'] = 0;
        }
        if (! isset($arr['videos_descriptions'][$v['login']]['sum'])) {
            $arr['videos_descriptions'][$v['login']]['sum'] = 0;
        }
        if (! isset($arr['galleries_descriptions'][$v['login']]['sum'])) {
            $arr['galleries_descriptions'][$v['login']]['sum'] = 0;
        }
        if (! isset($arr['games_descriptions'][$v['login']]['sum'])) {
            $arr['games_descriptions'][$v['login']]['sum'] = 0;
        }
        // $balance[$v['login']]['titles']=round(($arr['videos_titles'][$v['login']]['sum']+$arr['galleries_titles'][$v['login']]['sum']+$arr['games_titles'][$v['login']]['sum']-$paid[$v['login']]['titles'])*$cost['title'],2);
        // $balance[$v['login']]['descriptions']=round(($arr['videos_descriptions'][$v['login']]['sum']+$arr['galleries_descriptions'][$v['login']]['sum']+$arr['games_descriptions'][$v['login']]['sum']-$paid[$v['login']]['descriptions'])*$cost['description'],2);
        $balance[$v['login']]['titles'] = round(($arr['videos_titles'][$v['login']]['sum'] + $arr['videos_titles_alt'][$v['login']]['sum'] + $arr['galleries_titles'][$v['login']]['sum'] + $arr['games_titles'][$v['login']]['sum'] - $paid[$v['login']]['titles']) * $user_cost[$v['login']]['titles'], 2);
        $balance[$v['login']]['descriptions'] = round(($arr['videos_descriptions'][$v['login']]['sum'] + $arr['galleries_descriptions'][$v['login']]['sum'] + $arr['games_descriptions'][$v['login']]['sum'] - $paid[$v['login']]['descriptions']) * $user_cost[$v['login']]['descriptions'], 2);
        $balance[$v['login']]['common'] = round($balance[$v['login']]['titles'] + $balance[$v['login']]['descriptions'], 3);
    }

    // pre($arr);
    // pre($balance);
    return $balance;
}

function test_balance()
{
    pre(get_translate_balance());
}

function translate_user_info()
{
    $balance = get_translate_balance_cached();

    return '������: '.$balance[auth_login(['type' => 'videohub'])]['common'].'�.';
}

function translate_user_info_old()
{
    $cost['title'] = value_from_config('videohub', 'translate_title_one_symbol_cost');
    $cost['description'] = value_from_config('videohub', 'description_one_symbol_cost');
    $data = query('select paid_symbols,paid_symbols_description from '.tabname('videohub', 'users').' where login=\''.auth_login(['type' => 'videohub']).'\';');
    $arr = mysql_fetch_assoc($data);
    $paid['title'] = $arr['paid_symbols'];
    $paid['description'] = $arr['paid_symbols_description'];
    $data = query('
		    SELECT ifnull(sum(length(videos.title_ru)),0) as sum_videos_titles
		    FROM '.tabname('videohub', 'users').'
		    JOIN '.tabname('videohub', 'videos').' ON videos.translater=users.login
		    WHERE login=\''.auth_login(['type' => 'videohub']).'\'
		');
    $arr = mysql_fetch_assoc($data);
    $sum['videos_titles'] = $arr['sum_videos_titles'];
    $data = query('
		    SELECT ifnull(sum(length(videos.description)),0) as sum_videos_descriptions
		    FROM '.tabname('videohub', 'users').'
		    JOIN '.tabname('videohub', 'videos').' ON videos.description_translater=users.login
		    WHERE login=\''.auth_login(['type' => 'videohub']).'\'
		');
    $arr = mysql_fetch_assoc($data);
    $sum['videos_descriptions'] = $arr['sum_videos_descriptions'];
    //
    $data = query('
		    SELECT ifnull(sum(length(galleries.title_ru)),0) as sum_galleries_titles
		    FROM '.tabname('videohub', 'users').'
		    JOIN '.tabname('videohub', 'galleries').' ON galleries.translater=users.login
		    WHERE login=\''.auth_login(['type' => 'videohub']).'\'
		');
    $arr = mysql_fetch_assoc($data);
    $sum['galleries_titles'] = $arr['sum_galleries_titles'];
    $data = query('
		    SELECT ifnull(sum(length(galleries.description)),0) as sum_galleries_descriptions
		    FROM '.tabname('videohub', 'users').'
		    JOIN '.tabname('videohub', 'galleries').' ON galleries.description_translater=users.login
		    WHERE login=\''.auth_login(['type' => 'videohub']).'\'
		');
    $arr = mysql_fetch_assoc($data);
    $sum['galleries_descriptions'] = $arr['sum_galleries_descriptions'];
    //
    $data = query('
		    SELECT ifnull(sum(length(games.title)),0) as sum_games_titles
		    FROM '.tabname('videohub', 'users').'
		    JOIN '.tabname('videohub', 'games').' ON games.title_translater=users.login
		    WHERE login=\''.auth_login(['type' => 'videohub']).'\'
		');
    $arr = mysql_fetch_assoc($data);
    $sum['games_titles'] = $arr['sum_games_titles'];
    $data = query('
		    SELECT ifnull(sum(length(games.description)),0) as sum_games_descriptions
		    FROM '.tabname('videohub', 'users').'
		    JOIN '.tabname('videohub', 'games').' ON games.description_translater=users.login
		    WHERE login=\''.auth_login(['type' => 'videohub']).'\'
		');
    $arr = mysql_fetch_assoc($data);
    $sum['games_descriptions'] = $arr['sum_games_descriptions'];
    //
    $balance['video_title'] = round(($sum['videos_titles'] + $sum['galleries_titles'] + $sum['games_titles'] - $paid['title']) * $cost['title'], 2);
    $balance['video_description'] = round(($sum['videos_descriptions'] + $sum['galleries_descriptions'] + $sum['games_descriptions'] - $paid['description']) * $cost['title'], 2);
    // pre($balance);
    $balance['common'] = $balance['video_title'] + $balance['video_description'];
    $content = '������: '.$balance['common'].'�.';

    // $content='���������: $'.$balance['video_title'].', ��������: $'.$balance['video_description'];
    return $content;
}

// ���������� ������ � �������� ������ � ���������� � ������� ����� ������������
function preparing_domains_with_description()
{
    $data = query('
		select booked_domain,count(*) as q
		from '.tabname('videohub', 'videos').'
		where (title_ru is null or title_ru=\'\') and (description is null or description=\'\')
		and domain is null
		and (booked_domain in (
			select domain from '.tabname('videohub', 'config_for_domain').' where property=\'only_described_videos\' and value=\'1\' order by domain
			)
	or booked_domain in (select distinct value from '.typetab('config_for_domain').' where property=\'booking_type\')
			or booked_domain=\'with_description\')
		group by booked_domain;
	');
    $arr = readdata($data, 'nokey');

    return $arr;
}

// ������ ������� ������ � ���������� (����������� ��� � 10 ���)
function translate_domains_with_description_list()
{
    if (empty($_GET['domain'])) {
        $domain = '';
    } else {
        $domain = $_GET['domain'];
    }
    $args['domain'] = $domain;
    $arr = cache('preparing_domains_with_description', $args, 600);
    $list = '';
    foreach ($arr as $v) {
        $class_active = '';
        if ($domain == $v['booked_domain']) {
            $class_active = ' class="active"';
        }
        $booked_domain = $v['booked_domain'];
        if ($v['booked_domain'] == 'with_description') {
            $booked_domain = '������';
        }
        $list .= '<li'.$class_active.'><a href="/?page=admin/translate/titles_descriptions&amp;domain='.$v['booked_domain'].'">'.$booked_domain.' ('.$v['q'].')</a></li>';
    }

    return $list;
}

// �������������� ��������� � ��������
function correct_titles_descriptions()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    // if (empty($_GET['domain'])) {return '';};
    // $domain=$_GET['domain'];
    $per_page = value_from_config('videohub', 'translate_titles_per_page');
    $from = ($_GET['p'] - 1) * $per_page;
    $arr = query_arr('select SQL_CALC_FOUND_ROWS id,title_en,title_ru,description,booked_domain from '.tabname('videohub', 'videos').'
		where
		domain is null
		and booked_domain like \'%_correct\' and translater=\'adult\'
		order by add_date desc
		limit '.$from.','.$per_page.'
		;');
    $rows = rows_without_limit();
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $list = '';
    $q = 0;
    foreach ($arr as $k => $v) {
        if ($q % 2 == 0) {
            $list .= '<tr>';
        }
        $list .= '<li class="well well-large">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/view_video&amp;id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/images/'.$v['id'].'.jpg" alt="" /></a></td>
		<td>
		<p>'.$v['title_en'].'</p>
		<p>'.video_tags_list_simple($v['id']).'</p>
		<input type="text" name="videos['.$v['id'].'][title_ru]" spellcheck="true" style="width: 90%;"
		id="title'.$v['id'].'"
		onkeyup="symbol_counter(\'title'.$v['id'].'\',\'symbols_title_'.$v['id'].'\',80);"
		maxlength="80" value="'.$v['title_ru'].'" />
		<div id="symbols_title_'.$v['id'].'" class="max_symbols"></div>
		<br/><textarea id="text'.$v['id'].'" name="videos['.$v['id'].'][description]"
		spellcheck="true" style="width: 90%; height: 70px;"
		onkeyup="symbol_counter(\'text'.$v['id'].'\',\'symbols'.$v['id'].'\',250);"
		maxlength="250"
		>'.$v['description'].'</textarea>
		<div id="symbols'.$v['id'].'" class="max_symbols"></div>
		</td></tr></table>
		</li>
		';
        $q++;
        if ($q % 2 == 0) {
            $list .= '</tr>';
        }
    }
    if ($q % 2 != 0) {
        $list .= '<td colspan="'.(2 - ($q % 2)).'"></td></tr>';
    }
    $content = '
	<form method="post" action="?page=admin/translate/submit_translate">
	<div class="pages"><div class="all_count">�����: '.$rows.'</div>
	<ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/correct_titles_descriptions&amp;p=').'</ul><hr/></div><hr/>
	<ul class="desc">'.$list.'</ul>
	<div class="clearboth"></div>
	<input type="hidden" name="make_descriptions" value="yes" />
	<input type="hidden" name="correct" value="yes" />
	<input type="submit" name="submit" value="���������" />
	</form>';

    return $content;
}

// ��������� � ��������
function translate_titles_descriptions()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    if (empty($_GET['domain'])) {
        return '';
    }
    $domain = $_GET['domain'];
    $per_page = value_from_config('videohub', 'translate_titles_per_page');
    $video_title_alt = value_from_config_for_domain('videohub', 'video_title_alt', $domain);
    $title_limit = value_from_config_for_domain('videohub', 'video_title_limit', $domain);
    $title_alt_limit = value_from_config_for_domain('videohub', 'video_title_alt_limit', $domain);
    $description_limit = value_from_config_for_domain('videohub', 'video_description_limit', $domain);
    $translate_to_eng = value_from_config_for_domain('videohub', 'translate_to_eng', $domain);
    if ($translate_to_eng == '1') {
        authorizate_videohub_translater_eng();
    }
    // pre($video_title_alt);
    $make_title_ru_alt = '';
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_en,booked_domain,
		id_hub,hub,ph_viewkey,ph_date
		from '.tabname('videohub', 'videos').'
		where (title_ru is null or title_ru=\'\') and (description is null or description=\'\')
		and domain is null
		and booked_domain=\''.$domain.'\'
		order by add_date desc
		limit '.$from.','.$per_page.'
		;');
    $rows = rows_without_limit();
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    $q = 0;
    foreach ($arr as $k => $v) {
        if ($q % 2 == 0) {
            $list .= '<tr>';
        }
        $code_title_alt = '';
        if ($video_title_alt == 1) {
            $make_title_ru_alt = '<input type="hidden" name="make_title_ru_alt" value="yes" />';
            $code_title_alt = '
			<input type="text" name="videos['.$v['id'].'][title_ru_alt]" spellcheck="true" style="width: 90%;"
			id="title_ru_alt'.$v['id'].'"
			onkeyup="symbol_counter(\'title_ru_alt'.$v['id'].'\',\'symbols_title_ru_alt_'.$v['id'].'\','.$title_alt_limit.');"
			maxlength="'.$title_alt_limit.'" />
			<div id="symbols_title_ru_alt_'.$v['id'].'" class="max_symbols"></div>
			<br/>
			';
        }
        $list .= '<li class="well well-large">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/view_video&amp;id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/images/'.$v['id'].'.jpg" alt="" id="pic_'.$v['id'].'" '.img_scroll($v).' /></a></td>
		<td>
		<p>'.$v['title_en'].'</p>
		<p>'.video_tags_list_simple($v['id']).'</p>
		<input type="text" name="videos['.$v['id'].'][title_ru]" spellcheck="true" style="width: 90%;"
		id="title'.$v['id'].'"
		onkeyup="symbol_counter(\'title'.$v['id'].'\',\'symbols_title_'.$v['id'].'\','.$title_limit.');"
		maxlength="'.$title_limit.'" />
		<div id="symbols_title_'.$v['id'].'" class="max_symbols"></div>
		<br/>
		'.$code_title_alt.'
		<textarea id="text'.$v['id'].'" name="videos['.$v['id'].'][description]"
		spellcheck="true" style="width: 90%; height: 70px;"
		onkeyup="symbol_counter(\'text'.$v['id'].'\',\'symbols'.$v['id'].'\','.$description_limit.');"
		maxlength="'.$description_limit.'"
		></textarea>
		<div id="symbols'.$v['id'].'" class="max_symbols"></div>
		</td></tr></table>
		</li>
		';
        $q++;
        if ($q % 2 == 0) {
            $list .= '</tr>';
        }
    }
    if ($q % 2 != 0) {
        $list .= '<td colspan="'.(2 - ($q % 2)).'"></td></tr>';
    }
    $content = '
	<form method="post" action="?page=admin/translate/submit_translate">
	<div class="pages"><div class="all_count">�����: '.$rows.'</div>
	<ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/titles_descriptions&amp;domain='.$domain.'&amp;p=').'</ul><hr/></div><hr/>
	<ul class="desc">'.$list.'</ul>
	<div class="clearboth"></div>
	<input type="hidden" name="make_descriptions" value="yes" />
	'.$make_title_ru_alt.'
	<input type="submit" name="submit" value="���������" />
	</form>';

    return $content;
}

// ������ ���������. ������������ ��� ����� ��� �����.
function translate_view()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $page_domain = '';
    // ��������������� ����� ���������� ������ ���������� ��� ������ ����.
    // $domain_query='booked_domain is null';
    $domain_query = '1';
    if (! empty($_GET['domain'])) {
        $domain_query = 'booked_domain=\''.$_GET['domain'].'\'';
        $page_domain = '&domain='.$_GET['domain'];
        $translate_to_eng = value_from_config_for_domain('videohub', 'translate_to_eng', $_GET['domain']);
        if ($translate_to_eng == '1') {
            authorizate_videohub_translater_eng();
        }
    }
    $per_page = value_from_config('videohub', 'translate_titles_per_page');
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_en,
		id_hub,hub,ph_viewkey,ph_date
		from '.tabname('videohub', 'videos').'
		where (title_ru is null or title_ru=\'\')
		and '.$domain_query.'
		and domain is null /* ����� ����������� ����� � ����������� */
		order by add_date desc
		limit '.$from.','.$per_page);
    $rows = rows_without_limit();
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    $q = 0;
    foreach ($arr as $k => $v) {
        if ($q % 2 == 0) {
            $list .= '<tr>';
        }
        $list .= '<li class="well well-large">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/view_video&amp;id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/images/'.$v['id'].'.jpg" alt="" id="pic_'.$v['id'].'" '.img_scroll($v).' /></a></td>
		<td>
		<p>'.$v['title_en'].' [<a href="/?page=admin/translate/source&amp;id='.$v['id'].'" target="_blank">��������</a>] <a href="/?page=admin/delete_video&amp;id='.$v['id'].'" style="text-decoration: none;">[X]</a></p>
		<p>'.video_tags_list_simple($v['id']).'</p>
		<input type="text" name="videos['.$v['id'].'][title_ru]" spellcheck="true" style="width: 90%;"
		id="title'.$v['id'].'"
		onkeyup="symbol_counter(\'title'.$v['id'].'\',\'symbols_title_'.$v['id'].'\',90);"
		maxlength="90"	/>
		<div id="symbols_title_'.$v['id'].'" class="max_symbols"></div>
		<!--
		<br/><textarea name="videos['.$v['id'].'][description]" spellcheck="true" style="width: 90%; height: 70px;"></textarea>
		-->
		</td></tr></table>
		</li>
		';
        $q++;
        if ($q % 2 == 0) {
            $list .= '</tr>';
        }
    }
    if ($q % 2 != 0) {
        $list .= '<td colspan="'.(2 - ($q % 2)).'"></td></tr>';
    }
    $content = '
	<div class="pages"><div class="all_count">�����: '.$rows.'</div>
	<ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/view'.$page_domain.'&amp;p=').'</ul><hr/></div><hr/>
	<form method="post" action="?page=admin/translate/submit_translate">
	<ul class="desc">'.$list.'</ul>
	<div class="clearboth"></div>
	<input type="submit" name="submit" value="���������" />
	</form>
	';

    return $content;
}

/*
function fix_title_translit() {
    $arr=query_arr('select id,title_ru from '.typetab('videos').' where translate_date>\'2018-05-14\';');
    // pre($arr);
    foreach ($arr as $v) {
        query('update '.typetab('videos').' set title_ru_translit=\''.url_translit($v['title_ru']).'\' where id=\''.$v['id'].'\';');
    };
    alert('ok');
}
*/

// ���������� ������� � ����
function translate_submit_translate()
{
    if (empty($_POST['videos']) == 1) {
        error('�� �������� ���������');
    }
    $correct = isset($_POST['correct']) ? $_POST['correct'] : 'no';
    // pre($correct);
    $q_titles = 0;
    $q_all = 0;
    $q_descriptions = 0;
    $q_all_descriptions = 0;
    $q_titles_ru_alt = 0;
    $q_all_titles_ru_alt = 0;
    $content = '';
    foreach ($_POST['videos'] as $k => $v) {
        if (empty($v['title_ru']) == 0) {
            $data = query('select title_ru,title_en,translater from '.tabname('videohub', 'videos').' where id=\''.$k.'\'');
            $arr = mysql_fetch_assoc($data);
            if ($correct == 'no' and ! empty($arr['title_ru'])) {
                $content .= '"'.$arr['title_en'].'" ��� ������� '.$arr['translater'].' ("'.$arr['title_ru'].'")<br/>';
            } else {
                $v['title_ru'] = trim($v['title_ru']);
                if (! empty($v['title_ru_alt'])) {
                    $v['title_ru_alt'] = trim($v['title_ru_alt']);
                }
                if (! empty($v['descriptions'])) {
                    $v['description'] = trim($v['description']);
                }
                $data = query('select id,title_en,title_ru,translater from '.tabname('videohub', 'videos').' where title_ru=\''.checkstr($v['title_ru']).'\'');
                if ($correct == 'no' and mysql_num_rows($data) != 0) {
                    $content .= '��������� "'.$v['title_ru'].'" ��� ������������ � ����.<br/>';
                } else {
                    $query_str = 'title_ru=\''.checkstr($v['title_ru']).'\', title_ru_translit=\''.url_translit($v['title_ru']).'\', translater=\''.auth_login(['type' => 'videohub']).'\', translate_date=current_timestamp';
                    $q_titles++;
                    $q = strlen($v['title_ru']);
                    $q_all = $q_all + $q;
                    if (empty($v['title_ru_alt'])) {
                        if (isset($_POST['make_title_ru_alt']) and $_POST['make_title_ru_alt'] == 'yes') {
                            alert('���������� ������� �������������� ��������� ��� "'.$v['title_ru'].'"');
                        }
                    }
                    if (! empty($v['title_ru_alt'])) {
                        $q_titles_ru_alt++;
                        $q_title_ru_alt = strlen($v['title_ru_alt']);
                        $q_all_titles_ru_alt = $q_all_titles_ru_alt + $q_title_ru_alt;
                        $query_str .= ', title_ru_alt=\''.checkstr($v['title_ru_alt']).'\'';
                    }
                    if (empty($v['description'])) {
                        if (isset($_POST['make_descriptions']) and $_POST['make_descriptions'] == 'yes') {
                            alert('���������� ������� �������� ��� "'.$v['title_ru'].'"');
                        }
                    }
                    if (! empty($v['description'])) {
                        $q_descriptions++;
                        $q_description = strlen($v['description']);
                        $q_all_descriptions = $q_all_descriptions + $q_description;
                        $query_str .= ', description=\''.checkstr($v['description']).'\', description_translater=\''.auth_login(['type' => 'videohub']).'\', description_translate_date=current_timestamp';
                    }
                    query('update '.tabname('videohub', 'videos').' set '.$query_str.' where id=\''.$k.'\'');
                }
            }
        }
    }
    $content .= '<b>���������:</b>
	<br/>���������� ����������: '.$q_titles.'
	<br/>��������: '.$q_all.'
	<br/>���������� �������������� ����������: '.$q_titles_ru_alt.'
	<br/>��������: '.$q_all_titles_ru_alt.'
	<br/>���������� ��������: '.$q_descriptions.'
	<br/>��������: '.$q_all_descriptions.'
	<br/><a href="/">�������</a>';
    // ���������� ��� �������
    reset_cache('preparing_get_translate_balance');
    alert($content);
    // return $content;
}

function domain_option_list()
{
    if (empty($_REQUEST['domain']) == 1) {
        $_REQUEST['domain'] = '';
    }
    $data = query('select domain from '.tabname('engine', 'domains').'
		where status=\'on\' and type=\'videohub\'
		and ifnull((select value from '.tabname('videohub', 'config_for_domain').' where domain=domains.domain and property=\'visible\'),1
	)=1
	order by domain
	');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $selected = '';
        if ($v['domain'] == $_REQUEST['domain']) {
            $selected = 'selected';
        }
        $list .= '<option value="'.$v['domain'].'"" '.$selected.'>'.$v['domain'].'</option>';
    }

    return $list;
}

// ������ ��������. ����������� ����� ����������� �� ����� � ������� ������ ��������� (��� ������� � ������� visiblw_translate=1) + ����� ��� �������,�������,�������� � ���������� � ��� ��������
function translate_descriptions()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $data = query('select value from '.tabname('videohub', 'config').' where property=\'translate_titles_per_page\'');
    $arr = mysql_fetch_assoc($data);
    $per_page = $arr['value']; // ���������� �� ��������
    $from = ($_GET['p'] - 1) * $per_page;
    //
    $clause['order'] = '';
    $clause['domain'] = '';
    // if (empty($_GET['order'])==1) {$_GET['order']='translate_date';};
    // if (empty($_GET['order_direction'])==1) {$_GET['order_direction']='desc';};
    if (empty($_REQUEST['domain']) == 1) {
        $_REQUEST['domain'] = '';
    }
    /*
    if (empty($_GET['order'])==0) {
        $clause['order']='order by '.$_GET['order'].' '.$_GET['order_direction'];
    };
    */
    if (empty($_REQUEST['domain']) == 0) {
        $clause['domain'] = 'and (domain=\''.$_REQUEST['domain'].'\')';
    }
    //
    $data = query('select SQL_CALC_FOUND_ROWS id,title_en,title_ru,booked_domain from '.tabname('videohub', 'videos').'
		where
		(
		(title_ru is not null and title_ru!=\'\')
		and (description is null or description=\'\')
		and (domain is not null and domain!=\'\')
		and domain in (select domain from '.tabname('videohub', 'config_for_domain').' where property=\'visible_translater\' and value=1)
		) or ( (booked_domain=\'rutrah.net\' or booked_domain=\'sextut.net\' or booked_domain=\'sexhome.tv\') and title_ru is not null and description is null)
	'.$clause['domain'].'
	order by FIELD(booked_domain, \'sexhome.tv\',\'sextut.net\',\'rutrah.net\')
	/* order by add_date desc */
	limit '.$from.','.$per_page);
    $rows = rows_without_limit();
    if ($rows == 0) {
        alert('��� ����������� ��� �����������');
    }
    $arr = readdata($data, 'nokey');
    $list = '';
    $q = 0;
    foreach ($arr as $k => $v) {
        $list .= '<li class="well well-large">
		<table class="table"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/view_video&amp;id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/images/'.$v['id'].'.jpg" alt="" /></a></td>
		<td>
		<p>'.$v['title_en'].' [<a href="/?page=admin/translate/source&amp;id='.$v['id'].'" target="_blank">��������</a>]</p>
		<p>'.$v['title_ru'].'</p><p>'.video_tags_list_simple($v['id']).'</p>
		<textarea name="videos['.$v['id'].'][description]" spellcheck="true" style="width: 90%; height: 70px;"></textarea>
		</td></tr></table>
		</li>
		';
    }
    $content = '
	<div class="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/descriptions&amp;domain='.$_REQUEST['domain'].'&amp;p=').'</ul><hr/></div><hr/>
	<ul class="desc">'.$list.'</ul>';

    return $content;
}

function preparing_translaters_list()
{
    $data = query('select login from '.tabname('videohub', 'users').'
		where level in (\'translater\',\'translater_eng\',\'admin\',\'root\',\'worker\')
		order by login
		;');
    $arr = readdata($data, 'nokey');

    return $arr;
}

function check_descriptions()
{
    $content = '';
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $sql['booked_domain'] = '';
    $sql['description_translater'] = '';
    $sql['unique'] = '';
    $pages['booked_domain'] = '';
    $pages['description_translater'] = '';
    $pages['unique'] = '';
    $selected['booked_domain'] = 'null';
    $selected['description_translater'] = 'null';
    $selected['unique'] = 'null';
    $sql_preset['unique']['not_unique'] = 'and video_text_unique.result_short is not null and video_text_unique.result_short!=100';
    if (! empty($_GET['description_translater']) and $_GET['description_translater'] != 'null') {
        $sql['description_translater'] = 'and description_translater=\''.$_GET['description_translater'].'\'';
        $pages['description_translater'] = '&amp;description_translater='.$_GET['description_translater'];
        $selected['description_translater'] = $_GET['description_translater'];
    }
    if (! empty($_GET['booked_domain']) and $_GET['booked_domain'] != 'null') {
        $sql['booked_domain'] = 'and booked_domain=\''.$_GET['booked_domain'].'\'';
        $pages['booked_domain'] = '&amp;booked_domain='.$_GET['booked_domain'];
        $selected['booked_domain'] = $_GET['booked_domain'];
    }
    if (! empty($_GET['unique']) and $_GET['unique'] != 'null') {
        $selected['unique'] = $_GET['unique'];
        $sql['unique'] = $sql_preset['unique'][$selected['unique']];
        $pages['unique'] = '&amp;unique='.$selected['unique'];
    }
    // $per_page=value_from_config('videohub','translate_titles_per_page'); //���������� �� ��������
    $per_page = 50;
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_en,title_ru,description,description_translater,
		unix_timestamp(description_translate_date) as date, video_text_unique.result as text_ru_result,
		video_text_unique.result_short as text_ru_result_short
		from '.tabname('videohub', 'videos').'
		left join '.tabname('videohub', 'video_text_unique').' on video_text_unique.video_id=videos.id
		where description is not null and description!=\'\'
		'.$sql['description_translater'].'
		'.$sql['booked_domain'].'
		'.$sql['unique'].'
		order by description_translate_date desc
		limit '.$from.','.$per_page);
    $rows = rows_without_limit();
    $arr = readdata($data, 'nokey');
    $list = '';
    $q = 0;
    foreach ($arr as $k => $v) {
        $text_ru_unique = '����������';
        if (! empty($v['text_ru_result'])) {
            $text_ru_result_decode = json_decode($v['text_ru_result']);
            $text_ru_unique = $text_ru_result_decode->unique.'%';
            if ($text_ru_result_decode->unique != 100) {
                $text_ru_unique = '<span style="color:red; font-weight: bold;">'.$text_ru_unique.'</span>';
            }
        }
        $mgw_date = my_date($v['date']);
        $date = $mgw_date['day'].' '.$mgw_date['month'].' '.$mgw_date['year'].' � '.$mgw_date['clock'];
        if ($q % 2 == 0) {
            $list .= '<tr>';
        }
        // <p>'.video_tags_list_simple($v['id']).'</p>
        $list .= '<td style="width: 50%;">
		<table class="table table-striped"><tr>
		<td style="width: 160px;"><a href="/?page=admin/translate/view_video&amp;id='.$v['id'].'" target="_blank">
		<img src="/types/videohub/images/'.$v['id'].'.jpg" alt="" /></a></td>
		<td>
		<p>'.$v['title_en'].' [<a href="/?page=admin/translate/source&amp;id='.$v['id'].'" target="_blank">��������</a>]</p>
		<p>'.$v['title_ru'].'</p>
		<p>'.$v['description'].'</p>
		<table><tr>
			<td><p style="text-align: left;">������������: '.$text_ru_unique.' <a href="/?page=admin/execute&function=text_ru_unique_info&amp;video_id='.$v['id'].'" onclick="window.open(this.href,this.target,\'width=600,height=300,scrollbars=1\'); return false;" target="_blank">���������</a></p></td>
			<td><p style="text-align: right;"><b>'.$v['description_translater'].'</b>, '.$date.'</p></td>
		</tr></table>
		</td>
		</tr></table>
		</td>
		';
        $q++;
        if ($q % 2 == 0) {
            $list .= '</tr>';
        }
    }
    if ($q % 2 != 0) {
        $list .= '<td colspan="'.(2 - ($q % 2)).'"></td></tr>';
    }
    $booked_domains = preparing_booking_domains();
    $booked_domains[]['domain'] = 'with_description';
    $unique = [['text' => '�� 100%', 'value' => 'not_unique']];
    // pre($booked_domains);
    $translaters = preparing_translaters_list();
    $content = '
	<div style="">
	<form action="/" method="get" style="float: left;">
		�����: <select name="description_translater">'.get_select_option_list(['array' => $translaters, 'value' => 'login', 'text' => 'login', 'selected_value' => $selected['description_translater']]).'</select>
		�����: <select name="booked_domain">'.get_select_option_list(['array' => $booked_domains, 'value' => 'domain', 'text' => 'domain', 'selected_value' => $selected['booked_domain']]).'</select>
		������������: <select name="unique">'.get_select_option_list(['array' => $unique, 'value' => 'value', 'text' => 'text', 'selected_value' => $selected['unique']]).'</select>
		<input type="hidden" name="page" value="admin/translate/check_descriptions" />
		<input type="submit" name="submit" value="�����" />
	</form>
	<a href="/?page=admin/execute&function=text_ru_get_pending_results&q=10" class="btn" style="float: right;">��������� ����������</a>
	<a href="/?page=admin/execute&function=text_ru_check_video_descriptions&q=10'.$pages['booked_domain'].$pages['description_translater'].'" class="btn btn-primary" style="float: right;">��������� ��� ��������</a>
	</div>
	<table class="table">'.$list.'</table>
	<div class="pages">
	<ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/check_descriptions'.$pages['booked_domain'].$pages['description_translater'].$pages['unique'].'&amp;p=').'</ul><hr/>
	</div><hr/>';

    return $content;
}

// ���������� ������� � ����
function submit_translate_descriptions()
{
    if (empty($_POST['videos']) == 1) {
        error('�� �������� ��������');
    }
    $q_titles = 0;
    $q_all = 0;
    $content = '';
    foreach ($_POST['videos'] as $k => $v) {
        if (empty($v['description']) == 0) {
            $data = query('select title_ru,description,description_translater,unix_timestamp(description_translate_date) as desc_trans_date
				from '.tabname('videohub', 'videos').' where id=\''.$k.'\';');
            $arr = mysql_fetch_assoc($data);
            if (! empty($arr['description'])) {
                $content .= '�������� � ����� <b>'.$arr['title_ru'].'</b>
				��� ��������� ������������� <b>'.$arr['description_translater'].'</b>
				('.date('d.m.Y H:i', $arr['desc_trans_date']).')<br/>';

                continue;
            }
            $q_titles++;
            $v['description'] = trim($v['description']);
            query('update '.tabname('videohub', 'videos').' set description=\''.$v['description'].'\', description_translater=\''.auth_login(['type' => 'videohub']).'\', description_translate_date=current_timestamp where id=\''.$k.'\'');
        }
    }
    // ���������� ��� �������
    reset_cache('preparing_get_translate_balance');
    $content .= '���������:</b>
	<br/>���������� �����: '.$q_titles.'
	<br/><a href="/?page=admin/translate/descriptions&amp;domain='.$_POST['domain'].'">��������� � ��������</a>';

    return $content;
}

//
function translate_translated()
{
    if (isset($_GET['p']) == 0) {
        $_GET['p'] = 1;
    }
    $data = query('select value from '.tabname('videohub', 'config').' where property=\'translate_titles_per_page\'');
    $arr = mysql_fetch_assoc($data);
    $per_page = $arr['value']; // ���������� �� ��������
    $from = ($_GET['p'] - 1) * $per_page;
    //
    $clause['order'] = '';
    $clause['domain'] = '';
    if (empty($_GET['order']) == 1) {
        $_GET['order'] = 'translate_date';
    }
    if (empty($_GET['order_direction']) == 1) {
        $_GET['order_direction'] = 'desc';
    }
    if (empty($_GET['domain']) == 1) {
        $_GET['domain'] = '';
    }
    if (empty($_GET['order']) == 0) {
        $clause['order'] = 'order by '.$_GET['order'].' '.$_GET['order_direction'];
    }
    if (empty($_GET['domain']) == 0) {
        $clause['domain'] = 'and (domain=\''.$_GET['domain'].'\')';
    }
    //
    $data = query('
		select SQL_CALC_FOUND_ROWS id,translater,title_en,title_ru,description from '.tabname('videohub', 'videos').'
		where title_ru is not null and title_ru!=\'\'
		'.$clause['domain'].'
		'.$clause['order'].'
		limit '.$from.','.$per_page
    );
    $rows = rows_without_limit();
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {

        $list .= '<tr>
		<td><a href="/?page=admin/translate/view_video&amp;id='.$v['id'].'" target="_blank"><img src="/types/videohub/template/images/player_play.png" width="20" height="20" alt="" /></a></td>
		<td>'.$v['title_en'].'</td>
		<td><input type="text" name="videos['.$v['id'].'][title_ru]" value="'.$v['title_ru'].'" onfocus="javascript:LoadImage(\''.$v['id'].'\');" /></td>
		<td><textarea rows="3" cols="50" name="videos['.$v['id'].'][description]" onfocus="javascript:LoadImage(\''.$v['id'].'\');">'.$v['description'].'</textarea></td>
		</tr>';

        // $list.='<tr><td>'.($v['id']).'</td><td>'.$v['translater'].'</td><td>'.$v['title_en'].'</td><td>'.$v['title_ru'].'</td></tr>'."\r\n";
    }
    $data = query('select domain from '.tabname('engine', 'domains').' where type=\'videohub\' order by domain');
    $arr = readdata($data, 'nokey');
    $list_domains = '';
    foreach ($arr as $v) {
        $selected = '';
        if ($v['domain'] == $_GET['domain']) {
            $selected = 'selected';
        }
        $list_domains .= '<option value="'.$v['domain'].'"" '.$selected.'>'.$v['domain'].'</option>';
    }
    $content = '<form method="get">
	<select name="domain">
	<option value="">���</option>
	'.$list_domains.'
	</select>
	<input type="hidden" name="page" value="admin/translate/translated" />
	<input type="submit" name="submit" value="OK" />
	</form>
	<div class="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=admin/translate/translated&amp;domain='.$_GET['domain'].'&amp;order='.$_GET['order'].'&order_direction='.$_GET['order_direction'].'&amp;p=').'</ul><hr/></div><hr/>
	<form method="post" action="?page=admin/translate/submit_edit_translated">
	<table>
	<tr><td colspan="4" align="center" style="border: 0;"><img id="ImagePlaceHolder" src="" alt="" /></td></tr>
	<tr style="font-weight: bold; text-align: center;"><td></td><td></td><td>���������</td><td>��������</td></tr>
	'.$list.'
	<tr><td colspan="4" align="center" style="border: 0;"><input type="submit" name="submit" value="���������" /></td></tr>
	</table>
	</form>';

    return $content;
}

function translate_stats()
{
    $data = query('
		    SELECT login, info, paid_symbols, sum( length( videos.title_ru ) ) AS q_symbols, count( title_ru ) AS q_titles,
		    (sum( length( videos.title_ru ) ) - paid_symbols) AS not_paid_symbols,
		    round( (sum( length( videos.title_ru ) ) - paid_symbols ) * (
		    SELECT value
		    FROM '.tabname('videohub', 'config').'
		    WHERE property = \'translate_title_one_symbol_cost\' ) , 2) AS balance,
		    (select sum(length(tags_translation.tag_ru))*0.003 from '.tabname('videohub', 'tags_translation').'
		    where tags_translation.translater=users.login) as balance_tags
		    FROM '.tabname('videohub', 'users').'
		    JOIN '.tabname('videohub', 'videos').' ON videos.translater = users.login
		    WHERE LEVEL in (\'translater\',\'admin\',\'root\',\'translater_eng\',\'worker\')
		    GROUP BY login
		');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<tr><td>'.$v['login'].'</td><td>'.$v['q_symbols'].'</td><td>'.$v['q_titles'].'</td><td>'.$v['paid_symbols'].'</td><td>'.$v['not_paid_symbols'].'</td><td>'.$v['balance'].'</td><td>'.$v['balance_tags'].'</td><td>'.nl2br($v['info']).'</td></tr>'."\r\n";
    }
    $content = '<table class="table table-striped"><tr><td><b>�����</b></td><td><b>��������</b></td><td><b>����������</b></td><td><b>�������� ��</b></td><td><b>�� ��������</b></td><td><b>���������</b></td><td><b>����</b></td><td><b>��������</b></td></tr>'.$list.'</table>';

    return $content;
}

function translate_pay()
{
    /*
    $data=query('
            SELECT login, round( (sum( length( videos.title_ru ) ) - paid_symbols ) * (
            SELECT value
            FROM '.tabname('videohub','config').'
            WHERE property = \'translate_title_one_symbol_cost\' ) , 2) AS balance
            FROM '.tabname('videohub','users').'
            JOIN '.tabname('videohub','videos').' ON videos.translater = users.login
            WHERE LEVEL = \'translater\'
            OR LEVEL = \'admin\'
            OR LEVEL = \'root\'
            GROUP BY login
        ');
    $arr=readdata($data,'nokey');
    */
    $arr = get_translate_balance();
    $list = '';
    foreach ($arr as $login => $v) {
        $list .= '
		<tr><td>'.($login).'</td>
		<td>'.$v['common'].'</td>
		<td><input type="text" name="sum['.$login.']" style="width: 40px;" /></td>
		</tr>
		';
    }
    $content = '<form method="post" action="?page=admin/translate/submit_pay">
	<table class="table table-striped" style="width: 400px;"><tr><td><b>�����</b></td><td><b>������</b></td><td><b>�����</b></td></tr>'.$list.'</table>
	<input type="submit" name="submit" value="��������" />
	</form>';

    return $content;
}

function translate_submit_pay()
{
    if (empty($_POST['sum']) == 1) {
        error('�� ������� ������ �����');
    }
    foreach ($_POST['sum'] as $login => $money) {
        if (empty($money) == 0) {
            if (is_numeric($money) == 0) {
                error('��� ������������ '.$login.' �������� �� �����');
            }
            query('update '.tabname('videohub', 'users').' SET paid_symbols = ( paid_symbols +'.$money.' /
				ifnull(title_cost,
			(SELECT value
				FROM '.tabname('videohub', 'config').'
				WHERE property = \'translate_title_one_symbol_cost\' )
			))
			WHERE login = \''.$login.'\'
			;');
            logger()->info('�������� '.$login.' '.$money.' ���');
        }
    }
    // ���������� ��� �������
    reset_cache('preparing_get_translate_balance');
    alert('������ ������');
}
