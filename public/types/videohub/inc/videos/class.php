<?php

namespace MegawebV1;

// pre('test');

class video
{
    public function __construct($args = [])
    {
        $id = empty($args['id']) ? error('ID required to create video class') : $args['id'];
        $domain = empty($args['domain']) ? error('Domain is required to create video class') : $args['domain'];
        $arr = query_assoc('select id,domain,hub,id_hub,add_date,apply_date,title_en,title_ru,title_ru_alt,
			title_ru_translit,translater,translate_date,description,description_translater,description_translate_date,
			ph_viewkey,ph_date,booked_domain,deleted,source_url,local_id,deleted_from_hub
		 from '.typetab('videos').' where id=\''.$id.'\' and domain=\''.$domain.'\';');
        if (! $arr) {
            error('Video doesn\'t exist');
        }
        $this->id = $arr['id'];
        $this->domain = $arr['domain'];
        $this->hub = $arr['hub'];
        $this->id_hub = $arr['id_hub'];
        $this->add_date = $arr['add_date'];
        $this->apply_date = $arr['apply_date'];
        $this->title_en = $arr['title_en'];
        $this->title_ru = $arr['title_ru'];
        $this->title = $arr['title_ru'];
        $this->title_alt = $arr['title_ru_alt'];
        $this->title_translit = $arr['title_ru_translit'];
        $this->title_writer = $arr['translater'];
        $this->title_date = $arr['translate_date'];
        $this->description = $arr['description'];
        $this->description_writer = $arr['description_translater'];
        $this->description_date = $arr['description_translate_date'];
        $this->ph_viewkey = $arr['ph_viewkey'];
        $this->ph_date = $arr['ph_date'];
        $this->booked_domain = $arr['booked_domain'];
        $this->is_deleted = $arr['deleted'];
        $this->source_url = $arr['source_url'];
        $this->local_id = $arr['local_id'];
        $this->deleted_from_hub = $arr['deleted_from_hub'];
    }

    public function getCategories()
    {
        return preparing_categories_of_video(['video_id' => $this->id, 'stars' => 0]);
    }

    public function getStars()
    {
        return preparing_categories_of_video(['video_id' => $this->id, 'stars' => 1]);
    }

    public function getTags()
    {
        return preparing_video_tags_list_array(['id' => $this->id]);
    }

    public function getSame($args = [])
    {
        $sort = empty($args['sort']) ? 'mr' : $args['sort'];
        $limit = empty($args['limit']) ? 20 : $args['limit'];
        $sort_sql = [
            'mv' => 'views desc',
            'mr' => 'apply_date desc',
            'tr' => 'rating desc, votes desc',
            'ms' => 'same_tags_q desc, rating_sko desc',
            'mc' => 'q_comments desc',
        ];
        $tags = query_arr('select tags_translation.tag_ru
			from '.typetab('tags').'
			join '.typetab('tags_translation').' ON tags.tag_en = tags_translation.tag_en
			where tags.video_id = \''.$this->id.'\'
			');
        // ��������� ������ �����
        $video_tags = [];
        foreach ($tags as $v) {
            $video_tags[] = $v['tag_ru'];
        }
        $arr = query_arr('select SQL_CALC_FOUND_ROWS videos.id
			from '.typetab('tags').'
			join '.typetab('videos').' on videos.id=tags.video_id
			left join '.typetab('videos_views').' on tags.video_id=videos_views.video_id
			where domain=\''.$this->domain.'\' and tags.tag_ru in ('.array_to_str($video_tags).') and tags.video_id!=\''.$this->id.'\'
			and deleted_from_hub=\'no\'
			group by id
			order by '.$sort_sql[$sort].'
			limit '.$limit.'
			');
        $object = new stdClass;
        $object->rows = rows_without_limit();
        $object->array = $arr;

        return $object;
    }
}

class category
{
    public function __construct($args = [])
    {
        $id = empty($args['id']) ? error('ID required to create category class') : $args['id'];
        $domain = empty($args['domain']) ? error('Domain is required to create category class') : $args['domain'];
        $arr = query_assoc('select id,domain,category_id,text,text_2,title,h1,img,keywords,description,
			photo_title,photo_description,photo_h1,photo_text,photo_img,title_translit,active,h2
			from '.typetab('categories_domains').' where id=\''.$id.'\' and domain=\''.$domain.'\';');
        if (! $arr) {
            error('Category doesn\'t exist');
        }
        $this->id = $arr['id'];
        $this->domain = $arr['domain'];
        $this->category_id = $arr['category_id'];
        $this->text = $arr['text'];
        $this->text_2 = $arr['text_2'];
        $this->title = $arr['title'];
        $this->h1 = $arr['h1'];
        $this->img = $arr['img'];
        $this->keywords = $arr['keywords'];
        $this->description = $arr['description'];
        $this->photo_title = $arr['photo_title'];
        $this->photo_description = $arr['photo_description'];
        $this->photo_h1 = $arr['photo_h1'];
        $this->photo_text = $arr['photo_text'];
        $this->photo_img = $arr['photo_img'];
        $this->title_translit = $arr['title_translit'];
        $this->active = $arr['active'];
        $this->h2 = $arr['h2'];
    }

    public function getVideos($args = [])
    {
        $page = empty($args['page']) ? error('Page number is required') : $args['page'];
        $sort = empty($args['sort']) ? error('Sort is required') : $args['sort'];
        $limit = empty($args['limit']) ? error('Limit is required') : $args['limit'];
        $from = ($page - 1) * $limit;
        $sort_sql = [
            'mv' => 'views desc',
            'mr' => 'apply_date desc',
            'tr' => 'rating desc, votes desc',
            'ms' => 'same_tags_q desc, rating_sko desc',
            'mc' => 'q_comments desc',
        ];
        if (! isset($sort_sql[$sort])) {
            error('���������� �� ����������');
        }
        $arr = query_arr('
			select catgroups_tags.tag_ru, categories_groups.name_translit
			FROM '.typetab('categories_domains').'
			JOIN  '.typetab('categories_groups').' ON categories_groups.id = categories_domains.category_id
			JOIN  '.typetab('catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
			WHERE categories_domains.id=\''.$this->id.'\' and categories_domains.domain=\''.$this->domain.'\' and categories_domains.active=\'1\'
			');
        // ��������� ������ ������ �����
        $cat_tags = [];
        foreach ($arr as $v) {
            $cat_tags[] = $v['tag_ru'];
        }
        if (empty($cat_tags)) {
            error('��������� ����������', '��� ��������� ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
        }
        // ������ �� ��������� ���� ���������� ����� �����, ��������������� �������
        $arr = query_arr('
			select SQL_CALC_FOUND_ROWS videos.id
			FROM '.typetab('tags').'
			inner join '.typetab('videos').' on videos.id=tags.video_id
			left join '.typetab('videos_views').' on tags.video_id=videos_views.video_id
			WHERE domain=\''.$this->domain.'\' and tags.tag_ru in ('.array_to_str($cat_tags).')
			group by videos.id
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$limit
        );
        $object = new stdClass;
        $object->rows = rows_without_limit();
        $object->array = $arr;

        return $object;
    }

    public function getString()
    {
        return 'String Test';
    }
}

function test_class()
{
    return pre_return(objcache(['class' => 'category', 'class_args' => ['id' => 4895, 'domain' => 'turboporno.info'], 'method' => 'getString']));

    // $videos=objcache(array('class'=>'category','class_args'=>array('id'=>4895,'domain'=>'turboporno.info'),'method'=>'getVideos','method_args'=>array('page'=>1,'sort'=>'mr','limit'=>100)));
    // foreach ($videos->array as $v) {
    // 	$arr[]=objcache(array('class'=>'video','class_args'=>array('id'=>$v['id'],'domain'=>'turboporno.info')));
    // };
    // pre($arr);
    // $category=new category(array('id'=>4895,'domain'=>'turboporno.info'));
    // pre($category->getVideos(array('page'=>1,'sort'=>'mr','limit'=>100)));
    // $video=new video(array('id'=>331132));
    // $return=objcache(array('class'=>'video','class_args'=>array('id'=>331132,'domain'=>'pornostars-tube.com'),'method'=>'getSame','method_args'=>array('limit'=>1000)));
    // $return=objcache(array('class'=>'video','class_args'=>array('id'=>331132),'method'=>'getTags'));
    // $return=objcache(array('class'=>'video','class_args'=>array('id'=>331132)));
    return pre_return($return);
    // pre(get_object_vars($video));
    // pre(get_class($video));
    // foreach ($video->getSame()->array as $v) {
    // 	$video2=new video($v['id']);
    // 	$videos[]=$video2->getSame();
    // };
    // pre($videos);
    // new Cache()
    // $arr=cache('preparing_categories_list',$args,43200);
    // $arr=cache('video','getCategories',$args,43200);
    // $cache= new cache($video->getSame());
    // pre($video->getSame());
}
