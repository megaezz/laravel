<?php

namespace MegawebV1;

function media_for_video($id, $action)
{
    if (isset($id) == 0) {
        error('�� ����� ID');
    }
    $data = query('select hub,id_hub,title_ru_translit,ph_viewkey,ph_date,local_id from '.tabname('videohub', 'videos').' where id=\''.$id.'\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr)) {
        error('����� �� ����������', '����� '.$id.' �� ����������', 1, 404);
    }
    $id_hub = $arr['id_hub'];
    $hub = $arr['hub'];
    if (empty($arr['ph_viewkey'])) {
        $ph_viewkey = 'no';
    } else {
        $ph_viewkey = $arr['ph_viewkey'];
    }
    if (empty($arr['ph_date'])) {
        $ph_date = 'no';
    } else {
        $ph_date = $arr['ph_date'];
    }
    if ($action == 'video_save_redirect_link') {
        return 'http://save.awmengine.net/?hub='.$arr['hub'].'&id_hub='.$arr['id_hub'].'&videokey='.$_GET['videokey'];
    }
    if ($action == 'video_image_link') {
        return '/types/videohub/images/'.$id.'.jpg';
    }
    if ($action == 'video_watch_link') {
        return '/video/'.$id.'-'.$arr['title_ru_translit'];
    }
    if ($action == 'video_save_link') {
        $data = query('select value from '.tabname('videohub', 'config').' where property=\'videokey_current\'');
        $arr2 = mysql_fetch_assoc($data);
        if (empty($arr2['value'])) {
            error('����������� ��� ��� ������');
        }
        temp_var('id', $id);
        temp_var('local_id', $arr['local_id']);
        temp_var('videokey', $arr2['value']);
        temp_var('title_ru_translit', $arr['title_ru_translit']);

        return template('li/video_save_link');
        // return '/save/'.$arr2['value'].'/'.$id.'-'.$arr['title_ru_translit'];
    }
    if ($action == 'video_load_link') {
        return '/load/'.$id.'-'.$arr['title_ru_translit'];
    }
    if ($hub == 'pornhub.com') {
        return media_from_pornhub($id_hub, $action, $ph_viewkey, $ph_date);
    }
    if ($hub == 'hardsextube.com') {
        return media_from_hardsextube($id_hub, $action);
    }
    if ($hub == 'xvideos.com') {
        return media_from_xvideos($id_hub, $action);
    }
    if ($hub == 'paradisehill.tv') {
        return media_from_paradisehill($id_hub, $action);
    }
    error('���������� �������� �����, ��� '.$hub.' �� ��������������.');
}

function preparing_working_same_video($args)
{
    global $config;
    if (empty($args['video_id'])) {
        error('�� ������� ID �����');
    }
    $videos_same = videos_list_same_arr($args['video_id']);
    $i = 0;
    foreach ($videos_same['videos'] as $v) {
        $i++;
        $link_same = video_direct_link_by_id($v['id'], 'default');
        if ($link_same == 'unavailable_video') {
            // logs_alt('������� ����� ��� '.$_GET['id'].' ('.$v['id'].')('.$i.') ���� �� ��������',array('type'=>'direct_video'));
        } else {
            // logs_alt('������� ����� ��� '.$_GET['id'].' ('.$v['id'].')('.$i.') ��������',array('type'=>'direct_video'));
            // $link=$link_same;
            return $v['id'];
        }
        if ($i > 10) {
            break;
        }
    }

    return false;
}

function working_same_video($video_id)
{
    return cache('preparing_working_same_video', ['video_id' => $video_id], 43200);
}

function video_direct_link($ext = 'default')
{
    if (empty($_GET['id'])) {
        error('�� ������� ID �����');
    }
    $args['video_id'] = $_GET['id'];
    $args['ext'] = $ext;

    // 900 �����
    return cache('preparing_video_direct_link', $args, 1200);
}

function video_direct_link_by_id($video_id, $ext = 'default')
{
    $args['video_id'] = $video_id;
    $args['ext'] = $ext;

    return cache('preparing_video_direct_link', $args, 1200);
}

// ����� ������ ������ �� �������� �����
function preparing_video_direct_link($args)
{
    global $config;
    // pre($args);
    if (isset($args['video_id']) == 0) {
        error('�� ����� ID');
    }
    if (! isset($args['ext'])) {
        error('�� ������ ����������');
    }
    if (! isset($args['check_domain'])) {
        $args['check_domain'] = 'yes';
    }
    $param = 'video_direct_link';
    if ($args['ext'] == 'default') {
        $param = 'video_direct_link';
    }
    if ($args['ext'] == 'mp4') {
        $param = 'video_direct_link';
    }
    if ($args['ext'] == 'flv') {
        $param = 'video_direct_link_flv';
    }
    if ($args['check_domain'] == 'yes') {
        $data = query('select count(*) from '.tabname('videohub', 'videos').' where domain=\''.$config['domain'].'\' and id=\''.$args['video_id'].'\'');
        $arr = mysql_fetch_array($data);
        if ($arr[0] == 0) {
            error('����� �� �������', '����� '.$args['video_id'].' ��� ����� '.$config['domain'].' �� �������', 1, 404);
        }
    }
    $answer = media_for_video($args['video_id'], $param);
    if (empty($answer)) {
        $answer = 'unavailable_video';
    }

    /*
    if ($answer=='unavailable_video') {
        downloads_error_count($args['video_id']);
    } else {
        downloads_ok_count($args['video_id']);
    };
    */
    return $answer;
}

// �������� �� ������ ������ ��� �������� �����
function redirect_save_video()
{
    global $config;
    if (isset($_GET['videokey']) == 0) {
        error('�� ����� VIDEOKEY');
    }
    check_videokey($_GET['videokey']);
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID');
    }
    $data = query('select count(*) from '.tabname('videohub', 'videos').' where domain=\''.$config['domain'].'\' and id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_array($data);
    if ($arr[0] == 0) {
        error('����� �� �������', '����� '.$_GET['id'].' ��� ����� '.$config['domain'].' �� �������', 1, 404);
    }
    $start = 0;
    if (empty($_GET['start']) == 0) {
        $start = $_GET['start'];
    }
    $flv = direct_save_video_link();
    if ($flv == 'unavailable_video') {
        $flv = 'http://cdn1.static.pornhub.phncdn.com/flash/novideo.flv';
    }
    $parse = parse_url($flv);
    if (empty($parse['query'])) {
        $symbol = '?';
    } else {
        $symbol = '&';
    }
    // print_r($parse);
    // die(' ');
    header('Location: '.$flv.$symbol.'start='.$start);
}

// �������� �� awmengine ��� ��������� �� ������ ������
function redirect_to_awmengine()
{
    global $config;
    // if (empty($_GET['videokey'])==1) {error('�� �������� �������� ������');};
    // check_videokey($_GET['videokey']);
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID');
    }
    // $download_domain='awmengine.net';
    $download_domain = 'awmzone.net';
    $data = query('select count(*) as q,id,hub,saved,ext,subdomain from '.tabname('videohub', 'videos').'
		where domain=\''.$config['domain'].'\' and id=\''.$_GET['id'].'\'
		group by id');
    $arr = mysql_fetch_assoc($data);
    if ($arr['q'] == 0) {
        error('����� �� �������', '����� '.$_GET['id'].' ��� ����� '.$config['domain'].' �� �������', 1, 404);
    }
    $query = '';
    $parse_url = parse_url($_SERVER['REQUEST_URI']);
    /*
    if ($arr['hub']=='paradisehill.tv') {
        if ($arr['saved']=='yes') {
            if (empty($parse_url['query'])==0) {$query='?'.$parse_url['query'];};
            header('Location: http://'.$arr['subdomain'].'.'.$download_domain.'/'.$arr['id'].'.'.$arr['ext'].$query);
            die();
        } else {
            alert('��������, ���� ����� ������ ���������');
        };
    };
    */
    if (redirect_to_video_direct_link() == 'unavailable_video') {
        // logs_alt('�� ���� ������� ����� �� ������� ��� '.$_GET['id'],array('type'=>'direct_video'));
        alert('��������, ���� ����� ������ ���������');
    }
    // if (empty($parse_url['query'])==0) {$query='&'.$parse_url['query'];};
    // header('Location: '.media_for_video($_GET['id'],'video_save_redirect_link').$query);
}

function redirect_to_video_direct_link_flv()
{
    return redirect_to_video_direct_link('flv');
}

function redirect_to_video_direct_link($ext = 'default')
{
    if (empty($_GET['id'])) {
        error('�� ����� ID');
    }
    if (empty($_GET['start'])) {
        $start = 0;
    } else {
        $start = $_GET['start'];
    }
    $hub = video_hub();
    $streaming_param = 'start';
    if ($hub == 'xvideos.com') {
        if ($ext == 'flv') {
            $streaming_param = 'fs';
        }
    }
    if ($start == 0) {
        $start_str = '';
    } else {
        $start_str = '&'.$streaming_param.'='.$start;
    }
    $link = video_direct_link($ext);
    if ($link == 'unavailable_video') {
        $video_same = working_same_video($_GET['id']);
        if ($video_same) {
            $link = video_direct_link_by_id($video_same, 'default');
        }
    }
    // pre($link);
    if ($link != 'unavailable_video') {
        header('Location: '.$link.$start_str);
        exit();
    }

    return $link;
}
