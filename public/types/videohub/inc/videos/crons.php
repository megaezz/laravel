<?php

namespace MegawebV1;

function check_pornhub_availability()
{
    $url = 'http://www.pornhub.com';
    // $url=$_GET['url'];
    $status = check_site_availability($url);
    set_config('videohub', 'pornhub_availability', $status ? 1 : 0);
    if ($status) {
        logger()->info('Pornhub ��������');
    } else {
        logger()->info('Pornhub �� ��������');
    }
}

function videos_same_link_percent()
{
    $data = query('
		select round((
			(select count(*) from '.tabname('engine', 'cache').' where function=\'preparing_working_same_video\')
			/
			(select count(*) from '.tabname('engine', 'cache').' where function=\'preparing_video_direct_link\')
			)*100,0) as same_link_percent;
	');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['same_link_percent'])) {
        $arr['same_link_percent'] = 0;
    }
    set_config('videohub', 'video_same_link_percent', $arr['same_link_percent']);
}

function video_direct_links_errors_percent()
{
    $data = query('
		select round((
(select count(*) from '.tabname('engine', 'cache').'
 where function=\'preparing_video_direct_link\' and
 date between (current_timestamp-interval 5 minute) and current_timestamp and
 result like \'%unavailable_video%\')
/
(select count(*) from '.tabname('engine', 'cache').'
	where function=\'preparing_video_direct_link\' and
	date between (current_timestamp-interval 5 minute) and current_timestamp)
)*100,0) as errors_percent;
		');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['errors_percent'])) {
        $arr['errors_percent'] = 0;
    }
    // pre($arr);
    set_config('videohub', 'video_direct_link_errors_percent', $arr['errors_percent']);
    // query('insert into '.tabname('videohub','config').' set property=\'video_direct_link_errors_percent\',value=\''.$arr['errors_percent'].'\'
    // 	on duplicate key update value=\''.$arr['errors_percent'].'\';');
    logger()->info('��������� ������� ������ ������ ������. Errors_percent: '.$arr['errors_percent']);
}

// �������� ��������� � logs
function set_deleted_from_hub_videos()
{
    query('update '.tabname('videohub', 'videos').'
		set deleted_from_hub=\'yes\'
		where id in (
			select video_id from '.tabname('videohub', 'videos_views').'
			where downloads_ok=0 and downloads_error>2
			);');
    $affected1 = mysql_affected_rows();
    query('update '.tabname('videohub', 'videos').'
		join '.tabname('videohub', 'videos_views').' on videos_views.video_id=videos.id
		set deleted_from_hub=\'no\'
		where videos.deleted_from_hub=\'yes\' and videos_views.downloads_ok>0;');
    $affected2 = mysql_affected_rows();
    logger()->info('�������� �����, � ���-��� ��������� ��������>2 � ������� ���-��� �������. ��������� �����: '.$affected1.'.
		������ ������� � �����, �� ������� ��������� ������� ��������. ���������: '.$affected2.'.');
}

// ���������� ������� ����� ����� � �� tags. ������� ����������� � xrest
function cron_translate_tags()
{
    query('update '.tabname('videohub', 'tags').'
		join '.tabname('videohub', 'videos').' on videos.id=tags.video_id
		left join '.tabname('videohub', 'tags_translation').' vh_tt on vh_tt.tag_en=tags.tag_en
		left join '.tabname('xrest_net_changed', 'tags_translation').' xrest_tt on xrest_tt.tag_en=tags.tag_en
		set tags.tag_ru=(case videos.domain when \'xrest.net\' then xrest_tt.tag_ru else vh_tt.tag_ru end)
		where tags.tag_ru is null or tags.tag_ru!=(case videos.domain when \'xrest.net\' then xrest_tt.tag_ru else vh_tt.tag_ru end)
		;');
    $affected = mysql_affected_rows();
    $return = '�������� ����. ��������� �����: '.$affected;
    logger()->info($return);
    // return $return;
}

function backup_videos_views()
{
    $data = query('select count(*) as q from '.tabname('videohub', 'videos_views').';');
    $arr = mysql_fetch_assoc($data);
    $q['videos_views'] = $arr['q'];
    $data = query('select count(*) as q from '.tabname('videohub', 'videos_views_copy').';');
    $arr = mysql_fetch_assoc($data);
    $q['videos_views_copy'] = $arr['q'];
    if ($q['videos_views'] >= $q['videos_views_copy']) {
        query('delete from '.tabname('videohub', 'videos_views_copy').';');
        query('insert into '.tabname('videohub', 'videos_views_copy').' (select * from '.tabname('videohub', 'videos_views').');');
        logger()->info('���������� ����� ������� videos_views');
    }
    /* � ���� ���������� ������� �� video_views ������?
    if ($q['videos_views']<$q['videos_views_copy']) {
        query('delete from '.tabname('videohub','videos_views').';');
        query('insert into '.tabname('videohub','videos_views').' (select * from '.tabname('videohub','videos_views_copy').');');
        logger()->info('����������� �������������� ������� videos_views');
    };
    */
}

// �������� ������ �� ��� �����
function add_videos()
{
    // query('update '.tabname('engine','config').' set value=\'off\' where property=\'status\';');
    switch_engine('off');
    logger()->info('������� ���������� ����� �����.');
    //
    $data = query('
		select domains.domain,
		ifnull((select value from '.tabname('videohub', 'config_for_domain').' where property=\'number_of_videos_per_update\' and domain=domains.domain),
			(select value from '.tabname('videohub', 'config').' where property=\'number_of_videos_per_update\')
			) as videos_per_update
	from '.tabname('engine', 'domains').'
	where type=\'videohub\' and status=\'on\'
	having videos_per_update!=0;
	');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        add_videos_on_domain($v['domain'], $v['videos_per_update']);
    }
    //
    logger()->info('������� ���������� ����� ��������.');
    switch_engine('on');
    // query('update '.tabname('engine','config').' set value=\'on\' where property=\'status\';');
}

// ������������� ����� ���� ���������� � �������� ��� � ��������� ������� � �� �����
function change_videokey()
{
    $rand = generate_code();
    // query('update '.tabname('videohub','config').' set value=md5('.$rand.') where property=\'videokey\'');
    $data = query('select property,value from '.tabname('videohub', 'config').' where property=\'videokey_current\'');
    $arr = readdata($data, 'property');
    $current = $arr['videokey_current']['value'];
    query('update '.tabname('videohub', 'config').' set value=\''.$current.'\' where property=\'videokey_last\'');
    query('update '.tabname('videohub', 'config').' set value=\''.$rand.'\' where property=\'videokey_current\'');
    @file_get_contents('http://save.awmengine.net/?page=api/set_new_videokey&videokey='.$rand);
}
