<?php

namespace MegawebV1;

// ������� ������� ������ ��������������
function copy_config_to_alt()
{
    $arr = query_arr('select domain,property,value from '.typetab('config_for_domain').';');
    foreach ($arr as $v) {
        query('update '.typetab('config_for_domain_alt').'
			set '.$v['property'].'='.$v['value'].' where domain='.$v['domain'].'
			;');
    }
}

function adframe_or_our()
{
    $geo_country = get_geoip_country_code(request()->ip());
    if (isset($_GET['country'])) {
        $geo_country = $_GET['country'];
    }
    $file = 'promo/adframe/player_ad';
    if (($geo_country == 'RU')) {
        $file = random_event(['promo/adframe/player_ad' => '50', 'promo/our/player_ad' => '50']);
    }

    return template($file);
}

/*
function stars_in_video() {
    if (empty($_GET['id'])) {error('�� ������� ID');};
    $data=query('select stars.name,stars.name_ru from '.tabname('videohub','tags').'
        join '.tabname('videohub','stars').' on stars.name=tags.tag_en
        where video_id=\''.$_GET['id'].'\';');
    $arr=readdata($data,'nokey');
    $list='';
    foreach ($arr as $v) {
        temp_var('star_name',$v['name_ru']);
        temp_var('star_name_en',$v['name']);
        temp_var('star_name_url',without_spaces($v['name_ru']));
        $list.=template('li/item_stars_in_video');
    };
    return $list;
}
*/

function serials_list()
{
    $args['type'] = 'serial';
    $args['mobile'] = 'no';
    $args['template'] = 'li/item_serials_list';
    $args['show_empty'] = 1;

    return categories_list_new($args);
}

function get_tag_ru_by_tag_en($tag_en)
{
    $arr = mysql_fetch_assoc(query('select tag_en,tag_ru from '.tabname('videohub', 'tags_translation').' where tag_en=\''.$tag_en.'\';'));
    if (empty($arr['tag_en'])) {
        error('��� �� ������');
    } // ���� ����� tag_ru ������ ������, �� ��������� �� tag_en

    return $arr['tag_ru'];
}

function get_star_id_by_name_en($name_en)
{
    $arr = mysql_fetch_assoc(query('select id from '.tabname('videohub', 'stars').' where name=\''.$name_en.'\';'));
    if (empty($arr['id'])) {
        error('������ �� �������');
    }

    return $arr['id'];
}

function get_star_id_by_name_ru($name_ru)
{
    $arr = mysql_fetch_assoc(query('select id from '.tabname('videohub', 'stars').' where name_ru=\''.$name_ru.'\';'));
    if (empty($arr['id'])) {
        error('������ �� �������');
    }

    return $arr['id'];
}

function preparing_star_info($args)
{
    if (empty($args['category_id'])) {
        error('�� ������� ID');
    }
    $data = query('
		select stars.id,stars.name,stars.name_ru,stars.aka,stars.pic,stars.age,stars.country,stars.region,
		stars.city,stars.height,stars.weight,stars.gender as gender_en,stars.ethnicity as ethnicity_en,
		stars.hair as hair_en,stars.country_en,stars.region_en,stars.city_en,
		case stars.gender when \'woman\' then \'�������\' end as gender,
		case stars.ethnicity
		when \'white\' then \'���������\'
		when \'indian\' then \'��������\'
		when \'asian\' then \'�������\'
		when \'mixed\' then \'���������\'
		when \'latina\' then \'������\'
		when \'black\' then \'����������\'
		when \'arab\' then \'������\'
		end as ethnicity,
		case stars.hair
		when \'brown\' then \'����������\'
		when \'black\' then \'������\'
		when \'blonde\' then \'�������\'
		when \'red\' then \'�����\'
		when \'white\' then \'�����\'
		end as hair
			from '.tabname('videohub', 'categories_domains').'
			join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
			join '.tabname('videohub', 'stars').' on stars.group_id=categories_groups.group_id
			where categories_domains.id=\''.$args['category_id'].'\';
		');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('������ �� ����������');
    }
    $arr['name_url'] = without_spaces($arr['name']);
    $arr['name_ru_url'] = without_spaces($arr['name_ru']);

    return $arr;
}

function preparing_stars_list($args = [])
{
    global $config;
    if (empty($args['domain'])) {
        $args['domain'] = $config['domain'];
    }
    $data = query('
		select stars.name as star_name_en,tags_translation.tag_ru as star_name,
		count(distinct videos.id) as q
		from '.tabname('videohub', 'tags').'
		join '.tabname('videohub', 'tags_translation').' on tags_translation.tag_en=tags.tag_en
		join '.tabname('videohub', 'videos').' on videos.id=tags.video_id
		join '.tabname('videohub', 'stars').' on stars.name=tags.tag_en
		where videos.domain=\''.$args['domain'].'\'
		group by stars.name
		order by star_name;
		');
    $arr = readdata($data, 'nokey');

    return $arr;
}

function stars_list()
{
    return categories_list_new($args = ['mobile' => 'no', 'template' => 'li/item_stars_list', 'type' => 'star', 'show_empty' => 0]);
}

function stars_list_with_pic()
{
    return categories_list_new($args = ['mobile' => 'no', 'template' => 'li/item_stars_list_with_pic', 'type' => 'star', 'show_empty' => 0]);
}

function stars_list_with_pic_with_empty()
{
    return categories_list_new($args = ['mobile' => 'no', 'template' => 'li/item_stars_list_with_pic', 'type' => 'star', 'show_empty' => 1]);
}

function stars_list_with_empty()
{
    return categories_list_new($args = ['mobile' => 'no', 'template' => 'li/item_stars_list', 'type' => 'star', 'show_empty' => 1]);
}

function categories_list_without_empty()
{
    return categories_list_new($args = ['mobile' => 'no', 'template' => 'li/categories_list', 'type' => 'default', 'show_empty' => 0]);
}

/*
function stars_list_old() {
    $args=array();
    $arr=cache('preparing_stars_list',$args);
    $list='';
    foreach ($arr as $v) {
        temp_var('star_name',$v['star_name']);
        temp_var('star_name_en',$v['star_name_en']);
        temp_var('star_name_en_url',without_spaces($v['star_name_en']));
        temp_var('star_name_url',without_spaces($v['star_name']));
        temp_var('rows',$v['q']);
        if (isset($_GET['tag_ru']) and $_GET['tag_ru']==$v['star_name']) {
            temp_var('class_active','class="active"');
        } else {
            temp_var('class_active','');
        };
        $list.=template('li/item_stars_list');
    };
    return $list;
}
*/

function check_tag_exist($tag_ru)
{
    $tag_ru = checkstr($tag_ru);
    $data = query('select 1 from '.tabname('videohub', 'tags_translation').' where tag_ru=\''.$tag_ru.'\';');
    $arr = mysql_fetch_assoc($data);
    if (! isset($arr[1]) or ($arr[1] != '1')) {
        error('��� �� ����������');
    }
}

function check_tag_en_exist($tag_en)
{
    $tag_en = checkstr($tag_en);
    $data = query('select 1 from '.tabname('videohub', 'tags').' where tag_en=\''.$tag_en.'\';');
    $arr = mysql_fetch_assoc($data);
    if ($arr[1] != '1') {
        error('��� �� ����������');
    }
}

function submit_likes()
{
    if (empty($_GET['type'])) {
        error('�� ������� ���');
    }
    $type = $_GET['type'];
    if ($type == 'video') {
        return submit_video_likes();
    }
    // if ($type=='gallery') {return submit_gallery_rating();};
    // if ($type=='story') {return submit_story_rating();};
    // if ($type=='game') {return submit_game_rating();};
}

function submit_video_likes()
{
    global $config;
    check_session_start();
    if (empty($_GET['video_id'])) {
        error('�� ������� ID �����');
    }
    if (empty($_GET['action'])) {
        error('�� �������� ��������');
    }
    $video_id = $_GET['video_id'];
    $action = $_GET['action'];
    if (isset($_SESSION['videohub']['likes']['videos'][$video_id])) {
        $session_action = $_SESSION['videohub']['likes']['videos'][$video_id];
        if ($session_action == 'like') {
            $like_text['ru'] = '����';
            $like_text['en'] = 'liked';
        }
        if ($session_action == 'dislike') {
            $like_text['ru'] = '�������';
            $like_text['en'] = 'disliked';
        }
        $return['ru'] = '�� ��� ��������� ����� ����� '.$like_text['ru'].'.';
        $return['en'] = 'You have already '.$like_text['en'].' this video.';

        return $return[$config['lang']];
    }
    if ($action == 'like') {
        $mysql['update'] = 'likes=likes+1';
        $mysql['likes']['insert'] = 1;
        $mysql['dislikes']['insert'] = 0;
    } else {
        if ($action == 'dislike') {
            $mysql['update'] = 'dislikes=dislikes+1';
            $mysql['likes']['insert'] = 0;
            $mysql['dislikes']['insert'] = 1;
        } else {
            error('�������� �������� ��������');
        }
    }
    query('update '.tabname('videohub', 'videos_views').'
		join '.tabname('videohub', 'videos').' on videos.id=videos_views.video_id
		set '.$mysql['update'].'
		where videos_views.video_id=\''.$video_id.'\' and videos.domain=\''.$config['domain'].'\'');
    if (mysql_affected_rows() == 0) {
        $data = query('select * from '.tabname('videohub', 'videos').' where id=\''.$video_id.'\' and domain=\''.$config['domain'].'\'');
        if (mysql_affected_rows() == 0) {
            error('����� �� �������', '����� '.$video_id.' �� ������� ��� ����� '.$config['domain'].' ������');
        }
        query('insert into '.tabname('videohub', 'videos_views').' (video_id,likes,dislikes) values (\''.$video_id.'\','.$mysql['likes']['insert'].','.$mysql['dislikes']['insert'].');');
    }
    $_SESSION['videohub']['likes']['videos'][$video_id] = $action;
    $args['video_id'] = $video_id;
    reset_cache('preparing_view_video', $args);
    // die('������ ���� ����');
    $arr = cache('preparing_view_video', $args);

    // pre($arr);
    return $arr['likes_sum'];
}

function preparing_direct_save_video_link($args)
{
    if (isset($args['id']) == 0) {
        error('�� ����� ID �����');
    }

    return media_for_video($args['id'], 'video_direct_link');
}

// ���������� ������ ������ �� ���������� �����
function direct_save_video_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }
    $args['id'] = $_GET['id'];

    return cache('preparing_direct_save_video_link', $args);
}

/*
function pages_videos_list_all() {
    global $config;
    $arr=cache('preparing_stories_list',$args);

    $arr=cache('pages_list',array('rows'=));
    $args['pages_length']=5;
    $args['pages_length']=value_from_config_for_domain('videohub','pages_length');
    $args['domain']=$config['domain'];
    $args['type']='all';
    $args['value']='value';
    $args['page']=(int)$_GET['p'];
    $args['per_page']=value_from_config_for_domain('videohub','stories_per_page');
    $args['sort']=$_GET['sort'];
    $arr=cache('preparing_stories_list',$args);
    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows=$arr['rows']; //����� �������
}

*/

// get_pages_list('videos_list_all')

function pages_list($page)
{
    global $config;
    if (empty($page)) {
        error('�� ������ ��������');
    }
    $args['domain'] = $config['domain'];
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    if (empty($_GET['p'])) {
        $args['page'] = 1;
    } else {
        $args['page'] = (int) $_GET['p'];
    }
    if ($page == 'videos_list_all') {
        $preparing = 'preparing_videos_list';
        if (empty($_GET['sort'])) {
            $args['sort'] = 'mr';
        } else {
            $args['sort'] = $_GET['sort'];
        }
        $args['type'] = 'all';
        $args['value'] = 'value';
        $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
        $args['link_start'] = value_from_config_for_domain('videohub', 'pages_videos_list_all_link_start');
        $args['link_end'] = value_from_config_for_domain('videohub', 'pages_videos_list_all_link_end');
        $args['a_inside_inactive'] = value_from_config_for_domain('videohub', 'pages_videos_list_all_a_inside_inactive');
        $args['current_class_name'] = value_from_config_for_domain('videohub', 'pages_videos_list_all_current_class_name');
        $args['points_class_name'] = value_from_config_for_domain('videohub', 'pages_videos_list_all_points_class_name');
        $args['mobile'] = 'no';
    }
    if ($page == 'videos_list_same') {
        $preparing = 'preparing_videos_list';
        $args['type'] = 'same';
        $args['value'] = $_GET['id'];
        $args['per_page'] = value_from_config_for_domain('videohub', 'number_of_same_videos');
        $args['mobile'] = 'no';
        $args['sort'] = 'ms';
    }
    if ($page == 'videos_list_search') {
        if (empty($_REQUEST['search_query'])) {
            error('�� ������� ��������� ������');
        }
        $preparing = 'preparing_videos_list';
        $args['type'] = 'search';
        $args['value'] = $_REQUEST['search_query'];
        $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
        $args['mobile'] = 'no';
        $args['sort'] = 'mr';
    }
    if ($page == 'videos_list_category') {
        $preparing = 'preparing_videos_list';
        $args['type'] = 'category';
        $args['value'] = $_GET['category_id'];
        $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
        $args['mobile'] = 'no';
        $args['sort'] = 'mr';
    }
    if ($page == 'videos_list_tag') {
        if (empty($_GET['sort'])) {
            error('�� �������� ����������');
        }
        if (empty($_GET['tag_ru'])) {
            error('�� ������� ���');
        }
        $preparing = 'preparing_videos_list';
        $args['type'] = 'tag';
        $args['value'] = $_GET['tag_ru'];
        $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
        $args['mobile'] = 'no';
        $args['sort'] = $_GET['sort'];
        $args['link_start'] = value_from_config_for_domain('videohub', 'pages_videos_list_tag_link_start');
        temp_var('tag_ru', urlencode($_GET['tag_ru']));
        temp_var('sort', $_GET['sort']);
    }
    $arr = cache($preparing, $args);
    // pre($arr);
    // unset($args);
    $args['rows'] = $arr['rows'];
    $args['page_number'] = $args['page'];
    // pre($args);
    $content = preparing_pages_list($args);

    return $content;
}

// ������ ������� �������
function preparing_pages_list($args)
{
    if (! isset(
        $args['rows'],
        $args['per_page'],
        $args['page_number'],
        $args['link_start']
    )) {
        error('�� ������� ����������� ���������');
    }
    if (empty($args['link_end'])) {
        $args['link_end'] = '';
    }
    if (empty($args['length'])) {
        $args['length'] = 5;
    }
    if (empty($args['previous_text'])) {
        $args['previous_text'] = '&laquo;';
    }
    if (empty($args['next_text'])) {
        $args['next_text'] = '&raquo;';
    }
    if (empty($args['first_last_pages'])) {
        $args['first_last_pages'] = 'yes';
    }
    if (empty($args['current_class_name'])) {
        $args['current_class_name'] = 'current';
    }
    if (empty($args['points_class_name'])) {
        $args['points_class_name'] = 'points';
    }
    if (empty($args['a_inside_inactive'])) {
        $args['a_inside_inactive'] = 0;
    }
    $content = page_numbers($args['rows'], $args['per_page'], $args['page_number'],
        $args['link_start'], $args['length'], $args['link_end'], $args['previous_text'],
        $args['next_text'], $args['first_last_pages'], $args['current_class_name'],
        $args['points_class_name'], $args['a_inside_inactive']);

    return $content;
}

/*

function pages_video_collection() {
    $arr=func_replacement('preparing_video_collection');
    $rows=$arr['rows']; //����� �������
    $per_page=value_from_config_for_domain('videohub','collection_videos_per_page'); //���������� �� ��������
    $pages_length=value_from_config_for_domain('videohub','pages_length');
    $pages=ceil($rows/$per_page); //�������
    $list=page_numbers($rows,$per_page,$_GET['p'],'/videos/'.$_GET['sort'].'/',$pages_length);
    return $list;
}

function pages_list($args) {
    global $config;
    $args['pages_length']=5;
    $args['domain']=$config['domain'];
    $args['type']='all';
    $args['value']='value';
    $args['page']=(int)$_GET['p'];
    $args['per_page']=value_from_config_for_domain('videohub','stories_per_page');
    $args['sort']=$_GET['sort'];
    $arr=cache('preparing_stories_list',$args);
    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows=$arr['rows']; //����� �������
    $pages=ceil($rows/$args['per_page']); //�������
    $list=page_numbers($rows,$args['per_page'],$_GET['p'],'/sex-texts/page/',$pages_length,'/','&laquo;','&raquo;','yes','active','active','1');
    return $list;
}

*/

function preparing_video_tags_list_array($args)
{
    if (empty($args['id'])) {
        error('�� ������� ID �����');
    }
    $data = query('
		SELECT tag_ru,tag_en
		FROM '.tabname('videohub', 'tags').'
		WHERE tags.video_id = \''.$args['id'].'\' and tag_ru is not null
		group by tag_ru
		;');
    $arr = readdata($data, 'nokey');

    return $arr;
}

function video_tags_list_simple($id)
{
    $args['id'] = $id;
    $arr = cache('preparing_video_tags_list_array', $args);
    // $arr=video_tags_list_array($id);
    $list = '';
    $i = 0;
    foreach ($arr as $v) {
        if ($i != 0) {
            $list .= ', ';
        }	$i = 1;
        $list .= $v['tag_ru'];
    }

    return $list;
}

// ����� ����� ��� ����� (�������� 6 �����)
function video_tags_list()
{
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_video_tags_list_array', $args);
    // pre($arr);
    $list = '';
    $i = 0;
    foreach ($arr as $v) {
        $i++;
        if ($i > 6) {
            break;
        }
        temp_var('tag_ru', $v['tag_ru']);
        temp_var('tag_ru_url', without_spaces($v['tag_ru']));
        temp_var('tag_ru_urlencode', urlencode($v['tag_ru']));
        if (isset($v['tag_en'])) {
            temp_var('tag_en', $v['tag_en']);
            temp_var('tag_en_url', without_spaces($v['tag_en']));
            temp_var('tag_en_urlencode', urlencode($v['tag_en']));
        }
        if ($i < 6 and $i != count($arr)) {
            temp_var('comma', ', ');
        } else {
            temp_var('comma', '');
        }
        $list .= template('li/list_tags_of_video');
    }

    return $list;
}

function categories_list_mobile()
{
    $args['mobile'] = 'yes';
    $args['template'] = 'li/categories_list';

    return categories_list_new($args);
}

function categories_list_new($args = ['mobile' => 'no', 'template' => 'li/categories_list'])
{
    global $config;
    if (! isset($args['mobile'],$args['template'])) {
        error('�� �������� ����������� ����������');
    }
    // pre($args);
    $arr = cache('preparing_categories_list', $args, 43200);
    // pre($arr);
    $list = '';
    foreach ($arr as $v) {
        temp_var('id', $v['id']);
        temp_var('name_translit', $v['name_translit']);
        temp_var('name', $v['name']);
        if (isset($v['name_url'])) {
            temp_var('name_url', $v['name_url']);
        } else {
            temp_var('name_url', without_spaces($v['name']));
        }
        temp_var('rows', $v['rows']);
        temp_var('class_active', '');
        if (! empty($v['title'])) {
            temp_var('title', $v['title']);
        }
        if (! empty($v['title_translit'])) {
            temp_var('title_translit', $v['title_translit']);
        }
        if (! empty($_GET['category_id'])) {
            // pre($v);
            if ($_GET['category_id'] == $v['id']) {
                temp_var('class_active', 'class="active"');
            }
        }
        if (isset($args['with_pic']) and $args['with_pic'] == 'yes') {
            temp_var('pic_video_id', $v['pic_video_id']);
        }
        if (isset($v['img'])) {
            temp_var('category_image', $v['img']);
        }
        $list .= template($args['template']);
        // �����, ���� ��� ��������� �� ��� �������� ���������, �� � ������� ��������� ��������� ������� �� ������ ���������,
        // ����� ������ � ���� ������ ������� ������, ��� ���������� ����������
        clear_temp_vars();
    }

    // pre($_GET);
    return $list;
}

function categories_list_with_pic()
{
    $args['with_pic'] = 'yes';
    $args['mobile'] = 'no';
    $args['template'] = 'li/item_categories_list_with_pic';

    return categories_list_new($args);
}

function preparing_categories_list_old($args)
{
    global $config;
    if (! isset($args['mobile'])) {
        error('�� ������ ���������� mobile');
    }
    if ($args['mobile'] == 'no') {
        $mobile_only_mp4 = '';
    }
    if ($args['mobile'] == 'yes') {
        $mobile_only_mp4 = 'and ext=\'mp4\'';
    }
    if (! isset($args['with_pic'])) {
        $args['with_pic'] = 'no';
    }
    if (! isset($args['stars'])) {
        $args['stars'] = 0;
    }
    if (! isset($args['show_empty'])) {
        $args['show_empty'] = 1;
    }
    $query['stars'] = 'and star=\''.$args['stars'].'\'';
    $data = query('
		select categories_domains.category_id,categories_domains.id,categories_domains.title,
		categories_domains.title_translit,categories_groups.name,categories_groups.name_translit,
		name_url
		from '.tabname('videohub', 'categories_domains').'
		inner join '.tabname('videohub', 'categories_groups').'  on categories_domains.category_id=categories_groups.id
		where domain=\''.$config['domain'].'\' '.$query['stars'].' order by name');
    $arr = readdata($data, 'nokey');
    // if (empty($arr)) {error('��������� �����������');};
    $arr_answer = [];
    $used_videos_ids = [];
    $print = '';
    $auto_categories = value_from_config_for_domain('videohub', 'auto_categories');
    foreach ($arr as $v) {
        if ($auto_categories == '0') {
            $data = query('
			SELECT count(videos.id) as q_videos
			FROM '.tabname('videohub', 'videos').'
			WHERE domain=\''.$config['domain'].'\' '.$mobile_only_mp4.' and category_id=\''.$v['id'].'\'
			');
        }
        if ($auto_categories == '1') {
            // ��� �� categories_videos
            // ������ �� ��������� ����� ����������� � ������ ���������
            $data = query('
				SELECT catgroups_tags.tag_ru
				FROM '.tabname('videohub', 'categories_domains').'
				INNER JOIN  '.tabname('videohub', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
				INNER JOIN  '.tabname('videohub', 'catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
				WHERE categories_domains.id=\''.$v['id'].'\' and categories_domains.domain=\''.$config['domain'].'\'
				');
            $arr2 = readdata($data, 'nokey');
            // ��������� ������ ������ �����
            $cat_tags = [];
            foreach ($arr2 as $v2) {
                $cat_tags[] = $v2['tag_ru'];
            }
            if (empty($cat_tags)) {
                error('��������� ����������', '��� ��������� '.$v['id'].' ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
            }
            // ������ �� ��������� ���������� ���� ���������� ID, ��������������� �������
            $data = query('
				SELECT count(distinct tags.video_id) as q_videos
				FROM '.tabname('videohub', 'tags').'
				/* inner join '.tabname('videohub', 'tags_translation').' on tags.tag_en=tags_translation.tag_en */
				inner join '.tabname('videohub', 'videos').' on videos.id=tags.video_id
				WHERE domain=\''.$config['domain'].'\' '.$mobile_only_mp4.' and tags.tag_ru in ('.array_to_str($cat_tags).')
				');
        }
        $arr2 = mysql_fetch_array($data);
        $v['rows'] = $arr2[0];
        if (($args['show_empty'] == 0) and $v['rows'] == 0) {
            continue;
        }
        if ($args['with_pic'] == 'yes') {
            $args['domain'] = $config['domain'];
            $args['type'] = 'category';
            $args['value'] = $v['id'];
            $args['page'] = 1;
            $args['sort'] = 'mv';
            $args['per_page'] = 15; // �� ������ ������������� �����
            $args['mobile'] = 'no';
            $videos = cache('preparing_videos_list', $args);
            // pre($videos);
            // ����� �������� �� �����������
            foreach ($videos['videos'] as $k_video => $v_video) {
                // die($v_video['id']);
                if (isset($used_videos_ids[$v_video['id']])) {
                    // die('���������'.$v_video['id']);
                    unset($videos['videos'][$k_video]);
                }
            }
            if (empty($videos['videos'])) {
                $v['pic_video_id'] = 'no_image';
            } else {
                $current_in_videos = current($videos['videos']);
                $used_videos_ids[$current_in_videos['id']] = '';
                $v['pic_video_id'] = $current_in_videos['id'];
            }
        }
        $arr_answer[] = $v;
    }
    // pre($used_videos_ids);
    pre($arr_answer);

    return $arr_answer;
}

function preparing_categories_list($args)
{
    global $config;
    if (! isset($args['mobile'])) {
        error('�� ������ ���������� mobile');
    }
    if ($args['mobile'] == 'no') {
        $mobile_only_mp4 = '';
    }
    if ($args['mobile'] == 'yes') {
        $mobile_only_mp4 = 'and ext=\'mp4\'';
    }
    if (! isset($args['with_pic'])) {
        $args['with_pic'] = 'no';
    }
    if (! isset($args['type'])) {
        $args['type'] = 'default';
    }
    if (! isset($args['show_empty'])) {
        $args['show_empty'] = 1;
    }
    if (empty($args['domain'])) {
        $args['domain'] = $config['domain'];
    }
    $query['type'] = 'and type=\''.$args['type'].'\'';
    $data = query('
		select categories_domains.category_id,categories_domains.id,categories_domains.title,
		categories_domains.title_translit,categories_groups.name,categories_groups.name_translit,
		name_url,categories_domains.img
		from '.tabname('videohub', 'categories_domains').'
		inner join '.tabname('videohub', 'categories_groups').'  on categories_domains.category_id=categories_groups.id
		where domain=\''.$args['domain'].'\' and categories_domains.active=\'1\' '.$query['type'].' order by name');
    $arr = readdata($data, 'nokey');
    // if (empty($arr)) {error('��������� �����������');};
    $arr_answer = [];
    $used_videos_ids = [];
    foreach ($arr as $v) {
        $videos = cache('preparing_videos_list', ['domain' => $args['domain'], 'type' => 'category', 'value' => $v['id'], 'page' => 1, 'sort' => 'mv', 'per_page' => 15, 'mobile' => 'no']);
        $v['rows'] = $videos['rows'];
        if (($args['show_empty'] == 0) and $v['rows'] == 0) {
            continue;
        }
        if ($args['with_pic'] == 'yes') {
            // ����� �������� �� �����������
            foreach ($videos['videos'] as $k_video => $v_video) {
                if (isset($used_videos_ids[$v_video['id']])) {
                    unset($videos['videos'][$k_video]);
                }
            }
            if (empty($videos['videos'])) {
                $v['pic_video_id'] = 'no_image';
            } else {
                $current_in_videos = current($videos['videos']);
                $used_videos_ids[$current_in_videos['id']] = '';
                $v['pic_video_id'] = $current_in_videos['id'];
            }
        }
        $arr_answer[] = $v;
    }

    // pre($used_videos_ids);
    // pre($arr_answer);
    return $arr_answer;
}

function preparing_category_info($args)
{
    global $config;
    if (! isset($args['category_id'])) {
        error('�� ������� ID ���������');
    }
    $data = query('
		SELECT categories_domains.id, categories_domains.h1, categories_domains.h2, categories_domains.title, categories_domains.title_translit, categories_domains.description,
		categories_domains.photo_h1, categories_domains.photo_title, categories_domains.photo_description, categories_domains.photo_text, categories_domains.photo_img,
		categories_domains.keywords, categories_domains.text, categories_domains.text_2, categories_domains.img,
		categories_groups.name, categories_groups.name_translit, categories_groups.name_url,categories_groups.type
		FROM '.tabname('videohub', 'categories_domains').'
		JOIN  '.tabname('videohub', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
		WHERE categories_domains.id=\''.$args['category_id'].'\' and categories_domains.domain=\''.$config['domain'].'\'
		');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('��������� �� ����������', '�� ������� ����� ��������� ��� ������� ������');
    }

    return $arr;
}

function video_rating_stars()
{
    $args['video_id'] = $_GET['id'];
    $arr = cache('preparing_view_video', $args);
    temp_var('video_id', $arr['id']);
    temp_var('video_votes', $arr['votes']);
    temp_var('video_rating', $arr['rating']);

    return template('li/rating_stars');
}

function preparing_view_video($args)
{
    global $config;
    if (! isset($args['video_id'])) {
        error('�� �������� ����������� ���������');
    }
    if (empty($args['check_domain'])) {
        $args['check_domain'] = 'yes';
    }
    if ($args['check_domain'] == 'yes') {
        $check_domain = 'and domain=\''.$config['domain'].'\'';
    }
    if ($args['check_domain'] == 'no') {
        $check_domain = '';
    }
    $data = query('select id,id_hub,hub,title_ru,title_ru_alt,title_ru_translit,ifnull(views,0) as views,ifnull(votes,0) as votes,ifnull(rating,0) as rating,
		description,ph_viewkey,deleted,likes,dislikes,apply_date,unix_timestamp(apply_date) as unix_apply_date,local_id
		from '.tabname('videohub', 'videos').'
		left join '.tabname('videohub', 'videos_views').' on videos_views.video_id=videos.id
		where id=\''.$args['video_id'].'\' '.$check_domain.';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('����� �� �������');
    }
    $date_arr = my_date($arr['unix_apply_date']);
    $arr['rus_date'] = $date_arr['day'].' '.$date_arr['month'].' '.$date_arr['year'].' � '.$date_arr['clock'];
    $arr['likes_sum'] = $arr['likes'] - $arr['dislikes'];
    $arr['likes_votes'] = $arr['likes'] + $arr['dislikes'];
    $arr['date_slashed'] = date('Y/m/d', $arr['unix_apply_date']);

    return $arr;
}

function videos_list_same_arr($video_id)
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'same';
    $args['value'] = $video_id;
    $args['page'] = 1;
    $args['per_page'] = value_from_config_for_domain('videohub', 'number_of_same_videos');
    $args['sort'] = 'ms';
    $args['mobile'] = 'no';
    // ��� �� 7 ����
    $arr = cache('preparing_videos_list', $args, 604800);

    return $arr;
}

function videos_list_same()
{
    $arr = videos_list_same_arr($_GET['id']);

    return thumbs_from_array($arr['videos']);
}

function videos_list_mobile_same()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'same';
    $args['value'] = $_GET['id'];
    $args['page'] = 1;
    $args['per_page'] = value_from_config_for_domain('videohub', 'number_of_same_videos');
    $args['sort'] = 'ms';
    $args['mobile'] = 'yes';
    // ��� �� 7 ����
    $arr = cache('preparing_videos_list', $args, 604800);

    return thumbs_from_array($arr['videos']);
}

function videos_list_search()
{
    global $config;
    if (empty($_REQUEST['search_query'])) {
        error('�� ������� ��������� ������');
    }
    $args['domain'] = $config['domain'];
    $args['type'] = 'search';
    $args['value'] = $_REQUEST['search_query'];
    if (! isset($_GET['p'])) {
        $args['page'] = 1;
    } else {
        $args['page'] = (int) $_GET['p'];
    }
    $args['sort'] = 'mv';
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args, 43200);

    return thumbs_from_array($arr['videos']);
}

function videos_list_mobile_search()
{
    global $config;
    if (empty($_REQUEST['search_query'])) {
        error('�� ������� ��������� ������');
    }
    $args['domain'] = $config['domain'];
    $args['type'] = 'search';
    $args['value'] = $_REQUEST['search_query'];
    if (! isset($_GET['p'])) {
        $args['page'] = 1;
    } else {
        $args['page'] = (int) $_GET['p'];
    }
    $args['sort'] = 'mv';
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['mobile'] = 'yes';
    $arr = cache('preparing_videos_list', $args, 43200);

    return thumbs_from_array($arr['videos']);
}

function videos_list_all()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (! isset($_GET['p'])) {
        $args['page'] = 1;
    } else {
        $args['page'] = (int) $_GET['p'];
    }
    if (! isset($_GET['sort'])) {
        $args['sort'] = 'mr';
    } else {
        $args['sort'] = $_GET['sort'];
    }
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    // ����� ��� ��������� �� ����������� ��������� ������ � cache
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args, 43200);

    return thumbs_from_array($arr['videos']);
}

function videos_list_mobile_all()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    // $args['page']=(int)$_GET['p'];
    // $args['sort']=$_GET['sort'];
    if (! isset($_GET['p'])) {
        $args['page'] = 1;
    } else {
        $args['page'] = (int) $_GET['p'];
    }
    if (! isset($_GET['sort'])) {
        $args['sort'] = 'mr';
    } else {
        $args['sort'] = $_GET['sort'];
    }
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['mobile'] = 'yes';
    $arr = cache('preparing_videos_list', $args, 43200);

    return thumbs_from_array($arr['videos']);
}

function videos_list_category()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (! isset($_GET['p'])) {
        $args['page'] = 1;
    } else {
        $args['page'] = (int) $_GET['p'];
    }
    if (! isset($_GET['sort'])) {
        $args['sort'] = 'mr';
    } else {
        $args['sort'] = $_GET['sort'];
    }
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    // ����� ��� ��������� �� ����������� ��������� ������ � cache
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['mobile'] = 'no';
    // pre($args);
    $arr = cache('preparing_videos_list', $args);

    // pre($arr);
    return thumbs_from_array($arr['videos']);
}

function videos_list_mobile_category()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'yes';
    $arr = cache('preparing_videos_list', $args);

    return thumbs_from_array($arr['videos']);
}

function videos_list_tag()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'tag';
    $args['value'] = $_GET['tag_ru'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);

    return thumbs_from_array($arr['videos']);
}

function videos_list_tag_en()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'tag_en';
    $args['value'] = $_GET['tag_en'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);

    return thumbs_from_array($arr['videos']);
}

function videos_list_mobile_tag()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'tag';
    $args['value'] = $_GET['tag_ru'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'yes';
    $arr = cache('preparing_videos_list', $args);

    return thumbs_from_array($arr['videos']);
}

function preparing_videos_list($args)
{
    global $config;
    if (empty($args)) {
        error('�� �������� ���������');
    }
    if (! isset($args['domain'],$args['type'],$args['value'],$args['page'],$args['sort'],$args['mobile'])) {
        error('�� �������� ����������� ���������');
    }
    if ($args['page'] <= 0) {
        error('�������� ����� ��������');
    }
    $domain = $args['domain'];
    $type = $args['type'];
    $value = $args['value'];
    $page = $args['page'];
    $sort = $args['sort'];
    $mobile = $args['mobile'];
    $per_page = $args['per_page'];
    if (empty($domain)) {
        error('�� ������� �����');
    }
    if (empty($value)) {
        error('�� �������� ����������� ����������');
    }
    $types = ['all' => '', 'category' => '', 'tag' => '', 'same' => '', 'search' => '', 'tag_en' => ''];
    $sort_sql = [
        'mv' => 'views desc',
        'mr' => 'apply_date desc',
        'tr' => 'rating desc, votes desc',
        'ms' => 'same_tags_q desc, rating_sko desc',
        'mc' => 'q_comments desc',
    ];
    if (isset($types[$type]) == 0) {
        error('��� ������ �� ����������');
    }
    if (isset($sort_sql[$sort]) == 0) {
        error('���������� �� ����������');
    }
    if ($mobile == 'no') {
        $mobile_only_mp4 = '';
        // $per_page=value_from_config_for_domain('videohub','collection_videos_per_page'); //���������� �� ��������
    }
    if ($mobile == 'yes') {
        $mobile_only_mp4 = 'and ext=\'mp4\'';
        // $per_page=value_from_config_for_domain('videohub','collection_videos_per_page_mobile'); //���������� �� ��������
    }
    $from = ($page - 1) * $per_page;
    // if ($type='new') {
    // 	$data=query('select SQL_CALC_FOUND_ROWS id,title_ru,title_ru_translit,unix_timestamp(add_date) as date,
    // 	ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub,
    // 	((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko
    // 	from '.tabname('videohub','videos').'
    // 					left join '.tabname('videohub','videos_views').' on videos.id=videos_views.video_id
    // 					where domain=\''.$config['domain'].'\'
    // 					order by rating_sko desc, views desc
    // 					limit '.$value);
    // };
    if ($type == 'all') {
        $data = query('select SQL_CALC_FOUND_ROWS id,hub,title_ru,title_ru_translit,description,unix_timestamp(apply_date) as date,
		ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, deleted, ph_date,ph_viewkey,
		ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
		(select count(*) from '.tabname('videohub', 'comments').' where video_id=videos.id) as q_comments
		from '.tabname('videohub', 'videos').'
						left join '.tabname('videohub', 'videos_views').' on videos.id=videos_views.video_id
						where domain=\''.$config['domain'].'\' '.$mobile_only_mp4.'
						order by '.$sort_sql[$sort].' limit '.$from.','.$per_page);
    }
    if ($type == 'category') {
        $category_id = $value;
        $auto_categories = value_from_config_for_domain('videohub', 'auto_categories');
        if ($auto_categories == '0') {
            $data = query('
				SELECT SQL_CALC_FOUND_ROWS videos.id as id, hub, videos.title_ru, videos.title_ru_translit, videos.description, unix_timestamp(videos.apply_date) as date,
				ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, videos.deleted, ph_date,ph_viewkey,
				ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
				(select count(*) from '.tabname('videohub', 'comments').' where video_id=videos.id) as q_comments
				FROM '.tabname('videohub', 'videos').'
				left join '.tabname('videohub', 'videos_views').' on videos.id=videos_views.video_id
				WHERE domain=\''.$config['domain'].'\' '.$mobile_only_mp4.' and category_id=\''.$category_id.'\' and categories_domains.active=\'1\'
				order by '.$sort_sql[$sort].'
				limit '.$from.','.$per_page
            );
        }
        if ($auto_categories == '1') {
            // ������ �� ��������� ����� ����������� � ������ ��������� � ����� ��������� �� ��������� ��� ���������
            $data = query('
				SELECT catgroups_tags.tag_ru, categories_groups.name_translit
				FROM '.tabname('videohub', 'categories_domains').'
				INNER JOIN  '.tabname('videohub', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
				INNER JOIN  '.tabname('videohub', 'catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
				WHERE categories_domains.id=\''.$category_id.'\' and categories_domains.domain=\''.$config['domain'].'\' and categories_domains.active=\'1\'
				');
            $arr = readdata($data, 'nokey');
            // ��������� ������ ������ �����
            $cat_tags = [];
            foreach ($arr as $v) {
                $cat_tags[] = $v['tag_ru'];
            }
            if (empty($cat_tags)) {
                error('��������� ����������', '��� ��������� ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
            }
            // �������� �������� ��������� �� ���������
            $content_array['category_name_translit'] = $arr[0]['name_translit'];
            // ������ �� ��������� ���� ���������� ����� �����, ��������������� �������
            $data = query('
				SELECT SQL_CALC_FOUND_ROWS tags.video_id as id,hub,tags.tag_en,tags.tag_ru, videos.title_ru,
				videos.title_ru_translit, videos.description,
				unix_timestamp(videos.apply_date) as date,
				ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, videos.deleted, ph_date,ph_viewkey,
				ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
				(select count(*) from '.tabname('videohub', 'comments').' where video_id=videos.id) as q_comments
				FROM '.tabname('videohub', 'tags').'
				/* inner join '.tabname('videohub', 'tags_translation').' on tags.tag_en=tags_translation.tag_en */
				inner join '.tabname('videohub', 'videos').' on videos.id=tags.video_id
				left join '.tabname('videohub', 'videos_views').' on tags.video_id=videos_views.video_id
				WHERE domain=\''.$config['domain'].'\' '.$mobile_only_mp4.' and tags.tag_ru in ('.array_to_str($cat_tags).')
				group by id
				order by '.$sort_sql[$sort].'
				limit '.$from.','.$per_page
            );
        }
    }
    if ($type == 'tag') {
        $tag_ru = $value;
        check_tag_exist($tag_ru);
        $data = query('
			SELECT SQL_CALC_FOUND_ROWS tags.video_id as id,hub,tags.tag_en,tags_translation.tag_ru, videos.title_ru,
			videos.title_ru_translit, videos.description, unix_timestamp(videos.apply_date) as date,
			ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, videos.deleted, ph_date,ph_viewkey,
			ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
			(select count(*) from '.tabname('videohub', 'comments').' where video_id=videos.id) as q_comments
			FROM '.tabname('videohub', 'tags').'
			join '.tabname('videohub', 'tags_translation').' on tags.tag_en=tags_translation.tag_en and tags_translation.tag_ru=\''.$tag_ru.'\'
			join '.tabname('videohub', 'videos').' on videos.id=tags.video_id and domain=\''.$config['domain'].'\'
			left join '.tabname('videohub', 'videos_views').' on tags.video_id=videos_views.video_id
			group by id
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$per_page
        );
    }
    if ($type == 'tag_en') {
        $tag_en = checkstr($value);
        check_tag_en_exist($tag_en);
        $data = query('
			SELECT SQL_CALC_FOUND_ROWS tags.video_id as id,hub,tags.tag_en,tags.tag_ru, videos.title_ru,
			videos.title_ru_translit, videos.description, unix_timestamp(videos.apply_date) as date,
			ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes,
			videos.id_hub, videos.deleted, ph_date,ph_viewkey,
			ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
			(select count(*) from '.tabname('videohub', 'comments').' where video_id=videos.id) as q_comments
			FROM '.tabname('videohub', 'tags').'
			join '.tabname('videohub', 'videos').' on videos.id=tags.video_id and domain=\''.$config['domain'].'\'
			left join '.tabname('videohub', 'videos_views').' on tags.video_id=videos_views.video_id
			where tags.tag_en=\''.$tag_en.'\'
			group by id
			order by '.$sort_sql[$sort].'
			limit '.$from.','.$per_page
        );
    }
    if ($type == 'same') {
        $video_id = $value;
        // ������ �� ��������� ����� �������� �����
        $data = query('
			SELECT tags_translation.tag_ru
			FROM '.tabname('videohub', 'tags').'
			INNER JOIN '.tabname('videohub', 'tags_translation').' ON tags.tag_en = tags_translation.tag_en
			WHERE tags.video_id = \''.$video_id.'\'
			');
        $arr = readdata($data, 'nokey');
        // ��������� ������ �����
        $video_tags = [];
        foreach ($arr as $v) {
            $video_tags[] = $v['tag_ru'];
        }
        // $data=query('
        // 	SELECT SQL_CALC_FOUND_ROWS tags.video_id as id,hub,tags.tag_en,tags.tag_ru, videos.title_ru, videos.title_ru_translit, videos.description,
        // 	unix_timestamp(videos.apply_date) as date, ifnull(videos_views.views,0) as views, count(tags.video_id) as same_tags_q,
        // 	ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, videos.deleted, ph_date, ph_viewkey,((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko,
        // 	ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
        // 	(select count(*) from '.tabname('videohub','comments').' where video_id=videos.id) as q_comments
        // 	FROM '.tabname('videohub','tags').'
        // 	inner join '.tabname('videohub','videos').' on videos.id=tags.video_id
        // 	left join '.tabname('videohub','videos_views').' on tags.video_id=videos_views.video_id
        // 	WHERE domain=\''.$config['domain'].'\' '.$mobile_only_mp4.' and tags.tag_ru in ('.array_to_str($video_tags).') and tags.video_id!=\''.$video_id.'\'
        // 	and deleted_from_hub=\'no\'
        // 	group by id
        // 	order by '.$sort_sql[$sort].'
        // 	limit '.$per_page.'
        // 	');
        $data = query('
			SELECT SQL_CALC_FOUND_ROWS tags.video_id as id,hub,tags.tag_en,tags.tag_ru, videos.title_ru, videos.title_ru_translit, videos.description,
			unix_timestamp(videos.apply_date) as date, ifnull(videos_views.views,0) as views, tags.q as same_tags_q,
			ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, videos.deleted, ph_date, ph_viewkey,((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko,
			ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
			(select count(*) from '.tabname('videohub', 'comments').' where video_id=videos.id) as q_comments
			FROM (
			select tags.*,count(*) as q
			from '.tabname('videohub', 'tags').'
			join '.tabname('videohub', 'videos').' on videos.id = tags.video_id
			/* left join '.tabname('videohub', 'videos_views').' on tags.video_id=videos_views.video_id */
			where
			domain = \''.$config['domain'].'\' '.$mobile_only_mp4.' and deleted_from_hub=\'no\' and
			tags.video_id!=\''.$video_id.'\' and
			tags.tag_ru in ('.array_to_str($video_tags).')
			group by videos.id
			order by q desc
			limit '.$per_page.'
			) tags
			join '.tabname('videohub', 'videos').' on videos.id = tags.video_id
			left join '.tabname('videohub', 'videos_views').' on tags.video_id = videos_views.video_id
			order by '.$sort_sql[$sort].'
			');
        /*
        $data=query('
            SELECT SQL_CALC_FOUND_ROWS tags.video_id as id,tags.tag_en,tags_translation.tag_ru, videos.title_ru, videos.title_ru_translit, videos.description,
            unix_timestamp(videos.apply_date) as date, ifnull(videos_views.views,0) as views, count(tags.video_id) as same_tags_q,
            ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, videos.deleted, ph_date, ((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko,
            ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
            (select count(*) from '.tabname('videohub','comments').' where video_id=videos.id) as q_comments
            FROM '.tabname('videohub','tags').'
            inner join '.tabname('videohub','tags_translation').' on tags.tag_en=tags_translation.tag_en
            inner join '.tabname('videohub','videos').' on videos.id=tags.video_id
            left join '.tabname('videohub','videos_views').' on tags.video_id=videos_views.video_id
            WHERE domain=\''.$config['domain'].'\' '.$mobile_only_mp4.' and tags_translation.tag_ru in ('.array_to_str($video_tags).') and tags.video_id!=\''.$video_id.'\'
            group by id
            order by '.$sort_sql[$sort].'
            limit '.$per_page.'
            ');
            */
    }
    if ($type == 'search') {
        $value = checkstr($value);
        $data = query('select SQL_CALC_FOUND_ROWS videos.id as id,hub,videos.title_ru, videos.title_ru_translit, videos.description,
			unix_timestamp(videos.apply_date) as date,
			ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, videos.deleted, ph_date,ph_viewkey,
			ifnull(likes,0) as likes,ifnull(dislikes,0) as dislikes, local_id,
			(select count(*) from '.tabname('videohub', 'comments').' where video_id=videos.id) as q_comments
			from '.tabname('videohub', 'videos').'
			left join '.tabname('videohub', 'videos_views').' on videos.id=videos_views.video_id
			where
			(domain=\''.$config['domain'].'\') and (
				title_ru like \'%'.$value.'%\'
				or title_en like \'%'.$value.'%\'
				or description like \'%'.$value.'%\'
				or cast(date(apply_date) as char)=\''.$value.'\'
				or 1=(
					select 1 from '.tabname('videohub', 'tags').'
					where video_id=videos.id and (tags.tag_ru like \'%'.$value.'%\' or tags.tag_en like \'%'.$value.'%\')
					group by 1
					)
		)
		order by '.$sort_sql[$sort].'
		limit '.$from.','.$per_page
        );
    }
    $arr = readdata($data, 'nokey');
    $content_array['rows'] = rows_without_limit();
    if (value_from_config_for_domain('videohub', 'thumbs_categories_of_video') == '1') {
        foreach ($arr as $k => $v) {
            $arr[$k]['categories_of_video'] = list_categories_of_video_id(['video_id' => $v['id']]);
        }
    }
    $content_array['videos'] = $arr;
    // pre($content_array);
    if (empty($content_array['videos'])) {
        if ($type == 'same') {
            $args['value'] = 'value';
            $args['type'] = 'all';
            $args['sort'] = 'mv';
            $content_array = cache('preparing_videos_list', $args, 43200);
            logger()->info('�� ������� �� ������ �������� �����, �������� �����');
        } else {
            logger()->info('�� ������� �� ������ �����');
        }
    }

    return $content_array;
}
