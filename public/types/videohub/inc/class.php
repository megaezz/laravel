<?php

namespace MegawebV1;

// ���� ��������� ������� ������� ���������� ��� ������ � ��������
//

function redirect_to_direct_video()
{
    $id = empty($_GET['id']) ? error('ID required') : checkstr($_GET['id']);
    $video = new video($id);
    $video->checkConfigDomain();
    $link = $video->getDirectLinkSameCached();
    if (! $link) {
        return 'Unable to get direct link';
    }
    header('Location: '.$link);
    exit();
}

function video_embed_player()
{
    $id = empty($_GET['id']) ? error('ID required') : checkstr($_GET['id']);
    $video = new videoWriter($id);
    $video->checkConfigDomain();

    return $video->getPlayer();
}
