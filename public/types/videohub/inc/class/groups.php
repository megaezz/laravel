<?php

namespace MegawebV1;

class groups
{
    public function __construct()
    {
        // $this->domain=null;
    }

    public function get()
    {
        $arr = query_arr('select distinct catgroups_tags.group_id
			from '.typetab('catgroups_tags').'
			;');
        $rows = rows_without_limit();
        $object = new stdClass;
        $object->rows = $rows;
        $object->array = $arr;

        return $object;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }
}
