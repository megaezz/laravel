<?php

namespace MegawebV1;

class domainCategory extends category
{
    public function __construct($id = null, $domain = null)
    {
        parent::__construct($id);
        $this->domain = $domain ? $domain : error('Domain required to create domainCategory class');
        // pre(var_dump($this));
        $arr = query_assoc('
			select id,domain,category_id,text,text_2,title,h1,img,keywords,description,
			photo_title,photo_description,photo_h1,photo_text,photo_img,title_translit,active,h2
			from '.typetab('categories_domains').' 
			where category_id=\''.$this->id.'\' and domain=\''.$this->domain.'\'
			;');
        if (! $arr) {
            error('Category doesn\'t exist');
        }
        $this->category_id = $arr['category_id'];
        $this->text = $arr['text'];
        $this->text_2 = $arr['text_2'];
        $this->title = $arr['title'];
        $this->h1 = $arr['h1'];
        $this->img = $arr['img'];
        $this->keywords = $arr['keywords'];
        $this->description = $arr['description'];
        $this->photo_title = $arr['photo_title'];
        $this->photo_description = $arr['photo_description'];
        $this->photo_h1 = $arr['photo_h1'];
        $this->photo_text = $arr['photo_text'];
        $this->photo_img = $arr['photo_img'];
        $this->title_translit = $arr['title_translit'];
        $this->active = $arr['active'];
        $this->h2 = $arr['h2'];
    }
}
