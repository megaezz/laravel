<?php

namespace MegawebV1;

class videoWriter extends video
{
    public function getEmbedPlayer()
    {
        temp_var('id_hub', $this->id_hub);
        temp_var('ph_viewkey', $this->ph_viewkey);

        return template('player/'.$this->hub.'/iframe');
    }

    public function getInternalPlayer()
    {
        temp_var('video_id', $this->id);
        temp_var('thumb', $this->thumb);

        return template('player/videojs.com/code');
    }

    public function getPlayer()
    {
        if ($this->hub == 'pornhub.com') {
            return $this->getInternalPlayer();
        }
        if ($this->hub == 'xvideos.com') {
            return $this->getInternalPlayer();
        }

        // для остальных хабов которые не обозначены выше используем встроенный плеер
        return $this->getInternalPlayer();
    }

    public function getRuTagsStr()
    {
        $arr = $this->getTags();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<li>'.$v.'</li>';
        }

        return $list;
    }
}
