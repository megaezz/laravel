<?php

namespace MegawebV1;

class videosRewritePage extends videosPage
{
    public function __construct()
    {
        authorizate_videohub_translater();
        parent::__construct();
        $this->listTemplate = 'admin/rewrite/list';
        // $this->page=1;
        // $this->limit=100;
    }

    public function page()
    {
        temp_var('getPagesBootstrap', $this->getPagesBootstrap());
        temp_var('list', $this->list());
        temp_var('getRows', $this->getRows());

        return template('admin/rewrite/videos');
    }

    public function submitHeaders()
    {
        $title = empty($_GET['title']) ? null : checkstr($_GET['title']);
        $description = empty($_GET['description']) ? null : checkstr($_GET['description']);
        $video_id = empty($_GET['id']) ? error('Video ID required') : checkstr($_GET['id']);
        $video = new video($video_id);
        $login = auth_login(['type' => 'videohub']);
        if ($title) {
            if (! empty($video->title)) {
                exit('Already writed by '.$video->title_writer);
            }
            $video->setTitle($title);
            $video->setTitleWriter($login);
        }
        if ($description) {
            if (! empty($video->description)) {
                exit('Already writed by '.$video->description_writer);
            }
            $video->setDescription($title);
            $video->setDescriptionWriter($login);
        }
        pre($video);
    }
}
