<?php

namespace MegawebV1;

/*

методы:

getVideos()
getRows() - кол-во строк результата без лимита
setWithTitle(bool) - видео с заголовками/без boolean
setWithDescription(bool) - видео с описанием и без boolean
setWithDomain(bool) - с доменом или без boolean
setWithBookedDomain(bool) - с бронью или без boolean
setBookedDomain(домен) - домен брони
setPage(1) - номер страницы выдачи
setLimit(100) - лимит выдачи
setDomain(домен) - домен
setOrder(mv) - сортировка mv,mc,tr,ms,mr
setExist(bool) - видео с прямой ссылкой
setTags(массив русских тегов) - уст. теги по которым искать видео
setCategory(ID категории) - уст. категорию
setSameVideo(ID видео) - уст. видео для которого искать похожие
getOrders() - массив доступных сортировок (прим. mr=>most recent)
getPage() - возвр. номер страницы выдачи
setGroupByBooking() - уст. выдачу с группировкой по брони
getBookings() - возвр. список видео сгруппированные по брони
setID(ID) - уст. ID видео, котрое нужно найти
getID() - возвр ID видео по которому идет поиск
getDomain() - возвр домен по которому идет поиск
setExclude() - уст ID видео который исключить из поиска (для похожих)
*/

class videos
{
    public $withStarTags;

    public $excludeID;

    public $id;

    public $groupByBooking;

    public $tags;

    public $exist;

    public $order;

    public $domain;

    public $limit;

    public $page;

    public $withDomain;

    public $withDescription;

    public $withTitle;

    public $bookedDomain;

    public $withBookedDomain;

    public function __construct()
    {
        // null - все, true - с заголовком, false - без заголовка
        $this->withTitle = null;
        // null - все, true - с описанием, false - без описания
        $this->withDescription = null;
        // null - все, true - с доменом, false - без домена
        $this->withDomain = null;
        // null - все, true - с бронью, false - без брони
        $this->withBookedDomain = null;
        $this->bookedDomain = null;
        // номер страницы выдачи
        $this->page = null;
        // лимит выдачи
        $this->limit = null;
        $this->domain = null;
        // сортировка (mr,mv,mc,tr,ms)
        $this->order = null;
        // pre(var_dump($this));
        // только видео для которых есть прямая ссылка
        $this->exist = null;
        // тэги по которым искать видео
        $this->tags = null;
        // группировать по брони
        $this->groupByBooking = null;
        // вывести видео с определенным ID
        $this->id = null;
        // исключить определенный ID
        $this->excludeID = null;
        // только видео с тегами звезд
        $this->withStarTags = null;
    }

    public function getVideos()
    {
        if (! isset($this->page)) {
            error('Page number is required');
        }
        if (! isset($this->limit)) {
            error('Limit is required');
        }
        $joins = [
            'videos_views' => 'left join '.typetab('videos_views').' on tags.video_id=videos_views.video_id',
            'tags' => 'join '.typetab('tags').' on videos.id=tags.video_id',
        ];
        $do['join_videos_views'] = false;
        $do['join_tags'] = false;
        $do['select_views'] = false;
        $do['select_votes'] = false;
        $do['select_rating'] = false;
        $do['select_same_tags_q'] = false;
        $do['select_rating_sko'] = false;
        $do['select_q_comments'] = false;
        $do['select_count'] = false;
        $do['group_by_booking'] = false;
        $do['select_booking'] = false;
        $do['select_video_id'] = true;
        $do['group_by_id'] = false;
        $do['join_tags_translation'] = false;
        $orders = [
            'mv' => [
                'order_by' => 'views desc',
                'joins' => ['videos_views'],
                'selects' => ['views'],
            ],
            'mr' => [
                'order_by' => 'videos.apply_date desc',
                'joins' => [],
                'selects' => [],
            ],
            'tr' => [
                'order_by' => 'rating desc, votes desc',
                'joins' => ['videos_views'],
                'selects' => ['rating'],
            ],
            'ms' => [
                'order_by' => 'same_tags_q desc, rating_sko desc',
                'joins' => ['tags', 'videos_views'],
                'selects' => ['same_tags_q', 'rating_sko'],
            ],
            'mc' => [
                'order_by' => 'q_comments desc',
                'joins' => [],
                'selects' => ['q_comments'],
            ],
            'grabber date' => [
                'order_by' => 'videos.add_date desc',
                'joins' => [],
                'selects' => [],
            ],
            'title date' => [
                'order_by' => 'videos.translate_date desc',
                'joins' => [],
                'selects' => [],
            ],
            'description date' => [
                'order_by' => 'videos.description_translate_date desc',
                'joins' => [],
                'selects' => [],
            ],
        ];
        if ($this->order) {
            if (! isset($orders[$this->order])) {
                error('This order is unavailable');
            }
            foreach ($orders[$this->order]['joins'] as $v) {
                $do['join_'.$v] = true;
            }
            foreach ($orders[$this->order]['selects'] as $v) {
                $do['select_'.$v] = true;
            }
        }
        if ($this->tags) {
            $do['join_tags'] = true;
            $do['group_by_id'] = true;
        }
        if ($this->groupByBooking) {
            $do['select_count'] = true;
            $do['group_by_booking'] = true;
            $do['select_booking'] = true;
            $do['select_video_id'] = false;
        }
        if (isset($this->withStarTags)) {
            $do['join_tags'] = true;
            $do['join_tags_translation'] = true;
            $do['group_by_id'] = true;
        }
        $sql['order'] = $this->order ? 'order by '.$orders[$this->order]['order_by'] : '';
        // $sql['withStarTags']=is_null($this->withStarTags)?1:($this->withStarTags?'tags_translation.star':'not tags_translation.star');
        // придумать как находить видео без звезд. если писать not tags_translation.star то все равно выводятся видео со свездами
        // т.к. они содержат и другие теги
        $sql['withStarTags'] = $this->withStarTags ? 'tags_translation.star' : 1;
        $sql['tags'] = $this->tags ? 'tags.tag_ru in ('.array_to_str($this->tags).')' : 1;
        $sql['group_by_id'] = $do['group_by_id'] ? 'group by videos.id' : '';
        $sql['group_by_booking'] = $do['group_by_booking'] ? 'group by booked_domain' : '';
        $sql['join_videos_views'] = $do['join_videos_views'] ? 'left join '.typetab('videos_views').' on videos.id=videos_views.video_id' : '';
        $sql['join_tags'] = $do['join_tags'] ? 'join '.typetab('tags').' on videos.id=tags.video_id' : '';
        $sql['join_tags_translation'] = $do['join_tags_translation'] ? 'join '.typetab('tags_translation').' on tags_translation.tag_en=tags.tag_en' : '';
        $sql['select_video_id'] = $do['select_video_id'] ? 'videos.id' : '';
        $sql['select_booking'] = $do['select_booking'] ? ', videos.booked_domain' : '';
        $sql['select_count'] = $do['select_count'] ? 'count(*) as q' : '';
        $sql['select_views'] = $do['select_views'] ? ', ifnull(videos_views.views,0) as views' : '';
        $sql['select_votes'] = $do['select_votes'] ? ', ifnull(votes,0) as votes' : '';
        $sql['select_rating'] = $do['select_rating'] ? ', ifnull(rating,0) as rating' : '';
        $sql['select_same_tags_q'] = $do['select_same_tags_q'] ? ', count(tags.video_id) as same_tags_q' : '';
        $sql['select_rating_sko'] = $do['select_rating_sko'] ? ', ((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko' : '';
        $sql['select_q_comments'] = $do['select_q_comments'] ? ', (select count(*) from '.typetab('comments').' where video_id=videos.id) as q_comments' : '';
        //
        // $sql['order']=($this->order)?('order by '.$orders[$this->order]):'';
        $sql['withTitle'] = is_null($this->withTitle) ? '1' : (($this->withTitle) ? '(videos.title_ru is not null and videos.title_ru!=\'\')' : '(videos.title_ru is null or videos.title_ru=\'\')');
        $sql['withDescription'] = is_null($this->withDescription) ? '1' : (($this->withDescription) ? '(videos.description is not null and videos.description!=\'\')' : '(videos.description is null or videos.description=\'\')');
        $sql['domain'] = is_null($this->domain) ? '1' : 'videos.domain=\''.$this->domain.'\'';
        $sql['id'] = is_null($this->id) ? '1' : 'videos.id=\''.$this->id.'\'';
        // pre(var_dump($this));
        $sql['bookedDomain'] = is_null($this->bookedDomain) ? '1' : 'videos.booked_domain=\''.$this->bookedDomain.'\'';
        $sql['withDomain'] = is_null($this->withDomain) ? '1' : ($this->withDomain ? 'videos.domain is not null' : 'videos.domain is null');
        $sql['withBookedDomain'] = is_null($this->withBookedDomain) ? '1' : ($this->withBookedDomain ? 'videos.booked_domain is not null' : 'videos.booked_domain is null');
        $sql['from'] = ($this->page - 1) * $this->limit;
        $sql['exist'] = is_null($this->exist) ? '1' : ($this->exist ? 'videos.exist' : 'not videos.exist');
        $sql['excludeID'] = $this->excludeID ? 'videos.id!=\''.$this->excludeID.'\'' : 1;
        // не забыть использовать exist
        $arr = query_arr('select SQL_CALC_FOUND_ROWS
			'.$sql['select_video_id'].'
			'.$sql['select_count'].'
			'.$sql['select_booking'].'
			'.$sql['select_views'].'
			'.$sql['select_votes'].'
			'.$sql['select_rating'].'
			'.$sql['select_same_tags_q'].'
			'.$sql['select_rating_sko'].'
			'.$sql['select_q_comments'].'
			from '.typetab('videos').'
			'.$sql['join_videos_views'].'
			'.$sql['join_tags'].'
			'.$sql['join_tags_translation'].'
			where 
			'.$sql['id'].' and
			'.$sql['withTitle'].' and 
			'.$sql['withDescription'].' and 
			'.$sql['withDomain'].' and 
			'.$sql['withBookedDomain'].' and 
			'.$sql['domain'].' and 
			'.$sql['bookedDomain'].' and 
			'.$sql['exist'].' and
			'.$sql['tags'].' and
			'.$sql['withStarTags'].' and
			'.$sql['excludeID'].'
			'.$sql['group_by_id'].'
			'.$sql['group_by_booking'].'
			'.$sql['order'].'
			limit '.$sql['from'].','.$this->limit.'
			;');
        $object = new \stdClass;
        $object->rows = rows_without_limit();
        $object->array = $arr;

        return $object;
    }

    public function getRows()
    {
        return $this->getVideos()->rows;
    }

    public function setWithTitle($b)
    {
        $this->withTitle = $b;
    }

    public function setWithDescription($b)
    {
        $this->withDescription = $b;
    }

    public function setWithDomain($b)
    {
        $this->withDomain = $b;
    }

    public function setWithBookedDomain($b)
    {
        $this->withBookedDomain = $b;
    }

    public function setBookedDomain($domain)
    {
        $this->bookedDomain = $domain;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function setExist($b = null)
    {
        $this->exist = $b;
    }

    public function setTags($tags = [])
    {
        $this->tags = is_array($tags) ? $tags : error('setTags value should be an array');
    }

    public function setCategory($categoryID = null)
    {
        if (! $categoryID) {
            return false;
        }
        // if (empty($categoryID)) {error('Category ID required');};
        $category = new category($categoryID);
        $this->setTags($category->getTags());
    }

    public function setSameVideo($videoID = null)
    {
        $video = new video($videoID);
        $this->setTags($video->getTags());
        $this->setOrder('ms');
        $this->setExcludeID($videoID);
    }

    public function setExcludeID($videoID = null)
    {
        $this->excludeID = $videoID;
    }

    public static function getOrders()
    {
        $arr = [
            'mr' => 'most recent',
            'mv' => 'most viewed',
            'tr' => 'top rated',
            'ms' => 'most same',
            'mc' => 'most commented',
            'grabber date' => 'by grabber date',
            'title date' => 'by title date',
            'description date' => 'by description date',
        ];

        return $arr;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setGroupByBooking($b)
    {
        $this->groupByBooking = $b;
    }

    public function getBookings()
    {
        $this->setGroupByBooking(true);
        $obj = $this->getVideos();
        // foreach ($obj->array as $v) {};
        // pre($obj->array);
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id = null)
    {
        $this->id = $id;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setWithStarTags($b = null)
    {
        $this->withStarTags = $b;
    }

    public function getFreeVideosWithTitleWithoutDescription()
    {
        $this->setWithDomain(false);
        $this->setWithTitle(true);
        $this->setWithDescription(false);

        return $this->getVideos();
    }

    public function getFreeVideosWithTitleAndDescription()
    {
        $this->setWithDomain(false);
        $this->setWithTitle(true);
        $this->setWithDescription(true);

        return $this->getVideos();
    }

    public function getFreeNotBookedVideosWithTitleWithoutDescription()
    {
        $this->setWithDomain(false);
        $this->setWithBookedDomain(false);
        $this->setWithTitle(true);
        $this->setWithDescription(false);

        return $this->getVideos();
    }

    public function getFreeNotBookedVideosWithTitleAndDescription()
    {
        $this->setWithDomain(false);
        $this->setWithBookedDomain(false);
        $this->setWithTitle(true);
        $this->setWithDescription(true);

        return $this->getVideos();
    }

    public function updateLocalIds()
    {
        if (! $this->domain) {
            error('Domain is required to update Local Ids');
        }
        update_local_ids([
            'table' => typetab('videos'),
            'where_clause' => 'domain=\''.$this->domain.'\'',
        ]);
    }
}
