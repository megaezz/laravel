<?php

namespace MegawebV1;

class category
{
    public function __construct($id = null)
    {
        $this->id = $id ? $id : error('ID required to create domainCategory class');
        // $arr=cacheObject($this,'getInfo',null,0);
        $arr = query_assoc('
			select id,name,name_translit,group_id,name_url,star,type
			from '.typetab('categories_groups').' 
			where id=\''.$this->id.'\'
			;');
        if (! $arr) {
            error('Category doesn\'t exist');
        }
        $this->name = $arr['name'];
        $this->nameTranslit = $arr['name_translit'];
        $this->groupID = $arr['group_id'];
        $this->nameURL = $arr['name_url'];
        $this->type = $arr['type'];
        $this->isStar = ($arr['type'] == 'star') ? true : false;
        $this->isSerial = ($arr['type'] == 'serial') ? true : false;
    }

    public function getTags()
    {
        $tags = query_arr('
				select catgroups_tags.tag_ru
				from '.typetab('categories_groups').'
				join  '.typetab('catgroups_tags').' on catgroups_tags.group_id = categories_groups.group_id
				where categories_groups.id=\''.$this->id.'\'
				');
        $video_tags = [];
        foreach ($tags as $v) {
            $video_tags[] = $v['tag_ru'];
        }

        return $video_tags;
    }
}
