<?php

namespace MegawebV1;

class domainCategoriesWriter extends domainCategories
{
    public function __construct($domain = null)
    {
        parent::__construct($domain);
        $this->listTemplate = null;
    }

    public function get()
    {
        if (! $this->listTemplate) {
            exit('Set template for list of categories first');
        }
        $categories = parent::get();
        $arr = $categories->array;
        $rows = $categories->rows;
        // pre($arr);
        $list = false;
        foreach ($arr as $v) {
            $category = new domainCategory($v['id'], $this->domain);
            temp_var('name', $category->name);
            temp_var('id', $category->id);
            $list .= template($this->listTemplate);
        }

        return $list;
    }

    public function getSelectList()
    {
        $this->setTemplate('admin/videos/categories_option');

        return $this->get();
    }

    public function setTemplate($path = null)
    {
        $this->listTemplate = $path;
    }
}
