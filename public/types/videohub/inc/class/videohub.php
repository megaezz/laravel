<?php

namespace MegawebV1;

class videohub extends megaweb
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function getDomains()
    {
        return query_arr('select domain from '.tabname('engine', 'domains').' where type=\'videohub\' order by domain;');
    }
}
