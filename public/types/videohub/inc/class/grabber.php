<?php

namespace MegawebV1;

class grabber
{
    public function __construct($args = [])
    {
        $url = empty($args['url']) ? error('URL is required') : $args['url'];
        $this->url = $url;
        $this->page = filegetcontents($url);
        // pre($this);
    }

    public static function setUrl($args = [])
    {
        $url = empty($args['url']) ? error('URL is required') : $args['url'];
        $parse_url = parse_url($url);
        if (isset($parse_url['host']) == 0) {
            error('URL wasn\'t recognized');
        }
        $site = str_replace('www.', '', $parse_url['host']);
        if ($site == 'xvideos.com') {
            $grabber = new xvideos(['url' => $url]);
        }
        if ($site == 'pornhub.com') {
            $grabber = new pornhub(['url' => $url]);
        }
        if (empty($grabber)) {
            error('Link wasn\'t recognized');
        }

        return $grabber;
    }
}
