<?php

namespace MegawebV1;

/*
�������� �����

list() - ������ �����
getDomainsSelect() - ������ ������� ��� select
getBookedDomainsSelect() - ������ ������� ����� ��� select
getOrdersSelect() - ������ ���������� ��� select
page() - ��������

*/

class videosPage extends videosWriter
{
    public function __construct()
    {
        // pre(var_dump($_POST));
        parent::__construct();
        // ������� ���������� true �.�. �� ��������� ������
        $this->urlPattern = true;
        $this->paginatorTemplate = 'pagination/bootstrap';
        $this->setPage((! empty($_POST['p']) and is_numeric($_POST['p'])) ? checkstr($_POST['p']) : 1);
        $this->setLimit(empty($_POST['limit']) ? 100 : checkstr($_POST['limit']));
        $this->setDomain(empty($_POST['domain']) ? null : checkstr($_POST['domain']));
        $this->setBookedDomain(empty($_POST['bookedDomain']) ? null : checkstr($_POST['bookedDomain']));
        $this->setOrder(empty($_POST['order']) ? null : checkstr($_POST['order']));
        $this->setID(empty($_POST['id']) ? null : checkstr($_POST['id']));
        $this->setCategory(empty($_POST['category']) ? null : checkstr($_POST['category']));
        if (isset($_POST['withDomain'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withDomain']])) {
                $this->setWithDomain($arr[$_POST['withDomain']]);
            }
        }
        if (isset($_POST['withBookedDomain'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withBookedDomain']])) {
                $this->setWithBookedDomain($arr[$_POST['withBookedDomain']]);
            }
        }
        if (isset($_POST['withTitle'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withTitle']])) {
                $this->setWithTitle($arr[$_POST['withTitle']]);
            }
        }
        if (isset($_POST['withDescription'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withDescription']])) {
                $this->setWithDescription($arr[$_POST['withDescription']]);
            }
        }
        if (isset($_POST['exist'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['exist']])) {
                $this->setExist($arr[$_POST['exist']]);
            }
        }
        if (isset($_POST['withStarTags'])) {
            $arr = ['false' => false, 'true' => true];
            if (isset($arr[$_POST['withStarTags']])) {
                $this->setWithStarTags($arr[$_POST['withStarTags']]);
            }
        }
        // $this->setGroupByBooking(true);
        $this->listTemplate = 'admin/videos/list';
    }

    public function list()
    {
        // pre(var_dump($this));
        $getVideos = $this->getVideos();
        $arr = $getVideos->array;
        $rows = $getVideos->rows;
        $list = '';
        foreach ($arr as $v) {
            $video = new videoWriter($v['id']);
            // pre($video);
            temp_var('thumb', $video->thumb);
            temp_var('id', $video->id);
            temp_var('title_en', $video->title_en);
            temp_var('add_date', $video->add_date);
            temp_var('tags', $video->getRuTagsStr());
            temp_var('player', $video->getEmbedPlayer());
            temp_var('domain', empty($video->domain) ? '' : $video->domain);
            temp_var('bookedDomain', empty($video->bookedDomain) ? '' : $video->bookedDomain);
            temp_var('hub', $video->hub);
            temp_var('card_bg', $video->domain ? 'light' : ($video->title ? 'success' : 'warning'));
            // temp_var('');
            $list .= template($this->listTemplate);
        }

        return $list;
    }

    public function getDomainsSelect()
    {
        $arr = videohub::getDomains();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<option value="'.$v['domain'].'">'.$v['domain'].'</option>';
        }

        return $list;
    }

    public function getBookedDomainsSelect()
    {
        $arr = videohub::getDomains();
        $list = '';
        foreach ($arr as $v) {
            $list .= '<option value="'.$v['domain'].'">'.$v['domain'].'</option>';
        }

        return $list;
    }

    public function getOrdersSelect()
    {
        $ru = [
            'mr' => '���� ���������� �� ����',
            'mv' => '����������',
            'mc' => '�����������',
            'tr' => '��������',
            'ms' => '�������� � ��������� �����',
            'grabber date' => '���� ��������',
            'title date' => '���� ��������� ���������',
            'description date' => '���� ��������� ��������',
        ];
        $arr = videos::getOrders();
        unset($arr['ms']);
        $list = '';
        foreach ($arr as $k => $v) {
            $list .= '<option value="'.$k.'">'.(empty($ru[$k]) ? $v : $ru[$k]).'</option>';
        }

        return $list;
    }

    public function page()
    {
        temp_var('getPagesBootstrap', $this->getPagesBootstrap());
        temp_var('list', $this->list());
        temp_var('getRows', $this->getRows());

        return template('admin/videos/videos');
    }

    public function changeDomainForm()
    {
        return template('admin/videos/change_domain_form');
    }

    public function changeBookedDomainForm()
    {
        return template('admin/videos/change_booked_domain_form');
    }

    public function submit_change_domain()
    {
        authorizate_videohub_root();
        if (! isset($_POST['changeDomain'])) {
            error('changeDomain is required to submit change domain');
        }
        $domain = empty($_POST['changeDomain']['domain']) ? null : checkstr($_POST['changeDomain']['domain']);
        $arr = $this->getVideos()->array;
        $i = 0;
        foreach ($arr as $v) {
            $video = new video($v['id']);
            $b = $video->setDomain($domain);
            // ���� ����� ���������� � ������� ����� �� null �� ������� ��������������, ��� ������� ����� �������� �����. ��� �������������� ��������� ����� ������
            if (! $b) {
                exit('Task is stopped on video #'.$v['id'].'. You can change domain only for videos with null domain for security reasons. Please first change domain to null.');
            }
            $i++;
        }
        exit('Changed domain of '.$i.' videos');
    }

    public function submit_change_booked_domain()
    {
        authorizate_videohub_root();
        if (! isset($_POST['changeBookedDomain'])) {
            error('changeBookedDomain is required to submit change domain');
        }
        $domain = empty($_POST['changeBookedDomain']['domain']) ? error('Domain for change required') : checkstr($_POST['changeBookedDomain']['domain']);
        $arr = $this->getVideos()->array;
        $i = 0;
        foreach ($arr as $v) {
            $video = new video($v['id']);
            $video->setBookedDomain($domain);
            $i++;
        }
        exit('Changed booked domain of '.$i.' videos');
    }

    public function getDomainCategoriesSelect()
    {
        if (! $this->domain) {
            return false;
        }
        $categories = new domainCategoriesWriter($this->domain);
        temp_var('getDomainCategoriesSelect', $categories->getSelectList());
        temp_var('getDomain', $this->domain);

        return template('admin/videos/domain_categories');

    }

    public function getCategoriesSelect()
    {
        $categories = new categoriesWriter;

        return $categories->getSelectList();
    }

    public function freeVideosStat()
    {
        temp_var('getFreeVideosWithTitleWithoutDescription', $this->getFreeVideosWithTitleWithoutDescription()->rows);
        temp_var('getFreeVideosWithTitleAndDescription', $this->getFreeVideosWithTitleAndDescription()->rows);
        temp_var('getFreeNotBookedVideosWithTitleWithoutDescription', $this->getFreeNotBookedVideosWithTitleWithoutDescription()->rows);
        temp_var('getFreeNotBookedVideosWithTitleAndDescription', $this->getFreeNotBookedVideosWithTitleAndDescription()->rows);

        return template('admin/videos/free_videos_stat');
    }
}
