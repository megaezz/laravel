<?php

namespace MegawebV1;

class domainCategories extends categories
{
    public function __construct($domain = null)
    {
        parent::__construct();
        $this->domain = $domain;
        if (! $this->domain) {
            error('Domain is required to create domainCategories');
        }
        // null - любые, true - активные, false - не активные
        $this->active = null;
    }

    public function get()
    {
        $sql['type'] = is_null($this->type) ? 1 : 'cg.type=\''.$this->type.'\'';
        $sql['active'] = is_null($this->active) ? 1 : ($this->active ? 'cd.active=\'1\'' : 'cd.active=\'0\'');
        $sql['domain'] = 'cd.domain=\''.$this->domain.'\'';
        $arr = query_arr('
			select cg.id
			from '.typetab('categories_domains').' cd
			join '.typetab('categories_groups').' cg on cg.id=cd.category_id
			where
			'.$sql['domain'].' and
			'.$sql['active'].' and
			'.$sql['type'].'
			order by cg.type, cd.active desc, cg.name
			');
        $object = new stdClass;
        $object->array = $arr;
        $object->rows = rows_without_limit();

        return $object;
    }

    public function setActive($active = null)
    {
        $this->active = $active;
    }
}
