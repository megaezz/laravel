<?php

namespace MegawebV1;

/*

getInfo() - заполняет объект данными о видео из базы
checkHub() - проверяет существует ли хаб указанный в объекте (не понятно зачем нужно, проверить где используется)
getCategories() - возвр категории видео
getCategoriesCached() - кэш
getStars() - возвращает звезд видео
getStarsCached() - кэш
getTags() - возвр список тегов для видео
getTagsCached() - кэш
getSame(array()) - возвращает список похожих видео
getSameCached($args=array()) - кэш
setVar(поле,значение) - менчет значение в таблице
addTag($tag_en) - добавляет тэг для видео в таблицу
setThumb($url) - устанавлиает тумбу для видео
addVideosFromGrabber($grabber) - записывает данные переданные граббером
getDirectLink() - получает прямую ссылку
getDirectLinkCached() - кэш
getDirectLinkSame() - получает прямую ссылку, если ее нет, то похожее видео из 10
getDirectLinkSameCached() - кэш
checkConfigDomain() - проверяет относится ли видео к домену из конфига
getObjectByDomainAndTitleTranslit($domain=null,$titleTranslit=null) - получает ID видео по домену и транслиту
setID($id=null) - уст. ID видео в объекте
setExist($b=null) - уст. в базе значение exist

*/

class video
{
    public $id;

    public $thumb;

    public $domain;

    public $hub;

    public $id_hub;

    public $add_date;

    public $apply_date;

    public $title_en;

    public $title_ru;

    public $title;

    public $title_alt;

    public $title_translit;

    public $title_writer;

    public $description;

    public $description_writer;

    public $ph_viewkey;

    public $ph_date;

    public $bookedDomain;

    public $is_deleted;

    public $source_url;

    public $local_id;

    public $deleted_from_hub;

    public $title_date;

    public $description_date;

    public function __construct($id = null)
    {
        if ($id) {
            $this->setID($id);
        }
        if (empty($this->id)) {
            error('ID required to create video class');
        }
        $this->thumb = '/types/videohub/images/'.$this->id.'.jpg';
        $arr = cacheObject($this, 'getInfo', null, 0);
        if (! $arr) {
            error('Video doesn\'t exist');
        }
        $this->id = $arr['id'];
        $this->domain = $arr['domain'];
        $this->hub = $arr['hub'];
        $this->checkHub();
        $this->id_hub = $arr['id_hub'];
        $this->add_date = $arr['add_date'];
        $this->apply_date = $arr['apply_date'];
        $this->title_en = $arr['title_en'];
        $this->title_ru = $arr['title_ru'];
        $this->title = $arr['title_ru'];
        $this->title_alt = $arr['title_ru_alt'];
        $this->title_translit = $arr['title_ru_translit'];
        $this->title_writer = $arr['translater'];
        $this->title_date = $arr['translate_date'];
        $this->description = $arr['description'];
        $this->description_writer = $arr['description_translater'];
        $this->description_date = $arr['description_translate_date'];
        $this->ph_viewkey = $arr['ph_viewkey'];
        $this->ph_date = $arr['ph_date'];
        $this->bookedDomain = $arr['booked_domain'];
        $this->is_deleted = $arr['deleted'];
        $this->source_url = $arr['source_url'];
        $this->local_id = $arr['local_id'];
        $this->deleted_from_hub = $arr['deleted_from_hub'];
    }

    public function getInfo()
    {
        $arr = query_assoc('select id,domain,hub,id_hub,add_date,apply_date,title_en,title_ru,title_ru_alt,
			title_ru_translit,translater,translate_date,description,description_translater,description_translate_date,
			ph_viewkey,ph_date,booked_domain,deleted,source_url,local_id,deleted_from_hub
		 from '.typetab('videos').' where id=\''.$this->id.'\';');

        return $arr;
    }

    public function checkHub()
    {
        if (! in_array($this->hub, ['pornhub.com', 'xvideos.com', 'hardsextube.com', 'paradisehill.tv'])) {
            error('Video #'.$this->id.'. Hub '.$this->hub.' does not exist.');
        }
    }

    public function getCategories()
    {
        return preparing_categories_of_video(['video_id' => $this->id, 'stars' => 0]);
    }

    public function getCategoriesCached()
    {
        return cacheObject($this, 'getCategories');
    }

    public function getStars()
    {
        return preparing_categories_of_video(['video_id' => $this->id, 'stars' => 1]);
    }

    public function getStarsCached()
    {
        return cacheObject($this, 'getStars');
    }

    public function getTags()
    {
        $tags = preparing_video_tags_list_array(['id' => $this->id]);
        $video_tags = [];
        foreach ($tags as $v) {
            $video_tags[] = $v['tag_ru'];
        }

        return $video_tags;
    }

    public function getTagsCached()
    {
        return cacheObject($this, 'getTags');
    }

    public function getSame($args = [])
    {
        $limit = empty($args['limit']) ? 10 : $args['limit'];
        $page = empty($args['page']) ? 1 : $args['page'];
        $videos = new videos;
        $videos->setDomain($this->domain);
        $videos->setPage($page);
        $videos->setLimit($limit);
        $videos->setExist(true);
        $videos->setSameVideo($this->id);

        return $videos->getVideos()->array;
    }

    public function getSameCached($args = [])
    {
        return cacheObject($this, 'getSame', $args, 2592000);
    }

    public function setVar($field, $value)
    {
        $sql_value = is_null($value) ? 'null' : '\''.$value.'\'';
        query('update '.typetab('videos').' set '.$field.'='.$sql_value.' where id=\''.$this->id.'\';');

        return true;
    }

    public function addTag($tag_en)
    {
        query('insert '.typetab('tags').' set tag_en=\''.$tag_en.'\', video_id=\''.$this->id.'\';');

        return true;
    }

    public function setThumb($url)
    {
        global $config;
        copytoserv($url, $config['videohub']['path']['images'].$this->id.'.jpg');
    }

    public static function addVideosFromGrabber($grabber)
    {
        // pre($args);
        $hub = empty($grabber->hub) ? error('Hub is required') : $grabber->hub;
        $source_url = empty($grabber->url) ? error('Source url is required') : $grabber->url;
        // pre($grabber->getVideos());
        foreach ($grabber->getVideos() as $args) {
            $tags = empty($args['tags']) ? error('Tags are required') : $args['tags'];
            $img_src = empty($args['img_src']) ? error('IMG src is required') : $args['img_src'];
            $id_hub = empty($args['id_hub']) ? error('Hub ID is required') : $args['id_hub'];
            $title_en = empty($args['title_en']) ? error('Title EN is required') : $args['title_en'];
            $booked_domain = empty($args['booked_domain']) ? null : $args['booked_domain'];
            $viewkey = empty($args['viewkey']) ? null : $args['viewkey'];
            $ph_date = empty($args['ph_date']) ? null : $args['ph_date'];
            query('insert into '.typetab('videos').' () values ()');
            $video_id = mysql_insert_id();
            $video = new video(['id' => $video_id]);
            $video->setVar('hub', $hub);
            $video->setVar('id_hub', $id_hub);
            $video->setVar('title_en', checkstr($title_en));
            $video->setVar('source_url', $source_url);
            $video->setVar('booked_domain', $booked_domain);
            $video->setVar('ph_viewkey', $viewkey);
            $mysql_tags_values = '';
            foreach ($tags as $tag) {
                $video->addTag(checkstr($tag));
            }
            $video->setThumb($img_src);
        }
    }

    public function getDirectLink()
    {
        $link = false;
        if ($this->hub == 'pornhub.com') {
            $link = pornhub::getDirectLink(['ph_viewkey' => $this->ph_viewkey]);
        }
        if ($this->hub == 'xvideos.com') {
            $link = xvideos::getDirectLink(['id_hub' => $this->id_hub]);
        }
        // if ($this->hub=='hardsextube.com') {$link=hardsextube::getDirectLink(array('id_hub'=>$this->id_hub));};
        $this->setExist($link ? true : false);

        return $link;
    }

    public function getDirectLinkCached()
    {
        return cacheObject($this, 'getDirectLink', null, 1200);
    }

    public function getDirectLinkSame()
    {
        $link = $this->getDirectLinkCached();
        if ($link) {
            return $link;
        }
        // pre(var_dump($link));
        // $video=new videoCached($this->id);
        // $same_arr=$video->getSame(array('sort'=>'ms','limit'=>10))->array;
        // $same_arr=$this->getSame(array('sort'=>'ms','limit'=>10))->array;
        $same_arr = $this->getSameCached(['sort' => 'ms', 'limit' => 5]);
        // pre($same_arr);
        foreach ($same_arr as $v) {
            $video = new video($v['id']);
            $link = $video->getDirectLinkCached();
            if ($link) {
                return $link;
            }
        }

        // pre(var_dump($link));
        return $link;
    }

    public function getDirectLinkSameCached()
    {
        return cacheObject($this, 'getDirectLinkSame', null, 1200);
    }

    public function checkConfigDomain()
    {
        global $config;
        if ($this->domain != $config['domain']) {
            error('Wrong domain');
        }
    }

    public static function getObjectByDomainAndTitleTranslit($domain = null, $titleTranslit = null)
    {
        if (empty($domain)) {
            error('Specify domain');
        }
        if (empty($titleTranslit)) {
            error('Specify titleTranslit');
        }
        $arr = query_assoc('select id from '.typetab('videos').' 
			where domain=\''.$domain.'\' and title_ru_translit=\''.$titleTranslit.'\'
			;');
        if (! $arr) {
            error('Video was not found');
        }

        return new video($arr['id']);
    }

    public function setID($id = null)
    {
        $this->id = $id;
    }

    public function setExist($b = null)
    {
        $this->setVar('exist', $b ? 1 : 0);
    }

    public function setApplyDate()
    {
        query('update '.typetab('videos').' set apply_date=current_timestamp where id=\''.$this->id.'\';');
    }

    public function setDomain($domain = null)
    {
        // если домен установлен и передан домен не null то не выполняем задачу, для предотвращения случайной смены домена
        if ($this->domain and $domain) {
            return false;
        }
        // if (!$domain) {die('Domain is required');};
        // обнуляем local_id
        $this->setVar('local_id', null);
        // устанавливаем дату добавления на сайт
        $this->setApplyDate();
        // меняем домен
        $this->setVar('domain', $domain);
        // обновляем local_id если домен не null
        if ($domain) {
            $videos = new videos;
            $videos->setDomain($domain);
            $videos->updateLocalIds();
        }

        return true;
    }

    public function setBookedDomain($domain = null)
    {
        $this->setVar('booked_domain', $domain);
    }

    public function redirectToDirectLink()
    {
        // $this->checkConfigDomain();
        $link = $this->getDirectLinkSameCached();
        if (! $link) {
            return 'Unable to get direct link';
        }
        header('Location: '.$link);
        exit();
    }

    public function getVideoInfo()
    {
        return '<pre>'.print_r($this).'</pre>';
    }
}
