<?php

namespace MegawebV1;

class categories
{
    public function __construct()
    {
        // null - любые
        $this->type = null;
    }

    public function get()
    {
        $sql['type'] = is_null($this->type) ? 1 : 'cg.type=\''.$this->type.'\'';
        $arr = query_arr('
			select cg.id
			from '.typetab('categories_groups').' cg
			where
			'.$sql['type'].'
			order by cg.type, cg.name
			;');
        $object = new stdClass;
        $object->array = $arr;
        $object->rows = rows_without_limit();

        return $object;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function setStar($b = null)
    {
        $this->type = $b ? 'star' : null;
    }
}
