<?php

namespace MegawebV1;

/*

�������� ������� ��� �����

getPages() - ���������
getPagesBootstrap() - ��������� ��� ��������

*/

class videosWriter extends videos
{
    public function __construct()
    {
        parent::__construct();
        // ������� ������ N- (:num)
        $this->urlPattern = null;
        // ������ �������� ����������
        $this->paginationTemplateList = 'pagination/bootstrap';
        $this->paginationTemplateNext = 'pagination/next';
        $this->paginationTemplatePrev = 'pagination/prev';
    }

    public function getPagesBootstrap()
    {
        // if (!$this->urlPattern) {error('UrlPattern required');};
        $rows = $this->getVideos()->rows;
        $Paginator = new Paginator($rows, $this->limit, $this->page, $this->urlPattern);
        $Paginator->setMaxPagesToShow(5);
        $arr = $Paginator->getPages();
        $list = '';
        temp_var('disabled', $Paginator->getPrevPage() ? '' : 'disabled');
        temp_var('num', $Paginator->getPrevPage() ? $Paginator->getPrevPage() : '');
        $list .= template($this->paginationTemplatePrev);
        foreach ($arr as $page) {
            temp_var('active', $page['isCurrent'] ? 'active' : '');
            temp_var('num', $page['num']);
            temp_var('disabled', $page['url'] ? '' : 'disabled');
            temp_var('url', empty($page['url']) ? '#' : $page['url']);
            $list .= template($this->paginationTemplateList);
            clear_temp_vars();
        }
        temp_var('disabled', $Paginator->getNextPage() ? '' : 'disabled');
        temp_var('num', $Paginator->getNextPage() ? $Paginator->getNextPage() : '');
        $list .= template($this->paginationTemplateNext);

        // temp_var('list',$list);
        // $container=template('paginator/container');
        return $list;
    }

    public function getPages()
    {
        // if (!$this->urlPattern) {error('UrlPattern required');};
        $rows = $this->getVideos()->rows;
        $result = new Paginator($rows, $this->limit, $this->page, $this->urlPattern);

        /*
        $result=page_numbers_alt(array(
            'rows'=>$videos->rows,
            'per_page'=>$this->limit,
            'page'=>$this->page,
            'link'=>$this->pagesLink
        ));
        */
        return $result;
    }
}
