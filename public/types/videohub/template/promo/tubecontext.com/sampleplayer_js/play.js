var FlashHttpRequest_ready;
var FlashHttpRequest_objects;
var FlashHttpRequest_counter;
if (typeof (FlashHttpRequest_counter) == 'undefined') {
    
    FlashHttpRequest_objects = new Object();
    FlashHttpRequest_counter = 0;
    function FlashHttpRequest_(parent) {
        this.parent = parent;
        this.id = FlashHttpRequest_counter++;
        FlashHttpRequest_objects[this.id] = this;
        var gateway;
        this.open = function (method, url) {
            gateway = document.getElementById("FlashHttpRequest_gateway");
            gateway.create(this.id, method, url);
        }
        this.send = function (content) {
            gateway.send(this.id, content);
        }
        this.handler = function (status, data) {
            var obj = this.parent ? parent : this;
            obj.readyState = 4;
            obj.responseText = data;
            obj.status = status;

            var id = this.id;
            setTimeout(function () {  // must release flash
                gateway.finished(id);
                obj.onreadystatechange.apply(obj);
            }, 10);
        }
    }
    function FlashHttpRequest_handler(id, status, data) {
        FlashHttpRequest_objects[id].handler(status, data);
    }
    function CrossXHR() {
        var obj;
        var queue = new Array();
        var max_wait = 100;
        var gateway = document.getElementById("FlashHttpRequest_gateway");
        if (gateway && gateway.create)
            if (typeof (FlashHttpRequest_ready) != 'undefined')
                obj = new FlashHttpRequest_(this);
        if (!obj) {

            var self = this;
            queue.push(function () { obj = new FlashHttpRequest_(self) });
            setTimeout(function () { self._process_queue() }, 100);
        }

        this.open = function (arg1, arg2) {
            if (obj)
                obj.open(arg1, arg2);
            else {
                queue.push(function () { obj.open(arg1, arg2) });
            }
        }
        this.send = function (arg1) {
            if (obj) {
                obj.send(arg1);
            } else {
                queue.push(function () {
                    obj.send(arg1)
                });
            }
        }
        this._process_queue = function () {
            gateway = document.getElementById("FlashHttpRequest_gateway");
            var ok = obj ? true : false;
            if (!ok)
                if (gateway && gateway.create)
                    if (typeof (FlashHttpRequest_ready) != 'undefined')
                        ok = true;
            if (!ok) {
                if (max_wait-- > 0)
                    setTimeout(function () { self._process_queue() }, 100);
            } else {
                while (queue.length > 0) {
                    var task = queue.shift();
                    task.apply(this);
                }
            }
        }
    }

    var SWFURL = 'http://' + window.location.hostname + ':' + window.location.port + '/types/videohub/template/promo/tubecontext.com/sampleplayer_js/crossxhr.swf';
    //alert(SWFURL);
    document.write('<span style="position:absolute;top:0;left:0"><span id="FlashHttpRequest_gateway"></span></span>');
    swfobject.embedSWF(SWFURL, "FlashHttpRequest_gateway", "1", "1", "9.0.0", "player.swf", {}, { wmode: 'transparent', allowscriptaccess: "always" });
}

var request;
function callback() {
    if (request.readyState == 4) {
        try {
            if (request.status != 200) {
                alert('error');
            } else {


                var xmlDoc = loadXMLString(request.responseText);
                var flvUrl = xmlDoc.getElementsByTagName("flv_url")[0].childNodes[0].nodeValue;  
                var imgUrl = "";              
                ShowPlayer(flvUrl, imgUrl);
            }
        } catch (e) {
            alert(e.message);
        }
    }
}

function show(id) {
    request = new CrossXHR();
    request.onreadystatechange = callback;
    request.open('GET', 'http://www.keezmovies.com/embed_player.php?id=' + id);
    request.send();
}

function loadXMLString(txt) {
    try {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(txt);
        return (xmlDoc);
    }
    catch (e) {
        try {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(txt, "text/xml");
            return (xmlDoc);
        }
        catch (e) {
            alert(e.message)
        }
    }
    return (null);
}
 