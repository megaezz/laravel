<?php

function EncodeHTML($Text)
{
    $Text = (get_magic_quotes_gpc()) ? htmlspecialchars(stripslashes($Text)) : htmlspecialchars($Text);
    $Text = str_replace([/* '&', */ '"', "'", '<', '>'], [/* '&amp;', */ '&quot;', '&apos;', '&lt;', '&gt;'], $Text);

    return $Text;
}

function XmlEncrypt($pt)
{
    $key = 'TubeContext@Player';
    $s = [];

    for ($i = 0; $i < 256; $i++) {
        $s[$i] = $i;
    }

    $j = 0;

    for ($i = 0; $i < 256; $i++) {
        $j = ($j + $s[$i] + ord($key[$i % strlen($key)])) % 256;
        $x = $s[$i];
        $s[$i] = $s[$j];
        $s[$j] = $x;
    }

    $i = 0;
    $j = 0;
    $ct = '';

    for ($y = 0; $y < strlen($pt); $y++) {
        $i = ($i + 1) % 256;
        $j = ($j + $s[$i]) % 256;
        $x = $s[$i];
        $s[$i] = $s[$j];
        $s[$j] = $x;
        $ct .= $pt[$y] ^ chr($s[($s[$i] + $s[$j]) % 256]);
    }

    return bin2hex($ct);
}

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Cache-Control: no-cache');
header('Content-type: text/xml; charset="utf-8"');

$LowVideoURL = EncodeHTML($_GET['v']);
// $MediumVideoURL = EncodeHTML($_GET['v2']);
// $HighVideoURL = EncodeHTML($_GET['v3']);
$AutoPlay = isset($_GET['aplay']) ? (int) $_GET['aplay'] : 0;
$ImageURL = EncodeHTML($_GET['i']);
$SiteURL = EncodeHTML('http://'.$_SERVER['HTTP_HOST'].'/');
$LogoURL = EncodeHTML('http://tubecontext.com/App_Themes/Account/img/logo.png');

$XmlSource = '<?xml version="1.0" encoding="UTF-8" ?>'."\n";
$XmlSource .= "<result>\n";
$XmlSource .= '<config smoothing="0" autostart="'.$AutoPlay.'" bufferlength="5" />'."\n";
$XmlSource .= '<video intro_pic="'.$ImageURL.'">'."\n";
$XmlSource .= '<logo open="_self" url="'.$SiteURL.'">'.$LogoURL.'</logo>'."\n";

$XmlSource .= '<file_320 open="_self" url="" default="1">'.$LowVideoURL.'</file_320>'."\n";

/**
 * Другие качества видео / Other High Queality of video
 * --------------------------------------------------
$XmlSource .= '<file_480 open="_self" url="" default="0">' . $MediumVideoURL . '</file_480>' . "\n";
$XmlSource .= '<file_720 open="_self" url="" default="0">' . $HighVideoURL . '</file_720>' . "\n";
 */

/**
 * Схожие видео / Related videos
 * --------------------------------------------------
$LinkOnPage = EncodeHTML("/");
$XmlSource .= '<related_videos target="_self">' . "\n";
$XmlSource .= '<header><![CDATA[<font color="#ffffff" size="16">Related videos</font>]]></header>';
$XmlSource .= '<video stars="4.5" time="00:10" views="1" added="07 May" title="Title of Sample Video 1" src="' . $ImageURL . '" url="' . $LinkOnPage . '" />' . "\n";
$XmlSource .= '<video stars="4.5" time="00:10" views="1" added="07 May" title="Title of Sample Video 2" src="' . $ImageURL . '" url="' . $LinkOnPage . '" />' . "\n";
$XmlSource .= "</related_videos>\n";
 * */
$XmlSource .= "</video>\n";
$XmlSource .= "</result>\n";
$XmlSource = XmlEncrypt($XmlSource);

exit($XmlSource);
