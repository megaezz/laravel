document.addEventListener('DOMContentLoaded',init);

function responsive_kt_player() {
  player=document.getElementById('kt_player');
  player.style.width='';
  player.style.height='';
  player.style.position='absolute';
  player_internal=document.getElementById('kt_player_internal');
  player_internal.style.width='';
  player_internal.style.height='';
  player_internal.className='embed-responsive-item';
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function init() {
  document.addEventListener('click',eventHandler);
}

//если roleHandler существует и event.target содержит data-role то выполнить обработчик
function eventHandler(event) {
  if ((typeof roleHandler!='undefined') && (event.target.dataset.role)) {roleHandler(event.target);};
}

function roleHandler(target) {
  if (target.dataset.role=='open_hide_comments') {
    open_hide_comments();
  };
}
function open_hide_comments() {
  el=document.getElementById('comments');
  txt=document.getElementById('comments_action');
  if (el.style.display=='none') {
    el.style.display='block';
    txt.innerHTML='Скрыть';
  } else {
    el.style.display='none';
    txt.innerHTML='Показать';
  };

}

//выполняет функцию func для каждого data-role="role"
function forEachRole(role,func) {
  roles=document.querySelectorAll('[data-role="'+role+'"]');
  [].forEach.call(roles,function(role) {
    func(role);
  });
}

function seconds_timer(from,block_id,hidden_text) {
  var obj=document.getElementById(block_id);
  if (obj.innerHTML=='') {obj.innerHTML=from;};
  obj.innerHTML--; 
  if (obj.innerHTML==0){
    setTimeout(function(){obj.innerHTML=hidden_text},1000);
  } else {
    setTimeout(function(){seconds_timer(from,block_id,hidden_text);},1000);
  };
}


function symbol_counter(text_block_id,counter_block_id,max_symbols) {
  counter_block=document.getElementById(counter_block_id);
  text_block=document.getElementById(text_block_id);
  text_length=text_block.value.length;
  counter_block.innerHTML=max_symbols-text_length;
}

function hide_elements(elements) {
  for (var v in elements) {
    if (document.getElementById(elements[v])) {
      // alert('Скрыли '+elements[v]);
      document.getElementById(elements[v]).style.display='none';
    };
  };
}

next_page_number=1;
function show_more_videos(block_id,show_more_button_id,list_type,sort,max_page,different,change_next_page_number) {
  // alert(next_page_number);
  next_page_number++;
  if (typeof(change_next_page_number)!='undefined') {
    real_next_page_number=change_next_page_number+next_page_number-2;
  } else {
    real_next_page_number=next_page_number;
  };
  // alert(next_page_number);
  // alert(sort);
  if (list_type=='all') {var link='/?r=apiVideo/getVideos&p='+real_next_page_number+'&order='+sort;};
  if (list_type=='category') {var link='/?r=apiVideo/getVideos&categoryID='+different+'&p='+real_next_page_number+'&order='+sort;};
  if (list_type=='tag') {var link='/?r=apiVideo/getVideos&tag='+different+'&p='+real_next_page_number+'&order='+sort;};
  var new_content_div=document.createElement('div');
  ajax_request_by_element(new_content_div,link);
  block_id_element=document.getElementById(block_id);
  block_id_element.appendChild(new_content_div);
  if (real_next_page_number>=max_page) {
    show_more_button_element=document.getElementById(show_more_button_id);
    show_more_button_element.style.display='none';
  };
    // block_id_element=document.getElementById(block_id);
    // block_id_element.appendChild(new_content_div);
}

function gallery_show_pic(img_id,src) {
  var img=document.getElementById(img_id);
  img.src=src;
}

function video_like(video_id,action) {
  ajax_request('percent','/?r=apiVideo/likeCounter&id='+video_id);
}

function video_dislike(video_id,action) {
  ajax_request('percent','/?r=apiVideo/dislikeCounter&id='+video_id);
}

function video_like2(video_id,action) {
  ajax_request(action+'s','/?page=submit_likes&type=video&video_id='+video_id+'&action='+action);
}

function ajax_request(id,link) {
    var cont=document.getElementById(id);
    //cont.innerHTML='Идет загрузка...';
    var http=new XMLHttpRequest;
    http.open('get', link);
    http.onreadystatechange=function () {
      if (http.readyState==4) {
        cont.innerHTML=http.responseText;
          // if (type=='clear') {cont.innerHTML=http.responseText;};
          // if (type=='add') {cont.innerHTML+=http.responseText;};
        };
      }
      http.send(null);
}

function ajax_request_by_element(element,link) {
    var http=new XMLHttpRequest;
    window.returned='created';
    // var returned;
    http.open('get', link);
    http.onreadystatechange=function () {
      if (http.readyState==4) {
        element.innerHTML=http.responseText;
        window.returned=http.responseText;
      };
      // alert(window.returned);
      }
      http.send(null);
}

// Перезагрузка каптчи
function captcha_reload() {
document.images['captcha'].src='/captcha/captcha.php?rnd='+Math.round(Math.random(0)*1000);
}

// Текст в форме копируется в память
function copyText()
{
	var buffTxt = document.htmlcode.text;
	txtVar = buffTxt.createTextRange();
	txtVar.execCommand("Copy");
}
/////////////////////////////////////////////////////////////////////////////////////
//Раскрыть Embed
/////////////////////////////////////////////////////////////////////////////////////
function show_hide_text(divObj) {
    var div = divObj.parentNode.getElementsByTagName('div')[1];


    if (div.style.display == 'block') {
        div.style.display = 'none';
    } else {
        div.style.display = 'block';
    }
}
/////////////////////////////////////////////////////////////////////////////////////


// Добавить в Избранное 
function add_favorite(a) { 
  title=document.title; 
  url=document.location; 
  try { 
    // Internet Explorer 
    window.external.AddFavorite(url, title); 
  } 
  catch (e) { 
    try { 
      // Mozilla 
      window.sidebar.addPanel(title, url, ""); 
    } 
    catch (e) { 
      // Opera 
      if (typeof(opera)=="object") { 
        a.rel="sidebar"; 
        a.title=title; 
        a.url=url; 
        return true; 
      } 
      else { 
        // Unknown 
        alert('Нажмите Ctrl-D чтобы добавить страницу в закладки'); 
      } 
    } 
  } 
  return false; 
}

function startThumbSlide(IdDoc, Url) {
  if (CurDocument == 0) {
    OriPicNum = IdDoc;
    CurDocument = IdDoc;
    CurUrl = Url;
    nextthumb = getNextThumbNum(CurNum);
    preLoadPicture(nextthumb);
    CurPicNum = nextthumb;
    setTimeout("nextThumbnail("+ CurDocument +")", 1000);
  }
}

function stopThumbSlide() {
  TmpDoc = CurDocument;
  CurDocument = 0;
  document.getElementById("pic_" + TmpDoc).src = "/video-images/" + OriPicNum + ".jpg";  
}

function nextThumbnail(idDoc) {
  if (CurDocument != 0 && idDoc == CurDocument) {
    //alert("CurPicNum : " + CurPicNum + " CurDocument : " + CurDocument);
    //alert(document.getElementById("pic_" + CurDocument));
    document.getElementById("pic_" + CurDocument).src = CurUrl + CurPicNum + ".jpg";
    nextthumb = getNextThumbNum(CurPicNum);
    preLoadPicture(nextthumb);
    CurPicNum = nextthumb;
    setTimeout("nextThumbnail("+ CurDocument +")", 1000);
  }
}

var OriPicNum = 0;
var CurDocument = 0;
var CurPicNum = 0;
var CurUrl = "";
var CurNum = 0;

function getNextThumbNum(CurNum) {
  thumbs = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
  for (i = 0; i < thumbs.length; i++) {
    if (thumbs[i] > CurNum) {
      return thumbs[i];
    }
  }
  return thumbs[0];
}

function preLoadPicture(pic) {
  heavyImage = new Image(); 
  heavyImage.src = CurUrl + pic + ".jpg";

}