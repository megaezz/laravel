<?php

function videohub_save_init()
{
    global $config;
    include $config['path']['server'].'types/videohub_save/inc/grabber.php';
}

function set_sizes_for_all_files()
{
    $data = query('select id,server_path,ext from '.tabname('videohub', 'videos').'
		join '.tabname('videohub_save', 'subdomains').' on subdomains.sub=videos.subdomain
		where saved=\'yes\' and (size=0 or size is null or size=\'\');');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        query('update '.tabname('videohub', 'videos').' set size=\''.filesize($v['server_path'].$v['id'].'.'.$v['ext']).'\' where id=\''.$v['id'].'\';');
    }
    alert('����������� ������� ('.count($arr).') ���������� ������');
}

function submit_delete_file_simple()
{
    if (empty($_GET['path'])) {
        error('�� ������� ���� (path)');
    }
    unlink($_GET['path']);
    alert('������ ����: '.$_GET['path']);
}

function submit_compare_files_and_db()
{
    exit('����������');
    $data = query('select id from '.tabname('videohub', 'videos').'
		where saved=\'yes\'
		and concat(subdomain,\'.\',(select value from '.tabname('videohub_save', 'config').' where property=\'main_domain\'))=\''.$config['domain'].'\';
		');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        $files_db[] = $v['id'];
    }
    $data = query('select sub,server_path from '.tabname('videohub_save', 'subdomains').' where domain=\''.$config['domain'].'\';');
    $arr = readdata($data, 'nokey');
    $files_fs = [];
    foreach ($arr as $v) {
        $files = list_directory_files($v['server_path']);
        foreach ($files as $k => $file) {
            $arr_file = explode('.', $file);
            $files_path[$k] = $v['server_path'];
            $files_fs[$k] = $arr_file[0];
            // ������ ������ yes ���� ���������� ����
            // query('update '.tabname('videohub_save','videos').' set saved=\'yes\',subdomain=\''.$v['sub'].'\',ext=\''.$arr_file[1].'\' where id=\''.$arr_file[0].'\';');
        }
        $files_fs = $files_fs + $files;
    }
    $no_file = array_diff($files_db, $files_fs);
    $no_db = array_diff($files_fs, $files_db);
    $list_no_file = '';
    $list_no_db = '';
    foreach ($no_file as $v) {
        $list_no_file .= '<li>'.$v.'</li>';
    }
    foreach ($no_db as $k => $v) {
        $path_info = pathinfo($v);
        $list_no_db .= '<li>'.$files[$k].'
		(<a href="/?page=admin/submit_delete_file_simple&amp;path='.$files_path[$k].$files[$k].'">�������</a>,
			<a href="/?page=admin/submit_delete_video_file&amp;id='.$path_info['filename'].'&amp;saved=no">������� �� �������� NO</a>
			)
		</li>';
    }
    $content = '���� ������ � �� �� ��� �����: <ol>'.$list_no_file.'</ol>
	���� ����, �� ��� ������ � ��: <ol>'.$list_no_db.'</ol>';

    return $content;
}

function submit_delete_video_file()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    if (empty($_GET['saved'])) {
        error('�� ������� ������ saved');
    }
    $data = query('select id,saved,server_path,ext from '.tabname('videohub', 'videos').'
		join '.tabname('videohub_save', 'subdomains').' on subdomains.sub=videos.subdomain
		where id=\''.$_GET['id'].'\';');
    $arr = mysql_fetch_assoc($data);
    unlink($arr['server_path'].$_GET['id'].'.'.$arr['ext']);
    query('update '.tabname('videohub', 'videos').' set saved=\''.$_GET['saved'].'\' where id=\''.$_GET['id'].'\';');
    alert('������� �����: '.$_GET['id'].'<br/>��������� ������: '.$_GET['saved']);
}

function video_saved_status()
{
    global $config;
    $data = query('
		select (select count(*) from '.tabname('videohub', 'videos').' where saved=\'yes\') as yes,
		(select count(*) from '.tabname('videohub', 'videos').' where saved=\'loading\') as loading,
		(select count(*) from '.tabname('videohub', 'videos').' where saved=\'error\') as error,
		(select count(*) from '.tabname('videohub', 'videos').' where saved=\'no\') as no;
		');
    $arr = mysql_fetch_assoc($data);
    $data2 = query('select sub,server_path from '.tabname('videohub_save', 'subdomains').' where domain=\''.$config['domain'].'\';');
    $arr2 = readdata($data2, 'nokey');
    $list = '';
    foreach ($arr2 as $v) {
        // die($v['server_path']);
        $files = list_directory_files($v['server_path']);
        $size = exec('du -s -h '.$v['server_path']);
        // $list.=$v['sub'].':<ul><li>files: '.count($files).'</li><li>size: '.$size.'</li></ul>';
        $list .= '<tr><td>'.$v['sub'].'</td><td>'.count($files).' '.$size.'</td></tr>';
    }
    $content = '<table class="table table-striped" style="width: 500px; margin: 30px;">
	<tr><td>yes:</td><td>'.$arr['yes'].'</td></tr>
	<tr><td>loading:</td><td>'.$arr['loading'].'</td></tr>
	<tr><td>error:</td><td>'.$arr['error'].'</td></tr>
	<tr><td>no:</td><td>'.$arr['no'].'</td></tr>
	'.$list.'
	<tr><td colspan="2" style="text-align: center;"><a href="/?page=admin/submit_compare_files_and_db">�������� � ��</a></td></tr>
	</table>';

    return $content;
}

function login_videohub_save()
{
    return login_engine();
}

function authorizate_videohub_save_admin()
{
    authorizate('videohub_save', tabname('videohub_save', 'users'), ['root', 'admin']);
}

function check_loading_videos()
{
    global $config;
    $data = query('select id,ext,subdomain,server_path,size_remote from '.tabname('videohub', 'videos').'
		join '.tabname('videohub_save', 'subdomains').' on subdomains.sub=videos.subdomain
		where saved=\'loading\' limit 50;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $file = $v['server_path'].$v['id'].'.'.$v['ext'];
        $size = filesize($file);
        if (empty($size)) {
            error('���� '.$file.' ���� (<a href="/?page=admin/submit_delete_video_file&amp;saved=no&amp;id='.$v['id'].'">������� �� �������� NO</a>, <a href="/?page=admin/submit_delete_video_file&amp;saved=error&amp;id='.$v['id'].'">������� �� �������� ERROR</a>)');
        }
        if ($size == $v['size_remote']) {
            query('update '.tabname('videohub', 'videos').' set saved=\'yes\', size=\''.$size.'\' where id=\''.$v['id'].'\';');
            $list .= '<li style="color: green;">'.$v['id'].'</li>';
        } else {
            $list .= '<li style="color: blue;">'.$v['id'].' ('.round($size / 1024 / 1024, 1).' �� �� '.round($v['size_remote'] / 1024 / 1024, 1).' ��) <a href="/?page=admin/submit_delete_video_file&saved=no&id='.$v['id'].'">�������</a></li>';
        }
    }

    return $list;
}

// ��������� ������������� ������������� �������
function get_embed_player_of_unsaved_videos()
{
    if (empty($_GET['from'])) {
        $from = 0;
    } else {
        $from = $_GET['from'];
    }
    $data = query('select SQL_CALC_FOUND_ROWS id,hub,id_hub from '.tabname('videohub', 'videos').'
		where saved=\'no\' and hub!=\'hardsextube.com\' order by add_date desc limit '.$from.',1;');
    $arr = mysql_fetch_assoc($data);
    // print_r($arr); die('fff');
    $content = media_for_video($arr['hub'], $arr['id_hub'], 'embed_player');
    $content .= '<br/><a id="next" href="/?page=admin/embed_player&amp;from='.($from + 1).'">����� '.($from + 1).'</a>';

    return $content;
}

// ��������� ����� �� ������
function save_videos_on_server()
{
    global $config;
    // �������� ���������� ��� ������
    $data = query('select sub,server_path
		from '.tabname('videohub_save', 'subdomains').'
		where sub=(select value from '.tabname('videohub_save', 'config').' where property=\'active_subdomain\');');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['sub'])) {
        error('�� ��������� subdomain');
    }
    $subdomain = $arr['sub'];
    $server_path = $arr['server_path'];
    // ���������, �� �������� �� ����� ������������� ��������
    $max_q = value_from_config('videohub_save', 'max_loading_videos');
    exec('ps -C wget', $wget_arr);
    $wget_q = count($wget_arr) - 1;
    $content = '�����������: '.$wget_q.'<br/>';
    // die($content);
    if ($wget_q >= $max_q) {
        return $content;
    }
    if (empty($_GET['q'])) {
        error('�� ������� ���������� �����');
    }
    $data = query('select SQL_CALC_FOUND_ROWS id,hub,id_hub from '.tabname('videohub', 'videos').'
		where saved=\'no\' and hub!=\'hardsextube.com\' order by add_date desc limit '.$_GET['q'].';');
    $videos = readdata($data, 'nokey');
    $q_videos = rows_without_limit();
    if (empty($videos)) {
        alert('��� ������ ��� ���������');
    }
    $list = '';
    foreach ($videos as $video) {
        $video_source = media_for_video($video['hub'], $video['id_hub'], 'video_direct_link');
        if ($video_source == 'unavailable_video') {
            query('update '.tabname('videohub', 'videos').' set saved=\'error\', load_date=current_timestamp where id=\''.$video['id'].'\';');
            $list .= '<li>'.$video['id'].' error</li>';
        } else {
            $vs_parse_url = parse_url($video_source);
            $vs_pathinfo = pathinfo($vs_parse_url['path']);
            // ��� xvideos
            $vs_pathinfo['extension'] = str_replace(';v=1', '', $vs_pathinfo['extension']);
            $vs_extension = strtolower($vs_pathinfo['extension']);
            $video_path = $server_path.$video['id'].'.'.$vs_extension;
            if (file_exists($video_path) == 1) {
                error('���� '.$video_path.' ��� ����������');
            }
            // ���������� ������
            // die($video['id']);
            $video_size = remote_filesize($video_source);
            $cmd = 'wget "'.$video_source.'" -O '.$video_path.' > /dev/null &';
            query('update '.tabname('videohub', 'videos').' set saved=\'loading\',subdomain=\''.$subdomain.'\',ext=\''.$vs_pathinfo['extension'].'\', size_remote=\''.$video_size.'\', load_date=current_timestamp where id=\''.$video['id'].'\';');
            exec($cmd);
            $list .= '<li>'.$video['id'].' '.$video_source.' '.$video_size.'</li>';
        }
    }
    $content .= '����� ��������������� �������: '.$q_videos.'<br/><ol>'.$list.'</ol><br/>';

    return $content;
}

// �.�. ���� ����� �� ������� ���� $temp � �������� ������, ������ ��� ���������� �������
function google_error_fix_redirect()
{
    global $config;
    $temp = '/%3Fhub=([\w\.]+)%26id_hub=([0-9]+)/';
    $repl = '?hub=$1&id_hub=$2';
    $query = $_SERVER['REQUEST_URI'];
    $query_arr = @parse_url($query) or error('�������� ������');
    $path = $query_arr['path'];
    $link = '';
    if (preg_match($temp, $path) == 1) {
        $link = urldecode($path);
        // $link=preg_replace($temp,$repl,$path); //����� � ���
        // die($link);
        // die ������ ����������, ����� ������ �� ���������� ������, ����� ��������� �� �����
        exit(header('Location: '.$link));
    }
}

// ������� ������ ���� ������ �� ������, � ����������� �� ���� ������ �� ������� videokey
function check_videokey($videokey)
{
    if (empty($videokey)) {
        error('�� ������� ���');
    }
    $data = query('select property,value from '.tabname('videohub_save', 'config').' where property in (\'videokey_current\',\'videokey_last\')');
    $arr = readdata($data, 'property');
    if (empty($arr)) {
        error('����������� ����������� ����');
    }
    if ($videokey != $arr['videokey_current']['value'] and $videokey != $arr['videokey_last']['value']) {
        error('�������� ����������� ���');
    }
}

// ������������� ����� ���� ���������� � �������� ��� � ��������� �������. ����������� IP ���������.
function change_videokey()
{
    // if (request()->ip()!='176.9.95.106') {error('������ ��������','������� ������� � �������������� IP');};
    if (empty($_GET['videokey']) == 1) {
        error('�� ������� ����');
    }
    $data = query('select property,value from '.tabname('videohub_save', 'config').' where property=\'videokey_current\'');
    $arr = readdata($data, 'property');
    $current = $arr['videokey_current']['value'];
    query('update '.tabname('videohub_save', 'config').' set value=\''.$current.'\' where property=\'videokey_last\'');
    query('update '.tabname('videohub_save', 'config').' set value=\''.$_GET['videokey'].'\' where property=\'videokey_current\'');
    logs('�������� videokey �� �������');
}

function media_for_video($hub, $id_hub, $action, $ph_viewkey = '')
{
    if (empty($hub) == 1) {
        error('�� ������ ����������� ���������', '�� ����� hub');
    }
    if (empty($id_hub) == 1) {
        error('�� ������ ����������� ���������', '�� ����� id_hub');
    }
    // if (is_numeric($id_hub)==0) {error('�������� ������','id_hub ������ ���� ������');};
    if (empty($action) == 1) {
        error('�� ������ ����������� ���������', '�� ����� action');
    }
    if ($hub == 'pornhub.com') {
        return media_from_pornhub($id_hub, $action, $ph_viewkey);
    }
    if ($hub == 'hardsextube.com') {
        return media_from_hardsextube($id_hub, $action);
    }
    if ($hub == 'xvideos.com') {
        return media_from_xvideos($id_hub, $action);
    }
    if ($hub == 'paradisehill.tv') {
        return media_from_paradisehill($id_hub, $action);
    }
    error('���������� �������� �����, ��� '.$hub.' �� ��������������.');
}

function preparing_direct_save_video_link($args)
{
    if (empty($args['hub'])) {
        error('�� ����� ���');
    }
    if (empty($args['id_hub'])) {
        error('�� ����� ID');
    }
    if (empty($args['ph_viewkey'])) {
        $args['ph_viewkey'] = '';
    }

    return media_for_video($args['hub'], $args['id_hub'], 'video_direct_link', $args['ph_viewkey']);
}

// ���������� ������ ������ �� ���������� �����
function direct_save_video_link()
{
    if (isset($_GET['hub'],$_GET['id_hub']) == 0) {
        error('������ �� ����� ���� ����������', '', 0);
    }
    $args['id_hub'] = $_GET['id_hub'];
    $args['hub'] = $_GET['hub'];
    if (isset($_GET['ph_viewkey'])) {
        $args['ph_viewkey'] = $_GET['ph_viewkey'];
    }

    return cache('preparing_direct_save_video_link', $args, 900);
}

/*
//���������� ������ ������ �� ���������� �����
function direct_save_video_link() {
    // �� xrest.net �� ���������� ����
    //if (empty($_GET['videokey'])==1) {error('�� ����� VIDEOKEY');};
    //check_videokey($_GET['videokey']);

    if (isset($_GET['hub'],$_GET['id_hub'])==0) {error('������ �� ����� ���� ����������','',0);};
    return media_for_video($_GET['hub'],$_GET['id_hub'],'video_direct_link');
}
*/

// �������� �� ������ ������ ��� �������� �����
function redirect_save_video()
{
    if (isset($_GET['hub'],$_GET['id_hub']) == 0) {
        // error('������ �� ����� ���� ����������','',0);
        logs('������ �� ����� ���� ����������');
        exit(template('chokurej'));
    }
    $start = 0;
    if (! empty($_GET['start'])) {
        $start = $_GET['start'];
    }
    $flv = direct_save_video_link();
    // $flv=direct_save_video_link();
    if ($flv == 'unavailable_video') {
        return '��������, ����� ���������� ��� ����������';
    }
    $parse = parse_url($flv);
    if (empty($parse['query'])) {
        $symbol = '?';
    } else {
        $symbol = '&';
    }
    // print_r($parse);
    // die(' ');
    $startvar = media_for_video($_GET['hub'], $_GET['id_hub'], 'startvar');
    // logs('������ ����������');
    header('Location: '.$flv.$symbol.$startvar.'='.$start);
}
