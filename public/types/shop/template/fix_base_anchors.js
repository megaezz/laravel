function fix_base_url_anchors() {
    var pathname = window.location.href.split('#')[0];
    $('a[href^="#"]').each(function() {
        var $this = $(this),
            link = $this.attr('href');
        $this.attr('href', pathname + link);
    });
}

document.addEventListener('DOMContentLoaded',fix_base_url_anchors);