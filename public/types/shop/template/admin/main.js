document.addEventListener('DOMContentLoaded',shop_init);

function wide_stat_check_statuses(set) {
	if (!set) {
		alert('�� �������� �������� ������ ��������');
		return;
	};
	sets=[];
	sets['confirmed']=['sent','delivered not paid','return paid','return','refund paid','refund','take payment','paid','packed','waiting send','check address'];
	wide_stat_uncheck_statuses();
	if (!sets[set]) {alert('����� "'+set+'" �� ����������');};
	sets[set].forEach(function(value) {
		document.getElementById('statuses['+value+']').checked=true;
	});
}

function wide_stat_uncheck_statuses() {
	roles=document.querySelectorAll('[data-role="statuses"]');
	[].forEach.call(roles,function(role) {
		role.checked=false;
	});
}

function shop_init() {
	if (document.getElementById('addPurchase')!=null) {
		prevent_form_submit('addPurchase');
	};
}

function roleHandler(target) {
	if (target.dataset.role=='duplicate_order') {
		if (!confirm('����������� ����� #'+target.dataset.order_id+'?')) {return;};
		// ��������� ��������
		$("#spinner").overlay();
		// ������� ����� ������ ����� � ���������� ID � ���������� result_create_order
		ajax_request_new({
			id:'result_duplicate_order_'+target.dataset.order_id,link:'/?page=admin/ajax/duplicate_order&order_id='+target.dataset.order_id,clear:true,
			success_function: function () {
				// �������� id ���������� ������
				new_order_id=document.getElementById('result_duplicate_order_'+target.dataset.order_id).innerHTML;
				// ���� �������, ���������� � ������ ������ ���� �����
				ajax_request_new({
					id:'content_table',link:'/?page=admin/ajax/orders_table&status=all&order_id='+new_order_id,
					success_function: function () {
										// ��������� ��������
										$("#spinner").overlayout();
					}
				});
				// �������� ��������� ���� � ��������������� ���������� ������, ���������� ��������� ID ������
				/*
				ajax_request_new({
					id:'modal_content',link:'/?page=admin/ajax/edit_order&order_id='+new_order_id,clear:true,
					success_function: function () {
										// ��������� ��������
										$("#spinner").overlayout();
					}
				});
				*/
				// �� ������ ������ �������� result_duplicate_order_ID, ���� �� � ��������� ����� ����������� content_table, �.�. � ��� �� � ����������
				document.getElementById('result_duplicate_order_'+target.dataset.order_id).innerHTML='';
			}
		});
	};
	if (target.dataset.role=='create_order') {
		// ��������� ��������
		$("#spinner").overlay();
		// ������� ����� ������ ����� � ���������� ID � ���������� result_create_order
		ajax_request_new({
			id:'result_create_order',link:'/?page=admin/ajax/create_order',clear:true,
			success_function: function () {
				// ���� �������, ��������� ������� �������
				ajax_request_new({id:'content_table',link:content_table});
				// �������� ��������� ���� � ��������������� ���������� ������, ���������� ��������� ID ������
				ajax_request_new({
					id:'modal_content',link:'/?page=admin/ajax/edit_order&order_id='+document.getElementById('result_create_order').innerHTML,clear:true,
					success_function: function () {$("#spinner").overlayout();}
				});
				// �� ������ ������ �������� result_create_order, ���� �� � ��������� ����� ����������� content_table, �.�. � ��� �� � ����������
				document.getElementById('result_create_order').innerHTML='';
			}
		});
	};
	if (target.dataset.role=='edit_order') {
		$("#spinner").overlay();
		// ajax_request('modal_content','/?page=admin/ajax/edit_order&order_id='+target.dataset.id);
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/edit_order&order_id='+target.dataset.id,clear:true,
			success_function: function() {
				$("#spinner").overlayout();
			}
		});
	};
	if (target.dataset.role=='edit_purchase') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/edit_purchase&purchase_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='edit_item') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/edit_item&item_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='add_item') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/add_item',clear:true});
	};
	if (target.dataset.role=='add_stock') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/add_stock&item_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='edit_stock') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/edit_stock&stock_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='order_send_sms') {
		// alert(target.dataset.id);
		// document.getElementById('modal_content').innerHTML='test';
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/order_send_sms&order_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='order_sent_sms') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/order_sent_sms&order_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='ajax_submit_add_stock') {
		args={form_id:'addStock',url:'/?page=admin/ajax/submit_add_stock',button_id:'result',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'result'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_edit_stock') {
		args={form_id:'editStock',url:'/?page=admin/ajax/submit_edit_stock',button_id:'result',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'result'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_add_item') {
		args={form_id:'addItem',url:'/?page=admin/ajax/submit_add_item',button_id:'result',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'result'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_edit_item') {
		args={form_id:'editItem',url:'/?page=admin/ajax/submit_edit_item',button_id:'result',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'result'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_delete_item') {
		args={form_id:'editItem',url:'/?page=admin/ajax/submit_delete_item',button_id:'delete',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'delete'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_edit_order') {
		$("#spinner").overlay();
		args={form_id:'editOrder',url:'/?page=admin/ajax/submit_edit_order',button_id:'result',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'result',
			success_text:1,
			success_function_reload: function() {$("#spinner").overlayout();}
		};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_delete_order') {
		if (!confirm('������� �����?')) {return;};
		$("#spinner").overlay();
		args={form_id:'editOrder',url:'/?page=admin/ajax/submit_delete_order',button_id:'delete',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'delete',
			success_function: function() {$("#spinner").overlayout();}
		};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_edit_purchase') {
		args={form_id:'editPurchase',url:'/?page=admin/ajax/submit_edit_purchase',button_id:'result',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'result'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_add_purchase') {
		args={form_id:'addPurchase',url:'/?page=admin/ajax/submit_add_purchase',button_id:'addPurchaseBtn',
		reload_page:content_table,reload_id:'content_table',result_id:'addPurchaseBtn'};
		ajax_submit_post_button_modal(args);
		document.getElementById(args.form_id).reset();
		$('#'+args.button_id).button('reset');
	};
	if (target.dataset.role=='ajax_submit_delete_purchase') {
		args={form_id:'editPurchase',url:'/?page=admin/ajax/submit_delete_purchase',button_id:'delete',modal_id:'modal',
			modal_hide:true,reload_page:content_table,reload_id:'content_table',result_id:'delete'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='show_landing') {
		document.getElementById('iframe_landing').src=target.dataset.dir;
		document.getElementById('iframe_landing').style.display='block';
	};
	if (target.dataset.role=='ajax_submit_add_item_to_order') {
		args={form_id:'addItemToOrder',url:'/?page=admin/ajax/submit_add_item_to_order',button_id:'add_item_button',
		reload_page:'/?page=admin/ajax/order_items_list&order_id='+target.dataset.order_id,reload_id:'items_list',result_id:'add_item_button'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_add_discount_to_order') {
		args={
			form_id:'addDiscountToOrder',url:'/?page=admin/ajax/submit_add_discount_to_order',button_id:'add_discount_button',
			reload_page:'/?page=admin/ajax/order_items_list&order_id='+target.dataset.order_id,reload_id:'items_list',
			result_id:'add_discount_button',
			success_function: function() {
				ajax_request_new({
					id:'content_table',
					link:content_table,
					success_function: function () {
						$("#spinner").overlayout();
					}
				});
			}
		};
		$('#spinner').overlay();
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_change_item_q_in_package') {
		args={
			link:'/?page=admin/ajax/change_item_q_in_package&order_id='+target.dataset.order_id+
			'&item_id='+target.dataset.item_id+'&action='+target.dataset.action,
			success_function: function() {
				ajax_request_new({
					id:'content_table',
					link:content_table,
					success_function: function () {
						$("#spinner").overlayout();
					}
				});
				ajax_request_new({
					id:'items_list',
					link:'/?page=admin/ajax/order_items_list&order_id='+target.dataset.order_id,
					success_function: function() {
						$('#spinner').overlayout();
					}
				});
			}
		};
		$('#spinner').overlay();
		ajax_request_new(args);
	};
	if (target.dataset.role=='change_select') {
		document.getElementById(target.dataset.id).value=target.dataset.value;
	};
	if (target.dataset.role=='ajax_submit_send_sms') {
		args={form_id:'sendSms',url:'/?page=admin/ajax/submit_send_sms',button_id:'send_sms',
		reload_page:content_table,reload_id:'content_table',result_id:'sms_answer'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_send_sms_check_cost') {
		args={form_id:'sendSms',url:'/?page=admin/ajax/submit_send_sms&action=check_cost',button_id:'check_cost',
		reload_page:content_table,reload_id:'content_table',result_id:'sms_answer',button_reset:true};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_send_sms_mass') {
		args={form_id:'sendSms_'+target.dataset.order_id,url:'/?page=admin/ajax/submit_send_sms',
		button_id:'send_sms_'+target.dataset.order_id,result_id:'sms_answer_'+target.dataset.order_id};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_submit_send_sms_check_cost_mass') {
		args={form_id:'sendSms_'+target.dataset.order_id,url:'/?page=admin/ajax/submit_send_sms&action=check_cost',
		button_id:'check_cost_'+target.dataset.order_id,result_id:'sms_answer_'+target.dataset.order_id,
		button_reset:true};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_set_caller') {
		// $('#button_set_caller').button('loading');
		 $('#button_set_caller').addClass('disabled');
		args={
			id:'answer_caller',link:'/?page=admin/ajax/set_caller&order_id='+target.dataset.order_id,
			success_function: function() {ajax_request('content_table',content_table);}
		};
		ajax_request_new(args);
	};
	if (target.dataset.role=='ajax_submit_caller_comment') {
		args={form_id:'caller_comment',url:'/?page=admin/ajax/submit_caller_comment',
		button_id:'button_caller_comment',result_id:'block_caller_comment',
		reload_page:content_table,reload_id:'content_table'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_check_callers') {
		display=document.getElementById('check_callers').style.display;
		if (display=='none') {
			new_display='block';
			args={
				id:'check_callers',link:'/?page=admin/ajax/check_callers&order_id='+target.dataset.order_id,
				success_function: function() {ajax_request('content_table',content_table);}
			};
			ajax_request_new(args);
		};
		if (display=='block') {
			new_display='none';
		};
		document.getElementById('check_callers').style.display=new_display;
	};
	if (target.dataset.role=='edit_day_ad') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/edit_day_ad&date='+target.dataset.date,clear:true});
	};
	if (target.dataset.role=='ajax_submit_edit_day_ad') {
		args={form_id:'editDayAd',url:'/?page=admin/ajax/submit_edit_day_ad',button_id:'result',
		result_id:'result',modal_id:'modal',modal_hide:true,location_reload:true};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='show_post_info') {
		document.getElementById(target.dataset.element).innerHTML='<img src="/types/shop/template/admin/img/wait.gif" alt="" />';
		ajax_request_new({id:target.dataset.element,link:'/?page=admin/ajax/kz_post_info&zip='+target.dataset.zip+'&track='+target.dataset.track+'&addr_link='+target.dataset.addr_link});
	};
	if (target.dataset.role=='show_track_info') {
		document.getElementById(target.dataset.element).innerHTML='<img src="/types/shop/template/admin/img/wait.gif" alt="" />';
		ajax_request_new({id:target.dataset.element,link:'/?page=admin/ajax/kz_track_info&track='+target.dataset.track});
	};
	if (target.dataset.role=='get_post_address') {
		// ��������� ������
		document.getElementById(target.dataset.element).innerHTML+=' ';
		ajax_request_new({
			id:target.dataset.element,
			link:'/?page=admin/ajax/kz_post_address&zip='+target.dataset.zip+'&n='+target.dataset.n,
			add:true,
			success_function: function() {document.getElementById('message_length').innerHTML=document.getElementById('message').value.length;}
		});
	};
	if (target.dataset.role=='order_changes_list') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/order_changes_list&order_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='edit_login_paid') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/edit_login_paid&type='+target.dataset.type+'&login='+target.dataset.login,clear:true});
	};
	if (target.dataset.role=='ajax_submit_edit_login_paid') {
		args={form_id:'editLoginPaid',url:'/?page=admin/ajax/submit_edit_login_paid',button_id:'result',
		result_id:'result',modal_id:'modal',modal_hide:true,location_reload:true};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='show_input_track_form') {
		display=document.getElementById('input_track_form_'+target.dataset.order_id).style.display;
		if (display=='none') {new_display='block';};
		if (display=='block') {new_display='none';};
		document.getElementById('input_track_form_'+target.dataset.order_id).style.display=new_display;
		if (document.getElementById('input_track_value_'+target.dataset.order_id).value=='') {
			$("#input_track_value_"+target.dataset.order_id).mask("CC140999999KZ",{placeholder:"CC140______KZ"});
		};
	};
	if (target.dataset.role=='order_set_status') {
		rus={'packed':'��������','paid':'�������','return paid':'������� �������'};
		if (!confirm('��������� ������ "'+rus[target.dataset.status]+'" ��� ������ '+target.dataset.order_id+'?')) {return;};
		result_id='order_set_status_'+target.dataset.status+'_result_'+target.dataset.order_id;
		ajax_request_new({
			id:result_id,
			link:'/?page=admin/ajax/order_set_status&order_id='+target.dataset.order_id+'&status='+target.dataset.status,
			success_function: function() {
				// ajax_request_new({id:'content_table',link:content_table});
				if (document.getElementById(result_id).innerHTML==1) {
					document.getElementById(result_id).innerHTML='';
					ajax_request_new({id:'content_table',link:content_table});
				} else {
					alert(document.getElementById(result_id).innerHTML);
				};
			}
		});
	};
	if (target.dataset.role=='delete_order') {
		if (!confirm('������� ����� '+target.dataset.order_id+'?')) {return;};
		$("#spinner").overlay();
		ajax_request_new({
			link:'/?page=admin/ajax/delete_order&order_id='+target.dataset.order_id,
			success_function: function() {
				ajax_request_new({id:'content_table',link:content_table,success_function: function(){$("#spinner").overlayout();}});
			}
		});
	};
	if (target.dataset.role=='ajax_submit_input_track') {
		args={form_id:'input_track_form_'+target.dataset.order_id,url:'/?page=admin/ajax/submit_input_track',
		button_id:'input_track_button_'+target.dataset.order_id,result_id:'input_track_result_'+target.dataset.order_id,
		location_reload:false};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='delete_return_request') {
		if (confirm('������� ���� ������ �� �������?')) {
			location.href='/?page=admin/returns/delete&id='+target.dataset.id;
		} else {
			return;
		};
	}
	if (target.dataset.role=='item_detailed_stat') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/item_detailed_stat&item_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='fast_order_info') {
		info=document.getElementById('info');
		if (info.value!='') {info.value=info.value+' ';};
		info.value=info.value+target.dataset.value;
	};
	if (target.dataset.role=='fast_order_info_shave') {
		info=document.getElementById('info_shave');
		if (info.value!='') {info.value=info.value+' ';};
		info.value=info.value+target.dataset.value;
	};
	if (target.dataset.role=='order_set_print') {
		result_id='order_set_print_result_'+target.dataset.order_id;
		// label_id='order_set_print_label_'+target.dataset.order_id;
		button_id='order_set_print_button_'+target.dataset.order_id;
		$('#'+button_id).prop('disabled', true);
		ajax_request_new({
			id:result_id,
			link:'/?page=admin/ajax/order_set_print&order_id='+target.dataset.order_id,
			success_function: function() {
				// alert('"'+document.getElementById(result_id).innerHTML.trim()+'"');
				$('#'+button_id).prop('disabled', false);
				if (document.getElementById(result_id).innerHTML.trim()=='1') {
					// alert('ok');
					// $('#'+button_id).prop('active',true);
					$('#'+button_id).addClass('active');
				} else {
					// alert('no');
					// $('#'+button_id).prop('active',false);
					$('#'+button_id).removeClass('active');
				};
			}
		});
	};
	if (target.dataset.role=='edit_bonuses') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/edit_bonuses&item_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='ajax_submit_edit_bonuses') {
		args={form_id:'editBonuses',url:'/?page=admin/ajax/submit_edit_bonuses',button_id:'result',modal_id:'modal',
			modal_hide:true,reload_id:'content_table',reload_page:content_table,result_id:'result'};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='order_stat') {
		ajax_request_new({id:'modal_lg_content',link:'/?page=admin/stat/order_stat/ajax&order_id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='edit_landing') {
		ajax_request_new({id:'modal_content',link:'/?page=admin/ajax/edit_landing&id='+target.dataset.id,clear:true});
	};
	if (target.dataset.role=='order_shave') {
		// /?page=admin/ajax/order_shave&order_id=83230
		// if (!confirm('������� ����� '+target.dataset.order_id+'?')) {return;};
		$("#spinner").overlay();
		ajax_request_new({
			link:'/?page=admin/ajax/order_shave&order_id='+target.dataset.order_id,
			success_function: function() {
				// ��������� ��������� ���� �������������� ������
				roleHandler({
					dataset:{
						role:'edit_order',
						id:target.dataset.order_id
					}
				});
				// ������������ � ���� ��������� �������� �� ��������� �����
				$("#spinner").overlay();
				ajax_request_new({
					id:'content_table',
					link:content_table,
					success_function: function () {
						$("#spinner").overlayout();
					}
				});
			}
		});
	};
	if (target.dataset.role=='order_set_delivery') {
		// ��������� ��������� ����
		$('#modal').modal('hide');
		$('#spinner').overlay();
		ajax_request_new({
			link:'/?page=admin/ajax/order_set_delivery&order_id='+target.dataset.order_id+'&item_id='+target.dataset.item_id,
			success_function: function() {
				// ��������� ��������� ���� �������������� ������
				roleHandler({
					dataset:{
						role:'edit_order',
						id:target.dataset.order_id
					}
				});
				$("#spinner").overlayout();
				$('#modal').modal('show');
				// ������������ � ���� ��������� �������� �� ��������� �����
				// $("#spinner").overlay();
				ajax_request_new({
					id:'content_table',
					link:content_table,
					success_function: function () {
						$("#spinner").overlayout();
					}
				});
			}
		});
	};
	if (target.dataset.role=='edit_order_add_set') {
		// ��������� ��������� ����
		$('#modal').modal('hide');
		$('#spinner').overlay();
		ajax_request_new({
			link:'/?page=admin/ajax/add_set_to_order&order_id='+target.dataset.order_id+'&set_id='+target.dataset.set_id,
			success_function: function() {
				// ��������� ��������� ���� �������������� ������
				roleHandler({
					dataset:{
						role:'edit_order',
						id:target.dataset.order_id
					}
				});
				$("#spinner").overlayout();
				$('#modal').modal('show');
				// ������������ � ���� ��������� �������� �� ��������� �����
				// $("#spinner").overlay();
				ajax_request_new({
					id:'content_table',
					link:content_table,
					success_function: function () {
						$("#spinner").overlayout();
					}
				});
			}
		});
	};
	if (target.dataset.role=='show_edit_package_item_price') {
		item_cost_id='package_item_price_'+target.dataset.item_id;
		// alert(item_cost_id);
		item_cost_edit_id='package_item_price_edit_'+target.dataset.item_id;
		item_cost_display=document.getElementById(item_cost_id).style.display;
		// alert(item_cost_display);
		if (item_cost_display=='none') {
			new_item_cost_display='inline';
			new_item_cost_edit_display='none';
		};
		if (item_cost_display=='inline') {
			new_item_cost_display='none'
			new_item_cost_edit_display='block';
		};
		document.getElementById(item_cost_id).style.display=new_item_cost_display;
		document.getElementById(item_cost_edit_id).style.display=new_item_cost_edit_display;
	};
	if (target.dataset.role=='edit_package_item_price') {
		args={
			form_id:'package_item_price_edit_'+target.dataset.item_id,
			button_id:'package_item_price_edit_button_'+target.dataset.item_id,
			url:'/?page=admin/ajax/edit_package_item_price',
			reload_page:'/?page=admin/ajax/order_items_list&order_id='+target.dataset.order_id,
			reload_id:'items_list',
			result_id:'package_item_price_edit_button_'+target.dataset.item_id,
			success_function: function() {
				ajax_request_new({
					id:'content_table',
					link:content_table,
					success_function: function () {
						$("#spinner").overlayout();
					}
				});
			}
		};
		$('#spinner').overlay();
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='order_upsale') {
		$("#spinner").overlay();
		ajax_request_new({
			link:'/?page=admin/ajax/order_upsale&order_id='+target.dataset.order_id,
			success_function: function() {
				// ��������� ��������� ���� �������������� ������
				roleHandler({
					dataset:{
						role:'edit_order',
						id:target.dataset.order_id
					}
				});
				// ������������ � ���� ��������� �������� �� ��������� �����
				$("#spinner").overlay();
				ajax_request_new({
					id:'content_table',
					link:content_table,
					success_function: function () {
						$("#spinner").overlayout();
					}
				});
			}
		});
	};
	if (target.dataset.role=='fast_caller_comment') {
		info=document.getElementById('caller_comment_input');
		// ������� ������, ����� �� ��������� placeholder
		info.value=' ';
		info.value=target.dataset.value;
	};
}

function ajax_submit_post_button_modal(args) {
	var	form_id=args.form_id;
	var url=args.url;
	var button_id=args.button_id;
	var modal_id=args.modal_id || false;
	var modal_hide=args.modal_hide || false;
	var reload_page=args.reload_page || false;
	var reload_id=args.reload_id || false;
	var result_id=args.result_id || false;
	var button_reset=args.button_reset || false;
	var success_text=args.success_text || false;
	var success_function=args.success_function || false;
	var success_function_reload=args.success_function_reload || false;
	var location_reload=args.location_reload || false;
	button_default=$('#'+button_id).text();
	$btn=$('#'+button_id).button('loading');
	queryString=$('#'+form_id).serialize();
	$.ajax({
		type: "POST",
		url: url,
		data: queryString,
		success: function(answer) {
			if (success_function) {success_function();};
			if (result_id) {
				if (success_text) {
					if (answer!=success_text) {alert(answer);};
				};
				$('#'+result_id).empty();
				$('#'+result_id).append(answer);
			};
			$('#'+button_id).empty();
			$('#'+button_id).append(button_default);
			if (button_reset) {
				$('#'+button_id).button('reset');
			};
			if (modal_hide && modal_hide==true) {
				$('#'+modal_id).modal('hide');
			};
			if (reload_id && reload_page) {
				ajax_request_new({id:reload_id,link:reload_page,success_function:success_function_reload});
			};
			if (location_reload) {location.reload();};
		}
	});
}

/*
function ajax_submit_post(form_id,url,result_id,page_reload) {
	queryString=$('#'+form_id).serialize();
	$.ajax({
		type: "POST",
		url: url,
		data: queryString,
		success: function(answer) {
			$('#'+result_id).empty();
			$('#'+result_id).append(answer);
			if (typeof(page_reload)!='undefined' && page_reload==true) {
				location.reload();
			};
		}
	});
}
*/