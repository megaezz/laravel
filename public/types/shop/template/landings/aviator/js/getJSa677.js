$(window).load(function() {

});



$(document).ready(function () {



    function myStartFunction() {
        $('.top-line').addClass('fixed');
    }
    function myCompleteFunction() {
        $('.top-line').removeClass('fixed');
    }

    $( ".more" ).click(function() {
        var rev_not_hidden=$('.rev-block:not(.hidden)')
        var rev_hidden=$('.rev-block.hidden')
        rev_not_hidden.addClass('hidden');
        rev_hidden.removeClass('hidden');
    });


    $('.fancybox').fancybox({
        fitToView : false,
        autoSize  : false,
        width     : 'auto',
        height    : 'auto',
        beforeLoad: myStartFunction,
        beforeClose: myCompleteFunction
    });

    $('.thumbnails .item').hover(
        function () {
            $('.thumbnails .item').removeClass('active');
            var url_this = $(this).css('background-image');
            console.log(url_this);
            $('.big-img').css('background-image', url_this);
            $(this).addClass('active');
        },
        function () {
//            $(this).parent().siblings('.big-img').removeAttr('style');
        });

     $('.big-img').css('background-image', $(".item.active").css('background-image'));


if($('#video').attr('src')){
var youtube=$('#video').attr('src').match(/youtube.com\/embed\/([^?]+)/im);
if(youtube)
    $('#video_img').css('background-image','url(http://img.youtube.com/vi/'+youtube[1]+'/0.jpg)')

var vimeo=$('#video').attr('src').match(/vimeo.com\/video\/([0-9]+)/im);
if(vimeo){
$.ajax({
    type:'GET',
    url: 'http://vimeo.com/api/v2/video/' + vimeo[1] + '.json',
    jsonp: 'callback',
    dataType: 'jsonp',
    success: function(data){
        var thumbnail_src = data[0].thumbnail_large;
        $('#video_img').css('background-image','url('+thumbnail_src+')')
    }
});
}
}




start_timer();
});




var dbt=new Date(new Date().getTime() + (2 * 24 * 60 * 60 * 1000));
var finisDate = (dbt.getMonth()+1)+"/"+dbt.getDate()+"/"+dbt.getFullYear()+" 5:00 AM";
function start_timer()
{
	var finisTime = new Date(finisDate);
	var nowTime = new Date();
	var diffTime = new Date(finisTime-nowTime);
	var finishSeconds = Math.floor(diffTime.valueOf()/1000);

    var days=parseInt(finishSeconds/86400);
    var hours = parseInt(finishSeconds/3600)%24;
    var minutes = parseInt(finishSeconds/60)%60;
    var seconds = finishSeconds%60;
    if (days < 10) days = "0" + days;
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;

    days=days.toString();
    hours=days.toString();
    minutes=minutes.toString();
    seconds=seconds.toString();

    $('.days_1').html(days.charAt(0));
    $('.days_2').html(days.charAt(1));

    $('.hours_1').html(hours.charAt(0));
    $('.hours_2').html(hours.charAt(1));

    $('.minutes_1').html(minutes.charAt(0));
    $('.minutes_2').html(minutes.charAt(1));

    $('.seconds_1').html(seconds.charAt(0));
    $('.seconds_2').html(seconds.charAt(1));

    strDay=dbt.getDate();
    strMonth=(dbt.getMonth()+1);
    if (strDay < 10) strDay = "0" + strDay;
    if (strMonth < 10) strMonth = "0" + strMonth;

    $('.date_action').html((strDay)+"."+(strMonth)+"."+dbt.getFullYear());

    $(' .timer .digits').html(days+":"+hours + ":" + minutes + ":" + seconds);

    setTimeout(start_timer, 1000);
}