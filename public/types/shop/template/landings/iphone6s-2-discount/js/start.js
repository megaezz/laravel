function format(val){
	res=String(val);
	var re = /(?=\B(?:\d{3})+(?!\d))/g
	return res.replace(re,' ');
}
function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}
$(document).ready(function(){
	//$('#count').mask('99');
	$('#count').keypress(function(e) {
		var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
		if (verified) {
			e.preventDefault();
		}
	}).keyup(function(){
		if(parseInt($(this).attr('value'))>15)$(this).attr('value','15');
		var count=parseInt($(this).attr('value')),sum,ship=510,total;
		
		sum=(count)?6990*count:0;
		total=510+sum;
		$('#sum').attr('value',format(sum)+' руб.');
		$('#ship').attr('value',format(ship)+' руб.');
		$('#total').attr('value',format(total)+' руб.');
	});
	$('#sum,#total,#ship').keypress(function(e){
		return false;
	});
	$('button.submit').click(function(){
		var email=$('input[type="email"]').val();
		if($(this).hasClass('disable'))return false;
		if(($('.form-horizontal input:required:valid, .form-horizontal textarea:required:valid').length==3)&((email==0)||(isValidEmailAddress(email)))){
			$(this).addClass('disable');
		}
	});
});