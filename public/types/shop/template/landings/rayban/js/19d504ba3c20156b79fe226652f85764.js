(function () {
  var _id = '19d504ba3c20156b79fe226652f85764';
  while (document.getElementById('timer' + _id)) _id = _id + '0';
  document.write('<div id=\'timer' + _id + '\' style=\'min-width:371px;height:79px;\'></div>');
  var _t = document.createElement('script');
  _t.src = 'http://megatimer.ru/timer/timer.min.js';
  //_t.src = 'js/timer.min.js';
  var _f = function (_k) {
    var l = new MegaTimer(_id, {
      'view': [
        1,
		1,
        1,
        1
      ],
      'type': {
        'currentType': '1',
        'params': {
          'usertime': true,
          'tz': '3',
          'utc': 2431119000000
        }
      },
      'design': {
        'type': 'circle',
        'params': {
          'width': '1',
          'radius': '37',
          'line': 'gradient',
          'line-color': [
            '#ffe000',
            '#f70000'
          ],
          'background': 'opacity',
          'direction': 'direct',
          'number-font-family': {
            'family': 'Comfortaa',
            'link': '<link href=\'http://fonts.googleapis.com/css?family=Comfortaa&subset=latin,cyrillic\' rel=\'stylesheet\' type=\'text/css\'>'
          },
          'number-font-size': '30',
          'number-font-color': 'black',
          'separator-margin': '6',
          'separator-on': true,
          'separator-text': ':',
          'text-on': true,
          'text-font-family': {
            'family': 'Arial'
          },
          'text-font-size': '12',
          'text-font-color': '#434343'
        }
      },
      'designId': 5,
      'theme': 'white',
      'width': 271,
      'height': 79
    });
    if (_k != null) l.run();
  };
  _t.onload = _f;
  _t.onreadystatechange = function () {
    if (_t.readyState == 'loaded') _f(1);
  };
  var _h = document.head || document.getElementsByTagName('head') [0];
  _h.appendChild(_t);
}).call(this);
