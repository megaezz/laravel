﻿$(document).ready(function(){
	$('.color-block ul>li').click(function() {
		$('body').find('input[name="color"]').val($(this).attr('value'));
		$('body').find('.color-block ul').children('li').removeClass('active');
		$('body').find('.color-block ul>li.' + $(this).attr('class')).addClass('active');
		$('body').find('.info-block4 .container').children('.item').children('.image').children('.front').hide().animate({"left" : "0"},300);
		$('body').find('.info-block4 .container').children('.item').children('.image').children('.back').animate({"left" : "0"},300);
		$('body').find('.info-block4 .container').children('.item').removeClass('active');
		$('body').find('.info-block4 .item[value="' + $(this).attr('value') + '"]').addClass('active');
		$('body').find('.info-block4 .item[value="' + $(this).attr('value') + '"]').children('.image').children('.front').show().animate({"left" : "39px"},500);
		$('body').find('.info-block4 .item[value="' + $(this).attr('value') + '"]').children('.image').children('.back').animate({"left" : "-39px"},500);
		$(this).parent().children('li').removeClass('active');
		$(this).addClass('active');
	});
	
	$('.info-block4').on('click', '.item:not(.active)', function() {
		$('body').find('input[name="color"]').val($(this).attr('value'));
		$(this).parent().children('.item').children('.image').children('.front').hide().animate({"left" : "0"},300);
		$(this).parent().children('.item').children('.image').children('.back').animate({"left" : "0"},300);
		$('body').find('.color-block ul').children('li').removeClass('active');
		$(this).parent().children('.item').removeClass('active');
		$(this).children('.image').children('.front').show().animate({"left" : "39px"},500);
		$(this).children('.image').children('.back').animate({"left" : "-39px"},500);
		$('body').find('.color-block ul>li[value="' + $(this).attr('value') + '"]').addClass('active');
		$(this).addClass('active');
	});
    GetCount();
});
//Timer
var today = new Date(),
    tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
	tomorrow = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 0, 0, 0);
function GetCount() {
    dateNow = new Date();
    amount = tomorrow.getTime() - dateNow.getTime();
    delete dateNow;
    if (amount < 0) {
        $('.countdown').html('<div><span>00</span><p>Часов</p></div><b>:</b><div><span>00</span><p>Минут</p></div><b>:</b><div><span>00</span><p>Секунд</p></div>'); 
    } else {
        hours = 0;
        mins = 0;
        secs = 0;
        amount = Math.floor(amount / 1000);
        amount = amount % 86400;
        hours = Math.floor(amount / 3600);
        amount = amount % 3600;
        mins = Math.floor(amount / 60);
        amount = amount % 60;
        secs = Math.floor(amount);
        if(hours < 10) hours = '0'+hours;
        if(mins < 10) mins = '0'+mins;
        if(secs < 10) secs = '0'+secs;
		
		$('.countdown').html('<div><span>' + hours + '</span><p>Часов</p></div><b>:</b><div><span>' + mins + '</span><p>Минут</p></div><b>:</b><div><span>'+ secs + '</span><p>Секунд</p></div>'); 
        setTimeout("GetCount()", 1000);
    }
}

//Scrolling
jQuery().ready(function() {
	$('a[href*=#]:not([href=#]):not(.fancybox)').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top-55
				}, 1000);
				return false;
			}
		}
	});
});

$('.fancybox').fancybox();
