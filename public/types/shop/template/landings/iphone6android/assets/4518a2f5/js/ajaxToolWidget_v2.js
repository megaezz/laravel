//alert(1);

/*
 Цель: получить все значения полей из .ajaxToolForm #main, кроме значений полей которые внутри других .ajaxToolForm

 другими словами - нужно рекурсивно искать поля внутри ajaxToolForm, вглубь, как это делает .find(), но обходить дочерние элементы класса .ajaxToolForm И ИХ ДОЧЕРНИЕ ЭЛЕМЕНТЫ ТОЖЕ (чего не удалось добиться через find)

 Это позволит без конфликтов вкладывать ajaxToolForm-ы друг в друга.

 http://jsfiddle.net/MaxLord/WkSkD/
 */

/*
var AjaxToolWidget = function(){

}
*/

function showAjaxToolWidgetError(html){
    var NewDialog = $("<div class=\"ajaxToolWidgetError\"> <a class=\"pull-right close-btn\"  onclick=\"$(this).parent().remove();return false;\"><i class=icon-close></i>Закрыть</a>"+html+"</div>");
    NewDialog.css('top', (window.scrollY+80)+'px'); // выводим предупреждение на уровне глаз
    $("body").append(NewDialog);
}

function jsUnserialize(serializedString){
    var str = decodeURI(serializedString);
    var pairs = str.split('&');
    var obj = {}, p, idx, val;
    for (var i=0, n=pairs.length; i < n; i++) {
        p = pairs[i].split('=');
        idx = p[0];

        if (idx.indexOf("[]") == (idx.length - 2)) {
            // Eh um vetor
            var ind = idx.substring(0, idx.length-2)
            if (obj[ind] === undefined) {
                obj[ind] = [];
            }
            obj[ind].push(p[1]);
        }
        else {
            obj[idx] = p[1];
        }
    }
    return obj;
}


/**
 * @param element
 * @param parsefiles
 * @param callback
 */
function findAjaxToolFormInputs(element, parsefiles,submitName, callback){

    if(window.ajaxToolFormInputCounter===undefined){
        window.ajaxToolFormInputCounter=[];
    }

    // для считчика надо использовать глобальную переменную, локальные срабатывают неправильно - callback вызывается несколько раз или не в том месте.
    var uid=element.parent().parent().attr("id");
    if(window.ajaxToolFormInputCounter[uid]===undefined){
        window.ajaxToolFormInputCounter[uid]=[];
    }
    window.ajaxToolFormInputCounter[uid]["counter"]=0;


    window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"]=false;
    if (parsefiles && window.FormData) {
        // не все браузеры поддерживают, но это позволяет загружать файлы через js
        window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"] = new FormData();
    }else{
        window.ajaxToolFormInputCounter[uid]["dataOldBrowsers"]=new Object;
    }



    var pseudoCallback=function(){
        // console.log("pseudoCallback", window.ajaxToolFormInputCounter[uid]["counter"], results);
        if (window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"]){
            // конвертим results в formdata
            //for(var key in results){
            //    if(results.hasOwnProperty(key)){
            //        window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"].append(key, results[key]);
            //    }
            //}
            if(typeof submitName === 'undefined'){
                //ничего
            }else{
                window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"].append('submit', submitName);
            }
            callback(window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"]);
        }else{
            if(typeof submitName === 'undefined'){
                //ничего
            }else{
                jQuery.extend(window.ajaxToolFormInputCounter[uid]["dataOldBrowsers"], {submit : submitName});
            }
            var results=window.ajaxToolFormInputCounter[uid]["dataOldBrowsers"];
            callback(results);
        }
    }


    findAjaxToolFormInputs2(element, pseudoCallback, uid);
}

function findAjaxToolFormInputs2(element, callback, uid){

    var local_children=element.children('input:not([type=file]), textarea, select');


    if(window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"]){

        if(!empty(local_children[0])){
            var i1 = 0, len1 = local_children.length, input, value;
            for ( ; i1 < len1; i1++ ) {
                input=local_children[i1];

                if(input.type=="checkbox"){
                    // мутки с чекбоксами
                    if(input.checked){
                        value=1;
                    }else{
                        // пустые чекбоксы даже не передаются в request
                        continue;
                    }
                }else if(input.type=="radio"){
                    if(input.checked){
                        value=input.value;
                    }else{
                        continue;
                    }
                }
                else if(input.type=="select-one"){
                    value=input.value;
                }
                else{
                    value=encodeURIComponent(input.value)
                }
                window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"].append(input.name, value);
            }
        }


        var local_children_files=element.children('input[type=file]');
        if(!empty(local_children_files[0])){
            var i = 0, len = local_children_files.length, file, fileFile;
            for ( ; i < len; i++ ) {
                file=local_children_files[i];
                var i2 = 0, len2 = file.files.length, file2;
                for ( ; i2 < len2; i2++ ) {
                    file2=file.files[i2];
                    window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"].append(file.name+"[]",file2);
                }
            }

            //window.ajaxToolFormInputCounter[uid]["dataNewBrowsers"].append();
            //var fileReader=new FileReader;
            //window.ajaxToolFormInputCounter[uid]["counter"]++;
            //fileReader.readAsDataURL(local_children_files[0].files[0]);
        }
    }else{

        var local_serialized=local_children.serialize();

        var local_results=jsUnserialize(local_serialized);

        // http://stackoverflow.com/questions/1334660/combining-javascript-objects-into-one
        jQuery.extend(window.ajaxToolFormInputCounter[uid]["dataOldBrowsers"], local_results);
    }

    var elements=element.children(":not(.ajaxToolForm)");

    window.ajaxToolFormInputCounter[uid]["counter"]+=elements.length;
    //result[]=jsUnserialize($(this).children('input, textarea, select, option').serialize());
    elements.each(
        function(){
            findAjaxToolFormInputs2($(this), callback, uid);
            window.ajaxToolFormInputCounter[uid]["counter"]--;
        }
    );

    if (window.ajaxToolFormInputCounter[uid]["counter"] == 0 && callback){
        callback();
    }
}