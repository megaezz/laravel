/* используется везде, подключается после jquery*/

function show(elem) {
  if (arguments.length > 1) {
    for (var i = 0; i < arguments.length; ++i) {
      show(arguments[i]);
    }
    return;
  }
  $('#'+elem).show();
}

function hide(elem) {
  if (arguments.length > 1) {
    for (var i = 0; i < arguments.length; ++i) {
      show(arguments[i]);
    }
    return;
  }
  $('#'+elem).hide();
}

function show_hide(elem) {
  if (arguments.length > 1) {
    for (var i = 0; i < arguments.length; ++i) {
      show_hide(arguments[i]);
    }
    return;
  }
    if($('#'+elem).css('display')=='none')
        $('#'+elem).show();
    else
        $('#'+elem).hide();
}


function str_replace(search, replace, subject) {
return subject.split(search).join(replace);
}

function empty(mixed_var){
    return(mixed_var ==="" || mixed_var ===0 || mixed_var ==="0" || mixed_var ===null || mixed_var ===false || mixed_var === undefined);
}

function jsUnserialize(serializedString){
    var str = decodeURI(serializedString);
    var pairs = str.split('&');
    var obj = {}, p, idx, val;
    for (var i=0, n=pairs.length; i < n; i++) {
        p = pairs[i].split('=');
        idx = p[0];

        if (idx.indexOf("[]") == (idx.length - 2)) {
            // Eh um vetor
            var ind = idx.substring(0, idx.length-2)
            if (obj[ind] === undefined) {
                obj[ind] = [];
            }
            obj[ind].push(p[1]);
        }
        else {
            obj[idx] = p[1];
        }
    }
    return obj;
}

function replaceURLWithHTMLLinks(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp,"<a href='$1' target='_blank'>$1</a>");
}


/**
 * Авторесайз
 * https://gist.github.com/pamelafox/2156602
 * http://jsfiddle.net/lerouxb/vRSP2/
$(window).load(function(){
    var resizeTextarea = function() {
        this.style.height = "";

        var
            $this = $(this),
            outerHeight = $this.outerHeight(),
            scrollHeight = this.scrollHeight,
            innerHeight = $this.innerHeight(),
            magic = outerHeight - innerHeight;
        this.style.height = scrollHeight + magic + "px";
    };

    // keydown is too soon and keyup only happens AFTER the character is inserted,
    // so the screen would have updated already.
    // So just set overflow: hidden on the textarea.
    // not sure about cut/paste events.
    $('textarea')
        .keydown(resizeTextarea)
        .keyup(resizeTextarea)
        .change(resizeTextarea)
        .focus(resizeTextarea);
});
 */

