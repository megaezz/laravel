var ImageWidget = function ImageWidget(params){
    this.id = params.id;

    this.type = params.type;
    this.href = params.href;
    this.text = params.text;

    this.smallImage = jq_144('#' + this.id).find('img');

    this.init();
};

ImageWidget.prototype.init = function(){
    if(this.type == 'mouseover'){
        this.initMouseOver(this.href);
        return;
    }
    this.initClick(this.href, this.text);
};

ImageWidget.prototype.initMouseOver = function(href){
    var smallImg = this.smallImage;
    var parent = smallImg.parent();
    var zIndex = parent.css('z-index');
    var d2 = {
        width : smallImg.width(),
        height : smallImg.height()
    };
    var bigImg = jq_144('<img />');
    bigImg.css('position', 'absolute');
    bigImg.css('top', '0');
    bigImg.css('left', '0');
    bigImg.css('z-index', '1000');
    bigImg.css('visibility', 'hidden');

    var img = new Image();
    var d = {};
    img.onload = function(){
        d.width = this.width;
        d.height = this.height;

        bigImg.attr('src', href);
        bigImg.css('width', d2.width + 'px');
        bigImg.css('height', d2.height + 'px');

        smallImg.parent().append(bigImg);

        var interval = null;
        bigImg.hover(function(e){
            e.preventDefault();
            clearInterval(interval);
            interval = setTimeout(function(){
                parent.css('z-index', 1000);
                bigImg.animate({
                    height: d.height + 'px',
                    width: d.width + 'px',
                    left: -(d.width - d2.width)/2 + 'px',
                    top: -(d.height - d2.height)/2 + 'px'
                }, "fast");
            }, 100);
        }, function(e){
            e.preventDefault();
            clearInterval(interval);
            interval = setTimeout(function(e){
                bigImg.animate({
                    height: d2.height + 'px',
                    width: d2.width + 'px',
                    left: 0 + 'px',
                    top: 0 + 'px'
                }, "fast", function(){
                    bigImg.css('visibility', 'hidden');
                    parent.css('z-index', zIndex);
                });
            }, 100);
        });


    };
    img.src = href;

    smallImg.mouseover(function(){
        bigImg.css('visibility', 'visible');
    });
};

ImageWidget.prototype.initClick = function(href, text){
    this.smallImage.css('cursor', 'pointer');
    this.smallImage.click(function(e){
        e.preventDefault();
        jq_144.fancybox({
            'padding'		: 0,
            'href'			: href,
            'title'   		: text,
            'transitionIn'	: 'elastic',
            'transitionOut'	: 'elastic'
        });
    });
};


widget.init('image', ImageWidget);