$(document).ready(function(){
 
	$('.show_popup').click(function () {
		$('div.'+$(this).attr("rel")).fadeIn(500);
		$("body").append("<div id='overlay'></div>");
		$('#overlay').show().css({'filter' : 'alpha(opacity=90)'});
		var value = $( this ).attr('data-id');
		$( "#tovar_id" ).val( value );	
	return false;			
	});	
	$('.cross, #overlay').click(function () {
		$('.popup' ).fadeOut(100);
		$('#overlay').remove('#overlay');
		return false;
	});
	$('.show_cart').click(function () {
		$('div.'+$(this).attr("rel")).css('top', '0');
		return false;
	});
	$('.cross, .show_popup').click(function () {
		$(this).parents('.cart').css('top', '100%');
		return false;
		
	});
	
	$( ".kit-block div:nth-child(3)" ).addClass('push-right');
	$( ".kit-block div:nth-child(1)" ).addClass('push-1-3');
	
});