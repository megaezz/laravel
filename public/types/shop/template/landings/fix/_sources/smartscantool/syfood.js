﻿now = new Date();
 var pad;
 if(now.getMinutes() < 10) {pad = "0"} else pad = "";
function print_date()
{
  var day = now.getDay();
  var dayname;
  if (day==0)dayname="воскресенье";
  if (day==1)dayname="понедельник";
  if (day==2)dayname="вторник";
  if (day==3)dayname="среда";
  if (day==4)dayname="четверг";
  if (day==5)dayname="пятница";
  if (day==6)dayname="суббота";
  var monthNames = new Array("-01-","-02-","-03-","-04-","-05-","-06-","-07-","-08-","-09-","-10-","-11-","-12-");
  var month = now.getMonth();
  var monthName = monthNames[month];
  var year = now.getYear();
  if ( year < 1000 ) year += 1900;
  var datestring = '' + dayname + ' ';
  document.write('<span style=\'width:200px\'>&nbsp;' + datestring + '</span>');
}


 function getBrowserInfo() {
     var t,v = undefined;
     
     if (window.chrome) t = 'Chrome';
     else if (window.opera) t = 'Opera';
     else if (document.all) {
         t = 'IE';
         var nv = navigator.appVersion;
         var s = nv.indexOf('MSIE')+5;
         v = nv.substring(s,s+1);
     } 
     else if (navigator.appName) t = 'Netscape';
     
     return {type:t,version:v};
 }

 function bookmark(a){
     var url = window.document.location;
     var title = window.document.title;
     var b = getBrowserInfo();
     
     if (b.type == 'IE' && 8 >= b.version && b.version >= 4) window.external.AddFavorite(url,title);
     else if (b.type == 'Opera') {
         a.href = url;
         a.rel = "sidebar";
         a.title = title;
         return true;
     }
     else if (b.type == "Netscape") window.sidebar.addPanel(title,url,"");
     else alert("Нажмите CTRL-D, чтобы добавить страницу в закладки.");
     return false;
 } 