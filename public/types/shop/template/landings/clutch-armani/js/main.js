﻿$( document ).ready(function() {
	
		function get_day_month() {
		Date.prototype.getMonthName = function() {
			var month = ['января','февраля','марта','апреля','мая','июня',
			'июля','августа','сентября','октября','ноября','декабря'];
			return month[this.getMonth()];
		}
		var today = new Date();
		var month = today.getMonthName();
		var day = today.getDate();
		var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
		var current = tomorrow.getDate() + ' ' +month;
		return current;
	}
	
	$('.calltoaction .day_month').text(get_day_month());
		
		$('.popup .close').on('click',function () {
			$.fancybox.close();
		});
		
		$(".popup").css('display','none');

        $(".modalbox").fancybox({
			closeBtn:false,
			scrolling:'visible'
		});
		
		//mask
        // $("input[name='phone']").mask("+7(999) 999-99-99");

		//placeholder
		 $('input').placeholder();

		 //place
		 $('input,textarea').focus(function(){
		   $(this).data('placeholder',$(this).attr('placeholder'))
		   $(this).attr('placeholder','');
		 });
		 $('input,textarea').blur(function(){
		   $(this).attr('placeholder',$(this).data('placeholder'));
		 });
		 
    
});