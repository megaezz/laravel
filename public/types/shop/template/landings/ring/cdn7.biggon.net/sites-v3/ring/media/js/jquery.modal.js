(function($) {

	$.fn.modal = function() {
		var $container = this;
		$container.append('<table class="modal"><tr><td><h1></h1><div></div></td></tr></table>');
		$container.find('h1').html($container.attr('data-title'));
		$container.find('div').html($container.html());

	}

})(jQuery);
