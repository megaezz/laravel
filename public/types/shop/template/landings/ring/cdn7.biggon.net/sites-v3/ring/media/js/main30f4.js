$(document).ready(function(){

	$('#modal').on('hidden.bs.modal', function (e) {
		$('#modal .modal-body').html('');
	})

	$('.coupon').click(function() {
		var code = prompt('Введите код купона', '');
		if(!code) return;
		$.post('index.html', {coupon: 1, offer: offer_id, code: code}, function(r) {
			if(r.success) {
				offer_id = r.offer_id;
				if(r.curr == 'RUB') r.curr = 'руб.';
				if(r.curr == 'UAH') r.curr = 'грн.';
				$('.coupon').html('Стоимость по купону: ' + r.price + ' ' + r.curr);
				$('.price-rw').html(r.price);
			} else {
				alert(r.message);
			}
		}, 'json');
	});

	$('.order .submit').bind('click', function() {

		if(!$('.order .fio').val()) {
			alert('Вы не указали Фамилию, Имя или Отчество!');
			$('.order .fio').focus();
			return;
		}

		if(!$('.order .tel').val()) {
			alert('Вы не указали Телефон!');
			$('.order .tel').focus();
			return;
		}

		if(!$('.order .addr').val()) {
			alert('Вы не указали Адрес доставки!');
			$('.order .addr').focus();
			return;
		}

		$('.order .submit').hide();
		$('.order .loader').show();

		$.post('index.html', {
			order : 1,
			//debug: 1,
			//test: 1,
			offer: offer_id,
			cnt : 1,
			fio : $('.order .fio').val(),
			tel : $('.order .tel').val(),
			addr: $('.order .addr').val(),
			timezone: -(new Date().getTimezoneOffset()/60)
		}, function(r) {
			if(r.fail) {
				alert(r.message);
			} else if(r.success) {
				location.href='finish?no=' + r.result;
				$('.cnt').val(1);
				$('.fio').val('');
				$('.addr').val('');
				$('.tel').val('');
			}

			$('.order .loader').hide();
			$('.order .submit').show();
		}, 'json');

	});

	$(".various").fancybox({
		maxWidth	: 1000,
		maxHeight	: 800,
		fitToView	: false,
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'fade',
		closeEffect	: 'fade'
	});

});


