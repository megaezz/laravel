$(document).ready(function () {
  $('button').hide();
  $('input[name=name_last]').val('белый').closest('.form-group').hide();
  $('#indexButton2').on('click', function(){
    $(this).closest('#wb_indexForm2').find('button').click();
  });
  $('.color-iphone').change(function(){
    var v = $(this).val();
    $('input[name=name_last]').val(v);
    console.log($('input[name=name_last]').val())
  });
  setInterval(function () {
    var now = new Date();
    var startdate = new Date(2014, 00, 14, 00, 00, 00).getTime();
    var nowdate = now.getTime();
    var period = 36 * 2 * 60 * 1000;
    while (startdate < nowdate) {
      startdate = startdate + period;
    }
    var totalRemains = (startdate - nowdate);
    if (totalRemains > 1) {
      var RemainsSec = (parseInt(totalRemains / 1000));
      var RemainsFullDays = (parseInt(RemainsSec / (24 * 60 * 60)));
      var secInLastDay = RemainsSec - RemainsFullDays * 24 * 3600;
      if (RemainsFullDays < 10){RemainsFullDays="0"+RemainsFullDays};
      var RemainsFullHours = (parseInt(secInLastDay / 3600));
      if (RemainsFullHours < 10){RemainsFullHours="0"+RemainsFullHours};
      var secInLastHour = secInLastDay - RemainsFullHours * 3600;
      var RemainsMinutes = (parseInt(secInLastHour / 60));
      if (RemainsMinutes < 10){RemainsMinutes="0"+RemainsMinutes};
      var lastSec = secInLastHour - RemainsMinutes * 60;
      if (lastSec < 10){lastSec="0"+lastSec};
      //var fullHours = parseInt(RemainsFullHours,10) + RemainsFullDays * 24;
      //$('.timer>.digits').html(fullHours+":"+RemainsMinutes+":"+lastSec);
      $('.timer>.digits').html(RemainsFullDays + ":" + RemainsFullHours + ":" + RemainsMinutes + ":" + lastSec);
    }
    else {$(".timer").remove();}
  }, 1000);
});