$(function(){
    $('a[href^="#"]').click(function (){
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated), body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
    }); 

    now = new Date();
    var perem = (24-now.getHours())*3600;
    $(".timer-container").attr("data-timer", perem);
    $(".timer-container").TimeCircles({
        "animation": "smooth",
        "bg_width": 1,
        "fg_width": 0.04,
        "circle_bg_color": "#008aff",
        "time": {
            "Days": {
                "text": "Days",
                "color": "#798691",
                "show": false
            },
            "Hours": {
                "text": "Часов",
                "color": "#798691",
                "show": true
            },
            "Minutes": {
                "text": "Минут",
                "color": "#798691",
                "show": true
            },
            "Seconds": {
                "text": "Секунд",
                "color": "#798691",
                "show": true
            }
        }
    }); 
});