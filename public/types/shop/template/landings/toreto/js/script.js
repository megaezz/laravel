  // <!-- Подключаем lightbox -->      
  //            $(document).ready(function(){
  //                $('.nivo a').nivoLightbox();
  //            });
  // <!-- Подключаем скролинг -->
  $(document).ready(function() {
      $.scrollIt({
          topOffset: -20
      });
      var $window = $(document),
          $navigation = $("#fix_menu");

      if (!$navigation.hasClass("fixed") && ($window.scrollTop() > $navigation.offset().top)) {
          $navigation.addClass("fixed").data("top", $navigation.offset().top);
      }

      $window.scroll(function() {
          if (!$navigation.hasClass("fixed") && ($window.scrollTop() > $navigation.offset().top)) {
              $navigation.addClass("fixed").data("top", $navigation.offset().top);
          } else if ($navigation.hasClass("fixed") && ($window.scrollTop() <= 564)) {
              $navigation.removeClass("fixed");
          }
      });
  });
  // <!-- Подключаем счетчик -->
  $(function() {
      $(document).ready(function() {
          var addDays = 28800000;
          var timerData = $.cookie('timerData');
          var currTime = (new Date()).getTime();
          //                    console.log(currTime);
          var newTime = (new Date()).getTime() + addDays;
          if (timerData) {
              if (timerData < currTime) {
                  timerData = newTime;
              }
          } else {
              timerData = newTime;
          }
          $.cookie('timerData', timerData);
          $('.counter').each(function() {
              $(this).countdown({
                  timestamp: timerData - 100000,
              });
          });
      });
  });
