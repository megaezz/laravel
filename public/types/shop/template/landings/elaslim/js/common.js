$(function() {

	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};


	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });
	
});

  jQuery().ready(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });

        $(document).ready(function(){
            // действия для кнопок
            $('#show_popup').click(function(){
                show_popup();
            });
            
            $('.hide_popup').click(function(){
                hide_popup();
            });
            return false;
        });
        
        // показать попап
        function show_popup(){
            // получаем размер экрана
            var bg_height = $(document).height();
            var bg_width = $(window).width();
            // задаем темному фону размер на весь экран
            $('.popup_bg').css({
                'width': bg_width,
                'height':bg_height
            });
            // плавно показываем фон и попап
            $('.popup_bg').fadeIn(500);       
            $('.div_popup').fadeIn(500);
        }
        
        // скрыть попап
        function hide_popup(){
            // плавно скрываем попап и фон
            $('.popup_bg').fadeOut(500);        
            $('.div_popup').fadeOut(500);
        }
  $(document).ready(function() {
 $('select[name="country"]').change(function(){
    var el = $(this).val();
    if (el=="RU")
        { 
          $('.box-drr').css('display','none');
          $('.box-redd').css('display','inline-block');
         $(".block-f").removeClass("zzzz")
        }
    else 
        {
         $('.box-redd').css('display','none');
         $('.box-drr').css('display','inline-block');
         $(".block-f").addClass("zzzz")
        }
});
});



function get_timer_52(string_was_52, string_sec_52) {
    var date_new_was_52 = new Date(string_was_52);
    var date_new_sec_52 = string_sec_52;
    var date_52 = new Date();
    var razn_52, left_52, left_n_52, vraz_52, ost_52;
    razn_52 = date_52 - date_new_was_52;
    left_52 = date_new_sec_52 - razn_52;
    if (left_52 > 0) {
        left_n_52 = left_52;
    } else {
        if (Math.abs(left_52) > date_new_sec_52) {
            vraz_52 = (Math.abs(left_52)) / date_new_sec_52;
            vraz_52 = vraz_52.toString().split(".");
            left_n_52 = Math.abs(left_52) - (vraz_52[0]) * date_new_sec_52;
            left_n_52 = date_new_sec_52 - left_n_52;
        } else {
            left_n_52 = date_new_sec_52 - Math.abs(left_52);
        }
    }
    ost_52 = left_n_52;
    var day_52 = parseInt(ost_52 / (60 * 60 * 1000 * 24));
    if (day_52 < 10) {
        day_52 = "0" + day_52;
    }
    day_52 = day_52.toString();
    var hour_52 = parseInt(ost_52 / (60 * 60 * 1000)) % 24;
    if (hour_52 < 10) {
        hour_52 = "0" + hour_52;
    }
    hour_52 = hour_52.toString();
    var min_52 = parseInt(ost_52 / (1000 * 60)) % 60;
    if (min_52 < 10) {
        min_52 = "0" + min_52;
    }
    min_52 = min_52.toString();
    var sec_52 = parseInt(ost_52 / 1000) % 60;
    if (sec_52 < 10) {
        sec_52 = "0" + sec_52;
    }
    sec_52 = sec_52.toString();
    timethis_52 = day_52 + " : " + hour_52 + " : " + min_52 + " : " + sec_52;
    $(".timerhello_52 p.result .result-day").text(day_52);
    $(".timerhello_52 p.result .result-hour").text(hour_52);
    $(".timerhello_52 p.result .result-minute").text(min_52);
    $(".timerhello_52 p.result .result-second").text(sec_52);
}
function getfrominputs_52() {
    string_was_52 = "Sun Jan 31 2016 11:29:09 GMT+0500 (RTZ 4 (зима))";
    string_sec_52 = "86400900";
    get_timer_52(string_was_52, string_sec_52);
    setInterval(function () {
        get_timer_52(string_was_52, string_sec_52);
    }, 1000);
}
$(document).ready(function () {
    getfrominputs_52();
});

    $("#drugoe").click(function() {
          $('.window-container iframe').toggleClass("disp");
})
    
$(function(){
    $(window).scroll(function() {
        var top = $(document).scrollTop();
        if (top < 100) $(".floating").css({top: '0', position: 'relative'});
        else $(".floating").css({top: '0px', position: 'fixed'});
    });
});

