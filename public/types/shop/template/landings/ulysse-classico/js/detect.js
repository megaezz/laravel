var detects = {
    paste: [],
    timer: 0,
    interval: null
};
function onPaste(e) {
    var tagName = e.target.tagName;
    var name = $(e.target).attr('name');
    detects.paste.push({
        name: name,
        tag: tagName
    });
}

function startTimer() {
    detects['interval'] = setInterval(function () {
        detects.timer++;
    }, 1000);
}
function stopTimer() {
    clearInterval(detects.interval);
}

startTimer();

