// Выгрузка cookie
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined
}


function lastpack(numpack) {
    var minNumPack = 6; // Минимальное количество упаковок
    var lastClass = $('.lastpack'); // Объект
    var numpackCookie = getCookie("lastpack");

    if(numpackCookie == undefined) {
        document.cookie = numpack;
    } else {
        var numpack =  numpackCookie;
    }
    
    if (numpack > minNumPack) {
        numpack--;

        var numObj = new Array();
        if (numpack < 10) {
            numObj[0] = "0";
            numObj[1] = numpack;
        } else {
            var numObj = numpack+"";
        }

        document.cookie = "lastpack="+numpack;
        lastClass.html("<li>0</li><li>"+numObj[0]+"</li><li>"+numObj[1]+"</li>");
    } else {
        lastClass.html("<li>0</li><li>0</li><li>"+minNumPack+"</li>");
    }

    setTimeout(lastpack, 45000, numpack);
}

$(document).ready(function() {
    // Подгрузка цен
    $('[name="country"]').on('change', function() {
        var geoKey = $(this).find('option:selected').val();
        var data = $jsonData.prices[geoKey];
        var price = data.price;
        var oldPrice = data.old_price;
        var currency = data.currency
        $("[value = "+geoKey+"]").attr("selected", true).siblings().attr('selected', false);

        $('.price_land_s1').text(price);
        $('.price_land_s2').text(oldPrice);
        $('.price_land_curr').text(currency);
    });

    // Плавный скролл
    $('a[href*=#]').click( function(){
        var scroll_el = $(this).attr('href');
        if ($(scroll_el).length != 0) {
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);
        }
        return false;
    });

    // Валидация
    

    lastpack(20);

    // Инициализация таймера
    initializeClock('timer', deadline);

    $('.swipebox').swipebox();

    $('.color-block ul>li').click(function() {
        $('body').find('input.dop_params').val($(this).attr('value'));
        $('body').find('.color-block ul').children('li').removeClass('active');
        $('body').find('.color-block ul>li.' + $(this).attr('class')).addClass('active');
        $('body').find('.info-block4 .container').children('.item').children('.image').children('.front').hide().animate({"left" : "0"},300);
        $('body').find('.info-block4 .container').children('.item').children('.image').children('.back').animate({"left" : "0"},300);
        $('body').find('.info-block4 .container').children('.item').removeClass('active');
        $('body').find('.info-block4 .item[value="' + $(this).attr('value') + '"]').addClass('active');
        $('body').find('.info-block4 .item[value="' + $(this).attr('value') + '"]').children('.image').children('.front').show().animate({"left" : "39px"},500);
        $('body').find('.info-block4 .item[value="' + $(this).attr('value') + '"]').children('.image').children('.back').animate({"left" : "-39px"},500);
        $(this).parent().children('li').removeClass('active');
        $(this).addClass('active');
    });
    
    $('.info-block4').on('click', '.item:not(.active)', function() {
        $('body').find('input.dop_params').val($(this).attr('value'));
        $(this).parent().children('.item').children('.image').children('.front').hide().animate({"left" : "0"},300);
        $(this).parent().children('.item').children('.image').children('.back').animate({"left" : "0"},300);
        $('body').find('.color-block ul').children('li').removeClass('active');
        $(this).parent().children('.item').removeClass('active');
        $(this).children('.image').children('.front').show().animate({"left" : "39px"},500);
        $(this).children('.image').children('.back').animate({"left" : "-39px"},500);
        $('body').find('.color-block ul>li[value="' + $(this).attr('value') + '"]').addClass('active');
        $(this).addClass('active');
    });

    var ic = 0;
    $('.spec').click( function(){
        ic++
        if(ic == 6) {
            $('.fake').slideToggle();
        }
    });
});