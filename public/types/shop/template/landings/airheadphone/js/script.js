$(function() {
    $('a[href*=#]:not([href=#]):not(.fancybox)').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    $('.flexslider').flexslider( {
        animation: "slide",
        start: function(slider){
            $('body').removeClass('loading');
        }
    });
	/*
    $(".fancybox").fancybox({
        //'centerOnScroll' : 'false',
        'titlePosition' : 'over',
        'transitionIn' : 'fade',
        'speedIn' : '150',
        'transitionOut' : 'fade',
        'speedOut' : '150',
        'overlayColor' :  '#4d232f',
        'overlayOpacity' : '0.6',
        'autoDimensions': false,
        'width' : '435',
        'height' : 'auto',
        'padding' : '30'
    });
    if ($('html').width()>990) {
        $("#delivery").click(function() {
            $('#fancybox-wrap').css('margin-left','-250px')
        });
        $("#contact").click(function() {
            $('#fancybox-wrap').css('margin-left','250px')
        });
    }*/
});
