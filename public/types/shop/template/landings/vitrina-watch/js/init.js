$(function(){
    $('[placeholder]').placeholder();
    $('a[href^="#"]').click(function (){
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated), body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
    });
    $('a.tov-button').click(function () {
        var title = $(this).attr("title");
        $(".popup").fadeIn(500);
        $("body").append("<div id='overlay'></div>");
        $('#overlay').show().css('opacity','0.8');
        $('.popup .center').prepend("<div id='tov-title'>"+title+"</div>");
        $("input[type='hidden']").val(title);                          
        $('a.close, #overlay').click(function () {
            $('.popup').fadeOut(100);
            $('#overlay').remove();
            $('#tov-title').remove();
            return false;
        });
        $('.popup').click(function(e){
            e.stopPropagation();    
        });
        return false;                
    });
});
 