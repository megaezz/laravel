$(function(){
    $('a[href^="#"]').click(function (){
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated), body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
    });
    $('.slider-wrap').slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed: 4000,
      dots: false,
      arrows: true,
      fade: false,
      speed: 800,
      slidesToShow: 1,
      slidesToScroll: 1
    });
});