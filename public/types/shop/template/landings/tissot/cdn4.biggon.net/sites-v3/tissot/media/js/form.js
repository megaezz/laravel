function ajaxsend_form(id) { //Ajax отправка формы
    var msg = $("#form"+id).serialize();
    var delay_popup = 0000;
    var faults = $('input#input'+id).filter(function() {
    return $(this).data('required') && $(this).val() === "";
    }).css({"border-color": "#ee5151"}); // выделяем это поле красным
    if(faults.length) {return false; }
    else
    {
    $.ajax({
        type: "POST",
        url: "php/mail.php",
        data: msg,
        success: function(data) {
            $("#text_send").html(data);
            setTimeout("document.getElementById('parent_popup').style.display='block'", delay_popup);
        },
        error:  function(xhr, str){
            alert("Возникла ошибка!");
        }
    });
    }
}

jQuery.fn.notExists = function() { //Проверка на существование элемента
    return $(this).length == 0;
}