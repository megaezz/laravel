(function () {

    function ModuleGeotarget() {
        if (typeof YMaps !== 'undefined' && YMaps.location.city.length >= 1) {
            this.city = YMaps.location.city;
        } else {
            this.city = '';
        }
        this.context = $("#sections_list");
        this.replaceCity();
    }

    ModuleGeotarget.prototype.replaceCity = function () {
        var that = this;
        this.context.find('span.geotarget_module').removeClass('hidden').text(that.city);
    };

    window.module_geotarget = new ModuleGeotarget();

}());
