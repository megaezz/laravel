$(document).ready(function(){

 	var name = document.getElementById('placeName');
	var phone = document.getElementById('placePhone');

	name.value = 'Ваше имя';
	phone.value = 'Телефон';
	
	
	name.onfocus = function() {
		if(name.value == 'Ваше имя')
			name.value = '';
	}

	name.onblur = function() {
		if(name.value == '') {
			name.value = 'Ваше имя';
		}
	}

	phone.onfocus = function() {
		if(phone.value == 'Телефон')
			phone.value = '';
	}

	phone.onblur = function() {
		if(phone.value == '') {
			phone.value = 'Телефон';
		}
	}

});