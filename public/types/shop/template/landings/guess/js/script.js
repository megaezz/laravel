$(function(){
	
	var note = $('#note'),
		ts = new Date(new Date().getTime() + 9*35*60*1000);
		$('#countdown, #countdown2, #countdown3, #countdown1').each(function() {
			$(this).countdown({
		timestamp	: ts,
		callback	: function(days, hours, minutes, seconds){
			
			var message = "";
			
			message += days + " day" + ( days==1 ? '':'s' ) + ", ";
			message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
			message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
			message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";
			
			note.html(message);
		}
	});
});

});
