$(function() {
    $('#timer').timeTo(5435, {
        displayDays: 2,
        theme: "black",
        displayCaptions: true,
        fontSize: 40,
        captionSize: 14
    });

    $('.scroll-bot').click(function() {
        $("html, body").animate({ scrollTop: $(".row-6").offset().top }, "slow");
        return false;
    });

    function detectIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        var trident = ua.indexOf('Trident/');

        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        if (trident > 0) {
            // IE 11 (or newer) => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        // other browser
        return false;
    }


    if(!detectIE()){
        $('.animated').css('opacity', '0');
    }

    $(function(){
        $('.animated').each(function(){
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+800) {
                $(this).addClass($(this).attr('data-effect'));
            }
            else{
                if(!detectIE()){
                    $(this).removeClass($(this).attr('data-effect'));
                    $(this).css('opacity', '0');
                }
            }
        });
    });

    $(window).scroll(function() {
        $('.animated').each(function(){
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+800) {
                $(this).addClass($(this).attr('data-effect'));
            }
            else{
                if(!detectIE()){
                    $(this).removeClass($(this).attr('data-effect'));
                    $(this).css('opacity', '0');
                }
            }
        });
    });

    $('.menu a').click(function() {
        $('.menu li').removeClass('selected');
        $(this).parent().addClass('selected');
        $("html, body").animate({ scrollTop: $($(this).data('scroll')).offset().top }, "slow");
        return false;
    });

});