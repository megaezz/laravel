$(function(){
    function scrollMenu(dest){
        $("html:not(:animated), body:not(:animated)").animate({scrollTop: destination}, 800);
    }

    $("span.sale").each(function(){
        var el_s = $(this).text();
        if ((parseInt(el_s) * -1) > 65) 
            $(this).addClass("col-1");
    })
    
    $('.catalog-cont-img').click(function(){
        var _this = $(this);
        if(!_this.hasClass('active')){
            $('.catalog-cont-img').removeClass('active').next('div').slideUp(200);
            _this.addClass('active').next('div').slideDown(200, function(){
                $('html, body').animate({
                    scrollTop: _this.offset().top
                }, 250);
            });
        } else {
            _this.removeClass('active').next('div').slideUp();
        }
    });

});
 