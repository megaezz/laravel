$(document).ready(function() {

    var text1_1 = "Ночная акция! С 22:00-02:00  скидка 50%!<br> Осталось всего 4 штуки!";
    var text2_1 = "С 22:00-02:00  скидка 50%. Внимание! осталось 4 штуки.";
    var text1_2 = "5 часов тотальной распродажи!<br> С 02:00 до 07:00 скидка 50%!";
    var text2_2 = "С 02:00 до 07:00 скидка 50%. Внимание! осталось 4 штуки.";
    var text1_3 = "3 часа тотальной распродажи!<br> С 07:00 до 10:00 скидка 50%!";
    var text2_3 = "С 07:00 до 10:00 скидка 50%. Внимание! осталось 4 штуки.";
    var text1_4 = "4 часа тотальной распродажи!<br> С 10:00 до 14:00 скидка 50%!";
    var text2_4 = "С 10:00 до 14:00 скидка 50%. Внимание! осталось 4 штуки.";
    var text1_5 = "4 часа безумных скидок!<br> С 14:00 до 18:00 скидка 50%!";
    var text2_5 = "С 14:00 до 18:00 скидка 50%. Внимание! осталось 4 штуки.";
    var text1_6 = "Ограниченная акция!<br> С 18:00 до 22:00 скидка 50%!";
    var text2_6 = "С 18:00 до 22:00 скидка 50%. Внимание! осталось 4 штуки.";
    var date = new Date();
    var day = date.getDate();
    var year = date.getFullYear();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var mounth = date.getMonth();
    var end_date;

    if(hour>=22 || hour<2){
        $('.variant_text1').html(text1_1);
        $('.variant_text2').html(text2_1);
        if(hour<2){
            end_date = new Date(year, mounth, day, 1, 59, 59, 0);
        } else {
            end_date = new Date(year, mounth, date.getDate()+1, 1, 59, 59, 0);
        }

        myTime(end_date.getTime());
        GetCount(end_date.getTime());
    }
    if(hour>=2 && hour<7){
        $('.variant_text1').html(text1_2);
        $('.variant_text2').html(text2_2);
        end_date = new Date(year, mounth, day, 6, 59, 59, 0);
        myTime(end_date.getTime());
    }
    if(hour>=7 && hour<10){
        $('.variant_text1').html(text1_3);
        $('.variant_text2').html(text2_3);
        end_date = new Date(year, mounth, day, 9, 59, 59, 0);
        myTime(end_date.getTime());
    }
    if(hour>=10 && hour<14){
        $('.variant_text1').html(text1_4);
        $('.variant_text2').html(text2_4);
        end_date = new Date(year, mounth, day, 13, 59, 59, 0);
        myTime(end_date.getTime());
    }
    if(hour>=14 && hour<18){
        $('.variant_text1').html(text1_5);
        $('.variant_text2').html(text2_5);
        end_date = new Date(year, mounth, day, 17, 59, 59, 0);
        myTime(end_date.getTime());
    }
    if(hour>=18 && hour<22){
        $('.variant_text1').html(text1_6);
        $('.variant_text2').html(text2_6);
        end_date = new Date(year, mounth, day, 21, 59, 59, 0);
        myTime(end_date.getTime());
    }

    fixMenuHeight();
    $(window).resize(function(){
        fixMenuHeight();
        $('.fix_i_b, .text_variant_wrap_mob, .fix_btn_control').removeClass('turn');
        $('.fix-menu').removeClass('turn').animate({height: 'show'}, 500);
        $('.turn_btn_wrapper').removeClass('turn');
    });

    $('.fix_close').on('click', function(event){
        event.preventDefault();
        $('.fix-menu').css('min-height', 0).animate({height: 'hide'}, 500);
        $('body').animate({'padding-top': 0}, 500);
    });

    $('.fix_turn').on('click', function(event){
        event.preventDefault();
        $('.fix_i_b, .text_variant_wrap_mob, .fix_btn_control').addClass('turn'); // dn
        $('.fix-menu').addClass('turn');//min 40
        $('.turn_btn_wrapper').addClass('turn');
        $('body').animate({'padding-top': '40px'}, 100);
    });


	function actionStickyHeight() {
		var actionSticky = $('.action-sticky').outerHeight();
		$('.action-sticky-wrapper').css({'padding-top': actionSticky + 'px'});
	}
	actionStickyHeight();
	setTimeout(function () {
		actionStickyHeight();
	}, 100);
	$(window).resize(function () {
		actionStickyHeight();
	});

	$('.action-sticky-closer').on('click', function (e) {
		if(!$('.action-sticky-closer').length){ return; }
		$('body').toggleClass('action-sticky-hidden');
		actionStickyHeight();

		e.preventDefault();
	})

	$('.btn-default, .el3_2 a').on('click', function(event){
		event.preventDefault();
		var form_position = $('.form').offset().top;
		var menu_height = $('.action-sticky-wrapper').height();

		$('body, html').animate({scrollTop: form_position-menu_height}, 400);
	})

});

function myTime(end_date){

    var time_current = new Date();

    time_current = time_current.getTime();

    var time_interval = (end_date - time_current)/1000;

    $('.counter, #time').each(function(){
        timer(this, time_interval);
    });
}

function timer(element, counter) {
    var el = $(element);
    var html = '';
    if (counter) {
        el.html(formatTime(counter));
        setInterval(function(){
            if (counter > 1) {
                counter = counter - 1;
                el.html(formatTime(counter));
            }
        }, 1000);
    }
}

function two(a) {
    return parseInt(a - Math.floor(a/10)*10).toString();
}

function one(a) {
    return Math.floor(a/10).toString();
}

function formatTime(seconds) {
    seconds = Math.floor(seconds);
    var minutes = Math.floor(seconds / 60), hours = Math.floor(minutes / 60), days = Math.floor(hours / 24);
    //hours = hours % 24;
    minutes %= 60;
    seconds %= 60;
    var hours_name;
    if(hours==1){
        hours_name = "час";
    } else {
        if(hours==0){
            hours_name = "часов";
        } else {
            hours_name = "часа";
        }
    }

    html = '<li class="count-div">' + one(hours) + two(hours) + '</li><li class="count_separator">:</li><li class="count-div">' + one(minutes) + two(minutes) + '</li><li class="count_separator">:</li><li class="count-div ">' + one(seconds) + two(seconds) + '</li>';
    return html;
};


function fixMenuHeight(){
    var menu_height = $('.fix-menu').height();
    $('body').css({'padding-top': menu_height + 'px'});
}
