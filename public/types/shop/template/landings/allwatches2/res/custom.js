$(document).ready(function(){
	$("#header > .wrapper > ul > li > a").click(function(){
		event.preventDefault();
		$("body").animate({scrollTop: $(this.hash).offset().top-50}, 1000);
	});
	$(".move").click(function(){
		$("body").animate({scrollTop: $("#advantages").offset().top-50}, 1000);
	});
	$(".move").click(function(){
		$("body").animate({scrollTop: $("#advantages").offset().top-50}, 1000);
	});
	$(".moveWatch").click(function(){
		$("body").animate({scrollTop: $(".model").eq($(this).attr("data-id") - 1).offset().top-50}, 1000);
	});
	popup();
	videoPopup();
	watches();
	mobile();
	GetCount();
});
$(window).load(function(){
	$(".model > .wrapper > img").each(function(){
		$width = -$(this).width()/2 + "px";
		$(this).css({marginLeft: $width});
	});
});
$(window).scroll(function(){
	if (!$("#header").hasClass("sticky") && ($(window).scrollTop() > 10)) {
		$("#header").addClass("sticky");
	}
	else if ($("#header").hasClass("sticky") && ($(window).scrollTop() < 10)) {
		$("#header").removeClass("sticky");
	};
});
function popup() {
	$(".popupOpen").click(function(){
		$("#popup").fadeIn();
	});
	$("#popupClose").click(function(){
		$("#popup").fadeOut();
	});
	$(".videoOpen").click(function(){
		$("#video").fadeIn();
	});
	$("#videoClose").click(function(){
		$("#video").fadeOut();
	});
	$(".policyOpen").click(function(){
		$("#policy").fadeIn();
	});
	$("#policyClose").click(function(){
		$("#policy").fadeOut();
	});
};
function videoPopup() {
	$(".tag").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/mQAVitua7X0");
	});
	$(".rolex").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/9g_fM8MJBAM");
	});
	$(".nardin").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/PKs5jm5vF1s");
	});
	$(".armani").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/nQDoCjEVm_U");
	});
	$(".tissot").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/SQn3WrFN9PE");
	});
	$(".breit").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/5GA-ma85u8Y");
	});
	$(".hublot").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/99eHkXzuwp8");
	});
	$(".rado2").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/WtVz6pEiw-E");
	});
	$(".skymoon").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/WA6pOzf-CGA");
	});
	$(".disel").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/FBXtIsI2DgE");
	});
	$(".hublot2").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/2KaIeEHEHp0");
	});
	$(".hublot3").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/THz0uuQb-fQ");
	});
	$(".сalibre").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/7YgAuB7Qogo");
	});
	$(".panerai").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/NbBWT-slk7Y");
	});
	$(".weide").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/N_9Aob1Jrnc");
	});
	$(".breitling2").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/PPMU89jxHOY");
	});
	$(".shark").click(function(){
		$("#video > div > iframe").attr("src", "https://www.youtube.com/embed/xhWHcD50Xs0");
	});
};
function watches() {
};
function mobile() {
	$(".mobile").click(function(){
		if ($(".mobile").hasClass("active")) {
			$("#header > .wrapper > ul").stop().fadeOut();
			$(".mobile").removeClass("active");
		}
		else {
			$("#header > .wrapper > ul").stop().fadeIn();
			$(".mobile").addClass("active");
		}
	});
};
var today = new Date(),
    tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
tomorrow = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 0, 0, 0);
function GetCount() {
    dateNow = new Date();
    amount = tomorrow.getTime() - dateNow.getTime();
    delete dateNow;
    if (amount < 0) {
        $('#timer .hours').html('00');
        $('#timer .mins').html('00');
        $('#timer .secs').html('00');
    } else {
        hours = 0;
        mins = 0;
        secs = 0;
        out = "";
        amount = Math.floor(amount / 1000);
        amount = amount % 86400;
        hours = Math.floor(amount / 3600);
        amount = amount % 3600;
        mins = Math.floor(amount / 60);
        amount = amount % 60;
        secs = Math.floor(amount);
        if(hours < 10) hours = '0'+hours;
        if(mins < 10) mins = '0'+mins;
        if(secs < 10) secs = '0'+secs;

        $('#timer .hours').html(hours);
        $('#timer .mins').html(mins);
        $('#timer .secs').html(secs);
        setTimeout("GetCount()", 1000);
    }
}