    ! function(a, b) {
        "use strict";

        function c(a, b) {
            var c, d;
            b = b || {}, a = "raven" + a.substr(0, 1).toUpperCase() + a.substr(1), document.createEvent ? (c = document.createEvent("HTMLEvents"), c.initEvent(a, !0, !0)) : (c = document.createEventObject(), c.eventType = a);
            for (d in b) b.hasOwnProperty(d) && (c[d] = b[d]);
            if (document.createEvent) document.dispatchEvent(c);
            else try {
                document.fireEvent("on" + c.eventType.toLowerCase(), c)
            } catch (e) {}
        }

        function d(a) {
            this.name = "RavenConfigError", this.message = a
        }

        function e(a) {
            var b = O.exec(a),
                c = {},
                e = 7;
            try {
                for (; e--;) c[N[e]] = b[e] || ""
            } catch (f) {
                throw new d("Invalid DSN: " + a)
            }
            if (c.pass) throw new d("Do not specify your private key in the DSN!");
            return c
        }

        function f(a) {
            return "undefined" == typeof a
        }

        function g(a) {
            return "function" == typeof a
        }

        function h(a) {
            return "string" == typeof a
        }

        function i(a) {
            for (var b in a) return !1;
            return !0
        }

        function j(a, b) {
            return Object.prototype.hasOwnProperty.call(a, b)
        }

        function k(a, b) {
            var c, d;
            if (f(a.length))
                for (c in a) a.hasOwnProperty(c) && b.call(null, c, a[c]);
            else if (d = a.length)
                for (c = 0; d > c; c++) b.call(null, c, a[c])
        }

        function l() {
            I = "?sentry_version=4&sentry_client=raven-js/" + M.VERSION + "&sentry_key=" + G
        }

        function m(a, b) {
            var d = [];
            a.stack && a.stack.length && k(a.stack, function(a, b) {
                var c = n(b);
                c && d.push(c)
            }), c("handle", {
                stackInfo: a,
                options: b
            }), p(a.name, a.message, a.url, a.lineno, d, b)
        }

        function n(a) {
            if (a.url) {
                var b, c = {
                        filename: a.url,
                        lineno: a.line,
                        colno: a.column,
                        "function": a.func || "?"
                    },
                    d = o(a);
                if (d) {
                    var e = ["pre_context", "context_line", "post_context"];
                    for (b = 3; b--;) c[e[b]] = d[b]
                }
                return c.in_app = !(!L.includePaths.test(c.filename) || /(Raven|TraceKit)\./.test(c["function"]) || /raven\.(min\.)js$/.test(c.filename)), c
            }
        }

        function o(a) {
            if (a.context && L.fetchContext) {
                for (var b = a.context, c = ~~(b.length / 2), d = b.length, e = !1; d--;)
                    if (b[d].length > 300) {
                        e = !0;
                        break
                    }
                if (e) {
                    if (f(a.column)) return;
                    return [
                        [],
                        b[c].substr(a.column, 50), []
                    ]
                }
                return [b.slice(0, c), b[c], b.slice(c + 1)]
            }
        }

        function p(a, b, c, d, e, f) {
            var g, h;
            ("Error" !== a || b) && (L.ignoreErrors.test(b) || (e && e.length ? (c = e[0].filename || c, e.reverse(), g = {
                frames: e
            }) : c && (g = {
                frames: [{
                    filename: c,
                    lineno: d,
                    in_app: !0
                }]
            }), b = r(b, 100), L.ignoreUrls && L.ignoreUrls.test(c) || (!L.whitelistUrls || L.whitelistUrls.test(c)) && (h = d ? b + " at " + d : b, t(q({
                exception: {
                    type: a,
                    value: b
                },
                stacktrace: g,
                culprit: c,
                message: h
            }, f)))))
        }

        function q(a, b) {
            return b ? (k(b, function(b, c) {
                a[b] = c
            }), a) : a
        }

        function r(a, b) {
            return a.length <= b ? a : a.substr(0, b) + "тАж"
        }

        function s() {
            var a = {
                url: document.location.href,
                headers: {
                    "User-Agent": navigator.userAgent
                }
            };
            return document.referrer && (a.headers.Referer = document.referrer), a
        }

        function t(a) {
            v() && (a = q({
                project: H,
                logger: L.logger,
                site: L.site,
                platform: "javascript",
                request: s()
            }, a), a.tags = q(L.tags, a.tags), a.extra = q(L.extra, a.extra), i(a.tags) && delete a.tags, i(a.extra) && delete a.extra, F && (a.user = F), g(L.dataCallback) && (a = L.dataCallback(a)), (!g(L.shouldSendCallback) || L.shouldSendCallback(a)) && (D = a.event_id || (a.event_id = x()), u(a)))
        }

        function u(a) {
            var b = new Image,
                d = E + I + "&sentry_data=" + encodeURIComponent(JSON.stringify(a));
            b.onload = function() {
                c("success", {
                    data: a,
                    src: d
                })
            }, b.onerror = b.onabort = function() {
                c("failure", {
                    data: a,
                    src: d
                })
            }, b.src = d
        }

        function v() {
            return K ? E ? !0 : (a.console && console.error && console.error("Error: Raven has not been configured."), !1) : !1
        }

        function w(a) {
            for (var b, c = [], d = 0, e = a.length; e > d; d++) b = a[d], h(b) ? c.push(b.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1")) : b && b.source && c.push(b.source);
            return new RegExp(c.join("|"), "i")
        }

        function x() {
            return "xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx".replace(/[xy]/g, function(a) {
                var b = 16 * Math.random() | 0,
                    c = "x" == a ? b : 3 & b | 8;
                return c.toString(16)
            })
        }

        function y() {
            var b = a.RavenConfig;
            b && M.config(b.dsn, b.config).install()
        }

        var z = {
                remoteFetching: !1,
                collectWindowErrors: !0,
                linesOfContext: 7
            },
            A = [].slice,
            B = "?";
        z.wrap = function(a) {
            function b() {
                try {
                    return a.apply(this, arguments)
                } catch (b) {
                    throw z.report(b), b
                }
            }

            return b
        }, z.report = function() {
            function c(a) {
                h(), o.push(a)
            }

            function d(a) {
                for (var b = o.length - 1; b >= 0; --b) o[b] === a && o.splice(b, 1)
            }

            function e() {
                i(), o = []
            }

            function f(a, b) {
                var c = null;
                if (!b || z.collectWindowErrors) {
                    for (var d in o)
                        if (j(o, d)) try {
                            o[d].apply(null, [a].concat(A.call(arguments, 2)))
                        } catch (e) {
                            c = e
                        }
                    if (c) throw c
                }
            }

            function g(a, b, c, d, e) {
                var g = null;
                if (r) z.computeStackTrace.augmentStackTraceWithInitialElement(r, b, c, a), k();
                else if (e) g = z.computeStackTrace(e), f(g, !0);
                else {
                    var h = {
                        url: b,
                        line: c,
                        column: d
                    };
                    h.func = z.computeStackTrace.guessFunctionName(h.url, h.line), h.context = z.computeStackTrace.gatherContext(h.url, h.line), g = {
                        message: a,
                        url: document.location.href,
                        stack: [h]
                    }, f(g, !0)
                }
                return m ? m.apply(this, arguments) : !1
            }

            function h() {
                n || (m = a.onerror, a.onerror = g, n = !0)
            }

            function i() {
                n && (a.onerror = m, n = !1, m = b)
            }

            function k() {
                var a = r,
                    b = p;
                p = null, r = null, q = null, f.apply(null, [a, !1].concat(b))
            }

            function l(b, c) {
                var d = A.call(arguments, 1);
                if (r) {
                    if (q === b) return;
                    k()
                }
                var e = z.computeStackTrace(b);
                if (r = e, q = b, p = d, a.setTimeout(function() {
                        q === b && k()
                    }, e.incomplete ? 2e3 : 0), c !== !1) throw b
            }

            var m, n, o = [],
                p = null,
                q = null,
                r = null;
            return l.subscribe = c, l.unsubscribe = d, l.uninstall = e, l
        }(), z.computeStackTrace = function() {
            function b(b) {
                if (!z.remoteFetching) return "";
                try {
                    var c = function() {
                            try {
                                return new a.XMLHttpRequest
                            } catch (b) {
                                return new a.ActiveXObject("Microsoft.XMLHTTP")
                            }
                        },
                        d = c();
                    return d.open("GET.html", b, !1), d.send(""), d.responseText
                } catch (e) {
                    return ""
                }
            }

            function c(a) {
                if (!h(a)) return [];
                if (!j(v, a)) {
                    var c = ""; - 1 !== a.indexOf(document.domain) && (c = b(a)), v[a] = c ? c.split("\n") : []
                }
                return v[a]
            }

            function d(a, b) {
                var d, e = /function ([^(]*)\(([^)]*)\)/,
                    g = /['"]?([0-9A-Za-z$_]+)['"]?\s*[:=]\s*(function|eval|new Function)/,
                    h = "",
                    i = 10,
                    j = c(a);
                if (!j.length) return B;
                for (var k = 0; i > k; ++k)
                    if (h = j[b - k] + h, !f(h)) {
                        if (d = g.exec(h)) return d[1];
                        if (d = e.exec(h)) return d[1]
                    }
                return B
            }

            function e(a, b) {
                var d = c(a);
                if (!d.length) return null;
                var e = [],
                    g = Math.floor(z.linesOfContext / 2),
                    h = g + z.linesOfContext % 2,
                    i = Math.max(0, b - g - 1),
                    j = Math.min(d.length, b + h - 1);
                b -= 1;
                for (var k = i; j > k; ++k) f(d[k]) || e.push(d[k]);
                return e.length > 0 ? e : null
            }

            function g(a) {
                return a.replace(/[\-\[\]{}()*+?.,\\\^$|#]/g, "\\$&")
            }

            function i(a) {
                return g(a).replace("<", "(?:<|&lt;)").replace(">", "(?:>|&gt;)").replace("&", "(?:&|&amp;)").replace('"', '(?:"|&quot;)').replace(/\s+/g, "\\s+")
            }

            function k(a, b) {
                for (var d, e, f = 0, g = b.length; g > f; ++f)
                    if ((d = c(b[f])).length && (d = d.join("\n"), e = a.exec(d))) return {
                        url: b[f],
                        line: d.substring(0, e.index).split("\n").length,
                        column: e.index - d.lastIndexOf("\n", e.index) - 1
                    };
                return null
            }

            function l(a, b, d) {
                var e, f = c(b),
                    h = new RegExp("\\b" + g(a) + "\\b");
                return d -= 1, f && f.length > d && (e = h.exec(f[d])) ? e.index : null
            }

            function m(b) {
                for (var c, d, e, f, h = [a.location.href], j = document.getElementsByTagName("script"), l = "" + b, m = /^function(?:\s+([\w$]+))?\s*\(([\w\s,]*)\)\s*\{\s*(\S[\s\S]*\S)\s*\}\s*$/, n = /^function on([\w$]+)\s*\(event\)\s*\{\s*(\S[\s\S]*\S)\s*\}\s*$/, o = 0; o < j.length; ++o) {
                    var p = j[o];
                    p.src && h.push(p.src)
                }
                if (e = m.exec(l)) {
                    var q = e[1] ? "\\s+" + e[1] : "",
                        r = e[2].split(",").join("\\s*,\\s*");
                    c = g(e[3]).replace(/;$/, ";?"), d = new RegExp("function" + q + "\\s*\\(\\s*" + r + "\\s*\\)\\s*{\\s*" + c + "\\s*}")
                } else d = new RegExp(g(l).replace(/\s+/g, "\\s+"));
                if (f = k(d, h)) return f;
                if (e = n.exec(l)) {
                    var s = e[1];
                    if (c = i(e[2]), d = new RegExp("on" + s + "=[\\'\"]\\s*" + c + "\\s*[\\'\"]", "i"), f = k(d, h[0])) return f;
                    if (d = new RegExp(c), f = k(d, h)) return f
                }
                return null
            }

            function n(a) {
                if (!a.stack) return null;
                for (var b, c, g = /^\s*at (?:((?:\[object object\])?\S+(?: \[as \S+\])?) )?\(?((?:file|https?):.*?):(\d+)(?::(\d+))?\)?\s*$/i, h = /^\s*(\S*)(?:\((.*?)\))?@((?:file|https?).*?):(\d+)(?::(\d+))?\s*$/i, i = a.stack.split("\n"), j = [], k = /^(.*) is undefined$/.exec(a.message), m = 0, n = i.length; n > m; ++m) {
                    if (b = h.exec(i[m])) c = {
                        url: b[3],
                        func: b[1] || B,
                        args: b[2] ? b[2].split(",") : "",
                        line: +b[4],
                        column: b[5] ? +b[5] : null
                    };
                    else {
                        if (!(b = g.exec(i[m]))) continue;
                        c = {
                            url: b[2],
                            func: b[1] || B,
                            line: +b[3],
                            column: b[4] ? +b[4] : null
                        }
                    }!c.func && c.line && (c.func = d(c.url, c.line)), c.line && (c.context = e(c.url, c.line)), j.push(c)
                }
                return j.length ? (j[0].line && !j[0].column && k ? j[0].column = l(k[1], j[0].url, j[0].line) : j[0].column || f(a.columnNumber) || (j[0].column = a.columnNumber + 1), {
                    name: a.name,
                    message: a.message,
                    url: document.location.href,
                    stack: j
                }) : null
            }

            function o(a) {
                for (var b, c = a.stacktrace, f = / line (\d+), column (\d+) in (?:<anonymous function: ([^>]+)>|([^\)]+))\((.*)\) in (.*):\s*$/i, g = c.split("\n"), h = [], i = 0, j = g.length; j > i; i += 2)
                    if (b = f.exec(g[i])) {
                        var k = {
                            line: +b[1],
                            column: +b[2],
                            func: b[3] || b[4],
                            args: b[5] ? b[5].split(",") : [],
                            url: b[6]
                        };
                        if (!k.func && k.line && (k.func = d(k.url, k.line)), k.line) try {
                            k.context = e(k.url, k.line)
                        } catch (l) {}
                        k.context || (k.context = [g[i + 1]]), h.push(k)
                    }
                return h.length ? {
                    name: a.name,
                    message: a.message,
                    url: document.location.href,
                    stack: h
                } : null
            }

            function p(b) {
                var f = b.message.split("\n");
                if (f.length < 4) return null;
                var g, h, l, m, n = /^\s*Line (\d+) of linked script ((?:file|https?)\S+)(?:: in function (\S+))?\s*$/i,
                    o = /^\s*Line (\d+) of inline#(\d+) script in ((?:file|https?)\S+)(?:: in function (\S+))?\s*$/i,
                    p = /^\s*Line (\d+) of function script\s*$/i,
                    q = [],
                    r = document.getElementsByTagName("script"),
                    s = [];
                for (h in r) j(r, h) && !r[h].src && s.push(r[h]);
                for (h = 2, l = f.length; l > h; h += 2) {
                    var t = null;
                    if (g = n.exec(f[h])) t = {
                        url: g[2],
                        func: g[3],
                        line: +g[1]
                    };
                    else if (g = o.exec(f[h])) {
                        t = {
                            url: g[3],
                            func: g[4]
                        };
                        var u = +g[1],
                            v = s[g[2] - 1];
                        if (v && (m = c(t.url))) {
                            m = m.join("\n");
                            var w = m.indexOf(v.innerText);
                            w >= 0 && (t.line = u + m.substring(0, w).split("\n").length)
                        }
                    } else if (g = p.exec(f[h])) {
                        var x = a.location.href.replace(/#.*$/, ""),
                            y = g[1],
                            z = new RegExp(i(f[h + 1]));
                        m = k(z, [x]), t = {
                            url: x,
                            line: m ? m.line : y,
                            func: ""
                        }
                    }
                    if (t) {
                        t.func || (t.func = d(t.url, t.line));
                        var A = e(t.url, t.line),
                            B = A ? A[Math.floor(A.length / 2)] : null;
                        t.context = A && B.replace(/^\s*/, "") === f[h + 1].replace(/^\s*/, "") ? A : [f[h + 1]], q.push(t)
                    }
                }
                return q.length ? {
                    name: b.name,
                    message: f[0],
                    url: document.location.href,
                    stack: q
                } : null
            }

            function q(a, b, c, f) {
                var g = {
                    url: b,
                    line: c
                };
                if (g.url && g.line) {
                    a.incomplete = !1, g.func || (g.func = d(g.url, g.line)), g.context || (g.context = e(g.url, g.line));
                    var h = / '([^']+)' /.exec(f);
                    if (h && (g.column = l(h[1], g.url, g.line)), a.stack.length > 0 && a.stack[0].url === g.url) {
                        if (a.stack[0].line === g.line) return !1;
                        if (!a.stack[0].line && a.stack[0].func === g.func) return a.stack[0].line = g.line, a.stack[0].context = g.context, !1
                    }
                    return a.stack.unshift(g), a.partial = !0, !0
                }
                return a.incomplete = !0, !1
            }

            function r(a, b) {
                for (var c, e, f, g = /function\s+([_$a-zA-Z\xA0-\uFFFF][_$a-zA-Z0-9\xA0-\uFFFF]*)?\s*\(/i, h = [], i = {}, j = !1, k = r.caller; k && !j; k = k.caller)
                    if (k !== s && k !== z.report) {
                        if (e = {
                                url: null,
                                func: B,
                                line: null,
                                column: null
                            }, k.name ? e.func = k.name : (c = g.exec(k.toString())) && (e.func = c[1]), f = m(k)) {
                            e.url = f.url, e.line = f.line, e.func === B && (e.func = d(e.url, e.line));
                            var n = / '([^']+)' /.exec(a.message || a.description);
                            n && (e.column = l(n[1], f.url, f.line))
                        }
                        i["" + k] ? j = !0 : i["" + k] = !0, h.push(e)
                    }
                b && h.splice(0, b);
                var o = {
                    name: a.name,
                    message: a.message,
                    url: document.location.href,
                    stack: h
                };
                return q(o, a.sourceURL || a.fileName, a.line || a.lineNumber, a.message || a.description), o
            }

            function s(a, b) {
                var c = null;
                b = null == b ? 0 : +b;
                try {
                    if (c = o(a)) return c
                } catch (d) {
                    if (u) throw d
                }
                try {
                    if (c = n(a)) return c
                } catch (d) {
                    if (u) throw d
                }
                try {
                    if (c = p(a)) return c
                } catch (d) {
                    if (u) throw d
                }
                try {
                    if (c = r(a, b + 1)) return c
                } catch (d) {
                    if (u) throw d
                }
                return {}
            }

            function t(a) {
                a = (null == a ? 0 : +a) + 1;
                try {
                    throw new Error
                } catch (b) {
                    return s(b, a + 1)
                }
            }

            var u = !1,
                v = {};
            return s.augmentStackTraceWithInitialElement = q, s.guessFunctionName = d, s.gatherContext = e, s.ofCaller = t, s
        }();
        var C, D, E, F, G, H, I, J = a.Raven,
            K = !(!a.JSON || !a.JSON.stringify),
            L = {
                logger: "javascript",
                ignoreErrors: [],
                ignoreUrls: [],
                whitelistUrls: [],
                includePaths: [],
                collectWindowErrors: !0,
                tags: {},
                extra: {}
            },
            M = {
                VERSION: "1.1.14",
                noConflict: function() {
                    return a.Raven = J, M
                },
                config: function(a, b) {
                    if (!a) return M;
                    var c = e(a),
                        d = c.path.lastIndexOf("/"),
                        f = c.path.substr(1, d);
                    return b && k(b, function(a, b) {
                        L[a] = b
                    }), L.ignoreErrors.push("Script error."), L.ignoreErrors.push("Script error"), L.ignoreErrors = w(L.ignoreErrors), L.ignoreUrls = L.ignoreUrls.length ? w(L.ignoreUrls) : !1, L.whitelistUrls = L.whitelistUrls.length ? w(L.whitelistUrls) : !1, L.includePaths = w(L.includePaths), G = c.user, H = c.path.substr(d + 1), E = "//" + c.host + (c.port ? ":" + c.port : "") + "/" + f + "api/" + H + "/store/", c.protocol && (E = c.protocol + ":" + E), L.fetchContext && (z.remoteFetching = !0), L.linesOfContext && (z.linesOfContext = L.linesOfContext), z.collectWindowErrors = !!L.collectWindowErrors, l(), M
                },
                install: function() {
                    return v() && z.report.subscribe(m), M
                },
                context: function(a, c, d) {
                    return g(a) && (d = c || [], c = a, a = b), M.wrap(a, c).apply(this, d)
                },
                wrap: function(a, c) {
                    function d() {
                        for (var b = [], d = arguments.length, e = !a || a && a.deep !== !1; d--;) b[d] = e ? M.wrap(a, arguments[d]) : arguments[d];
                        try {
                            return c.apply(this, b)
                        } catch (f) {
                            throw M.captureException(f, a), f
                        }
                    }

                    if (f(c) && !g(a)) return a;
                    if (g(a) && (c = a, a = b), !g(c)) return c;
                    if (c.__raven__) return c;
                    for (var e in c) c.hasOwnProperty(e) && (d[e] = c[e]);
                    return d.__raven__ = !0, d.__inner__ = c, d
                },
                uninstall: function() {
                    return z.report.uninstall(), M
                },
                captureException: function(a, b) {
                    if (h(a)) return M.captureMessage(a, b);
                    C = a;
                    try {
                        z.report(a, b)
                    } catch (c) {
                        if (a !== c) throw c
                    }
                    return M
                },
                captureMessage: function(a, b) {
                    return t(q({
                        message: a
                    }, b)), M
                },
                setUser: function(a) {
                    return F = a, M
                },
                lastException: function() {
                    return C
                },
                lastEventId: function() {
                    return D
                }
            },
            N = "source protocol user pass host port path".split(" "),
            O = /^(?:(\w+):)?\/\/(\w+)(:\w+)?@([\w\.-]+)(?::(\d+))?(\/.*)/;
        d.prototype = new Error, d.prototype.constructor = d, y(), a.Raven = M, "function" == typeof define && define.amd && define("raven", [], function() {
            return M
        })
    }(this),
    function(a, b, c) {
        "use strict";
        if (c) {
            var d = c.event.add;
            c.event.add = function(a, e, f, g, h) {
                var i;
                return f && f.handler ? (i = f.handler, f.handler = b.wrap(f.handler)) : (i = f, f = b.wrap(f)), f.guid = i.guid ? i.guid : i.guid = c.guid++, d.call(this, a, e, f, g, h)
            };
            var e = c.fn.ready;
            c.fn.ready = function(a) {
                return e.call(this, b.wrap(a))
            };
            var f = c.ajax;
            c.ajax = function(a, d) {
                var e, g = ["complete", "error", "success"];
                for ("object" == typeof a && (d = a, a = void 0), d = d || {}; e = g.pop();) c.isFunction(d[e]) && (d[e] = b.wrap(d[e]));
                try {
                    return f.call(this, a, d)
                } catch (h) {
                    throw b.captureException(h), h
                }
            }
        }
    }(this, Raven, window.jQuery),
    function(a, b) {
        "use strict";
        var c = function(c) {
            var d = a[c];
            a[c] = function() {
                var a = [].slice.call(arguments),
                    c = a[0];
                return "function" == typeof c && (a[0] = b.wrap(c)), d.apply ? d.apply(this, a) : d(a[0], a[1])
            }
        };
        c("setTimeout"), c("setInterval")
    }(this, Raven);;
    (function(global, factory) {
        if (typeof module === "object" && typeof module.exports === "object") {
            module.exports = global.document ? factory(global, true) : function(w) {
                if (!w.document) {
                    throw new Error("jQuery requires a window with a document");
                }
                return factory(w);
            };
        } else {
            factory(global);
        }
    }(typeof window !== "undefined" ? window : this, function(window, noGlobal) {
        var deletedIds = [];
        var slice = deletedIds.slice;
        var concat = deletedIds.concat;
        var push = deletedIds.push;
        var indexOf = deletedIds.indexOf;
        var class2type = {};
        var toString = class2type.toString;
        var hasOwn = class2type.hasOwnProperty;
        var trim = "".trim;
        var support = {};
        var
            version = "1.11.0",
            jQuery = function(selector, context) {
                return new jQuery.fn.init(selector, context);
            },
            rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            rmsPrefix = /^-ms-/,
            rdashAlpha = /-([\da-z])/gi,
            fcamelCase = function(all, letter) {
                return letter.toUpperCase();
            };
        jQuery.fn = jQuery.prototype = {
            jquery: version,
            constructor: jQuery,
            selector: "",
            length: 0,
            toArray: function() {
                return slice.call(this);
            },
            get: function(num) {
                return num != null ? (num < 0 ? this[num + this.length] : this[num]) : slice.call(this);
            },
            pushStack: function(elems) {
                var ret = jQuery.merge(this.constructor(), elems);
                ret.prevObject = this;
                ret.context = this.context;
                return ret;
            },
            each: function(callback, args) {
                return jQuery.each(this, callback, args);
            },
            map: function(callback) {
                return this.pushStack(jQuery.map(this, function(elem, i) {
                    return callback.call(elem, i, elem);
                }));
            },
            slice: function() {
                return this.pushStack(slice.apply(this, arguments));
            },
            first: function() {
                return this.eq(0);
            },
            last: function() {
                return this.eq(-1);
            },
            eq: function(i) {
                var len = this.length,
                    j = +i + (i < 0 ? len : 0);
                return this.pushStack(j >= 0 && j < len ? [this[j]] : []);
            },
            end: function() {
                return this.prevObject || this.constructor(null);
            },
            push: push,
            sort: deletedIds.sort,
            splice: deletedIds.splice
        };
        jQuery.extend = jQuery.fn.extend = function() {
            var src, copyIsArray, copy, name, options, clone, target = arguments[0] || {},
                i = 1,
                length = arguments.length,
                deep = false;
            if (typeof target === "boolean") {
                deep = target;
                target = arguments[i] || {};
                i++;
            }
            if (typeof target !== "object" && !jQuery.isFunction(target)) {
                target = {};
            }
            if (i === length) {
                target = this;
                i--;
            }
            for (; i < length; i++) {
                if ((options = arguments[i]) != null) {
                    for (name in options) {
                        src = target[name];
                        copy = options[name];
                        if (target === copy) {
                            continue;
                        }
                        if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)))) {
                            if (copyIsArray) {
                                copyIsArray = false;
                                clone = src && jQuery.isArray(src) ? src : [];
                            } else {
                                clone = src && jQuery.isPlainObject(src) ? src : {};
                            }
                            target[name] = jQuery.extend(deep, clone, copy);
                        } else if (copy !== undefined) {
                            target[name] = copy;
                        }
                    }
                }
            }
            return target;
        };
        jQuery.extend({
            expando: "jQuery" + (version + Math.random()).replace(/\D/g, ""),
            isReady: true,
            error: function(msg) {
                throw new Error(msg);
            },
            noop: function() {},
            isFunction: function(obj) {
                return jQuery.type(obj) === "function";
            },
            isArray: Array.isArray || function(obj) {
                return jQuery.type(obj) === "array";
            },
            isWindow: function(obj) {
                return obj != null && obj == obj.window;
            },
            isNumeric: function(obj) {
                return obj - parseFloat(obj) >= 0;
            },
            isEmptyObject: function(obj) {
                var name;
                for (name in obj) {
                    return false;
                }
                return true;
            },
            isPlainObject: function(obj) {
                var key;
                if (!obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow(obj)) {
                    return false;
                }
                try {
                    if (obj.constructor && !hasOwn.call(obj, "constructor") && !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                        return false;
                    }
                } catch (e) {
                    return false;
                }
                if (support.ownLast) {
                    for (key in obj) {
                        return hasOwn.call(obj, key);
                    }
                }
                for (key in obj) {}
                return key === undefined || hasOwn.call(obj, key);
            },
            type: function(obj) {
                if (obj == null) {
                    return obj + "";
                }
                return typeof obj === "object" || typeof obj === "function" ? class2type[toString.call(obj)] || "object" : typeof obj;
            },
            globalEval: function(data) {
                if (data && jQuery.trim(data)) {
                    (window.execScript || function(data) {
                        window["eval"].call(window, data);
                    })(data);
                }
            },
            camelCase: function(string) {
                return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase);
            },
            nodeName: function(elem, name) {
                return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
            },
            each: function(obj, callback, args) {
                var value, i = 0,
                    length = obj.length,
                    isArray = isArraylike(obj);
                if (args) {
                    if (isArray) {
                        for (; i < length; i++) {
                            value = callback.apply(obj[i], args);
                            if (value === false) {
                                break;
                            }
                        }
                    } else {
                        for (i in obj) {
                            value = callback.apply(obj[i], args);
                            if (value === false) {
                                break;
                            }
                        }
                    }
                } else {
                    if (isArray) {
                        for (; i < length; i++) {
                            value = callback.call(obj[i], i, obj[i]);
                            if (value === false) {
                                break;
                            }
                        }
                    } else {
                        for (i in obj) {
                            value = callback.call(obj[i], i, obj[i]);
                            if (value === false) {
                                break;
                            }
                        }
                    }
                }
                return obj;
            },
            trim: trim && !trim.call("\uFEFF\xA0") ? function(text) {
                return text == null ? "" : trim.call(text);
            } : function(text) {
                return text == null ? "" : (text + "").replace(rtrim, "");
            },
            makeArray: function(arr, results) {
                var ret = results || [];
                if (arr != null) {
                    if (isArraylike(Object(arr))) {
                        jQuery.merge(ret, typeof arr === "string" ? [arr] : arr);
                    } else {
                        push.call(ret, arr);
                    }
                }
                return ret;
            },
            inArray: function(elem, arr, i) {
                var len;
                if (arr) {
                    if (indexOf) {
                        return indexOf.call(arr, elem, i);
                    }
                    len = arr.length;
                    i = i ? i < 0 ? Math.max(0, len + i) : i : 0;
                    for (; i < len; i++) {
                        if (i in arr && arr[i] === elem) {
                            return i;
                        }
                    }
                }
                return -1;
            },
            merge: function(first, second) {
                var len = +second.length,
                    j = 0,
                    i = first.length;
                while (j < len) {
                    first[i++] = second[j++];
                }
                if (len !== len) {
                    while (second[j] !== undefined) {
                        first[i++] = second[j++];
                    }
                }
                first.length = i;
                return first;
            },
            grep: function(elems, callback, invert) {
                var callbackInverse, matches = [],
                    i = 0,
                    length = elems.length,
                    callbackExpect = !invert;
                for (; i < length; i++) {
                    callbackInverse = !callback(elems[i], i);
                    if (callbackInverse !== callbackExpect) {
                        matches.push(elems[i]);
                    }
                }
                return matches;
            },
            map: function(elems, callback, arg) {
                var value, i = 0,
                    length = elems.length,
                    isArray = isArraylike(elems),
                    ret = [];
                if (isArray) {
                    for (; i < length; i++) {
                        value = callback(elems[i], i, arg);
                        if (value != null) {
                            ret.push(value);
                        }
                    }
                } else {
                    for (i in elems) {
                        value = callback(elems[i], i, arg);
                        if (value != null) {
                            ret.push(value);
                        }
                    }
                }
                return concat.apply([], ret);
            },
            guid: 1,
            proxy: function(fn, context) {
                var args, proxy, tmp;
                if (typeof context === "string") {
                    tmp = fn[context];
                    context = fn;
                    fn = tmp;
                }
                if (!jQuery.isFunction(fn)) {
                    return undefined;
                }
                args = slice.call(arguments, 2);
                proxy = function() {
                    return fn.apply(context || this, args.concat(slice.call(arguments)));
                };
                proxy.guid = fn.guid = fn.guid || jQuery.guid++;
                return proxy;
            },
            now: function() {
                return +(new Date());
            },
            support: support
        });
        jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
            class2type["[object " + name + "]"] = name.toLowerCase();
        });

        function isArraylike(obj) {
            var length = obj.length,
                type = jQuery.type(obj);
            if (type === "function" || jQuery.isWindow(obj)) {
                return false;
            }
            if (obj.nodeType === 1 && length) {
                return true;
            }
            return type === "array" || length === 0 || typeof length === "number" && length > 0 && (length - 1) in obj;
        }

        var Sizzle = (function(window) {
            var i, support, Expr, getText, isXML, compile, outermostContext, sortInput, hasDuplicate, setDocument, document, docElem, documentIsHTML, rbuggyQSA, rbuggyMatches, matches, contains, expando = "sizzle" + -(new Date()),
                preferredDoc = window.document,
                dirruns = 0,
                done = 0,
                classCache = createCache(),
                tokenCache = createCache(),
                compilerCache = createCache(),
                sortOrder = function(a, b) {
                    if (a === b) {
                        hasDuplicate = true;
                    }
                    return 0;
                },
                strundefined = typeof undefined,
                MAX_NEGATIVE = 1 << 31,
                hasOwn = ({}).hasOwnProperty,
                arr = [],
                pop = arr.pop,
                push_native = arr.push,
                push = arr.push,
                slice = arr.slice,
                indexOf = arr.indexOf || function(elem) {
                    var i = 0,
                        len = this.length;
                    for (; i < len; i++) {
                        if (this[i] === elem) {
                            return i;
                        }
                    }
                    return -1;
                },
                booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                whitespace = "[\\x20\\t\\r\\n\\f]",
                characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                identifier = characterEncoding.replace("w", "w#"),
                attributes = "\\[" + whitespace + "*(" + characterEncoding + ")" + whitespace + "*(?:([*^$|!~]?=)" + whitespace + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + identifier + ")|)|)" + whitespace + "*\\]",
                pseudos = ":(" + characterEncoding + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + attributes.replace(3, 8) + ")*)|.*)\\)|)",
                rtrim = new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g"),
                rcomma = new RegExp("^" + whitespace + "*," + whitespace + "*"),
                rcombinators = new RegExp("^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*"),
                rattributeQuotes = new RegExp("=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g"),
                rpseudo = new RegExp(pseudos),
                ridentifier = new RegExp("^" + identifier + "$"),
                matchExpr = {
                    "ID": new RegExp("^#(" + characterEncoding + ")"),
                    "CLASS": new RegExp("^\\.(" + characterEncoding + ")"),
                    "TAG": new RegExp("^(" + characterEncoding.replace("w", "w*") + ")"),
                    "ATTR": new RegExp("^" + attributes),
                    "PSEUDO": new RegExp("^" + pseudos),
                    "CHILD": new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i"),
                    "bool": new RegExp("^(?:" + booleans + ")$", "i"),
                    "needsContext": new RegExp("^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
                        whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i")
                },
                rinputs = /^(?:input|select|textarea|button)$/i,
                rheader = /^h\d$/i,
                rnative = /^[^{]+\{\s*\[native \w/,
                rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                rsibling = /[+~]/,
                rescape = /'|\\/g,
                runescape = new RegExp("\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig"),
                funescape = function(_, escaped, escapedWhitespace) {
                    var high = "0x" + escaped - 0x10000;
                    return high !== high || escapedWhitespace ? escaped : high < 0 ? String.fromCharCode(high + 0x10000) : String.fromCharCode(high >> 10 | 0xD800, high & 0x3FF | 0xDC00);
                };
            try {
                push.apply((arr = slice.call(preferredDoc.childNodes)), preferredDoc.childNodes);
                arr[preferredDoc.childNodes.length].nodeType;
            } catch (e) {
                push = {
                    apply: arr.length ? function(target, els) {
                        push_native.apply(target, slice.call(els));
                    } : function(target, els) {
                        var j = target.length,
                            i = 0;
                        while ((target[j++] = els[i++])) {}
                        target.length = j - 1;
                    }
                };
            }

            function Sizzle(selector, context, results, seed) {
                var match, elem, m, nodeType, i, groups, old, nid, newContext, newSelector;
                if ((context ? context.ownerDocument || context : preferredDoc) !== document) {
                    setDocument(context);
                }
                context = context || document;
                results = results || [];
                if (!selector || typeof selector !== "string") {
                    return results;
                }
                if ((nodeType = context.nodeType) !== 1 && nodeType !== 9) {
                    return [];
                }
                if (documentIsHTML && !seed) {
                    if ((match = rquickExpr.exec(selector))) {
                        if ((m = match[1])) {
                            if (nodeType === 9) {
                                elem = context.getElementById(m);
                                if (elem && elem.parentNode) {
                                    if (elem.id === m) {
                                        results.push(elem);
                                        return results;
                                    }
                                } else {
                                    return results;
                                }
                            } else {
                                if (context.ownerDocument && (elem = context.ownerDocument.getElementById(m)) && contains(context, elem) && elem.id === m) {
                                    results.push(elem);
                                    return results;
                                }
                            }
                        } else if (match[2]) {
                            push.apply(results, context.getElementsByTagName(selector));
                            return results;
                        } else if ((m = match[3]) && support.getElementsByClassName && context.getElementsByClassName) {
                            push.apply(results, context.getElementsByClassName(m));
                            return results;
                        }
                    }
                    if (support.qsa && (!rbuggyQSA || !rbuggyQSA.test(selector))) {
                        nid = old = expando;
                        newContext = context;
                        newSelector = nodeType === 9 && selector;
                        if (nodeType === 1 && context.nodeName.toLowerCase() !== "object") {
                            groups = tokenize(selector);
                            if ((old = context.getAttribute("id"))) {
                                nid = old.replace(rescape, "\\$&");
                            } else {
                                context.setAttribute("id", nid);
                            }
                            nid = "[id='" + nid + "'] ";
                            i = groups.length;
                            while (i--) {
                                groups[i] = nid + toSelector(groups[i]);
                            }
                            newContext = rsibling.test(selector) && testContext(context.parentNode) || context;
                            newSelector = groups.join(",");
                        }
                        if (newSelector) {
                            try {
                                push.apply(results, newContext.querySelectorAll(newSelector));
                                return results;
                            } catch (qsaError) {} finally {
                                if (!old) {
                                    context.removeAttribute("id");
                                }
                            }
                        }
                    }
                }
                return select(selector.replace(rtrim, "$1"), context, results, seed);
            }

            function createCache() {
                var keys = [];

                function cache(key, value) {
                    if (keys.push(key + " ") > Expr.cacheLength) {
                        delete cache[keys.shift()];
                    }
                    return (cache[key + " "] = value);
                }

                return cache;
            }

            function markFunction(fn) {
                fn[expando] = true;
                return fn;
            }

            function assert(fn) {
                var div = document.createElement("div");
                try {
                    return !!fn(div);
                } catch (e) {
                    return false;
                } finally {
                    if (div.parentNode) {
                        div.parentNode.removeChild(div);
                    }
                    div = null;
                }
            }

            function addHandle(attrs, handler) {
                var arr = attrs.split("|"),
                    i = attrs.length;
                while (i--) {
                    Expr.attrHandle[arr[i]] = handler;
                }
            }

            function siblingCheck(a, b) {
                var cur = b && a,
                    diff = cur && a.nodeType === 1 && b.nodeType === 1 && (~b.sourceIndex || MAX_NEGATIVE) -
                    (~a.sourceIndex || MAX_NEGATIVE);
                if (diff) {
                    return diff;
                }
                if (cur) {
                    while ((cur = cur.nextSibling)) {
                        if (cur === b) {
                            return -1;
                        }
                    }
                }
                return a ? 1 : -1;
            }

            function createInputPseudo(type) {
                return function(elem) {
                    var name = elem.nodeName.toLowerCase();
                    return name === "input" && elem.type === type;
                };
            }

            function createButtonPseudo(type) {
                return function(elem) {
                    var name = elem.nodeName.toLowerCase();
                    return (name === "input" || name === "button") && elem.type === type;
                };
            }

            function createPositionalPseudo(fn) {
                return markFunction(function(argument) {
                    argument = +argument;
                    return markFunction(function(seed, matches) {
                        var j, matchIndexes = fn([], seed.length, argument),
                            i = matchIndexes.length;
                        while (i--) {
                            if (seed[(j = matchIndexes[i])]) {
                                seed[j] = !(matches[j] = seed[j]);
                            }
                        }
                    });
                });
            }

            function testContext(context) {
                return context && typeof context.getElementsByTagName !== strundefined && context;
            }

            support = Sizzle.support = {};
            isXML = Sizzle.isXML = function(elem) {
                var documentElement = elem && (elem.ownerDocument || elem).documentElement;
                return documentElement ? documentElement.nodeName !== "HTML" : false;
            };
            setDocument = Sizzle.setDocument = function(node) {
                var hasCompare, doc = node ? node.ownerDocument || node : preferredDoc,
                    parent = doc.defaultView;
                if (doc === document || doc.nodeType !== 9 || !doc.documentElement) {
                    return document;
                }
                document = doc;
                docElem = doc.documentElement;
                documentIsHTML = !isXML(doc);
                if (parent && parent !== parent.top) {
                    if (parent.addEventListener) {
                        parent.addEventListener("unload", function() {
                            setDocument();
                        }, false);
                    } else if (parent.attachEvent) {
                        parent.attachEvent("onunload", function() {
                            setDocument();
                        });
                    }
                }
                support.attributes = assert(function(div) {
                    div.className = "i";
                    return !div.getAttribute("className");
                });
                support.getElementsByTagName = assert(function(div) {
                    div.appendChild(doc.createComment(""));
                    return !div.getElementsByTagName("*").length;
                });
                support.getElementsByClassName = rnative.test(doc.getElementsByClassName) && assert(function(div) {
                    div.innerHTML = "<div class='a'></div><div class='a i'></div>";
                    div.firstChild.className = "i";
                    return div.getElementsByClassName("i").length === 2;
                });
                support.getById = assert(function(div) {
                    docElem.appendChild(div).id = expando;
                    return !doc.getElementsByName || !doc.getElementsByName(expando).length;
                });
                if (support.getById) {
                    Expr.find["ID"] = function(id, context) {
                        if (typeof context.getElementById !== strundefined && documentIsHTML) {
                            var m = context.getElementById(id);
                            return m && m.parentNode ? [m] : [];
                        }
                    };
                    Expr.filter["ID"] = function(id) {
                        var attrId = id.replace(runescape, funescape);
                        return function(elem) {
                            return elem.getAttribute("id") === attrId;
                        };
                    };
                } else {
                    delete Expr.find["ID"];
                    Expr.filter["ID"] = function(id) {
                        var attrId = id.replace(runescape, funescape);
                        return function(elem) {
                            var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode("id");
                            return node && node.value === attrId;
                        };
                    };
                }
                Expr.find["TAG"] = support.getElementsByTagName ? function(tag, context) {
                    if (typeof context.getElementsByTagName !== strundefined) {
                        return context.getElementsByTagName(tag);
                    }
                } : function(tag, context) {
                    var elem, tmp = [],
                        i = 0,
                        results = context.getElementsByTagName(tag);
                    if (tag === "*") {
                        while ((elem = results[i++])) {
                            if (elem.nodeType === 1) {
                                tmp.push(elem);
                            }
                        }
                        return tmp;
                    }
                    return results;
                };
                Expr.find["CLASS"] = support.getElementsByClassName && function(className, context) {
                    if (typeof context.getElementsByClassName !== strundefined && documentIsHTML) {
                        return context.getElementsByClassName(className);
                    }
                };
                rbuggyMatches = [];
                rbuggyQSA = [];
                if ((support.qsa = rnative.test(doc.querySelectorAll))) {
                    assert(function(div) {
                        div.innerHTML = "<select t=''><option selected=''></option></select>";
                        if (div.querySelectorAll("[t^='']").length) {
                            rbuggyQSA.push("[*^$]=" + whitespace + "*(?:''|\"\")");
                        }
                        if (!div.querySelectorAll("[selected]").length) {
                            rbuggyQSA.push("\\[" + whitespace + "*(?:value|" + booleans + ")");
                        }
                        if (!div.querySelectorAll(":checked").length) {
                            rbuggyQSA.push(":checked");
                        }
                    });
                    assert(function(div) {
                        var input = doc.createElement("input");
                        input.setAttribute("type", "hidden");
                        div.appendChild(input).setAttribute("name", "D");
                        if (div.querySelectorAll("[name=d]").length) {
                            rbuggyQSA.push("name" + whitespace + "*[*^$|!~]?=");
                        }
                        if (!div.querySelectorAll(":enabled").length) {
                            rbuggyQSA.push(":enabled", ":disabled");
                        }
                        div.querySelectorAll("*,:x");
                        rbuggyQSA.push(",.*:");
                    });
                }
                if ((support.matchesSelector = rnative.test((matches = docElem.webkitMatchesSelector || docElem.mozMatchesSelector || docElem.oMatchesSelector || docElem.msMatchesSelector)))) {
                    assert(function(div) {
                        support.disconnectedMatch = matches.call(div, "div");
                        matches.call(div, "[s!='']:x");
                        rbuggyMatches.push("!=", pseudos);
                    });
                }
                rbuggyQSA = rbuggyQSA.length && new RegExp(rbuggyQSA.join("|"));
                rbuggyMatches = rbuggyMatches.length && new RegExp(rbuggyMatches.join("|"));
                hasCompare = rnative.test(docElem.compareDocumentPosition);
                contains = hasCompare || rnative.test(docElem.contains) ? function(a, b) {
                    var adown = a.nodeType === 9 ? a.documentElement : a,
                        bup = b && b.parentNode;
                    return a === bup || !!(bup && bup.nodeType === 1 && (adown.contains ? adown.contains(bup) : a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16));
                } : function(a, b) {
                    if (b) {
                        while ((b = b.parentNode)) {
                            if (b === a) {
                                return true;
                            }
                        }
                    }
                    return false;
                };
                sortOrder = hasCompare ? function(a, b) {
                    if (a === b) {
                        hasDuplicate = true;
                        return 0;
                    }
                    var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
                    if (compare) {
                        return compare;
                    }
                    compare = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1;
                    if (compare & 1 || (!support.sortDetached && b.compareDocumentPosition(a) === compare)) {
                        if (a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a)) {
                            return -1;
                        }
                        if (b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b)) {
                            return 1;
                        }
                        return sortInput ? (indexOf.call(sortInput, a) - indexOf.call(sortInput, b)) : 0;
                    }
                    return compare & 4 ? -1 : 1;
                } : function(a, b) {
                    if (a === b) {
                        hasDuplicate = true;
                        return 0;
                    }
                    var cur, i = 0,
                        aup = a.parentNode,
                        bup = b.parentNode,
                        ap = [a],
                        bp = [b];
                    if (!aup || !bup) {
                        return a === doc ? -1 : b === doc ? 1 : aup ? -1 : bup ? 1 : sortInput ? (indexOf.call(sortInput, a) - indexOf.call(sortInput, b)) : 0;
                    } else if (aup === bup) {
                        return siblingCheck(a, b);
                    }
                    cur = a;
                    while ((cur = cur.parentNode)) {
                        ap.unshift(cur);
                    }
                    cur = b;
                    while ((cur = cur.parentNode)) {
                        bp.unshift(cur);
                    }
                    while (ap[i] === bp[i]) {
                        i++;
                    }
                    return i ? siblingCheck(ap[i], bp[i]) : ap[i] === preferredDoc ? -1 : bp[i] === preferredDoc ? 1 : 0;
                };
                return doc;
            };
            Sizzle.matches = function(expr, elements) {
                return Sizzle(expr, null, null, elements);
            };
            Sizzle.matchesSelector = function(elem, expr) {
                if ((elem.ownerDocument || elem) !== document) {
                    setDocument(elem);
                }
                expr = expr.replace(rattributeQuotes, "='$1']");
                if (support.matchesSelector && documentIsHTML && (!rbuggyMatches || !rbuggyMatches.test(expr)) && (!rbuggyQSA || !rbuggyQSA.test(expr))) {
                    try {
                        var ret = matches.call(elem, expr);
                        if (ret || support.disconnectedMatch || elem.document && elem.document.nodeType !== 11) {
                            return ret;
                        }
                    } catch (e) {}
                }
                return Sizzle(expr, document, null, [elem]).length > 0;
            };
            Sizzle.contains = function(context, elem) {
                if ((context.ownerDocument || context) !== document) {
                    setDocument(context);
                }
                return contains(context, elem);
            };
            Sizzle.attr = function(elem, name) {
                if ((elem.ownerDocument || elem) !== document) {
                    setDocument(elem);
                }
                var fn = Expr.attrHandle[name.toLowerCase()],
                    val = fn && hasOwn.call(Expr.attrHandle, name.toLowerCase()) ? fn(elem, name, !documentIsHTML) : undefined;
                return val !== undefined ? val : support.attributes || !documentIsHTML ? elem.getAttribute(name) : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
            };
            Sizzle.error = function(msg) {
                throw new Error("Syntax error, unrecognized expression: " + msg);
            };
            Sizzle.uniqueSort = function(results) {
                var elem, duplicates = [],
                    j = 0,
                    i = 0;
                hasDuplicate = !support.detectDuplicates;
                sortInput = !support.sortStable && results.slice(0);
                results.sort(sortOrder);
                if (hasDuplicate) {
                    while ((elem = results[i++])) {
                        if (elem === results[i]) {
                            j = duplicates.push(i);
                        }
                    }
                    while (j--) {
                        results.splice(duplicates[j], 1);
                    }
                }
                sortInput = null;
                return results;
            };
            getText = Sizzle.getText = function(elem) {
                var node, ret = "",
                    i = 0,
                    nodeType = elem.nodeType;
                if (!nodeType) {
                    while ((node = elem[i++])) {
                        ret += getText(node);
                    }
                } else if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
                    if (typeof elem.textContent === "string") {
                        return elem.textContent;
                    } else {
                        for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                            ret += getText(elem);
                        }
                    }
                } else if (nodeType === 3 || nodeType === 4) {
                    return elem.nodeValue;
                }
                return ret;
            };
            Expr = Sizzle.selectors = {
                cacheLength: 50,
                createPseudo: markFunction,
                match: matchExpr,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: true
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: true
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    "ATTR": function(match) {
                        match[1] = match[1].replace(runescape, funescape);
                        match[3] = (match[4] || match[5] || "").replace(runescape, funescape);
                        if (match[2] === "~=") {
                            match[3] = " " + match[3] + " ";
                        }
                        return match.slice(0, 4);
                    },
                    "CHILD": function(match) {
                        match[1] = match[1].toLowerCase();
                        if (match[1].slice(0, 3) === "nth") {
                            if (!match[3]) {
                                Sizzle.error(match[0]);
                            }
                            match[4] = +(match[4] ? match[5] + (match[6] || 1) : 2 * (match[3] === "even" || match[3] === "odd"));
                            match[5] = +((match[7] + match[8]) || match[3] === "odd");
                        } else if (match[3]) {
                            Sizzle.error(match[0]);
                        }
                        return match;
                    },
                    "PSEUDO": function(match) {
                        var excess, unquoted = !match[5] && match[2];
                        if (matchExpr["CHILD"].test(match[0])) {
                            return null;
                        }
                        if (match[3] && match[4] !== undefined) {
                            match[2] = match[4];
                        } else if (unquoted && rpseudo.test(unquoted) && (excess = tokenize(unquoted, true)) && (excess = unquoted.indexOf(")", unquoted.length - excess) - unquoted.length)) {
                            match[0] = match[0].slice(0, excess);
                            match[2] = unquoted.slice(0, excess);
                        }
                        return match.slice(0, 3);
                    }
                },
                filter: {
                    "TAG": function(nodeNameSelector) {
                        var nodeName = nodeNameSelector.replace(runescape, funescape).toLowerCase();
                        return nodeNameSelector === "*" ? function() {
                            return true;
                        } : function(elem) {
                            return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
                        };
                    },
                    "CLASS": function(className) {
                        var pattern = classCache[className + " "];
                        return pattern || (pattern = new RegExp("(^|" + whitespace + ")" + className + "(" + whitespace + "|$)")) && classCache(className, function(elem) {
                            return pattern.test(typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== strundefined && elem.getAttribute("class") || "");
                        });
                    },
                    "ATTR": function(name, operator, check) {
                        return function(elem) {
                            var result = Sizzle.attr(elem, name);
                            if (result == null) {
                                return operator === "!=";
                            }
                            if (!operator) {
                                return true;
                            }
                            result += "";
                            return operator === "=" ? result === check : operator === "!=" ? result !== check : operator === "^=" ? check && result.indexOf(check) === 0 : operator === "*=" ? check && result.indexOf(check) > -1 : operator === "$=" ? check && result.slice(-check.length) === check : operator === "~=" ? (" " + result + " ").indexOf(check) > -1 : operator === "|=" ? result === check || result.slice(0, check.length + 1) === check + "-" : false;
                        };
                    },
                    "CHILD": function(type, what, argument, first, last) {
                        var simple = type.slice(0, 3) !== "nth",
                            forward = type.slice(-4) !== "last",
                            ofType = what === "of-type";
                        return first === 1 && last === 0 ? function(elem) {
                            return !!elem.parentNode;
                        } : function(elem, context, xml) {
                            var cache, outerCache, node, diff, nodeIndex, start, dir = simple !== forward ? "nextSibling" : "previousSibling",
                                parent = elem.parentNode,
                                name = ofType && elem.nodeName.toLowerCase(),
                                useCache = !xml && !ofType;
                            if (parent) {
                                if (simple) {
                                    while (dir) {
                                        node = elem;
                                        while ((node = node[dir])) {
                                            if (ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) {
                                                return false;
                                            }
                                        }
                                        start = dir = type === "only" && !start && "nextSibling";
                                    }
                                    return true;
                                }
                                start = [forward ? parent.firstChild : parent.lastChild];
                                if (forward && useCache) {
                                    outerCache = parent[expando] || (parent[expando] = {});
                                    cache = outerCache[type] || [];
                                    nodeIndex = cache[0] === dirruns && cache[1];
                                    diff = cache[0] === dirruns && cache[2];
                                    node = nodeIndex && parent.childNodes[nodeIndex];
                                    while ((node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop())) {
                                        if (node.nodeType === 1 && ++diff && node === elem) {
                                            outerCache[type] = [dirruns, nodeIndex, diff];
                                            break;
                                        }
                                    }
                                } else if (useCache && (cache = (elem[expando] || (elem[expando] = {}))[type]) && cache[0] === dirruns) {
                                    diff = cache[1];
                                } else {
                                    while ((node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop())) {
                                        if ((ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) && ++diff) {
                                            if (useCache) {
                                                (node[expando] || (node[expando] = {}))[type] = [dirruns, diff];
                                            }
                                            if (node === elem) {
                                                break;
                                            }
                                        }
                                    }
                                }
                                diff -= last;
                                return diff === first || (diff % first === 0 && diff / first >= 0);
                            }
                        };
                    },
                    "PSEUDO": function(pseudo, argument) {
                        var args, fn = Expr.pseudos[pseudo] || Expr.setFilters[pseudo.toLowerCase()] || Sizzle.error("unsupported pseudo: " + pseudo);
                        if (fn[expando]) {
                            return fn(argument);
                        }
                        if (fn.length > 1) {
                            args = [pseudo, pseudo, "", argument];
                            return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase()) ? markFunction(function(seed, matches) {
                                var idx, matched = fn(seed, argument),
                                    i = matched.length;
                                while (i--) {
                                    idx = indexOf.call(seed, matched[i]);
                                    seed[idx] = !(matches[idx] = matched[i]);
                                }
                            }) : function(elem) {
                                return fn(elem, 0, args);
                            };
                        }
                        return fn;
                    }
                },
                pseudos: {
                    "not": markFunction(function(selector) {
                        var input = [],
                            results = [],
                            matcher = compile(selector.replace(rtrim, "$1"));
                        return matcher[expando] ? markFunction(function(seed, matches, context, xml) {
                            var elem, unmatched = matcher(seed, null, xml, []),
                                i = seed.length;
                            while (i--) {
                                if ((elem = unmatched[i])) {
                                    seed[i] = !(matches[i] = elem);
                                }
                            }
                        }) : function(elem, context, xml) {
                            input[0] = elem;
                            matcher(input, null, xml, results);
                            return !results.pop();
                        };
                    }),
                    "has": markFunction(function(selector) {
                        return function(elem) {
                            return Sizzle(selector, elem).length > 0;
                        };
                    }),
                    "contains": markFunction(function(text) {
                        return function(elem) {
                            return (elem.textContent || elem.innerText || getText(elem)).indexOf(text) > -1;
                        };
                    }),
                    "lang": markFunction(function(lang) {
                        if (!ridentifier.test(lang || "")) {
                            Sizzle.error("unsupported lang: " + lang);
                        }
                        lang = lang.replace(runescape, funescape).toLowerCase();
                        return function(elem) {
                            var elemLang;
                            do {
                                if ((elemLang = documentIsHTML ? elem.lang : elem.getAttribute("xml:lang") || elem.getAttribute("lang"))) {
                                    elemLang = elemLang.toLowerCase();
                                    return elemLang === lang || elemLang.indexOf(lang + "-") === 0;
                                }
                            } while ((elem = elem.parentNode) && elem.nodeType === 1);
                            return false;
                        };
                    }),
                    "target": function(elem) {
                        var hash = window.location && window.location.hash;
                        return hash && hash.slice(1) === elem.id;
                    },
                    "root": function(elem) {
                        return elem === docElem;
                    },
                    "focus": function(elem) {
                        return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
                    },
                    "enabled": function(elem) {
                        return elem.disabled === false;
                    },
                    "disabled": function(elem) {
                        return elem.disabled === true;
                    },
                    "checked": function(elem) {
                        var nodeName = elem.nodeName.toLowerCase();
                        return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
                    },
                    "selected": function(elem) {
                        if (elem.parentNode) {
                            elem.parentNode.selectedIndex;
                        }
                        return elem.selected === true;
                    },
                    "empty": function(elem) {
                        for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                            if (elem.nodeType < 6) {
                                return false;
                            }
                        }
                        return true;
                    },
                    "parent": function(elem) {
                        return !Expr.pseudos["empty"](elem);
                    },
                    "header": function(elem) {
                        return rheader.test(elem.nodeName);
                    },
                    "input": function(elem) {
                        return rinputs.test(elem.nodeName);
                    },
                    "button": function(elem) {
                        var name = elem.nodeName.toLowerCase();
                        return name === "input" && elem.type === "button" || name === "button";
                    },
                    "text": function(elem) {
                        var attr;
                        return elem.nodeName.toLowerCase() === "input" && elem.type === "text" && ((attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text");
                    },
                    "first": createPositionalPseudo(function() {
                        return [0];
                    }),
                    "last": createPositionalPseudo(function(matchIndexes, length) {
                        return [length - 1];
                    }),
                    "eq": createPositionalPseudo(function(matchIndexes, length, argument) {
                        return [argument < 0 ? argument + length : argument];
                    }),
                    "even": createPositionalPseudo(function(matchIndexes, length) {
                        var i = 0;
                        for (; i < length; i += 2) {
                            matchIndexes.push(i);
                        }
                        return matchIndexes;
                    }),
                    "odd": createPositionalPseudo(function(matchIndexes, length) {
                        var i = 1;
                        for (; i < length; i += 2) {
                            matchIndexes.push(i);
                        }
                        return matchIndexes;
                    }),
                    "lt": createPositionalPseudo(function(matchIndexes, length, argument) {
                        var i = argument < 0 ? argument + length : argument;
                        for (; --i >= 0;) {
                            matchIndexes.push(i);
                        }
                        return matchIndexes;
                    }),
                    "gt": createPositionalPseudo(function(matchIndexes, length, argument) {
                        var i = argument < 0 ? argument + length : argument;
                        for (; ++i < length;) {
                            matchIndexes.push(i);
                        }
                        return matchIndexes;
                    })
                }
            };
            Expr.pseudos["nth"] = Expr.pseudos["eq"];
            for (i in {
                    radio: true,
                    checkbox: true,
                    file: true,
                    password: true,
                    image: true
                }) {
                Expr.pseudos[i] = createInputPseudo(i);
            }
            for (i in {
                    submit: true,
                    reset: true
                }) {
                Expr.pseudos[i] = createButtonPseudo(i);
            }

            function setFilters() {}

            setFilters.prototype = Expr.filters = Expr.pseudos;
            Expr.setFilters = new setFilters();

            function tokenize(selector, parseOnly) {
                var matched, match, tokens, type, soFar, groups, preFilters, cached = tokenCache[selector + " "];
                if (cached) {
                    return parseOnly ? 0 : cached.slice(0);
                }
                soFar = selector;
                groups = [];
                preFilters = Expr.preFilter;
                while (soFar) {
                    if (!matched || (match = rcomma.exec(soFar))) {
                        if (match) {
                            soFar = soFar.slice(match[0].length) || soFar;
                        }
                        groups.push((tokens = []));
                    }
                    matched = false;
                    if ((match = rcombinators.exec(soFar))) {
                        matched = match.shift();
                        tokens.push({
                            value: matched,
                            type: match[0].replace(rtrim, " ")
                        });
                        soFar = soFar.slice(matched.length);
                    }
                    for (type in Expr.filter) {
                        if ((match = matchExpr[type].exec(soFar)) && (!preFilters[type] || (match = preFilters[type](match)))) {
                            matched = match.shift();
                            tokens.push({
                                value: matched,
                                type: type,
                                matches: match
                            });
                            soFar = soFar.slice(matched.length);
                        }
                    }
                    if (!matched) {
                        break;
                    }
                }
                return parseOnly ? soFar.length : soFar ? Sizzle.error(selector) : tokenCache(selector, groups).slice(0);
            }

            function toSelector(tokens) {
                var i = 0,
                    len = tokens.length,
                    selector = "";
                for (; i < len; i++) {
                    selector += tokens[i].value;
                }
                return selector;
            }

            function addCombinator(matcher, combinator, base) {
                var dir = combinator.dir,
                    checkNonElements = base && dir === "parentNode",
                    doneName = done++;
                return combinator.first ? function(elem, context, xml) {
                    while ((elem = elem[dir])) {
                        if (elem.nodeType === 1 || checkNonElements) {
                            return matcher(elem, context, xml);
                        }
                    }
                } : function(elem, context, xml) {
                    var oldCache, outerCache, newCache = [dirruns, doneName];
                    if (xml) {
                        while ((elem = elem[dir])) {
                            if (elem.nodeType === 1 || checkNonElements) {
                                if (matcher(elem, context, xml)) {
                                    return true;
                                }
                            }
                        }
                    } else {
                        while ((elem = elem[dir])) {
                            if (elem.nodeType === 1 || checkNonElements) {
                                outerCache = elem[expando] || (elem[expando] = {});
                                if ((oldCache = outerCache[dir]) && oldCache[0] === dirruns && oldCache[1] === doneName) {
                                    return (newCache[2] = oldCache[2]);
                                } else {
                                    outerCache[dir] = newCache;
                                    if ((newCache[2] = matcher(elem, context, xml))) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                };
            }

            function elementMatcher(matchers) {
                return matchers.length > 1 ? function(elem, context, xml) {
                    var i = matchers.length;
                    while (i--) {
                        if (!matchers[i](elem, context, xml)) {
                            return false;
                        }
                    }
                    return true;
                } : matchers[0];
            }

            function condense(unmatched, map, filter, context, xml) {
                var elem, newUnmatched = [],
                    i = 0,
                    len = unmatched.length,
                    mapped = map != null;
                for (; i < len; i++) {
                    if ((elem = unmatched[i])) {
                        if (!filter || filter(elem, context, xml)) {
                            newUnmatched.push(elem);
                            if (mapped) {
                                map.push(i);
                            }
                        }
                    }
                }
                return newUnmatched;
            }

            function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector) {
                if (postFilter && !postFilter[expando]) {
                    postFilter = setMatcher(postFilter);
                }
                if (postFinder && !postFinder[expando]) {
                    postFinder = setMatcher(postFinder, postSelector);
                }
                return markFunction(function(seed, results, context, xml) {
                    var temp, i, elem, preMap = [],
                        postMap = [],
                        preexisting = results.length,
                        elems = seed || multipleContexts(selector || "*", context.nodeType ? [context] : context, []),
                        matcherIn = preFilter && (seed || !selector) ? condense(elems, preMap, preFilter, context, xml) : elems,
                        matcherOut = matcher ? postFinder || (seed ? preFilter : preexisting || postFilter) ? [] : results : matcherIn;
                    if (matcher) {
                        matcher(matcherIn, matcherOut, context, xml);
                    }
                    if (postFilter) {
                        temp = condense(matcherOut, postMap);
                        postFilter(temp, [], context, xml);
                        i = temp.length;
                        while (i--) {
                            if ((elem = temp[i])) {
                                matcherOut[postMap[i]] = !(matcherIn[postMap[i]] = elem);
                            }
                        }
                    }
                    if (seed) {
                        if (postFinder || preFilter) {
                            if (postFinder) {
                                temp = [];
                                i = matcherOut.length;
                                while (i--) {
                                    if ((elem = matcherOut[i])) {
                                        temp.push((matcherIn[i] = elem));
                                    }
                                }
                                postFinder(null, (matcherOut = []), temp, xml);
                            }
                            i = matcherOut.length;
                            while (i--) {
                                if ((elem = matcherOut[i]) && (temp = postFinder ? indexOf.call(seed, elem) : preMap[i]) > -1) {
                                    seed[temp] = !(results[temp] = elem);
                                }
                            }
                        }
                    } else {
                        matcherOut = condense(matcherOut === results ? matcherOut.splice(preexisting, matcherOut.length) : matcherOut);
                        if (postFinder) {
                            postFinder(null, results, matcherOut, xml);
                        } else {
                            push.apply(results, matcherOut);
                        }
                    }
                });
            }

            function matcherFromTokens(tokens) {
                var checkContext, matcher, j, len = tokens.length,
                    leadingRelative = Expr.relative[tokens[0].type],
                    implicitRelative = leadingRelative || Expr.relative[" "],
                    i = leadingRelative ? 1 : 0,
                    matchContext = addCombinator(function(elem) {
                        return elem === checkContext;
                    }, implicitRelative, true),
                    matchAnyContext = addCombinator(function(elem) {
                        return indexOf.call(checkContext, elem) > -1;
                    }, implicitRelative, true),
                    matchers = [function(elem, context, xml) {
                        return (!leadingRelative && (xml || context !== outermostContext)) || ((checkContext = context).nodeType ? matchContext(elem, context, xml) : matchAnyContext(elem, context, xml));
                    }];
                for (; i < len; i++) {
                    if ((matcher = Expr.relative[tokens[i].type])) {
                        matchers = [addCombinator(elementMatcher(matchers), matcher)];
                    } else {
                        matcher = Expr.filter[tokens[i].type].apply(null, tokens[i].matches);
                        if (matcher[expando]) {
                            j = ++i;
                            for (; j < len; j++) {
                                if (Expr.relative[tokens[j].type]) {
                                    break;
                                }
                            }
                            return setMatcher(i > 1 && elementMatcher(matchers), i > 1 && toSelector(tokens.slice(0, i - 1).concat({
                                value: tokens[i - 2].type === " " ? "*" : ""
                            })).replace(rtrim, "$1"), matcher, i < j && matcherFromTokens(tokens.slice(i, j)), j < len && matcherFromTokens((tokens = tokens.slice(j))), j < len && toSelector(tokens));
                        }
                        matchers.push(matcher);
                    }
                }
                return elementMatcher(matchers);
            }

            function matcherFromGroupMatchers(elementMatchers, setMatchers) {
                var bySet = setMatchers.length > 0,
                    byElement = elementMatchers.length > 0,
                    superMatcher = function(seed, context, xml, results, outermost) {
                        var elem, j, matcher, matchedCount = 0,
                            i = "0",
                            unmatched = seed && [],
                            setMatched = [],
                            contextBackup = outermostContext,
                            elems = seed || byElement && Expr.find["TAG"]("*", outermost),
                            dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
                            len = elems.length;
                        if (outermost) {
                            outermostContext = context !== document && context;
                        }
                        for (; i !== len && (elem = elems[i]) != null; i++) {
                            if (byElement && elem) {
                                j = 0;
                                while ((matcher = elementMatchers[j++])) {
                                    if (matcher(elem, context, xml)) {
                                        results.push(elem);
                                        break;
                                    }
                                }
                                if (outermost) {
                                    dirruns = dirrunsUnique;
                                }
                            }
                            if (bySet) {
                                if ((elem = !matcher && elem)) {
                                    matchedCount--;
                                }
                                if (seed) {
                                    unmatched.push(elem);
                                }
                            }
                        }
                        matchedCount += i;
                        if (bySet && i !== matchedCount) {
                            j = 0;
                            while ((matcher = setMatchers[j++])) {
                                matcher(unmatched, setMatched, context, xml);
                            }
                            if (seed) {
                                if (matchedCount > 0) {
                                    while (i--) {
                                        if (!(unmatched[i] || setMatched[i])) {
                                            setMatched[i] = pop.call(results);
                                        }
                                    }
                                }
                                setMatched = condense(setMatched);
                            }
                            push.apply(results, setMatched);
                            if (outermost && !seed && setMatched.length > 0 && (matchedCount + setMatchers.length) > 1) {
                                Sizzle.uniqueSort(results);
                            }
                        }
                        if (outermost) {
                            dirruns = dirrunsUnique;
                            outermostContext = contextBackup;
                        }
                        return unmatched;
                    };
                return bySet ? markFunction(superMatcher) : superMatcher;
            }

            compile = Sizzle.compile = function(selector, group) {
                var i, setMatchers = [],
                    elementMatchers = [],
                    cached = compilerCache[selector + " "];
                if (!cached) {
                    if (!group) {
                        group = tokenize(selector);
                    }
                    i = group.length;
                    while (i--) {
                        cached = matcherFromTokens(group[i]);
                        if (cached[expando]) {
                            setMatchers.push(cached);
                        } else {
                            elementMatchers.push(cached);
                        }
                    }
                    cached = compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers));
                }
                return cached;
            };

            function multipleContexts(selector, contexts, results) {
                var i = 0,
                    len = contexts.length;
                for (; i < len; i++) {
                    Sizzle(selector, contexts[i], results);
                }
                return results;
            }

            function select(selector, context, results, seed) {
                var i, tokens, token, type, find, match = tokenize(selector);
                if (!seed) {
                    if (match.length === 1) {
                        tokens = match[0] = match[0].slice(0);
                        if (tokens.length > 2 && (token = tokens[0]).type === "ID" && support.getById && context.nodeType === 9 && documentIsHTML && Expr.relative[tokens[1].type]) {
                            context = (Expr.find["ID"](token.matches[0].replace(runescape, funescape), context) || [])[0];
                            if (!context) {
                                return results;
                            }
                            selector = selector.slice(tokens.shift().value.length);
                        }
                        i = matchExpr["needsContext"].test(selector) ? 0 : tokens.length;
                        while (i--) {
                            token = tokens[i];
                            if (Expr.relative[(type = token.type)]) {
                                break;
                            }
                            if ((find = Expr.find[type])) {
                                if ((seed = find(token.matches[0].replace(runescape, funescape), rsibling.test(tokens[0].type) && testContext(context.parentNode) || context))) {
                                    tokens.splice(i, 1);
                                    selector = seed.length && toSelector(tokens);
                                    if (!selector) {
                                        push.apply(results, seed);
                                        return results;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                compile(selector, match)(seed, context, !documentIsHTML, results, rsibling.test(selector) && testContext(context.parentNode) || context);
                return results;
            }

            support.sortStable = expando.split("").sort(sortOrder).join("") === expando;
            support.detectDuplicates = !!hasDuplicate;
            setDocument();
            support.sortDetached = assert(function(div1) {
                return div1.compareDocumentPosition(document.createElement("div")) & 1;
            });
            if (!assert(function(div) {
                    div.innerHTML = "<a href='#'></a>";
                    return div.firstChild.getAttribute("href") === "#";
                })) {
                addHandle("type|href|height|width", function(elem, name, isXML) {
                    if (!isXML) {
                        return elem.getAttribute(name, name.toLowerCase() === "type" ? 1 : 2);
                    }
                });
            }
            if (!support.attributes || !assert(function(div) {
                    div.innerHTML = "<input/>";
                    div.firstChild.setAttribute("value", "");
                    return div.firstChild.getAttribute("value") === "";
                })) {
                addHandle("value", function(elem, name, isXML) {
                    if (!isXML && elem.nodeName.toLowerCase() === "input") {
                        return elem.defaultValue;
                    }
                });
            }
            if (!assert(function(div) {
                    return div.getAttribute("disabled") == null;
                })) {
                addHandle(booleans, function(elem, name, isXML) {
                    var val;
                    if (!isXML) {
                        return elem[name] === true ? name.toLowerCase() : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
                    }
                });
            }
            return Sizzle;
        })(window);
        jQuery.find = Sizzle;
        jQuery.expr = Sizzle.selectors;
        jQuery.expr[":"] = jQuery.expr.pseudos;
        jQuery.unique = Sizzle.uniqueSort;
        jQuery.text = Sizzle.getText;
        jQuery.isXMLDoc = Sizzle.isXML;
        jQuery.contains = Sizzle.contains;
        var rneedsContext = jQuery.expr.match.needsContext;
        var rsingleTag = (/^<(\w+)\s*\/?>(?:<\/\1>|)$/);
        var risSimple = /^.[^:#\[\.,]*$/;

        function winnow(elements, qualifier, not) {
            if (jQuery.isFunction(qualifier)) {
                return jQuery.grep(elements, function(elem, i) {
                    return !!qualifier.call(elem, i, elem) !== not;
                });
            }
            if (qualifier.nodeType) {
                return jQuery.grep(elements, function(elem) {
                    return (elem === qualifier) !== not;
                });
            }
            if (typeof qualifier === "string") {
                if (risSimple.test(qualifier)) {
                    return jQuery.filter(qualifier, elements, not);
                }
                qualifier = jQuery.filter(qualifier, elements);
            }
            return jQuery.grep(elements, function(elem) {
                return (jQuery.inArray(elem, qualifier) >= 0) !== not;
            });
        }

        jQuery.filter = function(expr, elems, not) {
            var elem = elems[0];
            if (not) {
                expr = ":not(" + expr + ")";
            }
            return elems.length === 1 && elem.nodeType === 1 ? jQuery.find.matchesSelector(elem, expr) ? [elem] : [] : jQuery.find.matches(expr, jQuery.grep(elems, function(elem) {
                return elem.nodeType === 1;
            }));
        };
        jQuery.fn.extend({
            find: function(selector) {
                var i, ret = [],
                    self = this,
                    len = self.length;
                if (typeof selector !== "string") {
                    return this.pushStack(jQuery(selector).filter(function() {
                        for (i = 0; i < len; i++) {
                            if (jQuery.contains(self[i], this)) {
                                return true;
                            }
                        }
                    }));
                }
                for (i = 0; i < len; i++) {
                    jQuery.find(selector, self[i], ret);
                }
                ret = this.pushStack(len > 1 ? jQuery.unique(ret) : ret);
                ret.selector = this.selector ? this.selector + " " + selector : selector;
                return ret;
            },
            filter: function(selector) {
                return this.pushStack(winnow(this, selector || [], false));
            },
            not: function(selector) {
                return this.pushStack(winnow(this, selector || [], true));
            },
            is: function(selector) {
                return !!winnow(this, typeof selector === "string" && rneedsContext.test(selector) ? jQuery(selector) : selector || [], false).length;
            }
        });
        var rootjQuery, document = window.document,
            rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
            init = jQuery.fn.init = function(selector, context) {
                var match, elem;
                if (!selector) {
                    return this;
                }
                if (typeof selector === "string") {
                    if (selector.charAt(0) === "<" && selector.charAt(selector.length - 1) === ">" && selector.length >= 3) {
                        match = [null, selector, null];
                    } else {
                        match = rquickExpr.exec(selector);
                    }
                    if (match && (match[1] || !context)) {
                        if (match[1]) {
                            context = context instanceof jQuery ? context[0] : context;
                            jQuery.merge(this, jQuery.parseHTML(match[1], context && context.nodeType ? context.ownerDocument || context : document, true));
                            if (rsingleTag.test(match[1]) && jQuery.isPlainObject(context)) {
                                for (match in context) {
                                    if (jQuery.isFunction(this[match])) {
                                        this[match](context[match]);
                                    } else {
                                        this.attr(match, context[match]);
                                    }
                                }
                            }
                            return this;
                        } else {
                            elem = document.getElementById(match[2]);
                            if (elem && elem.parentNode) {
                                if (elem.id !== match[2]) {
                                    return rootjQuery.find(selector);
                                }
                                this.length = 1;
                                this[0] = elem;
                            }
                            this.context = document;
                            this.selector = selector;
                            return this;
                        }
                    } else if (!context || context.jquery) {
                        return (context || rootjQuery).find(selector);
                    } else {
                        return this.constructor(context).find(selector);
                    }
                } else if (selector.nodeType) {
                    this.context = this[0] = selector;
                    this.length = 1;
                    return this;
                } else if (jQuery.isFunction(selector)) {
                    return typeof rootjQuery.ready !== "undefined" ? rootjQuery.ready(selector) : selector(jQuery);
                }
                if (selector.selector !== undefined) {
                    this.selector = selector.selector;
                    this.context = selector.context;
                }
                return jQuery.makeArray(selector, this);
            };
        init.prototype = jQuery.fn;
        rootjQuery = jQuery(document);
        var rparentsprev = /^(?:parents|prev(?:Until|All))/,
            guaranteedUnique = {
                children: true,
                contents: true,
                next: true,
                prev: true
            };
        jQuery.extend({
            dir: function(elem, dir, until) {
                var matched = [],
                    cur = elem[dir];
                while (cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery(cur).is(until))) {
                    if (cur.nodeType === 1) {
                        matched.push(cur);
                    }
                    cur = cur[dir];
                }
                return matched;
            },
            sibling: function(n, elem) {
                var r = [];
                for (; n; n = n.nextSibling) {
                    if (n.nodeType === 1 && n !== elem) {
                        r.push(n);
                    }
                }
                return r;
            }
        });
        jQuery.fn.extend({
            has: function(target) {
                var i, targets = jQuery(target, this),
                    len = targets.length;
                return this.filter(function() {
                    for (i = 0; i < len; i++) {
                        if (jQuery.contains(this, targets[i])) {
                            return true;
                        }
                    }
                });
            },
            closest: function(selectors, context) {
                var cur, i = 0,
                    l = this.length,
                    matched = [],
                    pos = rneedsContext.test(selectors) || typeof selectors !== "string" ? jQuery(selectors, context || this.context) : 0;
                for (; i < l; i++) {
                    for (cur = this[i]; cur && cur !== context; cur = cur.parentNode) {
                        if (cur.nodeType < 11 && (pos ? pos.index(cur) > -1 : cur.nodeType === 1 && jQuery.find.matchesSelector(cur, selectors))) {
                            matched.push(cur);
                            break;
                        }
                    }
                }
                return this.pushStack(matched.length > 1 ? jQuery.unique(matched) : matched);
            },
            index: function(elem) {
                if (!elem) {
                    return (this[0] && this[0].parentNode) ? this.first().prevAll().length : -1;
                }
                if (typeof elem === "string") {
                    return jQuery.inArray(this[0], jQuery(elem));
                }
                return jQuery.inArray(elem.jquery ? elem[0] : elem, this);
            },
            add: function(selector, context) {
                return this.pushStack(jQuery.unique(jQuery.merge(this.get(), jQuery(selector, context))));
            },
            addBack: function(selector) {
                return this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
            }
        });

        function sibling(cur, dir) {
            do {
                cur = cur[dir];
            } while (cur && cur.nodeType !== 1);
            return cur;
        }

        jQuery.each({
            parent: function(elem) {
                var parent = elem.parentNode;
                return parent && parent.nodeType !== 11 ? parent : null;
            },
            parents: function(elem) {
                return jQuery.dir(elem, "parentNode");
            },
            parentsUntil: function(elem, i, until) {
                return jQuery.dir(elem, "parentNode", until);
            },
            next: function(elem) {
                return sibling(elem, "nextSibling");
            },
            prev: function(elem) {
                return sibling(elem, "previousSibling");
            },
            nextAll: function(elem) {
                return jQuery.dir(elem, "nextSibling");
            },
            prevAll: function(elem) {
                return jQuery.dir(elem, "previousSibling");
            },
            nextUntil: function(elem, i, until) {
                return jQuery.dir(elem, "nextSibling", until);
            },
            prevUntil: function(elem, i, until) {
                return jQuery.dir(elem, "previousSibling", until);
            },
            siblings: function(elem) {
                return jQuery.sibling((elem.parentNode || {}).firstChild, elem);
            },
            children: function(elem) {
                return jQuery.sibling(elem.firstChild);
            },
            contents: function(elem) {
                return jQuery.nodeName(elem, "iframe") ? elem.contentDocument || elem.contentWindow.document : jQuery.merge([], elem.childNodes);
            }
        }, function(name, fn) {
            jQuery.fn[name] = function(until, selector) {
                var ret = jQuery.map(this, fn, until);
                if (name.slice(-5) !== "Until") {
                    selector = until;
                }
                if (selector && typeof selector === "string") {
                    ret = jQuery.filter(selector, ret);
                }
                if (this.length > 1) {
                    if (!guaranteedUnique[name]) {
                        ret = jQuery.unique(ret);
                    }
                    if (rparentsprev.test(name)) {
                        ret = ret.reverse();
                    }
                }
                return this.pushStack(ret);
            };
        });
        var rnotwhite = (/\S+/g);
        var optionsCache = {};

        function createOptions(options) {
            var object = optionsCache[options] = {};
            jQuery.each(options.match(rnotwhite) || [], function(_, flag) {
                object[flag] = true;
            });
            return object;
        }

        jQuery.Callbacks = function(options) {
            options = typeof options === "string" ? (optionsCache[options] || createOptions(options)) : jQuery.extend({}, options);
            var
                firing, memory, fired, firingLength, firingIndex, firingStart, list = [],
                stack = !options.once && [],
                fire = function(data) {
                    memory = options.memory && data;
                    fired = true;
                    firingIndex = firingStart || 0;
                    firingStart = 0;
                    firingLength = list.length;
                    firing = true;
                    for (; list && firingIndex < firingLength; firingIndex++) {
                        if (list[firingIndex].apply(data[0], data[1]) === false && options.stopOnFalse) {
                            memory = false;
                            break;
                        }
                    }
                    firing = false;
                    if (list) {
                        if (stack) {
                            if (stack.length) {
                                fire(stack.shift());
                            }
                        } else if (memory) {
                            list = [];
                        } else {
                            self.disable();
                        }
                    }
                },
                self = {
                    add: function() {
                        if (list) {
                            var start = list.length;
                            (function add(args) {
                                jQuery.each(args, function(_, arg) {
                                    var type = jQuery.type(arg);
                                    if (type === "function") {
                                        if (!options.unique || !self.has(arg)) {
                                            list.push(arg);
                                        }
                                    } else if (arg && arg.length && type !== "string") {
                                        add(arg);
                                    }
                                });
                            })(arguments);
                            if (firing) {
                                firingLength = list.length;
                            } else if (memory) {
                                firingStart = start;
                                fire(memory);
                            }
                        }
                        return this;
                    },
                    remove: function() {
                        if (list) {
                            jQuery.each(arguments, function(_, arg) {
                                var index;
                                while ((index = jQuery.inArray(arg, list, index)) > -1) {
                                    list.splice(index, 1);
                                    if (firing) {
                                        if (index <= firingLength) {
                                            firingLength--;
                                        }
                                        if (index <= firingIndex) {
                                            firingIndex--;
                                        }
                                    }
                                }
                            });
                        }
                        return this;
                    },
                    has: function(fn) {
                        return fn ? jQuery.inArray(fn, list) > -1 : !!(list && list.length);
                    },
                    empty: function() {
                        list = [];
                        firingLength = 0;
                        return this;
                    },
                    disable: function() {
                        list = stack = memory = undefined;
                        return this;
                    },
                    disabled: function() {
                        return !list;
                    },
                    lock: function() {
                        stack = undefined;
                        if (!memory) {
                            self.disable();
                        }
                        return this;
                    },
                    locked: function() {
                        return !stack;
                    },
                    fireWith: function(context, args) {
                        if (list && (!fired || stack)) {
                            args = args || [];
                            args = [context, args.slice ? args.slice() : args];
                            if (firing) {
                                stack.push(args);
                            } else {
                                fire(args);
                            }
                        }
                        return this;
                    },
                    fire: function() {
                        self.fireWith(this, arguments);
                        return this;
                    },
                    fired: function() {
                        return !!fired;
                    }
                };
            return self;
        };
        jQuery.extend({
            Deferred: function(func) {
                var tuples = [
                        ["resolve", "done", jQuery.Callbacks("once memory"), "resolved"],
                        ["reject", "fail", jQuery.Callbacks("once memory"), "rejected"],
                        ["notify", "progress", jQuery.Callbacks("memory")]
                    ],
                    state = "pending",
                    promise = {
                        state: function() {
                            return state;
                        },
                        always: function() {
                            deferred.done(arguments).fail(arguments);
                            return this;
                        },
                        then: function() {
                            var fns = arguments;
                            return jQuery.Deferred(function(newDefer) {
                                jQuery.each(tuples, function(i, tuple) {
                                    var fn = jQuery.isFunction(fns[i]) && fns[i];
                                    deferred[tuple[1]](function() {
                                        var returned = fn && fn.apply(this, arguments);
                                        if (returned && jQuery.isFunction(returned.promise)) {
                                            returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);
                                        } else {
                                            newDefer[tuple[0] + "With"](this === promise ? newDefer.promise() : this, fn ? [returned] : arguments);
                                        }
                                    });
                                });
                                fns = null;
                            }).promise();
                        },
                        promise: function(obj) {
                            return obj != null ? jQuery.extend(obj, promise) : promise;
                        }
                    },
                    deferred = {};
                promise.pipe = promise.then;
                jQuery.each(tuples, function(i, tuple) {
                    var list = tuple[2],
                        stateString = tuple[3];
                    promise[tuple[1]] = list.add;
                    if (stateString) {
                        list.add(function() {
                            state = stateString;
                        }, tuples[i ^ 1][2].disable, tuples[2][2].lock);
                    }
                    deferred[tuple[0]] = function() {
                        deferred[tuple[0] + "With"](this === deferred ? promise : this, arguments);
                        return this;
                    };
                    deferred[tuple[0] + "With"] = list.fireWith;
                });
                promise.promise(deferred);
                if (func) {
                    func.call(deferred, deferred);
                }
                return deferred;
            },
            when: function(subordinate) {
                var i = 0,
                    resolveValues = slice.call(arguments),
                    length = resolveValues.length,
                    remaining = length !== 1 || (subordinate && jQuery.isFunction(subordinate.promise)) ? length : 0,
                    deferred = remaining === 1 ? subordinate : jQuery.Deferred(),
                    updateFunc = function(i, contexts, values) {
                        return function(value) {
                            contexts[i] = this;
                            values[i] = arguments.length > 1 ? slice.call(arguments) : value;
                            if (values === progressValues) {
                                deferred.notifyWith(contexts, values);
                            } else if (!(--remaining)) {
                                deferred.resolveWith(contexts, values);
                            }
                        };
                    },
                    progressValues, progressContexts, resolveContexts;
                if (length > 1) {
                    progressValues = new Array(length);
                    progressContexts = new Array(length);
                    resolveContexts = new Array(length);
                    for (; i < length; i++) {
                        if (resolveValues[i] && jQuery.isFunction(resolveValues[i].promise)) {
                            resolveValues[i].promise().done(updateFunc(i, resolveContexts, resolveValues)).fail(deferred.reject).progress(updateFunc(i, progressContexts, progressValues));
                        } else {
                            --remaining;
                        }
                    }
                }
                if (!remaining) {
                    deferred.resolveWith(resolveContexts, resolveValues);
                }
                return deferred.promise();
            }
        });
        var readyList;
        jQuery.fn.ready = function(fn) {
            jQuery.ready.promise().done(fn);
            return this;
        };
        jQuery.extend({
            isReady: false,
            readyWait: 1,
            holdReady: function(hold) {
                if (hold) {
                    jQuery.readyWait++;
                } else {
                    jQuery.ready(true);
                }
            },
            ready: function(wait) {
                if (wait === true ? --jQuery.readyWait : jQuery.isReady) {
                    return;
                }
                if (!document.body) {
                    return setTimeout(jQuery.ready);
                }
                jQuery.isReady = true;
                if (wait !== true && --jQuery.readyWait > 0) {
                    return;
                }
                readyList.resolveWith(document, [jQuery]);
                if (jQuery.fn.trigger) {
                    jQuery(document).trigger("ready").off("ready");
                }
            }
        });

        function detach() {
            if (document.addEventListener) {
                document.removeEventListener("DOMContentLoaded", completed, false);
                window.removeEventListener("load", completed, false);
            } else {
                document.detachEvent("onreadystatechange", completed);
                window.detachEvent("onload", completed);
            }
        }

        function completed() {
            if (document.addEventListener || event.type === "load" || document.readyState === "complete") {
                detach();
                jQuery.ready();
            }
        }

        jQuery.ready.promise = function(obj) {
            if (!readyList) {
                readyList = jQuery.Deferred();
                if (document.readyState === "complete") {
                    setTimeout(jQuery.ready);
                } else if (document.addEventListener) {
                    document.addEventListener("DOMContentLoaded", completed, false);
                    window.addEventListener("load", completed, false);
                } else {
                    document.attachEvent("onreadystatechange", completed);
                    window.attachEvent("onload", completed);
                    var top = false;
                    try {
                        top = window.frameElement == null && document.documentElement;
                    } catch (e) {}
                    if (top && top.doScroll) {
                        (function doScrollCheck() {
                            if (!jQuery.isReady) {
                                try {
                                    top.doScroll("left");
                                } catch (e) {
                                    return setTimeout(doScrollCheck, 50);
                                }
                                detach();
                                jQuery.ready();
                            }
                        })();
                    }
                }
            }
            return readyList.promise(obj);
        };
        var strundefined = typeof undefined;
        var i;
        for (i in jQuery(support)) {
            break;
        }
        support.ownLast = i !== "0";
        support.inlineBlockNeedsLayout = false;
        jQuery(function() {
            var container, div, body = document.getElementsByTagName("body")[0];
            if (!body) {
                return;
            }
            container = document.createElement("div");
            container.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px";
            div = document.createElement("div");
            body.appendChild(container).appendChild(div);
            if (typeof div.style.zoom !== strundefined) {
                div.style.cssText = "border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1";
                if ((support.inlineBlockNeedsLayout = (div.offsetWidth === 3))) {
                    body.style.zoom = 1;
                }
            }
            body.removeChild(container);
            container = div = null;
        });
        (function() {
            var div = document.createElement("div");
            if (support.deleteExpando == null) {
                support.deleteExpando = true;
                try {
                    delete div.test;
                } catch (e) {
                    support.deleteExpando = false;
                }
            }
            div = null;
        })();
        jQuery.acceptData = function(elem) {
            var noData = jQuery.noData[(elem.nodeName + " ").toLowerCase()],
                nodeType = +elem.nodeType || 1;
            return nodeType !== 1 && nodeType !== 9 ? false : !noData || noData !== true && elem.getAttribute("classid") === noData;
        };
        var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
            rmultiDash = /([A-Z])/g;

        function dataAttr(elem, key, data) {
            if (data === undefined && elem.nodeType === 1) {
                var name = "data-" + key.replace(rmultiDash, "-$1").toLowerCase();
                data = elem.getAttribute(name);
                if (typeof data === "string") {
                    try {
                        data = data === "true" ? true : data === "false" ? false : data === "null" ? null : +data + "" === data ? +data : rbrace.test(data) ? jQuery.parseJSON(data) : data;
                    } catch (e) {}
                    jQuery.data(elem, key, data);
                } else {
                    data = undefined;
                }
            }
            return data;
        }

        function isEmptyDataObject(obj) {
            var name;
            for (name in obj) {
                if (name === "data" && jQuery.isEmptyObject(obj[name])) {
                    continue;
                }
                if (name !== "toJSON") {
                    return false;
                }
            }
            return true;
        }

        function internalData(elem, name, data, pvt) {
            if (!jQuery.acceptData(elem)) {
                return;
            }
            var ret, thisCache, internalKey = jQuery.expando,
                isNode = elem.nodeType,
                cache = isNode ? jQuery.cache : elem,
                id = isNode ? elem[internalKey] : elem[internalKey] && internalKey;
            if ((!id || !cache[id] || (!pvt && !cache[id].data)) && data === undefined && typeof name === "string") {
                return;
            }
            if (!id) {
                if (isNode) {
                    id = elem[internalKey] = deletedIds.pop() || jQuery.guid++;
                } else {
                    id = internalKey;
                }
            }
            if (!cache[id]) {
                cache[id] = isNode ? {} : {
                    toJSON: jQuery.noop
                };
            }
            if (typeof name === "object" || typeof name === "function") {
                if (pvt) {
                    cache[id] = jQuery.extend(cache[id], name);
                } else {
                    cache[id].data = jQuery.extend(cache[id].data, name);
                }
            }
            thisCache = cache[id];
            if (!pvt) {
                if (!thisCache.data) {
                    thisCache.data = {};
                }
                thisCache = thisCache.data;
            }
            if (data !== undefined) {
                thisCache[jQuery.camelCase(name)] = data;
            }
            if (typeof name === "string") {
                ret = thisCache[name];
                if (ret == null) {
                    ret = thisCache[jQuery.camelCase(name)];
                }
            } else {
                ret = thisCache;
            }
            return ret;
        }

        function internalRemoveData(elem, name, pvt) {
            if (!jQuery.acceptData(elem)) {
                return;
            }
            var thisCache, i, isNode = elem.nodeType,
                cache = isNode ? jQuery.cache : elem,
                id = isNode ? elem[jQuery.expando] : jQuery.expando;
            if (!cache[id]) {
                return;
            }
            if (name) {
                thisCache = pvt ? cache[id] : cache[id].data;
                if (thisCache) {
                    if (!jQuery.isArray(name)) {
                        if (name in thisCache) {
                            name = [name];
                        } else {
                            name = jQuery.camelCase(name);
                            if (name in thisCache) {
                                name = [name];
                            } else {
                                name = name.split(" ");
                            }
                        }
                    } else {
                        name = name.concat(jQuery.map(name, jQuery.camelCase));
                    }
                    i = name.length;
                    while (i--) {
                        delete thisCache[name[i]];
                    }
                    if (pvt ? !isEmptyDataObject(thisCache) : !jQuery.isEmptyObject(thisCache)) {
                        return;
                    }
                }
            }
            if (!pvt) {
                delete cache[id].data;
                if (!isEmptyDataObject(cache[id])) {
                    return;
                }
            }
            if (isNode) {
                jQuery.cleanData([elem], true);
            } else if (support.deleteExpando || cache != cache.window) {
                delete cache[id];
            } else {
                cache[id] = null;
            }
        }

        jQuery.extend({
            cache: {},
            noData: {
                "applet ": true,
                "embed ": true,
                "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
            },
            hasData: function(elem) {
                elem = elem.nodeType ? jQuery.cache[elem[jQuery.expando]] : elem[jQuery.expando];
                return !!elem && !isEmptyDataObject(elem);
            },
            data: function(elem, name, data) {
                return internalData(elem, name, data);
            },
            removeData: function(elem, name) {
                return internalRemoveData(elem, name);
            },
            _data: function(elem, name, data) {
                return internalData(elem, name, data, true);
            },
            _removeData: function(elem, name) {
                return internalRemoveData(elem, name, true);
            }
        });
        jQuery.fn.extend({
            data: function(key, value) {
                var i, name, data, elem = this[0],
                    attrs = elem && elem.attributes;
                if (key === undefined) {
                    if (this.length) {
                        data = jQuery.data(elem);
                        if (elem.nodeType === 1 && !jQuery._data(elem, "parsedAttrs")) {
                            i = attrs.length;
                            while (i--) {
                                name = attrs[i].name;
                                if (name.indexOf("data-") === 0) {
                                    name = jQuery.camelCase(name.slice(5));
                                    dataAttr(elem, name, data[name]);
                                }
                            }
                            jQuery._data(elem, "parsedAttrs", true);
                        }
                    }
                    return data;
                }
                if (typeof key === "object") {
                    return this.each(function() {
                        jQuery.data(this, key);
                    });
                }
                return arguments.length > 1 ? this.each(function() {
                    jQuery.data(this, key, value);
                }) : elem ? dataAttr(elem, key, jQuery.data(elem, key)) : undefined;
            },
            removeData: function(key) {
                return this.each(function() {
                    jQuery.removeData(this, key);
                });
            }
        });
        jQuery.extend({
            queue: function(elem, type, data) {
                var queue;
                if (elem) {
                    type = (type || "fx") + "queue";
                    queue = jQuery._data(elem, type);
                    if (data) {
                        if (!queue || jQuery.isArray(data)) {
                            queue = jQuery._data(elem, type, jQuery.makeArray(data));
                        } else {
                            queue.push(data);
                        }
                    }
                    return queue || [];
                }
            },
            dequeue: function(elem, type) {
                type = type || "fx";
                var queue = jQuery.queue(elem, type),
                    startLength = queue.length,
                    fn = queue.shift(),
                    hooks = jQuery._queueHooks(elem, type),
                    next = function() {
                        jQuery.dequeue(elem, type);
                    };
                if (fn === "inprogress") {
                    fn = queue.shift();
                    startLength--;
                }
                if (fn) {
                    if (type === "fx") {
                        queue.unshift("inprogress");
                    }
                    delete hooks.stop;
                    fn.call(elem, next, hooks);
                }
                if (!startLength && hooks) {
                    hooks.empty.fire();
                }
            },
            _queueHooks: function(elem, type) {
                var key = type + "queueHooks";
                return jQuery._data(elem, key) || jQuery._data(elem, key, {
                    empty: jQuery.Callbacks("once memory").add(function() {
                        jQuery._removeData(elem, type + "queue");
                        jQuery._removeData(elem, key);
                    })
                });
            }
        });
        jQuery.fn.extend({
            queue: function(type, data) {
                var setter = 2;
                if (typeof type !== "string") {
                    data = type;
                    type = "fx";
                    setter--;
                }
                if (arguments.length < setter) {
                    return jQuery.queue(this[0], type);
                }
                return data === undefined ? this : this.each(function() {
                    var queue = jQuery.queue(this, type, data);
                    jQuery._queueHooks(this, type);
                    if (type === "fx" && queue[0] !== "inprogress") {
                        jQuery.dequeue(this, type);
                    }
                });
            },
            dequeue: function(type) {
                return this.each(function() {
                    jQuery.dequeue(this, type);
                });
            },
            clearQueue: function(type) {
                return this.queue(type || "fx", []);
            },
            promise: function(type, obj) {
                var tmp, count = 1,
                    defer = jQuery.Deferred(),
                    elements = this,
                    i = this.length,
                    resolve = function() {
                        if (!(--count)) {
                            defer.resolveWith(elements, [elements]);
                        }
                    };
                if (typeof type !== "string") {
                    obj = type;
                    type = undefined;
                }
                type = type || "fx";
                while (i--) {
                    tmp = jQuery._data(elements[i], type + "queueHooks");
                    if (tmp && tmp.empty) {
                        count++;
                        tmp.empty.add(resolve);
                    }
                }
                resolve();
                return defer.promise(obj);
            }
        });
        var pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;
        var cssExpand = ["Top", "Right", "Bottom", "Left"];
        var isHidden = function(elem, el) {
            elem = el || elem;
            return jQuery.css(elem, "display") === "none" || !jQuery.contains(elem.ownerDocument, elem);
        };
        var access = jQuery.access = function(elems, fn, key, value, chainable, emptyGet, raw) {
            var i = 0,
                length = elems.length,
                bulk = key == null;
            if (jQuery.type(key) === "object") {
                chainable = true;
                for (i in key) {
                    jQuery.access(elems, fn, i, key[i], true, emptyGet, raw);
                }
            } else if (value !== undefined) {
                chainable = true;
                if (!jQuery.isFunction(value)) {
                    raw = true;
                }
                if (bulk) {
                    if (raw) {
                        fn.call(elems, value);
                        fn = null;
                    } else {
                        bulk = fn;
                        fn = function(elem, key, value) {
                            return bulk.call(jQuery(elem), value);
                        };
                    }
                }
                if (fn) {
                    for (; i < length; i++) {
                        fn(elems[i], key, raw ? value : value.call(elems[i], i, fn(elems[i], key)));
                    }
                }
            }
            return chainable ? elems : bulk ? fn.call(elems) : length ? fn(elems[0], key) : emptyGet;
        };
        var rcheckableType = (/^(?:checkbox|radio)$/i);
        (function() {
            var fragment = document.createDocumentFragment(),
                div = document.createElement("div"),
                input = document.createElement("input");
            div.setAttribute("className", "t");
            div.innerHTML = "  <link/><table></table><a href='/a'>a</a>";
            support.leadingWhitespace = div.firstChild.nodeType === 3;
            support.tbody = !div.getElementsByTagName("tbody").length;
            support.htmlSerialize = !!div.getElementsByTagName("link").length;
            support.html5Clone = document.createElement("nav").cloneNode(true).outerHTML !== "<:nav></:nav>";
            input.type = "checkbox";
            input.checked = true;
            fragment.appendChild(input);
            support.appendChecked = input.checked;
            div.innerHTML = "<textarea>x</textarea>";
            support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue;
            fragment.appendChild(div);
            div.innerHTML = "<input type='radio' checked='checked' name='t'/>";
            support.checkClone = div.cloneNode(true).cloneNode(true).lastChild.checked;
            support.noCloneEvent = true;
            if (div.attachEvent) {
                div.attachEvent("onclick", function() {
                    support.noCloneEvent = false;
                });
                div.cloneNode(true).click();
            }
            if (support.deleteExpando == null) {
                support.deleteExpando = true;
                try {
                    delete div.test;
                } catch (e) {
                    support.deleteExpando = false;
                }
            }
            fragment = div = input = null;
        })();
        (function() {
            var i, eventName, div = document.createElement("div");
            for (i in {
                    submit: true,
                    change: true,
                    focusin: true
                }) {
                eventName = "on" + i;
                if (!(support[i + "Bubbles"] = eventName in window)) {
                    div.setAttribute(eventName, "t");
                    support[i + "Bubbles"] = div.attributes[eventName].expando === false;
                }
            }
            div = null;
        })();
        var rformElems = /^(?:input|select|textarea)$/i,
            rkeyEvent = /^key/,
            rmouseEvent = /^(?:mouse|contextmenu)|click/,
            rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
            rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;

        function returnTrue() {
            return true;
        }

        function returnFalse() {
            return false;
        }

        function safeActiveElement() {
            try {
                return document.activeElement;
            } catch (err) {}
        }

        jQuery.event = {
            global: {},
            add: function(elem, types, handler, data, selector) {
                var tmp, events, t, handleObjIn, special, eventHandle, handleObj, handlers, type, namespaces, origType, elemData = jQuery._data(elem);
                if (!elemData) {
                    return;
                }
                if (handler.handler) {
                    handleObjIn = handler;
                    handler = handleObjIn.handler;
                    selector = handleObjIn.selector;
                }
                if (!handler.guid) {
                    handler.guid = jQuery.guid++;
                }
                if (!(events = elemData.events)) {
                    events = elemData.events = {};
                }
                if (!(eventHandle = elemData.handle)) {
                    eventHandle = elemData.handle = function(e) {
                        return typeof jQuery !== strundefined && (!e || jQuery.event.triggered !== e.type) ? jQuery.event.dispatch.apply(eventHandle.elem, arguments) : undefined;
                    };
                    eventHandle.elem = elem;
                }
                types = (types || "").match(rnotwhite) || [""];
                t = types.length;
                while (t--) {
                    tmp = rtypenamespace.exec(types[t]) || [];
                    type = origType = tmp[1];
                    namespaces = (tmp[2] || "").split(".").sort();
                    if (!type) {
                        continue;
                    }
                    special = jQuery.event.special[type] || {};
                    type = (selector ? special.delegateType : special.bindType) || type;
                    special = jQuery.event.special[type] || {};
                    handleObj = jQuery.extend({
                        type: type,
                        origType: origType,
                        data: data,
                        handler: handler,
                        guid: handler.guid,
                        selector: selector,
                        needsContext: selector && jQuery.expr.match.needsContext.test(selector),
                        namespace: namespaces.join(".")
                    }, handleObjIn);
                    if (!(handlers = events[type])) {
                        handlers = events[type] = [];
                        handlers.delegateCount = 0;
                        if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) {
                            if (elem.addEventListener) {
                                elem.addEventListener(type, eventHandle, false);
                            } else if (elem.attachEvent) {
                                elem.attachEvent("on" + type, eventHandle);
                            }
                        }
                    }
                    if (special.add) {
                        special.add.call(elem, handleObj);
                        if (!handleObj.handler.guid) {
                            handleObj.handler.guid = handler.guid;
                        }
                    }
                    if (selector) {
                        handlers.splice(handlers.delegateCount++, 0, handleObj);
                    } else {
                        handlers.push(handleObj);
                    }
                    jQuery.event.global[type] = true;
                }
                elem = null;
            },
            remove: function(elem, types, handler, selector, mappedTypes) {
                var j, handleObj, tmp, origCount, t, events, special, handlers, type, namespaces, origType, elemData = jQuery.hasData(elem) && jQuery._data(elem);
                if (!elemData || !(events = elemData.events)) {
                    return;
                }
                types = (types || "").match(rnotwhite) || [""];
                t = types.length;
                while (t--) {
                    tmp = rtypenamespace.exec(types[t]) || [];
                    type = origType = tmp[1];
                    namespaces = (tmp[2] || "").split(".").sort();
                    if (!type) {
                        for (type in events) {
                            jQuery.event.remove(elem, type + types[t], handler, selector, true);
                        }
                        continue;
                    }
                    special = jQuery.event.special[type] || {};
                    type = (selector ? special.delegateType : special.bindType) || type;
                    handlers = events[type] || [];
                    tmp = tmp[2] && new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)");
                    origCount = j = handlers.length;
                    while (j--) {
                        handleObj = handlers[j];
                        if ((mappedTypes || origType === handleObj.origType) && (!handler || handler.guid === handleObj.guid) && (!tmp || tmp.test(handleObj.namespace)) && (!selector || selector === handleObj.selector || selector === "**" && handleObj.selector)) {
                            handlers.splice(j, 1);
                            if (handleObj.selector) {
                                handlers.delegateCount--;
                            }
                            if (special.remove) {
                                special.remove.call(elem, handleObj);
                            }
                        }
                    }
                    if (origCount && !handlers.length) {
                        if (!special.teardown || special.teardown.call(elem, namespaces, elemData.handle) === false) {
                            jQuery.removeEvent(elem, type, elemData.handle);
                        }
                        delete events[type];
                    }
                }
                if (jQuery.isEmptyObject(events)) {
                    delete elemData.handle;
                    jQuery._removeData(elem, "events");
                }
            },
            trigger: function(event, data, elem, onlyHandlers) {
                var handle, ontype, cur, bubbleType, special, tmp, i, eventPath = [elem || document],
                    type = hasOwn.call(event, "type") ? event.type : event,
                    namespaces = hasOwn.call(event, "namespace") ? event.namespace.split(".") : [];
                cur = tmp = elem = elem || document;
                if (elem.nodeType === 3 || elem.nodeType === 8) {
                    return;
                }
                if (rfocusMorph.test(type + jQuery.event.triggered)) {
                    return;
                }
                if (type.indexOf(".") >= 0) {
                    namespaces = type.split(".");
                    type = namespaces.shift();
                    namespaces.sort();
                }
                ontype = type.indexOf(":") < 0 && "on" + type;
                event = event[jQuery.expando] ? event : new jQuery.Event(type, typeof event === "object" && event);
                event.isTrigger = onlyHandlers ? 2 : 3;
                event.namespace = namespaces.join(".");
                event.namespace_re = event.namespace ? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
                event.result = undefined;
                if (!event.target) {
                    event.target = elem;
                }
                data = data == null ? [event] : jQuery.makeArray(data, [event]);
                special = jQuery.event.special[type] || {};
                if (!onlyHandlers && special.trigger && special.trigger.apply(elem, data) === false) {
                    return;
                }
                if (!onlyHandlers && !special.noBubble && !jQuery.isWindow(elem)) {
                    bubbleType = special.delegateType || type;
                    if (!rfocusMorph.test(bubbleType + type)) {
                        cur = cur.parentNode;
                    }
                    for (; cur; cur = cur.parentNode) {
                        eventPath.push(cur);
                        tmp = cur;
                    }
                    if (tmp === (elem.ownerDocument || document)) {
                        eventPath.push(tmp.defaultView || tmp.parentWindow || window);
                    }
                }
                i = 0;
                while ((cur = eventPath[i++]) && !event.isPropagationStopped()) {
                    event.type = i > 1 ? bubbleType : special.bindType || type;
                    handle = (jQuery._data(cur, "events") || {})[event.type] && jQuery._data(cur, "handle");
                    if (handle) {
                        handle.apply(cur, data);
                    }
                    handle = ontype && cur[ontype];
                    if (handle && handle.apply && jQuery.acceptData(cur)) {
                        event.result = handle.apply(cur, data);
                        if (event.result === false) {
                            event.preventDefault();
                        }
                    }
                }
                event.type = type;
                if (!onlyHandlers && !event.isDefaultPrevented()) {
                    if ((!special._default || special._default.apply(eventPath.pop(), data) === false) && jQuery.acceptData(elem)) {
                        if (ontype && elem[type] && !jQuery.isWindow(elem)) {
                            tmp = elem[ontype];
                            if (tmp) {
                                elem[ontype] = null;
                            }
                            jQuery.event.triggered = type;
                            try {
                                elem[type]();
                            } catch (e) {}
                            jQuery.event.triggered = undefined;
                            if (tmp) {
                                elem[ontype] = tmp;
                            }
                        }
                    }
                }
                return event.result;
            },
            dispatch: function(event) {
                event = jQuery.event.fix(event);
                var i, ret, handleObj, matched, j, handlerQueue = [],
                    args = slice.call(arguments),
                    handlers = (jQuery._data(this, "events") || {})[event.type] || [],
                    special = jQuery.event.special[event.type] || {};
                args[0] = event;
                event.delegateTarget = this;
                if (special.preDispatch && special.preDispatch.call(this, event) === false) {
                    return;
                }
                handlerQueue = jQuery.event.handlers.call(this, event, handlers);
                i = 0;
                while ((matched = handlerQueue[i++]) && !event.isPropagationStopped()) {
                    event.currentTarget = matched.elem;
                    j = 0;
                    while ((handleObj = matched.handlers[j++]) && !event.isImmediatePropagationStopped()) {
                        if (!event.namespace_re || event.namespace_re.test(handleObj.namespace)) {
                            event.handleObj = handleObj;
                            event.data = handleObj.data;
                            ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args);
                            if (ret !== undefined) {
                                if ((event.result = ret) === false) {
                                    event.preventDefault();
                                    event.stopPropagation();
                                }
                            }
                        }
                    }
                }
                if (special.postDispatch) {
                    special.postDispatch.call(this, event);
                }
                return event.result;
            },
            handlers: function(event, handlers) {
                var sel, handleObj, matches, i, handlerQueue = [],
                    delegateCount = handlers.delegateCount,
                    cur = event.target;
                if (delegateCount && cur.nodeType && (!event.button || event.type !== "click")) {
                    for (; cur != this; cur = cur.parentNode || this) {
                        if (cur.nodeType === 1 && (cur.disabled !== true || event.type !== "click")) {
                            matches = [];
                            for (i = 0; i < delegateCount; i++) {
                                handleObj = handlers[i];
                                sel = handleObj.selector + " ";
                                if (matches[sel] === undefined) {
                                    matches[sel] = handleObj.needsContext ? jQuery(sel, this).index(cur) >= 0 : jQuery.find(sel, this, null, [cur]).length;
                                }
                                if (matches[sel]) {
                                    matches.push(handleObj);
                                }
                            }
                            if (matches.length) {
                                handlerQueue.push({
                                    elem: cur,
                                    handlers: matches
                                });
                            }
                        }
                    }
                }
                if (delegateCount < handlers.length) {
                    handlerQueue.push({
                        elem: this,
                        handlers: handlers.slice(delegateCount)
                    });
                }
                return handlerQueue;
            },
            fix: function(event) {
                if (event[jQuery.expando]) {
                    return event;
                }
                var i, prop, copy, type = event.type,
                    originalEvent = event,
                    fixHook = this.fixHooks[type];
                if (!fixHook) {
                    this.fixHooks[type] = fixHook = rmouseEvent.test(type) ? this.mouseHooks : rkeyEvent.test(type) ? this.keyHooks : {};
                }
                copy = fixHook.props ? this.props.concat(fixHook.props) : this.props;
                event = new jQuery.Event(originalEvent);
                i = copy.length;
                while (i--) {
                    prop = copy[i];
                    event[prop] = originalEvent[prop];
                }
                if (!event.target) {
                    event.target = originalEvent.srcElement || document;
                }
                if (event.target.nodeType === 3) {
                    event.target = event.target.parentNode;
                }
                event.metaKey = !!event.metaKey;
                return fixHook.filter ? fixHook.filter(event, originalEvent) : event;
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function(event, original) {
                    if (event.which == null) {
                        event.which = original.charCode != null ? original.charCode : original.keyCode;
                    }
                    return event;
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function(event, original) {
                    var body, eventDoc, doc, button = original.button,
                        fromElement = original.fromElement;
                    if (event.pageX == null && original.clientX != null) {
                        eventDoc = event.target.ownerDocument || document;
                        doc = eventDoc.documentElement;
                        body = eventDoc.body;
                        event.pageX = original.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
                        event.pageY = original.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0);
                    }
                    if (!event.relatedTarget && fromElement) {
                        event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
                    }
                    if (!event.which && button !== undefined) {
                        event.which = (button & 1 ? 1 : (button & 2 ? 3 : (button & 4 ? 2 : 0)));
                    }
                    return event;
                }
            },
            special: {
                load: {
                    noBubble: true
                },
                focus: {
                    trigger: function() {
                        if (this !== safeActiveElement() && this.focus) {
                            try {
                                this.focus();
                                return false;
                            } catch (e) {}
                        }
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        if (this === safeActiveElement() && this.blur) {
                            this.blur();
                            return false;
                        }
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function() {
                        if (jQuery.nodeName(this, "input") && this.type === "checkbox" && this.click) {
                            this.click();
                            return false;
                        }
                    },
                    _default: function(event) {
                        return jQuery.nodeName(event.target, "a");
                    }
                },
                beforeunload: {
                    postDispatch: function(event) {
                        if (event.result !== undefined) {
                            event.originalEvent.returnValue = event.result;
                        }
                    }
                }
            },
            simulate: function(type, elem, event, bubble) {
                var e = jQuery.extend(new jQuery.Event(), event, {
                    type: type,
                    isSimulated: true,
                    originalEvent: {}
                });
                if (bubble) {
                    jQuery.event.trigger(e, null, elem);
                } else {
                    jQuery.event.dispatch.call(elem, e);
                }
                if (e.isDefaultPrevented()) {
                    event.preventDefault();
                }
            }
        };
        jQuery.removeEvent = document.removeEventListener ? function(elem, type, handle) {
            if (elem.removeEventListener) {
                elem.removeEventListener(type, handle, false);
            }
        } : function(elem, type, handle) {
            var name = "on" + type;
            if (elem.detachEvent) {
                if (typeof elem[name] === strundefined) {
                    elem[name] = null;
                }
                elem.detachEvent(name, handle);
            }
        };
        jQuery.Event = function(src, props) {
            if (!(this instanceof jQuery.Event)) {
                return new jQuery.Event(src, props);
            }
            if (src && src.type) {
                this.originalEvent = src;
                this.type = src.type;
                this.isDefaultPrevented = src.defaultPrevented || src.defaultPrevented === undefined && (src.returnValue === false || src.getPreventDefault && src.getPreventDefault()) ? returnTrue : returnFalse;
            } else {
                this.type = src;
            }
            if (props) {
                jQuery.extend(this, props);
            }
            this.timeStamp = src && src.timeStamp || jQuery.now();
            this[jQuery.expando] = true;
        };
        jQuery.Event.prototype = {
            isDefaultPrevented: returnFalse,
            isPropagationStopped: returnFalse,
            isImmediatePropagationStopped: returnFalse,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = returnTrue;
                if (!e) {
                    return;
                }
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    e.returnValue = false;
                }
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = returnTrue;
                if (!e) {
                    return;
                }
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
                e.cancelBubble = true;
            },
            stopImmediatePropagation: function() {
                this.isImmediatePropagationStopped = returnTrue;
                this.stopPropagation();
            }
        };
        jQuery.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout"
        }, function(orig, fix) {
            jQuery.event.special[orig] = {
                delegateType: fix,
                bindType: fix,
                handle: function(event) {
                    var ret, target = this,
                        related = event.relatedTarget,
                        handleObj = event.handleObj;
                    if (!related || (related !== target && !jQuery.contains(target, related))) {
                        event.type = handleObj.origType;
                        ret = handleObj.handler.apply(this, arguments);
                        event.type = fix;
                    }
                    return ret;
                }
            };
        });
        if (!support.submitBubbles) {
            jQuery.event.special.submit = {
                setup: function() {
                    if (jQuery.nodeName(this, "form")) {
                        return false;
                    }
                    jQuery.event.add(this, "click._submit keypress._submit", function(e) {
                        var elem = e.target,
                            form = jQuery.nodeName(elem, "input") || jQuery.nodeName(elem, "button") ? elem.form : undefined;
                        if (form && !jQuery._data(form, "submitBubbles")) {
                            jQuery.event.add(form, "submit._submit", function(event) {
                                event._submit_bubble = true;
                            });
                            jQuery._data(form, "submitBubbles", true);
                        }
                    });
                },
                postDispatch: function(event) {
                    if (event._submit_bubble) {
                        delete event._submit_bubble;
                        if (this.parentNode && !event.isTrigger) {
                            jQuery.event.simulate("submit", this.parentNode, event, true);
                        }
                    }
                },
                teardown: function() {
                    if (jQuery.nodeName(this, "form")) {
                        return false;
                    }
                    jQuery.event.remove(this, "._submit");
                }
            };
        }
        if (!support.changeBubbles) {
            jQuery.event.special.change = {
                setup: function() {
                    if (rformElems.test(this.nodeName)) {
                        if (this.type === "checkbox" || this.type === "radio") {
                            jQuery.event.add(this, "propertychange._change", function(event) {
                                if (event.originalEvent.propertyName === "checked") {
                                    this._just_changed = true;
                                }
                            });
                            jQuery.event.add(this, "click._change", function(event) {
                                if (this._just_changed && !event.isTrigger) {
                                    this._just_changed = false;
                                }
                                jQuery.event.simulate("change", this, event, true);
                            });
                        }
                        return false;
                    }
                    jQuery.event.add(this, "beforeactivate._change", function(e) {
                        var elem = e.target;
                        if (rformElems.test(elem.nodeName) && !jQuery._data(elem, "changeBubbles")) {
                            jQuery.event.add(elem, "change._change", function(event) {
                                if (this.parentNode && !event.isSimulated && !event.isTrigger) {
                                    jQuery.event.simulate("change", this.parentNode, event, true);
                                }
                            });
                            jQuery._data(elem, "changeBubbles", true);
                        }
                    });
                },
                handle: function(event) {
                    var elem = event.target;
                    if (this !== elem || event.isSimulated || event.isTrigger || (elem.type !== "radio" && elem.type !== "checkbox")) {
                        return event.handleObj.handler.apply(this, arguments);
                    }
                },
                teardown: function() {
                    jQuery.event.remove(this, "._change");
                    return !rformElems.test(this.nodeName);
                }
            };
        }
        if (!support.focusinBubbles) {
            jQuery.each({
                focus: "focusin",
                blur: "focusout"
            }, function(orig, fix) {
                var handler = function(event) {
                    jQuery.event.simulate(fix, event.target, jQuery.event.fix(event), true);
                };
                jQuery.event.special[fix] = {
                    setup: function() {
                        var doc = this.ownerDocument || this,
                            attaches = jQuery._data(doc, fix);
                        if (!attaches) {
                            doc.addEventListener(orig, handler, true);
                        }
                        jQuery._data(doc, fix, (attaches || 0) + 1);
                    },
                    teardown: function() {
                        var doc = this.ownerDocument || this,
                            attaches = jQuery._data(doc, fix) - 1;
                        if (!attaches) {
                            doc.removeEventListener(orig, handler, true);
                            jQuery._removeData(doc, fix);
                        } else {
                            jQuery._data(doc, fix, attaches);
                        }
                    }
                };
            });
        }
        jQuery.fn.extend({
            on: function(types, selector, data, fn, one) {
                var type, origFn;
                if (typeof types === "object") {
                    if (typeof selector !== "string") {
                        data = data || selector;
                        selector = undefined;
                    }
                    for (type in types) {
                        this.on(type, selector, data, types[type], one);
                    }
                    return this;
                }
                if (data == null && fn == null) {
                    fn = selector;
                    data = selector = undefined;
                } else if (fn == null) {
                    if (typeof selector === "string") {
                        fn = data;
                        data = undefined;
                    } else {
                        fn = data;
                        data = selector;
                        selector = undefined;
                    }
                }
                if (fn === false) {
                    fn = returnFalse;
                } else if (!fn) {
                    return this;
                }
                if (one === 1) {
                    origFn = fn;
                    fn = function(event) {
                        jQuery().off(event);
                        return origFn.apply(this, arguments);
                    };
                    fn.guid = origFn.guid || (origFn.guid = jQuery.guid++);
                }
                return this.each(function() {
                    jQuery.event.add(this, types, fn, data, selector);
                });
            },
            one: function(types, selector, data, fn) {
                return this.on(types, selector, data, fn, 1);
            },
            off: function(types, selector, fn) {
                var handleObj, type;
                if (types && types.preventDefault && types.handleObj) {
                    handleObj = types.handleObj;
                    jQuery(types.delegateTarget).off(handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler);
                    return this;
                }
                if (typeof types === "object") {
                    for (type in types) {
                        this.off(type, selector, types[type]);
                    }
                    return this;
                }
                if (selector === false || typeof selector === "function") {
                    fn = selector;
                    selector = undefined;
                }
                if (fn === false) {
                    fn = returnFalse;
                }
                return this.each(function() {
                    jQuery.event.remove(this, types, fn, selector);
                });
            },
            trigger: function(type, data) {
                return this.each(function() {
                    jQuery.event.trigger(type, data, this);
                });
            },
            triggerHandler: function(type, data) {
                var elem = this[0];
                if (elem) {
                    return jQuery.event.trigger(type, data, elem, true);
                }
            }
        });

        function createSafeFragment(document) {
            var list = nodeNames.split("|"),
                safeFrag = document.createDocumentFragment();
            if (safeFrag.createElement) {
                while (list.length) {
                    safeFrag.createElement(list.pop());
                }
            }
            return safeFrag;
        }

        var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" + "header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
            rinlinejQuery = / jQuery\d+="(?:null|\d+)"/g,
            rnoshimcache = new RegExp("<(?:" + nodeNames + ")[\\s/>]", "i"),
            rleadingWhitespace = /^\s+/,
            rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
            rtagName = /<([\w:]+)/,
            rtbody = /<tbody/i,
            rhtml = /<|&#?\w+;/,
            rnoInnerhtml = /<(?:script|style|link)/i,
            rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
            rscriptType = /^$|\/(?:java|ecma)script/i,
            rscriptTypeMasked = /^true\/(.*)/,
            rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
            wrapMap = {
                option: [1, "<select multiple='multiple'>", "</select>"],
                legend: [1, "<fieldset>", "</fieldset>"],
                area: [1, "<map>", "</map>"],
                param: [1, "<object>", "</object>"],
                thead: [1, "<table>", "</table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
            },
            safeFragment = createSafeFragment(document),
            fragmentDiv = safeFragment.appendChild(document.createElement("div"));
        wrapMap.optgroup = wrapMap.option;
        wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
        wrapMap.th = wrapMap.td;

        function getAll(context, tag) {
            var elems, elem, i = 0,
                found = typeof context.getElementsByTagName !== strundefined ? context.getElementsByTagName(tag || "*") : typeof context.querySelectorAll !== strundefined ? context.querySelectorAll(tag || "*") : undefined;
            if (!found) {
                for (found = [], elems = context.childNodes || context;
                    (elem = elems[i]) != null; i++) {
                    if (!tag || jQuery.nodeName(elem, tag)) {
                        found.push(elem);
                    } else {
                        jQuery.merge(found, getAll(elem, tag));
                    }
                }
            }
            return tag === undefined || tag && jQuery.nodeName(context, tag) ? jQuery.merge([context], found) : found;
        }

        function fixDefaultChecked(elem) {
            if (rcheckableType.test(elem.type)) {
                elem.defaultChecked = elem.checked;
            }
        }

        function manipulationTarget(elem, content) {
            return jQuery.nodeName(elem, "table") && jQuery.nodeName(content.nodeType !== 11 ? content : content.firstChild, "tr") ? elem.getElementsByTagName("tbody")[0] || elem.appendChild(elem.ownerDocument.createElement("tbody")) : elem;
        }

        function disableScript(elem) {
            elem.type = (jQuery.find.attr(elem, "type") !== null) + "/" + elem.type;
            return elem;
        }

        function restoreScript(elem) {
            var match = rscriptTypeMasked.exec(elem.type);
            if (match) {
                elem.type = match[1];
            } else {
                elem.removeAttribute("type");
            }
            return elem;
        }

        function setGlobalEval(elems, refElements) {
            var elem, i = 0;
            for (;
                (elem = elems[i]) != null; i++) {
                jQuery._data(elem, "globalEval", !refElements || jQuery._data(refElements[i], "globalEval"));
            }
        }

        function cloneCopyEvent(src, dest) {
            if (dest.nodeType !== 1 || !jQuery.hasData(src)) {
                return;
            }
            var type, i, l, oldData = jQuery._data(src),
                curData = jQuery._data(dest, oldData),
                events = oldData.events;
            if (events) {
                delete curData.handle;
                curData.events = {};
                for (type in events) {
                    for (i = 0, l = events[type].length; i < l; i++) {
                        jQuery.event.add(dest, type, events[type][i]);
                    }
                }
            }
            if (curData.data) {
                curData.data = jQuery.extend({}, curData.data);
            }
        }

        function fixCloneNodeIssues(src, dest) {
            var nodeName, e, data;
            if (dest.nodeType !== 1) {
                return;
            }
            nodeName = dest.nodeName.toLowerCase();
            if (!support.noCloneEvent && dest[jQuery.expando]) {
                data = jQuery._data(dest);
                for (e in data.events) {
                    jQuery.removeEvent(dest, e, data.handle);
                }
                dest.removeAttribute(jQuery.expando);
            }
            if (nodeName === "script" && dest.text !== src.text) {
                disableScript(dest).text = src.text;
                restoreScript(dest);
            } else if (nodeName === "object") {
                if (dest.parentNode) {
                    dest.outerHTML = src.outerHTML;
                }
                if (support.html5Clone && (src.innerHTML && !jQuery.trim(dest.innerHTML))) {
                    dest.innerHTML = src.innerHTML;
                }
            } else if (nodeName === "input" && rcheckableType.test(src.type)) {
                dest.defaultChecked = dest.checked = src.checked;
                if (dest.value !== src.value) {
                    dest.value = src.value;
                }
            } else if (nodeName === "option") {
                dest.defaultSelected = dest.selected = src.defaultSelected;
            } else if (nodeName === "input" || nodeName === "textarea") {
                dest.defaultValue = src.defaultValue;
            }
        }

        jQuery.extend({
            clone: function(elem, dataAndEvents, deepDataAndEvents) {
                var destElements, node, clone, i, srcElements, inPage = jQuery.contains(elem.ownerDocument, elem);
                if (support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test("<" + elem.nodeName + ">")) {
                    clone = elem.cloneNode(true);
                } else {
                    fragmentDiv.innerHTML = elem.outerHTML;
                    fragmentDiv.removeChild(clone = fragmentDiv.firstChild);
                }
                if ((!support.noCloneEvent || !support.noCloneChecked) && (elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem)) {
                    destElements = getAll(clone);
                    srcElements = getAll(elem);
                    for (i = 0;
                        (node = srcElements[i]) != null; ++i) {
                        if (destElements[i]) {
                            fixCloneNodeIssues(node, destElements[i]);
                        }
                    }
                }
                if (dataAndEvents) {
                    if (deepDataAndEvents) {
                        srcElements = srcElements || getAll(elem);
                        destElements = destElements || getAll(clone);
                        for (i = 0;
                            (node = srcElements[i]) != null; i++) {
                            cloneCopyEvent(node, destElements[i]);
                        }
                    } else {
                        cloneCopyEvent(elem, clone);
                    }
                }
                destElements = getAll(clone, "script");
                if (destElements.length > 0) {
                    setGlobalEval(destElements, !inPage && getAll(elem, "script"));
                }
                destElements = srcElements = node = null;
                return clone;
            },
            buildFragment: function(elems, context, scripts, selection) {
                var j, elem, contains, tmp, tag, tbody, wrap, l = elems.length,
                    safe = createSafeFragment(context),
                    nodes = [],
                    i = 0;
                for (; i < l; i++) {
                    elem = elems[i];
                    if (elem || elem === 0) {
                        if (jQuery.type(elem) === "object") {
                            jQuery.merge(nodes, elem.nodeType ? [elem] : elem);
                        } else if (!rhtml.test(elem)) {
                            nodes.push(context.createTextNode(elem));
                        } else {
                            tmp = tmp || safe.appendChild(context.createElement("div"));
                            tag = (rtagName.exec(elem) || ["", ""])[1].toLowerCase();
                            wrap = wrapMap[tag] || wrapMap._default;
                            tmp.innerHTML = wrap[1] + elem.replace(rxhtmlTag, "<$1></$2>") + wrap[2];
                            j = wrap[0];
                            while (j--) {
                                tmp = tmp.lastChild;
                            }
                            if (!support.leadingWhitespace && rleadingWhitespace.test(elem)) {
                                nodes.push(context.createTextNode(rleadingWhitespace.exec(elem)[0]));
                            }
                            if (!support.tbody) {
                                elem = tag === "table" && !rtbody.test(elem) ? tmp.firstChild : wrap[1] === "<table>" && !rtbody.test(elem) ? tmp : 0;
                                j = elem && elem.childNodes.length;
                                while (j--) {
                                    if (jQuery.nodeName((tbody = elem.childNodes[j]), "tbody") && !tbody.childNodes.length) {
                                        elem.removeChild(tbody);
                                    }
                                }
                            }
                            jQuery.merge(nodes, tmp.childNodes);
                            tmp.textContent = "";
                            while (tmp.firstChild) {
                                tmp.removeChild(tmp.firstChild);
                            }
                            tmp = safe.lastChild;
                        }
                    }
                }
                if (tmp) {
                    safe.removeChild(tmp);
                }
                if (!support.appendChecked) {
                    jQuery.grep(getAll(nodes, "input"), fixDefaultChecked);
                }
                i = 0;
                while ((elem = nodes[i++])) {
                    if (selection && jQuery.inArray(elem, selection) !== -1) {
                        continue;
                    }
                    contains = jQuery.contains(elem.ownerDocument, elem);
                    tmp = getAll(safe.appendChild(elem), "script");
                    if (contains) {
                        setGlobalEval(tmp);
                    }
                    if (scripts) {
                        j = 0;
                        while ((elem = tmp[j++])) {
                            if (rscriptType.test(elem.type || "")) {
                                scripts.push(elem);
                            }
                        }
                    }
                }
                tmp = null;
                return safe;
            },
            cleanData: function(elems, acceptData) {
                var elem, type, id, data, i = 0,
                    internalKey = jQuery.expando,
                    cache = jQuery.cache,
                    deleteExpando = support.deleteExpando,
                    special = jQuery.event.special;
                for (;
                    (elem = elems[i]) != null; i++) {
                    if (acceptData || jQuery.acceptData(elem)) {
                        id = elem[internalKey];
                        data = id && cache[id];
                        if (data) {
                            if (data.events) {
                                for (type in data.events) {
                                    if (special[type]) {
                                        jQuery.event.remove(elem, type);
                                    } else {
                                        jQuery.removeEvent(elem, type, data.handle);
                                    }
                                }
                            }
                            if (cache[id]) {
                                delete cache[id];
                                if (deleteExpando) {
                                    delete elem[internalKey];
                                } else if (typeof elem.removeAttribute !== strundefined) {
                                    elem.removeAttribute(internalKey);
                                } else {
                                    elem[internalKey] = null;
                                }
                                deletedIds.push(id);
                            }
                        }
                    }
                }
            }
        });
        jQuery.fn.extend({
            text: function(value) {
                return access(this, function(value) {
                    return value === undefined ? jQuery.text(this) : this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode(value));
                }, null, value, arguments.length);
            },
            append: function() {
                return this.domManip(arguments, function(elem) {
                    if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                        var target = manipulationTarget(this, elem);
                        target.appendChild(elem);
                    }
                });
            },
            prepend: function() {
                return this.domManip(arguments, function(elem) {
                    if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                        var target = manipulationTarget(this, elem);
                        target.insertBefore(elem, target.firstChild);
                    }
                });
            },
            before: function() {
                return this.domManip(arguments, function(elem) {
                    if (this.parentNode) {
                        this.parentNode.insertBefore(elem, this);
                    }
                });
            },
            after: function() {
                return this.domManip(arguments, function(elem) {
                    if (this.parentNode) {
                        this.parentNode.insertBefore(elem, this.nextSibling);
                    }
                });
            },
            remove: function(selector, keepData) {
                var elem, elems = selector ? jQuery.filter(selector, this) : this,
                    i = 0;
                for (;
                    (elem = elems[i]) != null; i++) {
                    if (!keepData && elem.nodeType === 1) {
                        jQuery.cleanData(getAll(elem));
                    }
                    if (elem.parentNode) {
                        if (keepData && jQuery.contains(elem.ownerDocument, elem)) {
                            setGlobalEval(getAll(elem, "script"));
                        }
                        elem.parentNode.removeChild(elem);
                    }
                }
                return this;
            },
            empty: function() {
                var elem, i = 0;
                for (;
                    (elem = this[i]) != null; i++) {
                    if (elem.nodeType === 1) {
                        jQuery.cleanData(getAll(elem, false));
                    }
                    while (elem.firstChild) {
                        elem.removeChild(elem.firstChild);
                    }
                    if (elem.options && jQuery.nodeName(elem, "select")) {
                        elem.options.length = 0;
                    }
                }
                return this;
            },
            clone: function(dataAndEvents, deepDataAndEvents) {
                dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
                deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
                return this.map(function() {
                    return jQuery.clone(this, dataAndEvents, deepDataAndEvents);
                });
            },
            html: function(value) {
                return access(this, function(value) {
                    var elem = this[0] || {},
                        i = 0,
                        l = this.length;
                    if (value === undefined) {
                        return elem.nodeType === 1 ? elem.innerHTML.replace(rinlinejQuery, "") : undefined;
                    }
                    if (typeof value === "string" && !rnoInnerhtml.test(value) && (support.htmlSerialize || !rnoshimcache.test(value)) && (support.leadingWhitespace || !rleadingWhitespace.test(value)) && !wrapMap[(rtagName.exec(value) || ["", ""])[1].toLowerCase()]) {
                        value = value.replace(rxhtmlTag, "<$1></$2>");
                        try {
                            for (; i < l; i++) {
                                elem = this[i] || {};
                                if (elem.nodeType === 1) {
                                    jQuery.cleanData(getAll(elem, false));
                                    elem.innerHTML = value;
                                }
                            }
                            elem = 0;
                        } catch (e) {}
                    }
                    if (elem) {
                        this.empty().append(value);
                    }
                }, null, value, arguments.length);
            },
            replaceWith: function() {
                var arg = arguments[0];
                this.domManip(arguments, function(elem) {
                    arg = this.parentNode;
                    jQuery.cleanData(getAll(this));
                    if (arg) {
                        arg.replaceChild(elem, this);
                    }
                });
                return arg && (arg.length || arg.nodeType) ? this : this.remove();
            },
            detach: function(selector) {
                return this.remove(selector, true);
            },
            domManip: function(args, callback) {
                args = concat.apply([], args);
                var first, node, hasScripts, scripts, doc, fragment, i = 0,
                    l = this.length,
                    set = this,
                    iNoClone = l - 1,
                    value = args[0],
                    isFunction = jQuery.isFunction(value);
                if (isFunction || (l > 1 && typeof value === "string" && !support.checkClone && rchecked.test(value))) {
                    return this.each(function(index) {
                        var self = set.eq(index);
                        if (isFunction) {
                            args[0] = value.call(this, index, self.html());
                        }
                        self.domManip(args, callback);
                    });
                }
                if (l) {
                    fragment = jQuery.buildFragment(args, this[0].ownerDocument, false, this);
                    first = fragment.firstChild;
                    if (fragment.childNodes.length === 1) {
                        fragment = first;
                    }
                    if (first) {
                        scripts = jQuery.map(getAll(fragment, "script"), disableScript);
                        hasScripts = scripts.length;
                        for (; i < l; i++) {
                            node = fragment;
                            if (i !== iNoClone) {
                                node = jQuery.clone(node, true, true);
                                if (hasScripts) {
                                    jQuery.merge(scripts, getAll(node, "script"));
                                }
                            }
                            callback.call(this[i], node, i);
                        }
                        if (hasScripts) {
                            doc = scripts[scripts.length - 1].ownerDocument;
                            jQuery.map(scripts, restoreScript);
                            for (i = 0; i < hasScripts; i++) {
                                node = scripts[i];
                                if (rscriptType.test(node.type || "") && !jQuery._data(node, "globalEval") && jQuery.contains(doc, node)) {
                                    if (node.src) {
                                        if (jQuery._evalUrl) {
                                            jQuery._evalUrl(node.src);
                                        }
                                    } else {
                                        jQuery.globalEval((node.text || node.textContent || node.innerHTML || "").replace(rcleanScript, ""));
                                    }
                                }
                            }
                        }
                        fragment = first = null;
                    }
                }
                return this;
            }
        });
        jQuery.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function(name, original) {
            jQuery.fn[name] = function(selector) {
                var elems, i = 0,
                    ret = [],
                    insert = jQuery(selector),
                    last = insert.length - 1;
                for (; i <= last; i++) {
                    elems = i === last ? this : this.clone(true);
                    jQuery(insert[i])[original](elems);
                    push.apply(ret, elems.get());
                }
                return this.pushStack(ret);
            };
        });
        var iframe, elemdisplay = {};

        function actualDisplay(name, doc) {
            var elem = jQuery(doc.createElement(name)).appendTo(doc.body),
                display = window.getDefaultComputedStyle ? window.getDefaultComputedStyle(elem[0]).display : jQuery.css(elem[0], "display");
            elem.detach();
            return display;
        }

        function defaultDisplay(nodeName) {
            var doc = document,
                display = elemdisplay[nodeName];
            if (!display) {
                display = actualDisplay(nodeName, doc);
                if (display === "none" || !display) {
                    iframe = (iframe || jQuery("<iframe frameborder='0' width='0' height='0'/>")).appendTo(doc.documentElement);
                    doc = (iframe[0].contentWindow || iframe[0].contentDocument).document;
                    doc.write();
                    doc.close();
                    display = actualDisplay(nodeName, doc);
                    iframe.detach();
                }
                elemdisplay[nodeName] = display;
            }
            return display;
        }

        (function() {
            var a, shrinkWrapBlocksVal, div = document.createElement("div"),
                divReset = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;" + "display:block;padding:0;margin:0;border:0";
            div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
            a = div.getElementsByTagName("a")[0];
            a.style.cssText = "float:left;opacity:.5";
            support.opacity = /^0.5/.test(a.style.opacity);
            support.cssFloat = !!a.style.cssFloat;
            div.style.backgroundClip = "content-box";
            div.cloneNode(true).style.backgroundClip = "";
            support.clearCloneStyle = div.style.backgroundClip === "content-box";
            a = div = null;
            support.shrinkWrapBlocks = function() {
                var body, container, div, containerStyles;
                if (shrinkWrapBlocksVal == null) {
                    body = document.getElementsByTagName("body")[0];
                    if (!body) {
                        return;
                    }
                    containerStyles = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px";
                    container = document.createElement("div");
                    div = document.createElement("div");
                    body.appendChild(container).appendChild(div);
                    shrinkWrapBlocksVal = false;
                    if (typeof div.style.zoom !== strundefined) {
                        div.style.cssText = divReset + ";width:1px;padding:1px;zoom:1";
                        div.innerHTML = "<div></div>";
                        div.firstChild.style.width = "5px";
                        shrinkWrapBlocksVal = div.offsetWidth !== 3;
                    }
                    body.removeChild(container);
                    body = container = div = null;
                }
                return shrinkWrapBlocksVal;
            };
        })();
        var rmargin = (/^margin/);
        var rnumnonpx = new RegExp("^(" + pnum + ")(?!px)[a-z%]+$", "i");
        var getStyles, curCSS, rposition = /^(top|right|bottom|left)$/;
        if (window.getComputedStyle) {
            getStyles = function(elem) {
                return elem.ownerDocument.defaultView.getComputedStyle(elem, null);
            };
            curCSS = function(elem, name, computed) {
                var width, minWidth, maxWidth, ret, style = elem.style;
                computed = computed || getStyles(elem);
                ret = computed ? computed.getPropertyValue(name) || computed[name] : undefined;
                if (computed) {
                    if (ret === "" && !jQuery.contains(elem.ownerDocument, elem)) {
                        ret = jQuery.style(elem, name);
                    }
                    if (rnumnonpx.test(ret) && rmargin.test(name)) {
                        width = style.width;
                        minWidth = style.minWidth;
                        maxWidth = style.maxWidth;
                        style.minWidth = style.maxWidth = style.width = ret;
                        ret = computed.width;
                        style.width = width;
                        style.minWidth = minWidth;
                        style.maxWidth = maxWidth;
                    }
                }
                return ret === undefined ? ret : ret + "";
            };
        } else if (document.documentElement.currentStyle) {
            getStyles = function(elem) {
                return elem.currentStyle;
            };
            curCSS = function(elem, name, computed) {
                var left, rs, rsLeft, ret, style = elem.style;
                computed = computed || getStyles(elem);
                ret = computed ? computed[name] : undefined;
                if (ret == null && style && style[name]) {
                    ret = style[name];
                }
                if (rnumnonpx.test(ret) && !rposition.test(name)) {
                    left = style.left;
                    rs = elem.runtimeStyle;
                    rsLeft = rs && rs.left;
                    if (rsLeft) {
                        rs.left = elem.currentStyle.left;
                    }
                    style.left = name === "fontSize" ? "1em" : ret;
                    ret = style.pixelLeft + "px";
                    style.left = left;
                    if (rsLeft) {
                        rs.left = rsLeft;
                    }
                }
                return ret === undefined ? ret : ret + "" || "auto";
            };
        }

        function addGetHookIf(conditionFn, hookFn) {
            return {
                get: function() {
                    var condition = conditionFn();
                    if (condition == null) {
                        return;
                    }
                    if (condition) {
                        delete this.get;
                        return;
                    }
                    return (this.get = hookFn).apply(this, arguments);
                }
            };
        }

        (function() {
            var a, reliableHiddenOffsetsVal, boxSizingVal, boxSizingReliableVal, pixelPositionVal, reliableMarginRightVal, div = document.createElement("div"),
                containerStyles = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px",
                divReset = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;" + "display:block;padding:0;margin:0;border:0";
            div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
            a = div.getElementsByTagName("a")[0];
            a.style.cssText = "float:left;opacity:.5";
            support.opacity = /^0.5/.test(a.style.opacity);
            support.cssFloat = !!a.style.cssFloat;
            div.style.backgroundClip = "content-box";
            div.cloneNode(true).style.backgroundClip = "";
            support.clearCloneStyle = div.style.backgroundClip === "content-box";
            a = div = null;
            jQuery.extend(support, {
                reliableHiddenOffsets: function() {
                    if (reliableHiddenOffsetsVal != null) {
                        return reliableHiddenOffsetsVal;
                    }
                    var container, tds, isSupported, div = document.createElement("div"),
                        body = document.getElementsByTagName("body")[0];
                    if (!body) {
                        return;
                    }
                    div.setAttribute("className", "t");
                    div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
                    container = document.createElement("div");
                    container.style.cssText = containerStyles;
                    body.appendChild(container).appendChild(div);
                    div.innerHTML = "<table><tr><td></td><td>t</td></tr></table>";
                    tds = div.getElementsByTagName("td");
                    tds[0].style.cssText = "padding:0;margin:0;border:0;display:none";
                    isSupported = (tds[0].offsetHeight === 0);
                    tds[0].style.display = "";
                    tds[1].style.display = "none";
                    reliableHiddenOffsetsVal = isSupported && (tds[0].offsetHeight === 0);
                    body.removeChild(container);
                    div = body = null;
                    return reliableHiddenOffsetsVal;
                },
                boxSizing: function() {
                    if (boxSizingVal == null) {
                        computeStyleTests();
                    }
                    return boxSizingVal;
                },
                boxSizingReliable: function() {
                    if (boxSizingReliableVal == null) {
                        computeStyleTests();
                    }
                    return boxSizingReliableVal;
                },
                pixelPosition: function() {
                    if (pixelPositionVal == null) {
                        computeStyleTests();
                    }
                    return pixelPositionVal;
                },
                reliableMarginRight: function() {
                    var body, container, div, marginDiv;
                    if (reliableMarginRightVal == null && window.getComputedStyle) {
                        body = document.getElementsByTagName("body")[0];
                        if (!body) {
                            return;
                        }
                        container = document.createElement("div");
                        div = document.createElement("div");
                        container.style.cssText = containerStyles;
                        body.appendChild(container).appendChild(div);
                        marginDiv = div.appendChild(document.createElement("div"));
                        marginDiv.style.cssText = div.style.cssText = divReset;
                        marginDiv.style.marginRight = marginDiv.style.width = "0";
                        div.style.width = "1px";
                        reliableMarginRightVal = !parseFloat((window.getComputedStyle(marginDiv, null) || {}).marginRight);
                        body.removeChild(container);
                    }
                    return reliableMarginRightVal;
                }
            });

            function computeStyleTests() {
                var container, div, body = document.getElementsByTagName("body")[0];
                if (!body) {
                    return;
                }
                container = document.createElement("div");
                div = document.createElement("div");
                container.style.cssText = containerStyles;
                body.appendChild(container).appendChild(div);
                div.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;" + "position:absolute;display:block;padding:1px;border:1px;width:4px;" + "margin-top:1%;top:1%";
                jQuery.swap(body, body.style.zoom != null ? {
                    zoom: 1
                } : {}, function() {
                    boxSizingVal = div.offsetWidth === 4;
                });
                boxSizingReliableVal = true;
                pixelPositionVal = false;
                reliableMarginRightVal = true;
                if (window.getComputedStyle) {
                    pixelPositionVal = (window.getComputedStyle(div, null) || {}).top !== "1%";
                    boxSizingReliableVal = (window.getComputedStyle(div, null) || {
                        width: "4px"
                    }).width === "4px";
                }
                body.removeChild(container);
                div = body = null;
            }
        })();
        jQuery.swap = function(elem, options, callback, args) {
            var ret, name, old = {};
            for (name in options) {
                old[name] = elem.style[name];
                elem.style[name] = options[name];
            }
            ret = callback.apply(elem, args || []);
            for (name in options) {
                elem.style[name] = old[name];
            }
            return ret;
        };
        var
            ralpha = /alpha\([^)]*\)/i,
            ropacity = /opacity\s*=\s*([^)]*)/,
            rdisplayswap = /^(none|table(?!-c[ea]).+)/,
            rnumsplit = new RegExp("^(" + pnum + ")(.*)$", "i"),
            rrelNum = new RegExp("^([+-])=(" + pnum + ")", "i"),
            cssShow = {
                position: "absolute",
                visibility: "hidden",
                display: "block"
            },
            cssNormalTransform = {
                letterSpacing: 0,
                fontWeight: 400
            },
            cssPrefixes = ["Webkit", "O", "Moz", "ms"];

        function vendorPropName(style, name) {
            if (name in style) {
                return name;
            }
            var capName = name.charAt(0).toUpperCase() + name.slice(1),
                origName = name,
                i = cssPrefixes.length;
            while (i--) {
                name = cssPrefixes[i] + capName;
                if (name in style) {
                    return name;
                }
            }
            return origName;
        }

        function showHide(elements, show) {
            var display, elem, hidden, values = [],
                index = 0,
                length = elements.length;
            for (; index < length; index++) {
                elem = elements[index];
                if (!elem.style) {
                    continue;
                }
                values[index] = jQuery._data(elem, "olddisplay");
                display = elem.style.display;
                if (show) {
                    if (!values[index] && display === "none") {
                        elem.style.display = "";
                    }
                    if (elem.style.display === "" && isHidden(elem)) {
                        values[index] = jQuery._data(elem, "olddisplay", defaultDisplay(elem.nodeName));
                    }
                } else {
                    if (!values[index]) {
                        hidden = isHidden(elem);
                        if (display && display !== "none" || !hidden) {
                            jQuery._data(elem, "olddisplay", hidden ? display : jQuery.css(elem, "display"));
                        }
                    }
                }
            }
            for (index = 0; index < length; index++) {
                elem = elements[index];
                if (!elem.style) {
                    continue;
                }
                if (!show || elem.style.display === "none" || elem.style.display === "") {
                    elem.style.display = show ? values[index] || "" : "none";
                }
            }
            return elements;
        }

        function setPositiveNumber(elem, value, subtract) {
            var matches = rnumsplit.exec(value);
            return matches ? Math.max(0, matches[1] - (subtract || 0)) + (matches[2] || "px") : value;
        }

        function augmentWidthOrHeight(elem, name, extra, isBorderBox, styles) {
            var i = extra === (isBorderBox ? "border" : "content") ? 4 : name === "width" ? 1 : 0,
                val = 0;
            for (; i < 4; i += 2) {
                if (extra === "margin") {
                    val += jQuery.css(elem, extra + cssExpand[i], true, styles);
                }
                if (isBorderBox) {
                    if (extra === "content") {
                        val -= jQuery.css(elem, "padding" + cssExpand[i], true, styles);
                    }
                    if (extra !== "margin") {
                        val -= jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
                    }
                } else {
                    val += jQuery.css(elem, "padding" + cssExpand[i], true, styles);
                    if (extra !== "padding") {
                        val += jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
                    }
                }
            }
            return val;
        }

        function getWidthOrHeight(elem, name, extra) {
            var valueIsBorderBox = true,
                val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
                styles = getStyles(elem),
                isBorderBox = support.boxSizing() && jQuery.css(elem, "boxSizing", false, styles) === "border-box";
            if (val <= 0 || val == null) {
                val = curCSS(elem, name, styles);
                if (val < 0 || val == null) {
                    val = elem.style[name];
                }
                if (rnumnonpx.test(val)) {
                    return val;
                }
                valueIsBorderBox = isBorderBox && (support.boxSizingReliable() || val === elem.style[name]);
                val = parseFloat(val) || 0;
            }
            return (val +
                augmentWidthOrHeight(elem, name, extra || (isBorderBox ? "border" : "content"), valueIsBorderBox, styles)) + "px";
        }

        jQuery.extend({
            cssHooks: {
                opacity: {
                    get: function(elem, computed) {
                        if (computed) {
                            var ret = curCSS(elem, "opacity");
                            return ret === "" ? "1" : ret;
                        }
                    }
                }
            },
            cssNumber: {
                "columnCount": true,
                "fillOpacity": true,
                "fontWeight": true,
                "lineHeight": true,
                "opacity": true,
                "order": true,
                "orphans": true,
                "widows": true,
                "zIndex": true,
                "zoom": true
            },
            cssProps: {
                "float": support.cssFloat ? "cssFloat" : "styleFloat"
            },
            style: function(elem, name, value, extra) {
                if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) {
                    return;
                }
                var ret, type, hooks, origName = jQuery.camelCase(name),
                    style = elem.style;
                name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(style, origName));
                hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
                if (value !== undefined) {
                    type = typeof value;
                    if (type === "string" && (ret = rrelNum.exec(value))) {
                        value = (ret[1] + 1) * ret[2] + parseFloat(jQuery.css(elem, name));
                        type = "number";
                    }
                    if (value == null || value !== value) {
                        return;
                    }
                    if (type === "number" && !jQuery.cssNumber[origName]) {
                        value += "px";
                    }
                    if (!support.clearCloneStyle && value === "" && name.indexOf("background") === 0) {
                        style[name] = "inherit";
                    }
                    if (!hooks || !("set" in hooks) || (value = hooks.set(elem, value, extra)) !== undefined) {
                        try {
                            style[name] = "";
                            style[name] = value;
                        } catch (e) {}
                    }
                } else {
                    if (hooks && "get" in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) {
                        return ret;
                    }
                    return style[name];
                }
            },
            css: function(elem, name, extra, styles) {
                var num, val, hooks, origName = jQuery.camelCase(name);
                name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(elem.style, origName));
                hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
                if (hooks && "get" in hooks) {
                    val = hooks.get(elem, true, extra);
                }
                if (val === undefined) {
                    val = curCSS(elem, name, styles);
                }
                if (val === "normal" && name in cssNormalTransform) {
                    val = cssNormalTransform[name];
                }
                if (extra === "" || extra) {
                    num = parseFloat(val);
                    return extra === true || jQuery.isNumeric(num) ? num || 0 : val;
                }
                return val;
            }
        });
        jQuery.each(["height", "width"], function(i, name) {
            jQuery.cssHooks[name] = {
                get: function(elem, computed, extra) {
                    if (computed) {
                        return elem.offsetWidth === 0 && rdisplayswap.test(jQuery.css(elem, "display")) ? jQuery.swap(elem, cssShow, function() {
                            return getWidthOrHeight(elem, name, extra);
                        }) : getWidthOrHeight(elem, name, extra);
                    }
                },
                set: function(elem, value, extra) {
                    var styles = extra && getStyles(elem);
                    return setPositiveNumber(elem, value, extra ? augmentWidthOrHeight(elem, name, extra, support.boxSizing() && jQuery.css(elem, "boxSizing", false, styles) === "border-box", styles) : 0);
                }
            };
        });
        if (!support.opacity) {
            jQuery.cssHooks.opacity = {
                get: function(elem, computed) {
                    return ropacity.test((computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "") ? (0.01 * parseFloat(RegExp.$1)) + "" : computed ? "1" : "";
                },
                set: function(elem, value) {
                    var style = elem.style,
                        currentStyle = elem.currentStyle,
                        opacity = jQuery.isNumeric(value) ? "alpha(opacity=" + value * 100 + ")" : "",
                        filter = currentStyle && currentStyle.filter || style.filter || "";
                    style.zoom = 1;
                    if ((value >= 1 || value === "") && jQuery.trim(filter.replace(ralpha, "")) === "" && style.removeAttribute) {
                        style.removeAttribute("filter");
                        if (value === "" || currentStyle && !currentStyle.filter) {
                            return;
                        }
                    }
                    style.filter = ralpha.test(filter) ? filter.replace(ralpha, opacity) : filter + " " + opacity;
                }
            };
        }
        jQuery.cssHooks.marginRight = addGetHookIf(support.reliableMarginRight, function(elem, computed) {
            if (computed) {
                return jQuery.swap(elem, {
                    "display": "inline-block"
                }, curCSS, [elem, "marginRight"]);
            }
        });
        jQuery.each({
            margin: "",
            padding: "",
            border: "Width"
        }, function(prefix, suffix) {
            jQuery.cssHooks[prefix + suffix] = {
                expand: function(value) {
                    var i = 0,
                        expanded = {},
                        parts = typeof value === "string" ? value.split(" ") : [value];
                    for (; i < 4; i++) {
                        expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
                    }
                    return expanded;
                }
            };
            if (!rmargin.test(prefix)) {
                jQuery.cssHooks[prefix + suffix].set = setPositiveNumber;
            }
        });
        jQuery.fn.extend({
            css: function(name, value) {
                return access(this, function(elem, name, value) {
                    var styles, len, map = {},
                        i = 0;
                    if (jQuery.isArray(name)) {
                        styles = getStyles(elem);
                        len = name.length;
                        for (; i < len; i++) {
                            map[name[i]] = jQuery.css(elem, name[i], false, styles);
                        }
                        return map;
                    }
                    return value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name);
                }, name, value, arguments.length > 1);
            },
            show: function() {
                return showHide(this, true);
            },
            hide: function() {
                return showHide(this);
            },
            toggle: function(state) {
                if (typeof state === "boolean") {
                    return state ? this.show() : this.hide();
                }
                return this.each(function() {
                    if (isHidden(this)) {
                        jQuery(this).show();
                    } else {
                        jQuery(this).hide();
                    }
                });
            }
        });

        function Tween(elem, options, prop, end, easing) {
            return new Tween.prototype.init(elem, options, prop, end, easing);
        }

        jQuery.Tween = Tween;
        Tween.prototype = {
            constructor: Tween,
            init: function(elem, options, prop, end, easing, unit) {
                this.elem = elem;
                this.prop = prop;
                this.easing = easing || "swing";
                this.options = options;
                this.start = this.now = this.cur();
                this.end = end;
                this.unit = unit || (jQuery.cssNumber[prop] ? "" : "px");
            },
            cur: function() {
                var hooks = Tween.propHooks[this.prop];
                return hooks && hooks.get ? hooks.get(this) : Tween.propHooks._default.get(this);
            },
            run: function(percent) {
                var eased, hooks = Tween.propHooks[this.prop];
                if (this.options.duration) {
                    this.pos = eased = jQuery.easing[this.easing](percent, this.options.duration * percent, 0, 1, this.options.duration);
                } else {
                    this.pos = eased = percent;
                }
                this.now = (this.end - this.start) * eased + this.start;
                if (this.options.step) {
                    this.options.step.call(this.elem, this.now, this);
                }
                if (hooks && hooks.set) {
                    hooks.set(this);
                } else {
                    Tween.propHooks._default.set(this);
                }
                return this;
            }
        };
        Tween.prototype.init.prototype = Tween.prototype;
        Tween.propHooks = {
            _default: {
                get: function(tween) {
                    var result;
                    if (tween.elem[tween.prop] != null && (!tween.elem.style || tween.elem.style[tween.prop] == null)) {
                        return tween.elem[tween.prop];
                    }
                    result = jQuery.css(tween.elem, tween.prop, "");
                    return !result || result === "auto" ? 0 : result;
                },
                set: function(tween) {
                    if (jQuery.fx.step[tween.prop]) {
                        jQuery.fx.step[tween.prop](tween);
                    } else if (tween.elem.style && (tween.elem.style[jQuery.cssProps[tween.prop]] != null || jQuery.cssHooks[tween.prop])) {
                        jQuery.style(tween.elem, tween.prop, tween.now + tween.unit);
                    } else {
                        tween.elem[tween.prop] = tween.now;
                    }
                }
            }
        };
        Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
            set: function(tween) {
                if (tween.elem.nodeType && tween.elem.parentNode) {
                    tween.elem[tween.prop] = tween.now;
                }
            }
        };
        jQuery.easing = {
            linear: function(p) {
                return p;
            },
            swing: function(p) {
                return 0.5 - Math.cos(p * Math.PI) / 2;
            }
        };
        jQuery.fx = Tween.prototype.init;
        jQuery.fx.step = {};
        var
            fxNow, timerId, rfxtypes = /^(?:toggle|show|hide)$/,
            rfxnum = new RegExp("^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i"),
            rrun = /queueHooks$/,
            animationPrefilters = [defaultPrefilter],
            tweeners = {
                "*": [function(prop, value) {
                    var tween = this.createTween(prop, value),
                        target = tween.cur(),
                        parts = rfxnum.exec(value),
                        unit = parts && parts[3] || (jQuery.cssNumber[prop] ? "" : "px"),
                        start = (jQuery.cssNumber[prop] || unit !== "px" && +target) && rfxnum.exec(jQuery.css(tween.elem, prop)),
                        scale = 1,
                        maxIterations = 20;
                    if (start && start[3] !== unit) {
                        unit = unit || start[3];
                        parts = parts || [];
                        start = +target || 1;
                        do {
                            scale = scale || ".5";
                            start = start / scale;
                            jQuery.style(tween.elem, prop, start + unit);
                        } while (scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations);
                    }
                    if (parts) {
                        start = tween.start = +start || +target || 0;
                        tween.unit = unit;
                        tween.end = parts[1] ? start + (parts[1] + 1) * parts[2] : +parts[2];
                    }
                    return tween;
                }]
            };

        function createFxNow() {
            setTimeout(function() {
                fxNow = undefined;
            });
            return (fxNow = jQuery.now());
        }

        function genFx(type, includeWidth) {
            var which, attrs = {
                    height: type
                },
                i = 0;
            includeWidth = includeWidth ? 1 : 0;
            for (; i < 4; i += 2 - includeWidth) {
                which = cssExpand[i];
                attrs["margin" + which] = attrs["padding" + which] = type;
            }
            if (includeWidth) {
                attrs.opacity = attrs.width = type;
            }
            return attrs;
        }

        function createTween(value, prop, animation) {
            var tween, collection = (tweeners[prop] || []).concat(tweeners["*"]),
                index = 0,
                length = collection.length;
            for (; index < length; index++) {
                if ((tween = collection[index].call(animation, prop, value))) {
                    return tween;
                }
            }
        }

        function defaultPrefilter(elem, props, opts) {
            var prop, value, toggle, tween, hooks, oldfire, display, dDisplay, anim = this,
                orig = {},
                style = elem.style,
                hidden = elem.nodeType && isHidden(elem),
                dataShow = jQuery._data(elem, "fxshow");
            if (!opts.queue) {
                hooks = jQuery._queueHooks(elem, "fx");
                if (hooks.unqueued == null) {
                    hooks.unqueued = 0;
                    oldfire = hooks.empty.fire;
                    hooks.empty.fire = function() {
                        if (!hooks.unqueued) {
                            oldfire();
                        }
                    };
                }
                hooks.unqueued++;
                anim.always(function() {
                    anim.always(function() {
                        hooks.unqueued--;
                        if (!jQuery.queue(elem, "fx").length) {
                            hooks.empty.fire();
                        }
                    });
                });
            }
            if (elem.nodeType === 1 && ("height" in props || "width" in props)) {
                opts.overflow = [style.overflow, style.overflowX, style.overflowY];
                display = jQuery.css(elem, "display");
                dDisplay = defaultDisplay(elem.nodeName);
                if (display === "none") {
                    display = dDisplay;
                }
                if (display === "inline" && jQuery.css(elem, "float") === "none") {
                    if (!support.inlineBlockNeedsLayout || dDisplay === "inline") {
                        style.display = "inline-block";
                    } else {
                        style.zoom = 1;
                    }
                }
            }
            if (opts.overflow) {
                style.overflow = "hidden";
                if (!support.shrinkWrapBlocks()) {
                    anim.always(function() {
                        style.overflow = opts.overflow[0];
                        style.overflowX = opts.overflow[1];
                        style.overflowY = opts.overflow[2];
                    });
                }
            }
            for (prop in props) {
                value = props[prop];
                if (rfxtypes.exec(value)) {
                    delete props[prop];
                    toggle = toggle || value === "toggle";
                    if (value === (hidden ? "hide" : "show")) {
                        if (value === "show" && dataShow && dataShow[prop] !== undefined) {
                            hidden = true;
                        } else {
                            continue;
                        }
                    }
                    orig[prop] = dataShow && dataShow[prop] || jQuery.style(elem, prop);
                }
            }
            if (!jQuery.isEmptyObject(orig)) {
                if (dataShow) {
                    if ("hidden" in dataShow) {
                        hidden = dataShow.hidden;
                    }
                } else {
                    dataShow = jQuery._data(elem, "fxshow", {});
                }
                if (toggle) {
                    dataShow.hidden = !hidden;
                }
                if (hidden) {
                    jQuery(elem).show();
                } else {
                    anim.done(function() {
                        jQuery(elem).hide();
                    });
                }
                anim.done(function() {
                    var prop;
                    jQuery._removeData(elem, "fxshow");
                    for (prop in orig) {
                        jQuery.style(elem, prop, orig[prop]);
                    }
                });
                for (prop in orig) {
                    tween = createTween(hidden ? dataShow[prop] : 0, prop, anim);
                    if (!(prop in dataShow)) {
                        dataShow[prop] = tween.start;
                        if (hidden) {
                            tween.end = tween.start;
                            tween.start = prop === "width" || prop === "height" ? 1 : 0;
                        }
                    }
                }
            }
        }

        function propFilter(props, specialEasing) {
            var index, name, easing, value, hooks;
            for (index in props) {
                name = jQuery.camelCase(index);
                easing = specialEasing[name];
                value = props[index];
                if (jQuery.isArray(value)) {
                    easing = value[1];
                    value = props[index] = value[0];
                }
                if (index !== name) {
                    props[name] = value;
                    delete props[index];
                }
                hooks = jQuery.cssHooks[name];
                if (hooks && "expand" in hooks) {
                    value = hooks.expand(value);
                    delete props[name];
                    for (index in value) {
                        if (!(index in props)) {
                            props[index] = value[index];
                            specialEasing[index] = easing;
                        }
                    }
                } else {
                    specialEasing[name] = easing;
                }
            }
        }

        function Animation(elem, properties, options) {
            var result, stopped, index = 0,
                length = animationPrefilters.length,
                deferred = jQuery.Deferred().always(function() {
                    delete tick.elem;
                }),
                tick = function() {
                    if (stopped) {
                        return false;
                    }
                    var currentTime = fxNow || createFxNow(),
                        remaining = Math.max(0, animation.startTime + animation.duration - currentTime),
                        temp = remaining / animation.duration || 0,
                        percent = 1 - temp,
                        index = 0,
                        length = animation.tweens.length;
                    for (; index < length; index++) {
                        animation.tweens[index].run(percent);
                    }
                    deferred.notifyWith(elem, [animation, percent, remaining]);
                    if (percent < 1 && length) {
                        return remaining;
                    } else {
                        deferred.resolveWith(elem, [animation]);
                        return false;
                    }
                },
                animation = deferred.promise({
                    elem: elem,
                    props: jQuery.extend({}, properties),
                    opts: jQuery.extend(true, {
                        specialEasing: {}
                    }, options),
                    originalProperties: properties,
                    originalOptions: options,
                    startTime: fxNow || createFxNow(),
                    duration: options.duration,
                    tweens: [],
                    createTween: function(prop, end) {
                        var tween = jQuery.Tween(elem, animation.opts, prop, end, animation.opts.specialEasing[prop] || animation.opts.easing);
                        animation.tweens.push(tween);
                        return tween;
                    },
                    stop: function(gotoEnd) {
                        var index = 0,
                            length = gotoEnd ? animation.tweens.length : 0;
                        if (stopped) {
                            return this;
                        }
                        stopped = true;
                        for (; index < length; index++) {
                            animation.tweens[index].run(1);
                        }
                        if (gotoEnd) {
                            deferred.resolveWith(elem, [animation, gotoEnd]);
                        } else {
                            deferred.rejectWith(elem, [animation, gotoEnd]);
                        }
                        return this;
                    }
                }),
                props = animation.props;
            propFilter(props, animation.opts.specialEasing);
            for (; index < length; index++) {
                result = animationPrefilters[index].call(animation, elem, props, animation.opts);
                if (result) {
                    return result;
                }
            }
            jQuery.map(props, createTween, animation);
            if (jQuery.isFunction(animation.opts.start)) {
                animation.opts.start.call(elem, animation);
            }
            jQuery.fx.timer(jQuery.extend(tick, {
                elem: elem,
                anim: animation,
                queue: animation.opts.queue
            }));
            return animation.progress(animation.opts.progress).done(animation.opts.done, animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);
        }

        jQuery.Animation = jQuery.extend(Animation, {
            tweener: function(props, callback) {
                if (jQuery.isFunction(props)) {
                    callback = props;
                    props = ["*"];
                } else {
                    props = props.split(" ");
                }
                var prop, index = 0,
                    length = props.length;
                for (; index < length; index++) {
                    prop = props[index];
                    tweeners[prop] = tweeners[prop] || [];
                    tweeners[prop].unshift(callback);
                }
            },
            prefilter: function(callback, prepend) {
                if (prepend) {
                    animationPrefilters.unshift(callback);
                } else {
                    animationPrefilters.push(callback);
                }
            }
        });
        jQuery.speed = function(speed, easing, fn) {
            var opt = speed && typeof speed === "object" ? jQuery.extend({}, speed) : {
                complete: fn || !fn && easing || jQuery.isFunction(speed) && speed,
                duration: speed,
                easing: fn && easing || easing && !jQuery.isFunction(easing) && easing
            };
            opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration : opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[opt.duration] : jQuery.fx.speeds._default;
            if (opt.queue == null || opt.queue === true) {
                opt.queue = "fx";
            }
            opt.old = opt.complete;
            opt.complete = function() {
                if (jQuery.isFunction(opt.old)) {
                    opt.old.call(this);
                }
                if (opt.queue) {
                    jQuery.dequeue(this, opt.queue);
                }
            };
            return opt;
        };
        jQuery.fn.extend({
            fadeTo: function(speed, to, easing, callback) {
                return this.filter(isHidden).css("opacity", 0).show().end().animate({
                    opacity: to
                }, speed, easing, callback);
            },
            animate: function(prop, speed, easing, callback) {
                var empty = jQuery.isEmptyObject(prop),
                    optall = jQuery.speed(speed, easing, callback),
                    doAnimation = function() {
                        var anim = Animation(this, jQuery.extend({}, prop), optall);
                        if (empty || jQuery._data(this, "finish")) {
                            anim.stop(true);
                        }
                    };
                doAnimation.finish = doAnimation;
                return empty || optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
            },
            stop: function(type, clearQueue, gotoEnd) {
                var stopQueue = function(hooks) {
                    var stop = hooks.stop;
                    delete hooks.stop;
                    stop(gotoEnd);
                };
                if (typeof type !== "string") {
                    gotoEnd = clearQueue;
                    clearQueue = type;
                    type = undefined;
                }
                if (clearQueue && type !== false) {
                    this.queue(type || "fx", []);
                }
                return this.each(function() {
                    var dequeue = true,
                        index = type != null && type + "queueHooks",
                        timers = jQuery.timers,
                        data = jQuery._data(this);
                    if (index) {
                        if (data[index] && data[index].stop) {
                            stopQueue(data[index]);
                        }
                    } else {
                        for (index in data) {
                            if (data[index] && data[index].stop && rrun.test(index)) {
                                stopQueue(data[index]);
                            }
                        }
                    }
                    for (index = timers.length; index--;) {
                        if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
                            timers[index].anim.stop(gotoEnd);
                            dequeue = false;
                            timers.splice(index, 1);
                        }
                    }
                    if (dequeue || !gotoEnd) {
                        jQuery.dequeue(this, type);
                    }
                });
            },
            finish: function(type) {
                if (type !== false) {
                    type = type || "fx";
                }
                return this.each(function() {
                    var index, data = jQuery._data(this),
                        queue = data[type + "queue"],
                        hooks = data[type + "queueHooks"],
                        timers = jQuery.timers,
                        length = queue ? queue.length : 0;
                    data.finish = true;
                    jQuery.queue(this, type, []);
                    if (hooks && hooks.stop) {
                        hooks.stop.call(this, true);
                    }
                    for (index = timers.length; index--;) {
                        if (timers[index].elem === this && timers[index].queue === type) {
                            timers[index].anim.stop(true);
                            timers.splice(index, 1);
                        }
                    }
                    for (index = 0; index < length; index++) {
                        if (queue[index] && queue[index].finish) {
                            queue[index].finish.call(this);
                        }
                    }
                    delete data.finish;
                });
            }
        });
        jQuery.each(["toggle", "show", "hide"], function(i, name) {
            var cssFn = jQuery.fn[name];
            jQuery.fn[name] = function(speed, easing, callback) {
                return speed == null || typeof speed === "boolean" ? cssFn.apply(this, arguments) : this.animate(genFx(name, true), speed, easing, callback);
            };
        });
        jQuery.each({
            slideDown: genFx("show"),
            slideUp: genFx("hide"),
            slideToggle: genFx("toggle"),
            fadeIn: {
                opacity: "show"
            },
            fadeOut: {
                opacity: "hide"
            },
            fadeToggle: {
                opacity: "toggle"
            }
        }, function(name, props) {
            jQuery.fn[name] = function(speed, easing, callback) {
                return this.animate(props, speed, easing, callback);
            };
        });
        jQuery.timers = [];
        jQuery.fx.tick = function() {
            var timer, timers = jQuery.timers,
                i = 0;
            fxNow = jQuery.now();
            for (; i < timers.length; i++) {
                timer = timers[i];
                if (!timer() && timers[i] === timer) {
                    timers.splice(i--, 1);
                }
            }
            if (!timers.length) {
                jQuery.fx.stop();
            }
            fxNow = undefined;
        };
        jQuery.fx.timer = function(timer) {
            jQuery.timers.push(timer);
            if (timer()) {
                jQuery.fx.start();
            } else {
                jQuery.timers.pop();
            }
        };
        jQuery.fx.interval = 13;
        jQuery.fx.start = function() {
            if (!timerId) {
                timerId = setInterval(jQuery.fx.tick, jQuery.fx.interval);
            }
        };
        jQuery.fx.stop = function() {
            clearInterval(timerId);
            timerId = null;
        };
        jQuery.fx.speeds = {
            slow: 600,
            fast: 200,
            _default: 400
        };
        jQuery.fn.delay = function(time, type) {
            time = jQuery.fx ? jQuery.fx.speeds[time] || time : time;
            type = type || "fx";
            return this.queue(type, function(next, hooks) {
                var timeout = setTimeout(next, time);
                hooks.stop = function() {
                    clearTimeout(timeout);
                };
            });
        };
        (function() {
            var a, input, select, opt, div = document.createElement("div");
            div.setAttribute("className", "t");
            div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
            a = div.getElementsByTagName("a")[0];
            select = document.createElement("select");
            opt = select.appendChild(document.createElement("option"));
            input = div.getElementsByTagName("input")[0];
            a.style.cssText = "top:1px";
            support.getSetAttribute = div.className !== "t";
            support.style = /top/.test(a.getAttribute("style"));
            support.hrefNormalized = a.getAttribute("href") === "/a";
            support.checkOn = !!input.value;
            support.optSelected = opt.selected;
            support.enctype = !!document.createElement("form").enctype;
            select.disabled = true;
            support.optDisabled = !opt.disabled;
            input = document.createElement("input");
            input.setAttribute("value", "");
            support.input = input.getAttribute("value") === "";
            input.value = "t";
            input.setAttribute("type", "radio");
            support.radioValue = input.value === "t";
            a = input = select = opt = div = null;
        })();
        var rreturn = /\r/g;
        jQuery.fn.extend({
            val: function(value) {
                var hooks, ret, isFunction, elem = this[0];
                if (!arguments.length) {
                    if (elem) {
                        hooks = jQuery.valHooks[elem.type] || jQuery.valHooks[elem.nodeName.toLowerCase()];
                        if (hooks && "get" in hooks && (ret = hooks.get(elem, "value")) !== undefined) {
                            return ret;
                        }
                        ret = elem.value;
                        return typeof ret === "string" ? ret.replace(rreturn, "") : ret == null ? "" : ret;
                    }
                    return;
                }
                isFunction = jQuery.isFunction(value);
                return this.each(function(i) {
                    var val;
                    if (this.nodeType !== 1) {
                        return;
                    }
                    if (isFunction) {
                        val = value.call(this, i, jQuery(this).val());
                    } else {
                        val = value;
                    }
                    if (val == null) {
                        val = "";
                    } else if (typeof val === "number") {
                        val += "";
                    } else if (jQuery.isArray(val)) {
                        val = jQuery.map(val, function(value) {
                            return value == null ? "" : value + "";
                        });
                    }
                    hooks = jQuery.valHooks[this.type] || jQuery.valHooks[this.nodeName.toLowerCase()];
                    if (!hooks || !("set" in hooks) || hooks.set(this, val, "value") === undefined) {
                        this.value = val;
                    }
                });
            }
        });
        jQuery.extend({
            valHooks: {
                option: {
                    get: function(elem) {
                        var val = jQuery.find.attr(elem, "value");
                        return val != null ? val : jQuery.text(elem);
                    }
                },
                select: {
                    get: function(elem) {
                        var value, option, options = elem.options,
                            index = elem.selectedIndex,
                            one = elem.type === "select-one" || index < 0,
                            values = one ? null : [],
                            max = one ? index + 1 : options.length,
                            i = index < 0 ? max : one ? index : 0;
                        for (; i < max; i++) {
                            option = options[i];
                            if ((option.selected || i === index) && (support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null) && (!option.parentNode.disabled || !jQuery.nodeName(option.parentNode, "optgroup"))) {
                                value = jQuery(option).val();
                                if (one) {
                                    return value;
                                }
                                values.push(value);
                            }
                        }
                        return values;
                    },
                    set: function(elem, value) {
                        var optionSet, option, options = elem.options,
                            values = jQuery.makeArray(value),
                            i = options.length;
                        while (i--) {
                            option = options[i];
                            if (jQuery.inArray(jQuery.valHooks.option.get(option), values) >= 0) {
                                try {
                                    option.selected = optionSet = true;
                                } catch (_) {
                                    option.scrollHeight;
                                }
                            } else {
                                option.selected = false;
                            }
                        }
                        if (!optionSet) {
                            elem.selectedIndex = -1;
                        }
                        return options;
                    }
                }
            }
        });
        jQuery.each(["radio", "checkbox"], function() {
            jQuery.valHooks[this] = {
                set: function(elem, value) {
                    if (jQuery.isArray(value)) {
                        return (elem.checked = jQuery.inArray(jQuery(elem).val(), value) >= 0);
                    }
                }
            };
            if (!support.checkOn) {
                jQuery.valHooks[this].get = function(elem) {
                    return elem.getAttribute("value") === null ? "on" : elem.value;
                };
            }
        });
        var nodeHook, boolHook, attrHandle = jQuery.expr.attrHandle,
            ruseDefault = /^(?:checked|selected)$/i,
            getSetAttribute = support.getSetAttribute,
            getSetInput = support.input;
        jQuery.fn.extend({
            attr: function(name, value) {
                return access(this, jQuery.attr, name, value, arguments.length > 1);
            },
            removeAttr: function(name) {
                return this.each(function() {
                    jQuery.removeAttr(this, name);
                });
            }
        });
        jQuery.extend({
            attr: function(elem, name, value) {
                var hooks, ret, nType = elem.nodeType;
                if (!elem || nType === 3 || nType === 8 || nType === 2) {
                    return;
                }
                if (typeof elem.getAttribute === strundefined) {
                    return jQuery.prop(elem, name, value);
                }
                if (nType !== 1 || !jQuery.isXMLDoc(elem)) {
                    name = name.toLowerCase();
                    hooks = jQuery.attrHooks[name] || (jQuery.expr.match.bool.test(name) ? boolHook : nodeHook);
                }
                if (value !== undefined) {
                    if (value === null) {
                        jQuery.removeAttr(elem, name);
                    } else if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
                        return ret;
                    } else {
                        elem.setAttribute(name, value + "");
                        return value;
                    }
                } else if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
                    return ret;
                } else {
                    ret = jQuery.find.attr(elem, name);
                    return ret == null ? undefined : ret;
                }
            },
            removeAttr: function(elem, value) {
                var name, propName, i = 0,
                    attrNames = value && value.match(rnotwhite);
                if (attrNames && elem.nodeType === 1) {
                    while ((name = attrNames[i++])) {
                        propName = jQuery.propFix[name] || name;
                        if (jQuery.expr.match.bool.test(name)) {
                            if (getSetInput && getSetAttribute || !ruseDefault.test(name)) {
                                elem[propName] = false;
                            } else {
                                elem[jQuery.camelCase("default-" + name)] = elem[propName] = false;
                            }
                        } else {
                            jQuery.attr(elem, name, "");
                        }
                        elem.removeAttribute(getSetAttribute ? name : propName);
                    }
                }
            },
            attrHooks: {
                type: {
                    set: function(elem, value) {
                        if (!support.radioValue && value === "radio" && jQuery.nodeName(elem, "input")) {
                            var val = elem.value;
                            elem.setAttribute("type", value);
                            if (val) {
                                elem.value = val;
                            }
                            return value;
                        }
                    }
                }
            }
        });
        boolHook = {
            set: function(elem, value, name) {
                if (value === false) {
                    jQuery.removeAttr(elem, name);
                } else if (getSetInput && getSetAttribute || !ruseDefault.test(name)) {
                    elem.setAttribute(!getSetAttribute && jQuery.propFix[name] || name, name);
                } else {
                    elem[jQuery.camelCase("default-" + name)] = elem[name] = true;
                }
                return name;
            }
        };
        jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g), function(i, name) {
            var getter = attrHandle[name] || jQuery.find.attr;
            attrHandle[name] = getSetInput && getSetAttribute || !ruseDefault.test(name) ? function(elem, name, isXML) {
                var ret, handle;
                if (!isXML) {
                    handle = attrHandle[name];
                    attrHandle[name] = ret;
                    ret = getter(elem, name, isXML) != null ? name.toLowerCase() : null;
                    attrHandle[name] = handle;
                }
                return ret;
            } : function(elem, name, isXML) {
                if (!isXML) {
                    return elem[jQuery.camelCase("default-" + name)] ? name.toLowerCase() : null;
                }
            };
        });
        if (!getSetInput || !getSetAttribute) {
            jQuery.attrHooks.value = {
                set: function(elem, value, name) {
                    if (jQuery.nodeName(elem, "input")) {
                        elem.defaultValue = value;
                    } else {
                        return nodeHook && nodeHook.set(elem, value, name);
                    }
                }
            };
        }
        if (!getSetAttribute) {
            nodeHook = {
                set: function(elem, value, name) {
                    var ret = elem.getAttributeNode(name);
                    if (!ret) {
                        elem.setAttributeNode((ret = elem.ownerDocument.createAttribute(name)));
                    }
                    ret.value = value += "";
                    if (name === "value" || value === elem.getAttribute(name)) {
                        return value;
                    }
                }
            };
            attrHandle.id = attrHandle.name = attrHandle.coords = function(elem, name, isXML) {
                var ret;
                if (!isXML) {
                    return (ret = elem.getAttributeNode(name)) && ret.value !== "" ? ret.value : null;
                }
            };
            jQuery.valHooks.button = {
                get: function(elem, name) {
                    var ret = elem.getAttributeNode(name);
                    if (ret && ret.specified) {
                        return ret.value;
                    }
                },
                set: nodeHook.set
            };
            jQuery.attrHooks.contenteditable = {
                set: function(elem, value, name) {
                    nodeHook.set(elem, value === "" ? false : value, name);
                }
            };
            jQuery.each(["width", "height"], function(i, name) {
                jQuery.attrHooks[name] = {
                    set: function(elem, value) {
                        if (value === "") {
                            elem.setAttribute(name, "auto");
                            return value;
                        }
                    }
                };
            });
        }
        if (!support.style) {
            jQuery.attrHooks.style = {
                get: function(elem) {
                    return elem.style.cssText || undefined;
                },
                set: function(elem, value) {
                    return (elem.style.cssText = value + "");
                }
            };
        }
        var rfocusable = /^(?:input|select|textarea|button|object)$/i,
            rclickable = /^(?:a|area)$/i;
        jQuery.fn.extend({
            prop: function(name, value) {
                return access(this, jQuery.prop, name, value, arguments.length > 1);
            },
            removeProp: function(name) {
                name = jQuery.propFix[name] || name;
                return this.each(function() {
                    try {
                        this[name] = undefined;
                        delete this[name];
                    } catch (e) {}
                });
            }
        });
        jQuery.extend({
            propFix: {
                "for": "htmlFor",
                "class": "className"
            },
            prop: function(elem, name, value) {
                var ret, hooks, notxml, nType = elem.nodeType;
                if (!elem || nType === 3 || nType === 8 || nType === 2) {
                    return;
                }
                notxml = nType !== 1 || !jQuery.isXMLDoc(elem);
                if (notxml) {
                    name = jQuery.propFix[name] || name;
                    hooks = jQuery.propHooks[name];
                }
                if (value !== undefined) {
                    return hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined ? ret : (elem[name] = value);
                } else {
                    return hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null ? ret : elem[name];
                }
            },
            propHooks: {
                tabIndex: {
                    get: function(elem) {
                        var tabindex = jQuery.find.attr(elem, "tabindex");
                        return tabindex ? parseInt(tabindex, 10) : rfocusable.test(elem.nodeName) || rclickable.test(elem.nodeName) && elem.href ? 0 : -1;
                    }
                }
            }
        });
        if (!support.hrefNormalized) {
            jQuery.each(["href", "src"], function(i, name) {
                jQuery.propHooks[name] = {
                    get: function(elem) {
                        return elem.getAttribute(name, 4);
                    }
                };
            });
        }
        if (!support.optSelected) {
            jQuery.propHooks.selected = {
                get: function(elem) {
                    var parent = elem.parentNode;
                    if (parent) {
                        parent.selectedIndex;
                        if (parent.parentNode) {
                            parent.parentNode.selectedIndex;
                        }
                    }
                    return null;
                }
            };
        }
        jQuery.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
            jQuery.propFix[this.toLowerCase()] = this;
        });
        if (!support.enctype) {
            jQuery.propFix.enctype = "encoding";
        }
        var rclass = /[\t\r\n\f]/g;
        jQuery.fn.extend({
            addClass: function(value) {
                var classes, elem, cur, clazz, j, finalValue, i = 0,
                    len = this.length,
                    proceed = typeof value === "string" && value;
                if (jQuery.isFunction(value)) {
                    return this.each(function(j) {
                        jQuery(this).addClass(value.call(this, j, this.className));
                    });
                }
                if (proceed) {
                    classes = (value || "").match(rnotwhite) || [];
                    for (; i < len; i++) {
                        elem = this[i];
                        cur = elem.nodeType === 1 && (elem.className ? (" " + elem.className + " ").replace(rclass, " ") : " ");
                        if (cur) {
                            j = 0;
                            while ((clazz = classes[j++])) {
                                if (cur.indexOf(" " + clazz + " ") < 0) {
                                    cur += clazz + " ";
                                }
                            }
                            finalValue = jQuery.trim(cur);
                            if (elem.className !== finalValue) {
                                elem.className = finalValue;
                            }
                        }
                    }
                }
                return this;
            },
            removeClass: function(value) {
                var classes, elem, cur, clazz, j, finalValue, i = 0,
                    len = this.length,
                    proceed = arguments.length === 0 || typeof value === "string" && value;
                if (jQuery.isFunction(value)) {
                    return this.each(function(j) {
                        jQuery(this).removeClass(value.call(this, j, this.className));
                    });
                }
                if (proceed) {
                    classes = (value || "").match(rnotwhite) || [];
                    for (; i < len; i++) {
                        elem = this[i];
                        cur = elem.nodeType === 1 && (elem.className ? (" " + elem.className + " ").replace(rclass, " ") : "");
                        if (cur) {
                            j = 0;
                            while ((clazz = classes[j++])) {
                                while (cur.indexOf(" " + clazz + " ") >= 0) {
                                    cur = cur.replace(" " + clazz + " ", " ");
                                }
                            }
                            finalValue = value ? jQuery.trim(cur) : "";
                            if (elem.className !== finalValue) {
                                elem.className = finalValue;
                            }
                        }
                    }
                }
                return this;
            },
            toggleClass: function(value, stateVal) {
                var type = typeof value;
                if (typeof stateVal === "boolean" && type === "string") {
                    return stateVal ? this.addClass(value) : this.removeClass(value);
                }
                if (jQuery.isFunction(value)) {
                    return this.each(function(i) {
                        jQuery(this).toggleClass(value.call(this, i, this.className, stateVal), stateVal);
                    });
                }
                return this.each(function() {
                    if (type === "string") {
                        var className, i = 0,
                            self = jQuery(this),
                            classNames = value.match(rnotwhite) || [];
                        while ((className = classNames[i++])) {
                            if (self.hasClass(className)) {
                                self.removeClass(className);
                            } else {
                                self.addClass(className);
                            }
                        }
                    } else if (type === strundefined || type === "boolean") {
                        if (this.className) {
                            jQuery._data(this, "__className__", this.className);
                        }
                        this.className = this.className || value === false ? "" : jQuery._data(this, "__className__") || "";
                    }
                });
            },
            hasClass: function(selector) {
                var className = " " + selector + " ",
                    i = 0,
                    l = this.length;
                for (; i < l; i++) {
                    if (this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf(className) >= 0) {
                        return true;
                    }
                }
                return false;
            }
        });
        jQuery.each(("blur focus focusin focusout load resize scroll unload click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup error contextmenu").split(" "), function(i, name) {
            jQuery.fn[name] = function(data, fn) {
                return arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
            };
        });
        jQuery.fn.extend({
            hover: function(fnOver, fnOut) {
                return this.mouseenter(fnOver).mouseleave(fnOut || fnOver);
            },
            bind: function(types, data, fn) {
                return this.on(types, null, data, fn);
            },
            unbind: function(types, fn) {
                return this.off(types, null, fn);
            },
            delegate: function(selector, types, data, fn) {
                return this.on(types, selector, data, fn);
            },
            undelegate: function(selector, types, fn) {
                return arguments.length === 1 ? this.off(selector, "**") : this.off(types, selector || "**", fn);
            }
        });
        var nonce = jQuery.now();
        var rquery = (/\?/);
        var rvalidtokens = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
        jQuery.parseJSON = function(data) {
            if (window.JSON && window.JSON.parse) {
                return window.JSON.parse(data + "");
            }
            var requireNonComma, depth = null,
                str = jQuery.trim(data + "");
            return str && !jQuery.trim(str.replace(rvalidtokens, function(token, comma, open, close) {
                if (requireNonComma && comma) {
                    depth = 0;
                }
                if (depth === 0) {
                    return token;
                }
                requireNonComma = open || comma;
                depth += !close - !open;
                return "";
            })) ? (Function("return " + str))() : jQuery.error("Invalid JSON: " + data);
        };
        jQuery.parseXML = function(data) {
            var xml, tmp;
            if (!data || typeof data !== "string") {
                return null;
            }
            try {
                if (window.DOMParser) {
                    tmp = new DOMParser();
                    xml = tmp.parseFromString(data, "text/xml");
                } else {
                    xml = new ActiveXObject("Microsoft.XMLDOM");
                    xml.async = "false";
                    xml.loadXML(data);
                }
            } catch (e) {
                xml = undefined;
            }
            if (!xml || !xml.documentElement || xml.getElementsByTagName("parsererror").length) {
                jQuery.error("Invalid XML: " + data);
            }
            return xml;
        };
        var
            ajaxLocParts, ajaxLocation, rhash = /#.*$/,
            rts = /([?&])_=[^&]*/,
            rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
            rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
            rnoContent = /^(?:GET|HEAD)$/,
            rprotocol = /^\/\//,
            rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
            prefilters = {},
            transports = {},
            allTypes = "*/".concat("*");
        try {
            ajaxLocation = location.href;
        } catch (e) {
            ajaxLocation = document.createElement("a");
            ajaxLocation.href = "";
            ajaxLocation = ajaxLocation.href;
        }
        ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [];

        function addToPrefiltersOrTransports(structure) {
            return function(dataTypeExpression, func) {
                if (typeof dataTypeExpression !== "string") {
                    func = dataTypeExpression;
                    dataTypeExpression = "*";
                }
                var dataType, i = 0,
                    dataTypes = dataTypeExpression.toLowerCase().match(rnotwhite) || [];
                if (jQuery.isFunction(func)) {
                    while ((dataType = dataTypes[i++])) {
                        if (dataType.charAt(0) === "+") {
                            dataType = dataType.slice(1) || "*";
                            (structure[dataType] = structure[dataType] || []).unshift(func);
                        } else {
                            (structure[dataType] = structure[dataType] || []).push(func);
                        }
                    }
                }
            };
        }

        function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR) {
            var inspected = {},
                seekingTransport = (structure === transports);

            function inspect(dataType) {
                var selected;
                inspected[dataType] = true;
                jQuery.each(structure[dataType] || [], function(_, prefilterOrFactory) {
                    var dataTypeOrTransport = prefilterOrFactory(options, originalOptions, jqXHR);
                    if (typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[dataTypeOrTransport]) {
                        options.dataTypes.unshift(dataTypeOrTransport);
                        inspect(dataTypeOrTransport);
                        return false;
                    } else if (seekingTransport) {
                        return !(selected = dataTypeOrTransport);
                    }
                });
                return selected;
            }

            return inspect(options.dataTypes[0]) || !inspected["*"] && inspect("*");
        }

        function ajaxExtend(target, src) {
            var deep, key, flatOptions = jQuery.ajaxSettings.flatOptions || {};
            for (key in src) {
                if (src[key] !== undefined) {
                    (flatOptions[key] ? target : (deep || (deep = {})))[key] = src[key];
                }
            }
            if (deep) {
                jQuery.extend(true, target, deep);
            }
            return target;
        }

        function ajaxHandleResponses(s, jqXHR, responses) {
            var firstDataType, ct, finalDataType, type, contents = s.contents,
                dataTypes = s.dataTypes;
            while (dataTypes[0] === "*") {
                dataTypes.shift();
                if (ct === undefined) {
                    ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
                }
            }
            if (ct) {
                for (type in contents) {
                    if (contents[type] && contents[type].test(ct)) {
                        dataTypes.unshift(type);
                        break;
                    }
                }
            }
            if (dataTypes[0] in responses) {
                finalDataType = dataTypes[0];
            } else {
                for (type in responses) {
                    if (!dataTypes[0] || s.converters[type + " " + dataTypes[0]]) {
                        finalDataType = type;
                        break;
                    }
                    if (!firstDataType) {
                        firstDataType = type;
                    }
                }
                finalDataType = finalDataType || firstDataType;
            }
            if (finalDataType) {
                if (finalDataType !== dataTypes[0]) {
                    dataTypes.unshift(finalDataType);
                }
                return responses[finalDataType];
            }
        }

        function ajaxConvert(s, response, jqXHR, isSuccess) {
            var conv2, current, conv, tmp, prev, converters = {},
                dataTypes = s.dataTypes.slice();
            if (dataTypes[1]) {
                for (conv in s.converters) {
                    converters[conv.toLowerCase()] = s.converters[conv];
                }
            }
            current = dataTypes.shift();
            while (current) {
                if (s.responseFields[current]) {
                    jqXHR[s.responseFields[current]] = response;
                }
                if (!prev && isSuccess && s.dataFilter) {
                    response = s.dataFilter(response, s.dataType);
                }
                prev = current;
                current = dataTypes.shift();
                if (current) {
                    if (current === "*") {
                        current = prev;
                    } else if (prev !== "*" && prev !== current) {
                        conv = converters[prev + " " + current] || converters["* " + current];
                        if (!conv) {
                            for (conv2 in converters) {
                                tmp = conv2.split(" ");
                                if (tmp[1] === current) {
                                    conv = converters[prev + " " + tmp[0]] || converters["* " + tmp[0]];
                                    if (conv) {
                                        if (conv === true) {
                                            conv = converters[conv2];
                                        } else if (converters[conv2] !== true) {
                                            current = tmp[0];
                                            dataTypes.unshift(tmp[1]);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        if (conv !== true) {
                            if (conv && s["throws"]) {
                                response = conv(response);
                            } else {
                                try {
                                    response = conv(response);
                                } catch (e) {
                                    return {
                                        state: "parsererror",
                                        error: conv ? e : "No conversion from " + prev + " to " + current
                                    };
                                }
                            }
                        }
                    }
                }
            }
            return {
                state: "success",
                data: response
            };
        }

        jQuery.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: ajaxLocation,
                type: "GET",
                isLocal: rlocalProtocol.test(ajaxLocParts[1]),
                global: true,
                processData: true,
                async: true,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": allTypes,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {
                    xml: /xml/,
                    html: /html/,
                    json: /json/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON"
                },
                converters: {
                    "* text": String,
                    "text html": true,
                    "text json": jQuery.parseJSON,
                    "text xml": jQuery.parseXML
                },
                flatOptions: {
                    url: true,
                    context: true
                }
            },
            ajaxSetup: function(target, settings) {
                return settings ? ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) : ajaxExtend(jQuery.ajaxSettings, target);
            },
            ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
            ajaxTransport: addToPrefiltersOrTransports(transports),
            ajax: function(url, options) {
                if (typeof url === "object") {
                    options = url;
                    url = undefined;
                }
                options = options || {};
                var
                    parts, i, cacheURL, responseHeadersString, timeoutTimer, fireGlobals, transport, responseHeaders, s = jQuery.ajaxSetup({}, options),
                    callbackContext = s.context || s,
                    globalEventContext = s.context && (callbackContext.nodeType || callbackContext.jquery) ? jQuery(callbackContext) : jQuery.event,
                    deferred = jQuery.Deferred(),
                    completeDeferred = jQuery.Callbacks("once memory"),
                    statusCode = s.statusCode || {},
                    requestHeaders = {},
                    requestHeadersNames = {},
                    state = 0,
                    strAbort = "canceled",
                    jqXHR = {
                        readyState: 0,
                        getResponseHeader: function(key) {
                            var match;
                            if (state === 2) {
                                if (!responseHeaders) {
                                    responseHeaders = {};
                                    while ((match = rheaders.exec(responseHeadersString))) {
                                        responseHeaders[match[1].toLowerCase()] = match[2];
                                    }
                                }
                                match = responseHeaders[key.toLowerCase()];
                            }
                            return match == null ? null : match;
                        },
                        getAllResponseHeaders: function() {
                            return state === 2 ? responseHeadersString : null;
                        },
                        setRequestHeader: function(name, value) {
                            var lname = name.toLowerCase();
                            if (!state) {
                                name = requestHeadersNames[lname] = requestHeadersNames[lname] || name;
                                requestHeaders[name] = value;
                            }
                            return this;
                        },
                        overrideMimeType: function(type) {
                            if (!state) {
                                s.mimeType = type;
                            }
                            return this;
                        },
                        statusCode: function(map) {
                            var code;
                            if (map) {
                                if (state < 2) {
                                    for (code in map) {
                                        statusCode[code] = [statusCode[code], map[code]];
                                    }
                                } else {
                                    jqXHR.always(map[jqXHR.status]);
                                }
                            }
                            return this;
                        },
                        abort: function(statusText) {
                            var finalText = statusText || strAbort;
                            if (transport) {
                                transport.abort(finalText);
                            }
                            done(0, finalText);
                            return this;
                        }
                    };
                deferred.promise(jqXHR).complete = completeDeferred.add;
                jqXHR.success = jqXHR.done;
                jqXHR.error = jqXHR.fail;
                s.url = ((url || s.url || ajaxLocation) + "").replace(rhash, "").replace(rprotocol, ajaxLocParts[1] + "//");
                s.type = options.method || options.type || s.method || s.type;
                s.dataTypes = jQuery.trim(s.dataType || "*").toLowerCase().match(rnotwhite) || [""];
                if (s.crossDomain == null) {
                    parts = rurl.exec(s.url.toLowerCase());
                    s.crossDomain = !!(parts && (parts[1] !== ajaxLocParts[1] || parts[2] !== ajaxLocParts[2] || (parts[3] || (parts[1] === "http:" ? "80" : "443")) !== (ajaxLocParts[3] || (ajaxLocParts[1] === "http:" ? "80" : "443"))));
                }
                if (s.data && s.processData && typeof s.data !== "string") {
                    s.data = jQuery.param(s.data, s.traditional);
                }
                inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);
                if (state === 2) {
                    return jqXHR;
                }
                fireGlobals = s.global;
                if (fireGlobals && jQuery.active++ === 0) {
                    jQuery.event.trigger("ajaxStart");
                }
                s.type = s.type.toUpperCase();
                s.hasContent = !rnoContent.test(s.type);
                cacheURL = s.url;
                if (!s.hasContent) {
                    if (s.data) {
                        cacheURL = (s.url += (rquery.test(cacheURL) ? "&" : "?") + s.data);
                        delete s.data;
                    }
                    if (s.cache === false) {
                        s.url = rts.test(cacheURL) ? cacheURL.replace(rts, "$1_=" + nonce++) : cacheURL + (rquery.test(cacheURL) ? "&" : "?") + "_=" + nonce++;
                    }
                }
                if (s.ifModified) {
                    if (jQuery.lastModified[cacheURL]) {
                        jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[cacheURL]);
                    }
                    if (jQuery.etag[cacheURL]) {
                        jqXHR.setRequestHeader("If-None-Match", jQuery.etag[cacheURL]);
                    }
                }
                if (s.data && s.hasContent && s.contentType !== false || options.contentType) {
                    jqXHR.setRequestHeader("Content-Type", s.contentType);
                }
                jqXHR.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== "*" ? ", " + allTypes + "; q=0.01" : "") : s.accepts["*"]);
                for (i in s.headers) {
                    jqXHR.setRequestHeader(i, s.headers[i]);
                }
                if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || state === 2)) {
                    return jqXHR.abort();
                }
                strAbort = "abort";
                for (i in {
                        success: 1,
                        error: 1,
                        complete: 1
                    }) {
                    jqXHR[i](s[i]);
                }
                transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR);
                if (!transport) {
                    done(-1, "No Transport");
                } else {
                    jqXHR.readyState = 1;
                    if (fireGlobals) {
                        globalEventContext.trigger("ajaxSend", [jqXHR, s]);
                    }
                    if (s.async && s.timeout > 0) {
                        timeoutTimer = setTimeout(function() {
                            jqXHR.abort("timeout");
                        }, s.timeout);
                    }
                    try {
                        state = 1;
                        transport.send(requestHeaders, done);
                    } catch (e) {
                        if (state < 2) {
                            done(-1, e);
                        } else {
                            throw e;
                        }
                    }
                }

                function done(status, nativeStatusText, responses, headers) {
                    var isSuccess, success, error, response, modified, statusText = nativeStatusText;
                    if (state === 2) {
                        return;
                    }
                    state = 2;
                    if (timeoutTimer) {
                        clearTimeout(timeoutTimer);
                    }
                    transport = undefined;
                    responseHeadersString = headers || "";
                    jqXHR.readyState = status > 0 ? 4 : 0;
                    isSuccess = status >= 200 && status < 300 || status === 304;
                    if (responses) {
                        response = ajaxHandleResponses(s, jqXHR, responses);
                    }
                    response = ajaxConvert(s, response, jqXHR, isSuccess);
                    if (isSuccess) {
                        if (s.ifModified) {
                            modified = jqXHR.getResponseHeader("Last-Modified");
                            if (modified) {
                                jQuery.lastModified[cacheURL] = modified;
                            }
                            modified = jqXHR.getResponseHeader("etag");
                            if (modified) {
                                jQuery.etag[cacheURL] = modified;
                            }
                        }
                        if (status === 204 || s.type === "HEAD") {
                            statusText = "nocontent";
                        } else if (status === 304) {
                            statusText = "notmodified";
                        } else {
                            statusText = response.state;
                            success = response.data;
                            error = response.error;
                            isSuccess = !error;
                        }
                    } else {
                        error = statusText;
                        if (status || !statusText) {
                            statusText = "error";
                            if (status < 0) {
                                status = 0;
                            }
                        }
                    }
                    jqXHR.status = status;
                    jqXHR.statusText = (nativeStatusText || statusText) + "";
                    if (isSuccess) {
                        deferred.resolveWith(callbackContext, [success, statusText, jqXHR]);
                    } else {
                        deferred.rejectWith(callbackContext, [jqXHR, statusText, error]);
                    }
                    jqXHR.statusCode(statusCode);
                    statusCode = undefined;
                    if (fireGlobals) {
                        globalEventContext.trigger(isSuccess ? "ajaxSuccess" : "ajaxError", [jqXHR, s, isSuccess ? success : error]);
                    }
                    completeDeferred.fireWith(callbackContext, [jqXHR, statusText]);
                    if (fireGlobals) {
                        globalEventContext.trigger("ajaxComplete", [jqXHR, s]);
                        if (!(--jQuery.active)) {
                            jQuery.event.trigger("ajaxStop");
                        }
                    }
                }

                return jqXHR;
            },
            getJSON: function(url, data, callback) {
                return jQuery.get(url, data, callback, "json");
            },
            getScript: function(url, callback) {
                return jQuery.get(url, undefined, callback, "script");
            }
        });
        jQuery.each(["get", "post"], function(i, method) {
            jQuery[method] = function(url, data, callback, type) {
                if (jQuery.isFunction(data)) {
                    type = type || callback;
                    callback = data;
                    data = undefined;
                }
                return jQuery.ajax({
                    url: url,
                    type: method,
                    dataType: type,
                    data: data,
                    success: callback
                });
            };
        });
        jQuery.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(i, type) {
            jQuery.fn[type] = function(fn) {
                return this.on(type, fn);
            };
        });
        jQuery._evalUrl = function(url) {
            return jQuery.ajax({
                url: url,
                type: "GET",
                dataType: "script",
                async: false,
                global: false,
                "throws": true
            });
        };
        jQuery.fn.extend({
            wrapAll: function(html) {
                if (jQuery.isFunction(html)) {
                    return this.each(function(i) {
                        jQuery(this).wrapAll(html.call(this, i));
                    });
                }
                if (this[0]) {
                    var wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(true);
                    if (this[0].parentNode) {
                        wrap.insertBefore(this[0]);
                    }
                    wrap.map(function() {
                        var elem = this;
                        while (elem.firstChild && elem.firstChild.nodeType === 1) {
                            elem = elem.firstChild;
                        }
                        return elem;
                    }).append(this);
                }
                return this;
            },
            wrapInner: function(html) {
                if (jQuery.isFunction(html)) {
                    return this.each(function(i) {
                        jQuery(this).wrapInner(html.call(this, i));
                    });
                }
                return this.each(function() {
                    var self = jQuery(this),
                        contents = self.contents();
                    if (contents.length) {
                        contents.wrapAll(html);
                    } else {
                        self.append(html);
                    }
                });
            },
            wrap: function(html) {
                var isFunction = jQuery.isFunction(html);
                return this.each(function(i) {
                    jQuery(this).wrapAll(isFunction ? html.call(this, i) : html);
                });
            },
            unwrap: function() {
                return this.parent().each(function() {
                    if (!jQuery.nodeName(this, "body")) {
                        jQuery(this).replaceWith(this.childNodes);
                    }
                }).end();
            }
        });
        jQuery.expr.filters.hidden = function(elem) {
            return elem.offsetWidth <= 0 && elem.offsetHeight <= 0 || (!support.reliableHiddenOffsets() && ((elem.style && elem.style.display) || jQuery.css(elem, "display")) === "none");
        };
        jQuery.expr.filters.visible = function(elem) {
            return !jQuery.expr.filters.hidden(elem);
        };
        var r20 = /%20/g,
            rbracket = /\[\]$/,
            rCRLF = /\r?\n/g,
            rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
            rsubmittable = /^(?:input|select|textarea|keygen)/i;

        function buildParams(prefix, obj, traditional, add) {
            var name;
            if (jQuery.isArray(obj)) {
                jQuery.each(obj, function(i, v) {
                    if (traditional || rbracket.test(prefix)) {
                        add(prefix, v);
                    } else {
                        buildParams(prefix + "[" + (typeof v === "object" ? i : "") + "]", v, traditional, add);
                    }
                });
            } else if (!traditional && jQuery.type(obj) === "object") {
                for (name in obj) {
                    buildParams(prefix + "[" + name + "]", obj[name], traditional, add);
                }
            } else {
                add(prefix, obj);
            }
        }

        jQuery.param = function(a, traditional) {
            var prefix, s = [],
                add = function(key, value) {
                    value = jQuery.isFunction(value) ? value() : (value == null ? "" : value);
                    s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value);
                };
            if (traditional === undefined) {
                traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
            }
            if (jQuery.isArray(a) || (a.jquery && !jQuery.isPlainObject(a))) {
                jQuery.each(a, function() {
                    add(this.name, this.value);
                });
            } else {
                for (prefix in a) {
                    buildParams(prefix, a[prefix], traditional, add);
                }
            }
            return s.join("&").replace(r20, "+");
        };
        jQuery.fn.extend({
            serialize: function() {
                return jQuery.param(this.serializeArray());
            },
            serializeArray: function() {
                return this.map(function() {
                    var elements = jQuery.prop(this, "elements");
                    return elements ? jQuery.makeArray(elements) : this;
                }).filter(function() {
                    var type = this.type;
                    return this.name && !jQuery(this).is(":disabled") && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) && (this.checked || !rcheckableType.test(type));
                }).map(function(i, elem) {
                    var val = jQuery(this).val();
                    return val == null ? null : jQuery.isArray(val) ? jQuery.map(val, function(val) {
                        return {
                            name: elem.name,
                            value: val.replace(rCRLF, "\r\n")
                        };
                    }) : {
                        name: elem.name,
                        value: val.replace(rCRLF, "\r\n")
                    };
                }).get();
            }
        });
        jQuery.ajaxSettings.xhr = window.ActiveXObject !== undefined ? function() {
            return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && createStandardXHR() || createActiveXHR();
        } : createStandardXHR;
        var xhrId = 0,
            xhrCallbacks = {},
            xhrSupported = jQuery.ajaxSettings.xhr();
        if (window.ActiveXObject) {
            jQuery(window).on("unload", function() {
                for (var key in xhrCallbacks) {
                    xhrCallbacks[key](undefined, true);
                }
            });
        }
        support.cors = !!xhrSupported && ("withCredentials" in xhrSupported);
        xhrSupported = support.ajax = !!xhrSupported;
        if (xhrSupported) {
            jQuery.ajaxTransport(function(options) {
                if (!options.crossDomain || support.cors) {
                    var callback;
                    return {
                        send: function(headers, complete) {
                            var i, xhr = options.xhr(),
                                id = ++xhrId;
                            xhr.open(options.type, options.url, options.async, options.username, options.password);
                            if (options.xhrFields) {
                                for (i in options.xhrFields) {
                                    xhr[i] = options.xhrFields[i];
                                }
                            }
                            if (options.mimeType && xhr.overrideMimeType) {
                                xhr.overrideMimeType(options.mimeType);
                            }
                            if (!options.crossDomain && !headers["X-Requested-With"]) {
                                headers["X-Requested-With"] = "XMLHttpRequest";
                            }
                            for (i in headers) {
                                if (headers[i] !== undefined) {
                                    xhr.setRequestHeader(i, headers[i] + "");
                                }
                            }
                            xhr.send((options.hasContent && options.data) || null);
                            callback = function(_, isAbort) {
                                var status, statusText, responses;
                                if (callback && (isAbort || xhr.readyState === 4)) {
                                    delete xhrCallbacks[id];
                                    callback = undefined;
                                    xhr.onreadystatechange = jQuery.noop;
                                    if (isAbort) {
                                        if (xhr.readyState !== 4) {
                                            xhr.abort();
                                        }
                                    } else {
                                        responses = {};
                                        status = xhr.status;
                                        if (typeof xhr.responseText === "string") {
                                            responses.text = xhr.responseText;
                                        }
                                        try {
                                            statusText = xhr.statusText;
                                        } catch (e) {
                                            statusText = "";
                                        }
                                        if (!status && options.isLocal && !options.crossDomain) {
                                            status = responses.text ? 200 : 404;
                                        } else if (status === 1223) {
                                            status = 204;
                                        }
                                    }
                                }
                                if (responses) {
                                    complete(status, statusText, responses, xhr.getAllResponseHeaders());
                                }
                            };
                            if (!options.async) {
                                callback();
                            } else if (xhr.readyState === 4) {
                                setTimeout(callback);
                            } else {
                                xhr.onreadystatechange = xhrCallbacks[id] = callback;
                            }
                        },
                        abort: function() {
                            if (callback) {
                                callback(undefined, true);
                            }
                        }
                    };
                }
            });
        }

        function createStandardXHR() {
            try {
                return new window.XMLHttpRequest();
            } catch (e) {}
        }

        function createActiveXHR() {
            try {
                return new window.ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }

        jQuery.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
            },
            contents: {
                script: /(?:java|ecma)script/
            },
            converters: {
                "text script": function(text) {
                    jQuery.globalEval(text);
                    return text;
                }
            }
        });
        jQuery.ajaxPrefilter("script", function(s) {
            if (s.cache === undefined) {
                s.cache = false;
            }
            if (s.crossDomain) {
                s.type = "GET";
                s.global = false;
            }
        });
        jQuery.ajaxTransport("script", function(s) {
            if (s.crossDomain) {
                var script, head = document.head || jQuery("head")[0] || document.documentElement;
                return {
                    send: function(_, callback) {
                        script = document.createElement("script");
                        script.async = true;
                        if (s.scriptCharset) {
                            script.charset = s.scriptCharset;
                        }
                        script.src = s.url;
                        script.onload = script.onreadystatechange = function(_, isAbort) {
                            if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
                                script.onload = script.onreadystatechange = null;
                                if (script.parentNode) {
                                    script.parentNode.removeChild(script);
                                }
                                script = null;
                                if (!isAbort) {
                                    callback(200, "success");
                                }
                            }
                        };
                        head.insertBefore(script, head.firstChild);
                    },
                    abort: function() {
                        if (script) {
                            script.onload(undefined, true);
                        }
                    }
                };
            }
        });
        var oldCallbacks = [],
            rjsonp = /(=)\?(?=&|$)|\?\?/;
        jQuery.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function() {
                var callback = oldCallbacks.pop() || (jQuery.expando + "_" + (nonce++));
                this[callback] = true;
                return callback;
            }
        });
        jQuery.ajaxPrefilter("json jsonp", function(s, originalSettings, jqXHR) {
            var callbackName, overwritten, responseContainer, jsonProp = s.jsonp !== false && (rjsonp.test(s.url) ? "url" : typeof s.data === "string" && !(s.contentType || "").indexOf("application/x-www-form-urlencoded") && rjsonp.test(s.data) && "data");
            if (jsonProp || s.dataTypes[0] === "jsonp") {
                callbackName = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback;
                if (jsonProp) {
                    s[jsonProp] = s[jsonProp].replace(rjsonp, "$1" + callbackName);
                } else if (s.jsonp !== false) {
                    s.url += (rquery.test(s.url) ? "&" : "?") + s.jsonp + "=" + callbackName;
                }
                s.converters["script json"] = function() {
                    if (!responseContainer) {
                        jQuery.error(callbackName + " was not called");
                    }
                    return responseContainer[0];
                };
                s.dataTypes[0] = "json";
                overwritten = window[callbackName];
                window[callbackName] = function() {
                    responseContainer = arguments;
                };
                jqXHR.always(function() {
                    window[callbackName] = overwritten;
                    if (s[callbackName]) {
                        s.jsonpCallback = originalSettings.jsonpCallback;
                        oldCallbacks.push(callbackName);
                    }
                    if (responseContainer && jQuery.isFunction(overwritten)) {
                        overwritten(responseContainer[0]);
                    }
                    responseContainer = overwritten = undefined;
                });
                return "script";
            }
        });
        jQuery.parseHTML = function(data, context, keepScripts) {
            if (!data || typeof data !== "string") {
                return null;
            }
            if (typeof context === "boolean") {
                keepScripts = context;
                context = false;
            }
            context = context || document;
            var parsed = rsingleTag.exec(data),
                scripts = !keepScripts && [];
            if (parsed) {
                return [context.createElement(parsed[1])];
            }
            parsed = jQuery.buildFragment([data], context, scripts);
            if (scripts && scripts.length) {
                jQuery(scripts).remove();
            }
            return jQuery.merge([], parsed.childNodes);
        };
        var _load = jQuery.fn.load;
        jQuery.fn.load = function(url, params, callback) {
            if (typeof url !== "string" && _load) {
                return _load.apply(this, arguments);
            }
            var selector, response, type, self = this,
                off = url.indexOf(" ");
            if (off >= 0) {
                selector = url.slice(off, url.length);
                url = url.slice(0, off);
            }
            if (jQuery.isFunction(params)) {
                callback = params;
                params = undefined;
            } else if (params && typeof params === "object") {
                type = "POST";
            }
            if (self.length > 0) {
                jQuery.ajax({
                    url: url,
                    type: type,
                    dataType: "html",
                    data: params
                }).done(function(responseText) {
                    response = arguments;
                    self.html(selector ? jQuery("<div>").append(jQuery.parseHTML(responseText)).find(selector) : responseText);
                }).complete(callback && function(jqXHR, status) {
                    self.each(callback, response || [jqXHR.responseText, status, jqXHR]);
                });
            }
            return this;
        };
        jQuery.expr.filters.animated = function(elem) {
            return jQuery.grep(jQuery.timers, function(fn) {
                return elem === fn.elem;
            }).length;
        };
        var docElem = window.document.documentElement;

        function getWindow(elem) {
            return jQuery.isWindow(elem) ? elem : elem.nodeType === 9 ? elem.defaultView || elem.parentWindow : false;
        }

        jQuery.offset = {
            setOffset: function(elem, options, i) {
                var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition, position = jQuery.css(elem, "position"),
                    curElem = jQuery(elem),
                    props = {};
                if (position === "static") {
                    elem.style.position = "relative";
                }
                curOffset = curElem.offset();
                curCSSTop = jQuery.css(elem, "top");
                curCSSLeft = jQuery.css(elem, "left");
                calculatePosition = (position === "absolute" || position === "fixed") && jQuery.inArray("auto", [curCSSTop, curCSSLeft]) > -1;
                if (calculatePosition) {
                    curPosition = curElem.position();
                    curTop = curPosition.top;
                    curLeft = curPosition.left;
                } else {
                    curTop = parseFloat(curCSSTop) || 0;
                    curLeft = parseFloat(curCSSLeft) || 0;
                }
                if (jQuery.isFunction(options)) {
                    options = options.call(elem, i, curOffset);
                }
                if (options.top != null) {
                    props.top = (options.top - curOffset.top) + curTop;
                }
                if (options.left != null) {
                    props.left = (options.left - curOffset.left) + curLeft;
                }
                if ("using" in options) {
                    options.using.call(elem, props);
                } else {
                    curElem.css(props);
                }
            }
        };
        jQuery.fn.extend({
            offset: function(options) {
                if (arguments.length) {
                    return options === undefined ? this : this.each(function(i) {
                        jQuery.offset.setOffset(this, options, i);
                    });
                }
                var docElem, win, box = {
                        top: 0,
                        left: 0
                    },
                    elem = this[0],
                    doc = elem && elem.ownerDocument;
                if (!doc) {
                    return;
                }
                docElem = doc.documentElement;
                if (!jQuery.contains(docElem, elem)) {
                    return box;
                }
                if (typeof elem.getBoundingClientRect !== strundefined) {
                    box = elem.getBoundingClientRect();
                }
                win = getWindow(doc);
                return {
                    top: box.top + (win.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0),
                    left: box.left + (win.pageXOffset || docElem.scrollLeft) - (docElem.clientLeft || 0)
                };
            },
            position: function() {
                if (!this[0]) {
                    return;
                }
                var offsetParent, offset, parentOffset = {
                        top: 0,
                        left: 0
                    },
                    elem = this[0];
                if (jQuery.css(elem, "position") === "fixed") {
                    offset = elem.getBoundingClientRect();
                } else {
                    offsetParent = this.offsetParent();
                    offset = this.offset();
                    if (!jQuery.nodeName(offsetParent[0], "html")) {
                        parentOffset = offsetParent.offset();
                    }
                    parentOffset.top += jQuery.css(offsetParent[0], "borderTopWidth", true);
                    parentOffset.left += jQuery.css(offsetParent[0], "borderLeftWidth", true);
                }
                return {
                    top: offset.top - parentOffset.top - jQuery.css(elem, "marginTop", true),
                    left: offset.left - parentOffset.left - jQuery.css(elem, "marginLeft", true)
                };
            },
            offsetParent: function() {
                return this.map(function() {
                    var offsetParent = this.offsetParent || docElem;
                    while (offsetParent && (!jQuery.nodeName(offsetParent, "html") && jQuery.css(offsetParent, "position") === "static")) {
                        offsetParent = offsetParent.offsetParent;
                    }
                    return offsetParent || docElem;
                });
            }
        });
        jQuery.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function(method, prop) {
            var top = /Y/.test(prop);
            jQuery.fn[method] = function(val) {
                return access(this, function(elem, method, val) {
                    var win = getWindow(elem);
                    if (val === undefined) {
                        return win ? (prop in win) ? win[prop] : win.document.documentElement[method] : elem[method];
                    }
                    if (win) {
                        win.scrollTo(!top ? val : jQuery(win).scrollLeft(), top ? val : jQuery(win).scrollTop());
                    } else {
                        elem[method] = val;
                    }
                }, method, val, arguments.length, null);
            };
        });
        jQuery.each(["top", "left"], function(i, prop) {
            jQuery.cssHooks[prop] = addGetHookIf(support.pixelPosition, function(elem, computed) {
                if (computed) {
                    computed = curCSS(elem, prop);
                    return rnumnonpx.test(computed) ? jQuery(elem).position()[prop] + "px" : computed;
                }
            });
        });
        jQuery.each({
            Height: "height",
            Width: "width"
        }, function(name, type) {
            jQuery.each({
                padding: "inner" + name,
                content: type,
                "": "outer" + name
            }, function(defaultExtra, funcName) {
                jQuery.fn[funcName] = function(margin, value) {
                    var chainable = arguments.length && (defaultExtra || typeof margin !== "boolean"),
                        extra = defaultExtra || (margin === true || value === true ? "margin" : "border");
                    return access(this, function(elem, type, value) {
                        var doc;
                        if (jQuery.isWindow(elem)) {
                            return elem.document.documentElement["client" + name];
                        }
                        if (elem.nodeType === 9) {
                            doc = elem.documentElement;
                            return Math.max(elem.body["scroll" + name], doc["scroll" + name], elem.body["offset" + name], doc["offset" + name], doc["client" + name]);
                        }
                        return value === undefined ? jQuery.css(elem, type, extra) : jQuery.style(elem, type, value, extra);
                    }, type, chainable ? margin : undefined, chainable, null);
                };
            });
        });
        jQuery.fn.size = function() {
            return this.length;
        };
        jQuery.fn.andSelf = jQuery.fn.addBack;
        if (typeof define === "function" && define.amd) {
            define("jquery", [], function() {
                return jQuery;
            });
        }
        var
            _jQuery = window.jQuery,
            _$ = window.$;
        jQuery.noConflict = function(deep) {
            if (window.$ === jQuery) {
                window.$ = _$;
            }
            if (deep && window.jQuery === jQuery) {
                window.jQuery = _jQuery;
            }
            return jQuery;
        };
        if (typeof noGlobal === strundefined) {
            window.jQuery = window.$ = jQuery;
        }
        return jQuery;
    }));
    (function($) {
        'use strict';
        var escape = /["\\\x00-\x1f\x7f-\x9f]/g,
            meta = {
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"': '\\"',
                '\\': '\\\\'
            },
            hasOwn = Object.prototype.hasOwnProperty;
        $.toJSON = typeof JSON === 'object' && JSON.stringify ? JSON.stringify : function(o) {
            if (o === null) {
                return 'null';
            }
            var pairs, k, name, val, type = $.type(o);
            if (type === 'undefined') {
                return undefined;
            }
            if (type === 'number' || type === 'boolean') {
                return String(o);
            }
            if (type === 'string') {
                return $.quoteString(o);
            }
            if (typeof o.toJSON === 'function') {
                return $.toJSON(o.toJSON());
            }
            if (type === 'date') {
                var month = o.getUTCMonth() + 1,
                    day = o.getUTCDate(),
                    year = o.getUTCFullYear(),
                    hours = o.getUTCHours(),
                    minutes = o.getUTCMinutes(),
                    seconds = o.getUTCSeconds(),
                    milli = o.getUTCMilliseconds();
                if (month < 10) {
                    month = '0' + month;
                }
                if (day < 10) {
                    day = '0' + day;
                }
                if (hours < 10) {
                    hours = '0' + hours;
                }
                if (minutes < 10) {
                    minutes = '0' + minutes;
                }
                if (seconds < 10) {
                    seconds = '0' + seconds;
                }
                if (milli < 100) {
                    milli = '0' + milli;
                }
                if (milli < 10) {
                    milli = '0' + milli;
                }
                return '"' + year + '-' + month + '-' + day + 'T' +
                    hours + ':' + minutes + ':' + seconds + '.' + milli + 'Z"';
            }
            pairs = [];
            if ($.isArray(o)) {
                for (k = 0; k < o.length; k++) {
                    pairs.push($.toJSON(o[k]) || 'null');
                }
                return '[' + pairs.join(',') + ']';
            }
            if (typeof o === 'object') {
                for (k in o) {
                    if (hasOwn.call(o, k)) {
                        type = typeof k;
                        if (type === 'number') {
                            name = '"' + k + '"';
                        } else if (type === 'string') {
                            name = $.quoteString(k);
                        } else {
                            continue;
                        }
                        type = typeof o[k];
                        if (type !== 'function' && type !== 'undefined') {
                            val = $.toJSON(o[k]);
                            pairs.push(name + ':' + val);
                        }
                    }
                }
                return '{' + pairs.join(',') + '}';
            }
        };
        $.evalJSON = typeof JSON === 'object' && JSON.parse ? JSON.parse : function(str) {
            return eval('(' + str + ')');
        };
        $.secureEvalJSON = typeof JSON === 'object' && JSON.parse ? JSON.parse : function(str) {
            var filtered = str.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, '');
            if (/^[\],:{}\s]*$/.test(filtered)) {
                return eval('(' + str + ')');
            }
            throw new SyntaxError('Error parsing JSON, source is not valid.');
        };
        $.quoteString = function(str) {
            if (str.match(escape)) {
                return '"' + str.replace(escape, function(a) {
                    var c = meta[a];
                    if (typeof c === 'string') {
                        return c;
                    }
                    c = a.charCodeAt();
                    return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
                }) + '"';
            }
            return '"' + str + '"';
        };
    }(jQuery));
    (function(factory) {
            "use strict";
            if (typeof define === 'function' && define.amd) {
                define(['jquery'], factory);
            } else {
                factory((typeof(jQuery) != 'undefined') ? jQuery : window.Zepto);
            }
        }
        (function($) {
            "use strict";
            var feature = {};
            feature.fileapi = $("<input type='file'/>").get(0).files !== undefined;
            feature.formdata = window.FormData !== undefined;
            var hasProp = !!$.fn.prop;
            $.fn.attr2 = function() {
                if (!hasProp) {
                    return this.attr.apply(this, arguments);
                }
                var val = this.prop.apply(this, arguments);
                if ((val && val.jquery) || typeof val === 'string') {
                    return val;
                }
                return this.attr.apply(this, arguments);
            };
            $.fn.ajaxSubmit = function(options) {
                if (!this.length) {
                    log('ajaxSubmit: skipping submit process - no element selected');
                    return this;
                }
                var method, action, url, $form = this;
                if (typeof options == 'function') {
                    options = {
                        success: options
                    };
                } else if (options === undefined) {
                    options = {};
                }
                method = options.type || this.attr2('method');
                action = options.url || this.attr2('action');
                url = (typeof action === 'string') ? $.trim(action) : '';
                url = url || window.location.href || '';
                if (url) {
                    url = (url.match(/^([^#]+)/) || [])[1];
                }
                options = $.extend(true, {
                    url: url,
                    success: $.ajaxSettings.success,
                    type: method || $.ajaxSettings.type,
                    iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank'
                }, options);
                var veto = {};
                this.trigger('form-pre-serialize', [this, options, veto]);
                if (veto.veto) {
                    log('ajaxSubmit: submit vetoed via form-pre-serialize trigger');
                    return this;
                }
                if (options.beforeSerialize && options.beforeSerialize(this, options) === false) {
                    log('ajaxSubmit: submit aborted via beforeSerialize callback');
                    return this;
                }
                var traditional = options.traditional;
                if (traditional === undefined) {
                    traditional = $.ajaxSettings.traditional;
                }
                var elements = [];
                var qx, a = this.formToArray(options.semantic, elements);
                if (options.data) {
                    options.extraData = options.data;
                    qx = $.param(options.data, traditional);
                }
                if (options.beforeSubmit && options.beforeSubmit(a, this, options) === false) {
                    log('ajaxSubmit: submit aborted via beforeSubmit callback');
                    return this;
                }
                this.trigger('form-submit-validate', [a, this, options, veto]);
                if (veto.veto) {
                    log('ajaxSubmit: submit vetoed via form-submit-validate trigger');
                    return this;
                }
                var q = $.param(a, traditional);
                if (qx) {
                    q = (q ? (q + '&' + qx) : qx);
                }
                if (options.type.toUpperCase() == 'GET') {
                    options.url += (options.url.indexOf('?') >= 0 ? '&' : '?') + q;
                    options.data = null;
                } else {
                    options.data = q;
                }
                var callbacks = [];
                if (options.resetForm) {
                    callbacks.push(function() {
                        $form.resetForm();
                    });
                }
                if (options.clearForm) {
                    callbacks.push(function() {
                        $form.clearForm(options.includeHidden);
                    });
                }
                if (!options.dataType && options.target) {
                    var oldSuccess = options.success || function() {};
                    callbacks.push(function(data) {
                        var fn = options.replaceTarget ? 'replaceWith' : 'html';
                        $(options.target)[fn](data).each(oldSuccess, arguments);
                    });
                } else if (options.success) {
                    callbacks.push(options.success);
                }
                options.success = function(data, status, xhr) {
                    var context = options.context || this;
                    for (var i = 0, max = callbacks.length; i < max; i++) {
                        callbacks[i].apply(context, [data, status, xhr || $form, $form]);
                    }
                };
                if (options.error) {
                    var oldError = options.error;
                    options.error = function(xhr, status, error) {
                        var context = options.context || this;
                        oldError.apply(context, [xhr, status, error, $form]);
                    };
                }
                if (options.complete) {
                    var oldComplete = options.complete;
                    options.complete = function(xhr, status) {
                        var context = options.context || this;
                        oldComplete.apply(context, [xhr, status, $form]);
                    };
                }
                var fileInputs = $('input[type=file]:enabled', this).filter(function() {
                    return $(this).val() !== '';
                });
                var hasFileInputs = fileInputs.length > 0;
                var mp = 'multipart/form-data';
                var multipart = ($form.attr('enctype') == mp || $form.attr('encoding') == mp);
                var fileAPI = feature.fileapi && feature.formdata;
                log("fileAPI :" + fileAPI);
                var shouldUseFrame = (hasFileInputs || multipart) && !fileAPI;
                var jqxhr;
                if (options.iframe !== false && (options.iframe || shouldUseFrame)) {
                    if (options.closeKeepAlive) {
                        $.get(options.closeKeepAlive, function() {
                            jqxhr = fileUploadIframe(a);
                        });
                    } else {
                        jqxhr = fileUploadIframe(a);
                    }
                } else if ((hasFileInputs || multipart) && fileAPI) {
                    jqxhr = fileUploadXhr(a);
                } else {
                    jqxhr = $.ajax(options);
                }
                $form.removeData('jqxhr').data('jqxhr', jqxhr);
                for (var k = 0; k < elements.length; k++) {
                    elements[k] = null;
                }
                this.trigger('form-submit-notify', [this, options]);
                return this;

                function deepSerialize(extraData) {
                    var serialized = $.param(extraData, options.traditional).split('&');
                    var len = serialized.length;
                    var result = [];
                    var i, part;
                    for (i = 0; i < len; i++) {
                        serialized[i] = serialized[i].replace(/\+/g, ' ');
                        part = serialized[i].split('=');
                        result.push([decodeURIComponent(part[0]), decodeURIComponent(part[1])]);
                    }
                    return result;
                }

                function fileUploadXhr(a) {
                    var formdata = new FormData();
                    for (var i = 0; i < a.length; i++) {
                        formdata.append(a[i].name, a[i].value);
                    }
                    if (options.extraData) {
                        var serializedData = deepSerialize(options.extraData);
                        for (i = 0; i < serializedData.length; i++) {
                            if (serializedData[i]) {
                                formdata.append(serializedData[i][0], serializedData[i][1]);
                            }
                        }
                    }
                    options.data = null;
                    var s = $.extend(true, {}, $.ajaxSettings, options, {
                        contentType: false,
                        processData: false,
                        cache: false,
                        type: method || 'POST'
                    });
                    if (options.uploadProgress) {
                        s.xhr = function() {
                            var xhr = $.ajaxSettings.xhr();
                            if (xhr.upload) {
                                xhr.upload.addEventListener('progress', function(event) {
                                    var percent = 0;
                                    var position = event.loaded || event.position;
                                    var total = event.total;
                                    if (event.lengthComputable) {
                                        percent = Math.ceil(position / total * 100);
                                    }
                                    options.uploadProgress(event, position, total, percent);
                                }, false);
                            }
                            return xhr;
                        };
                    }
                    s.data = null;
                    var beforeSend = s.beforeSend;
                    s.beforeSend = function(xhr, o) {
                        if (options.formData) {
                            o.data = options.formData;
                        } else {
                            o.data = formdata;
                        }
                        if (beforeSend) {
                            beforeSend.call(this, xhr, o);
                        }
                    };
                    return $.ajax(s);
                }

                function fileUploadIframe(a) {
                    var form = $form[0],
                        el, i, s, g, id, $io, io, xhr, sub, n, timedOut, timeoutHandle;
                    var deferred = $.Deferred();
                    deferred.abort = function(status) {
                        xhr.abort(status);
                    };
                    if (a) {
                        for (i = 0; i < elements.length; i++) {
                            el = $(elements[i]);
                            if (hasProp) {
                                el.prop('disabled', false);
                            } else {
                                el.removeAttr('disabled');
                            }
                        }
                    }
                    s = $.extend(true, {}, $.ajaxSettings, options);
                    s.context = s.context || s;
                    id = 'jqFormIO' + (new Date().getTime());
                    if (s.iframeTarget) {
                        $io = $(s.iframeTarget);
                        n = $io.attr2('name');
                        if (!n) {
                            $io.attr2('name', id);
                        } else {
                            id = n;
                        }
                    } else {
                        $io = $('<iframe name="' + id + '" src="' + s.iframeSrc + '" />');
                        $io.css({
                            position: 'absolute',
                            top: '-1000px',
                            left: '-1000px'
                        });
                    }
                    io = $io[0];
                    xhr = {
                        aborted: 0,
                        responseText: null,
                        responseXML: null,
                        status: 0,
                        statusText: 'n/a',
                        getAllResponseHeaders: function() {},
                        getResponseHeader: function() {},
                        setRequestHeader: function() {},
                        abort: function(status) {
                            var e = (status === 'timeout' ? 'timeout' : 'aborted');
                            log('aborting upload... ' + e);
                            this.aborted = 1;
                            try {
                                if (io.contentWindow.document.execCommand) {
                                    io.contentWindow.document.execCommand('Stop');
                                }
                            } catch (ignore) {}
                            $io.attr('src', s.iframeSrc);
                            xhr.error = e;
                            if (s.error) {
                                s.error.call(s.context, xhr, e, status);
                            }
                            if (g) {
                                $.event.trigger("ajaxError", [xhr, s, e]);
                            }
                            if (s.complete) {
                                s.complete.call(s.context, xhr, e);
                            }
                        }
                    };
                    g = s.global;
                    if (g && 0 === $.active++) {
                        $.event.trigger("ajaxStart");
                    }
                    if (g) {
                        $.event.trigger("ajaxSend", [xhr, s]);
                    }
                    if (s.beforeSend && s.beforeSend.call(s.context, xhr, s) === false) {
                        if (s.global) {
                            $.active--;
                        }
                        deferred.reject();
                        return deferred;
                    }
                    if (xhr.aborted) {
                        deferred.reject();
                        return deferred;
                    }
                    sub = form.clk;
                    if (sub) {
                        n = sub.name;
                        if (n && !sub.disabled) {
                            s.extraData = s.extraData || {};
                            s.extraData[n] = sub.value;
                            if (sub.type == "image") {
                                s.extraData[n + '.x'] = form.clk_x;
                                s.extraData[n + '.y'] = form.clk_y;
                            }
                        }
                    }
                    var CLIENT_TIMEOUT_ABORT = 1;
                    var SERVER_ABORT = 2;

                    function getDoc(frame) {
                        var doc = null;
                        try {
                            if (frame.contentWindow) {
                                doc = frame.contentWindow.document;
                            }
                        } catch (err) {
                            log('cannot get iframe.contentWindow document: ' + err);
                        }
                        if (doc) {
                            return doc;
                        }
                        try {
                            doc = frame.contentDocument ? frame.contentDocument : frame.document;
                        } catch (err) {
                            log('cannot get iframe.contentDocument: ' + err);
                            doc = frame.document;
                        }
                        return doc;
                    }

                    var csrf_token = $('meta[name=csrf-token]').attr('content');
                    var csrf_param = $('meta[name=csrf-param]').attr('content');
                    if (csrf_param && csrf_token) {
                        s.extraData = s.extraData || {};
                        s.extraData[csrf_param] = csrf_token;
                    }

                    function doSubmit() {
                        var t = $form.attr2('target'),
                            a = $form.attr2('action'),
                            mp = 'multipart/form-data',
                            et = $form.attr('enctype') || $form.attr('encoding') || mp;
                        form.setAttribute('target', id);
                        if (!method || /post/i.test(method)) {
                            form.setAttribute('method', 'POST');
                        }
                        if (a != s.url) {
                            form.setAttribute('action', s.url);
                        }
                        if (!s.skipEncodingOverride && (!method || /post/i.test(method))) {
                            $form.attr({
                                encoding: 'multipart/form-data',
                                enctype: 'multipart/form-data'
                            });
                        }
                        if (s.timeout) {
                            timeoutHandle = setTimeout(function() {
                                timedOut = true;
                                cb(CLIENT_TIMEOUT_ABORT);
                            }, s.timeout);
                        }

                        function checkState() {
                            try {
                                var state = getDoc(io).readyState;
                                log('state = ' + state);
                                if (state && state.toLowerCase() == 'uninitialized') {
                                    setTimeout(checkState, 50);
                                }
                            } catch (e) {
                                log('Server abort: ', e, ' (', e.name, ')');
                                cb(SERVER_ABORT);
                                if (timeoutHandle) {
                                    clearTimeout(timeoutHandle);
                                }
                                timeoutHandle = undefined;
                            }
                        }

                        var extraInputs = [];
                        try {
                            if (s.extraData) {
                                for (var n in s.extraData) {
                                    if (s.extraData.hasOwnProperty(n)) {
                                        if ($.isPlainObject(s.extraData[n]) && s.extraData[n].hasOwnProperty('name') && s.extraData[n].hasOwnProperty('value')) {
                                            extraInputs.push($('<input type="hidden" name="' + s.extraData[n].name + '">').val(s.extraData[n].value).appendTo(form)[0]);
                                        } else {
                                            extraInputs.push($('<input type="hidden" name="' + n + '">').val(s.extraData[n]).appendTo(form)[0]);
                                        }
                                    }
                                }
                            }
                            if (!s.iframeTarget) {
                                $io.appendTo('body');
                            }
                            if (io.attachEvent) {
                                io.attachEvent('onload', cb);
                            } else {
                                io.addEventListener('load', cb, false);
                            }
                            setTimeout(checkState, 15);
                            try {
                                form.submit();
                            } catch (err) {
                                var submitFn = document.createElement('form').submit;
                                submitFn.apply(form);
                            }
                        } finally {
                            form.setAttribute('action', a);
                            form.setAttribute('enctype', et);
                            if (t) {
                                form.setAttribute('target', t);
                            } else {
                                $form.removeAttr('target');
                            }
                            $(extraInputs).remove();
                        }
                    }

                    if (s.forceSync) {
                        doSubmit();
                    } else {
                        setTimeout(doSubmit, 10);
                    }
                    var data, doc, domCheckCount = 50,
                        callbackProcessed;

                    function cb(e) {
                        if (xhr.aborted || callbackProcessed) {
                            return;
                        }
                        doc = getDoc(io);
                        if (!doc) {
                            log('cannot access response document');
                            e = SERVER_ABORT;
                        }
                        if (e === CLIENT_TIMEOUT_ABORT && xhr) {
                            xhr.abort('timeout');
                            deferred.reject(xhr, 'timeout');
                            return;
                        } else if (e == SERVER_ABORT && xhr) {
                            xhr.abort('server abort');
                            deferred.reject(xhr, 'error', 'server abort');
                            return;
                        }
                        if (!doc || doc.location.href == s.iframeSrc) {
                            if (!timedOut) {
                                return;
                            }
                        }
                        if (io.detachEvent) {
                            io.detachEvent('onload', cb);
                        } else {
                            io.removeEventListener('load', cb, false);
                        }
                        var status = 'success',
                            errMsg;
                        try {
                            if (timedOut) {
                                throw 'timeout';
                            }
                            var isXml = s.dataType == 'xml' || doc.XMLDocument || $.isXMLDoc(doc);
                            log('isXml=' + isXml);
                            if (!isXml && window.opera && (doc.body === null || !doc.body.innerHTML)) {
                                if (--domCheckCount) {
                                    log('requeing onLoad callback, DOM not available');
                                    setTimeout(cb, 250);
                                    return;
                                }
                            }
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            xhr.responseText = docRoot ? docRoot.innerHTML : null;
                            xhr.responseXML = doc.XMLDocument ? doc.XMLDocument : doc;
                            if (isXml) {
                                s.dataType = 'xml';
                            }
                            xhr.getResponseHeader = function(header) {
                                var headers = {
                                    'content-type': s.dataType
                                };
                                return headers[header.toLowerCase()];
                            };
                            if (docRoot) {
                                xhr.status = Number(docRoot.getAttribute('status')) || xhr.status;
                                xhr.statusText = docRoot.getAttribute('statusText') || xhr.statusText;
                            }
                            var dt = (s.dataType || '').toLowerCase();
                            var scr = /(json|script|text)/.test(dt);
                            if (scr || s.textarea) {
                                var ta = doc.getElementsByTagName('textarea')[0];
                                if (ta) {
                                    xhr.responseText = ta.value;
                                    xhr.status = Number(ta.getAttribute('status')) || xhr.status;
                                    xhr.statusText = ta.getAttribute('statusText') || xhr.statusText;
                                } else if (scr) {
                                    var pre = doc.getElementsByTagName('pre')[0];
                                    var b = doc.getElementsByTagName('body')[0];
                                    if (pre) {
                                        xhr.responseText = pre.textContent ? pre.textContent : pre.innerText;
                                    } else if (b) {
                                        xhr.responseText = b.textContent ? b.textContent : b.innerText;
                                    }
                                }
                            } else if (dt == 'xml' && !xhr.responseXML && xhr.responseText) {
                                xhr.responseXML = toXml(xhr.responseText);
                            }
                            try {
                                data = httpData(xhr, dt, s);
                            } catch (err) {
                                status = 'parsererror';
                                xhr.error = errMsg = (err || status);
                            }
                        } catch (err) {
                            log('error caught: ', err);
                            status = 'error';
                            xhr.error = errMsg = (err || status);
                        }
                        if (xhr.aborted) {
                            log('upload aborted');
                            status = null;
                        }
                        if (xhr.status) {
                            status = (xhr.status >= 200 && xhr.status < 300 || xhr.status === 304) ? 'success' : 'error';
                        }
                        if (status === 'success') {
                            if (s.success) {
                                s.success.call(s.context, data, 'success', xhr);
                            }
                            deferred.resolve(xhr.responseText, 'success', xhr);
                            if (g) {
                                $.event.trigger("ajaxSuccess", [xhr, s]);
                            }
                        } else if (status) {
                            if (errMsg === undefined) {
                                errMsg = xhr.statusText;
                            }
                            if (s.error) {
                                s.error.call(s.context, xhr, status, errMsg);
                            }
                            deferred.reject(xhr, 'error', errMsg);
                            if (g) {
                                $.event.trigger("ajaxError", [xhr, s, errMsg]);
                            }
                        }
                        if (g) {
                            $.event.trigger("ajaxComplete", [xhr, s]);
                        }
                        if (g && !--$.active) {
                            $.event.trigger("ajaxStop");
                        }
                        if (s.complete) {
                            s.complete.call(s.context, xhr, status);
                        }
                        callbackProcessed = true;
                        if (s.timeout) {
                            clearTimeout(timeoutHandle);
                        }
                        setTimeout(function() {
                            if (!s.iframeTarget) {
                                $io.remove();
                            } else {
                                $io.attr('src', s.iframeSrc);
                            }
                            xhr.responseXML = null;
                        }, 100);
                    }

                    var toXml = $.parseXML || function(s, doc) {
                        if (window.ActiveXObject) {
                            doc = new ActiveXObject('Microsoft.XMLDOM');
                            doc.async = 'false';
                            doc.loadXML(s);
                        } else {
                            doc = (new DOMParser()).parseFromString(s, 'text/xml');
                        }
                        return (doc && doc.documentElement && doc.documentElement.nodeName != 'parsererror') ? doc : null;
                    };
                    var parseJSON = $.parseJSON || function(s) {
                        return window['eval']('(' + s + ')');
                    };
                    var httpData = function(xhr, type, s) {
                        var ct = xhr.getResponseHeader('content-type') || '',
                            xml = type === 'xml' || !type && ct.indexOf('xml') >= 0,
                            data = xml ? xhr.responseXML : xhr.responseText;
                        if (xml && data.documentElement.nodeName === 'parsererror') {
                            if ($.error) {
                                $.error('parsererror');
                            }
                        }
                        if (s && s.dataFilter) {
                            data = s.dataFilter(data, type);
                        }
                        if (typeof data === 'string') {
                            if (type === 'json' || !type && ct.indexOf('json') >= 0) {
                                data = parseJSON(data);
                            } else if (type === "script" || !type && ct.indexOf("javascript") >= 0) {
                                $.globalEval(data);
                            }
                        }
                        return data;
                    };
                    return deferred;
                }
            };
            $.fn.ajaxForm = function(options) {
                options = options || {};
                options.delegation = options.delegation && $.isFunction($.fn.on);
                if (!options.delegation && this.length === 0) {
                    var o = {
                        s: this.selector,
                        c: this.context
                    };
                    if (!$.isReady && o.s) {
                        log('DOM not ready, queuing ajaxForm');
                        $(function() {
                            $(o.s, o.c).ajaxForm(options);
                        });
                        return this;
                    }
                    log('terminating; zero elements found by selector' + ($.isReady ? '' : ' (DOM not ready)'));
                    return this;
                }
                if (options.delegation) {
                    $(document).off('submit.form-plugin', this.selector, doAjaxSubmit).off('click.form-plugin', this.selector, captureSubmittingElement).on('submit.form-plugin', this.selector, options, doAjaxSubmit).on('click.form-plugin', this.selector, options, captureSubmittingElement);
                    return this;
                }
                return this.ajaxFormUnbind().bind('submit.form-plugin', options, doAjaxSubmit).bind('click.form-plugin', options, captureSubmittingElement);
            };

            function doAjaxSubmit(e) {
                var options = e.data;
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    $(e.target).ajaxSubmit(options);
                }
            }

            function captureSubmittingElement(e) {
                var target = e.target;
                var $el = $(target);
                if (!($el.is("[type=submit],[type=image]"))) {
                    var t = $el.closest('[type=submit]');
                    if (t.length === 0) {
                        return;
                    }
                    target = t[0];
                }
                var form = this;
                form.clk = target;
                if (target.type == 'image') {
                    if (e.offsetX !== undefined) {
                        form.clk_x = e.offsetX;
                        form.clk_y = e.offsetY;
                    } else if (typeof $.fn.offset == 'function') {
                        var offset = $el.offset();
                        form.clk_x = e.pageX - offset.left;
                        form.clk_y = e.pageY - offset.top;
                    } else {
                        form.clk_x = e.pageX - target.offsetLeft;
                        form.clk_y = e.pageY - target.offsetTop;
                    }
                }
                setTimeout(function() {
                    form.clk = form.clk_x = form.clk_y = null;
                }, 100);
            }

            $.fn.ajaxFormUnbind = function() {
                return this.unbind('submit.form-plugin click.form-plugin');
            };
            $.fn.formToArray = function(semantic, elements) {
                var a = [];
                if (this.length === 0) {
                    return a;
                }
                var form = this[0];
                var formId = this.attr('id');
                var els = semantic ? form.getElementsByTagName('*') : form.elements;
                var els2;
                if (els && !/MSIE [678]/.test(navigator.userAgent)) {
                    els = $(els).get();
                }
                if (formId) {
                    els2 = $(':input[form=' + formId + ']').get();
                    if (els2.length) {
                        els = (els || []).concat(els2);
                    }
                }
                if (!els || !els.length) {
                    return a;
                }
                var i, j, n, v, el, max, jmax;
                for (i = 0, max = els.length; i < max; i++) {
                    el = els[i];
                    n = el.name;
                    if (!n || el.disabled) {
                        continue;
                    }
                    if (semantic && form.clk && el.type == "image") {
                        if (form.clk == el) {
                            a.push({
                                name: n,
                                value: $(el).val(),
                                type: el.type
                            });
                            a.push({
                                name: n + '.x',
                                value: form.clk_x
                            }, {
                                name: n + '.y',
                                value: form.clk_y
                            });
                        }
                        continue;
                    }
                    v = $.fieldValue(el, true);
                    if (v && v.constructor == Array) {
                        if (elements) {
                            elements.push(el);
                        }
                        for (j = 0, jmax = v.length; j < jmax; j++) {
                            a.push({
                                name: n,
                                value: v[j]
                            });
                        }
                    } else if (feature.fileapi && el.type == 'file') {
                        if (elements) {
                            elements.push(el);
                        }
                        var files = el.files;
                        if (files.length) {
                            for (j = 0; j < files.length; j++) {
                                a.push({
                                    name: n,
                                    value: files[j],
                                    type: el.type
                                });
                            }
                        } else {
                            a.push({
                                name: n,
                                value: '',
                                type: el.type
                            });
                        }
                    } else if (v !== null && typeof v != 'undefined') {
                        if (elements) {
                            elements.push(el);
                        }
                        a.push({
                            name: n,
                            value: v,
                            type: el.type,
                            required: el.required
                        });
                    }
                }
                if (!semantic && form.clk) {
                    var $input = $(form.clk),
                        input = $input[0];
                    n = input.name;
                    if (n && !input.disabled && input.type == 'image') {
                        a.push({
                            name: n,
                            value: $input.val()
                        });
                        a.push({
                            name: n + '.x',
                            value: form.clk_x
                        }, {
                            name: n + '.y',
                            value: form.clk_y
                        });
                    }
                }
                return a;
            };
            $.fn.formSerialize = function(semantic) {
                return $.param(this.formToArray(semantic));
            };
            $.fn.fieldSerialize = function(successful) {
                var a = [];
                this.each(function() {
                    var n = this.name;
                    if (!n) {
                        return;
                    }
                    var v = $.fieldValue(this, successful);
                    if (v && v.constructor == Array) {
                        for (var i = 0, max = v.length; i < max; i++) {
                            a.push({
                                name: n,
                                value: v[i]
                            });
                        }
                    } else if (v !== null && typeof v != 'undefined') {
                        a.push({
                            name: this.name,
                            value: v
                        });
                    }
                });
                return $.param(a);
            };
            $.fn.fieldValue = function(successful) {
                for (var val = [], i = 0, max = this.length; i < max; i++) {
                    var el = this[i];
                    var v = $.fieldValue(el, successful);
                    if (v === null || typeof v == 'undefined' || (v.constructor == Array && !v.length)) {
                        continue;
                    }
                    if (v.constructor == Array) {
                        $.merge(val, v);
                    } else {
                        val.push(v);
                    }
                }
                return val;
            };
            $.fieldValue = function(el, successful) {
                var n = el.name,
                    t = el.type,
                    tag = el.tagName.toLowerCase();
                if (successful === undefined) {
                    successful = true;
                }
                if (successful && (!n || el.disabled || t == 'reset' || t == 'button' || (t == 'checkbox' || t == 'radio') && !el.checked || (t == 'submit' || t == 'image') && el.form && el.form.clk != el || tag == 'select' && el.selectedIndex == -1)) {
                    return null;
                }
                if (tag == 'select') {
                    var index = el.selectedIndex;
                    if (index < 0) {
                        return null;
                    }
                    var a = [],
                        ops = el.options;
                    var one = (t == 'select-one');
                    var max = (one ? index + 1 : ops.length);
                    for (var i = (one ? index : 0); i < max; i++) {
                        var op = ops[i];
                        if (op.selected) {
                            var v = op.value;
                            if (!v) {
                                v = (op.attributes && op.attributes.value && !(op.attributes.value.specified)) ? op.text : op.value;
                            }
                            if (one) {
                                return v;
                            }
                            a.push(v);
                        }
                    }
                    return a;
                }
                return $(el).val();
            };
            $.fn.clearForm = function(includeHidden) {
                return this.each(function() {
                    $('input,select,textarea', this).clearFields(includeHidden);
                });
            };
            $.fn.clearFields = $.fn.clearInputs = function(includeHidden) {
                var re = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
                return this.each(function() {
                    var t = this.type,
                        tag = this.tagName.toLowerCase();
                    if (re.test(t) || tag == 'textarea') {
                        this.value = '';
                    } else if (t == 'checkbox' || t == 'radio') {
                        this.checked = false;
                    } else if (tag == 'select') {
                        this.selectedIndex = -1;
                    } else if (t == "file") {
                        if (/MSIE/.test(navigator.userAgent)) {
                            $(this).replaceWith($(this).clone(true));
                        } else {
                            $(this).val('');
                        }
                    } else if (includeHidden) {
                        if ((includeHidden === true && /hidden/.test(t)) || (typeof includeHidden == 'string' && $(this).is(includeHidden))) {
                            this.value = '';
                        }
                    }
                });
            };
            $.fn.resetForm = function() {
                return this.each(function() {
                    if (typeof this.reset == 'function' || (typeof this.reset == 'object' && !this.reset.nodeType)) {
                        this.reset();
                    }
                });
            };
            $.fn.enable = function(b) {
                if (b === undefined) {
                    b = true;
                }
                return this.each(function() {
                    this.disabled = !b;
                });
            };
            $.fn.selected = function(select) {
                if (select === undefined) {
                    select = true;
                }
                return this.each(function() {
                    var t = this.type;
                    if (t == 'checkbox' || t == 'radio') {
                        this.checked = select;
                    } else if (this.tagName.toLowerCase() == 'option') {
                        var $sel = $(this).parent('select');
                        if (select && $sel[0] && $sel[0].type == 'select-one') {
                            $sel.find('option').selected(false);
                        }
                        this.selected = select;
                    }
                });
            };
            $.fn.ajaxSubmit.debug = false;

            function log() {
                if (!$.fn.ajaxSubmit.debug) {
                    return;
                }
                var msg = '[jquery.form] ' + Array.prototype.join.call(arguments, '');
                if (window.console && window.console.log) {
                    window.console.log(msg);
                } else if (window.opera && window.opera.postError) {
                    window.opera.postError(msg);
                }
            }
        }));
    jQuery.easing['jswing'] = jQuery.easing['swing'];
    jQuery.extend(jQuery.easing, {
        def: 'easeOutQuad',
        swing: function(x, t, b, c, d) {
            return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
        },
        easeInQuad: function(x, t, b, c, d) {
            return c * (t /= d) * t + b;
        },
        easeOutQuad: function(x, t, b, c, d) {
            return -c * (t /= d) * (t - 2) + b;
        },
        easeInOutQuad: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t + b;
            return -c / 2 * ((--t) * (t - 2) - 1) + b;
        },
        easeInCubic: function(x, t, b, c, d) {
            return c * (t /= d) * t * t + b;
        },
        easeOutCubic: function(x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t + 1) + b;
        },
        easeInOutCubic: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t + 2) + b;
        },
        easeInQuart: function(x, t, b, c, d) {
            return c * (t /= d) * t * t * t + b;
        },
        easeOutQuart: function(x, t, b, c, d) {
            return -c * ((t = t / d - 1) * t * t * t - 1) + b;
        },
        easeInOutQuart: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
            return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
        },
        easeInQuint: function(x, t, b, c, d) {
            return c * (t /= d) * t * t * t * t + b;
        },
        easeOutQuint: function(x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
        },
        easeInOutQuint: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
        },
        easeInSine: function(x, t, b, c, d) {
            return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
        },
        easeOutSine: function(x, t, b, c, d) {
            return c * Math.sin(t / d * (Math.PI / 2)) + b;
        },
        easeInOutSine: function(x, t, b, c, d) {
            return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
        },
        easeInExpo: function(x, t, b, c, d) {
            return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
        },
        easeOutExpo: function(x, t, b, c, d) {
            return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
        },
        easeInOutExpo: function(x, t, b, c, d) {
            if (t == 0) return b;
            if (t == d) return b + c;
            if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
            return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
        },
        easeInCirc: function(x, t, b, c, d) {
            return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
        },
        easeOutCirc: function(x, t, b, c, d) {
            return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
        },
        easeInOutCirc: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
            return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
        },
        easeInElastic: function(x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) return b;
            if ((t /= d) == 1) return b + c;
            if (!p) p = d * .3;
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        },
        easeOutElastic: function(x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) return b;
            if ((t /= d) == 1) return b + c;
            if (!p) p = d * .3;
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
        },
        easeInOutElastic: function(x, t, b, c, d) {
            var s = 1.70158;
            var p = 0;
            var a = c;
            if (t == 0) return b;
            if ((t /= d / 2) == 2) return b + c;
            if (!p) p = d * (.3 * 1.5);
            if (a < Math.abs(c)) {
                a = c;
                var s = p / 4;
            } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
            return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
        },
        easeInBack: function(x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c * (t /= d) * t * ((s + 1) * t - s) + b;
        },
        easeOutBack: function(x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
        },
        easeInOutBack: function(x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
            return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
        },
        easeInBounce: function(x, t, b, c, d) {
            return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
        },
        easeOutBounce: function(x, t, b, c, d) {
            if ((t /= d) < (1 / 2.75)) {
                return c * (7.5625 * t * t) + b;
            } else if (t < (2 / 2.75)) {
                return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
            } else if (t < (2.5 / 2.75)) {
                return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
            } else {
                return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
            }
        },
        easeInOutBounce: function(x, t, b, c, d) {
            if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
            return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
        }
    });
    (function(factory) {
        if (typeof define === 'function' && define.amd) {
            define(['jquery'], factory);
        } else {
            factory(jQuery);
        }
    }(function($) {
        var pluses = /\+/g;

        function encode(s) {
            return config.raw ? s : encodeURIComponent(s);
        }

        function decode(s) {
            return config.raw ? s : decodeURIComponent(s);
        }

        function stringifyCookieValue(value) {
            return encode(config.json ? JSON.stringify(value) : String(value));
        }

        function parseCookieValue(s) {
            if (s.indexOf('"') === 0) {
                s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
            }
            try {
                s = decodeURIComponent(s.replace(pluses, ' '));
            } catch (e) {
                return;
            }
            try {
                return config.json ? JSON.parse(s) : s;
            } catch (e) {}
        }

        function read(s, converter) {
            var value = config.raw ? s : parseCookieValue(s);
            return $.isFunction(converter) ? converter(value) : value;
        }

        var config = $.cookie = function(key, value, options) {
            if (value !== undefined && !$.isFunction(value)) {
                options = $.extend({}, config.defaults, options);
                if (typeof options.expires === 'number') {
                    var days = options.expires,
                        t = options.expires = new Date();
                    t.setDate(t.getDate() + days);
                }
                return (document.cookie = [encode(key), '=', stringifyCookieValue(value), options.expires ? '; expires=' + options.expires.toUTCString() : '', options.path ? '; path=' + options.path : '', options.domain ? '; domain=' + options.domain : '', options.secure ? '; secure' : ''].join(''));
            }
            var result = key ? undefined : {};
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            for (var i = 0, l = cookies.length; i < l; i++) {
                var parts = cookies[i].split('=');
                var name = decode(parts.shift());
                var cookie = parts.join('=');
                if (key && key === name) {
                    result = read(cookie, value);
                    break;
                }
                if (!key && (cookie = read(cookie)) !== undefined) {
                    result[name] = cookie;
                }
            }
            return result;
        };
        config.defaults = {};
        $.removeCookie = function(key, options) {
            if ($.cookie(key) !== undefined) {
                $.cookie(key, '', $.extend({}, options, {
                    expires: -1
                }));
                return true;
            }
            return false;
        };
    }));; + function($) {
        'use strict';
        var Tooltip = function(element, options) {
            this.type = this.options = this.enabled = this.timeout = this.hoverState = this.$element = null
            this.init('tooltip', element, options)
        }
        Tooltip.DEFAULTS = {
            animation: true,
            placement: 'top',
            selector: false,
            template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
            trigger: 'hover focus',
            title: '',
            delay: 0,
            html: false,
            container: false
        }
        Tooltip.prototype.init = function(type, element, options) {
            this.enabled = true
            this.type = type
            this.$element = $(element)
            this.options = this.getOptions(options)
            var triggers = this.options.trigger.split(' ')
            for (var i = triggers.length; i--;) {
                var trigger = triggers[i]
                if (trigger == 'click') {
                    this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
                } else if (trigger != 'manual') {
                    var eventIn = trigger == 'hover' ? 'mouseenter' : 'focusin'
                    var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'
                    this.$element.on(eventIn + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
                    this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
                }
            }
            this.options.selector ? (this._options = $.extend({}, this.options, {
                trigger: 'manual',
                selector: ''
            })) : this.fixTitle()
        }
        Tooltip.prototype.getDefaults = function() {
            return Tooltip.DEFAULTS
        }
        Tooltip.prototype.getOptions = function(options) {
            options = $.extend({}, this.getDefaults(), this.$element.data(), options)
            if (options.delay && typeof options.delay == 'number') {
                options.delay = {
                    show: options.delay,
                    hide: options.delay
                }
            }
            return options
        }
        Tooltip.prototype.getDelegateOptions = function() {
            var options = {}
            var defaults = this.getDefaults()
            this._options && $.each(this._options, function(key, value) {
                if (defaults[key] != value) options[key] = value
            })
            return options
        }
        Tooltip.prototype.enter = function(obj) {
            var self = obj instanceof this.constructor ? obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
            clearTimeout(self.timeout)
            self.hoverState = 'in'
            if (!self.options.delay || !self.options.delay.show) return self.show()
            self.timeout = setTimeout(function() {
                if (self.hoverState == 'in') self.show()
            }, self.options.delay.show)
        }
        Tooltip.prototype.leave = function(obj) {
            var self = obj instanceof this.constructor ? obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
            clearTimeout(self.timeout)
            self.hoverState = 'out'
            if (!self.options.delay || !self.options.delay.hide) return self.hide()
            self.timeout = setTimeout(function() {
                if (self.hoverState == 'out') self.hide()
            }, self.options.delay.hide)
        }
        Tooltip.prototype.show = function() {
            var e = $.Event('show.bs.' + this.type)
            if (this.hasContent() && this.enabled) {
                this.$element.trigger(e)
                if (e.isDefaultPrevented()) return
                var that = this;
                var $tip = this.tip()
                this.setContent()
                if (this.options.animation) $tip.addClass('fade')
                var placement = typeof this.options.placement == 'function' ? this.options.placement.call(this, $tip[0], this.$element[0]) : this.options.placement
                var autoToken = /\s?auto?\s?/i
                var autoPlace = autoToken.test(placement)
                if (autoPlace) placement = placement.replace(autoToken, '') || 'top'
                $tip.detach().css({
                    top: 0,
                    left: 0,
                    display: 'block'
                }).addClass(placement)
                this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
                var pos = this.getPosition()
                var actualWidth = $tip[0].offsetWidth
                var actualHeight = $tip[0].offsetHeight
                if (autoPlace) {
                    var $parent = this.$element.parent()
                    var orgPlacement = placement
                    var docScroll = document.documentElement.scrollTop || document.body.scrollTop
                    var parentWidth = this.options.container == 'body' ? window.innerWidth : $parent.outerWidth()
                    var parentHeight = this.options.container == 'body' ? window.innerHeight : $parent.outerHeight()
                    var parentLeft = this.options.container == 'body' ? 0 : $parent.offset().left
                    placement = placement == 'bottom' && pos.top + pos.height + actualHeight - docScroll > parentHeight ? 'top' : placement == 'top' && pos.top - docScroll - actualHeight < 0 ? 'bottom' : placement == 'right' && pos.right + actualWidth > parentWidth ? 'left' : placement == 'left' && pos.left - actualWidth < parentLeft ? 'right' : placement
                    $tip.removeClass(orgPlacement).addClass(placement)
                }
                var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)
                this.applyPlacement(calculatedOffset, placement)
                this.hoverState = null
                var complete = function() {
                    that.$element.trigger('shown.bs.' + that.type)
                }
                $.support.transition && this.$tip.hasClass('fade') ? $tip.one($.support.transition.end, complete).emulateTransitionEnd(150) : complete()
            }
        }
        Tooltip.prototype.applyPlacement = function(offset, placement) {
            var replace
            var $tip = this.tip()
            var width = $tip[0].offsetWidth
            var height = $tip[0].offsetHeight
            var marginTop = parseInt($tip.css('margin-top'), 10)
            var marginLeft = parseInt($tip.css('margin-left'), 10)
            if (isNaN(marginTop)) marginTop = 0
            if (isNaN(marginLeft)) marginLeft = 0
            offset.top = offset.top + marginTop
            offset.left = offset.left + marginLeft
            $.offset.setOffset($tip[0], $.extend({
                using: function(props) {
                    $tip.css({
                        top: Math.round(props.top),
                        left: Math.round(props.left)
                    })
                }
            }, offset), 0)
            $tip.addClass('in')
            var actualWidth = $tip[0].offsetWidth
            var actualHeight = $tip[0].offsetHeight
            if (placement == 'top' && actualHeight != height) {
                replace = true
                offset.top = offset.top + height - actualHeight
            }
            if (/bottom|top/.test(placement)) {
                var delta = 0
                if (offset.left < 0) {
                    delta = offset.left * -2
                    offset.left = 0
                    $tip.offset(offset)
                    actualWidth = $tip[0].offsetWidth
                    actualHeight = $tip[0].offsetHeight
                }
                this.replaceArrow(delta - width + actualWidth, actualWidth, 'left')
            } else {
                this.replaceArrow(actualHeight - height, actualHeight, 'top')
            }
            if (replace) $tip.offset(offset)
        }
        Tooltip.prototype.replaceArrow = function(delta, dimension, position) {
            this.arrow().css(position, delta ? (50 * (1 - delta / dimension) + '%') : '')
        }
        Tooltip.prototype.setContent = function() {
            var $tip = this.tip()
            var title = this.getTitle()
            $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
            $tip.removeClass('fade in top bottom left right')
        }
        Tooltip.prototype.hide = function() {
            var that = this
            var $tip = this.tip()
            var e = $.Event('hide.bs.' + this.type)

            function complete() {
                if (that.hoverState != 'in') $tip.detach()
                that.$element.trigger('hidden.bs.' + that.type)
            }

            this.$element.trigger(e)
            if (e.isDefaultPrevented()) return
            $tip.removeClass('in')
            $.support.transition && this.$tip.hasClass('fade') ? $tip.one($.support.transition.end, complete).emulateTransitionEnd(150) : complete()
            this.hoverState = null
            return this
        }
        Tooltip.prototype.fixTitle = function() {
            var $e = this.$element
            if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
                $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
            }
        }
        Tooltip.prototype.hasContent = function() {
            return this.getTitle()
        }
        Tooltip.prototype.getPosition = function() {
            var el = this.$element[0]
            return $.extend({}, (typeof el.getBoundingClientRect == 'function') ? el.getBoundingClientRect() : {
                width: el.offsetWidth,
                height: el.offsetHeight
            }, this.$element.offset())
        }
        Tooltip.prototype.getCalculatedOffset = function(placement, pos, actualWidth, actualHeight) {
            return placement == 'bottom' ? {
                top: pos.top + pos.height,
                left: pos.left + pos.width / 2 - actualWidth / 2
            } : placement == 'top' ? {
                top: pos.top - actualHeight,
                left: pos.left + pos.width / 2 - actualWidth / 2
            } : placement == 'left' ? {
                top: pos.top + pos.height / 2 - actualHeight / 2,
                left: pos.left - actualWidth
            } : {
                top: pos.top + pos.height / 2 - actualHeight / 2,
                left: pos.left + pos.width
            }
        }
        Tooltip.prototype.getTitle = function() {
            var title
            var $e = this.$element
            var o = this.options
            title = $e.attr('data-original-title') || (typeof o.title == 'function' ? o.title.call($e[0]) : o.title)
            return title
        }
        Tooltip.prototype.tip = function() {
            return this.$tip = this.$tip || $(this.options.template)
        }
        Tooltip.prototype.arrow = function() {
            return this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow')
        }
        Tooltip.prototype.validate = function() {
            if (!this.$element[0].parentNode) {
                this.hide()
                this.$element = null
                this.options = null
            }
        }
        Tooltip.prototype.enable = function() {
            this.enabled = true
        }
        Tooltip.prototype.disable = function() {
            this.enabled = false
        }
        Tooltip.prototype.toggleEnabled = function() {
            this.enabled = !this.enabled
        }
        Tooltip.prototype.toggle = function(e) {
            var self = e ? $(e.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type) : this
            self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
        }
        Tooltip.prototype.destroy = function() {
            clearTimeout(this.timeout)
            this.hide().$element.off('.' + this.type).removeData('bs.' + this.type)
        }
        var old = $.fn.tooltip
        $.fn.tooltip = function(option) {
            return this.each(function() {
                var $this = $(this)
                var data = $this.data('bs.tooltip')
                var options = typeof option == 'object' && option
                if (!data && option == 'destroy') return
                if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
                if (typeof option == 'string') data[option]()
            })
        }
        $.fn.tooltip.Constructor = Tooltip
        $.fn.tooltip.noConflict = function() {
            $.fn.tooltip = old
            return this
        }
    }(jQuery); + function($) {
        'use strict';
        var dismiss = '[data-dismiss="alert"]'
        var Alert = function(el) {
            $(el).on('click', dismiss, this.close)
        }
        Alert.prototype.close = function(e) {
            var $this = $(this)
            var selector = $this.attr('data-target')
            if (!selector) {
                selector = $this.attr('href')
                selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '')
            }
            var $parent = $(selector)
            if (e) e.preventDefault()
            if (!$parent.length) {
                $parent = $this.hasClass('alert') ? $this : $this.parent()
            }
            $parent.trigger(e = $.Event('close.bs.alert'))
            if (e.isDefaultPrevented()) return
            $parent.removeClass('in')

            function removeElement() {
                $parent.trigger('closed.bs.alert').remove()
            }

            $.support.transition && $parent.hasClass('fade') ? $parent.one($.support.transition.end, removeElement).emulateTransitionEnd(150) : removeElement()
        }
        var old = $.fn.alert
        $.fn.alert = function(option) {
            return this.each(function() {
                var $this = $(this)
                var data = $this.data('bs.alert')
                if (!data) $this.data('bs.alert', (data = new Alert(this)))
                if (typeof option == 'string') data[option].call($this)
            })
        }
        $.fn.alert.Constructor = Alert
        $.fn.alert.noConflict = function() {
            $.fn.alert = old
            return this
        }
        $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)
    }(jQuery); + function($) {
        'use strict';
        var Modal = function(element, options) {
            this.options = options
            this.$element = $(element)
            this.$backdrop = this.isShown = null
            if (this.options.remote) {
                this.$element.find('.modal-content').load(this.options.remote, $.proxy(function() {
                    this.$element.trigger('loaded.bs.modal')
                }, this))
            }
        }
        Modal.DEFAULTS = {
            backdrop: true,
            keyboard: true,
            show: true
        }
        Modal.prototype.toggle = function(_relatedTarget) {
            return this[!this.isShown ? 'show' : 'hide'](_relatedTarget)
        }
        Modal.prototype.show = function(_relatedTarget) {
            var that = this
            var e = $.Event('show.bs.modal', {
                relatedTarget: _relatedTarget
            })
            this.$element.trigger(e)
            if (this.isShown || e.isDefaultPrevented()) return
            this.isShown = true
            this.escape()
            this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))
            this.backdrop(function() {
                var transition = $.support.transition && that.$element.hasClass('fade')
                if (!that.$element.parent().length) {
                    that.$element.appendTo(document.body)
                }
                that.$element.show().scrollTop(0)
                if (transition) {
                    that.$element[0].offsetWidth
                }
                that.$element.addClass('in').attr('aria-hidden', false)
                that.enforceFocus()
                var e = $.Event('shown.bs.modal', {
                    relatedTarget: _relatedTarget
                })
                transition ? that.$element.find('.modal-dialog').one($.support.transition.end, function() {
                    that.$element.focus().trigger(e)
                }).emulateTransitionEnd(300) : that.$element.focus().trigger(e)
            })
        }
        Modal.prototype.hide = function(e) {
            if (e) e.preventDefault()
            e = $.Event('hide.bs.modal')
            this.$element.trigger(e)
            if (!this.isShown || e.isDefaultPrevented()) return
            this.isShown = false
            this.escape()
            $(document).off('focusin.bs.modal')
            this.$element.removeClass('in').attr('aria-hidden', true).off('click.dismiss.bs.modal')
            $.support.transition && this.$element.hasClass('fade') ? this.$element.one($.support.transition.end, $.proxy(this.hideModal, this)).emulateTransitionEnd(300) : this.hideModal()
        }
        Modal.prototype.enforceFocus = function() {
            $(document).off('focusin.bs.modal').on('focusin.bs.modal', $.proxy(function(e) {
                if (this.$element[0] !== e.target && !this.$element.has(e.target).length) {
                    this.$element.focus()
                }
            }, this))
        }
        Modal.prototype.escape = function() {
            if (this.isShown && this.options.keyboard) {
                this.$element.on('keyup.dismiss.bs.modal', $.proxy(function(e) {
                    e.which == 27 && this.hide()
                }, this))
            } else if (!this.isShown) {
                this.$element.off('keyup.dismiss.bs.modal')
            }
        }
        Modal.prototype.hideModal = function() {
            var that = this
            this.$element.hide()
            this.backdrop(function() {
                that.removeBackdrop()
                that.$element.trigger('hidden.bs.modal')
            })
        }
        Modal.prototype.removeBackdrop = function() {
            this.$backdrop && this.$backdrop.remove()
            this.$backdrop = null
        }
        Modal.prototype.backdrop = function(callback) {
            var animate = this.$element.hasClass('fade') ? 'fade' : ''
            if (this.isShown && this.options.backdrop) {
                var doAnimate = $.support.transition && animate
                this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />').appendTo(document.body)
                this.$element.on('click.dismiss.bs.modal', $.proxy(function(e) {
                    if (e.target !== e.currentTarget) return
                    this.options.backdrop == 'static' ? this.$element[0].focus.call(this.$element[0]) : this.hide.call(this)
                }, this))
                if (doAnimate) this.$backdrop[0].offsetWidth
                this.$backdrop.addClass('in')
                if (!callback) return
                doAnimate ? this.$backdrop.one($.support.transition.end, callback).emulateTransitionEnd(150) : callback()
            } else if (!this.isShown && this.$backdrop) {
                this.$backdrop.removeClass('in')
                $.support.transition && this.$element.hasClass('fade') ? this.$backdrop.one($.support.transition.end, callback).emulateTransitionEnd(150) : callback()
            } else if (callback) {
                callback()
            }
        }
        var old = $.fn.modal
        $.fn.modal = function(option, _relatedTarget) {
            return this.each(function() {
                var $this = $(this)
                var data = $this.data('bs.modal')
                var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)
                if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
                if (typeof option == 'string') data[option](_relatedTarget)
                else if (options.show) data.show(_relatedTarget)
            })
        }
        $.fn.modal.Constructor = Modal
        $.fn.modal.noConflict = function() {
            $.fn.modal = old
            return this
        }
        $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function(e) {
            var $this = $(this)
            var href = $this.attr('href')
            var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, '')))
            var option = $target.data('bs.modal') ? 'toggle' : $.extend({
                remote: !/#/.test(href) && href
            }, $target.data(), $this.data())
            if ($this.is('a')) e.preventDefault()
            $target.modal(option, this).one('hide', function() {
                $this.is(':visible') && $this.focus()
            })
        })
        $(document).on('show.bs.modal', '.modal', function() {
            $(document.body).addClass('modal-open')
        }).on('hidden.bs.modal', '.modal', function() {
            $(document.body).removeClass('modal-open')
        })
    }(jQuery); + function($) {
        'use strict';
        var Popover = function(element, options) {
            this.init('popover', element, options)
        }
        if (!$.fn.tooltip) throw new Error('Popover%20requires%20tooltip.html')
        Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
            placement: 'right',
            trigger: 'click',
            content: '',
            template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        })
        Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)
        Popover.prototype.constructor = Popover
        Popover.prototype.getDefaults = function() {
            return Popover.DEFAULTS
        }
        Popover.prototype.setContent = function() {
            var $tip = this.tip()
            var title = this.getTitle()
            var content = this.getContent()
            $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
            $tip.find('.popover-content')[this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'](content)
            $tip.removeClass('fade top bottom left right in')
            if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
        }
        Popover.prototype.hasContent = function() {
            return this.getTitle() || this.getContent()
        }
        Popover.prototype.getContent = function() {
            var $e = this.$element
            var o = this.options
            return $e.attr('data-content') || (typeof o.content == 'function' ? o.content.call($e[0]) : o.content)
        }
        Popover.prototype.arrow = function() {
            return this.$arrow = this.$arrow || this.tip().find('.arrow')
        }
        Popover.prototype.tip = function() {
            if (!this.$tip) this.$tip = $(this.options.template)
            return this.$tip
        }
        var old = $.fn.popover
        $.fn.popover = function(option) {
            return this.each(function() {
                var $this = $(this)
                var data = $this.data('bs.popover')
                var options = typeof option == 'object' && option
                if (!data && option == 'destroy') return
                if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
                if (typeof option == 'string') data[option]()
            })
        }
        $.fn.popover.Constructor = Popover
        $.fn.popover.noConflict = function() {
            $.fn.popover = old
            return this
        }
    }(jQuery);;;
    (function() {
        var undefined;
        var arrayPool = [],
            objectPool = [];
        var idCounter = 0;
        var keyPrefix = +new Date + '';
        var largeArraySize = 75;
        var maxPoolSize = 40;
        var whitespace = (' \t\x0B\f\xA0\ufeff' + '\n\r\u2028\u2029' + '\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000');
        var reEmptyStringLeading = /\b__p \+= '';/g,
            reEmptyStringMiddle = /\b(__p \+=) '' \+/g,
            reEmptyStringTrailing = /(__e\(.*?\)|\b__t\)) \+\n'';/g;
        var reEsTemplate = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g;
        var reFlags = /\w*$/;
        var reFuncName = /^\s*function[ \n\r\t]+\w/;
        var reInterpolate = /<%=([\s\S]+?)%>/g;
        var reLeadingSpacesAndZeros = RegExp('^[' + whitespace + ']*0+(?=.$)');
        var reNoMatch = /($^)/;
        var reThis = /\bthis\b/;
        var reUnescapedString = /['\n\r\t\u2028\u2029\\]/g;
        var contextProps = ['Array', 'Boolean', 'Date', 'Function', 'Math', 'Number', 'Object', 'RegExp', 'String', '_', 'attachEvent', 'clearTimeout', 'isFinite', 'isNaN', 'parseInt', 'setTimeout'];
        var templateCounter = 0;
        var argsClass = '[object Arguments]',
            arrayClass = '[object Array]',
            boolClass = '[object Boolean]',
            dateClass = '[object Date]',
            funcClass = '[object Function]',
            numberClass = '[object Number]',
            objectClass = '[object Object]',
            regexpClass = '[object RegExp]',
            stringClass = '[object String]';
        var cloneableClasses = {};
        cloneableClasses[funcClass] = false;
        cloneableClasses[argsClass] = cloneableClasses[arrayClass] = cloneableClasses[boolClass] = cloneableClasses[dateClass] = cloneableClasses[numberClass] = cloneableClasses[objectClass] = cloneableClasses[regexpClass] = cloneableClasses[stringClass] = true;
        var debounceOptions = {
            'leading': false,
            'maxWait': 0,
            'trailing': false
        };
        var descriptor = {
            'configurable': false,
            'enumerable': false,
            'value': null,
            'writable': false
        };
        var objectTypes = {
            'boolean': false,
            'function': true,
            'object': true,
            'number': false,
            'string': false,
            'undefined': false
        };
        var stringEscapes = {
            '\\': '\\',
            "'": "'",
            '\n': 'n',
            '\r': 'r',
            '\t': 't',
            '\u2028': 'u2028',
            '\u2029': 'u2029'
        };
        var root = (objectTypes[typeof window] && window) || this;
        var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;
        var freeModule = objectTypes[typeof module] && module && !module.nodeType && module;
        var moduleExports = freeModule && freeModule.exports === freeExports && freeExports;
        var freeGlobal = objectTypes[typeof global] && global;
        if (freeGlobal && (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal)) {
            root = freeGlobal;
        }

        function baseIndexOf(array, value, fromIndex) {
            var index = (fromIndex || 0) - 1,
                length = array ? array.length : 0;
            while (++index < length) {
                if (array[index] === value) {
                    return index;
                }
            }
            return -1;
        }

        function cacheIndexOf(cache, value) {
            var type = typeof value;
            cache = cache.cache;
            if (type == 'boolean' || value == null) {
                return cache[value] ? 0 : -1;
            }
            if (type != 'number' && type != 'string') {
                type = 'object';
            }
            var key = type == 'number' ? value : keyPrefix + value;
            cache = (cache = cache[type]) && cache[key];
            return type == 'object' ? (cache && baseIndexOf(cache, value) > -1 ? 0 : -1) : (cache ? 0 : -1);
        }

        function cachePush(value) {
            var cache = this.cache,
                type = typeof value;
            if (type == 'boolean' || value == null) {
                cache[value] = true;
            } else {
                if (type != 'number' && type != 'string') {
                    type = 'object';
                }
                var key = type == 'number' ? value : keyPrefix + value,
                    typeCache = cache[type] || (cache[type] = {});
                if (type == 'object') {
                    (typeCache[key] || (typeCache[key] = [])).push(value);
                } else {
                    typeCache[key] = true;
                }
            }
        }

        function charAtCallback(value) {
            return value.charCodeAt(0);
        }

        function compareAscending(a, b) {
            var ac = a.criteria,
                bc = b.criteria,
                index = -1,
                length = ac.length;
            while (++index < length) {
                var value = ac[index],
                    other = bc[index];
                if (value !== other) {
                    if (value > other || typeof value == 'undefined') {
                        return 1;
                    }
                    if (value < other || typeof other == 'undefined') {
                        return -1;
                    }
                }
            }
            return a.index - b.index;
        }

        function createCache(array) {
            var index = -1,
                length = array.length,
                first = array[0],
                mid = array[(length / 2) | 0],
                last = array[length - 1];
            if (first && typeof first == 'object' && mid && typeof mid == 'object' && last && typeof last == 'object') {
                return false;
            }
            var cache = getObject();
            cache['false'] = cache['null'] = cache['true'] = cache['undefined'] = false;
            var result = getObject();
            result.array = array;
            result.cache = cache;
            result.push = cachePush;
            while (++index < length) {
                result.push(array[index]);
            }
            return result;
        }

        function escapeStringChar(match) {
            return '\\' + stringEscapes[match];
        }

        function getArray() {
            return arrayPool.pop() || [];
        }

        function getObject() {
            return objectPool.pop() || {
                'array': null,
                'cache': null,
                'criteria': null,
                'false': false,
                'index': 0,
                'null': false,
                'number': null,
                'object': null,
                'push': null,
                'string': null,
                'true': false,
                'undefined': false,
                'value': null
            };
        }

        function releaseArray(array) {
            array.length = 0;
            if (arrayPool.length < maxPoolSize) {
                arrayPool.push(array);
            }
        }

        function releaseObject(object) {
            var cache = object.cache;
            if (cache) {
                releaseObject(cache);
            }
            object.array = object.cache = object.criteria = object.object = object.number = object.string = object.value = null;
            if (objectPool.length < maxPoolSize) {
                objectPool.push(object);
            }
        }

        function slice(array, start, end) {
            start || (start = 0);
            if (typeof end == 'undefined') {
                end = array ? array.length : 0;
            }
            var index = -1,
                length = end - start || 0,
                result = Array(length < 0 ? 0 : length);
            while (++index < length) {
                result[index] = array[start + index];
            }
            return result;
        }

        function runInContext(context) {
            context = context ? _.defaults(root.Object(), context, _.pick(root, contextProps)) : root;
            var Array = context.Array,
                Boolean = context.Boolean,
                Date = context.Date,
                Function = context.Function,
                Math = context.Math,
                Number = context.Number,
                Object = context.Object,
                RegExp = context.RegExp,
                String = context.String,
                TypeError = context.TypeError;
            var arrayRef = [];
            var objectProto = Object.prototype;
            var oldDash = context._;
            var toString = objectProto.toString;
            var reNative = RegExp('^' +
                String(toString).replace(/[.*+?^${}()|[\]\\]/g, '\\$&').replace(/toString| for [^\]]+/g, '.*?') + '$');
            var ceil = Math.ceil,
                clearTimeout = context.clearTimeout,
                floor = Math.floor,
                fnToString = Function.prototype.toString,
                getPrototypeOf = isNative(getPrototypeOf = Object.getPrototypeOf) && getPrototypeOf,
                hasOwnProperty = objectProto.hasOwnProperty,
                push = arrayRef.push,
                setTimeout = context.setTimeout,
                splice = arrayRef.splice,
                unshift = arrayRef.unshift;
            var defineProperty = (function() {
                try {
                    var o = {},
                        func = isNative(func = Object.defineProperty) && func,
                        result = func(o, o, o) && func;
                } catch (e) {}
                return result;
            }());
            var nativeCreate = isNative(nativeCreate = Object.create) && nativeCreate,
                nativeIsArray = isNative(nativeIsArray = Array.isArray) && nativeIsArray,
                nativeIsFinite = context.isFinite,
                nativeIsNaN = context.isNaN,
                nativeKeys = isNative(nativeKeys = Object.keys) && nativeKeys,
                nativeMax = Math.max,
                nativeMin = Math.min,
                nativeParseInt = context.parseInt,
                nativeRandom = Math.random;
            var ctorByClass = {};
            ctorByClass[arrayClass] = Array;
            ctorByClass[boolClass] = Boolean;
            ctorByClass[dateClass] = Date;
            ctorByClass[funcClass] = Function;
            ctorByClass[objectClass] = Object;
            ctorByClass[numberClass] = Number;
            ctorByClass[regexpClass] = RegExp;
            ctorByClass[stringClass] = String;

            function lodash(value) {
                return (value && typeof value == 'object' && !isArray(value) && hasOwnProperty.call(value, '__wrapped__')) ? value : new lodashWrapper(value);
            }

            function lodashWrapper(value, chainAll) {
                this.__chain__ = !!chainAll;
                this.__wrapped__ = value;
            }

            lodashWrapper.prototype = lodash.prototype;
            var support = lodash.support = {};
            support.funcDecomp = !isNative(context.WinRTError) && reThis.test(runInContext);
            support.funcNames = typeof Function.name == 'string';
            lodash.templateSettings = {
                'escape': /<%-([\s\S]+?)%>/g,
                'evaluate': /<%([\s\S]+?)%>/g,
                'interpolate': reInterpolate,
                'variable': '',
                'imports': {
                    '_': lodash
                }
            };

            function baseBind(bindData) {
                var func = bindData[0],
                    partialArgs = bindData[2],
                    thisArg = bindData[4];

                function bound() {
                    if (partialArgs) {
                        var args = slice(partialArgs);
                        push.apply(args, arguments);
                    }
                    if (this instanceof bound) {
                        var thisBinding = baseCreate(func.prototype),
                            result = func.apply(thisBinding, args || arguments);
                        return isObject(result) ? result : thisBinding;
                    }
                    return func.apply(thisArg, args || arguments);
                }

                setBindData(bound, bindData);
                return bound;
            }

            function baseClone(value, isDeep, callback, stackA, stackB) {
                if (callback) {
                    var result = callback(value);
                    if (typeof result != 'undefined') {
                        return result;
                    }
                }
                var isObj = isObject(value);
                if (isObj) {
                    var className = toString.call(value);
                    if (!cloneableClasses[className]) {
                        return value;
                    }
                    var ctor = ctorByClass[className];
                    switch (className) {
                        case boolClass:
                        case dateClass:
                            return new ctor(+value);
                        case numberClass:
                        case stringClass:
                            return new ctor(value);
                        case regexpClass:
                            result = ctor(value.source, reFlags.exec(value));
                            result.lastIndex = value.lastIndex;
                            return result;
                    }
                } else {
                    return value;
                }
                var isArr = isArray(value);
                if (isDeep) {
                    var initedStack = !stackA;
                    stackA || (stackA = getArray());
                    stackB || (stackB = getArray());
                    var length = stackA.length;
                    while (length--) {
                        if (stackA[length] == value) {
                            return stackB[length];
                        }
                    }
                    result = isArr ? ctor(value.length) : {};
                } else {
                    result = isArr ? slice(value) : assign({}, value);
                }
                if (isArr) {
                    if (hasOwnProperty.call(value, 'index')) {
                        result.index = value.index;
                    }
                    if (hasOwnProperty.call(value, 'input')) {
                        result.input = value.input;
                    }
                }
                if (!isDeep) {
                    return result;
                }
                stackA.push(value);
                stackB.push(result);
                (isArr ? forEach : forOwn)(value, function(objValue, key) {
                    result[key] = baseClone(objValue, isDeep, callback, stackA, stackB);
                });
                if (initedStack) {
                    releaseArray(stackA);
                    releaseArray(stackB);
                }
                return result;
            }

            function baseCreate(prototype, properties) {
                return isObject(prototype) ? nativeCreate(prototype) : {};
            }

            if (!nativeCreate) {
                baseCreate = (function() {
                    function Object() {}

                    return function(prototype) {
                        if (isObject(prototype)) {
                            Object.prototype = prototype;
                            var result = new Object;
                            Object.prototype = null;
                        }
                        return result || context.Object();
                    };
                }());
            }

            function baseCreateCallback(func, thisArg, argCount) {
                if (typeof func != 'function') {
                    return identity;
                }
                if (typeof thisArg == 'undefined' || !('prototype' in func)) {
                    return func;
                }
                var bindData = func.__bindData__;
                if (typeof bindData == 'undefined') {
                    if (support.funcNames) {
                        bindData = !func.name;
                    }
                    bindData = bindData || !support.funcDecomp;
                    if (!bindData) {
                        var source = fnToString.call(func);
                        if (!support.funcNames) {
                            bindData = !reFuncName.test(source);
                        }
                        if (!bindData) {
                            bindData = reThis.test(source);
                            setBindData(func, bindData);
                        }
                    }
                }
                if (bindData === false || (bindData !== true && bindData[1] & 1)) {
                    return func;
                }
                switch (argCount) {
                    case 1:
                        return function(value) {
                            return func.call(thisArg, value);
                        };
                    case 2:
                        return function(a, b) {
                            return func.call(thisArg, a, b);
                        };
                    case 3:
                        return function(value, index, collection) {
                            return func.call(thisArg, value, index, collection);
                        };
                    case 4:
                        return function(accumulator, value, index, collection) {
                            return func.call(thisArg, accumulator, value, index, collection);
                        };
                }
                return bind(func, thisArg);
            }

            function baseCreateWrapper(bindData) {
                var func = bindData[0],
                    bitmask = bindData[1],
                    partialArgs = bindData[2],
                    partialRightArgs = bindData[3],
                    thisArg = bindData[4],
                    arity = bindData[5];
                var isBind = bitmask & 1,
                    isBindKey = bitmask & 2,
                    isCurry = bitmask & 4,
                    isCurryBound = bitmask & 8,
                    key = func;

                function bound() {
                    var thisBinding = isBind ? thisArg : this;
                    if (partialArgs) {
                        var args = slice(partialArgs);
                        push.apply(args, arguments);
                    }
                    if (partialRightArgs || isCurry) {
                        args || (args = slice(arguments));
                        if (partialRightArgs) {
                            push.apply(args, partialRightArgs);
                        }
                        if (isCurry && args.length < arity) {
                            bitmask |= 16 & ~32;
                            return baseCreateWrapper([func, (isCurryBound ? bitmask : bitmask & ~3), args, null, thisArg, arity]);
                        }
                    }
                    args || (args = arguments);
                    if (isBindKey) {
                        func = thisBinding[key];
                    }
                    if (this instanceof bound) {
                        thisBinding = baseCreate(func.prototype);
                        var result = func.apply(thisBinding, args);
                        return isObject(result) ? result : thisBinding;
                    }
                    return func.apply(thisBinding, args);
                }

                setBindData(bound, bindData);
                return bound;
            }

            function baseDifference(array, values) {
                var index = -1,
                    indexOf = getIndexOf(),
                    length = array ? array.length : 0,
                    isLarge = length >= largeArraySize && indexOf === baseIndexOf,
                    result = [];
                if (isLarge) {
                    var cache = createCache(values);
                    if (cache) {
                        indexOf = cacheIndexOf;
                        values = cache;
                    } else {
                        isLarge = false;
                    }
                }
                while (++index < length) {
                    var value = array[index];
                    if (indexOf(values, value) < 0) {
                        result.push(value);
                    }
                }
                if (isLarge) {
                    releaseObject(values);
                }
                return result;
            }

            function baseFlatten(array, isShallow, isStrict, fromIndex) {
                var index = (fromIndex || 0) - 1,
                    length = array ? array.length : 0,
                    result = [];
                while (++index < length) {
                    var value = array[index];
                    if (value && typeof value == 'object' && typeof value.length == 'number' && (isArray(value) || isArguments(value))) {
                        if (!isShallow) {
                            value = baseFlatten(value, isShallow, isStrict);
                        }
                        var valIndex = -1,
                            valLength = value.length,
                            resIndex = result.length;
                        result.length += valLength;
                        while (++valIndex < valLength) {
                            result[resIndex++] = value[valIndex];
                        }
                    } else if (!isStrict) {
                        result.push(value);
                    }
                }
                return result;
            }

            function baseIsEqual(a, b, callback, isWhere, stackA, stackB) {
                if (callback) {
                    var result = callback(a, b);
                    if (typeof result != 'undefined') {
                        return !!result;
                    }
                }
                if (a === b) {
                    return a !== 0 || (1 / a == 1 / b);
                }
                var type = typeof a,
                    otherType = typeof b;
                if (a === a && !(a && objectTypes[type]) && !(b && objectTypes[otherType])) {
                    return false;
                }
                if (a == null || b == null) {
                    return a === b;
                }
                var className = toString.call(a),
                    otherClass = toString.call(b);
                if (className == argsClass) {
                    className = objectClass;
                }
                if (otherClass == argsClass) {
                    otherClass = objectClass;
                }
                if (className != otherClass) {
                    return false;
                }
                switch (className) {
                    case boolClass:
                    case dateClass:
                        return +a == +b;
                    case numberClass:
                        return (a != +a) ? b != +b : (a == 0 ? (1 / a == 1 / b) : a == +b);
                    case regexpClass:
                    case stringClass:
                        return a == String(b);
                }
                var isArr = className == arrayClass;
                if (!isArr) {
                    var aWrapped = hasOwnProperty.call(a, '__wrapped__'),
                        bWrapped = hasOwnProperty.call(b, '__wrapped__');
                    if (aWrapped || bWrapped) {
                        return baseIsEqual(aWrapped ? a.__wrapped__ : a, bWrapped ? b.__wrapped__ : b, callback, isWhere, stackA, stackB);
                    }
                    if (className != objectClass) {
                        return false;
                    }
                    var ctorA = a.constructor,
                        ctorB = b.constructor;
                    if (ctorA != ctorB && !(isFunction(ctorA) && ctorA instanceof ctorA && isFunction(ctorB) && ctorB instanceof ctorB) && ('constructor' in a && 'constructor' in b)) {
                        return false;
                    }
                }
                var initedStack = !stackA;
                stackA || (stackA = getArray());
                stackB || (stackB = getArray());
                var length = stackA.length;
                while (length--) {
                    if (stackA[length] == a) {
                        return stackB[length] == b;
                    }
                }
                var size = 0;
                result = true;
                stackA.push(a);
                stackB.push(b);
                if (isArr) {
                    length = a.length;
                    size = b.length;
                    result = size == length;
                    if (result || isWhere) {
                        while (size--) {
                            var index = length,
                                value = b[size];
                            if (isWhere) {
                                while (index--) {
                                    if ((result = baseIsEqual(a[index], value, callback, isWhere, stackA, stackB))) {
                                        break;
                                    }
                                }
                            } else if (!(result = baseIsEqual(a[size], value, callback, isWhere, stackA, stackB))) {
                                break;
                            }
                        }
                    }
                } else {
                    forIn(b, function(value, key, b) {
                        if (hasOwnProperty.call(b, key)) {
                            size++;
                            return (result = hasOwnProperty.call(a, key) && baseIsEqual(a[key], value, callback, isWhere, stackA, stackB));
                        }
                    });
                    if (result && !isWhere) {
                        forIn(a, function(value, key, a) {
                            if (hasOwnProperty.call(a, key)) {
                                return (result = --size > -1);
                            }
                        });
                    }
                }
                stackA.pop();
                stackB.pop();
                if (initedStack) {
                    releaseArray(stackA);
                    releaseArray(stackB);
                }
                return result;
            }

            function baseMerge(object, source, callback, stackA, stackB) {
                (isArray(source) ? forEach : forOwn)(source, function(source, key) {
                    var found, isArr, result = source,
                        value = object[key];
                    if (source && ((isArr = isArray(source)) || isPlainObject(source))) {
                        var stackLength = stackA.length;
                        while (stackLength--) {
                            if ((found = stackA[stackLength] == source)) {
                                value = stackB[stackLength];
                                break;
                            }
                        }
                        if (!found) {
                            var isShallow;
                            if (callback) {
                                result = callback(value, source);
                                if ((isShallow = typeof result != 'undefined')) {
                                    value = result;
                                }
                            }
                            if (!isShallow) {
                                value = isArr ? (isArray(value) ? value : []) : (isPlainObject(value) ? value : {});
                            }
                            stackA.push(source);
                            stackB.push(value);
                            if (!isShallow) {
                                baseMerge(value, source, callback, stackA, stackB);
                            }
                        }
                    } else {
                        if (callback) {
                            result = callback(value, source);
                            if (typeof result == 'undefined') {
                                result = source;
                            }
                        }
                        if (typeof result != 'undefined') {
                            value = result;
                        }
                    }
                    object[key] = value;
                });
            }

            function baseRandom(min, max) {
                return min + floor(nativeRandom() * (max - min + 1));
            }

            function baseUniq(array, isSorted, callback) {
                var index = -1,
                    indexOf = getIndexOf(),
                    length = array ? array.length : 0,
                    result = [];
                var isLarge = !isSorted && length >= largeArraySize && indexOf === baseIndexOf,
                    seen = (callback || isLarge) ? getArray() : result;
                if (isLarge) {
                    var cache = createCache(seen);
                    indexOf = cacheIndexOf;
                    seen = cache;
                }
                while (++index < length) {
                    var value = array[index],
                        computed = callback ? callback(value, index, array) : value;
                    if (isSorted ? !index || seen[seen.length - 1] !== computed : indexOf(seen, computed) < 0) {
                        if (callback || isLarge) {
                            seen.push(computed);
                        }
                        result.push(value);
                    }
                }
                if (isLarge) {
                    releaseArray(seen.array);
                    releaseObject(seen);
                } else if (callback) {
                    releaseArray(seen);
                }
                return result;
            }

            function createAggregator(setter) {
                return function(collection, callback, thisArg) {
                    var result = {};
                    callback = lodash.createCallback(callback, thisArg, 3);
                    var index = -1,
                        length = collection ? collection.length : 0;
                    if (typeof length == 'number') {
                        while (++index < length) {
                            var value = collection[index];
                            setter(result, value, callback(value, index, collection), collection);
                        }
                    } else {
                        forOwn(collection, function(value, key, collection) {
                            setter(result, value, callback(value, key, collection), collection);
                        });
                    }
                    return result;
                };
            }

            function createWrapper(func, bitmask, partialArgs, partialRightArgs, thisArg, arity) {
                var isBind = bitmask & 1,
                    isBindKey = bitmask & 2,
                    isCurry = bitmask & 4,
                    isCurryBound = bitmask & 8,
                    isPartial = bitmask & 16,
                    isPartialRight = bitmask & 32;
                if (!isBindKey && !isFunction(func)) {
                    throw new TypeError;
                }
                if (isPartial && !partialArgs.length) {
                    bitmask &= ~16;
                    isPartial = partialArgs = false;
                }
                if (isPartialRight && !partialRightArgs.length) {
                    bitmask &= ~32;
                    isPartialRight = partialRightArgs = false;
                }
                var bindData = func && func.__bindData__;
                if (bindData && bindData !== true) {
                    bindData = slice(bindData);
                    if (bindData[2]) {
                        bindData[2] = slice(bindData[2]);
                    }
                    if (bindData[3]) {
                        bindData[3] = slice(bindData[3]);
                    }
                    if (isBind && !(bindData[1] & 1)) {
                        bindData[4] = thisArg;
                    }
                    if (!isBind && bindData[1] & 1) {
                        bitmask |= 8;
                    }
                    if (isCurry && !(bindData[1] & 4)) {
                        bindData[5] = arity;
                    }
                    if (isPartial) {
                        push.apply(bindData[2] || (bindData[2] = []), partialArgs);
                    }
                    if (isPartialRight) {
                        unshift.apply(bindData[3] || (bindData[3] = []), partialRightArgs);
                    }
                    bindData[1] |= bitmask;
                    return createWrapper.apply(null, bindData);
                }
                var creater = (bitmask == 1 || bitmask === 17) ? baseBind : baseCreateWrapper;
                return creater([func, bitmask, partialArgs, partialRightArgs, thisArg, arity]);
            }

            function escapeHtmlChar(match) {
                return htmlEscapes[match];
            }

            function getIndexOf() {
                var result = (result = lodash.indexOf) === indexOf ? baseIndexOf : result;
                return result;
            }

            function isNative(value) {
                return typeof value == 'function' && reNative.test(value);
            }

            var setBindData = !defineProperty ? noop : function(func, value) {
                descriptor.value = value;
                defineProperty(func, '__bindData__', descriptor);
            };

            function shimIsPlainObject(value) {
                var ctor, result;
                if (!(value && toString.call(value) == objectClass) || (ctor = value.constructor, isFunction(ctor) && !(ctor instanceof ctor))) {
                    return false;
                }
                forIn(value, function(value, key) {
                    result = key;
                });
                return typeof result == 'undefined' || hasOwnProperty.call(value, result);
            }

            function unescapeHtmlChar(match) {
                return htmlUnescapes[match];
            }

            function isArguments(value) {
                return value && typeof value == 'object' && typeof value.length == 'number' && toString.call(value) == argsClass || false;
            }

            var isArray = nativeIsArray || function(value) {
                return value && typeof value == 'object' && typeof value.length == 'number' && toString.call(value) == arrayClass || false;
            };
            var shimKeys = function(object) {
                var index, iterable = object,
                    result = [];
                if (!iterable) return result;
                if (!(objectTypes[typeof object])) return result;
                for (index in iterable) {
                    if (hasOwnProperty.call(iterable, index)) {
                        result.push(index);
                    }
                }
                return result
            };
            var keys = !nativeKeys ? shimKeys : function(object) {
                if (!isObject(object)) {
                    return [];
                }
                return nativeKeys(object);
            };
            var htmlEscapes = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#39;'
            };
            var htmlUnescapes = invert(htmlEscapes);
            var reEscapedHtml = RegExp('(' + keys(htmlUnescapes).join('|') + ')', 'g'),
                reUnescapedHtml = RegExp('[' + keys(htmlEscapes).join('') + ']', 'g');
            var assign = function(object, source, guard) {
                var index, iterable = object,
                    result = iterable;
                if (!iterable) return result;
                var args = arguments,
                    argsIndex = 0,
                    argsLength = typeof guard == 'number' ? 2 : args.length;
                if (argsLength > 3 && typeof args[argsLength - 2] == 'function') {
                    var callback = baseCreateCallback(args[--argsLength - 1], args[argsLength--], 2);
                } else if (argsLength > 2 && typeof args[argsLength - 1] == 'function') {
                    callback = args[--argsLength];
                }
                while (++argsIndex < argsLength) {
                    iterable = args[argsIndex];
                    if (iterable && objectTypes[typeof iterable]) {
                        var ownIndex = -1,
                            ownProps = objectTypes[typeof iterable] && keys(iterable),
                            length = ownProps ? ownProps.length : 0;
                        while (++ownIndex < length) {
                            index = ownProps[ownIndex];
                            result[index] = callback ? callback(result[index], iterable[index]) : iterable[index];
                        }
                    }
                }
                return result
            };

            function clone(value, isDeep, callback, thisArg) {
                if (typeof isDeep != 'boolean' && isDeep != null) {
                    thisArg = callback;
                    callback = isDeep;
                    isDeep = false;
                }
                return baseClone(value, isDeep, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 1));
            }

            function cloneDeep(value, callback, thisArg) {
                return baseClone(value, true, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 1));
            }

            function create(prototype, properties) {
                var result = baseCreate(prototype);
                return properties ? assign(result, properties) : result;
            }

            var defaults = function(object, source, guard) {
                var index, iterable = object,
                    result = iterable;
                if (!iterable) return result;
                var args = arguments,
                    argsIndex = 0,
                    argsLength = typeof guard == 'number' ? 2 : args.length;
                while (++argsIndex < argsLength) {
                    iterable = args[argsIndex];
                    if (iterable && objectTypes[typeof iterable]) {
                        var ownIndex = -1,
                            ownProps = objectTypes[typeof iterable] && keys(iterable),
                            length = ownProps ? ownProps.length : 0;
                        while (++ownIndex < length) {
                            index = ownProps[ownIndex];
                            if (typeof result[index] == 'undefined') result[index] = iterable[index];
                        }
                    }
                }
                return result
            };

            function findKey(object, callback, thisArg) {
                var result;
                callback = lodash.createCallback(callback, thisArg, 3);
                forOwn(object, function(value, key, object) {
                    if (callback(value, key, object)) {
                        result = key;
                        return false;
                    }
                });
                return result;
            }

            function findLastKey(object, callback, thisArg) {
                var result;
                callback = lodash.createCallback(callback, thisArg, 3);
                forOwnRight(object, function(value, key, object) {
                    if (callback(value, key, object)) {
                        result = key;
                        return false;
                    }
                });
                return result;
            }

            var forIn = function(collection, callback, thisArg) {
                var index, iterable = collection,
                    result = iterable;
                if (!iterable) return result;
                if (!objectTypes[typeof iterable]) return result;
                callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
                for (index in iterable) {
                    if (callback(iterable[index], index, collection) === false) return result;
                }
                return result
            };

            function forInRight(object, callback, thisArg) {
                var pairs = [];
                forIn(object, function(value, key) {
                    pairs.push(key, value);
                });
                var length = pairs.length;
                callback = baseCreateCallback(callback, thisArg, 3);
                while (length--) {
                    if (callback(pairs[length--], pairs[length], object) === false) {
                        break;
                    }
                }
                return object;
            }

            var forOwn = function(collection, callback, thisArg) {
                var index, iterable = collection,
                    result = iterable;
                if (!iterable) return result;
                if (!objectTypes[typeof iterable]) return result;
                callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
                var ownIndex = -1,
                    ownProps = objectTypes[typeof iterable] && keys(iterable),
                    length = ownProps ? ownProps.length : 0;
                while (++ownIndex < length) {
                    index = ownProps[ownIndex];
                    if (callback(iterable[index], index, collection) === false) return result;
                }
                return result
            };

            function forOwnRight(object, callback, thisArg) {
                var props = keys(object),
                    length = props.length;
                callback = baseCreateCallback(callback, thisArg, 3);
                while (length--) {
                    var key = props[length];
                    if (callback(object[key], key, object) === false) {
                        break;
                    }
                }
                return object;
            }

            function functions(object) {
                var result = [];
                forIn(object, function(value, key) {
                    if (isFunction(value)) {
                        result.push(key);
                    }
                });
                return result.sort();
            }

            function has(object, key) {
                return object ? hasOwnProperty.call(object, key) : false;
            }

            function invert(object) {
                var index = -1,
                    props = keys(object),
                    length = props.length,
                    result = {};
                while (++index < length) {
                    var key = props[index];
                    result[object[key]] = key;
                }
                return result;
            }

            function isBoolean(value) {
                return value === true || value === false || value && typeof value == 'object' && toString.call(value) == boolClass || false;
            }

            function isDate(value) {
                return value && typeof value == 'object' && toString.call(value) == dateClass || false;
            }

            function isElement(value) {
                return value && value.nodeType === 1 || false;
            }

            function isEmpty(value) {
                var result = true;
                if (!value) {
                    return result;
                }
                var className = toString.call(value),
                    length = value.length;
                if ((className == arrayClass || className == stringClass || className == argsClass) || (className == objectClass && typeof length == 'number' && isFunction(value.splice))) {
                    return !length;
                }
                forOwn(value, function() {
                    return (result = false);
                });
                return result;
            }

            function isEqual(a, b, callback, thisArg) {
                return baseIsEqual(a, b, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 2));
            }

            function isFinite(value) {
                return nativeIsFinite(value) && !nativeIsNaN(parseFloat(value));
            }

            function isFunction(value) {
                return typeof value == 'function';
            }

            function isObject(value) {
                return !!(value && objectTypes[typeof value]);
            }

            function isNaN(value) {
                return isNumber(value) && value != +value;
            }

            function isNull(value) {
                return value === null;
            }

            function isNumber(value) {
                return typeof value == 'number' || value && typeof value == 'object' && toString.call(value) == numberClass || false;
            }

            var isPlainObject = !getPrototypeOf ? shimIsPlainObject : function(value) {
                if (!(value && toString.call(value) == objectClass)) {
                    return false;
                }
                var valueOf = value.valueOf,
                    objProto = isNative(valueOf) && (objProto = getPrototypeOf(valueOf)) && getPrototypeOf(objProto);
                return objProto ? (value == objProto || getPrototypeOf(value) == objProto) : shimIsPlainObject(value);
            };

            function isRegExp(value) {
                return value && typeof value == 'object' && toString.call(value) == regexpClass || false;
            }

            function isString(value) {
                return typeof value == 'string' || value && typeof value == 'object' && toString.call(value) == stringClass || false;
            }

            function isUndefined(value) {
                return typeof value == 'undefined';
            }

            function mapValues(object, callback, thisArg) {
                var result = {};
                callback = lodash.createCallback(callback, thisArg, 3);
                forOwn(object, function(value, key, object) {
                    result[key] = callback(value, key, object);
                });
                return result;
            }

            function merge(object) {
                var args = arguments,
                    length = 2;
                if (!isObject(object)) {
                    return object;
                }
                if (typeof args[2] != 'number') {
                    length = args.length;
                }
                if (length > 3 && typeof args[length - 2] == 'function') {
                    var callback = baseCreateCallback(args[--length - 1], args[length--], 2);
                } else if (length > 2 && typeof args[length - 1] == 'function') {
                    callback = args[--length];
                }
                var sources = slice(arguments, 1, length),
                    index = -1,
                    stackA = getArray(),
                    stackB = getArray();
                while (++index < length) {
                    baseMerge(object, sources[index], callback, stackA, stackB);
                }
                releaseArray(stackA);
                releaseArray(stackB);
                return object;
            }

            function omit(object, callback, thisArg) {
                var result = {};
                if (typeof callback != 'function') {
                    var props = [];
                    forIn(object, function(value, key) {
                        props.push(key);
                    });
                    props = baseDifference(props, baseFlatten(arguments, true, false, 1));
                    var index = -1,
                        length = props.length;
                    while (++index < length) {
                        var key = props[index];
                        result[key] = object[key];
                    }
                } else {
                    callback = lodash.createCallback(callback, thisArg, 3);
                    forIn(object, function(value, key, object) {
                        if (!callback(value, key, object)) {
                            result[key] = value;
                        }
                    });
                }
                return result;
            }

            function pairs(object) {
                var index = -1,
                    props = keys(object),
                    length = props.length,
                    result = Array(length);
                while (++index < length) {
                    var key = props[index];
                    result[index] = [key, object[key]];
                }
                return result;
            }

            function pick(object, callback, thisArg) {
                var result = {};
                if (typeof callback != 'function') {
                    var index = -1,
                        props = baseFlatten(arguments, true, false, 1),
                        length = isObject(object) ? props.length : 0;
                    while (++index < length) {
                        var key = props[index];
                        if (key in object) {
                            result[key] = object[key];
                        }
                    }
                } else {
                    callback = lodash.createCallback(callback, thisArg, 3);
                    forIn(object, function(value, key, object) {
                        if (callback(value, key, object)) {
                            result[key] = value;
                        }
                    });
                }
                return result;
            }

            function transform(object, callback, accumulator, thisArg) {
                var isArr = isArray(object);
                if (accumulator == null) {
                    if (isArr) {
                        accumulator = [];
                    } else {
                        var ctor = object && object.constructor,
                            proto = ctor && ctor.prototype;
                        accumulator = baseCreate(proto);
                    }
                }
                if (callback) {
                    callback = lodash.createCallback(callback, thisArg, 4);
                    (isArr ? forEach : forOwn)(object, function(value, index, object) {
                        return callback(accumulator, value, index, object);
                    });
                }
                return accumulator;
            }

            function values(object) {
                var index = -1,
                    props = keys(object),
                    length = props.length,
                    result = Array(length);
                while (++index < length) {
                    result[index] = object[props[index]];
                }
                return result;
            }

            function at(collection) {
                var args = arguments,
                    index = -1,
                    props = baseFlatten(args, true, false, 1),
                    length = (args[2] && args[2][args[1]] === collection) ? 1 : props.length,
                    result = Array(length);
                while (++index < length) {
                    result[index] = collection[props[index]];
                }
                return result;
            }

            function contains(collection, target, fromIndex) {
                var index = -1,
                    indexOf = getIndexOf(),
                    length = collection ? collection.length : 0,
                    result = false;
                fromIndex = (fromIndex < 0 ? nativeMax(0, length + fromIndex) : fromIndex) || 0;
                if (isArray(collection)) {
                    result = indexOf(collection, target, fromIndex) > -1;
                } else if (typeof length == 'number') {
                    result = (isString(collection) ? collection.indexOf(target, fromIndex) : indexOf(collection, target, fromIndex)) > -1;
                } else {
                    forOwn(collection, function(value) {
                        if (++index >= fromIndex) {
                            return !(result = value === target);
                        }
                    });
                }
                return result;
            }

            var countBy = createAggregator(function(result, value, key) {
                (hasOwnProperty.call(result, key) ? result[key]++ : result[key] = 1);
            });

            function every(collection, callback, thisArg) {
                var result = true;
                callback = lodash.createCallback(callback, thisArg, 3);
                var index = -1,
                    length = collection ? collection.length : 0;
                if (typeof length == 'number') {
                    while (++index < length) {
                        if (!(result = !!callback(collection[index], index, collection))) {
                            break;
                        }
                    }
                } else {
                    forOwn(collection, function(value, index, collection) {
                        return (result = !!callback(value, index, collection));
                    });
                }
                return result;
            }

            function filter(collection, callback, thisArg) {
                var result = [];
                callback = lodash.createCallback(callback, thisArg, 3);
                var index = -1,
                    length = collection ? collection.length : 0;
                if (typeof length == 'number') {
                    while (++index < length) {
                        var value = collection[index];
                        if (callback(value, index, collection)) {
                            result.push(value);
                        }
                    }
                } else {
                    forOwn(collection, function(value, index, collection) {
                        if (callback(value, index, collection)) {
                            result.push(value);
                        }
                    });
                }
                return result;
            }

            function find(collection, callback, thisArg) {
                callback = lodash.createCallback(callback, thisArg, 3);
                var index = -1,
                    length = collection ? collection.length : 0;
                if (typeof length == 'number') {
                    while (++index < length) {
                        var value = collection[index];
                        if (callback(value, index, collection)) {
                            return value;
                        }
                    }
                } else {
                    var result;
                    forOwn(collection, function(value, index, collection) {
                        if (callback(value, index, collection)) {
                            result = value;
                            return false;
                        }
                    });
                    return result;
                }
            }

            function findLast(collection, callback, thisArg) {
                var result;
                callback = lodash.createCallback(callback, thisArg, 3);
                forEachRight(collection, function(value, index, collection) {
                    if (callback(value, index, collection)) {
                        result = value;
                        return false;
                    }
                });
                return result;
            }

            function forEach(collection, callback, thisArg) {
                var index = -1,
                    length = collection ? collection.length : 0;
                callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
                if (typeof length == 'number') {
                    while (++index < length) {
                        if (callback(collection[index], index, collection) === false) {
                            break;
                        }
                    }
                } else {
                    forOwn(collection, callback);
                }
                return collection;
            }

            function forEachRight(collection, callback, thisArg) {
                var length = collection ? collection.length : 0;
                callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
                if (typeof length == 'number') {
                    while (length--) {
                        if (callback(collection[length], length, collection) === false) {
                            break;
                        }
                    }
                } else {
                    var props = keys(collection);
                    length = props.length;
                    forOwn(collection, function(value, key, collection) {
                        key = props ? props[--length] : --length;
                        return callback(collection[key], key, collection);
                    });
                }
                return collection;
            }

            var groupBy = createAggregator(function(result, value, key) {
                (hasOwnProperty.call(result, key) ? result[key] : result[key] = []).push(value);
            });
            var indexBy = createAggregator(function(result, value, key) {
                result[key] = value;
            });

            function invoke(collection, methodName) {
                var args = slice(arguments, 2),
                    index = -1,
                    isFunc = typeof methodName == 'function',
                    length = collection ? collection.length : 0,
                    result = Array(typeof length == 'number' ? length : 0);
                forEach(collection, function(value) {
                    result[++index] = (isFunc ? methodName : value[methodName]).apply(value, args);
                });
                return result;
            }

            function map(collection, callback, thisArg) {
                var index = -1,
                    length = collection ? collection.length : 0;
                callback = lodash.createCallback(callback, thisArg, 3);
                if (typeof length == 'number') {
                    var result = Array(length);
                    while (++index < length) {
                        result[index] = callback(collection[index], index, collection);
                    }
                } else {
                    result = [];
                    forOwn(collection, function(value, key, collection) {
                        result[++index] = callback(value, key, collection);
                    });
                }
                return result;
            }

            function max(collection, callback, thisArg) {
                var computed = -Infinity,
                    result = computed;
                if (typeof callback != 'function' && thisArg && thisArg[callback] === collection) {
                    callback = null;
                }
                if (callback == null && isArray(collection)) {
                    var index = -1,
                        length = collection.length;
                    while (++index < length) {
                        var value = collection[index];
                        if (value > result) {
                            result = value;
                        }
                    }
                } else {
                    callback = (callback == null && isString(collection)) ? charAtCallback : lodash.createCallback(callback, thisArg, 3);
                    forEach(collection, function(value, index, collection) {
                        var current = callback(value, index, collection);
                        if (current > computed) {
                            computed = current;
                            result = value;
                        }
                    });
                }
                return result;
            }

            function min(collection, callback, thisArg) {
                var computed = Infinity,
                    result = computed;
                if (typeof callback != 'function' && thisArg && thisArg[callback] === collection) {
                    callback = null;
                }
                if (callback == null && isArray(collection)) {
                    var index = -1,
                        length = collection.length;
                    while (++index < length) {
                        var value = collection[index];
                        if (value < result) {
                            result = value;
                        }
                    }
                } else {
                    callback = (callback == null && isString(collection)) ? charAtCallback : lodash.createCallback(callback, thisArg, 3);
                    forEach(collection, function(value, index, collection) {
                        var current = callback(value, index, collection);
                        if (current < computed) {
                            computed = current;
                            result = value;
                        }
                    });
                }
                return result;
            }

            var pluck = map;

            function reduce(collection, callback, accumulator, thisArg) {
                if (!collection) return accumulator;
                var noaccum = arguments.length < 3;
                callback = lodash.createCallback(callback, thisArg, 4);
                var index = -1,
                    length = collection.length;
                if (typeof length == 'number') {
                    if (noaccum) {
                        accumulator = collection[++index];
                    }
                    while (++index < length) {
                        accumulator = callback(accumulator, collection[index], index, collection);
                    }
                } else {
                    forOwn(collection, function(value, index, collection) {
                        accumulator = noaccum ? (noaccum = false, value) : callback(accumulator, value, index, collection)
                    });
                }
                return accumulator;
            }

            function reduceRight(collection, callback, accumulator, thisArg) {
                var noaccum = arguments.length < 3;
                callback = lodash.createCallback(callback, thisArg, 4);
                forEachRight(collection, function(value, index, collection) {
                    accumulator = noaccum ? (noaccum = false, value) : callback(accumulator, value, index, collection);
                });
                return accumulator;
            }

            function reject(collection, callback, thisArg) {
                callback = lodash.createCallback(callback, thisArg, 3);
                return filter(collection, function(value, index, collection) {
                    return !callback(value, index, collection);
                });
            }

            function sample(collection, n, guard) {
                if (collection && typeof collection.length != 'number') {
                    collection = values(collection);
                }
                if (n == null || guard) {
                    return collection ? collection[baseRandom(0, collection.length - 1)] : undefined;
                }
                var result = shuffle(collection);
                result.length = nativeMin(nativeMax(0, n), result.length);
                return result;
            }

            function shuffle(collection) {
                var index = -1,
                    length = collection ? collection.length : 0,
                    result = Array(typeof length == 'number' ? length : 0);
                forEach(collection, function(value) {
                    var rand = baseRandom(0, ++index);
                    result[index] = result[rand];
                    result[rand] = value;
                });
                return result;
            }

            function size(collection) {
                var length = collection ? collection.length : 0;
                return typeof length == 'number' ? length : keys(collection).length;
            }

            function some(collection, callback, thisArg) {
                var result;
                callback = lodash.createCallback(callback, thisArg, 3);
                var index = -1,
                    length = collection ? collection.length : 0;
                if (typeof length == 'number') {
                    while (++index < length) {
                        if ((result = callback(collection[index], index, collection))) {
                            break;
                        }
                    }
                } else {
                    forOwn(collection, function(value, index, collection) {
                        return !(result = callback(value, index, collection));
                    });
                }
                return !!result;
            }

            function sortBy(collection, callback, thisArg) {
                var index = -1,
                    isArr = isArray(callback),
                    length = collection ? collection.length : 0,
                    result = Array(typeof length == 'number' ? length : 0);
                if (!isArr) {
                    callback = lodash.createCallback(callback, thisArg, 3);
                }
                forEach(collection, function(value, key, collection) {
                    var object = result[++index] = getObject();
                    if (isArr) {
                        object.criteria = map(callback, function(key) {
                            return value[key];
                        });
                    } else {
                        (object.criteria = getArray())[0] = callback(value, key, collection);
                    }
                    object.index = index;
                    object.value = value;
                });
                length = result.length;
                result.sort(compareAscending);
                while (length--) {
                    var object = result[length];
                    result[length] = object.value;
                    if (!isArr) {
                        releaseArray(object.criteria);
                    }
                    releaseObject(object);
                }
                return result;
            }

            function toArray(collection) {
                if (collection && typeof collection.length == 'number') {
                    return slice(collection);
                }
                return values(collection);
            }

            var where = filter;

            function compact(array) {
                var index = -1,
                    length = array ? array.length : 0,
                    result = [];
                while (++index < length) {
                    var value = array[index];
                    if (value) {
                        result.push(value);
                    }
                }
                return result;
            }

            function difference(array) {
                return baseDifference(array, baseFlatten(arguments, true, true, 1));
            }

            function findIndex(array, callback, thisArg) {
                var index = -1,
                    length = array ? array.length : 0;
                callback = lodash.createCallback(callback, thisArg, 3);
                while (++index < length) {
                    if (callback(array[index], index, array)) {
                        return index;
                    }
                }
                return -1;
            }

            function findLastIndex(array, callback, thisArg) {
                var length = array ? array.length : 0;
                callback = lodash.createCallback(callback, thisArg, 3);
                while (length--) {
                    if (callback(array[length], length, array)) {
                        return length;
                    }
                }
                return -1;
            }

            function first(array, callback, thisArg) {
                var n = 0,
                    length = array ? array.length : 0;
                if (typeof callback != 'number' && callback != null) {
                    var index = -1;
                    callback = lodash.createCallback(callback, thisArg, 3);
                    while (++index < length && callback(array[index], index, array)) {
                        n++;
                    }
                } else {
                    n = callback;
                    if (n == null || thisArg) {
                        return array ? array[0] : undefined;
                    }
                }
                return slice(array, 0, nativeMin(nativeMax(0, n), length));
            }

            function flatten(array, isShallow, callback, thisArg) {
                if (typeof isShallow != 'boolean' && isShallow != null) {
                    thisArg = callback;
                    callback = (typeof isShallow != 'function' && thisArg && thisArg[isShallow] === array) ? null : isShallow;
                    isShallow = false;
                }
                if (callback != null) {
                    array = map(array, callback, thisArg);
                }
                return baseFlatten(array, isShallow);
            }

            function indexOf(array, value, fromIndex) {
                if (typeof fromIndex == 'number') {
                    var length = array ? array.length : 0;
                    fromIndex = (fromIndex < 0 ? nativeMax(0, length + fromIndex) : fromIndex || 0);
                } else if (fromIndex) {
                    var index = sortedIndex(array, value);
                    return array[index] === value ? index : -1;
                }
                return baseIndexOf(array, value, fromIndex);
            }

            function initial(array, callback, thisArg) {
                var n = 0,
                    length = array ? array.length : 0;
                if (typeof callback != 'number' && callback != null) {
                    var index = length;
                    callback = lodash.createCallback(callback, thisArg, 3);
                    while (index-- && callback(array[index], index, array)) {
                        n++;
                    }
                } else {
                    n = (callback == null || thisArg) ? 1 : callback || n;
                }
                return slice(array, 0, nativeMin(nativeMax(0, length - n), length));
            }

            function intersection() {
                var args = [],
                    argsIndex = -1,
                    argsLength = arguments.length,
                    caches = getArray(),
                    indexOf = getIndexOf(),
                    trustIndexOf = indexOf === baseIndexOf,
                    seen = getArray();
                while (++argsIndex < argsLength) {
                    var value = arguments[argsIndex];
                    if (isArray(value) || isArguments(value)) {
                        args.push(value);
                        caches.push(trustIndexOf && value.length >= largeArraySize && createCache(argsIndex ? args[argsIndex] : seen));
                    }
                }
                var array = args[0],
                    index = -1,
                    length = array ? array.length : 0,
                    result = [];
                outer: while (++index < length) {
                    var cache = caches[0];
                    value = array[index];
                    if ((cache ? cacheIndexOf(cache, value) : indexOf(seen, value)) < 0) {
                        argsIndex = argsLength;
                        (cache || seen).push(value);
                        while (--argsIndex) {
                            cache = caches[argsIndex];
                            if ((cache ? cacheIndexOf(cache, value) : indexOf(args[argsIndex], value)) < 0) {
                                continue outer;
                            }
                        }
                        result.push(value);
                    }
                }
                while (argsLength--) {
                    cache = caches[argsLength];
                    if (cache) {
                        releaseObject(cache);
                    }
                }
                releaseArray(caches);
                releaseArray(seen);
                return result;
            }

            function last(array, callback, thisArg) {
                var n = 0,
                    length = array ? array.length : 0;
                if (typeof callback != 'number' && callback != null) {
                    var index = length;
                    callback = lodash.createCallback(callback, thisArg, 3);
                    while (index-- && callback(array[index], index, array)) {
                        n++;
                    }
                } else {
                    n = callback;
                    if (n == null || thisArg) {
                        return array ? array[length - 1] : undefined;
                    }
                }
                return slice(array, nativeMax(0, length - n));
            }

            function lastIndexOf(array, value, fromIndex) {
                var index = array ? array.length : 0;
                if (typeof fromIndex == 'number') {
                    index = (fromIndex < 0 ? nativeMax(0, index + fromIndex) : nativeMin(fromIndex, index - 1)) + 1;
                }
                while (index--) {
                    if (array[index] === value) {
                        return index;
                    }
                }
                return -1;
            }

            function pull(array) {
                var args = arguments,
                    argsIndex = 0,
                    argsLength = args.length,
                    length = array ? array.length : 0;
                while (++argsIndex < argsLength) {
                    var index = -1,
                        value = args[argsIndex];
                    while (++index < length) {
                        if (array[index] === value) {
                            splice.call(array, index--, 1);
                            length--;
                        }
                    }
                }
                return array;
            }

            function range(start, end, step) {
                start = +start || 0;
                step = typeof step == 'number' ? step : (+step || 1);
                if (end == null) {
                    end = start;
                    start = 0;
                }
                var index = -1,
                    length = nativeMax(0, ceil((end - start) / (step || 1))),
                    result = Array(length);
                while (++index < length) {
                    result[index] = start;
                    start += step;
                }
                return result;
            }

            function remove(array, callback, thisArg) {
                var index = -1,
                    length = array ? array.length : 0,
                    result = [];
                callback = lodash.createCallback(callback, thisArg, 3);
                while (++index < length) {
                    var value = array[index];
                    if (callback(value, index, array)) {
                        result.push(value);
                        splice.call(array, index--, 1);
                        length--;
                    }
                }
                return result;
            }

            function rest(array, callback, thisArg) {
                if (typeof callback != 'number' && callback != null) {
                    var n = 0,
                        index = -1,
                        length = array ? array.length : 0;
                    callback = lodash.createCallback(callback, thisArg, 3);
                    while (++index < length && callback(array[index], index, array)) {
                        n++;
                    }
                } else {
                    n = (callback == null || thisArg) ? 1 : nativeMax(0, callback);
                }
                return slice(array, n);
            }

            function sortedIndex(array, value, callback, thisArg) {
                var low = 0,
                    high = array ? array.length : low;
                callback = callback ? lodash.createCallback(callback, thisArg, 1) : identity;
                value = callback(value);
                while (low < high) {
                    var mid = (low + high) >>> 1;
                    (callback(array[mid]) < value) ? low = mid + 1: high = mid;
                }
                return low;
            }

            function union() {
                return baseUniq(baseFlatten(arguments, true, true));
            }

            function uniq(array, isSorted, callback, thisArg) {
                if (typeof isSorted != 'boolean' && isSorted != null) {
                    thisArg = callback;
                    callback = (typeof isSorted != 'function' && thisArg && thisArg[isSorted] === array) ? null : isSorted;
                    isSorted = false;
                }
                if (callback != null) {
                    callback = lodash.createCallback(callback, thisArg, 3);
                }
                return baseUniq(array, isSorted, callback);
            }

            function without(array) {
                return baseDifference(array, slice(arguments, 1));
            }

            function xor() {
                var index = -1,
                    length = arguments.length;
                while (++index < length) {
                    var array = arguments[index];
                    if (isArray(array) || isArguments(array)) {
                        var result = result ? baseUniq(baseDifference(result, array).concat(baseDifference(array, result))) : array;
                    }
                }
                return result || [];
            }

            function zip() {
                var array = arguments.length > 1 ? arguments : arguments[0],
                    index = -1,
                    length = array ? max(pluck(array, 'length')) : 0,
                    result = Array(length < 0 ? 0 : length);
                while (++index < length) {
                    result[index] = pluck(array, index);
                }
                return result;
            }

            function zipObject(keys, values) {
                var index = -1,
                    length = keys ? keys.length : 0,
                    result = {};
                if (!values && length && !isArray(keys[0])) {
                    values = [];
                }
                while (++index < length) {
                    var key = keys[index];
                    if (values) {
                        result[key] = values[index];
                    } else if (key) {
                        result[key[0]] = key[1];
                    }
                }
                return result;
            }

            function after(n, func) {
                if (!isFunction(func)) {
                    throw new TypeError;
                }
                return function() {
                    if (--n < 1) {
                        return func.apply(this, arguments);
                    }
                };
            }

            function bind(func, thisArg) {
                return arguments.length > 2 ? createWrapper(func, 17, slice(arguments, 2), null, thisArg) : createWrapper(func, 1, null, null, thisArg);
            }

            function bindAll(object) {
                var funcs = arguments.length > 1 ? baseFlatten(arguments, true, false, 1) : functions(object),
                    index = -1,
                    length = funcs.length;
                while (++index < length) {
                    var key = funcs[index];
                    object[key] = createWrapper(object[key], 1, null, null, object);
                }
                return object;
            }

            function bindKey(object, key) {
                return arguments.length > 2 ? createWrapper(key, 19, slice(arguments, 2), null, object) : createWrapper(key, 3, null, null, object);
            }

            function compose() {
                var funcs = arguments,
                    length = funcs.length;
                while (length--) {
                    if (!isFunction(funcs[length])) {
                        throw new TypeError;
                    }
                }
                return function() {
                    var args = arguments,
                        length = funcs.length;
                    while (length--) {
                        args = [funcs[length].apply(this, args)];
                    }
                    return args[0];
                };
            }

            function curry(func, arity) {
                arity = typeof arity == 'number' ? arity : (+arity || func.length);
                return createWrapper(func, 4, null, null, null, arity);
            }

            function debounce(func, wait, options) {
                var args, maxTimeoutId, result, stamp, thisArg, timeoutId, trailingCall, lastCalled = 0,
                    maxWait = false,
                    trailing = true;
                if (!isFunction(func)) {
                    throw new TypeError;
                }
                wait = nativeMax(0, wait) || 0;
                if (options === true) {
                    var leading = true;
                    trailing = false;
                } else if (isObject(options)) {
                    leading = options.leading;
                    maxWait = 'maxWait' in options && (nativeMax(wait, options.maxWait) || 0);
                    trailing = 'trailing' in options ? options.trailing : trailing;
                }
                var delayed = function() {
                    var remaining = wait - (now() - stamp);
                    if (remaining <= 0) {
                        if (maxTimeoutId) {
                            clearTimeout(maxTimeoutId);
                        }
                        var isCalled = trailingCall;
                        maxTimeoutId = timeoutId = trailingCall = undefined;
                        if (isCalled) {
                            lastCalled = now();
                            result = func.apply(thisArg, args);
                            if (!timeoutId && !maxTimeoutId) {
                                args = thisArg = null;
                            }
                        }
                    } else {
                        timeoutId = setTimeout(delayed, remaining);
                    }
                };
                var maxDelayed = function() {
                    if (timeoutId) {
                        clearTimeout(timeoutId);
                    }
                    maxTimeoutId = timeoutId = trailingCall = undefined;
                    if (trailing || (maxWait !== wait)) {
                        lastCalled = now();
                        result = func.apply(thisArg, args);
                        if (!timeoutId && !maxTimeoutId) {
                            args = thisArg = null;
                        }
                    }
                };
                return function() {
                    args = arguments;
                    stamp = now();
                    thisArg = this;
                    trailingCall = trailing && (timeoutId || !leading);
                    if (maxWait === false) {
                        var leadingCall = leading && !timeoutId;
                    } else {
                        if (!maxTimeoutId && !leading) {
                            lastCalled = stamp;
                        }
                        var remaining = maxWait - (stamp - lastCalled),
                            isCalled = remaining <= 0;
                        if (isCalled) {
                            if (maxTimeoutId) {
                                maxTimeoutId = clearTimeout(maxTimeoutId);
                            }
                            lastCalled = stamp;
                            result = func.apply(thisArg, args);
                        } else if (!maxTimeoutId) {
                            maxTimeoutId = setTimeout(maxDelayed, remaining);
                        }
                    }
                    if (isCalled && timeoutId) {
                        timeoutId = clearTimeout(timeoutId);
                    } else if (!timeoutId && wait !== maxWait) {
                        timeoutId = setTimeout(delayed, wait);
                    }
                    if (leadingCall) {
                        isCalled = true;
                        result = func.apply(thisArg, args);
                    }
                    if (isCalled && !timeoutId && !maxTimeoutId) {
                        args = thisArg = null;
                    }
                    return result;
                };
            }

            function defer(func) {
                if (!isFunction(func)) {
                    throw new TypeError;
                }
                var args = slice(arguments, 1);
                return setTimeout(function() {
                    func.apply(undefined, args);
                }, 1);
            }

            function delay(func, wait) {
                if (!isFunction(func)) {
                    throw new TypeError;
                }
                var args = slice(arguments, 2);
                return setTimeout(function() {
                    func.apply(undefined, args);
                }, wait);
            }

            function memoize(func, resolver) {
                if (!isFunction(func)) {
                    throw new TypeError;
                }
                var memoized = function() {
                    var cache = memoized.cache,
                        key = resolver ? resolver.apply(this, arguments) : keyPrefix + arguments[0];
                    return hasOwnProperty.call(cache, key) ? cache[key] : (cache[key] = func.apply(this, arguments));
                }
                memoized.cache = {};
                return memoized;
            }

            function once(func) {
                var ran, result;
                if (!isFunction(func)) {
                    throw new TypeError;
                }
                return function() {
                    if (ran) {
                        return result;
                    }
                    ran = true;
                    result = func.apply(this, arguments);
                    func = null;
                    return result;
                };
            }

            function partial(func) {
                return createWrapper(func, 16, slice(arguments, 1));
            }

            function partialRight(func) {
                return createWrapper(func, 32, null, slice(arguments, 1));
            }

            function throttle(func, wait, options) {
                var leading = true,
                    trailing = true;
                if (!isFunction(func)) {
                    throw new TypeError;
                }
                if (options === false) {
                    leading = false;
                } else if (isObject(options)) {
                    leading = 'leading' in options ? options.leading : leading;
                    trailing = 'trailing' in options ? options.trailing : trailing;
                }
                debounceOptions.leading = leading;
                debounceOptions.maxWait = wait;
                debounceOptions.trailing = trailing;
                return debounce(func, wait, debounceOptions);
            }

            function wrap(value, wrapper) {
                return createWrapper(wrapper, 16, [value]);
            }

            function constant(value) {
                return function() {
                    return value;
                };
            }

            function createCallback(func, thisArg, argCount) {
                var type = typeof func;
                if (func == null || type == 'function') {
                    return baseCreateCallback(func, thisArg, argCount);
                }
                if (type != 'object') {
                    return property(func);
                }
                var props = keys(func),
                    key = props[0],
                    a = func[key];
                if (props.length == 1 && a === a && !isObject(a)) {
                    return function(object) {
                        var b = object[key];
                        return a === b && (a !== 0 || (1 / a == 1 / b));
                    };
                }
                return function(object) {
                    var length = props.length,
                        result = false;
                    while (length--) {
                        if (!(result = baseIsEqual(object[props[length]], func[props[length]], null, true))) {
                            break;
                        }
                    }
                    return result;
                };
            }

            function escape(string) {
                return string == null ? '' : String(string).replace(reUnescapedHtml, escapeHtmlChar);
            }

            function identity(value) {
                return value;
            }

            function mixin(object, source, options) {
                var chain = true,
                    methodNames = source && functions(source);
                if (!source || (!options && !methodNames.length)) {
                    if (options == null) {
                        options = source;
                    }
                    ctor = lodashWrapper;
                    source = object;
                    object = lodash;
                    methodNames = functions(source);
                }
                if (options === false) {
                    chain = false;
                } else if (isObject(options) && 'chain' in options) {
                    chain = options.chain;
                }
                var ctor = object,
                    isFunc = isFunction(ctor);
                forEach(methodNames, function(methodName) {
                    var func = object[methodName] = source[methodName];
                    if (isFunc) {
                        ctor.prototype[methodName] = function() {
                            var chainAll = this.__chain__,
                                value = this.__wrapped__,
                                args = [value];
                            push.apply(args, arguments);
                            var result = func.apply(object, args);
                            if (chain || chainAll) {
                                if (value === result && isObject(result)) {
                                    return this;
                                }
                                result = new ctor(result);
                                result.__chain__ = chainAll;
                            }
                            return result;
                        };
                    }
                });
            }

            function noConflict() {
                context._ = oldDash;
                return this;
            }

            function noop() {}

            var now = isNative(now = Date.now) && now || function() {
                return new Date().getTime();
            };
            var parseInt = nativeParseInt(whitespace + '08') == 8 ? nativeParseInt : function(value, radix) {
                return nativeParseInt(isString(value) ? value.replace(reLeadingSpacesAndZeros, '') : value, radix || 0);
            };

            function property(key) {
                return function(object) {
                    return object[key];
                };
            }

            function random(min, max, floating) {
                var noMin = min == null,
                    noMax = max == null;
                if (floating == null) {
                    if (typeof min == 'boolean' && noMax) {
                        floating = min;
                        min = 1;
                    } else if (!noMax && typeof max == 'boolean') {
                        floating = max;
                        noMax = true;
                    }
                }
                if (noMin && noMax) {
                    max = 1;
                }
                min = +min || 0;
                if (noMax) {
                    max = min;
                    min = 0;
                } else {
                    max = +max || 0;
                }
                if (floating || min % 1 || max % 1) {
                    var rand = nativeRandom();
                    return nativeMin(min + (rand * (max - min + parseFloat('1e-' + ((rand + '').length - 1)))), max);
                }
                return baseRandom(min, max);
            }

            function result(object, key) {
                if (object) {
                    var value = object[key];
                    return isFunction(value) ? object[key]() : value;
                }
            }

            function template(text, data, options) {
                var settings = lodash.templateSettings;
                text = String(text || '');
                options = defaults({}, options, settings);
                var imports = defaults({}, options.imports, settings.imports),
                    importsKeys = keys(imports),
                    importsValues = values(imports);
                var isEvaluating, index = 0,
                    interpolate = options.interpolate || reNoMatch,
                    source = "__p += '";
                var reDelimiters = RegExp((options.escape || reNoMatch).source + '|' +
                    interpolate.source + '|' +
                    (interpolate === reInterpolate ? reEsTemplate : reNoMatch).source + '|' +
                    (options.evaluate || reNoMatch).source + '|$', 'g');
                text.replace(reDelimiters, function(match, escapeValue, interpolateValue, esTemplateValue, evaluateValue, offset) {
                    interpolateValue || (interpolateValue = esTemplateValue);
                    source += text.slice(index, offset).replace(reUnescapedString, escapeStringChar);
                    if (escapeValue) {
                        source += "' +\n__e(" + escapeValue + ") +\n'";
                    }
                    if (evaluateValue) {
                        isEvaluating = true;
                        source += "';\n" + evaluateValue + ";\n__p += '";
                    }
                    if (interpolateValue) {
                        source += "' +\n((__t = (" + interpolateValue + ")) == null ? '' : __t) +\n'";
                    }
                    index = offset + match.length;
                    return match;
                });
                source += "';\n";
                var variable = options.variable,
                    hasVariable = variable;
                if (!hasVariable) {
                    variable = 'obj';
                    source = 'with (' + variable + ') {\n' + source + '\n}\n';
                }
                source = (isEvaluating ? source.replace(reEmptyStringLeading, '') : source).replace(reEmptyStringMiddle, '$1').replace(reEmptyStringTrailing, '$1;');
                source = 'function(' + variable + ') {\n' +
                    (hasVariable ? '' : variable + ' || (' + variable + ' = {});\n') + "var __t, __p = '', __e = _.escape" +
                    (isEvaluating ? ', __j = Array.prototype.join;\n' + "function print() { __p += __j.call(arguments, '') }\n" : ';\n') +
                    source + 'return __p\n}';
                var sourceURL = '\n/*\n//# sourceURL=' + (options.sourceURL || '/lodash/template/source[' + (templateCounter++) + ']') + '\n*/';
                try {
                    var result = Function(importsKeys, 'return ' + source + sourceURL).apply(undefined, importsValues);
                } catch (e) {
                    e.source = source;
                    throw e;
                }
                if (data) {
                    return result(data);
                }
                result.source = source;
                return result;
            }

            function times(n, callback, thisArg) {
                n = (n = +n) > -1 ? n : 0;
                var index = -1,
                    result = Array(n);
                callback = baseCreateCallback(callback, thisArg, 1);
                while (++index < n) {
                    result[index] = callback(index);
                }
                return result;
            }

            function unescape(string) {
                return string == null ? '' : String(string).replace(reEscapedHtml, unescapeHtmlChar);
            }

            function uniqueId(prefix) {
                var id = ++idCounter;
                return String(prefix == null ? '' : prefix) + id;
            }

            function chain(value) {
                value = new lodashWrapper(value);
                value.__chain__ = true;
                return value;
            }

            function tap(value, interceptor) {
                interceptor(value);
                return value;
            }

            function wrapperChain() {
                this.__chain__ = true;
                return this;
            }

            function wrapperToString() {
                return String(this.__wrapped__);
            }

            function wrapperValueOf() {
                return this.__wrapped__;
            }

            lodash.after = after;
            lodash.assign = assign;
            lodash.at = at;
            lodash.bind = bind;
            lodash.bindAll = bindAll;
            lodash.bindKey = bindKey;
            lodash.chain = chain;
            lodash.compact = compact;
            lodash.compose = compose;
            lodash.constant = constant;
            lodash.countBy = countBy;
            lodash.create = create;
            lodash.createCallback = createCallback;
            lodash.curry = curry;
            lodash.debounce = debounce;
            lodash.defaults = defaults;
            lodash.defer = defer;
            lodash.delay = delay;
            lodash.difference = difference;
            lodash.filter = filter;
            lodash.flatten = flatten;
            lodash.forEach = forEach;
            lodash.forEachRight = forEachRight;
            lodash.forIn = forIn;
            lodash.forInRight = forInRight;
            lodash.forOwn = forOwn;
            lodash.forOwnRight = forOwnRight;
            lodash.functions = functions;
            lodash.groupBy = groupBy;
            lodash.indexBy = indexBy;
            lodash.initial = initial;
            lodash.intersection = intersection;
            lodash.invert = invert;
            lodash.invoke = invoke;
            lodash.keys = keys;
            lodash.map = map;
            lodash.mapValues = mapValues;
            lodash.max = max;
            lodash.memoize = memoize;
            lodash.merge = merge;
            lodash.min = min;
            lodash.omit = omit;
            lodash.once = once;
            lodash.pairs = pairs;
            lodash.partial = partial;
            lodash.partialRight = partialRight;
            lodash.pick = pick;
            lodash.pluck = pluck;
            lodash.property = property;
            lodash.pull = pull;
            lodash.range = range;
            lodash.reject = reject;
            lodash.remove = remove;
            lodash.rest = rest;
            lodash.shuffle = shuffle;
            lodash.sortBy = sortBy;
            lodash.tap = tap;
            lodash.throttle = throttle;
            lodash.times = times;
            lodash.toArray = toArray;
            lodash.transform = transform;
            lodash.union = union;
            lodash.uniq = uniq;
            lodash.values = values;
            lodash.where = where;
            lodash.without = without;
            lodash.wrap = wrap;
            lodash.xor = xor;
            lodash.zip = zip;
            lodash.zipObject = zipObject;
            lodash.collect = map;
            lodash.drop = rest;
            lodash.each = forEach;
            lodash.eachRight = forEachRight;
            lodash.extend = assign;
            lodash.methods = functions;
            lodash.object = zipObject;
            lodash.select = filter;
            lodash.tail = rest;
            lodash.unique = uniq;
            lodash.unzip = zip;
            mixin(lodash);
            lodash.clone = clone;
            lodash.cloneDeep = cloneDeep;
            lodash.contains = contains;
            lodash.escape = escape;
            lodash.every = every;
            lodash.find = find;
            lodash.findIndex = findIndex;
            lodash.findKey = findKey;
            lodash.findLast = findLast;
            lodash.findLastIndex = findLastIndex;
            lodash.findLastKey = findLastKey;
            lodash.has = has;
            lodash.identity = identity;
            lodash.indexOf = indexOf;
            lodash.isArguments = isArguments;
            lodash.isArray = isArray;
            lodash.isBoolean = isBoolean;
            lodash.isDate = isDate;
            lodash.isElement = isElement;
            lodash.isEmpty = isEmpty;
            lodash.isEqual = isEqual;
            lodash.isFinite = isFinite;
            lodash.isFunction = isFunction;
            lodash.isNaN = isNaN;
            lodash.isNull = isNull;
            lodash.isNumber = isNumber;
            lodash.isObject = isObject;
            lodash.isPlainObject = isPlainObject;
            lodash.isRegExp = isRegExp;
            lodash.isString = isString;
            lodash.isUndefined = isUndefined;
            lodash.lastIndexOf = lastIndexOf;
            lodash.mixin = mixin;
            lodash.noConflict = noConflict;
            lodash.noop = noop;
            lodash.now = now;
            lodash.parseInt = parseInt;
            lodash.random = random;
            lodash.reduce = reduce;
            lodash.reduceRight = reduceRight;
            lodash.result = result;
            lodash.runInContext = runInContext;
            lodash.size = size;
            lodash.some = some;
            lodash.sortedIndex = sortedIndex;
            lodash.template = template;
            lodash.unescape = unescape;
            lodash.uniqueId = uniqueId;
            lodash.all = every;
            lodash.any = some;
            lodash.detect = find;
            lodash.findWhere = find;
            lodash.foldl = reduce;
            lodash.foldr = reduceRight;
            lodash.include = contains;
            lodash.inject = reduce;
            mixin(function() {
                var source = {}
                forOwn(lodash, function(func, methodName) {
                    if (!lodash.prototype[methodName]) {
                        source[methodName] = func;
                    }
                });
                return source;
            }(), false);
            lodash.first = first;
            lodash.last = last;
            lodash.sample = sample;
            lodash.take = first;
            lodash.head = first;
            forOwn(lodash, function(func, methodName) {
                var callbackable = methodName !== 'sample';
                if (!lodash.prototype[methodName]) {
                    lodash.prototype[methodName] = function(n, guard) {
                        var chainAll = this.__chain__,
                            result = func(this.__wrapped__, n, guard);
                        return !chainAll && (n == null || (guard && !(callbackable && typeof n == 'function'))) ? result : new lodashWrapper(result, chainAll);
                    };
                }
            });
            lodash.VERSION = '2.4.1';
            lodash.prototype.chain = wrapperChain;
            lodash.prototype.toString = wrapperToString;
            lodash.prototype.value = wrapperValueOf;
            lodash.prototype.valueOf = wrapperValueOf;
            forEach(['join', 'pop', 'shift'], function(methodName) {
                var func = arrayRef[methodName];
                lodash.prototype[methodName] = function() {
                    var chainAll = this.__chain__,
                        result = func.apply(this.__wrapped__, arguments);
                    return chainAll ? new lodashWrapper(result, chainAll) : result;
                };
            });
            forEach(['push', 'reverse', 'sort', 'unshift'], function(methodName) {
                var func = arrayRef[methodName];
                lodash.prototype[methodName] = function() {
                    func.apply(this.__wrapped__, arguments);
                    return this;
                };
            });
            forEach(['concat', 'slice', 'splice'], function(methodName) {
                var func = arrayRef[methodName];
                lodash.prototype[methodName] = function() {
                    return new lodashWrapper(func.apply(this.__wrapped__, arguments), this.__chain__);
                };
            });
            return lodash;
        }

        var _ = runInContext();
        if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
            root._ = _;
            define(function() {
                return _;
            });
        } else if (freeExports && freeModule) {
            if (moduleExports) {
                (freeModule.exports = _)._ = _;
            } else {
                freeExports._ = _;
            }
        } else {
            root._ = _;
        }
    }.call(this));;
    (function() {
        try {
            $(function() {
                try {
                    var $block, blockMatch, offset, offsetMatch, scrollTop;
                    blockMatch = location.href.match(/scrollblock=(\d+)/);
                    if (blockMatch) {
                        offsetMatch = location.href.match(/scrolloffset=(-?\d+)/);
                        offset = offsetMatch.length ? offsetMatch[1] * 1 : 0;
                        $block = $('.node.section').eq(blockMatch[1] * 1);
                        scrollTop = $block.offset().top + offset;
                        return $(document).scrollTop(scrollTop);
                    }
                } catch (e) {
                    Raven.captureException(e, {
                        extra: {
                            logger: Logger.send()
                        }
                    });
                    console.error && console.error(e.message);
                }
            });
        } catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;



    (function() {
        try {
            $(function() {
                try {
                    var $btns, $modals, closemodal, keep;
                    $.fn.findUpmost = function(selector) {
                        try {
                            var level, levels, _i, _len;
                            levels = [];
                            $(this).find(selector).each(function() {
                                try {
                                    var level;
                                    level = $(this).parents().length;
                                    if (levels[level]) {
                                        return levels[level].push(this);
                                    } else {
                                        return levels[level] = [this];
                                    }
                                } catch (e) {
                                    Raven.captureException(e, {
                                        extra: {
                                            logger: Logger.send()
                                        }
                                    });
                                    console.error && console.error(e.message);
                                }
                            });
                            for (_i = 0, _len = levels.length; _i < _len; _i++) {
                                level = levels[_i];
                                if (level) {
                                    return $(level);
                                }
                            }
                        } catch (e) {
                            Raven.captureException(e, {
                                extra: {
                                    logger: Logger.send()
                                }
                            });
                            console.error && console.error(e.message);
                        }
                    };
                    $modals = [];
                    keep = '.modal-content';
                    $.fn.modalcontrol = function($btn, $modal) {
                        try {
                            $modal.appendTo('body');
                            $modal.modal({
                                backdrop: false,
                                keyboard: false,
                                show: false
                            });
                            return $btn.on('click', function(event) {
                                try {
                                    $modal.modal('show');
                                    $modal.before('<div class="modal-backdrop fade in"></div>');
                                    return $modals.push($modal);
                                } catch (e) {
                                    Raven.captureException(e, {
                                        extra: {
                                            logger: Logger.send()
                                        }
                                    });
                                    console.error && console.error(e.message);
                                }
                            });
                        } catch (e) {
                            Raven.captureException(e, {
                                extra: {
                                    logger: Logger.send()
                                }
                            });
                            console.error && console.error(e.message);
                        }
                    };
                    $btns = $('.btn[data-modal], .btn-modal[data-modal]');
                    $btns.each(function() {
                        try {
                            var $btn, $modal, $node;
                            $btn = $(this);
                            $node = $btn.closest('.node');
                            $modal = $node.findUpmost('.modal[data-modal="' + $btn.attr('data-modal') + '"]');
                            return $btn.modalcontrol($btn, $modal);
                        } catch (e) {
                            Raven.captureException(e, {
                                extra: {
                                    logger: Logger.send()
                                }
                            });
                            console.error && console.error(e.message);
                        }
                    });
                    closemodal = function() {
                        try {
                            var $modal;
                            $modal = _.last($modals).modal('hide');
                            $modal.prev('.modal-backdrop').remove();
                            return $modals = _.without($modals, $modal);
                        } catch (e) {
                            Raven.captureException(e, {
                                extra: {
                                    logger: Logger.send()
                                }
                            });
                            console.error && console.error(e.message);
                        }
                    };
                    return $(document).on('click', function(event) {
                        try {
                            var $closest, $modal;
                            if (!$modals.length) {
                                return;
                            }
                            if ($(event.target).closest($btns).length) {
                                return;
                            }
                            $modal = _.last($modals);
                            if (event.target === $modal[0]) {
                                return closemodal();
                            }
                            if ($(event.target).closest('.modal-header > .close').length) {
                                return closemodal();
                            }
                            $closest = $(event.target).closest('.modal');
                            if ($closest.length && $closest[0] !== $modal[0]) {
                                return closemodal();
                            }
                            if (!$(event.target).closest(keep).length) {
                                return closemodal();
                            }
                        } catch (e) {
                            Raven.captureException(e, {
                                extra: {
                                    logger: Logger.send()
                                }
                            });
                            console.error && console.error(e.message);
                        }
                    });
                } catch (e) {
                    Raven.captureException(e, {
                        extra: {
                            logger: Logger.send()
                        }
                    });
                    console.error && console.error(e.message);
                }
            });
        } catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function($, document, window) {
        var
            defaults = {
                html: false,
                photo: false,
                iframe: false,
                inline: false,
                transition: "elastic",
                speed: 300,
                fadeOut: 300,
                width: false,
                initialWidth: "600",
                innerWidth: false,
                maxWidth: false,
                height: false,
                initialHeight: "450",
                innerHeight: false,
                maxHeight: false,
                scalePhotos: true,
                scrolling: true,
                opacity: 0.9,
                preloading: true,
                className: false,
                overlayClose: true,
                escKey: true,
                arrowKey: true,
                top: false,
                bottom: false,
                left: false,
                right: false,
                fixed: false,
                data: undefined,
                closeButton: true,
                fastIframe: true,
                open: false,
                reposition: true,
                loop: true,
                slideshow: false,
                slideshowAuto: true,
                slideshowSpeed: 2500,
                slideshowStart: "start slideshow",
                slideshowStop: "stop slideshow",
                photoRegex: /\.(gif|png|jp(e|g|eg)|bmp|ico|webp|jxr|svg)((#|\?).*)?$/i,
                retinaImage: false,
                retinaUrl: false,
                retinaSuffix: '@2x.$1',
                current: "image {current} of {total}",
                previous: "previous",
                next: "next",
                close: "close",
                xhrError: "This content failed to load.",
                imgError: "This image failed to load.",
                returnFocus: true,
                trapFocus: true,
                onOpen: false,
                onLoad: false,
                onComplete: false,
                onCleanup: false,
                onClosed: false,
                rel: function() {
                    return this.rel;
                },
                href: function() {
                    return $(this).attr('href');
                },
                title: function() {
                    return this.title;
                }
            },
            colorbox = 'colorbox',
            prefix = 'cbox',
            boxElement = prefix + 'Element',
            event_open = prefix + '_open',
            event_load = prefix + '_load',
            event_complete = prefix + '_complete',
            event_cleanup = prefix + '_cleanup',
            event_closed = prefix + '_closed',
            event_purge = prefix + '_purge',
            $overlay, $box, $wrap, $content, $topBorder, $leftBorder, $rightBorder, $bottomBorder, $related, $window, $loaded, $loadingBay, $loadingOverlay, $title, $current, $slideshow, $next, $prev, $close, $groupControls, $events = $('<a/>'),
            settings, interfaceHeight, interfaceWidth, loadedHeight, loadedWidth, index, photo, open, active, closing, loadingTimer, publicMethod, div = "div",
            requests = 0,
            previousCSS = {},
            init;

        function $tag(tag, id, css) {
            var element = document.createElement(tag);
            if (id) {
                element.id = prefix + id;
            }
            if (css) {
                element.style.cssText = css;
            }
            return $(element);
        }

        function winheight() {
            return window.innerHeight ? window.innerHeight : $(window).height();
        }

        function Settings(element, options) {
            if (options !== Object(options)) {
                options = {};
            }
            this.cache = {};
            this.el = element;
            this.value = function(key) {
                var dataAttr;
                if (this.cache[key] === undefined) {
                    dataAttr = $(this.el).attr('data-cbox-' + key);
                    if (dataAttr !== undefined) {
                        this.cache[key] = dataAttr;
                    } else if (options[key] !== undefined) {
                        this.cache[key] = options[key];
                    } else if (defaults[key] !== undefined) {
                        this.cache[key] = defaults[key];
                    }
                }
                return this.cache[key];
            };
            this.get = function(key) {
                var value = this.value(key);
                return $.isFunction(value) ? value.call(this.el, this) : value;
            };
        }

        function getIndex(increment) {
            var
                max = $related.length,
                newIndex = (index + increment) % max;
            return (newIndex < 0) ? max + newIndex : newIndex;
        }

        function setSize(size, dimension) {
            return Math.round((/%/.test(size) ? ((dimension === 'x' ? $window.width() : winheight()) / 100) : 1) * parseInt(size, 10));
        }

        function isImage(settings, url) {
            return settings.get('photo') || settings.get('photoRegex').test(url);
        }

        function retinaUrl(settings, url) {
            return settings.get('retinaUrl') && window.devicePixelRatio > 1 ? url.replace(settings.get('photoRegex'), settings.get('retinaSuffix')) : url;
        }

        function trapFocus(e) {
            if ('contains' in $box[0] && !$box[0].contains(e.target) && e.target !== $overlay[0]) {
                e.stopPropagation();
                $box.focus();
            }
        }

        function setClass(str) {
            if (setClass.str !== str) {
                $box.add($overlay).removeClass(setClass.str).addClass(str);
                setClass.str = str;
            }
        }

        function getRelated(rel) {
            index = 0;
            if (rel && rel !== false && rel !== 'nofollow') {
                $related = $('.' + boxElement).filter(function() {
                    var options = $.data(this, colorbox);
                    var settings = new Settings(this, options);
                    return (settings.get('rel') === rel);
                });
                index = $related.index(settings.el);
                if (index === -1) {
                    $related = $related.add(settings.el);
                    index = $related.length - 1;
                }
            } else {
                $related = $(settings.el);
            }
        }

        function trigger(event) {
            $(document).trigger(event);
            $events.triggerHandler(event);
        }

        var slideshow = (function() {
            var active, className = prefix + "Slideshow_",
                click = "click." + prefix,
                timeOut;

            function clear() {
                clearTimeout(timeOut);
            }

            function set() {
                if (settings.get('loop') || $related[index + 1]) {
                    clear();
                    timeOut = setTimeout(publicMethod.next, settings.get('slideshowSpeed'));
                }
            }

            function start() {
                $slideshow.html(settings.get('slideshowStop')).unbind(click).one(click, stop);
                $events.bind(event_complete, set).bind(event_load, clear);
                $box.removeClass(className + "off").addClass(className + "on");
            }

            function stop() {
                clear();
                $events.unbind(event_complete, set).unbind(event_load, clear);
                $slideshow.html(settings.get('slideshowStart')).unbind(click).one(click, function() {
                    publicMethod.next();
                    start();
                });
                $box.removeClass(className + "on").addClass(className + "off");
            }

            function reset() {
                active = false;
                $slideshow.hide();
                clear();
                $events.unbind(event_complete, set).unbind(event_load, clear);
                $box.removeClass(className + "off " + className + "on");
            }

            return function() {
                if (active) {
                    if (!settings.get('slideshow')) {
                        $events.unbind(event_cleanup, reset);
                        reset();
                    }
                } else {
                    if (settings.get('slideshow') && $related[1]) {
                        active = true;
                        $events.one(event_cleanup, reset);
                        if (settings.get('slideshowAuto')) {
                            start();
                        } else {
                            stop();
                        }
                        $slideshow.show();
                    }
                }
            };
        }());

        function launch(element) {
            var options;
            if (!closing) {
                options = $(element).data('colorbox');
                settings = new Settings(element, options);
                getRelated(settings.get('rel'));
                if (!open) {
                    open = active = true;
                    setClass(settings.get('className'));
                    $box.css({
                        visibility: 'hidden',
                        display: 'block',
                        opacity: ''
                    });
                    $loaded = $tag(div, 'LoadedContent', 'width:0; height:0; overflow:hidden; visibility:hidden');
                    $content.css({
                        width: '',
                        height: ''
                    }).append($loaded);
                    interfaceHeight = $topBorder.height() + $bottomBorder.height() + $content.outerHeight(true) - $content.height();
                    interfaceWidth = $leftBorder.width() + $rightBorder.width() + $content.outerWidth(true) - $content.width();
                    loadedHeight = $loaded.outerHeight(true);
                    loadedWidth = $loaded.outerWidth(true);
                    var initialWidth = setSize(settings.get('initialWidth'), 'x');
                    var initialHeight = setSize(settings.get('initialHeight'), 'y');
                    var maxWidth = settings.get('maxWidth');
                    var maxHeight = settings.get('maxHeight');
                    settings.w = (maxWidth !== false ? Math.min(initialWidth, setSize(maxWidth, 'x')) : initialWidth) - loadedWidth - interfaceWidth;
                    settings.h = (maxHeight !== false ? Math.min(initialHeight, setSize(maxHeight, 'y')) : initialHeight) - loadedHeight - interfaceHeight;
                    $loaded.css({
                        width: '',
                        height: settings.h
                    });
                    publicMethod.position();
                    trigger(event_open);
                    settings.get('onOpen');
                    $groupControls.add($title).hide();
                    $box.focus();
                    if (settings.get('trapFocus')) {
                        if (document.addEventListener) {
                            document.addEventListener('focus', trapFocus, true);
                            $events.one(event_closed, function() {
                                document.removeEventListener('focus', trapFocus, true);
                            });
                        }
                    }
                    if (settings.get('returnFocus')) {
                        $events.one(event_closed, function() {
                            $(settings.el).focus();
                        });
                    }
                }
                $overlay.css({
                    opacity: parseFloat(settings.get('opacity')) || '',
                    cursor: settings.get('overlayClose') ? 'pointer' : '',
                    visibility: 'visible'
                }).show();
                if (settings.get('closeButton')) {
                    $close.html(settings.get('close')).appendTo($content);
                } else {
                    $close.appendTo('<div/>');
                }
                load();
            }
        }

        function appendHTML() {
            if (!$box && document.body) {
                init = false;
                $window = $(window);
                $box = $tag(div).attr({
                    id: colorbox,
                    'class': $.support.opacity === false ? prefix + 'IE' : '',
                    role: 'dialog',
                    tabindex: '-1'
                }).hide();
                $overlay = $tag(div, "Overlay").hide();
                $loadingOverlay = $([$tag(div, "LoadingOverlay")[0], $tag(div, "LoadingGraphic")[0]]);
                $wrap = $tag(div, "Wrapper");
                $content = $tag(div, "Content").append($title = $tag(div, "Title"), $current = $tag(div, "Current"), $prev = $('<button type="button"/>').attr({
                    id: prefix + 'Previous'
                }), $next = $('<button type="button"/>').attr({
                    id: prefix + 'Next'
                }), $slideshow = $tag('button', "Slideshow"), $loadingOverlay);
                $close = $('<button type="button"/>').attr({
                    id: prefix + 'Close'
                });
                $wrap.append($tag(div).append($tag(div, "TopLeft"), $topBorder = $tag(div, "TopCenter"), $tag(div, "TopRight")), $tag(div, false, 'clear:left').append($leftBorder = $tag(div, "MiddleLeft"), $content, $rightBorder = $tag(div, "MiddleRight")), $tag(div, false, 'clear:left').append($tag(div, "BottomLeft"), $bottomBorder = $tag(div, "BottomCenter"), $tag(div, "BottomRight"))).find('div div').css({
                    'float': 'left'
                });
                $loadingBay = $tag(div, false, 'position:absolute; width:9999px; visibility:hidden; display:none; max-width:none;');
                $groupControls = $next.add($prev).add($current).add($slideshow);
                $(document.body).append($overlay, $box.append($wrap, $loadingBay));
            }
        }

        function addBindings() {
            function clickHandler(e) {
                if (!(e.which > 1 || e.shiftKey || e.altKey || e.metaKey || e.ctrlKey)) {
                    e.preventDefault();
                    launch(this);
                }
            }

            if ($box) {
                if (!init) {
                    init = true;
                    $next.click(function() {
                        publicMethod.next();
                    });
                    $prev.click(function() {
                        publicMethod.prev();
                    });
                    $close.click(function() {
                        publicMethod.close();
                    });
                    $overlay.click(function() {
                        if (settings.get('overlayClose')) {
                            publicMethod.close();
                        }
                    });
                    $(document).bind('keydown.' + prefix, function(e) {
                        var key = e.keyCode;
                        if (open && settings.get('escKey') && key === 27) {
                            e.preventDefault();
                            publicMethod.close();
                        }
                        if (open && settings.get('arrowKey') && $related[1] && !e.altKey) {
                            if (key === 37) {
                                e.preventDefault();
                                $prev.click();
                            } else if (key === 39) {
                                e.preventDefault();
                                $next.click();
                            }
                        }
                    });
                    if ($.isFunction($.fn.on)) {
                        $(document).on('click.' + prefix, '.' + boxElement, clickHandler);
                    } else {
                        $('.' + boxElement).live('click.' + prefix, clickHandler);
                    }
                }
                return true;
            }
            return false;
        }

        if ($.colorbox) {
            return;
        }
        $(appendHTML);
        publicMethod = $.fn[colorbox] = $[colorbox] = function(options, callback) {
            var settings;
            var $obj = this;
            options = options || {};
            if ($.isFunction($obj)) {
                $obj = $('<a/>');
                options.open = true;
            } else if (!$obj[0]) {
                return $obj;
            }
            if (!$obj[0]) {
                return $obj;
            }
            appendHTML();
            if (addBindings()) {
                if (callback) {
                    options.onComplete = callback;
                }
                $obj.each(function() {
                    var old = $.data(this, colorbox) || {};
                    $.data(this, colorbox, $.extend(old, options));
                }).addClass(boxElement);
                settings = new Settings($obj[0], options);
                if (settings.get('open')) {
                    launch($obj[0]);
                }
            }
            return $obj;
        };
        publicMethod.position = function(speed, loadedCallback) {
            var
                css, top = 0,
                left = 0,
                offset = $box.offset(),
                scrollTop, scrollLeft;
            $window.unbind('resize.' + prefix);
            $box.css({
                top: -9e4,
                left: -9e4
            });
            scrollTop = $window.scrollTop();
            scrollLeft = $window.scrollLeft();
            if (settings.get('fixed')) {
                offset.top -= scrollTop;
                offset.left -= scrollLeft;
                $box.css({
                    position: 'fixed'
                });
            } else {
                top = scrollTop;
                left = scrollLeft;
                $box.css({
                    position: 'absolute'
                });
            }
            if (settings.get('right') !== false) {
                left += Math.max($window.width() - settings.w - loadedWidth - interfaceWidth - setSize(settings.get('right'), 'x'), 0);
            } else if (settings.get('left') !== false) {
                left += setSize(settings.get('left'), 'x');
            } else {
                left += Math.round(Math.max($window.width() - settings.w - loadedWidth - interfaceWidth, 0) / 2);
            }
            if (settings.get('bottom') !== false) {
                top += Math.max(winheight() - settings.h - loadedHeight - interfaceHeight - setSize(settings.get('bottom'), 'y'), 0);
            } else if (settings.get('top') !== false) {
                top += setSize(settings.get('top'), 'y');
            } else {
                top += Math.round(Math.max(winheight() - settings.h - loadedHeight - interfaceHeight, 0) / 2);
            }
            $box.css({
                top: offset.top,
                left: offset.left,
                visibility: 'visible'
            });
            $wrap[0].style.width = $wrap[0].style.height = "9999px";

            function modalDimensions() {
                $topBorder[0].style.width = $bottomBorder[0].style.width = $content[0].style.width = (parseInt($box[0].style.width, 10) - interfaceWidth) + 'px';
                $content[0].style.height = $leftBorder[0].style.height = $rightBorder[0].style.height = (parseInt($box[0].style.height, 10) - interfaceHeight) + 'px';
            }

            css = {
                width: settings.w + loadedWidth + interfaceWidth,
                height: settings.h + loadedHeight + interfaceHeight,
                top: top,
                left: left
            };
            if (speed) {
                var tempSpeed = 0;
                $.each(css, function(i) {
                    if (css[i] !== previousCSS[i]) {
                        tempSpeed = speed;
                        return;
                    }
                });
                speed = tempSpeed;
            }
            previousCSS = css;
            if (!speed) {
                $box.css(css);
            }
            $box.dequeue().animate(css, {
                duration: speed || 0,
                complete: function() {
                    modalDimensions();
                    active = false;
                    $wrap[0].style.width = (settings.w + loadedWidth + interfaceWidth) + "px";
                    $wrap[0].style.height = (settings.h + loadedHeight + interfaceHeight) + "px";
                    if (settings.get('reposition')) {
                        setTimeout(function() {
                            $window.bind('resize.' + prefix, publicMethod.position);
                        }, 1);
                    }
                    if (loadedCallback) {
                        loadedCallback();
                    }
                },
                step: modalDimensions
            });
        };
        publicMethod.resize = function(options) {
            var scrolltop;
            if (open) {
                options = options || {};
                if (options.width) {
                    settings.w = setSize(options.width, 'x') - loadedWidth - interfaceWidth;
                }
                if (options.innerWidth) {
                    settings.w = setSize(options.innerWidth, 'x');
                }
                $loaded.css({
                    width: settings.w
                });
                if (options.height) {
                    settings.h = setSize(options.height, 'y') - loadedHeight - interfaceHeight;
                }
                if (options.innerHeight) {
                    settings.h = setSize(options.innerHeight, 'y');
                }
                if (!options.innerHeight && !options.height) {
                    scrolltop = $loaded.scrollTop();
                    $loaded.css({
                        height: "auto"
                    });
                    settings.h = $loaded.height();
                }
                $loaded.css({
                    height: settings.h
                });
                if (scrolltop) {
                    $loaded.scrollTop(scrolltop);
                }
                publicMethod.position(settings.get('transition') === "none" ? 0 : settings.get('speed'));
            }
        };
        publicMethod.prep = function(object) {
            if (!open) {
                return;
            }
            var callback, speed = settings.get('transition') === "none" ? 0 : settings.get('speed');
            $loaded.remove();
            $loaded = $tag(div, 'LoadedContent').append(object);

            function getWidth() {
                settings.w = settings.w || $loaded.width();
                settings.w = settings.mw && settings.mw < settings.w ? settings.mw : settings.w;
                return settings.w;
            }

            function getHeight() {
                settings.h = settings.h || $loaded.height();
                settings.h = settings.mh && settings.mh < settings.h ? settings.mh : settings.h;
                return settings.h;
            }

            $loaded.hide().appendTo($loadingBay.show()).css({
                width: getWidth(),
                overflow: settings.get('scrolling') ? 'auto' : 'hidden'
            }).css({
                height: getHeight()
            }).prependTo($content);
            $loadingBay.hide();
            $(photo).css({
                'float': 'none'
            });
            setClass(settings.get('className'));
            callback = function() {
                var total = $related.length,
                    iframe, complete;
                if (!open) {
                    return;
                }

                function removeFilter() {
                    if ($.support.opacity === false) {
                        $box[0].style.removeAttribute('filter');
                    }
                }

                complete = function() {
                    clearTimeout(loadingTimer);
                    $loadingOverlay.hide();
                    trigger(event_complete);
                    settings.get('onComplete');
                };
                $title.html(settings.get('title')).show();
                $loaded.show();
                if (total > 1) {
                    if (typeof settings.get('current') === "string") {
                        $current.html(settings.get('current').replace('{current}', index + 1).replace('{total}', total)).show();
                    }
                    $next[(settings.get('loop') || index < total - 1) ? "show" : "hide"]().html(settings.get('next'));
                    $prev[(settings.get('loop') || index) ? "show" : "hide"]().html(settings.get('previous'));
                    slideshow();
                    if (settings.get('preloading')) {
                        $.each([getIndex(-1), getIndex(1)], function() {
                            var img, i = $related[this],
                                settings = new Settings(i, $.data(i, colorbox)),
                                src = settings.get('href');
                            if (src && isImage(settings, src)) {
                                src = retinaUrl(settings, src);
                                img = document.createElement('img');
                                img.src = src;
                            }
                        });
                    }
                } else {
                    $groupControls.hide();
                }
                if (settings.get('iframe')) {
                    iframe = document.createElement('iframe');
                    if ('frameBorder' in iframe) {
                        iframe.frameBorder = 0;
                    }
                    if ('allowTransparency' in iframe) {
                        iframe.allowTransparency = "true";
                    }
                    if (!settings.get('scrolling')) {
                        iframe.scrolling = "no";
                    }
                    $(iframe).attr({
                        src: settings.get('href'),
                        name: (new Date()).getTime(),
                        'class': prefix + 'Iframe',
                        allowFullScreen: true
                    }).one('load', complete).appendTo($loaded);
                    $events.one(event_purge, function() {
                        iframe.src = "//about:blank";
                    });
                    if (settings.get('fastIframe')) {
                        $(iframe).trigger('load');
                    }
                } else {
                    complete();
                }
                if (settings.get('transition') === 'fade') {
                    $box.fadeTo(speed, 1, removeFilter);
                } else {
                    removeFilter();
                }
            };
            if (settings.get('transition') === 'fade') {
                $box.fadeTo(speed, 0, function() {
                    publicMethod.position(0, callback);
                });
            } else {
                publicMethod.position(speed, callback);
            }
        };

        function load() {
            var href, setResize, prep = publicMethod.prep,
                $inline, request = ++requests;
            active = true;
            photo = false;
            trigger(event_purge);
            trigger(event_load);
            settings.get('onLoad');
            settings.h = settings.get('height') ? setSize(settings.get('height'), 'y') - loadedHeight - interfaceHeight : settings.get('innerHeight') && setSize(settings.get('innerHeight'), 'y');
            settings.w = settings.get('width') ? setSize(settings.get('width'), 'x') - loadedWidth - interfaceWidth : settings.get('innerWidth') && setSize(settings.get('innerWidth'), 'x');
            settings.mw = settings.w;
            settings.mh = settings.h;
            if (settings.get('maxWidth')) {
                settings.mw = setSize(settings.get('maxWidth'), 'x') - loadedWidth - interfaceWidth;
                settings.mw = settings.w && settings.w < settings.mw ? settings.w : settings.mw;
            }
            if (settings.get('maxHeight')) {
                settings.mh = setSize(settings.get('maxHeight'), 'y') - loadedHeight - interfaceHeight;
                settings.mh = settings.h && settings.h < settings.mh ? settings.h : settings.mh;
            }
            href = settings.get('href');
            loadingTimer = setTimeout(function() {
                $loadingOverlay.show();
            }, 100);
            if (settings.get('inline')) {
                var $target = $(href);
                $inline = $('<div>').hide().insertBefore($target);
                $events.one(event_purge, function() {
                    $inline.replaceWith($target);
                });
                prep($target);
            } else if (settings.get('iframe')) {
                prep(" ");
            } else if (settings.get('html')) {
                prep(settings.get('html'));
            } else if (isImage(settings, href)) {
                href = retinaUrl(settings, href);
                photo = new Image();
                $(photo).addClass(prefix + 'Photo').bind('error', function() {
                    prep($tag(div, 'Error').html(settings.get('imgError')));
                }).one('load', function() {
                    if (request !== requests) {
                        return;
                    }
                    setTimeout(function() {
                        var percent;
                        $.each(['alt', 'longdesc', 'aria-describedby'], function(i, val) {
                            var attr = $(settings.el).attr(val) || $(settings.el).attr('data-' + val);
                            if (attr) {
                                photo.setAttribute(val, attr);
                            }
                        });
                        if (settings.get('retinaImage') && window.devicePixelRatio > 1) {
                            photo.height = photo.height / window.devicePixelRatio;
                            photo.width = photo.width / window.devicePixelRatio;
                        }
                        if (settings.get('scalePhotos')) {
                            setResize = function() {
                                photo.height -= photo.height * percent;
                                photo.width -= photo.width * percent;
                            };
                            if (settings.mw && photo.width > settings.mw) {
                                percent = (photo.width - settings.mw) / photo.width;
                                setResize();
                            }
                            if (settings.mh && photo.height > settings.mh) {
                                percent = (photo.height - settings.mh) / photo.height;
                                setResize();
                            }
                        }
                        if (settings.h) {
                            photo.style.marginTop = Math.max(settings.mh - photo.height, 0) / 2 + 'px';
                        }
                        if ($related[1] && (settings.get('loop') || $related[index + 1])) {
                            photo.style.cursor = 'pointer';
                            photo.onclick = function() {
                                publicMethod.next();
                            };
                        }
                        photo.style.width = photo.width + 'px';
                        photo.style.height = photo.height + 'px';
                        prep(photo);
                    }, 1);
                });
                photo.src = href;
            } else if (href) {
                $loadingBay.load(href, settings.get('data'), function(data, status) {
                    if (request === requests) {
                        prep(status === 'error' ? $tag(div, 'Error').html(settings.get('xhrError')) : $(this).contents());
                    }
                });
            }
        }

        publicMethod.next = function() {
            if (!active && $related[1] && (settings.get('loop') || $related[index + 1])) {
                index = getIndex(1);
                launch($related[index]);
            }
        };
        publicMethod.prev = function() {
            if (!active && $related[1] && (settings.get('loop') || index)) {
                index = getIndex(-1);
                launch($related[index]);
            }
        };
        publicMethod.close = function() {
            if (open && !closing) {
                closing = true;
                open = false;
                trigger(event_cleanup);
                settings.get('onCleanup');
                $window.unbind('.' + prefix);
                $overlay.fadeTo(settings.get('fadeOut') || 0, 0);
                $box.stop().fadeTo(settings.get('fadeOut') || 0, 0, function() {
                    $box.hide();
                    $overlay.hide();
                    trigger(event_purge);
                    $loaded.remove();
                    setTimeout(function() {
                        closing = false;
                        trigger(event_closed);
                        settings.get('onClosed');
                    }, 1);
                });
            }
        };
        publicMethod.remove = function() {
            if (!$box) {
                return;
            }
            $box.stop();
            $.colorbox.close();
            $box.stop().remove();
            $overlay.remove();
            closing = false;
            $box = null;
            $('.' + boxElement).removeData(colorbox).removeClass(boxElement);
            $(document).unbind('click.' + prefix).unbind('keydown.' + prefix);
        };
        publicMethod.element = function() {
            return $(settings.el);
        };
        publicMethod.settings = defaults;
    }(jQuery, document, window));;
    (function() {
        try {
            window.Logger = {
                send: function() {
                    try {
                        return null;
                    } catch (e) {
                        Raven.captureException(e, {
                            extra: {
                                logger: Logger.send()
                            }
                        });
                        console.error && console.error(e.message);
                    }
                }
            };
        } catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        var initializing = false;
        window.JQClass = function() {};
        JQClass.classes = {};
        JQClass.extend = function extender(prop) {
            var base = this.prototype;
            initializing = true;
            var prototype = new this();
            initializing = false;
            for (var name in prop) {
                prototype[name] = typeof prop[name] == 'function' && typeof base[name] == 'function' ? (function(name, fn) {
                    return function() {
                        var __super = this._super;
                        this._super = function(args) {
                            return base[name].apply(this, args);
                        };
                        var ret = fn.apply(this, arguments);
                        this._super = __super;
                        return ret;
                    };
                })(name, prop[name]) : prop[name];
            }

            function JQClass() {
                if (!initializing && this._init) {
                    this._init.apply(this, arguments);
                }
            }

            JQClass.prototype = prototype;
            JQClass.prototype.constructor = JQClass;
            JQClass.extend = extender;
            return JQClass;
        };
    })();
    (function($) {
        JQClass.classes.JQPlugin = JQClass.extend({
            name: 'plugin',
            defaultOptions: {},
            regionalOptions: {},
            _getters: [],
            _getMarker: function() {
                return 'is-' + this.name;
            },
            _init: function() {
                $.extend(this.defaultOptions, (this.regionalOptions && this.regionalOptions['']) || {});
                var jqName = camelCase(this.name);
                $[jqName] = this;
                $.fn[jqName] = function(options) {
                    var otherArgs = Array.prototype.slice.call(arguments, 1);
                    if ($[jqName]._isNotChained(options, otherArgs)) {
                        return $[jqName][options].apply($[jqName], [this[0]].concat(otherArgs));
                    }
                    return this.each(function() {
                        if (typeof options === 'string') {
                            if (options[0] === '_' || !$[jqName][options]) {
                                throw 'Unknown method: ' + options;
                            }
                            $[jqName][options].apply($[jqName], [this].concat(otherArgs));
                        } else {
                            $[jqName]._attach(this, options);
                        }
                    });
                };
            },
            setDefaults: function(options) {
                $.extend(this.defaultOptions, options || {});
            },
            _isNotChained: function(name, otherArgs) {
                if (name === 'option' && (otherArgs.length === 0 || (otherArgs.length === 1 && typeof otherArgs[0] === 'string'))) {
                    return true;
                }
                return $.inArray(name, this._getters) > -1;
            },
            _attach: function(elem, options) {
                elem = $(elem);
                if (elem.hasClass(this._getMarker())) {
                    return;
                }
                elem.addClass(this._getMarker());
                options = $.extend({}, this.defaultOptions, this._getMetadata(elem), options || {});
                var inst = $.extend({
                    name: this.name,
                    elem: elem,
                    options: options
                }, this._instSettings(elem, options));
                elem.data(this.name, inst);
                this._postAttach(elem, inst);
                this.option(elem, options);
            },
            _instSettings: function(elem, options) {
                return {};
            },
            _postAttach: function(elem, inst) {},
            _getMetadata: function(elem) {
                try {
                    var data = elem.data(this.name.toLowerCase()) || '';
                    data = data.replace(/'/g, '"');
                    data = data.replace(/([a-zA-Z0-9]+):/g, function(match, group, i) {
                        var count = data.substring(0, i).match(/"/g);
                        return (!count || count.length % 2 === 0 ? '"' + group + '":' : group + ':');
                    });
                    data = $.parseJSON('{' + data + '}');
                    for (var name in data) {
                        var value = data[name];
                        if (typeof value === 'string' && value.match(/^new Date\((.*)\)$/)) {
                            data[name] = eval(value);
                        }
                    }
                    return data;
                } catch (e) {
                    return {};
                }
            },
            _getInst: function(elem) {
                return $(elem).data(this.name) || {};
            },
            option: function(elem, name, value) {
                elem = $(elem);
                var inst = elem.data(this.name);
                if (!name || (typeof name === 'string' && value == null)) {
                    var options = (inst || {}).options;
                    return (options && name ? options[name] : options);
                }
                if (!elem.hasClass(this._getMarker())) {
                    return;
                }
                var options = name || {};
                if (typeof name === 'string') {
                    options = {};
                    options[name] = value;
                }
                this._optionsChanged(elem, inst, options);
                $.extend(inst.options, options);
            },
            _optionsChanged: function(elem, inst, options) {},
            destroy: function(elem) {
                elem = $(elem);
                if (!elem.hasClass(this._getMarker())) {
                    return;
                }
                this._preDestroy(elem, this._getInst(elem));
                elem.removeData(this.name).removeClass(this._getMarker());
            },
            _preDestroy: function(elem, inst) {}
        });

        function camelCase(name) {
            return name.replace(/-([a-z])/g, function(match, group) {
                return group.toUpperCase();
            });
        }

        $.JQPlugin = {
            createPlugin: function(superClass, overrides) {
                if (typeof superClass === 'object') {
                    overrides = superClass;
                    superClass = 'JQPlugin';
                }
                superClass = camelCase(superClass);
                var className = camelCase(overrides.name);
                JQClass.classes[className] = JQClass.classes[superClass].extend(overrides);
                new JQClass.classes[className]();
            }
        };
    })(jQuery);
    (function($) {
        var pluginName = 'countdown';
        var Y = 0;
        var O = 1;
        var W = 2;
        var D = 3;
        var H = 4;
        var M = 5;
        var S = 6;
        $.JQPlugin.createPlugin({
            name: pluginName,
            defaultOptions: {
                until: null,
                since: null,
                timezone: null,
                serverSync: null,
                format: 'dHMS',
                layout: '',
                compact: false,
                padZeroes: false,
                significant: 0,
                description: '',
                expiryUrl: '',
                expiryText: '',
                alwaysExpire: false,
                onExpiry: null,
                onTick: null,
                tickInterval: 1
            },
            regionalOptions: {
                '': {
                    labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Minutes', 'Seconds'],
                    labels1: ['Year', 'Month', 'Week', 'Day', 'Hour', 'Minute', 'Second'],
                    compactLabels: ['y', 'm', 'w', 'd'],
                    whichLabels: null,
                    digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
                    timeSeparator: ':',
                    isRTL: false
                }
            },
            _getters: ['getTimes'],
            _rtlClass: pluginName + '-rtl',
            _sectionClass: pluginName + '-section',
            _amountClass: pluginName + '-amount',
            _periodClass: pluginName + '-period',
            _rowClass: pluginName + '-row',
            _holdingClass: pluginName + '-holding',
            _showClass: pluginName + '-show',
            _descrClass: pluginName + '-descr',
            _timerElems: [],
            _init: function() {
                var self = this;
                this._super();
                this._serverSyncs = [];
                var now = (typeof Date.now == 'function' ? Date.now : function() {
                    return new Date().getTime();
                });
                var perfAvail = (window.performance && typeof window.performance.now == 'function');

                function timerCallBack(timestamp) {
                    var drawStart = (timestamp < 1e12 ? (perfAvail ? (performance.now() + performance.timing.navigationStart) : now()) : timestamp || now());
                    if (drawStart - animationStartTime >= 1000) {
                        self._updateElems();
                        animationStartTime = drawStart;
                    }
                    requestAnimationFrame(timerCallBack);
                }

                var requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || null;
                var animationStartTime = 0;
                if (!requestAnimationFrame || $.noRequestAnimationFrame) {
                    $.noRequestAnimationFrame = null;
                    setInterval(function() {
                        self._updateElems();
                    }, 980);
                } else {
                    animationStartTime = window.animationStartTime || window.webkitAnimationStartTime || window.mozAnimationStartTime || window.oAnimationStartTime || window.msAnimationStartTime || now();
                    requestAnimationFrame(timerCallBack);
                }
            },
            UTCDate: function(tz, year, month, day, hours, mins, secs, ms) {
                if (typeof year == 'object' && year.constructor == Date) {
                    ms = year.getMilliseconds();
                    secs = year.getSeconds();
                    mins = year.getMinutes();
                    hours = year.getHours();
                    day = year.getDate();
                    month = year.getMonth();
                    year = year.getFullYear();
                }
                var d = new Date();
                d.setUTCFullYear(year);
                d.setUTCDate(1);
                d.setUTCMonth(month || 0);
                d.setUTCDate(day || 1);
                d.setUTCHours(hours || 0);
                d.setUTCMinutes((mins || 0) - (Math.abs(tz) < 30 ? tz * 60 : tz));
                d.setUTCSeconds(secs || 0);
                d.setUTCMilliseconds(ms || 0);
                return d;
            },
            periodsToSeconds: function(periods) {
                return periods[0] * 31557600 + periods[1] * 2629800 + periods[2] * 604800 +
                    periods[3] * 86400 + periods[4] * 3600 + periods[5] * 60 + periods[6];
            },
            _instSettings: function(elem, options) {
                return {
                    _periods: [0, 0, 0, 0, 0, 0, 0]
                };
            },
            _addElem: function(elem) {
                if (!this._hasElem(elem)) {
                    this._timerElems.push(elem);
                }
            },
            _hasElem: function(elem) {
                return ($.inArray(elem, this._timerElems) > -1);
            },
            _removeElem: function(elem) {
                this._timerElems = $.map(this._timerElems, function(value) {
                    return (value == elem ? null : value);
                });
            },
            _updateElems: function() {
                for (var i = this._timerElems.length - 1; i >= 0; i--) {
                    this._updateCountdown(this._timerElems[i]);
                }
            },
            _optionsChanged: function(elem, inst, options) {
                if (options.layout) {
                    options.layout = options.layout.replace(/&lt;/g, '<').replace(/&gt;/g, '>');
                }
                this._resetExtraLabels(inst.options, options);
                var timezoneChanged = (inst.options.timezone != options.timezone);
                $.extend(inst.options, options);
                this._adjustSettings(elem, inst, options.until != null || options.since != null || timezoneChanged);
                var now = new Date();
                if ((inst._since && inst._since < now) || (inst._until && inst._until > now)) {
                    this._addElem(elem[0]);
                }
                this._updateCountdown(elem, inst);
            },
            _updateCountdown: function(elem, inst) {
                elem = elem.jquery ? elem : $(elem);
                inst = inst || elem.data(this.name);
                if (!inst) {
                    return;
                }
                elem.html(this._generateHTML(inst)).toggleClass(this._rtlClass, inst.options.isRTL);
                if ($.isFunction(inst.options.onTick)) {
                    var periods = inst._hold != 'lap' ? inst._periods : this._calculatePeriods(inst, inst._show, inst.options.significant, new Date());
                    if (inst.options.tickInterval == 1 || this.periodsToSeconds(periods) % inst.options.tickInterval == 0) {
                        inst.options.onTick.apply(elem[0], [periods]);
                    }
                }
                var expired = inst._hold != 'pause' && (inst._since ? inst._now.getTime() < inst._since.getTime() : inst._now.getTime() >= inst._until.getTime());
                if (expired && !inst._expiring) {
                    inst._expiring = true;
                    if (this._hasElem(elem[0]) || inst.options.alwaysExpire) {
                        this._removeElem(elem[0]);
                        if ($.isFunction(inst.options.onExpiry)) {
                            inst.options.onExpiry.apply(elem[0], []);
                        }
                        if (inst.options.expiryText) {
                            var layout = inst.options.layout;
                            inst.options.layout = inst.options.expiryText;
                            this._updateCountdown(elem[0], inst);
                            inst.options.layout = layout;
                        }
                        if (inst.options.expiryUrl) {
                            window.location = inst.options.expiryUrl;
                        }
                    }
                    inst._expiring = false;
                } else if (inst._hold == 'pause') {
                    this._removeElem(elem[0]);
                }
            },
            _resetExtraLabels: function(base, options) {
                var changingLabels = false;
                for (var n in options) {
                    if (n != 'whichLabels' && n.match(/[Ll]abels/)) {
                        changingLabels = true;
                        break;
                    }
                }
                if (changingLabels) {
                    for (var n in base) {
                        if (n.match(/[Ll]abels[02-9]|compactLabels1/)) {
                            base[n] = null;
                        }
                    }
                }
            },
            _adjustSettings: function(elem, inst, recalc) {
                var now;
                var serverOffset = 0;
                var serverEntry = null;
                for (var i = 0; i < this._serverSyncs.length; i++) {
                    if (this._serverSyncs[i][0] == inst.options.serverSync) {
                        serverEntry = this._serverSyncs[i][1];
                        break;
                    }
                }
                if (serverEntry != null) {
                    serverOffset = (inst.options.serverSync ? serverEntry : 0);
                    now = new Date();
                } else {
                    var serverResult = ($.isFunction(inst.options.serverSync) ? inst.options.serverSync.apply(elem[0], []) : null);
                    now = new Date();
                    serverOffset = (serverResult ? now.getTime() - serverResult.getTime() : 0);
                    this._serverSyncs.push([inst.options.serverSync, serverOffset]);
                }
                var timezone = inst.options.timezone;
                timezone = (timezone == null ? -now.getTimezoneOffset() : timezone);
                if (recalc || (!recalc && inst._until == null && inst._since == null)) {
                    inst._since = inst.options.since;
                    if (inst._since != null) {
                        inst._since = this.UTCDate(timezone, this._determineTime(inst._since, null));
                        if (inst._since && serverOffset) {
                            inst._since.setMilliseconds(inst._since.getMilliseconds() + serverOffset);
                        }
                    }
                    inst._until = this.UTCDate(timezone, this._determineTime(inst.options.until, now));
                    if (serverOffset) {
                        inst._until.setMilliseconds(inst._until.getMilliseconds() + serverOffset);
                    }
                }
                inst._show = this._determineShow(inst);
            },
            _preDestroy: function(elem, inst) {
                this._removeElem(elem[0]);
                elem.empty();
            },
            pause: function(elem) {
                this._hold(elem, 'pause');
            },
            lap: function(elem) {
                this._hold(elem, 'lap');
            },
            resume: function(elem) {
                this._hold(elem, null);
            },
            toggle: function(elem) {
                var inst = $.data(elem, this.name) || {};
                this[!inst._hold ? 'pause' : 'resume'](elem);
            },
            toggleLap: function(elem) {
                var inst = $.data(elem, this.name) || {};
                this[!inst._hold ? 'lap' : 'resume'](elem);
            },
            _hold: function(elem, hold) {
                var inst = $.data(elem, this.name);
                if (inst) {
                    if (inst._hold == 'pause' && !hold) {
                        inst._periods = inst._savePeriods;
                        var sign = (inst._since ? '-' : '+');
                        inst[inst._since ? '_since' : '_until'] = this._determineTime(sign + inst._periods[0] + 'y' +
                            sign + inst._periods[1] + 'o' + sign + inst._periods[2] + 'w' +
                            sign + inst._periods[3] + 'd' + sign + inst._periods[4] + 'h' +
                            sign + inst._periods[5] + 'm' + sign + inst._periods[6] + 's');
                        this._addElem(elem);
                    }
                    inst._hold = hold;
                    inst._savePeriods = (hold == 'pause' ? inst._periods : null);
                    $.data(elem, this.name, inst);
                    this._updateCountdown(elem, inst);
                }
            },
            getTimes: function(elem) {
                var inst = $.data(elem, this.name);
                return (!inst ? null : (inst._hold == 'pause' ? inst._savePeriods : (!inst._hold ? inst._periods : this._calculatePeriods(inst, inst._show, inst.options.significant, new Date()))));
            },
            _determineTime: function(setting, defaultTime) {
                var self = this;
                var offsetNumeric = function(offset) {
                    var time = new Date();
                    time.setTime(time.getTime() + offset * 1000);
                    return time;
                };
                var offsetString = function(offset) {
                    offset = offset.toLowerCase();
                    var time = new Date();
                    var year = time.getFullYear();
                    var month = time.getMonth();
                    var day = time.getDate();
                    var hour = time.getHours();
                    var minute = time.getMinutes();
                    var second = time.getSeconds();
                    var pattern = /([+-]?[0-9]+)\s*(s|m|h|d|w|o|y)?/g;
                    var matches = pattern.exec(offset);
                    while (matches) {
                        switch (matches[2] || 's') {
                            case 's':
                                second += parseInt(matches[1], 10);
                                break;
                            case 'm':
                                minute += parseInt(matches[1], 10);
                                break;
                            case 'h':
                                hour += parseInt(matches[1], 10);
                                break;
                            case 'd':
                                day += parseInt(matches[1], 10);
                                break;
                            case 'w':
                                day += parseInt(matches[1], 10) * 7;
                                break;
                            case 'o':
                                month += parseInt(matches[1], 10);
                                day = Math.min(day, self._getDaysInMonth(year, month));
                                break;
                            case 'y':
                                year += parseInt(matches[1], 10);
                                day = Math.min(day, self._getDaysInMonth(year, month));
                                break;
                        }
                        matches = pattern.exec(offset);
                    }
                    return new Date(year, month, day, hour, minute, second, 0);
                };
                var time = (setting == null ? defaultTime : (typeof setting == 'string' ? offsetString(setting) : (typeof setting == 'number' ? offsetNumeric(setting) : setting)));
                if (time) time.setMilliseconds(0);
                return time;
            },
            _getDaysInMonth: function(year, month) {
                return 32 - new Date(year, month, 32).getDate();
            },
            _normalLabels: function(num) {
                return num;
            },
            _generateHTML: function(inst) {
                var self = this;
                inst._periods = (inst._hold ? inst._periods : this._calculatePeriods(inst, inst._show, inst.options.significant, new Date()));
                var shownNonZero = false;
                var showCount = 0;
                var sigCount = inst.options.significant;
                var show = $.extend({}, inst._show);
                for (var period = Y; period <= S; period++) {
                    shownNonZero |= (inst._show[period] == '?' && inst._periods[period] > 0);
                    show[period] = (inst._show[period] == '?' && !shownNonZero ? null : inst._show[period]);
                    showCount += (show[period] ? 1 : 0);
                    sigCount -= (inst._periods[period] > 0 ? 1 : 0);
                }
                var showSignificant = [false, false, false, false, false, false, false];
                for (var period = S; period >= Y; period--) {
                    if (inst._show[period]) {
                        if (inst._periods[period]) {
                            showSignificant[period] = true;
                        } else {
                            showSignificant[period] = sigCount > 0;
                            sigCount--;
                        }
                    }
                }
                var labels = (inst.options.compact ? inst.options.compactLabels : inst.options.labels);
                var whichLabels = inst.options.whichLabels || this._normalLabels;
                var showCompact = function(period) {
                    var labelsNum = inst.options['compactLabels' + whichLabels(inst._periods[period])];
                    return (show[period] ? self._translateDigits(inst, inst._periods[period]) +
                        (labelsNum ? labelsNum[period] : labels[period]) + ' ' : '');
                };
                var minDigits = (inst.options.padZeroes ? 2 : 1);
                var showFull = function(period) {
                    var labelsNum = inst.options['labels' + whichLabels(inst._periods[period])];
                    return ((!inst.options.significant && show[period]) || (inst.options.significant && showSignificant[period]) ? '<span class="' + self._sectionClass + '">' + '<span class="' + self._amountClass + '">' +
                        self._minDigits(inst, inst._periods[period], minDigits) + '</span>' + '<span class="' + self._periodClass + '">' +
                        (labelsNum ? labelsNum[period] : labels[period]) + '</span></span>' : '');
                };
                return (inst.options.layout ? this._buildLayout(inst, show, inst.options.layout, inst.options.compact, inst.options.significant, showSignificant) : ((inst.options.compact ? '<span class="' + this._rowClass + ' ' + this._amountClass +
                        (inst._hold ? ' ' + this._holdingClass : '') + '">' +
                        showCompact(Y) + showCompact(O) + showCompact(W) + showCompact(D) +
                        (show[H] ? this._minDigits(inst, inst._periods[H], 2) : '') +
                        (show[M] ? (show[H] ? inst.options.timeSeparator : '') +
                            this._minDigits(inst, inst._periods[M], 2) : '') +
                        (show[S] ? (show[H] || show[M] ? inst.options.timeSeparator : '') +
                            this._minDigits(inst, inst._periods[S], 2) : '') : '<span class="' + this._rowClass + ' ' + this._showClass + (inst.options.significant || showCount) +
                        (inst._hold ? ' ' + this._holdingClass : '') + '">' +
                        showFull(Y) + showFull(O) + showFull(W) + showFull(D) +
                        showFull(H) + showFull(M) + showFull(S)) + '</span>' +
                    (inst.options.description ? '<span class="' + this._rowClass + ' ' + this._descrClass + '">' +
                        inst.options.description + '</span>' : '')));
            },
            _buildLayout: function(inst, show, layout, compact, significant, showSignificant) {
                var labels = inst.options[compact ? 'compactLabels' : 'labels'];
                var whichLabels = inst.options.whichLabels || this._normalLabels;
                var labelFor = function(index) {
                    return (inst.options[(compact ? 'compactLabels' : 'labels') +
                        whichLabels(inst._periods[index])] || labels)[index];
                };
                var digit = function(value, position) {
                    return inst.options.digits[Math.floor(value / position) % 10];
                };
                var subs = {
                    desc: inst.options.description,
                    sep: inst.options.timeSeparator,
                    yl: labelFor(Y),
                    yn: this._minDigits(inst, inst._periods[Y], 1),
                    ynn: this._minDigits(inst, inst._periods[Y], 2),
                    ynnn: this._minDigits(inst, inst._periods[Y], 3),
                    y1: digit(inst._periods[Y], 1),
                    y10: digit(inst._periods[Y], 10),
                    y100: digit(inst._periods[Y], 100),
                    y1000: digit(inst._periods[Y], 1000),
                    ol: labelFor(O),
                    on: this._minDigits(inst, inst._periods[O], 1),
                    onn: this._minDigits(inst, inst._periods[O], 2),
                    onnn: this._minDigits(inst, inst._periods[O], 3),
                    o1: digit(inst._periods[O], 1),
                    o10: digit(inst._periods[O], 10),
                    o100: digit(inst._periods[O], 100),
                    o1000: digit(inst._periods[O], 1000),
                    wl: labelFor(W),
                    wn: this._minDigits(inst, inst._periods[W], 1),
                    wnn: this._minDigits(inst, inst._periods[W], 2),
                    wnnn: this._minDigits(inst, inst._periods[W], 3),
                    w1: digit(inst._periods[W], 1),
                    w10: digit(inst._periods[W], 10),
                    w100: digit(inst._periods[W], 100),
                    w1000: digit(inst._periods[W], 1000),
                    dl: labelFor(D),
                    dn: this._minDigits(inst, inst._periods[D], 1),
                    dnn: this._minDigits(inst, inst._periods[D], 2),
                    dnnn: this._minDigits(inst, inst._periods[D], 3),
                    d1: digit(inst._periods[D], 1),
                    d10: digit(inst._periods[D], 10),
                    d100: digit(inst._periods[D], 100),
                    d1000: digit(inst._periods[D], 1000),
                    hl: labelFor(H),
                    hn: this._minDigits(inst, inst._periods[H], 1),
                    hnn: this._minDigits(inst, inst._periods[H], 2),
                    hnnn: this._minDigits(inst, inst._periods[H], 3),
                    h1: digit(inst._periods[H], 1),
                    h10: digit(inst._periods[H], 10),
                    h100: digit(inst._periods[H], 100),
                    h1000: digit(inst._periods[H], 1000),
                    ml: labelFor(M),
                    mn: this._minDigits(inst, inst._periods[M], 1),
                    mnn: this._minDigits(inst, inst._periods[M], 2),
                    mnnn: this._minDigits(inst, inst._periods[M], 3),
                    m1: digit(inst._periods[M], 1),
                    m10: digit(inst._periods[M], 10),
                    m100: digit(inst._periods[M], 100),
                    m1000: digit(inst._periods[M], 1000),
                    sl: labelFor(S),
                    sn: this._minDigits(inst, inst._periods[S], 1),
                    snn: this._minDigits(inst, inst._periods[S], 2),
                    snnn: this._minDigits(inst, inst._periods[S], 3),
                    s1: digit(inst._periods[S], 1),
                    s10: digit(inst._periods[S], 10),
                    s100: digit(inst._periods[S], 100),
                    s1000: digit(inst._periods[S], 1000)
                };
                var html = layout;
                for (var i = Y; i <= S; i++) {
                    var period = 'yowdhms'.charAt(i);
                    var re = new RegExp('\\[' + period + '<\\]([\\s\\S]*)\\[' + period + '>\\]', 'g');
                    html = html.replace(re, ((!significant && show[i]) || (significant && showSignificant[i]) ? '$1' : ''));
                }
                $.each(subs, function(n, v) {
                    var re = new RegExp('\\[' + n + '\\]', 'g');
                    html = html.replace(re, v);
                });
                return html;
            },
            _minDigits: function(inst, value, len) {
                value = '' + value;
                if (value.length >= len) {
                    return this._translateDigits(inst, value);
                }
                value = '0000000000' + value;
                return this._translateDigits(inst, value.substr(value.length - len));
            },
            _translateDigits: function(inst, value) {
                return ('' + value).replace(/[0-9]/g, function(digit) {
                    return inst.options.digits[digit];
                });
            },
            _determineShow: function(inst) {
                var format = inst.options.format;
                var show = [];
                show[Y] = (format.match('y') ? '?' : (format.match('Y') ? '!' : null));
                show[O] = (format.match('o') ? '?' : (format.match('O') ? '!' : null));
                show[W] = (format.match('w') ? '?' : (format.match('W') ? '!' : null));
                show[D] = (format.match('d') ? '?' : (format.match('D') ? '!' : null));
                show[H] = (format.match('h') ? '?' : (format.match('H') ? '!' : null));
                show[M] = (format.match('m') ? '?' : (format.match('M') ? '!' : null));
                show[S] = (format.match('s') ? '?' : (format.match('S') ? '!' : null));
                return show;
            },
            _calculatePeriods: function(inst, show, significant, now) {
                inst._now = now;
                inst._now.setMilliseconds(0);
                var until = new Date(inst._now.getTime());
                if (inst._since) {
                    if (now.getTime() < inst._since.getTime()) {
                        inst._now = now = until;
                    } else {
                        now = inst._since;
                    }
                } else {
                    until.setTime(inst._until.getTime());
                    if (now.getTime() > inst._until.getTime()) {
                        inst._now = now = until;
                    }
                }
                var periods = [0, 0, 0, 0, 0, 0, 0];
                if (show[Y] || show[O]) {
                    var lastNow = this._getDaysInMonth(now.getFullYear(), now.getMonth());
                    var lastUntil = this._getDaysInMonth(until.getFullYear(), until.getMonth());
                    var sameDay = (until.getDate() == now.getDate() || (until.getDate() >= Math.min(lastNow, lastUntil) && now.getDate() >= Math.min(lastNow, lastUntil)));
                    var getSecs = function(date) {
                        return (date.getHours() * 60 + date.getMinutes()) * 60 + date.getSeconds();
                    };
                    var months = Math.max(0, (until.getFullYear() - now.getFullYear()) * 12 + until.getMonth() - now.getMonth() +
                        ((until.getDate() < now.getDate() && !sameDay) || (sameDay && getSecs(until) < getSecs(now)) ? -1 : 0));
                    periods[Y] = (show[Y] ? Math.floor(months / 12) : 0);
                    periods[O] = (show[O] ? months - periods[Y] * 12 : 0);
                    now = new Date(now.getTime());
                    var wasLastDay = (now.getDate() == lastNow);
                    var lastDay = this._getDaysInMonth(now.getFullYear() + periods[Y], now.getMonth() + periods[O]);
                    if (now.getDate() > lastDay) {
                        now.setDate(lastDay);
                    }
                    now.setFullYear(now.getFullYear() + periods[Y]);
                    now.setMonth(now.getMonth() + periods[O]);
                    if (wasLastDay) {
                        now.setDate(lastDay);
                    }
                }
                var diff = Math.floor((until.getTime() - now.getTime()) / 1000);
                var extractPeriod = function(period, numSecs) {
                    periods[period] = (show[period] ? Math.floor(diff / numSecs) : 0);
                    diff -= periods[period] * numSecs;
                };
                extractPeriod(W, 604800);
                extractPeriod(D, 86400);
                extractPeriod(H, 3600);
                extractPeriod(M, 60);
                extractPeriod(S, 1);
                if (diff > 0 && !inst._since) {
                    var multiplier = [1, 12, 4.3482, 7, 24, 60, 60];
                    var lastShown = S;
                    var max = 1;
                    for (var period = S; period >= Y; period--) {
                        if (show[period]) {
                            if (periods[lastShown] >= max) {
                                periods[lastShown] = 0;
                                diff = 1;
                            }
                            if (diff > 0) {
                                periods[period]++;
                                diff = 0;
                                lastShown = period;
                                max = 1;
                            }
                        }
                        max *= multiplier[period];
                    }
                }
                if (significant) {
                    for (var period = Y; period <= S; period++) {
                        if (significant && periods[period]) {
                            significant--;
                        } else if (!significant) {
                            periods[period] = 0;
                        }
                    }
                }
                return periods;
            }
        });
    })(jQuery);﻿
    (function($) {
        $.countdown.regionalOptions['ru'] = {
            labels: ['Лет', 'Месяцев', 'Недель', 'Дней', 'Часов', 'Минут', 'Секунд'],
            labels1: ['Год', 'Месяц', 'Неделя', 'День', 'Час', 'Минута', 'Секунда'],
            labels2: ['Года', 'Месяца', 'Недели', 'Дня', 'Часа', 'Минуты', 'Секунды'],
            compactLabels: ['л', 'м', 'н', 'д'],
            compactLabels1: ['г', 'м', 'н', 'д'],
            whichLabels: function(amount) {
                var units = amount % 10;
                var tens = Math.floor((amount % 100) / 10);
                return (amount == 1 ? 1 : (units >= 2 && units <= 4 && tens != 1 ? 2 : (units == 1 && tens != 1 ? 1 : 0)));
            },
            digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
            timeSeparator: ':',
            isRTL: false
        };
        $.countdown.setDefaults($.countdown.regionalOptions['ru']);
    })(jQuery);;;
    (function($) {
        var plugin = {};
        var defaults = {
            mode: 'horizontal',
            slideSelector: '',
            infiniteLoop: true,
            hideControlOnEnd: false,
            speed: 500,
            easing: null,
            slideMargin: 0,
            startSlide: 0,
            randomStart: false,
            captions: false,
            ticker: false,
            tickerHover: false,
            adaptiveHeight: false,
            adaptiveHeightSpeed: 500,
            video: false,
            useCSS: true,
            preloadImages: 'visible',
            responsive: true,
            slideZIndex: 50,
            wrapperClass: 'bx-wrapper',
            touchEnabled: true,
            swipeThreshold: 50,
            oneToOneTouch: true,
            preventDefaultSwipeX: true,
            preventDefaultSwipeY: false,
            pager: true,
            pagerType: 'full',
            pagerShortSeparator: ' / ',
            pagerSelector: null,
            buildPager: null,
            pagerCustom: null,
            controls: true,
            nextText: 'Next',
            prevText: 'Prev',
            nextSelector: null,
            prevSelector: null,
            autoControls: false,
            startText: 'Start',
            stopText: 'Stop',
            autoControlsCombine: false,
            autoControlsSelector: null,
            auto: false,
            pause: 4000,
            autoStart: true,
            autoDirection: 'next',
            autoHover: false,
            autoDelay: 0,
            autoSlideForOnePage: false,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 0,
            slideWidth: 0,
            onSliderLoad: function() {},
            onSlideBefore: function() {},
            onSlideAfter: function() {},
            onSlideNext: function() {},
            onSlidePrev: function() {},
            onSliderResize: function() {}
        }
        $.fn.bxSlider = function(options) {
            if (this.length == 0) return this;
            if (this.length > 1) {
                this.each(function() {
                    $(this).bxSlider(options)
                });
                return this;
            }
            var slider = {};
            var el = this;
            plugin.el = this;
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();
            var init = function() {
                slider.settings = $.extend({}, defaults, options);
                slider.settings.slideWidth = parseInt(slider.settings.slideWidth);
                slider.children = el.children(slider.settings.slideSelector);
                if (slider.children.length < slider.settings.minSlides) slider.settings.minSlides = slider.children.length;
                if (slider.children.length < slider.settings.maxSlides) slider.settings.maxSlides = slider.children.length;
                if (slider.settings.randomStart) slider.settings.startSlide = Math.floor(Math.random() * slider.children.length);
                slider.active = {
                    index: slider.settings.startSlide
                }
                slider.carousel = slider.settings.minSlides > 1 || slider.settings.maxSlides > 1;
                if (slider.carousel) slider.settings.preloadImages = 'all';
                slider.minThreshold = (slider.settings.minSlides * slider.settings.slideWidth) + ((slider.settings.minSlides - 1) * slider.settings.slideMargin);
                slider.maxThreshold = (slider.settings.maxSlides * slider.settings.slideWidth) + ((slider.settings.maxSlides - 1) * slider.settings.slideMargin);
                slider.working = false;
                slider.controls = {};
                slider.interval = null;
                slider.animProp = slider.settings.mode == 'vertical' ? 'top' : 'left';
                slider.usingCSS = slider.settings.useCSS && slider.settings.mode != 'fade' && (function() {
                    var div = document.createElement('div');
                    var props = ['WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
                    for (var i in props) {
                        if (div.style[props[i]] !== undefined) {
                            slider.cssPrefix = props[i].replace('Perspective', '').toLowerCase();
                            slider.animProp = '-' + slider.cssPrefix + '-transform';
                            return true;
                        }
                    }
                    return false;
                }());
                if (slider.settings.mode == 'vertical') slider.settings.maxSlides = slider.settings.minSlides;
                el.data("origStyle", el.attr("style"));
                el.children(slider.settings.slideSelector).each(function() {
                    $(this).data("origStyle", $(this).attr("style"));
                });
                setup();
            }
            var setup = function() {
                el.wrap('<div class="' + slider.settings.wrapperClass + '"><div class="bx-viewport"></div></div>');
                slider.viewport = el.parent();
                slider.loader = $('<div class="bx-loading" />');
                slider.viewport.prepend(slider.loader);
                el.css({
                    width: slider.settings.mode == 'horizontal' ? (slider.children.length * 100 + 215) + '%' : 'auto',
                    position: 'relative'
                });
                if (slider.usingCSS && slider.settings.easing) {
                    el.css('-' + slider.cssPrefix + '-transition-timing-function', slider.settings.easing);
                } else if (!slider.settings.easing) {
                    slider.settings.easing = 'swing';
                }
                var slidesShowing = getNumberSlidesShowing();
                slider.viewport.css({
                    width: '100%',
                    overflow: 'hidden',
                    position: 'relative'
                });
                slider.viewport.parent().css({
                    maxWidth: getViewportMaxWidth()
                });
                if (!slider.settings.pager) {
                    slider.viewport.parent().css({
                        margin: '0 auto 0px'
                    });
                }
                slider.children.css({
                    'float': slider.settings.mode == 'horizontal' ? 'left' : 'none',
                    listStyle: 'none',
                    position: 'relative'
                });
                slider.children.css('width', getSlideWidth());
                if (slider.settings.mode == 'horizontal' && slider.settings.slideMargin > 0) slider.children.css('marginRight', slider.settings.slideMargin);
                if (slider.settings.mode == 'vertical' && slider.settings.slideMargin > 0) slider.children.css('marginBottom', slider.settings.slideMargin);
                if (slider.settings.mode == 'fade') {
                    slider.children.css({
                        position: 'absolute',
                        zIndex: 0,
                        display: 'none'
                    });
                    slider.children.eq(slider.settings.startSlide).css({
                        zIndex: slider.settings.slideZIndex,
                        display: 'block'
                    });
                }
                slider.controls.el = $('<div class="bx-controls" />');
                if (slider.settings.captions) appendCaptions();
                slider.active.last = slider.settings.startSlide == getPagerQty() - 1;
                if (slider.settings.video) el.fitVids();
                var preloadSelector = slider.children.eq(slider.settings.startSlide);
                if (slider.settings.preloadImages == "all") preloadSelector = slider.children;
                if (!slider.settings.ticker) {
                    if (slider.settings.pager) appendPager();
                    if (slider.settings.controls) appendControls();
                    if (slider.settings.auto && slider.settings.autoControls) appendControlsAuto();
                    if (slider.settings.controls || slider.settings.autoControls || slider.settings.pager) slider.viewport.after(slider.controls.el);
                } else {
                    slider.settings.pager = false;
                }
                loadElements(preloadSelector, start);
            }
            var loadElements = function(selector, callback) {
                var total = selector.find('img, iframe').length;
                if (total == 0) {
                    callback();
                    return;
                }
                var count = 0;
                selector.find('img, iframe').each(function() {
                    $(this).one('load', function() {
                        if (++count == total) callback();
                    }).each(function() {
                        if (this.complete) $(this).load();
                    });
                });
            }
            var start = function() {
                if (slider.settings.infiniteLoop && slider.settings.mode != 'fade' && !slider.settings.ticker) {
                    var slice = slider.settings.mode == 'vertical' ? slider.settings.minSlides : slider.settings.maxSlides;
                    var sliceAppend = slider.children.slice(0, slice).clone().addClass('bx-clone');
                    var slicePrepend = slider.children.slice(-slice).clone().addClass('bx-clone');
                    el.append(sliceAppend).prepend(slicePrepend);
                }
                slider.loader.remove();
                setSlidePosition();
                if (slider.settings.mode == 'vertical') slider.settings.adaptiveHeight = true;
                slider.viewport.height(getViewportHeight());
                el.redrawSlider();
                slider.settings.onSliderLoad(slider.active.index);
                slider.initialized = true;
                if (slider.settings.responsive) $(window).bind('resize', resizeWindow);
                if (slider.settings.auto && slider.settings.autoStart && (getPagerQty() > 1 || slider.settings.autoSlideForOnePage)) initAuto();
                if (slider.settings.ticker) initTicker();
                if (slider.settings.pager) updatePagerActive(slider.settings.startSlide);
                if (slider.settings.controls) updateDirectionControls();
                if (slider.settings.touchEnabled && !slider.settings.ticker) initTouch();
            }
            var getViewportHeight = function() {
                var height = 0;
                var children = $();
                if (slider.settings.mode != 'vertical' && !slider.settings.adaptiveHeight) {
                    children = slider.children;
                } else {
                    if (!slider.carousel) {
                        children = slider.children.eq(slider.active.index);
                    } else {
                        var currentIndex = slider.settings.moveSlides == 1 ? slider.active.index : slider.active.index * getMoveBy();
                        children = slider.children.eq(currentIndex);
                        for (i = 1; i <= slider.settings.maxSlides - 1; i++) {
                            if (currentIndex + i >= slider.children.length) {
                                children = children.add(slider.children.eq(i - 1));
                            } else {
                                children = children.add(slider.children.eq(currentIndex + i));
                            }
                        }
                    }
                }
                if (slider.settings.mode == 'vertical') {
                    children.each(function(index) {
                        height += $(this).outerHeight();
                    });
                    if (slider.settings.slideMargin > 0) {
                        height += slider.settings.slideMargin * (slider.settings.minSlides - 1);
                    }
                } else {
                    height = Math.max.apply(Math, children.map(function() {
                        return $(this).outerHeight(false);
                    }).get());
                }
                if (slider.viewport.css('box-sizing') == 'border-box') {
                    height += parseFloat(slider.viewport.css('padding-top')) + parseFloat(slider.viewport.css('padding-bottom')) +
                        parseFloat(slider.viewport.css('border-top-width')) + parseFloat(slider.viewport.css('border-bottom-width'));
                } else if (slider.viewport.css('box-sizing') == 'padding-box') {
                    height += parseFloat(slider.viewport.css('padding-top')) + parseFloat(slider.viewport.css('padding-bottom'));
                }
                return height;
            }
            var getViewportMaxWidth = function() {
                var width = '100%';
                if (slider.settings.slideWidth > 0) {
                    if (slider.settings.mode == 'horizontal') {
                        width = (slider.settings.maxSlides * slider.settings.slideWidth) + ((slider.settings.maxSlides - 1) * slider.settings.slideMargin);
                    } else {
                        width = slider.settings.slideWidth;
                    }
                }
                return width;
            }
            var getSlideWidth = function() {
                var newElWidth = slider.settings.slideWidth;
                var wrapWidth = slider.viewport.width();
                if (slider.settings.slideWidth == 0 || (slider.settings.slideWidth > wrapWidth && !slider.carousel) || slider.settings.mode == 'vertical') {
                    newElWidth = wrapWidth;
                } else if (slider.settings.maxSlides > 1 && slider.settings.mode == 'horizontal') {
                    if (wrapWidth > slider.maxThreshold) {} else if (wrapWidth < slider.minThreshold) {
                        newElWidth = (wrapWidth - (slider.settings.slideMargin * (slider.settings.minSlides - 1))) / slider.settings.minSlides;
                    }
                }
                return newElWidth;
            }
            var getNumberSlidesShowing = function() {
                var slidesShowing = 1;
                if (slider.settings.mode == 'horizontal' && slider.settings.slideWidth > 0) {
                    if (slider.viewport.width() < slider.minThreshold) {
                        slidesShowing = slider.settings.minSlides;
                    } else if (slider.viewport.width() > slider.maxThreshold) {
                        slidesShowing = slider.settings.maxSlides;
                    } else {
                        var childWidth = slider.children.first().width() + slider.settings.slideMargin;
                        slidesShowing = Math.floor((slider.viewport.width() +
                            slider.settings.slideMargin) / childWidth);
                    }
                } else if (slider.settings.mode == 'vertical') {
                    slidesShowing = slider.settings.minSlides;
                }
                return slidesShowing;
            }
            var getPagerQty = function() {
                var pagerQty = 0;
                if (slider.settings.moveSlides > 0) {
                    if (slider.settings.infiniteLoop) {
                        pagerQty = Math.ceil(slider.children.length / getMoveBy());
                    } else {
                        var breakPoint = 0;
                        var counter = 0
                        while (breakPoint < slider.children.length) {
                            ++pagerQty;
                            breakPoint = counter + getNumberSlidesShowing();
                            counter += slider.settings.moveSlides <= getNumberSlidesShowing() ? slider.settings.moveSlides : getNumberSlidesShowing();
                        }
                    }
                } else {
                    pagerQty = Math.ceil(slider.children.length / getNumberSlidesShowing());
                }
                return pagerQty;
            }
            var getMoveBy = function() {
                if (slider.settings.moveSlides > 0 && slider.settings.moveSlides <= getNumberSlidesShowing()) {
                    return slider.settings.moveSlides;
                }
                return getNumberSlidesShowing();
            }
            var setSlidePosition = function() {
                if (slider.children.length > slider.settings.maxSlides && slider.active.last && !slider.settings.infiniteLoop) {
                    if (slider.settings.mode == 'horizontal') {
                        var lastChild = slider.children.last();
                        var position = lastChild.position();
                        setPositionProperty(-(position.left - (slider.viewport.width() - lastChild.outerWidth())), 'reset', 0);
                    } else if (slider.settings.mode == 'vertical') {
                        var lastShowingIndex = slider.children.length - slider.settings.minSlides;
                        var position = slider.children.eq(lastShowingIndex).position();
                        setPositionProperty(-position.top, 'reset', 0);
                    }
                } else {
                    var position = slider.children.eq(slider.active.index * getMoveBy()).position();
                    if (slider.active.index == getPagerQty() - 1) slider.active.last = true;
                    if (position != undefined) {
                        if (slider.settings.mode == 'horizontal') setPositionProperty(-position.left, 'reset', 0);
                        else if (slider.settings.mode == 'vertical') setPositionProperty(-position.top, 'reset', 0);
                    }
                }
            }
            var setPositionProperty = function(value, type, duration, params) {
                if (slider.usingCSS) {
                    var propValue = slider.settings.mode == 'vertical' ? 'translate3d(0, ' + value + 'px, 0)' : 'translate3d(' + value + 'px, 0, 0)';
                    el.css('-' + slider.cssPrefix + '-transition-duration', duration / 1000 + 's');
                    if (type == 'slide') {
                        el.css(slider.animProp, propValue);
                        el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
                            el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
                            updateAfterSlideTransition();
                        });
                    } else if (type == 'reset') {
                        el.css(slider.animProp, propValue);
                    } else if (type == 'ticker') {
                        el.css('-' + slider.cssPrefix + '-transition-timing-function', 'linear');
                        el.css(slider.animProp, propValue);
                        el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
                            el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
                            setPositionProperty(params['resetValue'], 'reset', 0);
                            tickerLoop();
                        });
                    }
                } else {
                    var animateObj = {};
                    animateObj[slider.animProp] = value;
                    if (type == 'slide') {
                        el.animate(animateObj, duration, slider.settings.easing, function() {
                            updateAfterSlideTransition();
                        });
                    } else if (type == 'reset') {
                        el.css(slider.animProp, value)
                    } else if (type == 'ticker') {
                        el.animate(animateObj, speed, 'linear', function() {
                            setPositionProperty(params['resetValue'], 'reset', 0);
                            tickerLoop();
                        });
                    }
                }
            }
            var populatePager = function() {
                var pagerHtml = '';
                var pagerQty = getPagerQty();
                for (var i = 0; i < pagerQty; i++) {
                    var linkContent = '';
                    if (slider.settings.buildPager && $.isFunction(slider.settings.buildPager)) {
                        linkContent = slider.settings.buildPager(i);
                        slider.pagerEl.addClass('bx-custom-pager');
                    } else {
                        linkContent = i + 1;
                        slider.pagerEl.addClass('bx-default-pager');
                    }
                    pagerHtml += '<div class="bx-pager-item"><a href="" data-slide-index="' + i + '" class="bx-pager-link">' + linkContent + '</a></div>';
                };
                slider.pagerEl.html(pagerHtml);
            }
            var appendPager = function() {
                if (!slider.settings.pagerCustom) {
                    slider.pagerEl = $('<div class="bx-pager" />');
                    if (slider.settings.pagerSelector) {
                        $(slider.settings.pagerSelector).html(slider.pagerEl);
                    } else {
                        slider.controls.el.addClass('bx-has-pager').append(slider.pagerEl);
                    }
                    populatePager();
                } else {
                    slider.pagerEl = $(slider.settings.pagerCustom);
                }
                slider.pagerEl.on('click', 'a', clickPagerBind);
            }
            var appendControls = function() {
                slider.controls.next = $('<a class="bx-next" href="">' + slider.settings.nextText + '</a>');
                slider.controls.prev = $('<a class="bx-prev" href="">' + slider.settings.prevText + '</a>');
                slider.controls.next.bind('click', clickNextBind);
                slider.controls.prev.bind('click', clickPrevBind);
                if (slider.settings.nextSelector) {
                    $(slider.settings.nextSelector).append(slider.controls.next);
                }
                if (slider.settings.prevSelector) {
                    $(slider.settings.prevSelector).append(slider.controls.prev);
                }
                if (!slider.settings.nextSelector && !slider.settings.prevSelector) {
                    slider.controls.directionEl = $('<div class="bx-controls-direction" />');
                    slider.controls.directionEl.append(slider.controls.prev).append(slider.controls.next);
                    slider.controls.el.addClass('bx-has-controls-direction').append(slider.controls.directionEl);
                }
            }
            var appendControlsAuto = function() {
                slider.controls.start = $('<div class="bx-controls-auto-item"><a class="bx-start" href="">' + slider.settings.startText + '</a></div>');
                slider.controls.stop = $('<div class="bx-controls-auto-item"><a class="bx-stop" href="">' + slider.settings.stopText + '</a></div>');
                slider.controls.autoEl = $('<div class="bx-controls-auto" />');
                slider.controls.autoEl.on('click', '.bx-start', clickStartBind);
                slider.controls.autoEl.on('click', '.bx-stop', clickStopBind);
                if (slider.settings.autoControlsCombine) {
                    slider.controls.autoEl.append(slider.controls.start);
                } else {
                    slider.controls.autoEl.append(slider.controls.start).append(slider.controls.stop);
                }
                if (slider.settings.autoControlsSelector) {
                    $(slider.settings.autoControlsSelector).html(slider.controls.autoEl);
                } else {
                    slider.controls.el.addClass('bx-has-controls-auto').append(slider.controls.autoEl);
                }
                updateAutoControls(slider.settings.autoStart ? 'stop' : 'start');
            }
            var appendCaptions = function() {
                slider.children.each(function(index) {
                    var title = $(this).find('img:first').attr('title');
                    if (title != undefined && ('' + title).length) {
                        $(this).append('<div class="bx-caption"><span>' + title + '</span></div>');
                    }
                });
            }
            var clickNextBind = function(e) {
                if (slider.settings.auto) el.stopAuto();
                el.goToNextSlide();
                e.preventDefault();
            }
            var clickPrevBind = function(e) {
                if (slider.settings.auto) el.stopAuto();
                el.goToPrevSlide();
                e.preventDefault();
            }
            var clickStartBind = function(e) {
                el.startAuto();
                e.preventDefault();
            }
            var clickStopBind = function(e) {
                el.stopAuto();
                e.preventDefault();
            }
            var clickPagerBind = function(e) {
                if (slider.settings.auto) el.stopAuto();
                var pagerLink = $(e.currentTarget);
                if (pagerLink.attr('data-slide-index') !== undefined) {
                    var pagerIndex = parseInt(pagerLink.attr('data-slide-index'));
                    if (pagerIndex != slider.active.index) el.goToSlide(pagerIndex);
                    e.preventDefault();
                }
            }
            var updatePagerActive = function(slideIndex) {
                var len = slider.children.length;
                if (slider.settings.pagerType == 'short') {
                    if (slider.settings.maxSlides > 1) {
                        len = Math.ceil(slider.children.length / slider.settings.maxSlides);
                    }
                    slider.pagerEl.html((slideIndex + 1) + slider.settings.pagerShortSeparator + len);
                    return;
                }
                slider.pagerEl.find('a').removeClass('active');
                slider.pagerEl.each(function(i, el) {
                    $(el).find('a').eq(slideIndex).addClass('active');
                });
            }
            var updateAfterSlideTransition = function() {
                if (slider.settings.infiniteLoop) {
                    var position = '';
                    if (slider.active.index == 0) {
                        position = slider.children.eq(0).position();
                    } else if (slider.active.index == getPagerQty() - 1 && slider.carousel) {
                        position = slider.children.eq((getPagerQty() - 1) * getMoveBy()).position();
                    } else if (slider.active.index == slider.children.length - 1) {
                        position = slider.children.eq(slider.children.length - 1).position();
                    }
                    if (position) {
                        if (slider.settings.mode == 'horizontal') {
                            setPositionProperty(-position.left, 'reset', 0);
                        } else if (slider.settings.mode == 'vertical') {
                            setPositionProperty(-position.top, 'reset', 0);
                        }
                    }
                }
                slider.working = false;
                slider.settings.onSlideAfter(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
            }
            var updateAutoControls = function(state) {
                if (slider.settings.autoControlsCombine) {
                    slider.controls.autoEl.html(slider.controls[state]);
                } else {
                    slider.controls.autoEl.find('a').removeClass('active');
                    slider.controls.autoEl.find('a:not(.bx-' + state + ')').addClass('active');
                }
            }
            var updateDirectionControls = function() {
                if (getPagerQty() == 1) {
                    slider.controls.prev.addClass('disabled');
                    slider.controls.next.addClass('disabled');
                } else if (!slider.settings.infiniteLoop && slider.settings.hideControlOnEnd) {
                    if (slider.active.index == 0) {
                        slider.controls.prev.addClass('disabled');
                        slider.controls.next.removeClass('disabled');
                    } else if (slider.active.index == getPagerQty() - 1) {
                        slider.controls.next.addClass('disabled');
                        slider.controls.prev.removeClass('disabled');
                    } else {
                        slider.controls.prev.removeClass('disabled');
                        slider.controls.next.removeClass('disabled');
                    }
                }
            }
            var initAuto = function() {
                if (slider.settings.autoDelay > 0) {
                    var timeout = setTimeout(el.startAuto, slider.settings.autoDelay);
                } else {
                    el.startAuto();
                }
                if (slider.settings.autoHover) {
                    el.hover(function() {
                        if (slider.interval) {
                            el.stopAuto(true);
                            slider.autoPaused = true;
                        }
                    }, function() {
                        if (slider.autoPaused) {
                            el.startAuto(true);
                            slider.autoPaused = null;
                        }
                    });
                }
            }
            var initTicker = function() {
                var startPosition = 0;
                if (slider.settings.autoDirection == 'next') {
                    el.append(slider.children.clone().addClass('bx-clone'));
                } else {
                    el.prepend(slider.children.clone().addClass('bx-clone'));
                    var position = slider.children.first().position();
                    startPosition = slider.settings.mode == 'horizontal' ? -position.left : -position.top;
                }
                setPositionProperty(startPosition, 'reset', 0);
                slider.settings.pager = false;
                slider.settings.controls = false;
                slider.settings.autoControls = false;
                if (slider.settings.tickerHover && !slider.usingCSS) {
                    slider.viewport.hover(function() {
                        el.stop();
                    }, function() {
                        var totalDimens = 0;
                        slider.children.each(function(index) {
                            totalDimens += slider.settings.mode == 'horizontal' ? $(this).outerWidth(true) : $(this).outerHeight(true);
                        });
                        var ratio = slider.settings.speed / totalDimens;
                        var property = slider.settings.mode == 'horizontal' ? 'left' : 'top';
                        var newSpeed = ratio * (totalDimens - (Math.abs(parseInt(el.css(property)))));
                        tickerLoop(newSpeed);
                    });
                }
                tickerLoop();
            }
            var tickerLoop = function(resumeSpeed) {
                speed = resumeSpeed ? resumeSpeed : slider.settings.speed;
                var position = {
                    left: 0,
                    top: 0
                };
                var reset = {
                    left: 0,
                    top: 0
                };
                if (slider.settings.autoDirection == 'next') {
                    position = el.find('.bx-clone').first().position();
                } else {
                    reset = slider.children.first().position();
                }
                var animateProperty = slider.settings.mode == 'horizontal' ? -position.left : -position.top;
                var resetValue = slider.settings.mode == 'horizontal' ? -reset.left : -reset.top;
                var params = {
                    resetValue: resetValue
                };
                setPositionProperty(animateProperty, 'ticker', speed, params);
            }
            var initTouch = function() {
                slider.touch = {
                    start: {
                        x: 0,
                        y: 0
                    },
                    end: {
                        x: 0,
                        y: 0
                    }
                }
                slider.viewport.bind('touchstart', onTouchStart);
            }
            var onTouchStart = function(e) {
                if (slider.working) {
                    e.preventDefault();
                } else {
                    slider.touch.originalPos = el.position();
                    var orig = e.originalEvent;
                    slider.touch.start.x = orig.changedTouches[0].pageX;
                    slider.touch.start.y = orig.changedTouches[0].pageY;
                    slider.viewport.bind('touchmove', onTouchMove);
                    slider.viewport.bind('touchend', onTouchEnd);
                }
            }
            var onTouchMove = function(e) {
                var orig = e.originalEvent;
                var xMovement = Math.abs(orig.changedTouches[0].pageX - slider.touch.start.x);
                var yMovement = Math.abs(orig.changedTouches[0].pageY - slider.touch.start.y);
                if ((xMovement * 3) > yMovement && slider.settings.preventDefaultSwipeX) {
                    e.preventDefault();
                } else if ((yMovement * 3) > xMovement && slider.settings.preventDefaultSwipeY) {
                    e.preventDefault();
                }
                if (slider.settings.mode != 'fade' && slider.settings.oneToOneTouch) {
                    var value = 0;
                    if (slider.settings.mode == 'horizontal') {
                        var change = orig.changedTouches[0].pageX - slider.touch.start.x;
                        value = slider.touch.originalPos.left + change;
                    } else {
                        var change = orig.changedTouches[0].pageY - slider.touch.start.y;
                        value = slider.touch.originalPos.top + change;
                    }
                    setPositionProperty(value, 'reset', 0);
                }
            }
            var onTouchEnd = function(e) {
                slider.viewport.unbind('touchmove', onTouchMove);
                var orig = e.originalEvent;
                var value = 0;
                slider.touch.end.x = orig.changedTouches[0].pageX;
                slider.touch.end.y = orig.changedTouches[0].pageY;
                if (slider.settings.mode == 'fade') {
                    var distance = Math.abs(slider.touch.start.x - slider.touch.end.x);
                    if (distance >= slider.settings.swipeThreshold) {
                        slider.touch.start.x > slider.touch.end.x ? el.goToNextSlide() : el.goToPrevSlide();
                        el.stopAuto();
                    }
                } else {
                    var distance = 0;
                    if (slider.settings.mode == 'horizontal') {
                        distance = slider.touch.end.x - slider.touch.start.x;
                        value = slider.touch.originalPos.left;
                    } else {
                        distance = slider.touch.end.y - slider.touch.start.y;
                        value = slider.touch.originalPos.top;
                    }
                    if (!slider.settings.infiniteLoop && ((slider.active.index == 0 && distance > 0) || (slider.active.last && distance < 0))) {
                        setPositionProperty(value, 'reset', 200);
                    } else {
                        if (Math.abs(distance) >= slider.settings.swipeThreshold) {
                            distance < 0 ? el.goToNextSlide() : el.goToPrevSlide();
                            el.stopAuto();
                        } else {
                            setPositionProperty(value, 'reset', 200);
                        }
                    }
                }
                slider.viewport.unbind('touchend', onTouchEnd);
            }
            var resizeWindow = function(e) {
                if (!slider.initialized) return;
                var windowWidthNew = $(window).width();
                var windowHeightNew = $(window).height();
                if (windowWidth != windowWidthNew || windowHeight != windowHeightNew) {
                    windowWidth = windowWidthNew;
                    windowHeight = windowHeightNew;
                    el.redrawSlider();
                    slider.settings.onSliderResize.call(el, slider.active.index);
                }
            }
            el.goToSlide = function(slideIndex, direction) {
                if (slider.working || slider.active.index == slideIndex) return;
                slider.working = true;
                slider.oldIndex = slider.active.index;
                if (slideIndex < 0) {
                    slider.active.index = getPagerQty() - 1;
                } else if (slideIndex >= getPagerQty()) {
                    slider.active.index = 0;
                } else {
                    slider.active.index = slideIndex;
                }
                slider.settings.onSlideBefore(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
                if (direction == 'next') {
                    slider.settings.onSlideNext(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
                } else if (direction == 'prev') {
                    slider.settings.onSlidePrev(slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
                }
                slider.active.last = slider.active.index >= getPagerQty() - 1;
                if (slider.settings.pager) updatePagerActive(slider.active.index);
                if (slider.settings.controls) updateDirectionControls();
                if (slider.settings.mode == 'fade') {
                    if (slider.settings.adaptiveHeight && slider.viewport.height() != getViewportHeight()) {
                        slider.viewport.animate({
                            height: getViewportHeight()
                        }, slider.settings.adaptiveHeightSpeed);
                    }
                    slider.children.filter(':visible').fadeOut(slider.settings.speed).css({
                        zIndex: 0
                    });
                    slider.children.eq(slider.active.index).css('zIndex', slider.settings.slideZIndex + 1).fadeIn(slider.settings.speed, function() {
                        $(this).css('zIndex', slider.settings.slideZIndex);
                        updateAfterSlideTransition();
                    });
                } else {
                    if (slider.settings.adaptiveHeight && slider.viewport.height() != getViewportHeight()) {
                        slider.viewport.animate({
                            height: getViewportHeight()
                        }, slider.settings.adaptiveHeightSpeed);
                    }
                    var moveBy = 0;
                    var position = {
                        left: 0,
                        top: 0
                    };
                    if (!slider.settings.infiniteLoop && slider.carousel && slider.active.last) {
                        if (slider.settings.mode == 'horizontal') {
                            var lastChild = slider.children.eq(slider.children.length - 1);
                            position = lastChild.position();
                            moveBy = slider.viewport.width() - lastChild.outerWidth();
                        } else {
                            var lastShowingIndex = slider.children.length - slider.settings.minSlides;
                            position = slider.children.eq(lastShowingIndex).position();
                        }
                    } else if (slider.carousel && slider.active.last && direction == 'prev') {
                        var eq = slider.settings.moveSlides == 1 ? slider.settings.maxSlides - getMoveBy() : ((getPagerQty() - 1) * getMoveBy()) - (slider.children.length - slider.settings.maxSlides);
                        var lastChild = el.children('.bx-clone').eq(eq);
                        position = lastChild.position();
                    } else if (direction == 'next' && slider.active.index == 0) {
                        position = el.find('> .bx-clone').eq(slider.settings.maxSlides).position();
                        slider.active.last = false;
                    } else if (slideIndex >= 0) {
                        var requestEl = slideIndex * getMoveBy();
                        position = slider.children.eq(requestEl).position();
                    }
                    if ("undefined" !== typeof(position)) {
                        var value = slider.settings.mode == 'horizontal' ? -(position.left - moveBy) : -position.top;
                        setPositionProperty(value, 'slide', slider.settings.speed);
                    }
                }
            }
            el.goToNextSlide = function() {
                if (!slider.settings.infiniteLoop && slider.active.last) return;
                var pagerIndex = parseInt(slider.active.index) + 1;
                el.goToSlide(pagerIndex, 'next');
            }
            el.goToPrevSlide = function() {
                if (!slider.settings.infiniteLoop && slider.active.index == 0) return;
                var pagerIndex = parseInt(slider.active.index) - 1;
                el.goToSlide(pagerIndex, 'prev');
            }
            el.startAuto = function(preventControlUpdate) {
                if (slider.interval) return;
                slider.interval = setInterval(function() {
                    slider.settings.autoDirection == 'next' ? el.goToNextSlide() : el.goToPrevSlide();
                }, slider.settings.pause);
                if (slider.settings.autoControls && preventControlUpdate != true) updateAutoControls('stop');
            }
            el.stopAuto = function(preventControlUpdate) {
                if (!slider.interval) return;
                clearInterval(slider.interval);
                slider.interval = null;
                if (slider.settings.autoControls && preventControlUpdate != true) updateAutoControls('start');
            }
            el.getCurrentSlide = function() {
                return slider.active.index;
            }
            el.getCurrentSlideElement = function() {
                return slider.children.eq(slider.active.index);
            }
            el.getSlideCount = function() {
                return slider.children.length;
            }
            el.redrawSlider = function() {
                slider.children.add(el.find('.bx-clone')).width(getSlideWidth());
                slider.viewport.css('height', getViewportHeight());
                if (!slider.settings.ticker) setSlidePosition();
                if (slider.active.last) slider.active.index = getPagerQty() - 1;
                if (slider.active.index >= getPagerQty()) slider.active.last = true;
                if (slider.settings.pager && !slider.settings.pagerCustom) {
                    populatePager();
                    updatePagerActive(slider.active.index);
                }
            }
            el.destroySlider = function() {
                if (!slider.initialized) return;
                slider.initialized = false;
                $('.bx-clone', this).remove();
                slider.children.each(function() {
                    $(this).data("origStyle") != undefined ? $(this).attr("style", $(this).data("origStyle")) : $(this).removeAttr('style');
                });
                $(this).data("origStyle") != undefined ? this.attr("style", $(this).data("origStyle")) : $(this).removeAttr('style');
                $(this).unwrap().unwrap();
                if (slider.controls.el) slider.controls.el.remove();
                if (slider.controls.next) slider.controls.next.remove();
                if (slider.controls.prev) slider.controls.prev.remove();
                if (slider.pagerEl && slider.settings.controls) slider.pagerEl.remove();
                $('.bx-caption', this).remove();
                if (slider.controls.autoEl) slider.controls.autoEl.remove();
                clearInterval(slider.interval);
                if (slider.settings.responsive) $(window).unbind('resize', resizeWindow);
            }
            el.reloadSlider = function(settings) {
                if (settings != undefined) options = settings;
                el.destroySlider();
                init();
            }
            init();
            return this;
        }
    })(jQuery);;
    (function() {
        try {
            var outDay;
            outDay = function(theirDay) {
                try {
                    if (theirDay > 0) {
                        return theirDay - 1;
                    }
                    if (theirDay === 0) {
                        return 6;
                    }
                } catch (e) {
                    Raven.captureException(e, {
                        extra: {
                            logger: Logger.send()
                        }
                    });
                    console.error && console.error(e.message);
                }
            };
            $(function() {
                try {
                    return $('.section-countdown').each(function() {
                        try {
                            var $cd, add, date, hash, now, settings;
                            $cd = $(this).find('> .wrapper > .container > .text-center > .countdown');
                            settings = $cd.data('vals');
                            now = new Date();
                            if (settings.type === 'full') {
                                settings.full_date.day++;
                                date = new Date(settings.full_date.year, settings.full_date.month, settings.full_date.day, settings.full_time.hour, settings.full_time.minute);
                            } else if (settings.type === 'month') {
                                settings.month_day++;
                                date = new Date(now.getFullYear(), now.getMonth(), settings.month_day, settings.month_time.hour, settings.month_time.minute);
                                if (date.getTime() < now.getTime()) {
                                    date = new Date(now.getFullYear(), now.getMonth() + 1, settings.month_day, settings.month_time.hour, settings.month_time.minute);
                                }
                            } else if (settings.type === 'week') {
                                add = settings.week_day - outDay(now.getDay());
                                date = new Date(now.getFullYear(), now.getMonth(), now.getDate() + add, settings.week_time.hour, settings.week_time.minute);
                                if (date.getTime() < now.getTime()) {
                                    add = 7 - outDay(now.getDay()) + settings.week_day;
                                    date = new Date(now.getFullYear(), now.getMonth(), now.getDate() + add, settings.week_time.hour, settings.week_time.minute);
                                }
                            } else if (settings.type === 'day') {
                                date = new Date(now.getFullYear(), now.getMonth(), now.getDate(), settings.day_time.hour, settings.day_time.minute);
                                if (date.getTime() < now.getTime()) {
                                    date = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, settings.day_time.hour, settings.day_time.minute);
                                }
                            } else if (settings.type === 'fake') {
                                hash = [settings.fake_days, settings.fake_time.hour, settings.fake_time.minute].join(':');
                                if ($.cookie(hash)) {
                                    date = new Date($.cookie(hash) * 1);
                                } else {
                                    date = new Date(now.getFullYear(), now.getMonth(), now.getDate() + settings.fake_days, now.getHours() + settings.fake_time.hour, now.getMinutes() + settings.fake_time.minute);
                                    $.cookie(hash, date.getTime(), {
                                        expires: 365,
                                        path: '/'
                                    });
                                }
                            }
                            return $cd.countdown({
                                until: date,
                                compact: true,
                                layout: $cd.html()
                            });
                        } catch (e) {
                            Raven.captureException(e, {
                                extra: {
                                    logger: Logger.send()
                                }
                            });
                            console.error && console.error(e.message);
                        }
                    });
                } catch (e) {
                    Raven.captureException(e, {
                        extra: {
                            logger: Logger.send()
                        }
                    });
                    console.error && console.error(e.message);
                }
            });
        } catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {
            $(function() {
                try {
                    return $('.section-slider').each(function() {
                        try {
                            var $root, $slider;
                            $root = $(this).find('> .wrapper > .container > .incon > .wrap');
                            $slider = $root.find('> .bxslider');
                            $slider.bxSlider({
                                slideSelector: '.slide',
                                auto: true,
                                pause: $slider.data('pause') * 1
                            });
                            $root.find('> .right').on('click', function() {
                                try {
                                    return $slider.goToNextSlide();
                                } catch (e) {
                                    Raven.captureException(e, {
                                        extra: {
                                            logger: Logger.send()
                                        }
                                    });
                                    console.error && console.error(e.message);
                                }
                            });
                            return $root.find('> .left').on('click', function() {
                                try {
                                    return $slider.goToPrevSlide();
                                } catch (e) {
                                    Raven.captureException(e, {
                                        extra: {
                                            logger: Logger.send()
                                        }
                                    });
                                    console.error && console.error(e.message);
                                }
                            });
                        } catch (e) {
                            Raven.captureException(e, {
                                extra: {
                                    logger: Logger.send()
                                }
                            });
                            console.error && console.error(e.message);
                        }
                    });
                } catch (e) {
                    Raven.captureException(e, {
                        extra: {
                            logger: Logger.send()
                        }
                    });
                    console.error && console.error(e.message);
                }
            });
        } catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {
            $(function() {
                try {
                    return $('.widget-button').each(function() {
                        try {
                            var $section, $sectionsNext, $sectionsPrev, $this, action;
                            $this = $(this);
                            $section = $this.closest('.section');
                            $sectionsPrev = $section.prevAll('.section');
                            $sectionsNext = $section.nextAll('.section');
                            action = $this.data('action');
                            if (action === 'formscroll') {
                                return $this.find('.btn').on('click', function() {
                                    try {
                                        var $formCurrent, $formNext, $formPrev;
                                        $formNext = $sectionsNext.find('.form:visible').first();
                                        if ($formNext.length === 1) {
                                            return $('body').animate({
                                                scrollTop: $formNext.offset().top - 100
                                            }, {
                                                duration: 1500,
                                                easing: 'easeInOutQuint'
                                            });
                                        }
                                        $formCurrent = $section.find('.form:visible').first();
                                        if ($formCurrent.length === 1) {
                                            return $('body').animate({
                                                scrollTop: $formCurrent.offset().top - 100
                                            }, {
                                                duration: 1500,
                                                easing: 'easeInOutQuint'
                                            });
                                        }
                                        $formPrev = $sectionsPrev.find('.form:visible').last();
                                        if ($formPrev.length === 1) {
                                            return $('body').animate({
                                                scrollTop: $formPrev.offset().top - 100
                                            }, {
                                                duration: 1500,
                                                easing: 'easeInOutQuint'
                                            });
                                        }
                                    } catch (e) {
                                        Raven.captureException(e, {
                                            extra: {
                                                logger: Logger.send()
                                            }
                                        });
                                        console.error && console.error(e.message);
                                    }
                                });
                            }
                        } catch (e) {
                            Raven.captureException(e, {
                                extra: {
                                    logger: Logger.send()
                                }
                            });
                            console.error && console.error(e.message);
                        }
                    });
                } catch (e) {
                    Raven.captureException(e, {
                        extra: {
                            logger: Logger.send()
                        }
                    });
                    console.error && console.error(e.message);
                }
            });
        } catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;
    (function() {
        try {} catch (e) {
            Raven.captureException(e, {
                extra: {
                    logger: Logger.send()
                }
            });
            console.error && console.error(e.message);
        }
    }.call(this));;