$(function () {
	function makeTabs(contId) {
    	var tabContainers = $('#'+contId+' div.tabs > div');
    	tabContainers.hide().filter(':first').show();
    
    	$('#'+contId+' div.tabs ul.tabNavigation a').click(function () {
        	tabContainers.hide();
        	tabContainers.filter(this.hash).show();
        	$('#'+contId+' div.tabs ul.tabNavigation a').removeClass('selected');
        	$(this).addClass('selected');
        	return false;
    	}).filter(':first').click();
    }
    makeTabs('tabs-1');
});