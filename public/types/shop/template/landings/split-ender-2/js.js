if (callhunterON != undefined && ctd24ON != undefined && ctd60ON != undefined && ctdtestON != undefined && ctdspanON != undefined && anchor != undefined) {
	var callhunterON;
	var ctd24ON;
	var ctdtestON;
	var ctd60ON;
	var ctdspanON;
	var anchor; 
}


/* ========== СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ДНЯ ========== */

function get_timer24(d){
	var date_new = d; 
	var date_t = new Date(date_new);
	var date = new Date();
	var timer = date_t - date;
	if(date_t > date) {
		var day = parseInt(timer/(60*60*1000*24));
		if(day < 10) {
			day = "<span class='day'><span class='day day-ten'>0</span><span class='day-unit'>" + day + "</span></span>";
		} else {
			day = day.toString();
			day = "<span class='day'><span class='day day-ten'>"+day[0]+"</span><span class='day-unit'>"+day[1]+"</span></span>";
		}
		var hour = parseInt(timer/(60*60*1000))%24;
		if(hour < 10) {
			hour = "<span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>" + hour + "</span></span>";
		} else {
			hour = hour.toString();
			hour = "<span class='hour'><span class='hour-ten'>"+hour[0]+"</span><span class='hour-unit'>"+hour[1]+"</span></span>";
		}
		var min = parseInt(timer/(1000*60))%60;
		if(min < 10) {
			min = "<span class='min'><span class='min-ten'>0</span><span class='min-unit'>" + min + "</span></span>";
		} else {
			min = min.toString();
			min = "<span class='min'><span class='min-ten'>"+min[0]+"</span><span class='min-unit'>"+min[1]+"</span></span>";
		}
		var sec = parseInt(timer/1000)%60;
		if(sec < 10) {
			sec = "<span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>" + sec + "</span></span>";
		} else {
			sec = sec.toString();
			sec = "<span class='sec'><span class='sec-ten'>"+sec[0]+"</span><span class='sec-unit'>"+sec[1]+"</span></span>";
		}
		var ctd24 = day+" "+hour+" "+min+" "+sec;
		$(".ctd24").html(ctd24);
	} else {
		$(".ctd24").html("<span class='day'><span class='day-ten'>0</span><span class='day-unit'>0</span></span> <span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>0</span></span> <span class='min'><span class='min-ten'>0</span><span class='min-unit'>0</span></span> <span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>0</span></span>");
	}
}
function getfrominputs24(){	
	var d = new Date();
	d.setHours(23,59,59,0);	
	get_timer24(d);
	setInterval(function(){
		get_timer24(d);
	},1000);
}
$(document).ready(function(){ 
	if (ctd24ON == true) {
		getfrominputs24(); 
	}
});

/* ========== /СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ДНЯ ========== */



/* ========== СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ЧАСА ========== */

function get_timer60(d){
	var date_new = d; 
	var date_t = new Date(date_new);
	var date = new Date();
	var timer = date_t - date;
	if(date_t > date) {
		var day = parseInt(timer/(60*60*1000*24));
		if(day < 10) {
			day = "<span class='day'><span class='day day-ten'>0</span><span class='day-unit'>" + day + "</span></span>";
		} else {
			day = day.toString();
			day = "<span class='day'><span class='day day-ten'>"+day[0]+"</span><span class='day-unit'>"+day[1]+"</span></span>";
		}
		var hour = parseInt(timer/(60*60*1000))%24;
		if(hour < 10) {
			hour = "<span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>" + hour + "</span></span>";
		} else {
			hour = hour.toString();
			hour = "<span class='hour'><span class='hour-ten'>"+hour[0]+"</span><span class='hour-unit'>"+hour[1]+"</span></span>";
		}
		var min = parseInt(timer/(1000*60))%60;
		if(min < 10) {
			min = "<span class='min'><span class='min-ten'>0</span><span class='min-unit'>" + min + "</span></span>";
		} else {
			min = min.toString();
			min = "<span class='min'><span class='min-ten'>"+min[0]+"</span><span class='min-unit'>"+min[1]+"</span></span>";
		}
		var sec = parseInt(timer/1000)%60;
		if(sec < 10) {
			sec = "<span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>" + sec + "</span></span>";
		} else {
			sec = sec.toString();
			sec = "<span class='sec'><span class='sec-ten'>"+sec[0]+"</span><span class='sec-unit'>"+sec[1]+"</span></span>";
		}
		var ctd60 = day+" "+hour+" "+min+" "+sec;
		$(".ctd60").html(ctd60);
	} else {
		$(".ctd60").html("<span class='day'><span class='day-ten'>0</span><span class='day-unit'>0</span></span> <span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>0</span></span> <span class='min'><span class='min-ten'>0</span><span class='min-unit'>0</span></span> <span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>0</span></span>");
	}
}
function getfrominputs60(){	
	var d = new Date();
	d.setMinutes(d.getMinutes()+53);
	d.setSeconds(d.getSeconds()+23);	
	get_timer60(d);
	setInterval(function(){
		get_timer60(d);
	},1000);
}
$(document).ready(function(){ 
	if (ctd60ON == true) {
		getfrominputs60(); 
	}
});

/* ========== /СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ЧАСА ========== */



/* ========== СЧЕТЧИК ЗАМОРОЖЕННЫЙ ========== */

function get_timerTEST(d){
	var date_new = d; 
	var date_t = new Date(date_new);
	var date = new Date();
	var timer = date_t - date;
	if(date_t > date) {
		var day = parseInt(timer/(60*60*1000*24));
		if(day < 10) {
			day = "<span class='day'><span class='day day-ten'>0</span><span class='day-unit'>" + day + "</span></span>";
		} else {
			day = day.toString();
			day = "<span class='day'><span class='day day-ten'>"+day[0]+"</span><span class='day-unit'>"+day[1]+"</span></span>";
		}
		var hour = parseInt(timer/(60*60*1000))%24;
		if(hour < 10) {
			hour = "<span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>" + hour + "</span></span>";
		} else {
			hour = hour.toString();
			hour = "<span class='hour'><span class='hour-ten'>"+hour[0]+"</span><span class='hour-unit'>"+hour[1]+"</span></span>";
		}
		var min = parseInt(timer/(1000*60))%60;
		if(min < 10) {
			min = "<span class='min'><span class='min-ten'>0</span><span class='min-unit'>" + min + "</span></span>";
		} else {
			min = min.toString();
			min = "<span class='min'><span class='min-ten'>"+min[0]+"</span><span class='min-unit'>"+min[1]+"</span></span>";
		}
		var sec = parseInt(timer/1000)%60;
		if(sec < 10) {
			sec = "<span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>" + sec + "</span></span>";
		} else {
			sec = sec.toString();
			sec = "<span class='sec'><span class='sec-ten'>"+sec[0]+"</span><span class='sec-unit'>"+sec[1]+"</span></span>";
		}
		var ctdtest = day+" "+hour+" "+min+" "+sec;
		$(".ctdtest").html(ctdtest);
	} else {
		$(".ctdtest").html("<span class='day'><span class='day-ten'>0</span><span class='day-unit'>0</span></span> <span class='hour'><span class='hour-ten'>0</span><span class='hour-unit'>0</span></span> <span class='min'><span class='min-ten'>0</span><span class='min-unit'>0</span></span> <span class='sec'><span class='sec-ten'>0</span><span class='sec-unit'>0</span></span>");
	}
}
function getfrominputsTEST(){	
	var d = new Date();
	d.setMinutes(d.getMinutes()+12345);
	get_timerTEST(d);
	setInterval(function(){
		get_timerTEST(d);
	},10000000000);
}
$(document).ready(function(){ 
	if (ctdtestON == true) {
		getfrominputsTEST(); 
	}
});

/* ========== /СЧЕТЧИК ЗАМОРОЖЕННЫЙ ========== */


	
/* СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ЧАСА ПО СПАНАМ */
function get_timerSPAN(string){
	var date_new = string; 
	var date_t = new Date(date_new);
	var date = new Date();
	var timer = date_t - date;
	if(date_t > date) {
		var day = parseInt(timer/(60*60*1000*24));
		if(day < 10) {
			day = "0" + day;
		} day = day.toString();
		var hour = parseInt(timer/(60*60*1000))%24;
		if(hour < 10) {
			hour = "0" + hour;
		} hour = hour.toString();
		var min = parseInt(timer/(1000*60))%60;
		if(min < 10) {
			min = "0" + min;
		} min = min.toString();
		var sec = parseInt(timer/1000)%60;
		if(sec < 10) {
			sec = "0" + sec;
		} sec = sec.toString();
		timethis = day + " : " + hour + " : " + min + " : " + sec;
		$(".ctd-day").text(day);
		$(".ctd-hour").text(hour);
		$(".ctd-minute").text(min);
		$(".ctd-second").text(sec);}
	else {
		$(".ctd-day").text("00");
		$(".ctd-hour").text("00");
		$(".ctd-minute").text("00");
		$(".ctd-second").text("00");} 
	}
function getfrominputsSPAN(){
	var d = new Date();
	d.setMinutes(d.getMinutes()+53);
	d.setSeconds(d.getSeconds()+23);	
	get_timerSPAN(d);
	setInterval(function(){
		get_timerSPAN(d);
	},1000);
}
$(document).ready(function(){ 
	if (ctdspanON == true) {
		getfrominputsSPAN(); 
	}
});
/* СЧЕТЧИК ДО КОНЦА ТЕКУЩЕГО ЧАСА ПО СПАНАМ */



/* ========== Плавный якорь ===================================== */

if (anchor == true) {
	$(document).ready(function() { 
		$('a[href^="#"]').on('click', function() {
			var target = $(this).attr('href');
			if (target.search('#modal') == -1) {
				$('html, body').animate({scrollTop: $(target).offset().top}, 800);
				return false;
			}
		});  
	});
}

/* ========== Плавный якорь ===================================== */
