
$(document).ready(function() {

    var text1_1 = "Ночная акция! С 22:00-02:00  скидка 50%!<br> Осталось всего 9 штук!";
    var text2_1 = "С 22:00-02:00  скидка 50%. Внимание! осталось 9 штук.";
    var text1_2 = "5 часов тотальной распродажи!<br> С 02:00 до 07:00 скидка 50%!";
    var text2_2 = "С 02:00 до 07:00 скидка 50%. Внимание! осталось 9 штук.";
    var text1_3 = "3 часа тотальной распродажи!<br> С 07:00 до 10:00 скидка 50%!";
    var text2_3 = "С 07:00 до 10:00 скидка 50%. Внимание! осталось 9 штук.";
    var text1_4 = "4 часа тотальной распродажи!<br> С 10:00 до 14:00 скидка 50%!";
    var text2_4 = "С 10:00 до 14:00 скидка 50%. Внимание! осталось 9 штук.";
    var text1_5 = "4 часа безумных скидок!<br> С 14:00 до 18:00 скидка 50%!";
    var text2_5 = "С 14:00 до 18:00 скидка 50%. Внимание! осталось 9 штук.";
    var text1_6 = "Ограниченная акция!<br> С 18:00 до 22:00 скидка 50%!";
    var text2_6 = "С 18:00 до 22:00 скидка 50%. Внимание! осталось 9 штук.";
    var date = new Date();
    var day = date.getDate();
    var year = date.getFullYear();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var mounth = date.getMonth();
    var end_date;

    if(hour>=22 || hour<2){
        $('.variant_text1').html(text1_1);
        $('.variant_text2').html(text2_1);
        if(hour<2){
            end_date = new Date(year, mounth, day, 1, 59, 59, 0);
        } else {
            end_date = new Date(year, mounth, date.getDate()+1, 1, 59, 59, 0);
        }

        myTime(end_date.getTime());
    }
    if(hour>=2 && hour<7){
        $('.variant_text1').html(text1_2);
        $('.variant_text2').html(text2_2);
        end_date = new Date(year, mounth, day, 6, 59, 59, 0);
        myTime(end_date.getTime());
    }
    if(hour>=7 && hour<10){
        $('.variant_text1').html(text1_3);
        $('.variant_text2').html(text2_3);
        end_date = new Date(year, mounth, day, 9, 59, 59, 0);
        myTime(end_date.getTime());
    }
    if(hour>=10 && hour<14){
        $('.variant_text1').html(text1_4);
        $('.variant_text2').html(text2_4);
        end_date = new Date(year, mounth, day, 13, 59, 59, 0);
        myTime(end_date.getTime());
    }
    if(hour>=14 && hour<18){
        $('.variant_text1').html(text1_5);
        $('.variant_text2').html(text2_5);
        end_date = new Date(year, mounth, day, 17, 59, 59, 0);
        myTime(end_date.getTime());
    }
    if(hour>=18 && hour<22){
        $('.variant_text1').html(text1_6);
        $('.variant_text2').html(text2_6);
        end_date = new Date(year, mounth, day, 21, 59, 59, 0);
        myTime(end_date.getTime());
    }

    fixMenuHeight();
    $(window).resize(function(){
        fixMenuHeight();
    })


    $('.fix-order-btn, .ord-btn1').on('click', function(event){
        event.preventDefault();
        $('.scroll_label').each(function(){
            if($(this).is(':visible')){
                $(this).addClass('scroll_label_active');
            } else {
                $(this).removeClass('scroll_label_active');
            }
        })

        $('html, body').animate({scrollTop: $('.scroll_label_active').offset().top - 50}, 400)
    })

    toTop();
    postImg();

});

function myTime(end_date){

    var time_current = new Date();

    time_current = time_current.getTime();

    var time_interval = (end_date - time_current)/1000;

    $('.counter, .counter-list, .counter-list_mob').each(function(){
        timer(this, time_interval);
    });
}

function timer(element, counter) {
    var el = $(element);
    var html = '';
    if (counter) {
        el.html(formatTime(counter));
        setInterval(function(){
            if (counter > 1) {
                counter = counter - 1;
                el.html(formatTime(counter));
            }
        }, 1000);
    }
}

function two(a) {
    return parseInt(a - Math.floor(a/10)*10).toString();
}

function one(a) {
    return Math.floor(a/10).toString();
}

function formatTime(seconds) {
    seconds = Math.floor(seconds);
    var minutes = Math.floor(seconds / 60), hours = Math.floor(minutes / 60), days = Math.floor(hours / 24);
    //hours = hours % 24;
    minutes %= 60;
    seconds %= 60;
    var hours_name;
    if(hours==1){
        hours_name = "час";
    } else {
        if(hours==0){
            hours_name = "часов";
        } else {
            hours_name = "часа";
        }
    }

    html = '<li class="count-div">' + one(hours) + two(hours) + '</li> <li class="count-div">' + one(minutes) + two(minutes) + '</li> <li class="count-div">' + one(seconds) + two(seconds) + '</li>';
    return html;
};

function fixMenuHeight(){
    var menu_height = $('.fix-menu').height();
    $('body').css({'padding-top': menu_height + 'px'});
}

/*back to top*/
function toTop() {
    var offset = 220;
    var duration = 500;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
        } else {
            jQuery('.back-to-top').fadeOut(duration);
        }
    });

    jQuery('.back-to-top').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
}
/*back to top end*/

/*post img*/
function postImg() {
    var orderForm = $('form');
    orderForm.on('change', 'select#country', function () {
        var checkedValue = $(this).val();
        $('.delivery-post').attr('id', checkedValue + '-post-img');
        if(checkedValue == "GEO"){
            $('.delivery-label').html('Доставка по всей Грузии');
        } else if (checkedValue == "ISR"){
            $('.delivery-label').html('Доставка по всему Израилю');
        } else if (checkedValue == "LVA"){
            $('.delivery-label').html('Доставка');
        } else if (checkedValue == "LTU"){
            $('.delivery-label').html('Доставка');
        } else if (checkedValue == ""){
            $('.delivery-label').html('Доставка');
        } else {
            $('.delivery-label').html('Доставка почтой или курьером');
        }
    });
    orderForm.find('select#country').trigger('change');
}
/*post img img*/
