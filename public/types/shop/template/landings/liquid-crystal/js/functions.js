//Timer
var today = new Date(),
    tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
	tomorrow = new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 0, 0, 0);
function GetCount() {
    dateNow = new Date();
    amount = tomorrow.getTime() - dateNow.getTime();
    delete dateNow;
    if (amount < 0) {
        $('.timer').html('<div><p>00</p><span>hh</span></div><div><p>00</p><span>minuty</span></div><div><p>00</p><span>sekundy</span></div>'); 
    } else {
        hours = 0;
        mins = 0;
        secs = 0;
        out = "";
        amount = Math.floor(amount / 1000);
        amount = amount % 86400;
        hours = Math.floor(amount / 3600);
        amount = amount % 3600;
        mins = Math.floor(amount / 60);
        amount = amount % 60;
        secs = Math.floor(amount);
        if(hours < 10) hours = '0'+hours;
        if(mins < 10) mins = '0'+mins;
        if(secs < 10) secs = '0'+secs;

        $('.timer').html('<div><p>' + hours + '</p><span>hh</span></div><div><p>' + mins + '</p><span>minuty</span></div><div><p>'+ secs + '</p><span>sekundy</span></div>'); 
        setTimeout("GetCount()", 1000);
    }
}



$(document).ready(function(){
    
    $('a[href^="#"]').click(function (){
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated), body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
    });
	//Timer Activate
    GetCount();

	//Info Tabs
	$('ul.tabs').on('click', 'li:not(.current)', function() {
		$(this).addClass('current').siblings().removeClass('current')
			.parents('.video-reviews').find('.item').eq($(this).index()).fadeIn(150).siblings('.item').hide();
	});
});