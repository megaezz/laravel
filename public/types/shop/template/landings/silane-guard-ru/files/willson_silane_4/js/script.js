$(document).ready(function() {

    //FlexSlider
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "fade",
            prevText: "",
            nextText: "",
        });
    });

    $('[name="country"]').on('change', function() {
        var geoKey = $(this).find('option:selected').val();
        if (geoKey == 357 || geoKey == 272) {
            $("body").addClass('bel');
        } else {
            $("body").removeClass('bel');
        }
        var data = $jsonData.prices[geoKey];
        var price = data.price;
        var oldPrice = data.old_price;
        var currency = data.currency
        $("[value = "+geoKey+"]").attr("selected", true).siblings().attr('selected', false);

        $('.price_land_s1').text(price);
        $('.price_land_s2').text(oldPrice);
        $('.price_land_curr').text(currency);
    });

    $('.scrollTo').click( function(){
        var scroll_el = $(this).attr('href');
        if ($(scroll_el).length != 0) {
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);
            //$(scroll_el).find("[name='name']").focus();
        }
        return false;
    });

    $('.mobile').on('click',function(){
        $('.desctop').slideToggle();
    
        $(document).click(function(event){
            if($(event.target).closest(".navigation").length)
            return;
            $(".desctop").slideUp();
            event.stopPropagation();
        });     
    
        $('.desctop a').on('click',function(){
            $('.desctop').slideUp();
        });
    });

    initializeClock('countdown', deadline);

});

