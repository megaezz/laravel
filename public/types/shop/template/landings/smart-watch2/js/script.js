$(document).ready(function(){

$(".modal_sob").click(function(){
	$('#modal_callback').arcticmodal();
});

$('.yakor1').on('click', function(e){
  $('html,body').stop().animate({ scrollTop: $('#home').offset().top }, 1000);
  e.preventDefault();
});

$('.yakor2').on('click', function(e){
  $('html,body').stop().animate({ scrollTop: $('#function').offset().top }, 1000);
  e.preventDefault();
});

$('.yakor3').on('click', function(e){
  $('html,body').stop().animate({ scrollTop: $('#tech').offset().top }, 1000);
  e.preventDefault();
});

$('.yakor4').on('click', function(e){
  $('html,body').stop().animate({ scrollTop: $('#zakaz').offset().top }, 1000);
  e.preventDefault();
});

$('.yakor5').on('click', function(e){
  $('html,body').stop().animate({ scrollTop: $('#dostavka').offset().top }, 1000);
  e.preventDefault();
});

$('.yakor6').on('click', function(e){
  $('html,body').stop().animate({ scrollTop: $('#otziv').offset().top }, 1000);
  e.preventDefault();
});

$(document).ready(function()
  {
    $(".slider").each(function ()
    {
      var obj = $(this);
      $(obj).append("<div class='nav'></div>");

      $(obj).find("li").each(function ()
      {
        $(obj).find(".nav").append("<span rel='"+$(this).index()+"'></span>");
        $(this).addClass("slider"+$(this).index());
      });

      $(obj).find("span").first().addClass("on");
    });
  });

  function sliderJS (obj, sl) // slider function
  {
    var ul = $(sl).find("ul");
    var bl = $(sl).find("li.slider"+obj);
    var step = $(bl).width();
    $(ul).animate({marginLeft: "-"+step*obj}, 500);
  }

  $(document).on("click", ".slider .nav span", function() // slider click navigate
  {
    var sl = $(this).closest(".slider");
    $(sl).find("span").removeClass("on");
    $(this).addClass("on");
    var obj = $(this).attr("rel");
    sliderJS(obj, sl);
    return false;
  });

});