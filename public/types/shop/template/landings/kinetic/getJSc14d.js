﻿

$(document).ready(function(){


    /* init counter */

    var dateFuture = new Date(apishopsActionDate('date'));

    $('.timer').countdown
({
        until: dateFuture,
        padZeroes: true,
        layout: '<ul><li><span class="count"><span>{hnn}</span></span><span class="num-text">Часов</span></li><li class="points">:</li>'+
            '<li><span class="count"><span>{mnn}</span></span><span class="num-text">Минут</span></li><li class="points">:</li>' +
            '<li><span class="count"><span>{snn}</span></span><span class="num-text">Секунд</span></li></ul>'

    });


    /*slow scroll*/

    /*
     arrays of anchors and hrefs
     */

    var anchors = $('.scroll a'),
        hrefs = [],
        i;

    for( i = 0; i < anchors.length; i++) {
        hrefs[i] = $(anchors[i]).attr('href');
        $(hrefs[i]).attr('data-anchor', i);
    }

    /*
     slow scrolling to href
     */

    anchors.click(function (e) {
        var anchor = $(this);

        $('html, body')
            .stop()
            .animate( {
                scrollTop: ( $(anchor.attr('href'))
                    .offset().top - 60
                    )
            }, 800);

        e.preventDefault();
    });

    //show comments

    $('.more-comments').click(function(e){
        e.preventDefault();
        $(this).hide();
        $('.hidden-comments').show(300);
    })

});



            function apishopsActionDate(type)
            {
                var currentTimestamp=new Date().getTime()

                if(typeof apishopsActionGetCookie('apishopsActionDate') == 'undefined' || currentTimestamp > parseInt(apishopsActionGetCookie('apishopsActionDate'))){
                    endTimestamp=currentTimestamp + (1 * 24 * 60 * 60 * 1000);
                    apishopsActionSetCookie('apishopsActionDate', endTimestamp);
                }else{
                    endTimestamp=parseInt(apishopsActionGetCookie('apishopsActionDate'))
                }

                if(type=='timestamp')
                    return endTimestamp;
                else if(type=='circularEnd'){
                    date=new Date(endTimestamp);
                    y=date.getFullYear();
                    d=date.getDate();
                    m=date.getMonth()+1;
                    h=date.getHours()+1;
                    mn=date.getMinutes();
                    return y+","+m+","+d+","+h+","+mn+",0";   //'2016,-6,-4,16,19,10',  //year,month,day,hour,minute,second
                }
                else if(type=='circularBegin'){
                    date=new Date();
                    y=date.getFullYear();
                    d=date.getDate();
                    m=date.getMonth()+1;
                    h=date.getHours()+1;
                    mn=date.getMinutes();
                    return y+","+m+","+d+","+h+","+mn+",0";   //'2016,-6,-4,16,19,10',  //year,month,day,hour,minute,second
                }
                else
                    return new Date(endTimestamp);
            }

            function apishopsActionGetCookie(name) {
              var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\/\+^])/g, '\$1') + "=([^;]*)"
              ));
              return matches ? decodeURIComponent(matches[1]) : undefined;
            }

            function apishopsActionSetCookie(name, value, options) {
              options = options || {};

              var expires = options.expires;

              if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires*1000);
                expires = options.expires = d;
              }
              if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
              }

              value = encodeURIComponent(value);

              var updatedCookie = name + "=" + value;

              for(var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                  updatedCookie += "=" + propValue;
                 }
              }

              document.cookie = updatedCookie;

              return value;
            }
            