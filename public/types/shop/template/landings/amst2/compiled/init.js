$(function() {
    $('a[href^="#"]').click(function (){
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top - 60;
        jQuery("html:not(:animated), body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
    });  

    $(function(){ 
        $('[placeholder]').placeholder();
    }); 
         var clock;
        
        $(document).ready(function() {
            var clock;

            clock = $('.clock').FlipClock({
                clockFace: 'hourly',
                autoStart: false,
                language:'ru',
                callbacks: {
                    stop: function() {
                        $('.message').html('The clock has stopped!')
                    }
                }
            });
                    
            clock.setTime(22880);
            clock.setCountdown(true);
            clock.start();

        });


        var clock2;
        
        $(document).ready(function() {
            var clock2;

            clock2 = $('.clock2').FlipClock({
                clockFace: 'hourly',
                autoStart: false,
                language:'ru',
                callbacks: {
                    stop: function() {
                        $('.message').html('The clock has stopped!')
                    }
                }
            });
                    
            clock2.setTime(22880);
            clock2.setCountdown(true);
            clock2.start();

        });

        // Resposive Youtube

        /*
 * Youtube video auto-resizer script
 * Created by Skipser.com
*/
 
$(document).ready(function() {
  if(typeof YOUTUBE_VIDEO_MARGIN == 'undefined') {
    YOUTUBE_VIDEO_MARGIN=5;
  }
  $('iframe').each(function(index,item) {
    if($(item).attr('src').match(/(https?:)?\/\/www\.youtube\.com/)) {
      var w=$(item).attr('width');
      var h=$(item).attr('height');
      var ar = h/w*100;
      ar=ar.toFixed(2);
      //Style iframe    
      $(item).css('position','absolute');
      $(item).css('top','0');
      $(item).css('left','0');    
      $(item).css('width','100%');
      $(item).css('height','100%');
      $(item).css('max-width',w+'px');
      $(item).css('max-height', h+'px');        
      $(item).wrap('<div style="max-width:'+w+'px;margin:0 auto; padding:'+YOUTUBE_VIDEO_MARGIN+'px;" />');
      $(item).wrap('<div style="position: relative;padding-bottom: '+ar+'%; height: 0; overflow: hidden; clear: both;" />');
    }
  });
});

});  