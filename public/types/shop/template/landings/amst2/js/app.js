


(function(t, e, i, s) {
    function n(e, i) {
        this.settings = null;
        this.options = t.extend({}, n.Defaults, i);
        this.$element = t(e);
        this._handlers = {};
        this._plugins = {};
        this._supress = {};
        this._current = null;
        this._speed = null;
        this._coordinates = [];
        this._breakpoint = null;
        this._width = null;
        this._items = [];
        this._clones = [];
        this._mergers = [];
        this._widths = [];
        this._invalidated = {};
        this._pipe = [];
        this._drag = {
            time: null,
            target: null,
            pointer: null,
            stage: {
                start: null,
                current: null
            },
            direction: null
        };
        this._states = {
            current: {},
            tags: {
                initializing: ["busy"],
                animating: ["busy"],
                dragging: ["interacting"]
            }
        };
        t.each(["onResize", "onThrottledResize"], t.proxy(function(e, i) {
            this._handlers[i] = t.proxy(this[i], this)
        }, this));
        t.each(n.Plugins, t.proxy(function(t, e) {
            this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this)
        }, this));
        t.each(n.Workers, t.proxy(function(e, i) {
            this._pipe.push({
                filter: i.filter,
                run: t.proxy(i.run, this)
            })
        }, this));
        this.setup();
        this.initialize()
    }
    n.Defaults = {
        items: 3,
        loop: false,
        center: false,
        rewind: false,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        freeDrag: false,
        margin: 0,
        stagePadding: 0,
        merge: false,
        mergeFit: true,
        autoWidth: false,
        startPosition: 0,
        rtl: false,
        smartSpeed: 250,
        fluidSpeed: false,
        dragEndSpeed: false,
        responsive: {},
        responsiveRefreshRate: 200,
        responsiveBaseElement: e,
        fallbackEasing: "swing",
        info: false,
        nestedItemSelector: false,
        itemElement: "div",
        stageElement: "div",
        refreshClass: "owl-refresh",
        loadedClass: "owl-loaded",
        loadingClass: "owl-loading",
        rtlClass: "owl-rtl",
        responsiveClass: "owl-responsive",
        dragClass: "owl-drag",
        itemClass: "owl-item",
        stageClass: "owl-stage",
        stageOuterClass: "owl-stage-outer",
        grabClass: "owl-grab"
    };
    n.Width = {
        Default: "default",
        Inner: "inner",
        Outer: "outer"
    };
    n.Type = {
        Event: "event",
        State: "state"
    };
    n.Plugins = {};
    n.Workers = [{
        filter: ["width", "settings"],
        run: function() {
            this._width = this.$element.width()
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            t.current = this._items && this._items[this.relative(this._current)]
        }
    }, {
        filter: ["items", "settings"],
        run: function() {
            this.$stage.children(".cloned").remove()
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e = this.settings.margin || "",
                i = !this.settings.autoWidth,
                s = this.settings.rtl,
                n = {
                    width: "auto",
                    "margin-left": s ? e : "",
                    "margin-right": s ? "" : e
                };
            !i && this.$stage.children().css(n);
            t.css = n
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
                i = null,
                s = this._items.length,
                n = !this.settings.autoWidth,
                r = [];
            t.items = {
                merge: false,
                width: e
            };
            while (s--) {
                i = this._mergers[s];
                i = this.settings.mergeFit && Math.min(i, this.settings.items) || i;
                t.items.merge = i > 1 || t.items.merge;
                r[s] = !n ? this._items[s].width() : e * i
            }
            this._widths = r
        }
    }, {
        filter: ["items", "settings"],
        run: function() {
            var e = [],
                i = this._items,
                s = this.settings,
                n = Math.max(s.items * 2, 4),
                r = Math.ceil(i.length / 2) * 2,
                o = s.loop && i.length ? s.rewind ? n : Math.max(n, r) : 0,
                a = "",
                h = "";
            o /= 2;
            while (o--) {
                e.push(this.normalize(e.length / 2, true));
                a = a + i[e[e.length - 1]][0].outerHTML;
                e.push(this.normalize(i.length - 1 - (e.length - 1) / 2, true));
                h = i[e[e.length - 1]][0].outerHTML + h
            }
            this._clones = e;
            t(a).addClass("cloned").appendTo(this.$stage);
            t(h).addClass("cloned").prependTo(this.$stage)
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function() {
            var t = this.settings.rtl ? 1 : -1,
                e = this._clones.length + this._items.length,
                i = -1,
                s = 0,
                n = 0,
                r = [];
            while (++i < e) {
                s = r[i - 1] || 0;
                n = this._widths[this.relative(i)] + this.settings.margin;
                r.push(s + n * t)
            }
            this._coordinates = r
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function() {
            var t = this.settings.stagePadding,
                e = this._coordinates,
                i = {
                    width: Math.ceil(Math.abs(e[e.length - 1])) + t * 2,
                    "padding-left": t || "",
                    "padding-right": t || ""
                };
            this.$stage.css(i)
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e = this._coordinates.length,
                i = !this.settings.autoWidth,
                s = this.$stage.children();
            if (i && t.items.merge) {
                while (e--) {
                    t.css.width = this._widths[this.relative(e)];
                    s.eq(e).css(t.css)
                }
            } else if (i) {
                t.css.width = t.items.width;
                s.css(t.css)
            }
        }
    }, {
        filter: ["items"],
        run: function() {
            this._coordinates.length < 1 && this.$stage.removeAttr("style")
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            t.current = t.current ? this.$stage.children().index(t.current) : 0;
            t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current));
            this.reset(t.current)
        }
    }, {
        filter: ["position"],
        run: function() {
            this.animate(this.coordinates(this._current))
        }
    }, {
        filter: ["width", "position", "items", "settings"],
        run: function() {
            var t = this.settings.rtl ? 1 : -1,
                e = this.settings.stagePadding * 2,
                i = this.coordinates(this.current()) + e,
                s = i + this.width() * t,
                n, r, o = [],
                a, h;
            for (a = 0, h = this._coordinates.length; a < h; a++) {
                n = this._coordinates[a - 1] || 0;
                r = Math.abs(this._coordinates[a]) + e * t;
                if (this.op(n, "<=", i) && this.op(n, ">", s) || this.op(r, "<", i) && this.op(r, ">", s)) {
                    o.push(a)
                }
            }
            this.$stage.children(".active").removeClass("active");
            this.$stage.children(":eq(" + o.join("), :eq(") + ")").addClass("active");
            if (this.settings.center) {
                this.$stage.children(".center").removeClass("center");
                this.$stage.children().eq(this.current()).addClass("center")
            }
        }
    }];
    n.prototype.initialize = function() {
        this.enter("initializing");
        this.trigger("initialize");
        this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);
        if (this.settings.autoWidth && !this.is("pre-loading")) {
            var e, i, n;
            e = this.$element.find("img");
            i = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : s;
            n = this.$element.children(i).width();
            if (e.length && n <= 0) {
                this.preloadAutoWidthImages(e)
            }
        }
        this.$element.addClass(this.options.loadingClass);
        this.$stage = t("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>');
        this.$element.append(this.$stage.parent());
        this.replace(this.$element.children().not(this.$stage.parent()));
        if (this.$element.is(":visible")) {
            this.refresh()
        } else {
            this.invalidate("width")
        }
        this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass);
        this.registerEventHandlers();
        this.leave("initializing");
        this.trigger("initialized")
    };
    n.prototype.setup = function() {
        var e = this.viewport(),
            i = this.options.responsive,
            s = -1,
            n = null;
        if (!i) {
            n = t.extend({}, this.options)
        } else {
            t.each(i, function(t) {
                if (t <= e && t > s) {
                    s = Number(t)
                }
            });
            n = t.extend({}, this.options, i[s]);
            if (typeof n.stagePadding === "function") {
                n.stagePadding = n.stagePadding()
            }
            delete n.responsive;
            if (n.responsiveClass) {
                this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + s))
            }
        }
        this.trigger("change", {
            property: {
                name: "settings",
                value: n
            }
        });
        this._breakpoint = s;
        this.settings = n;
        this.invalidate("settings");
        this.trigger("changed", {
            property: {
                name: "settings",
                value: this.settings
            }
        })
    };
    n.prototype.optionsLogic = function() {
        if (this.settings.autoWidth) {
            this.settings.stagePadding = false;
            this.settings.merge = false
        }
    };
    n.prototype.prepare = function(e) {
        var i = this.trigger("prepare", {
            content: e
        });
        if (!i.data) {
            i.data = t("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(e)
        }
        this.trigger("prepared", {
            content: i.data
        });
        return i.data
    };
    n.prototype.update = function() {
        var e = 0,
            i = this._pipe.length,
            s = t.proxy(function(t) {
                return this[t]
            }, this._invalidated),
            n = {};
        while (e < i) {
            if (this._invalidated.all || t.grep(this._pipe[e].filter, s).length > 0) {
                this._pipe[e].run(n)
            }
            e++
        }
        this._invalidated = {};
        !this.is("valid") && this.enter("valid")
    };
    n.prototype.width = function(t) {
        t = t || n.Width.Default;
        switch (t) {
            case n.Width.Inner:
            case n.Width.Outer:
                return this._width;
            default:
                return this._width - this.settings.stagePadding * 2 + this.settings.margin
        }
    };
    n.prototype.refresh = function() {
        this.enter("refreshing");
        this.trigger("refresh");
        this.setup();
        this.optionsLogic();
        this.$element.addClass(this.options.refreshClass);
        this.update();
        this.$element.removeClass(this.options.refreshClass);
        this.leave("refreshing");
        this.trigger("refreshed")
    };
    n.prototype.onThrottledResize = function() {
        e.clearTimeout(this.resizeTimer);
        this.resizeTimer = e.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
    };
    n.prototype.onResize = function() {
        if (!this._items.length) {
            return false
        }
        if (this._width === this.$element.width()) {
            return false
        }
        if (!this.$element.is(":visible")) {
            return false
        }
        this.enter("resizing");
        if (this.trigger("resize").isDefaultPrevented()) {
            this.leave("resizing");
            return false
        }
        this.invalidate("width");
        this.refresh();
        this.leave("resizing");
        this.trigger("resized")
    };
    n.prototype.registerEventHandlers = function() {
        if (t.support.transition) {
            this.$stage.on(t.support.transition.end + ".owl.core", t.proxy(this.onTransitionEnd, this))
        }
        if (this.settings.responsive !== false) {
            this.on(e, "resize", this._handlers.onThrottledResize)
        }
        if (this.settings.mouseDrag) {
            this.$element.addClass(this.options.dragClass);
            this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this));
            this.$stage.on("dragstart.owl.core selectstart.owl.core", function() {
                return false
            })
        }
        if (this.settings.touchDrag) {
            this.$stage.on("touchstart.owl.core", t.proxy(this.onDragStart, this));
            this.$stage.on("touchcancel.owl.core", t.proxy(this.onDragEnd, this))
        }
    };
    n.prototype.onDragStart = function(e) {
        var s = null;
        if (e.which === 3) {
            return
        }
        if (t.support.transform) {
            s = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(",");
            s = {
                x: s[s.length === 16 ? 12 : 4],
                y: s[s.length === 16 ? 13 : 5]
            }
        } else {
            s = this.$stage.position();
            s = {
                x: this.settings.rtl ? s.left + this.$stage.width() - this.width() + this.settings.margin : s.left,
                y: s.top
            }
        }
        if (this.is("animating")) {
            t.support.transform ? this.animate(s.x) : this.$stage.stop();
            this.invalidate("position")
        }
        this.$element.toggleClass(this.options.grabClass, e.type === "mousedown");
        this.speed(0);
        this._drag.time = (new Date).getTime();
        this._drag.target = t(e.target);
        this._drag.stage.start = s;
        this._drag.stage.current = s;
        this._drag.pointer = this.pointer(e);
        t(i).on("mouseup.owl.core touchend.owl.core", t.proxy(this.onDragEnd, this));
        t(i).one("mousemove.owl.core touchmove.owl.core", t.proxy(function(e) {
            var s = this.difference(this._drag.pointer, this.pointer(e));
            t(i).on("mousemove.owl.core touchmove.owl.core", t.proxy(this.onDragMove, this));
            if (Math.abs(s.x) < Math.abs(s.y) && this.is("valid")) {
                return
            }
            e.preventDefault();
            this.enter("dragging");
            this.trigger("drag")
        }, this))
    };
    n.prototype.onDragMove = function(t) {
        var e = null,
            i = null,
            s = null,
            n = this.difference(this._drag.pointer, this.pointer(t)),
            r = this.difference(this._drag.stage.start, n);
        if (!this.is("dragging")) {
            return
        }
        t.preventDefault();
        if (this.settings.loop) {
            e = this.coordinates(this.minimum());
            i = this.coordinates(this.maximum() + 1) - e;
            r.x = ((r.x - e) % i + i) % i + e
        } else {
            e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum());
            i = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum());
            s = this.settings.pullDrag ? -1 * n.x / 5 : 0;
            r.x = Math.max(Math.min(r.x, e + s), i + s)
        }
        this._drag.stage.current = r;
        this.animate(r.x)
    };
    n.prototype.onDragEnd = function(e) {
        var s = this.difference(this._drag.pointer, this.pointer(e)),
            n = this._drag.stage.current,
            r = s.x > 0 ^ this.settings.rtl ? "left" : "right";
        t(i).off(".owl.core");
        this.$element.removeClass(this.options.grabClass);
        if (s.x !== 0 && this.is("dragging") || !this.is("valid")) {
            this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed);
            this.current(this.closest(n.x, s.x !== 0 ? r : this._drag.direction));
            this.invalidate("position");
            this.update();
            this._drag.direction = r;
            if (Math.abs(s.x) > 3 || (new Date).getTime() - this._drag.time > 300) {
                this._drag.target.one("click.owl.core", function() {
                    return false
                })
            }
        }
        if (!this.is("dragging")) {
            return
        }
        this.leave("dragging");
        this.trigger("dragged")
    };
    n.prototype.closest = function(e, i) {
        var s = -1,
            n = 30,
            r = this.width(),
            o = this.coordinates();
        if (!this.settings.freeDrag) {
            t.each(o, t.proxy(function(t, a) {
                if (i === "left" && e > a - n && e < a + n) {
                    s = t
                } else if (i === "right" && e > a - r - n && e < a - r + n) {
                    s = t + 1
                } else if (this.op(e, "<", a) && this.op(e, ">", o[t + 1] || a - r)) {
                    s = i === "left" ? t + 1 : t
                }
                return s === -1
            }, this))
        }
        if (!this.settings.loop) {
            if (this.op(e, ">", o[this.minimum()])) {
                s = e = this.minimum()
            } else if (this.op(e, "<", o[this.maximum()])) {
                s = e = this.maximum()
            }
        }
        return s
    };
    n.prototype.animate = function(e) {
        var i = this.speed() > 0;
        this.is("animating") && this.onTransitionEnd();
        if (i) {
            this.enter("animating");
            this.trigger("translate")
        }
        if (t.support.transform3d && t.support.transition) {
            this.$stage.css({
                transform: "translate3d(" + e + "px,0px,0px)",
                transition: this.speed() / 1e3 + "s"
            })
        } else if (i) {
            this.$stage.animate({
                left: e + "px"
            }, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this))
        } else {
            this.$stage.css({
                left: e + "px"
            })
        }
    };
    n.prototype.is = function(t) {
        return this._states.current[t] && this._states.current[t] > 0
    };
    n.prototype.current = function(t) {
        if (t === s) {
            return this._current
        }
        if (this._items.length === 0) {
            return s
        }
        t = this.normalize(t);
        if (this._current !== t) {
            var e = this.trigger("change", {
                property: {
                    name: "position",
                    value: t
                }
            });
            if (e.data !== s) {
                t = this.normalize(e.data)
            }
            this._current = t;
            this.invalidate("position");
            this.trigger("changed", {
                property: {
                    name: "position",
                    value: this._current
                }
            })
        }
        return this._current
    };
    n.prototype.invalidate = function(e) {
        if (t.type(e) === "string") {
            this._invalidated[e] = true;
            this.is("valid") && this.leave("valid")
        }
        return t.map(this._invalidated, function(t, e) {
            return e
        })
    };
    n.prototype.reset = function(t) {
        t = this.normalize(t);
        if (t === s) {
            return
        }
        this._speed = 0;
        this._current = t;
        this.suppress(["translate", "translated"]);
        this.animate(this.coordinates(t));
        this.release(["translate", "translated"])
    };
    n.prototype.normalize = function(t, e) {
        var i = this._items.length,
            n = e ? 0 : this._clones.length;
        if (!this.isNumeric(t) || i < 1) {
            t = s
        } else if (t < 0 || t >= i + n) {
            t = ((t - n / 2) % i + i) % i + n / 2
        }
        return t
    };
    n.prototype.relative = function(t) {
        t -= this._clones.length / 2;
        return this.normalize(t, true)
    };
    n.prototype.maximum = function(t) {
        var e = this.settings,
            i = this._coordinates.length,
            s, n, r;
        if (e.loop) {
            i = this._clones.length / 2 + this._items.length - 1
        } else if (e.autoWidth || e.merge) {
            s = this._items.length;
            n = this._items[--s].width();
            r = this.$element.width();
            while (s--) {
                n += this._items[s].width() + this.settings.margin;
                if (n > r) {
                    break
                }
            }
            i = s + 1
        } else if (e.center) {
            i = this._items.length - 1
        } else {
            i = this._items.length - e.items
        }
        if (t) {
            i -= this._clones.length / 2
        }
        return Math.max(i, 0)
    };
    n.prototype.minimum = function(t) {
        return t ? 0 : this._clones.length / 2
    };
    n.prototype.items = function(t) {
        if (t === s) {
            return this._items.slice()
        }
        t = this.normalize(t, true);
        return this._items[t]
    };
    n.prototype.mergers = function(t) {
        if (t === s) {
            return this._mergers.slice()
        }
        t = this.normalize(t, true);
        return this._mergers[t]
    };
    n.prototype.clones = function(e) {
        var i = this._clones.length / 2,
            n = i + this._items.length,
            r = function(t) {
                return t % 2 === 0 ? n + t / 2 : i - (t + 1) / 2
            };
        if (e === s) {
            return t.map(this._clones, function(t, e) {
                return r(e)
            })
        }
        return t.map(this._clones, function(t, i) {
            return t === e ? r(i) : null
        })
    };
    n.prototype.speed = function(t) {
        if (t !== s) {
            this._speed = t
        }
        return this._speed
    };
    n.prototype.coordinates = function(e) {
        var i = 1,
            n = e - 1,
            r;
        if (e === s) {
            return t.map(this._coordinates, t.proxy(function(t, e) {
                return this.coordinates(e)
            }, this))
        }
        if (this.settings.center) {
            if (this.settings.rtl) {
                i = -1;
                n = e + 1
            }
            r = this._coordinates[e];
            r += (this.width() - r + (this._coordinates[n] || 0)) / 2 * i
        } else {
            r = this._coordinates[n] || 0
        }
        r = Math.ceil(r);
        return r
    };
    n.prototype.duration = function(t, e, i) {
        if (i === 0) {
            return 0
        }
        return Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(i || this.settings.smartSpeed)
    };
    n.prototype.to = function(t, e) {
        var i = this.current(),
            s = null,
            n = t - this.relative(i),
            r = (n > 0) - (n < 0),
            o = this._items.length,
            a = this.minimum(),
            h = this.maximum();
        if (this.settings.loop) {
            if (!this.settings.rewind && Math.abs(n) > o / 2) {
                n += r * -1 * o
            }
            t = i + n;
            s = ((t - a) % o + o) % o + a;
            if (s !== t && s - n <= h && s - n > 0) {
                i = s - n;
                t = s;
                this.reset(i)
            }
        } else if (this.settings.rewind) {
            h += 1;
            t = (t % h + h) % h
        } else {
            t = Math.max(a, Math.min(h, t))
        }
        this.speed(this.duration(i, t, e));
        this.current(t);
        if (this.$element.is(":visible")) {
            this.update()
        }
    };
    n.prototype.next = function(t) {
        t = t || false;
        this.to(this.relative(this.current()) + 1, t)
    };
    n.prototype.prev = function(t) {
        t = t || false;
        this.to(this.relative(this.current()) - 1, t)
    };
    n.prototype.onTransitionEnd = function(t) {
        if (t !== s) {
            t.stopPropagation();
            if ((t.target || t.srcElement || t.originalTarget) !== this.$stage.get(0)) {
                return false
            }
        }
        this.leave("animating");
        this.trigger("translated")
    };
    n.prototype.viewport = function() {
        var s;
        if (this.options.responsiveBaseElement !== e) {
            s = t(this.options.responsiveBaseElement).width()
        } else if (e.innerWidth) {
            s = e.innerWidth
        } else if (i.documentElement && i.documentElement.clientWidth) {
            s = i.documentElement.clientWidth
        } else {
            throw "Can not detect viewport width."
        }
        return s
    };
    n.prototype.replace = function(e) {
        this.$stage.empty();
        this._items = [];
        if (e) {
            e = e instanceof jQuery ? e : t(e)
        }
        if (this.settings.nestedItemSelector) {
            e = e.find("." + this.settings.nestedItemSelector)
        }
        e.filter(function() {
            return this.nodeType === 1
        }).each(t.proxy(function(t, e) {
            e = this.prepare(e);
            this.$stage.append(e);
            this._items.push(e);
            this._mergers.push(e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") * 1 || 1)
        }, this));
        this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0);
        this.invalidate("items")
    };
    n.prototype.add = function(e, i) {
        var n = this.relative(this._current);
        i = i === s ? this._items.length : this.normalize(i, true);
        e = e instanceof jQuery ? e : t(e);
        this.trigger("add", {
            content: e,
            position: i
        });
        e = this.prepare(e);
        if (this._items.length === 0 || i === this._items.length) {
            this._items.length === 0 && this.$stage.append(e);
            this._items.length !== 0 && this._items[i - 1].after(e);
            this._items.push(e);
            this._mergers.push(e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") * 1 || 1)
        } else {
            this._items[i].before(e);
            this._items.splice(i, 0, e);
            this._mergers.splice(i, 0, e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") * 1 || 1)
        }
        this._items[n] && this.reset(this._items[n].index());
        this.invalidate("items");
        this.trigger("added", {
            content: e,
            position: i
        })
    };
    n.prototype.remove = function(t) {
        t = this.normalize(t, true);
        if (t === s) {
            return
        }
        this.trigger("remove", {
            content: this._items[t],
            position: t
        });
        this._items[t].remove();
        this._items.splice(t, 1);
        this._mergers.splice(t, 1);
        this.invalidate("items");
        this.trigger("removed", {
            content: null,
            position: t
        })
    };
    n.prototype.preloadAutoWidthImages = function(e) {
        e.each(t.proxy(function(e, i) {
            this.enter("pre-loading");
            i = t(i);
            t(new Image).one("load", t.proxy(function(t) {
                i.attr("src", t.target.src);
                i.css("opacity", 1);
                this.leave("pre-loading");
                !this.is("pre-loading") && !this.is("initializing") && this.refresh()
            }, this)).attr("src", i.attr("src") || i.attr("data-src") || i.attr("data-src-retina"))
        }, this))
    };
    n.prototype.destroy = function() {
        this.$element.off(".owl.core");
        this.$stage.off(".owl.core");
        t(i).off(".owl.core");
        if (this.settings.responsive !== false) {
            e.clearTimeout(this.resizeTimer);
            this.off(e, "resize", this._handlers.onThrottledResize)
        }
        for (var s in this._plugins) {
            this._plugins[s].destroy()
        }
        this.$stage.children(".cloned").remove();
        this.$stage.unwrap();
        this.$stage.children().contents().unwrap();
        this.$stage.children().unwrap();
        this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
    };
    n.prototype.op = function(t, e, i) {
        var s = this.settings.rtl;
        switch (e) {
            case "<":
                return s ? t > i : t < i;
            case ">":
                return s ? t < i : t > i;
            case ">=":
                return s ? t <= i : t >= i;
            case "<=":
                return s ? t >= i : t <= i;
            default:
                break
        }
    };
    n.prototype.on = function(t, e, i, s) {
        if (t.addEventListener) {
            t.addEventListener(e, i, s)
        } else if (t.attachEvent) {
            t.attachEvent("on" + e, i)
        }
    };
    n.prototype.off = function(t, e, i, s) {
        if (t.removeEventListener) {
            t.removeEventListener(e, i, s)
        } else if (t.detachEvent) {
            t.detachEvent("on" + e, i)
        }
    };
    n.prototype.trigger = function(e, i, s, r, o) {
        var a = {
                item: {
                    count: this._items.length,
                    index: this.current()
                }
            },
            h = t.camelCase(t.grep(["on", e, s], function(t) {
                return t
            }).join("-").toLowerCase()),
            l = t.Event([e, "owl", s || "carousel"].join(".").toLowerCase(), t.extend({
                relatedTarget: this
            }, a, i));
        if (!this._supress[e]) {
            t.each(this._plugins, function(t, e) {
                if (e.onTrigger) {
                    e.onTrigger(l)
                }
            });
            this.register({
                type: n.Type.Event,
                name: e
            });
            this.$element.trigger(l);
            if (this.settings && typeof this.settings[h] === "function") {
                this.settings[h].call(this, l)
            }
        }
        return l
    };
    n.prototype.enter = function(e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function(t, e) {
            if (this._states.current[e] === s) {
                this._states.current[e] = 0
            }
            this._states.current[e]++
        }, this))
    };
    n.prototype.leave = function(e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function(t, e) {
            this._states.current[e]--
        }, this))
    };
    n.prototype.register = function(e) {
        if (e.type === n.Type.Event) {
            if (!t.event.special[e.name]) {
                t.event.special[e.name] = {}
            }
            if (!t.event.special[e.name].owl) {
                var i = t.event.special[e.name]._default;
                t.event.special[e.name]._default = function(t) {
                    if (i && i.apply && (!t.namespace || t.namespace.indexOf("owl") === -1)) {
                        return i.apply(this, arguments)
                    }
                    return t.namespace && t.namespace.indexOf("owl") > -1
                };
                t.event.special[e.name].owl = true
            }
        } else if (e.type === n.Type.State) {
            if (!this._states.tags[e.name]) {
                this._states.tags[e.name] = e.tags
            } else {
                this._states.tags[e.name] = this._states.tags[e.name].concat(e.tags)
            }
            this._states.tags[e.name] = t.grep(this._states.tags[e.name], t.proxy(function(i, s) {
                return t.inArray(i, this._states.tags[e.name]) === s
            }, this))
        }
    };
    n.prototype.suppress = function(e) {
        t.each(e, t.proxy(function(t, e) {
            this._supress[e] = true
        }, this))
    };
    n.prototype.release = function(e) {
        t.each(e, t.proxy(function(t, e) {
            delete this._supress[e]
        }, this))
    };
    n.prototype.pointer = function(t) {
        var i = {
            x: null,
            y: null
        };
        t = t.originalEvent || t || e.event;
        t = t.touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t;
        if (t.pageX) {
            i.x = t.pageX;
            i.y = t.pageY
        } else {
            i.x = t.clientX;
            i.y = t.clientY
        }
        return i
    };
    n.prototype.isNumeric = function(t) {
        return !isNaN(parseFloat(t))
    };
    n.prototype.difference = function(t, e) {
        return {
            x: t.x - e.x,
            y: t.y - e.y
        }
    };
    t.fn.owlCarousel = function(e) {
        var i = Array.prototype.slice.call(arguments, 1);
        return this.each(function() {
            var s = t(this),
                r = s.data("owl.carousel");
            if (!r) {
                r = new n(this, typeof e == "object" && e);
                s.data("owl.carousel", r);
                t.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function(e, i) {
                    r.register({
                        type: n.Type.Event,
                        name: i
                    });
                    r.$element.on(i + ".owl.carousel.core", t.proxy(function(t) {
                        if (t.namespace && t.relatedTarget !== this) {
                            this.suppress([i]);
                            r[i].apply(this, [].slice.call(arguments, 1));
                            this.release([i])
                        }
                    }, r))
                })
            }
            if (typeof e == "string" && e.charAt(0) !== "_") {
                r[e].apply(r, i)
            }
        })
    };
    t.fn.owlCarousel.Constructor = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._interval = null;
        this._visible = null;
        this._handlers = {
            "initialized.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoRefresh) {
                    this.watch()
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        autoRefresh: true,
        autoRefreshInterval: 500
    };
    n.prototype.watch = function() {
        if (this._interval) {
            return
        }
        this._visible = this._core.$element.is(":visible");
        this._interval = e.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval)
    };
    n.prototype.refresh = function() {
        if (this._core.$element.is(":visible") === this._visible) {
            return
        }
        this._visible = !this._visible;
        this._core.$element.toggleClass("owl-hidden", !this._visible);
        this._visible && (this._core.invalidate("width") && this._core.refresh())
    };
    n.prototype.destroy = function() {
        var t, i;
        e.clearInterval(this._interval);
        for (t in this._handlers) {
            this._core.$element.off(t, this._handlers[t])
        }
        for (i in Object.getOwnPropertyNames(this)) {
            typeof this[i] != "function" && (this[i] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._loaded = [];
        this._handlers = {
            "initialized.owl.carousel change.owl.carousel resized.owl.carousel": t.proxy(function(e) {
                if (!e.namespace) {
                    return
                }
                if (!this._core.settings || !this._core.settings.lazyLoad) {
                    return
                }
                if (e.property && e.property.name == "position" || e.type == "initialized") {
                    var i = this._core.settings,
                        n = i.center && Math.ceil(i.items / 2) || i.items,
                        r = i.center && n * -1 || 0,
                        o = (e.property && e.property.value !== s ? e.property.value : this._core.current()) + r,
                        a = this._core.clones().length,
                        h = t.proxy(function(t, e) {
                            this.load(e)
                        }, this);
                    while (r++ < n) {
                        this.load(a / 2 + this._core.relative(o));
                        a && t.each(this._core.clones(this._core.relative(o)), h);
                        o++
                    }
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        lazyLoad: false
    };
    n.prototype.load = function(i) {
        var s = this._core.$stage.children().eq(i),
            n = s && s.find(".owl-lazy");
        if (!n || t.inArray(s.get(0), this._loaded) > -1) {
            return
        }
        n.each(t.proxy(function(i, s) {
            var n = t(s),
                r, o = e.devicePixelRatio > 1 && n.attr("data-src-retina") || n.attr("data-src");
            this._core.trigger("load", {
                element: n,
                url: o
            }, "lazy");
            if (n.is("img")) {
                n.one("load.owl.lazy", t.proxy(function() {
                    n.css("opacity", 1);
                    this._core.trigger("loaded", {
                        element: n,
                        url: o
                    }, "lazy")
                }, this)).attr("src", o)
            } else {
                r = new Image;
                r.onload = t.proxy(function() {
                    n.css({
                        "background-image": "url(" + o + ")",
                        opacity: "1"
                    });
                    this._core.trigger("loaded", {
                        element: n,
                        url: o
                    }, "lazy")
                }, this);
                r.src = o
            }
        }, this));
        this._loaded.push(s.get(0))
    };
    n.prototype.destroy = function() {
        var t, e;
        for (t in this.handlers) {
            this._core.$element.off(t, this.handlers[t])
        }
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != "function" && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Lazy = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._handlers = {
            "initialized.owl.carousel refreshed.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoHeight) {
                    this.update()
                }
            }, this),
            "changed.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoHeight && t.property.name == "position") {
                    this.update()
                }
            }, this),
            "loaded.owl.lazy": t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass).index() === this._core.current()) {
                    this.update()
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this._core.$element.on(this._handlers)
    };
    n.Defaults = {
        autoHeight: false,
        autoHeightClass: "owl-height"
    };
    n.prototype.update = function() {
        var e = this._core._current,
            i = e + this._core.settings.items,
            s = this._core.$stage.children().toArray().slice(e, i),
            n = [],
            r = 0;
        t.each(s, function(e, i) {
            n.push(t(i).height())
        });
        r = Math.max.apply(null, n);
        this._core.$stage.parent().height(r).addClass(this._core.settings.autoHeightClass)
    };
    n.prototype.destroy = function() {
        var t, e;
        for (t in this._handlers) {
            this._core.$element.off(t, this._handlers[t])
        }
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != "function" && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.AutoHeight = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._videos = {};
        this._playing = null;
        this._handlers = {
            "initialized.owl.carousel": t.proxy(function(t) {
                if (t.namespace) {
                    this._core.register({
                        type: "state",
                        name: "playing",
                        tags: ["interacting"]
                    })
                }
            }, this),
            "resize.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._core.settings.video && this.isInFullScreen()) {
                    t.preventDefault()
                }
            }, this),
            "refreshed.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._core.is("resizing")) {
                    this._core.$stage.find(".cloned .owl-video-frame").remove()
                }
            }, this),
            "changed.owl.carousel": t.proxy(function(t) {
                if (t.namespace && t.property.name === "position" && this._playing) {
                    this.stop()
                }
            }, this),
            "prepared.owl.carousel": t.proxy(function(e) {
                if (!e.namespace) {
                    return
                }
                var i = t(e.content).find(".owl-video");
                if (i.length) {
                    i.css("display", "none");
                    this.fetch(i, t(e.content))
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this._core.$element.on(this._handlers);
        this._core.$element.on("click.owl.video", ".owl-video-play-icon", t.proxy(function(t) {
            this.play(t)
        }, this))
    };
    n.Defaults = {
        video: false,
        videoHeight: false,
        videoWidth: false
    };
    n.prototype.fetch = function(t, e) {
        var i = function() {
                if (t.attr("data-vimeo-id")) {
                    return "vimeo"
                } else if (t.attr("data-vzaar-id")) {
                    return "vzaar"
                } else {
                    return "youtube"
                }
            }(),
            s = t.attr("data-vimeo-id") || t.attr("data-youtube-id") || t.attr("data-vzaar-id"),
            n = t.attr("data-width") || this._core.settings.videoWidth,
            r = t.attr("data-height") || this._core.settings.videoHeight,
            o = t.attr("href");
        if (o) {
            s = o.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
            if (s[3].indexOf("youtu") > -1) {
                i = "youtube"
            } else if (s[3].indexOf("vimeo") > -1) {
                i = "vimeo"
            } else if (s[3].indexOf("vzaar") > -1) {
                i = "vzaar"
            } else {
                throw new Error("Video URL not supported.")
            }
            s = s[6]
        } else {
            throw new Error("Missing video URL.")
        }
        this._videos[o] = {
            type: i,
            id: s,
            width: n,
            height: r
        };
        e.attr("data-video", o);
        this.thumbnail(t, this._videos[o])
    };
    n.prototype.thumbnail = function(e, i) {
        var s, n, r, o = i.width && i.height ? 'style="width:' + i.width + "px;height:" + i.height + 'px;"' : "",
            a = e.find("img"),
            h = "src",
            l = "",
            c = this._core.settings,
            d = function(t) {
                n = '<div class="owl-video-play-icon"></div>';
                if (c.lazyLoad) {
                    s = '<div class="owl-video-tn ' + l + '" ' + h + '="' + t + '"></div>'
                } else {
                    s = '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>'
                }
                e.after(s);
                e.after(n)
            };
        e.wrap('<div class="owl-video-wrapper"' + o + "></div>");
        if (this._core.settings.lazyLoad) {
            h = "data-src";
            l = "owl-lazy"
        }
        if (a.length) {
            d(a.attr(h));
            a.remove();
            return false
        }
        if (i.type === "youtube") {
            r = "//img.youtube.com/vi/" + i.id + "/hqdefault.jpg";
            d(r)
        } else if (i.type === "vimeo") {
            t.ajax({
                type: "GET",
                url: "//vimeo.com/api/v2/video/" + i.id + ".json",
                jsonp: "callback",
                dataType: "jsonp",
                success: function(t) {
                    r = t[0].thumbnail_large;
                    d(r)
                }
            })
        } else if (i.type === "vzaar") {
            t.ajax({
                type: "GET",
                url: "//vzaar.com/api/videos/" + i.id + ".json",
                jsonp: "callback",
                dataType: "jsonp",
                success: function(t) {
                    r = t.framegrab_url;
                    d(r)
                }
            })
        }
    };
    n.prototype.stop = function() {
        this._core.trigger("stop", null, "video");
        this._playing.find(".owl-video-frame").remove();
        this._playing.removeClass("owl-video-playing");
        this._playing = null;
        this._core.leave("playing");
        this._core.trigger("stopped", null, "video")
    };
    n.prototype.play = function(e) {
        var i = t(e.target),
            s = i.closest("." + this._core.settings.itemClass),
            n = this._videos[s.attr("data-video")],
            r = n.width || "100%",
            o = n.height || this._core.$stage.height(),
            a;
        if (this._playing) {
            return
        }
        this._core.enter("playing");
        this._core.trigger("play", null, "video");
        s = this._core.items(this._core.relative(s.index()));
        this._core.reset(s.index());
        if (n.type === "youtube") {
            a = '<iframe width="' + r + '" height="' + o + '" src="//www.youtube.com/embed/' + n.id + "?autoplay=1&v=" + n.id + '" frameborder="0" allowfullscreen></iframe>'
        } else if (n.type === "vimeo") {
            a = '<iframe src="//player.vimeo.com/video/' + n.id + '?autoplay=1" width="' + r + '" height="' + o + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
        } else if (n.type === "vzaar") {
            a = '<iframe frameborder="0"' + 'height="' + o + '"' + 'width="' + r + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen ' + 'src="//view.vzaar.com/' + n.id + '/player?autoplay=true"></iframe>'
        }
        t('<div class="owl-video-frame">' + a + "</div>").insertAfter(s.find(".owl-video"));
        this._playing = s.addClass("owl-video-playing")
    };
    n.prototype.isInFullScreen = function() {
        var e = i.fullscreenElement || i.mozFullScreenElement || i.webkitFullscreenElement;
        return e && t(e).parent().hasClass("owl-video-frame")
    };
    n.prototype.destroy = function() {
        var t, e;
        this._core.$element.off("click.owl.video");
        for (t in this._handlers) {
            this._core.$element.off(t, this._handlers[t])
        }
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != "function" && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Video = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this.core = e;
        this.core.options = t.extend({}, n.Defaults, this.core.options);
        this.swapping = true;
        this.previous = s;
        this.next = s;
        this.handlers = {
            "change.owl.carousel": t.proxy(function(t) {
                if (t.namespace && t.property.name == "position") {
                    this.previous = this.core.current();
                    this.next = t.property.value
                }
            }, this),
            "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(function(t) {
                if (t.namespace) {
                    this.swapping = t.type == "translated"
                }
            }, this),
            "translate.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn)) {
                    this.swap()
                }
            }, this)
        };
        this.core.$element.on(this.handlers)
    };
    n.Defaults = {
        animateOut: false,
        animateIn: false
    };
    n.prototype.swap = function() {
        if (this.core.settings.items !== 1) {
            return
        }
        if (!t.support.animation || !t.support.transition) {
            return
        }
        this.core.speed(0);
        var e, i = t.proxy(this.clear, this),
            s = this.core.$stage.children().eq(this.previous),
            n = this.core.$stage.children().eq(this.next),
            r = this.core.settings.animateIn,
            o = this.core.settings.animateOut;
        if (this.core.current() === this.previous) {
            return
        }
        if (o) {
            e = this.core.coordinates(this.previous) - this.core.coordinates(this.next);
            s.one(t.support.animation.end, i).css({
                left: e + "px"
            }).addClass("animated owl-animated-out").addClass(o)
        }
        if (r) {
            n.one(t.support.animation.end, i).addClass("animated owl-animated-in").addClass(r)
        }
    };
    n.prototype.clear = function(e) {
        t(e.target).css({
            left: ""
        }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut);
        this.core.onTransitionEnd()
    };
    n.prototype.destroy = function() {
        var t, e;
        for (t in this.handlers) {
            this.core.$element.off(t, this.handlers[t])
        }
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != "function" && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Animate = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = function(e) {
        this._core = e;
        this._timeout = null;
        this._paused = false;
        this._handlers = {
            "changed.owl.carousel": t.proxy(function(t) {
                if (t.namespace && t.property.name === "settings") {
                    if (this._core.settings.autoplay) {
                        this.play()
                    } else {
                        this.stop()
                    }
                } else if (t.namespace && t.property.name === "position") {
                    if (this._core.settings.autoplay) {
                        this._setAutoPlayInterval()
                    }
                }
            }, this),
            "initialized.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._core.settings.autoplay) {
                    this.play()
                }
            }, this),
            "play.owl.autoplay": t.proxy(function(t, e, i) {
                if (t.namespace) {
                    this.play(e, i)
                }
            }, this),
            "stop.owl.autoplay": t.proxy(function(t) {
                if (t.namespace) {
                    this.stop()
                }
            }, this),
            "mouseover.owl.autoplay": t.proxy(function() {
                if (this._core.settings.autoplayHoverPause && this._core.is("rotating")) {
                    this.pause()
                }
            }, this),
            "mouseleave.owl.autoplay": t.proxy(function() {
                if (this._core.settings.autoplayHoverPause && this._core.is("rotating")) {
                    this.play()
                }
            }, this),
            "touchstart.owl.core": t.proxy(function() {
                if (this._core.settings.autoplayHoverPause && this._core.is("rotating")) {
                    this.pause()
                }
            }, this),
            "touchend.owl.core": t.proxy(function() {
                if (this._core.settings.autoplayHoverPause) {
                    this.play()
                }
            }, this)
        };
        this._core.$element.on(this._handlers);
        this._core.options = t.extend({}, n.Defaults, this._core.options)
    };
    n.Defaults = {
        autoplay: false,
        autoplayTimeout: 5e3,
        autoplayHoverPause: false,
        autoplaySpeed: false
    };
    n.prototype.play = function(t, e) {
        this._paused = false;
        if (this._core.is("rotating")) {
            return
        }
        this._core.enter("rotating");
        this._setAutoPlayInterval()
    };
    n.prototype._getNextTimeout = function(s, n) {
        if (this._timeout) {
            e.clearTimeout(this._timeout)
        }
        return e.setTimeout(t.proxy(function() {
            if (this._paused || this._core.is("busy") || this._core.is("interacting") || i.hidden) {
                return
            }
            this._core.next(n || this._core.settings.autoplaySpeed)
        }, this), s || this._core.settings.autoplayTimeout)
    };
    n.prototype._setAutoPlayInterval = function() {
        this._timeout = this._getNextTimeout()
    };
    n.prototype.stop = function() {
        if (!this._core.is("rotating")) {
            return
        }
        e.clearTimeout(this._timeout);
        this._core.leave("rotating")
    };
    n.prototype.pause = function() {
        if (!this._core.is("rotating")) {
            return
        }
        this._paused = true
    };
    n.prototype.destroy = function() {
        var t, e;
        this.stop();
        for (t in this._handlers) {
            this._core.$element.off(t, this._handlers[t])
        }
        for (e in Object.getOwnPropertyNames(this)) {
            typeof this[e] != "function" && (this[e] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.autoplay = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    "use strict";
    var n = function(e) {
        this._core = e;
        this._initialized = false;
        this._pages = [];
        this._controls = {};
        this._templates = [];
        this.$element = this._core.$element;
        this._overrides = {
            next: this._core.next,
            prev: this._core.prev,
            to: this._core.to
        };
        this._handlers = {
            "prepared.owl.carousel": t.proxy(function(e) {
                if (e.namespace && this._core.settings.dotsData) {
                    this._templates.push('<div class="' + this._core.settings.dotClass + '">' + t(e.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
                }
            }, this),
            "added.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._core.settings.dotsData) {
                    this._templates.splice(t.position, 0, this._templates.pop())
                }
            }, this),
            "remove.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._core.settings.dotsData) {
                    this._templates.splice(t.position, 1)
                }
            }, this),
            "changed.owl.carousel": t.proxy(function(t) {
                if (t.namespace && t.property.name == "position") {
                    this.draw()
                }
            }, this),
            "initialized.owl.carousel": t.proxy(function(t) {
                if (t.namespace && !this._initialized) {
                    this._core.trigger("initialize", null, "navigation");
                    this.initialize();
                    this.update();
                    this.draw();
                    this._initialized = true;
                    this._core.trigger("initialized", null, "navigation")
                }
            }, this),
            "refreshed.owl.carousel": t.proxy(function(t) {
                if (t.namespace && this._initialized) {
                    this._core.trigger("refresh", null, "navigation");
                    this.update();
                    this.draw();
                    this._core.trigger("refreshed", null, "navigation")
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this.$element.on(this._handlers)
    };
    n.Defaults = {
        nav: false,
        navText: ["prev", "next"],
        navSpeed: false,
        navElement: "div",
        navContainer: false,
        navContainerClass: "owl-nav",
        navClass: ["owl-prev", "owl-next"],
        slideBy: 1,
        dotClass: "owl-dot",
        dotsClass: "owl-dots",
        dots: true,
        dotsEach: false,
        dotsData: false,
        dotsSpeed: false,
        dotsContainer: false
    };
    n.prototype.initialize = function() {
        var e, i = this._core.settings;
        this._controls.$relative = (i.navContainer ? t(i.navContainer) : t("<div>").addClass(i.navContainerClass).appendTo(this.$element)).addClass("disabled");
        this._controls.$previous = t("<" + i.navElement + ">").addClass(i.navClass[0]).html(i.navText[0]).prependTo(this._controls.$relative).on("click", t.proxy(function(t) {
            this.prev(i.navSpeed)
        }, this));
        this._controls.$next = t("<" + i.navElement + ">").addClass(i.navClass[1]).html(i.navText[1]).appendTo(this._controls.$relative).on("click", t.proxy(function(t) {
            this.next(i.navSpeed)
        }, this));
        if (!i.dotsData) {
            this._templates = [t("<div>").addClass(i.dotClass).append(t("<span>")).prop("outerHTML")]
        }
        this._controls.$absolute = (i.dotsContainer ? t(i.dotsContainer) : t("<div>").addClass(i.dotsClass).appendTo(this.$element)).addClass("disabled");
        this._controls.$absolute.on("click", "div", t.proxy(function(e) {
            var s = t(e.target).parent().is(this._controls.$absolute) ? t(e.target).index() : t(e.target).parent().index();
            e.preventDefault();
            this.to(s, i.dotsSpeed)
        }, this));
        for (e in this._overrides) {
            this._core[e] = t.proxy(this[e], this)
        }
    };
    n.prototype.destroy = function() {
        var t, e, i, s;
        for (t in this._handlers) {
            this.$element.off(t, this._handlers[t])
        }
        for (e in this._controls) {
            this._controls[e].remove()
        }
        for (s in this.overides) {
            this._core[s] = this._overrides[s]
        }
        for (i in Object.getOwnPropertyNames(this)) {
            typeof this[i] != "function" && (this[i] = null)
        }
    };
    n.prototype.update = function() {
        var t, e, i, s = this._core.clones().length / 2,
            n = s + this._core.items().length,
            r = this._core.maximum(true),
            o = this._core.settings,
            a = o.center || o.autoWidth || o.dotsData ? 1 : o.dotsEach || o.items;
        if (o.slideBy !== "page") {
            o.slideBy = Math.min(o.slideBy, o.items)
        }
        if (o.dots || o.slideBy == "page") {
            this._pages = [];
            for (t = s, e = 0, i = 0; t < n; t++) {
                if (e >= a || e === 0) {
                    this._pages.push({
                        start: Math.min(r, t - s),
                        end: t - s + a - 1
                    });
                    if (Math.min(r, t - s) === r) {
                        break
                    }
                    e = 0, ++i
                }
                e += this._core.mergers(this._core.relative(t))
            }
        }
    };
    n.prototype.draw = function() {
        var e, i = this._core.settings,
            s = this._core.items().length <= i.items,
            n = this._core.relative(this._core.current()),
            r = i.loop || i.rewind;
        this._controls.$relative.toggleClass("disabled", !i.nav || s);
        if (i.nav) {
            this._controls.$previous.toggleClass("disabled", !r && n <= this._core.minimum(true));
            this._controls.$next.toggleClass("disabled", !r && n >= this._core.maximum(true))
        }
        this._controls.$absolute.toggleClass("disabled", !i.dots || s);
        if (i.dots) {
            e = this._pages.length - this._controls.$absolute.children().length;
            if (i.dotsData && e !== 0) {
                this._controls.$absolute.html(this._templates.join(""))
            } else if (e > 0) {
                this._controls.$absolute.append(new Array(e + 1).join(this._templates[0]))
            } else if (e < 0) {
                this._controls.$absolute.children().slice(e).remove()
            }
            this._controls.$absolute.find(".active").removeClass("active");
            this._controls.$absolute.children().eq(t.inArray(this.current(), this._pages)).addClass("active")
        }
    };
    n.prototype.onTrigger = function(e) {
        var i = this._core.settings;
        e.page = {
            index: t.inArray(this.current(), this._pages),
            count: this._pages.length,
            size: i && (i.center || i.autoWidth || i.dotsData ? 1 : i.dotsEach || i.items)
        }
    };
    n.prototype.current = function() {
        var e = this._core.relative(this._core.current());
        return t.grep(this._pages, t.proxy(function(t, i) {
            return t.start <= e && t.end >= e
        }, this)).pop()
    };
    n.prototype.getPosition = function(e) {
        var i, s, n = this._core.settings;
        if (n.slideBy == "page") {
            i = t.inArray(this.current(), this._pages);
            s = this._pages.length;
            e ? ++i : --i;
            i = this._pages[(i % s + s) % s].start
        } else {
            i = this._core.relative(this._core.current());
            s = this._core.items().length;
            e ? i += n.slideBy : i -= n.slideBy
        }
        return i
    };
    n.prototype.next = function(e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(true), e)
    };
    n.prototype.prev = function(e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(false), e)
    };
    n.prototype.to = function(e, i, s) {
        var n;
        if (!s && this._pages.length) {
            n = this._pages.length;
            t.proxy(this._overrides.to, this._core)(this._pages[(e % n + n) % n].start, i)
        } else {
            t.proxy(this._overrides.to, this._core)(e, i)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Navigation = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    "use strict";
    var n = function(i) {
        this._core = i;
        this._hashes = {};
        this.$element = this._core.$element;
        this._handlers = {
            "initialized.owl.carousel": t.proxy(function(i) {
                if (i.namespace && this._core.settings.startPosition === "URLHash") {
                    t(e).trigger("hashchange.owl.navigation")
                }
            }, this),
            "prepared.owl.carousel": t.proxy(function(e) {
                if (e.namespace) {
                    var i = t(e.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
                    if (!i) {
                        return
                    }
                    this._hashes[i] = e.content
                }
            }, this),
            "changed.owl.carousel": t.proxy(function(i) {
                if (i.namespace && i.property.name === "position") {
                    var s = this._core.items(this._core.relative(this._core.current())),
                        n = t.map(this._hashes, function(t, e) {
                            return t === s ? e : null
                        }).join();
                    if (!n || e.location.hash.slice(1) === n) {
                        return
                    }
                    e.location.hash = n
                }
            }, this)
        };
        this._core.options = t.extend({}, n.Defaults, this._core.options);
        this.$element.on(this._handlers);
        t(e).on("hashchange.owl.navigation", t.proxy(function(t) {
            var i = e.location.hash.substring(1),
                n = this._core.$stage.children(),
                r = this._hashes[i] && n.index(this._hashes[i]);
            if (r === s || r === this._core.current()) {
                return
            }
            this._core.to(this._core.relative(r), false, true)
        }, this))
    };
    n.Defaults = {
        URLhashListener: false
    };
    n.prototype.destroy = function() {
        var i, s;
        t(e).off("hashchange.owl.navigation");
        for (i in this._handlers) {
            this._core.$element.off(i, this._handlers[i])
        }
        for (s in Object.getOwnPropertyNames(this)) {
            typeof this[s] != "function" && (this[s] = null)
        }
    };
    t.fn.owlCarousel.Constructor.Plugins.Hash = n
})(window.Zepto || window.jQuery, window, document);
(function(t, e, i, s) {
    var n = t("<support>").get(0).style,
        r = "Webkit Moz O ms".split(" "),
        o = {
            transition: {
                end: {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "oTransitionEnd",
                    transition: "transitionend"
                }
            },
            animation: {
                end: {
                    WebkitAnimation: "webkitAnimationEnd",
                    MozAnimation: "animationend",
                    OAnimation: "oAnimationEnd",
                    animation: "animationend"
                }
            }
        },
        a = {
            csstransforms: function() {
                return !!h("transform")
            },
            csstransforms3d: function() {
                return !!h("perspective")
            },
            csstransitions: function() {
                return !!h("transition")
            },
            cssanimations: function() {
                return !!h("animation")
            }
        };

    function h(e, i) {
        var o = false,
            a = e.charAt(0).toUpperCase() + e.slice(1);
        t.each((e + " " + r.join(a + " ") + a).split(" "), function(t, e) {
            if (n[e] !== s) {
                o = i ? e : true;
                return false
            }
        });
        return o
    }

    function l(t) {
        return h(t, true)
    }
    if (a.csstransitions()) {
        t.support.transition = new String(l("transition"));
        t.support.transition.end = o.transition.end[t.support.transition]
    }
    if (a.cssanimations()) {
        t.support.animation = new String(l("animation"));
        t.support.animation.end = o.animation.end[t.support.animation]
    }
    if (a.csstransforms()) {
        t.support.transform = new String(l("transform"));
        t.support.transform3d = a.csstransforms3d()
    }
})(window.Zepto || window.jQuery, window, document);
$('a[href*="#scroll-"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
        var t = $(this.hash);
        t = t.length ? t : $("[name=" + this.hash.slice(1) + "]");
        if (t.length) {
            $("html, body").animate({
                scrollTop: t.offset().top
            }, 1e3);
            return false
        }
    }
});
$(document).ready(function() {
    $("#js-product-slider1").owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        nav: false
    });
    $("#js-product-slider2").owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        nav: false
    });
    $("#js-reviews-slider").owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        nav: true,
        navContainerClass: "reviews-slider__arrows",
        navClass: ["arrow--left-w", "arrow--right-w"]
    })
});