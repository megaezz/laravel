﻿$(document).ready(function() {

	$(".various").fancybox({
		maxWidth	: '90%',
		maxHeight	: '90%',
		width		: '90%',
		height		: 'auto',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'

	});

	$('.slider1').bxSlider({
		mode: 'vertical',
	   	slideWidth: 800,
	   	minSlides: 1,
	   	maxSlides: 1,
	   	slideMargin:0,
	   	adaptiveHeight: true
	});

	$(".fancybox")
	.attr('rel', 'gallery')
	.fancybox({
		type: 'iframe',
		autoSize : false,
		width	 : 800,
		height	 : 600,
		padding  : 0
	});

	$(".open-callback").click(function() {
		window.scrollTo(0, 0);
	});

	$('.timecount').county({ endDateTime: ((new Date()).getTime() + (21*60*60*1000) + (47*60*1000) + (36*1000)), reflection: false, animation: 'scroll', theme: 'black' });

	$('.coupon').click(function() {
		var code = prompt('Введите код купона', '');
		if(!code) return;
		$.post('index.html', {coupon: 1, offer: offer_id, code: code}, function(r) {
			if(r.success) {
				offer_id = r.offer_id;
				if(r.curr == 'BYR') r.curr = 'руб.';
				if(r.curr == 'RUB') r.curr = 'руб.';
				if(r.curr == 'UAH') r.curr = 'грн.';
				$('.coupon').html('Стоимость по купону: ' + r.price + ' ' + r.curr);
				$('.price-rw').html(r.price);
			} else {
				alert(r.message);
			}
		}, 'json');
	});

	$('.bottom-form .submit').bind('click', function() {
		// $('.order-form .fio').val($('.bottom-form .fio').val());
		// $('.order-form .addr').val($('.bottom-form .addr').val());
		// $('.order-form .tel').val($('.bottom-form .tel').val());
		// $('.order-form .cnt').val($('.bottom-form .cnt').val());
		// window.scrollTo(0, 0);
		// $('.order-form .submit').click();
	});

	$('.order-form .submit').bind('click', function() {

		if(!$('.order-form .fio').val()) {
			alert('Вы не указали Фамилию, Имя или Отчество!');
			$('.order-form .fio').focus();
			return;
		}

		if(!$('.order-form .addr').val()) {
			alert('Вы не указали Адрес доставки!');
			$('.order-form .addr').focus();
			return;
		}

		if(!$('.order-form .tel').val()) {
			alert('Вы не указали Телефон!');
			$('.order-form .tel').focus();
			return;
		}

		$('.order-form .submit').hide();
		$('.order-form .loader').show();

		$.post('index.html', {
			order : 1,
			// debug: 1,
			// test: 1,
			offer: offer_id,
			cnt : 1, //$('.order-form .cnt').val(),
			fio : $('.order-form .fio').val(),
			tel : $('.order-form .tel').val(),
			addr: $('.order-form .addr').val(),
			timezone: -(new Date().getTimezoneOffset()/60),
			comment: comment
		}, function(r) {
			if(r.fail) {
				alert(r.message);
			} else if(r.success) {
				location.href='finish?no=' + r.result;
				$('.cnt').val(1);
				$('.fio').val('');
				$('.addr').val('');
				$('.tel').val('');
			}

			$('.order-form .loader').hide();
			$('.order-form .submit').show();
		}, 'json');

	});



});



