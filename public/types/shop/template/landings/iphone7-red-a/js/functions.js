$(function(){
    $('a[href^="#"]').click(function (){
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top -50;
        jQuery("html:not(:animated), body:not(:animated)").animate({scrollTop: destination}, 800);
        return false;
    });
    times=function(){         
        now = new Date();  
        
        hour=24-now.getHours();
        minu=60-now.getMinutes();
        secu=60-now.getSeconds(); 
        str=((hour+'').length==1?hour='0'+hour:hour)+'';
        $('.hours').html(str);
        str=((minu+'').length==1?minu='0'+minu:minu)+'';
        $('.minutes').html(str);
        str=((secu+'').length==1?secu='0'+secu:secu)+'';
        $('.seconds').html(str);
    }   
    times();                                              
    setInterval(times,1000);
});