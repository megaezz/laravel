$(function () {

    $('.img-wrap-top .btn-wrap-small .btn').click(function () {
        $('.img-wrap-top .btn-wrap-small .btn').removeClass('active');
        $(this).addClass('active');

    })

    $('.img-wrap-top .btn-wrap-small .btn-1').click(function () {

        $('.img-2').removeClass('active');
        $('.img-2-1').addClass('active');
    })
    $('.img-wrap-top .btn-wrap-small .btn-2').click(function () {
        $('.img-2').removeClass('active');
        $('.img-2-2').addClass('active');
    })

    $('.img-top-wrap .btn-wrap-small .btn').click(function () {
        $('.img-top-wrap .btn-wrap-small .btn').removeClass('active');
        $(this).addClass('active');

    })

    $('.img-top-wrap .btn-wrap-small .btn-1').click(function () {

        $('.img-topt-2').removeClass('active');
        $('.img-topt-2-1').addClass('active');
    })
    $('.img-top-wrap .btn-wrap-small .btn-2').click(function () {
        $('.img-topt-2').removeClass('active');
        $('.img-topt-2-2').addClass('active');
    })


    var toggMnu = $(".toggle-mnu").click(function () {

        $(this).toggleClass("on");
        $(".hidden-mnu").slideToggle();
        return false;
    });

    $(function () {
        $('.hidden-mnu ul li a').on('click', function () {
            $(".toggle-mnu").click();
        });
    });





    $('.s-slider-owl-carousel').owlCarousel({
        items: 1,

        lazyLoad: true,
        loop: true,
        margin: 10,
        nav: true,
        // autoplay:true,
        // autoplayTimeout:4000,
        navText: ["d", "f"]
    });
    $('.s-otz-owl-carousel').owlCarousel({
        items: 1,

        lazyLoad: true,
        loop: true,
        margin: 20,
        nav: true,
        // autoplay:true,
        // autoplayTimeout:4000,
        navText: ["r", "t"],

    });





    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });




    function heightses() {
        $(".s-preim .wrap-item ").height('auto').equalHeights();
        $(".s-work .text-wrap ").height('auto').equalHeights();
        $(".s-opis .wrap-item ").height('auto').equalHeights();
        // $(".prod-img").height($(".prod-img").width()); 

    }

    $(window).resize(function () {
        heightses();
    });

    heightses();




    $(".header-text-wrap a, .main-r a, .s-opis .btn-accent, .landingMenu a, .landingMenu2 a, .btn-d, .btn-d, .s-work a, .btn-accent.s-d, .main-r .btn-accent").click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;

        $('html, body').animate({scrollTop: destination}, 1100);

        return false;
    });


    $(function () {

        $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
            $(this)
                    .addClass('active').siblings().removeClass('active')
                    .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });

    });



    $(".col-1 img").click(function () {
        $(".col-1 img").removeClass("active");
        $(this).addClass("active");

        $("form #order").val($(this).next().text());
    })


    $(".col-2 img").click(function () {
        $(".colors-wrap img").removeClass("active");
        $(this).addClass("active");

        $("form #order2").val($(this).next().text());
    })





    $(".modal_btn").click(function (e) {
        e.preventDefault();
        //$('#small-modal').arcticmodal();
        $('#modal').arcticmodal({afterClose: function (data, el) {
                $("body").css({"overflow-y": "scroll"})
            }});

    });





    $('section').magnificPopup({
        delegate: 'a.gal1',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            // titleSrc: function(item) {
            //   return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
            // }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function (element) {
                return element.find('img');
            }
        }

    });

    $('.tabs__content').magnificPopup({
        delegate: 'a.gal2',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            // titleSrc: function(item) {
            //   return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
            // }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function (element) {
                return element.find('img');
            }
        }

    });


// timer
    GetCount();


    $('.tabs__content a.big').attr('href', $(this).find('img').attr('src'));

    $('.tabs__content .color').click(function () {
        $('.tabs__content .color').removeClass('active');
        $(this).addClass('active');
        $(this).parent().parent().find('  img').attr('src', $(this).attr('ref'))
    })

    $(".phone").inputmask("+7 (999) 999-99-99");

});

