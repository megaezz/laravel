"use strict";
$(document).ready(function(){function e(e){var o=Date.parse(e)-Date.parse(new Date),t=Math.floor(o/1e3%60),n=Math.floor(o/1e3/60%60);return{total:o,hours:Math.floor(o/36e5%24),minutes:n,seconds:t}}$(function(){if(window.location.hash){var e=window.location.hash;$(e).modal("toggle")}}),$(".slider__wrapper").slick({slidesToShow:1,slidesToScroll:1,dots:!1});var o=new Date(Date.parse(new Date)+18e5);!function(o,t){function n(){var o=e(t);a.innerHTML=("0"+o.hours).slice(-2),s.innerHTML=("0"+o.minutes).slice(-2),l.innerHTML=("0"+o.seconds).slice(-2),o.total<=0&&clearInterval(i)}var r=document.getElementById(o),a=r.querySelector(".hours"),s=r.querySelector(".minutes"),l=r.querySelector(".seconds");n();var i=setInterval(n,1e3)}("clockdiv",o)
	$('.button').click(function(){
		$('.modal').modal('show')
	})
});