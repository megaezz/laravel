$(document).ready(function(){

	var day = true;

	$(".main_slider").mousemove(function(event){
		var sm = $(".slider").offset();
		if(day){
			if(event.pageX-sm.left>85 && event.pageX-sm.left<675){
				$(".img2").css("width", event.pageX-sm.left);
				$(".raz").css("left", event.pageX-sm.left-22);
			}
		} else {
			if(event.pageX-sm.left>85 && event.pageX-sm.left<675){
				$(".img4").css("width", event.pageX-sm.left);
				$(".raz").css("left", event.pageX-sm.left-22);
			}
		}
	});


	$(".main_slider").hover(function(event){}, function(){
		if(day){
			$(".img2").css("width", "386px");
			$(".raz").css("left", "363px");
		}else{
			$(".img4").css("width", "386px");
			$(".raz").css("left", "363px");
		}
	});

	$(".zakaz").click(function(event){
		event.preventDefault();
		var form_top = $(".hot_form_wrap").offset().top;

		$('html, body').animate({scrollTop: form_top}, 400);
	});


	$(".day").click(function(){
		day = true;
		$(this).css("background-position", "0 0");
		$(".night").css("background-position", "87px 0");
		$(".img1").show(0);
		$(".img2").show(0);
		$(".img3").hide(0);
		$(".img4").hide(0);
	});

	$(".night").click(function(){
		day = false;
		$(".day").css("background-position", "73px 0");
		$(this).css("background-position", "0 0");
		$(".img4").show(0);
		$(".img3").show(0);
		$(".img2").hide(0);
		$(".img1").hide(0);
	});

	(function($) {
		function Placeholder(input) {
			this.input = input;
			if (input.attr('type') == 'password') {
				this.handlePassword();
			}
			// Prevent placeholder values from submitting
			$(input[0].form).submit(function() {
				if (input.hasClass('placeholder') && input[0].value == input.attr('placeholder')) {
					input[0].value = '';
				}
			});
		}
		Placeholder.prototype = {
			show : function(loading) {
				// FF and IE saves values when you refresh the page. If the user refreshes the page with
				// the placeholders showing they will be the default values and the input fields won't be empty.
				if (this.input[0].value === '' || (loading && this.valueIsPlaceholder())) {
					if (this.isPassword) {
						try {
							this.input[0].setAttribute('type', 'text');
						} catch (e) {
							this.input.before(this.fakePassword.show()).hide();
						}
					}
					this.input.addClass('placeholder');
					this.input[0].value = this.input.attr('placeholder');
				}
			},
			hide : function() {
				if (this.valueIsPlaceholder() && this.input.hasClass('placeholder')) {
					this.input.removeClass('placeholder');
					this.input[0].value = '';
					if (this.isPassword) {
						try {
							this.input[0].setAttribute('type', 'password');
						} catch (e) { }
						// Restore focus for Opera and IE
						this.input.show();
						this.input[0].focus();
					}
				}
			},
			valueIsPlaceholder : function() {
				return this.input[0].value == this.input.attr('placeholder');
			},
			handlePassword: function() {
				var input = this.input;
				input.attr('realType', 'password');
				this.isPassword = true;
				// IE < 9 doesn't allow changing the type of password inputs
				if ($.browser.msie && input[0].outerHTML) {
					var fakeHTML = $(input[0].outerHTML.replace(/type=(['"])?password\1/gi, 'type=$1text$1'));
					this.fakePassword = fakeHTML.val(input.attr('placeholder')).addClass('placeholder').focus(function() {
						input.trigger('focus');
						$(this).hide();
					});
					$(input[0].form).submit(function() {
						fakeHTML.remove();
						input.show()
					});
				}
			}
		};
		var NATIVE_SUPPORT = !!("placeholder" in document.createElement( "input" ));
		$.fn.placeholder = function() {
			return NATIVE_SUPPORT ? this : this.each(function() {
				var input = $(this);
				var placeholder = new Placeholder(input);
				placeholder.show(true);
				input.focus(function() {
					placeholder.hide();
				});
				input.blur(function() {
					placeholder.show(false);
				});

				// On page refresh, IE doesn't re-populate user input
				// until the window.onload event is fired.
				if ($.browser.msie) {
					$(window).load(function() {
						if(input.val()) {
							input.removeClass("placeholder");
						}
						placeholder.show(true);
					});
					input.focus(function() {
						if(this.value == "") {
							var range = this.createTextRange();
							range.collapse(true);
							range.moveStart('character', 0);
							range.select();
						}
					});
				}
			});
		}
	})(jQuery);



	$('input[name*="telephone"]').attr("placeholder", "Телефон").placeholder();
	$('input[name*="name"]').attr("placeholder", "Имя").placeholder();

	var text1_1 = "Ночная акция! С 22:00-02:00  скидка 50%!<br> Осталось всего 4 штуки!";
	var text2_1 = "С 22:00-02:00  скидка 50%. Внимание! осталось 4 штуки.";
	var text1_2 = "5 часов тотальной распродажи!<br> С 02:00 до 07:00 скидка 50%!";
	var text2_2 = "С 02:00 до 07:00 скидка 50%. Внимание! осталось 4 штуки.";
	var text1_3 = "3 часа тотальной распродажи!<br> С 07:00 до 10:00 скидка 50%!";
	var text2_3 = "С 07:00 до 10:00 скидка 50%. Внимание! осталось 4 штуки.";
	var text1_4 = "4 часа тотальной распродажи!<br> С 10:00 до 14:00 скидка 50%!";
	var text2_4 = "С 10:00 до 14:00 скидка 50%. Внимание! осталось 4 штуки.";
	var text1_5 = "4 часа безумных скидок!<br> С 14:00 до 18:00 скидка 50%!";
	var text2_5 = "С 14:00 до 18:00 скидка 50%. Внимание! осталось 4 штуки.";
	var text1_6 = "Ограниченная акция!<br> С 18:00 до 22:00 скидка 50%!";
	var text2_6 = "С 18:00 до 22:00 скидка 50%. Внимание! осталось 4 штуки.";
	var date = new Date();
	var day = date.getDate();
	var year = date.getFullYear();
	var hour = date.getHours();
	var minutes = date.getMinutes();
	var mounth = date.getMonth();
	var end_date;

	if(hour>=22 || hour<2){
		$('.variant_text1').html(text1_1);
		$('.variant_text2').html(text2_1);
		if(hour<2){
			end_date = new Date(year, mounth, day, 1, 59, 59, 0);
		} else {
			end_date = new Date(year, mounth, date.getDate()+1, 1, 59, 59, 0);
		}

		myTime(end_date.getTime());
	}
	if(hour>=2 && hour<7){
		$('.variant_text1').html(text1_2);
		$('.variant_text2').html(text2_2);
		end_date = new Date(year, mounth, day, 6, 59, 59, 0);
		myTime(end_date.getTime());
	}
	if(hour>=7 && hour<10){
		$('.variant_text1').html(text1_3);
		$('.variant_text2').html(text2_3);
		end_date = new Date(year, mounth, day, 9, 59, 59, 0);
		myTime(end_date.getTime());
	}
	if(hour>=10 && hour<14){
		$('.variant_text1').html(text1_4);
		$('.variant_text2').html(text2_4);
		end_date = new Date(year, mounth, day, 13, 59, 59, 0);
		myTime(end_date.getTime());
	}
	if(hour>=14 && hour<18){
		$('.variant_text1').html(text1_5);
		$('.variant_text2').html(text2_5);
		end_date = new Date(year, mounth, day, 17, 59, 59, 0);
		myTime(end_date.getTime());
	}
	if(hour>=18 && hour<22){
		$('.variant_text1').html(text1_6);
		$('.variant_text2').html(text2_6);
		end_date = new Date(year, mounth, day, 21, 59, 59, 0);
		myTime(end_date.getTime());
	}

	fixMenuHeight();
	$(window).resize(function(){
		fixMenuHeight();
	});

	$('.fix-order-btn').on('click', function(event){
		event.preventDefault();
		var form_pos = $('#hr5').offset().top;
		var menu_height = $('.fix-menu').height();

		$('html, body').animate({scrollTop: form_pos-menu_height}, 400);
	})

});

function myTime(end_date){

	var time_current = new Date();

	time_current = time_current.getTime();

	var time_interval = (end_date - time_current)/1000;

	$('.counter').each(function(){
		timer(this, time_interval);
	});
}

function timer(element, counter) {
	var el = $(element);
	var html = '';
	if (counter) {
		el.html(formatTime(counter));
		setInterval(function(){
			if (counter > 1) {
				counter = counter - 1;
				el.html(formatTime(counter));
			}
		}, 1000);
	}
}

function two(a) {
	return parseInt(a - Math.floor(a/10)*10).toString();
}

function one(a) {
	return Math.floor(a/10).toString();
}

function formatTime(seconds) {
	seconds = Math.floor(seconds);
	var minutes = Math.floor(seconds / 60), hours = Math.floor(minutes / 60), days = Math.floor(hours / 24);
	//hours = hours % 24;
	minutes %= 60;
	seconds %= 60;
	var hours_name;
	if(hours==1){
		hours_name = "час";
	} else {
		if(hours==0){
			hours_name = "часов";
		} else {
			hours_name = "часа";
		}
	}

	html = '<li class="count-div">' + one(hours) + two(hours) + '</li><li class="c_sepor">:</li><li class="count-div">' + one(minutes) + two(minutes) + '</li><li class="c_sepor">:</li><li class="count-div ">' + one(seconds) + two(seconds) + '</li>';
	return html;
};

function fixMenuHeight(){
	var menu_height = $('.fix-menu').height();
	$('body').css({'padding-top': menu_height + 'px'});
}
