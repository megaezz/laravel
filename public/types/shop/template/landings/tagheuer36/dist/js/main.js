App = {

	//
	// ������ �������
	// --------------------------------------------------
	Init : function() {
		$(document).ready(function() {

			//������
			App.Func.timer();	

			//���� ��� ������
			App.Func.share_to();

			//animate
			App.Func.animate();

			//�������� ������ "������ ��?"
			App.Func.animate_why();
			
			//����������� ����
			App.Func.menu();

			//��������� ������� ����
			App.Func.scrollspynav();

			//������� ���������
			App.Func.smoothscroll();

			//�������� ����� ������
			App.Func.open_feedback();

			//��������� ������� ���������
			App.Func.validExtend();

			//�������������� ���������
			App.Func.formValid();

			//���������� ��� � ���������
			//App.Func.addLeadAnalytics();

			//�������� ����
			var feedback_modal = new App.Func.feedback_send('#form-modal');
			var feedback1 = new App.Func.feedback_send('#feedback1');

			//Fancybox
			App.Func.fancy();

			//���������� �����
			App.Func.city_ya_detect();

		})
	},



	//
	// ���������
	// --------------------------------------------------
	Const : {
		MODAL : {
			quest : {
				title : "������ ������",
				desc : "��������� �����, � �� � ���� ����������� ��������"
			},
			order : {
				title : "�������� ������",
				desc : "��������� �����, � �� � ��� ���������� � ��������� �����."
			},
			feedcall : {
				title : "�������� ������",
				desc : "��������� �����, � �� � ��� ���������� � ��������� �����."
			}
		}
	},



	//
	// ��� �������
	// --------------------------------------------------
	Func : {


		//
		// ������
		// --------------------------------------------------
		timer : function() {

			//�������� ������� ����
			var time_real = new Date;

			//������� ����� �������
			var time_future = time_real.getHours()+3;
				time_future = time_real.setHours(time_future);

			//��������� ������
			var timerId =
				countdown(
					time_future,
					function(ts) {

						var hours = App.Func.timerNorm(ts.hours);
						var minutes = App.Func.timerNorm(ts.minutes);
						var seconds = App.Func.timerNorm(ts.seconds);

						$('#hours').text(hours);
						$('#minutes').text(minutes);
						$('#seconds').text(seconds);

					},
					countdown.HOURS|countdown.MINUTES|countdown.SECONDS);
		},



		//
		// ������������ �������� �������
		// --------------------------------------------------
		timerNorm : function(num) {

			return  num<10 ? '0'+num : num;

		},



		//
		// ���������� ���� ����������� ���
		// --------------------------------------------------
		share_to : function() {

			var qwmonth = [
				'������',
				'�������',
				'�����',
				'������',
				'���',
				'����',
				'����',
				'�������',
				'��������',
				'�������',
				'������',
				'�������'
			];

			//�������� ����
			var time_real = new Date;

			//�������� ��������� ����
			var time_future = new Date(time_real.getFullYear(), time_real.getMonth(), time_real.getDate()+1);

			//�������������
			$('#share_to').text(time_future.getDate() + ' ' + qwmonth[time_future.getMonth()]);

		},



		//
		// �������� 
		// --------------------------------------------------
		animate : function() {

			/* Mobile & Animation */
			var Android = navigator.userAgent.search(/Android/i);
			var iPhone = navigator.userAgent.search(/iPhone/i);
			var iPad = navigator.userAgent.search(/iPad/i); 

			if(Android != -1 || iPhone != -1 || iPad != -1) { 
				// ���� �������
				$(window).resize(function(event) {
					$('html').css('width', window.innerWidth + 'px'); 
				});
				
				
			} else { 
				// ���� �������
				$(".scroll").each(function() {
					var block = $(this);
					$(window).scroll(function() {
						var top = block.offset().top;
						var bottom = block.height() + top;
						top = top - $(window).height();
						var scroll_top = $(this).scrollTop();
						if ((scroll_top > top) && (scroll_top < bottom)) {
							if (!block.hasClass("animated")) {
								block.addClass("animated");
								block.trigger('animateIn');
							}
						} else {
								block.removeClass("animated");
								block.trigger('animateOut');
						}
					});
				}); 
			}
		},



		//
		// �������� ������ ������ ��
		// --------------------------------------------------
		animate_why : function() {
			$(".steps em").each(function() {
				$(this).attr("data-number", parseInt($(this).text()));
			});
			$(".steps").on("animateIn", function() {
				var inter = 1;
				$(this).find("em").each(function() {
					var count = parseInt(	$(this).attr("data-number")), 
											block = $(this), 
											timeout = null, 
											step = 1; 
											timeout = setInterval(function() {
												if (step == 25) {
													block.text(count.toString());clearInterval(timeout);
												} else {
													block.text((Math.floor(count*step/25)).toString());
													step++;
												}
											}, 60);
										});
			}).on('animateOut', function() {
				$(this).find('.anim').each(function() {
					$(this).css('opacity', 0.01);
					$(this).css({
						'-webkit-transform': 'scale(0.7, 0.7)', 
						'-moz-transform': 'scale(0.7, 0.7)'});
				});
			});	
		},



		//
		// ���������� �������� ����
		// --------------------------------------------------
		menu : function() {
			var nav = $('.navbar');

			// ����������
			function newWinSize() {
				var navheight = nav.height();
				$('body').css('padding-top', navheight+'px');
			}

			$(window).resize(function(event) {
				newWinSize();
			});

			newWinSize();	

			// �������� popup ��� ����� � ��������� ������	
			$('.navbar-collapse').click(function(event) {
				$(this).removeClass('in');
			});




		},



		//
		// ��������� ������� ���� ��� ����������� ���������������� �����
		// --------------------------------------------------
		scrollspynav : function() {
			$('body').scrollspy({ 
				target: '.navbar-theme',
				offset: 65
			});
		},




		//
		// ������� ���������
		// --------------------------------------------------
		smoothscroll : function() {
			$('.navbar-theme a').smoothScroll({
				speed: 1000,
				offset: -61,
			});
		},



		//
		// �������� ����� �������� �����
		// --------------------------------------------------
		open_feedback : function() {
			$('.open-feedback').click(function(){
				var type = $(this).data('type');
				$('.feed_back_win').find('h3').text(App.Const.MODAL[type].title);
				$('.feed_back_win').find('.desc').text(App.Const.MODAL[type].desc);
				$('.feed_back_win').modal('show');

				return false;
			});

			$('#close').click(function() {
				$('.feed_back_win').modal('hide');
			});
		},



		//
		// ����������� �� �������� ��������
		// --------------------------------------------------
		load_info_win : function(rezult) {

			$('.info_win').find('h3').text(rezult['title']);
			$('.info_win').find('p').text(rezult['desc']);

			$('.info_win').modal('show');
			$('.info_win').on('click', '#close', function(){
				$('.info_win').modal('hide');
			});			
		},



		//
		// ��������� ������� ���������
		// --------------------------------------------------
		validExtend : function() {
			jQuery.validator.addMethod("phone", function(value, element) {
				return this.optional(element) || /^[0-9\s+\(\)-]+$/.test(value);
			},	"������ ����� � �������");
		},



		//
		// ��������� ����
		// --------------------------------------------------
		formValid : function() {
			$('#form-modal').validate(App.Func.validParam);
			$('#feedback1').validate(App.Func.validParam);
		},	// <--- Close: ��������� ����



		//
		// ��������� ���������
		// --------------------------------------------------
		validParam : {
				rules: {
					tel: {
						required: true,
						minlength: 10,
						phone: true
					}
				},
				messages: {
					tel: {
						required: '������� ��� ��������� �������.',
						minlength: '����� ������� ��������.'
					}
				},
				errorElement : 'div',
				errorClass : 'error red'
		},	// <--- Close: ��������� ���������



		//
		// �������� URL
		// --------------------------------------------------
		get_url_param : function(name) {
			//��� �������� UTM
			return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
		},
		
		addLeadAnalytics : function(product) {
			ga('ec:addProduct', {
			    'id': product.id,
			    'name': product.name,
			    'category': product.category,
			    'brand': product.brand,
				'variant': product.variant,
				'price': product.price,
				'quantity': product.qty
			});
			ga('set', '&cu', 'RUB');
			ga('ec:setAction', 'purchase', {
				'id': product.dealid,
				'tax': '',
				'shipping': '400',
			});
		},


		//
		// �������� ����� �������� �����
		// --------------------------------------------------
		feedback_send : function(form) {

			$(form).submit(function(event) {
				event.preventDefault();
				if($(form).find('input.error').length <= 0) {
					
					//var dataSend = $(this).serialize();
					//var action = $(this).attr('action');  // <--- ����� URL �� �����
					var action = '';

					var thisForm = $(this),
						$form = $(this),
						name = $('input[name="name"]', $form).val(),
						phone = $('input[name="tel"]', $form).val(),
						adr = $('input[name="adr"]', $form).val(),
						city = $('input[name="city"]', $form).val(),
						ques = $('textarea[name="message"]', $form).val(),
						sbt = $('input[type="button"]', $form).attr("name"),
						submit = $('input[name='+sbt+']', $form).val();

						var formname = $('input[name="formname"]').val();						
						if(ques==undefined) ques = '';

						var utm_source=App.Func.get_url_param("utm_source");
						if(utm_source==null) utm_source = '';


						var dataSend =	"name="+name+
										"&phone="+phone+
										"&action=zakaz"+
										"&address="+
										"&city="+city+
										"&ques="+ques+
										"&code=carrera"+
										"&price=3990"+
										"&product_param2="+utm_source
										"&formname="+
										"&ref=";

					$.ajax({
						type: "POST",
						url: action,
						data: dataSend,
						dataType: "html",
						crossDomain : true,
						beforeSend: function(){

							//������ ������� �� ������ ��� �������� ���������
							thisForm.find('button').text('���������').attr('disabled','true');

						},
						complete: function(){

							thisForm.find('button').text('����������').removeAttr('disabled');
							$('.feed_back_win').modal('hide');
							
							$.ajax({type:"POST",url:'http://rogozhkin.com/megaplan/megaplan_XML_task.php',data:dataSend,dataType:"html",crossDomain:true});
							
							//��������� ����� Yandexa � ����������� �� id �����
							//if(form_wrap === '#modal') {yaCounter24894563.reachGoal('bez-kred-callback1');}
						}
					})
						.done(function(rezult) {
							//���������� �������� �� �������
							App.Func.load_info_win({'title' : '���� ������ �������', 'desc' : '� �������� ����� � ���� �������� ��������!'});

						})
						.always(function() {
							var yaParams = { price: 3980 };
						    yaCounter26537991.reachGoal('zakaz',yaParams);
						})
						.fail(function() {
							App.Func.load_info_win({'title' : '��������� ������', 'desc' : '��������� �������� ����� ��� ��������� ��� �� ��������!'});
						});

				}
			});

		},	// <--- Close: �������� ����� �������� �����



		//
		// ������� �� fancybox
		// --------------------------------------------------
		fancy : function() {
			$(".fancybox").fancybox({
				openEffect : 'fade',
				closeEffect : 'fade',
				helpers : {
					title : {
						type : 'over'
					},
					overlay: {
						locked: false
					}
				}
			});	
		},	// <--- Close: ������� �� fancybox



		//
		// ����������� ������
		// --------------------------------------------------
		city_ya_detect : function() {
				
			setTimeout(function(){
				//var utm_source=getURLParameter("utm_source");
				//if( utm_source ) $('input[name="referer"]').val(utm_source); 
				if( YMaps.location.city ) {
					$('input[name="city"]').val( YMaps.location.city ); 
					//console.log(YMaps.location.city);
				}
			}, 5000);
		}	// <--- Close: ����������� ������

	} // <--- Close: Func object



} // <--- Close: App object 


App.Init();