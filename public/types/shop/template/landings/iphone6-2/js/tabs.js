function One()
{
  // Табы
  document.getElementById('OneTab').className = 'SelectedTab';
  document.getElementById('TwoTab').className = 'Tab';
  document.getElementById('ThreeTab').className = 'Tab';
  document.getElementById('FourTab').className = 'Tab';

  // Страницы
  document.getElementById('One').style.display = 'block';
  document.getElementById('OneTab').className = 'SelectedTab';
  document.getElementById('Two').style.display = 'none';
  document.getElementById('Three').style.display = 'none';
  document.getElementById('Four').style.display = 'none';

}
// 2
function Two()
{
  // Табы
  document.getElementById('OneTab').className = 'Tab';
  document.getElementById('TwoTab').className = 'SelectedTab';
  document.getElementById('ThreeTab').className = 'Tab';
  document.getElementById('FourTab').className = 'Tab';

  // Страницы
  document.getElementById('One').style.display = 'none';
  document.getElementById('Two').style.display = 'block';
  document.getElementById('Three').style.display = 'none';
  document.getElementById('Four').style.display = 'none';

}
// 3
function Three()
{
  // Табы
  document.getElementById('OneTab').className = 'Tab';
  document.getElementById('TwoTab').className = 'Tab';
  document.getElementById('ThreeTab').className = 'SelectedTab';
  document.getElementById('ThreeTab').href = 'Tab';
  document.getElementById('FourTab').className = 'Tab';

  // Страницы
  document.getElementById('One').style.display = 'none';
  document.getElementById('Two').style.display = 'none';
  document.getElementById('Three').style.display = 'block';
  document.getElementById('Four').style.display = 'none';
}
	
// 4
function Four()
{
  // Табы
  document.getElementById('OneTab').className = 'Tab';
  document.getElementById('TwoTab').className = 'Tab';
  document.getElementById('ThreeTab').className = 'Tab';
  document.getElementById('FourTab').className = 'SelectedTab';
  document.getElementById('FourTab').href = 'Tab';

  // Страницы
  document.getElementById('One').style.display = 'none';
  document.getElementById('Two').style.display = 'none';
  document.getElementById('Three').style.display = 'none';
  document.getElementById('Four').style.display = 'block';
}
	
	  
//////////////////////////////////////////	


// 1
function rose()
{
  // Табы
  document.getElementById('roseTab').className = 'SelectedTab_color';
  document.getElementById('blackTab').className = 'Tab_color';
  document.getElementById('silverTab').className = 'Tab_color';
  document.getElementById('goldTab').className = 'Tab_color';

  // Страницы
  document.getElementById('rose').style.display = 'block';
  document.getElementById('roseTab').className = 'SelectedTab_color';
  document.getElementById('black').style.display = 'none';
  document.getElementById('silver').style.display = 'none';
  document.getElementById('gold').style.display = 'none';

}
// 2
function black()
{
  // Табы
  document.getElementById('roseTab').className = 'Tab_color';
  document.getElementById('blackTab').className = 'SelectedTab_color';
  document.getElementById('silverTab').className = 'Tab_color';
  document.getElementById('goldTab').className = 'Tab_color';

  // Страницы
  document.getElementById('rose').style.display = 'none';
  document.getElementById('black').style.display = 'block';
  document.getElementById('silver').style.display = 'none';
  document.getElementById('gold').style.display = 'none';

}
// 3
function silver()
{
  // Табы
  document.getElementById('roseTab').className = 'Tab_color';
  document.getElementById('blackTab').className = 'Tab_color';
  document.getElementById('silverTab').className = 'SelectedTab_color';
  document.getElementById('silverTab').href = 'Tab_color';
  document.getElementById('goldTab').className = 'Tab_color';

  // Страницы
  document.getElementById('rose').style.display = 'none';
  document.getElementById('black').style.display = 'none';
  document.getElementById('silver').style.display = 'block';
  document.getElementById('gold').style.display = 'none';
}
	
// 4
function gold()
{
  // Табы
  document.getElementById('roseTab').className = 'Tab_color';
  document.getElementById('blackTab').className = 'Tab_color';
  document.getElementById('silverTab').className = 'Tab_color';
  document.getElementById('goldTab').className = 'SelectedTab_color';
  document.getElementById('goldTab').href = 'Tab_color';

  // Страницы
  document.getElementById('rose').style.display = 'none';
  document.getElementById('black').style.display = 'none';
  document.getElementById('silver').style.display = 'none';
  document.getElementById('gold').style.display = 'block';
}
		  
	  
//////////////////////////////////////////	  
	  

// 1
function rose_s()
{
  // Табы
  document.getElementById('roseTab_s').className = 'SelectedTab_color';
  document.getElementById('blackTab_s').className = 'Tab_color';
  document.getElementById('silverTab_s').className = 'Tab_color';
  document.getElementById('goldTab_s').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_s').style.display = 'block';
  document.getElementById('roseTab_s').className = 'SelectedTab_color';
  document.getElementById('black_s').style.display = 'none';
  document.getElementById('silver_s').style.display = 'none';
  document.getElementById('gold_s').style.display = 'none';

}
// 2
function black_s()
{
  // Табы
  document.getElementById('roseTab_s').className = 'Tab_color';
  document.getElementById('blackTab_s').className = 'SelectedTab_color';
  document.getElementById('silverTab_s').className = 'Tab_color';
  document.getElementById('goldTab_s').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_s').style.display = 'none';
  document.getElementById('black_s').style.display = 'block';
  document.getElementById('silver_s').style.display = 'none';
  document.getElementById('gold_s').style.display = 'none';

}
// 3
function silver_s()
{
  // Табы
  document.getElementById('roseTab_s').className = 'Tab_color';
  document.getElementById('blackTab_s').className = 'Tab_color';
  document.getElementById('silverTab_s').className = 'SelectedTab_color';
  document.getElementById('silverTab_s').href = 'Tab_color';
  document.getElementById('goldTab_s').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_s').style.display = 'none';
  document.getElementById('black_s').style.display = 'none';
  document.getElementById('silver_s').style.display = 'block';
  document.getElementById('gold_s').style.display = 'none';
}
	
// 4
function gold_s()
{
  // Табы
  document.getElementById('roseTab_s').className = 'Tab_color';
  document.getElementById('blackTab_s').className = 'Tab_color';
  document.getElementById('silverTab_s').className = 'Tab_color';
  document.getElementById('goldTab_s').className = 'SelectedTab_color';
  document.getElementById('goldTab_s').href = 'Tab_color';

  // Страницы
  document.getElementById('rose_s').style.display = 'none';
  document.getElementById('black_s').style.display = 'none';
  document.getElementById('silver_s').style.display = 'none';
  document.getElementById('gold_s').style.display = 'block';
}




	  
//////////////////////////////////////////	


// 1
function rose_b()
{
  // Табы
  document.getElementById('roseTab_b').className = 'SelectedTab_color';
  document.getElementById('blackTab_b').className = 'Tab_color';
  document.getElementById('silverTab_b').className = 'Tab_color';
  document.getElementById('goldTab_b').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_b').style.display = 'block';
  document.getElementById('roseTab_b').className = 'SelectedTab_color';
  document.getElementById('black_b').style.display = 'none';
  document.getElementById('silver_b').style.display = 'none';
  document.getElementById('gold_b').style.display = 'none';

}
// 2
function black_b()
{
  // Табы
  document.getElementById('roseTab_b').className = 'Tab_color';
  document.getElementById('blackTab_b').className = 'SelectedTab_color';
  document.getElementById('silverTab_b').className = 'Tab_color';
  document.getElementById('goldTab_b').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_b').style.display = 'none';
  document.getElementById('black_b').style.display = 'block';
  document.getElementById('silver_b').style.display = 'none';
  document.getElementById('gold_b').style.display = 'none';

}
// 3
function silver_b()
{
  // Табы
  document.getElementById('roseTab_b').className = 'Tab_color';
  document.getElementById('blackTab_b').className = 'Tab_color';
  document.getElementById('silverTab_b').className = 'SelectedTab_color';
  document.getElementById('silverTab_b').href = 'Tab_color';
  document.getElementById('goldTab_b').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_b').style.display = 'none';
  document.getElementById('black_b').style.display = 'none';
  document.getElementById('silver_b').style.display = 'block';
  document.getElementById('gold_b').style.display = 'none';
}
	
// 4
function gold_b()
{
  // Табы
  document.getElementById('roseTab_b').className = 'Tab_color';
  document.getElementById('blackTab_b').className = 'Tab_color';
  document.getElementById('silverTab_b').className = 'Tab_color';
  document.getElementById('goldTab_b').className = 'SelectedTab_color';
  document.getElementById('goldTab_b').href = 'Tab_color';

  // Страницы
  document.getElementById('rose_b').style.display = 'none';
  document.getElementById('black_b').style.display = 'none';
  document.getElementById('silver_b').style.display = 'none';
  document.getElementById('gold_b').style.display = 'block';
}
		  
	  
//////////////////////////////////////////	  
	  

// 1
function rose_s_b()
{
  // Табы
  document.getElementById('roseTab_s_b').className = 'SelectedTab_color';
  document.getElementById('blackTab_s_b').className = 'Tab_color';
  document.getElementById('silverTab_s_b').className = 'Tab_color';
  document.getElementById('goldTab_s_b').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_s_b').style.display = 'block';
  document.getElementById('roseTab_s_b').className = 'SelectedTab_color';
  document.getElementById('black_s_b').style.display = 'none';
  document.getElementById('silver_s_b').style.display = 'none';
  document.getElementById('gold_s_b').style.display = 'none';

}
// 2
function black_s_b()
{
  // Табы
  document.getElementById('roseTab_s_b').className = 'Tab_color';
  document.getElementById('blackTab_s_b').className = 'SelectedTab_color';
  document.getElementById('silverTab_s_b').className = 'Tab_color';
  document.getElementById('goldTab_s_b').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_s_b').style.display = 'none';
  document.getElementById('black_s_b').style.display = 'block';
  document.getElementById('silver_s_b').style.display = 'none';
  document.getElementById('gold_s_b').style.display = 'none';

}
// 3
function silver_s_b()
{
  // Табы
  document.getElementById('roseTab_s_b').className = 'Tab_color';
  document.getElementById('blackTab_s_b').className = 'Tab_color';
  document.getElementById('silverTab_s_b').className = 'SelectedTab_color';
  document.getElementById('silverTab_s_b').href = 'Tab_color';
  document.getElementById('goldTab_s_b').className = 'Tab_color';

  // Страницы
  document.getElementById('rose_s_b').style.display = 'none';
  document.getElementById('black_s_b').style.display = 'none';
  document.getElementById('silver_s_b').style.display = 'block';
  document.getElementById('gold_s_b').style.display = 'none';
}
	
// 4
function gold_s_b()
{
  // Табы
  document.getElementById('roseTab_s_b').className = 'Tab_color';
  document.getElementById('blackTab_s_b').className = 'Tab_color';
  document.getElementById('silverTab_s_b').className = 'Tab_color';
  document.getElementById('goldTab_s_b').className = 'SelectedTab_color';
  document.getElementById('goldTab_s_b').href = 'Tab_color';

  // Страницы
  document.getElementById('rose_s_b').style.display = 'none';
  document.getElementById('black_s_b').style.display = 'none';
  document.getElementById('silver_s_b').style.display = 'none';
  document.getElementById('gold_s_b').style.display = 'block';
}
