(function() {
    var _id = "2d9629c325db20e5ce0cbd52ca1b28cc";
    while (document.getElementById("timer" + _id)) _id = _id + "0";
    document.write("<div id='timer" + _id + "' style='min-width:333px;height:66px;'></div>");
    var _t = document.createElement("script");
    _t.src = "js/timer.min.js";
    var _f = function(_k) {
        var l = new MegaTimer(_id, {
            "view": [1, 1, 1, 1],
            "type": {
                "currentType": "3",
                "params": {
                    "weekdays": [1, 1, 1, 1, 1, 1, 1],
                    "usertime": true,
                    "time": "00:00",
                    "tz": -180,
                    "hours": "24",
                    "minutes": "0"
                }
            },
            "design": {
                "type": "circle",
                "params": {
                    "width": "2",
                    "radius": "30",
                    "line": "gradient",
                    "line-color": ["#bf3a48", "#ff5b6d"],
                    "background": "opacity",
                    "direction": "direct",
                    "number-font-family": {
                        "family": "Open Sans",
                        "link": "<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>"
                    },
                    "number-font-size": "26",
                    "number-font-color": "black",
                    "separator-margin": "8",
                    "separator-on": true,
                    "separator-text": ":",
                    "text-on": true,
                    "text-font-family": {
                        "family": "Arial"
                    },
                    "text-font-size": "10",
                    "text-font-color": "#bababa"
                }
            },
            "designId": 5,
            "theme": "white",
            "width": 333,
            "height": 66
        });
        if (_k != null) l.run();
    };
    _t.onload = _f;
    _t.onreadystatechange = function() {
        if (_t.readyState == "loaded") _f(1);
    };
    var _h = document.head || document.getElementsByTagName("head")[0];
    _h.appendChild(_t);
}).call(this);