$(window).resize(function () {
    var footer_height = $('footer').height();
    $('footer').css('margin-top',-footer_height);
    $('.wrap').css('padding-bottom',footer_height);
});
$(document).ready(function(){
    var footer_height = $('footer').height();
    $('footer').css('margin-top',-footer_height);
    $('.wrap').css('padding-bottom',footer_height);
    $(".fancybox").fancybox({
        padding : 0,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $(".fancybox-gal").fancybox({
        padding: 0,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    /*---------------------- Slider Gallery ----------------------------*/
    $(".eagle-gallery").eagleGallery({
        changeMediumStyle: 'scaleOut',
        items: 4,
        miniSlider: {
            itemsCustom: [
                [0, 2],
                [320, 3],
                [480, 4],
                [630, 4],
                [700, 4]
            ],
            margin: 20
        }        
    }); 
});
(function($) {
    $("#tab-label-category-1").owlCarousel({
        items: 5,
        nav: true,
        navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        dots: false,
        autoplay: false,
        responsive:{
            0 : {items: 1},
            560 : {items: 2},
            768 : {items: 3},
            980 : {items: 4},
            1200 : {items: 5}
        }
    });
    $("#tab-label-category-2").owlCarousel({
        items: 1,
        nav: false,
        navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        dots: true,
        autoplay: false,
        responsive:{
            0 : {items: 1},
            560 : {items: 1},
            768 : {items: 1},
            980 : {items: 1},
            1200 : {items: 1}
        }
    });
})(jQuery);