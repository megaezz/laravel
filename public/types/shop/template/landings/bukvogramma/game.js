jQuery(function () {

    "use strict";

    jQuery('[data-toggle="popover"]').each(function () {
        var _this    = jQuery(this),
            template = jQuery(_this.data('target')).eq(0);

        if (template.length == 0) {
            console.warn('Could not find template by selector %s', _this.data('target'));
            return;
        }

        _this.popover({
            template:  template.html(),
            placement: 'bottom',
            container: 'body',
            content:   '1',
            title:     '1'
        });
    });

    jQuery(document).on('click', 'a[href^="#"]', function (e) {
        e.preventDefault();

        var anchor = jQuery(this),
            offset = jQuery(anchor.attr('href')).offset() || {top: 0};

        jQuery('.nav.navbar-nav').find('LI').removeClass('active');
        anchor.closest('LI').addClass('active');
        jQuery('html, body').stop().animate({
            scrollTop: offset.top
        }, 1000);
    });

    jQuery("#certificates").owlCarousel({
        autoPlay:       false,
        items:          4,
        navigation:     true,
        navigationText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        pagination:     false,
        itemsMobile:    [530, 1]
    });

    jQuery(".fancybox").fancybox({
        openEffect:  'none',
        closeEffect: 'none'
    });

    jQuery('.fancybox-features').fancybox({
        openEffect:  'none',
        closeEffect: 'none',
        wrapCSS:     'feature-box-slider'
    });

    var review_box = jQuery("#reviews_box");

    review_box.owlCarousel({
        items:           1,
        //autoPlay:        12000,
        autoPlay:        false,
        stopOnHover:     true,
        navigation:      false,
        paginationSpeed: 1000,
        goToFirstSpeed:  2000,
        singleItem:      true,
        autoHeight:      true,
        transitionStyle: "fade"
    });

 
});
