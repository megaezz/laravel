function FsitesMan()
{
  this.siteExpandSpeed = 200;
  this.siteExpandMargins = 20;
  this.tarifMaxSites=0;
  this.tarifCurSites=0;
  this.sitePrice=0;
  this.draggingSiteData={};
  this.hasTariffDaily = false;
  this.in_preview = false;
}

FsitesMan.prototype.ready = function()
{
  this.$sitesList = $('#sites_list1');
  this.initSiteHeaderClick();
//  this.initSiteTabs();
//  this.initSiteLinks()
    if ($('table.file_config').length){
        this.initFilesTab();
    }
  consoleDbg('init: sitesMan');
};

FsitesMan.prototype.initFilesTab = function(){
    $('.file_ops .edit_file').click(sitesMan.startEditFileName);
    $('.file_name_h_span').dblclick(sitesMan.startEditFileName);
    $('.file_name_rollback').click(function(){
        $(this).closest('.file_name').removeClass('edit-mode');
    });
    $('.file_name_save').click(sitesMan.finishEditFileName);
    $('.file_name_inp').keydown(function(e){
        if (13== e.keyCode){
            sitesMan.finishEditFileName.call(this);
        }
    });
};
FsitesMan.prototype.startEditFileName=function(){
    var fn=$(this).hasClass('edit_file') ? $(this).closest('tr').find('.file_name') : $(this).closest('.file_name');
    fn.addClass('edit-mode');
    fn.find('.file_name_inp').focus();
};
FsitesMan.prototype.finishEditFileName=function(){
    var sn=$(this).closest('.file_name'), inp=sn.find('.file_name_edit .file_name_inp'),
        state=$(this).closest('tr.file_row').find('.file_upload_state div');
    ajaxLoad({
        url:inp.attr('site_id')+'/sites/rename_site_file/id:'+inp.attr('file_id'),
        data:{name:inp.val()},
        success:function(data){
            if ('undefined'===typeof data.res){return;}
            if (0==data.res){
                if ('undefined'===typeof data.mes){
                    alert('Ошибка! '+mes);
                }
            }
        }
    });
    sn.find('.file_name_h_span').text(inp.val());
    sn.removeClass('edit-mode');
    state.removeClass('ok not-pub').addClass('sync');
    sn=inp=null;
};

/**
 * Сворачивает и разворачивает блоки сайтов
 */
FsitesMan.prototype.initSiteHeaderClick = function()
{
  this.$sitesList.children('.one_site').find('.site_expander a').click(function(){
    var p     = $(this).closest('.one_site'),
        p_id  = p.attr('id').replace('site-',''),
        state = $.cookie('sites_expanded');

    if (!isset(state)) state = '';

    if (p.hasClass('collapsed'))
    {//раскрываем
      if (-1 == state.indexOf(p_id)) state+=','+p_id;
      p.removeClass('collapsed');
    }
    else
    {//скрываем
      if (-1 !== state.indexOf(p_id)) state = state.replace(p_id,'');
      p.addClass('collapsed');
    }

    state = state.replace(/[,]+/g,',');//убираем повторение запятых
    state = state.replace(/^[,]+/,'');//убираем запятую, если она первая

    setStateCookie('sites_expanded',state);
    state=p=p_id=null;
  });
};

FsitesMan.prototype.initSiteTabs = function()
{
  this.$sitesList.children('.one_site').find('.one_site_tabs .td_one_site_tab').click(function(){
    $(this).parent().children('.td_one_site_tab.sel').removeClass('sel');//снимаем выделение со всех ярлыков вкладок
    $(this).addClass('sel');//выделяем текущий ярлык

    $(this).closest('.one_site_cont').find('.one_site_tabcont > .one_site_tab')
      .hide() //скрываем все блоки с содержимым вкладок
      .filter('#'+$(this).attr('tab')).show(); //показываем содержимое выделенной вкладки
  });

  this.$sitesList.find('.one_site').find('.one_site_tabs .td_one_site_tab:first').click();
};

FsitesMan.prototype.initSiteLinks = function()
{
  this.$sitesList.find('.site_links a').click(function(){
    if ('undefined'!==typeof $(this).data('clicked')) return false;

    $(this).animate({paddingLeft:26},100,function(){ $(this).addClass('loading'); });
    $(this).data('clicked',true);
    return true;
  });
};

FsitesMan.prototype.collapseAllSites = function()
{
  $('.one_site').addClass('collapsed');
  setStateCookie('sites_expanded','');
};

FsitesMan.prototype.initStatChart = function(chart_id, stat_categories, stat_data)
{
  $('#'+chart_id).highcharts({
    chart: {
      type: 'column',
      showAxes:true,
      style: {
        fontSize:'16'
      },
      spacingTop: 0,
      spacingRight: 0,
      spacingBottom: 10,
      spacingLeft: 0,
      height:'250',
      width: '718'
    },
    title: {
      text: 'Посещаемость сайта'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: stat_categories, //['раз','два']
      gridLineWidth: 1,
      gridLineColor: '#EDEDED',
      tickmarkPlacement: 'on',
      labels: {
        step:2
      }
    },
    yAxis: {
      title: {
        text: 'Посетители',
        style: {
          fontFamily: 'Verdana',
          color: '#CCC',
          fontWeight: 'normal'
        }
      },
      labels: {
        step:1
      },
      gridLineColor: '#EDEDED'
    },
    series: [{
      name: 'Посетителей',
      data: stat_data //[{y:1},{y:2}]
    }],
    tooltip:{
      headerFormat:'<span style="font-size: 14px">{point.key}</span><br/>',
      pointFormat: '<span style="font-size: 14px">{series.name} <b>{point.y}</b></span>',
      useHTML:true
    },
    plotOptions:{
      column:{
        animation:false
      }
    },
    legend:{ enabled:false },
    colors: ['#C3B0FA']
  });
};


FsitesMan.prototype.initDatePiker = function(obj)
{
  if ('string'===typeof obj) obj = $(obj);
  obj.datepicker({
    showAnim:'show',
    prevText:'Предыдущий месяц',
    nextText:'Следующий месяц',
    monthNames:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    dayNamesMin:['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
    dateFormat:'dd.mm.yy',
    firstDay:1,
    showButtonPanel:true,
    currentText:'Сегодня',
    closeText:'Закрыть',
    constrainInput:true,
    beforeShow: function(input, inst){
      $(input).attr('disabled','disabled').blur();
    },
    onClose: function(dateText, inst){
      $(this).removeAttr('disabled');
    }
  });
};

FsitesMan.prototype.sendDeclarationForm = function(obj)
{
  var el=$(obj);
  var frm=el.closest('form');
  var frm_id = frm.attr('id');
  var data={};
  var ajaxData={};
  var k= 0, m=0;

//  console.log(frm.serialize());

  data['sum']     = frm.find('input[name="'+frm_id+'[sum]"]');
  data['d_start'] = frm.find('input[name="'+frm_id+'[date_start]"]');
  data['d_end']   = frm.find('input[name="'+frm_id+'[date_end]"]');
  data['check']   = frm.find('input[name="'+frm_id+'[avg_check]"]');
  data['conv']    = frm.find('input[name="'+frm_id+'[conversion]"]');

//  console.log(data);
//  var matches={'d_start':null, 'd_end':null};

  //var ar=['sum','check','conv'];
  for(k in data)
  {
    if ('d_start'==k || 'd_end'==k)
    {
      m = data[k].val().match(/^(\d{2})\.(\d{2})\.(\d{4})$/);
      if (!m)
      {
        alert('Необходимо ввести правильную дату');
        data[k].focus();
        return;
      }
      else
      {
        ajaxData[k]=parseInt(m[3]+m[2]+m[1]);
      }
    }
    else
    {
      ajaxData[k]=data[k].val();
      if (!data[k].val().match(/^\d+$/))
      {
        alert('Необходимо ввести правильное число');
        data[k].focus();
        return;
      }
    }
  }

//  for (k in matches)
//  {
//    matches[k]['val'] = parseInt(matches[k][3]+matches[k][2]+matches[k][1]);
//  }

  if (ajaxData['d_start'] > ajaxData['d_end'])
  {
    alert('Дата начала должна быть раньше даты окончания');
    return;
  }


//console.log(frm.serialize());
  $.ajax({
    url: window.baseUrl+'sites/ajax/frm:frmdeclaration/id:'+frm_id,
    type:'POST',
    cache:false,
    dataType: 'json',
    data: frm.serialize(),
    timeout: 40000
  });
};

FsitesMan.prototype.changeDeclaration = function(obj)
{
  var dv = $(obj).closest('.declaration_view');
  dv.hide();
  dv.prev('.declaration_edit_form').show();
};

FsitesMan.prototype.cancelDeclarationEditting = function(obj)
{
  var def = $(obj).closest('.declaration_edit_form');
  def.hide();
  def.next('.declaration_view').show();
};

FsitesMan.prototype.createSiteChooseTpl = function(obj)
{
  var el=$(obj);
  el.closest('#tpls_list').children('.active').removeClass('active');
  el.closest('.one_tpl').addClass('active');
  el=null;
};

FsitesMan.prototype.createInit = function()
{
  $('#frmonesite__empty_tpl input[type=radio]').click(function(){
    var tl=$('#tpls_list'), scb=$('#sites_create_blk'), tbb=$('#td_before_btn'), ot=tl.find('.one_tpl');

    if (!scb.is(':animated'))
    {
      if (1==parseInt($(this).val()))
      {
        if (803 > scb.width() && !tl.is(':visible'))
        {
          ot.hide();
          tl.show();
          scb.animate({width:803},600,function(){ $('#tpls_list .one_tpl').show(); });
          tbb.animate({width:0},600);
        }
      }
      else
      {
        if (tl.is(':visible'))
        {
          ot.hide();
          tl.hide();
          scb.animate({width:700},600);
          tbb.animate({width:60},600);
        }
      }
    }
    tl=scb=tbb=ot=null;
  });

  helpSlider('#tplslide_helper');
};
FsitesMan.prototype.createSiteCheckboxEmptyClick = function()
{
//  var el=$(obj);
  var tl=$('#frmonesite__empty_tpl input[type=radio]:checked'), scb=$('#sites_create_blk'), tbb=$('#td_before_btn');
  if (1==tl.val())
  {
    tl.show();
    scb.animate({width:803},600);
    tbb.animate({width:0},600);
  }
  else
  {
    tl.hide();
    scb.animate({width:700},600);
    tbb.animate({width:60},600);
  }
  tl=scb=tbb=null;
};


FsitesMan.prototype.initNewList = function()
{
  $('#sitesTutorialVideo')
    .wind('setShowFunc',function(obj){
      var wb=obj.$el.find('.wind-body'), pat=obj.$el.find('.pat');
      wb.html('<iframe '+pat.html()+'></iframe>');
      wb=pat=null;
      return true;
    })
    .wind('setAfterCloseFunc',function(obj){
      obj.$el.find('.wind-body').html('');
      setcookie('sites_tutorial_closed', 1, new Date().getTime()+2592000, 'index.html');
      return true;
    });

  //this.getMonthlySitesCount();
  $('.tbody_sites').sortable({
    connectWith:'.tbody_sites',
    handle: '.site_lbl',
    placeholder: 'sort_ph',
    opacity: 1,
    cursor: 'move',
    tolerance:'pointer',
    dropOnEmpty:true,
    start:function(event, ui){
      $(this).sortable( 'refreshPositions' );
      ui.placeholder.addClass('tr_site fnt2').html('<td colspan="5"><div>ПОМЕСТИТЬ САЙТ ЗДЕСЬ</div></td>');
      ui.helper.css({width:'auto', height:'auto'});//alert(ui.item.attr('id-site'));return;
      sitesMan.draggingSiteData[ui.item.attr('id-site')]={parent: ui.item.parent(), prev: ui.item.prev(), item: ui.item};
    },
    stop:function(event, ui){
      if (null!==sitesMan.sitesAjax)
      {//если аякс тормозит, а чел перетаскивает снова
        jAlert('Подождите, когда завершится предыдущая команда перемещения.');
        sitesMan.moveSiteBack(ui.item.attr('id-site'));
        return;
      }

      var closestBlockId=ui.item.closest('.iblk').attr('id');
      if ('iblk-monthly'==closestBlockId && sitesMan.tarifMaxSites>0 && sitesMan.getMonthlySitesCount() > sitesMan.tarifMaxSites)
      {//если бросаем сайт в блок тарифа, а туда больше нельзя
        sitesMan.moveSiteBack(ui.item.attr('id-site'));
//        jAlert('<p>В текущий тариф можно поместить максимум <strong>'+sitesMan.tarifMaxSites+' сайт'+plural(sitesMan.tarifMaxSites,'','а','ов')+'</strong>.</p>' +
//          '<p>Можно убрать один из сайтов, чтобы поместить новый.</p>' +
//          '<p>Либо, если вам нужно больше сайтов, в любой момент можно <a href="'+window.baseUrl+'profile/balance">перейти</a> на более "мощный" тариф.</p>');
        sitesMan.alertTarifIsFull(sitesMan.tarifMaxSites);
      }
      else if ('iblk-daily'==closestBlockId)
      {//положили сайт в дневной блок
        var lp=parseInt(ui.item.attr('last-pay'));
        if (isNaN(lp) || lp < sitesMan.curCronDate)
        {
//          jConfirm('Этот сайт еще не публиковался сегодня.<br/>За него сразу будет списана оплата за 1 день.', {
//            width:355,
//            onOk:function(){sitesMan.recountSitesPos();},
//            onCancel:function(){sitesMan.moveSiteBack(ui.item.attr('id-site'));}
//          })
          sitesMan.alertDailySiteNeedPayment(function(){sitesMan.updateSiteSwitch(ui.item, closestBlockId);sitesMan.recountSitesPos();}, function(){sitesMan.moveSiteBack(ui.item.attr('id-site'));});
        }
        else
        {
          sitesMan.updateSiteSwitch(ui.item, closestBlockId);
          sitesMan.recountSitesPos();
        }
      }
      else
      {//остальные случаи
        //ui.item.find('.site_publish .switch')['iblk-disabled'==closestBlockId ? 'removeClass':'addClass']('on')['iblk-disabled'==closestBlockId ? 'addClass':'removeClass']('off');
        sitesMan.updateSiteSwitch(ui.item, closestBlockId);
        sitesMan.recountSitesPos();
      }
      closestBlockId=null;
    },
    over:function(event, ui){
      sitesMan.showHideEmptyBlocks();
      sitesMan.recountDailyExpense();
    }
  });

  $(document).on('click', '.tr_site .site_name .site_name_h div.edit_name', function(e){
    e.preventDefault();
    e.stopPropagation();
    var sn=$(this).closest('.site_name'), inp=sn.find('.site_name_edit .site_name_inp');
    inp.val($(this).closest('.site_name_h').children('span').text());
    sn.addClass('edit-mode');
    inp.focus();
    sn=inp=null;
  });

  $(document).on('click', '.tr_site .site_name .site_name_save', function(e){
    var sn=$(this).closest('.site_name'), inp=sn.find('.site_name_edit .site_name_inp');
    ajaxLoad({
      url:inp.attr('site_id')+'/sites/ajax/frm:frmsitename',
      data:{frmsitename:{name:inp.val()}, from_list:1}
    });
    sn.find('.site_name_h span').text(inp.val());
    sn.removeClass('edit-mode');
    sn=inp=null;
  });

  $(document).on('click', '.tr_site .site_name .site_name_rollback', function(e){
    $(this).closest('.site_name').removeClass('edit-mode');
  });

  this.curCronDate=null;
  this.dailyExpenseBlock=$('#daily_expense');
  this.dailySitesList=$('#iblk-daily .tbody_sites:first');
  this.monthlySitesList=$('#iblk-monthly .tbody_sites:first');
  this.sitesAjax=null;
};

FsitesMan.prototype.initEditSiteName = function () {
    $(document).on('click', '.bs_block_title h1.fnt .icon-pencil2', function (e) {
        var sn = $(this).parent('.fnt:first');
        var inp = sn.find('.site_name_edit .site_name_inp');
        sn.css('height', sn.outerHeight());
        inp.val(sn.children('span').text());
        sn.addClass('edit-mode');
        inp.focus();
        sn = inp = null;
    });

    $(document).on('click', '.bs_block_title h1.fnt .site_name_save', function (e) {
        var sn = $(this).parents('.fnt:first');
        var inp = sn.find('.site_name_edit .site_name_inp');
        ajaxLoad({
            url: siteId + '/sites/ajax/frm:frmsitename',
            data: {frmsitename: {name: inp.val()}, from_list: 1}
        });
        sn.css('height', 'auto').removeClass('edit-mode');
        sn.children('span:first').text(inp.val());
        sn = inp = null;
    });

    $(document).on('click', '.bs_block_title h1.fnt .site_name_rollback', function (e) {
        $(this).parents('.fnt:first').css('height', 'auto').removeClass('edit-mode');
    });
};

FsitesMan.prototype.alertDailySiteNeedPayment = function(okFunc,cancelFunc){
  jConfirm('Этот сайт еще не публиковался сегодня.<br/>За него сразу будет списана оплата за 1 день.', {
    width:355,
    okText:'Продолжить',
    onOk: okFunc||function(){},
    onCancel: cancelFunc||function(){}
  })
};
FsitesMan.prototype.alertTarifIsFull = function(maxSites){
  jAlert('<p>На текущем тарифе вы можете запустить максимум <strong>'+maxSites+' сайт'+plural(maxSites,'','а','ов')+'</strong>.</p>' +
    '<p class="fw_bold pt1" style="color: #F58F36;">Что теперь делать?</p>' +
    '<table class="mb2"><tr><td style="width:42%">Выключите один из сайтов, нажав на переключатель <span id="switch_example"></span></td>' +
    '<td class="vam c_text fw_bold" style="width:16%;">ИЛИ</td>' +
    '<td style="width:42%">Перейдите на более "мощный" тариф в разделе Баланс.</td>' +
    '</tr></table>',{
    width:550,
    btnWidth:160,
    okClass:'ibtn-green',
    okText:'Перейти в Баланс',
    headerText:'Количество запущенных сайтов превышено',//Количество запущенных сайтов в тарифе превышено
    onOk:function(){
      mxt('btn balance', {'источник': mxGetSourceLink() });
      setTimeout("goTo('profile/balance');",200);
      return false;
    }
  });
};

FsitesMan.prototype.moveSiteBack = function(id_site, movePosBack)
{
  if (this.draggingSiteData[id_site])
  {
    if (this.draggingSiteData[id_site].prev.length) this.draggingSiteData[id_site].item.insertAfter(this.draggingSiteData[id_site].prev);
    else this.draggingSiteData[id_site].item.prependTo(this.draggingSiteData[id_site].parent);
    this.draggingSiteData[id_site].item.attr('cur-grp', this.draggingSiteData[id_site].item.closest('.iblk').attr('id'));
  }
  this.showHideEmptyBlocks();
  this.recountDailyExpense();

  if ('undefined'!==typeof movePosBack)
  {
    for (var k in movePosBack)
    {
      $('.tr_site[id-site='+k+']').attr('pos',movePosBack[k]);
    }
    k=null;
  }
  //this.recountSitesPos();
};

FsitesMan.prototype.getMonthlySitesCount = function()
{
  return this.monthlySitesList.children('tr.tr_site:not(.ui-sortable-helper):not(.sort_ph)').length;
};

FsitesMan.prototype.showHideEmptyBlocks = function()
{
  $('.tbody_sites .tr_empty').each(function(){
    var el=$(this);
    if (el.parent().children('tr.tr_site:not(.ui-sortable-helper)').length > 0) el.hide();
    else el.show();
    el=null;
  });
};

FsitesMan.prototype.recountDailyExpense = function()
{
  if (this.dailyExpenseBlock.length)
  {
    this.dailyExpenseBlock.text(this.dailySitesList.find('.tr_site:not(.ui-sortable-helper)').length * this.sitePrice);
  }
};

FsitesMan.prototype.recountSitesPos = function()
{
  var curPos=null, savePos={}, cnt=0, siteId=null, newPar=null, oldPar=null, oldPos=null;
  $('#sites_list').children('.iblk').each(function(){
    newPar = $(this).attr('id');
    $(this).find('.tbody_sites .tr_site').each(function(idx){
      ++idx;
      curPos=parseInt($(this).attr('pos'));
      oldPar=$(this).attr('cur-grp');
      if (isNaN(curPos) || curPos != idx || newPar!=oldPar)
      {
//        oldPos=parseInt($(this).attr('pos'));
//        if (isNaN(oldPos)) oldPos='';

        siteId = parseInt($(this).attr('id-site'));
        if (!isNaN(siteId))
        {
          cnt++;
          savePos[siteId]={pos:idx, oldPos:isNaN(curPos)?'':curPos, newParent: newPar};
          $(this).attr({pos: idx, 'cur-grp': newPar});
        }
      }
    });
  });

  var m=window.location.href.match(/user:(\d+)/);
  if (cnt>0){this.sitesAjax=ajaxLoad({url: 'sites/ajax/frm:frmsitespos'+(null!==m ? '/user:'+m[1] : ''), data:{list:savePos} });}
  curPos=savePos=cnt=siteId=newPar=oldPar=oldPos=null;
};

FsitesMan.prototype.showNotEnoughMoneyForDailySite = function(curCronDate)
{
//  jAlert('<p>На вашем балансе недостаточно средств для публикации сайта за '+curCronDate.substr(4,2)+'.'+curCronDate.substr(2,2)+'.20'+curCronDate.substr(0,2)+'.</p>' +
//    '<p><a href="'+window.baseUrl+'profile/balance">Пополните баланс</a> и повторите операцию.</p>');

  jConfirm('<p>На вашем балансе недостаточно средств.</p>' +
    '<p>Для запуска сайта за <strong>'+curCronDate.substr(4,2)+'.'+curCronDate.substr(2,2)+'.20'+curCronDate.substr(0,2)+'</strong> нужно, чтобы на балансе было минимум <strong>'+number_format(this.sitePrice,0)+' руб</strong>.</p>' +
    '<p>Пополните баланс и повторите операцию.</p>',{
    okText:'Пополнить',
    btnWidth: 110,
    onOk:function(){
      //кнопка перехода в баланс
      //window.location.href=window.baseUrl+'profile/balance#daily1';
      mxt('btn balance', {'источник': mxGetSourceLink() });
      setTimeout("goTo('profile/balance');",200);
    }
  });
};

FsitesMan.prototype.errTplPublish = function()
{
  jAlert('<p>Данный сайт является шаблоном, его нельзя опубликовать.</p><p>Вы можете нажать на шестеренку, выбрать пункт &laquo;Клонировать&raquo; и уже клонированный сайт сможете опубликовать.</p>');
};


FsitesMan.prototype.onFaviconUpload = function(event,obj)
{console.log(event.target.files[0]);
  if (!event.target.files[0].type.match(/^image\/(png|ico|x-icon|icon|vnd.microsoft.icon)/)) { alert('Нужно выбрать картинку с расширением ICO или PNG.'); return false; }
  if (event.target.files[0].size > 120000) {alert('Максимальный размер файла - 120 Кб.'); return;}
  if ($('#upload_target').length==0){$('body').append('<iframe id="upload_target" name="upload_target" style="width:1px;height:1px;border:0;position:absolute;left:-9999px;visibility:hidden;"></iframe>');}
  var frm = $(obj).closest('form');
  frm.submit();//отправляем форму
  frm=null;
};
FsitesMan.prototype.faviconUploadOk = function(data)
{
  $('img#favicon_img').attr('src', data.url);
  $('.favicon_edit').removeClass('empty');
};


FsitesMan.prototype.goToSites = function()
{
    location.href = window.baseUrl + 'sites/list';
};

FsitesMan.prototype.onSiteDelete = function(id_site, on_success)
{
  //<img alt="grumpy_cat" src="'+window.baseUrl+'web/images/grumpy_cat1.jpg" class="mt1">
  jConfirm('Удалить этот сайт?', {
    site: id_site,
    width: 330,
    okClass:'ibtn-red',
    okText:'Да, удалить',
    btnWidth: 114,
    onOk:function(){
      ajaxLoad({
          url:'sites/ajax/frm:delete_site/id:'+id_site,
          onSuccess: on_success
      });
    }
  });
};
FsitesMan.prototype.afterSiteDelete = function(id_site)
{
  showDoneBox('Сайт удален',{width:200, cls:'c_text', showTime:1000});
  var tr=$('#sites_list .tr_site[id-site='+id_site+']');
  tr.animate({opacity:0},1000,function(){$(this).remove()});
  tr=null;
};

FsitesMan.prototype.siteTransfer = function(site_id, site_dom, site_name)
{
  $('#transver_site_name h5').text(site_name);
  $('#transver_site_name a').attr('href','http://'+site_dom).text(site_dom);
  $('#frmtransfer__site').val(site_id);
  $('#wndTransfer .ui_error').hide();
  $('.wind#wndTransfer').wind('show');
  $('#frmtransfer__mail').val('').focus();
};
FsitesMan.prototype.confirmSiteTransfer = function()
{
  jConfirm('Переносим сайт?', {
    width: 330,
    btnWidth:109,
    okClass:'ibtn-red',
    okText:'Да, переносим',
    onOk:function(){
      ajaxLoad({
        url:'sites/ajax/frm:frmtransfer/ok:1' + (location.href.indexOf('/sites/view') >= 0 ? '/goto:sites' : ''),
        data: $('#frmtransfer').serializeArray()
      });
    }
  });
};
FsitesMan.prototype.siteTransferDone = function (siteId, siteName, userMail, callback) {
    $('#sites_list .tr_site[id-site=' + siteId + ']').remove();
    alert('Сайт "' + siteName + '" успешно перенесен пользователю ' + userMail + '!');
    $('.wind#wndTransfer').wind('hide');

    if (typeof(callback) == 'function') {
        callback();
    }
};

//var m=window.location.href.match(/user:(\d+)/);
//if (cnt>0){this.sitesAjax=ajaxLoad({url: 'sites/ajax/frm:frmsitespos'+(null!==m ? '/user:'+m[1] : ''), data:{list:savePos} });}
FsitesMan.prototype.updateSiteSwitch = function(siteEl,closestBlockId){
  siteEl.find('.site_publish .switch')['iblk-disabled'==closestBlockId ? 'removeClass':'addClass']('on')['iblk-disabled'==closestBlockId ? 'addClass':'removeClass']('off');
  siteEl.stop().css('background-color','#FFF7C2').animate({backgroundColor:'#fff'},5000);
};

FsitesMan.prototype.setPublished = function(site_id, switchEl){
  var $switch=$(switchEl);
  var m=window.location.href.match(/user:(\d+)/);

  if ($switch.hasClass('on')){
    //включен. значит пользователь пытается выключить
    jConfirm('Вы уверены, что хотите выключить сайт?',{
      width: 330,
      btnWidth:109,
      okClass:'ibtn-red',
      okText:'Да, выключить',
      onOk:function(){
        ajaxLoad({
          url:'sites/ajax/frm:frmpublish2'+(null!==m ? '/user:'+m[1] : ''),
          data: {id:site_id, action:'off'},
          onSuccess:function(data){
            if (data.res==1){
              switch (data.mes){
                case 'ok_site_off':
                  var s=$('.tr_site[id-site='+data.id+']');
                  //s.css('background-color','#FFF7C2').animate({backgroundColor:'transparent'},10000);
                  s.appendTo($('#iblk-disabled .tbody_sites'));
                  //s.find('.site_publish .switch').removeClass('on').addClass('off');
                  console.log(s.closest('.iblk'));
                  sitesMan.updateSiteSwitch(s, s.closest('.iblk').attr('id'));
                  s.attr('pos',data.pos);
                  sitesMan.showHideEmptyBlocks();
                  showDoneBox('Сайт выключен',{width:200, cls:'c_text', showTime:1000});
                  sitesMan.recountDailyExpense();
                  s=null;
                  break;
              }
            }
          }
        });
      }
    });
  } else {
    mxt('btn run site'/*'Кнопка [Запустить сайт]'*/, {'источник': mxGetSourceLink(), 'id сайта':site_id });
    //выключен. значит пользователь пытается включить
    if (sitesMan.hasTariffDaily) {
      jConfirm('Выберите месячный или дневной тариф для запуска сайта',{
        width:400,
        btnWidth:155,
        okText:'Тариф по месяцам',
        cancelText:'По дням',
        headerText:'Запуск сайта',
        onOk:function(){ sitesMan.setPublishedOn(site_id); },
        onCancel:function(){ sitesMan.setPublishedOn(site_id,'daily'); }
      });
    } else {
      this.setPublishedOn(site_id);
    }
    //

  }
  m=$switch=null;
};

FsitesMan.prototype.setPublishedOn = function(site_id, run_type, forced){
  var m=window.location.href.match(/user:(\d+)/);
  ajaxLoad({
    url:'sites/ajax/frm:frmpublish2'+(null!==m ? '/user:'+m[1] : ''),
    data: {id:site_id, action:'on', type: run_type||'monthly', force: forced||0},
    withCredentials: true,
    onSuccess:function(data){
      if (data.res==1){
        switch (data.mes){
          case 'ok_site_on':
            var s=$('.tr_site[id-site='+data.id+']');
            if (s.length){
              //s.css('background-color','#FFF7C2').animate({backgroundColor:'transparent'},10000);
              //console.log('#iblk-'+data.type+' .tbody_sites',$('#iblk-'+data.type+' .tbody_sites'));
              s.appendTo($('#iblk-'+data.type+' .tbody_sites'));
              if ('daily'==data.type){sitesMan.recountDailyExpense();}
              //s.find('.site_publish .switch').removeClass('off').addClass('on');
              sitesMan.updateSiteSwitch(s, s.closest('.iblk').attr('id'));
              s.attr('pos',data.pos);
              sitesMan.showHideEmptyBlocks();
            } else {
              $('#btn-run-site').fadeOut(500);
            }
            showDoneBox('Сайт запущен'+('undefined'!==typeof data.is_paid && 1==data.is_paid ? ' и оплачен' : '')+'!',{width:200, cls:'c_text', showTime:1000});
            if (sitesMan.in_preview){
                window.location.href = window.location.href;
            }
            s=null;
            break;

          case 'tarif_is_full':
            sitesMan.alertTarifIsFull(data.max_sites);
            break;

          case 'need_tarif_activate':
            //$('#runSite').wind('show');
            jAlert('<p>Для запуска сайта вам нужно перейти в раздел &laquo;Баланс&raquo; и подключить тариф.</p>',{
              width:400,
              btnWidth:160,
              okClass:'ibtn-green',
              okText:'Перейти в Баланс',
              headerText:'Запуск сайта',
              onOk:function(){
                mxt('btn balance', {'источник': mxGetSourceLink() });
                setTimeout("goTo('profile/balance');",200);
              }
            });
            break;

          case 'daily_tarif_is_off':
            jConfirm('<p>Чтобы запустить сайт <strong>по дням</strong>, вам нужно активировать режим для PRO.</p><p>Для этого перейдите в раздел Баланс и в блоке с названием "ДЛЯ PRO" нажмите кнопку "Подключить".</p>',{
              width: 450,
              btnWidth:120,
              okClass:'ibtn-green',
              okText:'Перейти в Баланс',
              onOk:function(){
                //window.location.href = window.baseUrl + 'profile/balance';
                mxt('btn balance', {'источник': mxGetSourceLink() });
                setTimeout("goTo('profile/balance');",200);
              }
            });
            break;

          case 'daily_site_need_payment':
            sitesMan.alertDailySiteNeedPayment(function(){ sitesMan.setPublishedOn(site_id, run_type, 1); });
            break;

          case 'need_balance':
            sitesMan.showNotEnoughMoneyForDailySite(data.curCronDate);
            break;
        }
      }
    }
  });
  m=null;
};

FsitesMan.prototype.initCreatePage=function(){
    this.$tplsList = $('#tpls-list');
    $('#tpls-list')
        .on('mousedown','.btn-tpl-choose', function(){
            $('input.tpl-from-list').val($(this).attr('data-site'));
        })
        .on('click', 'img.tpl-img, .btn-tpl-preview', this.previewTpl)
    ;
    $(window).on('scroll.tpl-load',function(){
        var t=sitesMan.$tplsList.children('.tpl.not-loaded');
        if (t.length){
            var w=$(window), st=w.scrollTop(), wh= w.height(), img;
            for (var k in t){
                //console.log('top = ',t[k].offsetTop,' bottom=', st+wh);
                if (t[k].offsetTop - (st+wh) < 20){
                    img=$(t[k]).find('.tpl-img');
                    img.attr('src', img.attr('data-src'));
                    img.removeAttr('data-src');
                    $(t[k]).removeClass('not-loaded');
                } else {
                    break;
                }
            }
            w=st=wh=k=img=null;
        } else {
            $(window).off('scroll.tpl-load');
        }
    });
    $(window).scroll();
};
FsitesMan.prototype.previewTpl=function(){
    var el=$(this).closest('.tpl');
    var id = parseInt(el.attr('id').replace('tpl-',''));
    $('body').addClass('no_overflow');
    var p =$('#tpl-preview-wrap'), frameWrap = p.find('#tpl-preview-iframe-wrap');
    p.show();
    frameWrap.children('iframe:visible').hide();
    var frame = frameWrap.children('iframe[data-site='+id+']');
    if (frame.length){
        frame.show();
    } else {
        $('#tpl-loader').show();
        $('<iframe src="'+window.baseUrl+id+'/pages/preview_tpl" data-site="'+id+'" onload="$(\'#tpl-loader\').hide()"></iframe>').appendTo(frameWrap);
    }
    $('#frm3__name').val($('#frm2__name').val());
    $('#tpl-id-in-preview').val(id);
    if ($(this).hasClass('btn-tpl-preview')){
        $(this).addClass('disabled');
    }
};
FsitesMan.prototype.closePreviewTpl=function(){
    $('body').removeClass('no_overflow');
    $('#tpl-preview-wrap').hide();
};
FsitesMan.prototype.chooseTpl=function(){
    var el=$(this).closest('.tpl');
    var id = parseInt(el.attr('id').replace('tpl-',''));
//    console.log('choose', id);
//    var wnd=$('#wndChosenTpl');
//    wnd.find('.wind-body img.tpl').attr('src', el.find('img.tpl-img').attr('src'));
//    wnd.wind('show');
};

window.sitesMan = new FsitesMan();
FE.add('onready', 'sitesMan.ready();');
FE.add('file_uploaded_favicon', function(data){ sitesMan.faviconUploadOk(data); });