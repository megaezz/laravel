<?php

namespace MegawebV1;

function advertiser_paid_charts()
{
    $login = auth_login(['type' => 'shop']);
    // ���������� ����� �� minus ��������� ������
    $minus = value_from_config('shop', 'advertiser_paid_percent_minus');
    //
    $p['sources'] = empty($_POST['sources']) ? [] : $_POST['sources'];
    $m['sources']['where'] = empty($p['sources']) ? '1' : 'utm_source in ('.array_to_str_alt($p['sources'], 'key').')';
    $p['date_from'] = empty($_POST['date_from']) ? false : checkstr($_POST['date_from']);
    $p['date_to'] = empty($_POST['date_to']) ? false : checkstr($_POST['date_to']);
    $m['date']['where'] = ($p['date_to'] and $p['date_from']) ? '(date between \''.$p['date_from'].'\' and \''.$p['date_to'].'\')' : 1;
    temp_var('date_from', $p['date_from'] ? $p['date_from'] : '');
    temp_var('date_to', $p['date_to'] ? $p['date_to'] : '');
    //
    // ��������� ���� ����������
    $sources = query_arr('select utm_source from '.tabname('shop', 'orders').' where advertiser=\''.$login.'\' and utm_source!=\'\' group by utm_source;');

    $list = '';
    foreach ($sources as $v) {
        $checked = isset($p['sources'][$v['utm_source']]) ? 'checked' : '';
        $list .= '<div class="checkbox">
		<label><input type="checkbox" name="sources['.$v['utm_source'].']" '.$checked.'>'.$v['utm_source'].'</label>
		</div>';
    }
    temp_var('block_sources', $list);
    //
    $arr = query_assoc('select show_paid_charts from '.typetab('users').' where login=\''.$login.'\';');
    // pre($arr);
    if (! $arr['show_paid_charts']) {
        return;
    }
    $by_days = query_arr('
		select date(date) as day,count(orders.id) as q_all,
		sum(if(orders.user_status not in (\'new\',\'unavailable\',\'canceled\'),1,0)) as q_confirmed,
		sum(if(orders.user_status in (\'take payment\',\'paid\'),1,0)) as q_paid
		from shop.orders
		where advertiser=\''.$login.'\' and '.$m['date']['where'].' and '.$m['sources']['where'].'
		group by day
		');
    $by_months = query_arr('
		select date_format(date, \'%Y-%m\') as month,count(orders.id) as q_all,
		sum(if(orders.user_status not in (\'new\',\'unavailable\',\'canceled\'),1,0)) as q_confirmed,
		sum(if(orders.user_status in (\'take payment\',\'paid\'),1,0)) as q_paid
		from shop.orders
		where advertiser=\''.$login.'\' and '.$m['date']['where'].' and '.$m['sources']['where'].'
		group by month
		');
    $list = '';
    foreach ($by_days as $v) {
        if (! empty($list)) {
            $list .= ',';
        }
        $paid_percent = round($v['q_paid'] / (empty($v['q_confirmed']) ? 1 : $v['q_confirmed']) * 100);
        $paid_percent = round($paid_percent - $minus / 100 * $paid_percent);
        $list .= '[\''.$v['day'].'\','.$paid_percent.']';
    }
    temp_var('chart_values_paid_percent_by_days', '[\'����\',\'�����\'],'.$list);
    $list = '';
    foreach ($by_months as $v) {
        if (! empty($list)) {
            $list .= ',';
        }
        $paid_percent = round($v['q_paid'] / (empty($v['q_confirmed']) ? 1 : $v['q_confirmed']) * 100);
        $paid_percent = round($paid_percent - $minus / 100 * $paid_percent);
        $list .= '[\''.$v['month'].'\','.$paid_percent.']';
    }
    temp_var('chart_values_paid_percent_by_months', '[\'�����\',\'�����\'],'.$list);
    temp_var('block_charts', template('admin/advertiser/paid_charts/charts'));
    $content = template('admin/advertiser/paid_charts/block');

    return $content;
}

function advertiser_delete_domain()
{
    $domain = empty($_GET['domain']) ? error('�� ������� �����') : checkstr($_GET['domain']);
    $login = auth_login(['type' => 'shop']);
    $arr = query_assoc('select * from '.typetab('domain_config').' where domain=\''.$domain.'\' and default_advertiser=\''.$login.'\';');
    if (! $arr) {
        error('����� �� ������, ���� ��� �� �����������');
    }
    query('delete from '.typetab('domain_config').' where domain=\''.$domain.'\';');
    query('delete from '.tabname('engine', 'domains').' where domain=\''.$domain.'\';');
    redirect('/?page=admin/advertiser/domains');
}

function advertiser_submit_add_domain()
{
    $domain = empty($_POST['domain']) ? error('�� ������� �����') : checkstr($_POST['domain']);
    $domain = str_replace(['http://', 'https://'], '', $domain);
    $parse = parse_url('http://'.$domain);
    $domain = empty($parse['host']) ? error('������ � �������� ������') : $parse['host'];
    // pre($domain);
    $login = auth_login(['type' => 'shop']);
    query('insert into '.tabname('engine', 'domains').' set domain=\''.$domain.'\', owner=\'not\', type=\'shop\', rewrite_function=\'landing_rewrite_query\';');
    query('insert into '.typetab('domain_config').' set domain=\''.$domain.'\', default_advertiser=\''.$login.'\',show_advertiser=1;');
    redirect('/?page=admin/advertiser/domains');
}

function advertiser_domains()
{
    $login = auth_login(['type' => 'shop']);
    $arr = query_arr('
		select domain,default_advertiser
		from '.typetab('domain_config').'
		where show_advertiser and (default_advertiser=\''.$login.'\' or default_advertiser is null)
		order by default_advertiser,domain
		;');
    $list['login'] = '';
    $list['default'] = '';
    foreach ($arr as $v) {
        if (empty($v['default_advertiser'])) {
            $list['default'] .= '<li class="list-group-item list-group-item"><a href="http://'.$v['domain'].'">'.$v['domain'].'</a></li>';
        } else {
            $list['login'] .= '<li class="list-group-item list-group-item"><a href="http://'.$v['domain'].'">'.$v['domain'].'</a> <a href="/?page=admin/advertiser/delete_domain&amp;domain='.$v['domain'].'" style="color: red;"><span class="glyphicon glyphicon-remove"></span></a></li>';
        }
    }
    temp_var('domains_login', $list['login']);
    temp_var('domains_default', $list['default']);

    return template('admin/advertiser/block_domains');
}

function advertiser_payments()
{
    $login = auth_login(['type' => 'shop']);
    // $login='stark';
    $arr = query_arr('select date(date) as day,sum(sum) as sum
		from '.typetab('purchase').'
		where login=\''.$login.'\' and currency=\'rur\'
		group by date with rollup
		;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.(is_null($v['day']) ? '�����' : $v['day']).'</td><td>'.$v['sum'].'</td></tr>';
    }
    $content = '<table class="table table-striped">
	<thead><tr><th>����</th><th>�����, ���.</th></tr></thead>
	'.$list.'
	</table>
	';

    return $content;
}

function advertiser_links()
{
    $login = auth_login(['type' => 'shop']);
    $login_info = cache('preparing_login_info', ['login' => $login], 0);
    // pre($login_info);
    $arr = query_arr('select dir,name,pic,country,item_id
		from '.typetab('landings').'
		where show_advertisers
		order by position
		;');
    // pre($arr);
    $user_settings = query_assoc('select timezone,metrika,url,scripts,pb_new,pb_confirm,pb_cancel,pb_cancel_after_confirm
		from '.typetab('users').'
		where login=\''.$login.'\'
		;');
    $arr_vars = query_arr('select var,type from '.typetab('advertisers_vars').' where login=\''.$login.'\';');
    $list_possible_vars = '';
    $list_required_vars = '';
    foreach ($arr_vars as $k => $v) {
        if ($v['type'] == 'possible') {
            if (! empty($list_possible_vars)) {
                $list_possible_vars .= ', ';
            }
            $list_possible_vars .= '&amp;'.$v['var'].'=';
        }
        if ($v['type'] == 'required') {
            if (! empty($list_required_vars)) {
                $list_required_vars .= ', ';
            }
            $list_required_vars .= '&amp;'.$v['var'].'=';
        }
    }
    $list = '';
    foreach ($arr as $v) {
        $item_info = empty($v['item_id']) ? [] : cache('preparing_item_info', ['id' => $v['item_id']]);
        $bonus = empty($v['item_id']) ? '?' : get_advertiser_bonus_for_item(['item_id' => $v['item_id'], 'advertiser' => $login]);
        $img = empty($v['pic']) ? 'style="width: 200px; height: 200px; background-color: pink;"' : 'src="/types/shop/template/landings/main/photos/'.$v['pic'].'" style="width: 200px;"';
        $list .= '<tr>
		<td><img '.$img.'></td>
		<td>
		<b>'.$v['name'].'</b><br>
		��������������: '.$bonus.' ���.<br>
		<!-- ��������: '.(empty($item_info['q_available']) ? '?' : $item_info['q_available']).' ��. -->
		</td>
		<td><a href="http://elitbrand.com/'.$v['dir'].'?uid='.$login_info['id'].'">http://elitbrand.com/'.$v['dir'].'?uid='.$login_info['id'].'</a></td>
		<td>'.$v['country'].'</td>
		</tr>';
    }
    $content = '
	<p><b>�������������� ��������� ������: </b><code>utm_source,utm_content,utm_campaign,utm_medium</code></p>
	'.(! empty($list_possible_vars) ? '<p><b>���� �������������� ���������:</b> <code>'.$list_possible_vars.'</code></p>' : '').'
	'.(! empty($list_required_vars) ? '<p><b>���� ������������ ���������:</b> <code>'.$list_required_vars.'</code></p>' : '').'
	'.(! empty($user_settings['url']) ? '<p><b>�������:</b> <code>'.braces2square_irreversible($user_settings['url']).'</code></p>' : '').'
	'.(! empty($user_settings['timezone']) ? '<p><b>�����:</b> <code>'.$user_settings['timezone'].' GMT</code></p>' : '').'
	'.(! empty($user_settings['metrika']) ? '<p><b>������.�������:</b> <code>'.$user_settings['metrika'].'</code></p>' : '').'
	'.(! empty($user_settings['scripts']) ? '<b>��� ��� �� ���������:</b><pre>'.htmlspecialchars($user_settings['scripts']).'</pre>' : '').'
	<table class="table table-striped">
	<thead><tr><th></th><th></th><th></th><th></th></tr></thead>
	'.$list.'
	</table>';

    return $content;
}

function advertiser_orders()
{
    $var['level'] = check_level();
    // $sql['group_day']=empty($_POST['group_day'])?'':'group by day desc';
    $sql['group_source']['status'] = empty($_POST['group_source']) ? false : true;
    $sql['group_source']['group'] = $sql['group_source']['status'] ? '' : ', utm_source';
    $arr = query_arr('select
		date(date) as day,
		count(orders.id) as q_all, utm_source,
		sum(if(orders.user_status not in (\'new\',\'unavailable\',\'canceled\'),1,0)) as q_confirmed,
		sum(if(orders.user_status in (\'take payment\',\'paid\'),1,0)) as q_paid
		from '.typetab('orders').'
		where '.$var['level']['where'].'
		group by day desc '.$sql['group_source']['group'].'
		;');
    $list = '';
    foreach ($arr as $k => $v) {
        $tr_margin = ($k != 0 and $v['day'] != $arr[$k - 1]['day']) ? 'style="margin-top: 10px;"' : '';
        $v2['source'] = $sql['group_source']['status'] ? '' : (empty($v['utm_source']) ? '��� �����' : $v['utm_source']);
        $list .= '<tr '.$tr_margin.'><td>'.$v['day'].'</td><td>'.$v2['source'].'</td><td>'.$v['q_all'].'</td><td>'.$v['q_confirmed'].'</td><td>'.$v['q_paid'].'</td></tr>';
    }
    $return = '<table class="table table-striped">
	<thead><tr><th>����</th><th>��������</th><th>�������</th><th>������������</th><th>���������</th></tr></thead>
	'.$list.'
	</table>';

    return $return;
}

function advertiser_stat_html()
{
    return advertiser_stat(['format' => 'html', 'daterange' => empty($_GET['daterange']) ? '' : $_GET['daterange'], 'group_by' => empty($_GET['group_by']) ? '' : $_GET['group_by']]);
}

function advertiser_stat_csv()
{
    return advertiser_stat(['format' => 'csv', 'daterange' => empty($_GET['daterange']) ? '' : $_GET['daterange'], 'group_by' => empty($_GET['group_by']) ? '' : $_GET['group_by']]);
}

function advertiser_stat($args)
{
    global $config;
    $format = empty($args['format']) ? 'html' : (in_array($args['format'], ['html', 'csv']) ? $args['format'] : error('������������ ������'));
    $daterange = empty($args['daterange']) ? '' : check_input($args['daterange']);
    $groups = ['day', 'source', 'item', 'id'];
    // $groups=array('day','source','item','sub_id','id');
    $group_by = empty($args['group_by']) ? 'day' : (in_array($args['group_by'], $groups) ? $args['group_by'] : error('������������ �������� ����������'));
    if (empty($daterange)) {
        $sql['date_from'] = 'current_date';
        $sql['date_to'] = 'date_add(current_date,interval 1 day)';
    } else {
        $explode_daterange = explode(' � ', $daterange);
        $sql['date_from'] = '\''.$explode_daterange[0].'\'';
        if (empty($explode_daterange[1])) {
            $explode_daterange[1] = $explode_daterange[0];
        }
        $sql['date_to'] = 'date_add(\''.$explode_daterange[1].'\',interval 1 day)';
        // } else {
        // $sql['date_to']='\''.$explode_daterange[1].'\'';
        // };
        // pre($explode_daterange);
    }
    // ����� mysql ������� ���� � ������ ��������� ���� ��������� current_timestamp
    $arr_daterange = query_assoc('select '.$sql['date_from'].' as date_from,'.$sql['date_to'].' as date_to;');
    if (empty($daterange)) {
        $daterange = $arr_daterange['date_from'];
    }
    // pre($arr_daterange);
    temp_var('date_from', $arr_daterange['date_from']);
    temp_var('date_to', $arr_daterange['date_to']);
    temp_var('daterange', $daterange);
    temp_var('group_by', $group_by);
    $sql['main_item'] = '';
    $sql['main_item_confirm_cost'] = '';
    if ($group_by == 'day') {
        $sql['group_by'] = 'day';
    }
    if ($group_by == 'item') {
        $sql['group_by'] = 'main_item';
    }
    if ($group_by == 'id') {
        if ((date_diff(date_create($arr_daterange['date_from']), date_create($arr_daterange['date_to']))->format('%a')) > 60) {
            alert('�������� 60 ����');
        }
        $sql['group_by'] = 'orders.id';
    }
    if ($group_by == 'source') {
        $sql['group_by'] = 'utm_source asc';
    }
    foreach ($groups as $v) {
        if ($group_by == $v) {
            $group_class = 'active';
            $group_checked = 'checked';
        } else {
            $group_class = '';
            $group_checked = '';
        }
        temp_var('group_by_'.$v.'_active', $group_class);
        temp_var('group_by_'.$v.'_checked', $group_checked);
    }
    //
    $sql['user'] = check_level();
    // pre($sql);
    //
    $arr = query_arr('select
		date(date) as day, orders.date, orders.id, orders.main_item, items.name as main_item_name, orders.bonus,
		count(orders.id) as q_all, orders.utm_source,utm_content,utm_campaign,utm_medium,orders.vars, orders.country,
		sum(if(orders.user_status not in (\'new\',\'unavailable\',\'canceled\'),1,0)) as q_confirmed,
		sum(if(orders.user_status not in (\'new\',\'unavailable\',\'canceled\'),orders.bonus,0)) as sum_cost_confirm,
		sum(if(orders.user_status in (\'take payment\',\'paid\'),1,0)) as q_paid,
		sum(if(orders.user_status in (\'canceled\'),1,0)) as q_canceled,
		sum(if(orders.user_status in (\'new\',\'unavailable\'),1,0)) as q_waiting
		from '.typetab('orders').'
		left join '.typetab('items').' on items.id=orders.main_item
		left join '.typetab('packages').' on packages.order_id=orders.id and packages.item_id=orders.main_item
		where '.$sql['user']['where'].' and date between '.$sql['date_from'].' and '.$sql['date_to'].'
		group by '.$sql['group_by'].'
		order by date
		;');
    $list['html'] = '';
    $list['csv'] = '';
    foreach ($arr as $k => $v) {
        if ($group_by == 'day') {
            $group_var = $v['day'];
        }
        if ($group_by == 'source') {
            $group_var = (empty($v['utm_source']) ? '��� �����' : $v['utm_source']);
        }
        if ($group_by == 'item') {
            $group_var = (empty($v['main_item']) ? '��� �������' : $v['main_item_name']);
        }
        // if ($group_by=='sub_id') {$group_var=(empty($v['user_sub_id'])?'��� SubID':$v['user_sub_id']);};
        if ($group_by == 'id') {
            $list_vars['html'] = '';
            $list_vars['csv'] = '';
            if (! empty($v['vars'])) {
                $vars = unserialize($v['vars']);
                foreach ($vars as $key => $var) {
                    $list_vars['csv'] .= empty($list_vars['csv']) ? '' : ', ';
                    // braces2square ����� ���� �������� ���������� �������� ������, ������ ������� �� ���������
                    $list_vars['html'] .= '<li><b>'.$key.'</b>: '.braces2square($var).'</li>';
                    $list_vars['csv'] .= $key.': '.braces2square($var);
                }
            }
            $list_utm['html'] = '';
            $list_utm['csv'] = '';
            $vars_utm['utm_source'] = $v['utm_source'];
            $vars_utm['utm_campaign'] = $v['utm_campaign'];
            $vars_utm['utm_medium'] = $v['utm_medium'];
            $vars_utm['utm_content'] = $v['utm_content'];
            foreach ($vars_utm as $key => $var) {
                $list_utm['csv'] .= empty($list_utm['csv']) ? '' : ', ';
                $list_utm['html'] .= '<li><b>'.$key.'</b>: '.braces2square($var).'</li>';
                $list_utm['csv'] .= $key.': '.braces2square($var);
            }
            $list['html'] .= '<tr><td>'.$v['id'].'</td><td>'.$v['date'].'</td><td>'.$v['country'].'</td><td>'.$v['main_item_name'].'</td>
			<td>'.(($v['q_canceled'] == 1) ? '�������' : (($v['q_waiting'] == 1) ? '� ���������' : '�����������')).'</td>
			<td>'.$v['sum_cost_confirm'].'</td><td><small><ul class="list-unstyled">'.$list_utm['html'].$list_vars['html'].'</small></ul></td></tr>';
            $list['csv'] .= $v['id'].';'.$v['date'].';'.$v['country'].';'.$v['main_item_name'].';'.
            (($v['q_canceled'] == 1) ? '�������' : (($v['q_waiting'] == 1) ? '� ���������' : '�����������')).';'.
            $v['sum_cost_confirm'].';'.$list_utm['csv'].$list_vars['csv'];
            $list['csv'] .= "\r\n";
        } else {
            $list['html'] .= '<tr><td>'.$group_var.'</td><td>'.$v['q_all'].'</td><td>'.$v['q_confirmed'].'</td>
			<td>'.$v['q_waiting'].'</td><td>'.$v['q_canceled'].'</td><td>'.$v['sum_cost_confirm'].'</td></tr>';
            $list['csv'] .= $group_var.';'.$v['q_all'].';'.$v['q_confirmed'].';'.$v['q_waiting'].';'.$v['q_canceled'].';'.$v['sum_cost_confirm'];
            $list['csv'] .= "\r\n";
        }
    }
    $group_field = ['day' => '����', 'item' => '�����', 'source' => '��������', 'sub_id' => 'SubID', 'id' => 'ID'];
    if ($group_by == 'id') {
        $fields['html'] = '<tr><th>'.$group_field[$group_by].'</th><th>����</th><th>������</th><th>�����</th><th>������</th><th>������</th><th>����������</th></tr>';
        $fields['csv'] = $group_field[$group_by].';����;������;�����;������;������;����������';
    } else {
        $fields['html'] = '<tr><th>'.$group_field[$group_by].'</th><th>�������</th><th>������������</th><th>� ���������</th><th>������</th><th>������</th></tr>';
        $fields['csv'] = $group_field[$group_by].';�������;������������;� ���������;������;������';
    }
    temp_var('stat_table_list', $fields['html'].$list['html']);
    if ($format == 'html') {
        return template('admin/advertiser/block_stat');
    }
    if ($format == 'csv') {
        header('Content-Type: text/csv; charset: utf-8');
        header('Content-Disposition: attachment; filename="stat.csv"');

        return mb_convert_encoding($fields['csv']."\r\n".$list['csv'], 'utf-8', $config['charset']);
    }
}
