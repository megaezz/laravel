<?php

namespace MegawebV1;

function user_login()
{
    $login = auth_login(['type' => 'shop']);
    $arr = preparing_login_info(['login' => $login]);

    // pre($arr);
    return $arr['login'];
}

function user_id()
{
    $login = auth_login(['type' => 'shop']);
    $arr = preparing_login_info(['login' => $login]);

    return $arr['id'];
}

function user_token()
{
    $login = auth_login(['type' => 'shop']);
    $arr = preparing_login_info(['login' => $login]);

    return $arr['token'];
}

function fb_pixel_head()
{
    global $config;
    $item_id = isset($config['vars']['item_id']) ? $config['vars']['item_id'] : error('�� ������� ID ������');
    $arr = cache('preparing_item_info', ['id' => $item_id], 0);
    if (empty($arr['fb_pixel_id'])) {
        return;
    }
    temp_var('fb_pixel_id', $arr['fb_pixel_id']);

    return template('landings/default/fb_pixel/head');
}

function fb_pixel_onclick()
{
    global $config;
    $item_id = isset($config['vars']['item_id']) ? $config['vars']['item_id'] : error('�� ������� ID ������');
    $arr = cache('preparing_item_info', ['id' => $item_id], 0);
    if (empty($arr['fb_pixel_id'])) {
        return;
    }
    temp_var('item_cost_kzt', $arr['cost_kzt']);

    return template('landings/default/fb_pixel/onclick');
}

function item_carousel($args = [])
{
    global $config;
    // ���� ����� �� ���
    $args['js_id'] = empty($args['js_id']) ? '' : $args['js_id'];
    $item_id = isset($config['vars']['item_id']) ? $config['vars']['item_id'] : error('�� ������� ID ������');
    $arr = cache('preparing_item_info', ['id' => $item_id], 0);
    $arr_pics = query_arr('select path from '.typetab('items_images').' where carousel and item_id=\''.$item_id.'\';');
    $i = 0;
    $list_pictures = '';
    $list_indicators = '';
    foreach ($arr_pics as $v) {
        temp_var('i', $i);
        temp_var('pic', $v['path']);
        temp_var('active', (($i === 0) ? 'active' : ''));
        temp_var('selected', (($i === 0) ? 'selected' : ''));
        // temp_var('name',$arr['land_name']);
        $list_pictures .= template('landings/default/item_carousel_picture');
        $list_indicators .= template('landings/default/item_carousel_indicator');
        clear_temp_vars();
        $i++;
    }
    temp_var('pictures', $list_pictures);
    temp_var('indicators', $list_indicators);
    $content = template('landings/default/block_carousel');
    clear_temp_vars();

    // pre($pics);
    // pre($list);
    return $content;
}

function item_name()
{
    global $config;
    $item_id = isset($config['vars']['item_id']) ? $config['vars']['item_id'] : error('�� ������� ID ������');
    $arr = cache('preparing_item_info', ['id' => $item_id], 0);

    return $arr['name'];
}

function item_description()
{
    global $config;
    $item_id = isset($config['vars']['item_id']) ? $config['vars']['item_id'] : error('�� ������� ID ������');
    $arr = cache('preparing_item_info', ['id' => $item_id], 0);

    return $arr['description'];
}

function item_video()
{
    global $config;
    $item_id = isset($config['vars']['item_id']) ? $config['vars']['item_id'] : error('�� ������� ID ������');
    $arr = cache('preparing_item_info', ['id' => $item_id], 0);
    if (! $arr['youtube_id']) {
        return '';
    }
    temp_var('youtube_id', $arr['youtube_id']);
    $content = template('landings/default/block_video');
    clear_temp_vars();

    return $content;
}

function set_get_first($first = '')
{
    $_GET['first'] = empty($first) ? false : $first;
}

function order_status_list($type = '')
{
    /*
    'canceled' 'new' 'unavailable' 'check address' 'waiting send' 'packed' 'sent' 'delivered not paid'
    'take payment' 'paid'
    'refund' 'refund paid'	'return' 'return paid'
    */
    // 'return delivered'
    $arr = [
        '������' => ['canceled'],
        '�����' => ['new', 'unavailable'],
        '� ���������' => ['check address', 'waiting send', 'packed'],
        '������� �����' => ['refund', 'refund paid'],
        '�� �����������' => ['return', 'return paid'],
        '��������' => ['paid', 'take payment'],
        '����������' => ['sent', 'delivered not paid'],
    ];
    $arr = array_merge($arr, [
        '����������������' => array_merge($arr['�����'], $arr['������']),
        '��������������' => array_merge($arr['� ���������'], $arr['����������'], $arr['������� �����'], $arr['�� �����������'], $arr['��������']),
        '����������' => array_merge($arr['��������'], $arr['������� �����']),
    ]);
    // pre($arr);
    $str = [];
    foreach ($arr as $k => $v) {
        $str[$k] = array_to_str_alt($v);
    }
    pre($str);
}
// order_status_list();

function item_name_by_id()
{
    $item_id = (empty($_GET['item_id'])) ? error('�� ������� ID ������') : checkstr($_GET['item_id']);
    $arr = query_assoc('select name from '.typetab('items').' where id=\''.$item_id.'\';');

    return $arr['name'];
}

function toreto_discount_day()
{
    return discount_date(['template' => 'li_discount_date', 'type' => 'day']);
}

function toreto_discount_month()
{
    global $db;
    $date_arr = getdate();
    $date = my_date($date_arr[0]);
    $source_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];

    return iconv($source_encoding, 'utf-8', $date['month']);
}

function toreto_discount_year()
{
    return discount_date(['template' => 'li_discount_date', 'type' => 'year']);
}

// ������� ���������� ���� (����, ����� ��� ���) �� ������� �� ��������, ��������� ������ ����� �������� � ��������� $args['template']
function discount_date($args)
{
    global $config;
    $plus_sec = 86400;
    if (! isset($args['type'])) {
        error('�� ������� ����� ����');
    }
    $tname = isset($args['template']) ? $args['template'] : error('�� ������� ������');
    $template = 'landings/'.landing_name().'/'.$tname;
    // pre($template);
    $date_arr = getdate();
    $new_date_unix = $date_arr[0] + $plus_sec;
    $date = my_date($new_date_unix);
    $text = isset($date[$args['type']]) ? $date[$args['type']] : error('��� ����� ����� ����');
    $arr = str_split($text);
    $list = '';
    foreach ($arr as $v) {
        temp_var('symbol', $v);
        $list .= template($template);
    }

    return $list;

}

function ok_post_name()
{
    global $config;
    $var['charset'] = isset($_POST['charset']) ? $_POST['charset'] : $config['charset'];
    $var['name'] = isset($_POST['name']) ? check_input($_POST['name'].',', $var['charset']) : '';

    return $var['name'];
}

function date_plus_days($days)
{
    $plus = $days * 86400;
    $date = getdate();
    $new_date = getdate($date[0] + $plus);

    return str_pad($new_date['mday'], 2, 0, STR_PAD_LEFT).'-'.str_pad($new_date['mon'], 2, 0, STR_PAD_LEFT).'-'.$new_date['year'];
}

function date_plus_days_slashed($days)
{
    $plus = $days * 86400;
    $date = getdate();
    $new_date = getdate($date[0] + $plus);

    return $new_date['year'].'/'.str_pad($new_date['mon'], 2, 0, STR_PAD_LEFT).'/'.str_pad($new_date['mday'], 2, 0, STR_PAD_LEFT);
}

function vars_item_id()
{
    global $config;

    return (empty($config['vars']['item_id'])) ? error('�� ����� item_id') : $config['vars']['item_id'];
    /*
    if (isset($config['vars']['item_id'])) {
        return $config['vars']['item_id'];
    } else {
        error('�� ����� item_id');
    };
    */
}

function vars_set_id()
{
    global $config;

    return (empty($config['vars']['set_id'])) ? error('�� ����� set_id') : $config['vars']['set_id'];
    /*
    if (isset($config['vars']['set_id'])) {
        return $config['vars']['set_id'];
    } else {
        error('�� ����� set_id');
    };
    */
}
/*
function landing_default_carousel_2() {
    return landing_default_carousel(array('js_id'=>'2'));
}
*/

/*
function landing_info_name() {
    $arr=cache('preparing_landing_info',array('dir'=>landing_name()),0);
    return $arr['land_name'];
}

function landing_info_description() {
    $arr=cache('preparing_landing_info',array('dir'=>landing_name()),0);
    return $arr['land_description'];
}
*/

function preparing_landing_info($args = [])
{
    $args['dir'] = empty($args['dir']) ? error('�� ������� �������') : check_input($args['dir']);
    $arr = query_assoc('
		select item_id,set_id,land_name,land_description,land_pic_1,land_pic_2,land_pic_3,
		land_pic_4,land_pic_5,youtube_id
		from '.typetab('landings').'
		where landings.dir=\''.$args['dir'].'\'
		;');
    $arr['item_info'] = ($arr['item_id']) ? preparing_item_info(['id' => $arr['item_id']]) : false;
    $arr['set_info'] = ($arr['set_id']) ? preparing_set_info(['id' => $arr['set_id']]) : false;
    if (! $arr) {
        error('������� �� ����������');
    }

    // pre($arr);
    return $arr;
}

function landing_name()
{
    global $config;

    return isset($config['vars']['path']) ? $config['vars']['path'] : error('�� ������� ��� ��������');
    // pre($config['vars']);
    // return isset($config['vars']['path'])?$config['vars']['path']:'n/a';
}

function landing_path()
{
    global $config;
    $path = isset($config['vars']['path']) ? $config['vars']['path'] : error('�� ������� ��� ��������');

    return '/types/shop/template/landings/'.$path.'/';
}

function item_price_x_kzt($id = false, $p = false)
{
    if (! $p) {
        error('�� ������� �������');
    }

    // if (!$id) {$id=vars_item_id();};
    // $item_info=cache('preparing_item_info',array('id'=>$id));
    return ceil(item_price_kzt($id) / (1 - $p / 100));
}

function item_price_x_rur($id = false, $p = false)
{
    if (! $p) {
        error('�� ������� �������');
    }

    return ceil(item_price_rur($id) / (1 - $p / 100));
}

function item_price_x_dif_kzt($id, $p)
{
    return item_price_x_kzt($id, $p) - item_price_kzt($id);
}

function item_price_x_dif_rur($id, $p)
{
    return item_price_x_rur($id, $p) - item_price_rur($id);
}

function item_price_70_kzt($id = false)
{
    return item_price_x_kzt($id, 70);
}

function item_price_60_kzt($id = false)
{
    return item_price_x_kzt($id, 60);
}

function item_price_30_kzt($id = false)
{
    return item_price_x_kzt($id, 30);
}

function item_price_25_kzt($id = false)
{
    return item_price_x_kzt($id, 25);
}

function item_price_40_kzt($id = false)
{
    return item_price_x_kzt($id, 40);
}

function item_price_45_kzt($id = false)
{
    return item_price_x_kzt($id, 45);
}

function item_price_50_rur($id = false)
{
    return item_price_x_rur($id, 50);
}

function item_price_70_rur($id = false)
{
    return item_price_x_rur($id, 70);
}

function item_price_60_rur($id = false)
{
    return item_price_x_rur($id, 60);
}

function item_price_40_rur($id = false)
{
    return item_price_x_rur($id, 40);
}

function item_price_50_kzt($id = false)
{
    return item_price_x_kzt($id, 50);
}

function item_price_70_dif_kzt($id = false)
{
    return item_price_x_dif_kzt($id, 70);
}

function item_price_60_dif_kzt($id = false)
{
    return item_price_x_dif_kzt($id, 60);
}

function item_price_rur($id = false)
{
    if (! $id) {
        $id = vars_item_id();
    }
    $item_info = cache('preparing_item_info', ['id' => $id]);

    return $item_info['cost_rur'];
}

function item_price_kzt($id = false)
{
    if (! $id) {
        $id = vars_item_id();
    }
    $item_info = cache('preparing_item_info', ['id' => $id]);

    return $item_info['cost_kzt'];
}

function item_price_70_kzt_by_land($land = false)
{
    return item_price_x_kzt_by_land($land, 70);
}

function item_price_x_kzt_by_land($land = false, $p = false)
{
    if (! $p) {
        error('�� ������� �������');
    }
    if (! $land) {
        error('�� ������� �������');
    }

    return ceil(item_price_kzt_by_land($land) / (1 - $p / 100));
}

function item_price_kzt_by_land($land = false)
{
    if (! $land) {
        error('�� ������� �������');
    }
    $item_info = cache('preparing_item_info', ['dir' => $land]);

    return $item_info['cost_kzt'];
}

function item_id_by_land($land)
{
    if (! $land) {
        error('�� ������� �������');
    }
    $item_info = cache('preparing_item_info', ['dir' => $land]);

    return $item_info['id'];
}

function set_price_real_kzt($id = false)
{
    if (! $id) {
        $id = vars_set_id();
    }
    $set_info = cache('preparing_set_info', ['id' => $id]);

    // pre($set_info);
    return $set_info['cost_kzt_real'];
}

function set_price_kzt($id = false)
{
    if (! $id) {
        $id = vars_set_id();
    }
    $set_info = cache('preparing_set_info', ['id' => $id]);

    // pre($set_info);
    return $set_info['cost_kzt'];
}

function set_price_x_kzt($id = false, $p = false)
{
    if (! $p) {
        error('�� ������� �������');
    }

    // if (!$id) {$id=vars_item_id();};
    // $item_info=cache('preparing_item_info',array('id'=>$id));
    return ceil(set_price_kzt($id) / (1 - $p / 100));
}

function set_price_30_kzt($id = false)
{
    return set_price_x_kzt($id, 30);
}

function set_price_40_kzt($id = false)
{
    return set_price_x_kzt($id, 40);
}

function set_price_50_kzt($id = false)
{
    return set_price_x_kzt($id, 50);
}

function set_price_70_kzt($id = false)
{
    return set_price_x_kzt($id, 70);
}

function item_available($id = false)
{
    if (! $id) {
        $id = vars_item_id();
    }
    $item_info = cache('preparing_item_info', ['id' => $id]);

    return $item_info['q_available'];
}
