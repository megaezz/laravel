<?php

namespace MegawebV1;

// ���������� ������ ������� ��� ������� ��� ���������� �� �����
function kazpost_orders_without_track_info()
{
    $arr = query_arr('
		select orders.id,orders.date,orders.name,orders.address,orders.info
		from '.typetab('orders').'
		left join '.typetab('kazpost_api').' on kazpost_api.track=orders.track
		where orders.status not in (\'new\',\'unavailable\',\'canceled\',\'check address\',\'packed\',\'waiting send\')
		and kazpost_api.track is null
		order by orders.date;
		');
    $list = '';
    foreach ($arr as $v) {
        $list .= '
		<tr>
			<td><a href="/?page=admin/orders&amp;status=all&amp;order_id='.$v['id'].'">#'.$v['id'].'</a></td>
			<td>'.$v['date'].'</td>
			<td>'.$v['name'].'</td>
			<td>'.$v['address'].'</td>
			<td>'.$v['info'].'</td>
		</tr>
		';
    }

    return $list;
}

// ���������� ������ ������� �������� � ������� ������, �� �� �� ��� �� �������� ��� �����������
function kazpost_orders_should_been_paid()
{
    $arr = query_arr('select kazpost_incomes_orders.date,kazpost_incomes_orders.file,orders.id,orders.status
		from '.typetab('kazpost_incomes_orders').'
		join '.typetab('orders').' on orders.id=kazpost_incomes_orders.order_id
		where orders.status not in (\'paid\',\'refund paid\',\'refund\')
		;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '
		<tr>
			<td><a href="/?page=admin/kazpost/incomes&file='.$v['file'].'">'.$v['date'].'</a></td>
			<td><a href="/?page=admin/orders&amp;status=all&amp;order_id='.$v['id'].'">#'.$v['id'].'</a></td>
		</tr>';
    }

    return $list;
}

function kazpost_sent_orders_by_months()
{
    $arr = query_arr('
		select
		year(kazpost_api.sent_date) as sent_year,
		month(kazpost_api.sent_date) as sent_month,
		(select count(*) from '.typetab('packages').' where order_id=orders.id and item_id=195) as is_ems,
		count(*) as q
		from '.typetab('kazpost_api').'
		join '.typetab('orders').' on orders.track=kazpost_api.track
		group by sent_year,sent_month,is_ems
		order by sent_year,kazpost_api.sent_date;
		');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['sent_year'].'</td><td>'.rus_month($v['sent_month']).'</td><td>'.($v['is_ems'] ? 'EMS' : '�������').'</td><td>'.$v['q'].'</td></tr>';
    }
    $content = '
	<table class="table table-striped">
	<thead><tr><th>���</th><th>�����</th><th>��������</th><th>���-��</th></tr></thead>
	'.$list.'
	</table>
	';

    return $content;
}

function kazpost_update_sent_dates()
{
    // $from_date=empty($_GET['from_date'])?error('from_date?'):$_GET['from_date'];
    // $to_date=empty($_GET['to_date'])?error('to_date?'):$_GET['to_date'];
    // $from_sent_date=empty($_GET['from_sent_date'])?error('from_sent_date?'):$_GET['from_sent_date'];
    // $to_sent_date=empty($_GET['to_sent_date'])?error('to_sent_date?'):$_GET['to_sent_date'];
    // query('update '.typetab('kazpost_api').' set sent_date=null;');
    $arr = query_arr('select track from '.typetab('kazpost_api').' where sent_date is null;');
    $list = '';
    $rows = 0;
    foreach ($arr as $v) {
        $track_info = get_track_info_kz(['track' => $v['track'], 'from' => 'db']);
        // pre($track_info);
        $sent_date = '';
        if (isset($track_info['events'])) {
            $events_reversed = array_reverse($track_info['events']);
            // pre($events_reversed);
            foreach ($events_reversed as $event) {
                if (isset($event['date'])) {
                    $sent_date = date('Y-m-d', strtotime($event['date']));
                    // pre($sent_date);
                    break;
                }
            }
            query('update '.typetab('kazpost_api').' set sent_date=\''.$sent_date.'\' where track=\''.$v['track'].'\';');
            $rows++;
        }
        // pre($track_info);
        // $list.='<li>'.$v['track'].': '.$sent_date.'</li>';
    }
    // $content='<ol>'.$list.'</ol>';
    // return $content;
    logger()->info('���� �������� ���������. ��������� �����: '.$rows);
}

function kazpost_incomes_summary()
{
    /*
    $summary=query_arr('
        select substr(file,1,4) as num,concat(\'20\',substr(file,10,2)) as year,count(*) as q,sum(sum) as sum
        from '.typetab('kazpost_incomes').'
        group by year,num
        order by num,year
        ;');
        */
    $arr_distinct = query_arr('
			select distinct order_id,too,sum,year(date) as year
			from '.typetab('kazpost_incomes_orders').'
			;');
    foreach ($arr_distinct as $v) {
        if (! isset($arr_dist[$v['year']][$v['too']])) {
            $arr_dist[$v['year']][$v['too']] = 0;
        }
        $arr_dist[$v['year']][$v['too']] = $arr_dist[$v['year']][$v['too']] + $v['sum'];
    }
    $arr = query_arr('
		select extract(year from date) as year,too,sum(sum) as sum,count(*) as q
		from '.typetab('kazpost_incomes_orders').'
		group by year,too
		');
    // pre($arr);
    $list = '';
    // pre($years);
    foreach ($arr as $v) {
        $list .= '<li><b>'.$v['year'].'�. ���: </b> <b>'.$v['too'].'</b> ������� '.$v['q'].' �� ����� '.number_format($v['sum'], 0, ',', ' ').'
		(��� ������: '.(isset($arr_dist[$v['year']][$v['too']]) ? number_format($arr_dist[$v['year']][$v['too']], 0, ',', ' ') : '��� ������').') �����</li>';
    }
    /*
    foreach ($summary as $v) {
        $list.='<li><b>'.$v['num'].' �� </b> <b>'.$v['year'].' �.</b> ������� '.$v['q'].' �� ����� '.$v['sum'].' �����</li>';
    };
    */
    $return = '<ul style="list-style-type: none;">'.$list.'</ul>';

    return $return;
}

function kazpost_incomes_list()
{
    global $config;
    $data_too = query('select distinct too, \'\' as list
		from '.typetab('kazpost_incomes_orders').' where too is not null;');
    $toos = readdata($data_too, 'too');
    // pre($toos);
    $data = query('select substr(file,1,4) as num,file,sum,status from '.typetab('kazpost_incomes').';');
    $db_incomes = readdata($data, 'file');
    // pre($db_incomes);
    $files_dir = $config['path']['server'].'types/'.$config['type'].'/files/kazpost/incomes/';
    $selected_filename = empty($_GET['file']) ? false : $_GET['file'];
    $selected_file = $files_dir.$selected_filename;
    $arr = list_directory_files($files_dir);
    foreach ($arr as $k => $v) {
        // $arr2[$k]=preg_match('/^([0-9]{4})_([0-9]{2})([0-9]{2})([0-9]{2})\.xlsx$/',$v,$preg)?('<small>'.$preg[1].'</small> 20'.$preg[4].'-'.$preg[3].'-'.$preg[2]):$v;
        $file_info = kazpost_get_info_from_registry_filename($v);
        $arr2[$k] = empty($file_info) ? $v : ('<small>'.$file_info['too'].'</small> '.$file_info['date']);
    }
    arsort($arr2);
    $list = '';
    $list_time = '';
    $toos['undefined']['list'] = '';
    $toos['undefined']['too'] = 'undefined';
    foreach ($arr2 as $k => $v) {
        $filename = $arr[$k];
        $selected_style = ($filename === $selected_filename) ? 'style="font-weight: bold;"' : '';
        $status_text = isset($db_incomes[$filename]) ? ($db_incomes[$filename]['status'] ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove" style="color: red;"></span>') : '<span class="glyphicon glyphicon-time"></span>';
        $str = '<li>'.$status_text.' <a href="/?page=admin/kazpost/incomes&amp;file='.$filename.'" '.$selected_style.'>'.$v.'</a></li>';
        $list_time .= isset($db_incomes[$filename]) ? '' : $str;
        // $list.=$str;
        $num = (isset($db_incomes[$filename]['num']) and isset($toos[$db_incomes[$filename]['num']])) ? $db_incomes[$filename]['num'] : false;
        // pre($num);
        if ($num) {
            // pre($num);
            $toos[$num]['list'] .= $str;
        } else {
            $toos['undefined']['list'] .= $str;
        }
        // $list.=$str;
    }
    // pre($num);
    // pre($toos);
    foreach ($toos as $too) {
        $list .= '
		<div class="col-md-2">
		<b>'.$too['too'].'</b>
		<ol>'.$too['list'].'</ol>
		</div>
		';
    }
    $list_errors = '';
    // pre($db_incomes);
    // pre($arr);
    foreach ($db_incomes as $db) {
        // pre($db['file']);
        $is_exist = false;
        foreach ($arr as $v) {
            // pre($v);
            if ($db['file'] === $v) {
                $is_exist = true;
                break;
            }
        }
        if (! $is_exist) {
            $list_errors .= '<li>'.$db['file'].'</li>';
        }
    }
    $errors = empty($list_errors) ? '' : '���� � �� �� ��� �����: <ol>'.$list_errors.'</ol>';
    $return = '<div class="col-md-2">'.$errors.'<ol>'.$list_time.'</ol></div>
	'.$list.'
	';

    return $return;
}

function kazpost_get_info_from_registry_filename($filename)
{
    $arr = [];
    if (preg_match('/^([0-9]{4})_([0-9]{2})([0-9]{2})([0-9]{2})\.xlsx$/', $filename, $preg)) {
        $arr['date'] = '20'.$preg[4].'-'.$preg[3].'-'.$preg[2];
        $arr['too'] = $preg[1];
    }

    return $arr;
}

function kazpost_incomes_result()
{
    global $config;
    $files_dir = $config['path']['server'].'types/'.$config['type'].'/files/kazpost/incomes/';
    $rus['statuses'] = shop_translation('order_status');
    $selected_filename = empty($_GET['file']) ? false : $_GET['file'];
    if (! $selected_filename) {
        return;
    }
    $selected_file = $files_dir.$selected_filename;
    if (! file_exists($selected_file)) {
        error('���� �� ����������');
    }
    $arr = get_array_from_xlsx(['file' => $selected_file, 'temp_dir' => $files_dir.'temp/']);
    // pre($arr);
    $arr = $arr['sheet1.xml'];
    // pre($arr);
    $list = '';
    $q_ok = 0;
    $q_tracks = 0;
    $sum_all_real = 0;
    $sum_all = false;
    $sum_all_str = false;
    // ������� ������ ������� ����� �� ���� ��������� � kazpost_incomes_orders
    query('insert into '.typetab('kazpost_incomes').' set file=\''.$selected_filename.'\' on duplicate key update file=file;');
    foreach ($arr as $k => $v) {
        foreach ($v as $k1 => $v1) {
            // ���������, ����� ���� ����� ���������� "�����"
            $v[$k1] = mb_convert_encoding($v1, 'windows-1251', 'utf-8');
            $arr[$k] = $v;
            // ���� ����� ��� ���������� - ������
            if ($sum_all) {
                continue;
            }
            // ���� ����� ����� ��� � ���� ������ ���� - �� ������, ����
            if ($sum_all_str and ! empty($v[$k1])) {
                $sum_all = (int) trim(str_replace(' ', '', $v[$k1]));
                // pre($v);
                // pre($sum_all);
            }
            // ���� � ������ ��� ����� ����� - ������
            if (! strstr($v[$k1], '�����:')) {
                continue;
            }
            // ���� ����� ����� ���� ����� - ���������� �����, ���� ��� - ������ ����� ��� � ���� ������ ����� � ������
            if (trim(str_replace('�����:', '', trim($v[$k1]))) !== '') {
                $sum_all = (int) trim(str_replace('�����:', '', trim($v[$k1])));
                // pre($v);
                // pre($sum_all);
            } else {
                // ������ ����� �� ����� ����� �����
                $sum_all_str = true;

                continue;
            }

            // $sum_all=(!$sum_all and strstr($v[$k1],'�����:'))?((int)trim(str_replace('�����:','',trim($v[$k1])))):$sum_all;
            // ���� ��� � ���� ������ ���� �����: �� ��������� ������ ������� � ���� ��� �� �����, ���������� � sum_all_alt
            // $sum_all_str=(!$sum_all and strstr($v[$k1],'�����:'))?($sum_all_str=true):$sum_all_str;
            // $sum_all_alt=(!$sum_all and $sum_all_str and !empty($v[$k1]))?((int)trim($v[$k1])):$sum_all_alt;
            // $sum_all=$sum_all?$sum_all:$sum_all_alt;
        }
        // ���������� ������ �������
        if ((int) $v[0] === 0) {
            continue;
        }
        $q_tracks++;
        $track = $v[7];
        // pre($v);
        $sum = (int) str_replace(' ', '', $v[3]);
        $order = query_assoc('select id,status,track from '.typetab('orders').' where track=\''.trim($track).'\';');
        if (empty($order['id'])) {
            $list .= '<li>'.$track.' <span style="color: red;">����� �� ������</span></li>';

            continue;
        }
        $order_info = preparing_order_info(['order_id' => $order['id']]);
        $file_info = kazpost_get_info_from_registry_filename($selected_filename);
        query('
			insert into '.typetab('kazpost_incomes_orders').'
			set file=\''.$selected_filename.'\', order_id=\''.$order['id'].'\', sum=\''.$sum.'\',
			too='.(empty($file_info['too']) ? 'null' : '\''.$file_info['too'].'\'').',
			date='.(empty($file_info['date']) ? 'null' : '\''.$file_info['date'].'\'').'
			on duplicate key update sum=\''.$sum.'\',
			too='.(empty($file_info['too']) ? 'null' : '\''.$file_info['too'].'\'').',
			date='.(empty($file_info['date']) ? 'null' : '\''.$file_info['date'].'\'').'
			;');
        $order_link = '<a href="/?page=admin/orders&amp;status=all&amp;order_id='.$order['id'].'">#'.$order['id'].'</a>';
        // pre(var_dump($order_info));
        $sum_all_real = $sum_all_real + $order_info['cost_kzt'];
        $sum_check_result = ($sum === (int) $order_info['cost_kzt']) ? '' : '<span class="glyphicon glyphicon-remove"></span> � �������: '.$sum.' �� ����: '.$order_info['cost_kzt'];
        if (! in_array($order['status'], ['take payment', 'delivered not paid'])) {
            if (in_array($order['status'], ['paid', 'refund', 'refund paid'])) {
                $list .= '<li>'.$track.' '.$order_link.' ������ ������ '.$sum_check_result.'</li>';
            } else {
                $list .= '<li>'.$track.' '.$order_link.' <span style="color: red;">������� ������ <b>'.$rus['statuses'][$order['status']].'</b>, ������ ���� <b>'.$rus['statuses']['take payment'].'</b></span> '.$sum_check_result.'</li>';
            }

            // pre($order_info);
            continue;
        }
        order_set_status(['order_id' => $order['id'], 'status' => 'paid']);
        $q_ok++;
        $list .= '<li>'.$order['track'].' '.$order_link.' <span style="color: green;">ok</span> '.$sum_check_result.'</li>';
    }
    $sum_all_check_result = ($sum_all === $sum_all_real) ? true : false;
    query('
		insert into '.typetab('kazpost_incomes').'
		set file=\''.$selected_filename.'\', sum=\''.$sum_all.'\', status=\''.($sum_all_check_result ? 1 : 0).'\'
		on duplicate key update sum=\''.$sum_all.'\', status=\''.($sum_all_check_result ? 1 : 0).'\'
		;');
    $sum_all_check_text = $sum_all_check_result ? '<span class="glyphicon glyphicon-ok"></span> '.$sum_all_real : '<span class="glyphicon glyphicon-remove"></span> � �������: '.$sum_all.' �� ����: '.$sum_all_real;
    $result = '<ol>'.$list.'</ol>
	<p>���������� ������: '.$q_tracks.'<br>
	�������� �������: '.$q_ok.'<br>
	�������� ����� �����: '.$sum_all_check_text.' �����</p>
	';

    return $result;
}

function kazpost_submit_incomes_upload()
{
    global $config;
    $files_dir = $config['path']['server'].'types/'.$config['type'].'/files/kazpost/incomes/';
    $xls_files_dir = $files_dir.'xls/';
    $files_arr = empty($_FILES['file']['tmp_name']) ? error('�� �������� ����� �������') : $_FILES['file'];
    $q_files = count($files_arr['tmp_name']);
    for ($i = 0; $i < $q_files; $i++) {
        $uploaded_file = $files_arr['tmp_name'][$i];
        $uploaded_filename = $files_arr['name'][$i];
        $pathinfo = pathinfo($uploaded_filename);
        if ($pathinfo['extension'] === 'xlsx') {
            $dest_filename = $uploaded_filename;
            $dest_file = $files_dir.$dest_filename;
            if (! file_exists($dest_file)) {
                move_uploaded_file($uploaded_file, $dest_file);
            }
        }
        if ($pathinfo['extension'] === 'xls') {
            $source_file = $xls_files_dir.$uploaded_filename;
            $dest_filename = $uploaded_filename.'x';
            $dest_file = $files_dir.$dest_filename;
            // ���� ������ ����� ���, �� �������� ���� � ���������� �������
            if (! file_exists($dest_file)) {
                move_uploaded_file($uploaded_file, $source_file);
                // ������������ � xlsx
                // pre($saved_file_xlsx);
                convertio(['from' => $source_file, 'to' => $dest_file, 'ext' => 'xlsx']);
            }
        }
    }
    // $uploaded_file=empty($_FILES['file']['tmp_name'])?error('�� ������� ���� �������'):$_FILES['file']['tmp_name'];
    // $uploaded_filename=$_FILES['file']['name'];
    // $saved_file=$files_dir.$uploaded_filename;
    redirect('/?page=admin/kazpost/incomes&file='.$dest_filename);
}

function kazpost_statuses($status)
{
    if (empty($status)) {
        // error('�� ������� ������');
        return '�� ������� ������';
    }

    $arr = [
        'BOXISS_UNDO' => ['old' => '������ �������� �� �-�', 'new' => '������ �������� �� �-�', 'lv' => 0],
        'AVIASND' => ['old' => '�������� �� ����', 'new' => '�������� �� ����', 'lv' => 0],
        'AVIASND_UNDO' => ['old' => '������ �������� �� ����', 'new' => '������ �������� �� ����', 'lv' => 0],
        'BAT' => ['old' => '���������� �����', 'new' => '���������� �����', 'lv' => 0],
        'CUSTOM_RET' => ['old' => '������� � �������', 'new' => '������� � �������', 'lv' => 0],
        'CUSTSRT_SND' => ['old' => '������ � �������(� ��������)', 'new' => '������ � �������(� ��������)', 'lv' => 0],
        'CUSTSTR_RET' => ['old' => '������� � ������� (� ��������)', 'new' => '������� � ������� (� ��������)', 'lv' => 0],
        'DELAY_RET' => ['old' => '������� � �������', 'new' => '������� � �������', 'lv' => 0, 'special' => 'warn'],
        'DLV' => ['old' => '������ �� ��������', 'new' => '������� �������/������� ���������', 'lv' => 0, 'special' => 'info'],
        'DLV_POBOX' => ['old' => '�������� � �/�', 'new' => '�������� � �/�', 'lv' => 0],
        'DPAY' => ['old' => '�������', 'new' => '�������', 'lv' => 0, 'special' => 'info'],
        'ISSPAY' => ['old' => '�������', 'new' => '�������', 'lv' => 0, 'special' => 'info'],
        'ISSSC' => ['old' => '�������', 'new' => '�������', 'lv' => 0, 'special' => 'info'],
        'ISS' => ['old' => '�������', 'new' => '�������', 'lv' => 0, 'special' => 'info'],
        'NON' => ['old' => '�� ������', 'new' => '�� ������', 'lv' => 0],
        'NONDLV' => ['old' => '�� ����������', 'new' => '��������� ������� ��������', 'lv' => 0, 'special' => 'warn'],
        'NONDLV_S' => ['old' => '������� �� ��������', 'new' => '������� �� ��������', 'lv' => 0, 'special' => 'warn'],
        'NONDLV_Z' => ['old' => '������� �� ��������', 'new' => '������� �������, �� ��������', 'lv' => 0, 'special' => 'warn'],
        'NON_S' => ['old' => '�� ������', 'new' => '�� ������', 'lv' => 0, 'special' => 'warn'],
        'PRC' => ['old' => '�����������', 'new' => '�����������', 'lv' => 0],
        'RCP' => ['old' => '����� 1', 'new' => '����� 1', 'lv' => 0],
        'RCPOPS' => ['old' => '�����', 'new' => '�����', 'lv' => 0],
        'RDR' => ['old' => '�����', 'new' => '�����', 'lv' => 0],
        'RDRSC' => ['old' => '�����', 'new' => '�����', 'lv' => 0],
        'RDRSCSTR' => ['old' => '�����', 'new' => '�����', 'lv' => 0],
        'RET' => ['old' => '�������', 'new' => '�������', 'lv' => 0, 'special' => 'warn'],
        'RETSC' => ['old' => '�������', 'new' => '�������', 'lv' => 0, 'special' => 'warn'],
        'RETSCSTR' => ['old' => '�������', 'new' => '�������', 'lv' => 0, 'special' => 'warn'],
        'RPODELAY' => ['old' => '�������� �� ���������� ��������', 'new' => '�������� �� ���������� ��������', 'lv' => 0],
        'SND' => ['old' => '��������', 'new' => '��������', 'lv' => 0],
        'SNDDELAY' => ['old' => '������ ������������  ��  ����������� �������� �� �������', 'new' => '������ ������������  ��  ����������� �������� �� �������', 'lv' => 0],
        'SNDZONE' => ['old' => '����������� �� ������� ����������', 'new' => '����������� �� ������� ����������', 'lv' => 0],
        'SNDZONE_T' => ['old' => '�������� ��������', 'new' => '�������� ��������', 'lv' => 0, 'special' => 'info'],
        'SRTRPOREG' => ['old' => '����������', 'new' => '����������', 'lv' => 0],
        'SRTSND' => ['old' => '�������� ���������� �� ����������', 'new' => '�������� �� ������� ����������', 'lv' => 0],
        'SRTSNDB' => ['old' => '�������� �� ��', 'new' => '�������� �� ��', 'lv' => 0],
        'SRTSNDB_UNDO' => ['old' => '������ ��������', 'new' => '������ ��������', 'lv' => 0],
        'SRTSNDIM' => ['old' => '�������� �� ��', 'new' => '�������� �� ��', 'lv' => 0],
        'SRTSNDIM_UNDO' => ['old' => '������ ��������', 'new' => '������ ��������', 'lv' => 0],
        'SRTSND_UNDO' => ['old' => '������ �������� ����������', 'new' => '������ �������� ����������', 'lv' => 0],
        'SRT_CUSTOM' => ['old' => '�������� �������', 'new' => '�������� �������', 'lv' => 0],
        'STR' => ['old' => '��������', 'new' => '������� �������', 'lv' => 0],
        'STRSC' => ['old' => '������� � ��������', 'new' => '������� �������', 'lv' => 0],
        'TRNRPO' => ['old' => '��������', 'new' => '��������', 'lv' => 0],
        'TRNSRT' => ['old' => '�������� ���������� � ���������', 'new' => '�������� ���������� � ���������', 'lv' => 0],
        'ATTACH' => ['new' => '����������', 'lv' => 0],

        'BOXISS' => ['old' => '�������� �� �/�', 'new' => '�������� �� �/�', 'hidden' => 1, 'lv' => 0],
        'DEL_STR' => ['old' => '�������� � ��������', 'new' => '�������� � ��������', 'hidden' => 1, 'lv' => 0],
        'DLV_POBOX_UNDO' => ['old' => '������ �������� � �/�', 'new' => '������ �������� � �/�', 'hidden' => 1, 'lv' => 0],
        'DLV_UNDO' => ['old' => '������ ��������', 'new' => '������ ��������', 'hidden' => 1, 'lv' => 0],
        'NONTRNOPS' => ['old' => '����������', 'new' => '����������', 'hidden' => 1, 'lv' => 0],
        'NONTRNSRT' => ['old' => '����������', 'new' => '����������', 'hidden' => 1, 'lv' => 0],
        'PRNNTC2_UNDO' => ['old' => '������  ��������', 'new' => '������  ��������', 'hidden' => 1, 'lv' => 0],
        'RDRSCSTR_UNDO' => ['old' => '������ ������ � �������� � ��', 'new' => '������ ������ � �������� � ��', 'hidden' => 1, 'lv' => 0],
        'RDRSC_UNDO' => ['old' => '������ ������ � ��', 'new' => '������ ������ � ��', 'hidden' => 1, 'lv' => 0],
        'RDR_UNDO' => ['old' => '������ ������', 'new' => '������ ������', 'hidden' => 1, 'lv' => 0],
        'REGPBT' => ['old' => '����������� �� �������', 'new' => '����������� �� �������', 'hidden' => 1, 'lv' => 0],
        'REGPBT_UNDO' => ['old' => '������ �����������', 'new' => '������ �����������', 'hidden' => 1, 'lv' => 0],
        'REGSRT' => ['old' => '����������� �� �������', 'new' => '����������� �� �������', 'hidden' => 1, 'lv' => 0],
        'REGSRT_UNDO' => ['old' => '������ �����������', 'new' => '������ �����������', 'hidden' => 1, 'lv' => 0],
        'RETSCSTR_UNDO' => ['old' => '������ �������� � �������� � ��', 'new' => '������ �������� � �������� � ��', 'hidden' => 1, 'lv' => 0],
        'RETSC_UNDO' => ['old' => '������ �������� � ��', 'new' => '������ �������� � ��', 'hidden' => 1, 'lv' => 0],
        'RET_UNDO' => ['old' => '������ ��������', 'new' => '������ ��������', 'hidden' => 1, 'lv' => 0],
        'RPODELAY_UNDO' => ['old' => '������ �������� ���', 'new' => '������ �������� ���', 'hidden' => 1, 'lv' => 0],
        'SNDDELAY_UNDO' => ['old' => '������ ������� ������������', 'new' => '������ ������� ������������', 'hidden' => 1, 'lv' => 0],
        'SNDZONE_T_UNDO' => ['old' => '������ ������� �� ������� ��', 'new' => '������ ������� �� ������� ��', 'hidden' => 1, 'lv' => 0],
        'SNDZONE_UNDO' => ['old' => '������ �������� � ���� ����������', 'new' => '������ �������� � ���� ����������', 'hidden' => 1, 'lv' => 0],
        'SRTRPOREG_UNDO' => ['old' => '������ �������� � ������� (���������)', 'new' => '������ �������� � ������� (���������)', 'hidden' => 1, 'lv' => 0],
        'SRT_CUSTOM_UNDO' => ['old' => '������ �������� �� ���������� ��������', 'new' => '������ �������� �� ���������� ��������', 'hidden' => 1, 'lv' => 0],
        'STRCUST' => ['old' => '�������� �� ��������', 'new' => '�������� �� ��������', 'hidden' => 1, 'lv' => 0],
        'STRCUST_UNDO' => ['old' => '������ �������� �� ��������', 'new' => '������ �������� �� ��������', 'hidden' => 1, 'lv' => 0],
        'TRN' => ['old' => '�������� ����������', 'new' => '�������� ����������', 'hidden' => 1, 'lv' => 0],
        'TRNBAG' => ['old' => '�������� �������', 'new' => '�������� �������', 'hidden' => 1, 'lv' => 0],
        'TRNSRT_UNDO' => ['old' => '������ ��������', 'new' => '������ ��������', 'hidden' => 1, 'lv' => 0],
        'TRN_UNDO' => ['old' => '������ �������� ����������', 'new' => '������ �������� ����������', 'hidden' => 1, 'lv' => 0],
    ];

    // $arr['ISSPAY']['new']='������� �������';
    // $arr['RET']['new']='������� �� �����';
    // $arr['RETSCSTR']['new']='������� (����� �������?)';
    // pre($arr2);
    return isset($arr[$status]) ? $arr[$status]['new'] : $status;
}

function kazpost_returns_paid_dates()
{
    $arr['waiting'] = query_arr('
		select orders.id,orders.track,kazpost_api.date,kazpost_api.status,kazpost_api.place
		from '.typetab('orders').'
		left join '.typetab('kazpost_api').' on kazpost_api.track=orders.track
		where orders.status=\'return\' and kazpost_api.status in (\'DLV\',\'STR\',\'STRSC\',\'TRNRPO\')
		order by kazpost_api.date
		;');

    $arr['return_dates'] = query_arr('select date(kazpost_api.date) as day,count(*) as q
		from '.tabname('shop', 'orders').'
		left join '.tabname('shop', 'kazpost_api').' on kazpost_api.track=orders.track
		where orders.status in (\'return paid\') and country=\'���������\'
		group by day
		order by day
		;');
    $list['return_dates'] = '';
    foreach ($arr['return_dates'] as $v) {
        $list['return_dates'] .= '<li><a href="/?page=admin/kazpost/returns/returns&amp;date='.(empty($v['day']) ? 'null' : $v['day']).'">'.(empty($v['day']) ? '��� ����' : $v['day']).'</a> '.$v['q'].' �������</li>';
    }
    $list['waiting'] = '';
    foreach ($arr['waiting'] as $v) {
        $list['waiting'] .= '<tr>
		<td>'.$v['date'].'</td>
		<td>
		<a href="/?page=admin/orders&status=all&order_id='.$v['id'].'">#'.$v['id'].'</a>
		<!--
		<button data-role="order_set_status" data-order_id="'.$v['id'].'" data-status="return paid" class="btn btn-default" title="������� �������">�������</button>
		-->
		</td>
		<td>'.kazpost_statuses($v['status']).'</td><td>'.$v['place'].'</td><td>'.order_items_list(['order_id' => $v['id']]).'</td></tr>';
    }
    temp_var('return_dates', $list['return_dates']);
    temp_var('waiting', $list['waiting']);

    return template('admin/kazpost/returns/block_return_dates');
}

function kazpost_returns_paid_detail()
{
    $date = empty($_GET['date']) ? error('�� �������� ����') : $_GET['date'];
    $mysql['nulldate'] = ($date == 'null') ? 'kazpost_api.date is null' : 0;
    $arr['orders'] = query_arr('select orders.id,orders.track,kazpost_api.date,kazpost_api.status,kazpost_api.place
		from '.tabname('shop', 'orders').'
		left join '.tabname('shop', 'kazpost_api').' on kazpost_api.track=orders.track
		where orders.status in (\'return paid\') and country=\'���������\' and (date(kazpost_api.date)=\''.$date.'\' or '.$mysql['nulldate'].')
		order by kazpost_api.date
		;');
    $arr['items'] = query_arr('
		select items.id,items.name,count(*) as q
		from '.typetab('packages').'
		join '.typetab('items').' on items.id=packages.item_id
		join '.typetab('orders').' on orders.id=packages.order_id
		left join '.tabname('shop', 'kazpost_api').' on kazpost_api.track=orders.track
		where orders.status in (\'return paid\') and country=\'���������\' and (date(kazpost_api.date)=\''.$date.'\' or '.$mysql['nulldate'].')
		group by items.id
		order by items.name
		;');
    $list['orders'] = '';
    foreach ($arr['orders'] as $v) {
        $list['orders'] .= '<tr><td>'.$v['date'].'</td><td><a href="/?page=admin/orders&status=all&order_id='.$v['id'].'">#'.$v['id'].'</a></td><td>'.kazpost_statuses($v['status']).'</td><td>'.$v['place'].'</td><td>'.order_items_list(['order_id' => $v['id']]).'</td></tr>';
    }
    $list['items'] = '';
    foreach ($arr['items'] as $v) {
        $list['items'] .= '<tr><td>'.$v['name'].'</td><td>'.$v['q'].'</td></tr>';
    }
    $content = '
	<h3>������� '.$date.'</h3>
	<div class="row">
	<div class="col-md-4">
	<h3>������</h3>
	<table class="table table-striped">'.$list['items'].'</table>
	</div>
	<div class="col-md-8">
	<h3>������</h3>
	<table class="table table-striped">'.$list['orders'].'</table>
	</div>
	</div>
	';

    return $content;
}

function kazpost_all_tracks_statuses()
{
    $arr = query_arr('select orders.id,orders.track,orders.status,kazpost_api.events
		from '.tabname('shop', 'orders').'
		left join '.tabname('shop', 'kazpost_api').' on kazpost_api.track=orders.track
		where orders.status in (\'sent\',\'delivered not paid\',\'return\',\'return delivered\',\'refund\')
		;');
    $list = '';
    foreach ($arr as $v) {
        $set = $v['status'];
        $track_info = unserialize($v['events']);
        if (empty($track_info)) {
            $list .= '<li>#'.$v['id'].' <a href="http://track.kazpost.kz/'.$v['track'].'">'.$v['track'].'</a> <b>��� ������</b></li>';

            continue;
        }
        $last = kazpost_last_status(['track' => $v['track'], 'events' => $track_info]);
        $status = $last['status'];
        // $status=$track_info['events'][1]['activity'][0]['status'][0];
        // if (in_array($status,array('STR','ISSPAY','STRSC','RET','RETSCSTR','ISSSC'))) {continue;};
        // pre($track_info);
        $is_return = false;
        $is_paid = false;
        $is_delivered = false;
        $is_paid_return = false;
        $is_paid_by_client = false;
        // pre($track_info);
        foreach ($track_info['events'] as $v2) {
            // pre($v2);
            foreach ($v2['activity'] as $v3) {
                // pre($v3);
                if (! isset($v3['status'])) {
                    continue;
                }
                if (in_array($v3['status'][0], ['RET', 'RETSC', 'RETSCSTR'])) {
                    $is_return = true;
                }
                if (in_array($v3['status'][0], ['DPAY', 'ISS', 'ISSSC', 'ISSPAY'])) {
                    $is_paid = true;
                }
                if (in_array($v3['status'][0], ['DLV', 'STR', 'STRSC', 'TRNRPO'])) {
                    $is_delivered = true;
                }
            }
        }
        if ($is_paid and $is_return) {
            $is_paid_return = true;
        }
        if ($is_paid and ! $is_return) {
            $is_paid_by_client = true;
        }
        if (in_array($v['status'], ['sent']) and $is_delivered) {
            $set = 'delivered not paid';
        }
        if (in_array($v['status'], ['sent', 'delivered not paid']) and $is_paid_by_client) {
            $set = 'take payment';
        }
        if (in_array($v['status'], ['sent', 'delivered not paid']) and $is_return) {
            $set = 'return';
        }
        // if (in_array($v['status'],array('sent','delivered not paid','return')) and $is_paid_return) {$set='return paid';};
        // ������� �������������� ������������ ���������� ���������
        // if (in_array($v['status'],array('return')) and $is_paid_return) {$set='return paid';};
        // if (in_array($v['status'],array('return')) and $is_delivered_return) {$set='return delivered';};
        // if (in_array($v['status'],array('paid','take payment','refund paid')) and !$is_paid_by_client) {$set='�������';};
        // if (in_array($v['status'],array('return paid')) and !$is_paid_return) {$set='�������';};
        if ($set != $v['status']) {
            // TODO �������� �� order_set_status
            // query('update '.tabname('shop','orders').' set status=\''.$set.'\' where id=\''.$v['id'].'\';');
            order_set_status(['order_id' => $v['id'], 'status' => $set]);
        }
        // ��������
        // if ($is_return) {continue;};
        $list .= '<li>#'.$v['id'].' <a href="http://track.kazpost.kz/'.$v['track'].'">'.$v['track'].'</a>
		'.$status.' ('.kazpost_statuses($status).')
		'.($is_delivered ? '<b>����������</b>' : '').'
		'.($is_return ? '<b>�������</b>' : '').' '.($is_paid ? '<b>�������</b>' : '').'
		'.($is_paid_return ? '<b>������� �������</b>' : '').' '.($is_paid_by_client ? '<b>������� �������</b>' : '').'
		'.(($set == $v['status']) ? $set : $v['status'].'-><b style="color: red;">'.$set.'</b>').'
		</li>';
    }

    return '<ol>'.$list.'</ol>';
}

function kazpost_update_tracks_events()
{
    $arr = query_arr('select SQL_CALC_FOUND_ROWS orders.id,orders.track from '.tabname('shop', 'orders').'
		left join '.tabname('shop', 'kazpost_api').' on kazpost_api.track=orders.track
		where
		(
		orders.status in (\'sent\',\'delivered not paid\',\'return\',\'refund\')
		or
		kazpost_api.track is null
		)
		/* orders.status in (\'return paid\',\'refund paid\',\'paid\',\'take payment\') */
		and orders.track is not null
		and country=\'���������\'
		and (kazpost_api.last_update is null or kazpost_api.last_update<date_sub(current_timestamp,interval 6 hour))
		order by kazpost_api.last_update
		limit 24
		;');
    $rows = rows_without_limit();
    $last = query_assoc('select max(last_update) as date from '.typetab('kazpost_api').';');
    $list = '';
    foreach ($arr as $v) {
        if (empty($v['track'])) {
            error('����������� ���� ��� ������ '.$v['id']);
        }
        $track_info = get_track_info_kz(['track' => $v['track'], 'from' => 'kazpost']);
        // pre(var_dump($track_info));
        if (! $track_info) {
            $list .= '<li>'.$v['track'].' ������</li>';

            continue;
        }
        /*
        $last=kazpost_last_status(array('track'=>$v['track'],'events'=>$track_info));
        // pre($last);
        $track_info_serialized=serialize($track_info);
        query('insert into '.typetab('kazpost_api').' set track=\''.$v['track'].'\', events=\''.$track_info_serialized.'\',
            date=\''.$last['date'].'\', zip=\''.$last['zip'].'\', place=\''.$last['place'].'\', status=\''.$last['status'].'\'
            on duplicate key update	events=\''.$track_info_serialized.'\',
            date=\''.$last['date'].'\', zip=\''.$last['zip'].'\', place=\''.$last['place'].'\', status=\''.$last['status'].'\'
            ;');
            */
        $list .= '<li>'.$v['track'].'</li>';
    }

    return '<p>��������: '.($rows).'</p><ol>'.$list.'</ol>'.(($rows == 0) ? '<p>��������� ����������: '.$last['date'].'</p>' : '');
}

// ���� ���������� ������ events, �� ����� ������, ���� ���, ����������� �� ��
function kazpost_last_status($args)
{
    $track_info = isset($args['events']) ? $args['events'] : get_track_info_kz(['track' => $args['track'], 'from' => 'db']);
    // pre($args);
    // pre($track_info['events'][0]['activity']);
    if (empty($track_info['events'][0]['activity']['0'])) {
        $i = 1;
    } else {
        $i = 0;
    }
    // pre($track_info);
    if (empty($track_info)) {
        return false;
    }
    $arr['date'] = date('Y-m-d H:i:s', strtotime($track_info['events'][$i]['date'].' '.$track_info['events'][$i]['activity'][0]['time']));
    $arr['zip'] = $track_info['events'][$i]['activity'][0]['zip'];
    $arr['place'] = $track_info['events'][$i]['activity'][0]['name'];
    $arr['status'] = $track_info['events'][$i]['activity'][0]['status'][0];

    // pre($arr);
    return $arr;
}

// pre(get_track_info_kz(array('track'=>'CC140932396KZ','from'=>'db')));
function get_track_info_kz($args)
{
    $track = empty($args['track']) ? error('�� ������� ����') : $args['track'];
    $from = isset($args['from']) ? $args['from'] : false;
    $proxy = isset($args['proxy']) ? $args['proxy'] : false;
    // ���� �� ������� from �� ������ ��� ������������. ���� ���������� ����� ����� ����� �����, �� ����������� �����, ���� ���, �� ����� �� ����
    if (! $from) {
        $arr = query_assoc('select last_update,datediff(current_timestamp,last_update) as datediff
			from '.typetab('kazpost_api').'
			where track=\''.$track.'\'
			;');
        if (! $arr) {
            $from = 'kazpost';
        } else {
            // ���� ���������� ������ �����, �� ����������� � ��������
            $from = (($arr['datediff'] > 0) ? 'kazpost' : 'db');
        }
    }
    // pre($from);
    if ($from == 'db') {
        $arr = query_assoc('select events from '.typetab('kazpost_api').' where track=\''.$track.'\';');
        // pre(unserialize($arr['events']));
        $track_info = unserialize($arr['events']);
        $track_info['saved'] = true;

        return $track_info;
    }
    if ($from == 'kazpost') {
        // ��������� �������� � ������ �����
        $page = filegetcontents_alt('http://track.kazpost.kz/api/v2/'.$track.'/events', ['proxy' => 'default_proxy', 'error' => 'no_error']);
        // �������� �� rnd ������
        if ($page == 'error') {
            return false;
        }
        // pre($page);
        $arr = json_decode($page, true);
        $arr['saved'] = false;
        // pre($arr);
        if (empty($arr['events'])) {
            return false;
            // error('�� ������� ���������� �� ����� '.$track);
        }
        foreach ($arr['events'] as $k => $v) {
            foreach ($v['activity'] as $k2 => $v2) {
                if (isset($v2['city'])) {
                    $arr['events'][$k]['activity'][$k2]['city'] = iconv('utf-8', 'windows-1251', $v2['city']);
                }
                if (isset($v2['name'])) {
                    $arr['events'][$k]['activity'][$k2]['name'] = iconv('utf-8', 'windows-1251', $v2['name']);
                }
            }
        }
        // ��������� �������� � ������� �����
        $page_info = filegetcontents_alt('http://track.kazpost.kz/api/v2/'.$track, ['proxy' => 'default_proxy', 'error' => 'no_error']);
        // pre($page_info);
        if ($page_info == 'error') {
            return false;
        }
        // ���� �� �������� ���� ����� � 500 ������� �� ����������
        if (strstr($page_info, 'Status: 500')) {
            $explode = explode(',Status: 500', $page_info);
            $page_info = $explode[0].'}';
        }
        $arr_info = json_decode($page_info, true);
        // pre($arr);
        if (empty($arr_info)) {
            return false;
        }
        // die(var_dump($arr));
        // ���������� ���������
        foreach ($arr_info as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if (! is_array($v2)) {
                        $arr_info[$k][$k2] = iconv('utf-8', 'windows-1251', $v2);
                    }
                }
            } else {
                $arr_info[$k] = iconv('utf-8', 'windows-1251', $v);
            }
        }
        $arr['info'] = $arr_info;
        // pre($arr);
        // ������� �� kazpost_update_tracks_events 2017-05-13 ���� ��� �� - ��������
        $last = kazpost_last_status(['track' => $track, 'events' => $arr]);
        // pre($last);
        $track_info_serialized = serialize($arr);
        // pre($track_info_serialized);
        query('insert into '.typetab('kazpost_api').' set track=\''.$track.'\', events=\''.mysql_real_escape_string($track_info_serialized).'\',
			date=\''.$last['date'].'\', zip=\''.$last['zip'].'\', place=\''.$last['place'].'\', status=\''.$last['status'].'\'
			on duplicate key update	events=\''.mysql_real_escape_string($track_info_serialized).'\',
			date=\''.$last['date'].'\', zip=\''.$last['zip'].'\', place=\''.$last['place'].'\', status=\''.$last['status'].'\'
			;');

        // �������
        return $arr;
    }
}

// get_track_info_kz(array('track'=>'CC141071596KZ'));

// pre(get_post_info_kz(array('zip'=>'110011')));
function get_post_info_kz($args)
{
    global $db;
    $engine_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];
    $zip = isset($args['zip']) ? $args['zip'] : error('�� ������� ������');
    // pre($zip);
    $arr = query_assoc('select result from '.tabname('shop', 'post_info').' where zip=\''.$zip.'\';');
    if ($arr) {
        $result['saved'] = true;
        $result['result'] = unserialize($arr['result']);

        return $result;
    }
    $step1 = file_get_contents('http://old.post.kz/ru/api/maps?mapscategory_id=&region_id=&city_id=&street=&code='.$zip.'&service=');
    $step1_arr = json_decode($step1, true);
    // pre(iconv('utf-8',$engine_encoding,$step1_arr['data'][0]['title']));
    // pre($step1_arr);
    $result['saved'] = false;
    $result['result'] = [];
    foreach ($step1_arr['data'] as $step1_v) {
        $step2 = file_get_contents('http://old.post.kz/ru/api/maps/info/'.$step1_v['id']);
        $step2_arr = json_decode($step2, true);
        // echo(pre_return($step2_arr));
        unset($step2_arr['services']);
        foreach ($step2_arr as $k => $v) {
            $step2_arr[$k] = iconv('utf-8', $engine_encoding, $v);
        }
        $result['result'][] = $step2_arr;
    }
    // pre($result);
    query('insert into '.tabname('shop', 'post_info').' set zip=\''.$zip.'\',result=\''.mysql_real_escape_string(serialize($result['result'])).'\';');

    return $result;
}

function kz_clean_address($address)
{
    $del = [
        '�������������� �������', '���������� �������', '������������ �������', '������ �.������', '����������� �������', '������ �.������',
        '��������-������������� �������', '������������ �������', '�������-������������� �������', '����-������������� �������',
        '���������� �������', '������������� �������', '����������� �������', '�������������� �������',
        '����������� �������', '������-������������� �������', '������������ �����',
    ];
    foreach ($del as $d) {
        // ��� ������ �� ���� �� �����, ��� ����� �������
        $address = str_replace($d.', ', '', $address);
        // ��� ������ �� �������
        $address = str_replace($d, '', $address);
        /*
        if (preg_match('/^[�-���-ߨ\.\s]$/u',iconv('windows-1251','utf-8',$address),$arr)) {
            pre($arr);
        };
        */
    }

    return trim($address);
}

function kz_post_info()
{
    $zip = empty($_GET['zip']) ? error('�� ������� ������') : $_GET['zip'];
    $track = empty($_GET['track']) ? error('�� ������� ����') : $_GET['track'];
    $addr_link = isset($_GET['addr_link']) ? $_GET['addr_link'] : error('�� ������� addr_link');
    $arr = get_post_info_kz(['zip' => $zip]);
    $track_info = get_track_info_kz(['track' => $track]);
    // pre($track_info);
    $post_info_alt = empty($track_info['info']['last']['address']) ? '���������� �� �������' : $track_info['info']['last']['address'];
    $saved_text = $arr['saved'] ? '��������� � ��:' : '��������� � ��������:';
    $list = '';
    if (empty($arr['result'])) {
        $list = '<li>'.$saved_text.' ���������� �� �������</li>';
    } else {
        foreach ($arr['result'] as $k => $v) {
            if ($addr_link == 1) {
                $short_address = kz_clean_address($v['address']);
            }
            // pre($short_address);
            $addr_text = ($addr_link == 1) ?
            str_replace($short_address, '', $v['address']).'<span class="js-link" data-role="get_post_address" data-element="message" data-n="'.$k.'" data-zip="'.$zip.'">'.$short_address.'</span>'
            : $v['address'];
            $list .= '<li>
			'.$addr_text.'
			'.(empty($v['phone']) ? '' : '('.$v['phone'].')').'
			<br/>'.$v['type'].' '.(empty($v['duty']) ? '' : '('.$v['duty'].')').'</li>';
        }
    }
    // pre($track);
    $content = $saved_text.'<ol>
	'.$list.'
	<li>��������������: '.$post_info_alt.'</li>
	</ol>
	';

    return $content;
}

function kz_track_info()
{
    $track = empty($_GET['track']) ? error('�� ������� ����') : $_GET['track'];
    $track_info = get_track_info_kz(['track' => $track]);
    $content = '
	<p>'.(($track_info['saved']) ? '�� ��' : '� ��������').'</p>
	<div style="width: 350px;">'.pre_return(isset($track_info['info']) ? $track_info['info'] : []).'</div>
	';

    return $content;
}

function kz_post_address()
{
    $zip = isset($_GET['zip']) ? $_GET['zip'] : error('�� ������� ������');
    $n = isset($_GET['n']) ? $_GET['n'] : error('�� ������� ����� ���������');
    $arr = get_post_info_kz(['zip' => $zip]);

    // pre($arr);
    return kz_clean_address($arr['result'][$n]['address']);
}

function admin_export_tracks_pages()
{
    $arr = query_assoc('select count(*) as q from '.tabname('shop', 'orders').'
		where status in (\'sent\',\'delivered not paid\',\'return\');');
    $per_page = 500;
    $pages = ceil($arr['q'] / $per_page);
    $list = '';
    for ($i = 1; $i <= $pages; $i++) {
        $list .= '<li><a href="/?page=admin/kazpost/export_tracks&amp;p='.$i.'">���� '.$i.'</a></li>';
    }

    return '<h3>������� ������</h3><ul>'.$list.'</ul>';
}

function admin_export_tracks_str()
{
    $type = empty($_GET['type']) ? 'all' : check_input($_GET['type']);
    $p = empty($_GET['p']) ? '1' : (int) $_GET['p'];
    $per_page = 500;
    $from = $per_page * ($p - 1);
    $status['all'] = '\'sent\',\'delivered not paid\',\'return\'';
    $status['delivered'] = '\'delivered not paid\'';
    $status['sent'] = '\'sent\'';
    $status['take_payment'] = '\'take payment\'';
    $status['paid'] = '\'paid\'';
    if (! isset($status[$type])) {
        error('��� ������ ����');
    }
    $arr = query_arr('select track from '.tabname('shop', 'orders').'
		where status in ('.$status[$type].')
		limit '.$from.','.$per_page.'
		;');
    $list = '';
    foreach ($arr as $k => $v) {
        if ($k != 0) {
            $list .= ', ';
        }
        $list .= $v['track'];
    }

    return $list;
}

function kazpost_submit_write_tracks()
{
    global $config;
    if (empty($_FILES['tracks']['tmp_name'])) {
        error('�� ������� ���� ������������');
    }
    $uploaded_file = $_FILES['tracks']['tmp_name'];
    $datetime = date('Y.m.d_H:i:s');
    $file = 'tracks'.$datetime.'.xlsx';
    $files_dir = $config['path']['server'].'types/'.$config['type'].'/files/';
    move_uploaded_file($uploaded_file, $files_dir.$file);
    $arr = get_array_from_xlsx(['file' => $files_dir.$file, 'temp_dir' => $files_dir]);
    unlink($files_dir.$file);
    $arr1 = $arr['sheet1.xml'];
    unset($arr1[0]);
    unset($arr1[1]);
    unset($arr1[2]);
    $q['inserted'] = 0;
    $q['replaced'] = 0;
    foreach ($arr1 as $v) {
        foreach ($v as $k1 => $v1) {
            $v[$k1] = iconv('utf-8', 'windows-1251', $v1);
        }
        query('replace into '.tabname('shop', 'kazpost').'
			(`1`,track,`3`,`4`,`5`,`6`,`7`,`8`,`9`,`10`,`11`,`12`,`13`,`14`,`15`,`16`,`zip`)
			values (\''.$v[0].'\',\''.$v[1].'\',\''.$v[2].'\',\''.$v[3].'\',\''.$v[4].'\',\''.$v[5].'\',
				\''.$v[6].'\',\''.$v[7].'\',\''.$v[8].'\',\''.$v[9].'\',\''.$v[10].'\',\''.$v[11].'\',
				\''.$v[12].'\',\''.$v[13].'\',\''.$v[14].'\',\''.$v[15].'\',\''.copybetwen('[', ']', $v[12]).'\')
			;');
        if (mysql_affected_rows() == 1) {
            $q['inserted']++;
        }
        if (mysql_affected_rows() == 2) {
            $q['replaced']++;
        }
    }
    shop_auto_statuses();
    alert('����� ���������. ��������: '.$q['replaced'].'. ���������: '.$q['inserted'].'. ������� ���������.');
}
