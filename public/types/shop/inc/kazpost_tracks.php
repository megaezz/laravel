<?php

namespace MegawebV1;

function kazpost_tracks_list()
{
    global $config;
    $files_dir = $config['path']['server'].'types/'.$config['type'].'/files/kazpost/tracks/';
    $selected_filename = empty($_GET['file']) ? false : urldecode($_GET['file']);
    $selected_file = $files_dir.$selected_filename;
    // $arr=list_directory_files($files_dir);
    $arr = query_arr('select file,date 
		from '.typetab('kazpost_track_reports').'
		order by date desc
		;');
    $list = '';
    foreach ($arr as $v) {
        $selected = ($v['file'] === $selected_filename) ? 'font-weight: bold;' : '';
        $list .= '<li><small>'.$v['date'].'</small> <a href="/?page=admin/kazpost/tracks&amp;file='.urlencode($v['file']).'" style="'.$selected.'">'.$v['file'].'</a></li>';
    }

    return $list;
}

function kazpost_submit_tracks_upload()
{
    global $config;
    $files_dir = $config['path']['server'].'types/'.$config['type'].'/files/kazpost/tracks/';
    $xls_files_dir = $files_dir.'xls/';
    $files_arr = empty($_FILES['file']['tmp_name']) ? error('�� �������� ����� ������') : $_FILES['file'];
    $q_files = count($files_arr['tmp_name']);
    for ($i = 0; $i < $q_files; $i++) {
        $uploaded_file = $files_arr['tmp_name'][$i];
        $uploaded_filename = $files_arr['name'][$i];
        $pathinfo = pathinfo($uploaded_filename);
        if ($pathinfo['extension'] === 'xlsx') {
            $dest_filename = $uploaded_filename;
            $dest_file = $files_dir.$dest_filename;
            if (! file_exists($dest_file)) {
                move_uploaded_file($uploaded_file, $dest_file);
            }
        }
        if ($pathinfo['extension'] === 'xls') {
            $source_file = $xls_files_dir.$uploaded_filename;
            $dest_filename = $uploaded_filename.'x';
            $dest_file = $files_dir.$dest_filename;
            // ���� ������ ����� ���, �� �������� ���� � ���������� �������
            if (! file_exists($dest_file)) {
                move_uploaded_file($uploaded_file, $source_file);
                // ������������ � xlsx
                // pre($saved_file_xlsx);
                convertio(['from' => $source_file, 'to' => $dest_file, 'ext' => 'xlsx']);
            }
        }
        // ���������� ����������� ���� � ������� �������
        query('insert into '.typetab('kazpost_track_reports').' set file=\''.$dest_filename.'\', date=current_date;');
    }
    // $uploaded_file=empty($_FILES['file']['tmp_name'])?error('�� ������� ���� �������'):$_FILES['file']['tmp_name'];
    // $uploaded_filename=$_FILES['file']['name'];
    // $saved_file=$files_dir.$uploaded_filename;
    redirect('/?page=admin/kazpost/tracks&file='.$dest_filename);
}

function kazpost_tracks_result()
{
    global $config;
    $login = auth_login(['type' => 'shop']);
    $files_dir = $config['path']['server'].'types/'.$config['type'].'/files/kazpost/tracks/';
    $rus['statuses'] = shop_translation('order_status');
    $selected_filename = empty($_GET['file']) ? false : $_GET['file'];
    if (! $selected_filename) {
        return;
    }
    $selected_file = $files_dir.$selected_filename;
    if (! file_exists($selected_file)) {
        error('���� �� ����������');
    }
    $arr = query_assoc('select file,date from '.typetab('kazpost_track_reports').'
		where file=\''.$selected_filename.'\';');
    if (! $arr) {
        error('���� �� ������� � ����');
    }
    $arr = get_array_from_xlsx(['file' => $selected_file, 'temp_dir' => $files_dir.'temp/']);
    // pre($arr);
    $list = '';
    foreach ($arr as $sheet) {
        $result = false;
        foreach ($sheet as $row) {
            $track = isset($row[4]) ? $row[4] : false;
            if (! $track) {
                continue;
            }
            // ���� ��� ����� ����, �� ��������
            if (preg_match('/^([a-zA-Z]{2})([0-9]{9})([a-zA-Z]{2})$/', $row[4])) {
                $weight = isset($row[5]) ? $row[5] : '';
                // ���������� ���
                query('update '.typetab('kazpost_api').' set weight=\''.$weight.'\' where track=\''.$track.'\';');
                $order_id = isset($row[8]) ? $row[8] : false;
                if (! $order_id) {
                    continue;
                }
                $order['old'] = query_assoc('select * from '.typetab('orders').' where id=\''.$order_id.'\';');
                if ($order['old']) {
                    $old_track = empty($order['old']['track']) ? false : $order['old']['track'];
                    // ���� �� ��� ������� ����, �� ���������� ���, ���� ���, �� ������� ��������������
                    if (! $old_track) {
                        // ���������� ����
                        query('update '.typetab('orders').' set track=\''.$track.'\' where id=\''.$order_id.'\';');
                        log_changed_fields(['change_log' => typetab('orders_changes'), 'table' => typetab('orders'), 'primary_key' => $order_id,
                            'old' => $order['old'], 'login' => $login]);
                        // ������ ������ �� "���������"
                        if (in_array($order['old']['status'], ['check address', 'waiting send', 'packed'])) {
                            order_set_status(['order_id' => $order_id, 'status' => 'sent']);
                            $result = '�������� ���� '.$track.' ���������� ������ "sent"';
                        } else {
                            $result = '�������� ���� '.$track.'<span>�� ������ ������, �.�. ������� ������: '.$order['old']['status'].'</span>';
                        }
                    } else {
                        if ($old_track === $track) {
                            $result = $track.' ���� ��� �������';
                        } else {
                            $result = '<span style="color: red;">����� ����� ����: '.$old_track.', � � ����� ����: '.$track.'</span>';
                        }
                    }
                } else {
                    $result = '<span style="color: red;">����� �� ������</span>';
                }
                $list .= '<li><a href="/?page=admin/orders&amp;status=all&amp;order_id='.$order_id.'">#'.$order_id.'</a> '.$result.'</li>';
            }
        }
    }

    return $list;
}
