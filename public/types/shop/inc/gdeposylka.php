<?php

namespace MegawebV1;

function test_add_track()
{
    // pre('ffff');
    shop_add_track_to_gdeposylka('RC140334910KZ');
}

function test_add_many_tracks()
{
    // $tracks=array('RC140350912KZ','RC140334910KZ','RC140350912KZ');
    $arr = query_arr('select track from '.tabname('shop', 'orders').' where status=\'sent\' and track is not null;');
    foreach ($arr as $v) {
        shop_add_track_to_gdeposylka($v['track']);
    }
}

function gdeposylka_add_new()
{
    $gdeposylka_apikey = value_from_config('shop', 'gdeposylka_apikey');
    $client = new GdePosylka\Client\Client($gdeposylka_apikey);
    $trackList = $client->getTrackingList('default');
    foreach ($trackList->getTrackings() as $trackInfo) {
        $tracks[$trackInfo->getTrackingNumber()] = true;
    }
    $arr = query_arr('select track from '.tabname('shop', 'orders').' where status=\'delivered not paid\' and track is not null;');
    // pre(count($tracks));
    // pre($tracks);
    $q = 0;
    $max = 100;
    foreach ($arr as $v) {
        if ($q >= $max) {
            break;
        }
        if (! isset($tracks[$v['track']])) {
            shop_add_track_to_gdeposylka($v['track']);
            $q++;
        }
    }

    return '���������: '.$q.'. ��������: '.count($arr).' '.count($tracks).' '.$q.' '.(count($arr) - count($tracks) - $q).'.';
}

// �������� ���� �� ������������
function shop_add_track_to_gdeposylka($trackingNumber)
{
    if (empty($trackingNumber)) {
        error('�� ������� ����');
    }
    $gdeposylka_apikey = value_from_config('shop', 'gdeposylka_apikey');
    $client = new GdePosylka\Client\Client($gdeposylka_apikey);
    $couriersResponse = $client->detectCourier($trackingNumber)->getCouriers();
    if ($couriersResponse == null) {
        error('������ ����������� ��������� ������� ��� ����� '.$trackingNumber);
    }
    $courierSlug = $couriersResponse[0]->getSlug();
    $track = $client->addTracking($courierSlug, $trackingNumber);
    // echo '�������� ���� �� ������������. �����: '.var_dump($track).'<br>';
    logger()->info('��������� ������������ ������� '.$trackingNumber);
    // return '��������� ������������ ������� '.$trackingNumber;
}

// �������� ���������� ������� ������������ �������, ���������� � ����� ��������� �������, ��������� �� ������������ ����� �����
function shop_cron_gdeposylka()
{
    global $db;
    $engine_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];
    $gdeposylka_apikey = value_from_config('shop', 'gdeposylka_apikey');
    // $gdeposylka_url='http://gdeposylka.ru/ws/x1/tracks.list/json/?apikey='.$gdeposylka_apikey;
    // $gdeposylka_json=file_get_contents($gdeposylka_url);
    // $gdeposylka_obj=json_decode($gdeposylka_json);
    $client = new GdePosylka\Client\Client($gdeposylka_apikey);
    $trackList = $client->getTrackingList('default');
    // pre($trackList);
    $i = 0;
    $archived = 0;
    foreach ($trackList->getTrackings() as $trackInfo) {
        $message = '';
        $track = iconv('utf-8', $engine_encoding, $trackInfo->getTrackingNumber());
        $gdeposylka_tracks[$track] = true; // ���������� ������ ������������� ������
        if ($trackInfo->getLastCheckpoint()) {
            $checkpoint = $trackInfo->getLastCheckpoint();
            $message = iconv('utf-8', $engine_encoding, $checkpoint->getMessage());
        }
        query('insert into '.tabname('shop', 'gdeposylka').' set track=\''.$track.'\', message=\''.$message.'\'
			on duplicate key update message=\''.$message.'\';');
        $i = $i + mysql_affected_rows();
        // ��������� � ����� ��������� �������
        if (strpos($message, '�������� ��������') !== false) {
            $track = $client->archiveTracking($trackInfo->getCourierSlug(), $trackInfo->getTrackingNumber());
            $archived++;
        }
    }
    /*
    foreach ($gdeposylka_obj->tracks as $track=>$obj) {
        $message=iconv('utf-8',$engine_encoding,$obj->message);
        $track=iconv('utf-8',$engine_encoding,$track);
        $gdeposylka_tracks[$track]=true; // �������� ������ ������������� ������
        query('insert into '.tabname('shop','gdeposylka').' set track=\''.$track.'\', message=\''.$message.'\'
            on duplicate key update message=\''.$message.'\';');
        $i=$i+mysql_affected_rows();
        //��������� � ����� ��������� �������
        if (strpos($message,'�������� ��������')!==false) {
            file_get_contents('http://gdeposylka.ru/ws/x1/track.archive/json?apikey='.$gdeposylka_apikey.'&id='.$track);
            $archived++;
        };
    };
    */
    shop_auto_statuses(); // ��������� �������
    logger()->info('��������� ����� gdeposylka. ��������� '.$i.' �����. ��������� � �����: '.$archived.'.');
    // ��������� ����� ����� �� ������������
    $arr = query_arr('select track from '.tabname('shop', 'orders').' where track is not null;');
    // ���� ������-�� ����� ��� � ������ �������������, �� ��������� ���
    foreach ($arr as $v) {
        if (! isset($gdeposylka_tracks[$v['track']])) {
            shop_add_track_to_gdeposylka($v['track']);
        }
    }
}

// ��������� ������������ ����� ����� ������ "sent" � track �� null � ���� ����������� � �������
// shop_add_tracks_to_gdeposylka($tracks); //��������� ������� �� ������������
/*
function shop_add_tracks_to_gdeposylka() {
    $gdeposylka_apikey=value_from_config('shop','gdeposylka_apikey');
    $gdeposylka_url='http://gdeposylka.ru/ws/x1/tracks.list/json/?apikey='.$gdeposylka_apikey;
    $gdeposylka_json=file_get_contents($gdeposylka_url);
    $gdeposylka_obj=json_decode($gdeposylka_json);
    // �������� ������ ������������� ������
    foreach ($gdeposylka_obj->tracks as $track=>$obj) {
        $gdeposylka_tracks[$track]=true;
    };
    $arr=query_arr('select track from '.tabname('shop','orders').' where status=\'sent\' and track is not null;');
    // ���� ����� ��� � ������ �������������, �� ��������� ���
    foreach ($arr as $v) {
        if (!isset($gdeposylka_tracks[$v['track']])) {
            file_get_contents('http://gdeposylka.ru/ws/x1/track.add/json/?apikey='.$gdeposylka_apikey.'&id='.$v['track']);
            logger()->info('��������� ������������ ������� '.$v['track']);
        };
    };
}
*/
