<?php

namespace MegawebV1;

// ������ ��� API
function api_error($args = [])
{
    $text = empty($args['text']) ? error('�� ������� ����� ������') : check_input($args['text']);
    logger()->info($text);
    $answer['status'] = false;
    $answer['msg'] = iconv('windows-1251', 'utf-8', $text);
    // pre($answer);
    exit(json_encode($answer));
}

// ������������� ������������� ������ ��� �������������
function api_generate_tokens()
{
    $arr = query_arr('select id,login,pass from '.typetab('users').' where token is null;');
    foreach ($arr as $v) {
        $token = sha1(mt_rand().$v['login'].$v['pass']);
        query('update '.typetab('users').' set token=\''.$token.'\' where id=\''.$v['id'].'\';');
    }
    alert('������� ������������� API ������');
}

function api_status_format($our_status)
{
    if (empty($our_status)) {
        error('�� ������� ������');
    }
    if (in_array($our_status, ['new', 'unavailable'])) {
        $status = 'new';
    }
    if (in_array($our_status, ['canceled'])) {
        $status = 'canceled';
    }
    if (! in_array($our_status, ['new', 'unavailable', 'canceled'])) {
        $status = 'approved';
    }

    // if (in_array($our_status,array('paid','take payment'))) {$status=2;};
    // if (in_array($our_status,array('return','return paid','refund','refund paid'))) {$status=3;};
    return $status;
}

// ������ ������� POST[ids]=1,2,3,4,5
function api_get_order_status()
{
    // ��� �����
    // $_POST['ids']='83230,83231,83232,83387';
    $ids = empty($_GET['ids']) ? (empty($_POST['ids']) ? api_error(['text' => '�� �������� ID �������']) : check_input($_POST['ids'])) : check_input($_GET['ids']);
    $token = empty($_GET['token']) ? (empty($_POST['token']) ? api_error(['text' => '�� ������� �����']) : check_input($_POST['token'])) : check_input($_GET['token']);
    $arr_ids = explode(',', $ids);
    $arr = query_arr('
		select orders.id,orders.user_info,orders.user_status
		from '.typetab('orders').'
		join '.typetab('users').' on users.login=orders.advertiser
		where
		users.token=\''.$token.'\'
		and
		orders.id in ('.array_to_str($arr_ids).')
		;');
    // pre($arr);
    $answer = [];
    foreach ($arr as $v) {
        // ���� ���������� ���������������� ������ ���� �� ������ � shave=1
        // if ($v['shave'] and !empty($v['user_status'])) {$v['status']=$v['user_status'];};
        $status = false;
        $status = api_status_format($v['user_status']);
        $return['status'] = $status;
        $return['orderid'] = (int) $v['id'];
        $return['comment'] = iconv('windows-1251', 'utf-8', $v['user_info']);
        $answer[] = $return;
    }
    $answer = ['status' => true, 'result' => $answer];

    // pre($answer);
    // ����� ������� ����� �� ������������
    // return json_encode($answer,JSON_UNESCAPED_UNICODE);
    return json_encode($answer);
}

function submit_order_api()
{
    // logger()->info('TEST');
    $result = submit_order_result();
    if (isset($result['msg'])) {
        $result['msg'] = iconv('windows-1251', 'utf-8', $result['msg']);
    }

    return json_encode($result);
}

function api_edit_package_item_price()
{
    // pre($_GET);
    $order_id = empty($_POST['order_id']) ? error('�� ����� ID ������') : check_input($_POST['order_id']);
    $item_id = empty($_POST['item_id']) ? error('�� ����� ID ������') : check_input($_POST['item_id']);
    $cost_kzt = empty($_POST['cost_kzt']) ? error('�� �������� ��������� KZT') : check_input($_POST['cost_kzt']);

    return edit_package_item_price(['order_id' => $order_id, 'item_id' => $item_id, 'cost_kzt' => $cost_kzt]);
}

function api_add_set_to_order()
{
    $args['order_id'] = empty($_GET['order_id']) ? error('�� ����� ID ������') : check_input($_GET['order_id']);
    $args['set_id'] = empty($_GET['set_id']) ? error('�� ����� ID ������') : check_input($_GET['set_id']);
    $args['q'] = empty($_GET['q']) ? 1 : check_input($_GET['q']);

    return add_set_to_order(['order_id' => $args['order_id'], 'set_id' => $args['set_id'], 'q' => $args['q']]);
}

function api_add_item_to_order()
{
    $args['order_id'] = empty($_GET['order_id']) ? error('�� ����� ID ������') : check_input($_GET['order_id']);
    $args['item_id'] = empty($_GET['item_id']) ? error('�� ����� ID ������') : check_input($_GET['item_id']);
    $args['q'] = empty($_GET['q']) ? 1 : check_input($_GET['q']);

    return add_item_to_order(['order_id' => $args['order_id'], 'item_id' => $args['item_id'], 'q' => $args['q']]);
}
