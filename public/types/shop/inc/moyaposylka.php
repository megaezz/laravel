<?php

namespace MegawebV1;

function mp_callback()
{
    if (! isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
        error('���������� ���� �������');
    }
    $arr = json_decode($GLOBALS['HTTP_RAW_POST_DATA'], true);
    if (! is_array($arr)) {
        error('�� ������� ������ ������ �������');
    }
    // logger()->info(pre_return($arr));
    // �.�. callback �������� ������ ��������� ������, � ��� ����� ������ ������, ����������� ���
    $trackinfo = mp_request(['type' => 'user-tracker', 'method' => 'getUserTrackerStatuses', 'params' => ['number' => $arr['number']]]);
    if (! isset($trackinfo['result'])) {
        error('��� ���������� getUserTrackerStatuses �� ����� '.$arr['number']);
    }
    mp_update_track($trackinfo['result']);

    return 'MoyaPosylka';
}

function mp_update_track($arr)
{
    global $db;
    $engine_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];
    if (! isset($arr['statuses'][0])) {
        logger()->info('��� �������� �� ����� '.$arr['number']);

        return;
    }
    // ���� � ������ �������� ���� �������, �� ������ ������ "�������"
    foreach ($arr['statuses'] as $s) {
        $status_name = iconv('utf-8', $engine_encoding, $s['operation']['name']);
        if ($status_name == '�������') {
            query('update '.tabname('shop', 'orders').' set status=\'return\'
				where track=\''.$arr['number'].'\' and status not in (\'return\',\'return paid\');');
            if (mysql_affected_rows() != 0) {
                logger()->info('���������� ������� ��� '.$arr['number']);
            }
        }
    }
    $last_status = $arr['statuses'][0];
    $statuses = iconv('utf-8', 'windows-1251', serialize($arr['statuses']));
    $last_status['place'] = iconv('utf-8', $engine_encoding, $last_status['place']);
    $last_status['operation']['name'] = iconv('utf-8', $engine_encoding, $last_status['operation']['name']);
    query('insert into '.tabname('shop', 'moyaposylka').' set
		track=\''.$arr['number'].'\',
		message=\''.$last_status['operation']['name'].'\',
		place=\''.$last_status['place'].'\',
		status_date=\''.$last_status['date'].'\',
		mp_last_update=\''.$arr['lastUpdatedAt'].'\',
		zip=\''.$last_status['zipCode'].'\'
		on duplicate key update
		message=\''.$last_status['operation']['name'].'\',
		place=\''.$last_status['place'].'\',
		status_date=\''.$last_status['date'].'\',
		mp_last_update=\''.$arr['lastUpdatedAt'].'\',
		zip=\''.$last_status['zipCode'].'\',
		statuses=\''.mysql_real_escape_string($statuses).'\'
		;');
    logger()->info('Moyaposylka: �������� ���������� �� ����� '.$arr['number']);
    // ��������� � ����� ��������� �������
    if (strpos($last_status['operation']['name'], '�������') !== false) {
        $result = mp_request(['type' => 'user-tracker', 'method' => 'moveToArchive', 'params' => ['number' => $arr['number']]]);
        // $archived++;
    }
}

function mp_test()
{
    global $db;
    $engine_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];
    $arr = ['RC140410956KZ', 'RC140350740KZ', 'RC140350767KZ', 'RC140334923KZ'];
    $list = '';
    foreach ($arr as $v) {
        $trackinfo = mp_request(['type' => 'user-tracker', 'method' => 'getUserTrackerStatuses', 'params' => ['number' => $v]]);
        pre($trackinfo);
        foreach ($trackinfo['result']['statuses'] as $s) {
            $s['operation']['name'] = iconv('utf-8', $engine_encoding, $s['operation']['name']);
            echo pre_return($s);
            // if (strpos($s['operation']['name'],'�������')!==false) {
            if ($s['operation']['name'] == '�������') {
                exit('������� 1');
                // $arr=query_assoc('select id,status from '.tabname('shop','orders').' where track=\''.$v['number'].'\';');
                // if ($arr['status']!='') {};
                // logger()->info('���������� ������ �������');
                // query('update '.tabname('shop','orders').' set status=\'refund\' where track=\''.$v['number'].'\';');
            } else {
                echo $s['operation']['name'].'!=�������';
            }
        }
        $list .= pre_return($trackinfo);
    }

    return $list;
}

// �������� ���������� ������� ������������ �������, ���������� � ����� ��������� �������, ��������� �� ������������ ����� �����
function shop_cron_moyaposylka()
{
    global $db;
    $engine_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];
    $result = mp_request(['type' => 'user-tracker', 'method' => 'getUserTrackersLite']);
    // ����� ����� ��������� ����� ���������
    $arr_tracks = readdata(query('select track,mp_last_update from '.tabname('shop', 'moyaposylka').';'), 'track');
    // pre($arr_tracks);
    $i = 0;
    $archived = 0;
    $moyaposylka_tracks = [];
    foreach ($result['result'] as $v) {
        // ���������� ������ ������������� ������
        $moyaposylka_tracks[$v['number']] = true;
        /*
        if (isset($arr_tracks[$v['number']]['mp_last_update']) and $v['lastUpdatedAt']==$arr_tracks[$v['number']]['mp_last_update']) {
            logger()->info('������� �� ��������, ���������� ���� '.$v['number']);
            continue;
        };
        // �������� ������� �����
        $trackinfo=mp_request(array('type'=>'user-tracker','method'=>'getUserTrackerStatuses','params'=>array('number'=>$v['number'])));
        if (!isset($trackinfo['result'])) {
            logger()->info('��� ���������� getUserTrackerStatuses �� ����� '.$v['number']);
            continue;
        };
        mp_update_track($trackinfo['result']);
        $i=$i+mysql_affected_rows();
        */
    }
    // shop_auto_statuses(); //��������� �������
    // logger()->info('��������� ����� moyaposylka. ��������� '.$i.' �����. ��������� � �����: '.$archived.'.');
    // logger()->info('��������� ����� moyaposylka. ��������� '.$i.' �����.');
    // ��������� ����� ����� �� ������������
    $arr = query_arr('select track from '.tabname('shop', 'orders').' where status in (\'sent\',\'delivered not paid\',\'return\') and track is not null;');
    // pre($arr);
    // ���� ������-�� ����� ��� � ������ �������������, �� ��������� ���
    foreach ($arr as $v) {
        if (! isset($moyaposylka_tracks[$v['track']])) {
            moyaposylka_add_track(['track' => $v['track']]);
        }
    }
}

function moyaposylka_add_track($args)
{
    if (empty($args['track'])) {
        error('�� ������� track');
    }
    $result = mp_request(['type' => 'user-tracker', 'method' => 'add', 'params' => ['number' => $args['track']]]);
    // pre($result);
    logger()->info('��������� ������������ ������� '.$args['track']);
}

function mp_request($args)
{
    global $db;
    if (empty($args['type'])) {
        error('�� ������� ��� api');
    }
    if (empty($args['method'])) {
        error('�� �������� �������� method');
    }
    // if (!isset($args['params']) or !is_array($args['params'])) {error('�� ������� ������ params');};
    $engine_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];
    $apikey = value_from_config('shop', 'moyaposylka_apikey');
    $opts['http']['method'] = 'POST';
    $opts['http']['header'] = 'X-Api-Key: '.$apikey."\r\n".'Content-Type: application/x-www-form-urlencoded';
    $opts['http']['content'] = json_encode($args);
    $opts['http']['timeout'] = 60;
    $opts['http']['ignore_errors'] = true;
    // pre($opts);
    $context = stream_context_create($opts);
    $content = file_get_contents('https://moyaposylka.ru/apps/'.$args['type'].'/v2', false, $context);
    // $content=iconv('utf-8',$engine_encoding,$content);
    // pre($content);
    $result = json_decode($content, true);

    // pre($result);
    // pre($result['result'][0]['readable']);
    // pre(iconv('utf-8',$engine_encoding,$result['result'][0]['readable']));
    return $result;
}
