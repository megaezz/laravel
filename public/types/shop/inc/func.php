<?php

namespace MegawebV1;

function admin_ems_check()
{
    $arr = cache_query_arr('
		select orders.id as order_id,ka.track,ka.sent_date,ka.events,ka.weight
		from '.typetab('kazpost_api').' ka
		join '.typetab('orders').' on ka.track=orders.track
		where
		ka.track like \'EC%\' and
		ka.sent_date between \'2017-06-01\' and \'2017-07-31\'
		;');
    $list = '';
    $i = 0;
    $deliveries = 0;
    $insurances = 0;
    $taxes = 0;
    foreach ($arr as $v) {
        $i++;
        $events = unserialize($v['events']);
        $weight_post = empty($v['weight']) ? false : $v['weight'];
        $weight_track = empty($events['info']['weight']) ? false : $events['info']['weight'];
        if ($weight_track == '����� 1') {
            $weight_track = 0.5;
        }
        if ($weight_track == '����� 1') {
            $weight_track = 1;
        }
        if ($weight_track == '����� 2') {
            $weight_track = 2;
        }
        $weight = $weight_post ? $weight_post : $weight_track;
        $delivery = 0;
        if ($weight < 1) {
            $delivery = 700;
        }
        if ($weight >= 1 and $weight < 1.5) {
            $delivery = 1100;
        }
        if ($weight >= 1.5 and $weight < 2) {
            $delivery = 1400;
        }
        if ($weight >= 2 and $weight < 2.5) {
            $delivery = 1600;
        }
        if ($weight >= 2.5 and $weight < 3) {
            $delivery = 1800;
        }
        if ($weight >= 3 and $weight < 3.5) {
            $delivery = 2000;
        }
        if ($weight >= 3.5 and $weight < 4) {
            $delivery = 2200;
        }
        $info = preparing_order_info(['order_id' => $v['order_id']]);
        $insurance = $info['cost_kzt'] * 0.01;
        $tax = in_array($info['status'], ['paid', 'take payment']) ? ($info['cost_kzt'] * 0.02) : 0;
        // if (!isset($weights[$weight])) {$weights[$weight]=0;};
        // $weights[$weight]++;
        // pre($events);
        $deliveries = $deliveries + $delivery;
        $insurances = $insurances + $insurance;
        $taxes = $taxes + $tax;
        $list .= '<tr>
		<td>'.$i.'</td>
		<td>'.$v['track'].'</td>
		<td>'.$weight.'</td>
		<td>'.$delivery.'</td>
		<td>'.$info['cost_kzt'].'</td>
		<td>'.$insurance.'</td>
		<td>'.$tax.'</td>
		</tr>
		';
    }
    $list .= '
	<tr>
		<td><b>�����</b></td>
		<td></td>
		<td></td>
		<td>'.$deliveries.'</td>
		<td></td>
		<td>'.$insurances.'</td>
		<td>'.$taxes.'</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td><b>� ������</b></td>
		<td>'.($deliveries + $insurances + $taxes).' �����</td>
	</tr>
	';

    // pre($weights);
    return $list;
}

function test_spam_umseller()
{
    $url = 'http://o.umseller.com/handler.php';
    $p['key'] = 'dfb84a11f431c62436cfb760e30a34fe';
    $p['features']['price'] = '35000 ��.';
    $p['features']['qty'] = '1';
    $p['features']['id'] = 'um_iphone_6s';

    $names['man'] = ['����', '��������', '�����', '�����', '��������', '���������', '������', '������', '�����', '�����', '�����'];
    $names['woman'] = ['�������', '������', '�����', '�����', '������', '�������', '�����', '�������', '��������', '������'];
    $last_names['man'] = ['���������', '���������', '�������', '������������', '���������', '��������', '����������', '������������', '��������', '�����������'];
    $last_names['woman'] = ['��������', '���������', '���������', '��������', '�������������', '���������', '��������', '������������', '�������', '����������'];
    $genders = ['man', 'woman'];
    $gender = rnd_from_array($genders);
    $name = rnd_from_array($names[$gender]);
    $last_name = rnd_from_array($last_names[$gender]);
    $p['name_first'] = $name.' '.$last_name;
    $p['phone'] = '87'.mt_rand(100000000, 999999999);
    pre($p);
}

function list_categories()
{
    return get_list_categories([
        'current_group_id' => empty($_GET['group_id']) ? false : checkstr($_GET['group_id']),
        'public' => 'public',
        'type' => 'group',
        'order' => 'order by item_groups.position',
        'template' => 'landings/main/list_category',
    ]);
}

function list_categories_pic()
{
    return get_list_categories([
        'current_group_id' => empty($_GET['group_id']) ? false : checkstr($_GET['group_id']),
        'public' => 'public',
        'type' => 'group',
        'order' => 'order by item_groups.position',
        'template' => 'landings/main/list_category_pic',
    ]);
}

function list_categories2()
{
    return get_list_categories([
        'current_group_id' => empty($_GET['group_id']) ? false : checkstr($_GET['group_id']),
        'public' => 'public',
        'type' => 'group',
        'order' => 'order by item_groups.position',
        'template' => 'landings/main2/list_category',
    ]);
}

function list_categories3()
{
    return get_list_categories([
        'current_group_id' => empty($_GET['group_id']) ? false : checkstr($_GET['group_id']),
        'public' => 'public',
        'type' => 'group',
        'order' => 'order by item_groups.position',
        'template' => 'landings/main3/list_category',
    ]);
}

function list_categories3ru()
{
    return get_list_categories([
        'current_group_id' => empty($_GET['group_id']) ? false : checkstr($_GET['group_id']),
        'public' => 'public',
        'type' => 'group',
        'order' => 'order by item_groups.position',
        'template' => 'landings/main3ru/list_category',
    ]);
}

function list_categories_pic2()
{
    return get_list_categories([
        'current_group_id' => empty($_GET['group_id']) ? false : checkstr($_GET['group_id']),
        'public' => 'public',
        'type' => 'group',
        'order' => 'order by item_groups.position',
        'template' => 'landings/main2/list_category_pic',
    ]);
}

function get_list_categories($args)
{
    $current_group_id = empty($args['current_group_id']) ? false : $args['current_group_id'];
    $args['public'] = isset($args['public']) ? $args['public'] : 'public';
    $args['type'] = isset($args['type']) ? $args['type'] : 'group';
    $args['template'] = empty($args['template']) ? error('�� ������� ������ ������') : $args['template'];
    $args['order'] = empty($args['order']) ? false : $args['order'];
    $sql['order'] = $args['order'] ? $args['order'] : '';
    switch ($args['public']) {
        case 'public': $sql['public'] = 'public';
            break;
        case 'not public': $sql['public'] = 'not public';
            break;
        case 'all': $sql['public'] = '1';
            break;
        default: error('�� ��������� public');
    }
    switch ($args['type']) {
        case 'group': $sql['type'] = 'type=\'group\'';
            break;
        case 'offer': $sql['type'] = 'type=\'offer\'';
            break;
        case 'all': $sql['type'] = '1';
            break;
        default: error('�� ��������� type');
    }
    $arr = query_arr('
		select id,name,pic
		from '.typetab('item_groups').'
		where 1 and '.$sql['type'].' and '.$sql['public'].'
		'.$sql['order'].'
		;');
    $list = '';
    foreach ($arr as $v) {
        temp_var('class_active', ($current_group_id === $v['id']) ? 'active' : '');
        temp_var('id', $v['id']);
        temp_var('name', $v['name']);
        temp_var('pic', $v['pic'] ? $v['pic'] : '');
        $list .= template($args['template']);
        clear_temp_vars();
    }

    return $list;
}

// ���� ��� ���������� �������� ������� � ������������ ��� ����� ��� ������
function shop_cron_update_user_statuses_and_info()
{
    query('update '.typetab('orders').' set user_status=status,user_info=info where not shave;');
    $rows = mysql_affected_rows();
    logger()->info('SHOP: ��������� ������� � ����������� ��� ������. ��������� �����: '.$rows);
}

function widget_buy_kz_list_1251($price_function = 'item_price_kzt')
{
    return iconv('utf-8', 'windows-1251', widget_buy_kz_list($price_function));
}

// ����� ������� �������� set_price_kzt ��� ��������� ������ ��� random_price (��� ������)
function widget_buy_kz_list($price_function = 'item_price_kzt')
{
    $prices = ['10999', '14900', '8990', '6999', '7990', '17999', '16990', '11990', '5999', '13990'];
    $qs = [1, 1, 2];
    $genders = ['man', 'woman'];
    $cities = ['������', '������', '���������', '������', '�����', '��������', '����-�����������', '�����', '���������', '�������', '��������', '������', '�������������', '�����', '��������', '��������', '���������', '���������', '�����������', '������', '��������'];
    $names['man'] = ['����', '��������', '�����', '�����', '��������', '���������', '������', '������', '�����', '�����', '�����'];
    $names['woman'] = ['�������', '������', '�����', '�����', '������', '�������', '�����', '�������', '��������', '������'];
    $last_names['man'] = ['���������', '���������', '�������', '������������', '���������', '��������', '����������', '������������', '��������', '�����������'];
    $last_names['woman'] = ['��������', '���������', '���������', '��������', '�������������', '���������', '��������', '������������', '�������', '����������'];
    $endings['man'] = '';
    $endings['woman'] = '�';
    $list = '';
    for ($i = 0; $i <= 20; $i++) {
        $q = rnd_from_array($qs);
        $price = (($price_function === 'random_price') ? rnd_from_array($prices) : $price_function()) * $q;
        $city = rnd_from_array($cities);
        $gender = rnd_from_array($genders);
        $ending = $endings[$gender];
        $name = rnd_from_array($names[$gender]);
        $last_name = rnd_from_array($last_names[$gender]);
        $list .= '
		<div class="yved">
		<div>'.$last_name.' '.$name.',<br>�.'.$city.', ������'.$ending.' ����� �� ����� '.$price.' �����,<br>����������: '.$q.' ��.</div>
		</div>
		';
    }
    temp_var('list', $list);
    $content = template('landings/main/widgets/buy/block');

    return iconv('windows-1251', 'utf-8', $content);
}

// ���������� �� get ������� ���������� � cookies.
function set_cookies_from_get()
{
    $advertiser = return_advertiser_common();
    if ($advertiser) {
        $arr = query_arr('select var from '.typetab('advertisers_vars').' where login=\''.$advertiser.'\';');
    } else {
        $arr = [];
    }
    $arr[]['var'] = 'utm_source';
    $arr[]['var'] = 'utm_campaign';
    $arr[]['var'] = 'utm_medium';
    $arr[]['var'] = 'utm_content';
    $arr[]['var'] = 'uid';
    // pre($arr);
    foreach ($arr as $v) {
        if (isset($_GET[$v['var']])) {
            setcookie($v['var'], check_input($_GET[$v['var']]), time() + 3600 * 24, '/');
        }
    }
}

function user_metrika_code()
{
    $advertiser = return_advertiser_common();
    $arr = query_assoc('select metrika,fbpixel,scripts from '.typetab('users').' where login=\''.$advertiser.'\';');
    // pre($arr);
    $content = '';
    $list = '';
    if (! empty($arr['metrika'])) {
        $explode = explode(',', $arr['metrika']);
        foreach ($explode as $metrika_id) {
            temp_var('id', $metrika_id);
            $list .= template('user_analytics/metrika');
            clear_temp_vars();
        }
    }
    if (! empty($arr['fbpixel'])) {
        temp_var('id', $arr['fbpixel']);
        $list .= template('user_analytics/fbpixel');
        clear_temp_vars();
    }
    $content = $list.'
		'.$arr['scripts'];

    return $content;
}

function return_from_base_to_file()
{
    global $config;
    $returns_dir = $config['path']['server'].'types/'.$config['type'].'/files/returns/';
    $arr = query_arr('select id,text from '.typetab('returns_old').' where not transfered order by date desc limit 20;');
    // pre($arr);
    $imgs = [];
    $ru_to_file = [
        '���������� �������������' => 'id',
        '���������� ��������' => 'bill',
        '���������� �������' => 'box',
        '���������� ������ 1' => 'item_1',
        '���������� ������ 2' => 'item_2',
        '���������� ������ 3' => 'item_3',
    ];
    $list = '';
    // pre($ru_to_file);
    foreach ($arr as $v) {
        $list .= '<li>'.$v['id'].'</li>';
        $return_id = $v['id'];
        if (! file_exists($returns_dir.$return_id.'/')) {
            mkdir($returns_dir.$return_id.'/');
        }
        // if (preg_match('/<img(.*?)>/',$v['text'],$t)) {
        if (preg_match_all('/<label>(.*?)<\/label>
				<div><img src="data:image\/png;base64,(.*)" \/>/', $v['text'], $args)
        ) {
            // pre($args);
            // $imgs[]=$args;
            unset($args[0]);
            $sql = [];
            foreach ($args[1] as $k => $ru) {
                // $imgs[$v['id']][$ru_to_file[$ru]]=str_replace(' ', '+',$args[2][$k]);
                $type = $ru_to_file[$ru];
                $filename = $returns_dir.$return_id.'/'.$type.'.png';
                if (! file_exists($filename)) {
                    $data = base64_decode(str_replace(' ', '+', $args[2][$k]));
                    if (empty($data)) {
                        continue;
                    }
                    if (! file_put_contents($filename, $data)) {
                        error('������ ������ '.$filename);
                    }
                    $sql[$type] = 1;
                    $list .= '<li>'.$filename.'</li>';
                }
            }
        }
        /*
        query('update '.typetab('returns_new').' set
            transfered=1,
            photo_id=\''.(empty($sql['id'])?0:1).'\',
            photo_bill=\''.(empty($sql['bill'])?0:1).'\',
            photo_box=\''.(empty($sql['box'])?0:1).'\',
            photo_item_1=\''.(empty($sql['item_1'])?0:1).'\',
            photo_item_2=\''.(empty($sql['item_2'])?0:1).'\',
            photo_item_3=\''.(empty($sql['item_3'])?0:1).'\'
            where id=\''.$return_id.'\';');
            */
        query('update '.typetab('returns_old').' set
			transfered=1 where id=\''.$return_id.'\';');

    }

    return '<ul>'.$list.'</ul>';
}

function submit_return_form()
{
    global $config;
    // $names=array('id','bill','box','item_1','item_2','item_3');
    $returns_dir = $config['path']['server'].'types/'.$config['type'].'/files/returns/';
    // pre($HTTP_POST_VARS);
    $post['name'] = empty($_POST['name']) ? error('�� ������� ���') : checkstr($_POST['name']);
    $post['phone'] = empty($_POST['phone']) ? error('�� ������ �������') : checkstr($_POST['phone']);
    $post['item'] = empty($_POST['item']) ? error('�� ������� �������� ������') : checkstr($_POST['item']);
    $post['reason'] = empty($_POST['reason']) ? error('�� ������� ������� ��������') : checkstr($_POST['reason']);
    query('insert into '.tabname('shop', 'returns').' set
		name=\''.$post['name'].'\',
		phone=\''.$post['phone'].'\',
		item=\''.$post['item'].'\',
		reason=\''.$post['reason'].'\'
		;');
    $return_id = mysql_insert_id();
    $return_dir = $returns_dir.$return_id.'/';
    mkdir($return_dir);
    // error('�� ��������� ���� �������������')
    // error('�� ��������� ���� ��������')
    // error('�� ��������� ���� �������')
    $photo['id'] = empty($_FILES['photo_id']['tmp_name']) ? false : $_FILES['photo_id'];
    $photo['bill'] = empty($_FILES['photo_bill']['tmp_name']) ? false : $_FILES['photo_bill'];
    $photo['box'] = empty($_FILES['photo_box']['tmp_name']) ? false : $_FILES['photo_box'];
    $photo['item_1'] = empty($_FILES['photo_item_1']['tmp_name']) ? false : $_FILES['photo_item_1'];
    $photo['item_2'] = empty($_FILES['photo_item_2']['tmp_name']) ? false : $_FILES['photo_item_2'];
    $photo['item_3'] = empty($_FILES['photo_item_3']['tmp_name']) ? false : $_FILES['photo_item_3'];
    foreach ($photo as $k => $v) {
        if (! $v) {
            continue;
        }
        $fullname = 'photo_'.$k;
        $dest_src = $return_dir.$k.'.png';
        $img_ext = check_ext(['filename' => $v['name'], 'whitelist' => ['jpg', 'jpeg', 'png']]);
        if (! $img_ext) {
            error('������ ����������� �� �������������� (������ jpg,jpeg,png)');
        }
        $resized_img_id = resize_image($v['tmp_name'], $dest_src);
        query('update '.typetab('returns').' set '.$fullname.'=1 where id=\''.$return_id.'\';');
    }
}

// ������� �����
function submit_return_to_new()
{
    global $config;
    // $names=array('id','bill','box','item_1','item_2','item_3');
    $returns_dir = $config['path']['server'].'types/'.$config['type'].'/files/returns/';
    // pre($HTTP_POST_VARS);
    $post['name'] = empty($_POST['name']) ? '' : checkstr($_POST['name']);
    $post['phone'] = empty($_POST['phone']) ? '' : checkstr($_POST['phone']);
    $post['item'] = empty($_POST['item']) ? '' : checkstr($_POST['item']);
    $post['reason'] = empty($_POST['reason']) ? '' : checkstr($_POST['reason']);
    $post['return_id'] = empty($_POST['return_id']) ? '' : checkstr($_POST['return_id']);
    if (empty($post['return_id'])) {
        error('�� ������� return_id');
    }
    query('update '.tabname('shop', 'returns_new').' set
		name=\''.$post['name'].'\',
		phone=\''.$post['phone'].'\',
		item=\''.$post['item'].'\',
		reason=\''.$post['reason'].'\',
		transfered2=1
		where id=\''.$post['return_id'].'\'
		;');
    alert('#'.$post['return_id'].' <a href="/?page=admin/returns/requests">�������</a>');
}

function submit_return_form_old()
{
    global $config;
    $names = ['id', 'bill', 'box', 'item_1', 'item_2', 'item_3'];
    foreach ($names as $name) {
        $fullname = 'photo_'.$name;
        if (empty($_FILES[$fullname]['name'])) {
            // $images[$fullname]=false;
            temp_var($fullname, '���');

            // $vars[$fullname]='';
            continue;
        }
        // pre($_FILES);
        $img_ext = check_ext(['filename' => $_FILES[$fullname]['name'], 'whitelist' => ['jpg', 'jpeg', 'png']]);
        if (! $img_ext) {
            error('������ �� ��������������');
        }
        $img_base64 = resize_image_and_convert_to_base64($_FILES[$fullname]['tmp_name']);
        temp_var($fullname, '<img src="'.$img_base64.'" />');
        // $vars[$fullname]=$img_base64;
    }
    temp_var('name', $_POST['name']);
    temp_var('phone', $_POST['phone']);
    temp_var('item', $_POST['item']);
    temp_var('reason', $_POST['reason']);
    $content = template('admin/returns/filled_form');
    query('insert into '.tabname('shop', 'returns').' set text=\''.mysql_real_escape_string($content).'\';');
    // send_mail('mas90@mail.ru',$config['domain'],'megaezh@me.com','������� ������',$content);
    // return $content;
}

function client_add_item_to_order()
{
    $order_id = isset($_GET['order_id']) ? check_input($_GET['order_id']) : error('�� ������� ID ������');
    $item_id = isset($_GET['item_id']) ? check_input($_GET['item_id']) : error('�� ������� ID ������');
    $ip = request()->ip() ?? error('�� ������� IP');
    $order_info = preparing_order_info(['order_id' => $order_id]);
    // pre($order_info);
    if ($order_info['ip'] != $ip) {
        error('�� �� ������ �������� ����� � ������');
    }
    add_item_to_order(['order_id' => $order_id, 'item_id' => $item_id]);
    // plus_minus_items_in_package(array('order_id'=>$order_id,'item_id'=>$item_id,'action'=>'plus'));
}

function landings_index()
{
    return list_landings(['utm_content' => 'index page']);
}

function landings_index2()
{
    return list_landings([
        'utm_content' => 'index page',
        'template' => 'landings/main2/list_item',
        'template_form_order' => 'landings/main2/block_form_order',
    ]);
}

function landings_index3()
{
    return list_landings([
        'utm_content' => 'index page',
        'template' => 'landings/main3/list_item',
        'template_form_order' => 'landings/main3/block_form_order',
        'template_add_to_order' => 'landings/main3/add_to_order',
        'add_to_order' => false,
    ]);
}

function landings_index3ru()
{
    return list_landings([
        'utm_content' => 'index page',
        'template' => 'landings/main3ru/list_item',
        'template_form_order' => 'landings/main3ru/block_form_order',
        'template_add_to_order' => 'landings/main3ru/add_to_order',
        'add_to_order' => false,
    ]);
}

function landings_ok()
{
    $same_item = empty($_POST['item_id']) ? false : check_input($_POST['item_id']);

    // if (!$same_item) {
    // if ($)
    // };
    return list_landings(['utm_content' => 'ok page', 'add_to_order' => true, 'same_item' => $same_item]);
    // return list_landings(array('utm_content'=>'ok page','add_to_order'=>true));
}

function same_items($q = 3)
{
    /*
    $dir=empty($_GET['utm_content'])?false:check_input($_GET['utm_content']);
    $same_item=false;
    if ($dir) {
        $arr=query_assoc('select item_id
            from '.typetab('landings').'
            where dir=\''.$dir.'\'
            ;');
        $item_id=empty($arr['item_id'])?false:$arr['item_id'];
    };
    */
    return list_landings(['q' => $q, 'order' => 'order by rand()', 'utm_content' => 'same block']);
}

function watches_items()
{
    // return list_landings(array('item_type'=>'watches','utm_content'=>'watches page'));
    return list_landings(['group_id' => 13, 'utm_content' => 'watches']);
}

function watches_items_fake2()
{
    // return list_landings(array('item_type'=>'watches','utm_content'=>'watches page'));
    return list_landings(['group_id' => 27, 'utm_content' => 'purse-watches2', 'template' => 'landings/purse-watches2/list_item_fake']);
}

function watches_items_fake()
{
    return list_landings(['group_id' => 13, 'utm_content' => 'watches fake page', 'template' => 'landings/watches3/list_item_fake']);
}

function phones_items()
{
    return list_landings(['group_id' => 9, 'utm_content' => 'phones']);
}

function watches_items_stark()
{
    // return list_landings(array('item_type'=>'watches','utm_content'=>'watches page'));
    return list_landings(['group_id' => 13, 'utm_content' => 'watches2', 'order' => 'order by position_stark', 'item_type' => 'with position_stark']);
}

function watches_items_stark2()
{
    // return list_landings(array('item_type'=>'watches','utm_content'=>'watches page'));
    return list_landings(['group_id' => 13, 'utm_content' => 'watches-st2', 'order' => 'order by -position_stark2 desc']);
}

function women_watches_items()
{
    return list_landings(['group_id' => 10, 'utm_content' => 'women-watches']);
}

function women_items()
{
    return list_landings(['group_id' => 25, 'utm_content' => 'women-items']);
}

function purse_items()
{
    return list_landings(['group_id' => 11, 'utm_content' => 'purse']);
}

// ���� ����� �� ������������
function landings_items_group($group_id = false)
{
    if (! $group_id) {
        error('�� ������� group_id');
    }

    return list_landings(['group_id' => $group_id]);
}

function landings_index_categories()
{
    return list_landings(['utm_content' => 'index page', 'template' => 'landings/main/list_item_categories']);
}

function list_landings($args = [])
{
    return get_list_landings([
        'first_landings' => isset($args['first_landing']) ? $args['first_landing'] : (empty($_GET['first']) ? false : check_input($_GET['first'])),
        'country' => isset($args['country']) ? $args['country'] : (empty($_GET['country']) ? '���������' : check_input(mb_convert_encoding($_GET['country'], 'windows-1251'))),
        'group_id' => isset($args['group_id']) ? $args['group_id'] : (empty($_GET['group_id']) ? false : check_input($_GET['group_id'])),
        'hide_landing' => isset($args['hide_landing']) ? $args['hide_landing'] : (empty($_GET['hide_landing']) ? false : check_input($_GET['hide_landing'])),
        'only_with_item_id' => true,
        'order' => isset($args['order']) ? $args['order'] : 'order by landings.position',
        'show_advertisers' => 'all',
        'utm_content' => isset($args['utm_content']) ? $args['utm_content'] : false,
        'q' => isset($args['q']) ? $args['q'] : false,
        'item_type' => isset($args['item_type']) ? $args['item_type'] : false,
        'template' => isset($args['template']) ? $args['template'] : 'landings/main/list_item',
        'add_to_order' => isset($args['add_to_order']) ? $args['add_to_order'] : false,
        'template_add_to_order' => isset($args['template_add_to_order']) ? $args['template_add_to_order'] : 'landings/main/add_to_order',
        'same_item' => isset($args['same_item']) ? $args['same_item'] : false,
        'template_form_order' => isset($args['template_form_order']) ? $args['template_form_order'] : 'landings/main/block_form_order',
    ]);
}

function get_list_landings($args = [])
{
    $args['template'] = isset($args['template']) ? $args['template'] : error('�� ������� ������');
    $args['template_form_order'] = isset($args['template_form_order']) ? $args['template_form_order'] : false;
    $args['first_landings'] = empty($args['first_landings']) ? [] : explode(',', $args['first_landings']);
    $args['country'] = empty($args['country']) ? false : $args['country'];
    $args['group_id'] = empty($args['group_id']) ? false : $args['group_id'];
    $args['hide_landing'] = empty($args['hide_landing']) ? false : $args['hide_landing'];
    $args['only_with_item_id'] = isset($args['only_with_item_id']) ? $args['only_with_item_id'] : error('�� ������� ����� ����� ������');
    $args['show_index'] = isset($args['show_index']) ? $args['show_index'] : 'show_index';
    $args['show_advertisers'] = isset($args['show_advertisers']) ? $args['show_advertisers'] : 'show_advertisers';
    $args['order'] = empty($args['order']) ? false : $args['order'];
    $args['q'] = isset($args['q']) ? $args['q'] : false;
    $args['utm_content'] = empty($args['utm_content']) ? false : $args['utm_content'];
    $args['item_type'] = empty($args['item_type']) ? false : $args['item_type'];
    $args['add_to_order'] = isset($args['add_to_order']) ? $args['add_to_order'] : false;
    $args['template_add_to_order'] = isset($args['template_add_to_order']) ? $args['template_add_to_order'] : false;
    switch ($args['show_index']) {
        case 'show_index': $mysql['show_index'] = 'show_index';
            break;
        case 'not show_index': $mysql['show_index'] = 'not show_index';
            break;
        case 'all': $mysql['show_index'] = '1';
            break;
        default: error('�� ��������� show_index');
    }
    switch ($args['show_advertisers']) {
        case 'show_advertisers': $mysql['show_advertisers'] = 'show_advertisers';
            break;
        case 'not show_advertisers': $mysql['show_advertisers'] = 'not show_advertisers';
            break;
        case 'all': $mysql['show_advertisers'] = '1';
            break;
        default: error('�� ��������� show_advertisers');
    }
    switch ($args['item_type']) {
        case 'with position_stark': $mysql['item_type'] = 'landings.position_stark is not null';
            break;
        case 'with position_stark2': $mysql['item_type'] = 'landings.position_stark2 is not null';
            break;
        default: $mysql['item_type'] = '1';
    }
    $args['same_item'] = empty($args['same_item']) ? false : check_input($args['same_item']);
    // pre($args);
    // $first_arr=explode(',',$args['first_landing']);
    $mysql['limit'] = $args['q'] ? 'limit '.(int) $args['q'] : '';
    $mysql['order'] = $args['order'] ? $args['order'] : '';
    $mysql['hide_landing'] = $args['hide_landing'] ? 'landings.dir not in (\''.$args['hide_landing'].'\')' : '1';
    $mysql['country'] = $args['country'] ? 'landings.country=\''.$args['country'].'\'' : '1';
    $mysql['only_with_item_id'] = $args['only_with_item_id'] ? '' : 'left';
    if ($args['group_id']) {
        $mysql['group_id'] = 'items.id in (select item_id from '.typetab('items_groups').' where group_id=\''.$args['group_id'].'\')';
    } else {
        $mysql['group_id'] = '1';
    }
    // �������� ��� ������ � ���������� � ������� ���� ��������� ����� � ������� �������� � ����� ��������
    // ������� order=same_items.q ����� �������� ����� �������� �� �������� ���-�� ���������� ������� ������� � ����������
    $mysql['same_item'] = $args['same_item'] ? '
	left join
	(
	select item_id,count(*) as q
	from '.typetab('items_groups').'
	where group_id in (select group_id from '.typetab('items_groups').' where item_id=\''.$args['same_item'].'\')
	and item_id!=\''.$args['same_item'].'\'
	group by item_id
	) as same_items on same_items.item_id=landings.item_id
	' : '';
    $mysql['order'] = $args['same_item'] ? 'order by same_items.q desc' : $mysql['order'];
    $utm_content = $args['utm_content'] ? $args['utm_content'] : '';
    if ($utm_content == 'same block') {
        $form_target = 'target="_blank"';
    } else {
        $form_target = '';
    }
    $arr = query_arr('select landings.id,landings.item_id,landings.dir,landings.name,landings.pic,landings.description,
		landings.items_group,landings.show_index,landings.show_advertisers,landings.set_id,
		items.cost_kzt,items.cost_rur,landings.cost_kzt as landing_cost_kzt,sets.name as set_name,items.name as item_name
		from '.tabname('shop', 'landings').'
		'.$mysql['same_item'].'
		'.$mysql['only_with_item_id'].' join '.tabname('shop', 'items').' on items.id=landings.item_id
		left join '.typetab('sets').' on sets.id=landings.set_id
		where '.$mysql['show_index'].' and '.$mysql['show_advertisers'].' and '.$mysql['hide_landing'].'
		and '.$mysql['item_type'].' and '.$mysql['group_id'].'
		and '.$mysql['country'].'
		'.$mysql['order'].'
		'.$mysql['limit'].'
		;');
    $list = '';
    $first_landings = '';
    foreach ($arr as $v) {
        if ($v['landing_cost_kzt']) {
            $cost_kzt = $v['landing_cost_kzt'];
        } else {
            $cost_kzt = $v['cost_kzt'];
        }
        temp_var('item_id', $v['item_id'] ? $v['item_id'] : '');
        temp_var('id', $v['id']);
        temp_var('item_name', $v['item_name'] ? $v['item_name'] : '');
        temp_var('set_name', $v['set_name'] ? $v['set_name'] : '');
        temp_var('form_target', $form_target);
        temp_var('item_id_icon', $v['item_id'] ? '#'.$v['item_id'] : '<span class="glyphicon glyphicon-remove"></span>');
        temp_var('items_group', $v['items_group']);
        temp_var('set_id_icon', $v['set_id'] ? '#'.$v['set_id'] : '<span class="glyphicon glyphicon-remove"></span>');
        temp_var('show_index', '<span class="glyphicon glyphicon-'.($v['show_index'] ? 'ok' : 'remove').'"></span>');
        temp_var('show_advertisers', '<span class="glyphicon glyphicon-'.($v['show_advertisers'] ? 'ok' : 'remove').'"></span>');
        temp_var('utm_content', $utm_content);
        temp_var('dir', $v['dir']);
        temp_var('pic', $v['pic'] ? $v['pic'] : 'no-image-placeholder.png');
        temp_var('name', $v['name']);
        temp_var('description', $v['description']);
        temp_var('cost_kzt', $cost_kzt);
        temp_var('cost_rur', empty($v['cost_rur']) ? '?' : $v['cost_rur']);
        if ($args['template_add_to_order']) {
            temp_var('add_to_order', $args['add_to_order'] ? template($args['template_add_to_order']) : '');
        }
        if ($args['template_form_order']) {
            temp_var('form_order', $args['add_to_order'] ? '' : template($args['template_form_order']));
        }
        $content = template($args['template']);
        clear_temp_vars();
        foreach ($args['first_landings'] as $v2) {
            if ($v['dir'] === $v2) {
                $first_landings .= $content;
            }
        }
        $list .= $content;
    }

    return $first_landings.$list;
}

function list_landings_old($args = [])
{
    $mysql['limit'] = isset($args['q']) ? 'limit '.(int) $args['q'] : '';
    $mysql['order'] = isset($args['order']) ? $args['order'] : 'order by landings.position';
    $mysql['hide_landing'] = isset($_POST['hide_landing']) ? 'landings.dir not in (\''.check_input($_POST['hide_landing']).'\')' : '1';
    $mysql['item_type'] = '1';
    $mysql['group_id'] = '1';
    if (isset($args['item_type'])) {
        if ($args['item_type'] == 'watches') {
            $mysql['item_type'] = 'items.name like \'%����%\'';
        }
        if ($args['item_type'] == 'with position_stark') {
            $mysql['item_type'] = 'landings.position_stark is not null';
        }
    }
    if (isset($args['group_id'])) {
        $mysql['group_id'] = 'items.id in (select item_id from '.typetab('items_groups').' where group_id='.$args['group_id'].')';
    }
    $is_ok_page = isset($_POST['hide_landing']) ? true : false;
    $utm_content = isset($args['utm_content']) ? $args['utm_content'] : '';
    if ($utm_content == 'same block') {
        $form_target = 'target="_blank"';
    } else {
        $form_target = '';
    }
    $arr = query_arr('select landings.item_id,landings.dir,landings.name,landings.pic,landings.description,
		items.cost_kzt,landings.cost_kzt as landing_cost_kzt
		from '.tabname('shop', 'landings').'
		join '.tabname('shop', 'items').' on items.id=landings.item_id
		where landings.show_index and '.$mysql['hide_landing'].' and '.$mysql['item_type'].' and '.$mysql['group_id'].'
		'.$mysql['order'].'
		'.$mysql['limit'].'
		;');
    $list = '';
    foreach ($arr as $v) {
        if ($v['landing_cost_kzt']) {
            $cost_kzt = $v['landing_cost_kzt'];
        } else {
            $cost_kzt = $v['cost_kzt'];
        }
        $form = '';
        $add_to_order = '';
        /*
        if ($is_ok_page) {
            $add_to_order='
            <a class="btn btn-primary" data-role="client_add_item_to_order"
            data-order_id="'.created_order_id().'" data-item_id="'.$v['item_id'].'"
            data-loading-text="����������...">��������</a>';
        } else {
            */
        $form = '
			<form class="form-inline order" '.$form_target.' action="/?page=landings/ok/index" onsubmit="return check_phone(this);" method="post">
			<div class="form-group">
			<!-- <input type="hidden" name="name" value="����� � �������" class="form-control"> -->
			<input type="text" name="phone" placeholder="�������" class="form-control">
			<input type="hidden" name="item_id" value="'.$v['item_id'].'" class="form-control">
			<input type="hidden" name="utm_content" value="'.$utm_content.'" class="form-control">
			</div>
			<button type="submit" class="btn btn-primary">��������</button>
			</form>
			';
        /*
        };
        */
        $list .= '
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="thumbnail">
				<a href="/'.$v['dir'].'" target="_blank">
					<img src="/types/shop/template/landings/main/photos/'.$v['pic'].'" style="height: 350px; width: auto;">
					</a>
					<div class="circle"><span class="price">'.$cost_kzt.'</span> <span class="currency">���.</span></div>
					<div class="caption">
						<h3>'.$v['name'].'</h3>
						<p class="description">'.$v['description'].'</p>
						'.$form.'
						<div class="btn-group btn-group-justified" role="group">
						'.$add_to_order.'
						<a href="/'.$v['dir'].'" target="_blank" class="btn btn-success" role="button">���������</a>
						</div>
					</div>
				</div>
			</div>
		';
    }

    return $list;
}

function authorizate_shop_redirect_advertiser()
{
    authorizate_shop_all_users();
    $arr = preparing_user_info(['type' => 'shop', 'login' => auth_login(['type' => 'shop'])]);
    // pre($arr);
    if ($arr['level'] === 'advertiser') {
        redirect('/?page=admin/advertiser/stat');
    }
}

function authorizate_shop_all_users()
{
    // authorizate('shop',tabname('shop','users'),array('admin','manager','packer','packer_lite','caller','advertiser'));
    authorizate_alt(['type' => 'shop', 'levels' => ['admin', 'manager', 'packer', 'packer_lite', 'caller', 'advertiser', 'worker']]);
}

function authorizate_shop_advertiser()
{
    // authorizate('shop',tabname('shop','users'),array('admin','manager','advertiser'));
    authorizate_alt(['type' => 'shop', 'levels' => ['admin', 'manager', 'advertiser']]);
}

function authorizate_shop_caller()
{
    // authorizate('shop',tabname('shop','users'),array('admin','manager','packer','packer_lite','caller'));
    authorizate_alt(['type' => 'shop', 'levels' => ['admin', 'manager', 'packer', 'packer_lite', 'caller', 'worker']]);
}

function authorizate_shop_packer()
{
    // authorizate('shop',tabname('shop','users'),array('admin','manager','packer','packer_lite'));
    authorizate_alt(['type' => 'shop', 'levels' => ['admin', 'manager', 'packer', 'packer_lite']]);
}

function authorizate_shop_packer_lite()
{
    // authorizate('shop',tabname('shop','users'),array('admin','manager','packer','packer_lite'));
    authorizate_alt(['type' => 'shop', 'levels' => ['admin', 'manager', 'packer', 'packer_lite']]);
}

function authorizate_shop_manager()
{
    // authorizate('shop',tabname('shop','users'),array('admin','manager'));
    authorizate_alt(['type' => 'shop', 'levels' => ['admin', 'manager']]);
}

function authorizate_shop_admin()
{
    // authorizate('shop',tabname('shop','users'),array('admin'));
    authorizate_alt(['type' => 'shop', 'levels' => ['admin']]);
}

function authorizate_shop_worker()
{
    authorizate_alt(['type' => 'shop', 'levels' => ['admin', 'worker']]);
}

function authorizate_shop_logout()
{
    // authorizate_logout('shop');
    authorizate_logout(['type' => 'shop']);
}

function authorizate_shop_logout_redirect()
{
    // authorizate_logout_redirect(array('type'=>'shop'));
    authorizate_logout_redirect_alt(['type' => 'shop']);
}

function change_order_cost($arr)
{
    if (! isset($arr['order_id'],$arr['item_id'],$arr['action'])) {
        error('�� ������� ID ������ ��� ������ ��� action');
    }
    $item_info = preparing_item_info(['id' => $arr['item_id']]);
    if ($arr['action'] == 'plus') {
        query('update '.tabname('shop', 'orders').'
			set cost_rur=cost_rur+'.$item_info['cost_rur'].', cost_kzt=cost_kzt+'.$item_info['cost_kzt'].'
			where id=\''.$arr['order_id'].'\';');
    }
    if ($arr['action'] == 'minus') {
        query('update '.tabname('shop', 'orders').'
			set cost_rur=cost_rur-'.$item_info['cost_rur'].', cost_kzt=cost_kzt-'.$item_info['cost_kzt'].'
			where id=\''.$arr['order_id'].'\';');
    }

    return '�������� ��������� ��� ������ #'.$arr['order_id'];
}

function landings_list()
{
    $arr = query_arr('select dir,item_id from '.tabname('shop', 'landings').' order by dir;');
    $list = '';
    foreach ($arr as $v) {
        $item_info = preparing_item_info(['id' => $v['item_id']]);
        $list .= '<li><a class="pointer" data-role="show_landing" data-dir="'.$v['dir'].'">'.$v['dir'].'</a></li>';
    }
    // $content='<ul>'.$list.'</ul>';
    $content = $list;

    return $content;
}

function preparing_item_info($args)
{
    if (! empty($args['dir'])) {
        $arr = query_assoc('select item_id from '.typetab('landings').' where dir=\''.$args['dir'].'\';');
        $args['id'] = empty($arr['item_id']) ? error('�� ������ item_id � �������� '.$args['dir']) : $arr['item_id'];
    }
    if (empty($args['id'])) {
        error('�� ������� ID ������');
    }
    /*
    $arr=query_assoc('
        select
        sum(if(status in (\'sent\',\'paid\',\'delivered not paid\',\'take payment\',\'return\',\'refund\',\'return paid\',\'refund paid\'),q,0)) as q_sent,
        sum(if(status in (\'new\',\'waiting send\',\'check address\',\'packed\',\'unavailable\'),q,0)) as q_new,
        sum(if(status in (\'waiting send\',\'check address\',\'packed\'),q,0)) as q_waiting_send,
        sum(if(status in (\'canceled\'),q,0)) as q_canceled,
        sum(if(status in (\'paid\',\'take payment\'),q,0)) as q_paid,
        sum(if(status in (\'packed\'),q,0)) as q_packed,
        sum(if(status in (\'delivered not paid\'),q,0)) as q_delivered_not_paid,
        sum(if(status in (\'return paid\',\'refund paid\'),q,0)) as q_return_refund_paid,
        sum(if(status in (\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),q,0)) as q_not_paid
        from '.tabname('shop','packages').' p
        join '.tabname('shop','orders').' o on o.id=p.order_id
        --group by location
        where p.item_id=\'113\';
        ');
        */
    $arr = query_assoc('
		select items.id,items.name,items.cost_rur,items.cost_kzt,items.cost_confirm,items.show_admin,items.show_sms,items.sms_text,items_images.path as pic,items.kl_item_id,items.description,items.youtube_id,items.fb_pixel_id,
		(select sum(q) from '.tabname('shop', 'stock').' where item_id=items.id) as q_stock,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'sent\',\'paid\',\'delivered not paid\',\'take payment\',\'return\',\'refund\',\'return paid\',\'refund paid\')) as q_sent,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'new\',\'waiting send\',\'check address\',\'packed\',\'unavailable\')) as q_new,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'waiting send\',\'check address\',\'packed\')) as q_waiting_send,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'canceled\')) as q_canceled,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'paid\',\'take payment\')) as q_paid,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'packed\')) as q_packed,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'delivered not paid\')) as q_delivered_not_paid,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'return paid\',\'refund paid\')) as q_return_refund_paid,
		(select ifnull(sum(q),0) from '.tabname('shop', 'packages').' join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where packages.item_id=items.id and orders.status in (\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\')) as q_not_paid,
		(select ifnull(cost_kzt,0) from '.tabname('shop', 'stock').' where item_id=items.id order by date desc limit 1) as real_cost_kzt,
		(select ifnull(cost_rur,0) from '.tabname('shop', 'stock').' where item_id=items.id order by date desc limit 1) as real_cost_rur
		from '.tabname('shop', 'items').'
		left join '.typetab('items_images').' on items_images.id=items.icon_id
		where items.id=\''.$args['id'].'\'
		;');
    if (! $arr) {
        error('����� �� ����������', '����� '.$args['id'].' �� ����������');
    }
    $arr['q_in_stock'] = $arr['q_stock'] - $arr['q_sent'] + $arr['q_return_refund_paid'];
    $arr['q_available'] = $arr['q_in_stock'] - $arr['q_new'];

    // pre($arr);
    return $arr;
}

function preparing_items_info()
{
    $arr = readdata(query('
		select items.id,items.name,items.cost_rur,items.cost_kzt,items.cost_confirm,items.show_admin,items.show_sms,items.sms_text,items_images.path as pic,items.kl_item_id,
		(select sum(q) from shop.stock where item_id=items.id) as q_stock,
		ifnull(items_stat.q_sent,0) as q_sent,
		ifnull(items_stat.q_new,0) as q_new,
		ifnull(items_stat.q_waiting_send,0) as q_waiting_send,
		ifnull(items_stat.q_canceled,0) as q_canceled,
		ifnull(items_stat.q_paid,0) as q_paid,
		ifnull(items_stat.q_packed,0) as q_packed,
		ifnull(items_stat.q_delivered_not_paid,0) as q_delivered_not_paid,
		ifnull(items_stat.q_return_refund_paid,0) as q_return_refund_paid,
		ifnull(items_stat.q_not_paid,0) as q_not_paid,
		(select ifnull(cost_kzt,0) from shop.stock where item_id=items.id order by date desc limit 1) as real_cost_kzt,
		(select ifnull(cost_rur,0) from shop.stock where item_id=items.id order by date desc limit 1) as real_cost_rur
		from '.typetab('items').'
		left join '.typetab('items_images').' on items_images.id=items.icon_id
		left join (
			select item_id,
			sum(if(orders.status in (\'sent\',\'paid\',\'delivered not paid\',\'take payment\',\'return\',\'refund\',\'return paid\',\'refund paid\'),q,0)) as q_sent,
			sum(if(orders.status in (\'new\',\'waiting send\',\'check address\',\'packed\',\'unavailable\'),q,0)) as q_new,
			sum(if(orders.status in (\'waiting send\',\'check address\',\'packed\'),q,0)) as q_waiting_send,
			sum(if(orders.status in (\'canceled\'),q,0)) as q_canceled,
			sum(if(orders.status in (\'paid\',\'take payment\'),q,0)) as q_paid,
			sum(if(orders.status in (\'packed\'),q,0)) as q_packed,
			sum(if(orders.status in (\'delivered not paid\'),q,0)) as q_delivered_not_paid,
			sum(if(orders.status in (\'return paid\',\'refund paid\'),q,0)) as q_return_refund_paid,
			sum(if(orders.status in (\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),q,0)) as q_not_paid
			from '.typetab('packages').'
			join '.typetab('orders').' on orders.id=packages.order_id
			group by packages.item_id
			) items_stat on items_stat.item_id=items.id
		;'), 'id');
    /*
    $arr=readdata(query('
    select items.id,items.name,items.cost_rur,items.cost_kzt,items.show_admin,items.show_sms,items.sms_text,
    (select sum(q) from '.tabname('shop','stock').' where item_id=items.id) as q_stock,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'sent\',\'paid\',\'delivered not paid\',\'take payment\',\'return\',\'refund\',\'return paid\',\'refund paid\')) as q_sent,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'new\',\'waiting send\',\'check address\',\'packed\',\'unavailable\')) as q_new,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'waiting send\',\'check address\',\'packed\')) as q_waiting_send,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'canceled\')) as q_canceled,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'paid\',\'take payment\')) as q_paid,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'packed\')) as q_packed,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'delivered not paid\')) as q_delivered_not_paid,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'return paid\',\'refund paid\')) as q_return_refund_paid,
    (select ifnull(sum(q),0) from '.tabname('shop','packages').' join '.tabname('shop','orders').' on orders.id=packages.order_id
    where packages.item_id=items.id and orders.status in (\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\')) as q_not_paid,
    (select cost_kzt from '.tabname('shop','stock').' where item_id=items.id order by date desc limit 1) as real_cost_kzt,
    (select cost_rur from '.tabname('shop','stock').' where item_id=items.id order by date desc limit 1) as real_cost_rur
    from '.tabname('shop','items').'
    group by items.id
    ;'),'id');
    */
    foreach ($arr as $k => $v) {
        $arr[$k]['q_in_stock'] = $v['q_stock'] - $v['q_sent'] + $v['q_return_refund_paid'];
        $arr[$k]['q_available'] = $arr[$k]['q_in_stock'] - $v['q_new'];
    }

    // pre($arr);
    return $arr;
}

function preparing_set_info($args)
{
    if (empty($args['id'])) {
        error('�� ������� ID ������');
    }
    $arr_set = query_assoc('select name from '.tabname('shop', 'sets').' where id=\''.$args['id'].'\';');
    if (! $arr_set) {
        error('����� �� ����������');
    }
    $arr_set_items = query_arr('select item_id,q,discount,discount_rub,discount_kzt from '.tabname('shop', 'sets_items').' where set_id=\''.$args['id'].'\';');
    $q_smallest = false;
    $sum_cost_rur = 0;
    $sum_cost_rur_real = 0;
    $sum_cost_kzt = 0;
    $sum_cost_kzt_real = 0;
    foreach ($arr_set_items as $k => $set_item) {
        $item_info = cache('preparing_item_info', ['id' => $set_item['item_id']], 0);
        $discount = (100 - $set_item['discount']) / 100;
        $arr_set_items[$k]['cost_rur'] = $item_info['cost_rur'] * $discount - $set_item['discount_rub'];
        $arr_set_items[$k]['cost_kzt'] = $item_info['cost_kzt'] * $discount - $set_item['discount_kzt'];
        $sum_cost_rur = $sum_cost_rur + $arr_set_items[$k]['cost_rur'] * $set_item['q'];
        $sum_cost_kzt = $sum_cost_kzt + $arr_set_items[$k]['cost_kzt'] * $set_item['q'];
        $sum_cost_rur_real = $sum_cost_rur_real + $item_info['cost_rur'] * $set_item['q'];
        $sum_cost_kzt_real = $sum_cost_kzt_real + $item_info['cost_kzt'] * $set_item['q'];
        $q = ($set_item['q'] == 0) ? 0 : (($item_info['q_in_stock'] - $item_info['q_packed']) / $set_item['q']);
        if ($q_smallest === false or $q < $q_smallest) {
            $q_smallest = $q;
        }
    }
    $return['cost_rur_real'] = $sum_cost_rur_real;
    $return['cost_kzt_real'] = $sum_cost_kzt_real;
    $return['cost_rur'] = ceil($sum_cost_rur);
    $return['cost_kzt'] = ceil($sum_cost_kzt);
    $return['items'] = $arr_set_items;
    $return['name'] = $arr_set['name'];
    $return['q'] = $q_smallest;

    // pre($return);
    return $return;
}

function preparing_sets_info()
{
    $arr_set = query_arr('select id,name from '.tabname('shop', 'sets').';');
    // pre($arr_set);
    $arr_sets_items = readdata(query('select set_id,item_id,q,discount,discount_rub,discount_kzt from '.tabname('shop', 'sets_items').';'), 'set_id', false);
    // pre($arr_sets_items);
    $items_info = cache('preparing_items_info', [], 0);
    $discount_item_id = value_from_config('shop', 'discount_item_id');
    // pre($discount_item_id);
    foreach ($arr_set as $set) {
        // $arr_set_items=query_arr('select item_id,q,discount,discount_rub,discount_kzt from '.tabname('shop','sets_items').'
        $arr_set_items = empty($arr_sets_items[$set['id']]) ? [] : $arr_sets_items[$set['id']];
        // 	where set_id=\''.$set['id'].'\';');
        $q_smallest = false;
        $sum_cost_rur = 0;
        $sum_cost_rur_real = 0;
        $sum_cost_kzt = 0;
        $sum_cost_kzt_real = 0;
        foreach ($arr_set_items as $k => $set_item) {
            $item_info = $items_info[$set_item['item_id']];
            $discount = (100 - $set_item['discount']) / 100;
            $arr_set_items[$k]['cost_rur'] = $item_info['cost_rur'] * $discount - $set_item['discount_rub'];
            $arr_set_items[$k]['cost_kzt'] = $item_info['cost_kzt'] * $discount - $set_item['discount_kzt'];
            $sum_cost_rur = $sum_cost_rur + $arr_set_items[$k]['cost_rur'] * $set_item['q'];
            $sum_cost_kzt = $sum_cost_kzt + $arr_set_items[$k]['cost_kzt'] * $set_item['q'];
            $sum_cost_rur_real = $sum_cost_rur_real + $item_info['cost_rur'] * $set_item['q'];
            $sum_cost_kzt_real = $sum_cost_kzt_real + $item_info['cost_kzt'] * $set_item['q'];
            // �� ��������� ���-�� ������-������, �.�. ��� �� ���������� �����
            if ($set_item['item_id'] != $discount_item_id) {
                $q = ($set_item['q'] == 0) ? 0 : (($item_info['q_in_stock'] - $item_info['q_packed']) / $set_item['q']);
                if ($q_smallest === false or $q < $q_smallest) {
                    $q_smallest = $q;
                }
            }
        }
        $return[$set['id']]['id'] = $set['id'];
        $return[$set['id']]['cost_rur_real'] = $sum_cost_rur_real;
        $return[$set['id']]['cost_kzt_real'] = $sum_cost_kzt_real;
        $return[$set['id']]['cost_rur'] = ceil($sum_cost_rur);
        $return[$set['id']]['cost_kzt'] = ceil($sum_cost_kzt);
        $return[$set['id']]['items'] = $arr_set_items;
        $return[$set['id']]['name'] = $set['name'];
        $return[$set['id']]['q'] = $q_smallest;
    }

    // pre($return);
    return $return;
}

function shop_translation($name)
{
    $arr['order_status'] = [
        'new' => '�����', 'sent' => '���������', 'canceled' => '�������',
        'waiting send' => '������� ��������', 'unavailable' => '�����������',
        'paid' => '�������, ������ ��������', 'delivered not paid' => '���������, �� �������', 'take payment' => '�������, ������� ������',
        'check address' => '��������� �����', 'packed' => '��������', 'refund' => '�������, ������� ������', 'refund paid' => '�������, ������� ������',
        'return' => '������� �� �����', 'return paid' => '������� �� �����, ���������', 'return delivered' => '������� �� �����, �� �����',
    ];
    $arr['filters'] = [
        'items' => '������', 'item_groups' => '������ �������', 'date_from' => '�', 'date_to' => '��',
        'sources' => '���������', 'statuses' => '�������', 'advertisers' => '�������������',
    ];

    return $arr[$name];
}

// ����� ������ cost_rur,cost_kzt (���������� ��� ���������� ������� �� �������)
function plus_minus_items_in_package($arr)
{
    if (empty($arr['order_id'])) {
        error('�� ������� ID ������');
    }
    if (empty($arr['item_id'])) {
        error('�� ������� ID ������');
    }
    if (empty($arr['action'])) {
        error('�� �������� ��������');
    }
    if ($arr['action'] == 'plus') {
        $item_info = preparing_item_info(['id' => $arr['item_id']]);
        $cost_rur = isset($arr['cost_rur']) ? $arr['cost_rur'] : $item_info['cost_rur'];
        $cost_kzt = isset($arr['cost_kzt']) ? $arr['cost_kzt'] : $item_info['cost_kzt'];
        query('insert into '.tabname('shop', 'packages').'
			set order_id=\''.$arr['order_id'].'\', item_id=\''.$arr['item_id'].'\',q=1,
			cost_rur=\''.$cost_rur.'\',cost_kzt=\''.$cost_kzt.'\',
			item_cost_kzt=\''.$item_info['real_cost_kzt'].'\',
			item_cost_rur=\''.$item_info['real_cost_rur'].'\',
			cost_confirm=\''.$item_info['cost_confirm'].'\'
			on duplicate key update q=q+1;');
        // change_order_cost(array('order_id'=>$arr['order_id'],'item_id'=>$arr['item_id'],'action'=>'plus'));
    }
    if ($arr['action'] == 'minus') {
        query('update '.tabname('shop', 'packages').' set q=q-1 where order_id=\''.$arr['order_id'].'\' and item_id=\''.$arr['item_id'].'\';');
        $arr_package = query_assoc('select id,q from '.tabname('shop', 'packages').' where order_id=\''.$arr['order_id'].'\' and item_id=\''.$arr['item_id'].'\';');
        // pre($arr);
        if ($arr_package['q'] == 0) {
            query('delete from '.tabname('shop', 'packages').' where id=\''.$arr_package['id'].'\';');
        }
        // change_order_cost(array('order_id'=>$arr['order_id'],'item_id'=>$arr['item_id'],'action'=>'minus'));
    }
    // ���������� ������� �����
    set_main_item(['order_id' => $arr['order_id']]);

    /*
    $login=auth_login(array('type'=>'shop','error'=>false));
    log_changed_fields(array('change_log'=>typetab('packages_changes'),'table'=>typetab('packages'),'primary_key'=>$order_id,'old'=>$arr['old'],'login'=>$login));
    */
    return '��������� �������� '.$arr['action'];
}

function set_main_item($args)
{
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : $args['order_id'];
    // ���������� ������� �����
    query('update '.typetab('orders').'
		set main_item=(
			select item_id
			from '.typetab('packages').'
			where order_id=orders.id
			order by packages.cost_kzt desc
			limit 1)
	where orders.id=\''.$args['order_id'].'\'
	');
    // ���������� ����� ��� �������� ������ � ����������� �� �������������
    /*
    query('
        update '.typetab('orders').'
        left join '.typetab('bonuses').' on bonuses.item_id=orders.main_item and bonuses.advertiser=orders.advertiser
        set orders.bonus=bonuses.bonus
        where orders.id=\''.$args['order_id'].'\'
        ;');
        */
    // ���������� � orders.bonus - �������������� ������ �����, ���� �� ���, �� ������ �� ����� items.cost_confirm, ���� �� ���, �� 0
    /*
    query('
    update '.typetab('orders').'
    left join '.typetab('bonuses').' on bonuses.item_id=orders.main_item and bonuses.advertiser=orders.advertiser
    left join '.typetab('items').' on items.id=orders.main_item
    set orders.bonus=ifnull(bonuses.bonus,ifnull(items.cost_confirm,0))
    where orders.id=\''.$args['order_id'].'\'
    ;
    ');
    */

    $arr = query_assoc('select main_item,advertiser from '.typetab('orders').' where id=\''.$order_id.'\';');
    if (! empty($arr['main_item'])) {
        $bonus = get_advertiser_bonus_for_item(['item_id' => $arr['main_item'], 'advertiser' => $arr['advertiser']]);
        query('
				update '.typetab('orders').'
				set orders.bonus=\''.$bonus.'\'	where orders.id=\''.$args['order_id'].'\'
				;');
    }
    // die('���������� ������� �����');
}

function get_advertiser_bonus_for_item($args = [])
{
    // $var=null_if_empty($args);
    // pre($args);
    $item_id = empty($args['item_id']) ? error('�� ������� ID ������') : $args['item_id'];
    $advertiser = empty($args['advertiser']) ? '' : $args['advertiser'];
    $sql['advertiser'] = empty($advertiser) ? 'bonuses.advertiser is null' : 'bonuses.advertiser=\''.$advertiser.'\'';
    $arr = query_assoc('
		select ifnull(bonuses.bonus,ifnull(items.cost_confirm,0)) as bonus
		from '.typetab('items').'
		left join '.typetab('bonuses').' on bonuses.item_id=items.id and '.$sql['advertiser'].'
		where items.id=\''.$item_id.'\';
		');

    // pre($arr['bonus']);
    return $arr['bonus'];
}

// ��������� ����� � ���������� ID ������ ������
function duplicate_order()
{
    $order_id = empty($_GET['order_id']) ? error('�� ������� ID ������') : check_input($_GET['order_id']);
    query('insert into '.typetab('orders').' (date,name,phone,country,zip,address,status,info,utm_source,ip)
		select now() as date,name,phone,country,zip,address,\'canceled\' as status,concat_ws(\'\',\'����� ������ #\',id) as info,\'\' as utm_source,ip
		from '.typetab('orders').'
		where id=\''.$order_id.'\'
		;');
    // $created_id=create_order($arr);
    $created_id = mysql_insert_id();
    // �������, �.�. ���������� �������� �������������
    // query('delete from '.typetab('packages').' where order_id=\''.$created_id.'\';');
    $arr = query('
		insert into '.typetab('packages').'
		(order_id,item_id,q,cost_rur,cost_kzt,item_cost_rur,item_cost_kzt,cost_confirm)
		(select \''.$created_id.'\' as order_id,item_id,q,cost_rur,cost_kzt,item_cost_rur,item_cost_kzt,cost_confirm
			from '.typetab('packages').'
			where order_id=\''.$order_id.'\')
		;');
    /*
    foreach ($arr as $v) {
        for ($i=1;$i<=$q;$i++) {
            plus_minus_items_in_package(array('order_id'=>$created_id,'item_id'=>$v['item_id'],
                'action'=>'plus','cost_rur'=>$item['cost_rur'],'cost_kzt'=>$item['cost_kzt']));
        };
    };
    */
    // ��������� ����������� �������� ������, �.�. �� ����������� �� ����
    set_main_item(['order_id' => $created_id]);

    return $created_id;
}

// �������� ��� � �����
function add_set_to_order($args)
{
    $args['order_id'] = empty($args['order_id']) ? error('�� ����� ID ������') : check_input($args['order_id']);
    $args['set_id'] = empty($args['set_id']) ? error('�� ����� ID ������') : check_input($args['set_id']);
    $args['q'] = empty($args['q']) ? 1 : $args['q'];
    $info = preparing_set_info(['id' => $args['set_id']]);
    foreach ($info['items'] as $item) {
        $q = $args['q'] * $item['q'];
        for ($i = 1; $i <= $q; $i++) {
            plus_minus_items_in_package([
                'order_id' => $args['order_id'],
                'item_id' => $item['item_id'],
                'action' => 'plus',
                'cost_rur' => $item['cost_rur'],
                'cost_kzt' => $item['cost_kzt'],
            ]);
        }
    }

    return true;
}

function add_item_to_order($args)
{
    $args['order_id'] = empty($args['order_id']) ? error('�� ����� ID ������') : check_input($args['order_id']);
    $args['item_id'] = empty($args['item_id']) ? error('�� ����� ID ������') : check_input($args['item_id']);
    $args['q'] = empty($args['q']) ? 1 : $args['q'];
    $info = preparing_item_info(['id' => $args['item_id']]);
    for ($i = 1; $i <= $args['q']; $i++) {
        plus_minus_items_in_package(['order_id' => $args['order_id'], 'item_id' => $args['item_id'], 'action' => 'plus']);
    }

    return true;
}

// ��������� ����� � ����. ������: type=[item|set], q
function create_order($args = [])
{
    // if (empty($args['q'])) {error('�� ������ ����������');};
    $args['type'] = empty($args['type']) ? '' : $args['type'];
    $args['q'] = empty($args['q']) ? 1 : $args['q'];
    $args['phone'] = empty($args['phone']) ? '' : $args['phone'];
    $args['name'] = empty($args['name']) ? '' : $args['name'];
    $args['utm_source'] = empty($args['utm_source']) ? '' : $args['utm_source'];
    $args['utm_content'] = empty($args['utm_content']) ? '' : $args['utm_content'];
    $args['utm_campaign'] = empty($args['utm_campaign']) ? '' : $args['utm_campaign'];
    $args['utm_medium'] = empty($args['utm_medium']) ? '' : $args['utm_medium'];
    $args['address'] = empty($args['address']) ? '' : $args['address'];
    $args['comment'] = empty($args['comment']) ? '' : $args['comment'];
    $args['country'] = in_array((empty($args['country']) ? false : $args['country']), ['������', '���������']) ? $args['country'] : '���������';
    $args['ga'] = empty($args['ga']) ? '' : $args['ga'];
    $args['user_ip'] = empty($args['user_ip']) ? request()->ip() : $args['user_ip'];
    // pre($args);
    query('insert into '.tabname('shop', 'orders').'
		set date=now(), phone=\''.$args['phone'].'\', name=\''.$args['name'].'\',
		utm_source=\''.$args['utm_source'].'\', utm_content=\''.$args['utm_content'].'\',
		utm_campaign=\''.$args['utm_campaign'].'\', utm_medium=\''.$args['utm_medium'].'\',
		address=\''.$args['address'].'\', info=\''.$args['comment'].'\', ip=\''.$args['user_ip'].'\',
		country=\''.$args['country'].'\',
		ga=\''.$args['ga'].'\'
		;');
    $order_id = mysql_insert_id();
    // �������� ������
    // copy_order_to_history(array('id'=>$order_id));
    if ($args['type'] == 'set') {
        $args['set_id'] = empty($args['set_id']) ? error('�� ����� ID ������') : check_input($args['set_id']);
        add_set_to_order(['order_id' => $order_id, 'set_id' => $args['set_id'], 'q' => $args['q']]);
        /*
        if (empty($args['set_id'])) {error('�� ����� ID ������');};
        $info=preparing_set_info(array('id'=>$args['set_id']));
        foreach ($info['items'] as $item) {
            $q=$args['q']*$item['q'];
            for ($i=1;$i<=$q;$i++) {
                plus_minus_items_in_package(array('order_id'=>$order_id,'item_id'=>$item['item_id'],
                    'action'=>'plus','cost_rur'=>$item['cost_rur'],'cost_kzt'=>$item['cost_kzt']));
            };
        };
        */
    }
    if ($args['type'] == 'item') {
        $args['item_id'] = empty($args['item_id']) ? error('�� ����� ID ������') : check_input($args['item_id']);
        add_item_to_order(['order_id' => $order_id, 'item_id' => $args['item_id'], 'q' => $args['q']]);
        /*
        $info=preparing_item_info(array('id'=>$args['item_id']));
        for ($i=1;$i<=$args['q'];$i++) {
            plus_minus_items_in_package(array('order_id'=>$order_id,'item_id'=>$args['item_id'],'action'=>'plus'));
        };
        */
    }
    // ������������� ��������� ��������
    $delivery_id = value_from_config('shop', 'delivery_id');
    plus_minus_items_in_package(['order_id' => $order_id, 'item_id' => $delivery_id, 'action' => 'plus']);

    // ��������� �����
    // $coupon_id=value_from_config('shop','coupon_id');
    // plus_minus_items_in_package(array('order_id'=>$order_id,'item_id'=>$coupon_id,'action'=>'plus'));
    // ���������� ������� �����
    // set_main_item(array('order_id'=>$order_id));
    return $order_id;
}

// ����� ���������� ��� item_id="8", set_id="10" ��� � single_id="set_10" ��� "item_8"
function submit_order_result()
{
    // pre($_POST);
    // pre($_COOKIE);
    global $config;
    // ���� ������ �� ������� $_POST - �� ������ ������
    if (empty($_POST)) {
        return ['status' => false, 'msg' => '����������� ����������: item_id,phone'];
    }
    // $var['charset']=isset($_POST['charset'])?$_POST['charset']:$config['charset'];
    $var['charset'] = isset($_POST['charset']) ? $_POST['charset'] : 'utf8';
    $var['q'] = isset($_POST['q']) ? (int) $_POST['q'] : 1;
    $var['phone'] = isset($_POST['phone']) ? check_input($_POST['phone'], $var['charset']) : false;
    if (! $var['phone']) {
        return ['status' => false, 'msg' => '�� ������� phone'];
    }
    $var['name'] = isset($_POST['name']) ? check_input($_POST['name'], $var['charset']) : '��� �� �������';
    $var['address'] = isset($_POST['address']) ? check_input($_POST['address'], $var['charset']) : '';
    $var['country'] = isset($_POST['country']) ? check_input($_POST['country'], $var['charset']) : '';
    /*
    $var['utm_source']=isset($_COOKIE['utm_source'])?check_input($_COOKIE['utm_source'],$var['charset']):'';
    // ����������� ����� ���?
    if (empty($var['utm_source']) and isset($_POST['utm_source'])) {
        $var['utm_source']=check_input($_POST['utm_source']);
        setcookie("utm_source",$var['utm_source'],time()+3600*24);
        $_COOKIE['utm_source']=$var['utm_source'];
    };
    $var['utm_content']=isset($_POST['utm_content'])?check_input($_POST['utm_content'],$var['charset']):'';
    */
    $var['utm_content'] = check_input(empty($_COOKIE['utm_content']) ? (empty($_POST['utm_content']) ? '' : $_POST['utm_content']) : $_COOKIE['utm_content'], $var['charset']);
    $var['utm_source'] = check_input(empty($_COOKIE['utm_source']) ? (empty($_POST['utm_source']) ? '' : $_POST['utm_source']) : $_COOKIE['utm_source'], $var['charset']);
    $var['utm_campaign'] = check_input(empty($_COOKIE['utm_campaign']) ? (empty($_POST['utm_campaign']) ? '' : $_POST['utm_campaign']) : $_COOKIE['utm_campaign'], $var['charset']);
    $var['utm_medium'] = check_input(empty($_COOKIE['utm_medium']) ? (empty($_POST['utm_medium']) ? '' : $_POST['utm_medium']) : $_COOKIE['utm_medium'], $var['charset']);
    $var['ga'] = check_input(empty($_COOKIE['_ga']) ? '' : $_COOKIE['_ga']);
    if (empty($_POST['user_ip'])) {
        $var['user_ip'] = request()->ip();
    } else {
        $var['user_ip'] = filter_var($_POST['user_ip'], FILTER_VALIDATE_IP) ? $_POST['user_ip'] : '';
    }
    $var['comment'] = '';
    if (isset($_POST['comment'])) {
        if (is_array($_POST['comment'])) {
            $list = '';
            foreach ($_POST['comment'] as $comment) {
                if (! empty($list)) {
                    $list .= '<br/>';
                }
                $list .= check_input($comment, $var['charset']);
            }
        } else {
            $list = check_input($_POST['comment'], $var['charset']);
        }
        $var['comment'] = '��������� �� �������: "'.$list.'"';
    }
    if (isset($_POST['single_id'])) {
        $var['single_id'] = check_input($_POST['single_id'], $var['charset']);
        $arr_single = explode('_', $var['single_id']);
        if ($arr_single[0] == 'set') {
            $var['type'] = 'set';
            $var['set_id'] = $arr_single[1];
        }
        if ($arr_single[0] == 'item') {
            $var['type'] = 'item';
            $var['item_id'] = $arr_single[1];
        }
    } else {
        if (isset($_POST['item_id'])) {
            $var['type'] = 'item';
            $var['item_id'] = check_input($_POST['item_id'], $var['charset']);
        } else {
            if (isset($_POST['set_id'])) {
                $var['type'] = 'set';
                $var['set_id'] = check_input($_POST['set_id'], $var['charset']);
            } else {
                // return array('status'=>false,'msg'=>'�� ����� item_id ��� set_id ��� single_id');
                logger()->info('�� ����� item_id ��� set_id ��� single_id');
                $var['type'] = '';
            }
        }
    }
    // pre($var);
    // pre($config);
    $order_id = create_order($var);
    $config['vars']['created_order_id'] = $order_id;
    order_cpa(['step' => 'new', 'order_id' => $order_id]);
    // ��� ��� ��������� set_main_item ���� �� � ���������� ��� create_order,
    // �.�. ����� ����������� order_cpa � ���������� �������������, ������ ����� ���������.
    set_main_item(['order_id' => $order_id]);
    logger()->info('SHOP: ���������� ������ '.$order_id);

    // alert('�������, ��� ����� ������. �� ��� ����� ��������.');
    return ['status' => true, 'orderid' => $order_id];
}

function submit_order()
{
    $result = submit_order_result();
    if (! $result['status']) {
        error($result['msg']);
    }
}

function submit_test()
{
    $_POST['item_id'] = '16';
    $_POST['uid'] = 46;
    // $_POST['nnn']='test';
    // $_POST['utm_content']='iphone6_kma';
    // $_POST['utm_source']='stark';
    // $_POST['charset']='windows-1251';
    $_POST['name'] = '���';
    $_POST['phone'] = '�������';
}

// submit_test();

/*
function kma_submit_order() {
    $answer=submit_order();
    if () {
        $result['result']=true;
        $result['orderid']=$order_id;
        $return=json_encode($result);
    } else {
        shop_error('�� �������� ����������� ����������');
    };
}


function shop_error($text='') {
    $advertiser=return_advertiser_common();
    if (!$advertiser) {error($text);};
    // $arr=query_assoc('select error_format ');
    $return='';
    if ($advertiser=='kma') {
        $result['result']=false;
        $result['msg']=iconv('windows-1251','utf-8',$text);
        $return=json_encode($result);
    };
    if ($advertiser=='proffi') {
    };
    die($return);
}

function shop_ok_answer() {
    $advertiser=return_advertiser_common();
    $order_id=created_order_id();
    if (!$advertiser) {alert('�������� ����� #'.$order_id);};
    $return='';
    if ($advertiser=='kma') {
        $result['result']=true;
        $result['orderid']=$order_id;
        $return=json_encode($result);
    };
    die($return);
}
*/

function created_order_id()
{
    global $config;

    return isset($config['vars']['created_order_id']) ? $config['vars']['created_order_id'] : error('ID ���������� ������ ����������');
    // return get_var('created_order_id');
}

// ������� ���� ��� ������� (�� ������ ������� �� ����� �������)
// TODO �������� �� order_set_status ��� ������ ���������� �� ���� �������. ����������� ��������� ��� �������������� ������.
function shop_auto_statuses()
{
    // $arr['old']=query_assoc('select * from '.tabname('shop','orders').' where id='.$var['id'].';');
    // log_changed_fields(array('change_log'=>typetab('orders_changes'),'table'=>typetab('orders'),'primary_key'=>$var_norm['id'],
    // 	'old'=>$arr['old'],'login'=>$var_norm2['update_login']));

    // ���������� sent ����� ������ "���� ����", "������" ��� "���� �����" � track �� null
    /*
    query('update '.tabname('shop','orders').' set status=\'sent\'
        where status in (\'waiting send\',\'packed\',\'check address\') and track is not null;');
    logger()->info('��������� ������� �������');
    */
}

function shop_auto_statuses_old()
{
    // ���������� "waiting send" ����� ������ "new" � ����� �� null
    // query('update '.tabname('shop','orders').' set status=\'waiting send\' where status=\'new\' and address is not null;');
    // ���������� sent ����� ������ "waiting send" � track �� null
    query('update '.tabname('shop', 'orders').' set status=\'sent\'
		where status in (\'waiting send\',\'packed\',\'check address\') and track is not null;');
    // ���������� "delivered not paid" ����� ������ "sent" � ���� "������� � ����� ��������"
    // kazpost - �� �������
    query('update '.tabname('shop', 'orders').'
		/* left join '.tabname('shop', 'gdeposylka').' on gdeposylka.track=orders.track */
		left join '.tabname('shop', 'kazpost').' on kazpost.track=orders.track
		/* left join '.tabname('shop', 'moyaposylka').' on moyaposylka.track=orders.track */
		set status=\'delivered not paid\'
		where status in (\'sent\') and
		( /* gdeposylka.message like \'%������� � ����� ��������%\' or */
			kazpost.16=\'�� �������\'
			/* ������ (����. ������ ������ 010000 � ������ �� 010005, � 010000 ����� ����������)
			or moyaposylka.zip=orders.zip
			*/
			)
		;');
    // ���������� paid ����� �������� ��������
    query('update '.tabname('shop', 'orders').'
		/* left join '.tabname('shop', 'gdeposylka').' on gdeposylka.track=orders.track */
		left join '.tabname('shop', 'kazpost').' on kazpost.track=orders.track
		/* left join '.tabname('shop', 'moyaposylka').' on moyaposylka.track=orders.track */
		set status=\'take payment\'
		where status in (\'sent\',\'delivered not paid\') and
		(/* gdeposylka.message like \'%�������� ��������%\' or */ kazpost.11=\'�������\' /* or moyaposylka.message=\'�������\' */)
			;');
    // ���������� return ����� ���� "���� ����"
    query('update '.tabname('shop', 'orders').'
		/* left join '.tabname('shop', 'gdeposylka').' on gdeposylka.track=orders.track */
		left join '.tabname('shop', 'kazpost').' on kazpost.track=orders.track
		set status=\'return\'
		where status in (\'sent\',\'delivered not paid\')
		and (/* gdeposylka.message like \'%���� ����%\' or */ kazpost.11=\'�������\')
		;');
    // ���������� return ����� � ������ ����� ����������� ���� ������� (� ������� �� �������� "sent" � "delivered not paid")

    // ���������� return paid ����� ������ return � ���� �������
    query('update '.tabname('shop', 'orders').'
		/* left join '.tabname('shop', 'gdeposylka').' on gdeposylka.track=orders.track */
		left join '.tabname('shop', 'kazpost').' on kazpost.track=orders.track
		set status=\'return paid\'
		where status in (\'return\') and kazpost.11=\'�������\'
		;');
    logger()->info('��������� ������� �������');
}

function shop_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^admin$/', $query, $args)) {
        $config['vars']['page'] = 'admin/index';

        return;
    }
    if (preg_match('/^api\/([a-z\/-]+)$/', $query, $args)) {
        // pre($args);
        $config['vars']['page'] = 'api/'.$args[1];

        return;
    }
}

function landing_rewrite_query($query)
{
    global $config;
    // if (preg_match('/^return$/',$query,$args)) {
    // 	$config['vars']['page']='landings/main/return';
    // 	return;
    // };
    // if (preg_match('/^watches$/',$query,$args)) {
    // 	$config['vars']['page']='landings/main/watches';
    //     return;
    // };
    // if (preg_match('/^watches2$/',$query,$args)) {
    // 	$config['vars']['page']='landings/main/watches2';
    //     return;
    // };
    // if (preg_match('/^women-watches$/',$query,$args)) {
    // 	$config['vars']['page']='landings/main/women-watches';
    //     return;
    // };
    // if (preg_match('/^purse-watches$/',$query,$args)) {
    // 	$config['vars']['page']='landings/main/purse-watches';
    //     return;
    // };
    // if (preg_match('/^purse$/',$query,$args)) {
    // 	$config['vars']['page']='landings/main/purse';
    //     return;
    // };
    if (preg_match('/^([a-z0-9-]+)(\/?)$/', $query, $args)) {
        set_landing($args[1]);

        return;
    }
    if (preg_match('/^d\/([0-9-]+)(\/?)$/', $query, $args)) {
        set_cookies_from_get();
        $arr = preparing_item_info(['id' => $args[1]]);
        $config['vars']['page'] = 'landings/default/index';
        $config['vars']['item_id'] = $args[1];

        return;
    }
    if (preg_match('/^a\/([0-9-]+)(\/?)$/', $query, $args)) {
        set_cookies_from_get();
        $arr = preparing_item_info(['id' => $args[1]]);
        $config['vars']['page'] = 'landings/default2/index';
        $config['vars']['item_id'] = $args[1];

        return;
    }
    if (preg_match('/^([a-z0-9-]+)\/adwad\-frame$/', $query, $args)) {
        set_landing($args[1]);

        return;
    }
}

// �� ������ config.vars.page
function set_vars_for_landing($path = false)
{
    global $config;
    if (! $path) {
        error('�� ������� �������');
    }
    $arr_item = query_assoc('select item_id,set_id from '.tabname('shop', 'landings').' where dir=\''.$path.'\';');
    if (! $arr_item) {
        return;
    }
    $config['vars']['path'] = $path;
    $config['vars']['item_id'] = $arr_item['item_id'];
    $config['vars']['set_id'] = $arr_item['set_id'];
}

function set_landing($dir = 'main')
{
    global $config;
    $arr_item = query_assoc('select item_id,set_id from '.tabname('shop', 'landings').' where dir=\''.$dir.'\' and active;');
    if (! $arr_item) {
        error('������� �� ����������');
    }
    $config['vars']['path'] = $dir;
    $config['vars']['page'] = 'landings/'.$dir.'/index';
    $config['vars']['item_id'] = $arr_item['item_id'];
    $config['vars']['set_id'] = $arr_item['set_id'];
    set_cookies_from_get();
    landings_click_count(['landing' => $dir]);
}

function landings_click_count($args)
{
    query('insert into '.typetab('landings_clicks').' set dir=\''.$args['landing'].'\' on duplicate key update q=q+1;');
}

function shop_init()
{
    global $config;
    include $config['path']['server'].'types/shop/inc/admin.php';
    include $config['path']['server'].'types/shop/inc/gdeposylka.php';
    include $config['path']['server'].'types/shop/inc/smsc.php';
    include $config['path']['server'].'types/shop/inc/returns.php';
    include $config['path']['server'].'types/shop/inc/gdeposylka/autoload.php';
    include $config['path']['server'].'types/shop/inc/moyaposylka.php';
    include $config['path']['server'].'types/shop/inc/kazpost.php';
    include $config['path']['server'].'types/shop/inc/cpa.php';
    include $config['path']['server'].'types/shop/inc/advertiser.php';
    include $config['path']['server'].'types/shop/inc/ketlogistic.php';
    include $config['path']['server'].'types/shop/inc/api.php';
    include $config['path']['server'].'types/shop/inc/kazpost_tracks.php';
    // pre($_COOKIE);
    // pre($config);
    // pre($_GET);
    // ���� ������ ���� /?page=landings/breitling/order �� ��������� set_vars_for_landing, ����� �� �������� ����� ���� ������������ ���������� ��������
    if (! empty($config['vars']['page'])) {
        $pathinfo = pathinfo($config['vars']['page']);
        if (isset($pathinfo['dirname'])) {
            $explode = explode('/', $pathinfo['dirname']);
            if (isset($explode[0],$explode[1])) {
                if ($explode[0] == 'landings') {
                    set_vars_for_landing($explode[1]);
                }
            }
        }
    }
    if (empty($config['vars']['page'])) {
        shop_rewrite_query();
    }
    // cronjob(tabname('shop','crons'));
}
