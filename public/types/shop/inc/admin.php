<?php

namespace MegawebV1;

/*
// ���� ������� item_id �� ���������� ������ ��������� � ������� �� ������� �����
function get_groups_list($args) {
    global $config;
    $args['template']=empty($args['template'])?error('�� ������� ������ ������'):$args['template'];
    $args['item_id']
    $arr=query_arr('select item_groups.id,item_groups.name
        from '.typetab('item_groups').'
        order by item_groups.name
        ;');
    if ($args['item_id']) {
        $arr_item_groups=get_item_groups(array('item_id'=>$args['item_id']));
    };
    $list='';
    foreach ($arr as $v) {
        if ($args['item_id']) {
            foreach ($arr_item_groups as $v2) {
                if ($v2['id']===$v['id']) {continue 2;};
            };
        };
        // ���������� ��� ������ ������� � ����������
        $config['temp_vars']=$v;
        $list.=template($args['template']);
    };
    clear_temp_vars();
    return $list;
}
*/

function stat_incomes_and_expenses()
{
    $data = query('select date_format(date,\'%Y-%m\') as month, avg(rate) as rate from '.typetab('rates').' group by month;');
    $rates = readdata($data, 'month');
    $data = query('
		select date_format(orders.date,\'%Y-%m\') as month,sum(packages.cost_kzt*packages.q) as sum, count(distinct orders.id) as q
		from '.typetab('packages').'
		join '.typetab('orders').' on packages.order_id=orders.id
		where orders.status in (\'paid\',\'take payment\',\'refund\',\'refund paid\')
		group by month
		;');
    $incomes = readdata($data, 'month');
    $arr_expenses = query_arr('
		select date_format(date,\'%Y-%m\') as month,sum(sum) as sum, currency
		from '.typetab('purchase').'
		where login not in (\'ozerin_shop\',\'olekit\') or login is null
		group by month,currency
		;');
    foreach ($arr_expenses as $v) {
        $expenses[$v['month']][$v['currency']] = $v['sum'];
    }
    $list = '';
    $sum_inc = 0;
    $sum_exp_rub = 0;
    $sum_exp_kzt = 0;
    $sum_exp_rub_to_kzt = 0;
    $first_rate = $rates['2015-11']['rate'];
    $last_rate = false;
    // pre($rates);
    foreach ($incomes as $v) {
        // pre($rates[$v['month']]['rate']);
        // ���� ������ ������ �� ������������ ������ ����
        $use_first_rate = $last_rate ? false : true;
        $rate = isset($rates[$v['month']]['rate']) ? $rates[$v['month']]['rate'] : ($last_rate ? $last_rate : ($use_first_rate ? $first_rate : error('�� ��������� rate ��� '.$v['month'])));
        $last_rate = $rate;
        $exp_rub = isset($expenses[$v['month']]['rur']) ? $expenses[$v['month']]['rur'] : 0;
        $exp_kzt = isset($expenses[$v['month']]['kzt']) ? $expenses[$v['month']]['kzt'] : 0;
        $exp_rub_to_kzt = $rate * $exp_rub;
        $sum_inc = $sum_inc + $v['sum'];
        $sum_exp_rub = $sum_exp_rub + $exp_rub;
        $sum_exp_kzt = $sum_exp_kzt + $exp_kzt;
        $sum_exp_rub_to_kzt = $sum_exp_rub_to_kzt + $exp_rub_to_kzt;
        $list .= '<tr>
		<td>'.$v['month'].'</td>
		<td>'.number_format($exp_kzt).' �����</td>
		<td>'.number_format($exp_rub).' ���. (~'.number_format(round($exp_rub_to_kzt)).' �����)</td>
		<td>'.$v['q'].' <!-- ('.number_format(round($exp_kzt + $exp_rub_to_kzt) / $v['q']).' ���/�����) --></td>
		<td>'.number_format($v['sum']).' �����</td>
		<td>'.number_format($v['sum'] - $exp_kzt - $exp_rub_to_kzt).' �����</td>
		</tr>';
    }

    /*
    $list.='<tr>
        <td></td>
        <td>'.number_format($sum_exp_kzt).' �����</td>
        <td>'.number_format($sum_exp_rub).' ���. (~'.number_format(round($sum_exp_rub_to_kzt)).' �����)</td>
        <td></td>
        <td>'.number_format($sum_inc).' �����</td>
        <td>'.number_format($sum_inc-$sum_exp_kzt-$sum_exp_rub_to_kzt).'</td>
        </tr>';
        */
    return $list;
}

function admin_workers_stat()
{
    $dates = query_assoc('
		select current_date as today,
		(current_date - interval 1 day) as yesterday,
		(current_date + interval 1 day) as tomorrow
		;');
    $date_from = empty($_GET['date_from']) ? $dates['today'] : check_input($_GET['date_from']);
    $date_to = empty($_GET['date_to']) ? $dates['today'] : check_input($_GET['date_to']);
    $dates = array_merge($dates, query_assoc('
		select
		(\''.$date_from.'\' - interval 1 day) as day_before,
		(\''.$date_from.'\' + interval 1 day) as day_after
		'));
    // pre($dates);
    // ����� �� ����������
    $query['callers'] = '
	select oc.value,count(*) as q_approved,
	sum(if(orders.status in (\'paid\',\'take payment\'),1,0)) as q_paid,
	sum(orders.upsale) as q_upsale
	from '.typetab('orders_changes').' oc
	join '.typetab('orders').' on orders.id=oc.primary_key
	where (oc.date between \''.$date_from.'\' and (\''.$date_to.'\' + interval 1 day)) and oc.field=\'login\'
	';
    $arr['callers'] = query_assoc($query['callers']);
    $arr['callers_by_logins'] = query_arr(
        $query['callers'].'
		group by oc.value
		order by q_approved desc
		;');
    $list['callers'] = '';
    foreach ($arr['callers_by_logins'] as $v) {
        $approved_percent = ($arr['callers']['q_approved'] == 0) ? 0 : round($v['q_approved'] / $arr['callers']['q_approved'] * 100);
        $paid_percent = ($arr['callers']['q_approved'] == 0) ? 0 : round($v['q_paid'] / $v['q_approved'] * 100);
        $upsale_percent = ($arr['callers']['q_approved'] == 0) ? 0 : round($v['q_upsale'] / $v['q_approved'] * 100);
        $list['callers'] .= '
		<tr>
			<td>'.$v['value'].'</td>
			<td>'.$approved_percent.'% (<a href="/?page=admin/orders&amp;status=check_address&amp;paid_caller='.$v['value'].'">'.$v['q_approved'].'</a>)</td>
			<td>'.$upsale_percent.'% ('.$v['q_upsale'].')</td>
			<td>'.$paid_percent.'% ('.$v['q_paid'].')</td>
		</tr>';
    }
    // ����� ����� �� ����������
    // ����� �� �����������
    $query['packers'] = '
	select oc.value,count(*) as q_packed
	from '.typetab('orders_changes').' oc
	where (oc.date between \''.$date_from.'\' and (\''.$date_to.'\' + interval 1 day)) and oc.field=\'packer\'
	';
    $arr['packers'] = query_assoc($query['packers']);
    $arr['packers_by_logins'] = query_arr(
        $query['packers'].'
		group by oc.value
		order by q_packed desc
		;');
    $list['packers'] = '';
    foreach ($arr['packers_by_logins'] as $v) {
        $packed_percent = ($arr['packers']['q_packed'] == 0) ? 0 : round($v['q_packed'] / $arr['packers']['q_packed'] * 100);
        $list['packers'] .= '
		<tr>
			<td>'.$v['value'].'</td>
			<td>'.$packed_percent.'% ('.$v['q_packed'].')</td>
		</tr>';
    }
    // ����� ����� �� �����������
    $stat_period = ($date_from === $date_to) ? $date_from : ('������ � '.$date_from.' �� '.$date_to);
    temp_var('day_before', $dates['day_before']);
    temp_var('day_after', $dates['day_after']);
    temp_var('stat_period', $stat_period);
    temp_var('date_from', $date_from);
    temp_var('date_to', $date_to);
    temp_var('callers_table_rows', $list['callers']);
    temp_var('packers_table_rows', $list['packers']);
    $block = template('admin/stat/users/block_workers');
    clear_temp_vars();

    return $block;
}

function edit_package_item_price($args = [])
{
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : check_input($args['order_id']);
    $item_id = empty($args['item_id']) ? error('�� ������� ID ������') : check_input($args['item_id']);
    $cost_kzt = empty($args['cost_kzt']) ? error('�� �������� ��������� KZT') : check_input($args['cost_kzt']);
    query('update '.typetab('packages').' set cost_kzt=\''.$cost_kzt.'\'
		where order_id=\''.$order_id.'\' and item_id=\''.$item_id.'\'
		;');
}

// ���� �� ������� ������ - ���������� ������
function get_item_groups($args)
{
    global $config;
    $args['template'] = empty($args['template']) ? false : $args['template'];
    $args['item_id'] = empty($args['item_id']) ? false : $args['item_id'];
    $args['item_action'] = empty($args['item_action']) ? false : (in_array($args['item_action'], ['include', 'exclude']) ? $args['item_action'] : error('�������� � ������� �� ����������'));
    $args['group_type'] = empty($args['group_type']) ? false : (in_array($args['group_type'], ['group', 'offer']) ? $args['group_type'] : error('��� ������ �� ����������'));
    $sql['group_type']['where'] = $args['group_type'] ? 'type=\''.$args['group_type'].'\'' : '1';
    $sql['item_id']['join'] = '';
    $sql['item_id']['select'] = '';
    if ($args['item_id']) {
        $sql['item_id']['join'] = 'left join '.typetab('items_groups').' on items_groups.group_id=item_groups.id and items_groups.item_id=\''.$args['item_id'].'\'';
        $sql['item_id']['select'] = ', \''.$args['item_id'].'\' as item_id';
    }
    $sql['item_id']['include'] = 1;
    $sql['item_id']['exclude'] = 1;
    if ($args['item_id']) {
        if ($args['item_action'] === 'include') {
            $sql['item_id']['include'] = 'items_groups.group_id is not null';
        }
        if ($args['item_action'] === 'exclude') {
            $sql['item_id']['exclude'] = 'items_groups.group_id is null';
        }
    }
    $arr = query_arr('select item_groups.id as group_id,item_groups.name as group_name,item_groups.type,item_groups.public,item_groups.vip_call '.$sql['item_id']['select'].'
		from '.typetab('item_groups').'
		'.$sql['item_id']['join'].'
		where
		'.$sql['group_type']['where'].'
		and
		'.$sql['item_id']['include'].'
		and
		'.$sql['item_id']['exclude'].'
		order by item_groups.name
		;');
    /*
    $arr=query_arr('select item_groups.id,item_groups.name,item_groups.type,item_groups.public,item_groups.vip_call
        from '.typetab('items_groups').'
        join '.typetab('item_groups').' on item_groups.id=items_groups.group_id
        where items_groups.item_id=\''.$args['item_id'].'\'
        order by item_groups.name
        ;');
    */
    if (! $args['template']) {
        return $arr;
    }
    $list = '';
    foreach ($arr as $v) {
        // ���������� ��� ������ ������� � ����������
        $config['temp_vars'] = $v;
        $list .= template($args['template']);
    }
    clear_temp_vars();

    return $list;
}

function sets_list() {}

function admin_mass_set_returns()
{
    $list_tracks = empty($_POST['list_tracks']) ? error('�� ������� ������ ������') : trim($_POST['list_tracks']);
    $arr = explode("\r\n", $list_tracks);
    $rus['statuses'] = shop_translation('order_status');
    // pre($arr);
    $list = '';
    foreach ($arr as $v) {
        $order = query_assoc('select id from '.typetab('orders').' where upper(track)=\''.strtoupper($v).'\';');
        if (! $order) {
            $list .= '<li>'.$v.' <span style="color: red;">����� �� ������</span></li>';

            continue;
        }
        $order_info = preparing_order_info(['order_id' => $order['id']]);
        // pre($order_info);
        $order_link = '<a href="/?page=admin/orders&amp;status=all&amp;order_id='.$order['id'].'">#'.$order['id'].'</a>';
        if (in_array($order_info['status'], ['return paid'])) {
            $list .= '<li>'.$v.' '.$order_link.' �������</li>';

            continue;
        }
        if (in_array($order_info['status'], ['return'])) {
            order_set_status(['order_id' => $order_info['order_id'], 'status' => 'return paid']);
            $list .= '<li>'.$v.' '.$order_link.' <span style="color: green;">ok</span></li>';
        } else {
            $list .= '<li>'.$v.' '.$order_link.' <span style="color: red;">������� ������ <b>'.$rus['statuses'][$order_info['status']].'</b>, ������ ���� <b>'.$rus['statuses']['return'].'</b></span></li>';
        }
        // pre($order_info);
    }

    return $list;
}

function admin_coupons()
{
    $template = 'admin/orders/item_coupon';
    $q = 8;
    $i = 0;
    $list = '';
    for ($i; $i < $q; $i++) {
        $list .= template($template);
    }

    return $list;
}

function test_buyouts_all()
{
    ini_set('memory_limit', '1000M');
    $orders = query_arr('
		select orders.id,packages.item_id,orders.status, packages.cost_kzt*packages.q as cost_kzt,
		orders.utm_source,orders.advertiser
		from '.typetab('packages').'
		join '.typetab('orders').' on packages.order_id=orders.id
		where orders.status not in (\'new\',\'unavailable\',\'canceled\')
		;');
    $buyouts = query_arr('
		select
		');
    // pre('ff');
    $cost_real = 0;
    $cost_buyout = 0;
    foreach ($orders as $v) {
        if (in_array($v['status'], ['paid', 'take payment'])) {
            $cost_real += $v['cost_kzt'];
        }
        $arr_buyout = item_buyout(['item_id' => $v['item_id'], 'utm_source' => $v['utm_source'], 'advertiser' => $v['advertiser']]);
        $buyout = $arr_buyout['buyout'] / 100;
        $cost_buyout += $v['cost_kzt'] * $buyout;
    }

    return '��������: '.$cost_real.'<br>���������: '.$cost_buyout;
}

function admin_add_new_landing()
{
    query('insert into '.typetab('landings').' set dir=\'\';');
    redirect('/?page=admin/landings/index');
}

function admin_submit_edit_landing()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    foreach ($_POST as $k => $v) {
        $var_norm[$k] = check_input($v);
    }
    $var = null_if_empty($var_norm);
    // pre($var);
    query('update '.tabname('shop', 'landings').' set
		name='.$var['name'].',
		description='.$var['description'].',
		dir='.trim($var['dir']).',
		pic='.$var['pic'].',
		item_id='.$var['item_id'].',
		set_id='.$var['set_id'].',
		show_index='.(isset($var['show_index']) ? 1 : 0).',
		show_advertisers='.(isset($var['show_advertisers']) ? 1 : 0).'
		where id='.$var['id'].'
		;');
    logger()->info('SHOP: �������������� ��������');
    redirect('/?page=admin/landings/index');
}

function admin_edit_landing()
{
    if (empty($_GET['id'])) {
        error('�� ������� id ��������');
    }
    $landing_id = getvar('id');
    $arr = query_assoc('select id,dir,item_id,set_id,items_group,show_index,show_advertisers,
		name,description,pic
		from '.typetab('landings').'
		where id=\''.$landing_id.'\'
		;');
    // pre($arr);
    $all_items = query_arr('select id,name from '.tabname('shop', 'items').' order by show_admin desc,name;');
    $select_items = get_select_option_list(['array' => $all_items, 'value' => 'id', 'selected_value' => $arr['item_id'], 'text' => 'name', 'null_value' => '']);
    $all_sets = query_arr('select id,name from '.tabname('shop', 'sets').' order by name;');
    $select_sets = get_select_option_list(['array' => $all_sets, 'value' => 'id', 'selected_value' => $arr['set_id'], 'text' => 'name', 'null_value' => '']);
    temp_var('id', $arr['id']);
    temp_var('dir', $arr['dir']);
    temp_var('list_items', $select_items);
    temp_var('list_sets', $select_sets);
    temp_var('items_group', $arr['items_group']);
    temp_var('show_index', $arr['show_index'] ? 'checked' : '');
    temp_var('show_advertisers', $arr['show_advertisers'] ? 'checked' : '');
    temp_var('name', $arr['name']);
    temp_var('description', htmlspecialchars($arr['description'], ENT_QUOTES, 'cp1251'));
    temp_var('pic', $arr['pic']);
    $content = template('admin/landings/block_edit');

    return $content;
}

function admin_landings_list()
{
    return get_list_landings([
        'country' => false,
        'template' => 'admin/landings/list',
        'only_with_item_id' => false,
        'show_index' => 'all',
        'show_advertisers' => 'all',
        'order' => 'order by landings.id desc',
    ]);
}

function admin_landings_list_old()
{
    $arr = query_arr('select landings.id,landings.dir,landings.item_id,landings.set_id,
		landings.items_group,landings.show_index,landings.show_advertisers,landings.name,
		landings.description,landings.pic,sets.name as set_name,items.name as item_name
		from '.typetab('landings').'
		left join '.typetab('items').' on items.id=landings.item_id
		left join '.typetab('sets').' on sets.id=landings.set_id
		order by id desc
		;');
    $list = '';
    foreach ($arr as $v) {
        temp_var('id', $v['id']);
        temp_var('dir', $v['dir']);
        temp_var('item_id', $v['item_id'] ? '#'.$v['item_id'] : '<span class="glyphicon glyphicon-remove"></span>');
        temp_var('item_name', $v['item_name'] ? $v['item_name'] : '');
        temp_var('set_name', $v['set_name'] ? $v['set_name'] : '');
        temp_var('set_id', $v['set_id'] ? '#'.$v['set_id'] : '<span class="glyphicon glyphicon-remove"></span>');
        temp_var('items_group', $v['items_group']);
        temp_var('show_index', '<span class="glyphicon glyphicon-'.($v['show_index'] ? 'ok' : 'remove').'"></span>');
        temp_var('show_advertisers', '<span class="glyphicon glyphicon-'.($v['show_advertisers'] ? 'ok' : 'remove').'"></span>');
        temp_var('name', $v['name']);
        // temp_var('description',mb_strimwidth($v['description'],0,100,'...','windows-1251'));
        temp_var('description', $v['description']);
        temp_var('pic', $v['pic'] ? $v['pic'] : 'no-image-placeholder.png');
        $list .= template('admin/landings/list');
        clear_temp_vars();
    }

    return $list;
}

function admin_delete_domain()
{
    $domain = empty($_GET['domain']) ? error('�� ������� �����') : checkstr($_GET['domain']);
    query('delete from '.typetab('domain_config').' where domain=\''.$domain.'\';');
    query('delete from '.tabname('engine', 'domains').' where domain=\''.$domain.'\';');
    redirect('/?page=admin/domains/domains');
}

function admin_submit_add_domain()
{
    $domain = empty($_POST['domain']) ? error('�� ������� �����') : checkstr($_POST['domain']);
    $domain = str_replace(['http://', 'https://'], '', $domain);
    $parse = parse_url('http://'.$domain);
    $domain = empty($parse['host']) ? error('������ � �������� ������') : $parse['host'];
    query('insert into '.tabname('engine', 'domains').' set domain=\''.$domain.'\', owner=\'me\', type=\'shop\', rewrite_function=\'landing_rewrite_query\';');
    query('insert into '.typetab('domain_config').' set domain=\''.$domain.'\',show_advertiser=0;');
    redirect('/?page=admin/domains/domains');
}

// ������ ������� ��������
function admin_domains()
{
    $arr = query_arr('
		select domain,default_advertiser,info,show_advertiser
		from '.typetab('domain_config').'
		order by default_advertiser,domain
		;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '
		<tr>
		<td><a href="http://'.$v['domain'].'">'.$v['domain'].'</a>
		<a href="/?page=admin/domains/delete_domain&amp;domain='.$v['domain'].'" style="color: red;" onclick="if (!confirm(\'������� �����?\')) {return false;};"><span class="glyphicon glyphicon-remove"></span></a></td>
		<td>'.$v['default_advertiser'].'</td>
		<td>'.($v['show_advertiser'] ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '').'</td>
		<td>'.$v['info'].'</td>
		</tr>
		';
    }
    $content = '<table class="table table-striped">
	<thead>
	<tr>
	<th>�����</th><th>�������������</th><th>���������� �������������</th><th>����</th>
	</tr>
	</thead>
	'.$list.'
	</table>';

    return $content;
    // temp_var('domains_login',$list['login']);
    // temp_var('domains_default',$list['default']);
    // return template('admin/advertiser/block_domains');
}

// ������� ������ �������. � �������� �� ������, �� ������ � ���������
function shop_cron_buyout_by_cost($args = [])
{
    // ������ �� ��������������, ���������� � �������
    query('truncate '.typetab('buyouts').';');
    $arr = query('
		insert into '.typetab('buyouts').' (item_id,advertiser,utm_source,q_paid,q_delivered)
		select packages.item_id,orders.advertiser,orders.utm_source,
		sum(if(orders.status in (\'paid\',\'take payment\'),packages.cost_kzt*packages.q,0)) as q_paid,
		sum(if(orders.status in (\'paid\',\'take payment\',\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),packages.cost_kzt*packages.q,0)) as q_delivered
		from '.typetab('packages').'
		join '.typetab('orders').' on orders.id=packages.order_id
		group by packages.item_id,orders.advertiser,orders.utm_source
		');
    // pre($arr);
    logger()->info('����� �� �������� ��������');
}

// ������� ������ �������. � �������� �� ������, �� ������ � ���������
function shop_cron_buyout($args = [])
{
    // ������ �� ��������������, ���������� � �������
    query('truncate '.typetab('buyouts').';');
    $arr = query('
		insert into '.typetab('buyouts').' (item_id,advertiser,utm_source,q_paid,q_delivered)
		select packages.item_id,orders.advertiser,orders.utm_source,
		sum(if(orders.status in (\'paid\',\'take payment\'),packages.q,0)) as q_paid,
		sum(if(orders.status in (\'paid\',\'take payment\',\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),packages.q,0)) as q_delivered
		from '.typetab('packages').'
		join '.typetab('orders').' on orders.id=packages.order_id
		group by packages.item_id,orders.advertiser,orders.utm_source
		');
    // pre($arr);
    logger()->info('����� �� �������� ��������');
}

// $args=array();
// $args['advertiser']='';
// $args['utm_source']='targetmailru';
// $args['item_id']='16';
// pre(var_dump($args,item_buyouts($args)));

// ���������� ������ �������
function item_buyouts($args)
{
    $item_id = empty($args['item_id']) ? error('�� ������� item_id') : $args['item_id'];
    $advertiser = empty($args['advertiser']) ? false : $args['advertiser'];
    $utm_source = empty($args['utm_source']) ? false : $args['utm_source'];
    $sql['item_id'] = 'item_id=\''.$item_id.'\'';
    $sql['advertiser'] = ($advertiser === false) ? 'advertiser is null' : 'advertiser=\''.$advertiser.'\'';
    $sql['utm_source'] = ($utm_source === false) ? 'utm_source=\'\'' : 'utm_source=\''.$utm_source.'\'';
    // $sql['utm_source']=($args['advertiser']===false)?(($args['utm_source']===false)?'utm_source=\'\'':'utm_source=\''.$args['utm_source'].'\''):1;
    // $sql['by_sources']['utm_source']=($args['advertiser']===false)?(($args['utm_source']===false)?'utm_source=\'\'':'utm_source=\''.$args['utm_source'].'\''):'1=2';
    // pre($sql);
    /*
    $arr['buyouy_by_advertiser_and_item']=query_assoc('select buyout from buyout_by_advertisers_and_items where '.$sql['advertiser'].' and item_id=\''.$item_id.'\';');
    $arr['buyout_by_items']=query_assoc('select buyout from buyout_by_advertisers_and_items where '.$sql['advertiser'].' and item_id=\''.$item_id.'\';');
    $arr['buyout_by_advertisers']=query_assoc('select buyout from buyout_by_advertisers where '.$sql['advertiser'].';');
    $arr['buyout_general']=query_assoc('select buyout from buyout_general;');
    */
    $arr = query_assoc('
		select
		(select round(ifnull(q_paid/q_delivered,0)*100) as buyout from '.typetab('buyouts').' where '.$sql['advertiser'].' and '.$sql['utm_source'].' and '.$sql['item_id'].' and q_paid>10) as by_advertiser_source_item,
		(select round(ifnull(sum(q_paid)/sum(q_delivered),0)*100) as buyout from '.typetab('buyouts').' where '.$sql['advertiser'].' and '.$sql['item_id'].' having sum(q_paid)>10) as by_advertiser_item,
		(select round(ifnull(sum(q_paid)/sum(q_delivered),0)*100) as buyout from '.typetab('buyouts').' where '.$sql['advertiser'].' and '.$sql['utm_source'].' having sum(q_paid)>10) as by_advertiser_source,
		(select round(ifnull(sum(q_paid)/sum(q_delivered),0)*100) as buyout from '.typetab('buyouts').' where '.$sql['advertiser'].' having sum(q_paid)>10) as by_advertiser,
		(select round(ifnull(sum(q_paid)/sum(q_delivered),0)*100) as buyout from '.typetab('buyouts').' where '.$sql['item_id'].' having sum(q_paid)>10) as by_item,
		(select round(ifnull(sum(q_paid)/sum(q_delivered),0)*100) as buyout from '.typetab('buyouts').' having sum(q_paid)>10) as general
		;');

    // pre($arr);
    // var_dump($arr);
    return $arr;
}

// ���������� �������������� ����� �� ��������� ������������� � ������
// ��������� advertiser - ��������������, item_id - ������������
function item_buyout($args)
{
    $arr = cache('item_buyouts', $args, 0);
    $regularity_and_translation = [
        'by_advertiser_source_item' => '�� ����., ����� � ������',
        'by_advertiser_item' => '�� ����. � ������',
        'by_item' => '�� ������',
        'by_advertiser_source' => '�� ����. � �����',
        'by_advertiser' => '�� ����.',
        'general' => '�����',
    ];

    foreach ($regularity_and_translation as $k => $v) {
        if ($arr[$k] === null) {
            continue;
        }
        $return['type'] = $k;
        $return['type_ru'] = $v;
        $return['buyout'] = $arr[$k];
        break;
    }
    if (empty($return)) {
        error('���������� ������� ������');
    }

    return $return;
}

// ���������� ������ �������
function item_buyouts_old($args)
{
    $sql['advertiser'] = empty($args['advertiser']) ? 'advertiser is null' : 'advertiser=\''.$args['advertiser'].'\'';
    $item_id = empty($args['item_id']) ? error('�� ������� item_id') : $args['item_id'];
    /*
    $arr['buyouy_by_advertiser_and_item']=query_assoc('select buyout from buyout_by_advertisers_and_items where '.$sql['advertiser'].' and item_id=\''.$item_id.'\';');
    $arr['buyout_by_items']=query_assoc('select buyout from buyout_by_advertisers_and_items where '.$sql['advertiser'].' and item_id=\''.$item_id.'\';');
    $arr['buyout_by_advertisers']=query_assoc('select buyout from buyout_by_advertisers where '.$sql['advertiser'].';');
    $arr['buyout_general']=query_assoc('select buyout from buyout_general;');
    */
    $arr = query_assoc('
		select
		(select buyout from '.typetab('buyout_by_advertisers_and_items').' where '.$sql['advertiser'].' and item_id=\''.$item_id.'\') as by_advertisers_and_items,
		(select buyout from '.typetab('buyout_by_items').' where item_id=\''.$item_id.'\') as by_items,
		(select buyout from '.typetab('buyout_by_advertisers').' where '.$sql['advertiser'].') as by_advertisers,
		(select buyout from '.typetab('buyout_general').') as general;
		');

    // pre($arr);
    // var_dump($arr);
    return $arr;
}

function item_buyout_old($args)
{
    $arr = cache('item_buyouts_old', $args, 0);
    $regularity_and_translation = [
        'by_advertisers_and_items' => '�� ����. � ������',
        'by_items' => '�� ������',
        'by_advertisers' => '�� ����.',
        'general' => '�����',
    ];
    foreach ($regularity_and_translation as $k => $v) {
        if ($arr[$k] === null) {
            continue;
        }
        $return['type'] = $k;
        $return['type_ru'] = $v;
        $return['buyout'] = $arr[$k];
        break;
    }
    if (empty($return)) {
        error('���������� ������� ������');
    }

    return $return;
}

function admin_submit_edit_bonuses()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    $arr = empty($_POST['users']) ? error('�� ������� ������ ��������������') : $_POST['users'];
    $item_id = empty($_POST['item_id']) ? error('�� ������� ID ������') : $_POST['item_id'];
    $cost_confirm = isset($_POST['cost_confirm']) ? $_POST['cost_confirm'] : error('�� �������� ��������� ������������� �� ���������');
    foreach ($arr as $advertiser => $v) {
        if ($v['shave_percent'] == '') {
            $v['shave_percent'] = 0;
        }
        $v2 = null_if_empty($v);
        // pre($v2);
        query('insert into '.tabname('shop', 'bonuses').' set
			item_id=\''.$item_id.'\',
			advertiser=\''.$advertiser.'\',
			bonus='.$v2['bonus'].',
			action='.$v2['action'].',
			shave_percent='.$v2['shave_percent'].'
			on duplicate key update
			bonus='.$v2['bonus'].',
			action='.$v2['action'].',
			shave_percent='.$v2['shave_percent'].'
			;');
    }
    query('update '.typetab('items').' set cost_confirm=\''.$cost_confirm.'\' where id=\''.$item_id.'\';');

    // logger()->info('SHOP: �������������� �������');
    return '���������';
}

function admin_edit_bonuses()
{
    $item_id = empty($_GET['item_id']) ? error('�� ������� item_id') : $_GET['item_id'];
    $item_info = preparing_item_info(['id' => $item_id]);
    $arr = query_arr('
		select users.login,bonuses.bonus,bonuses.action,bonuses.shave_percent
		from '.typetab('users').'
		left join '.tabname('shop', 'bonuses').' on bonuses.advertiser=users.login and bonuses.item_id=\''.$item_id.'\'
		where users.level=\'advertiser\'
		order by users.login
		;');
    // pre($arr);
    $list = '';
    foreach ($arr as $v) {
        $v['action'] = empty($v['action']) ? 'confirm' : $v['action'];
        $selected = ['new' => '', 'confirm' => '', 'pay' => ''];
        $selected[$v['action']] = 'selected="selected"';
        // pre(var_dump($selected));
        $list .= '
		<div class="form-group">
		<label class="col-md-3">'.$v['login'].'</label>
		<div class="col-md-2">
		<input type="text" name="users['.$v['login'].'][bonus]" value="'.$v['bonus'].'" placeholder="��������������" class="form-control">
		</div>
		<div class="col-md-3">
		<select class="form-control" name="users['.$v['login'].'][action]">
		<option value="new" '.$selected['new'].'>�����</option>
		<option value="confirm" '.$selected['confirm'].'>�������������</option>
		<option value="pay" '.$selected['pay'].'>������</option>
		</select>
		</div>
		<div class="col-md-3">
		<input type="text" name="users['.$v['login'].'][shave_percent]" value="'.$v['shave_percent'].'" placeholder="% �����" class="form-control">
		</div>
		</div>
		';
    }

    $content = '
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">�������������� ������� ��� ������ #'.$item_id.'</h4>
	</div>
	<div class="modal-body">
	<h4>'.$item_info['name'].'</h4>
	<form class="form-horizontal" id="editBonuses">
	<div class="form-group">
	<label class="col-md-3">�� ���������</label>
	<div class="col-md-2">
	<input type="text" name="cost_confirm" value="'.$item_info['cost_confirm'].'" placeholder="�������������� �� ���������" class="form-control">
	</div>
	<div class="col-md-3">
	�������������
	</div>
	</div>
	'.$list.'
	<input type="hidden" name="item_id" value="'.$item_id.'">
	</form>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
	<button type="button" class="btn btn-primary" data-role="ajax_submit_edit_bonuses" id="result" data-loading-text="����������..." autocomplete="off">���������</button>
	</div>
	';

    return $content;
}

function admin_payments()
{
    // ������� ������ ������ �� ������� ���� ����������
    $arr_login = query_arr('
		select users.login
		from '.typetab('users').'
		join (select login,count(*) as q from '.typetab('purchase').' group by login) p on p.login=users.login
		where level in (\'admin\',\'advertiser\') and p.q!=0
		order by level,login
		;');
    $arr_all = query_arr('select level,currency,sum(sum) as sum
		from '.typetab('purchase').'
		join '.typetab('users').' on users.login=purchase.login
		where level in (\'admin\',\'advertiser\')
		group by level,currency
		;');
    $list_all = '';
    foreach ($arr_all as $v) {
        $list_all .= '<p>����� '.$v['level'].' '.$v['currency'].': '.$v['sum'].' '.$v['currency'].'</p>';
    }
    $list_user = '';
    foreach ($arr_login as $login) {
        $arr = query_arr('select date(date) as day,sum,currency,text
			from '.typetab('purchase').'
			where login=\''.$login['login'].'\'
			order by date
			;');
        $arr_sum = query_arr('select currency,sum(sum) as sum
			from '.typetab('purchase').'
			where login=\''.$login['login'].'\'
			group by currency
			;');
        $list = '';
        foreach ($arr as $v) {
            $list .= '<tr><td style="width: 100px;">'.$v['day'].'</td><td>'.$v['sum'].'</td><td>'.$v['currency'].'</td><td>'.$v['text'].'</td></tr>';
        }
        $list_sum = '';
        foreach ($arr_sum as $v) {
            $list_sum .= '<tr><td>�����:</td><td>'.$v['sum'].'</td><td>'.$v['currency'].'</td><td></td></tr>';
        }
        $list_user .= '
		<div class="col-md-6">
		<h2>'.$login['login'].'</h2>
		<table class="table table-striped">
		<thead><tr><th>����</th><th>�����</th><th>������</th><th>�����������</th></tr></thead>
		'.$list.'
		'.$list_sum.'
		</table>
		</div>
		';
        $i = empty($i) ? 1 : ($i + 1);
        $list_user .= ($i % 2) ? '' : '</div><div class="row">';
    }
    $content = '
	<div class="row">
	'.$list_user.'
	</div>
	<div class="row">
	<div class="col-md-12">
	'.$list_all.'
	</div>
	</div>
	';

    return $content;
}

function test_imap()
{
    $host = 'imap.mail.ru';
    $port = 993;
    $login = 'kovalev_mn@list.ru';
    $pass = 'koval111';
    $param = '/imap/ssl';
    $folder = 'INBOX';
    if ($mbox = imap_open('{'."{$host}:{$port}{$param}"."}$folder", $login, $pass)) {
        echo 'Connectedn';
    } else {
        exit("Can't connect: ".imap_last_error().'n');
        echo 'FAIL!n';
    }
    // $mbox = imap_open ("{imap.mail.ru:110}INBOX", "kovalev_mn@list.ru", "koval111");
    pre(var_dump($mbox));
}
// test_imap();

function preparing_groups_for_item($args)
{
    $args['item_id'] = empty($args['item_id']) ? error('�� ������� item_id') : $args['item_id'];
    $args['type'] = empty($args['type']) ? 'all' : $args['type'];
    $where['type'] = ($args['type'] === 'all') ? '1' : 'item_groups.type=\''.$args['type'].'\'';
    $arr = query_arr('select items_groups.group_id,items_groups.name
		from '.typetab('items_groups').'
		join '.typetab('item_groups').' on item_groups.id=items_groups.group_id
		where items_groups.item_id=\''.$args['item_id'].'\' and '.$where['type'].';');

    // pre($arr);
    return $arr;
}

function preparing_item_groups($args)
{
    $args['type'] = empty($args['type']) ? false : $args['type'];
    $args['public'] = isset($args['public']) ? $args['public'] : 'all';
    // pre($args);
    $where['type'] = $args['type'] ? 'type=\''.$args['type'].'\'' : '1';
    $where['public'] = ($args['public'] === 'all') ? '1' : ($args['public'] ? 'public' : 'not public');
    // pre($where);
    $arr = query_arr('select id,name from '.typetab('item_groups').' where '.$where['type'].' and '.$where['public'].';');

    // pre($arr);
    return $arr;
}

// preparing_item_groups(array('type'=>'group'));

function admin_item_groups_list($args)
{
    $args['item_id'] = empty($args['item_id']) ? error('�� ������� item_id') : $args['item_id'];
    $arr = preparing_groups_for_item(['item_id' => $args['item_id'], 'type' => 'groups']);
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li>'.$v['name'].'
		<span class="del-group" data-role="ajax_del_group_from_item" data-group_id="'.$v['group_id'].'" data-item_id="'.$args['item_id'].'">x</span>
		</li>';
    }
    temp_var('item_groups_list', $list);

}

// ���������� ������� �� �������������� � ����������
function shop_cron_advertisers_sources_stat()
{
    query('truncate table '.typetab('sources_stat').';');
    query('
		insert into '.typetab('sources_stat').' (advertiser,source,percent)
		(
			select advertiser,utm_source,
			round(sum(if(orders.status in (\'paid\',\'take payment\'),1,0))/
				sum(if(orders.status in (\'paid\',\'take payment\',\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),1,0))*100) as percent
	from '.typetab('orders').'
	group by advertiser,utm_source
	order by advertiser,utm_source
	)
	;');
    $rows = mysql_affected_rows();
    logger()->info('SHOP: �������� ���������� ������. ��������� �����: '.$rows);
    // $arr=readdata($data,'utm_source');
    // var_dump($arr);
    // pre($arr);
    // foreach ($arr as $k=>$v) {
    // 	$arr[$k]['paid_percent']=round($v['q_paid']/((($v['q_not_paid']+$v['q_paid'])==0)?1:($v['q_not_paid']+$v['q_paid']))*100);
    // };
    // pre($arr);
    // return $arr;
}

function items_groups_list()
{
    // ������� ������ ��� ��������� �����
    $arr = query_arr('select id,name,
		(select count(*) from '.typetab('items_groups').' join '.typetab('item_groups').' on item_groups.id=items_groups.group_id where item_groups.public and items_groups.item_id=items.id) as q_public
		from '.typetab('items').'
		where items.show_admin=\'1\'
		having q_public=0
		;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li>'.$v['name'].' #'.$v['id'].'</li>';
    }
    $items_without_public_group = $list;
    //
    $post_text = '';
    if (! empty($_POST['item_id'])) {
        $item_id = empty($_POST['item_id']) ? error('��� item_id') : $_POST['item_id'];
        $group_id = empty($_POST['group_id']) ? error('��� group_id') : $_POST['group_id'];
        query('insert into '.typetab('items_groups').' set group_id='.$group_id.', item_id='.$item_id.';');
        $post_text = '���������';
    }
    $ungrouped = query_arr('select items.id,name,cost_kzt
			from '.typetab('items').'
			left join (
				select items_groups.* from '.typetab('items_groups').'
				join '.typetab('item_groups').' on item_groups.id=items_groups.group_id
				where item_groups.type=\'offer\'
				) items_offers on items.id=items_offers.item_id
			where items_offers.group_id is null
			order by name;');
    $ungrouped_list = '';
    foreach ($ungrouped as $v) {
        $ungrouped_list .= '
		<li>
		<form class="form-inline" action="" method="post">
		'.$v['name'].' '.$v['cost_kzt'].' #'.$v['id'].'
		<div class="form-group">
		<input class="form-control" style="width: 70px;" type="text" name="group_id" value="" placeholder="group">
		</div>
		<input type="hidden" name="item_id" value="'.$v['id'].'">
		<input class="btn btn-default" type="submit" value="OK">
		</form>
		</li>';
    }
    $groups = query_arr('select id,name from '.typetab('item_groups').' where type=\'offer\';');
    $list = '';
    foreach ($groups as $group) {
        $arr = query_arr('select group_id,item_id,name,cost_kzt
			from '.typetab('items_groups').'
			join '.typetab('items').' on items.id=items_groups.item_id
			where group_id='.$group['id'].';');
        $items = '';
        foreach ($arr as $v) {
            $items .= '<li>'.$v['name'].' '.$v['cost_kzt'].' #'.$v['item_id'].'</li>';
        }
        $list .= '<li>'.$group['name'].' #'.$group['id'].'<ul>'.$items.'</ul></li>';
    }
    $content = '
	������ ��� ��������� �����: <ul>'.$items_without_public_group.'</ul>
	'.$post_text.'
	������ ��� �������:
	<ul>'.$ungrouped_list.'</ul>
	<ul>'.$list.'</ul>
	';

    return $content;
}

function submit_return_request_set_order_id()
{
    $id = empty($_POST['id']) ? error('�� ������� ID ������� �� �������') : $_POST['id'];
    $order_id = empty($_POST['order_id']) ? error('�� ������� ID ������') : $_POST['order_id'];
    query('update '.typetab('returns').' set order_id=\''.$order_id.'\' where id=\''.$id.'\';');
    redirect('/?page=admin/returns/request&id='.$id);
}

function return_request_delete()
{
    $id = empty($_GET['id']) ? error('�� ������� ID') : $_GET['id'];
    query('delete from '.typetab('returns').' where id=\''.$id.'\';');
    alert('������ �� ������� #'.$id.' ������. <a href="/?page=admin/returns/requests">������� �� �������</a>');
}

function return_request()
{
    global $config;
    $id = empty($_GET['id']) ? error('�� ������� ID') : $_GET['id'];
    $return_dir = '/types/'.$config['type'].'/files/returns/'.$id.'/';
    $return_dir_full = $config['path']['server'].'types/'.$config['type'].'/files/returns/'.$id.'/';
    $types = ['id', 'bill', 'box', 'item_1', 'item_2', 'item_3'];
    $arr = query_assoc('select returns.order_id,returns.name,returns.phone,returns.item,
		returns.reason,orders.name as order_name
		from '.typetab('returns').'
		left join '.typetab('orders').' on orders.id=returns.order_id
		where returns.id=\''.$id.'\';');
    if ($arr['order_id'] !== null) {

    }
    temp_var('js_order_id', (($arr['order_id'] === null) ? 'false' : $arr['order_id']));
    temp_var('js_order_name', (($arr['order_id'] === null) ? 'false' : '\''.$arr['order_name'].'\''));
    temp_var('order_id', ($arr['order_id'] ? $arr['order_id'] : ''));
    temp_var('order_name', ($arr['order_name'] ? $arr['order_name'] : ''));
    temp_var('name', $arr['name']);
    temp_var('phone', $arr['phone']);
    temp_var('item', $arr['item']);
    temp_var('reason', $arr['reason']);
    foreach ($types as $v) {
        temp_var('photo_'.$v, (file_exists($return_dir_full.$v.'.png') ? '<img src="'.$return_dir.$v.'.png'.'">' : '���'));
    }
    $content = template('admin/returns/block_return');

    return $content;
}

function return_request_old()
{
    $id = empty($_GET['id']) ? error('�� ������� ID') : $_GET['id'];
    $arr = query_assoc('select returns.order_id,returns.text,orders.name as order_name
		from '.typetab('returns').'
		left join '.typetab('orders').' on orders.id=returns.order_id
		where returns.id=\''.$id.'\';');
    if ($arr['order_id'] !== null) {

    }

    return str_replace(['</form>', '<form method="post">'], '', $arr['text']).'
	<script>
	var order_id='.(($arr['order_id'] === null) ? 'false' : $arr['order_id']).';
	var order_name='.(($arr['order_id'] === null) ? 'false' : '\''.$arr['order_name'].'\'').';
	</script>';
}

function return_requests()
{
    global $config;
    $arr = query_arr('select returns.id,returns.order_id,returns.date,returns.name,orders.name as order_name,
		orders.status as order_status, (photo_id or photo_bill or photo_box or photo_item_1 or photo_item_2 or photo_item_3) as is_photo
		from '.tabname('shop', 'returns').'
		left join '.typetab('orders').' on orders.id=returns.order_id
		order by returns.date;');
    $rus['statuses'] = shop_translation('order_status');
    $types = ['id', 'bill', 'box', 'item_1', 'item_2', 'item_3'];
    $list = '';
    foreach ($arr as $v) {
        // ����������� � ���� �����
        /*
        foreach ($types as $t) {
            $file=$config['path']['server'].'types/'.$config['type'].'/files/returns/'.$v['id'].'/'.$t.'.png';
            if (file_exists($file)) {
                query('update '.typetab('returns').' set photo_'.$t.'=1 where id=\''.$v['id'].'\';');
            };
        };
        */
        $order_info = '';
        if ($v['order_id'] !== null) {
            $order_info = '<a href="/?page=admin/orders&status=all&order_id='.$v['order_id'].'">#'.$v['order_id'].'</a>,
			'.$v['order_name'].',
			'.$rus['statuses'][$v['order_status']].'
			'.(($v['order_status'] == 'refund paid') ? '<span class="glyphicon glyphicon-ok"></span>' : '').'
			';
        } else {
            $order_info = '<span class="name-gray">'.$v['name'].'</span>';
        }
        $list .= '<li><a href="/?page=admin/returns/request&amp;id='.$v['id'].'">'.$v['date'].'</a>
		'.($v['is_photo'] ? '<span class="glyphicon glyphicon-camera"></span>' : '').'
		 '.$order_info.'
		 </li>';
    }

    return $list;
}

function return_requests_old()
{
    $arr = query_arr('select returns.id,returns.order_id,returns.date,orders.name as order_name,
		orders.status as order_status,returns_new.transfered2
		from '.tabname('shop', 'returns').'
		left join '.typetab('orders').' on orders.id=returns.order_id
		left join '.typetab('returns_new').' on returns_new.id=returns.id
		order by returns.date;');
    $rus['statuses'] = shop_translation('order_status');
    $list = '';
    foreach ($arr as $v) {
        $order_info = '';
        if ($v['order_id'] !== null) {
            $order_info = '<a href="/?page=admin/orders&status=all&order_id='.$v['id'].'">#'.$v['order_id'].'</a>,
			'.$v['order_name'].',
			'.$rus['statuses'][$v['order_status']].'
			'.(($v['order_status'] == 'refund paid') ? '<span class="glyphicon glyphicon-ok"></span' : '').'
			';
        }
        $list .= '<li>'.($v['transfered2'] ? '+' : '').'<a href="/?page=admin/returns/request&amp;id='.$v['id'].'">'.$v['date'].'</a> '.$order_info.'</li>';
    }

    return $list;
}

function landing_pos()
{
    $arr = query_arr('select name,dir,position,show_index from '.typetab('landings').' order by position;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li class="disabled">
		<a href="/?page=admin/change_landing_pos&amp;dir='.$v['dir'].'&amp;action=up"><span class="glyphicon glyphicon-arrow-up"></span></a>
		<a href="/?page=admin/change_landing_pos&amp;dir='.$v['dir'].'&amp;action=down"><span class="glyphicon glyphicon-arrow-down"></span></a>
		<a href="http://elitbrand.com/'.$v['dir'].'" style="color: '.($v['show_index'] ? '#333' : '#999').';">'.$v['name'].'</a>
		</li>';
    }
    $content = '<ol>'.$list.'</ol>';

    return $content;
    // pre($arr);
    // query('update '.typetab('landings').' set position=\''.$pos.'\';');
}

function change_landing_pos()
{
    $dir = empty($_GET['dir']) ? error('�� ������� �������') : $_GET['dir'];
    $action = empty($_GET['action']) ? error('�� �������� ��������') : $_GET['action'];
    $arr = query_assoc('select position from '.typetab('landings').' where dir=\''.$dir.'\';');
    $arr2 = query_assoc('select max(position) as max_pos from '.typetab('landings').';');
    $old_pos = $arr['position'];
    if ($action == 'up') {
        $new_pos = $old_pos - 1;
    }
    if ($action == 'down') {
        $new_pos = $old_pos + 1;
    }
    if ((int) $new_pos < 1) {
        error('������� �� ����� ���� ������ 1');
    }
    if ((int) $new_pos > $arr2['max_pos']) {
        error('������� �� ����� ���� ������ '.$arr2['max_pos']);
    }
    query('update '.typetab('landings').' set position=null where position='.$new_pos.';');
    query('update '.typetab('landings').' set position='.$new_pos.' where dir=\''.$dir.'\';');
    query('update '.typetab('landings').' set position='.$old_pos.' where position is null;');
    redirect('/?page=admin/landing_pos');
}

// ���������� ������� �� ����������
function preparing_sources_stat()
{
    $data = query('select utm_source,
		sum(if(orders.status in (\'paid\',\'take payment\'),1,0)) as q_paid,
		sum(if(orders.status in (\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),1,0)) as q_not_paid
		from '.typetab('orders').'
		group by utm_source
		');
    $arr = readdata($data, 'utm_source');
    foreach ($arr as $k => $v) {
        $arr[$k]['paid_percent'] = round($v['q_paid'] / ((($v['q_not_paid'] + $v['q_paid']) == 0) ? 1 : ($v['q_not_paid'] + $v['q_paid'])) * 100);
    }

    // pre($arr);
    return $arr;
}

// preparing_sources_stat();

function advertiser_source_stat()
{
    $sql_item_groups = empty($_GET['group_id']) ? '' : '
	join '.typetab('packages').' on packages.order_id=orders.id
	join '.typetab('items').' on items.id=packages.item_id
	where items.id in (select item_id from '.typetab('items_groups').' where group_id='.$_GET['group_id'].')
	';
    $arr = query_arr('select advertiser,if(advertiser is null,utm_source,\'\') as utm_source,(if(advertiser is null,utm_source,advertiser)) as advertiser_or_source,
		ifnull(round((sum(if(status not in (\'new\',\'unavailable\',\'canceled\'),1,0))/sum(if(status not in (\'new\',\'unavailable\'),1,0)))*100),0) as confirmed,
		ifnull(round((sum(if(status in (\'paid\',\'take payment\'),1,0))/sum(if(status in (\'paid\',\'take payment\',\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),1,0)))*100),0) as paid,
		ifnull(round((sum(if(status in (\'paid\',\'take payment\',\'refund\',\'refund paid\'),1,0))/sum(if(status in (\'paid\',\'take payment\',\'refund\',\'refund paid\',\'delivered not paid\',\'return\',\'return paid\',\'canceled\'),1,0)))*100),0) as new_paid,
		sum(if(status in (\'paid\',\'take payment\'),1,0)) as q_paid
		from '.typetab('orders').'
		'.$sql_item_groups.'
		group by advertiser_or_source
		/* having q>20 */
		order by advertiser,paid desc,confirmed desc,new_paid desc;
		');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr>
		<td>'.(empty($v['advertiser']) ? '' : '<span class="glyphicon glyphicon-user"></span>').' '.$v['advertiser'].'</td>
		<td>'.$v['utm_source'].'</td>
		<td>'.$v['paid'].'%</td><td>'.$v['confirmed'].'%</td>
		<td>'.$v['new_paid'].'%</td><td>'.$v['q_paid'].'</td>
		</tr>';
    }
    $content = '
	<h3>������������� � ���������</h3>
	<table class="table table-striped">
	<thead><tr><th>�������������</th><th>�����</th><th>�����</th><th>�������������</th><th>������������� � �����</th><th>������� ���������</th></thead>
	'.$list.'
	</table>';

    return $content;
}

// ������� ������ �������. � �������� �� ������, �� ������ � ���������
function preparing_items_stat($args = [])
{
    $sql['where']['item_id'] = 1;
    if (! empty($args['item_id'])) {
        $sql['where']['item_id'] = 'packages.item_id=\''.$args['item_id'].'\'';
    }
    // ����� ����� ������� �� �������
    $arr['all'] = readdata(query('
		select item_id,
		sum(if(orders.status in (\'paid\',\'take payment\'),q,0)) as q_paid,
		sum(if(orders.status in (\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),q,0)) as q_not_paid
		from '.typetab('packages').'
		join '.typetab('orders').' on orders.id=packages.order_id
		where '.$sql['where']['item_id'].'
		group by item_id
		'), 'item_id');
    // ������ �� ����������
    $arr['by_sources_ungrouped'] = query_arr('
		select item_id,utm_source,
		sum(if(orders.status in (\'paid\',\'take payment\'),q,0)) as q_paid,
		sum(if(orders.status in (\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),q,0)) as q_not_paid
		from '.typetab('packages').'
		join '.typetab('orders').' on orders.id=packages.order_id
		where '.$sql['where']['item_id'].'
		group by item_id,utm_source
		');
    // ������ �� ��������������
    $arr['by_advertisers_ungrouped'] = query_arr('
		select packages.item_id,orders.advertiser,
		round(
		sum(if(orders.status in (\'paid\',\'take payment\'),q,0))/
		sum(if(orders.status in (\'paid\',\'take payment\',\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\'),q,0))
		*100
		) as percent
		from '.typetab('packages').'
		join '.typetab('orders').' on orders.id=packages.order_id
		where '.$sql['where']['item_id'].'
		group by packages.item_id,orders.advertiser
		');
    // �������� ������ �� ���������� � ���� ������
    foreach ($arr['by_sources_ungrouped'] as $v) {
        $arr['by_sources'][$v['item_id']][$v['utm_source']] = [
            'q_paid' => $v['q_paid'],
            'q_not_paid' => $v['q_not_paid'],
            'paid_percent' => round($v['q_paid'] / ((($v['q_not_paid'] + $v['q_paid']) == 0) ? 1 : ($v['q_not_paid'] + $v['q_paid'])) * 100),
        ];
    }
    // ���������� ������ �� ���������� � ����� ����� �� �������
    foreach ($arr['all'] as $v) {
        $arr['all'][$v['item_id']]['paid_percent'] = round($v['q_paid'] / ((($v['q_not_paid'] + $v['q_paid']) == 0) ? 1 : ($v['q_not_paid'] + $v['q_paid'])) * 100);
        $arr['all'][$v['item_id']]['by_sources'] = $arr['by_sources'][$v['item_id']];
    }
    if (empty($args['item_id'])) {
        return $arr['all'];
    } else {
        return $arr['all'][$args['item_id']];
    }
}

function get_item_paid_percent($args)
{
    $item_id = empty($args['item_id']) ? error('�� ������� ID ������') : $args['item_id'];
    $source = empty($args['source']) ? '' : $args['source'];
    $paid_percent_common = 100 - value_from_config('shop', 'return_percent');
    $arr['items_stat'] = preparing_items_stat();
    $arr['sources_stat'] = preparing_sources_stat();
    $item_info = $arr['items_stat'][$item['id']];
    $paid_percent = false;
    // ���� ���� ����� ��� ������� ������ � ������� ������� ���������, � ���� ������� ��� ���� �� 5, �� �������, ���������� ��� �����
    if (isset($item_info['by_sources'][$v['utm_source']]) and $item_info['by_sources'][$v['utm_source']]['q_paid'] >= 5) {
        $paid_percent = $item_info['by_sources'][$v['utm_source']]['paid_percent'];
        // ���� ���, �� ��������� ���� �� ����� ����� �� ������� ������ � ���� �� 5 ��������
    } else {
        // ���� ���� �� ���������� ��� ����� �� ������
        if ($item_info['q_paid'] >= 5) {
            $paid_percent = $item_info['paid_percent'];
            // ���� ���, �� ���������� ����� ����� �� ���������
        } else {
            // ��������� ���� �� ����� �� ������� ��������� � ���� �� ��� ���� �� 5 �������
            if (isset($arr['sources_stat'][$v['utm_source']]) and $arr['sources_stat'][$v['utm_source']]['q_paid'] >= 5) {
                $paid_percent = $arr['sources_stat'][$v['utm_source']]['paid_percent'];
                // ���� ������ ���, �� ���������� ����� ������� ������ �� �������
            } else {
                $paid_percent = $paid_percent_common;
            }
        }
    }
    if (! $paid_percent) {
        error('�� �������� ������� ������, ���-�� ������');
    }
    // if (empty($args['source'])) {};
}

// pre(preparing_items_stat(array('item_id'=>2)));

// ��������� ���������� �� ������
function admin_item_detailed_stat()
{
    $item_id = empty($_GET['item_id']) ? error('�� ������� ID ������') : $_GET['item_id'];
    $arr = preparing_items_stat(['item_id' => $item_id]);
    $list = '';
    foreach ($arr['by_sources'] as $source => $v) {
        $list .= '<tr><td>'.$source.'</td><td>'.$v['paid_percent'].'% ('.$v['q_paid'].' �� '.($v['q_paid'] + $v['q_not_paid']).')</td></tr>';
    }

    return $list;
}

function potential_stat()
{
    $timezone = isset($_GET['timezone']) ? check_input($_GET['timezone']) : false;
    $sql['date']['where'] = empty($_GET['date']) ? '1' : 'date(orders.date) between \'2015-01-01\' and \''.$_GET['date'].'\'';
    // pre($_GET);
    $current_timezone = get_timezone();
    if ($timezone) {
        set_timezone($timezone);
    }
    // �������� ������ ������� � ����
    $arr['orders'] = query_arr('
		select orders.id,orders.date,orders.utm_source,order_package.q,order_package.cost_kzt,order_package.real_cost_kzt
		from '.typetab('orders').'
		left join (
			select order_id,count(*) as q,sum(cost_kzt*q) as cost_kzt,
			sum(item_cost_kzt*q) as real_cost_kzt
			from '.typetab('packages').'
			group by order_id
			) order_package on order_package.order_id=orders.id
		where orders.status in (\'packed\',\'check address\',\'waiting send\',\'sent\',\'delivered not paid\')
		and '.$sql['date']['where'].'
		order by orders.date
		;');
    // if ($timezone) {set_timezone('+6');};
    if ($timezone) {
        set_timezone_alt($current_timezone);
    }
    // ���-�� ���������� � ������������ ������� �� ��� ����� ����� � � ������� �� ����������
    $arr['items_stat'] = preparing_items_stat();
    $arr['sources_stat'] = preparing_sources_stat();
    // pre($arr['items_stat']);
    // ������ �� ��� �����
    $arr['paid'] = query_assoc('
		select sum(packages.cost_kzt*q) as cost_kzt from '.typetab('packages').'
		join '.typetab('orders').' on orders.id=packages.order_id
		where orders.status in (\'paid\',\'take payment\',\'refund\',\'refund paid\');
		');
    // ������� �� ��� �����
    $arr['expenses'] = query_assoc('
		select (select ifnull(sum(sum),0) from '.tabname('shop', 'purchase').' where currency=\'rur\') as rur,
		(select ifnull(sum(sum),0) from '.tabname('shop', 'purchase').' where currency=\'kzt\') as kzt
		;');
    // ������������� ������� ������� �������� � ������� �������
    $arr['items_by_statuses'] = query_assoc('
		select
		sum(if(status in (\'return\',\'refund\'),item_cost_kzt*q,0)) as items_returned_cost_kzt,
		sum(if(status in (\'delivered not paid\',\'sent\',\'waiting send\',\'packed\'),item_cost_kzt*q,0)) as items_confirmed_cost_kzt
		from '.typetab('packages').'
		join '.typetab('orders').' on packages.order_id=orders.id
		;');
    // ������ ������� ������
    $arr['items'] = query_arr('select id from '.tabname('shop', 'items').';');
    $arr['items_home'] = ['kzt' => 0, 'rur' => 0];
    foreach ($arr['items'] as $v) {
        $item_info = preparing_item_info(['id' => $v['id']]);
        if ($item_info['q_in_stock'] > 0) {
            $arr['items_home']['kzt'] = $arr['items_home']['kzt'] + ($item_info['q_in_stock'] - $item_info['q_waiting_send']) * $item_info['real_cost_kzt'];
            $arr['items_home']['rur'] = $arr['items_home']['rur'] + ($item_info['q_in_stock'] - $item_info['q_waiting_send']) * $item_info['real_cost_rur'];
        }
    }
    //
    $arr['work']['return'] = value_from_config('shop', 'delivery_back_cost');
    $arr['work']['call'] = value_from_config('shop', 'call_cost');
    $arr['work']['delivery'] = value_from_config('shop', 'delivery_cost');
    $arr['work']['pack'] = value_from_config('shop', 'pack_cost');
    $arr['work']['track'] = value_from_config('shop', 'track_cost');
    $arr['work']['zip'] = value_from_config('shop', 'zip_cost');
    $work_cost = $arr['work']['call'] + $arr['work']['delivery'] + $arr['work']['pack'] + $arr['work']['track'] + $arr['work']['zip'];
    /*
    $arr['items_stock']=readdata(query('
        select items.id,
        sum(if(status in (\'return\',\'refund\'),packages.q,0)) as returned,
        sum(if(status in (\'delivered not paid\',\'sent\',\'waiting send\',\'packed\'),packages.q,0)) as confirmed
        from '.typetab('items').'
        left join '.typetab('packages').' on packages.item_id=items.id
        left join '.typetab('orders').' on orders.id=packages.order_id
        group by items.id
        ;'),'id');

    $arr['items_real_cost']=readdata(query('
        select items.id,ifnull((select cost_kzt from shop.stock where item_id=items.id order by stock.date desc limit 1),0) as kzt
        from shop.items;
        '),'id');
    */
    $paid_percent_common = 100 - value_from_config('shop', 'return_percent');
    $list = '';
    $orders_cost_full = 0;
    $orders_cost_part = 0;
    $orders_real_cost_full = 0;
    $orders_real_cost_part = 0;
    $profit_sum = 0;
    foreach ($arr['orders'] as $v) {
        $order = preparing_order_info(['order_id' => $v['id']]);
        // pre($order);
        $list2 = '';
        $order_cost_part = 0;
        $order_real_cost_part = 0;
        foreach ($order['items'] as $item) {
            $item_info = $arr['items_stat'][$item['id']];
            $paid_percent = false;
            // ���� ���� ����� ��� ������� ������ � ������� ������� ���������, � ���� ������� ��� ���� �� 5, �� �������, ���������� ��� �����
            if (isset($item_info['by_sources'][$v['utm_source']]) and $item_info['by_sources'][$v['utm_source']]['q_paid'] >= 5) {
                $paid_percent = $item_info['by_sources'][$v['utm_source']]['paid_percent'];
                // ���� ���, �� ��������� ���� �� ����� ����� �� ������� ������ � ���� �� 5 ��������
            } else {
                // ���� ���� �� ���������� ��� ����� �� ������
                if ($item_info['q_paid'] >= 5) {
                    $paid_percent = $item_info['paid_percent'];
                    // ���� ���, �� ���������� ����� ����� �� ���������
                } else {
                    // ��������� ���� �� ����� �� ������� ��������� � ���� �� ��� ���� �� 5 �������
                    if (isset($arr['sources_stat'][$v['utm_source']]) and $arr['sources_stat'][$v['utm_source']]['q_paid'] >= 5) {
                        $paid_percent = $arr['sources_stat'][$v['utm_source']]['paid_percent'];
                        // ���� ������ ���, �� ���������� ����� ������� ������ �� �������
                    } else {
                        $paid_percent = $paid_percent_common;
                    }
                }
            }
            if (! $paid_percent) {
                error('�� �������� ������� ������, ���-�� ������');
            }
            $item_cost_full = $item['cost_kzt'] * $item['q'];
            $item_cost_part = round($item['cost_kzt'] * $item['q'] * $paid_percent / 100);
            $item_real_cost_full = $item['real_cost_kzt'] * $item['q'];
            $item_real_cost_part = round($item['real_cost_kzt'] * $item['q'] * (100 - $paid_percent) / 100);
            $order_cost_part = $order_cost_part + $item_cost_part;
            $order_real_cost_part = $order_real_cost_part + $item_real_cost_part;
            $list2 .= '<tr><td>'.$item['name'].'</td><td>'.$item['q'].'</td>
			<td>'.$item_cost_full.'</td><td>'.$item_real_cost_full.'</td><td>'.$paid_percent.'%</td><td>'.$item_cost_part.'</td><td>'.$item_real_cost_part.'</td>
			</tr>';
        }
        $orders_cost_full = $orders_cost_full + $v['cost_kzt'];
        $orders_cost_part = $orders_cost_part + $order_cost_part;
        $orders_real_cost_full = $orders_real_cost_full + $v['real_cost_kzt'];
        $orders_real_cost_part = $orders_real_cost_part + $order_real_cost_part;
        $profit = round($order_cost_part + $order_real_cost_part - $v['real_cost_kzt'] - $arr['work']['return'] * (100 - $paid_percent) / 100 - $work_cost);
        $profit_sum = $profit_sum + $profit;
        $table_items = '<table class="table table-bordered">
		'.$list2.'
		</table>';
        $list .= '<tr><!--<td>'.$v['date'].'</td><td>'.$v['id'].'</td>-->
		<td>'.$table_items.'</td>
		<td>
		<p>���� ������: '.$v['date'].'</p>
		<p>ID: '.$v['id'].'</p>
		<p>����� ������: '.$v['cost_kzt'].' (����� '.$order_cost_part.')</p>
		<p>��������� �������: '.$v['real_cost_kzt'].' (������� '.$order_real_cost_part.')</p>
		<p>��������: '.$v['utm_source'].'</p>
		<!--<p>�������: '.($order_cost_part + $order_real_cost_part - $v['real_cost_kzt']).' �����</p>-->
		</td>
		<td>'.$profit.'</td>
		<!--
		<td>'.$v['cost_kzt'].'</td>
		<td>'.$order_cost_part.'</td>
		<td>'.$v['real_cost_kzt'].'</td><td>'.$order_real_cost_part.'</td>
		-->
		</tr>';
    }
    $content = '
	<h3>������ � ����</h3>
	<p>'.count($arr['orders']).' ������� �� �����: '.$orders_cost_full.' �����,
	�������� ������������: '.$orders_cost_part.' ����� ('.round($orders_cost_part / $orders_cost_full * 100).'%)</p>
	<p>������� �� ����� '.$orders_real_cost_full.' �����,
	�������� '.$orders_real_cost_part.' ����� <!--('.round($orders_real_cost_part / $orders_real_cost_full * 100).'%)--></p>
	<!-- <p>������� �� ��� ������ ����� '.($profit_sum).' �����</p> -->
	<p>������� �� ��� �����: ������ '.$arr['paid']['cost_kzt'].' ����� + ����� � ������� '.$arr['items_home']['kzt'].' ����� +
	����� ������������ '.$arr['items_by_statuses']['items_returned_cost_kzt'].' ����� +
	����� �������� '.$orders_real_cost_part.' ����� - ������� '.$arr['expenses']['kzt'].' ����� + ������� '.$orders_cost_part.' �����
	= '.($arr['paid']['cost_kzt'] + $arr['items_home']['kzt'] + $arr['items_by_statuses']['items_returned_cost_kzt'] +
        $orders_real_cost_part - $arr['expenses']['kzt'] + $orders_cost_part).' �����</p>
	<table class="table table-bordered table-striped">
	<thead><tr><!--<th>����</th><th>ID</th>--><th>������</th>
	<th>������</th>
	<!--
	<th>����� ������</th><th>����� ������ �������� % ������</th>
	<th>����� ��������</th><th>����� �������� �������� % ��������</th>
	-->
	<th>�������</th>
	</tr></thead>
	'.$list.'
	</table>';

    return $content;
}

function admin_orders_clear_print()
{
    $redirect = empty($_GET['redirect']) ? error('�� �������� �������� ���������') : check_input($_GET['redirect']);
    $item_id = empty($_GET['item_id']) ? error('�� ������� item_id') : check_input($_GET['item_id']);
    query('update '.typetab('orders').'
		join '.typetab('packages').' on packages.order_id=orders.id and packages.item_id=\''.$item_id.'\'
		set orders.print=0
		;');
    // $rows=mysql_affected_rows();
    redirect('/?page='.$redirect);
    // alert('������� �������: '.$rows);
}

function admin_order_set_print()
{
    // $user_id=auth_id();
    // $login=auth_login();
    $kazpost_current_too = value_from_config('shop', 'kazpost_current_too');
    $order_id = empty($_GET['order_id']) ? error('�� ������� ID') : check_input($_GET['order_id']);
    $arr['old'] = query_assoc('select * from '.tabname('shop', 'orders').' where id=\''.$order_id.'\';');
    if ($arr['old']['print'] == 1) {
        $new['print'] = 0;
    }
    if ($arr['old']['print'] == 0) {
        $new['print'] = 1;
    }
    query('update '.typetab('orders').' set
		print='.$new['print'].',
		/* print_login=, */
		kazpost_too=\''.$kazpost_current_too.'\'
		where id=\''.$order_id.'\';');
    log_changed_fields(['table' => typetab('orders'), 'primary_key' => $order_id, 'old' => $arr['old']]);

    // return '����: '.$arr['old']['print'].' �����: '.$new['print'];
    return $new['print'];
}

// ���������� 1 ���� ������ � ����� ������ ���� ���
function order_set_status($args = [])
{
    // pre($args);
    $order_id = empty($args['order_id']) ? error('�� ������� ID') : check_input($args['order_id']);
    $status = empty($args['status']) ? error('�� ������� ������') : check_input($args['status']);
    $login = auth_login(['type' => 'shop']);
    // if (in_array($status,array('packed','paid','sent'))) {
    $arr['old'] = query_assoc('select * from '.tabname('shop', 'orders').' where id=\''.$order_id.'\';');
    $new['packer'] = $arr['old']['packer'];
    $set['packer'] = '';
    if ($status == 'packed') {
        if (empty($arr['old']['zip'])) {
            return '������ ��� �� ������, ������ ���������';
        }
        if (empty($new['packer'])) {
            $new['packer'] = auth_login(['type' => 'shop']);
            $set['packer'] = ', packer=\''.$new['packer'].'\'';
        }
    }
    query('update '.typetab('orders').' set status=\''.$status.'\' '.$set['packer'].' where id=\''.$order_id.'\';');
    log_changed_fields(['change_log' => typetab('orders_changes'), 'table' => typetab('orders'), 'primary_key' => $order_id,
        'old' => $arr['old'], 'login' => $login]);
    if (in_array($status, ['paid']) and ! in_array($arr['old']['status'], ['paid'])) {
        order_cpa(['step' => 'paid', 'order_id' => $order_id]);
    }

    // };
    return 1;
}

function admin_order_set_status()
{
    $order_id = empty($_GET['order_id']) ? error('�� ������� ID') : check_input($_GET['order_id']);
    $status = empty($_GET['status']) ? error('�� ������� ������') : check_input($_GET['status']);

    return order_set_status(['order_id' => $order_id, 'status' => $status]);
}

/*
function admin_order_set_status() {
    $order_id=empty($_GET['order_id'])?error('�� ������� ID'):check_input($_GET['order_id']);
    $status=empty($_GET['status'])?error('�� ������� ������'):check_input($_GET['status']);
    $login=empty($_SESSION['shop']['auth']['login'])?error('��� ������ � ������'):check_input($_SESSION['shop']['auth']['login']);
    if (in_array($status,array('packed','paid'))) {
        $arr['old']=query_assoc('select * from '.tabname('shop','orders').' where id=\''.$order_id.'\';');
        $new['packer']=$arr['old']['packer'];
        if ($status=='packed') {
            if (empty($arr['old']['zip'])) {return '������ ��� �� ������, ������ ���������';};
            if (empty($new['packer'])) {$new['packer']=$_SESSION['shop']['auth']['login'];};
        };
        query('update '.typetab('orders').' set status=\''.$status.'\',packer=\''.$new['packer'].'\' where id=\''.$order_id.'\';');
        log_changed_fields(array('change_log'=>typetab('orders_changes'),'table'=>typetab('orders'),'primary_key'=>$order_id,
        'old'=>$arr['old'],'login'=>$login));
    };
    return 1;
}
*/

function admin_submit_input_track()
{
    // return '���������';
    // pre($_POST);
    $order_id = empty($_POST['order_id']) ? error('�� ������� order_id') : check_input($_POST['order_id']);
    $track = empty($_POST['track']) ? error('�� ������� track') : check_input($_POST['track']);
    $login = auth_login(['type' => 'shop']);
    $arr = query_assoc('select tracker,track,zip from '.tabname('shop', 'orders').' where id=\''.$order_id.'\';');
    $tracker = $arr['tracker'];
    if (empty($track)) {
        return '�� ������� track';
    }
    // ���� ������ �� ������ � ���� � �� �������, �� ������ ����� ���-�� �� ���. ������ ������.
    if (empty($arr['zip'])) {
        return '������ ��� �� ��������, ������ ��������� track';
    }
    // ���� ��������� �� ������� � ����, �� ���������� ������� �����
    if (empty($arr['tracker'])) {
        $tracker = $login;
    }
    // ���� ���� ��� null � ������� �� null, �� ��������� ���� �� ������������ � ���������� ��� ����� - ���������
    // if (empty($arr['track']) and !empty($track)) {
    // };
    $arr['old'] = query_assoc('select * from '.tabname('shop', 'orders').' where id=\''.$order_id.'\';');
    query('update '.typetab('orders').' set track=\''.$track.'\',tracker=\''.$tracker.'\' where id=\''.$order_id.'\';');
    log_changed_fields(['change_log' => typetab('orders_changes'), 'table' => typetab('orders'), 'primary_key' => $order_id,
        'old' => $arr['old'], 'login' => $login]);
    // ����������� ������ ������ 'sent' ���� ����� ��� �����������
    if (in_array($arr['old']['status'], ['waiting send', 'packed', 'check address'])) {
        order_set_status(['order_id' => $order_id, 'status' => 'sent']);
    }

    // shop_auto_statuses();
    return '���������';
}

function order_changes_list()
{
    if (empty($_GET['order_id'])) {
        error('�� ������� order_id');
    }
    $order_id = check_input($_GET['order_id']);
    $arr = query_arr('select date,field,value,login from '.typetab('orders_changes').' where primary_key=\''.$order_id.'\' order by date;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['date'].'</td><td>'.$v['field'].'</td><td>'.$v['value'].'</td><td>'.$v['login'].'</td></tr>';
    }
    $content = '
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">��������� ������ #'.$order_id.'</h4>
	</div>
	<div class="modal-body">
	<table class="table table-striped">
	<thead><tr>
	<th style="width: 150px;">����</th>
	<th>����</th>
	<th>��������</th>
	<th>�����</th>
	</tr></thead>
	'.$list.'
	</table>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
	</div>
	';

    return $content;
}

function orders_changes()
{
    $arr = query_arr('select date,primary_key,field,value,login from '.typetab('orders_changes').' order by date desc limit 1000;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['date'].'</td><td><a href="/?page=admin/orders&status=all&order_id='.$v['primary_key'].'">#'.$v['primary_key'].'</a></td><td>'.$v['field'].'</td><td>'.$v['value'].'</td><td>'.$v['login'].'</td></tr>';
    }
    $return = '<table class="table">
	<thead><tr><th style="width: 150px;">����</th><th>�����</th><th>����</th><th>��������</th><th>�����</th></tr></thead>
	'.$list.'
	</table>';

    return $return;
}

function orders_wide_stat_form()
{
    $g['filters'] = empty($_GET['filters']) ? [] : unserialize($_GET['filters']);
    if (empty($g['filters'])) {
        $p['items'] = empty($_POST['items']) ? [] : $_POST['items'];
        $p['item_groups'] = empty($_POST['item_groups']) ? [] : $_POST['item_groups'];
        $p['sources'] = empty($_POST['sources']) ? [] : $_POST['sources'];
        $p['advertisers'] = empty($_POST['advertisers']) ? [] : $_POST['advertisers'];
        $p['statuses'] = empty($_POST['statuses']) ? [] : $_POST['statuses'];
        $p['date_from'] = empty($_POST['date_from']) ? date('Y-m-d') : $_POST['date_from'];
        $p['date_to'] = empty($_POST['date_to']) ? date('Y-m-d', strtotime('+1 day')) : $_POST['date_to'];
    } else {
        $p = $g['filters'];
    }
    temp_var('date_from', $p['date_from']);
    temp_var('date_to', $p['date_to']);
    // pre($p);
    $rus['statuses'] = shop_translation('order_status');
    $rus['filters'] = shop_translation('filters');
    // pre(unserialize($_GET['filters']));
    // $get=serialize($p);
    // pre($g['filters']);
    // ��������� ���� �������
    $items = query_arr('select id,name from '.tabname('shop', 'items').' order by name;');
    $list = '';
    foreach ($items as $v) {
        $checked = isset($p['items'][$v['id']]) ? 'checked' : '';
        $list .= '<div class="checkbox">
		<label><input type="checkbox" name="items['.$v['id'].']" '.$checked.'>'.$v['name'].'</label>
		</div>';
    }
    temp_var('block_items', $list);
    // ��������� ���� ����� �������
    $item_groups = query_arr('select id,name from '.tabname('shop', 'item_groups').' order by name;');
    $list = '';
    foreach ($item_groups as $v) {
        $checked = isset($p['item_groups'][$v['id']]) ? 'checked' : '';
        $list .= '<div class="checkbox">
		<label><input type="checkbox" name="item_groups['.$v['id'].']" '.$checked.'>'.$v['name'].'</label>
		</div>';
    }
    temp_var('block_item_groups', $list);
    // ��������� ���� ����������
    $sources = query_arr('select utm_source from '.tabname('shop', 'orders').' where advertiser is null and utm_source!=\'\' group by utm_source;');
    $list = '';
    foreach ($sources as $v) {
        $checked = isset($p['sources'][$v['utm_source']]) ? 'checked' : '';
        $list .= '<div class="checkbox">
		<label><input type="checkbox" name="sources['.$v['utm_source'].']" '.$checked.'>'.$v['utm_source'].'</label>
		</div>';
    }
    temp_var('block_sources', $list);
    // ��������� ���� ��������������
    $advertisers = query_arr('select advertiser from '.typetab('orders').' where advertiser is not null group by advertiser;');
    $list = '';
    foreach ($advertisers as $v) {
        $checked = isset($p['advertisers'][$v['advertiser']]) ? 'checked' : '';
        $list .= '<div class="checkbox">
		<label><input type="checkbox" name="advertisers['.$v['advertiser'].']" '.$checked.'><span class="glyphicon glyphicon-user"></span> '.$v['advertiser'].'</label>
		</div>';
    }
    temp_var('block_advertisers', $list);
    // ��������� ���� ��������
    // $statuses=query_arr('select status from '.tabname('shop','orders').' group by status;');
    $list = '';
    // pre($rus);
    foreach ($rus['statuses'] as $k => $v) {
        $checked = isset($p['statuses'][$k]) ? 'checked' : '';
        $list .= '<div class="checkbox">
		<label><input type="checkbox" name="statuses['.$k.']" id="statuses['.$k.']" '.$checked.' data-role="statuses">
		'.$v.'</label>
		</div>';
    }
    temp_var('block_statuses', $list);
    // ���������� ������
    // pre(array_to_str($p['items']));
    $m['items']['where'] = empty($p['items']) ? '0' : 'orders.id in (
		select distinct orders.id from '.tabname('shop', 'orders').'
		join '.tabname('shop', 'packages').' on packages.order_id=orders.id
		where packages.item_id in ('.array_to_str_alt($p['items'], 'key').')
		)';
    $m['item_groups']['where'] = empty($p['item_groups']) ? '0' : 'orders.id in (
		select distinct orders.id from '.tabname('shop', 'orders').'
		join '.tabname('shop', 'packages').' on packages.order_id=orders.id
		where packages.item_id in (
			select distinct item_id from '.tabname('shop', 'items_groups').'
			where group_id in ('.array_to_str_alt($p['item_groups'], 'key').')
			)
		)';
    // ���� �� ������� ������ � ������ �������, ������ �������, ��� ������� ���
    if (empty($m['items']['where']) and empty($m['item_groups']['where'])) {
        $m['items']['where'] = 1;
        $m['item_groups']['where'] = 1;
    }
    $m['sources']['where'] = empty($p['sources']) ? '1' : 'utm_source in ('.array_to_str_alt($p['sources'], 'key').')';
    $m['advertisers']['where'] = empty($p['advertisers']) ? '1' : 'advertiser in ('.array_to_str_alt($p['advertisers'], 'key').')';
    $m['statuses']['where'] = empty($p['statuses']) ? '1' : 'status in ('.array_to_str_alt($p['statuses'], 'key').')';
    $m['date']['where'] = '(orders.date between \''.$p['date_from'].'\' and \''.$p['date_to'].'\')';
    $m['sources']['group'] = empty($p['group_source']) ? '' : 'utm_source';
    $m['sources']['fields'] = empty($p['group_source']) ? '' : ',count(orders.id) as q';
    $var['page'] = check_order_page(isset($_GET['p']) ? (int) $_GET['p'] : 1);
    $list = '';
    foreach ($p as $k => $v) {
        if (is_array($v)) {
            $list .= empty($v) ? '' : '<b>'.$rus['filters'][$k].'</b>: '.array_to_str_alt($v, 'key').' ';
        } else {
            $list .= empty($v) ? '' : '<b>'.$rus['filters'][$k].'</b>: '.$v.' ';
        }
    }
    temp_var('block_filter', $list);
    // ��������� ������������� ������ �������
    $orders = query_arr('select sql_calc_found_rows id,date,name,status,utm_source,
		(select sum(q*cost_kzt) from '.typetab('packages').' where order_id=orders.id) as order_cost,
		(select sum(q*item_cost_kzt) from '.typetab('packages').' where order_id=orders.id) as order_real_cost
		'.$m['sources']['fields'].'
		from '.tabname('shop', 'orders').'
		where ('.$m['items']['where'].' or '.$m['item_groups']['where'].') and
		'.$m['sources']['where'].' and '.$m['advertisers']['where'].' and '.$m['statuses']['where'].' and '.$m['date']['where'].'
		'.$m['sources']['group'].'
		'.$var['page']['limit'].'
		;');
    $orders_no_limit = query_assoc('select count(orders.id) as q,
		sum((select sum(q*cost_kzt) from '.typetab('packages').' where order_id=orders.id)) as orders_cost,
		sum((select sum(q*item_cost_kzt) from '.typetab('packages').' where order_id=orders.id)) as orders_real_cost
		'.$m['sources']['fields'].'
		from '.tabname('shop', 'orders').'
		where ('.$m['items']['where'].' or '.$m['item_groups']['where'].') and
		'.$m['sources']['where'].' and '.$m['advertisers']['where'].' and '.$m['statuses']['where'].' and '.$m['date']['where'].'
		'.$m['sources']['group'].'
		;');
    // $rows=rows_without_limit();
    $rows = $orders_no_limit['q'];
    temp_var('rows', $rows);
    // pre($sum_orders_profit);
    // ��������� ������ ������� �� ���� (��� �������)
    $orders_for_chart = query_arr('select date(date) as day,count(orders.id) as q_all,
		sum(if(orders.status not in (\'new\',\'unavailable\',\'canceled\'),1,0)) as q_confirmed,
		sum(if(orders.status in (\'take payment\',\'paid\'),1,0)) as q_paid,
		sum(if(orders.status in (\'refund\',\'refund paid\'),1,0)) as q_refund
		from '.tabname('shop', 'orders').'
		where ('.$m['items']['where'].' or '.$m['item_groups']['where'].') and
		'.$m['sources']['where'].' and '.$m['advertisers']['where'].' and '.$m['date']['where'].'
		group by day
		');
    // ��������� ������ ������� �� ������� (��� �������)
    $orders_for_chart_month = query_arr('select date_format(orders.date, \'%Y-%m\') as month,count(orders.id) as q_all,
		sum(if(orders.status not in (\'new\',\'unavailable\',\'canceled\'),1,0)) as q_confirmed,
		sum(if(orders.status in (\'take payment\',\'paid\',\'refund\',\'refund paid\'),1,0)) as q_paid,
		sum(if(orders.status in (\'refund\',\'refund paid\'),1,0)) as q_refund,
		avg(datediff(kazpost_api.sent_date,orders.date)) as day_to_send
		from '.tabname('shop', 'orders').'
		left join '.typetab('kazpost_api').' on kazpost_api.track=orders.track
		where ('.$m['items']['where'].' or '.$m['item_groups']['where'].') and
		'.$m['sources']['where'].' and '.$m['advertisers']['where'].' and '.$m['date']['where'].'
		group by month
		');
    // pre($orders_for_chart);
    $list = '';
    $list2 = '';
    foreach ($orders_for_chart as $v) {
        if (! empty($list)) {
            $list .= ',';
        }
        if (! empty($list2)) {
            $list2 .= ',';
        }
        if (! empty($list3)) {
            $list3 .= ',';
        }
        $list .= '[\''.$v['day'].'\','.$v['q_confirmed'].','.$v['q_paid'].']';
        $list2 .= '[\''.$v['day'].'\','.round($v['q_paid'] / (empty($v['q_confirmed']) ? 1 : $v['q_confirmed']) * 100).']';
    }
    $list3 = '';
    foreach ($orders_for_chart_month as $v) {
        if (! empty($list3)) {
            $list3 .= ',';
        }
        $list3 .= '[\''.$v['month'].'\',
		'.round($v['q_paid'] / (empty($v['q_confirmed']) ? 1 : $v['q_confirmed']) * 100).',
		'.round($v['q_refund'] / (empty($v['q_confirmed']) ? 1 : $v['q_confirmed']) * 100).',
		'.round(($v['q_paid'] - $v['q_refund']) / (empty($v['q_confirmed']) ? 1 : $v['q_confirmed']) * 100).',
		'.round($v['day_to_send']).'
		]';
    }
    temp_var('chart_values', '[\'����\',\'�������������\',\'���������\'],'.$list);
    temp_var('chart_values_paid_percent', '[\'����\',\'�����\'],'.$list2);
    temp_var('chart_values_paid_percent_month', '[\'�����\',\'�����\',\'������� �����\',\'����� - �������\',\'���� �� ��������\'],'.$list3);
    // temp_var('chart_values_paid_percent_month','');
    $list = '';
    foreach ($orders as $v) {
        $order_info = cache('preparing_order_info', ['order_id' => $v['id']], 0);
        $list .= '<tr><td>'.$v['id'].'</td><td>'.$v['date'].'</td><td>'.$v['name'].'</td>
		<td><ul class="list-unstyled items_list">'.order_items_list(['order_id' => $v['id']]).'</ul></td>
		<td>'.$v['utm_source'].'</td><td>'.$v['status'].'</td><td>'.$order_info['profit'].'</td><td>'.$order_info['real_profit'].'</td></tr>';
    }
    temp_var('orders_list', $list);
    temp_var('pagination', page_numbers($rows, $var['page']['per_page'], getvar('p'),
        '/?page=admin/stat/orders_wide_stat/index&filters='.htmlspecialchars(serialize($p)).'&p=', 5, '', '&laquo;', '&raquo;', 'yes', 'active', 'disabled', 1, '', 'yes', 'yes'));
    $content = template('admin/stat/orders_wide_stat/block');

    // $content.=pre_return($m);
    // $content.=pre_return($orders);
    return $content;
}

function orders_wide_stat()
{
    // return pre_return($_POST);
}

function admin_sms_sets()
{
    $arr = query_arr('select id,name from '.typetab('sms_sets').'	where active;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li><a href="/?page=admin/sms/mass_send_sms&amp;id='.$v['id'].'">'.$v['name'].'</a></li>';
    }

    return $list;
}

function mass_send_sms_page()
{
    // $orders=isset($_GET['orders'])?$_GET['orders']:error('�� ������� ������ ID �������');
    $set_id = empty($_GET['id']) ? error('�� ������� ��� ���') : check_input($_GET['id']);
    $arr = query_assoc('
		select name,template,selection,days_interval,items_group,item_id
		from '.typetab('sms_sets').'
		where id=\''.$set_id.'\'
		;');
    if (! $arr) {
        error('��� ��� �� ����������');
    }
    $set_name = $arr['name'];
    $args['autosend'] = isset($_GET['autosend']) ? true : false;
    // $type=isset($_GET['type'])?$_GET['type']:'new';
    $type = $arr['selection'];
    // $args['format']=isset($_GET['format'])?$_GET['format']:'zip_address';
    $args['format'] = $arr['template'];
    // $road_diff=isset($_GET['road_diff'])?$_GET['road_diff']:10;
    $road_diff = 10;
    // $sms_datediff=isset($_GET['sms_datediff'])?$_GET['sms_datediff']:2;
    $sms_datediff = $arr['days_interval'];
    // $items_group=isset($_GET['items_group'])?$_GET['items_group']:'';
    $items_group = $arr['items_group'];
    $item_id = $arr['item_id'];
    // pre($arr);
    $where['item_id'] = empty($item_id) ? '1' : '(select 1 from '.typetab('packages').' where order_id=orders.id and item_id=\''.$item_id.'\' limit 1)';
    $where['items_group'] = empty($items_group) ? '1' : '(select 1 from '.typetab('packages').' where order_id=orders.id and item_id in (select item_id from '.typetab('items_groups').' where group_id='.$items_group.') limit 1)';
    $where['delivered'] = 'status=\'delivered not paid\'';
    $having['delivered'] = 'having road_diff<30 and sms_datediff>'.$sms_datediff;
    $where['new'] = 'status=\'delivered not paid\'';
    $having['new'] = 'having sms_new_ok_q=0';
    $where['road'] = 'status=\'sent\'';
    $having['road'] = 'having road_diff>'.$road_diff.' and (sms_datediff>'.$sms_datediff.' or sms_datediff is null)';
    $where['sent'] = 'status=\'sent\'';
    $having['sent'] = 'having sms_track_sent_ok_q=0';
    $where['paid_watches'] = 'status=\'paid\' and main_item in (select item_id from '.typetab('items_groups').' where group_id=\'13\') and phone is not null and length(phone)<14';
    $group['paid_watches'] = 'group by phone';
    $having['paid_watches'] = 'having sms_ignore_errors_datediff>'.$sms_datediff;
    $where['paid_all'] = 'status=\'paid\' and phone is not null and length(phone)<14';
    $group['paid_all'] = 'group by phone';
    $having['paid_all'] = 'having sms_ignore_errors_datediff>'.$sms_datediff;
    $where['new_coupon'] = 'status=\'delivered not paid\'';
    $having['new_coupon'] = 'having sms_new_coupon_ok_q=0';
    $where['waiting'] = 'status in (\'waiting send\',\'packed\',\'check address\')';
    $having['waiting'] = 'having sms_waiting_ok_q=0';
    $arr = query_arr('select sql_calc_found_rows id,
		(select count(*) from '.tabname('shop', 'sms').' where order_id=orders.id and delivered) as sms_ok_q,
		(select count(*) from '.tabname('shop', 'sms').' where order_id=orders.id and delivered and format in (\'zip_address_track\',\'track\',\'ems_zip_track\')) as sms_new_ok_q,
		(select count(*) from '.tabname('shop', 'sms').' where order_id=orders.id and delivered and format=\'track_sent\') as sms_track_sent_ok_q,
		(select count(*) from '.tabname('shop', 'sms').' where order_id=orders.id and delivered and format=\'coupon\') as sms_new_coupon_ok_q,
		(select count(*) from '.tabname('shop', 'sms').' where order_id=orders.id and delivered and format=\'wait_zhidkoe_steklo\') as sms_waiting_ok_q,
		datediff(current_timestamp,(select date from '.tabname('shop', 'sms').' where order_id=orders.id and delivered order by date desc limit 1)) as sms_datediff,
		datediff(current_timestamp,(select date from '.tabname('shop', 'sms').' where order_id=orders.id order by date desc limit 1)) as sms_ignore_errors_datediff,
		datediff(current_timestamp,(select date from '.tabname('shop', 'kazpost_api').' where track=orders.track)) as road_diff
		from '.tabname('shop', 'orders').'
		where '.$where[$type].'	and '.$where['items_group'].' and '.$where['item_id'].'
		'.(isset($group[$type]) ? $group[$type] : '').'
		'.$having[$type].'
		limit 100
		;');
    $all_rows = rows_without_limit();
    $rows = count($arr);
    foreach ($arr as $v) {
        $args['orders'][] = $v['id'];
    }
    // $args['format']=$format;
    // return mass_send_sms(array('orders'=>$orders,'type'=>$type));
    // $link='/?page=admin/sms/mass_send_sms&type='.$type.'&format='.$args['format'].'&amp;items_group='.$items_group.'&amp;sms_datediff='.$sms_datediff.'&amp;road_diff='.$road_diff;
    $link = '/?page=admin/sms/mass_send_sms&id='.$set_id;
    $autosend_btn = $args['autosend'] ? '' : '<a href="'.$link.'&amp;autosend" style="margin-right: 10px;" class="btn btn-danger">��������� ���� �������������</a>';
    $list_btn = $args['autosend'] ? '<a href="'.$link.'" style="margin-right: 10px;" class="btn btn-default">��������� � ������</a>' : '';

    return '<h3>'.$set_name.'</h3>'.$autosend_btn.$list_btn.'�������� '.$rows.' �� '.$all_rows.mass_send_sms($args);
}

function mass_send_sms($args)
{
    if (empty($args['orders'])) {
        alert('������� ��� ����������� ���');
    }
    if (empty($args['format'])) {
        error('�� ������� ������ ���');
    }
    $args['autosend'] = empty($args['autosend']) ? false : $args['autosend'];
    $list = '';
    foreach ($args['orders'] as $order_id) {
        $arr = query_assoc('select orders.phone from '.tabname('shop', 'orders').' where orders.id=\''.$order_id.'\';');
        $text = admin_sms_text(['order_id' => $order_id, 'format' => $args['format']]);
        if ($text['status']) {
            if ($args['autosend'] === true) {
                // if ($arr['phone']=='87025900173') {pre($text);};
                // if (empty($text['text'])) {die('#'.$order_id.' ������ ����� ('.pre($text).')');};
                $list .= '<li>#'.$order_id.' '.admin_submit_send_sms(['order_id' => $order_id, 'phone' => $arr['phone'], 'format' => $args['format'], 'message' => $text['text']]).' '.$arr['phone'].' '.$text['text'].'</li>';
                // flush();
            } else {
                $list .= '<li>'.send_sms_form(['order_id' => $order_id, 'phone' => $arr['phone'], 'format' => $args['format'], 'text' => $text['text']]).'</li>';
            }
        } else {
            $list .= '<li>'.$text['text'].'</li>';
        }
    }
    $return = '<ul class="list-unstyled mass_send_sms">'.$list.'</ul>';

    return $return;
}

function admin_edit_day_ad()
{
    $date = isset($_GET['date']) ? $_GET['date'] : error('�� �������� ����');
    $arr_rate = query_assoc('select rate from '.typetab('rates').' where date=\''.$date.'\';');
    $rate = empty($arr_rate['rate']) ? '' : $arr_rate['rate'];
    $data = query('
		select ifnull(utm_source,\'\') as utm_source,cost,currency from '.tabname('shop', 'ad').'
		where date=\''.$date.'\';');
    $arr_ads = readdata($data, 'utm_source');
    // pre($arr_ads);
    // todo ������ ������� �����
    $current_timezone = get_timezone();
    // pre(get_timezone());
    set_timezone('+3');
    // pre(get_timezone());
    // �������� ��� ��������� ������ �� ���� 2016-05-23, � ���������� ������ ������� �� ������� ������.
    $arr_sources = query_arr('select utm_source,
		sum(if(status not in (\'new\',\'canceled\',\'unavailable\'),1,0)) as q_confirmed
		from '.typetab('orders').'
		where date(date)=\''.$date.'\' and (advertiser is null or date<\'2016-05-23\')
		group by utm_source
		order by utm_source
		;');
    $arr_advertisers = query_arr('select advertiser,
		sum(if(status not in (\'new\',\'canceled\',\'unavailable\'),1,0)) as q_confirmed,
		sum(if(status not in (\'new\',\'unavailable\',\'canceled\'),orders.bonus,0)) as sum_cost_confirm
		from '.typetab('orders').'
		where date(date)=\''.$date.'\' and advertiser is not null
		group by advertiser
		having q_confirmed!=0
		order by advertiser
		;');
    set_timezone_alt($current_timezone);
    // pre(get_timezone());
    $list = '';
    // pre($arr_sources);
    foreach ($arr_sources as $v) {
        $cost = isset($arr_ads[$v['utm_source']]) ? $arr_ads[$v['utm_source']]['cost'] : 0;
        $currency = isset($arr_ads[$v['utm_source']]) ? $arr_ads[$v['utm_source']]['currency'] : 'rub';
        $selected['rub'] = '';
        $selected['kzt'] = '';
        $selected[$currency] = 'selected="selected"';
        if (empty($v['q_confirmed'])) {
            $order_cost_kzt = '��� �������';
        } else {
            $order_cost_kzt = $cost / $v['q_confirmed'];
            if ($currency == 'rub') {
                $order_cost_kzt = $order_cost_kzt * $rate;
            }
            $order_cost_kzt = round($order_cost_kzt).' ���. �� ����� ('.$v['q_confirmed'].' ��.)';
        }
        $list .= '
		<div class="row">
		<div class="col-md-3">
		<b>'.(empty($v['utm_source']) ? '��� �����' : $v['utm_source']).'</b>
		</div>
		<div class="col-md-2">
		<div class="form-group">
		<input type="text" name="sources['.(empty($v['utm_source']) ? 'null' : $v['utm_source']).'][cost]" value="'.$cost.'" placeholder="�����" class="form-control">
		</div>
		</div>
		<div class="col-md-2">
		<div class="form-group">
		<select class="form-control" name="sources['.(empty($v['utm_source']) ? 'null' : $v['utm_source']).'][currency]">
		<option value="rub" '.$selected['rub'].'>RUB</option>
		<option value="kzt" '.$selected['kzt'].'>KZT</option>
		</select>
		</div>
		</div>
		<div class="col-md-5">
		'.$order_cost_kzt.'
		</div>
		</div>
		';
    }
    foreach ($arr_advertisers as $v) {
        $cost = $v['sum_cost_confirm'];
        $currency = 'rub';
        if (empty($v['q_confirmed'])) {
            $order_cost_kzt = '��� �������';
        } else {
            $order_cost_kzt = $cost / $v['q_confirmed'];
            if ($currency == 'rub') {
                $order_cost_kzt = $order_cost_kzt * $rate;
            }
            $order_cost_kzt = round($order_cost_kzt).' ���. �� ����� ('.$v['q_confirmed'].' ��.)';
        }
        $list .= '
		<div class="row">
		<div class="col-md-3">
		<span class="glyphicon glyphicon-user"></span>
		<b>'.$v['advertiser'].'</b>
		</div>
		<div class="col-md-2">
		<div class="form-group">

		</div>
		</div>
		<div class="col-md-2">
		<div class="form-group">
		'.$v['sum_cost_confirm'].' ���.
		</div>
		</div>
		<div class="col-md-5">
		'.$order_cost_kzt.'
		</div>
		</div>
		';
    }
    $content = '
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">������� �� ������� '.$date.'</h4>
	</div>
	<div class="modal-body">
	<form id="editDayAd">
	<div class="row">
	<div class="col-md-5">
	<b>���� RUB/KZT</b>
	<div class="form-group">
	<input type="text" name="rate" value="'.$rate.'" placeholder="��������� 1 ����� � �����" class="form-control">
	</div>
	</div>
	</div>
	'.$list.'
	<input type="hidden" name="date" value="'.$date.'">
	</form>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
	<button type="button" class="btn btn-primary" data-role="ajax_submit_edit_day_ad" id="result" data-loading-text="����������..." autocomplete="off">���������</button>
	</div>';

    return $content;
}

function admin_submit_edit_day_ad()
{
    // pre($_POST);
    $date = isset($_POST['date']) ? $_POST['date'] : error('�� �������� ����');
    $rate = isset($_POST['rate']) ? $_POST['rate'] : error('�� ������� ����');
    $sources = empty($_POST['sources']) ? error('�� ������� ������ ����������') : $_POST['sources'];
    if (! empty($rate)) {
        query('insert into '.tabname('shop', 'rates').' set date=\''.$date.'\',rate=\''.$rate.'\'
		on duplicate key update rate=\''.$rate.'\'
		;');
    }
    foreach ($sources as $k => $v) {
        $source = ($k === 'null') ? '' : $k;
        $cost = isset($v['cost']) ? $v['cost'] : error('�� �������� �����');
        $currency = isset($v['currency']) ? $v['currency'] : error('�� �������� ������');
        query('insert into '.tabname('shop', 'ad').' set date=\''.$date.'\',utm_source=\''.$source.'\',cost=\''.$cost.'\',currency=\''.$currency.'\'
			on duplicate key update cost=\''.$cost.'\',currency=\''.$currency.'\'
			;');
    }

    return '���������';
}

function admin_edit_login_paid()
{
    $login = isset($_GET['login']) ? $_GET['login'] : error('�� ������� �����');
    $type = isset($_GET['type']) ? $_GET['type'] : error('�� ������� ���');
    $arr = cache('preparing_login_info', ['login' => $login], 0);
    // pre($arr);
    $rus['types'] = ['calls' => '��������', 'packs' => '��������', 'zips' => '�������', 'calls_vip' => '�������� vip', 'tracks' => '�����'];
    $content = '
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">������ '.$login.' �� '.$rus['types'][$type].'</h4>
	</div>
	<div class="modal-body">
	<form id="editLoginPaid">
	<p>��������: '.$arr['paid_'.$type].' ��.</p>
	<p>�� ��������: '.$arr['not_paid_'.$type].' ��.</p>
	<div class="form-group">
	<label for="q">�������� '.$rus['types'][$type].'</label>
	<input type="text" id="q" name="q" value="" placeholder="����������" class="form-control">
	</div>
	<input type="hidden" name="login" value="'.$login.'">
	<input type="hidden" name="type" value="'.$type.'">
	</form>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
	<button type="button" class="btn btn-primary" data-role="ajax_submit_edit_login_paid" id="result" data-loading-text="����������..." autocomplete="off">���������</button>
	</div>';

    return $content;
}

function admin_submit_edit_login_paid()
{
    $login = isset($_POST['login']) ? $_POST['login'] : error('�� ������� �����');
    $type = isset($_POST['type']) ? $_POST['type'] : error('�� ������� ���');
    $rus['types'] = ['calls' => '��������', 'packs' => '��������', 'zips' => '�������', 'calls_vip' => '�������� vip', 'tracks' => '�����'];
    $q = isset($_POST['q']) ? $_POST['q'] : error('�� �������� ����������');
    // pre($_POST);
    $cost['calls'] = value_from_config('shop', 'call_cost');
    $cost['packs'] = value_from_config('shop', 'pack_cost');
    $cost['tracks'] = value_from_config('shop', 'track_cost');
    $cost['zips'] = value_from_config('shop', 'zip_cost');
    $cost['calls_vip'] = value_from_config('shop', 'call_vip_cost');
    //
    query('update '.tabname('shop', 'users').' set paid_'.$type.'=paid_'.$type.'+'.$q.' where login=\''.$login.'\';');
    query('insert into '.typetab('purchase').' set
		login=\''.$login.'\',
		text=\'������ ������������ '.$login.' �� '.$rus['types'][$type].'\',
		sum='.$q * $cost[$type].',
		currency=\'kzt\',
		devided=\'1\'
		;');

    return '���������';
}

function admin_menu()
{
    // $login_info=preparing_user_info(array('login'=>$_SESSION['shop']['auth']['login'],'type'=>'shop'));
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'shop']), 'type' => 'shop'], 0);

    return template('admin/menu/'.$login_info['level']);
}

function admin_order_stat()
{
    $order_id = empty($_GET['order_id']) ? error('�� ������� ID ������') : $_GET['order_id'];

    return order_stat(['order_id' => $order_id]);
}

function order_stat($args)
{
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : $args['order_id'];
    $list = '';
    $order = cache('preparing_order_info', ['order_id' => $order_id], 0);
    $items_list = '';
    // pre($order);
    foreach ($order['items'] as $item) {
        temp_var('name', $item['name']);
        temp_var('q', $item['q']);
        temp_var('cost_kzt', $item['cost_kzt']);
        temp_var('real_cost_kzt', $item['real_cost_kzt']);
        temp_var('buyout', $item['buyout']['buyout']);
        temp_var('buyout_type_ru', $item['buyout']['type_ru']);
        temp_var('power', $item['power']);
        temp_var('order_part', round($item['order_part'] * 100));
        $items_list .= template('admin/stat/order_stat/item_item');
    }
    clear_temp_vars();
    temp_var('items_order_items', $items_list);
    $order_items = template('admin/stat/order_stat/block');
    $icon_status = '��� ����������� ��� ������� '.$order['status'];
    if (in_array($order['status'], ['new', 'unavailable'])) {
        $icon_status = '<span class="glyphicon glyphicon-headphones"></span>';
    }
    if (in_array($order['status'], ['canceled'])) {
        $icon_status = '<span class="glyphicon glyphicon-remove"></span>';
    }
    if (in_array($order['status'], ['waiting send', 'packed', 'check address'])) {
        $icon_status = '<span class="glyphicon glyphicon-gift"></span>';
    }
    if (in_array($order['status'], ['sent'])) {
        $icon_status = '<span class="glyphicon glyphicon-road"></span>';
    }
    if (in_array($order['status'], ['delivered not paid'])) {
        $icon_status = '<span class="glyphicon glyphicon-time"></span>';
    }
    if (in_array($order['status'], ['paid', 'take payment'])) {
        $icon_status = '<span class="glyphicon glyphicon-ok"></span>';
    }
    if (in_array($order['status'], ['return', 'refund', 'return paid', 'refund paid'])) {
        $icon_status = '<span class="glyphicon glyphicon-remove"></span>';
    }
    temp_var('id', $order['order_id']);
    temp_var('icon_status', $icon_status);
    temp_var('items', $order_items);
    temp_var('cost_kzt', $order['cost_kzt']);
    temp_var('real_cost_kzt', $order['real_cost_kzt']);
    temp_var('work_cost', $order['work_cost']);
    temp_var('insurance_cost', $order['insurance_cost']);
    temp_var('buyout', $order['buyout']);
    temp_var('delivery_cost', $order['delivery_cost']);
    temp_var('delivery_back_cost', $order['delivery_back_cost']);
    temp_var('profit', (($order['profit'] < 0) ? ('<span style="color: red;">'.$order['profit'].'</span>') : $order['profit']));
    temp_var('real_profit', $order['real_profit']);
    temp_var('advertiser', empty($order['advertiser']) ? '���' : $order['advertiser']);
    temp_var('utm_source', empty($order['utm_source']) ? '���' : $order['utm_source']);
    temp_var('advertiser_bonus_rub', $order['advertiser_bonus_rub']);
    temp_var('advertiser_bonus_kzt', $order['advertiser_bonus_kzt']);
    $return = template('admin/stat/order_stat/item_order');
    clear_temp_vars();

    return $return;
}

function stat_orders_in_day()
{
    $date = isset($_GET['date']) ? $_GET['date'] : error('�� �������� ����');
    $advertiser = isset($_GET['advertiser']) ? $_GET['advertiser'] : false;
    $utm_source = isset($_GET['utm_source']) ? $_GET['utm_source'] : false;
    $sql['advertiser'] = ($advertiser !== false) ? 'orders.advertiser=\''.$advertiser.'\'' : 1;
    $sql['utm_source'] = ($utm_source !== false) ? 'orders.advertiser is null and orders.utm_source=\''.$utm_source.'\'' : 1;
    $sql['ad_cost']['utm_source'] = (($advertiser !== false) ? '1=2' : (($utm_source !== false) ? 'ad.utm_source=\''.$utm_source.'\'' : 1));
    // pre(var_dump($sql['utm_source']));
    $current_timezone = get_timezone();
    set_timezone('+3');
    // ������ ��������� ���� �� ��������������
    $arr['items_by_advertisers'] = query_arr('select items.id,items.name,orders.advertiser,sum(packages.q) as q,
		sum(packages.cost_kzt*packages.q) as sum_cost_kzt,
		sum(packages.item_cost_kzt*packages.q) as sum_real_cost_kzt
		from '.tabname('shop', 'orders').'
		join '.tabname('shop', 'packages').' on packages.order_id=orders.id
		join '.tabname('shop', 'items').' on items.id=packages.item_id
		where date(date)=\''.$date.'\' and orders.status not in (\'new\',\'canceled\',\'unavailable\')
		and '.$sql['advertiser'].' and '.$sql['utm_source'].'
		group by items.id,orders.advertiser
		order by orders.advertiser,items.name
		;');
    // $arr=array_merge($arr1,$arr2);
    $list = '';
    foreach ($arr['items_by_advertisers'] as $v) {
        temp_var('advertiser', (empty($v['advertiser']) ? '' : $v['advertiser']));
        temp_var('name', $v['name']);
        temp_var('q', $v['q']);
        temp_var('sum_cost_kzt', $v['sum_cost_kzt']);
        temp_var('sum_real_cost_kzt', $v['sum_real_cost_kzt']);
        $list .= template('admin/stat/orders_in_day/item_stat_by_advertisers');
    }
    clear_temp_vars();
    $items_stat_by_advertisers = $list;
    $arr['min_max'] = query_assoc('select date(min(date)) as min_date, date(max(date)) as max_date
		from '.tabname('shop', 'orders').'
		;');
    // �������� ������ ��������� ����
    $arr['orders_all'] = query_assoc('select count(*) as q
		from '.tabname('shop', 'orders').'
		where date(orders.date)=\''.$date.'\' and orders.status not in (\'new\',\'canceled\',\'unavailable\')
		;');
    $arr['orders'] = query_arr('select orders.id,status
		from '.tabname('shop', 'orders').'
		where date(orders.date)=\''.$date.'\' and orders.status not in (\'new\',\'canceled\',\'unavailable\')
		and '.$sql['advertiser'].' and '.$sql['utm_source'].'
		;');
    $arr['orders_by_advertisers'] = query_arr('select advertiser,count(*) as q
		from '.tabname('shop', 'orders').'
		where date(orders.date)=\''.$date.'\' and advertiser is not null
		and orders.status not in (\'new\',\'canceled\',\'unavailable\')
		group by advertiser
		;');
    $arr['orders_by_utm_sources'] = query_arr('select utm_source,count(*) as q
		from '.tabname('shop', 'orders').'
		where date(orders.date)=\''.$date.'\' and advertiser is null
		and orders.status not in (\'new\',\'canceled\',\'unavailable\')
		group by utm_source
		;');
    $arr['orders_by_hours'] = readdata(query('select date_format(date,\'%H\') as hour,
		sum(if(status not in (\'new\',\'unavailable\',\'canceled\'),1,0)) as q_confirmed,
		count(orders.id) as q_all
		from '.tabname('shop', 'orders').'
		where date(orders.date)=\''.$date.'\'
		and '.$sql['advertiser'].' and '.$sql['utm_source'].'
		group by hour
		;'), 'hour');
    $arr['ad_cost'] = query_assoc('
		select ifnull(round(sum(if(currency=\'kzt\',cost,0))+sum(if(currency=\'rub\',cost*(select rate from '.typetab('rates').' where date=date(ad.date)),0))),0) as kzt
		from '.typetab('ad').'
		where date=\''.$date.'\' and '.$sql['ad_cost']['utm_source'].'
		');
    // pre($arr['ad_cost']);
    $list = '';
    $sum_profits = 0;
    $sum_real_profits = 0;
    foreach ($arr['orders'] as $v) {
        $list .= order_stat(['order_id' => $v['id']]);
        $order_info = cache('preparing_order_info', ['order_id' => $v['id']], 0);
        // pre($order_info);
        $sum_profits = $sum_profits + $order_info['profit'];
        $sum_real_profits = $sum_real_profits + $order_info['real_profit'];
    }
    temp_var('sum_profits', $sum_profits - $arr['ad_cost']['kzt']);
    temp_var('sum_real_profits', $sum_real_profits - $arr['ad_cost']['kzt']);
    temp_var('orders', $list);
    $list = '';
    for ($i = 0; $i < 24; $i++) {
        // foreach ($arr['orders_by_hours'] as $v) {
        $hour = str_pad($i, 2, 0, STR_PAD_LEFT);
        $q_all = empty($arr['orders_by_hours'][$hour]['q_all']) ? 0 : $arr['orders_by_hours'][$hour]['q_all'];
        $q_confirmed = empty($arr['orders_by_hours'][$hour]['q_confirmed']) ? 0 : $arr['orders_by_hours'][$hour]['q_confirmed'];
        if (! empty($list)) {
            $list .= ',';
        }
        $list .= '[\''.$hour.'\','.$q_all.','.$q_confirmed.']';
    }
    temp_var('chart_values_orders_in_day', $list);
    $list = '';
    foreach ($arr['orders_by_advertisers'] as $v) {
        $list .= '<li '.(($advertiser === $v['advertiser']) ? 'class="active"' : '').'><a href="/?page=admin/stat/orders_in_day/index&date='.$date.'&amp;advertiser='.$v['advertiser'].'">
		<!--<span class="glyphicon glyphicon-user"></span>--> '.$v['advertiser'].' <span class="badge">'.$v['q'].'</span>
		</a></li>';
    }
    temp_var('advertisers', $list);
    $list = '';
    foreach ($arr['orders_by_utm_sources'] as $v) {
        $utm = empty($v['utm_source']) ? '��� �����' : $v['utm_source'];
        $list .= '<li '.(($utm_source === $v['utm_source']) ? 'class="active"' : '').'><a href="/?page=admin/stat/orders_in_day/index&date='.$date.'&amp;utm_source='.$v['utm_source'].'">
		<!--<span class="glyphicon glyphicon-tags"></span>--> '.$utm.' <span class="badge">'.$v['q'].'</span>
		</a></li>';
    }
    temp_var('sum_ad_cost_kzt', $arr['ad_cost']['kzt']);
    temp_var('utm_sources', $list);
    temp_var('q_all', $arr['orders_all']['q']);
    temp_var('all_active', ($advertiser === false and $utm_source === false) ? 'class="active"' : '');
    $datetime = new datetime($date);
    $datetime->modify('-1 day');
    $previous_date = $datetime->format('Y-m-d');
    // pre(var_dump($arr['min_max']));
    $previous_date = ($previous_date < $arr['min_max']['min_date']) ? '' : $previous_date;
    temp_var('previous_date', $previous_date);
    $datetime->modify('+2 day');
    $next_date = $datetime->format('Y-m-d');
    $next_date = ($next_date > $arr['min_max']['max_date']) ? '' : $next_date;
    temp_var('next_date', $next_date);
    // set_timezone('+6');
    set_timezone_alt($current_timezone);
    temp_var('items_stat_by_advertisers', $items_stat_by_advertisers);
    temp_var('date', $date);
    $content = template('admin/stat/orders_in_day/content');

    return $content;
}

function stat_orders_by_days()
{
    // $mysql['where']['item_id']=isset($_GET['item_id'])?'packages.item_id='.$_GET['item_id']:'1';
    // if ((select count(*) from '.tabname('shop','packages').' where order_id=orders.id and item_id=\''.$_GET['item_id'].'\')=1,1,0) as item_q
    $item_id = isset($_GET['item_id']) ? $_GET['item_id'] : false;
    $sql['item']['having'] = $item_id ? 'having packages.item_id='.$item_id : '';
    $sql['item']['group'] = $item_id ? ',packages.item_id' : '';
    $sql['item']['join'] = $item_id ? 'join '.tabname('shop', 'packages').' on packages.order_id=orders.id' : '';
    $sql['item']['fields'] = $item_id ? ',packages.item_id,sum(packages.q) as item_q' : '';
    // $arr['items_stat']=preparing_items_stat();
    // $arr['sources_stat']=preparing_sources_stat();
    $arr['work']['return'] = value_from_config('shop', 'delivery_back_cost');
    $arr['work']['call'] = value_from_config('shop', 'call_cost');
    $arr['work']['delivery'] = value_from_config('shop', 'delivery_cost');
    $arr['work']['pack'] = value_from_config('shop', 'pack_cost');
    $arr['work']['track'] = value_from_config('shop', 'track_cost');
    $arr['work']['zip'] = value_from_config('shop', 'zip_cost');
    $work_cost = $arr['work']['call'] + $arr['work']['delivery'] + $arr['work']['pack'] + $arr['work']['track'] + $arr['work']['zip'];
    // pre($work_cost);
    $paid_percent_common = 100 - value_from_config('shop', 'return_percent');
    //
    $current_timezone = get_timezone();
    set_timezone('+3');
    // $arr['orders_info']=cache('preparing_order_info_all',array(),0);
    $page = empty($_GET['p']) ? 1 : (int) $_GET['p'];
    $per_page = 30;
    $from = ($page - 1) * $per_page;
    // ini_set("memory_limit", "640M");
    $arr['orders'] = query_arr('
		select sql_calc_found_rows date(orders.date) as date,utm_source,count(*) as q_all,
		/* ifnull(ad.cost_kzt,0) as ad_cost_kzt, */
		ifnull((select round(sum(if(currency=\'kzt\',cost,0))+sum(if(currency=\'rub\',cost*(select rate from '.typetab('rates').' where date=date(orders.date)),0))) as ad_cost_kzt from '.typetab('ad').' where date=date(orders.date)),0) as ad_cost_kzt,
		sum(status not in (\'new\',\'canceled\',\'unavailable\')) as q_confirmed,
		sum((select sum(cost_kzt*q) from '.tabname('shop', 'packages').' where order_id=orders.id)) as cost_kzt_all,
		sum(if(status not in (\'new\',\'canceled\',\'unavailable\'),(select sum(cost_kzt*q) from '.tabname('shop', 'packages').' where order_id=orders.id),0)) as cost_kzt_confirmed,
		sum(if(status not in (\'new\',\'canceled\',\'unavailable\'),(select sum(item_cost_kzt*q) from '.tabname('shop', 'packages').' where order_id=orders.id),0)) as real_cost_kzt_confirmed,
		sum(if(status in (\'paid\',\'take payment\'),(select sum(cost_kzt*q) from '.tabname('shop', 'packages').' where order_id=orders.id),0)) as cost_kzt_paid,
		sum(if(status in (\'paid\',\'take payment\',\'refund paid\',\'refund\'),(select sum(cost_kzt*q) from '.tabname('shop', 'packages').' where order_id=orders.id),0)) as cost_kzt_paid_all,
		sum(if(status in (\'paid\',\'take payment\'),(select sum(item_cost_kzt*q) from '.tabname('shop', 'packages').' where order_id=orders.id),0)) as real_cost_kzt_paid,
		sum(if(status in (\'return\',\'return paid\'),(select sum(item_cost_kzt*q) from '.tabname('shop', 'packages').' where order_id=orders.id),0)) as real_cost_kzt_return,
		sum(status in (\'return\',\'return paid\')) as q_return,
		sum(status in (\'paid\',\'take payment\')) as q_paid
		'.$sql['item']['fields'].'
		from '.tabname('shop', 'orders').'
		left join '.typetab('packages').' main_package on main_package.order_id=orders.id and main_package.item_id=orders.main_item
		/* left join '.tabname('shop', 'ad').' on ad.date=date(orders.date) */
		'.$sql['item']['join'].'
		-- where date(orders.date) between \'2016-05-01\' and \'2016-05-31\'
		group by date(orders.date) desc '.$sql['item']['group'].' /* with rollup */
		'.$sql['item']['having'].'
		-- order by date desc
		limit '.$from.','.$per_page.'
		;
		');
    // pre($arr);
    $q_paid = 0;
    $q_all = 0;
    $rows = rows_without_limit();
    $list = '';
    foreach ($arr['orders'] as $v) {
        $profit_sum = 0;
        $real_profit_sum = 0;
        $orders_day = query_arr('
			select id from '.typetab('orders').'
			where date(date)=\''.$v['date'].'\' and
			status not in (\'new\',\'canceled\',\'unavailable\')
			-- status in (\'paid\',\'take payment\',\'delivered not paid\',\'return\',\'return paid\',\'refund\',\'refund paid\')
			;');
        $advertiser_bonuses_sum = 0;
        foreach ($orders_day as $v2) {
            $order = preparing_order_info(['order_id' => $v2['id']]);
            // $profit=round(($order['cost_kzt']-$order['real_cost_kzt'])*$order['buyout']/100-$arr['work']['return']*(100-$order['buyout'])/100-$work_cost);
            $profit_sum = $profit_sum + $order['profit'];
            $real_profit_sum = $real_profit_sum + $order['real_profit'];
            $advertiser_bonuses_sum = $advertiser_bonuses_sum + $order['advertiser_bonus_kzt'];
            $q_paid += in_array($order['status'], ['paid', 'take payment']) ? 1 : 0;
            $q_all++;
        }
        $expenses = $v['real_cost_kzt_confirmed'] + $work_cost * $v['q_confirmed'] + $v['ad_cost_kzt'] + $advertiser_bonuses_sum;
        $cost_of_confirm = round(($v['ad_cost_kzt'] + $advertiser_bonuses_sum) / ($v['q_confirmed'] == 0 ? 1 : $v['q_confirmed']));
        // $real_return=$v['real_cost_kzt_return']-$arr['work']['return']*$v['q_return'];
        // $real_income=$v['cost_kzt_paid']-$v['real_cost_kzt_confirmed']-$work_cost*$v['q_confirmed']-$v['ad_cost_kzt']-$advertiser_bonuses_sum+$real_return;
        // ������ ������� �������� ���� ������ ���, ��� ��� ��� �� ��������� �������� �������
        $maybe_return = $v['real_cost_kzt_confirmed'] - $v['real_cost_kzt_paid'] - $arr['work']['return'] * ($v['q_confirmed'] - $v['q_paid']);
        $maybe_income = $v['cost_kzt_paid'] - $expenses + $maybe_return;
        $profit = $profit_sum - $v['ad_cost_kzt'];
        $profit_all = (empty($profit_all) ? 0 : $profit_all) + $profit;
        $maybe_income_all = (empty($maybe_income_all) ? 0 : $maybe_income_all) + $maybe_income;
        $real_profit = $real_profit_sum - $v['ad_cost_kzt'];
        $real_profit_all = (empty($real_profit_all) ? 0 : $real_profit_all) + $real_profit;
        temp_var('date', $v['date']);
        temp_var('q_all', $v['q_all']);
        temp_var('q_confirmed', $v['q_confirmed']);
        temp_var('percent_of_confirmed', ceil($v['q_confirmed'] / ($v['q_all'] == 0 ? 1 : $v['q_all']) * 100));
        temp_var('cost_kzt_confirmed', $v['cost_kzt_confirmed']);
        temp_var('expenses', ($work_cost * $v['q_confirmed'] + $v['real_cost_kzt_confirmed']));
        temp_var('ad_cost', $v['ad_cost_kzt']);
        temp_var('bonuses_cost', $advertiser_bonuses_sum);
        temp_var('cost_of_confirmation', $cost_of_confirm);
        // ����� �� ����� �������� ������ ������, �.�. ��� ����� � �������. �������� ������ ������� �����������
        temp_var('profit', $profit);
        temp_var('real_buyout', round($v['q_paid'] / ($v['q_confirmed'] == 0 ? 1 : $v['q_confirmed']) * 100));
        // temp_var('real_profit',$real_profit);
        temp_var('real_profit', $maybe_income);
        temp_var('cost_kzt_paid_all', $v['cost_kzt_paid_all']);
        temp_var('real_cost_kzt_return', $v['real_cost_kzt_return']);
        $list .= template('admin/stat/orders_by_days/item');
    }
    clear_temp_vars();
    temp_var('list', $list);
    temp_var('pagination', page_numbers($rows, $per_page, $page, '/?page=admin/stat/orders_by_days/index&amp;p=', 5, '', '&laquo;', '&raquo;', 'yes', 'active', 'disabled', 1, '', 'yes', 'yes'));
    temp_var('profit_all', $profit_all);
    // temp_var('real_profit_all',$real_profit_all);
    temp_var('real_profit_all', $maybe_income_all);
    temp_var('orders_all', $q_all);
    temp_var('orders_paid', $q_paid);
    // set_timezone('+6');
    set_timezone_alt($current_timezone);
    $content = template('admin/stat/orders_by_days/table');

    return $content;
}

function admin_check_callers()
{
    $order_id = empty($_GET['order_id']) ? error('�� ������� order_id') : $_GET['order_id'];
    $arr = query_arr('select login,date,comment from '.tabname('shop', 'orders_calls').'
		where order_id=\''.$order_id.'\'
		order by date
		;');
    if (! $arr) {
        $answer = '����� �� ������';
    } else {
        $list = '';
        foreach ($arr as $v) {
            temp_var('login', $v['login']);
            temp_var('date', $v['date']);
            temp_var('comment', empty($v['comment']) ? '' : $v['comment']);
            $list .= template('admin/orders/edit_order/item_check_callers');
            // $list.='<li><b>'.$v['login'].'</b> '.$v['date'].' '.$v['comment'].'</li>';
            clear_temp_vars();
        }
        temp_var('list', $list);
        $answer = template('admin/orders/edit_order/block_check_callers');
        // $answer='<ol>'.$list.'</ol>';
    }

    return $answer;
}

function admin_submit_caller_comment()
{
    $login = auth_login(['type' => 'shop']);
    $order_id = empty($_POST['order_id']) ? error('�� ������� order_id') : $_POST['order_id'];
    $comment = empty($_POST['comment']) ? '' : check_input(fix_encode(trim($_POST['comment']), 'utf-8'));
    query('insert into '.typetab('orders_calls').' set order_id=\''.$order_id.'\', login=\''.$login.'\', comment=\''.$comment.'\';');

    // temp_var('login',$login);
    // $return=template('admin/orders/edit_order/set_caller');
    // clear_temp_vars();
    return '<b>'.$login.'</b> ��������';
}

function admin_set_caller()
{
    $login = auth_login(['type' => 'shop']);
    $order_id = empty($_GET['order_id']) ? error('�� ������� order_id') : $_GET['order_id'];
    query('insert into '.tabname('shop', 'orders_calls').' set order_id=\''.$order_id.'\', login=\''.$login.'\';');

    // temp_var('login',$login);
    // $return=template('admin/orders/edit_order/set_caller');
    // clear_temp_vars();
    return '<b>'.$login.'</b> ��������';
}

function paid_stat()
{
    $var['status'] = check_status(isset($_GET['status']) ? $_GET['status'] : 'all');
    $var['source'] = check_source(isset($_GET['source']) ? $_GET['source'] : '');
    $var['country'] = check_country(isset($_GET['country']) ? $_GET['country'] : '');
    $arr_all = query_assoc('select count(*) as q from '.tabname('shop', 'orders').'
		where '.$var['source']['where'].' and '.$var['country']['where'].'
		;');
    $data = query('select status,count(*) as q from '.tabname('shop', 'orders').'
		where '.$var['source']['where'].' and '.$var['country']['where'].'
		group by status
		;');
    $arr = readdata($data, 'status');
    $all_statuses = enum_to_array(tabname('shop', 'orders'), 'status', shop_translation('order_status'));
    $sum['all'] = $arr_all['q'];
    // pre($all_statuses);
    foreach ($all_statuses as $k => $v) {
        $q[$k] = isset($arr[$k]['q']) ? (int) $arr[$k]['q'] : 0;
    }
    $sum['process'] = $q['new'] + $q['unavailable'] + $q['waiting send'] + $q['check address'] + $q['packed'];
    $sum['canceled_and_return'] = $q['canceled'] + $q['return'] + $q['return paid'] + $q['refund'] + $q['refund paid'];
    $sum['paid_all'] = $q['paid'] + $q['take payment'];
    $sum['sent_all'] = $q['delivered not paid'] + $q['sent'];
    foreach ($sum as $k => $v) {
        $p_sum[$k] = round($sum[$k] / $sum['all'] * 100);
    }
    // ���������� cancaled_and_return, ����� �� ���� ������ 100%
    $p_sum['canceled_and_return'] = 100 - $p_sum['process'] - $p_sum['paid_all'] - $p_sum['sent_all'];
    $sum2['paid_all'] = $q['paid'] + $q['take payment'];
    $sum2['not_paid_all'] = $q['delivered not paid'] + $q['return'] + $q['return paid'] + $q['refund'] + $q['refund paid'];
    $sum2['delivered_all'] = $sum2['paid_all'] + $sum2['not_paid_all'];
    $p_sum2['paid_all'] = round($sum2['paid_all'] / ($sum2['delivered_all']) * 100);
    $p_sum2['not_paid_all'] = 100 - $p_sum2['paid_all'];
    set_config('shop', 'return_percent', $p_sum2['not_paid_all']);
    $content = '
	<h3>���������� �� ����������</h3>
	<div class="progress">
	<div class="progress-bar progress-bar-success" style="width: '.$p_sum2['paid_all'].'%">
	<span>���������� � �������� '.$p_sum2['paid_all'].'% ('.$sum2['paid_all'].')</span>
	</div>
	<div class="progress-bar progress-bar-warning" style="width: '.$p_sum2['not_paid_all'].'%">
	<span>���������� � �� �������� '.$p_sum2['not_paid_all'].'% ('.($sum2['not_paid_all']).')</span>
	</div>
	</div>
	<div class="progress">
	<div class="progress-bar progress-bar-success" style="width: '.$p_sum['paid_all'].'%">
	<span>�������� '.$p_sum['paid_all'].'% ('.$sum['paid_all'].')</span>
	</div>
	<div class="progress-bar progress-bar-info" style="width: '.$p_sum['process'].'%">
	<span>���� �������� '.$p_sum['process'].'% ('.($sum['process']).')</span>
	</div>
	<div class="progress-bar progress-bar-warning" style="width: '.$p_sum['sent_all'].'%">
	<span>� ���� '.$p_sum['sent_all'].'% ('.($sum['sent_all']).')</span>
	</div>
	<div class="progress-bar progress-bar-danger" style="width: '.$p_sum['canceled_and_return'].'%">
	<span>������ � ������� '.$p_sum['canceled_and_return'].'% ('.($sum['canceled_and_return']).')</span>
	</div>
	</div>
	';

    return $content;
}

function admin_order_sent_sms()
{
    if (empty($_GET['order_id'])) {
        error('�� ������� order_id');
    }
    $order_id = check_input($_GET['order_id']);
    $arr = query_arr('select date,phone,message,answer from '.tabname('shop', 'sms').' where order_id=\''.$order_id.'\';');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['date'].'</td><td>'.$v['phone'].'</td><td>'.$v['message'].'</td><td>'.$v['answer'].'</td></tr>';
    }
    $content = '
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">������������ ��� ��� ������ #'.$order_id.'</h4>
	</div>
	<div class="modal-body">
	<table class="table table-striped">
	<thead><tr>
	<th>����</th>
	<th>�����</th>
	<th>���������</th>
	<th>���������</th>
	</tr></thead>
	'.$list.'
	</table>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
	</div>
	';

    return $content;
}

function admin_sms_text($args)
{
    if (empty($args['order_id'])) {
        error('�� ������� ID ������');
    }
    if (empty($args['format'])) {
        error('�� ������� ������ ���');
    }
    $login = auth_login(['type' => 'shop']);
    $arr = query_assoc('select text,kazpost_info from '.typetab('sms_templates').' where name=\''.$args['format'].'\';');
    if (empty($arr)) {
        error('��� ������ '.$args['format'].' �� ����������');
    }
    $login_info = cache('preparing_login_info', ['login' => $login], 0);
    // $arr=preparing_order_items(array('order_id'=>$args['order_id']));
    $order_info = preparing_order_info(['order_id' => $args['order_id']]);
    // pre($order_info);
    $discount_item_id = value_from_config('shop', 'discount_item_id');
    foreach ($order_info['items'] as $v) {
        $item = cache('preparing_item_info', ['id' => $v['id']], 0);
        // pre($item);
        if ($item['show_sms'] == '1') {
            $item_name = empty($item['sms_text']) ? $item['name'] : $item['sms_text'];
            $items_sms_arr[$item_name] = (isset($items_sms_arr[$item_name]) ? $items_sms_arr[$item_name] : 0) + $v['q'];
        }
    }
    $items_list = '';
    foreach ($items_sms_arr as $name => $q) {
        if (! empty($items_list)) {
            $items_list .= ',';
        }
        if ($q != 1) {
            $q_text = ' '.$q.' ��.';
        } else {
            $q_text = '';
        }
        $items_list .= $name.$q_text;
    }
    // $arr=query_assoc('select orders.track from '.tabname('shop','orders').' where id=\''.$args['order_id'].'\';');
    if ($arr['kazpost_info']) {
        // ���� ����� ���������, ������� ������� �� �����, ���� ���, �� �� ���������� �������
        $kazpost_last_status = kazpost_last_status(['track' => $order_info['track']]);
        // pre($kazpost_last_status);
        if (empty($kazpost_last_status['zip'])) {
            error('��� ������� ��� ������ '.$args['order_id']);
        }
        // �������� ���������� ��������� �� ������� �����
        $post_info = get_post_info_kz(['zip' => $kazpost_last_status['zip']]);
        // ������ ������� ��������� ��� 010000
        if ($kazpost_last_status['zip'] == '010000') {
            [$post_info['result'][0], $post_info['result'][1]] = [$post_info['result'][1], $post_info['result'][0]];
        }
        // �������� ���������� ��������� �� �����
        $track_info = get_track_info_kz(['track' => $order_info['track'], 'from' => 'db']);
        // ��������� ���� �� ����� ��������� �� �����
        $post_info_alt = empty($track_info['info']['last']['address']) ? false : $track_info['info']['last']['address'];
        // ���� ���� ����� ��������� �� �����, �� ���������� ���, ���� ��� - �� �� �������, ���� � ��� ���, �� fasle
        $post_info_result = $post_info_alt ? $post_info_alt : (empty($post_info['result'][0]) ? false : $post_info['result'][0]['address']);
        // if (empty($post_info['result'][0])) {
        if (! $post_info_result) {
            $return['status'] = false;
            $return['text'] = '��� ���������� �� ������� '.$kazpost_last_status['zip'].'. ����� #'.$order_info['order_id'];

            return $return;
        }
        temp_var('kazpost_post_info', kz_clean_address($post_info_result));
        temp_var('kazpost_last_zip', $kazpost_last_status['zip']);
        temp_var('kazpost_last_place', $kazpost_last_status['place']);
        // ���� ������� �������
        $arr_ems_phone = query_assoc('select ems_phone
			from '.typetab('cities').'
			where \''.$order_info['address'].'\' like CONCAT_WS(\'\',\'%\',name,\'%\')
			limit 1
			');
        temp_var('kazpost_ems_phone', empty($arr_ems_phone['ems_phone']) ? '' : str_replace(' ', '', $arr_ems_phone['ems_phone']));
    }
    temp_var('login_phone', empty($login_info['phone']) ? '' : $login_info['phone']);
    temp_var('items_list', $items_list);
    temp_var('order_track', empty($order_info['track']) ? '' : $order_info['track']);
    temp_var('name', $order_info['client_name']);
    temp_var('order_id', $order_info['order_id']);
    $return['status'] = true;
    $return['text'] = template($arr['text'], 1);
    clear_temp_vars();
    /*
    if ($args['format']=='zip_address') {
        $return['status']=true;
        $return['text']='��� ����� ('.$items_list.') �� ����� '.$order_info['zip'].' '.kz_clean_address($post_info_result);
    };
    if ($args['format']=='zip_address_track') {
        $return['status']=true;
        $return['text']='��� ����� ('.$items_list.') �� ����� '.$order_info['zip'].' '.kz_clean_address($post_info_result).'. ����: '.$order_info['track'];
    };
    if ($args['format']=='track') {
        $return['status']=true;
        $return['text']='��� ����� ('.$items_list.') �� �����. ����� �����������: '.$order_info['track'];
    };
    if ($args['format']=='track_sent') {
        $return['status']=true;
        $return['text']='��� ����� ('.$items_list.') ���������. ������������: https://post.kz/mail/search/track/'.$order_info['track'].'/detail';
    };
    if ($args['format']=='zip_place_track') {
        $return['status']=true;
        $return['text']='��� ����� ('.$items_list.') �� ����� '.$order_info['place'].'. ����: '.$order_info['track'];
    };
    if ($args['format']=='callback') {
        $return['status']=true;
        $return['text']='�� �������� '.$items_list.'. ������������� �� ���. '.$login_info['phone'];
    };
    if ($args['format']=='watches') {
        $return['status']=true;
        $return['text']='������. �� �������� � ��� ����, �� ������ ��� �������. ������ 22% �� ����� ����. ������� ��� "������" ���������. http://elitbrand.com/watches';
    };
    if ($args['format']=='all_goods') {
        $return['status']=true;
        $return['text']='������. �� ������ � ��� ������� �����. �� ����� ��� �������. ������ 22% �� ����� ������ ��������. ������� ��� "������" ���������. http://hit-kz.com';
    };
    if (empty($return['text'])) {
        $return['status']=false;
        $return['text']='��� ��������';
    };
    */
    // pre($content);
    $return['text'] = translit_ru($return['text']);

    return $return;
}

function admin_submit_send_sms($args = [])
{
    $login = auth_login(['type' => 'shop']);
    if (empty($args)) {
        $args['order_id'] = empty($_POST['order_id']) ? error('�� ������� order_id') : check_input($_POST['order_id']);
        $args['phone'] = empty($_POST['phone']) ? error('�� ������� �������') : check_input(fix_encode($_POST['phone'], 'utf-8'));
        $args['message'] = empty($_POST['message']) ? error('�� �������� ���������') : check_input(fix_encode($_POST['message'], 'utf-8'));
        $args['action'] = empty($_GET['action']) ? '' : check_input($_GET['action']);
        $args['format'] = empty($_POST['format']) ? false : $_POST['format'];
    } else {
        $args['order_id'] = empty($args['order_id']) ? error('�� ������� order_id') : check_input($args['order_id']);
        $args['phone'] = empty($args['phone']) ? error('�� ������� �������') : check_input(fix_encode($args['phone'], 'utf-8'));
        $args['message'] = empty($args['message']) ? error('�� �������� ���������') : check_input(fix_encode($args['message'], 'utf-8', 'mb'));
        $args['action'] = empty($args['action']) ? '' : check_input($args['action']);
        $args['format'] = empty($args['format']) ? false : $args['format'];
    }
    // if (empty($_POST['order_id'])) {error('�� ������� order_id');};
    // if (empty($_POST['phone'])) {error('�� ������� �������');};
    // if (empty($_POST['message'])) {error('�� �������� ���������');};
    // if (empty($_GET['action'])) {$action='';} else {$action=check_input($_GET['action']);};
    // admin_send_sms($args);
    $answer = smsc_send_message(['phone' => $args['phone'], 'message' => $args['message'], 'action' => $args['action']]);
    $answer = check_input($answer);
    $delivered = (substr($answer, 0, 2) === 'OK') ? true : false;
    // pre(substr($answer,0,2));
    // pre(var_dump($delivered));
    if ($args['action'] != 'check_cost') {
        query('insert into '.tabname('shop', 'sms').'
		set order_id=\''.$args['order_id'].'\',
		phone=\''.$args['phone'].'\',
		message=\''.$args['message'].'\',
		answer=\''.$answer.'\',
		delivered='.($delivered ? 1 : 0).',
		'.($args['format'] ? 'format=\''.$args['format'].'\',' : '').'
		login=\''.$login.'\'
		;');
    }

    // query('update '.tabname('shop','orders').'
    // 	set info=concat_ws(\'\',info,\'<br/>���������� SMS: \',current_timestamp)
    // 	where order_id=\''.$_POST['order_id'].'\';');
    return $answer;
}

function admin_order_send_sms()
{
    if (empty($_GET['order_id'])) {
        error('�� ������� order_id');
    }
    $order_id = check_input($_GET['order_id']);
    $order_info = preparing_order_info(['order_id' => $order_id]);
    $arr = query_arr('select name from '.typetab('sms_templates').' where show_caller;');
    // pre($arr);
    $sms_templates = '';
    foreach ($arr as $v) {
        $return = admin_sms_text(['order_id' => $order_id, 'format' => $v['name']]);
        $sms_templates .= '<option>'.$return['text'].'</option>';
    }
    // pre($arr);
    // $sms_text['track']=admin_sms_text(array('order_id'=>$order_id,'format'=>'track'));
    // $sms_text['callback']=admin_sms_text(array('order_id'=>$order_id,'format'=>'callback'));
    $zip_text = empty($order_info['zip']) ? '' : '<br/><span class="js-link" data-role="show_post_info" data-element="post_info"
	data-zip="'.$order_info['zip'].'" data-track="'.$order_info['track'].'" data-addr_link="1">������ ����� ���������</span>';
    temp_var('order_id', $order_id);
    temp_var('order_phone', $order_info['phone']);
    temp_var('zip_text', $zip_text);
    temp_var('sms_templates', $sms_templates);
    $content = template('admin/sms/block_send_sms');
    clear_temp_vars();

    return $content;
}

function send_sms_form($args)
{
    $order_id = (empty($args['order_id'])) ? error('�� ������� ID ������') : $args['order_id'];
    $phone = (empty($args['phone'])) ? error('�� ������� ������� ��� ������ '.$args['order_id']) : $args['phone'];
    $text = (empty($args['text'])) ? error('�� ������� �����') : $args['text'];
    $format = empty($args['format']) ? false : $args['format'];
    $content = '
	<form class="form-inline" id="sendSms_'.$order_id.'">
	<div class="form-group">
	<!--<label for="phone_'.$order_id.'">�������:</label>-->
	<input type="text" id="phone_'.$order_id.'" name="phone" value="'.$phone.'" placeholder="�������"
	class="form-control">
	</div>
	<div class="form-group">
	<!--<label for="message_'.$order_id.'">���������:</label>-->
	<textarea style="width: 500px;" id="message_'.$order_id.'"
	onkeyup="document.getElementById(\'message_length_'.$order_id.'\').innerHTML=document.getElementById(\'message_'.$order_id.'\').value.length;"
	name="message" placeholder="���������" class="form-control">'.$text.'</textarea>
	</div>
	<button type="button" class="btn btn-primary" data-role="ajax_submit_send_sms_mass"
	id="send_sms_'.$order_id.'" data-order_id="'.$order_id.'" data-loading-text="�����������..." autocomplete="off">���������</button>
	<button type="button" class="btn btn-primary" data-role="ajax_submit_send_sms_check_cost_mass"
	id="check_cost_'.$order_id.'" data-order_id="'.$order_id.'" data-loading-text="������..." autocomplete="off">����</button>
	<div class="form-group">
	��������: <span id="message_length_'.$order_id.'">'.strlen($text).'</span>
	<span id="sms_answer_'.$order_id.'"></span>
	</div>
	<input type="hidden" name="order_id" value="'.$order_id.'">
	<input type="hidden" name="format" value="'.$format.'">
	</form>
	';

    return $content;
}

function preparing_login_info($args)
{
    $arr = query_assoc('
		select users.login,users.id,users.phone,users.token,
		users.paid_calls,
		(select count(*) from '.typetab('orders').' where login=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_calls as not_paid_calls,
		users.paid_calls_vip,
		(select count(distinct orders.id) from '.typetab('packages').'
			join '.typetab('orders').' on orders.id=packages.order_id
			left join (
			select items_groups.item_id
			from '.typetab('item_groups').'
			join '.typetab('items_groups').' on items_groups.group_id=item_groups.id
			where item_groups.vip_call
			) vip_items on vip_items.item_id=packages.item_id
			where login=users.login and vip_items.item_id is not null
			and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_calls_vip as not_paid_calls_vip,
	users.paid_zips,
	(select count(*) from '.typetab('orders').' where zipper=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_zips as not_paid_zips,
	users.paid_tracks,
	(select count(*) from '.typetab('orders').' where tracker=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_tracks as not_paid_tracks,
	users.paid_packs,
	(select count(*) from '.typetab('orders').' where packer=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_packs as not_paid_packs,
	(select (ifnull(sum(orders.bonus),0)-(select ifnull(sum(sum),0) from '.typetab('purchase').' where currency=\'rur\' and login=users.login))
		from '.typetab('orders').'
		/* join '.typetab('cpa').' on cpa.order_id=orders.id */
		/* join '.typetab('packages').' on packages.order_id=orders.id and packages.item_id=orders.main_item */
		where users.level=\'advertiser\' and orders.advertiser=users.login and orders.user_status not in (\'canceled\',\'unavailable\',\'new\')) as advertiser_balance,
		(select (ifnull(sum(orders.bonus),0)-(select ifnull(sum(sum),0) from '.typetab('purchase').' where currency=\'rur\' and login=users.login))
		from '.typetab('orders').'
		where users.level=\'advertiser\' and orders.advertiser=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\')) as advertiser_balance_real
	from '.typetab('users').'
	where login=\''.$args['login'].'\'
	;');

    // pre($arr);
    return $arr;
}

function preparing_logins_info()
{
    $logins = query_arr('select login from '.typetab('users').';');
    /*
    $calls=readdata(query('
        select users.login,users.paid_calls,
        (select count(*) from '.tabname('shop','orders').' where login=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_calls as not_paid_calls
        from '.tabname('shop','users').';'),'login');
    $packs=readdata(query('select users.login,users.paid_packs,(count(*)-users.paid_packs) as not_paid_packs
        from '.tabname('shop','orders').'
        join '.tabname('shop','users').' on users.login=orders.packer
        where orders.status not in (\'canceled\',\'unavailable\',\'new\')
        group by orders.packer;'),'login');
    $zips=readdata(query('select users.login,users.paid_zips,(count(*)-users.paid_zips) as not_paid_zips
        from '.tabname('shop','orders').'
        join '.tabname('shop','users').' on users.login=orders.zipper
        where orders.status not in (\'canceled\',\'unavailable\',\'new\')
        group by orders.zipper;'),'login');
    $calls_vip=readdata(query('select users.login,users.paid_calls_vip,(count(distinct orders.id)-users.paid_calls_vip) as not_paid_calls_vip
        from '.typetab('packages').'
        join '.typetab('orders').' on orders.id=packages.order_id
        join '.tabname('shop','users').' on users.login=orders.login
        where item_id in (select item_id from '.typetab('items_groups').' where group_id in (2,8))
        and orders.status not in (\'canceled\',\'unavailable\',\'new\')
        group by orders.login;'),'login');
    $tracks=readdata(query('select users.login,users.paid_tracks,(count(*)-users.paid_tracks) as not_paid_tracks
        from '.tabname('shop','orders').'
        join '.tabname('shop','users').' on users.login=orders.tracker
        where orders.status not in (\'canceled\',\'unavailable\',\'new\')
        group by orders.tracker;'),'login');
        */
    // pre($tracks);
    /*
    foreach ($logins as $v) {
        $arr[$v['login']]=array_merge(
            array('login'=>$v['login']),
            isset($calls[$v['login']])?$calls[$v['login']]:array('paid_calls'=>'0','not_paid_calls'=>'0'),
            isset($packs[$v['login']])?$packs[$v['login']]:array('paid_packs'=>'0','not_paid_packs'=>'0'),
            isset($zips[$v['login']])?$zips[$v['login']]:array('paid_zips'=>'0','not_paid_zips'=>'0'),
            isset($calls_vip[$v['login']])?$calls_vip[$v['login']]:array('paid_calls_vip'=>'0','not_paid_calls_vip'=>'0'),
            isset($tracks[$v['login']])?$tracks[$v['login']]:array('paid_tracks'=>'0','not_paid_tracks'=>'0')
            );
    };
    */
    $arr = readdata(query('
		select users.login,users.phone,users.token,
		users.paid_calls,
		(select count(*) from '.typetab('orders').' where login=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_calls as not_paid_calls,
		users.paid_calls_vip,
		(select count(distinct orders.id) from '.typetab('packages').'
			join '.typetab('orders').' on orders.id=packages.order_id
			left join (
			select items_groups.item_id
			from '.typetab('item_groups').'
			join '.typetab('items_groups').' on items_groups.group_id=item_groups.id
			where item_groups.vip_call
			) vip_items on vip_items.item_id=packages.item_id
			where login=users.login and vip_items.item_id is not null
			and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_calls_vip as not_paid_calls_vip,
	users.paid_zips,
	(select count(*) from '.typetab('orders').' where zipper=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_zips as not_paid_zips,
	users.paid_tracks,
	(select count(*) from '.typetab('orders').' where tracker=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_tracks as not_paid_tracks,
	users.paid_packs,
	(select count(*) from '.typetab('orders').' where packer=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\'))-users.paid_packs as not_paid_packs,
	(select (ifnull(sum(orders.bonus),0)-(select ifnull(sum(sum),0) from '.typetab('purchase').' where currency=\'rur\' and login=users.login))
		from '.typetab('orders').'
		/* join '.typetab('cpa').' on cpa.order_id=orders.id */
		/* join '.typetab('packages').' on packages.order_id=orders.id and packages.item_id=orders.main_item */
		where users.level=\'advertiser\' and orders.advertiser=users.login and orders.user_status not in (\'canceled\',\'unavailable\',\'new\')) as advertiser_balance,
		(select (ifnull(sum(orders.bonus),0)-(select ifnull(sum(sum),0) from '.typetab('purchase').' where currency=\'rur\' and login=users.login))
		from '.typetab('orders').'
		where users.level=\'advertiser\' and orders.advertiser=users.login and orders.status not in (\'canceled\',\'unavailable\',\'new\')) as advertiser_balance_real
	from '.typetab('users').';
	'), 'login');

    // pre($arr);
    // var_dump($arr);
    return $arr;
}

function admin_user_block()
{
    $login = auth_login(['type' => 'shop']);
    $arr = cache('preparing_login_info', ['login' => $login], 0);
    // pre($arr);
    temp_var('login', $login);
    $ui = cache('preparing_user_info', ['login' => $login, 'type' => 'shop'], 0);
    temp_var('advertiser_balance', $arr['advertiser_balance']);
    temp_var('not_paid_calls', $arr['not_paid_calls']);
    temp_var('not_paid_calls_vip', $arr['not_paid_calls_vip']);
    temp_var('not_paid_packs', $arr['not_paid_packs']);
    temp_var('not_paid_tracks', $arr['not_paid_tracks']);
    temp_var('not_paid_zips', $arr['not_paid_zips']);
    $content = '';
    if (in_array($ui['level'], ['caller', 'packer', 'packer_lite', 'admin', 'manager'])) {
        $content = template('admin/menu/balance_caller');
    }
    if (in_array($ui['level'], ['advertiser'])) {
        $content = template('admin/menu/balance_advertiser');
    }
    clear_temp_vars();

    /*
    //����� ����� ������� ����� ������� ������
    if (in_array($ui['level'],array('manager'))) {
        $paid_manager=get_paid_manager();
        $content.='<span style="margin-left: 10px;">'.($paid_manager['kzt']-value_from_config('shop','paid_manager_kzt_minus')).'</span> <span style="font-weight: bold;">&#8376;</span>';
    };
    */
    return $content;
}

// ����� ����� ������� ����� ������� ������, ������ ����� �� ���, ������� ���� �� �����
function del_get_paid_manager()
{
    $paid_manager = query_assoc('select
		sum(if(orders.country in (\'������\'),packages.cost_rur*q,0)) as rur,
		sum(if(orders.country in (\'���������\'),packages.cost_kzt*q,0)) as kzt
		from '.tabname('shop', 'packages').'
		join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where status in (\'paid\',\'refund\',\'refund paid\')
		and ((select login from '.tabname('shop', 'orders_changes').' where primary_key=orders.id and field=\'status\' and value=\'paid\' order by orders_changes.date desc limit 1)
			in (select login from '.tabname('shop', 'users').' where level=\'manager\'))
		;');

    return $paid_manager;
}

function admin_login_stat()
{
    /*
    $arr=query_arr('select orders.login,users.paid_calls,(count(*)-users.paid_calls) as not_paid_calls
        from '.tabname('shop','orders').'
        join '.tabname('shop','users').' on users.login=orders.login
        where orders.login is not null group by login order by login;');
        */
    // $arr=query_arr('select login from '.tabname('shop','users').';');
    /*
    $arr_confirmed=query_arr('
    select login,
    (select count(distinct or_ch1.primary_key) from '.tabname('shop','orders_changes').' or_ch1 where field=\'status\' and value=\'canceled\' and login=orders_changes.login) as canceled,
    (select count(distinct orders.id) from '.tabname('shop','orders_changes').' or_ch2 join '.tabname('shop','orders').' on orders.id=or_ch2.primary_key where status not in (\'new\',\'unavailable\',\'canceled\') and orders.login=orders_changes.login) as confirmed,
    (select count(distinct orders.id) from '.tabname('shop','orders_changes').' or_ch3 join '.tabname('shop','orders').' on orders.id=or_ch3.primary_key where status in (\'paid\') and orders.login=orders_changes.login) as paid,
    (select count(distinct orders.id) from '.tabname('shop','orders_changes').' or_ch4 join '.tabname('shop','orders').' on orders.id=or_ch4.primary_key where status in (\'refund\',\'refund paid\') and orders.login=orders_changes.login) as refunded
    from '.tabname('shop','orders_changes').'
    where login is not null
    group by login;
    ');
    */
    // ��� ������, ������ �������, ����������� ������
    /*
    $arr=query_arr('
        select date_format(orders.date,\'%Y-%m\') as month,orders.login,
        count(*) as q,
        (select count(distinct primary_key) from shop.orders_changes where field=\'status\' and value=\'canceled\' and login=orders.login and date_format(date,\'%Y-%m\')=month) as canceled,
        sum(if(status not in (\'new\',\'unavailable\',\'canceled\'),1,0)) as confirmed,
        sum(if(status in (\'sent\',\'delivered not paid\',\'waiting send\',\'check address\',\'packed\'),1,0)) as road,
        sum(if(status in (\'paid\',\'take payment\'),1,0)) as paid,
        sum(if(status in (\'refund\',\'refund paid\',\'return\',\'return paid\'),1,0)) as returned_refunded
        from '.typetab('orders').'
        where orders.login is not null
        group by month,orders.login
        ');
        */
    // pre($arr);
    // �������� ������ � ������ ������ ���������
    // ������ �������� �����������
    /* ������ ��������������� ����� ������ �������
    foreach ($arr as $v) {
        $confirms[$v['month']][$v['login']]=$v;
    };
    // ��� ������ ��� �������
    $callers=query_arr('select distinct orders.login
        from '.typetab('orders').'
        join '.typetab('users').' on users.login=orders.login
        where orders.login is not null and users.show_in_approve_chart order by login;');
    $callers_list='';
    foreach ($callers as $v) {
        if ($callers_list!=='') {$callers_list.=',';};
        $callers_list.='\''.$v['login'].'\'';
    };
    // pre($callers_list);
    // ��� ������ ��� �������
    $months=query_arr('select distinct date_format(orders.date,\'%Y-%m\') as month from '.typetab('orders').' order by month;');
    $list['approved']='';
    $list['paid']='';
    $list['returned_refunded']='';
    foreach ($months as $month) {
        $list_values['approved']='';
        $list_values['paid']='';
        $list_values['returned_refunded']='';
        foreach ($callers as $caller) {
            $v=isset($confirms[$month['month']][$caller['login']])?$confirms[$month['month']][$caller['login']]:array('q'=>0,'canceled'=>0,'confirmed'=>0,'road'=>0,'paid'=>0,'returned_refunded'=>0);
            //
            $all=$v['confirmed']+$v['canceled'];
            $confirmed=($all===0)?0:round($v['confirmed']/$all*100);
            $paid=(($v['confirmed']-$v['road'])===0)?0:round($v['paid']/($v['confirmed']-$v['road'])*100);
            $returned_refunded=(($v['confirmed']-$v['road'])===0)?0:round($v['returned_refunded']/($v['confirmed']-$v['road'])*100);
            //
            if ($list_values['approved']!=='') {$list_values['approved'].=',';};
            $list_values['approved'].=$confirmed;
            if ($list_values['paid']!=='') {$list_values['paid'].=',';};
            $list_values['paid'].=$paid;
            if ($list_values['returned_refunded']!=='') {$list_values['returned_refunded'].=',';};
            $list_values['returned_refunded'].=$returned_refunded;
        };
        // pre($list_values);
        if ($list['approved']!=='') {$list['approved'].=',';};
        $list['approved'].='[\''.$month['month'].'\','.$list_values['approved'].']';
        if ($list['paid']!=='') {$list['paid'].=',';};
        $list['paid'].='[\''.$month['month'].'\','.$list_values['paid'].']';
        if ($list['returned_refunded']!=='') {$list['returned_refunded'].=',';};
        $list['returned_refunded'].='[\''.$month['month'].'\','.$list_values['returned_refunded'].']';
    };
    $list['approved']='[\'�����\','.$callers_list.'],'.$list['approved'];
    $list['paid']='[\'�����\','.$callers_list.'],'.$list['paid'];
    $list['returned_refunded']='[\'�����\','.$callers_list.'],'.$list['returned_refunded'];
    temp_var('chart_callers_approved_values',$list['approved']);
    temp_var('chart_callers_paid_values',$list['paid']);
    temp_var('chart_callers_returned_refunded_values',$list['returned_refunded']);
    // ����� ��������������� ����� ������ �������
    */
    /*
    foreach ($arr as $v) {
        if (!empty($list)) {$list.=',';};
        $confirmed[$v['login']]=empty($v['confirmed'])?0:ceil($v['confirmed']/($v['confirmed']+$v['canceled'])*100);
        $paid[$v['login']]=empty($v['confirmed'])?0:ceil($v['paid']/($v['confirmed']-$v['road'])*100);
        $returned_refunded[$v['login']]=empty($v['returned_refunded'])?0:ceil($v['returned_refunded']/($v['confirmed']-$v['road'])*100);
        $all[$v['login']]=empty($v['confirmed']+$v['canceled'])?0:($v['confirmed']+$v['canceled']);
    };
    */
    /*
    $iphones=readdata(query('select
        orders.login,count(distinct orders.id) as q
        from '.typetab('packages').'
        join '.typetab('orders').' on orders.id=packages.order_id
        where item_id in (select item_id from '.typetab('items_groups').' where group_id=2) and orders.status not in (\'canceled\',\'unavailable\',\'new\')
        and orders.login is not null
        group by orders.login
        order by login;'),'login');
        */
    // pre($iphones);
    $call_cost = value_from_config('shop', 'call_cost');
    $pack_cost = value_from_config('shop', 'pack_cost');
    $track_cost = value_from_config('shop', 'track_cost');
    $zip_cost = value_from_config('shop', 'zip_cost');
    $call_vip_cost = value_from_config('shop', 'call_vip_cost');
    $logins_info = cache('preparing_logins_info', [], 0);
    // pre($logins_info);
    $list = '';
    foreach ($logins_info as $info) {
        // $info=preparing_login_info(array('login'=>$v['login']));
        // pre($info);
        $list .= '<tr>
		<td>'.$info['login'].'</td>
		<td><a href="/?page=admin/orders&amp;paid_caller='.$info['login'].'">'.$info['not_paid_calls'].'</a> ('.($info['not_paid_calls'] * $call_cost).' ���.)
		<span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#modal" data-role="edit_login_paid" data-type="calls" data-login="'.$info['login'].'"></span>
		</td>
		<td>'.$info['not_paid_calls_vip'].' ('.($info['not_paid_calls_vip'] * $call_vip_cost).' ���.)
		<span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#modal" data-role="edit_login_paid" data-type="calls_vip" data-login="'.$info['login'].'"></span>
		</td>
		<td>'.$info['not_paid_zips'].' ('.($info['not_paid_zips'] * $zip_cost).' ���.)
		<span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#modal" data-role="edit_login_paid" data-type="zips" data-login="'.$info['login'].'"></span>
		</td>
		<td>'.$info['not_paid_packs'].' ('.($info['not_paid_packs'] * $pack_cost).' ���.)
		<span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#modal" data-role="edit_login_paid" data-type="packs" data-login="'.$info['login'].'"></span>
		</td>
		<td>'.$info['not_paid_tracks'].' ('.($info['not_paid_tracks'] * $track_cost).' ���.)
		<span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#modal" data-role="edit_login_paid" data-type="tracks" data-login="'.$info['login'].'"></span>
		</td>
		<!--
		<td>
		'.(isset($confirmed[$info['login']]) ? $confirmed[$info['login']].'%' : '��� ������').'
		'.(isset($all[$info['login']]) ? ('<p style=""><small>'.$all[$info['login']].' �������</small></p>') : '').'
		</td>
		<td>'.(isset($paid[$info['login']]) ? $paid[$info['login']].'%' : '��� ������').'</td>
		<td>'.(isset($returned_refunded[$info['login']]) ? $returned_refunded[$info['login']].'%' : '��� ������').'</td>
		-->
		<td>'.$info['advertiser_balance'].' (��������: '.$info['advertiser_balance_real'].')</td>
		</tr>';
    }
    temp_var('list_table', $list);
    $content = template('admin/stat/users/block');
    clear_temp_vars();

    return $content;
}

function submit_add_item_to_order()
{
    if (! isset($_POST['item_id'],$_POST['order_id'])) {
        error('�� �������� ����������� POST ����������');
    }
    if ($_POST['item_id'] == 'null') {
        return '�� �������� �����';
    }
    plus_minus_items_in_package(['order_id' => $_POST['order_id'], 'item_id' => $_POST['item_id'], 'action' => 'plus']);

    return '���������';
}

function submit_add_discount_to_order()
{
    $args['order_id'] = isset($_POST['order_id']) ? $_POST['order_id'] : error('�� ������� ID ������');
    $args['item_id'] = value_from_config('shop', 'discount_item_id');
    if (isset($_POST['cost_rur'])) {
        $args['cost_rur'] = -($_POST['cost_rur']);
    }
    if (isset($_POST['cost_kzt'])) {
        $args['cost_kzt'] = -($_POST['cost_kzt']);
    }
    // ������� ������ ���� ����, ����� ������ ���������� q=q+1, ��� ����� �������� ����
    $args['action'] = 'minus';
    plus_minus_items_in_package($args);
    // ��������� ������
    $args['action'] = 'plus';
    plus_minus_items_in_package($args);

    return '���������';
}

function change_item_q_in_package()
{
    if (! isset($_GET['item_id'],$_GET['order_id'],$_GET['action'])) {
        error('�� �������� ����������� GET ����������');
    }
    plus_minus_items_in_package(['order_id' => $_GET['order_id'], 'item_id' => $_GET['item_id'], 'action' => $_GET['action']]);

    // ����� ������ ������������, �������
    // log_changed_fields(array('change_log'=>typetab('packages_changes'),'table'=>typetab('packages'),'primary_key'=>$var_norm['id'],
    // 	'old'=>$arr['old'],'login'=>));
    return '��������';
}

function admin_submit_delete_item()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    $var['id'] = getpost('id');
    $var = null_if_empty($var);
    query('delete from '.tabname('shop', 'items').' where id='.$var['id'].';');
    logger()->info('SHOP: �������� ������');

    return '�������';
}

function admin_submit_edit_stock()
{
    if (empty($_POST['id'])) {
        error('��������� id ����������');
    }
    if (empty($_POST['item_id'])) {
        error('��������� item_id');
    }
    if (empty($_POST['purchase_id'])) {
        error('��������� purchase_id');
    }
    if (empty($_POST['q'])) {
        error('���������� ����������');
    }
    if (empty($_POST['cost_kzt'])) {
        error('���������� ���������');
    }
    if (empty($_POST['location'])) {
        error('��������� �����');
    }
    if (! isset($_POST['comment'])) {
        error('������ ���� ������� �����������');
    }
    foreach ($_POST as $k => $v) {
        $var[$k] = check_input(fix_encode($v, 'utf-8'));
    }
    $var = null_if_empty($var);
    query('update '.tabname('shop', 'stock').' set
		item_id='.$var['item_id'].',
		purchase_id='.$var['purchase_id'].',
		q='.$var['q'].',
		cost_kzt='.$var['cost_kzt'].',
		location='.$var['location'].',
		comment='.$var['comment'].'
		where id='.$var['id'].'
		;');
    logger()->info('SHOP: �������������� ����������');

    return '��������';
}

function admin_submit_add_stock()
{
    if (empty($_POST['item_id'])) {
        error('��������� item_id');
    }
    if (empty($_POST['purchase_id'])) {
        error('��������� purchase_id');
    }
    if (empty($_POST['q'])) {
        error('���������� ����������');
    }
    if (empty($_POST['cost_kzt'])) {
        error('���������� ���������');
    }
    if (empty($_POST['location'])) {
        error('��������� �����');
    }
    if (! isset($_POST['comment'])) {
        error('������ ���� ������� �����������');
    }
    foreach ($_POST as $k => $v) {
        $var[$k] = check_input(fix_encode($v, 'utf-8'));
    }
    $var = null_if_empty($var);
    query('insert into '.tabname('shop', 'stock').' set
		item_id='.$var['item_id'].',
		purchase_id='.$var['purchase_id'].',
		q='.$var['q'].',
		cost_kzt='.$var['cost_kzt'].',
		location='.$var['location'].',
		comment='.$var['comment'].'
		;');
    logger()->info('SHOP: ���������� ������');

    return '���������';
}

function admin_submit_add_item()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    if (empty($_POST['cost_rur'])) {
        $_POST['cost_rur'] = 0;
    }
    if (empty($_POST['cost_kzt'])) {
        $_POST['cost_kzt'] = 0;
    }
    foreach ($_POST as $k => $v) {
        $var[$k] = check_input(fix_encode($v, 'utf-8'));
    }
    $var = null_if_empty($var);
    query('insert into '.tabname('shop', 'items').' set
		name='.$var['name'].',
		cost_rur='.$var['cost_rur'].',
		cost_kzt='.$var['cost_kzt'].'
		;');
    logger()->info('SHOP: ���������� ������');

    return '���������';
}

function admin_submit_edit_item()
{
    // pre($_POST);
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    foreach ($_POST as $k => $v) {
        $var_norm[$k] = check_input(fix_encode($v, 'utf-8'));
    }
    $var = null_if_empty($var_norm);
    query('update '.tabname('shop', 'items').' set
		name='.$var['name'].',
		cost_rur='.$var['cost_rur'].',
		cost_kzt='.$var['cost_kzt'].',
		kl_item_id='.$var['kl_item_id'].',
		/* pic='.$var['pic'].', */
		description='.$var['description'].',
		youtube_id='.$var['youtube_id'].'
		where id='.$var['id'].'
		;');
    reset_cache_all_domains('preparing_item_info', ['id' => $var_norm['id']], 'shop');
    reset_cache_all_domains('preparing_set_info', 'all', 'shop');
    logger()->info('SHOP: �������������� ������');

    return '���������';
}

function admin_submit_delete_order()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    $order_id = getpost('id');
    delete_order(['order_id' => $order_id]);

    return '�������';
}

function admin_delete_order()
{
    $order_id = empty($_GET['order_id']) ? error('�� ������� ID ������') : $_GET['order_id'];
    delete_order(['order_id' => $order_id]);
}

function delete_order($args)
{
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : $args['order_id'];
    // �������� ����� ���������
    // copy_order_to_history(array('id'=>$var_norm['id'],'action'=>'delete'));
    $arr = query_assoc('select status from '.typetab('orders').' where id=\''.$order_id.'\';');
    if (! in_array($arr['status'], ['new', 'unavailable', 'canceled'])) {
        error('������ ������� ����� � ����� ��������');
    }
    query('insert into '.tabname('shop', 'orders_deleted').' (select * from '.tabname('shop', 'orders').' where id=\''.$order_id.'\')');
    // ��� �������� ������ �������� �� � ���
    order_cpa(['step' => 'cancel', 'order_id' => $order_id]);
    query('delete from '.tabname('shop', 'orders').' where id=\''.$order_id.'\';');
    logger()->info('SHOP: �������� ������ '.$order_id);
}

function admin_submit_delete_purchase()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    $var['id'] = getpost('id');
    $var = null_if_empty($var);
    query('delete from '.tabname('shop', 'purchase').' where id='.$var['id'].';');
    logger()->info('SHOP: �������� �������');

    return '�������';
}

function admin_submit_add_purchase()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    foreach ($_POST as $k => $v) {
        $var[$k] = check_input(fix_encode($v, 'utf-8'));
    }
    if ($var['login'] == 'null') {
        $var['login'] = '';
    }
    $var = null_if_empty($var);
    query('insert into '.tabname('shop', 'purchase').' set
		text='.$var['text'].',
		sum='.$var['sum'].',
		currency='.$var['currency'].',
		devided='.$var['devided'].',
		login='.$var['login'].'
		;');
    logger()->info('SHOP: ���������� �������');

    return '���������';
}

function admin_submit_edit_purchase()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    foreach ($_POST as $k => $v) {
        $var[$k] = check_input(fix_encode($v, 'utf-8'));
    }
    if ($var['login'] == 'null') {
        $var['login'] = '';
    }
    $var = null_if_empty($var);
    // pre($var);
    query('update '.tabname('shop', 'purchase').' set
		date='.$var['date'].',
		text='.$var['text'].',
		sum='.$var['sum'].',
		currency='.$var['currency'].',
		rub='.$var['rub'].',
		devided='.$var['devided'].',
		login='.$var['login'].'
		where id='.$var['id'].'
		;');
    logger()->info('SHOP: �������������� �������');

    return '���������';
}

function admin_submit_edit_order()
{
    if (empty($_POST)) {
        error('�� ������� POST ������');
    }
    foreach ($_POST as $k => $v) {
        $var_norm[$k] = check_input(fix_encode(trim($v), 'utf-8'));
    }
    $var = null_if_empty($var_norm);
    // pre($var);
    $var3['level'] = check_level();
    $arr = query_assoc('select packer,track,login,status,zip,zipper,tracker from '.tabname('shop', 'orders').'
		where id='.$var['id'].' and '.$var3['level']['where'].';');
    if (! $arr) {
        error('����� �� ����������, ���� �� �� ������ ������� � ��� ��������������');
    }
    // ����� ������������� ���������
    $var_norm2['update_login'] = auth_login(['type' => 'shop']);
    // ���������� �������� ������� �� ����
    $var_norm2['tracker'] = $arr['tracker'];
    // ���������� �������� ���������� �� ����
    $var_norm2['packer'] = $arr['packer'];
    // ���������� �������� ������� �� ����
    $var_norm2['zipper'] = $arr['zipper'];
    // ���������� �������� ����������� �� ����
    $var_norm2['login'] = $arr['login'];
    // ���� �� ������� status, ������ �� ���������, �� ������ �������� ������, ������� � ����.
    if (empty($var_norm['status'])) {
        $var_norm2['status'] = $arr['status'];
    } else {
        $var_norm2['status'] = $var_norm['status'];
    }
    // ���� ������� zip � � ���� ��� �� ����, �� ���������� zipper'� ���� �� ��� �� �������
    if (empty($arr['zip']) and ! empty($var_norm['zip'])) {
        if (empty($arr['zipper'])) {
            $var_norm2['zipper'] = auth_login(['type' => 'shop']);
        }
    }
    // ���� ���� ��� null � ������� �� null, �� ��������� ���� �� ������������ � ���������� �������
    if (empty($arr['track']) and ! empty($var_norm['track'])) {
        // ���� ������ �� ������ � ���� � �� �������, �� ������ ����� ���-�� �� ���. ������ ������.
        if (empty($arr['zip']) and empty($var_norm['zip'])) {
            return '������ ��� �� ��������, ������ ��������� track';
        }
        // ���� ������ �� ������� � ����, �� ���������� ������� �����
        if (empty($arr['tracker'])) {
            $var_norm2['tracker'] = auth_login(['type' => 'shop']);
        }
        // ����������� ������ ������ 'sent' ���� ����� ��� �����������
        if (in_array($arr['status'], ['waiting send', 'packed', 'check address'])) {
            $var_norm2['status'] = 'sent';
        }
        // ��������� ������� �� ������������ � ����������
        // shop_add_track_to_gdeposylka($var_norm['track']);
        // ��������� ������� �������, ����� �������� ����� ������ � ������ ������������
        // shop_cron_gdeposylka();
        // moyaposylka_add_track(array('track'=>$var_norm['track']));
        // shop_cron_moyaposylka();
    }
    /*
    if (in_array($arr['status'],array('new','unavailable')) and in_array($var_norm2['status'],array('check address','waiting send'))) {
        logger()->info('���������');
        // postback_kadam_cpa();
    };
    */
    // ���� ������ ���������� 'check address' ��� 'waiting send', � ��� �� 'check_address' �� ������ ������� ��������, ��������� ����� � ����� ���� �� ��� �� ������
    if (($var_norm2['status'] == 'check address' or $var_norm2['status'] == 'waiting send') and
        ($arr['status'] != 'check address' and $arr['status'] != 'waiting send')
        and empty($arr['login'])) {
        $var_norm2['login'] = auth_login(['type' => 'shop']);
        // order_cpa(array('step'=>'confirm','order_id'=>$var_norm['id']));
    }
    // ���� ������ ���������� 'packed' ��������� ������ �������
    if ($var_norm2['status'] == 'packed') {
        // ���� ��������� �� ������ - ��������� ����������
        if (empty($arr['packer'])) {
            $var_norm2['packer'] = auth_login(['type' => 'shop']);
        }
        // ���� �� ������ ������ - ������ �����������
        if (empty($arr['zip'])) {
            return '������ ��� �� ��������, ������ �����������';
        }
    }
    $var2 = null_if_empty($var_norm2);
    // pre(var_dump($var_norm2));
    // pre($var2);
    // $login=null_if_empty($login);
    $arr['old'] = query_assoc('select * from '.tabname('shop', 'orders').' where id='.$var['id'].';');
    query('update '.typetab('orders').' set
		name='.$var['name'].',
		phone='.$var['phone'].',
		country='.$var['country'].',
		zip='.$var['zip'].',
		status='.$var2['status'].',
		address='.$var['address'].',
		track='.$var['track'].',
		info='.$var['info'].',
		'.(isset($var['user_info']) ? ('user_info='.$var['user_info'].',') : '').'
		login='.$var2['login'].',
		packer='.$var2['packer'].',
		zipper='.$var2['zipper'].',
		tracker='.$var2['tracker'].',
		city_id='.$var['city_id'].'
		where id='.$var['id'].'
		;');
    // order_set_status(array('order_id'=>$var['id'],'status'=>$var_norm2['status']));
    // ���� ���������� ������ 'paid', ������ ����� �������
    if (in_array($var_norm2['status'], ['paid']) and ! in_array($arr['status'], ['paid'])) {
        order_cpa(['step' => 'paid', 'order_id' => $var_norm['id']]);
    }
    // ���� ������ ���������� 'check address' ��� 'waiting send', � ��� �� 'check_address' �� ������ ������� ��������,
    // ��������� CPA
    if (in_array($var_norm2['status'], ['check address', 'waiting send']) and ! in_array($arr['status'], ['check address', 'waiting send'])) {
        order_cpa(['step' => 'confirm', 'order_id' => $var_norm['id']]);
    }
    // ���� ������ ��������, �� ����������� ��������������� ��� order_cpa
    if (in_array($var_norm2['status'], ['canceled']) and ! in_array($arr['status'], ['canceled'])) {
        if (in_array($arr['status'], ['new', 'unavailable'])) {
            // ���� ������ �� ���� ������������, �� ��� ��� cancel
            order_cpa(['step' => 'cancel', 'order_id' => $var_norm['id']]);
        } else {
            // ���� ������ ���� ������������, �� ��� ��� cancel_after_confirm
            order_cpa(['step' => 'cancel_after_confirm', 'order_id' => $var_norm['id']]);
        }
    }
    log_changed_fields(['change_log' => typetab('orders_changes'), 'table' => typetab('orders'), 'primary_key' => $var_norm['id'],
        'old' => $arr['old'], 'login' => $var_norm2['update_login']]);
    // shop_auto_statuses(); //��������� �������, ������ ��� ��������� ������ ��� ����� ������ �� ������
    logger()->info('SHOP: �������������� ������');

    // ���������� 1 �.�. � JS ����������� ������ success_text=1
    return 1;
}

function admin_shave_order_page()
{
    $order_id = empty($_GET['order_id']) ? error('�� ������� ID ������') : check_input($_GET['order_id']);
    $return = admin_shave_order(['order_id' => $order_id]);

    return ($return) ? 1 : 0;
}

function admin_upsale_order_page()
{
    $order_id = empty($_GET['order_id']) ? error('�� ������� ID ������') : check_input($_GET['order_id']);
    $return = admin_upsale_order(['order_id' => $order_id]);

    return ($return) ? 1 : 0;
}

// ���� ����� � �������� - �������, ���� ��� - �� ������
function admin_upsale_order($args = [])
{
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : check_input($args['order_id']);
    $order_info = preparing_order_info(['order_id' => $order_id]);
    $arr['old'] = query_assoc('select * from '.typetab('orders').' where id=\''.$order_id.'\';');
    $upsale = $order_info['upsale'] ? 0 : 1;
    query('update '.typetab('orders').' set upsale='.$upsale.' where id=\''.$order_id.'\';');
    log_changed_fields(['change_log' => typetab('orders_changes'), 'table' => typetab('orders'), 'primary_key' => $order_id, 'old' => $arr['old']]);

    // logger()->info(($order_info['shave']?'����������':'���������').' ����� #'.$order_id);
    return true;
}

// ���� ����� ��������� - ������� ����, ���� ��� - �� ������
function admin_shave_order($args = [])
{
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : check_input($args['order_id']);
    $order_info = preparing_order_info(['order_id' => $order_id]);
    $arr['old'] = query_assoc('select * from '.typetab('orders').' where id=\''.$order_id.'\';');
    if ($order_info['shave']) {
        query('update '.typetab('orders').' set shave=0,user_info=info,user_status=status where id=\''.$order_id.'\';');
    } else {
        query('update '.typetab('orders').' set shave=1,user_info=null,user_status=\'canceled\' where id=\''.$order_id.'\';');
    }
    log_changed_fields(['change_log' => typetab('orders_changes'), 'table' => typetab('orders'), 'primary_key' => $order_id, 'old' => $arr['old']]);

    // logger()->info(($order_info['shave']?'����������':'���������').' ����� #'.$order_id);
    return true;
}

// �� ���������� �������������� �������. ������ ����� ���������� ��������������� ������ � �������: "������� ����������","�������","������ ��������"
function admin_edit_order()
{
    if (empty($_GET['order_id'])) {
        error('�� ������� order_id');
    }
    $order_id = check_input($_GET['order_id']);
    // �������� ������ �������� � ��������� � ��������� ������ ��������� ��� ��������� �������
    $all_statuses = enum_to_array(tabname('shop', 'orders'), 'status', shop_translation('order_status'));
    $cities = query_arr('select id,name from '.typetab('cities').' order by name;');
    // ��������� ������ ��������
    asort($all_statuses);
    // $disabled_statuses=array('take payment','delivered not paid','sent','new','null');
    // $disabled_statuses=array('take payment','sent','new','null');
    // ,'refund','refund paid'
    $disabled_statuses = ['sent', 'new', 'null'];
    $var['level'] = check_level();
    $arr = query_assoc('select date,sent_date,name,phone,country,zip,status,address,track,utm_source,info,user_info,cost_rur,cost_kzt,
		region,locality_type,locality,login,packer,zipper,tracker,city_id,shave,upsale
		from '.tabname('shop', 'orders').' where id=\''.$order_id.'\' and '.$var['level']['where'].';');
    if (! $arr) {
        error('����� �� ����������, ���� �� �� ������ ������� � ��� ��������������');
    }
    $allowed_statuses = get_allowed_statuses(['order_id' => $order_id]);
    // ��������� ���� ������ �� ����������� �������
    $list = '';
    foreach ($allowed_statuses as $v) {
        if ($v['name'] === $arr['status']) {
            continue;
        }
        temp_var('name', $v['name']);
        temp_var('description', $v['description']);
        // if (!empty($list)) {$list.=', ';};
        $list .= template('admin/orders/edit_order/item_status_link');
    }
    clear_temp_vars();
    temp_var('list_statuses_links', $list);
    //
    temp_var('list_statuses', get_select_option_list([
        'array' => $allowed_statuses,
        'value' => 'name',
        'text' => 'description',
        'selected_value' => $arr['status'],
        'show_null' => false,
    ]));
    temp_var('order_id', $order_id);
    temp_var('name', htmlspecialchars($arr['name'], ENT_QUOTES, 'cp1251'));
    temp_var('phone', htmlspecialchars($arr['phone'], ENT_QUOTES, 'cp1251'));
    temp_var('zip', htmlspecialchars($arr['zip'], ENT_QUOTES, 'cp1251'));
    temp_var('track', htmlspecialchars($arr['track'], ENT_QUOTES, 'cp1251'));
    temp_var('info', htmlspecialchars($arr['info'], ENT_QUOTES, 'cp1251'));
    temp_var('user_info', htmlspecialchars($arr['user_info'], ENT_QUOTES, 'cp1251'));
    temp_var('address', htmlspecialchars($arr['address'], ENT_QUOTES, 'cp1251'));
    temp_var('date', $arr['date']);
    temp_var('sent_date', empty($arr['sent_date']) ? '���' : $arr['sent_date']);
    temp_var('utm_source', $arr['utm_source']);
    temp_var('caller', empty($arr['login']) ? '���' : $arr['login']);
    temp_var('tracker', empty($arr['tracker']) ? '���' : $arr['tracker']);
    temp_var('zipper', empty($arr['zipper']) ? '���' : $arr['zipper']);
    temp_var('packer', empty($arr['packer']) ? '���' : $arr['packer']);
    temp_var('list_countries', get_select_option_list([
        'array' => enum_to_array(tabname('shop', 'orders'), 'country'),
        'value' => 'value', 'text' => 'value', 'selected_value' => $arr['country'], 'null_disabled' => 'no', 'null_value' => '']));
    temp_var('list_cities', get_select_option_list([
        'array' => $cities,
        'value' => 'id', 'text' => 'name', 'selected_value' => $arr['city_id'], 'null_value' => '']));
    $block_order_info =
    temp_var('block_order_info', $arr['shave'] ? template('admin/orders/edit_order/block_info_shave') : template('admin/orders/edit_order/block_info'));
    $is_shave = ($arr['shave']) ? 1 : 0;
    $shave_class_active = ($arr['shave']) ? 'active' : '';
    temp_var('is_shave', $is_shave);
    temp_var('shave_class_active', $shave_class_active);
    temp_var('shave_button', template('admin/orders/edit_order/shave_button'));
    $is_upsale = ($arr['upsale']) ? 1 : 0;
    $upsale_class_active = ($arr['upsale']) ? 'active' : '';
    temp_var('is_upsale', $is_upsale);
    temp_var('upsale_class_active', $upsale_class_active);
    temp_var('upsale_button', template('admin/orders/edit_order/upsale_button'));
    /*
    temp_var('list_statuses',get_select_option_list(array(
            'array'=>$all_statuses,'value'=>'key',
            'text'=>'value',
            'selected_value'=>$arr['status'],
            'disabled_values'=>$disabled_statuses,
            'null_disabled'=>'yes'
            )));
            */
    // temp_var('list_statuses',get_allowed_statuses_option_list(array('current_status'=>$arr['status'])));
    // temp_var('',$arr['']);
    $content = template('admin/orders/edit_order/block_edit_order');

    return $content;
}

// pre(var_dump(is_status_allowed(array('order_id'=>50,'status'=>'packed'))));

// ��������� ��������� �� ���������� ������ ��� ������
// ���������� ID ������ � ������
function is_status_allowed($args = [])
{
    // $current_status=empty($args['current_status'])?error('�� ������� ������� ������'):$args['current_status'];
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : check_input($args['order_id']);
    $new_status = empty($args['status']) ? error('�� ������� ������') : check_input($args['status']);
    $arr = query_assoc('select status from '.typetab('orders').' where id=\''.$order_id.'\';');
    if (! $arr) {
        error('����� �� ������');
    }
    $current_status = $arr['status'];
    $arr = get_allowed_statuses(['current_status' => $current_status]);
    $is_allowed = false;
    foreach ($arr as $v) {
        if ($new_status === $v['name']) {
            $is_allowed = true;
        }
    }

    return $is_allowed;
}

function get_statuses_sql($type = false)
{
    return get_statuses(['format' => 'sql', 'type' => $type]);
}

// ������� ������ �������� ������������ ����, ���� ������� �� � ������ ��� mysql �������
function get_statuses($args = [])
{
    $args['type'] = empty($args['type']) ? error('�� ������ ��� ��������') : $args['type'];
    $args['format'] = empty($args['format']) ? 'array' : $args['format'];
    $arr = query_arr('select name from '.typetab('statuses').' where '.$args['type'].' and active;');
    if ($args['format'] === 'sql') {
        foreach ($arr as $v) {
            if (! isset($list)) {
                $list = '';
            } else {
                $list .= ',';
            }
            $list .= '\''.$v['name'].'\'';
        }

        return $list;
    }
    if ($args['format'] === 'array') {
        return $arr;
    }
}

// pre(get_statuses_sql('approved'));

function get_allowed_statuses($args = [])
{
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : check_input($args['order_id']);
    $arr = query_assoc('select status from '.typetab('orders').' where id=\''.$order_id.'\';');
    if (! $arr) {
        error('����� �� ������');
    }
    $current_status = $arr['status'];
    // $current_status=empty($args['current_status'])?error('�� ������� ������� ������'):$args['current_status'];
    $arr_allowed_statuses = query_arr('
		select statuses.name,statuses.description
		from '.typetab('statuses').'
		where (name in (select status_to from '.typetab('statuses_changes').' where status_from=\''.$current_status.'\' and active) or name=\''.$current_status.'\') and active
		order by statuses.position
		;');

    return $arr_allowed_statuses;
}

function admin_edit_item()
{
    if (empty($_GET['item_id'])) {
        error('�� ������� item_id');
    }
    $item_id = getvar('item_id');
    // $arr=query_assoc('select name,cost_rur,cost_kzt,cost_confirm,kl_item_id from '.tabname('shop','items').' where id=\''.$item_id.'\';');
    $arr = preparing_item_info(['id' => $item_id]);
    temp_var('name', $arr['name']);
    temp_var('cost_rur', $arr['cost_rur']);
    temp_var('cost_kzt', $arr['cost_kzt']);
    temp_var('cost_confirm', $arr['cost_confirm']);
    temp_var('kl_item_id', empty($arr['kl_item_id']) ? '' : $arr['kl_item_id']);
    temp_var('item_id', $arr['id']);
    temp_var('pic', $arr['pic'] ? $arr['pic'] : '');
    temp_var('description', htmlspecialchars($arr['description'], ENT_QUOTES, 'cp1251'));
    temp_var('youtube_id', $arr['youtube_id']);
    // temp_var('',$arr['']);
    $content = template('admin/items/block_edit_item');

    return $content;
}

function admin_add_stock()
{
    if (empty($_GET['item_id'])) {
        error('�� ������� item_id');
    }
    $item_id = getvar('item_id');
    $item_info = preparing_item_info(['id' => $item_id]);
    // pre($item_info);
    $arr = query_arr('select stock.id,stock.purchase_id,stock.q,stock.date,stock.cost_kzt,stock.comment,locations.name
		from '.tabname('shop', 'stock').'
		join '.typetab('locations').' on locations.id=stock.location
		where item_id=\''.$item_id.'\' order by date;');
    $stock_list = '';
    foreach ($arr as $v) {
        $stock_list .= '<td>'.$v['date'].'</td><td>'.$v['name'].'</td><td>'.$v['q'].'</td>
		<td><button class="btn btn-info btn-xs" data-role="edit_purchase" data-id="'.$v['purchase_id'].'">#'.$v['purchase_id'].'</button></td>
		<td>'.$v['cost_kzt'].'</td>
		<td><button class="btn btn-default btn-xs" data-role="edit_stock" data-id="'.$v['id'].'">��������</button></td>
		</tr>';
    }
    $purchase = query_arr('select id,concat(date(date),\': \',left(text,50)) as title from '.tabname('shop', 'purchase').' order by date desc;');
    $locations = query_arr('select id,name from '.typetab('locations').' order by name;');
    $content = '
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">���������� ������ #'.$item_id.' ('.$item_info['name'].')</h4>
	</div>
	<div class="modal-body">
	<form id="addStock">
	<div class="form-group">
	<label>�����</label>
	<select name="location" class="form-control">
	'.get_select_option_list(['array' => $locations, 'value' => 'id', 'text' => 'name', 'null_disabled' => true]).'
	</select>
	</div>
	<div class="form-group">
      <label for="purchase">�������� ������</label>
      <select id="purchase" name="purchase_id" class="form-control">
        '.get_select_option_list(['array' => $purchase, 'value' => 'id', 'text' => 'title', 'null_disabled' => true]).'
      </select>
    </div>
    <div class="form-group">
	<label for="q">����������</label>
	<input type="text" id="q" name="q" placeholder="����������, ����" class="form-control">
	</div>
	<div class="form-group">
	<label for="cost_kzt">��������� 1 ��.</label>
	<input type="text" id="cost_kzt" name="cost_kzt" placeholder="�����, �����" class="form-control">
	</div>
	<div class="form-group">
	<label>�����������</label>
	<textarea name="comment" placeholder="�����������" class="form-control"></textarea>
	</div>
	<input type="hidden" name="item_id" value="'.$item_id.'">
	</form>
	<h2>���������� ��������</h2>
	<div style="height: 250px; overflow: scroll;">
	<table class="table table-striped">
	<thead><tr>
	<th class="date">����</th><th>�����</th><th>����������</th><th>������</th><th>1 ��.</th><th></th>
	</tr></thead>
	'.$stock_list.'
	</table>
	</div>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
	<button type="button" class="btn btn-primary" data-role="ajax_submit_add_stock" id="result" data-loading-text="����������..." autocomplete="off">��������</button>
	</div>
	';

    return $content;
}

function admin_edit_stock()
{
    $stock_id = empty($_GET['stock_id']) ? error('�� ������� id ����������') : $_GET['stock_id'];
    $arr = query_assoc('select id,date,purchase_id,item_id,q,cost_kzt,comment,location from '.tabname('shop', 'stock').' where id=\''.$stock_id.'\';');
    $item_info = preparing_item_info(['id' => $arr['item_id']]);
    $purchase = query_arr('select id,concat(date(date),\': \',left(text,50)) as title from '.tabname('shop', 'purchase').' order by date desc;');
    $locations = query_arr('select id,name from '.typetab('locations').' order by name;');
    $content = '
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">�������������� ���������� #'.$stock_id.' ('.$arr['date'].')</h4>
	</div>
	<div class="modal-body">
	<form id="editStock">
	<p><b>�����:</b> '.$item_info['name'].'</p>
	<div class="form-group">
	<label>�����</label>
	<select name="location" class="form-control">
	'.get_select_option_list(['array' => $locations, 'value' => 'id', 'text' => 'name', 'selected_value' => $arr['location'], 'null_disabled' => true]).'
	</select>
	</div>
	<div class="form-group">
      <label for="purchase">�������� ������</label>
      <select id="purchase" name="purchase_id" class="form-control">
        '.get_select_option_list(['array' => $purchase, 'value' => 'id', 'text' => 'title', 'selected_value' => $arr['purchase_id'], 'null_disabled' => true]).'
      </select>
    </div>
    <div class="form-group">
	<label for="q">����������</label>
	<input type="text" id="q" name="q" value="'.$arr['q'].'" placeholder="����������, ����" class="form-control">
	</div>
	<div class="form-group">
	<label for="cost_kzt">��������� 1 ��.</label>
	<input type="text" id="cost_kzt" name="cost_kzt" value="'.$arr['cost_kzt'].'" placeholder="�����, �����" class="form-control">
	</div>
	<div class="form-group">
	<label>�����������</label>
	<textarea name="comment" placeholder="�����������" class="form-control">'.$arr['comment'].'</textarea>
	</div>
	<input type="hidden" name="id" value="'.$arr['id'].'">
	<input type="hidden" name="item_id" value="'.$arr['item_id'].'">
	</form>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
	<button type="button" class="btn btn-primary" data-role="ajax_submit_edit_stock" id="result" data-loading-text="����������..." autocomplete="off">���������</button>
	</div>
	';

    return $content;
}

function advertisers_select_option_list($args = [])
{
    // pre($args);
    $selected_value = isset($args['selected_value']) ? $args['selected_value'] : '';
    // $arr_advertisers=query_arr('select login from '.typetab('users').' where level=\'advertiser\' order by login;');
    $arr_users = query_arr('select login from '.typetab('users').'
		where level in (\'advertiser\',\'caller\',\'packer\',\'packer_lite\',\'manager\',\'admin\')
		order by login;');
    $return = '<div class="form-group">
	<label for="advertiser">������������</label>
	<select id="advertiser" name="login" class="form-control">
	'.get_select_option_list([
        'array' => $arr_users,
        'value' => 'login', 'text' => 'login', 'selected_value' => $selected_value]).'
	</select>
	</div>
	';

    return $return;
}

function admin_edit_purchase()
{
    if (empty($_GET['purchase_id'])) {
        error('�� ������� purchase_id');
    }
    $purchase_id = getvar('purchase_id');
    // $arr_advertisers=query_arr('select login from '.typetab('users').' where type=\'advertiser\' order by login;');
    $arr = query_assoc('select text,sum,date,currency,rub,devided,login from '.tabname('shop', 'purchase').' where id=\''.$purchase_id.'\';');
    $selected['currency']['rur'] = '';
    $selected['currency']['kzt'] = '';
    $selected['currency'][$arr['currency']] = 'selected';
    $selected['devided']['1'] = '';
    $selected['devided']['0'] = '';
    $selected['devided'][$arr['devided']] = 'selected';
    $content = '
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">�������������� ������� #'.$purchase_id.'</h4>
	</div>
	<div class="modal-body">
	<form id="editPurchase">
	<div class="form-group">
	<label for="text">�����</label>
	<textarea id="text" name="text" placeholder="����������" class="form-control" style="height: 100px;">'.$arr['text'].'</textarea>
	</div>
	<div class="form-group">
	<label for="sum">�����</label>
	<input type="text" id="sum" name="sum" value="'.$arr['sum'].'" placeholder="�����" class="form-control">
	</div>
	<div class="form-group">
	<label for="currency">������</label>
	<select id="currency" name="currency" class="form-control">
	<option value="rur" '.$selected['currency']['rur'].'>RUB</option>
	<option value="kzt" '.$selected['currency']['kzt'].'>KZT</option>
	</select>
	</div>
	<div class="form-group">
	<label>����� � ������</label>
	<input type="text" name="rub" value="'.$arr['rub'].'" placeholder="�����, ���." class="form-control" autocomplete="off">
	</div>
	'.advertisers_select_option_list(['selected_value' => $arr['login']]).'
	<div class="form-group">
	<label for="devide">��������</label>
	<select id="devide" name="devided" class="form-control">
	<option value="1" '.$selected['devided']['1'].'>�������</option>
	<option value="0" '.$selected['devided']['0'].'>������</option>
	</select>
	</div>
	<div class="form-group">
	<label>����</label>
	<input type="text" name="date" value="'.$arr['date'].'" placeholder="����" class="form-control">
	</div>
	<input type="hidden" name="id" value="'.$purchase_id.'">
	</form>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-danger delete-btn" data-role="ajax_submit_delete_purchase" id="delete" data-loading-text="��������..." autocomplete="off">�������</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
	<button type="button" class="btn btn-primary" data-role="ajax_submit_edit_purchase" id="result" data-loading-text="����������..." autocomplete="off">���������</button>
	</div>
	';

    return $content;
}

function shop_admin_menu_active($section)
{
    global $config;
    $active = 0;
    // pre($config);
    $section_pages['orders'] = ['index', 'admin/index', 'admin/orders'];
    $section_pages['items'] = ['admin/items/items', 'admin/landings/index'];
    // � ������ � �����
    $section_pages['landings'] = ['admin/landings/index'];
    $section_pages['purchase'] = ['admin/purchase', 'admin/payments'];
    $section_pages['stat'] = ['admin/stat/orders_by_days/index', 'admin/stat/advertiser_source',
        'admin/stat/orders_wide_stat', 'admin/stat/potential_stat',
        'admin/stat/orders_in_day/index', 'admin/stat/stat', 'admin/stat/users',
    ];
    $section_pages['wide_stat'] = ['admin/orders_wide_stat'];
    $section_pages['tracks'] = ['admin/kazpost/tracks', 'admin/kazpost/update_tracks', 'admin/kazpost/all_track_statuses', 'admin/kazpost/incomes', 'admin/kazpost/sent_orders_by_months'];
    $section_pages['returns'] = ['admin/kazpost/return_dates'];
    $section_pages['cpa'] = ['admin/cpa_confirmed'];
    $section_pages['returns_requests'] = ['admin/returns/requests', 'admin/returns/request'];
    $section_pages['advertiser_stat'] = ['admin/advertiser/stat'];
    $section_pages['advertiser_links'] = ['index', 'admin/advertiser/links'];
    $section_pages['advertiser_payments'] = ['admin/advertiser/payments'];
    $section_pages['advertiser_domains'] = ['admin/advertiser/domains'];
    $section_pages['advertiser_paid_charts'] = ['admin/advertiser/paid_charts/index'];
    if (isset($section_pages[$section]) == 0) {
        error('������ �� ������������', '������ '.$section.' �� ������������');
    }
    foreach ($section_pages[$section] as $page) {
        if ($page == $config['vars']['page']) {
            $active = 1;
        }
    }
    if ($active == 0) {
        return '';
    }
    if ($active == 1) {
        return 'active';
    }
}

function admin_purchase()
{
    $per_page = 50;
    $arr = query_assoc('select count(*) as q from '.typetab('purchase').';');
    $rows = $arr['q'];
    $pages = ceil($rows / $per_page);
    $page = empty($_GET['p']) ? $pages : $_GET['p'];
    $from = ($page - 1) * $per_page;
    $arr = query_arr('select id,date(date) as day,text,sum,currency,rub,devided,login
		from '.tabname('shop', 'purchase').'
		order by date
		limit '.$from.','.$per_page.'
		;');
    $list = '';
    foreach ($arr as $v) {
        $stock_arr = query_arr('select items.name,stock.q,stock.cost_kzt,stock.cost_rur
			from '.tabname('shop', 'stock').'
			join '.tabname('shop', 'items').' on items.id=stock.item_id
			where purchase_id=\''.$v['id'].'\';');
        $stock_list = '';
        $sum_cost = 0;
        foreach ($stock_arr as $stock) {
            $stock_cost = $stock['q'] * $stock['cost_kzt'];
            $stock_list .= '<li>'.$stock['name'].' &mdash; '.$stock['q'].', '.$stock['cost_kzt'].' ���/�� ('.$stock_cost.' ���)</li>';
            $sum_cost = $sum_cost + $stock_cost;
        }
        $stock_text = empty($stock_list) ? '' : '<h4>�������:</h4><ul class="list-unstyled">'.$stock_list.'</ul> <h5>������� �� �����: '.$sum_cost.' ���</h5>';
        if ($v['devided'] == '1') {
            $class = ' class="info"';
        } else {
            $class = '';
        }
        $list .= '<tr'.$class.'>
		<td>'.$v['id'].'</td>
		<td>'.$v['day'].'</td>
		<td>
		'.nl2br($v['text']).'
		'.$stock_text.'
		</td>
		<td>'.$v['sum'].' '.(empty($v['rub']) ? '' : '<br>'.$v['rub'].'&nbsp;('.round($v['sum'] / $v['rub'], 2).')').'</td>
		<td>'.$v['currency'].' '.(empty($v['rub']) ? '' : '<br>rub').'</td>
		<td>'.$v['login'].'</td>
		<td><button data-toggle="modal" data-target="#modal" data-role="edit_purchase" data-id="'.$v['id'].'" class="btn btn-default">��������</button></td>
		</tr>';
    }
    $content = '
	<!-- <div class="table-responsive"> -->
	<table class="table table-striped purchase">
	<thead><tr>
	<th>ID</th><th class="date">����</th><th>��������</th><th>�����</th><th>������</th><th>�����</th><th></th>
	</tr></thead>
	'.$list.'
	</table>
	<!-- </div> -->
	<nav>
	<ul class="pagination pagination-lg">
	'.page_numbers($rows, $per_page, $page, '/?page=admin/purchase&amp;p=', 5, '', '&laquo;', '&raquo;', 'yes', 'active', 'disabled', 1, '', 'yes', 'yes').'
	</ul>
	</nav>';

    return $content;
}

function common_stat_table()
{
    // $items=query_arr('select id from '.tabname('shop','items').';');
    $items_cost['rur'] = 0;
    $items_cost['kzt'] = 0;
    $arr['items_info'] = preparing_items_info();
    foreach ($arr['items_info'] as $v) {
        // $arr=preparing_item_info(array('id'=>$v['id']));
        if ($v['q_in_stock'] > 0) {
            $items_cost['kzt'] = $items_cost['kzt'] + ($v['q_in_stock'] - $v['q_waiting_send']) * $v['real_cost_kzt'];
            $items_cost['rur'] = $items_cost['rur'] + ($v['q_in_stock'] - $v['q_waiting_send']) * $v['real_cost_rur'];
        }
    }
    $paid = query_assoc('select
		sum(if(orders.country in (\'������\'),packages.cost_rur*q,0)) as rur,
		sum(if(orders.country in (\'���������\'),packages.cost_kzt*q,0)) as kzt
		from '.tabname('shop', 'packages').'
		join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where status in (\'paid\',\'refund\',\'refund paid\');');
    // $paid_manager=get_paid_manager();
    /*
    $paid_admin=query_assoc('select
        sum(if(orders.country in (\'������\'),packages.cost_rur*q,0)) as rur,
        sum(if(orders.country in (\'���������\'),packages.cost_kzt*q,0)) as kzt
        from '.tabname('shop','packages').'
        join '.tabname('shop','orders').' on orders.id=packages.order_id
        where status in (\'paid\',\'refund\',\'refund paid\')
        and ((select login from '.tabname('shop','orders_changes').' where primary_key=orders.id and field=\'status\' and value=\'paid\' order by orders_changes.date desc limit 1)
            in (select login from '.tabname('shop','users').' where level=\'admin\'))
        ;');
        */
    $waiting = query_assoc('select
		sum(if(orders.country in (\'������\'),packages.cost_rur*q,0)) as rur,
		sum(if(orders.country in (\'���������\'),packages.cost_kzt*q,0)) as kzt
		from '.tabname('shop', 'packages').'
		join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where status=\'take payment\';');
    $potential = query_assoc('select
		sum(if(orders.country in (\'������\'),packages.cost_rur*q,0)) as rur,
		sum(if(orders.country in (\'���������\'),packages.cost_kzt*q,0)) as kzt
		from '.tabname('shop', 'packages').'
		join '.tabname('shop', 'orders').' on orders.id=packages.order_id
		where orders.status in (\'packed\',\'check address\',\'waiting send\',\'delivered not paid\',\'sent\')
		;');
    $waiting_text['rur'] = empty($waiting['rur']) ? '' : '(+'.$waiting['rur'].' ������� � �����)';
    $waiting_text['kzt'] = empty($waiting['kzt']) ? '' : '(+'.$waiting['kzt'].' ������� � �����)';
    $expenses = query_assoc('
		select (select ifnull(sum(sum),0) from '.tabname('shop', 'purchase').' where currency=\'rur\') as rur,
				(select ifnull(sum(sum),0) from '.tabname('shop', 'purchase').' where currency=\'kzt\') as kzt
		;');
    $devided = query_assoc('
		select (select ifnull(sum(sum),0) from '.tabname('shop', 'purchase').' where currency=\'rur\' and devided=\'1\') as rur,
				(select ifnull(sum(sum),0) from '.tabname('shop', 'purchase').' where currency=\'kzt\' and devided=\'1\') as kzt
		;');
    /*
    $devided_admin=query_assoc('
        select (select ifnull(sum(sum),0) from '.tabname('shop','purchase').' where currency=\'rur\' and devided=\'1\') as rur,
                (select ifnull(sum(sum),0) from '.tabname('shop','purchase').' where currency=\'kzt\' and devided=\'1\') as kzt
        ;');
    $devided_manager=query_assoc('
        select (select ifnull(sum(sum),0) from '.tabname('shop','purchase').' where currency=\'rur\' and devided=\'1\') as rur,
                (select ifnull(sum(sum),0) from '.tabname('shop','purchase').' where currency=\'kzt\' and devided=\'1\') as kzt
        ;');
    */
    /*
    $paid_recieves_by_login=query_arr('
        select
        ');
        */
    $income['rur'] = $paid['rur'] + $waiting['rur'] - $expenses['rur'];
    $income['kzt'] = $paid['kzt'] + $waiting['kzt'] - $expenses['kzt'];
    // $devided2['rur']=value_from_config('shop','divided_rur');
    // $devided2['kzt']=value_from_config('shop','divided_kzt');
    // $devided_all['rur']=$devided['rur']+$devided2['rur'];
    // $devided_all['kzt']=$devided['kzt']+$devided2['kzt'];
    $paid_percent = 100 - value_from_config('shop', 'return_percent');
    $content = '<table class="table table-striped purchase">
	<thead><tr>
	<th>������</th><th>�������</th><th>������</th><!--<th>������������� ������� '.$paid_percent.'%</th>-->
	<th>������� �������</th><th>��������</th><th>��������</th><th>�������</th>
	</tr></thead>
	<tr><td>rur</td>
	<td>'.$expenses['rur'].'</td>
	<td>'.$paid['rur'].' '.$waiting_text['rur'].'</td>
	<!--<td>'.round($potential['rur'] * ($paid_percent / 100) / 1000).'�</td>-->
	<td>'.$items_cost['rur'].'</td>
	<td>'.$devided['rur'].'</td>
	<td>'.($paid['rur'] - $devided['rur']).'</td>
	<td>'.$income['rur'].'</td></tr>
	<tr><td>kzt</td>
	<td>'.$expenses['kzt'].'</td>
	<td>'.$paid['kzt'].' '.$waiting_text['kzt'].'</td>
	<!--<td>'.round($potential['kzt'] * ($paid_percent / 100) / 1000).'�</td>-->
	<td>'.$items_cost['kzt'].'</td>
	<td>'.$devided['kzt'].'</td>
	<td>'.($paid['kzt'] - $devided['kzt']).'</td>
	<td>'.$income['kzt'].'</td></tr>
	</table>';

    // <br/>� ���������: '.$paid_manager['rur'].'
    // <br/>� ���������: <a href="/?page=admin/orders&amp;status=all&amp;paid_manager=1">'.($paid_manager['kzt']-value_from_config('shop','paid_manager_kzt_minus')).'</a>
    return $content;
}

function admin_items_old()
{
    // $arr_items=query_arr('select items.id
    // 	from '.tabname('shop','items').'
    // 	where show_admin=\'1\'
    // 	order by items.name;');
    $arr_items = query_arr('
		(select name,id,\'offer\' as type from '.tabname('shop', 'item_groups').' where type=\'offer\')
		union (
			select items.name,items.id,\'item\' as type from '.tabname('shop', 'items').' where id not in (
				select item_id from '.tabname('shop', 'items_groups').'
				join '.tabname('shop', 'item_groups').' on item_groups.id=items_groups.group_id
				where type=\'offer\') and items.show_admin=\'1\'
	)
	order by name
	;');
    // �������� ��������� ���� ����� � ����� ��� ������� ������� � �������������
    $arr_rate = query_assoc('select rate from '.typetab('rates').' order by date desc limit 1;');
    // �������� ��������� ������
    $work['call'] = value_from_config('shop', 'call_cost');
    $work['pack'] = value_from_config('shop', 'pack_cost');
    $work['track'] = value_from_config('shop', 'track_cost');
    $work['zip'] = value_from_config('shop', 'zip_cost');
    $work_cost = $work['call'] + $work['pack'] + $work['track'] + $work['zip'];
    // pre($arr_items);
    // $arr['items_']=preparing_items_stat();
    $arr_item_info = cache('preparing_items_info', [], 0);
    $arr_landings_by_item_id = readdata(query('select item_id,dir,show_index from '.typetab('landings').' where item_id is not null order by item_id'), 'item_id', false);
    // pre($arr_landings);
    // pre($arr_landings_by_id);
    // pre($arr_landings);
    $list_items = '';
    foreach ($arr_items as $item) {
        $arr_items2 = [];
        if ($item['type'] == 'offer') {
            $arr_items2 = query_arr('select item_id as id from '.typetab('items_groups').'
				join '.typetab('items').' on items.id=items_groups.item_id
				where show_admin=\'1\' and group_id=\''.$item['id'].'\';');

        } else {
            $arr_items2[]['id'] = $item['id'];
        }
        $list_items2 = '';
        foreach ($arr_items2 as $item2) {
            // $item_info=cache('preparing_item_info',array('id'=>$item2['id']),0);
            $item_info = $arr_item_info[$item2['id']];
            // pre($item_info);
            // $arr_landings=query_arr('select dir,show_index from '.tabname('shop','landings').' where item_id=\''.$item2['id'].'\';');
            $arr_landings = isset($arr_landings_by_item_id[$item2['id']]) ? $arr_landings_by_item_id[$item2['id']] : [];
            $list_landings = '';
            foreach ($arr_landings as $landing) {
                $list_landings .= '<li><a href="http://elitbrand.com/'.$landing['dir'].'">'.($landing['show_index'] ? $landing['dir'] : '<s>'.$landing['dir'].'</s>').'</a></li>';
            }
            $paid_percent = round($item_info['q_paid'] / ((($item_info['q_not_paid'] + $item_info['q_paid']) == 0) ? 1 : ($item_info['q_not_paid'] + $item_info['q_paid'])) * 100);
            $confirm_profit = (($item_info['q_paid'] >= 6) ? round(($item_info['cost_kzt'] - $item_info['real_cost_kzt']) * $paid_percent / 100 - $work_cost) : false);
            $item_margin['kzt'] = (empty($item_info['real_cost_kzt']) ? (empty($item_info['cost_kzt']) ? '<span style="color:red;">��� ���</span>' : '<span style="color:red;">��� �����. ����</span>') : round((($item_info['cost_kzt'] / $item_info['real_cost_kzt']) - 1) * 100).'%');
            $list_items2 .= '<tr><td><a href="/?page=admin/orders&amp;status=delivered_not_paid&amp;item_id='.$item2['id'].'" target="_blank">'.$item2['id'].'</a></td><td>'.$item_info['name'].'</td>
			<td><ul class="list-inline">'.$list_landings.'</ul></td>
			<!--<td>'.$item_info['q_waiting_send'].'</td><td>'.$item_info['q_sent'].'</td>-->
			<!--<td>'.$item_info['q_delivered_not_paid'].'</td><td>'.$item_info['q_paid'].'</td>-->
			<!--<td>'.$item_info['q_canceled'].'</td>-->
			<td>'.($item_info['q_in_stock'] - $item_info['q_packed']).'</td>
			<td>'.($item_info['q_in_stock'] - $item_info['q_waiting_send']).'</td>
			<!--<td>'.$item_info['q_available'].'</td>-->
			<!--<td>'.$item_info['cost_rur'].'</td>-->
			<td>'.$item_info['cost_kzt'].' '.$item_margin['kzt'].'</td>
			<td>'.$item_info['cost_confirm'].' '.(empty($confirm_profit) ? '?' : (round($item_info['cost_confirm'] * $arr_rate['rate'] / $confirm_profit * 100).'%')).'</td>
			<td>
			<span data-toggle="modal" data-target="#modal" data-role="item_detailed_stat" data-id="'.$item2['id'].'" class="js-link">
			'.$paid_percent.'%</span>&nbsp;('.$item_info['q_paid'].'&nbsp;��&nbsp;'.($item_info['q_not_paid'] + $item_info['q_paid']).')
			</td>
			<td>'.(empty($confirm_profit) ? '?' : $confirm_profit).'</td>
			<td><button data-toggle="modal" data-target="#modal" data-role="edit_item" data-id="'.$item2['id'].'" class="btn btn-default">��������</button></td>
			<td><button data-toggle="modal" data-target="#modal" data-role="add_stock" data-id="'.$item2['id'].'" class="btn btn-default">���������</button></td>
			<td><button data-toggle="modal" data-target="#modal" data-role="edit_bonuses" data-id="'.$item2['id'].'" class="btn btn-default">������</button></td>
			</tr>';
        }
        if ($item['type'] == 'offer' and ! empty($list_items2)) {
            // $list_items2='<tr><td colspan="10"><h3>'.$item['name'].'</h3><table class="table table-bordered items">'.$list_items2.'</table></td></tr>';
            $list_items2 = '<tr><td colspan="12"><h3>'.$item['name'].'</h3></td></tr>'.$list_items2.'<tr><td colspan="12">&nbsp;</td></tr>';
        }
        $list_items .= $list_items2;
    }
    $arr_sets = query_arr('select id from '.tabname('shop', 'sets').' where show_admin=\'1\';');
    $arr_sets_info = cache('preparing_sets_info', [], 0);
    // pre($arr_sets);
    $list_sets = '';
    foreach ($arr_sets as $set) {
        // $set_info=cache('preparing_set_info',array('id'=>$set['id']),0);
        $set_info = $arr_sets_info[$set['id']];
        // pre($set_info);
        $list_set_items = '';
        foreach ($set_info['items'] as $item) {
            // $item_info=cache('preparing_item_info',array('id'=>$item['item_id']),0);
            $item_info = $arr_item_info[$item['item_id']];
            $list_set_items .= '<li>'.$item_info['name'].'&nbsp;x&nbsp;'.$item['q'].' (������ '.$item['discount'].'%)</li>';
        }
        $list_sets .= '<tr>
		<td>'.$set['id'].'</td><td>'.$set_info['name'].'</td>
		<td><ul class="list-unstyled">'.$list_set_items.'</ul></td>
		<td>'.$set_info['q'].'</td>
		<td><s>'.$set_info['cost_rur_real'].'</s> '.$set_info['cost_rur'].'</td>
		<td><s>'.$set_info['cost_kzt_real'].'</s> '.$set_info['cost_kzt'].'</td>
		</tr>';
    }
    $content = '
	<!-- <div class="table-responsive"> -->
	<table class="table table-striped items">
	<thead><tr>
	<th>ID</th><th>�����</th><th>��������</th>
	<!--<th>����. ����.</th><th>����������</th>-->
	<!--<th>�������. �� �����.</th><th>���������</th><th>��������</th>-->
	<th><div class="tooltip-element">������� 1<div class="tooltip">��� ����� �����������</div></div></th>
	<th><div class="tooltip-element">������� 2<div class="tooltip">�������� �������������� ������</div></div></th>
	<!--<th>��������</th>-->
	<!--<th>���������, RUR</th>-->
	<th>���������, KZT</th>
	<th>�������������</th>
	<th>% ������</th>
	<th>������� � 1 ��������</th>
	<th></th><th></th><th></th>
	</tr></thead>
	'.$list_items.'
	</table>
	<table class="table table-striped sets">
	<thead><tr>
	<th>ID</th><th>�����</th><th>������</th><th>��������</th><th>���������, RUR</th><th>���������, KZT</th>
	</tr></thead>
	'.$list_sets.'
	</table>
	<!-- </div> -->
	';

    return $content;
}

function items_groups_actions()
{
    $group_id = empty($_GET['group_id']) ? error('�� ������� ID ������') : $_GET['group_id'];
    $item_id = empty($_GET['item_id']) ? error('�� ������� ID ������') : $_GET['item_id'];
    $action = empty($_GET['action']) ? error('�� �������� ��������') : $_GET['action'];
    if ($action == 'insert') {
        query('insert into '.typetab('items_groups').' set group_id=\''.$group_id.'\', item_id=\''.$item_id.'\' ;');
    }
    if ($action == 'delete') {
        query('delete from '.typetab('items_groups').' where group_id=\''.$group_id.'\' and item_id=\''.$item_id.'\' ;');
    }
    redirect('/?page=admin/items/items&group_id='.$group_id);
}

function admin_groups_list()
{
    return get_list_categories([
        'current_group_id' => empty($_GET['group_id']) ? false : checkstr($_GET['group_id']),
        'public' => 'all',
        'type' => 'group',
        'template' => 'admin/items/list_item_groups',
    ]);
}

function admin_items()
{
    // $arr_items=query_arr('select items.id
    // 	from '.tabname('shop','items').'
    // 	where show_admin=\'1\'
    // 	order by items.name;');
    $get['group_id'] = empty($_GET['group_id']) ? false : $_GET['group_id'];
    // ���� ������ group_id �� �������� ������ ������� ������
    $arr_group_items = [];
    if ($get['group_id']) {
        $arr_group_items = readdata(query('
			select item_id
			from '.typetab('items_groups').'
			where group_id=\''.$get['group_id'].'\'
			;
			'), 'item_id');
    }
    // pre($arr_group_items);
    $arr_items = query_arr('
		(select name,id,\'offer\' as type from '.tabname('shop', 'item_groups').' where type=\'offer\')
		union (
			select items.name,items.id,\'item\' as type from '.tabname('shop', 'items').' where id not in (
				select item_id from '.tabname('shop', 'items_groups').'
				join '.tabname('shop', 'item_groups').' on item_groups.id=items_groups.group_id
				where type=\'offer\') and items.show_admin=\'1\'
	)
	order by name
	;');
    // �������� ��������� ���� ����� � ����� ��� ������� ������� � �������������
    $arr_rate = query_assoc('select rate from '.typetab('rates').' order by date desc limit 1;');
    // �������� ��������� ������
    $work['call'] = value_from_config('shop', 'call_cost');
    $work['pack'] = value_from_config('shop', 'pack_cost');
    $work['track'] = value_from_config('shop', 'track_cost');
    $work['zip'] = value_from_config('shop', 'zip_cost');
    $work_cost = $work['call'] + $work['pack'] + $work['track'] + $work['zip'];
    // pre($arr_items);
    // $arr['items_']=preparing_items_stat();
    $arr_item_info = cache('preparing_items_info', [], 0);
    $arr_landings_by_item_id = readdata(query('select item_id,dir,show_index from '.typetab('landings').' where item_id is not null order by item_id'), 'item_id', false);
    // pre($arr_landings);
    // pre($arr_landings_by_id);
    // pre($arr_landings);
    $list_items = '';
    foreach ($arr_items as $item) {
        $arr_items2 = [];
        if ($item['type'] == 'offer') {
            $arr_items2 = query_arr('select item_id as id from '.typetab('items_groups').'
				join '.typetab('items').' on items.id=items_groups.item_id
				where show_admin=\'1\' and group_id=\''.$item['id'].'\';');

        } else {
            $arr_items2[]['id'] = $item['id'];
        }
        $list_items2 = '';
        foreach ($arr_items2 as $item2) {
            // $item_info=cache('preparing_item_info',array('id'=>$item2['id']),0);
            $item_info = $arr_item_info[$item2['id']];
            // pre($item_info);
            // $arr_landings=query_arr('select dir,show_index from '.tabname('shop','landings').' where item_id=\''.$item2['id'].'\';');
            $arr_landings = isset($arr_landings_by_item_id[$item2['id']]) ? $arr_landings_by_item_id[$item2['id']] : [];
            // ���� �� ���������
            $list_landings = '<li><a href="http://elitbrand.com/d/'.$item2['id'].'">d/'.$item2['id'].'</a></li>';
            $pic = false;
            foreach ($arr_landings as $landing) {
                $list_landings .= '<li><a href="http://elitbrand.com/'.$landing['dir'].'">'.($landing['show_index'] ? $landing['dir'] : '<s>'.$landing['dir'].'</s>').'</a></li>';
                // $pic=($landing['show_index']==1)?$landing['pic']:$pic;
            }
            $paid_percent = round($item_info['q_paid'] / ((($item_info['q_not_paid'] + $item_info['q_paid']) == 0) ? 1 : ($item_info['q_not_paid'] + $item_info['q_paid'])) * 100);
            $confirm_profit = (($item_info['q_paid'] >= 6) ? round(($item_info['cost_kzt'] - $item_info['real_cost_kzt']) * $paid_percent / 100 - $work_cost) : false);
            $item_margin['kzt'] = (empty($item_info['real_cost_kzt']) ? (empty($item_info['cost_kzt']) ? '<span style="color:red;">��� ���</span>' : '<span style="color:red;">��� �����. ����</span>') : round((($item_info['cost_kzt'] / $item_info['real_cost_kzt']) - 1) * 100).'%');
            // ����� �������, ������ ��� � ������� get_item_groups ������������ clear_temp_vars � ����� ���������� ���������� ����������
            $group_list_include = get_item_groups(['item_id' => $item2['id'], 'template' => 'admin/items/item_group_list_include', 'item_action' => 'include', 'group_type' => 'group']);
            $group_list_exclude = get_item_groups(['item_id' => $item2['id'], 'template' => 'admin/items/item_group_list_exclude', 'item_action' => 'exclude', 'group_type' => 'group']);
            // ������� �������� � ���������� �.�. ����� �� �� clear_temp_vars ���������� ����������
            temp_var('group_list_include', $group_list_include);
            temp_var('group_list_exclude', $group_list_exclude);
            temp_var('id', $item2['id']);
            temp_var('name', $item_info['name']);
            temp_var('landings', '<ul class="list-inline">'.$list_landings.'</ul>');
            temp_var('stock_1', $item_info['q_in_stock'] - $item_info['q_packed']);
            temp_var('stock_2', $item_info['q_in_stock'] - $item_info['q_waiting_send']);
            temp_var('cost_kzt', $item_info['cost_kzt']);
            temp_var('item_margin_kzt', $item_margin['kzt']);
            temp_var('cost_confirm', $item_info['cost_confirm']);
            temp_var('confirm_percent_from_profit', (empty($confirm_profit) ? '?' : (round($item_info['cost_confirm'] * $arr_rate['rate'] / $confirm_profit * 100))));
            temp_var('paid_percent', $paid_percent);
            temp_var('q_paid', $item_info['q_paid']);
            temp_var('q_all', $item_info['q_paid'] + $item_info['q_not_paid']);
            temp_var('confirm_profit', (empty($confirm_profit) ? '?' : $confirm_profit));
            temp_var('pic', empty($item_info['pic']) ? '' : ('/types/shop/'.$item_info['pic']));
            if ($get['group_id']) {
                temp_var('panel_style', isset($arr_group_items[$item2['id']]) ? 'panel-primary' : 'panel-default');
                temp_var('group_action_link', '<a href="/?page=admin/items/group_actions&amp;group_id='.$get['group_id'].'&amp;item_id='.$item2['id'].'&action='.(isset($arr_group_items[$item2['id']]) ? 'delete"><span class="glyphicon glyphicon-minus"></span>' : 'insert"><span class="glyphicon glyphicon-plus"></span>').'</a>'
                );
            } else {
                temp_var('panel_style', 'panel-default');
                temp_var('group_action_link', '');
            }
            $list_items2 .= template('admin/items/list_items');
            clear_temp_vars();
            /*
                        $list_items2.='<tr><td><a href="/?page=admin/orders&amp;status=delivered_not_paid&amp;item_id='.$item2['id'].'" target="_blank">'.$item2['id'].'</a></td><td>'.$item_info['name'].'</td>
                        <td><ul class="list-inline">'.$list_landings.'</ul></td>
                        <!--<td>'.$item_info['q_waiting_send'].'</td><td>'.$item_info['q_sent'].'</td>-->
                        <!--<td>'.$item_info['q_delivered_not_paid'].'</td><td>'.$item_info['q_paid'].'</td>-->
                        <!--<td>'.$item_info['q_canceled'].'</td>-->
                        <td>'.($item_info['q_in_stock']-$item_info['q_packed']).'</td>
                        <td>'.($item_info['q_in_stock']-$item_info['q_waiting_send']).'</td>
                        <!--<td>'.$item_info['q_available'].'</td>-->
                        <!--<td>'.$item_info['cost_rur'].'</td>-->
                        <td>'.$item_info['cost_kzt'].' '.$item_margin['kzt'].'</td>
                        <td>'.$item_info['cost_confirm'].' '.(empty($confirm_profit)?'?':(round($item_info['cost_confirm']*$arr_rate['rate']/$confirm_profit*100).'%')).'</td>
                        <td>
                        <span data-toggle="modal" data-target="#modal" data-role="item_detailed_stat" data-id="'.$item2['id'].'" class="js-link">
                        '.$paid_percent.'%</span>&nbsp;('.$item_info['q_paid'].'&nbsp;��&nbsp;'.($item_info['q_not_paid']+$item_info['q_paid']).')
                        </td>
                        <td>'.(empty($confirm_profit)?'?':$confirm_profit).'</td>
                        <td><button data-toggle="modal" data-target="#modal" data-role="edit_item" data-id="'.$item2['id'].'" class="btn btn-default">��������</button></td>
                        <td><button data-toggle="modal" data-target="#modal" data-role="add_stock" data-id="'.$item2['id'].'" class="btn btn-default">���������</button></td>
                        <td><button data-toggle="modal" data-target="#modal" data-role="edit_bonuses" data-id="'.$item2['id'].'" class="btn btn-default">������</button></td>
                        </tr>';
                        */
        }
        if ($item['type'] == 'offer' and ! empty($list_items2)) {
            // $list_items2='<tr><td colspan="10"><h3>'.$item['name'].'</h3><table class="table table-bordered items">'.$list_items2.'</table></td></tr>';
            $list_items2 = '<tr><td colspan="12"><h3>'.$item['name'].'</h3></td></tr>'.$list_items2.'<tr><td colspan="12">&nbsp;</td></tr>';
        }
        $list_items .= $list_items2;
    }

    return $list_items;
}

function admin_sets()
{
    $arr_sets = query_arr('select id from '.tabname('shop', 'sets').' where show_admin=\'1\' order by id desc;');
    $arr_item_info = cache('preparing_items_info', [], 0);
    $arr_sets_info = cache('preparing_sets_info', [], 0);
    // pre($arr_sets_info);
    $all_items = query_arr('select id,name from '.tabname('shop', 'items').' order by show_admin desc,name;');
    $select_items = get_select_option_list(['array' => $all_items, 'value' => 'id', 'text' => 'name', 'null_value' => '']);
    $list_sets = '';
    foreach ($arr_sets as $set) {
        $set_info = $arr_sets_info[$set['id']];
        $list_set_items = '';
        foreach ($set_info['items'] as $item) {
            $item_info = $arr_item_info[$item['item_id']];
            // $item_info=cache('preparing_item_info',array('id'=>$item['item_id']),0);
            // pre($item_info);
            $list_set_items .= '<li><small>'.$item_info['name'].'&nbsp;x&nbsp;'.$item['q'].' (������ '.$item['discount'].'%, '.$item['discount_kzt'].' ��., '.$item['discount_rub'].' ���.) <a href="/?page=admin/sets/delete_item_from_set&amp;set_id='.$set['id'].'&amp;item_id='.$item_info['id'].'" style="color: #000;" onclick="return confirm(\'������� &quot;'.$item_info['name'].'&quot; �� ������ #'.$set['id'].'?\')?true:false;"><span class="glyphicon glyphicon-remove"></span></a></small></li>';
        }
        temp_var('select_items', $select_items);
        temp_var('items', $list_set_items);
        temp_var('id', $set['id']);
        temp_var('name', $set_info['name']);
        temp_var('q', $set_info['q']);
        temp_var('cost_rur_real', $set_info['cost_rur_real']);
        temp_var('cost_kzt_real', $set_info['cost_kzt_real']);
        temp_var('cost_rur', $set_info['cost_rur']);
        temp_var('cost_kzt', $set_info['cost_kzt']);
        $list_sets .= template('admin/sets/item_set');
        clear_temp_vars();
    }

    return $list_sets;
}

function admin_submit_add_set()
{
    $name = empty($_POST['name']) ? '' : check_input($_POST['name']);
    query('insert into '.typetab('sets').' set name=\''.$name.'\';');
    redirect('/?page=admin/sets/index');
}

function admin_delete_set()
{
    $set_id = empty($_GET['id']) ? '' : check_input($_GET['id']);
    query('delete from '.typetab('sets').' where id=\''.$set_id.'\';');
    redirect('/?page=admin/sets/index');
}

function admin_submit_add_item_into_set()
{
    $set_id = empty($_POST['set_id']) ? error('�� ������� ID ����') : check_input($_POST['set_id']);
    $item_id = empty($_POST['item_id']) ? error('�� ������� ID ������') : check_input($_POST['item_id']);
    $q = empty($_POST['q']) ? 1 : check_input($_POST['q']);
    $discount_rub = empty($_POST['discount_rub']) ? 0 : check_input($_POST['discount_rub']);
    $discount_kzt = empty($_POST['discount_kzt']) ? 0 : check_input($_POST['discount_kzt']);
    $discount = empty($_POST['discount']) ? 0 : check_input($_POST['discount']);
    query('insert into '.typetab('sets_items').' set
		set_id=\''.$set_id.'\',
		item_id=\''.$item_id.'\',
		q=\''.$q.'\',
		discount_rub=\''.$discount_rub.'\',
		discount_kzt=\''.$discount_kzt.'\',
		discount=\''.$discount.'\'
		;');
    reset_cache_all_domains('preparing_set_info', ['id' => $set_id], 'shop');
    redirect('/?page=admin/sets/index');
}

function admin_delete_item_from_set()
{
    $set_id = empty($_GET['set_id']) ? error('�� ������� ID ����') : check_input($_GET['set_id']);
    $item_id = empty($_GET['item_id']) ? error('�� ������� ID ������') : check_input($_GET['item_id']);
    query('delete from '.typetab('sets_items').' where set_id=\''.$set_id.'\' and item_id=\''.$item_id.'\';');
    reset_cache_all_domains('preparing_set_info', ['id' => $set_id], 'shop');
    redirect('/?page=admin/sets/index');
}

function admin_order_set_delivery()
{
    $item_id = isset($_GET['item_id']) ? check_input($_GET['item_id']) : error('�� ������� item_id ��������');
    $order_id = empty($_GET['order_id']) ? error('�� ������� order_id') : check_input($_GET['order_id']);
    // $cost_kzt=empty($_GET['cost_kzt'])?error('�� ������� cost_kzt'):check_input($_GET['cost_kzt']);
    // ��������� ���� �� ����� �����
    $order_info = preparing_order_info(['order_id' => $order_id]);
    if (! empty($item_id)) {
        // ��������� ����������� �� item_id ��������
        $arr = query_assoc('select 1
			from '.typetab('items').'
			where id=\''.$item_id.'\' and is_delivery
			;');
        if (! $arr) {
            error('�������� � ����� item_id ���');
        }
    }
    // ������� ��������
    query('delete '.typetab('packages').' from '.typetab('packages').'
		join '.typetab('items').' on items.id=packages.item_id
		where packages.order_id=\''.$order_id.'\' and items.is_delivery
		;');
    add_item_to_order(['order_id' => $order_id, 'item_id' => $item_id]);

    /*
    if (!empty($item_id)) {
        // ��������� ��������
        plus_minus_items_in_package(array('order_id'=>$order_id,'item_id'=>$item_id,'action'=>'plus'));
    };
    */
    return 1;
}

function form_edit_order_items()
{
    if (empty($_GET['order_id'])) {
        error('�� ������� ID ������');
    }
    $order_id = $_GET['order_id'];
    $all_items = query_arr('select id,name from '.tabname('shop', 'items').' where show_edit_order order by name;');
    $order_info = preparing_order_info(['order_id' => $order_id]);
    // pre($order_info);
    $arr_items = $order_info['items'];
    // ��� ��������
    $delivery = query_arr('select id,name from '.typetab('items').' where is_delivery;');
    // $arr_order=query_assoc('select cost_rur,cost_kzt from '.tabname('shop','orders').' where id=\''.$order_id.'\';');
    $items_list = '';
    $sum_text_full = '';
    $delivery_id = false;
    foreach ($arr_items as $v_item) {
        if ($v_item['is_delivery']) {
            $delivery_id = $v_item['id'];
        }
        // ($v_item['cost_kzt']==$v_item['current_cost_kzt'])?'':'<s>'.$v_item['cost_kzt'].'</s>'
        // '.$v_item['cost_rur'].' ���., '.$v_item['cost_kzt'].' ��.
        if (! in_array($order_info['country'], ['������', '���������'])) {
            error('�������� ������');
        }
        if ($order_info['country'] == '������') {
            $cost_text_full = (empty($v_item['cost_rur']) and empty($v_item['current_cost_rur'])) ? '' : (
                (($v_item['cost_rur'] == $v_item['current_cost_rur']) ? '' : '<s>'.$v_item['current_cost_rur'].'</s>&nbsp;').$v_item['cost_rur'].'&nbsp;���. '
            );
            $sum_text_full = '<b>�����:</b> '.$order_info['cost_rur'].' ������';
        }
        if ($order_info['country'] == '���������') {
            $cost_text_full = (empty($v_item['cost_kzt']) and empty($v_item['current_cost_kzt'])) ? '' : (
                (($v_item['cost_kzt'] == $v_item['current_cost_kzt']) ? '' : '<s>'.$v_item['current_cost_kzt'].'</s>&nbsp;').$v_item['cost_kzt'].'&nbsp;���. '
            );
            $sum_text_full = '<b>�����:</b> '.$order_info['cost_kzt'].' �����';
        }
        // $cost_text_full=$cost_text['rur'].$cost_text['kzt'];
        $main_item_mark = ($order_info['main_item'] == $v_item['id']) ? '&bull; ' : '';
        $main_item_style = ($order_info['main_item'] == $v_item['id']) ? 'style="border-left: 2px solid red;"' : '';
        temp_var('cost_kzt', $v_item['cost_kzt']);
        temp_var('cost_rur', $v_item['cost_rur']);
        temp_var('main_item_style', $main_item_style);
        temp_var('main_item_mark', $main_item_mark);
        temp_var('item_name', $v_item['name']);
        temp_var('cost_text_full', $cost_text_full);
        temp_var('order_id', $order_id);
        temp_var('item_id', $v_item['id']);
        temp_var('item_q', $v_item['q']);
        // ���� ����� ������ �������� ��� ��� ��������, �� ������� ������������ ������
        if ($order_info['editable_cost']) {
            if ($v_item['is_delivery']) {
                $items_list .= template('admin/orders/edit_order/item_order_items_delivery');
            } else {
                $items_list .= template('admin/orders/edit_order/item_order_items');
            }
        } else {
            $items_list .= template('admin/orders/edit_order/item_order_items_disabled');
        }
        clear_temp_vars();
    }
    // ��������� ������ �������� �������� ������������ ���� delivery_id
    temp_var('class_active_no_delivery', ! $delivery_id ? 'active' : '');
    temp_var('order_id', $order_id);
    $delivery_list = template('admin/orders/edit_order/item_no_delivery_button');
    foreach ($delivery as $v) {
        temp_var('class_active', ($v['id'] === $delivery_id) ? 'active' : '');
        temp_var('name', $v['name']);
        temp_var('item_id', $v['id']);
        $delivery_list .= template('admin/orders/edit_order/item_delivery_button');
    }
    clear_temp_vars();
    // ����� ������������ ����� ������ ��������
    temp_var('delivery_list', $delivery_list);
    temp_var('items_list', $items_list);
    temp_var('order_id', $order_id);
    temp_var('sum_text_full', $sum_text_full);
    temp_var('options_all_items', get_select_option_list(['array' => $all_items, 'value' => 'id', 'text' => 'name']));
    if ($order_info['editable_cost']) {
        $content = template('admin/orders/edit_order/block_edit_order_items');
    } else {
        $content = template('admin/orders/edit_order/block_edit_order_items_disabled');
    }
    clear_temp_vars();

    return $content;
}

function order_items_list($arr)
{
    if (empty($arr['order_id'])) {
        error('�� ������� ID ������');
    }
    // $arr_items=preparing_order_items(array('order_id'=>$arr['order_id']));
    $arr_items = cache('preparing_order_items', ['order_id' => $arr['order_id']], 0);
    // pre($arr_items);
    $items_list = '';
    foreach ($arr_items as $v_item) {
        $items_list .= '<li>'.$v_item['name'].'&nbsp;x&nbsp;'.$v_item['q'].'</li>';
    }

    return $items_list;
}

function preparing_order_items($arr)
{
    if (empty($arr['order_id'])) {
        error('�� ������� ID ������');
    }
    $items = query_arr('
		select items.id,items.name,items.cost_rur as current_cost_rur,items.cost_kzt as current_cost_kzt,packages.cost_rur,
		packages.cost_kzt,packages.q, packages.item_cost_kzt as real_cost_kzt, packages.item_cost_rur as real_cost_rur,
		items.kl_item_id,items.power, items.is_delivery, (items.id in (select item_id from '.typetab('items_groups').' where group_id in (2,8,28))) as vip
		from '.tabname('shop', 'packages').'
		join '.tabname('shop', 'items').' on items.id=packages.item_id
		where packages.order_id=\''.$arr['order_id'].'\'
		;');

    // pre($items);
    // pre(preparing_item_info(array('id'=>$items[0]['id'])));
    return $items;
}

/*
function preparing_order_items_all() {
    $items=query_arr('
        select packages.order_id,items.id,items.name,items.cost_rur as current_cost_rur,items.cost_kzt as current_cost_kzt,packages.cost_rur,
        packages.cost_kzt,packages.q,
        packages.item_cost_kzt as real_cost_kzt,
        packages.item_cost_rur as real_cost_rur
        from '.tabname('shop','packages').'
        join '.tabname('shop','items').' on items.id=packages.item_id
        ;');
    foreach ($items as $item) {
        $return[$item['order_id']][]=$item;
    };
    // pre($return);
    // pre(preparing_item_info(array('id'=>$items[0]['id'])));
    return $return;
}
*/

/*
function preparing_order_info_all() {
    $orders=query_arr('select id,inet_ntoa(ip) as ip,country from '.typetab('orders').' ;');
    $orders_items=cache('preparing_order_items_all',array(),0);
    foreach ($orders as $order) {
        $return[$order['id']]=$order;
        $return[$order['id']]['items']=empty($orders_items[$order['id']])?array():$orders_items[$order['id']];
        $return[$order['id']]['cost_rur']=0;
        $return[$order['id']]['cost_kzt']=0;
        $return[$order['id']]['real_cost_rur']=0;
        $return[$order['id']]['real_cost_kzt']=0;
        // pre($return);
        foreach ($return[$order['id']]['items'] as $items) {
            $return[$order['id']]['cost_rur']=$return[$order['id']]['cost_rur']+$items['cost_rur']*$items['q'];
            $return[$order['id']]['cost_kzt']=$return[$order['id']]['cost_kzt']+$items['cost_kzt']*$items['q'];
            $return[$order['id']]['real_cost_rur']=$return[$order['id']]['real_cost_rur']+$items['real_cost_rur']*$items['q'];
            $return[$order['id']]['real_cost_kzt']=$return[$order['id']]['real_cost_kzt']+$items['real_cost_kzt']*$items['q'];
        };
        // pre($return);
    };
    // pre($return);
    return $return;
}
*/

/*
function preparing_order_info_old($args) {
    $order_id=empty($args['order_id'])?error('�� ������ order_id'):$args['order_id'];
    $arr=query_assoc('select inet_ntoa(ip) as ip,country,main_item
        from '.typetab('orders').'
        where orders.id=\''.$args['order_id'].'\';');
    if (empty($arr)) {error('����� �� ������');};
    $return=$arr;
    $return['items']=cache('preparing_order_items',array('order_id'=>$order_id),0);
    $return['cost_rur']=0;
    $return['cost_kzt']=0;
    $return['real_cost_rur']=0;
    $return['real_cost_kzt']=0;
    foreach ($return['items'] as $v) {
        $return['cost_rur']=$return['cost_rur']+$v['cost_rur']*$v['q'];
        $return['cost_kzt']=$return['cost_kzt']+$v['cost_kzt']*$v['q'];
        $return['real_cost_rur']=$return['real_cost_rur']+$v['real_cost_rur']*$v['q'];
        $return['real_cost_kzt']=$return['real_cost_kzt']+$v['real_cost_kzt']*$v['q'];
    };
    return $return;
}
*/

function preparing_order_info($args)
{
    $order_id = empty($args['order_id']) ? error('�� ������ order_id') : $args['order_id'];
    $discount_item_id = value_from_config('shop', 'discount_item_id');
    $arr = query_assoc('select orders.ip,orders.country,orders.main_item,orders.date,
		if(date(orders.date)=current_date,date_format(orders.date,\'������� � %H:%i\'),if(date(orders.date)=(current_date-interval 1 day),date_format(orders.date,\'����� � %H:%i\'),if(date(orders.date)=(current_date-interval 2 day),date_format(orders.date,\'��������� � %H:%i\'),date_format(orders.date,\'%Y-%m-%d � %H:%i\')))) as date_human,
		orders.phone,orders.address,orders.id as order_id,orders.country,orders.name as client_name,
		orders.zip,orders.advertiser,orders.utm_source,orders.status,orders.track,orders.shave,orders.upsale,
		cities.id as city_id,cities.name as city_name,cities.kl_city_id,
		ifnull(sum(packages.cost_kzt*packages.q),0) as cost_kzt,
		ifnull(sum(packages.cost_rur*packages.q),0) as cost_rur,
		ifnull(sum(if(packages.item_id='.$discount_item_id.',0,packages.cost_kzt)*packages.q),0) as cost_kzt_without_discount,
		ifnull(sum(if(packages.item_id='.$discount_item_id.',0,packages.cost_rur)*packages.q),0) as cost_rur_without_discount,
		ifnull(sum(packages.item_cost_rur*packages.q),0) as real_cost_rur,
		ifnull(sum(packages.item_cost_kzt*packages.q),0) as real_cost_kzt,
		if(advertiser is not null and orders.main_item is not null and not (shave and user_status in (\'new\',\'unavailable\',\'canceled\')),orders.bonus,0) as advertiser_bonus_rub,
		ifnull((select rate from '.typetab('rates').' where date=date(orders.date)),(select rate from '.typetab('rates').' order by date desc limit 1)) as rate_kzt,statuses.editable_cost,kazpost_api.sent_date
		from '.typetab('orders').'
		left join '.typetab('packages').' on packages.order_id=orders.id
		left join '.typetab('cities').' on cities.id=orders.city_id
		left join '.typetab('kazpost_api').' on kazpost_api.track=orders.track
		join '.typetab('statuses').' on statuses.name=orders.status
		where orders.id=\''.$args['order_id'].'\';');
    if (empty($arr['order_id'])) {
        error('����� �� ������');
    }
    // pre($arr);
    $arr['advertiser_bonus_kzt'] = round($arr['advertiser_bonus_rub'] * $arr['rate_kzt']);
    $arr['history'] = query_assoc('
		select
		max(if(oc.field=\'status\' and oc.value=\'check address\',date(oc.date),NULL)) as approve_date,
		max(if(oc.field=\'zipper\',date(oc.date),NULL)) as zip_date,
		max(if(oc.field=\'status\' and oc.value=\'packed\',date(oc.date),NULL)) as pack_date,
		max(if(oc.field=\'status\' and oc.value=\'delivered not paid\',date(oc.date),NULL)) as delivery_date
		from '.typetab('orders_changes').' oc
		where oc.primary_key=\''.$args['order_id'].'\'
		;');
    // ��������� ���-�� ���� ������� ������������� �� ��������� ������ �� ������� ������������� ������ �� ������� ��������
    $arr['send_interval'] = false;
    if (! empty($arr['sent_date']) and ! empty($arr['history']['approve_date'])) {
        $datetime1 = new DateTime($arr['history']['approve_date']);
        // �������� ������ ���� ��� �������, �.�. ���� �������� ���� ��� �������
        $datetime1->setTime(0, 0, 0);
        $datetime2 = new DateTime($arr['sent_date']);
        $interval = $datetime1->diff($datetime2);
        $arr['send_interval'] = $interval->format('%r%a');
    }
    // ��������� ���-�� ���� ������� ������������� �� �������� ������ �� ������� �������� �� ������� ��������
    $arr['delivery_interval'] = false;
    if (! empty($arr['sent_date']) and ! empty($arr['history']['delivery_date'])) {
        $datetime1 = new DateTime($arr['sent_date']);
        $datetime2 = new DateTime($arr['history']['delivery_date']);
        // �������� ������ ���� ��� �������, �.�. ���� �������� ���� ��� �������
        $datetime2->setTime(0, 0, 0);
        $interval = $datetime1->diff($datetime2);
        $arr['delivery_interval'] = $interval->format('%r%a');
    }
    // pre($arr['work_interval']);
    $arr['items'] = cache('preparing_order_items', ['order_id' => $order_id], 0);
    $arr['ketlogistic']['items_compatible'] = true;
    $arr['ketlogistic']['city_compatible'] = true;
    $all_power = 0;
    $arr['is_vip'] = 0;
    foreach ($arr['items'] as $v) {
        $all_power = $all_power + $v['q'] * ($v['power'] / 100);
        $arr['is_vip'] = empty($arr['is_vip']) ? $v['vip'] : 1;
    }
    // pre($q_items);
    // ��������� ����� ����� ���������� ����� �� ������ � ������� ��� ������
    foreach ($arr['items'] as $k => $v) {
        $arr['items'][$k]['order_part'] = round($v['q'] * ($v['power'] / 100) / ($all_power == 0 ? 1 : $all_power), 5);
        // if (isset($_GET['buyout_new'])) {
        $arr['items'][$k]['buyout'] = item_buyout(['item_id' => $v['id'], 'advertiser' => $arr['advertiser'], 'utm_source' => $arr['utm_source']]);
        // } else {
        // $arr['items'][$k]['buyout']=item_buyout_old(array('item_id'=>$v['id'],'advertiser'=>$arr['advertiser']));
        // };
        // $arr['items'][$k]['buyout']=0;
    }
    // ����������� ����� ����� ������
    $arr['buyout'] = 0;
    foreach ($arr['items'] as $v) {
        $arr['buyout'] = $arr['buyout'] + $v['buyout']['buyout'] * $v['order_part'];
    }
    // ��������� �������� ����� ������
    $arr['buyout'] = round($arr['buyout']);
    // $arr['buyout']=51;
    // $arr['no_buyout']=(100-$arr['buyout']);
    $arr['real_buyout'] = in_array($arr['status'], ['paid', 'take payment']) ? 100 : 0;
    // ��������� ������ �� ������
    $arr2['work']['return'] = value_from_config('shop', 'delivery_back_cost');
    $arr2['work']['call'] = value_from_config('shop', 'call_cost');
    $arr2['work']['call_vip'] = value_from_config('shop', 'call_vip_cost');
    $arr2['work']['delivery'] = value_from_config('shop', 'delivery_cost');
    $arr2['work']['pack'] = value_from_config('shop', 'pack_cost');
    $arr2['work']['track'] = value_from_config('shop', 'track_cost');
    $arr2['work']['zip'] = value_from_config('shop', 'zip_cost');
    $arr['work_cost'] = $arr2['work']['call'] + $arr2['work']['pack'] + $arr2['work']['track'] + $arr2['work']['zip'] + $arr['is_vip'] * $arr2['work']['call_vip'];
    $arr['delivery_cost'] = $arr2['work']['delivery'];
    // $arr['delivery_back_cost']=round($arr2['work']['return']*$arr['no_buyout']/100);
    $arr['delivery_back_cost'] = $arr2['work']['return'];
    $arr['insurance_cost'] = round($arr['cost_kzt'] * 0.01);
    // $arr['profit']=round(($arr['cost_kzt']-$arr['real_cost_kzt'])*$arr['buyout']/100-$arr['delivery_cost']-$arr['delivery_back_cost']-$arr['insurance_cost']-$arr['work_cost']-$arr['advertiser_bonus_kzt']);
    // ��������� � orders.date>=\'2016-05-23\' - �� ����� ��� ������� �� �������������� ����������� �������, ������ ��������� ���������.
    // ������� �� ����� ��� �� ������� ������� ����������� �������������, ����� �� ���� �������� ��������� (����� ��������� �������+����� ����������� �������������)
    // pre(var_dump(($arr['date']<'2016-05-23')?true:false));
    if ($arr['date'] < '2016-05-23') {
        $arr['advertiser_bonus_kzt'] = 0;
    }
    $arr['profit'] = round(($arr['cost_kzt'] - $arr['real_cost_kzt'] + $arr['delivery_back_cost']) * $arr['buyout'] / 100 - $arr['delivery_cost'] - $arr['delivery_back_cost'] - $arr['insurance_cost'] - $arr['work_cost'] - $arr['advertiser_bonus_kzt']);
    $arr['real_profit'] = round(($arr['cost_kzt'] - $arr['real_cost_kzt'] + $arr['delivery_back_cost']) * $arr['real_buyout'] / 100 - $arr['delivery_cost'] - $arr['delivery_back_cost'] - $arr['insurance_cost'] - $arr['work_cost'] - $arr['advertiser_bonus_kzt']);
    foreach ($arr['items'] as $v) {
        if (empty($v['kl_item_id'])) {
            $arr['ketlogistic']['items_compatible'] = false;
            break;
        }
    }
    if (empty($arr['kl_city_id'])) {
        $arr['ketlogistic']['items_compatible'] = false;
    }
    $arr['ketlogistic']['compatible'] = ($arr['ketlogistic']['city_compatible'] & $arr['ketlogistic']['items_compatible']) ? true : false;

    // pre($arr);
    return $arr;
}

/*
function is_defined($var=null,$arr=array()) {
    if (is_null($var)) {error('�� �������� ���������� ��� �������� �����������');};
    // pre(var_dump($arr));
    return array_key_exists($var,$arr)?true:false;
}

function test_null() {
    global $config;
    $f=null;
    $f['gg']=null;
    $result=is_defined('f',get_defined_vars());
    pre(var_dump($result));
    $arr=get_defined_vars();
    isset($arr[$value])?true:false;
    // pre(var_dump($arr['config']['temp_vars']));
    // pre(var_dump(isset($vvv)));
    pre(var_dump(is_null($vvv)));
}
test_null();
*/

// pre(var_dump(get_geoip_country_code('2001:DB0:0:123A:0:0:0:30')));

function block_orders_design()
{
    $design = ['new' => true, 'old' => true];
    $user_design = empty($_COOKIE['mgw_shop_orders_design']) ? 'new' : check_input($_COOKIE['mgw_shop_orders_design']);
    if (! isset($design[$user_design])) {
        error('��������� ������ �� ����������');
    }
    temp_var('active_old', ($user_design === 'old') ? 'active' : '');
    temp_var('active_new', ($user_design === 'new') ? 'active' : '');
    $content = template('admin/orders/block_design');
    clear_temp_vars();

    return $content;
}

function admin_set_orders_design()
{
    $design = ['new' => true, 'old' => true];
    $user_design = empty($_GET['name']) ? error('�� �������� �������� �������') : check_input($_GET['name']);
    if (! isset($design[$user_design])) {
        error('��������� ������ �� ����������');
    }
    // ������ ���� � ��������� �������� �� ���
    setcookie('mgw_shop_orders_design', $user_design, time() + 3600 * 24 * 365, '/');
    redirect('/?page=admin/orders');
}

function admin_orders($args = [])
{
    //
    $design = ['new' => 'admin/orders/new/', 'old' => 'admin/orders/old/'];
    $user_design = empty($_COOKIE['mgw_shop_orders_design']) ? 'new' : check_input($_COOKIE['mgw_shop_orders_design']);
    $user_design_path = isset($design[$user_design]) ? $design[$user_design] : error('��������� ������ �� ����������');
    //
    $args['template_order'] = empty($args['template_order']) ? $user_design_path.'order' : $args['template_order'];
    $args['template_block'] = empty($args['template_block']) ? $user_design_path.'block' : $args['template_block'];
    $args['no_limit'] = isset($args['no_limit']) ? $args['no_limit'] : false;
    // ���� �������� ��������� �������, �� � ���������� ��� (������������ �������� � admin_orders_too_f5)
    $args['print'] = isset($args['print']) ? $args['print'] : (isset($_GET['print']) ? $_GET['print'] : '');
    $args['status'] = isset($args['status']) ? $args['status'] : (isset($_GET['status']) ? $_GET['status'] : '');
    $args['road_dif_exact'] = empty($_GET['road_dif_exact']) ? false : check_input($_GET['road_dif_exact']);
    //
    $var['print'] = check_print($args['print']);
    $var['status'] = check_status($args['status']);
    $var['source'] = check_source(isset($_GET['source']) ? $_GET['source'] : '');
    $var['advertiser'] = check_advertiser(isset($_GET['advertiser']) ? $_GET['advertiser'] : '');
    // pre($var);
    $var['search'] = check_order_search(isset($_GET['search']) ? $_GET['search'] : '');
    $var['sms'] = check_getsms(isset($_GET['sms']) ? $_GET['sms'] : '');
    $var['country'] = check_country(isset($_GET['country']) ? $_GET['country'] : '');
    $var['item_id'] = check_order_item_id(isset($_GET['item_id']) ? $_GET['item_id'] : '');
    $var['order_id'] = check_order_id(isset($_GET['order_id']) ? $_GET['order_id'] : '');
    $var['page'] = check_order_page(isset($_GET['p']) ? (int) $_GET['p'] : 1);
    $var['zipper']['where'] = empty($_GET['zipper']) ? '1' : 'zipper=\''.check_input($_GET['zipper']).'\'';
    $var['sms_not_date']['max_sms_date'] = empty($_GET['sms_not_date']) ? '' : ',(select max(date(date)) from '.tabname('shop', 'sms').' where order_id=orders.id) as max_sms_date';
    $var['sms_not_date']['having'] = empty($_GET['sms_not_date']) ? '' : 'having max_sms_date!=\''.$_GET['sms_not_date'].'\'';
    $var['road_dif']['where'] = empty($_GET['road_dif']) ? '1' : 'datediff(current_timestamp,kazpost_api.date)>'.$_GET['road_dif'];
    $var['road_dif_exact']['where'] = empty($args['road_dif_exact']) ? '1' : 'datediff(current_timestamp,kazpost_api.date)='.$args['road_dif_exact'];
    $var['wrong_tracks']['where'] = empty($_GET['wrong_tracks']) ? '1' : '(orders.status in (\'sent\',\'delivered not paid\') and kazpost_api.track is null)';
    $var['paid_manager']['where'] = empty($_GET['paid_manager']) ? '1' : 'orders.id in (select primary_key from '.typetab('orders_changes').' where field=\'status\' and value=\'paid\' and login=\'letos\')';
    $var['caller']['where'] = empty($_GET['caller']) ? '1' : '(select count(*) from '.typetab('orders_calls').' where order_id=orders.id and login=\''.$_GET['caller'].'\')>0';
    $var['paid_caller'] = check_paid_caller(isset($_GET['paid_caller']) ? $_GET['paid_caller'] : '');
    $var['shave'] = check_shave(isset($_GET['shave']) ? $_GET['shave'] : '');
    $var['level'] = check_level();
    // $var['wrong_tracks']['where']='1';
    // date('Y-m-d',strtotime('26.05.2015 07:03:59'))
    // pre($var);
    // $login_info=preparing_user_info(array('login'=>$_SESSION['shop']['auth']['login'],'type'=>'shop'));
    // ����� ��������� ��� � �������� ������� �� ����������� �� ���� ���� � ������
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'shop']), 'type' => 'shop'], 0);
    $arr = query_arr('select sql_calc_found_rows orders.id,date_format(orders.date,\'%Y-%m-%d %H:%i\') as date,
		date_format(orders.date,\'%d.%m.%Y\') as order_date_dmy,
		orders.utm_source,orders.utm_content,orders.phone,orders.name,orders.zip,orders.region,orders.country,
		orders.locality_type,orders.locality,orders.address,orders.status,orders.user_status,orders.shave,orders.upsale,orders.track,orders.info,orders.kazpost_too,items_images.path as item_icon,
		orders.login,orders.zipper,orders.packer,orders.tracker,orders.advertiser,orders.print,
		orders.ip,right(orders.phone,7) as phone_part,
		kazpost_api.zip as kazpost_zip, kazpost_api.place as kazpost_place, kazpost_api.status as kazpost_status,
		date(kazpost_api.date) as kazpost_date,datediff(current_date,date(kazpost_api.date)) as kazpost_datediff,
		landings.dir as landing_dir,
		/* (select count(*) from '.tabname('shop', 'orders').' o2 where orders.phone!=\'\' and orders.phone is not null and o2.phone like concat(\'%\',right(orders.phone,7),\'%\')) as phone_q, */
		(select count(*) from '.tabname('shop', 'orders').' o2 where orders.phone!=\'\' and orders.phone is not null and o2.phone=orders.phone) as phone_q,
		(select count(*) from '.tabname('shop', 'orders').' o3 where o3.ip=orders.ip) as ip_q,
		(select count(*) from '.tabname('shop', 'sms').' where order_id=orders.id and delivered) as sms_ok,
		(select count(*) from '.tabname('shop', 'sms').' where order_id=orders.id) as sms_q
		'.$var['sms_not_date']['max_sms_date'].'
	 from '.tabname('shop', 'orders').'
	 left join '.typetab('items').' on items.id=orders.main_item
	 left join '.typetab('items_images').' on items_images.id=items.icon_id
	 left join '.tabname('shop', 'kazpost_api').' on kazpost_api.track=orders.track
	 left join '.tabname('shop', 'landings').' on landings.dir=orders.utm_content
	 '.$var['item_id']['join'].'
	 where '.$var['status']['where'].' and '.$var['source']['where'].' and '.$var['advertiser']['where'].' and '.$var['search']['where'].'
	 and '.$var['country']['where'].' and '.$var['item_id']['where'].' and '.$var['zipper']['where'].'
	 and '.$var['road_dif']['where'].' and '.$var['road_dif_exact']['where'].' and '.$var['wrong_tracks']['where'].' and '.$var['paid_manager']['where'].'
	 and '.$var['level']['where'].' and '.$var['caller']['where'].' and '.$var['paid_caller']['where'].' and '.$var['order_id']['where'].'
	 and '.$var['shave']['where'].' and '.$var['print']['where'].'
	 '.$var['sms']['having'].'
	 '.$var['sms_not_date']['having'].'
	order by orders.date asc
	'.($args['no_limit'] ? '' : $var['page']['limit']).'
	;');
    // pre($arr);
    $rows = rows_without_limit();
    $rus['status'] = enum_to_array(tabname('shop', 'orders'), 'status', shop_translation('order_status'));
    // $gdeposylka_apikey=value_from_config('shop','gdeposylka_apikey');
    $list = '';
    $n = 0;
    foreach ($arr as $v) {
        if (! in_array($v['country'], ['������', '���������'])) {
            error('�� ������� ������ ��� ������ '.$v['id']);
        }
        // $order_info=preparing_order_info(array('order_id'=>$v['id']));
        $order_info = cache('preparing_order_info', ['order_id' => $v['id']], 0);
        // pre($order_info);
        // �������� ������ �������
        $items_list = '';
        $items_list_ems_utf8 = '';
        $i = 0;
        // pre($order_info['items']);
        foreach ($order_info['items'] as $v_item) {
            $i++;
            $items_list_title = '';
            if (in_array($login_info['level'], ['admin'])) {
                $items_list_title = 'title="����� '.$v_item['buyout']['buyout'].'% '.$v_item['buyout']['type_ru'].'"';
            }
            // $items_list.='<li '.$items_list_title.'>'.$v_item['name'].'&nbsp;x&nbsp;'.$v_item['q'].'</li>';
            $is_main_item = ($order_info['main_item'] == $v_item['id']) ? true : false;
            $main_item_id = $is_main_item ? $v_item['id'] : false;
            temp_var('item_name', $v_item['name']);
            temp_var('main_item_style', $is_main_item ? 'style="border-left: 2px solid red;"' : '');
            // temp_var('cost_text_full',)
            temp_var('item_q', $v_item['q']);
            $items_list .= template($user_design_path.'order_item');
            $items_list_ems_utf8 .= '<tr><td>'.$i.'</td><td>'.$v_item['id'].'</td>
			<td colspan="4">'.iconv('windows-1251', 'utf-8', $v_item['name']).'</td><td>'.$v_item['q'].'</td>
			<td>'.$v_item['cost_kzt'].'</td><td>'.($v_item['cost_kzt'] * $v_item['q']).'</td></tr>';
            clear_temp_vars();
        }
        // ����� ��������� ������ �������
        // pre($order_info);
        $n++;
        // ����� �� �������
        temp_var('n', $n);
        temp_var('id', $v['id']);
        temp_var('main_item_id', $main_item_id ? $main_item_id : '');
        temp_var('track', empty($v['track']) ? '' : $v['track']);
        temp_var('track_link', empty($v['track']) ? '<em>���</em>' : ('<a href="https://post.kz/mail/search/track/'.$v['track'].'/detail" target="_blank">'.$v['track'].'</a>'));
        temp_var('pic', empty($v['item_icon']) ? '' : ('/types/shop/'.$v['item_icon']));
        temp_var('kazpost_datediff', (empty($v['kazpost_datediff']) and $v['kazpost_datediff'] !== 0) ? '' : $v['kazpost_datediff']);
        temp_var('kazpost_place', empty($v['kazpost_place']) ? '' : $v['kazpost_place']);
        temp_var('kazpost_date', empty($v['kazpost_date']) ? '' : $v['kazpost_date']);
        temp_var('kazpost_status', kazpost_statuses($v['kazpost_status']));
        temp_var('kazpost_zip', empty($v['kazpost_zip']) ? '' : $v['kazpost_zip']);
        $track_str = empty($v['kazpost_date']) ? '<em>���</em>' : template($user_design_path.'track_str');
        temp_var('sms_q', $v['sms_q']);
        $sms_info = empty($v['sms_q']) ? '' : template($user_design_path.'sms_q');
        $text_profit = '';
        if ($v['country'] == '������') {
            $show_cost = $order_info['cost_rur'].'&nbsp;���.';
            if (in_array($login_info['level'], ['admin'])) {
                $text_profit = '��� �������';
            }
        }
        if ($v['country'] == '���������') {
            $show_cost = $order_info['cost_kzt'].'&nbsp;��.';
            if (in_array($login_info['level'], ['admin'])) {
                $text_profit = '
				<span data-toggle="modal" data-target="#modal-lg" data-role="order_stat"
				data-id="'.$v['id'].'" class="js-link"
				'.(($order_info['profit'] < 0) ? 'style="color: red; font-weight: bold;"' : '').'>
				'.$order_info['profit'].'&nbsp;��.</span>
				'.(($order_info['advertiser_bonus_kzt'] == 0) ? '��� ����.' : '');
            }
        }
        // $show_cost['rur']=empty($order_info['cost_rur'])?'':$order_info['cost_rur'].'&nbsp;���.';
        // $show_cost['kzt']=empty($order_info['cost_kzt'])?'':$order_info['cost_kzt'].'&nbsp;��.';
        // $items_list=order_items_list(array('order_id'=>$v['id']));
        $text_phone = '';
        if ($v['phone_q'] > 1) {
            $text_phone = $v['phone_q'].' <a href="/?page=admin/orders&amp;status=all&amp;search='.$v['phone_part'].'">������</a>';
        }
        $text_ip = '';
        if ($v['ip_q'] > 1) {
            $text_ip = $v['ip_q'].' <a href="/?page=admin/orders&amp;status=all&amp;search='.$v['ip'].'">������</a>';
        }
        temp_var('class_print_active', $v['print'] ? 'active' : '');
        $buttons['caller'] = '';
        $buttons['packer'] = '';
        $buttons['admin'] = '';
        if (in_array($login_info['level'], ['admin', 'manager', 'packer', 'packer_lite', 'caller', 'worker'])) {
            $buttons['caller'] = template($user_design_path.'buttons_caller');
        }
        if (in_array($login_info['level'], ['admin', 'manager', 'packer', 'packer_lite'])) {
            $buttons['packer'] = template($user_design_path.'buttons_packer');
        }
        if (in_array($login_info['level'], ['admin', 'manager'])) {
            $buttons['admin'] = template($user_design_path.'buttons_admin');
        }
        $calls_list = '';
        if (in_array($v['status'], ['new', 'unavailable'])) {
            $calls = query_arr('select orders.id,orders_calls.login,orders_calls.comment,timestampdiff(minute,orders_calls.date,current_timestamp) as minutes_diff,
				timestampdiff(hour,orders_calls.date,current_timestamp) as hours_diff,
				timestampdiff(day,orders_calls.date,current_timestamp) as day_diff
				from '.typetab('orders').'
				join '.typetab('orders_calls').' on orders_calls.order_id=orders.id
				where orders.id=\''.$v['id'].'\'
				order by orders_calls.date desc
				limit 3
				;');
            $calls = array_reverse($calls);
            foreach ($calls as $c) {
                $calls_list .= '<span class="glyphicon glyphicon-phone-alt"></span>&nbsp;<b>'.$c['login'].'</b>
				'.(($c['day_diff'] == 0) ? (($c['hours_diff'] == 0) ? $c['minutes_diff'].'&nbsp;���.' : $c['hours_diff'].'&nbsp;�.') : $c['day_diff'].'&nbsp;��.').' '.$c['comment'].'
				<br>';
            }
            $calls_list = '<p>'.$calls_list.'</p>';
        }
        if (in_array($v['status'], ['delivered not paid'])) {
            $calls = query_arr('select orders.id,orders_calls.login,orders_calls.comment,timestampdiff(minute,orders_calls.date,current_timestamp) as minutes_diff,
				timestampdiff(hour,orders_calls.date,current_timestamp) as hours_diff,
				timestampdiff(day,orders_calls.date,current_timestamp) as day_diff
				from '.typetab('orders').'
				join '.typetab('orders_calls').' on orders_calls.order_id=orders.id
				where orders.id=\''.$v['id'].'\'
				and orders_calls.date>(select date from '.typetab('orders_changes').' where primary_key=orders.id and field=\'status\' and value=\'delivered not paid\' limit 1)
				order by orders_calls.date desc
				limit 3
				;');
            $calls = array_reverse($calls);
            foreach ($calls as $c) {
                $calls_list .= '<span class="glyphicon glyphicon-phone-alt"></span>&nbsp;<b>'.$c['login'].'</b>
				'.(($c['day_diff'] == 0) ? (($c['hours_diff'] == 0) ? $c['minutes_diff'].'&nbsp;���.' : $c['hours_diff'].'&nbsp;�.') : $c['day_diff'].'&nbsp;��.').' '.$c['comment'].'
				<br>';
            }
            $calls_list = '<p>'.$calls_list.'</p>';
        }
        // pre(var_dump($v));
        $geo_country = empty($v['ip']) ? 'ip ���' : get_geoip_country_code($v['ip']);
        temp_var('caller', empty($v['login']) ? '<em>���</em>' : $v['login']);
        temp_var('zipper', empty($v['zipper']) ? '<em>���</em>' : $v['zipper']);
        temp_var('packer', empty($v['packer']) ? '<em>���</em>' : $v['packer']);
        temp_var('tracker', empty($v['tracker']) ? '<em>���</em>' : $v['tracker']);
        temp_var('approve_date', empty($order_info['history']['approve_date']) ? '' : $order_info['history']['approve_date']);
        temp_var('zip_date', empty($order_info['history']['zip_date']) ? '' : $order_info['history']['zip_date']);
        temp_var('pack_date', empty($order_info['history']['pack_date']) ? '' : $order_info['history']['pack_date']);
        temp_var('sent_date', empty($order_info['sent_date']) ? '' : $order_info['sent_date']);
        temp_var('text_send_interval', empty($order_info['send_interval']) ? '' : '<br>
		��������� �� '.$order_info['send_interval'].' ��.');
        temp_var('text_delivery_interval', empty($order_info['delivery_interval']) ? '' : '<br>
		��������� �� '.$order_info['delivery_interval'].' ��.');
        // global $config;
        // pre(var_dump($config['temp_vars']));
        $elements['order_workers'] = in_array($login_info['level'], ['admin', 'caller', 'manager', 'packer']) ? template($user_design_path.'workers') : '';
        // $elements['phone']=in_array($login_info['level'],array('admin','caller','manager','packer'))?($v['phone'].$text_phone):(substr($v['phone'],0,-4).'XXXX');
        temp_var('kazpost_too', empty($v['kazpost_too']) ? '<em>���</em>' : $v['kazpost_too']);
        temp_var('insurance_cost', $order_info['insurance_cost']);
        temp_var('name', fix_encode(empty($v['name']) ? '' : $v['name']));
        temp_var('ip', empty($v['ip']) ? '' : $v['ip']);
        temp_var('date', $v['date']);
        temp_var('date_human', $order_info['date_human']);
        temp_var('order_date_dmy', $v['order_date_dmy']);
        temp_var('address', empty($v['address']) ? '' : $v['address']);
        temp_var('geo_country', $geo_country);
        temp_var('text_ip', $text_ip);
        temp_var('phone', empty($v['phone']) ? '' : $v['phone']);
        temp_var('text_phone', $text_phone);
        temp_var('items_list', $items_list);
        temp_var('is_item_ketlogistic', $order_info['ketlogistic']['items_compatible'] ? '<b>ketlogistic</b>' : '');
        temp_var('show_cost', $show_cost);
        temp_var('text_profit', $text_profit);
        temp_var('zip', empty($v['zip']) ? '' : $v['zip']);
        temp_var('is_order_ketlogistic', $order_info['ketlogistic']['compatible'] ? '<b>ketlogistic</b>' : '');
        temp_var('advertiser', empty($v['advertiser']) ? '' : '<span class="glyphicon glyphicon-user"></span> '.$v['advertiser']);
        temp_var('utm_source', $v['utm_source']);
        temp_var('landing_link', empty($v['landing_dir']) ? (empty($v['utm_content']) ? '' : $v['utm_content']) : '<a href="http://elitbrand.com/'.$v['utm_content'].'">'.$v['utm_content'].'</a>');
        temp_var('rus_status', $rus['status'][$v['status']]);
        temp_var('rus_shave_status', $v['shave'] ? $rus['status'][$v['user_status']] : '');
        temp_var('order_workers', $elements['order_workers']);
        temp_var('buttons_caller', $buttons['caller']);
        temp_var('buttons_packer', $buttons['packer']);
        temp_var('buttons_admin', $buttons['admin']);
        temp_var('city_name', empty($order_info['city_name']) ? '' : '<b>�. '.$order_info['city_name'].'</b>');
        temp_var('track_str', $track_str);
        temp_var('calls_list', $calls_list);
        temp_var('info', nl2br($v['info']));
        temp_var('sms_info', $sms_info);
        // ���������� ��� ���
        temp_var('cost_kzt', $order_info['cost_kzt']);
        temp_var('name_utf8', iconv('windows-1251', 'utf-8', $v['name']));
        temp_var('address_utf8', iconv('windows-1251', 'utf-8', $v['address']));
        temp_var('phone_utf8', iconv('windows-1251', 'utf-8', $v['phone']));
        temp_var('declared_value_cost_kzt', (($order_info['cost_kzt'] == 0) ? $order_info['cost_kzt_without_discount'] : $order_info['cost_kzt']));
        temp_var('declared_value_cost_kzt_no_currency_str_utf8',
            iconv('windows-1251', 'utf-8',
                num2str((($order_info['cost_kzt'] == 0) ? $order_info['cost_kzt_without_discount'] : $order_info['cost_kzt']), ['currency' => 'no', 'is_kop' => false])));
        temp_var('cost_kzt_no_currency_str_utf8', iconv('windows-1251', 'utf-8', num2str($order_info['cost_kzt'], ['currency' => 'no', 'is_kop' => false])));
        temp_var('items_list_ems_utf8', $items_list_ems_utf8);
        $my_date = my_date(time());
        // pre($my_date);
        temp_var('current_day', $my_date['day']);
        temp_var('current_month_utf8', iconv('windows-1251', 'utf-8', $my_date['month']));
        temp_var('current_year', $my_date['year']);
        // ������� � ������ � ����� ������� � ����� � ������� ����� �������� ��������� ������������ csv �����
        temp_var('name_utf8_csv', str_replace(['"', ';'], '', iconv('windows-1251', 'utf-8', $v['name'])));
        temp_var('address_utf8_csv', str_replace(['"', ';'], '', iconv('windows-1251', 'utf-8', $v['address'])));
        // ����� ���������� ��� ���
        temp_var('text_upsale', $v['upsale'] ? '<b><span class="glyphicon glyphicon-ok"></span>&nbsp;������</b>' : '');
        $list .= template($args['template_order']);
        clear_temp_vars();
    }
    if (empty($list)) {
        return;
    }
    $link['i_called'] = in_array($login_info['level'], ['admin', 'caller', 'manager']) ? '
	<a class="btn btn-'.(empty($_GET['caller']) ? 'default' : 'primary').' " href="/?page=admin/orders&amp;status='.getvar('status').'&amp;source='.getvar('source').
    '&amp;search='.get_or_post('search').'&amp;sms='.getvar('sms').'&amp;country='.getvar('country').'&amp;item_id='.getvar('item_id').
    '&amp;zipper='.getvar('zipper').'&amp;road_dif='.getvar('road_dif').'&amp;road_dif_exact='.getvar('road_dif_exact').'&amp;paid_manager='.getvar('paid_manager').'&amp;caller='.auth_login(['type' => 'shop']).'&amp;p=1">
	�������� ������, ������� � ������(�)
	</a>
	' : '';
    temp_var('block_not_paid_days', '');
    // pre($var['status']);
    if (in_array($var['status']['value_with_spaces'], ['delivered not paid'])) {
        temp_var('class_active_not_paid_days_5', ($args['road_dif_exact'] == 5) ? 'active' : '');
        temp_var('class_active_not_paid_days_10', ($args['road_dif_exact'] == 10) ? 'active' : '');
        temp_var('class_active_not_paid_days_15', ($args['road_dif_exact'] == 15) ? 'active' : '');
        temp_var('class_active_not_paid_days_25', ($args['road_dif_exact'] == 25) ? 'active' : '');
        temp_var('block_not_paid_days', template('admin/orders/block_not_paid_days'));
    }
    $pagination = page_numbers($rows, $var['page']['per_page'], getvar('p'),
        '/?page=admin/orders&status='.getvar('status').'&source='.getvar('source').
        '&search='.get_or_post('search').'&sms='.getvar('sms').'&country='.getvar('country').'&item_id='.getvar('item_id').
        '&zipper='.getvar('zipper').'&road_dif='.getvar('road_dif').'&road_dif_exact='.getvar('road_dif_exact').'&paid_manager='.getvar('paid_manager').
        '&paid_caller='.getvar('paid_caller').'&advertiser='.getvar('advertiser').'&shave='.getvar('shave').'&p=',
        5, '', '&laquo;', '&raquo;', 'yes', 'active', 'disabled', 1, '', 'yes', 'yes');
    temp_var('i_called', $link['i_called']);
    temp_var('list', $list);
    temp_var('pagination', $pagination);
    temp_var('q', count($arr));
    $content = template($args['template_block']);

    return $content;
    // alert(pre_return($arr));
}

/*
function admin_orders_print() {
    return admin_orders(array(
        'template_block'=>'admin/orders/print/block',
        'template_order'=>'admin/orders/print/order'
        ));
}

function admin_orders_print_old() {
    if (empty($_GET['status'])) {$_GET['status']='';};
    if (empty($_GET['source'])) {$_GET['source']='';};
    if (empty($_GET['search'])) {$_GET['search']='';};
    $var['status']=check_status($_GET['status']);
    $var['source']=check_source($_GET['source']);
    $var['search']=check_order_search($_GET['search']);
    $var['country']=check_country(isset($_GET['country'])?$_GET['country']:'');
    $data=query('select orders.id,orders.date,orders.utm_source,orders.phone,orders.name,orders.zip,orders.address,orders.info
     from '.tabname('shop','orders').'
     left join '.tabname('shop','kazpost_api').' on kazpost_api.track=orders.track
        where '.$var['status']['where'].' and '.$var['source']['where'].' and '.$var['search']['where'].'
        and '.$var['country']['where'].'
        order by date asc;');
    $arr=readdata($data,'nokey');
    $list='';
    foreach ($arr as $v) {
        $order_info=preparing_order_info(array('order_id'=>$v['id']));
        $show_cost['rur']=empty($order_info['cost_rur'])?'':$order_info['cost_rur'].'&nbsp;���.';
        $show_cost['kzt']=empty($order_info['cost_kzt'])?'':$order_info['cost_kzt'].'&nbsp;��.';
        $items_list=order_items_list(array('order_id'=>$v['id']));
        $list.='<tr><td>'.$v['id'].'</td><td>'.fix_encode($v['name']).'</td><td>'.$v['phone'].'</td>
        <td>'.$v['zip'].'</td><td>'.$v['address'].'</td><td>'.nl2br($v['info']).'</td>
        <td><ul class="list-unstyled">'.$items_list.'</ul></td>
        <td>'.$show_cost['rur'].'<br/>'.$show_cost['kzt'].'</td>
        </tr>';
    };
    $content='
    <table class="table table-striped orders">
    <thead>
        <tr>
            <th>ID</th><th>���</th><th>�������</th><th>������</th><th>�����</th><th>�����������</th><th>������</th><th>�����</th>
        </tr>
    </thead>
    '.$list.'
    </table>
    ';
    return $content;
}

function admin_orders_blank() {
    return preparing_admin_orders_blank(array('template'=>'admin/orders_blank_temp'));
}
*/

/*
function admin_orders_too_1() {
    return preparing_admin_orders_blank(array('template'=>'admin/kazpost/too_1_temp'));
}

function admin_orders_too_2() {
    return preparing_admin_orders_blank(array('template'=>'admin/kazpost/too_2_temp'));
}

// ������� ���� ��� ��
function admin_orders_too_f5_old() {
    $kazpost_current_too=value_from_config('shop','kazpost_current_too');
    return preparing_admin_orders_blank(array('template'=>'admin/kazpost/too_f5_temp_'.$kazpost_current_too));
}
*/

function admin_orders_regular()
{
    // $too=value_from_config('shop','kazpost_regular_too');
    $too = empty($_GET['too']) ? error('�� �������� ���') : check_input($_GET['too']);

    return admin_orders([
        'template_block' => 'admin/kazpost/regular/block',
        'template_order' => 'admin/kazpost/regular/temp_'.$too,
        'no_limit' => 1,
        'print' => 1,
        'status' => 'all',
    ]);
}

function admin_orders_ems()
{
    // $kazpost_current_too=value_from_config('shop','kazpost_ems_too');
    $too = empty($_GET['too']) ? error('�� �������� ���') : check_input($_GET['too']);

    return admin_orders([
        'template_block' => 'admin/kazpost/ems/block',
        'template_order' => 'admin/kazpost/ems/temp_'.$too,
        'no_limit' => 1,
        'print' => 1,
        'status' => 'all',
    ]);
}

function admin_orders_registry()
{
    // $kazpost_current_too=value_from_config('shop','kazpost_current_too');
    $too = empty($_GET['too']) ? error('�� �������� ���') : check_input($_GET['too']);

    return admin_orders([
        'template_block' => 'admin/kazpost/registry/block_'.$too,
        'template_order' => 'admin/kazpost/registry/temp',
        'no_limit' => 1,
        'print' => 1,
        'status' => 'all',
    ]);
}

function bom_str()
{
    return pack('CCC', 0xEF, 0xBB, 0xBF);
}

function admin_registry_csv_header()
{
    header('Content-Type: text/csv; charset: utf-8');
    header('Content-Disposition: attachment; filename="registry.csv"');
}

function admin_orders_registry_csv()
{
    // $kazpost_current_too=value_from_config('shop','kazpost_current_too');
    $too = empty($_GET['too']) ? error('�� �������� ���') : check_input($_GET['too']);

    return admin_orders([
        'template_block' => 'admin/kazpost/registry/block_'.$too.'_csv',
        'template_order' => 'admin/kazpost/registry/temp_csv',
        'no_limit' => 1,
        'print' => 1,
        'status' => 'all',
    ]);
}

function check_order_page($page)
{
    if (empty($page)) {
        $page = 1;
    }
    $arr['value'] = check_input($page);
    $arr['per_page'] = 50;
    $from = $arr['per_page'] * ($page - 1);
    $arr['limit'] = 'limit '.$from.','.$arr['per_page'];

    return $arr;
}

function check_order_search($search)
{
    // pre($search.' '.mb_detect_encoding($search));
    // pre($search);
    // $search=auto_encode($search,'windows-1251');
    // pre($search);
    $search = check_input($search);
    // pre($search);
    if ($search == '') {
        $arr['where'] = '1';
    } else {
        if (preg_match('/^\#([0-9]+)$/', $search, $args)) {
            $arr['where'] = 'orders.id='.$args[1];
        } else {
            // ���� ������������ ������� concat �� ��� ����� null ������������ null
            $arr['where'] = 'concat_ws(\'\',orders.id,orders.track,orders.name,orders.phone,orders.zip,orders.address,orders.info,orders.ip,kazpost_api.zip) like \'%'.mysql_real_escape_string($search).'%\'';
        }
    }

    return $arr;
}

function check_order_item_id($item_id)
{
    if (empty($item_id)) {
        $item_id = 'all';
    }
    $arr['value'] = $item_id;
    if ($item_id == 'all') {
        $arr['where'] = '1';
        $arr['join'] = '';
    }
    if (empty($arr['where'])) {
        $arr['join'] = 'join '.tabname('shop', 'packages').' on packages.order_id=orders.id';
        $arr['where'] = 'packages.item_id=\''.$arr['value'].'\'';
    }

    return $arr;
}

function check_order_id($order_id)
{
    if (empty($order_id)) {
        $order_id = 'all';
    }
    $arr['value'] = $order_id;
    if ($order_id == 'all') {
        $arr['where'] = '1';
    }
    if (empty($arr['where'])) {
        $arr['where'] = 'orders.id=\''.$arr['value'].'\'';
    }

    return $arr;
}

function check_level()
{
    // $login_info=preparing_user_info(array('login'=>$_SESSION['shop']['auth']['login'],'type'=>'shop'));
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'shop']), 'type' => 'shop'], 0);
    $arr['where'] = '1=2';
    if (in_array($login_info['level'], ['advertiser'])) {
        $arr['where'] = 'orders.advertiser=\''.$login_info['login'].'\'';
    }
    if (in_array($login_info['level'], ['packer_lite'])) {
        $arr['where'] = 'orders.advertiser in (\'kma\')';
    }
    if (in_array($login_info['level'], ['admin', 'manager', 'caller', 'packer', 'worker'])) {
        $arr['where'] = '1';
    }

    // if (empty($arr)) {$arr['where']='1';};
    return $arr;
}

function check_source($source)
{
    if (empty($source)) {
        $source = 'all';
    }
    $arr['value'] = $source;
    $arr['link_value'] = $source;
    if ($source == 'no') {
        // $arr['value']='no';
        $arr['where'] = '(utm_source=\'\' or utm_source is null)';
    }
    if ($source == 'all') {
        $arr['where'] = '1';
    }
    $arr['value_with_spaces'] = with_spaces($arr['value']);
    if (empty($arr['where'])) {
        $arr['where'] = 'utm_source=\''.$arr['value_with_spaces'].'\'';
    }

    return $arr;
}

function check_advertiser($advertiser)
{
    if (empty($advertiser)) {
        $advertiser = 'all';
    }
    $arr['value'] = $advertiser;
    if ($advertiser == 'no') {
        $arr['value'] = 'no';
        $arr['where'] = 'advertiser is null';
    }
    if ($advertiser == 'all') {
        $arr['where'] = '1';
    }
    if (empty($arr['where'])) {
        $arr['where'] = 'advertiser=\''.$arr['value'].'\'';
    }

    return $arr;
}

function check_status($status)
{
    if (empty($status)) {
        $status = 'new';
    }
    $arr['value'] = $status;
    $arr['value_with_spaces'] = with_spaces($status);
    $arr['where'] = 'orders.status=\''.$arr['value_with_spaces'].'\'';
    if ($status == 'all') {
        $arr['where'] = '1';
    }

    return $arr;
}

function check_country($country)
{
    if (empty($country)) {
        $country = '���������';
    }
    $arr['value'] = $country;
    $arr['where'] = 'orders.country=\''.$country.'\'';
    if ($country == 'all') {
        $arr['where'] = '1';
    }

    return $arr;
}

function check_shave($shave = 'all')
{
    if ($shave === '') {
        $shave = 'all';
    }
    $arr['value'] = $shave;
    $arr['where'] = 'orders.shave';
    if ((int) $shave === 0) {
        $arr['where'] = 'not orders.shave';
    }
    if ($shave == 'all') {
        $arr['where'] = '1';
    }

    return $arr;
}

function check_print($print = 'all')
{
    if ($print === '') {
        $print = 'all';
    }
    $arr['value'] = $print;
    $arr['where'] = 'orders.print';
    if ((int) $print === 0) {
        $arr['where'] = 'not orders.print';
    }
    if ($print == 'all') {
        $arr['where'] = '1';
    }

    return $arr;
}

function check_getsms($sms)
{
    if ($sms == '') {
        $sms = 2;
    }
    $arr['value'] = $sms;
    if ($sms == 0) {
        $arr['having'] = 'having sms_ok=0';
    }
    if ($sms == 1) {
        $arr['having'] = 'having sms_ok>0';
    }
    // ���
    if ($sms == 2) {
        $arr['having'] = '';
    }

    return $arr;
}

function check_paid_caller($login = '')
{
    $arr['value'] = $login;
    $arr['where'] = empty($login) ? '1' : '(orders.login=\''.$login.'\')';

    return $arr;
}

function admin_order_statuses()
{
    $var['status'] = check_status(isset($_GET['status']) ? $_GET['status'] : '');
    $var['source'] = check_source(isset($_GET['source']) ? $_GET['source'] : '');
    $var['advertiser'] = check_advertiser(isset($_GET['advertiser']) ? $_GET['advertiser'] : '');
    $var['country'] = check_country(isset($_GET['country']) ? $_GET['country'] : '');
    $var['item_id'] = check_order_item_id(isset($_GET['item_id']) ? $_GET['item_id'] : '');
    $var['level'] = check_level();
    $var['paid_caller'] = check_paid_caller(isset($_GET['paid_caller']) ? $_GET['paid_caller'] : '');
    $var['shave'] = check_shave(isset($_GET['shave']) ? $_GET['shave'] : '');
    $rus['status'] = enum_to_array(tabname('shop', 'orders'), 'status', shop_translation('order_status'));
    // ����� �� ������ all � ����������, ����� ��� ���������� ������� �����
    $statuses_only_manager = ['paid', 'take payment', 'refund', 'return', 'refund paid', 'return paid'];
    // $login_info=preparing_user_info(array('login'=>$_SESSION['shop']['auth']['login'],'type'=>'shop'));
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'shop']), 'type' => 'shop'], 0);
    // ���� ������ � ������� ������ ��� ���������� �� ��������� authorizate_shop_manager
    if (in_array($var['status']['value_with_spaces'], $statuses_only_manager)) {
        authorizate_shop_manager();
    }
    $arr = query_arr('select status,count(*) as q
		from '.tabname('shop', 'orders').'
		join '.typetab('statuses').' on statuses.name=orders.status
		'.$var['item_id']['join'].'
		where '.$var['source']['where'].' and '.$var['advertiser']['where'].' and '.$var['country']['where'].' and '.$var['item_id']['where'].'
		and '.$var['level']['where'].' and '.$var['paid_caller']['where'].' and '.$var['shave']['where'].'
		group by status
		order by statuses.position
		;');
    $arr_all = query_assoc('select count(*) as q from '.tabname('shop', 'orders').'
		'.$var['item_id']['join'].'
		where '.$var['source']['where'].' and '.$var['advertiser']['where'].' and '.$var['country']['where'].' and '.$var['item_id']['where'].'
		and '.$var['level']['where'].' and '.$var['paid_caller']['where'].' and '.$var['shave']['where'].'
		;');
    if ($var['status']['value_with_spaces'] == 'all') {
        $all_class = ' class="active"';
    } else {
        $all_class = '';
    }
    $list = '';
    // ���������� ������ all ������ ������� � ����������
    if (in_array($login_info['level'], ['admin', 'manager'])) {
        $list .= '<li role="presentation" '.$all_class.'>
		<a href="/?page=admin/orders&amp;advertiser='.$var['advertiser']['value'].'&amp;source='.$var['source']['link_value'].'&amp;status=all&country='.
        $var['country']['value'].'&item_id='.$var['item_id']['value'].'&road_dif='.getvar('road_dif').'&paid_caller='.getvar('paid_caller').'&shave='.getvar('shave').'">
		��� <span class="label label-default">'.$arr_all['q'].'</span></a>
		</li>';
    }
    foreach ($arr as $v) {
        // ���� level �� admin � �� manager �� �� ���������� ������
        if (in_array($v['status'], $statuses_only_manager)) {
            if (! in_array($login_info['level'], ['admin', 'manager'])) {
                continue;
            }
        }
        // ���� packe_lite � ������� �� ������, �� ����������
        if (in_array($login_info['level'], ['packer_lite']) and in_array($v['status'], ['delivered not paid', 'sent', 'new', 'unavailable', 'check address'])) {
            continue;
        }
        if ($v['status'] == $var['status']['value_with_spaces']) {
            $class = ' class="active"';
        } else {
            $class = '';
        }
        $class_label = 'primary';
        if (in_array($v['status'], ['take payment', 'paid'])) {
            $class_label = 'success';
        }
        if (in_array($v['status'], ['delivered not paid'])) {
            $class_label = 'info';
        }
        if (in_array($v['status'], ['return', 'refund', 'return paid', 'refund paid'])) {
            $class_label = 'danger';
        }
        if (in_array($v['status'], ['canceled'])) {
            $class_label = 'default';
        }
        if (in_array($v['status'], ['unavailable'])) {
            $class_label = 'warning';
        }
        $list .= '<li role="presentation"'.$class.'>
		<a href="/?page=admin/orders&amp;advertiser='.$var['advertiser']['value'].'&amp;source='.$var['source']['link_value'].'&amp;status='.
        without_spaces($v['status']).'&country='.$var['country']['value'].'&item_id='.
        $var['item_id']['value'].'&road_dif='.getvar('road_dif').'&paid_caller='.getvar('paid_caller').'&shave='.getvar('shave').'">
		'.$rus['status'][$v['status']].' <span class="label label-'.$class_label.'">'.$v['q'].'</span>
		</a></li>';
    }

    return $list;
}

function admin_order_countries()
{
    $var['status'] = check_status(isset($_GET['status']) ? $_GET['status'] : '');
    $var['source'] = check_source(isset($_GET['source']) ? $_GET['source'] : '');
    $var['country'] = check_country(isset($_GET['country']) ? $_GET['country'] : '');
    $arr = query_arr('select status,count(*) as q from '.tabname('shop', 'orders').'
		where '.$var['source']['where'].' and '.$var['country']['where'].' group by status;');
}

function get_current_source()
{
    $source = isset($_GET['source']) ? $_GET['source'] : '';
    $arr = check_source($source);

    return '<b>'.$arr['value'].'</b> ';
}

function admin_block_order_sources()
{
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'shop']), 'type' => 'shop'], 0);
    if (in_array($login_info['level'], ['admin', 'manager', 'advertiser'])) {
        return template('admin/orders_sources');
    }
}

function admin_order_sources()
{
    $source = empty($_GET['source']) ? '' : $_GET['source'];
    $status = empty($_GET['status']) ? '' : $_GET['status'];
    $advertiser = empty($_GET['advertiser']) ? 'no' : $_GET['advertiser'];
    $country = isset($_GET['country']) ? $_GET['country'] : '';
    $shave = isset($_GET['shave']) ? $_GET['shave'] : '';
    $var['status'] = check_status($status);
    $var['source'] = check_source($source);
    $var['advertiser'] = check_advertiser($advertiser);
    // pre($var);
    $var['country'] = check_country($country);
    $var['level'] = check_level();
    $var['shave'] = check_shave($shave);
    $arr = cache_query_arr('select utm_source as source,count(*) as q from '.tabname('shop', 'orders').'
		where '.$var['country']['where'].' and '.$var['level']['where'].' and '.$var['advertiser']['where'].'
		and '.$var['shave']['where'].'
		group by utm_source order by utm_source;');
    // var_dump($arr);
    $list = '';
    foreach ($arr as $v) {
        if ($v['source'] == $var['source']['value_with_spaces']) {
            $class = ' class="active"';
        } else {
            $class = '';
        }
        if ($v['source'] == '') {
            $source_name = 'no';
        } else {
            $source_name = $v['source'];
        }
        $list .= '<li role="presentation"'.$class.'>
		<a href="/?page=admin/orders&amp;status='.without_spaces($var['status']['value']).'&amp;source='.$source_name.'&advertiser='.$var['advertiser']['value'].'&amp;country='.$var['country']['value'].'&item_id='.getvar('item_id').'">
		'.$source_name.' <span class="badge">'.$v['q'].'</span>
		</a></li>';
    }

    return $list;
}

function get_current_advertiser()
{
    $advertiser = isset($_GET['advertiser']) ? $_GET['advertiser'] : '';
    $arr = check_advertiser($advertiser);

    // pre(var_dump($advertiser));
    return '<b>'.$arr['value'].'</b> ';
}

function admin_block_order_advertisers()
{
    $login_info = cache('preparing_user_info', ['login' => auth_login(['type' => 'shop']), 'type' => 'shop'], 0);
    if (in_array($login_info['level'], ['admin', 'manager', 'advertiser'])) {
        return template('admin/orders_advertisers');
    }
}

function admin_order_advertisers()
{
    if (empty($_GET['status'])) {
        $_GET['status'] = '';
    }
    if (empty($_GET['advertiser'])) {
        $_GET['advertiser'] = '';
    }
    if (empty($_GET['source'])) {
        $_GET['source'] = '';
    }
    $var['status'] = check_status($_GET['status']);
    $var['advertiser'] = check_advertiser($_GET['advertiser']);
    $var['source'] = check_source($_GET['source']);
    $var['country'] = check_country(isset($_GET['country']) ? $_GET['country'] : '');
    $var['level'] = check_level();
    $var['shave'] = check_shave(isset($_GET['shave']) ? $_GET['shave'] : '');
    $arr = cache_query_arr('select ifnull(advertiser,\'\') as advertiser,count(*) as q from '.tabname('shop', 'orders').'
		where '.$var['country']['where'].' and '.$var['level']['where'].' and '.$var['source']['where'].'
		and '.$var['shave']['where'].'
		group by advertiser order by advertiser;');
    // pre(var_dump($arr));
    // var_dump($arr);
    $list = '';
    foreach ($arr as $v) {
        if ($v['advertiser'] == $var['advertiser']['value']) {
            $class = ' class="active"';
        } else {
            $class = '';
        }
        if (empty($v['advertiser'])) {
            $advertiser_name = 'no';
        } else {
            $advertiser_name = $v['advertiser'];
        }
        $list .= '<li role="presentation"'.$class.'>
		<a href="/?page=admin/orders&amp;status='.without_spaces($var['status']['value']).'&amp;source='.$var['source']['value_with_spaces'].'&amp;advertiser='.$advertiser_name.'&country='.$var['country']['value'].'&item_id='.getvar('item_id').'">
		'.$advertiser_name.' <span class="badge">'.$v['q'].'</span>
		</a></li>';
    }

    return $list;
}
