<?php

namespace MegawebV1;

// ������ ��� ������, ������������� ������� ���� ��� ������� (������� ��������, ����� ���������� ������ ��������������)
function cpa_stark_script_cancel()
{
    $arr = query_arr('select SQL_CALC_FOUND_ROWS orders.id
		from '.typetab('orders').'
		where advertiser=\'stark\'
		/* and status not in (\'new\',\'unavailable\',\'canceled\') */
		and not postback
		limit 10
		;');
    $q = rows_without_limit();
    $list = '';
    foreach ($arr as $v) {
        order_cpa(['order_id' => $v['id'], 'step' => 'cancel_after_confirm']);
        $list .= '<li>'.$v['id'].'</li>';
    }
    $content = '�����: '.$q.'<ol>'.$list.'</ol>';

    return $content;
}

// ���������� ����� ������������� ������ �� utm_source
function return_advertiser_by_utm_source($utm_source = '')
{
    $arr = query_assoc('select users.login from '.typetab('advertiser_by_source').'
		join '.typetab('users').' on users.login=advertiser_by_source.login
		where source=\''.check_input($utm_source).'\' and users.level=\'advertiser\';');

    // pre($arr);
    return (empty($arr)) ? false : $arr['login'];
}

function return_vars_of_advertiser($advertiser)
{
    $arr = query_arr('select var,type from '.typetab('advertisers_vars').' where login=\''.$advertiser.'\';');
    $vars = [];
    foreach ($arr as $v) {
        $var = isset($_COOKIE[$v['var']]) ? $_COOKIE[$v['var']] : (isset($_POST[$v['var']]) ? $_POST[$v['var']] : false);
        // if (isset($_COOKIE[$v['var']])) {
        if ($var) {
            $vars[$v['var']] = check_input($var);
        } else {
            // if (isset($_GET[$v['var']])) {
            // $vars[$v['var']]=check_input($_GET['mg_user_id']);
            // } else {
            if ($v['type'] == 'required') {
                $vars[$v['var']] = '';
                logger()->info('�� �������� ������������ ���������� '.$v['var']);
                // error('�� �������� ������������ ���������� '.$v['var']);
            }
            // };
        }
    }

    return $vars;
}

// ���������� ����� ������������� �� uid
function return_advertiser_by_uid($uid = '')
{
    $arr = query_assoc('select login from '.typetab('users').' where id=\''.$uid.'\' and level=\'advertiser\';');

    return (empty($arr)) ? false : $arr['login'];
}

// ���������� ������������� �� ��������� ��� ������
function return_advertiser_by_domain($domain)
{
    $arr = query_assoc('select domain_config.default_advertiser
		from '.typetab('domain_config').'
		join '.typetab('users').' on users.login=domain_config.default_advertiser
		where domain_config.domain=\''.$domain.'\' and users.level=\'advertiser\';');

    return (empty($arr)) ? false : $arr['default_advertiser'];
}

function return_advertiser_common()
{
    $advertiser = false;
    // post ���� ��������� � api/new-order ��������, get - ����� set_cookies_from_get ���������� ����� �� get
    $uid = check_input(empty($_COOKIE['uid']) ? (empty($_POST['uid']) ? (empty($_GET['uid']) ? '' : $_GET['uid']) : $_POST['uid']) : $_COOKIE['uid']);
    $utm_source = check_input(empty($_COOKIE['utm_source']) ? (empty($_POST['utm_source']) ? (empty($_GET['utm_source']) ? '' : $_GET['utm_source']) : $_POST['utm_source']) : $_COOKIE['utm_source']);
    if (! $advertiser) {
        $advertiser = return_advertiser_by_uid($uid);
    }
    if (! $advertiser) {
        $advertiser = return_advertiser_by_utm_source($utm_source);
    }
    if (! $advertiser) {
        global $config;
        $advertiser = return_advertiser_by_domain($config['domain']);
    }

    // var_dump($advertiser);
    // pre($advertiser);
    return $advertiser;
}

/*
//������� ���� �� ����������� ������ (��������� sub_id � ����������)
function cpa_set_sub_id_to_vars() {
    $arr=query_arr('select id,kadam_cpa from '.typetab('orders_deleted').' where kadam_cpa!=\'\' and kadam_cpa is not null;');
    $i=0;
    foreach ($arr as $v) {
        $i++;
        $vars=array('kadam_cpa'=>$v['kadam_cpa']);
        query('update '.typetab('orders_deleted').' set vars=\''.mysql_real_escape_string(serialize($vars)).'\'
            where id=\''.$v['id'].'\' and advertiser is not null
            ;');
    };
    return '������ '.$i;
}
*/

// ��� new - ����������� ������� ��������� � ������������, ��������� �� ����� � ����� �� CPA
// ���� ��� �� new, �� ���������, ���� �� ���� ����� � ���� CPA, ���� ����, �� ��������� ��� ���
// ���������, ���� ��� ���� ����� ���� paid_action, �� �������� URL �� ������� ��� � ������� ���.
function order_cpa($args)
{
    $step = empty($args['step']) ? error('�� ������� ���') : $args['step'];
    if (! in_array($step, ['new', 'confirm', 'cancel', 'cancel_after_confirm', 'paid'])) {
        logger()->info('CPA: �������������� ��� "'.$step.'"');
    }
    $order_id = empty($args['order_id']) ? error('�� ������� ID ������') : $args['order_id'];
    if ($step == 'new') {
        $advertiser = return_advertiser_common();
        // var_dump($advertiser); pre('');
        // ���� ��� �������������, �� ���������
        if (! $advertiser) {
            return;
        }
        $vars = return_vars_of_advertiser($advertiser);
        // pre($vars);
        query('update '.typetab('orders').' set advertiser=\''.$advertiser.'\',
			vars=\''.mysql_real_escape_string(serialize($vars)).'\'
			where id=\''.$order_id.'\'
			;');
    }
    // �������� ����������� ���� �� ������
    $arr = query_assoc('select
		orders.id,orders.advertiser,orders.ip,orders.vars,orders.status,orders.user_status,orders.info,orders.user_info,
		users.url,users.pb_new,users.pb_confirm,users.pb_cancel,users.pb_cancel_after_confirm,pb_paid,users.pb_check_ip,
		orders.bonus as profit,
		(if(orders.status not in (\'new\',\'unavailable\',\'canceled\'),orders.bonus,0)) as profit_confirmed
		from '.typetab('orders').'
		/*
		join '.typetab('packages').' on packages.order_id=orders.id and packages.item_id=orders.main_item
		left join '.typetab('bonuses').' on bonuses.advertiser=orders.advertiser and bonuses.item_id=orders.main_item
		*/
		join '.typetab('users').' on users.login=orders.advertiser and users.level=\'advertiser\'
		where orders.id=\''.$order_id.'\';');
    /*
    if ($arr['advertiser']=='stark') {
        if ($step=='confirm') {
            if (random_event(array('shave'=>100,'ok'=>0))=='shave') {
                $step='cancel';
                // $arr['']
                query('update '.typetab('orders').' set shave=1, user_status=\'canceled\' where id=\''.$order_id.'\';');
            };
        };
    };
    */
    // var_dump($arr);
    // pre($arr);
    // ���� ������ �����, �� ���� ����� �� �� ��, ����������
    if (empty($arr)) {
        logger()->info('CPA: ����������, ��� ������������� ('.$order_id.','.$step.')');

        return;
    }
    // ���� � ���������� ������� �� ��������� ������� ��� ����� ��������, �� ������
    if (! $arr['pb_'.$step]) {
        logger()->info('CPA: ����������, ������� �� ����� ��� ����� �������� ('.$arr['advertiser'].','.$order_id.','.$step.')');

        return;
    }
    // ���� � ���������� ������� ��������� �������� �� IP, �� ���������
    if ($arr['pb_check_ip']) {
        // pre('ff');
        // �������� ���� �� ������� ����������� ���� � ���� �� ����
        $arr2 = query_assoc('select orders.id
			from '.typetab('orders').'
			where orders.ip=\''.$arr['ip'].'\'
			and (orders.date between (current_timestamp - interval 1 day) and current_timestamp)
			and orders.postback
			limit 1
			;');
        // pre($arr2);
        // ���� ����, �� �� ������� ������
        if (! empty($arr2['id'])) {
            logger()->info('CPA: �� ���������� postback, �.�. ����� IP ('.$arr['advertiser'].','.$order_id.','.$step.')');

            return;
        }
    }
    // �������������� ����������� ���������� ��� ������
    temp_var('order_id', $arr['id']);
    temp_var('order_info', urlencode($arr['info']));
    temp_var('order_info_utf8', urlencode(mb_convert_encoding($arr['info'], 'utf-8', 'windows-1251')));
    temp_var('profit', $arr['profit']);
    temp_var('profit_confirmed', $arr['profit_confirmed']);
    temp_var('status_proffi', proffi_status_format($arr['status']));
    temp_var('status_digital', digital_status_format($arr['status']));
    // temp_var('status_kma',kma_status_format($arr['status']));
    // temp_var('user_sub_id',$arr['user_sub_id']);
    // ���������� �� ��������� ���������� ��� ���������������� ���������� ������ � ��������� user_
    if (isset($arr['vars'])) {
        $vars = unserialize($arr['vars']);
        foreach ($vars as $k => $v) {
            temp_var('user_'.$k, $v);
        }
    }
    // global $config;
    // pre($config);
    if (empty($arr['url'])) {
        logger()->info('CPA: ��� ������ ��� �������� ('.$arr['advertiser'].','.$order_id.','.$step.')');

        return;
    }
    $url = template($arr['url'], 1);
    query('update '.typetab('orders').' set postback=1 where id=\''.$order_id.'\';');
    logger()->info('CPA: ������� �� ������: '.$url.' ('.$arr['advertiser'].','.$order_id.','.$step.')');
    file_get_contents($url);
    // pre($url);
}

// pre(urlencode(mb_convert_encoding('���� ����','utf-8')));

function proffi_orders_csv()
{
    set_timezone('+3');
    $arr = query_arr('select date,name,
		case user_status
		when \'unavailable\' then 1
		when \'new\' then 1
		when \'canceled\' then 4
		else 5 end as user_status,
			user_info
		from '.typetab('orders').' where utm_source in (\'proffi\')
		order by date
		;');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['date'].'</td><td>'.$v['name'].'</td><td>'.$v['user_status'].'</td><td>'.$v['user_info'].'</td></tr>';
    }

    return $list;
}

function proffi_status_format($our_status)
{
    if (empty($our_status)) {
        error('�� ������� ������');
    }
    if (in_array($our_status, ['new', 'unavailable'])) {
        $status = 1;
    }
    if (in_array($our_status, ['canceled'])) {
        $status = 4;
    }
    if (! in_array($our_status, ['new', 'unavailable', 'canceled'])) {
        $status = 5;
    }

    return $status;
}

function kma_status_format($our_status)
{
    if (empty($our_status)) {
        error('�� ������� ������');
    }
    if (in_array($our_status, ['new', 'unavailable'])) {
        $status = 0;
    }
    if (in_array($our_status, ['canceled'])) {
        $status = -1;
    }
    if (! in_array($our_status, ['new', 'unavailable', 'canceled'])) {
        $status = 1;
    }
    if (in_array($our_status, ['paid', 'take payment'])) {
        $status = 2;
    }
    if (in_array($our_status, ['return', 'return paid', 'refund', 'refund paid'])) {
        $status = 3;
    }

    return $status;
}

function digital_status_format($our_status)
{
    if (empty($our_status)) {
        error('�� ������� ������');
    }
    if (in_array($our_status, ['new', 'unavailable'])) {
        $status = 2;
    }
    if (in_array($our_status, ['canceled'])) {
        $status = 3;
    }
    if (! in_array($our_status, ['new', 'unavailable', 'canceled'])) {
        $status = 1;
    }

    return $status;
}

function digital_orders_csv()
{
    set_timezone('+3');
    $arr = query_arr('select id,date,user_status,user_info
		from '.typetab('orders').' where advertiser=\'digital\' /* and user_status not in (\'canceled\',\'new\',\'unavailable\') */
		order by date
		;');
    $list = '';
    $status = 0;
    foreach ($arr as $v) {
        $status = digital_status_format($v['user_status']);
        $list .= '<tr><td>'.$v['date'].'</td><td>'.$v['id'].'</td><td>'.$status.'</td><td>'.$v['user_info'].'</td></tr>';
    }

    return $list;
}

function kma_orders_csv()
{
    set_timezone('+3');
    $arr = query_arr('select id,date,name,user_status,user_info
		from '.typetab('orders').' where utm_source in (\'kma\') and user_status not in (\'canceled\',\'new\',\'unavailable\')
		order by date
		;');
    $list = '';
    $status = 0;
    foreach ($arr as $v) {
        $status = kma_status_format($v['user_status']);
        $list .= '<tr><td>'.$v['date'].'</td><td>'.$v['id'].'</td><td>'.$v['name'].'</td><td>'.$status.'</td><td>'.$v['user_info'].'</td></tr>';
    }

    return $list;
}

function proffi_submit_order()
{
    global $config;
    $result = [];
    $error = false;
    if (empty($_POST['item_id'])) {
        $error = '�� ������� item_id';
    }
    if (empty($_POST['phone'])) {
        $error = '�� ������� phone';
    }
    if (empty($_POST['utm_source'])) {
        $error = '�� ������� utm_source';
    }
    // $_POST['utm_source']='kma';
    if ($error) {
        $result['result'] = false;
        // $result['msg']=iconv('windows-1251','utf-8',$error);
        // return json_encode(array('result'=>false,'msg'=>$error));
    } else {
        $result['result'] = $config['temp_vars']['created_order_id'];
        // $result['orderid']=$config['temp_vars']['created_order_id'];
        // return json_encode(array('result'=>true,'orderid'=>$config['temp_vars']['created_order_id']));
    }

    // pre($result);
    return json_encode($result);
}

function kma_submit_order()
{
    global $config;
    $result = [];
    $error = false;
    if (empty($_POST['item_id'])) {
        $error = '�� ������� item_id';
    }
    if (empty($_POST['phone'])) {
        $error = '�� ������� phone';
    }
    if (empty($_POST['utm_source'])) {
        $error = '�� ������� utm_source';
    }
    // $_POST['utm_source']='kma';
    if ($error) {
        $result['result'] = false;
        $result['msg'] = iconv('windows-1251', 'utf-8', $error);
        // return json_encode(array('result'=>false,'msg'=>$error));
    } else {
        $result['result'] = true;
        $result['orderid'] = $config['temp_vars']['created_order_id'];
        // return json_encode(array('result'=>true,'orderid'=>$config['temp_vars']['created_order_id']));
    }

    // pre($result);
    return json_encode($result);
}

// ������ ������� POST[ids]=1,2,3,4,5
function kma_get_status()
{
    // $_POST['ids']='16106,16107,16108';
    if (empty($_GET['ids'])) {
        $ids = empty($_POST['ids']) ? error('�� �������� ID �������') : $_POST['ids'];
    } else {
        // ��� �����
        $ids = $_GET['ids'];
    }
    $arr_ids = explode(',', $ids);
    $arr = query_arr('select id,user_info,user_status from '.typetab('orders').'
		where id in ('.array_to_str($arr_ids).') and advertiser=\'kma\'
		;');
    // pre($arr);
    $answer = [];
    foreach ($arr as $v) {
        // ���� ���������� ���������������� ������ ���� �� ������ � shave=1
        // if ($v['shave'] and !empty($v['user_status'])) {$v['status']=$v['user_status'];};
        $status = 0;
        $status = kma_status_format($v['user_status']);
        $return['status'] = $status;
        $return['id'] = $v['id'];
        $return['comment'] = iconv('windows-1251', 'utf-8', $v['user_info']);
        // �� �������� ��� ������ ������, �.�. � ��� �� �� ����� ����� ����� �������� �� �����
        if (in_array($status, [1])) {
            unset($return['status']);
        }
        $answer[] = $return;
    }

    // pre($answer);
    // ����� ������� ����� �� ������������
    // return json_encode($answer,JSON_UNESCAPED_UNICODE);
    return json_encode($answer);
}
