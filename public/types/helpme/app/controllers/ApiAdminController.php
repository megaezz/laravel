<?php

namespace helpme\app\controllers;

use engine\app\models\F;
use helpme\app\models\eloquent\ModulkassaReceipt;
use helpme\app\models\eloquent\User;

class ApiAdminController
{
    protected $auth = null;

    protected $user = null;

    public function __construct()
    {
        header('Content-Type: application/json');
        $this->auth = new HelpmeAuthorizationController(['root', 'admin']);
        $this->user = User::find($this->auth->getLogin());
    }

    public function addReceipts()
    {
        header('Content-Type: text/html');
        $amo_group_id = $_POST['amo_group_id'] ?? F::error('amo_group_id required');
        $file_path = empty($_FILES['csv']['tmp_name']) ? F::error('Не передан файл') : $_FILES['csv']['tmp_name'];
        $csv = fopen($file_path, 'r');
        $str = 0;
        $added = 0;
        $isRefund = false;
        while (($data = fgetcsv($csv, null, ';')) !== false) {
            $str++;
            if ($str == 1) {
                // dd($data);
                if ($data[0] == 'Отчет за период') {
                    $isRefund = true;
                }
            }
            if ($str < 4) {
                continue;
            }
            // dd($data);
            $json = json_encode($data, JSON_UNESCAPED_UNICODE);
            $id = sha1($json);
            // dd($id);
            $receipt = ModulkassaReceipt::findOrNew($id);
            $receipt->id = $id;
            $receipt->data = $json;
            $receipt->amo_group_id = $amo_group_id;
            $receipt->refund = $isRefund;
            $receipt->save();
            $added++;
        }
        fclose($csv);
        F::alert('Добавлено '.$added.' чеков');
    }
}
