<?php

namespace helpme\app\controllers;

use engine\app\controllers\AuthorizationController;

class HelpmeAuthorizationController extends AuthorizationController
{
    public function __construct($levels = [])
    {
        parent::__construct('helpme', $levels);
    }
}
