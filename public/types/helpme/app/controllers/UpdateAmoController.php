<?php

namespace helpme\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;
use helpme\app\models\Amo;
use helpme\app\models\AmoLeads;

class UpdateAmoController extends \helpme\app\controllers\ManagerController
{
    public function updateCompanies()
    {
        $path = '/api/v2/companies';
        $amo = new Amo(1);
        $companies = $amo->q($path);
        // F::dump($companies);
        // F::dump($companies->_embedded->items[0]);
        foreach ($companies->_embedded->items as $v) {
            F::query('
				insert into '.F::typetab('amo_companies').'
				set
				id = '.$v->id.',
				name = \''.$v->name.'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				on duplicate key update
				id = '.$v->id.',
				name = \''.$v->name.'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				;');
        }

        // F::alert('Компании загружены');
        return true;
    }

    public function updateStatuses()
    {
        $path = '/api/v2/pipelines';
        $amo = new Amo(1);
        // получаем все воронки
        $pipelines = $amo->q($path);
        // F::dump($pipelines);
        foreach ($pipelines->_embedded->items as $v) {
            foreach ($v->statuses as $s) {
                F::query('
				insert into '.F::typetab('amo_statuses').'
				set
				id = '.$s->id.',
				name = \''.$s->name.'\',
				pipeline_id = '.$v->id.',
				pipeline_name = \''.$v->name.'\'
				on duplicate key update
				id = '.$s->id.',
				name = \''.$s->name.'\',
				pipeline_id = '.$v->id.',
				pipeline_name = \''.$v->name.'\'
				;');
            }
        }

        // F::alert('Статусы загружены');
        return true;
    }

    public function updateGroups()
    {
        $path = '/api/v2/account?with=groups';
        $amo = new Amo(1);
        // получаем все воронки
        $account = $amo->q($path);
        // F::dump($account->_embedded->groups);
        foreach ($account->_embedded->groups as $v) {
            F::query('
				insert into '.F::typetab('amo_groups').'
				set
				id = '.$v->id.',
				name = \''.$v->name.'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				on duplicate key update
				id = '.$v->id.',
				name = \''.$v->name.'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				;');
        }

        // F::alert('Статусы загружены');
        return true;
    }

    public function updateTasks()
    {
        // получаем список лидов для которых загрузить задачу
        // $leads = new AmoLeads;
        // $leads->set
        $amoUser = empty($_GET['amoUser']) ? F::error('Не передан amoUser') : F::checkstr($_GET['amoUser']);
        $limit = 500;
        $offset = empty($_GET['offset']) ? 0 : $_GET['offset'];
        // type=2 - сделки
        $path = '/api/v2/tasks?type=2&limit_rows='.$limit.'&limit_offset='.$offset;
        // F::dump($path);
        $amo = new Amo($amoUser);
        $tasks = $amo->q($path);
        if (empty($tasks->_embedded->items)) {
            $return = 'Обновление базы завершено. <a href="/admin">Вернуться на главную</a>.';

            return false;
            F::alert($return);
            // return '<script>
            // 		setTimeout(
            // 		window.location.replace(\'/admin\'),
            // 		5000);
            // 		</script>';
        }
        $rows = count($tasks->_embedded->items);
        // F::dump($tasks->_embedded->items);
        foreach ($tasks->_embedded->items as $v) {
            $leadId = ($v->element_type === 2) ? $v->element_id : null;
            F::query('
				insert into '.F::typetab('amo_tasks').'
				set
				id = '.$v->id.',
				lead = \''.$leadId.'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\',
				created_at = from_unixtime(\''.$v->created_at.'\'),
				updated_at = from_unixtime(\''.$v->updated_at.'\'),
				complete_till_at = from_unixtime(\''.$v->complete_till_at.'\'),
				task_type = \''.$v->task_type.'\',
				text = \''.F::escape_string($v->text).'\',
				is_completed = '.($v->is_completed ? 1 : 0).',
				result = \''.(isset($v->result->text) ? json_encode($v->result->text, JSON_UNESCAPED_UNICODE) : '').'\'
				on duplicate key update
				id = '.$v->id.',
				lead = \''.$leadId.'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\',
				created_at = from_unixtime(\''.$v->created_at.'\'),
				updated_at = from_unixtime(\''.$v->updated_at.'\'),
				complete_till_at = from_unixtime(\''.$v->complete_till_at.'\'),
				task_type = \''.$v->task_type.'\',
				text = \''.F::escape_string($v->text).'\',
				is_completed = '.($v->is_completed ? 1 : 0).',
				result = \''.(isset($v->result->text) ? json_encode($v->result->text, JSON_UNESCAPED_UNICODE) : '').'\'
				;');
        }
        $offset = $offset + $limit;
        $return = 'Загружено '.$rows.' задач, далее с '.$offset.'
			<script>
			setTimeout(
			window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateTasks&amoUser='.$amoUser.'&offset='.$offset.'\'),
			5000);
			</script>';

        return $return;
        F::alert($return);
        // F::alert('Компании загружены');
        // return true;
    }

    public function updateLeads()
    {
        Engine::setDisplayErrors(true);
        ini_set('memory_limit', '800M');
        $amoUserId = empty($_GET['amoUserId']) ? null : $_GET['amoUserId'];
        if (! $amoUserId) {
            F::error('Не передан amoUserId');
            // если не передан amoUserId значит скрипт запрашивается первый раз, обнуляем нужных юзеров
            F::query('update '.F::typetab('amo_users').' set updated = 0 where id in (1,2);');
            $arr = F::query_assoc('
			select id from '.F::typetab('amo_users').' where not updated limit 1
			;');
            if (! $arr) {
                F::alert('Нет лидов для обновления');
            }
            $amoUserId = $arr['id'];
        }
        $amo = new Amo($amoUserId);
        $from = empty($_GET['from']) ? 0 : $_GET['from'];
        $limit = 250;
        // $path = '/private/api/v2/json/leads/list?filter[active]=1';
        $path = '/private/api/v2/json/leads/list?limit_rows='.$limit.'&limit_offset='.$from;
        // F::dump($path);
        // формируем дату -1  день
        $ifModifiedSince = date('D, d M Y H:i:s', (time() - 12 * 30 * 24 * 3600));
        // если передается пустой массив fields, то данные post не передаются в заголовке запроса
        $fields = [];
        // делаем запрос
        $leads = $amo->q($path, $fields, $ifModifiedSince);
        // F::Dump($leads);
        foreach ($leads->response->leads as $v) {
            F::query('
				insert into '.F::typetab('amo_leads').'
				set json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\', id = '.$v->id.'
				on duplicate key update json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				;');
        }
        // если есть еще строки, то редиректим далее
        if (count($leads->response->leads) == $limit) {
            $from = $from + $limit - 1;
            // logger()->info('Редирект на /?r=Admin/updateLeads&amoUserId='.$amoUserId.'&from='.$from);
            F::alert('Аккаунт #'.$amoUserId.'. <a href="/?r=Manager/updateLeads&amoUserId='.$amoUserId.'&from='.$from.'">Далее с '.$from.'</a>
				<script>
				setTimeout(
				window.location.replace(\'/?r=Manager/updateLeads&amoUserId='.$amoUserId.'&from='.$from.'\'),
				5000);
				</script>
				');
        } else {
            F::query('update '.F::typetab('amo_users').' set updated = 1 where id = \''.$amoUserId.'\';');
            $arr = F::query_assoc('
			select id from '.F::typetab('amo_users').' where not updated limit 1
			;');
            if ($arr) {
                F::alert('Аккаунт #'.$amoUserId.' обновлен. <a href="/?r=Manager/updateLeads&amoUserId='.$arr['id'].'">Следующий аккаунт</a>
					<script>
					setTimeout(
					window.location.replace(\'/?r=UpdateAmo/updateLeads&amoUserId='.$arr['id'].'\'),
					5000);
					</script>
					');
            } else {
                F::alert('
					Лиды для всех аккаунтов обновлены
					<script>
					setTimeout(
					window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateStatuses&amoUser='.$amoUserId.'\'),
					5000);
					</script>
					');
            }
        }
        // F::alert('Лиды обновлены');
        // F::pre($leads);
    }

    public function updateBases()
    {
        F::error('Запрещено');
        $action = empty($_GET['action']) ? F::error('Не передано действие') : F::checkstr($_GET['action']);
        $amoUser = empty($_GET['amoUser']) ? null : F::checkstr($_GET['amoUser']);
        $groupId = empty($_GET['groupId']) ? null : F::checkstr($_GET['groupId']);
        if (! $amoUser) {
            if ($groupId) {
                $arr = F::query_assoc('
					select id 
					from '.F::typetab('amo_users').' 
					where group_id = \''.$groupId.'\'
					limit 1
					;');
                $amoUser = $arr['id'] ?? null;
            } else {
                F::error('Не передан amoUser');
            }
        }
        if ($action == 'start') {
            F::alert('
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateLeads&amoUserId='.$amoUser.'\'),
				5000);
				</script>
				');
        }
        if ($action == 'updateStatuses') {
            F::alert('
				'.($this->updateStatuses() ? 'Статусы обновлены' : 'Ошибка обновления статусов').'
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateCompanies&amoUser='.$amoUser.'\'),
				5000);
				</script>
				');
        }
        if ($action == 'updateCompanies') {
            F::alert('
				'.($this->updateCompanies() ? 'Компании обновлены' : 'Ошибка обновления компаний').'
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateGroups&amoUser='.$amoUser.'\'),
				5000);
				</script>
				');
        }
        if ($action == 'updateGroups') {
            F::alert('
				'.($this->updateGroups() ? 'Группы обновлены' : 'Ошибка обновления групп').'
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateBases&action=resetLeadsCheckers&amoUser='.$amoUser.'\'),
				5000);
				</script>
				');
        }
        if ($action == 'resetLeadsCheckers') {
            F::alert('
				'.($this->resetLeadsCheckers() ? 'Чеккеры лидов сброшены' : 'Ошибка сброса чеккеров лидов').'
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateLeadsInfo&amoUser='.$amoUser.'\'),
				5000);
				</script>
				');
        }
        if ($action == 'updateLeadsInfo') {
            $result = $this->updateLeadsInfo();
            if ($result) {
                F::alert('Информация о лидах обновляется, осталось: '.$result.'
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateLeadsInfo&amoUser='.$amoUser.'\'),
				5000);
				</script>
				');
            } else {
                F::alert('Информация о лидах обновлена
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateTasks&amoUser='.$amoUser.'\'),
				5000);
				</script>
				');
            }
        }
        if ($action == 'updateTasks') {
            $result = $this->updateTasks();
            if ($result) {
                F::alert($result);
            } else {
                F::alert('Задачи обновлены
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateContacts&amoUser='.$amoUser.'\'),
				5000);
				</script>
				');
            }
        }
        if ($action == 'updateContacts') {
            $result = $this->updateContacts();
            if ($result) {
                F::alert($result);
            } else {
                F::alert('Контакты обновлены
				<script>
				setTimeout(
				window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateNotes&amoUser='.$amoUser.'\'),
				5000);
				</script>
				');
            }
        }
        if ($action == 'updateNotes') {
            $result = $this->updateNotes();
            if ($result) {
                F::alert($result);
            } else {
                F::alert('Обновление базы завершено. <a href="/admin">Вернуться на главную</a>.');
            }
        }
    }

    public function resetLeadsCheckers()
    {
        $arr = F::query('update '.F::typetab('amo_leads').'
			set checked = 0
			;');

        return true;
    }

    public function updateLeadsInfo()
    {
        Engine::setDisplayErrors(true);
        ini_set('memory_limit', '800M');
        $arrConfigs = F::query_assoc('
			select encryption_key
			from '.F::typetab('configs').'
			limit 1
			;');
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS id,json 
			from '.F::typetab('amo_leads').'
			where checked = 0
			limit 10000
			;');
        $rows_not_checked = F::rows_without_limit() - count($arr);
        foreach ($arr as $v) {
            // $arrJson = F::query_assoc('select json from '.F::typetab('amo_leads').'
            // 	where id = \''.$v['id'].'\';');
            $lead = json_decode($v['json']);
            // if ($lead->id == 21089260) {
            // 	F::pre($lead);
            // 	F::dump($lead->custom_fields);
            // }
            $prepayment = null;
            $monthlyPayDay = null;
            $monthlyPaySum = null;
            $contractDate = null;
            $monthlyPayDate = null;
            $prepaymentDate = null;
            $payMonths = null;
            $contractId = null;
            $cancelReason = null;
            $promoSource = null;
            $utm = [];
            foreach ($lead->custom_fields as $f) {
                // F::dump($f);
                if ($f->id === 440301) {
                    $prepayment = $f->values[0]->value;
                }
                if ($f->id === 440319) {
                    $monthlyPayDay = $f->values[0]->value;
                }
                if ($f->id === 440389) {
                    $monthlyPaySum = $f->values[0]->value;
                }
                if ($f->id === 604743) {
                    $contractDate = $f->values[0]->value;
                }
                if ($f->id === 529445) {
                    $monthlyPayDate = $f->values[0]->value;
                }
                if ($f->id === 531175) {
                    $prepaymentDate = $f->values[0]->value;
                }
                if ($f->id === 599691) {
                    $payMonths = $f->values[0]->value;
                }
                if ($f->id === 604745) {
                    $contractId = $f->values[0]->value;
                }
                if ($f->id === 636594) {
                    $utm['source'] = $f->values[0]->value;
                }
                if ($f->id === 636596) {
                    $utm['medium'] = $f->values[0]->value;
                }
                if ($f->id === 636598) {
                    $utm['term'] = $f->values[0]->value;
                }
                if ($f->id === 636600) {
                    $utm['content'] = $f->values[0]->value;
                }
                if ($f->id === 636602) {
                    $utm['campaign'] = $f->values[0]->value;
                }
                if ($f->id === 636604) {
                    $utm['referer'] = $f->values[0]->value;
                }
                if ($f->id === 637018) {
                    $cancelReason = $f->values[0]->value;
                }
                if ($f->id === 621020) {
                    $promoSource = $f->values[0]->value;
                }
                if ($f->id === 617218) {
                    // F::dump($field);
                    $contractFio = $f->values[0]->value;
                    // F::dump($fio);
                }
            }
            // если utm_source пустой, то пишем туда promoSource
            if (empty($utm['source'])) {
                $utm['source'] = $promoSource;
            }
            // закончили подтягивать доп. поля.
            // подтягиваем тег города
            // foreach ($lead->tags as $t) {
            // 	if ($t->id === 1100401) {
            // 		$city = $t->values[0]->value;
            // 	}
            // }
            // закончили подтягивать тег города
            if (isset($utm)) {
                foreach ($utm as $k1 => $v1) {
                    $decoded = openssl_decrypt($v1, 'AES-128-ECB', $arrConfigs['encryption_key']);
                    if ($decoded !== false) {
                        $utm[$k1] = $decoded;
                    }
                }
            }
            F::query('
				update '.F::typetab('amo_leads').' set 
				checked = 1,
				name = \''.F::escape_string($lead->name).'\',
				price = \''.F::escape_string($lead->price).'\',
				status_id = \''.F::escape_string($lead->status_id).'\',
				status_name = (select name from '.F::typetab('amo_statuses').' where id = '.F::escape_string($lead->status_id).'),
				date_create = from_unixtime(\''.F::escape_string($lead->date_create).'\'),
				last_modified = from_unixtime(\''.F::escape_string($lead->last_modified).'\'),
				prepayment = '.(empty($prepayment) ? 'null' : '\''.F::escape_string($prepayment).'\'').',
				company_id = '.(empty($lead->linked_company_id) ? 'null' : '\''.F::escape_string($lead->linked_company_id).'\'').',
				monthlyPayDay = '.(empty($monthlyPayDay) ? 'null' : '\''.F::escape_string($monthlyPayDay).'\'').',
				monthlyPaySum = '.(empty($monthlyPaySum) ? 'null' : '\''.F::escape_string($monthlyPaySum).'\'').',
				monthlyPayDate = '.(empty($monthlyPayDate) ? 'null' : '\''.F::escape_string($monthlyPayDate).'\'').',
				prepaymentDate = '.(empty($prepaymentDate) ? 'null' : '\''.F::escape_string($prepaymentDate).'\'').',
				payMonths = '.(empty($payMonths) ? 'null' : '\''.F::escape_string($payMonths).'\'').',
				contractDate = '.(empty($contractDate) ? 'null' : '\''.F::escape_string($contractDate).'\'').',
				contractId = '.(empty($contractId) ? 'null' : '\''.F::escape_string($contractId).'\'').',
				utm_source = '.(empty($utm['source']) ? 'null' : '\''.F::escape_string($utm['source']).'\'').',
				utm_medium = '.(empty($utm['medium']) ? 'null' : '\''.F::escape_string($utm['medium']).'\'').',
				utm_term = '.(empty($utm['term']) ? 'null' : '\''.F::escape_string($utm['term']).'\'').',
				utm_content = '.(empty($utm['content']) ? 'null' : '\''.F::escape_string($utm['content']).'\'').',
				utm_campaign = '.(empty($utm['campaign']) ? 'null' : '\''.F::escape_string($utm['campaign']).'\'').',
				utm_referer = '.(empty($utm['referer']) ? 'null' : '\''.F::escape_string($utm['referer']).'\'').',
				cancel_reason = '.(empty($cancelReason) ? 'null' : '\''.F::escape_string($cancelReason).'\'').',
				group_id = '.(empty($lead->group_id) ? 'null' : '\''.F::escape_string($lead->group_id).'\'').',
				responsible_user_id = '.(empty($lead->responsible_user_id) ? 'null' : '\''.F::escape_string($lead->responsible_user_id).'\'').',
				contractFio = '.(empty($contractFio) ? 'null' : '\''.F::escape_string($contractFio).'\'').',
				contact_id = '.(empty($lead->main_contact_id) ? 'null' : '\''.F::escape_string($lead->main_contact_id).'\'').'
				where id = '.$v['id'].'
				;');
        }

        // statusName = \''.$lead->statusName.'\',
        // return true;
        return $rows_not_checked;
        // F::alert('Таблица лидов обновлена');
    }

    public function updateNotes()
    {
        // получаем список лидов для которых загрузить примечания
        // $leads = new AmoLeads;
        // $leads->set
        $amoUser = empty($_GET['amoUser']) ? F::error('Не передан amoUser') : F::checkstr($_GET['amoUser']);
        $limit = 250;
        $offset = empty($_GET['offset']) ? 0 : $_GET['offset'];
        // type=2 - сделки
        $path = '/api/v2/notes?type=2&limit_rows='.$limit.'&limit_offset='.$offset;
        // F::dump($path);
        $amo = new Amo($amoUser);
        $tasks = $amo->q($path);
        if (empty($tasks->_embedded->items)) {
            return false;
            // return '<script>
            // 		setTimeout(
            // 		window.location.replace(\'/admin\'),
            // 		5000);
            // 		</script>';
        }
        $rows = count($tasks->_embedded->items);
        // F::dump($tasks->_embedded->items[0]);
        foreach ($tasks->_embedded->items as $v) {
            $leadId = ($v->element_type === 2) ? $v->element_id : null;
            if (! isset($v->text)) {
                // F::dump($v);
            }
            F::query('
				insert into '.F::typetab('amo_notes').'
				set
				id = '.$v->id.',
				lead = \''.$leadId.'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\',
				created_at = from_unixtime(\''.$v->created_at.'\'),
				updated_at = from_unixtime(\''.$v->updated_at.'\'),
				text = \''.F::escape_string(isset($v->text) ? $v->text : '').'\',
				note_type = \''.F::escape_string($v->note_type).'\',
				group_id = \''.F::escape_string($v->group_id).'\'
				on duplicate key update
				id = '.$v->id.',
				lead = \''.$leadId.'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\',
				created_at = from_unixtime(\''.$v->created_at.'\'),
				updated_at = from_unixtime(\''.$v->updated_at.'\'),
				text = \''.F::escape_string(isset($v->text) ? $v->text : '').'\',
				note_type = \''.F::escape_string($v->note_type).'\',
				group_id = \''.F::escape_string($v->group_id).'\'
				;');
            // F::dump('ok');
        }
        $offset = $offset + $limit;
        $return = 'Загружено '.$rows.' примечаний, далее с '.$offset.'
			<script>
			setTimeout(
			window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateNotes&amoUser='.$amoUser.'&offset='.$offset.'\'),
			5000);
			</script>';

        return $return;
        F::alert($return);

        // F::alert('Компании загружены');
        return true;
    }

    public function updateContacts()
    {
        // получаем список контактов для которых загрузить примечания
        // $leads = new AmoLeads;
        // $leads->set
        $amoUser = empty($_GET['amoUser']) ? F::error('Не передан amoUser') : F::checkstr($_GET['amoUser']);
        $limit = 500;
        $offset = empty($_GET['offset']) ? 0 : $_GET['offset'];
        // type=2 - сделки
        $path = '/api/v2/contacts?type=2&limit_rows='.$limit.'&limit_offset='.$offset;
        // F::dump($path);
        $amo = new Amo($amoUser);
        $tasks = $amo->q($path);
        if (empty($tasks->_embedded->items)) {
            return false;
            // return '<script>
            // 		setTimeout(
            // 		window.location.replace(\'/admin\'),
            // 		5000);
            // 		</script>';
        }
        $rows = count($tasks->_embedded->items);
        // F::dump($tasks->_embedded->items[0]);
        foreach ($tasks->_embedded->items as $v) {
            $phone = '';
            if (isset($v->custom_fields)) {
                foreach ($v->custom_fields as $c) {
                    if ($c->id == '431738') {
                        foreach ($c->values as $cv) {
                            if (! empty($cv->value)) {
                                $phone = $cv->value;
                            }
                        }
                    }
                }
            }
            F::query('
				insert into '.F::typetab('amo_contacts').'
				set
				id = '.$v->id.',
				phone = \''.F::escape_string($phone).'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				on duplicate key update
				id = '.$v->id.',
				phone = \''.F::escape_string($phone).'\',
				json = \''.F::escape_string(json_encode($v, JSON_UNESCAPED_UNICODE)).'\'
				;');
            // F::dump('ok');
        }
        $offset = $offset + $limit;
        $return = '
			Загружено '.$rows.' контактов, далее с '.$offset.'
			<script>
			setTimeout(
			window.location.replace(\'/?r=UpdateAmo/updateBases&action=updateContacts&amoUser='.$amoUser.'&offset='.$offset.'\'),
			5000);
			</script>
			';

        return $return;
        F::alert($return);

        // F::alert('Компании загружены');
        return true;
    }
}
