<?php

namespace helpme\app\controllers;

use engine\app\models\Domain;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class MainController
{
    private $domain = null;

    private $template = null;

    public function __construct()
    {
        // parent::__construct();
        $this->domain = new Domain(Engine::getDomain());
        $this->template = Engine::template();
        // F::setPageVars($this->template);
        // $this->setMenuVars($this->template);
    }

    // для обычной текстовой страницы
    public function simple()
    {
        return $this->template->get();
    }

    public function statMonths() {}
}
