<?php

namespace helpme\app\controllers;

use engine\app\models\F;
use helpme\app\models\eloquent\AmoGroup;
use helpme\app\models\eloquent\AmoLead;
use helpme\app\models\eloquent\BitrixContact;
use helpme\app\models\eloquent\BitrixDeal;
use helpme\app\models\eloquent\ModulkassaReceipt;
use helpme\app\models\eloquent\Receipt;
use helpme\app\models\eloquent\Transfer;
use helpme\app\models\eloquent\Transfers;
use helpme\app\models\eloquent\User;

class ApiManagerController
{
    protected $auth = null;

    protected $user = null;

    public function __construct()
    {
        header('Content-Type: application/json');
        $this->auth = new HelpmeAuthorizationController(['root', 'admin', 'manager']);
        $this->user = User::find($this->auth->getLogin());
        if (! $this->user->active) {
            F::error('Аккаунт не активирован');
        }
    }

    public function logout()
    {
        $this->auth->clearSID();
        F::redirect('/admin');
    }

    public function getReceipts()
    {
        $groupId = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        $result = Receipt::doesnthave('transfer')
            ->whereIn('document_type', ['чек', 'кассовый чек'])
            ->whereRelation('amoGroup', 'id', $groupId)
            ->orderBy('date')
            ->get();

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getModulkassaReceipts()
    {
        $groupId = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        $result = ModulkassaReceipt::doesnthave('transfer')->get();

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getLeads()
    {
        $groupId = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        $result = F::query_arr('
			select 
			amo_leads.id, amo_leads.name, amo_leads.contractId
			from '.F::typetab('amo_leads').'
			join '.F::typetab('amo_statuses').' on amo_statuses.id = amo_leads.status_id
			where
			(amo_statuses.pay or amo_leads.status_id = 29151510 or (amo_leads.status_id = 143 and amo_leads.prepayment is not null))
			and amo_leads.name not like \'%Автосделка%\'
			and amo_leads.group_id = \''.$groupId.'\'
			order by name
			;');

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getTransfers()
    {
        // header('Content-Type: text/html');
        $input = json_decode(file_get_contents('php://input'));
        // $input = json_decode('{"groupId":"257463","page":1,"withoutLead":null,"withoutReceipt":true,"source":null,"bitrix_contact":null,"limit":25,"order_by":null,"notVerified":null,"text":null,"duplicates":null,"id":null,"dateFrom":null,"dateTo":null}');
        $groupId = $input->groupId ?? F::error('Не передан ID группы');
        $transfers = new Transfers;
        $transfers->setGroupId($groupId);
        if (empty($input->order_by)) {
            $transfers->setOrder('transfers.date desc, transfers.created_at desc, transfers.id desc');
        } else {
            $transfers->setQuery($transfers->getQuery()->orderByDesc($input->order_by));
        }
        $transfers->setLimit($input->limit ?? 25);
        $transfers->setPage($input->page ?? 1);
        if (isset($input->withoutLead) and $input->withoutLead) {
            $transfers->setWithLead(false);
            $transfers->setQuery($transfers->getQuery()->where('value', '>', 0));
        }
        $transfers->setWithReceipt(isset($input->withoutReceipt) ? ($input->withoutReceipt ? false : null) : null);
        $transfers->setVerified(isset($input->notVerified) ? ($input->notVerified ? false : null) : null);
        $transfers->setQuery($transfers->getQuery()->with(['amolead', 'receipt', 'modulkassaReceipt', 'bitrixDeal', 'bitrixContact']));
        $transfers->setSource($input->source ?? null);
        if (! empty($input->bitrix_contact->id)) {
            $transfers->setQuery($transfers->getQuery()->whereRelation('bitrixContact', 'id', $input->bitrix_contact->id));
        }
        if (isset($input->text) and $input->text) {
            $transfers->setQuery($transfers->getQuery()->where('text', 'like', '%'.$input->text.'%'));
        }
        if (isset($input->duplicates) and $input->duplicates) {
            $transfers->setQuery(
                $transfers->getQuery()
                    ->whereNotNull('transfers.bitrix_contact_id')
                    ->whereExists(function ($query) {
                        $query
                            ->select(Transfer::raw('count(*) as q'))
                            ->from('transfers', 't')
                            ->whereColumn('t.date', 'transfers.date')
                            ->whereColumn('t.value', 'transfers.value')
                            ->whereColumn('t.bitrix_contact_id', 'transfers.bitrix_contact_id')
                            ->groupBy('date')
                            ->groupBy('bitrix_contact_id')
                            ->groupBy('value')
                            ->having('q', '>', '1');
                    })
                    ->orderBy('transfers.date')
                    ->orderBy('transfers.bitrix_contact_id')
                    ->orderBy('transfers.value')
            );
        }
        if (isset($input->id) and $input->id) {
            $transfers->setQuery($transfers->getQuery()->whereId($input->id));
        }
        if (isset($input->dateFrom) and $input->dateFrom) {
            $transfers->setQuery($transfers->getQuery()->where('date', '>=', (new \DateTime($input->dateFrom))->format('Y-m-d')));
        }
        if (isset($input->dateTo) and $input->dateTo) {
            $transfers->setQuery($transfers->getQuery()->where('date', '<=', (new \DateTime($input->dateTo))->format('Y-m-d')));
        }
        if (isset($input->incomes) and $input->incomes) {
            $transfers->setQuery($transfers->getQuery()->where('value', '>', 0));
        }
        if (isset($input->expenses) and $input->expenses) {
            $transfers->setQuery($transfers->getQuery()->where('value', '<', 0));
        }
        // $transfers->getQuery()->dd();
        // dd($transfers->get());
        $result = $transfers->get()->append('moment_balance');

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getTransfers2()
    {
        header('Content-Type: text/html');
        $input = json_decode(file_get_contents('php://input'));
        $input = json_decode('{"groupId":"257463","page":1,"withoutLead":null,"withoutReceipt":false,"source":null,"bitrix_contact":null,"limit":25,"order_by":null,"notVerified":false,"duplicates":true}');
        $groupId = $input->groupId ?? F::error('Не передан ID группы');
        $transfers = new Transfer;
        dd($transfers->verified(true)->count());
        $transfers = $transfers->groupId($groupId);
        $transfers = $transfers->limit($input->limit ?? 25);
        dd($transfers);
        if (empty($input->order_by)) {
            $transfers->setOrder('transfers.date desc, transfers.created_at desc, transfers.id desc');
        } else {
            $transfers->setQuery($transfers->getQuery()->orderByDesc($input->order_by));
        }
        $transfers->setPage($input->page ?? 1);
        if (isset($input->withoutLead) and $input->withoutLead) {
            $transfers->setWithLead(false);
            $transfers->setQuery($transfers->getQuery()->where('value', '>', 0));
        }
        $transfers->setWithReceipt(isset($input->withoutReceipt) ? ($input->withoutReceipt ? false : null) : null);
        $transfers->setVerified(isset($input->notVerified) ? ($input->notVerified ? false : null) : null);
        $transfers->setQuery($transfers->getQuery()->with(['amolead', 'receipt', 'bitrixDeal', 'bitrixContact']));
        $transfers->setSource($input->source ?? null);
        if (! empty($input->bitrix_contact->id)) {
            $transfers->setQuery($transfers->getQuery()->whereRelation('bitrixContact', 'id', $input->bitrix_contact->id));
        }
        if (isset($input->text) and $input->text) {
            $transfers->setQuery($transfers->getQuery()->where('text', 'like', '%'.$input->text.'%'));
        }
        if (isset($input->duplicates) and $input->duplicates) {
            $transfers->setQuery(
                $transfers->getQuery()
                    ->whereNotNull('transfers.bitrix_contact_id')
                    ->whereExists(function ($query) {
                        $query
                            ->select(Transfer::raw('count(*) as q'))
                            ->from('transfers', 't')
                            ->whereColumn('t.date', 'transfers.date')
                            ->whereColumn('t.value', 'transfers.value')
                            ->whereColumn('t.bitrix_contact_id', 'transfers.bitrix_contact_id')
                            ->groupBy('date')
                            ->groupBy('bitrix_contact_id')
                            ->groupBy('value')
                            ->having('q', '>', '1');
                    })
                    ->orderBy('transfers.date')
                    ->orderBy('transfers.bitrix_contact_id')
                    ->orderBy('transfers.value')
            );
        }
        // $transfers->getQuery()->dd();
        dd($transfers->get());
        $result = $transfers->get()->append('moment_balance');

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getAuthUser()
    {
        return json_encode($this->user);
    }

    public function getBalances()
    {
        $groupId = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        // сколько денег в кассе?
        $transfers = new Transfers;
        $transfers->setSource('касса');
        $transfers->setGroupId($groupId);
        $arr['cash'] = $transfers->getBalance();
        // сколько в кассе судебных издержек?
        $transfers = new Transfers;
        $transfers->setSource('судебные');
        $transfers->setGroupId($groupId);
        $arr['legalCash'] = $transfers->getBalance();
        // сколько у ежеватова
        $transfers = new Transfers;
        $transfers->setSource('ежеватов');
        $transfers->setGroupId($groupId);
        $arr['ezhevatov'] = $transfers->getBalance();
        // сколько у горина
        $transfers = new Transfers;
        $transfers->setSource('горин');
        $transfers->setGroupId($groupId);
        $arr['gorin'] = $transfers->getBalance();
        // сколько на р/с
        $transfers = new Transfers;
        $transfers->setSource('р/с');
        $transfers->setGroupId($groupId);
        $arr['rs'] = (int) $transfers->getBalance();

        return json_encode($arr, JSON_UNESCAPED_UNICODE);
    }

    // делать проверку может ли вообще данный пользователь добавлять платеж для данного города
    public function addTransfer()
    {
        $input = json_decode(file_get_contents('php://input'));
        // F::pre($input);
        $transfer = new Transfer;
        $transfer->date = empty($input->date) ? F::error('Дата не указана') : (new \Datetime($input->date));
        $transfer->lead = empty($input->lead) ? null : $input->lead;
        $transfer->amo_group = empty($input->groupId) ? F::error('Не передан ID группы') : $input->groupId;
        $transfer->value = empty($input->value) ? F::error('Сумма должна быть указана') : $input->value;
        if (! empty($input->type) and $input->type == 'outcome') {
            $transfer->value = -$transfer->value;
        }
        $transfer->text = empty($input->text) ? F::error('Комментарий не указан') : $input->text;
        $transfer->source = empty($input->source) ? F::error('Не выбран источник') : $input->source;
        $transfer->to_source = empty($input->to_source) ? null : $input->to_source;
        $transfer->bitrix_contact_id = empty($input->bitrix_contact->id) ? null : $input->bitrix_contact->id;
        // F::dump($transfer);
        $transfer->save();

        return json_encode(true);
    }

    public function updateTransfer()
    {
        $newTransfer = json_decode(file_get_contents('php://input'));
        $transfer = Transfer::findOrFail($newTransfer->id ?? null);
        $transfer->receipt_id = $newTransfer->receipt_id;
        $transfer->bitrix_deal_id = $newTransfer->bitrix_deal_id;
        $transfer->bitrix_contact_id = $newTransfer->bitrix_contact_id;
        $transfer->verified = $newTransfer->verified;
        $transfer->text = $newTransfer->text;
        $transfer->date = $newTransfer->date;
        $transfer->source = $newTransfer->source;
        $transfer->to_source = $newTransfer->to_source;
        $transfer->modulkassa_receipt_id = $newTransfer->modulkassa_receipt_id;
        $transfer->save();

        return json_encode(true);
    }

    public function deleteTransfer()
    {
        if ($this->user->level != 'root') {
            F::error('denied');
        }
        $inputTransfer = json_decode(file_get_contents('php://input'));
        $transfer = Transfer::findOrFail($inputTransfer->id ?? null);
        $transfer->delete();

        return json_encode(true);
    }

    public function getSourceStat()
    {
        $groupId = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        $amoGroup = AmoGroup::find($groupId);
        if (! $amoGroup) {
            F::error('Группа не найдена');
        }
        // общая стата по источникам
        $result['bySources'] = AmoLead::select([
            AmoLead::raw('sum(amo_statuses.pay) as q_paid'),
            'utm_source',
            AmoLead::raw('count(*) as q'),
        ])
            ->join('amo_statuses', 'amo_statuses.id', '=', 'amo_leads.status_id')
            ->where('group_id', $amoGroup->id)
            ->groupBy('utm_source')
            ->orderByDesc('q_paid')
            ->orderByDesc('q')
            ->orderByDesc('utm_source')
            ->get();
        // количество заявок
        $result['leadsByMonths'] = AmoLead::select(AmoLead::raw('date_format(date_create,\'%Y-%m\') as month_create, count(*) as q'))
            ->where('group_id', $amoGroup->id)
            ->where('amo_leads.name', 'not like', '%Автосделка%')
            ->groupBy('month_create')
            ->get()
            ->keyBy('month_create');
        $result['contractsByMonths'] = AmoLead::select(AmoLead::raw('date_format(contractDate,\'%Y-%m\') as contractMonth, sum(price) as sumPrices, ifnull(motivation.value,0) as motivationValue, count(*) as q'))
            ->leftJoin('motivation', function ($join) {
                $join
                    ->on('motivation.amo_group', '=', 'amo_leads.group_id')
                    ->on(AmoLead::raw('date_format(motivation.month,\'%Y-%m\')'), '=', AmoLead::raw('date_format(contractDate,\'%Y-%m\')'));
            })
            ->where('group_id', $amoGroup->id)
            ->whereRelation('amoStatus', 'pay', true)
            ->where('amo_leads.name', 'not like', '%Автосделка%')
            ->groupBy('contractMonth')
            ->orderByDesc('contractMonth')
            ->get();
        // период за который получаем статистику по источникам и договорам
        $dateTo = new \Datetime;
        $dateFrom = (new \Datetime)->modify('-1 month');
        $result['leadsByDaysAndSources'] = AmoLead::select(AmoLead::raw('date(date_create) as `day`'), 'utm_source', AmoLead::raw('count(*) as q'))
            ->where('group_id', $amoGroup->id)
            ->whereBetween('date_create', [$dateFrom, $dateTo])
            ->groupBy('day')
            ->groupBy('utm_source')
            ->orderByDesc('day')
            ->orderByDesc('q')
            ->get()
            ->groupBy('day');
        $result['contractsByDays'] = AmoLead::selectRaw('count(*) as q')
            ->selectRaw('date(contractDate) as contractDay')
            ->join('amo_statuses', 'amo_statuses.id', '=', 'amo_leads.status_id')
            ->where('group_id', $amoGroup->id)
            ->where('amo_statuses.pay', 1)
            ->where('amo_leads.name', 'not like', '%Автосделка%')
            ->whereBetween('contractDate', [$dateFrom, $dateTo])
            ->groupBy('contractDay')
            ->get()
            ->keyBy('contractDay');
        // получаем сделки битрикса за последний месяц
        $dateFrom = new \DateTime;
        $dateFrom->modify('-1 month');
        $result['bitrixDeals'] = BitrixDeal::where('contractDate', '>', $dateFrom->format('Y-m-d H:i:s'))
            ->whereRelation('bitrixDealCity', 'amo_group_id', $groupId)
            ->limit(10)
            ->get();

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function getBitrixDeals()
    {
        $contract = $_GET['contract'] ?? null;
        $limit = $_GET['limit'] ?? 10;
        $offset = $_GET['offset'] ?? 0;
        $amo_group_id = $_GET['amo_group_id'] ?? null;
        $orderBy = $_GET['orderBy'] ?? null;
        $select = $_GET['select'] ?? null;
        $query = BitrixDeal::query();
        if (! is_null($contract)) {
            $query->whereHas('bitrixStage', function ($query) use ($contract) {
                $query->where('contract', $contract);
                $query->whereHas('bitrixCategory', function ($query) use ($contract) {
                    $query->where('contract', $contract);
                });
            });
            // $query->whereRelation('bitrixStage','contract',$contract);
        }
        if ($amo_group_id) {
            $query->whereRelation('bitrixDealCity', 'amo_group_id', $amo_group_id);
        }
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        if ($orderBy) {
            $query->orderBy($orderBy);
        }
        /*
        if ($select) {
            $query->select($select);
        }
        */
        $query->with('bitrixStage', 'bitrixStage.bitrixCategory');

        return json_encode($query->get(), JSON_UNESCAPED_UNICODE);
    }

    public function getBitrixContacts()
    {
        ini_set('memory_limit', '256M');
        $contract = $_GET['contract'] ?? null;
        $limit = $_GET['limit'] ?? 10;
        $offset = $_GET['offset'] ?? 0;
        $amo_group_id = $_GET['amo_group_id'] ?? null;
        $orderBy = $_GET['orderBy'] ?? null;
        $select = $_GET['select'] ?? null;
        $query = BitrixContact::query();
        // $contract = null;
        // $query->limit(10);
        if (! is_null($contract)) {
            $query->whereHas('bitrixDeals', function ($query) use ($contract) {
                $query->whereHas('bitrixStage', function ($query) use ($contract) {
                    $query->where('contract', $contract);
                    $query->whereHas('bitrixCategory', function ($query) use ($contract) {
                        $query->where('contract', $contract);
                    });
                });
            });
        }
        if ($amo_group_id) {
            $query->whereHas('bitrixDeals', function ($query) use ($amo_group_id) {
                $query->whereRelation('bitrixDealCity', 'amo_group_id', $amo_group_id);
            });
        }
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }
        if ($orderBy) {
            $query->orderBy($orderBy);
        }
        if ($select) {
            $query->select($select);
        }
        $query->with('bitrixDeals', 'bitrixDeals.bitrixStage', 'bitrixDeals.bitrixStage.bitrixCategory');

        return json_encode($query->get(), JSON_UNESCAPED_UNICODE);
    }
}
