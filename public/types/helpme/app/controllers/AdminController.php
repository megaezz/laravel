<?php

namespace helpme\app\controllers;

use engine\app\models\F;
use engine\app\models\Template;
use helpme\app\models\Amo;
use helpme\app\models\AmoLead;
use helpme\app\models\AmoLeads;
use helpme\app\models\Transfer;
use helpme\app\models\Transfers;
// use helpme\app\models\eloquent\Transfers;
use helpme\app\models\User;

class AdminController extends ManagerController
{
    public function __construct()
    {
        $this->auth = new HelpmeAuthorizationController(['root', 'admin']);
        $this->user = new User($this->auth->getLogin(), 'helpme');
    }

    public function transfersStat()
    {
        $group_id = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        $source = empty($_GET['source']) ? null : F::checkstr($_GET['source']);
        $t = new Template('admin/transfers_stat/index');
        $arr_sources = Transfers::getSources();
        $list = '';
        foreach ($arr_sources as $v) {
            $tList = new Template('admin/transfers_stat/item_source');
            $tList->v('group_id', $group_id);
            $tList->v('source', $v);
            $tList->v('source_urlencoded', urlencode($v));
            $list .= $tList->get();
        }
        $t->v('sourcesList', $list);
        $transfers = new Transfers;
        $transfers->setGroupId($group_id);
        $transfers->setListTemplate('admin/transfers_stat/item');
        $transfers->setSource($source);
        $transfers->setOrder('date');
        /*
        $arr = F::query_arr('
            select date, type, value, text, source, to_source
            from '.F::typetab('transfers').'
            where amo_group = \''.$group_id.'\'
            and (source = \''.F::escape_string($source).'\' or to_source = \''.F::escape_string($source).'\')
            order by date
            ;');
        $list = '';
        $sum = 0;
        foreach ($arr as $v) {
            if ($v['to_source'] == $source) {
                $v['value'] = - $v['value'];
            }
            $sum = $sum + $v['value'];
            $tList = new Template('admin/transfers_stat/item');
            $tList->v('date',$v['date']);
            $tList->v('type',$v['type']);
            $tList->v('value',$v['value']);
            $tList->v('text',$v['text']);
            $tList->v('source',$v['source']);
            $tList->v('to_source',$v['to_source']);
            $list.= $tList->get();
        }
        */
        $t->v('source', $source);
        // $t->v('list',$list);
        // $t->v('sum',$sum);
        $t->v('list', $transfers->getList());
        $t->v('sum', $transfers->getBalance());
        $this->setMenuVars($t);

        return $t->get();
    }

    public function submitEditTransfer()
    {
        // F::dump($_POST);
        $transfer_id = empty($_POST['transfer_id']) ? F::error('Не передан ID оплаты') : F::checkstr($_POST['transfer_id']);
        $receipt_id = empty($_POST['receipt_id']) ? null : F::checkstr($_POST['receipt_id']);
        F::query('update '.F::typetab('transfers').' set
			receipt_id = '.($receipt_id ? ('\''.$receipt_id.'\'') : 'null').'
			where id = \''.$transfer_id.'\'
			;');
        F::redirect(F::referer_url());
    }

    public function submitModulbankTransactions()
    {
        $group_id = 257463;
        $file_path = empty($_FILES['csv']['tmp_name']) ? F::error('Не передан файл') : $_FILES['csv']['tmp_name'];
        $csv = fopen($file_path, 'r');
        $str = 0;
        $added = 0;
        while (($data = fgetcsv($csv, null, ';')) !== false) {
            $str++;
            if (! is_numeric($data[0])) {
                continue;
            }
            // F::dump((new \DateTime($data[1]))->format('Y-m-d'));
            // F::dump($data);
            $income = floatval(str_replace(',', '.', $data[8] ?? 0));
            $outcome = floatval(str_replace(',', '.', $data[9] ?? 0));
            $sum = $income ? $income : ($outcome ? (-$outcome) : F::error('Нет прихода и расхода #'.$data[0]));
            $text = $data[7];
            $source = 'р/с';
            $to_source = null;
            if (strstr($text, 'Горин Кирилл')) {
                $source = 'горин';
                $to_source = 'р/с';
                $sum = -$sum;
            }
            // $transfer = Transfer::createOrUpdate();
            F::query('
				insert into '.F::typetab('transfers').' set
				modulbank_id = \''.F::escape_string($data[0]).'\',
				date = \''.F::escape_string((new \DateTime($data[1]))->format('Y-m-d')).'\',
				value = \''.$sum.'\',
				text = \''.F::escape_string($data[7]).'\',
				source = \''.$source.'\',
				to_source = '.($to_source ? ('\''.F::escape_string($to_source).'\'') : 'null').',
				amo_group = \''.F::escape_string($group_id).'\'
				on duplicate key update
				date = \''.F::escape_string((new \DateTime($data[1]))->format('Y-m-d')).'\',
				value = \''.$sum.'\',
				text = \''.F::escape_string($data[7]).'\',
				source = \''.$source.'\',
				to_source = '.($to_source ? ('\''.F::escape_string($to_source).'\'') : 'null').',
				amo_group = \''.F::escape_string($group_id).'\'
				;');
            $added++;
            // F::dump($data);
        }
        fclose($csv);
        F::alert('Добавлено '.$added.' транзакций');
        // F::dump($file);
        // F::dump($_FILES);
    }

    public function submitLoadReceipts()
    {
        $file_path = empty($_FILES['csv']['tmp_name']) ? F::error('Не передан файл') : $_FILES['csv']['tmp_name'];
        $csv = fopen($file_path, 'r');
        $str = 0;
        $added = 0;
        while (($data = fgetcsv($csv, null, ';')) !== false) {
            $str++;
            if ($str === 1) {
                continue;
            }
            // F::dump($data);
            // $arr = array(
            // 	'date' => $data[0],
            // 	'document_type' => $data[1],
            // 	'operation_type' => $data[4],
            // 	'cash' => $data[6],
            // 	'cashless' => $data[7],
            // 	'sum' => $data[8],
            // 	'num_fd' => $data[16],
            // 	'fpd' => $data[17],
            // 	'num_fn' => $data[19],
            // 	'reg_num_kkt' => $data[20]
            // );
            F::query('
				insert into '.F::typetab('receipts').' set
				receipts.date = \''.F::escape_string($data[0]).'\',
				receipts.document_type = \''.F::escape_string($data[1]).'\',
				receipts.operation_type = \''.F::escape_string($data[4]).'\',
				receipts.cash = \''.F::escape_string($data[6]).'\',
				receipts.cashless = \''.F::escape_string($data[7]).'\',
				receipts.sum = \''.F::escape_string($data[8]).'\',
				receipts.num_fd = \''.F::escape_string($data[16]).'\',
				receipts.fpd = \''.F::escape_string($data[17]).'\',
				receipts.num_fn = \''.F::escape_string($data[19]).'\',
				receipts.reg_num_kkt = \''.F::escape_string($data[20]).'\',
				data = \''.F::escape_string(json_encode($data, JSON_UNESCAPED_UNICODE)).'\'
				on duplicate key update
				receipts.date = \''.F::escape_string($data[0]).'\',
				receipts.document_type = \''.F::escape_string($data[1]).'\',
				receipts.operation_type = \''.F::escape_string($data[4]).'\',
				receipts.cash = \''.F::escape_string($data[6]).'\',
				receipts.cashless = \''.F::escape_string($data[7]).'\',
				receipts.sum = \''.F::escape_string($data[8]).'\',
				receipts.num_fd = \''.F::escape_string($data[16]).'\',
				receipts.fpd = \''.F::escape_string($data[17]).'\',
				receipts.num_fn = \''.F::escape_string($data[19]).'\',
				receipts.reg_num_kkt = \''.F::escape_string($data[20]).'\',
				data = \''.F::escape_string(json_encode($data, JSON_UNESCAPED_UNICODE)).'\'
				;');
            $added++;
            // F::dump($data);
        }
        fclose($csv);
        F::alert('Добавлено '.$added.' чеков');
        // F::dump($file);
        // F::dump($_FILES);
    }

    public function leadsDog()
    {
        $leads = new AmoLeads;
        // $leads->setHasPaymentTasks(true);
        $leads->setGroupId(257463);
        $leads->setAutoLead(false);
        $leads->setLimit($leads->getRows());
        $arr = $leads->get();
        // $lead = new AmoLead(20961334);
        // F::dump($lead->getData());
        $result = [];
        foreach ($arr as $v) {
            $lead = new AmoLead($v['id']);
            $customFields = $lead->getData()->custom_fields;
            $fio = null;
            foreach ($customFields as $field) {
                // $result[$lead->getId()] = null;
                if ($field->id === 617218) {
                    // F::dump($field);
                    $fio = $field->values[0]->value;
                    // F::dump($fio);
                }
            }
            if (! empty($fio)) {
                $result[$lead->getId()] = $fio;
            }
            // F::dump($customFields);
            // F::dump($lead);
        }
        asort($result);
        $list = '';
        foreach ($result as $id => $fio) {
            $lead = new AmoLead($id);
            $list .= '<li>'.$lead->getId().' '.$fio.' '.($lead->isTerminated() ? '<span style="color: red;">Расторгнут</span>' : '').'</li>';
        }

        return '<ol>'.$list.'</ol>';
        // F::pre($result);
        // F::dump($leads->getRows());
    }

    public function salary()
    {
        $group_id = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        $arr = F::query_arr('
			select users.login,users.name
			from '.F::typetab('users').'
			join '.f::typetab('users_amo_groups').' uag on uag.login = users.login
			where uag.amo_group = \''.$group_id.'\'
			;');
        $list = '';
        $date = new \Datetime;
        $do = true;
        $i = 0;
        $monthsList = '';
        do {
            // считаем расходы по всем пользователям за текущий месяц
            $list = '';
            $firstDay = new \Datetime($date->format('Y-m-d'));
            $firstDay->modify('first day of');
            $lastDay = new \Datetime($date->format('Y-m-d'));
            $lastDay->modify('last day of');
            foreach ($arr as $v) {
                $user = new User($v['login']);
                $transfers = F::query_assoc('
					select sum(value) as sum
					from '.F::typetab('transfers').'
					where login = \''.$v['login'].'\'
					and (
					(date(date) between \''.$firstDay->format('Y-m-d').'\'
					and \''.$lastDay->format('Y-m-d').'\'
					and salary_month is null)
					or salary_month = \''.$firstDay->format('Y-m').'\'
					);');
                $salary = F::query_assoc('
					select salary,bonus
					from '.F::typetab('salaries').'
					where login = \''.$v['login'].'\'
					and month = \''.$firstDay->format('Y-m').'\'
				;');
                $salary['salary'] = $salary['salary'] ?? null;
                $salary['bonus'] = $salary['bonus'] ?? null;
                // если ЗП не расчитана и пользователь не активирован, то не выводим его (чтобы убрать уволенных)
                if (! $salary['salary'] and ! $user->getActive()) {
                    continue;
                }
                $paid = empty($transfers['sum']) ? 0 : $transfers['sum'];
                $salary = $salary['salary'] + $salary['bonus'];
                $list .= '<tr>
				<td>'.$v['name'].'</td>
				<td>'.($salary ? $salary.' руб.' : 'Не расчитано').'</td>
				<td>'.$paid.' руб.</td>
				<td>'.($salary + $paid).' руб.</td>
				</tr>';
            }
            $itemMonth = new Template('admin/salary/item_month');
            $itemMonth->v('list', $list);
            $itemMonth->v('month', $date->format('Y-m'));
            $monthsList .= $itemMonth->get();
            $date->modify('-1 month');
            $i++;
            if ($i == 3) {
                $do = false;
            }
        } while ($do);
        $t = new Template('admin/salary/index');
        $t->v('monthsList', $monthsList);
        $this->setMenuVars($t, 'salary');

        return $t->get();
    }

    // учесть поле дата предоплаты
    // разобраться с отсутствием суммы ежемесячного платежа для допов
    public function monthlyPayCalendar()
    {
        $group_id = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        // если не root, то проверяем может ли этот юзер смотреть инфу по данной компании
        if ($this->user->getLevel() !== 'root') {
            // $arr = F::query_arr('
            // 	select * from
            // 	'.F::typetab('users_amo_users').'
            // 	join '.F::typetab('amo_users').' on amo_users.id = users_amo_users.amo_user
            // 	where users_amo_users.login = \''.$this->user->getLogin().'\' and
            // 	amo_users.company_id = \''.$company_id.'\'
            // 	;');
            $arr = F::query_arr('
				select * from
				'.F::typetab('users_amo_groups').'
				where login = \''.$this->user->getLogin().'\'
				and amo_group = \''.$group_id.'\'
				;');
            if (! $arr) {
                F::error('Вы не имеете доступа к данной группе');
            }
        }
        // $company_id = 38230370;
        // $company_id = 41432036;
        $arr = F::query_arr('
			select
			amo_leads.id, amo_leads.name, amo_leads.price, amo_leads.status_name,
			amo_leads.monthlyPayDay, amo_leads.monthlyPayDate, amo_leads.monthlyPaySum,
			amo_leads.contractDate, amo_leads.prepayment, amo_leads.company_id,
			amo_leads.prepaymentDate, amo_leads.payMonths
			from '.F::typetab('amo_leads').'
			join '.F::typetab('amo_statuses').' on amo_statuses.id = amo_leads.status_id
			where
			amo_statuses.pay
			and amo_leads.name not like \'%Автосделка%\'
			and amo_leads.group_id = \''.$group_id.'\'
			;');
        // F::dump($arr);
        $calendar = [];
        foreach ($arr as $v) {
            $monthlyPaySum = $v['monthlyPaySum'];
            // исправить! разобраться с суммой ежемесячного платежа у допов (не заполняется)
            if (! $v['monthlyPaySum']) {
                continue;
            }
            // сколько месяцев платить?
            // сначала считаем сами, если получаемое число месяцев больше 12, то оставляем 12
            $q_months = ceil(($v['price'] - $v['prepayment']) / $v['monthlyPaySum']);
            $q_months = ($q_months > 12) ? 12 : $q_months;
            // если в договоре прописано меньшее кол-во месяцев, то применяем это значение
            if ($v['payMonths'] < $q_months) {
                $q_months = $v['payMonths'];
            }
            // фиксируем предоплату
            if ($v['prepayment']) {
                $calendar[$v['prepaymentDate']][] = ['id' => $v['id'], 'sum' => $v['prepayment']];
            }
            // остальные платежи
            $i = 0;
            if (empty($v['monthlyPayDate'])) {
                // если 1 месяц значит это единовременная оплата, все ок
                if ($q_months != 1) {
                    // $v['monthlyPayDate'] = '1';
                    F::error('monthlyPayDate not required to be empty (lead #'.$v['id'].')');
                }
            }
            // дата первой оплаты по графику платежей
            $currentDate = new \DateTime($v['monthlyPayDate']);
            $sum = $v['prepayment'];
            // высчитывем платежи помесячно без последнего месяца (поэтому -2)
            for ($i = 0; $i <= ($q_months - 2); $i++) {
                $calendar[$currentDate->format('Y-m-d')][] = ['id' => $v['id'], 'sum' => $v['monthlyPaySum']];
                $sum = $sum + $v['monthlyPaySum'];
                // следующий месяц
                $currentDate->modify('+1 month');
            }
            // последний месяц (если есть, что доплатить)
            if (($v['price'] - $sum) != 0) {
                // $currentDate->m_checkstatus(conn, identifier)odify('+1 month');
                $calendar[$currentDate->format('Y-m-d')][] = ['id' => $v['id'], 'sum' => ($v['price'] - $sum)];
            }
        }
        // определяем первый и последний месяц
        $minDate = new \DateTime;
        $maxDate = new \DateTime;
        foreach ($calendar as $calendarDate => $v) {
            $date = new \DateTime($calendarDate);
            if ($date > $maxDate) {
                $maxDate = $date;
            }
            if ($date < $minDate) {
                $minDate = $date;
            }
        }
        // F::dump($maxDate);
        // F::dump($calendar);
        $date = new \DateTime($minDate->format('Y-m-d'));
        $list = '';
        $monthCalendar = [];
        while ($date <= $maxDate) {
            if (isset($calendar[$date->format('Y-m-d')])) {
                foreach ($calendar[$date->format('Y-m-d')] as $c) {
                    $monthCalendar[$date->format('Y-m')][] = $c;
                }
            }
            $date->modify('+1 day');
        }
        //
        // фактические оплаты
        $factPaysArr = F::query_arr('
			select transfers.value, amo_leads.name, date_format(transfers.date,\'%Y-%m\') as yearMonth,
			amo_leads.id as lead_id
			from '.F::typetab('transfers').'
			join '.F::typetab('amo_leads').' on amo_leads.id = transfers.lead and amo_leads.group_id = \''.$group_id.'\'
			where transfers.source is null or transfers.source in (\'р/с\',\'горин\',\'я\')
			;');
        $factPays = [];
        // F::dump($factPaysArr);
        foreach ($factPaysArr as $v) {
            $factPays[$v['yearMonth']][] = ['id' => $v['lead_id'], 'sum' => $v['value']];
        }
        // F::dump($factPays);
        //
        $list = '';
        $chartArr[] = ['Месяц', 'Прогноз', 'Фактически'];
        $i = 0;
        foreach ($monthCalendar as $month => $mc) {
            $date = new \DateTime($month);
            $mc_list = '';
            $monthSum = 0;
            foreach ($mc as $v) {
                $arr = F::query_assoc('select * from '.F::typetab('amo_leads').' where id = \''.$v['id'].'\';');
                $tLead = new Template('admin/calendar/lead');
                $tLead->v('leadName', $arr['name']);
                $tLead->v('leadId', $arr['id']);
                $tLead->v('sum', $v['sum']);
                $tLead->v('contractId', $arr['contractId']);
                $mc_list .= $tLead->get();
                $monthSum = $monthSum + $v['sum'];
            }
            // если есть фактические оплаты в этом месяце - подгружаем их
            $fp_list = '';
            $monthSumFact = 0;
            if (isset($factPays[$date->format('Y-m')])) {
                foreach ($factPays[$date->format('Y-m')] as $v) {
                    $arr = F::query_assoc('select * from '.F::typetab('amo_leads').' where id = \''.$v['id'].'\';');
                    $tLead = new Template('admin/calendar/lead');
                    $tLead->v('leadName', $arr['name']);
                    $tLead->v('leadId', $arr['id']);
                    $tLead->v('sum', $v['sum']);
                    $tLead->v('contractId', $arr['contractId']);
                    $fp_list .= $tLead->get();
                    $monthSumFact = $monthSumFact + $v['sum'];
                }
            }
            $chartArr[] = [$date->format('F Y'), round($monthSum), round($monthSumFact)];
            $tMonth = new Template('admin/calendar/month');
            $tMonth->v('monthYear', $date->format('F Y'));
            $tMonth->v('monthSum', number_format($monthSum, 0, ',', ' '));
            $tMonth->v('monthSumFact', number_format($monthSumFact, 0, ',', ' '));
            $tMonth->v('listLeads', $mc_list);
            $tMonth->v('listLeadsFact', $fp_list);
            $tMonth->v('yearMonth', $date->format('Y-m'));
            $list .= $tMonth->get();
            $i++;
        }
        // F::dump(json_encode($chartArr));
        $t = new Template('admin/calendar/index');
        $t->v('chartValues', json_encode($chartArr));
        $t->v('list', $list);
        $t->v('groupId', $group_id);
        $this->setMenuVars($t, 'calendar');

        return $t->get();
    }
}
