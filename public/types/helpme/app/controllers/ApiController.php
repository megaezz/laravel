<?php

namespace helpme\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use helpme\app\models\Amo;

class ApiController
{
    public function submitAddLead()
    {
        if (! request()->has('query_string')) {
            F::error('Empty query string');
        }
        $queries = [];
        parse_str(request()->getQueryString(), $queries);
        $amoUserId = empty(request()->query('id')) ? F::error('Amo User ID required to use addLead') : F::checkstr(request()->query('id'));
        $sum = empty($queries['sum']) ? (empty(request()->input('sum')) ? 'не указано' : F::checkstr(request()->input('sum'))) : F::checkstr($queries['sum']);
        $phone = empty($queries['phone']) ? (empty(request()->input('phone')) ? F::error('Phone is required') : F::checkstr(request()->input('phone'))) : F::checkstr($queries['phone']);
        $address = empty($queries['passport']) ? (empty(request()->input('passport')) ? 'не указано' : F::checkstr(request()->input('passport'))) : F::checkstr($queries['passport']);
        $name = empty($queries['name']) ? (empty(request()->input('name')) ? 'не указано' : F::checkstr(request()->input('name'))) : F::checkstr($queries['name']);
        $source = empty($queries['source']) ? (empty(request()->header('Referer')) ? 'не указано' : F::checkstr(request()->header('Referer'))) : F::checkstr($queries['source']);
        $utm['referer'] = $source;
        // парсим $source чтобы выцепить utm метки
        $parseUrl = parse_url($source);
        if (isset($parseUrl['query'])) {
            parse_str($parseUrl['query'], $parseStr);
        }
        $utm['source'] = isset($parseStr['utm_source']) ? $parseStr['utm_source'] : null;
        $utm['medium'] = isset($parseStr['utm_medium']) ? $parseStr['utm_medium'] : null;
        $utm['term'] = isset($parseStr['utm_term']) ? $parseStr['utm_term'] : null;
        $utm['content'] = isset($parseStr['utm_content']) ? $parseStr['utm_content'] : null;
        $utm['campaign'] = isset($parseStr['utm_campaign']) ? $parseStr['utm_campaign'] : null;
        // $utm['referer'] = isset($parseStr['utm_referer'])?$parseStr['utm_referer']:null;
        $arrConfigs = F::query_assoc('
			select encryption_key
			from '.F::typetab('configs').'
			limit 1
			;');
        foreach ($utm as $k => $v) {
            if ($v) {
                $utm[$k] = openssl_encrypt($v, 'AES-128-ECB', $arrConfigs['encryption_key']);
            }
        }
        // F::dump($utmSource);
        $leads['add'] = [
            [
                'name' => $name.' (с сайта)',
                'status_id' => 20237007,
                'custom_fields' => [
                    [
                        'id' => 636594,
                        'values' => [['value' => $utm['source']]],
                    ],
                    [
                        'id' => 636596,
                        'values' => [['value' => $utm['medium']]],
                    ],
                    [
                        'id' => 636598,
                        'values' => [['value' => $utm['term']]],
                    ],
                    [
                        'id' => 636600,
                        'values' => [['value' => $utm['content']]],
                    ],
                    [
                        'id' => 636602,
                        'values' => [['value' => $utm['campaign']]],
                    ],
                    [
                        'id' => 636604,
                        'values' => [['value' => $utm['referer']]],
                    ],
                ],
            ],
        ];
        // F::dump($leads['add'][0]['custom_fields'][0]);
        $amo = new Amo($amoUserId);
        // добавляем новую сделку
        $newLead = $amo->q('/api/v2/leads', $leads);
        // добавляем примечание к сделке
        $notes['add'] = [
            [
                'element_id' => $newLead->_embedded->items[0]->id,
                'element_type' => '2',
                'text' => '
				Имя: '.$name.'
				Телефон: '.$phone.'
				Сумма: '.$sum.'
				Адрес: '.$address.'
				Источник: '.$source.'
				',
                'note_type' => '4',
            ],
        ];
        $newNote = $amo->q('/api/v2/notes', $notes);
        logger()->info("Заявка с сайта $source: имя: $name, телефон: $phone, сумма: $sum, адрес: $address");

        return true;
    }

    public function addLead()
    {
        $this->submitAddLead();
        $answer = new \stdClass;
        $answer->status = true;
        $answer->message = '<h1>Заявка принята, спасибо. Мы вам перезвоним</h1>';
        header('Content-Type: application/json');

        return '';
        // return json_encode($answer);
    }

    public function submitAddLeadLaw()
    {
        $this->submitAddLead();
        $t = new Template('law/ok');

        return $t->get();
        F::alert('Ваша заявка принята, скоро мы вам позвоним');
    }

    public function submitPoll()
    {
        // $t = new Template('poll/result');
        // return $t->get();
        // F::dump($_POST);
        $queries = [];
        $amoUserId = empty($_GET['id']) ? F::error('Amo User ID required to use addLead') : F::checkstr($_GET['id']);
        $source = empty($queries['source']) ? (empty(request()->header('Referer')) ? 'не указано' : F::checkstr(request()->header('Referer'))) : F::checkstr($queries['source']);
        //
        $creditors = empty($_POST['creditors']) ? null : F::checkstr($_POST['creditors']);
        $debt = empty($_POST['debt']) ? null : F::checkstr($_POST['debt']);
        $income = empty($_POST['income']) ? null : F::checkstr($_POST['income']);
        $hypothec = empty($_POST['hypothec']) ? null : F::checkstr($_POST['hypothec']);
        $businessman = empty($_POST['businessman']) ? null : F::checkstr($_POST['businessman']);
        $personal_data = empty($_POST['personal_data']) ? null : F::checkstr($_POST['personal_data']);
        $name = empty($_POST['name']) ? null : F::checkstr($_POST['name']);
        $phone = empty($_POST['phone']) ? null : F::checkstr($_POST['phone']);
        if (! $phone) {
            F::error('Не указан телефон');
        }
        $answer = '
		Кредиторов: '.$creditors.'
		Общая сумма вашей задолженности: '.$debt.'
		Официальный ежемесячный доход: '.$income.'
		Оформлена ли на Вас ипотека: '.$hypothec.'
		Являетесь ли Вы действующим индивидуальным предпринимателем: '.$businessman.'
		Я хочу получить результат анализа моей ситуации: '.$personal_data.'
		Ваше имя: '.$name.'
		Ваш номер телефона: '.$phone.'
		Источник: '.$source.'
		';
        $utm['referer'] = $source;
        // парсим $source чтобы выцепить utm метки
        $parseUrl = parse_url($source);
        if (isset($parseUrl['query'])) {
            parse_str($parseUrl['query'], $parseStr);
        }
        $utm['source'] = isset($parseStr['utm_source']) ? $parseStr['utm_source'] : null;
        $utm['medium'] = isset($parseStr['utm_medium']) ? $parseStr['utm_medium'] : null;
        $utm['term'] = isset($parseStr['utm_term']) ? $parseStr['utm_term'] : null;
        $utm['content'] = isset($parseStr['utm_content']) ? $parseStr['utm_content'] : null;
        $utm['campaign'] = isset($parseStr['utm_campaign']) ? $parseStr['utm_campaign'] : null;
        // $utm['referer'] = isset($parseStr['utm_referer'])?$parseStr['utm_referer']:null;
        $arrConfigs = F::query_assoc('
			select encryption_key
			from '.F::typetab('configs').'
			limit 1
			;');
        foreach ($utm as $k => $v) {
            if ($v) {
                $utm[$k] = openssl_encrypt($v, 'AES-128-ECB', $arrConfigs['encryption_key']);
            }
        }
        // F::dump($utmSource);
        $leads['add'] = [
            [
                'name' => $name.' ('.($parseStr['utm_source'] ? $parseStr['utm_source'] : 'источник неизвестен').')',
                'status_id' => 20237007,
                'custom_fields' => [
                    [
                        'id' => 636594,
                        'values' => [['value' => $utm['source']]],
                    ],
                    [
                        'id' => 636596,
                        'values' => [['value' => $utm['medium']]],
                    ],
                    [
                        'id' => 636598,
                        'values' => [['value' => $utm['term']]],
                    ],
                    [
                        'id' => 636600,
                        'values' => [['value' => $utm['content']]],
                    ],
                    [
                        'id' => 636602,
                        'values' => [['value' => $utm['campaign']]],
                    ],
                    [
                        'id' => 636604,
                        'values' => [['value' => $utm['referer']]],
                    ],
                ],
            ],
        ];
        $amo = new Amo($amoUserId);
        // добавляем новую сделку
        $newLead = $amo->q('/api/v2/leads', $leads);
        // добавляем примечание к сделке
        $notes['add'] = [
            [
                'element_id' => $newLead->_embedded->items[0]->id,
                'element_type' => '2',
                'text' => $answer,
                'note_type' => '4',
            ],
        ];
        $newNote = $amo->q('/api/v2/notes', $notes);
        // F::dump($newLead);
        // F::dump(json_decode($json));
        logger()->info("Заявка с сайта $source: имя: $name, телефон: $phone, сумма: $debt");
        $t = new Template('poll/result');

        return $t->get();
        // F::pre($answer);
    }

    public function informer2gis()
    {
        Engine::setDebug(false);
        $url = 'https://catalog.api.2gis.ru/3.0/items/byid?id=70000001053400479_h47j7652230H3H8301I2GGG0e5qrdd95G6G9248132550358qEpt7A1925G42G6G1J142728o749uv193345101337AH8J3HHHGJHG257&key=rurbbn3446&locale=ru_RU&fields=items.locale,items.flags,search_attributes,items.adm_div,items.region_id,items.segment_id,items.reviews,items.point,request_type,context_rubrics,query_context,items.links,items.name_ex,items.org,items.group,items.dates,items.external_content,items.contact_groups,items.comment,items.ads.options,items.email_for_sending.allowed,items.stat,items.stop_factors,items.description,items.geometry.centroid,items.geometry.selection,items.geometry.style,items.timezone_offset,items.context,items.level_count,items.address,items.is_paid,items.access,items.access_comment,items.capacity,items.schedule,items.floors,ad,items.rubrics,items.routes,items.platforms,items.directions,items.barrier,items.reply_rate,items.purpose,items.attribute_groups,items.route_logo,items.has_goods,items.has_apartments_info,items.has_pinned_goods,items.has_realty,items.has_exchange,items.has_payments,items.is_promoted,items.delivery,items.order_with_cart,search_type,items.has_discount,items.metarubrics,broadcast,items.detailed_subtype&context_rubrics[0]=110425&viewpoint1=45.00493204070624,53.19757138684089&viewpoint2=45.02521595929376,53.190388613159115&stat[sid]=0f92e8bf-c007-4033-84b0-0f7f3c74cd62&stat[user]=70a31135-eaaa-4197-98d0-2dfb923744ac&shv=2021-08-31-08&r=4258093509';
        $data = json_decode(F::cache('engine\app\models\F', 'filegetcontentsCached', ['url' => $url]));
        // F::dump($data->result);
        $t = new Template('2gis_informer');
        $t->v('name', $data->result->items[0]->name);
        $t->v('subname', $data->result->items[0]->rubrics[0]->name);
        $t->v('rating', $data->result->items[0]->reviews->general_rating);

        // F::dump($t);
        return $t->get();
    }
}
