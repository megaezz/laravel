<?php

namespace helpme\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;
use helpme\app\models\AmoLead;
use helpme\app\models\AmoLeads;
use helpme\app\models\eloquent\BitrixDeal;
use helpme\app\models\User;

class ManagerController
{
    protected $auth = null;

    protected $user = null;

    protected $template = null;

    public function __construct()
    {
        $this->auth = new HelpmeAuthorizationController(['root', 'admin', 'manager']);
        $this->user = new User($this->auth->getLogin(), 'helpme');
        if (! $this->user->getActive()) {
            F::error('Аккаунт не активирован');
        }
        $this->template = Engine::template();
        $this->setMenuVars($this->template, str_replace('/', '', Engine::router()->getUri()));
    }

    // сделать проверку доступа пользователя к запрашиваемой группе
    public function page()
    {
        return $this->template->get();
    }

    public function setMenuVars($t = null, $activeSection = null)
    {
        // F::dump($activeSection);
        $menu = new Template('admin/menu');
        $group_id = empty($_GET['groupId']) ? null : F::checkstr($_GET['groupId']);
        // разделы работы с компанией
        $sections = [
            'index' => [
                'groupLink' => '/admin?groupId={groupId}',
            ],
            'transfers' => [
                'groupLink' => '/transfers?groupId={groupId}',
            ],
            'calendar' => [
                'groupLink' => '/?r=Admin/monthlyPayCalendar&amp;groupId={groupId}',
            ],
            'stat' => [
                'groupLink' => '/stat?groupId={groupId}',
            ],
            'notPaid' => [
                'groupLink' => '/notPaidThisMonth?groupId={groupId}',
            ],
            'leads' => [
                'groupLink' => '/leads?groupId={groupId}',
            ],
            'leadsPaidOnMonth' => [
                'groupLink' => '/leadsPaidOnMonth?groupId={groupId}',
            ],
            'salary' => [
                'groupLink' => '/?r=Admin/salary&amp;groupId={groupId}',
            ],
        ];
        if ($this->user->getLevel() == 'root') {
            $arrGroups = F::query_arr('
				select id as group_id, name as group_name
				from '.F::typetab('amo_groups').'
				where `show`
				order by group_name
				;');
        } else {
            // доступные компании пользователя
            $arrGroups = F::query_arr('
				select amo_groups.id as group_id, amo_groups.name as group_name
				from
				'.F::typetab('users_amo_groups').'
				join '.F::typetab('amo_groups').' on amo_groups.id = users_amo_groups.amo_group
				where
				users_amo_groups.login = \''.$this->user->getLogin().'\'
				and amo_groups.show
				order by group_name
				;');
        }
        // получаем список групп для аккаунта
        $list = '';
        $group_name = null;
        foreach ($arrGroups as $v) {
            $isGroupActive = false;
            if ($group_id == $v['group_id']) {
                // получаем название выбранной группы, для dropdown
                $group_name = $v['group_name'];
                $isGroupActive = true;
            }
            $groupsList = new Template('admin/index/item_group');
            $groupsList->v('groupId', $v['group_id']);
            $groupsList->v('groupActive', $isGroupActive ? 'active' : '');
            $groupsList->v('groupName', $v['group_name']);
            $groupsList->v('groupLink', isset($sections[$activeSection]['groupLink']) ? $sections[$activeSection]['groupLink'] : $sections['index']['groupLink']);
            $list .= $groupsList->get();
        }
        // если выбрана группа, то показываем меню групп
        if ($group_id) {
            if (in_array($this->user->getLevel(), ['admin', 'root'])) {
                $groupMenu = new Template('admin/group_menu');
            } else {
                $groupMenu = new Template('manager/group_menu');
            }
            foreach ($sections as $k => $v) {
                $groupMenu->v('menu'.ucfirst($k).'Active', ($k == $activeSection) ? 'active' : '');
            }
            $groupMenu->v('currentGroupId', $group_id);
            //
            $nowDay = new \DateTime;
            $yesterday = (new \DateTime)->modify('-1 day');
            $beforeYesterday = (new \DateTime)->modify('-2 day');
            $lastBitrixUpdateDate = new \DateTime(BitrixDeal::max('updated_at'));
            $shortDate = null;
            if ($lastBitrixUpdateDate->format('Y-m-d') == $nowDay->format('Y-m-d')) {
                $shortDate = 'Сегодня';
            }
            if ($lastBitrixUpdateDate->format('Y-m-d') == $yesterday->format('Y-m-d')) {
                $shortDate = 'Вчера';
            }
            if ($lastBitrixUpdateDate->format('Y-m-d') == $beforeYesterday->format('Y-m-d')) {
                $shortDate = 'Позавчера';
            }
            $groupMenu->v('lastBitrixUpdateDate', ($shortDate ? ($shortDate.' '.$lastBitrixUpdateDate->format('H:i')) : $lastBitrixUpdateDate->format('d.m.Y H:i')));
            //
            $menu->v('groupMenu', $groupMenu->get());
        } else {
            $menu->v('groupMenu', '');
        }
        $menu->v('currentGroupName', $group_name ? $group_name : 'Выберите город');
        $menu->v('listGroups', $list);
        $t->v('menu', $menu->get());

        return true;
    }

    public function notPaidThisMonth()
    {
        $date = new \DateTime;
        $group_id = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        $arr = F::query_arr('
			select (select 1 from '.F::typetab('transfers').' where lead = amo_leads.id and date_format(date,\'%Y-%m\') = \''.$date->format('Y-m').'\' limit 1) as paid, amo_leads.id, amo_leads.name
			from '.F::typetab('amo_leads').'
			join '.F::typetab('amo_statuses').' on amo_statuses.id = amo_leads.status_id
			where group_id = \''.$group_id.'\' and amo_statuses.pay
			and amo_leads.name not like \'%Автосделка%\'
			having paid is null
			');
        $list = '';
        foreach ($arr as $v) {
            $list .= '<li><a href="https://helpme90.amocrm.ru/leads/detail/'.$v['id'].'" target="_blank">'.$v['name'].'</a></li>';
        }
        $t = $this->template;
        $t->v('list', $list);

        return $t->get();
    }

    public function leads()
    {
        $group_id = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        $page = empty($_GET['p']) ? 1 : F::checkstr($_GET['p']);
        $leads = new AmoLeads;
        $leads->setHasPaymentTasks(true);
        $leads->setLimit(100);
        $leads->setPage($page);
        $leads->setGroupId($group_id);
        // $leads->setCreateDate('2019-06-21 09:42:42','2019-06-21 14:21:46');
        $leads->setOrder('amo_leads.date_create');
        $leads->setPaginationUrlPattern('/leads?groupId='.$group_id.'&amp;p=(:num)');
        $arr = $leads->get();
        $list = '';
        foreach ($arr as $v) {
            $listTemplate = new Template('admin/leads/item');
            $lead = new AmoLead($v['id']);
            //
            $payments = $lead->getPaymentTasks();
            $listP = '';
            foreach ($payments as $p) {
                $listP .= '<tr>
				<td style="width: 116px;">'.$p['created_at'].'</td>
				<td>'.$p['text'].'</td>
				<td style="width: 116px;">'.$p['updated_at'].'</td>
				<td>'.($p['is_completed'] ? '<i class="fas fa-check"></i>' : '<i class="fas fa-minus"></i>').' '.$p['result'].'</td>
				</tr>';
            }
            $listTemplate->v('payments', '<table class="table table-sm table-bordered" style="font-size: 10px;">
				<tr><td>Дата создания</td><td>Текст</td><td>Дата изменения</td><td>Результат</td></tr>
				'.$listP.'
				</table>');
            //
            //
            $transfers = $lead->getTransfers();
            $listT = '';
            foreach ($transfers as $tr) {
                $listT .= '<tr>
				<td>'.$tr['id'].'</td>
				<td>'.$tr['date'].'</td>
				<td>'.$tr['text'].'</td>
				<td>'.$tr['value'].'</td>
				<td>'.$tr['source'].'</td>
				</tr>';
            }
            $listTemplate->v('transfers', '<table class="table table-sm table-bordered" style="font-size: 10px;">
				<tr><td>#</td><td>Дата оплаты</td><td>Текст</td><td>Сумма</td><td>Источник</td></tr>
				'.$listT.'
				</table>');
            //
            $listTemplate->v('idUrl', '<a href="https://helpme90.amocrm.ru/leads/detail/'.$lead->getId().'" target="_blank">'.$lead->getId().'</a>');
            $listTemplate->v('nameUrl', '<a href="https://helpme90.amocrm.ru/leads/detail/'.$lead->getId().'" target="_blank">'.$lead->getName().'</a>');
            //
            $list .= $listTemplate->get();
        }
        $t = $this->template;
        $t->v('list', $list);
        $t->v('pagination', $leads->getPages());

        return $t->get();
    }

    public function leadsPaidOnMonth()
    {
        $group_id = empty($_GET['groupId']) ? F::error('Не передан ID группы') : F::checkstr($_GET['groupId']);
        Engine::setDebug(false);
        $month = empty($_GET['month']) ? null : F::checkstr($_GET['month']);
        $t = $this->template;
        if ($month) {
            $currentMonth = new \DateTime($month);
        } else {
            $currentMonth = new \DateTime;
        }
        $prevMonth = (clone $currentMonth)->modify('-1 month');
        $nextMonth = (clone $currentMonth)->modify('+1 month');
        $t->v('currentMonth', $currentMonth->format('Y-m'));
        $t->v('prevMonth', $prevMonth->format('Y-m'));
        $t->v('nextMonth', $nextMonth->format('Y-m'));
        // получаем все активные лиды
        $leads = new AmoLeads;
        $leads->setGroupId($group_id);
        $leads->setOrder('amo_leads.date_create');
        $leads->setPayStatuses(true);
        $leads->setAutoLead(false);
        $leads->setLimit($leads->getRows());
        $arr = $leads->get();
        // получаем все переводы по лидам в указанном месяце
        $transfers = F::query_arr('
			select transfers.id,transfers.date,transfers.value,transfers.text,transfers.source,transfers.lead
			from '.F::typetab('transfers').'
			join '.F::typetab('amo_leads').' on amo_leads.id = transfers.lead
			where date_format(transfers.date,\'%Y-%m\') = \''.$currentMonth->format('Y-m').'\' and lead is not null
			and amo_leads.group_id = \''.$group_id.'\'
			order by date
			;');
        $listTransfers = '';
        foreach ($transfers as $tr) {
            $lead = new AmoLead($tr['lead']);
            $listTransfers .= '<tr>
			<td>'.$tr['id'].'</td>
			<td><a href="https://helpme90.amocrm.ru/leads/detail/'.$lead->getId().'" target="_blank">'.$lead->getName().'</a><br>('.$lead->getStatusName().')</td>
			<td>'.$tr['date'].'</td>
			<td>'.$tr['text'].'</td>
			<td>'.$tr['value'].'</td>
			<td>'.$tr['source'].'</td>
			</tr>';
        }
        //
        $listNotPaid = '';
        foreach ($arr as $v) {
            $lead = new AmoLead($v['id']);
            // получаем все переводы по лиду и смотрим было ли в этом месяце
            // $transfers = $lead->getTransfers();
            $isLeadPaid = false;
            foreach ($transfers as $tr) {
                if ($tr['lead'] == $lead->getId()) {
                    $isLeadPaid = true;
                    break;
                }
            }
            if (! $isLeadPaid) {
                $listNotPaid .= '<li><a href="https://helpme90.amocrm.ru/leads/detail/'.$lead->getId().'" target="_blank">'.$lead->getName().'</a> ('.$lead->getStatusName().')</li>';
            }
        }
        $t->v('listTransfers', '<table class="table table-sm table-bordered">
				<tr><td>#</td><td>Клиент</td><td>Дата оплаты</td><td>Текст</td><td>Сумма</td><td>Источник</td></tr>
				'.$listTransfers.'
				</table>');
        $t->v('listNotPaid', '<ol>'.$listNotPaid.'</ol>');

        return $t->get();
    }
}
