<?php

namespace helpme\app\controllers;

use engine\app\models\F;
use helpme\app\models\bitrix24\Bitrix;
use helpme\app\models\eloquent\BitrixCategory;
use helpme\app\models\eloquent\BitrixCompany;
use helpme\app\models\eloquent\BitrixContact;
use helpme\app\models\eloquent\BitrixDeal;
use helpme\app\models\eloquent\BitrixDealCity;
use helpme\app\models\eloquent\BitrixDealField;
use helpme\app\models\eloquent\BitrixStage;

class UpdateBitrixController
{
    public $auth;

    public function __construct()
    {
        $this->auth = new HelpmeAuthorizationController(['root', 'admin', 'manager']);
    }

    public function updateStart()
    {
        return $this->updateContacts();
    }

    public function updateContacts()
    {
        $next = $_GET['next'] ?? null;
        $params = [
            'start' => $next,
            'select' => ['*', 'UF_*', 'PHONE'],
        ];
        $arr = Bitrix::call('crm.contact.list', $params);
        // F::dump($arr);
        foreach ($arr['result'] as $data) {
            $contact = BitrixContact::findOrNew($data['ID']);
            $contact->id = $data['ID'];
            $contact->json = json_encode($data, JSON_UNESCAPED_UNICODE);
            $contact->save();
        }
        if (empty($arr['next'])) {
            return '/?r=UpdateBitrix/updateCompanies';
        } else {
            return '/?r=UpdateBitrix/updateContacts&next='.$arr['next'];
        }
    }

    public function updateCompanies()
    {
        $next = $_GET['next'] ?? null;
        $params = [
            'start' => $next,
            'select' => ['*', 'UF_*'],
        ];
        $arr = Bitrix::call('crm.company.list', $params);
        foreach ($arr['result'] as $data) {
            $company = BitrixCompany::findOrNew($data['ID']);
            $company->id = $data['ID'];
            $company->json = json_encode($data, JSON_UNESCAPED_UNICODE);
            $company->save();
        }
        if (empty($arr['next'])) {
            return '/?r=UpdateBitrix/updateCategories';
        } else {
            return '/?r=UpdateBitrix/updateCompanies&next='.$arr['next'];
        }
    }

    public function updateCategories()
    {
        $next = $_GET['next'] ?? null;
        $params = [
            'start' => $next,
            'select' => ['*', 'UF_*'],
        ];
        $arr = Bitrix::call('crm.dealcategory.list', $params);
        foreach ($arr['result'] as $data) {
            $category = BitrixCategory::findOrNew($data['ID']);
            $category->id = $data['ID'];
            $category->json = json_encode($data, JSON_UNESCAPED_UNICODE);
            $category->save();
        }
        if (empty($arr['next'])) {
            return '/?r=UpdateBitrix/updateStages';
        } else {
            return '/?r=UpdateBitrix/updateCategories&next='.$arr['next'];
        }
    }

    public function updateStages()
    {
        $categories = BitrixCategory::all();
        foreach ($categories as $category) {
            $params = [
                'select' => ['*', 'UF_*'],
                'ID' => $category->id,
            ];
            $arr = Bitrix::call('crm.dealcategory.stage.list', $params);
            foreach ($arr['result'] as $data) {
                $stage = BitrixStage::findOrNew($data['STATUS_ID']);
                $stage->id = $data['STATUS_ID'];
                $stage->json = json_encode($data, JSON_UNESCAPED_UNICODE);
                $stage->bitrixCategory()->associate($category);
                $stage->save();
            }
        }

        return '/?r=UpdateBitrix/updateDealFields';
    }

    public function updateDealFields()
    {
        $arr = Bitrix::call('crm.deal.fields');
        foreach ($arr['result'] as $field_name => $data) {
            $field = BitrixDealField::findOrNew($field_name);
            $field->field = $field_name;
            $field->json = json_encode($data, JSON_UNESCAPED_UNICODE);
            $field->save();
        }

        return '/?r=UpdateBitrix/updateDealCities';
    }

    public function updateDealCities()
    {
        $arr = BitrixDealField::find('UF_CRM_1618266770570');
        foreach ($arr->data->items as $v) {
            $city = BitrixDealCity::findOrNew($v->ID);
            $city->id = $v->ID;
            $city->value = $v->VALUE;
            $city->save();
        }

        return '/?r=UpdateBitrix/updateDeals';
    }

    public function updateDeals()
    {
        $next = $_GET['next'] ?? null;
        $params = [
            'start' => $next,
            'select' => ['*', 'UF_*'],
        ];
        $arr = Bitrix::call('crm.deal.list', $params);
        foreach ($arr['result'] as $data) {
            $deal = BitrixDeal::findOrNew($data['ID']);
            $deal->id = $data['ID'];
            $deal->json = json_encode($data, JSON_UNESCAPED_UNICODE);
            if (isset($data->CONTACT_ID) and ! BitrixContact::find($data->CONTACT_ID)) {
                F::error('Контакт '.$data->CONTACT_ID.' не найден');
            }
            $deal->setFromJson();
            $deal->save();
        }
        if (empty($arr['next'])) {
            return 'Обновление завершено';
        } else {
            return '/?r=UpdateBitrix/updateDeals&next='.$arr['next'];
        }
    }
}
