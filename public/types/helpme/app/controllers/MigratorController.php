<?php

namespace helpme\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;
use helpme\app\models\AmoLead;
use helpme\app\models\AmoLeads;
use helpme\app\models\bitrix24\Bitrix;
use helpme\app\models\User;

class MigratorController
{
    private $bitrix_fields = null;

    // private $delete_notes = true;
    private $delete_notes = false;

    // private $delete_tasks = true;
    private $delete_tasks = false;

    // private $delete_deal = true;
    private $delete_deal = false;

    public function __construct()
    {
        $this->auth = new HelpmeAuthorizationController(['root', 'admin']);
        $this->user = new User($this->auth->getLogin(), 'helpme');
        if (! $this->user->getActive()) {
            F::error('Аккаунт не активирован');
        }
    }

    public function index()
    {
        $menu = ['deleteDeals', 'migrateLeads'];
        $list = '';
        foreach ($menu as $v) {
            $list .= '<li><a href="/?r=Migrator/'.$v.'">'.$v.'</a></li>';
        }
        F::alertLite('<ul>'.$list.'</ul>');
    }

    // удалить все сделки
    public function deleteDeals()
    {
        F::dump('Запрещено');
        $result = Bitrix::call('crm.deal.list', [
            'select' => ['ID'],
            'start' => -1,
        ]);
        foreach ($result['result'] as $v) {
            Bitrix::call('crm.deal.delete', ['id' => $v['ID']]);
        }
        if (count($result['result'])) {
            F::alert('Удаление
				<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>
				');
        } else {
            F::alert('Все удалено');
        }
    }

    // переносит сделку из амо в битрикс, обновляет, если существует
    public function migrateLeads()
    {
        F::error('Запрещено');
        Engine::setDisplayErrors(true);
        $page = $_GET['page'] ?? 1;
        // $groupId = 257463;
        $groupId = $_GET['groupId'] ?? F::error('Не передана группа');
        $limit = 1;
        $leads = new AmoLeads;
        $leads->setGroupId($groupId);
        $leads->setLimit($limit);
        $leads->setPage($page);
        $arr = $leads->get();
        $rows = $leads->getRows();
        $list = '';
        foreach ($arr as $v) {
            $lead = new AmoLead($v['id']);
            // f::dump($lead);
            $result = $this->migrateLead($lead->getId());
            $list .= '
			<li>
			'.$result->text.'
			<ul>
			<li>'.($this->migrateContact($lead->getContactId(), $result->bitrix_deal_id)->text).'</li>
			<li>'.($this->migrateNotes($lead->getId(), $result->bitrix_deal_id) ? 'Комментарии обновлены' : 'Ошибка обновления комментариев').'</li>
			</ul>
			</li>';
            // <li>'.($this->migrateTasks($lead->getId(),$result->bitrix_deal_id)?'Задачи обновлены':'Ошибка обновления задач').'</li>
        }
        if (empty($arr)) {
            F::alertLite('Обновление лидов завершено');
        } else {
            F::alertLite('
			Осталось: '.($rows - $limit * $page).'
			<ul>'.$list.'</ul>
			<script>
			setTimeout(window.location.replace(\'/?r=Migrator/migrateLeads&groupId='.$groupId.'&page='.($page + 1).'\'),5000);
			</script>
			');
        }
    }

    // перенести amoLeadId в битрикс, обновить, если существует
    public function migrateLead($amoLeadId = null)
    {
        $lead = new AmoLead($amoLeadId);
        // F::dump($lead->getData());
        $fields = $this->getBitrixFields();
        // проверяем есть ли такой лид в битриксе
        $result = Bitrix::call('crm.deal.list', [
            'select' => [$fields['deal']['amo_lead_id']],
            'filter' => [$fields['deal']['amo_lead_id'] => $lead->getId()],
        ]);
        // удаляем существующий лид, если нужно
        if ($this->delete_deal and isset($result['result'][0]['ID'])) {
            $this->deleteBitrixDeal($result['result'][0]['ID']);
            unset($result);
        }
        $bitrixDataByAmoStatusId = $this->getBitrixDataByAmoStatusId($lead->getStatusId());
        // если не найдено, то добавляем
        if (empty($result['result'])) {
            // F::dump($lead->getName());
            $resultAdd = Bitrix::call('crm.deal.add', [
                'fields' => [
                    'CATEGORY_ID' => $bitrixDataByAmoStatusId->category_id,
                    $fields['deal']['amo_lead_id'] => $lead->getId(),
                ],
            ]);
            // F::dump($resultAdd);
            $bitrixDealId = $resultAdd['result'];
            $resultLead = 'Добавлен лид '.$lead->getId().' => '.$bitrixDealId;
        } else {
            $bitrixDealId = $result['result'][0]['ID'];
            $resultLead = 'Лид '.$lead->getId().' существует => '.$bitrixDealId;
        }
        // обновляем инфу в сделке
        $resultUpdate = Bitrix::call('crm.deal.update', [
            'id' => $bitrixDealId,
            'fields' => [
                'TITLE' => $lead->getName(),
                'COMPANY_ID' => $this->getBitrixGroupIdByAmoGroupId($lead->getGroupId()),
                'STAGE_ID' => $bitrixDataByAmoStatusId->stage_id,
                'CATEGORY_ID' => $bitrixDataByAmoStatusId->category_id,
                'ASSIGNED_BY_ID' => $this->getBitrixUserIdByAmoUserId($lead->getData()->responsible_user_id),
                $fields['deal']['amo_lead_id'] => $lead->getId(),
                $fields['deal']['amo_contact_id'] => $lead->getContactId(),
                $fields['deal']['amo_status_id'] => $lead->getStatusId(),
                $fields['deal']['amo_status_name'] => $lead->getStatusName(),
                // сумма долга
                'UF_CRM_1616052454142' => $this->getChangedValue($lead->getContactCustomFieldValueById(637204), [
                    'До 50 тыс. руб.' => 44,
                    'До 100 тыс. руб.' => 44,
                    'До 200 тыс. руб.' => 44,
                    'До 300 тыс. руб.' => 44,
                    'До 400 тыс. руб.' => 46,
                    'До 500 тыс. руб.' => 48,
                    'До 600 тыс. руб.' => 182,
                    'До 700 тыс. руб.' => 50,
                    'До 800 тыс. руб.' => 52,
                    'До 900 тыс. руб.' => 54,
                    'До 1 млн. руб.' => 56,
                    'Свыше 1 млн. руб.' => 56,
                    'Свыше 1 млн. 500 тыс. руб.' => 56,
                    'Свыше 2 млн. руб.' => 56,
                    'Свыше 2 млн. 500 тыс. руб.' => 56,
                    'Свыше 3 млн. руб.' => 56,
                    'Свыше 3 млн. 500 тыс. руб.' => 56,
                    'Свыше 4 млн. руб.' => 56,
                    'Свыше 4 млн. 500 тыс. руб.' => 56,
                    'Более 5 млн. руб.' => 56,
                    'Неизвестно' => 0,
                ]),
                // фио +
                'UF_CRM_1618264791545' => $lead->getCustomFieldValueById(440231),
                // кем выдан +
                'UF_CRM_1618265295658' => $lead->getCustomFieldValueById(440263),
                // предоплата +
                'UF_CRM_1618937279511' => $lead->getCustomFieldValueById(440301),
                // какого числа оплаты +
                'UF_CRM_1619193567741' => $lead->getCustomFieldValueById(440319),
                // сумма ежемесячных оплат +
                'UF_CRM_1618266826927' => $lead->getCustomFieldValueById(440389),
                // дата ежемесячных оплат +
                'UF_CRM_1618264415437' => $lead->getCustomFieldValueById(529445),
                // код подразделения +
                'UF_CRM_1618265466533' => $lead->getCustomFieldValueById(542167),
                // дата рождения +
                'UF_CRM_1618265524789' => $lead->getCustomFieldValueById(576457),
                // место рождения +
                'UF_CRM_1618265653040' => $lead->getCustomFieldValueById(576459),
                // инн
                'UF_CRM_1619193460890' => $lead->getCustomFieldValueById(576465),
                // адрес проживания +
                'UF_CRM_1618264944025' => $lead->getCustomFieldValueById(577267),
                // количество месяцев для оплаты +
                'UF_CRM_1619193761090' => $lead->getCustomFieldValueById(599691),
                // дата заключения договора +
                'UF_CRM_1618264351273' => $lead->getCustomFieldValueById(604743),
                // номер договора +
                'UF_CRM_1618264325155' => $lead->getCustomFieldValueById(604745),
                // место работы +
                'UF_CRM_1619193124432' => $lead->getCustomFieldValueById(605001),
                // пол +
                'UF_CRM_1618264623337' => $this->getChangedValue($lead->getCustomFieldValueById(616468), [
                    'мужской' => 110,
                    'женский' => 176,
                ]),
                // перечень для выбора источника обращения клиента +
                'UF_CRM_1618263219616' => $this->getChangedValue($lead->getCustomFieldValueById(621020), [
                    'ТВ' => 72,
                    'Интернет' => 74,
                    'Goodlead (Интернет)' => 74,
                    'Brandmaker (Интернет)' => 74,
                    'Знакомые' => 76,
                    'Газета' => 78,
                    'Наружная реклама' => 80,
                    'Лифт' => 82,
                    'Радио' => 84,
                    'Листовки' => 86,
                    'Маршрутки' => 88,
                    'Нет данных' => 0,
                    'С улицы' => 0,
                    'Метро' => 0,
                    'Юрист' => 0,
                    'VK' => 90,
                    'Одноклассники' => 92,
                    'Instagram' => 94,
                    'Facebook' => 96,
                    'Бегущая строка' => 0,
                ]),
                // полных лет +
                'UF_CRM_1618265546556' => $lead->getCustomFieldValueById(626952),
                // фио родственника +
                'UF_CRM_1619193076673' => $lead->getCustomFieldValueById(632236),
                // контактный телефон родственника +
                'UF_CRM_1619193023573' => $lead->getCustomFieldValueById(632238),
                // серия паспорта +
                'UF_CRM_1618265884810' => $lead->getCustomFieldValueById(634758),
                // номер паспорта +
                'UF_CRM_1618264992498' => $lead->getCustomFieldValueById(634760),
                // снилс
                'UF_CRM_1619192883130' => $lead->getCustomFieldValueById(634956),
                // требуется ли сбор дебиторки +
                'UF_CRM_1619192986535' => $this->getChangedValue($lead->getCustomFieldValueById(637710), [
                    'Да' => 172,
                    'Нет' => 174,
                ]),
                // город обращения
                'UF_CRM_1618266770570' => $lead->getCustomFieldValueById(1100401),
                // Сумма по договору +
                'UF_CRM_1619549248827' => $lead->getData()->price,
                // '' => $lead->getCustomFieldValueById(),
                // '' => $lead->getCustomFieldValueById(),
                // '' => $lead->getCustomFieldValueById(),
                // '' => $lead->getCustomFieldValueById(),
                // '' => $lead->getCustomFieldValueById(),
                // '' => $lead->getCustomFieldValueById(),
                // '' => $lead->getCustomFieldValueById(),
            ],
        ]);
        $result = new \stdClass;
        $result->error = false;
        $result->text = $resultLead;
        $result->bitrix_deal_id = $bitrixDealId;

        return $result;
    }

    // переносит контакт из амо в битрикс, обновляет если существует
    public function migrateContact($amoContactId = null, $bitrixDealId = null)
    {
        if (empty($amoContactId)) {
            $result = new \stdClass;
            $result->error = true;
            $result->text = 'Контакт не передан';

            return $result;
        }
        $fields = $this->getBitrixFields();
        $arr = F::query_assoc('
			select id,json,phone
			from '.F::typetab('amo_contacts').'
			where id = \''.$amoContactId.'\'
			');
        if (empty($arr)) {
            $result = new \stdClass;
            $result->error = true;
            $result->text = 'Контакт не найден в базе';

            return $result;
        }
        $contact = json_decode($arr['json']);
        // проверяем есть ли такой лид в битриксе
        $result = Bitrix::call('crm.contact.list', [
            'select' => [$fields['contact']['amo_contact_id']],
            'filter' => [$fields['contact']['amo_contact_id'] => $arr['id']],
        ]);
        // если не найдено, то добавляем
        if (empty($result['result'])) {
            $resultAdd = Bitrix::call('crm.contact.add', [
                'fields' => [
                    $fields['contact']['amo_contact_id'] => $arr['id'],
                ],
            ]);
            // F::dump($resultAdd);
            $bitrixContactId = $resultAdd['result'];
            $return = 'Добавлен контакт '.$arr['id'].' => '.$bitrixContactId;
        } else {
            $bitrixContactId = $result['result'][0]['ID'];
            // $resultGet = Bitrix::call('crm.contact.get',['id' => $bitrixContactId]);
            // F::dump($resultGet);
            $return = 'Контакт '.$arr['id'].' существует => '.$bitrixContactId;
        }
        // обновляем инфу в контакте
        $resultUpdate = Bitrix::call('crm.contact.update', [
            'id' => $bitrixContactId,
            'fields' => [
                'NAME' => $contact->name,
                'PHONE' => [
                    [
                        'VALUE' => $arr['phone'],
                        'VALUE_TYPE' => 'WORK',
                    ],
                ],
            ],
        ]);
        // добавляем контакт к сделке
        Bitrix::call('crm.deal.contact.add', [
            'id' => $bitrixDealId,
            'fields' => [
                'CONTACT_ID' => $bitrixContactId,
            ],
        ]);
        $result = new \stdClass;
        $result->bitrix_contact_id = $bitrixContactId;
        $result->text = $return;
        $result->error = false;

        return $result;
    }

    // переносит заметки из амо в ленту битрикса
    public function migrateNotes($amoLeadId = null, $bitrixDealId = null)
    {
        // $amoLeadId =  6637131; // агапов завершенный
        // $bitrixDealId = 8370; // агапов завершенный
        // $bitrixDealId = 11632; // тест комментов
        if (empty($amoLeadId)) {
            F::error('Не передан amoLeadId');
        }
        if (empty($bitrixDealId)) {
            F::error('Не передан bitrixDealId');
        }
        // удаляем все комменты сделки
        if ($this->delete_notes) {
            $this->deleteBitrixDealComments($bitrixDealId);
        }
        // получаем все имеющиеся комменты сделки
        $bitrixComments = $this->getBitrixDealComments($bitrixDealId);
        // F::dump($bitrixComments);
        $bitrixCommentsArr = [];
        foreach ($bitrixComments as $bitrixComment) {
            preg_match('/amo_note_id: ([0-9]+)/', $bitrixComment['COMMENT'], $matches);
            $amo_note_id = $matches[1] ?? null;
            if ($amo_note_id) {
                $bitrixCommentsArr[$amo_note_id] = $bitrixComment['ID'];
            }
        }
        // получаем все заметки из амо
        $amoNotes = F::query_arr('
			select json 
			from '.F::typetab('amo_notes').' 
			where 
			lead = \''.$amoLeadId.'\'
			order by created_at
			;');
        // foreach ($amoNotes as $k => $v) {
        // 	$amoNotes[$k] = json_decode($v['json']);
        // }
        // F::dump($amoNotes[8]);
        foreach ($amoNotes as $k => $amoNote) {
            $amoNote = json_decode($amoNote['json']);
            $text = '';
            if (empty($amoNote->text)) {
                if (isset($amoNote->params->TEXT)) {
                    if (isset($amoNote->params->HTML)) {
                        if (preg_match('/<a href=\"(.+)\">/', $amoNote->params->HTML, $matches)) {
                            $text = '[URL='.$matches[1].']'.$amoNote->params->TEXT.'[/URL]';
                        }
                    } else {
                        $text = $amoNote->params->TEXT;
                    }
                }
            } else {
                $text = $amoNote->text;
                $text = htmlspecialchars_decode($text);
                $text = str_replace('<br>', '', $text);
            }
            // $amoNote->newText = $text;
            // $amoNotes[$k] = $amoNote;
            // if ($amoNote->id == 58303077) {
            // 	F::dump($amoNote);
            // }
            // если такой коммент есть, то обновляем, если нет - добавляем
            $date_created = (new \DateTime)->setTimestamp($amoNote->created_at);
            $date_updated = (new \DateTime)->setTimestamp($amoNote->updated_at);
            $text = '[SIZE=6pt]amo_note_id: '.$amoNote->id.', созд.: '.$date_created->format('Y-m-d H:i:s').', изм.: '.$date_updated->format('Y-m-d H:i:s')."\r\n".'[/SIZE]'.$text;
            if (isset($bitrixCommentsArr[$amoNote->id])) {
                $result = Bitrix::call('crm.timeline.comment.update', [
                    'id' => $bitrixCommentsArr[$amoNote->id],
                    'fields' => [
                        'COMMENT' => $text,
                    ],
                ]);
            } else {
                $result = Bitrix::call('crm.timeline.comment.add', [
                    'fields' => [
                        'ENTITY_ID' => $bitrixDealId,
                        'ENTITY_TYPE' => 'deal',
                        'COMMENT' => $text,
                    ],
                ]);
            }
        }

        return true;
    }

    // перенести все задачи лида amoLeadId в сделку bitrixDealId
    public function migrateTasks($amoLeadId = null, $bitrixDealId = null)
    {
        if (empty($amoLeadId)) {
            F::error('Не передан amoLeadId');
        }
        if (empty($bitrixDealId)) {
            F::error('Не передан bitrixDealId');
        }
        if ($this->delete_tasks) {
            $this->deleteBitrixDealTasks($bitrixDealId);
        }
        $bitrixTasks = $this->getBitrixDealTasks($bitrixDealId);
        // F::dump($bitrixTasks);
        $amoTasks = F::query_arr('
			select json
			from '.F::typetab('amo_tasks').' 
			where 
			lead = \''.$amoLeadId.'\'
			order by created_at
			;');
        // F::dump(Bitrix::call('crm.enum.activitytype'));
        foreach ($amoTasks as $amoTask) {
            $amoTask = json_decode($amoTask['json']);
            $bitrixTaskId = null;
            foreach ($bitrixTasks as $bitrixTask) {
                // если есть такая задача - то обновить
                if ($bitrixTask['ORIGIN_ID'] == $amoTask->id) {
                    $bitrixTaskId = $bitrixTask['ID'];
                }
            }
            // если задача не была найдена в сделке, то создаем
            $bitrixActivityFields = [
                'OWNER_ID' => $bitrixDealId,
                'OWNER_TYPE_ID' => 2,
                'ORIGIN_ID' => $amoTask->id,
                'TYPE_ID' => 6,
                'PROVIDER_ID' => 'TASKS',
                'PROVIDER_TYPE_ID' => 'TASKS',
                'SUBJECT' => 'Задача',
                'END_TIME' => '2021-04-27T19:00:00+03:00',
                'DEADLINE' => '2021-04-27T19:00:00+03:00',
                'STATUS' => '1',
                'DESCRIPTION' => $amoTask->text,
                'DESCRIPTION_TYPE' => 2,
            ];
            // $bitrixActivityFields = [
            // 	'ID' => '1756',
            // 	'OWNER_ID' => '6902',
            // 	'OWNER_TYPE_ID' => '2',
            // 	'TYPE_ID' => '3',
            // 	'PROVIDER_ID' => 'TASKS',
            // 	'PROVIDER_TYPE_ID' => 'TASK',
            // 	'PROVIDER_GROUP_ID' => null,
            // 	'ASSOCIATED_ENTITY_ID' => '746',
            // 	'SUBJECT' => 'CRM: Зголовок задачи',
            // 	'CREATED' => '2021-04-26T17:06:39+03:00',
            // 	'LAST_UPDATED' => '2021-04-26T17:06:39+03:00',
            // 	'START_TIME' => '2021-04-27T19:00:00+03:00',
            // 	'END_TIME' => '2021-04-27T19:00:00+03:00',
            // 	'DEADLINE' => '2021-04-27T19:00:00+03:00',
            // 	'COMPLETED' => 'N',
            // 	'STATUS' => '1',
            // 	'RESPONSIBLE_ID' => '36',
            // 	'PRIORITY' => '2',
            // 	'NOTIFY_TYPE' => '0',
            // 	'NOTIFY_VALUE' => '0',
            // 	'DESCRIPTION' => 'Текст задачи',
            // 	'DESCRIPTION_TYPE' => '2',
            // 	'DIRECTION' => '0',
            // 	'LOCATION' => null,
            // 	'ORIGINATOR_ID' => null,
            // 	'ORIGIN_ID' => null,
            // 	'AUTHOR_ID' => '36',
            // 	'EDITOR_ID' => '36',
            // 	'PROVIDER_DATA' => null,
            // 	'RESULT_MARK' => '0',
            // 	'RESULT_VALUE' => null,
            // 	'RESULT_SUM' => null,
            // 	'RESULT_CURRENCY_ID' => null,
            // 	'RESULT_STATUS' => '0',
            // 	'RESULT_STREAM' => '0',
            // 	'RESULT_SOURCE_ID' => null,
            // 	'AUTOCOMPLETE_RULE' => '0'
            // ];
            if (! $bitrixTaskId) {
                $result = Bitrix::call('crm.activity.add', [
                    'fields' => $bitrixActivityFields,
                ]);
                $bitrixTaskId = $result['result'] ?? F::dump($result);
            }
            // F::dump($amoTask);
            F::dump($result);
            $result = Bitrix::call('crm.activity.update', [
                'id' => $bitrixTaskId,
                'fields' => $bitrixActivityFields,
            ]);
        }

        return true;
    }

    // получить все заметки сделки
    public function getBitrixDealComments($bitrixDealId = null)
    {
        $next = true;
        $resultArr = [];
        while ($next) {
            $result = Bitrix::call('crm.timeline.comment.list', [
                'filter' => [
                    'ENTITY_ID' => $bitrixDealId,
                    'ENTITY_TYPE' => 'deal',
                ],
                'start' => $next,
            ]);
            foreach ($result['result'] as $item) {
                $resultArr[] = $item;
            }
            $next = $result['next'] ?? null;
        }

        return $resultArr;
    }

    // удалить все заметки сделки
    public function deleteBitrixDealComments($bitrixDealId = null)
    {
        $comments = $this->getBitrixDealComments($bitrixDealId);
        foreach ($comments as $item) {
            Bitrix::call('crm.timeline.comment.delete', [
                'id' => $item['ID'],
            ]);
        }

        return true;
    }

    // удалить сделку
    public function deleteBitrixDeal($bitrixDealId = null)
    {
        Bitrix::call('crm.deal.delete', ['id' => $bitrixDealId]);

        return true;
    }

    // поулчить все задачи сделки
    public function getBitrixDealTasks($bitrixDealId = null)
    {
        $next = true;
        $resultArr = [];
        while ($next) {
            $result = Bitrix::call('crm.activity.list', [
                // $result = Bitrix::call('tasks.task.list',[
                'filter' => [
                    'OWNER_ID' => $bitrixDealId,
                    'OWNER_TYPE_ID' => 2,
                ],
                'start' => $next,
            ]);
            foreach ($result['result'] as $item) {
                $resultArr[] = $item;
            }
            $next = $result['next'] ?? null;
        }

        return $resultArr;
    }

    // удалить все задачи сделки
    public function deleteBitrixDealTasks($bitrixDealId = null)
    {
        $tasks = $this->getBitrixDealTasks($bitrixDealId);
        foreach ($tasks as $item) {
            Bitrix::call('crm.activity.delete', [
                'id' => $item['ID'],
            ]);
        }

        return true;
    }

    // вернуть значение value замещенное значением из массива
    public function getChangedValue($value, $changes = [])
    {
        if (empty($changes)) {
            F::error('changes required');
        }
        foreach ($changes as $from => $to) {
            if ($value == $from) {
                return $to;
            }
        }

        return $value;
    }

    // получить ID пользователя битрикса по amoUserId
    public function getBitrixUserIdByAmoUserId($amoUserId = null)
    {
        if (empty($amoUserId)) {
            return false;
        }
        $arr = F::query_assoc('
			select bitrix_user_id
			from '.F::typetab('amo_users').'
			where user_id = \''.$amoUserId.'\'
			;');

        return $arr['bitrix_user_id'] ?? null;
    }

    // получить ID полей битрикса по их названию
    public function getBitrixFields()
    {
        if (isset($this->bitrix_fields)) {
            return $this->bitrix_fields;
        }
        $bx_fields['deal'] = Bitrix::call('crm.deal.fields');
        $bx_fields['contact'] = Bitrix::call('crm.contact.fields');
        $fields = [
            'deal' => [
                'amo_lead_id' => null,
                'amo_contact_id' => null,
                'amo_status_id' => null,
                'amo_status_name' => null,
            ],
            'contact' => [
                'amo_contact_id' => null,
            ],
        ];
        foreach ($fields as $table => $fields_arr) {
            foreach ($fields_arr as $field => $field_name) {
                foreach ($bx_fields[$table]['result'] as $bx_field_name => $bx_field_data) {
                    if (isset($bx_field_data['listLabel']) and $bx_field_data['listLabel'] === $field) {
                        $fields[$table][$field] = $bx_field_name;
                    }
                }
                if (empty($fields[$table][$field])) {
                    F::error('Не удается найти соответствие для поля '.$table.'['.$field.']');
                }
            }
        }
        $this->bitrix_fields = $fields;

        return $this->bitrix_fields;
    }

    public function test()
    {
        F::dump($this->getBitrixFields());
    }

    // получить ID компании битрикса по amoGroupId
    public function getBitrixGroupIdByAmoGroupId($amoGroupId = null)
    {
        if (empty($amoGroupId)) {
            return false;
        }
        $arr = F::query_assoc('
			select bitrix_group_id
			from '.F::typetab('amo_groups').'
			where id = \''.$amoGroupId.'\'
			;');

        return $arr['bitrix_group_id'] ?? F::error('Не найдено компании битрикса, соответствующей AmoGroupId: '.$amoGroupId);
    }

    // получить данные этапа битрикса по amoStatusId
    public function getBitrixDataByAmoStatusId($amoStatusId = null)
    {
        if (empty($amoStatusId)) {
            return false;
        }
        $arr = F::query_assoc('
			select bitrix_stage_id,bitrix_category_id
			from '.F::typetab('amo_statuses').'
			where id = \''.$amoStatusId.'\'
			;');
        $result = new \stdClass;
        $result->category_id = $arr['bitrix_category_id'] ?? null;
        $result->stage_id = $arr['bitrix_stage_id'] ?? null;

        return $result;
    }
}
