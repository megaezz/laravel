<?php

namespace helpme\app\models;

use engine\app\models\F;

class User extends \engine\app\models\User
{
    private $active = null;

    public function __construct($login = null)
    {
        parent::__construct($login, 'helpme');
        $arr = F::query_assoc('select active
			from '.F::typetab('users').'
			where login = \''.$login.'\'
			;');
        $this->active = $arr['active'];
    }

    public function getActive()
    {
        return $this->active;
    }
}
