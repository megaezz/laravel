<?php

namespace helpme\app\models;

use engine\app\models\F;

class AmoLead
{
    public function __construct($id = null)
    {
        if (! $id) {
            F::error('AmoLead ID is required to create AmoLead class');
        }
        $arr = F::query_assoc('select id,name,price,date_create,last_modified,status_id,monthlyPayDay,monthlyPaySum,
			prepayment,prepaymentDate,contractDate,company_id,monthlyPayDate,json,payMonths,contractId,
			utm_source,utm_medium,utm_campaign,utm_term,utm_content,utm_referer,cancel_reason,group_id,status_name,contact_id
			from '.F::typetab('amo_leads').'
			where id = \''.$id.'\'
			;');
        if (empty($arr)) {
            F::error('Lead not found');
        }
        $this->id = $arr['id'];
        $this->name = $arr['name'];
        $this->price = $arr['price'];
        $this->date_create = $arr['date_create'];
        $this->last_modified = $arr['last_modified'];
        $this->status_id = $arr['status_id'];
        $this->monthlyPayDay = $arr['monthlyPayDay'];
        $this->monthlyPaySum = $arr['monthlyPaySum'];
        $this->prepayment = $arr['prepayment'];
        $this->prepaymentDate = $arr['prepaymentDate'];
        $this->contractDate = $arr['contractDate'];
        $this->company_id = $arr['company_id'];
        $this->monthlyPayDate = $arr['monthlyPayDate'];
        $this->json = $arr['json'];
        $this->payMonths = $arr['payMonths'];
        $this->contractId = $arr['contractId'];
        $this->utm_source = $arr['utm_source'];
        $this->utm_medium = $arr['utm_medium'];
        $this->utm_campaign = $arr['utm_campaign'];
        $this->utm_term = $arr['utm_term'];
        $this->utm_content = $arr['utm_content'];
        $this->utm_referer = $arr['utm_referer'];
        $this->cancel_reason = $arr['cancel_reason'];
        $this->group_id = $arr['group_id'];
        $this->status_name = $arr['status_name'];
        $this->contact_id = $arr['contact_id'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPaymentTasks()
    {
        $arr = F::query_arr('
			select created_at,updated_at,is_completed,text,result
			from '.F::typetab('amo_tasks').'
			where lead = '.$this->getId().' and task_type = 991435
			order by created_at
			;');

        return $arr;
    }

    public function getTransfers()
    {
        $arr = F::query_arr('
			select id,date,value,text,source
			from '.F::typetab('transfers').'
			where lead = '.$this->getId().'
			order by date
			;');

        return $arr;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getData()
    {
        return json_decode($this->json);
    }

    public function getCustomFieldValueById($fieldId = null)
    {
        foreach ($this->getData()->custom_fields as $field) {
            if ($field->id === $fieldId) {
                return $field->values[0]->value;
            }
        }

        return null;
        // F::error('Custom field is not exist. AmoLead: '.$this->getId().'. FieldId: '.$fieldId);
    }

    public function getContactCustomFieldValueById($fieldId = null)
    {
        $arr = F::query_assoc('select json from '.F::typetab('amo_contacts').' where id = \''.$this->getContactId().'\';');
        if (empty($arr['json'])) {
            return null;
        }
        $data = json_decode($arr['json']);
        foreach ($data->custom_fields as $field) {
            if ($field->id === $fieldId) {
                return $field->values[0]->value;
            }
        }

        return null;
        // F::error('Custom field is not exist. AmoLead: '.$this->getId().'. AmoContact: '.$this->getContactId().'. FieldId: '.$fieldId);
    }

    public function isTerminated()
    {
        if (! empty($this->getContractDate()) and $this->getStatusId() == 143) {
            return true;
        }

        return false;
    }

    public function getContractDate()
    {
        return $this->contractDate;
    }

    public function getContractId()
    {
        return $this->contractId;
    }

    public function getStatusId()
    {
        return $this->status_id;
    }

    public function getStatusName()
    {
        return $this->status_name;
    }

    public function getContactId()
    {
        return $this->contact_id;
    }

    public function getGroupId()
    {
        return $this->group_id;
    }
}
