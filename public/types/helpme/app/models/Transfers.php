<?php

namespace helpme\app\models;

use engine\app\models\F;

class Transfers extends \engine\app\models\Lists
{
    private $group_id = null;

    private $source = null;

    private $withLead = null;

    private $withReceipt = null;

    public function __construct()
    {
        $this->setItemClass('\helpme\app\models\Transfer');
    }

    public function get()
    {
        $sql = $this->getSql();
        $sql['source'] = $this->getSource() ? ('(transfers.source = \''.F::escape_string($this->getSource()).'\' or transfers.to_source = \''.F::escape_string($this->getSource()).'\')') : 1;
        // $sql['to_source'] = $this->getToSource()?('transfers.to_source = \''.F::escape_string($this->getToSource()).'\''):1;
        $sql['group_id'] = $this->getGroupId() ? ('transfers.amo_group = \''.F::escape_string($this->getGroupId()).'\'') : 1;
        $sql['withLead'] = is_null($this->getWithLead()) ? 1 : ($this->getWithLead() ? 'transfers.lead is not null' : 'transfers.lead is null');
        $sql['withReceipt'] = is_null($this->getWithReceipt()) ? 1 : ($this->getWithReceipt() ? 'transfers.receipt_id is not null' : 'transfers.receipt_id is null');
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS transfers.id
			from '.F::typetab('transfers').'
			where
			'.$sql['source'].' and
			'.$sql['group_id'].' and
			'.$sql['withLead'].' and
			'.$sql['withReceipt'].'
			'.$sql['order'].'
			'.$sql['limit'].'
		;');
        $this->setRows(F::rows_without_limit());

        return $arr;
    }

    public function getBalance()
    {
        $source = $this->getSource() ?? F::error('set Source to use Transfers::getBalance');
        $group_id = $this->getGroupId() ?? F::error('set GroupId to use Transfers::getBalance');
        $arr = F::query_assoc('
			select SQL_CALC_FOUND_ROWS sum(transfers.value) as sum
			from '.F::typetab('transfers').'
			where
			source = \''.F::escape_string($this->getSource()).'\' and
			transfers.amo_group = \''.F::escape_string($this->getGroupId()).'\'
		;');
        $sourceSum = $arr['sum'];
        $arr = F::query_assoc('
			select SQL_CALC_FOUND_ROWS sum(-transfers.value) as sum
			from '.F::typetab('transfers').'
			where
			to_source = \''.F::escape_string($this->getSource()).'\' and
			transfers.amo_group = \''.F::escape_string($this->getGroupId()).'\'
		;');
        $toSourceSum = $arr['sum'];

        return $sourceSum + $toSourceSum;
    }

    protected function setListVars($t = null, $transfer = null)
    {
        $t->v('id', $transfer->getId());
        $t->v('date', $transfer->getDate());
        $t->v('type', $transfer->getType());
        $t->v('text', $transfer->getText());
        $t->v('source', $transfer->getSource());
        $t->v('to_source', $transfer->getToSource());
        $t->v('lead_id', $transfer->getLeadId());
        $t->v('group_id', $transfer->getGroupId());
        $t->v('receipt_id', $transfer->getReceiptId());
        $t->v('bitrix_deal_id', $transfer->getBitrixDealId());
        $t->v('modulbank_id', $transfer->getModulbankId());
        $t->v('value', $transfer->getValue());
    }

    public function getGroupId()
    {
        return $this->group_id;
    }

    public function setGroupId($group_id = null)
    {
        $this->group_id = $group_id;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setSource($source = null)
    {
        $this->source = $source;
    }

    public static function getSources()
    {
        $arr = F::query_arr('select distinct source from '.F::typetab('transfers').';');
        $sources = [];
        foreach ($arr as $v) {
            $sources[] = $v['source'];
        }

        return $sources;
    }

    public function getWithLead()
    {
        return $this->withLead;
    }

    public function getWithReceipt()
    {
        return $this->withReceipt;
    }

    public function setWithLead($b = null)
    {
        $this->withLead = $b;
    }

    public function setWithReceipt($b = null)
    {
        $this->withReceipt = $b;
    }
}
