<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Transfer extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    protected $database = 'helpme';

    // const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'update_date';

    public function amoLead()
    {
        return $this->belongsTo(AmoLead::class, 'lead');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'login');
    }

    public function amoGroup()
    {
        return $this->belongsTo(AmoGroup::class, 'amo_group');
    }

    public function bitrixDeal()
    {
        return $this->belongsTo(BitrixDeal::class);
    }

    public function bitrixContact()
    {
        return $this->belongsTo(BitrixContact::class);
    }

    public function receipt()
    {
        return $this->belongsTo(Receipt::class);
    }

    public function modulkassaReceipt()
    {
        return $this->belongsTo(ModulkassaReceipt::class);
    }

    public function getMomentBalanceAttribute()
    {
        $source = static::where('source', $this->source)
            ->where('amo_group', $this->amo_group)
            ->where('date', '<=', $this->date);
        $source->sum('value');
        $toSource = static::where('to_source', $this->source)
            ->where('amo_group', $this->amo_group)
            ->where('date', '<=', $this->date);

        return $source->sum('value') - $toSource->sum('value');
    }

    public function scopeVerified($query, $b = null)
    {
        return $query->where('verified', $b);
    }

    public function scopeGroupId($query, $group_id = null)
    {
        return $query->where('amo_group', $group_id);
    }

    public function scopeWithLead($query, $b = null)
    {
        return $b ? $query->whereNotNull('bitrix_contact_id') : $query->whereNull('bitrix_contact_id');
    }

    public function scopeWithReceipt($query, $b = null)
    {
        return $b ? $query->whereNotNull('receipt_id') : $query->whereNull('receipt_id');
    }

    public function scopeSource($query, $source = null)
    {
        return $query->where(
            function ($query) use ($source) {
                $query->orWhere('source', $source)
                    ->orWhere('to_source', $source);
            });
    }

    public function scopeTextSearch($query, $text = null)
    {
        return $query->where('text', 'like', '%'.$text.'%');
    }

    public function scopeDuplicates($query)
    {
        return $query
            ->whereNotNull('transfers.bitrix_contact_id')
            ->whereExists(function ($query) {
                $query
                    ->select(Transfer::raw('count(*) as q'))
                    ->from('transfers', 't')
                    ->whereColumn('t.date', 'transfers.date')
                    ->whereColumn('t.value', 'transfers.value')
                    ->whereColumn('t.bitrix_contact_id', 'transfers.bitrix_contact_id')
                    ->groupBy('date')
                    ->groupBy('bitrix_contact_id')
                    ->groupBy('value')
                    ->having('q', '>', '1');
            })
            ->orderBy('transfers.date')
            ->orderBy('transfers.bitrix_contact_id')
            ->orderBy('transfers.value');
    }
}
