<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class BitrixDealField extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $primaryKey = 'field';

    protected $keyType = 'string';

    protected $database = 'helpme';

    public function getDataAttribute()
    {
        return json_decode($this->json);
    }

    public function setDataAttribute($data = null)
    {
        $this->json = json_encode($data, JSON_UNESCAPED_UNICODE);
    }
}
