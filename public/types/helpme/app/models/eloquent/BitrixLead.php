<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class BitrixLead extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $database = 'helpme';

    public function getDataAttribute()
    {
        return json_decode($this->json);
    }
}
