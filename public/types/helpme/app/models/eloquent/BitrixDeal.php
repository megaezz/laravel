<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class BitrixDeal extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $database = 'helpme';

    protected $appends = ['data'];

    protected $hidden = ['json'];

    public function amoLead()
    {
        return $this->belongsTo(AmoLead::class);
    }

    public function bitrixContact()
    {
        return $this->belongsTo(BitrixContact::class);
    }

    public function bitrixStage()
    {
        return $this->belongsTo(BitrixStage::class);
    }

    public function bitrixDealCity()
    {
        return $this->belongsTo(BitrixDealCity::class);
    }

    public function getDataAttribute()
    {
        return json_decode($this->json);
    }

    public function setFromJson()
    {
        $data = $this->data;
        if (isset($data->UF_CRM_1618266770570)) {
            $this->bitrix_deal_city_id = $data->UF_CRM_1618266770570;
        } else {
            if (isset($this->amoLead->amoGroup->bitrixDealCity->id)) {
                // dd($this->amoLead->amoGroup->bitrixDealCity->id);
                $this->bitrix_deal_city_id = $this->amoLead->amoGroup->bitrixDealCity->id;
            }
        }
        if (isset($data->UF_CRM_1618953582363)) {
            $this->amo_lead_id = $data->UF_CRM_1618953582363 ?? null;
        }
        if (isset($data->STAGE_ID)) {
            $this->bitrix_stage_id = $data->STAGE_ID;
        }
        if (isset($data->CONTACT_ID)) {
            $this->bitrix_contact_id = $data->CONTACT_ID;
        }
        if (isset($data->COMPANY_ID)) {
            $this->bitrix_company_id = $data->COMPANY_ID;
        }
        //
        if (isset($data->TITLE)) {
            $this->name = $data->TITLE;
        }
        if (isset($data->UF_CRM_1619549248827)) {
            $this->price = str_replace('|RUB', '', $data->UF_CRM_1619549248827);
        }
        if (isset($data->UF_CRM_1618937279511)) {
            $this->prepayment = str_replace('|RUB', '', $data->UF_CRM_1618937279511);
        }
        if (isset($data->UF_CRM_1618266826927)) {
            $this->monthlyPaySum = str_replace('|RUB', '', $data->UF_CRM_1618266826927);
        }
        if (isset($data->UF_CRM_1618264415437)) {
            $this->monthlyPayDate = new \DateTime($data->UF_CRM_1618264415437);
        }
        if (isset($data->UF_CRM_1619193761090)) {
            $this->payMonths = $data->UF_CRM_1619193761090;
        }
        if (isset($data->UF_CRM_1618264351273)) {
            $this->contractDate = new \DateTime($data->UF_CRM_1618264351273);
        }
        if (isset($data->UF_CRM_1618264325155)) {
            $this->contractId = $data->UF_CRM_1618264325155;
        }
    }
}
