<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class AmoLead extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $database = 'helpme';

    public function amoGroup()
    {
        return $this->belongsTo(AmoGroup::class, 'group_id');
    }

    public function amoStatus()
    {
        return $this->belongsTo(AmoStatus::class, 'status_id');
    }
}
