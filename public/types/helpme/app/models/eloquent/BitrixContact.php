<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class BitrixContact extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $database = 'helpme';

    protected $appends = ['data'];

    protected $hidden = ['json'];

    public function getDataAttribute()
    {
        return json_decode($this->json);
    }

    public function bitrixDeals()
    {
        return $this->hasMany(BitrixDeal::class);
    }
}
