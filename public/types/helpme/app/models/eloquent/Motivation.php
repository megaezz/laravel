<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Motivation extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $primaryKey = 'month';

    protected $keyType = 'string';

    protected $database = 'helpme';

    protected $table = 'motivation';

    public function amoGroup()
    {
        return $this->belongsTo(AmoGroup::class, 'amo_group');
    }
}
