<?php

namespace helpme\app\models\eloquent;

use engine\app\models\F;

class Transfers extends \engine\app\models\eloquent\Lists
{
    private $group_id = null;

    private $source = null;

    private $withLead = null;

    private $withReceipt = null;

    private $transferId = null;

    private $verified = null;

    public function __construct()
    {
        $this->setItemClass(Transfer::class);
    }

    public function getBalance()
    {
        $source = $this->getSource() ?? F::error('set Source to use Transfers::getBalance');
        $group_id = $this->getGroupId() ?? F::error('set GroupId to use Transfers::getBalance');
        $source = Transfer::where('source', $this->getSource())
            ->where('amo_group', $this->getGroupId());
        $toSource = Transfer::where('to_source', $this->getSource())
            ->where('amo_group', $this->getGroupId());

        return $source->sum('value') - $toSource->sum('value');
    }

    protected function setListVars($t = null, $transfer = null)
    {
        $t->v('id', $transfer->id);
        $t->v('date', $transfer->date);
        $t->v('type', $transfer->type);
        $t->v('text', $transfer->text);
        $t->v('source', $transfer->source);
        $t->v('to_source', $transfer->to_source);
        $t->v('lead_id', $transfer->lead_id);
        $t->v('group_id', $transfer->group_id);
        $t->v('receipt_id', $transfer->receipt_id);
        $t->v('bitrix_deal_id', $transfer->bitrix_deal_id);
        $t->v('modulbank_id', $transfer->modulbank_id);
        $t->v('value', $transfer->value);
    }

    public function getGroupId()
    {
        return $this->group_id;
    }

    public function setGroupId($group_id = null)
    {
        $this->group_id = $group_id;

        return $this->setQuery($this->getQuery()->where('transfers.amo_group', $group_id));
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setSource($source = null)
    {
        if (! $source) {
            return null;
        }
        $this->source = $source;

        return $this->setQuery($this->getQuery()->where(
            function ($query) use ($source) {
                $query->orWhere('source', $source)
                    ->orWhere('to_source', $source);
            }));
    }

    public static function getSources()
    {
        $arr = Transfer::groupBy('source')->get();
        $sources = [];
        foreach ($arr as $v) {
            $sources[] = $v->source;
        }

        return $sources;
    }

    public function getWithLead()
    {
        return $this->withLead;
    }

    public function getWithReceipt()
    {
        return $this->withReceipt;
    }

    public function setWithLead($b = null)
    {
        $this->withLead = $b;
        if (is_null($b)) {
            return $this->getQuery();
        }

        return $b ? $this->setQuery($this->getQuery()->whereNotNull('bitrix_contact_id')) : $this->setQuery($this->getQuery()->whereNull('bitrix_contact_id'));
    }

    public function setWithReceipt($b = null)
    {
        $this->withReceipt = $b;
        if (is_null($b)) {
            return $this->getQuery();
        }

        // return $b?$this->setQuery($this->getQuery()->whereNotNull('receipt_id')):$this->setQuery($this->getQuery()->whereNull('receipt_id'));
        return $b ? $this->setQuery($this->getQuery()->where(function ($query) {
            $query->whereNotNull('receipt_id');
            $query->orWhereNotNull('modulkassa_receipt_id');
        })) : $this->setQuery($this->getQuery()->where(function ($query) {
            $query->whereNull('receipt_id');
            $query->whereNull('modulkassa_receipt_id');
        }));
    }

    public function setVerified($b = null)
    {
        $this->verified = $b;
        if (is_null($b)) {
            return $this->getQuery();
        }

        return $this->setQuery($this->getQuery()->where('verified', $b));
    }

    public function setTransferId($transferId = null)
    {
        $this->transferId = $transferId;
    }

    public function getTransferId()
    {
        return $this->transferId;
    }
}
