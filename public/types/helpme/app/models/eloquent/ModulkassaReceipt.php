<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class ModulkassaReceipt extends Model
{
    use HasDatabasePrefix;

    public $incrementing = false;

    protected $keyType = 'string';

    protected $database = 'helpme';

    public function transfer()
    {
        return $this->hasOne(Transfer::class);
    }

    public function getDataAttribute($value)
    {
        return json_decode($value);
    }
}
