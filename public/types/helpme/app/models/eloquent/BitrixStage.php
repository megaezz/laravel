<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class BitrixStage extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    protected $keyType = 'string';

    public $incrementing = false;

    protected $database = 'helpme';

    protected $appends = ['data'];

    protected $hidden = ['json'];

    public function bitrixCategory()
    {
        return $this->belongsTo(BitrixCategory::class);
    }

    public function getDataAttribute()
    {
        return json_decode($this->json);
    }
}
