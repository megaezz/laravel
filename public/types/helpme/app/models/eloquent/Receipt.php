<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class Receipt extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $database = 'helpme';

    public function transfer()
    {
        return $this->hasOne(Transfer::class);
    }

    public function amoGroup()
    {
        return $this->belongsTo(AmoGroup::class, 'reg_num_kkt', 'reg_num_kkt');
    }
}
