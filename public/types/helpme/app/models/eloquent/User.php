<?php

namespace helpme\app\models\eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Megaezz\LaravelDatabasePrefix\HasDatabasePrefix;

class User extends Model
{
    use HasDatabasePrefix;
    use HasFactory;

    public $incrementing = false;

    protected $primaryKey = 'login';

    protected $keyType = 'string';

    protected $database = 'helpme';

    protected $hidden = ['pass'];
}
