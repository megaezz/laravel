<?php

namespace helpme\app\models;

use engine\app\models\F;
use engine\app\models\PaginatorCustom;

class AmoLeads
{
    private $groupId = null;

    private $sql = null;

    private $limit = null;

    private $page = null;

    private $order = null;

    private $rows = null;

    private $createDate = null;

    private $paginationUrlPattern = null;

    private $hasPaymentTasks = null;

    private $autoLead = null;

    private $payStatuses = null;
    // private $listTemplate = null;
    // private $lastModifiedDate = null;
    // private $updatedAtDate = null;

    public function __construct() {}

    public function get()
    {
        $sql = $this->sql;
        if (! $this->getLimit()) {
            $this->setLimit(1);
        }
        if (! $this->getPage()) {
            $this->setPage(1);
        }
        $sql['from'] = ($this->getPage() - 1) * $this->getLimit();
        $sql['order'] = $this->getOrder() ? 'order by '.$this->getOrder() : '';
        $sql['createDate'] = isset($this->sql['createDate']) ? $this->sql['createDate'] : 1;
        $sql['groupId'] = $this->getGroupId() ? 'amo_leads.group_id = \''.$this->getGroupId().'\'' : 1;
        $sql['joinAmoGroups'] = '';
        $sql['hasPaymentTasks'] = is_null($this->getHasPaymentTasks()) ? 1 : (($this->getHasPaymentTasks() ? '' : 'not').' if((select 1 from '.F::typetab('amo_tasks').' where lead = amo_leads.id and amo_tasks.task_type = 991435 limit 1),1,0)');
        $sql['autoLead'] = is_null($this->getAutoLead()) ? 1 : ($this->getAutoLead() ? 'amo_leads.name like \'%Автосделка%\'' : 'amo_leads.name not like \'%Автосделка%\'');
        $doJoinAmoStatuses = false;
        $sql['payStatuses'] = '1';
        if (! is_null($this->getPayStatuses())) {
            $doJoinAmoStatuses = true;
            if ($this->getPayStatuses()) {
                $sql['payStatuses'] = 'amo_statuses.pay';
            } else {
                $sql['payStatuses'] = 'not amo_statuses.pay';
            }
        }
        if ($doJoinAmoStatuses) {
            $sql['joinAmoStatuses'] = 'join '.F::typetab('amo_statuses').' on amo_statuses.id = amo_leads.status_id';
        } else {
            $sql['joinAmoStatuses'] = '';
        }
        $arr = F::query_arr(
            'select SQL_CALC_FOUND_ROWS amo_leads.id
			from '.F::typetab('amo_leads').'
			'.$sql['joinAmoGroups'].'
			'.$sql['joinAmoStatuses'].'
			where
			'.$sql['createDate'].' and
			'.$sql['groupId'].' and
			'.$sql['hasPaymentTasks'].' and
			'.$sql['autoLead'].' and
			'.$sql['payStatuses'].'
			
			'.$sql['order'].'
			limit '.$sql['from'].','.$this->getLimit().'
			;'
        );
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function setGroupId($groupId = null)
    {
        $this->groupId = $groupId;
    }

    public function getGroupId()
    {
        return $this->groupId;
    }

    public function setCreateDate($from = null, $to = null)
    {
        if ($from and ! $to) {
            $this->sql['createDate'] = 'amo_leads.date_create = \''.$from.'\'';
        }
        if ($from and $to) {
            $this->sql['createDate'] = 'amo_leads.date_create >= \''.$from.'\' and amo_leads.date_create <= \''.$to.'\'';
        }
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setPage($page = null)
    {
        $this->page = (int) $page;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function getPages()
    {
        if (! $this->getPaginationUrlPattern()) {
            F::error('setPaginationUrlPattern first');
        }
        $Paginator = new PaginatorCustom;
        $Paginator->setUrlPattern($this->getPaginationUrlPattern());
        $Paginator->setTemplateList('pagination/list');
        $Paginator->setTemplatePrev('pagination/prev');
        $Paginator->setTemplateNext('pagination/next');
        $Paginator->setRows($this->getRows());
        $Paginator->setLimit($this->getLimit());
        $Paginator->setPage($this->getPage());
        $Paginator->setMaxPagesToShow(4);

        return $Paginator->get();
        // return new Paginator($this->getRows(),$this->getLimit(),$this->getPage(),$this->getPaginationUrlPattern());
    }

    public function getPaginationUrlPattern()
    {
        return $this->paginationUrlPattern;
    }

    public function setPaginationUrlPattern($v = null)
    {
        $this->paginationUrlPattern = $v;
    }

    public function setHasPaymentTasks($b = null)
    {
        $this->hasPaymentTasks = $b;
    }

    public function getHasPaymentTasks()
    {
        return $this->hasPaymentTasks;
    }

    // function setListTemplate($template = null) {
    // 	$this->listTemplate = $template;
    // }

    // function getListTemplate() {
    // 	return $this->listTemplate;
    // }

    public function getAutoLead()
    {
        return $this->autoLead;
    }

    public function setAutoLead($b = null)
    {
        $this->autoLead = $b;
    }

    public function getPayStatuses()
    {
        return $this->payStatuses;
    }

    public function setPayStatuses($b = null)
    {
        $this->payStatuses = $b;
    }
}
