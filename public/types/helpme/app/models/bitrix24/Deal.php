<?php

namespace helpme\app\models\bitrix24;

use engine\app\models\F;

class Deal
{
    private $id = null;

    private $data = null;

    public function __construct($id = null)
    {
        $arr = F::query_assoc('
			select id,json from '.F::typetab('bitrix_deals').'
			where id = \''.F::escape_string($id).'\'
			;');
        if (! $arr) {
            F::error('Сделка не существует');
        }
        $this->id = (int) $arr['id'];
        $this->data = json_decode($arr['json']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data = null)
    {
        $this->data = $data;
    }

    public function getAmoLeadId()
    {
        return $this->getData()->UF_CRM_1618953582363 ?? null;
    }

    public function save()
    {
        // F::dump($this->getData()->STAGE_ID);
        F::query('
			update '.F::typetab('bitrix_deals').'
			set 
			amo_lead_id = '.($this->getAmoLeadId() ? '\''.$this->getAmoLeadId().'\'' : 'null').',
			name = \''.F::escape_string($this->getData()->TITLE).'\',
			price = '.(empty($this->getData()->UF_CRM_1619549248827) ? 'null' : '\''.F::escape_string(str_replace('|RUB', '', $this->getData()->UF_CRM_1619549248827)).'\'').',
			status_id = '.(($this->getData()->STAGE_ID == 1) ? 'null' : '\''.F::escape_string($this->getData()->STAGE_ID).'\'').',
			prepayment = '.(empty($this->getData()->UF_CRM_1618937279511) ? 'null' : '\''.F::escape_string(str_replace('|RUB', '', $this->getData()->UF_CRM_1618937279511)).'\'').',
			monthlyPaySum = '.(empty($this->getData()->UF_CRM_1618266826927) ? 'null' : '\''.F::escape_string(str_replace('|RUB', '', $this->getData()->UF_CRM_1618266826927)).'\'').',
			monthlyPayDate = '.(empty($this->getData()->UF_CRM_1618264415437) ? 'null' : '\''.F::escape_string((new \DateTime($this->getData()->UF_CRM_1618264415437))->format('Y-m-d H:i:s')).'\'').',
			payMonths = '.(empty($this->getData()->UF_CRM_1619193761090) ? 'null' : '\''.F::escape_string($this->getData()->UF_CRM_1619193761090).'\'').',
			contractDate = '.(empty($this->getData()->UF_CRM_1618264351273) ? 'null' : '\''.F::escape_string((new \DateTime($this->getData()->UF_CRM_1618264351273))->format('Y-m-d H:i:s')).'\'').',
			contractId = '.(empty($this->getData()->UF_CRM_1618264325155) ? 'null' : '\''.F::escape_string($this->getData()->UF_CRM_1618264325155).'\'').',
			contact_id = '.(empty($this->getData()->CONTACT_ID) ? 'null' : '\''.F::escape_string($this->getData()->CONTACT_ID).'\'').'
			where id = \''.$this->getId().'\'
			;');
        // status_name = (select name from '.F::typetab('amo_statuses').' where id = '.F::escape_string($lead->status_id).'),
        // date_create = from_unixtime(\''.F::escape_string($lead->date_create).'\'),
        // last_modified = from_unixtime(\''.F::escape_string($lead->last_modified).'\'),
        // company_id = '.(empty($lead->linked_company_id)?'null':'\''.F::escape_string($lead->linked_company_id).'\'').',
        // monthlyPayDay = '.(empty($monthlyPayDay)?'null':'\''.F::escape_string($monthlyPayDay).'\'').',
        // prepaymentDate = '.(empty($prepaymentDate)?'null':'\''.F::escape_string($prepaymentDate).'\'').',
        // utm_source = '.(empty($utm['source'])?'null':'\''.F::escape_string($utm['source']).'\'').',
        // utm_medium = '.(empty($utm['medium'])?'null':'\''.F::escape_string($utm['medium']).'\'').',
        // utm_term = '.(empty($utm['term'])?'null':'\''.F::escape_string($utm['term']).'\'').',
        // utm_content = '.(empty($utm['content'])?'null':'\''.F::escape_string($utm['content']).'\'').',
        // utm_campaign = '.(empty($utm['campaign'])?'null':'\''.F::escape_string($utm['campaign']).'\'').',
        // utm_referer = '.(empty($utm['referer'])?'null':'\''.F::escape_string($utm['referer']).'\'').',
        // cancel_reason = '.(empty($cancelReason)?'null':'\''.F::escape_string($cancelReason).'\'').',
        // group_id = '.(empty($lead->group_id)?'null':'\''.F::escape_string($lead->group_id).'\'').',
        // responsible_user_id = '.(empty($lead->responsible_user_id)?'null':'\''.F::escape_string($lead->responsible_user_id).'\'').',
        // contractFio = '.(empty($contractFio)?'null':'\''.F::escape_string($contractFio).'\'').',
    }
}
