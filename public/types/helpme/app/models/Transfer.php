<?php

namespace helpme\app\models;

use engine\app\models\Engine;
use engine\app\models\F;

class Transfer
{
    private $id = null;

    private $date = null;

    private $object = null;

    private $type = null;

    private $text = null;

    private $source = null;

    private $to_source = null;

    private $lead_id = null;

    private $group_id = null;

    private $receipt_id = null;

    private $bitrix_deal_id = null;

    private $modulbank_id = null;

    private $update_date = null;

    public function __construct($id = null)
    {
        $arr = F::query_assoc('
			select id,date,object,type,value,text,source,to_source,lead,amo_group,receipt_id,
			bitrix_deal_id,modulbank_id
			from '.F::typetab('transfers').'
			where id = \''.F::escape_string($id).'\'
			');
        if (! $arr) {
            F::error('Платеж не найден');
        }
        $this->id = $arr['id'];
        $this->date = $arr['date'];
        $this->type = $arr['type'];
        $this->value = $arr['value'];
        $this->text = $arr['text'];
        $this->source = $arr['source'];
        $this->to_source = $arr['to_source'];
        $this->lead_id = $arr['lead'];
        $this->group_id = $arr['amo_group'];
        $this->receipt_id = $arr['receipt_id'];
        $this->bitrix_deal_id = $arr['bitrix_deal_id'];
        $this->modulbank_id = $arr['modulbank_id'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date = null)
    {
        $this->date = $date;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function setSource($source = null)
    {
        $this->source = $source;
    }

    public function getToSource()
    {
        return $this->to_source;
    }

    public function setToSource($source = null)
    {
        $this->to_source = $source;
    }

    public function getLeadId()
    {
        return $this->lead_id;
    }

    public function setLeadId($lead_id = null)
    {
        $this->lead_id = $lead_id;
    }

    public function getGroupId()
    {
        return $this->group_id;
    }

    public function setGroupId($group_id = null)
    {
        $this->group_id = $group_id;
    }

    public function getReceiptId()
    {
        return $this->receipt_id;
    }

    public function setReceiptId($receipt_id = null)
    {
        $this->receipt_id = $receipt_id;
    }

    public function getBitrixDealId()
    {
        return $this->bitrix_deal_id;
    }

    public function setBitrixDealId($bitrix_deal_id = null)
    {
        $this->bitrix_deal_id = $bitrix_deal_id;
    }

    public function getModulbankId()
    {
        return $this->modulbank_id;
    }

    public function setModulbankId($id = null)
    {
        $this->modulbank_id = $id;
    }

    public function getUpdateDate()
    {
        return $update_date;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value = null)
    {
        $this->value = $value;
    }

    public static function add()
    {
        F::query('insert into '.F::typetab('transfers').' set id = null;');

        return Engine::getSqlConnection()->insert_id;
    }

    public function save()
    {
        F::query('update '.F::typetab('transfers').'
			set
			date = '.($this->getDate() ? ('\''.F::escape_string($this->getDate()).'\'') : 'null').',
			type = '.($this->getType() ? ('\''.F::escape_string($this->getType()).'\'') : 'null').',
			value = '.($this->getValue() ? ('\''.F::escape_string($this->getValue()).'\'') : 'null').',
			text = '.($this->getText() ? ('\''.F::escape_string($this->getText()).'\'') : 'null').',
			source = '.($this->getSource() ? ('\''.F::escape_string($this->getSource()).'\'') : 'null').',
			to_source = '.($this->getToSource() ? ('\''.F::escape_string($this->getToSource()).'\'') : 'null').',
			lead = '.($this->getLeadId() ? ('\''.F::escape_string($this->getLeadId()).'\'') : 'null').',
			amo_group = '.($this->getGroupId() ? ('\''.F::escape_string($this->getGroupId()).'\'') : 'null').',
			receipt_id = '.($this->getReceiptId() ? ('\''.F::escape_string($this->getReceiptId()).'\'') : 'null').',
			bitrix_deal_id = '.($this->getBitrixDealId() ? ('\''.F::escape_string($this->getBitrixDealId()).'\'') : 'null').',
			modulbank_id = '.($this->getModulbankId() ? ('\''.F::escape_string($this->getModulbankId()).'\'') : 'null').'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }
}
