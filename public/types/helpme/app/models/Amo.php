<?php

namespace helpme\app\models;

use engine\app\models\F;
use engine\app\models\HamtimAmocrm\HamtimAmocrm;

class Amo extends HamtimAmocrm
{
    public function __construct($id = null)
    {
        if (empty($id)) {
            F::error('ID required to use Amo');
        }
        $config = F::query_assoc('
			select id, email, hash, session_id
			from '.F::typetab('amo_users').'
			where id = \''.$id.'\'
			;');
        if (! $config) {
            F::error('Amo config not found');
        }
        parent::__construct($config['email'], $config['hash'], 'helpme90', $config['session_id']);
        if (! $this->auth) {
            F::error('No connection with amoCRM');
        }
        if ($this->getSessionId()) {
            F::query('update '.F::typetab('amo_users').' set session_id = \''.$this->getSessionId().'\' where id = \''.$config['id'].'\';');
        }
    }
}
