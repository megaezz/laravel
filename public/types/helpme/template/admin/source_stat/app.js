const router = new VueRouter({
	mode: 'history',
	routes: []
});
var vue = new Vue({
	router,
	el: '#app',
	data: {
		groupId: null,
		data: {
			contractsByDays: {},
			leadsByDaysAndSources: {},
		},
	},
	methods: {
		leadsOfDaySum: function(dayLeads) {
			var sum = 0;
			dayLeads.forEach((data) => {
				sum = sum + data.q;
			});
			return sum;
		},
		getData: function() {
			axios
			.get('/?r=ApiManager/getSourceStat',{
				params: {
					groupId: this.groupId
				}
			})
			.then(response => (this.data = response.data));
		}
	},
	computed: {
		leadDays: function() {
			var to = new Date();
			var from = new Date();
			from.setMonth(from.getMonth() - 1);
			var arr = [];
			for (var d = to; d >= from; d.setDate(d.getDate() - 1)) {
				arr.push((new Date(d)).toISOString().split('T')[0]);
			}
			return arr;
		},
	},
	mounted() {
		this.groupId = this.$route.query.groupId;
		this.getData();
	},
	watch: {
		data: function (data) {
			for (const [key, v] of Object.entries(this.data.contractsByMonths)) {
				var bonus1 = 0;
				var bonus2 = 0;
				if (v.motivationValue != 0) {
					bonus1 = (v.sumPrices >= v.motivationValue)?7500:0;
					bonus1 = (v.sumPrices > v.motivationValue)?Math.round((v.sumPrices - v.motivationValue) * 0.1 / 3):0;
				}
				this.data.contractsByMonths[key].bonus1 = bonus1;
				this.data.contractsByMonths[key].bonus2 = bonus2;
				this.data.contractsByMonths[key].bonus = bonus1 + bonus2;
			}
		}
	}
});