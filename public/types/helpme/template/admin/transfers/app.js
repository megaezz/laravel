Vue.use(VueMask.VueMaskPlugin);

const router = new VueRouter({
	mode: 'history',
	routes: []
});

var vue = new Vue({
	router,
	components: {
		'contact-min': contactMin,
		'contact-full': contactFull,
		'modulkassa_receipt': modulkassa_receipt,
		'transfer': transfer,
	},
	el: '#app',
	data: {
		groupId: null,
		receipts: [],
		modulkassaReceipts: [],
		leads: [],
		transfers: [],
		bitrixContactsWithContract: [],
		user: {},
		balances: {},
		loading: false,
		addTransferFormDefault: {
			groupId: null,
			date: moment().format('DD.MM.YYYY'),
			lead: null,
			source: null,
			to_source: null,
			bitrix_contact: null,
			type: null,
			value: null,
		},
		addTransferForm: {},
		searchContact: {
			transferId: null,
			string: null
		},
		searchReceipt: {
			transferId: null,
		},
		searchModulkassaReceipt: {
			transferId: null,
		},
		transfersFilter: {
			groupId: null,
			page: 1,
			withoutLead: null,
			withoutReceipt: false,
			source: null,
			bitrix_contact: null,
			limit: 25,
			order_by: null,
			notVerified: null,
			text: null,
			duplicates: null,
			id: null,
			dateFrom: null,
			dateTo: null,
			incomes: null,
			outcomes: null,
		},
		transferSources: ['касса','р/с','судебные','ежеватов','горин'],
		showTransfersFilters: false,
		showTransferTextEdit: false,
		showTransferDateEdit: false,
		showTransferSourceEdit: false,
	},
	computed: {
		foundContacts: function() {
			var arr = [];
			var search = this.searchContact.string;
			if (!search) {
				return arr;
			}
			search = search.toLowerCase();
			this.bitrixContactsWithContract.forEach(function(contact){
				if (
					contact.data.NAME.toLowerCase().includes(search) ||
					(contact.data.PHONE && contact.data.PHONE.filter(phone => (phone.VALUE.toLowerCase().includes(search))).length) ||
					contact.bitrix_deals.filter(deal => (
						deal.data.TITLE.toLowerCase().includes(search) ||
						(deal.contractId && deal.contractId.toLowerCase().includes(search))
						)).length
					) 
				{
					arr.push(contact);
				}
			});
			return arr;
		},
	},
	methods: {
		transferContactEdit: function(data) {
			this.searchContact.transferId = ((this.searchContact.transferId == data.transfer.id)?null:data.transfer.id);
			this.searchContact.string = null;
		},
		transferContactClick: function(data) {
			data.transfer.bitrix_contact_id = data.contact.id;
			this.updateTransfer(data.transfer);
			this.searchContact.transferId = null;
		},
		transferContactDelete: function(data) {
			if (!confirm('Удалить клиента для платежа ' + data.transfer.id + '?')) {
				return false;
			}
			data.transfer.bitrix_contact_id = null;
			this.updateTransfer(data.transfer);
			this.searchContact.transferId = null;
		},
		transfersFilterContactClick: function(data) {
			this.transfersFilter.bitrix_contact = data.contact;
			this.transfersFilter.page = 1;
			this.searchContact.string = null;
		},
		transfersFilterContactDelete: function() {
			this.transfersFilter.bitrix_contact = null;
			this.searchContact.string = null;
		},
		addTransferFormContactClick: function(data) {
			this.addTransferForm.bitrix_contact = data.contact;
			this.searchContact.string = null;
		},
		addTransferFormContactDelete: function() {
			this.addTransferForm.bitrix_contact = null;
			this.searchContact.string = null;
		},
		updateTransfer: function(transfer) {
			axios
			.post('/?r=ApiManager/updateTransfer',transfer)
			.then(response => {
				this.getTransfers();
			})
			.catch(error => {
				alert('Ошибка при изменении платежа');
			});
		},
		deleteTransfer: function(transfer) {
			axios
			.post('/?r=ApiManager/deleteTransfer',transfer)
			.then(response => {
				this.getTransfers();
				this.getBalances();
			})
			.catch(error => {
				alert('Ошибка при удалении платежа');
			});
		},
		getTransfers: function() {
			this.loading = true;
			axios
			.post('/?r=ApiManager/getTransfers',this.transfersFilter)
			.then(response => {
				this.transfers = response.data;
				this.loading = false;
			});
		},
		addTransfer: function(form) {
			this.loading = true;
			axios
			.post('/?r=ApiManager/addTransfer',this.addTransferForm)
			.then(response => {
				this.getTransfers();
				this.getBalances();
				this.addTransferForm = Object.assign({},this.addTransferFormDefault);
				this.loading = false;
							// addTransferModal.hide();
						})
			.catch(error => {
				this.loading = false;
				alert('Ошибка, проверьте форму');
			});
		},
		getBalances: function() {
			axios
			.get('/?r=ApiManager/getBalances',{
				params: {
					groupId: this.groupId
				}
			})
			.then(response => (this.balances = response.data));
		},
		getBitrixContacts: function() {
			axios
			.get('/?r=ApiManager/getBitrixContacts',{
				params: {
					contract: 1,
					amo_group_id: this.groupId,
					limit: 0,
				}
			})
			.then(response => (this.bitrixContactsWithContract = response.data));
		},
		getAuthUser: function() {
			axios
			.get('/?r=ApiManager/getAuthUser')
			.then(response => (this.user = response.data));
		},
		getAmoLeads: function() {
			axios
			.get('/?r=ApiManager/getLeads',{
				params: {
					groupId: this.groupId
				}
			})
			.then(response => (this.leads = response.data));
		},
		getReceipts: function() {
			axios
			.get('/?r=ApiManager/getReceipts',{
				params: {
					groupId: this.groupId
				}
			})
			.then(response => (this.receipts = response.data));
		},
		getModulkassaReceipts: function() {
			axios
			.get('/?r=ApiManager/getModulkassaReceipts',{
				params: {
					groupId: this.groupId
				}
			})
			.then(response => {
				this.modulkassaReceipts = response.data;
				this.modulkassaReceipts.sort(function(a,b){
					return moment(b.data[1],'DD.MM.YYYY H:i:s') - moment(a.data[1],'DD.MM.YYYY H:i:s');
				});
			});
		},
		isModulkassaReceiptCorrect: function(transfer) {
			if (transfer.modulkassa_receipt.refund) {
				var source = (transfer.modulkassa_receipt.data[12] == 'Наличными')?'касса':'р/с';
				var value = - parseFloat(transfer.modulkassa_receipt.data[11].replace(',', '.'));
			} else {
				var source = (transfer.modulkassa_receipt.data[15] == 'Наличными')?'касса':'р/с';
				var value = parseFloat(transfer.modulkassa_receipt.data[13].replace(',', '.'));
			}
			if (source != transfer.source) {
				console.log(source + ' != ' + transfer.source);
				return false;
			}
			if (value != parseFloat(transfer.value)) {
				console.log(value + ' != ' + parseFloat(transfer.value));
				return false;
			}
			return true;
		},
	},
	mounted() {
		this.groupId = this.$route.query.groupId;
		this.addTransferForm = Object.assign({},this.addTransferFormDefault);
	},
	watch: {
		groupId: function() {
			this.transfersFilter.groupId = this.groupId;
			this.addTransferForm.groupId = this.groupId;
			this.addTransferFormDefault.groupId = this.groupId;
			this.getReceipts();
			this.getModulkassaReceipts();
			this.getAmoLeads();
			this.getBitrixContacts();
			this.getBalances();
			this.getAuthUser();
		},
		transfersFilter: {
			handler: function(newVal) {
				var proceed = true;
				if (this.transfersFilter.dateFrom && (this.transfersFilter.dateFrom.length < 10)) {
					proceed = false;
				}
				if (this.transfersFilter.dateTo && (this.transfersFilter.dateTo.length < 10)) {
					proceed = false;
				}
				if (proceed) {
					this.getTransfers();
				}
			},
			deep: true,
		},
		'addTransferForm.value': function(newVal,oldVal) {
			if (!newVal) {
				return;
			}
			if (!isNaN(newVal)) {
				if (newVal < 0) {
					alert('Больше не нужно указывать отрицательное значение. Достаточно выбрать "Расход".');
				}
				this.addTransferForm.value = Math.abs(newVal);
			} else {
				this.addTransferForm.value = oldVal;
			}
		},
	}
});