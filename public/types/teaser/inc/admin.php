<?php

namespace MegawebV1;

function submit_wap_settings()
{
    // pre($_POST);
    if (empty($_POST['link'])) {
        error('�� ������� ������ ������');
    }
    $affected_domains = '';
    foreach ($_POST['link'] as $domain => $links) {
        query('update '.tabname('teaser', 'wap').' set
            scheme=\''.trim($links['scheme']).'\',
            megafon=\''.trim($links['megafon']).'\',
            megafon_second=\''.trim($links['megafon_second']).'\',
            beeline=\''.trim($links['beeline']).'\',
            beeline_second=\''.trim($links['beeline_second']).'\',
            mts=\''.trim($links['mts']).'\',
            mts_tablet=\''.trim($links['mts_tablet']).'\',
            mts_second=\''.trim($links['mts_second']).'\',
            beeline_modem=\''.trim($links['beeline_modem']).'\',
            megafon_modem=\''.trim($links['megafon_modem']).'\',
            megafon_modem_second=\''.trim($links['megafon_modem_second']).'\',
            az=\''.trim($links['az']).'\',
            android_3g=\''.trim($links['android_3g']).'\',
            android_wifi=\''.trim($links['android_wifi']).'\',
            tele2=\''.trim($links['tele2']).'\'
            where domain=\''.$domain.'\';');
        if (mysql_affected_rows() != 0) {
            if (! empty($affected_domains)) {
                $affected_domains .= ', ';
            } $affected_domains .= $domain;
        }
    }
    if (empty($affected_domains)) {
        alert('������ �� ��������');
    }
    alert('��������� ���������. ���������� ������: '.$affected_domains);
}

function wap_settings()
{
    $data = query('select domain,scheme,latency,percent,index_page,
        megafon,megafon_second,beeline,beeline_second,mts,mts_tablet,mts_second,beeline_modem,megafon_modem,megafon_modem_second,
        az,android_3g,android_wifi,tele2
        from '.tabname('teaser', 'wap').' where deleted=\'0\';');
    $arr = readdata($data, 'nokey');
    $schemes[0]['scheme'] = 'megafon_mob_mod_beeline_mob_mts_mob_tab_android_3g';
    $schemes[1]['scheme'] = 'megafon_mob_mod_beeline_mob_mts_mob_tab';
    $schemes[2]['scheme'] = 'megafon_mob_mod_beeline_mob_mts_mob_tab_az';
    $schemes[3]['scheme'] = 'megafon_mob_mod_mts_mob_tab';
    $schemes[]['scheme'] = 'megafon_mob_mod_beeline_mob_mts_mob_tab_tele2';
    $schemes[]['scheme'] = 'megafon_mob_mod_mts_mob_tab_tele2';
    $list = '';
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['domain'].'</td>
        <td><select class="form scheme" name="link['.$v['domain'].'][scheme]">'.get_select_option_list(['array' => $schemes, 'value' => 'scheme', 'text' => 'scheme', 'selected_value' => $v['scheme']]).'</select></td>
        <td class="no">'.$v['latency'].'</td>
        <td class="no">'.$v['percent'].'</td>
        <td>'.$v['index_page'].'</td>
        <td><input type="text" name="link['.$v['domain'].'][megafon]" value="'.$v['megafon'].'" class="link" /></td>
        <td class="no"><input type="text" name="link['.$v['domain'].'][megafon_second]" value="'.$v['megafon_second'].'" class="link" /></td>
        <td><input type="text" name="link['.$v['domain'].'][megafon_modem]" value="'.$v['megafon_modem'].'" class="link" /></td>
        <td class="no"><input type="text" name="link['.$v['domain'].'][megafon_modem_second]" value="'.$v['megafon_modem_second'].'" class="link" /></td>
        <td><input type="text" name="link['.$v['domain'].'][beeline]" value="'.$v['beeline'].'" class="link" /></td>
        <td class="no"><input type="text" name="link['.$v['domain'].'][beeline_second]" value="'.$v['beeline_second'].'" class="link" /></td>
        <td class="no"><input type="text" name="link['.$v['domain'].'][beeline_modem]" value="'.$v['beeline_modem'].'" class="link" /></td>
        <td><input type="text" name="link['.$v['domain'].'][mts]" value="'.$v['mts'].'" class="link" /></td>
        <td class="no"><input type="text" name="link['.$v['domain'].'][mts_tablet]" value="'.$v['mts_tablet'].'" class="link" /></td>
        <td class="no"><input type="text" name="link['.$v['domain'].'][mts_second]" value="'.$v['mts_second'].'" class="link" /></td>
        <td><input type="text" name="link['.$v['domain'].'][az]" value="'.$v['az'].'" class="link" /></td>
        <td><input type="text" name="link['.$v['domain'].'][android_3g]" value="'.$v['android_3g'].'" class="link" /></td>
        <td><input type="text" name="link['.$v['domain'].'][android_wifi]" value="'.$v['android_wifi'].'" class="link" /></td>
        <td><input type="text" name="link['.$v['domain'].'][tele2]" value="'.$v['tele2'].'" class="link" /></td>
        </tr>
        ';
    }

    return $list;
}

function get_show()
{
    if (empty($_GET['show'])) {
        $return = '';
    } else {
        $return = $_GET['show'];
    }

    return $return;
}

function get_dir()
{
    if (empty($_GET['dir'])) {
        $return = '';
    } else {
        $return = $_GET['dir'];
    }

    return $return;
}

function get_order_by()
{
    if (empty($_GET['order_by'])) {
        $return = '';
    } else {
        $return = $_GET['order_by'];
    }

    return $return;
}

function sql_add_teasers_from_dir($dir)
{
    $path = 'types/teaser/images/'.$dir;
    $arr = list_directory_files($path);
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= 'insert into '.tabname('teaser', 'teasers').' set src=\''.$dir.'/'.$v.'\', active=0, title=\'��� ��������\';<br/>';
    }

    return $list;
}

function teaser_get_domain()
{
    if (empty($_GET['domain']) == 1) {
        error('�� �������� ����������� ����������');
    }

    return $_GET['domain'];
}

function get_b()
{
    if (empty($_GET['b']) == 1) {
        error('�� �������� ����������� ����������');
    }

    return $_GET['b'];
}

function blocks_code()
{
    $data = query('select id,domain,name from '.tabname('teaser', 'blocks'));
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $script1 = htmlspecialchars('<script type="text/javascript" src="{http_current_domain}/get'.$v['id'].'"></script>');
        $script2[1] = htmlspecialchars('<div id="teaser-'.$v['id'].'"></div>');
        $script2[2] = htmlspecialchars('<script type="text/javascript" src="{http_current_domain}/get/'.$v['domain'].'"></script>');
        $script3 = htmlspecialchars('<script type="text/javascript" src="{http_current_domain}/w/'.$v['domain'].'"></script>');
        $script4 = htmlspecialchars('<script>(function() {
            server=\''.$v['domain'].'\';
            domain=\'{current_domain}\';
            path=\'dom-dp-w\';
            var s=document.createElement(\'script\');
            s.src=\'http://\'+server+\'/\'+path+\'/\'+domain;
            document.head.appendChild(s);
        })();</script>');
        $script4 = nl2br($script4);
        $list .= '<tr><td>'.$v['id'].'</td><td>'.$v['domain'].'</td><td>'.$v['name'].'</td>
	<td><div style="width: 300px; height: 50px; overflow: scroll;">'.$script1.'</div></td>
	<td><div style="width: 350px; height: 90px; overflow: scroll;">'.$script2[1].'<br/><!--����� &lt;/body&gt;:<br/>-->'.$script2[2].'</div></td>
    <td><div style="width: 300px; height: 60px; overflow: scroll;">'.$script3.'</div></td>
    <td><div style="width: 300px; height: 170px; overflow: scroll;">'.$script4.'</td>
	<td><a href="/?page=admin/preview_block&amp;domain='.$v['domain'].'&amp;b='.$v['id'].'" target="blank">Preview</a></td>
	</tr>';
    }
    $content = '<table class="admin_table">
    <tr style="font-weight: bold; text-align: center;"><td>ID</td><td>�����</td><td>��������</td><td>��� #1</td><td>��� #2</td><td>��� #3</td><td>��� #4</td><td>&nbsp;</td></tr>
    '.$list.'
    </table>';

    return $content;
}

// ����� ������� ������ �� ������� � ����� � ��� ����� ��� API �������� �����
function get_matrix_stat_by_hours()
{
    $data = query('select hour(date) as hour,
		ceil(count(*)/((to_days(adddate(current_date, interval -1 day))-to_days(date(min(date))))+1)) as q
		from '.tabname('teaser', 'clicks').'
		where date(date)<=adddate(current_date, interval -1 day)
		group by hour');
    $alldays = readdata($data, 'hour');
    // print_r($alldays); die();
    $data = query('select hour(date) as hour, count(*) as q from '.tabname('teaser', 'clicks').'
		where date(date)=adddate(current_date, interval -1 day)
		group by hour'
    );
    $yesterday = readdata($data, 'hour');
    $data = query('select hour(date) as hour, count(*) as q from '.tabname('teaser', 'clicks').'
		where date(date)=current_date and hour(date)!=hour(current_time)
		group by hour'
    );
    $today = readdata($data, 'hour');
    $list = '';
    for ($i = 0; $i <= 23; $i++) {
        $hour = $i;
        if (empty($list) == 0) {
            $list .= ', ';
        }
        if (empty($alldays[$hour]) == 1) {
            $alldays[$hour]['q'] = 'null';
        }
        if (empty($yesterday[$hour]) == 1) {
            $yesterday[$hour]['q'] = 'null';
        }
        if (empty($today[$hour]) == 1) {
            $today[$hour]['q'] = 'null';
        }
        $list .= '[\''.$hour.'\', '.$yesterday[$hour]['q'].', '.$today[$hour]['q'].', '.$alldays[$hour]['q'].']';
        // $list.='[\''.$hour.'\', '.$yesterday[$hour]['q'].', '.$today[$hour]['q'].']';
    }

    return $list;
}

function days_stat()
{
    global $db;
    $data = query('select date(clicks.date) as day, count(clicks.teaser_id) as q, count(distinct clicks.ip) as q_uniq
		from '.tabname('teaser', 'clicks').'
		group by day
		order by day desc;
		');
    $arr = readdata($data, 'nokey');
    $db['days_stat_for_google']['nokey'] = array_reverse($arr);
    $list = '';
    $clicks = 0;
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['day'].'</td><td>'.$v['q'].'</td><td>'.$v['q_uniq'].'</td></tr>';
    }
    $content = '<table class="admin_table">
    <tr style="text-align: center; font-weight: bold;"><td>����</td><td>������</td><td>����������</td></tr>
    '.$list.'
    </table>';

    return $content;
}

function get_matrix_stat_days()
{
    global $db;
    $list = '';
    unset($db['days_stat_for_google']['nokey'][count($db['days_stat_for_google']['nokey']) - 1]);
    foreach ($db['days_stat_for_google']['nokey'] as $v) {
        if (empty($list) == 0) {
            $list .= ', ';
        }
        $list .= '[\''.$v['day'].'\', '.$v['q'].']';
    }

    return $list;
}

function domains_stat()
{
    $data = query('select blocks.domain, date(clicks.date) as day, count(clicks.teaser_id) as q from '.tabname('teaser', 'blocks').'
		join '.tabname('teaser', 'clicks').' on clicks.block_id=blocks.id
		where date(clicks.date)=current_date
		group by day,domain
		order by day desc, q desc;
		');
    $arr = readdata($data, 'nokey');
    $list = '';
    $clicks = 0;
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['day'].'</td><td>'.$v['domain'].'</td><td>'.$v['q'].'</td></tr>';
    }
    $content = '<table class="admin_table">
    <tr style="text-align: center; font-weight: bold;"><td>����</td><td>�����</td><td>������</td></tr>
    '.$list.'
    </table>';

    return $content;
}

function blocks_stat()
{
    $data = query('select blocks.id, blocks.domain, date(clicks.date) as day, count(clicks.teaser_id) as q from '.tabname('teaser', 'blocks').'
		join '.tabname('teaser', 'clicks').' on clicks.block_id=blocks.id
		where date(clicks.date)=current_date
		group by day,id
		order by day desc, q desc;
		');
    $arr = readdata($data, 'nokey');
    $list = '';
    $clicks = 0;
    foreach ($arr as $v) {
        $list .= '<tr><td>'.$v['day'].'</td><td>'.$v['id'].'</td><td>'.$v['domain'].'</td><td>'.$v['q'].'</td></tr>';
    }
    $content = '<table class="admin_table">
    <tr style="text-align: center; font-weight: bold;"><td>����</td><td>ID</td><td>�����</td><td>������</td></tr>
    '.$list.'
    </table>';

    return $content;
}

function submit_change_title()
{
    if (isset($_POST['teaser']) == 0) {
        error('�� �������� ����������� ���������');
    }
    if (empty($_POST['teaser']) == 1) {
        error('������ ������');
    }
    foreach ($_POST['teaser'] as $k => $v) {
        if (empty($v['active']) == 1) {
            $v['active'] = 0;
        }
        query('update '.tabname('teaser', 'teasers').' set title=\''.trim($v['title']).'\', active=\''.trim($v['active']).'\', link=\''.trim($v['link']).'\' where id=\''.$k.'\'');
    }
    alert('���������');
    // return '��������� ��� ������ <b>'.$_POST['id'].'</b> �������';
}

function list_of_teasers_dirs()
{
    $data = query('select substring_index(src,\'/\', 1) as dir from '.tabname('teaser', 'teasers').' group by dir');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<a href="/?page=admin/teasers&amp;show={get_show}&amp;order_by={get_order_by}&amp;dir='.$v['dir'].'">'.$v['dir'].'</a> ';
    }

    return $list;
}

function stats_edit_teasers()
{
    if (empty($_GET['order_by'])) {
        $order_by = 'ctr';
    } else {
        $order_by = $_GET['order_by'];
    }
    if (empty($_GET['show'])) {
        $show = 'active';
    } else {
        $show = $_GET['show'];
    }
    if (empty($_GET['dir'])) {
        $dir = 'all';
    } else {
        $dir = $_GET['dir'];
    }
    if ($show == 'all') {
        $query_show = '';
    }
    if ($show == 'active') {
        $query_show = 'active=1';
    }
    if ($show == 'unactive') {
        $query_show = 'active=0';
    }
    if ($dir == 'all') {
        $query_dir = '';
    } else {
        $query_dir = 'src like \''.$dir.'/%\'';
    }
    if ($order_by == 'ctr') {
        $query_order_by = 'ctr desc';
    }
    if ($order_by == 'src') {
        $query_order_by = 'src';
    }
    // ���������� �������
    $where = '';
    if (! empty($query_show)) {
        $where = 'where ('.$query_show.')';
    }
    if (! empty($query_dir)) {
        if (empty($where)) {
            $where = 'where ('.$query_dir.')';
        }
        if (! empty($where)) {
            $where .= ' and ('.$query_dir.')';
        }
    }
    $data = query('select teasers.id, teasers.src, teasers.title, teasers.active, teasers.link,
		ifnull(teasers_stat.views,0) as views,
		ifnull(count(clicks.teaser_id),0) as clicks,
		round(ifnull(100*count(clicks.teaser_id)/teasers_stat.views,0),3) as ctr
		from '.tabname('teaser', 'teasers').'
		left join '.tabname('teaser', 'teasers_stat').' on teasers_stat.teaser_id=teasers.id
		left join '.tabname('teaser', 'clicks').' on clicks.teaser_id=teasers.id
		'.$where.'
		group by id
		order by '.$query_order_by.';
		');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $active_checked = '';
        if ($v['active'] == 1) {
            $active_checked = 'checked="checked"';
        }
        $list .= '<tr style="text-align: center;"><td>'.$v['id'].'</td><td><img src="/types/teaser/images/'.$v['src'].'" width="160" height="120" alt="" /><br/>'.$v['src'].'</td><td>
        <input type="text" name="teaser['.$v['id'].'][title]" size="50" style="font-size: 13px;" value="'.$v['title'].'"/>
        </td><td>'.$v['views'].'</td><td>'.$v['clicks'].'</td><td>'.$v['ctr'].'</td><td>
	<input type="checkbox" name="teaser['.$v['id'].'][active]" value="1" '.$active_checked.' />
        </td><td>
	<input type="text" name="teaser['.$v['id'].'][link]" size="50" style="font-size: 13px;" value="'.$v['link'].'"/>
	</td></tr>';
    }
    $content = '
    <form method="post" action="/?page=admin/submit_change_title">
    <table class="admin_table">
    <tr style="font-weight: bold; text-align: center;"><td>ID</td><td>SRC</td><td>���������</td><td>�������</td><td>������</td><td>CTR</td><td>����������</td><td>������</td></tr>
    '.$list.'
    </table>
    <input type="submit" name="submit" value="��������� ���������"/>
    </form>
    ';

    return $content;
}

function stats_teasers_common()
{
    $data = query('select ifnull(sum(views),0) as s_views,
		ifnull((select count(*) from '.tabname('teaser', 'clicks').'),0) as s_clicks,
		ifnull(100*(select count(*) from '.tabname('teaser', 'clicks').')/sum(views),0) as ctr,
		round(ifnull(sum(views)/(select (unix_timestamp(max(date))-unix_timestamp(min(date))) from '.tabname('teaser', 'clicks').'),0),2) as views_per_sec,
		round(ifnull((select count(*)/(unix_timestamp(max(date))-unix_timestamp(min(date))) from '.tabname('teaser', 'clicks').'),0),2) as clicks_per_sec
		from '.tabname('teaser', 'teasers_stat'));
    $arr = mysql_fetch_assoc($data);
    $content = '
    <table class="admin_table">
    <tr style="font-weight: bold;"><td>�������</td><td>������</td><td>CTR</td><td>������� � �������</td><td>������ � �������</td></tr>
    <tr><td>'.$arr['s_views'].'</td><td>'.$arr['s_clicks'].'</td><td>'.$arr['ctr'].'</td><td>'.$arr['views_per_sec'].'</td><td>'.$arr['clicks_per_sec'].'</td></tr>
    </table>
    ';

    return $content;
}
