<?php

namespace MegawebV1;

function wap_schemes($args = [])
{
    // pre($args);
    if (! isset($args['arr'])) {
        error('Не передан массив настроек');
    }
    if (! isset($args['operator'])) {
        error('Не передан оператор');
    }
    if (! isset($args['device'])) {
        error('Не передано устройство');
    }
    $arr = $args['arr'];
    $operator = $args['operator'];
    $device = $args['device'];
    if ($arr['scheme'] == 'megafon_mob_mod_mts_mob_tab') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'mts') {
                if ($device->isMobile() or $device->isTablet()) {
                    $link = $arr['mts'];
                    $dest = 'mts';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_mts_mob_tab_tele2') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'mts') {
                if ($device->isMobile() or $device->isTablet()) {
                    $link = $arr['mts'];
                    $dest = 'mts';
                }
            } else {
                if ($operator == 'tele2') {
                    $link = $arr['tele2'];
                    $dest = 'tele2';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_mob_mts_mob_tab_tele2') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts']) or empty($arr['tele2'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                }
            } else {
                if ($operator == 'mts') {
                    if ($device->isMobile() or $device->isTablet()) {
                        $link = $arr['mts'];
                        $dest = 'mts';
                    }
                } else {
                    if ($operator == 'tele2') {
                        $link = $arr['tele2'];
                        $dest = 'tele2';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_mob_mts_mob_tab_android_3g') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($device->isAndroidOS() and ($operator == 'megafon' or $operator == 'beeline' or $operator == 'mts')) {
            $link = $arr['android_3g'];
            $dest = 'android_3g';
        } else {
            if ($operator == 'megafon') {
                if ($device->isMobile()) {
                    $link = $arr['megafon'];
                    $dest = 'megafon';
                } else {
                    $link = $arr['megafon_modem'];
                    $dest = 'megafon_modem';
                }
            } else {
                if ($operator == 'beeline') {
                    if ($device->isMobile()) {
                        $link = $arr['beeline'];
                        $dest = 'beeline';
                    }
                } else {
                    if ($operator == 'mts') {
                        if ($device->isMobile() or $device->isTablet()) {
                            $link = $arr['mts'];
                            $dest = 'mts';
                        }
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_mob_mts_mob_tab_az') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts']) or empty($arr['az'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                }
            } else {
                if ($operator == 'mts') {
                    if ($device->isMobile() or $device->isTablet()) {
                        $link = $arr['mts'];
                        $dest = 'mts';
                    }
                } else {
                    if ($operator == 'az') {
                        $link = $arr['az'];
                        $dest = 'az';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_mob_mts_mob_tab') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                }
            } else {
                if ($operator == 'mts') {
                    if ($device->isMobile() or $device->isTablet()) {
                        $link = $arr['mts'];
                        $dest = 'mts';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_all_mts_all') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'beeline') {
                $link = $arr['beeline'];
                $dest = 'beeline';

            } else {
                if ($operator == 'mts') {
                    $link = $arr['mts'];
                    $dest = 'mts';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_mob_mts') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                }
            } else {
                if ($operator == 'mts') {
                    $link = $arr['mts'];
                    $dest = 'mts';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_mob_mod_mts_mob_tab') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts']) or empty($arr['mts_tablet'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile() or $device->isTablet()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                }
            } else {
                if ($operator == 'mts') {
                    if ($device->isMobile()) {
                        $link = $arr['mts'];
                        $dest = 'mts';
                    }
                    if ($device->isTablet()) {
                        $link = $arr['mts_tablet'];
                        $dest = 'mts_tablet';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_mts_az') {
        if (empty($arr['megafon']) or empty($arr['mts']) or empty($arr['az'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'mts') {
                $link = $arr['mts'];
                $dest = 'mts';
            } else {
                $geoip_country = get_geoip_country_code(request()->ip());
                if ($geoip_country == 'AZ') {
                    $link = $arr['az'];
                    $dest = 'az';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_mob_mod_mts_az') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts']) or empty($arr['az'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile() or $device->isTablet()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                }
            } else {
                if ($operator == 'mts') {
                    $link = $arr['mts'];
                    $dest = 'mts';
                } else {
                    $geoip_country = get_geoip_country_code(request()->ip());
                    if ($geoip_country == 'AZ') {
                        $link = $arr['az'];
                        $dest = 'az';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_second_beeline_mob_mod_second_mts_second') {
        if (empty($arr['megafon']) or empty($arr['megafon_second']) or empty($arr['megafon_modem']) or empty($arr['megafon_modem_second']) or empty($arr['beeline']) or empty($arr['beeline_second']) or empty($arr['mts']) or empty($arr['mts_second'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $glc['mg'] = get_link_count('mg');
                if ($glc['mg'] == 1) {
                    $link = $arr['megafon'];
                    $dest = 'megafon';
                }
                if ($glc['mg'] == 2) {
                    $link = $arr['megafon_second'];
                    $dest = 'megafon_second';
                }
            } else {
                $glc['mg_md'] = get_link_count('mg_md');
                if ($glc['mg_md'] == 1) {
                    $link = $arr['megafon_modem'];
                    $dest = 'megafon_modem';
                }
                if ($glc['mg_md'] == 2) {
                    $link = $arr['megafon_modem_second'];
                    $dest = 'megafon_modem_second';
                }
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile() or $device->isTablet()) {
                    $glc['bl'] = get_link_count('bl');
                    if ($glc['bl'] == 1) {
                        $link = $arr['beeline'];
                        $dest = 'beeline';
                    }
                    if ($glc['bl'] == 2) {
                        $link = $arr['beeline_second'];
                        $dest = 'beeline_second';
                    }

                }
            } else {
                if ($operator == 'mts') {
                    $glc['mt'] = get_link_count('mt');
                    if ($glc['mt'] == 1) {
                        $link = $arr['mts'];
                        $dest = 'mts';
                    }
                    if ($glc['mt'] == 2) {
                        $link = $arr['mts_second'];
                        $dest = 'mts_second';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_mts') {
        if (empty($arr['megafon']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'mts') {
                $link = $arr['mts'];
                $dest = 'mts';
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mob_mod_beeline_mob_mod_mts') {
        if (empty($arr['megafon']) or empty($arr['megafon_modem']) or empty($arr['beeline']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            if ($device->isMobile()) {
                $link = $arr['megafon'];
                $dest = 'megafon';
            } else {
                $link = $arr['megafon_modem'];
                $dest = 'megafon_modem';
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile() or $device->isTablet()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                }
            } else {
                if ($operator == 'mts') {
                    $link = $arr['mts'];
                    $dest = 'mts';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_mts') {
        if (empty($arr['megafon']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'mts') {
                $link = $arr['mts'];
                $dest = 'mts';
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_wap_mts') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile() or $device->isTablet()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                    // $link='n/a';
                    // $dest='beeline_script';
                }
            } else {
                if ($operator == 'mts') {
                    $link = $arr['mts'];
                    $dest = 'mts';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_mts_straffic_teasernet_mobilabs') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['straffic']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            } else {
                if ($operator == 'mts') {
                    $link = $arr['mts'];
                    $dest = 'mts';
                } else {
                    if (is_straffic()) {
                        $link = $arr['straffic'];
                        $dest = 'straffic';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_modem_mts_second') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['mts']) or (empty($arr['megafon_second'])) or (empty($arr['beeline_second'])) or (empty($arr['mts_second']))) {
            error('Не указана требуемая ссылка');
        }
        $second = false;
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
            if ($second) {
                $link = $arr['megafon_second'];
                $dest = 'megafon_second';
            }
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                    if ($second) {
                        $link = $arr['beeline_second'];
                        $dest = 'beeline_second';
                    }
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            } else {
                if ($operator == 'mts') {
                    $link = $arr['mts'];
                    $dest = 'mts';
                    if ($second) {
                        $link = $arr['mts_second'];
                        $dest = 'mts_second';
                    }
                }
            }
        }
        exit('link: '.$link.' dest: '.$dest);
    }
    if ($arr['scheme'] == 'megafon_beeline_mts_teasernet_mobilabs') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['mts'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                } else {
                    // $link='n/a';
                    $link = $arr['beeline_modem'];
                    $dest = 'beeline_modem';
                }
            } else {
                if ($operator == 'mts') {
                    $link = $arr['mts'];
                    $dest = 'mts';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_mts_teasernet') {
        if (empty($arr['megafon']) or empty($arr['beeline'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            } else {
                if ($operator == 'mts') {
                    $link = 'n/a';
                    $dest = 'mts_teasernet';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_mts_straffic_teasernet') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['straffic'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            } else {
                if (is_straffic()) {
                    $link = $arr['straffic'];
                    $dest = 'straffic';
                } else {
                    if ($operator == 'mts') {
                        $link = 'n/a';
                        $dest = 'mts_teasernet';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_beeline_modem_ru_android') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['ru_android'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            } else {
                $geoip_country = get_geoip_country_code(request()->ip());
                if ($device->isAndroidOS() and $geoip_country == 'RU') {
                    $link = $arr['ru_android'];
                    $dest = 'ru_android';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_beeline_modem_straffic_mobiledetect') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['straffic'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            } else {
                if (is_straffic()) {
                    $link = $arr['straffic'];
                    $dest = 'straffic';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_beeline_modem_mobiledetect') {
        if (empty($arr['megafon']) or empty($arr['beeline'])) {
            error('Не указана требуемая ссылка');
        }
        if ($operator == 'megafon') {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if ($operator == 'beeline') {
                if ($device->isMobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_straffic') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['straffic'])) {
            error('Не указана требуемая ссылка');
        }
        if (is_megafon()) {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if (is_beeline()) {
                $link = $arr['beeline'];
                $dest = 'beeline';
            } else {
                if (is_straffic()) {
                    $link = $arr['straffic'];
                    $dest = 'straffic';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_beeline_modem_straffic') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['straffic'])) {
            error('Не указана требуемая ссылка');
        }
        if (is_megafon()) {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if (is_beeline()) {
                if (is_mobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                    // $link=$arr['ru_mobile']; $dest='beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            } else {
                if (is_straffic()) {
                    $link = $arr['straffic'];
                    $dest = 'straffic';
                }
                // if (is_straffic()) {$link=$arr['ru_mobile']; $dest='ru_mobile';};
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_beeline_modem_ru_mobile_straffic') {
        if (empty($arr['megafon']) or empty($arr['beeline']) or empty($arr['straffic'])) {
            error('Не указана требуемая ссылка');
        }
        if (is_megafon()) {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if (is_beeline()) {
                if (is_mobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                    // $link=$arr['ru_mobile']; $dest='beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            } else {
                $geoip_country = get_geoip_country_code(request()->ip());
                if (is_mobile() and ($geoip_country == 'RU')) {
                    $link = $arr['ru_mobile'];
                    $dest = 'ru_mobile';
                } else {
                    if (is_straffic()) {
                        $link = $arr['straffic'];
                        $dest = 'straffic';
                    }
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline_beeline_modem') {
        if (empty($arr['megafon']) or empty($arr['beeline'])) {
            error('Не указана требуемая ссылка');
        }
        if (is_megafon()) {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if (is_beeline()) {
                if (is_mobile()) {
                    $link = $arr['beeline'];
                    $dest = 'beeline';
                } else {
                    $link = 'n/a';
                    $dest = 'beeline_modem';
                }
            }
        }
    }
    if ($arr['scheme'] == 'megafon_beeline') {
        if (empty($arr['megafon']) or empty($arr['beeline'])) {
            error('Не указана требуемая ссылка');
        }
        if (is_megafon()) {
            $link = $arr['megafon'];
            $dest = 'megafon';
        } else {
            if (is_beeline()) {
                $link = $arr['beeline'];
                $dest = 'beeline';
            }
        }
    }
    if ($arr['scheme'] == 'megafon') {
        if (empty($arr['megafon'])) {
            error('Не указана требуемая ссылка');
        }
        if (is_megafon()) {
            $link = $arr['megafon'];
            $dest = 'megafon';
        }
    }
    if (isset($link,$dest)) {
        $return['link'] = $link;
        $return['dest'] = $dest;

        return $return;
    }

    return false;
}
