<?php

namespace MegawebV1;

function get_clickunder_link()
{
    global $config;
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    $domain = $_GET['domain'];

    return value_from_config_for_domain('teaser', 'clickunder_link', $domain);
}

function get_vk_group()
{
    global $config;
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    $domain = $_GET['domain'];

    return value_from_config_for_domain('teaser', 'vk_group', $domain);
}

function get_vk_api_id()
{
    global $config;
    if (empty($_GET['domain'])) {
        error('�� ������� �����');
    }
    $domain = $_GET['domain'];

    return value_from_config_for_domain('teaser', 'vk_api_id', $domain);
}

function get_link_count($type)
{
    // pre($_COOKIE);
    // if (isset($_GET['del'])) {
    //     setcookie('w['.$type.'][f]','');
    //     setcookie('w['.$type.'][s]','');
    //     pre($_COOKIE);
    // };
    if (empty($_COOKIE['w'][$type]['f'])) {
        setcookie('w['.$type.'][f]', '1', time() + 86400);

        return 1;
        // echo '������ ������';
    } else {
        if (empty($_COOKIE['w'][$type]['s'])) {
            setcookie('w['.$type.'][s]', '1', time() + 86400);

            return 2;
        } else {
            if (random_event(['f' => '50', 's' => '50']) == 'f') {
                return 1;
                // echo '������ ������ ������';
            } else {
                return 2;
                // echo '������ ������ ������';
            }
        }
    }
}

function device_name()
{
    global $config;
    $device = $config['Mobile_Detect'];
    if ($device->isMobile() or $device->isTablet()) {
        $device = 'mobile';
    } else {
        $device = 'pc';
    }

    return $device;
}

// ��������� ����� �������� cookie. ���� ���� ������ ��������� clicks, �� ����������� � ���� (������ �����)
// �������� ����� ���������� ���� � ����
function wap_redirect()
{
    global $config;
    $data = query('select domain,megafon,beeline,straffic,ru_mobile,ru_android,percent,latency,scheme,script_type,
        mts,mts_tablet,megafon_second,beeline_second,mts_second,beeline_modem,megafon_modem,megafon_modem_second,index_page,az,
        android_3g,android_wifi,tele2
        from '.tabname('teaser', 'wap').' where domain=\''.$_GET['domain'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['domain'])) {
        logger()->info('��������� ��� ������ '.$_GET['domain'].' �����������');

        return;
    }
    if (! is_numeric($arr['percent'])) {
        error('�� ������ �������');
    }
    if (empty($arr['scheme'])) {
    }
    if (! is_numeric($arr['latency'])) {
        error('�� ������� ��������');
    }
    $random_event = random_event(['yes' => $arr['percent'], 'no' => (100 - $arr['percent'])]);
    // if ($random_event=='no') {return;};
    $operator = check_operator();
    $device = $config['Mobile_Detect'];
    temp_var('device', device_name());
    // ���������� ������
    $geo_country = get_geoip_country_code(request()->ip());
    temp_var('country', $geo_country);
    //
    if (isset($_GET['operator'])) {
        $operator = $_GET['operator'];
    }
    if (isset($_GET['referer'])) {
        $_SERVER['HTTP_REFERER'] = $_GET['referer'];
    }
    if ($operator) {
        $is_operator = 1;
    } else {
        $is_operator = 0;
    }
    $show_teasers = 'yes';
    temp_var('is_operator', $is_operator);
    temp_var('show_teasers', $show_teasers);
    if ($random_event == 'no') {
        return template('pdp/common');
    }
    if (($arr['index_page'] == 0) and ($operator == 'megafon')) {
        if (! empty($_SERVER['HTTP_REFERER'])) {
            $url = parse_url($_SERVER['HTTP_REFERER']);
            if (empty($url['query']) and ($url['path'] == '/' or $url['path'] == '/index.html' or $url['path'] == '')) {
                $show_teasers = 'no';
                temp_var('show_teasers', $show_teasers);

                return template('pdp/index_megafon');
            }
        }
    }
    // temp_var('show_teasers',$show_teasers);
    $result = wap_schemes(['arr' => $arr, 'operator' => $operator, 'device' => $device]);
    // pre($result);
    if ($result) {
        $link = $result['link'];
        $dest = $result['dest'];
    }
    if (! empty($link)) {
        if (! isset($dest)) {
            error('������ link, �� destination �����������', '������ link="'.$link.'", �� destination �����������');
        }
        temp_var('link', $link);
        temp_var('latency', $arr['latency']);
        if ($arr['script_type'] == 'redirect') {
            if ($arr['latency'] == 0) {
                $script = template('pdp/redirect');
            } else {
                $script = template('pdp/redirect_latency');
            }
        }
        /*
        if ($arr['script_type']=='clickunder') {$script=template('pdp/clickunder');};
        if ($arr['script_type']=='clickunder_adprofy') {$script=template('pdp/clickunder_adprofy');};
        if ($arr['script_type']=='confirm') {$script=template('pdp/confirm');};
        if ($arr['script_type']=='confirm_styled') {$script=template('pdp/confirm_styled');};
        if ($arr['script_type']=='url_for_teasers') {$script=template('pdp/url_for_teasers');};
        if ($arr['script_type']=='fullscreen_pic') {$script=template('pdp/fullscreen_pic');};
        */
        $script = template('pdp/'.$arr['script_type']);
        query('insert into '.tabname('teaser', 'wap_clicks').' set ip=inet_aton(\''.request()->ip().'\'), domain=\''.$_GET['domain'].'\', link=\''.$link.'\', dest=\''.$dest.'\'');

        return $script;
    }
    if (isset($link)) {
        if (isset($dest)) {
            error('������ destination, �� ������ ������', '������ destination="'.$dest.'", �� ������ ������');
        }
        if (! isset($dest)) {
            error('������ ������ � ����������� destination');
        }
    }
    if (isset($dest)) {
        error('������ destination, �� ������ �����������', '������ destination="'.$dest.'", �� ������ �����������');
    }

    // die('no link');
    // error('����� �� ����������','����� '.$arr['scheme'].' �� ����������');
    // return '�����';
    return template('pdp/common');
}

function xrest_mobile_bodyclick($text, $block)
{
    $content = $text;
    if ($block == 3 or $block == 4 or $block == 13) {
        if (! is_mobile()) {
            if ($block == 3) {
                $content = template('blocks/bodyclick.net/xrest.net/v1');
            }
            if ($block == 4) {
                $content = template('blocks/bodyclick.net/xrest.net/v2');
            }
            if ($block == 13) {
                $content = template('blocks/bodyclick.net/xrest.net/h');
            }
            $content = str_replace('\'', '\\\'', $content);
        }
    }

    return $content;
}

function teaser_init()
{
    global $config;
    include $config['path']['server'].'types/teaser/inc/admin.php';
    // include($config['path']['server'].'types/videohub/inc/redirect/straffic.php');
    // include($config['path']['server'].'types/videohub/inc/redirect/megafon_beeline_android.php');
    include $config['path']['server'].'types/teaser/inc/redirect/straffic.php';
    include $config['path']['server'].'types/teaser/inc/redirect/megafon_beeline_android.php';
    include $config['path']['server'].'types/teaser/inc/redirect/wap_schemes.php';
    if (empty($config['vars']['page'])) {
        teaser_rewrite_query();
    }
}

function teaser_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^dom-mgwclk\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'dom/dom_clickunder';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^vk\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'dp/dpplugin_real';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^dom-vk\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'dom/dom_vk_real';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^dp\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'dp/dpplugin';
        // $config['vars']['page']='dp/dpplugin_real';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^dom-dp\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'dom/dom_vk';
        // $config['vars']['page']='dom/dom_vk_real';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^dom-w\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'dom/dom_wap';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^dom-dp-w\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'dom/dom_vk_wap';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^w\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'pdp/wap';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^get([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'get';
        $_GET['b'] = $args[1];

        return;
    }
    if (preg_match('/^get\/([\w.-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'get_all';
        $_GET['domain'] = $args[1];

        return;
    }
    if (preg_match('/^click([\d]+)-([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'click';
        $_GET['b'] = $args[1];
        $_GET['t'] = $args[2];

        return;
    }
    if (preg_match('/^admin$/', $query, $args)) {
        $config['vars']['page'] = 'admin/index';

        return;
    }
}

function teasers_click()
{
    if (isset($_GET['b'],$_GET['t']) == 0) {
        error('�� �������� ����������� ���������');
    }
    if (is_numeric($_GET['b']) == 0) {
        error('�������� ������');
    }
    if (is_numeric($_GET['t']) == 0) {
        error('�������� ������');
    }
    $block_id = $_GET['b'];
    $teaser_id = $_GET['t'];
    $data = query('select domain,dest from '.tabname('teaser', 'blocks').' where id=\''.$block_id.'\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr) == 1) {
        error('���� �� ����������');
    }
    $domain = $arr['domain'];
    $dest = $arr['dest'];
    $data = query('select id,link from '.tabname('teaser', 'teasers').' where id=\''.$teaser_id.'\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('����� �� ����������');
    }
    // ���� � ������ ������� ������, �� ��������� �� ���, ���� ���, �� ���������� �� dest �����
    if (empty($arr['link'])) {
        $location = 'http://go.awmengine.net/?from='.$domain.'&to='.$dest.'&t='.$teaser_id;
    } else {
        $location = $arr['link'];
    }
    // if (empty($arr['link'])==0) {$dest=$arr['link'];}; //��������������, �.�. ����� ���� ������� ������ �������� ������, �� ��� ������������ � &to
    // query('update '.tabname('teaser','teasers_stat').' set clicks=clicks+1 where teaser_id=\''.$teaser_id.'\'');
    query('insert into '.tabname('teaser', 'clicks').' set block_id=\''.$block_id.'\', teaser_id=\''.$teaser_id.'\', ip=inet_aton(\''.request()->ip().'\')');
    // $last_id=mysql_insert_id();
    header('Location: '.$location);
    // redirect($domain,$dest);
}

function get_all_teasers_for_site()
{
    global $config;
    if (isset($_GET['domain']) == 0) {
        error('�� �������� ����������� ���������', '', 1, 0, 1);
    }
    $domain = $_GET['domain'];
    if (isset($_GET['pdp_url'])) {
        temp_var('pdp_url', check_input($_GET['pdp_url']));
    }
    $data = query('select id,q,type,set_name from '.tabname('teaser', 'blocks').' where domain=\''.$domain.'\' and active=\'1\';');
    $blocks_arr = readdata($data, 'nokey');
    if (empty($blocks_arr) == 1) {
        error('������ ��� ������� ������ �� ����������');
    }
    // $data=query('select * from '.tabname('teaser','teasers').' where active=1');
    // $teasers_arr=readdata($data,'id');
    $script = '';
    foreach ($blocks_arr as $v) {
        $list = '';
        $data = query('select * from '.tabname('teaser', 'teasers').'
        join '.tabname('teaser', 'sets').' on sets.teaser_id=teasers.id
        where active=1 and sets.name=\''.$v['set_name'].'\';
        ');
        $arr = readdata($data, 'id');
        // $arr=$teasers_arr; //������ ���������� � ������ ��� ������
        temp_var('block_id', $v['id']);
        for ($i = 0; $i <= ($v['q'] - 1); $i++) {
            $rnd = rnd_from_array($arr);
            // query('insert into '.tabname('teaser','teasers_stat').' set teaser_id=\''.$rnd['id'].'\', views=1 on duplicate key update views=views+1');
            temp_var('teaser_id', $rnd['id']);
            temp_var('teaser_title', $rnd['title']);
            if (! empty($rnd['description'])) {
                temp_var('teaser_description', $rnd['description']);
            }
            temp_var('teaser_src', $rnd['src']);
            $list .= template('types/'.$v['type']);
            unset($arr[$rnd['id']]);
        }
        if (empty($list)) {
            error('����������� ����������');
        }
        temp_var('list', $list);
        $content = template('types/'.$v['type'].'_container');
        // �������: �������� ��� ���������� ����� �� ������
        $content = xrest_mobile_bodyclick($content, $v['id']);
        $content = str_replace("\n", '', str_replace("\r\n", '', $content));
        $script .= 'if (document.getElementById(\'teaser-'.$v['id'].'\')!=null) {document.getElementById(\'teaser-'.$v['id'].'\').innerHTML=\''.$content.'\';};
        ';
    }

    return $script;
}

function get_teasers()
{
    global $config;
    if (isset($_GET['b']) == 0) {
        error('�� �������� ����������� ���������', '', 1, 0, 1);
    }
    if (is_numeric($_GET['b']) == 0) {
        error('�������� ������');
    }
    if (isset($_GET['pdp_url'])) {
        temp_var('pdp_url', check_input($_GET['pdp_url']));
    }
    $block_id = $_GET['b'];
    $data = query('select domain,q,type,set_name from '.tabname('teaser', 'blocks').' where id=\''.$block_id.'\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr) == 1) {
        error('���� �� ����������');
    }
    $q = $arr['q'];
    $type = $arr['type'];
    $domain = $arr['domain'];
    $set_name = $arr['set_name'];
    // $data=query('select * from '.tabname('teaser','teasers').' where active=1');
    $data = query('select * from '.tabname('teaser', 'teasers').'
        join '.tabname('teaser', 'sets').' on sets.teaser_id=teasers.id
        where active=1 and sets.name=\''.$arr['set_name'].'\';
        ');
    $arr = readdata($data, 'id');
    $list = '';
    temp_var('block_id', $block_id);
    for ($i = 0; $i <= ($q - 1); $i++) {
        $rnd = rnd_from_array($arr);
        query('insert into '.tabname('teaser', 'teasers_stat').' set teaser_id=\''.$rnd['id'].'\', views=1 on duplicate key update views=views+1');
        temp_var('teaser_id', $rnd['id']);
        temp_var('teaser_title', $rnd['title']);
        if (! empty($rnd['description'])) {
            temp_var('teaser_description', $rnd['description']);
        }
        temp_var('teaser_src', $rnd['src']);
        $list .= template('types/'.$type);
        // $list.='<li style="height: 210px;"><a href="'.http_current_domain().'/?page=click&amp;b='.$block_id.'&amp;t='.$rnd['id'].'" target="_blank" style="font-size: 12px; font-weight: bold;"><img src="http://img.'.current_domain().'/types/teaser_img/template/images/'.$rnd['src'].'" alt="'.$rnd['title'].'" width="160" height="160" />'.$rnd['title'].'</a></li>';
        unset($arr[$rnd['id']]);
    }
    if (empty($list)) {
        error('����������� ����������');
    }
    temp_var('list', $list);
    $content = template('types/'.$type.'_container');
    // �������: �������� ��� ���������� ����� �� ������
    $content = xrest_mobile_bodyclick($content, $block_id);
    $content = str_replace("\n", '', str_replace("\r\n", '', $content));

    return $content;
}

function console_with_frod($block)
{
    if (isset($_GET['domain']) == 0) {
        error('�� �������� ����������� ���������');
    }
    $path_console = 'console/'.$_GET['domain'].'/'.$block;
    $path_teasers = 'teasers/'.$_GET['domain'].'/'.$block;
    $event = random_event(['console' => 90, 'our' => 10]);
    if ($event == 'console') {
        $content = js_document_write(str_replace('\'', '\\\'', template($path_console)));
    }
    if ($event == 'our') {
        $content = js_document_write(template($path_teasers));
    }

    return $content;
}

function js_get_iframe()
{
    if (isset($_GET['b']) == 0) {
        error('�� �������� ����������� ���������');
    }
    if (is_numeric($_GET['b']) == 0) {
        error('�������� ������');
    }
    $block_id = $_GET['b'];
    $data = query('select domain,q,type from '.tabname('teaser', 'blocks').' where id=\''.$block_id.'\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr) == 1) {
        error('���� �� ����������');
    }
    $content = '<iframe scrolling="no" frameborder="0" marginheight="0" marginwidth="0" allowtransparency="true" vspace="0" hspace="0" style="padding:0; margin:0; border:0px;" src="/?page=frame&amp;b='.$block_id.'"></iframe>';

    return $content;
}
