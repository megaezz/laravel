<?php

function telegram_post_to_channel($args)
{
    $bot_token = empty($args['bot_token']) ? error('Не передан api бота') : $args['bot_token'];
    $channel_id = empty($args['channel_id']) ? error('Не передан ID канала') : $args['channel_id'];
    $text = empty($args['text']) ? error('Не передан текст') : $args['text'];
    $data = [
        'chat_id' => $channel_id,
        'text' => $text,
        'parse_mode' => 'html',
        'disable_web_page_preview' => true,
    ];
    $response = file_get_contents('https://api.telegram.org/bot'.$bot_token.'/sendMessage?'.http_build_query($data));

    // pre(var_dump($response));
    return $response;
}

function telegram_pavlik_post($args)
{
    $channel_id = empty($args['channel_id']) ? error('Не передан ID канала') : $args['channel_id'];
    $text = empty($args['text']) ? error('Не передан текст') : $args['text'];
    $bot_token = '590663345:AAHKtjy9wLgdKfcKDpmErSTnrxerA6z2xoU';

    return telegram_post_to_channel(['bot_token' => $bot_token, 'channel_id' => $channel_id, 'text' => $text]);
}

function telegram_post_to_megaezh($args = [])
{
    $text = empty($args['text']) ? error('Text needed') : $args['text'];

    return telegram_pavlik_post([
        'channel_id' => '@hardcrypt',
        'text' => $text,
    ]);
    // 'text'=>mb_convert_encoding('тест http://google.com','utf-8','windows-1251')
}

function crypto_init()
{
    include $_SERVER['DOCUMENT_ROOT'].'/types/crypto/inc/airdrop.php';
}

function links_wrap($text)
{
    return preg_replace('|([\w\d]*)\s?(https?://([\d\w\.-]+\.[\w\.]{2,6})[^\,\s\]\[\<\>]*/?)|i', '$1 <a href="$2" target="_blank">$2</a>', $text);
}

function work_list()
{
    $arr = query_arr('select id,title,description,link,date,ref_link from '.typetab('work').' where not done order by date desc;');
    // pre($arr);
    $list = '';
    foreach ($arr as $v) {
        temp_var('title', $v['title']);
        $v['description'] = links_wrap($v['description']);
        temp_var('description', nl2br($v['description']));
        temp_var('date', $v['date']);
        temp_var('work_id', $v['id']);
        $list .= template('work/list');
        clear_temp_vars();
    }
    if (empty($arr)) {
        $list = template('work/all_done');
    }

    return $list;
}

function submit_add_work()
{
    // pre($_POST);
    $title = empty($_POST['title']) ? '' : mysql_real_escape_string($_POST['title']);
    $description = empty($_POST['description']) ? error('No description') : nl2br(mysql_real_escape_string($_POST['description']));
    query('insert into '.typetab('work').' set title=\''.$title.'\', description=\''.$description.'\',
		telegram_title=\''.$title.'\', telegram_description=\''.$description.'\'
		;');
    redirect('/?page=work/add_work');
    // alert('Added. <a href="/?page=work/add_work">Add more</a>');
}

function submit_work_done()
{
    $work_id = empty($_POST['work_id']) ? error('Work ID needed') : mysql_real_escape_string($_POST['work_id']);
    $link = empty($_POST['link']) ? '' : trim(mysql_real_escape_string($_POST['link']));
    $comments = empty($_POST['comments']) ? '' : mysql_real_escape_string($_POST['comments']);
    $ok = empty($_POST['ok']) ? 0 : 1;
    $telegram_title = isset($_POST['telegram_title']) ? mysql_real_escape_string($_POST['telegram_title']) : '';
    $telegram_description = isset($_POST['telegram_description']) ? mysql_real_escape_string($_POST['telegram_description']) : '';
    $telegram_ref_link_text = isset($_POST['telegram_ref_link_text']) ? mysql_real_escape_string($_POST['telegram_ref_link_text']) : '';
    query('update '.typetab('work').' set done=1, ref_link=\''.$link.'\', comments=\''.$comments.'\', ok=\''.$ok.'\',
		telegram_title=\''.$telegram_title.'\', telegram_description=\''.$telegram_description.'\',
		telegram_ref_link_text=\''.$telegram_ref_link_text.'\'
		where id=\''.$work_id.'\';');
    // alert('Задание отредактировано. <a href="/">К списку всех заданий</a>. <a href="/?page=work/done">К списку выполненных заданий</a>');
    // redirect('/?page=work/done#work_'.$work_id);
    redirect(referer_url().'#work_'.$work_id);
}

function done_work_list($args = [])
{
    $template = empty($args['template']) ? 'work/list_done' : $args['template'];
    $ok = isset($_GET['ok']) ? mysql_real_escape_string($_GET['ok']) : 'all';
    $mysql['ok'] = ($ok === 'all') ? '1' : (($ok == 1) ? 'ok' : 'not ok');
    $arr = query_arr('select id,title,description,link,date,ref_link,ok,comments,
		telegram_title,telegram_description,telegram_ref_link_text,
		if(date(telegram_last_post_time)=current_date,date_format(telegram_last_post_time,\'Сегодня в %H:%i\'),if(date(telegram_last_post_time)=(current_date-interval 1 day),date_format(telegram_last_post_time,\'Вчера в %H:%i\'),if(date(telegram_last_post_time)=(current_date-interval 2 day),date_format(telegram_last_post_time,\'Позавчера в %H:%i\'),date_format(telegram_last_post_time,\'%Y-%m-%d в %H:%i\')))) as telegram_last_post_time_human
		from '.typetab('work').' where done and '.$mysql['ok'].' order by date desc;');
    // pre($arr);
    $list = '';
    foreach ($arr as $v) {
        temp_var('title', $v['title']);
        temp_var('description', nl2br($v['description']));
        temp_var('date', $v['date']);
        temp_var('work_id', $v['id']);
        temp_var('ok_active_true', $v['ok'] ? 'active' : '');
        temp_var('ok_active_false', $v['ok'] ? '' : 'active');
        temp_var('ok_checked_true', $v['ok'] ? 'checked' : '');
        temp_var('ok_checked_false', $v['ok'] ? '' : 'checked');
        temp_var('ref_link', $v['ref_link']);
        temp_var('comments', htmlspecialchars($v['comments']));
        temp_var('telegram_title', $v['telegram_title']);
        temp_var('telegram_description', $v['telegram_description']);
        temp_var('telegram_description_wraped_links', links_wrap(nl2br($v['telegram_description'])));
        temp_var('telegram_ref_link_text', $v['telegram_ref_link_text']);
        temp_var('telegram_last_post_time_human', empty($v['telegram_last_post_time_human']) ? 'отсутствует' : $v['telegram_last_post_time_human']);
        $list .= template($template);
        clear_temp_vars();
    }

    return $list;
}

function ajax_is_work_exist()
{
    $text = empty($_POST['text']) ? error('Text needed') : trim(mysql_real_escape_string($_POST['text']));
    $arr = query_arr('select title,description,link from '.typetab('work').' where description like \'%'.$text.'%\';');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li><p>'.$v['title'].'</p><p>'.$v['description'].'</p></li>';
    }

    return '<ul>'.$list.'</ul>';
}

function submit_post_to_telegram()
{
    $id = empty($_GET['work_id']) ? error('Work ID needed') : mysql_real_escape_string($_GET['work_id']);
    $arr = query_assoc('select telegram_title, telegram_description,telegram_ref_link_text,ref_link from '.typetab('work').' where id=\''.$id.'\';');
    temp_var('title', $arr['telegram_title']);
    temp_var('description', $arr['telegram_description']);
    temp_var('ref_link', $arr['ref_link']);
    temp_var('ref_link_text', $arr['telegram_ref_link_text']);
    $text = template('work/telegram/post');
    // return $text;
    $result = telegram_post_to_megaezh(['text' => $text]);
    if ($result) {
        query('update '.typetab('work').' set telegram_last_post_time=current_timestamp where id=\''.$id.'\';');
    }

    return $result ? 'Отправлено' : 'Ошибка отправки';
    // return 'Ok';
}

function list_airdrops()
{
    $args['template'] = 'airdrops/list';

    // $args['template']='';
    return done_work_list($args);
}

function test_send_mail()
{
    $from = 'crypto@awmzone.net';
    $to = 'mas90@mail.ru';
    $subject = 'Test sending';
    $body = '<b>Hi</b> there!))';
    sendHtmlMail($from, $to, $subject, $body);
}

// function submit_edit_work() {
// 	$work_id=empty($_POST['work_id'])?error('Work ID needed'):mysql_real_escape_string($_POST['work_id']);
// 	$ref_link=empty($_POST['ref_link'])?'':mysql_real_escape_string($_POST['ref_link']);
// 	$comments=empty($_POST['comments'])?'':mysql_real_escape_string($_POST['comments']);
// 	$ok=empty($_POST['ok'])?0:1;
// 	query('update '.typetab('work').' set done=1, ref_link=\''.$link.'\', comments=\''.$comments.'\',ok=\''.$ok.'\' where id=\''.$work_id.'\';');
// 	redirect('/');
// }
