<?php

function referal_rating_list()
{
    $arr = query_arr('select requests.ref_id, r.telegram, r.twitter, count(*) as q
		from '.typetab('requests').'
		join '.typetab('requests').' r on r.id=requests.ref_id
		group by requests.ref_id
		order by q desc
		limit 50
		;');
    $list = '';
    foreach ($arr as $v) {
        temp_var('telegram', substr_replace($v['telegram'], '...', -5, 3));
        temp_var('twitter', $v['twitter']);
        temp_var('q_refs', $v['q']);
        $list .= template('airdrop/rating/list');
    }

    return $list;
}

function submit_user_balance()
{
    $eth = empty($_POST['eth']) ? false : mysql_real_escape_string($_POST['eth']);
    $user_id = get_user_id_from_eth(['eth' => $eth]);
    if (strlen($eth) < 20) {
        exit('Incorrect ETH address');
    }
    // pre($user_id);
    $arr = query_assoc('select id_hash from '.typetab('requests').' where id=\''.$user_id.'\';');
    temp_var('request_id_hash', $arr['id_hash']);
    $arr = query_assoc('select count(*) as q from '.typetab('requests').' where ref_id=\''.$user_id.'\';');
    $q_refs = $arr['q'];
    $balance = 0.005 + $q_refs * 0.005;
    temp_var('q_refs', $q_refs);
    temp_var('balance', $balance);

    return template('airdrop/ajax/user_request');
}

function print_get_ref_hash()
{
    return empty($_GET['ref']) ? '' : mysql_real_escape_string($_GET['ref']);
}

function get_user_id_from_id_hash($args = [])
{
    $hash = empty($args['hash']) ? false : mysql_real_escape_string($args['hash']);
    if (! $hash) {
        return false;
    }
    $arr = query_assoc('select id from '.typetab('requests').' where id_hash=\''.$hash.'\';');
    if (! $arr) {
        exit('Incorrect referal link');
    }

    return $arr['id'];
}

function get_user_id_from_eth($args = [])
{
    $eth = empty($args['eth']) ? false : mysql_real_escape_string($args['eth']);
    if (! $eth) {
        return false;
    }
    $arr = query_assoc('select id from '.typetab('requests').' where eth=\''.$eth.'\';');
    if (! $arr) {
        exit('No such ETH address in our database');
    }

    return $arr['id'];
}

function submit_airdrop_user_request()
{
    $name = empty($_POST['name']) ? '' : mysql_real_escape_string($_POST['name']);
    $email = empty($_POST['email']) ? '' : mysql_real_escape_string($_POST['email']);
    $twitter = empty($_POST['twitter']) ? exit('Fill your twitter account') : mysql_real_escape_string($_POST['twitter']);
    $facebook = empty($_POST['facebook']) ? '' : mysql_real_escape_string($_POST['facebook']);
    $telegram = empty($_POST['telegram']) ? exit('Fill your telegram account') : mysql_real_escape_string($_POST['telegram']);
    $eth = empty($_POST['eth']) ? exit('Fill your ETH wallet') : mysql_real_escape_string($_POST['eth']);
    $ref_hash = empty($_POST['ref']) ? false : mysql_real_escape_string($_POST['ref']);
    $ref_id = get_user_id_from_id_hash(['hash' => $ref_hash]);
    $mysql['is_fake'] = empty($_POST['is_fake']) ? 'null' : ($_POST['is_fake'] ? '1' : 'null');
    $mysql['ref_id'] = ($ref_id) ? '\''.$ref_id.'\'' : 'null';
    query('insert into '.typetab('requests').' set name=\''.$name.'\', email=\''.$email.'\', 
		twitter=\''.$twitter.'\', facebook=\''.$facebook.'\', telegram=\''.$telegram.'\', eth=\''.$eth.'\', ref_id='.$mysql['ref_id'].', is_fake=\''.$mysql['is_fake'].'\' on duplicate key update eth=eth;');
    if (! mysql_affected_rows()) {
        exit('Sorry, some data are already in our database, please contact us on telegram to fix a problem.');
    }
    // alert(mysql_insert_id());
    $request_id = mysql_insert_id();
    $request_id_hash = sha1($request_id.'ref');
    query('update '.typetab('requests').' set id_hash=\''.$request_id_hash.'\' where id=\''.$request_id.'\';');
    temp_var('request_id_hash', $request_id_hash);
    temp_var('q_refs', 0);
    temp_var('balance', 0.005);

    return template('airdrop/ajax/user_request');
}

function create_fake_users()
{
    $num = empty($_GET['q']) ? alert('Set q of new users') : mysql_real_escape_string($_GET['q']);
    $arr = query_assoc('select max(id) as max_id from '.typetab('requests').';');
    for ($i = $arr['max_id']; $i < ($arr['max_id'] + $num); $i++) {
        $_POST['telegram'] = $i;
        $_POST['twitter'] = $i;
        $_POST['eth'] = $i;
        $_POST['is_fake'] = true;
        submit_airdrop_user_request();
    }
    alert('Created '.$num.' fake users');
}

function set_fake_referals()
{
    $arr_fake_partners = query_arr('select id,percent from '.typetab('requests').' where is_fake_partner and is_fake;');
    foreach ($arr_fake_partners as $v) {
        $arr_rnd[$v['id']] = $v['percent'];
    }
    // $event=random_event($arr_rnd);
    // pre($event);
    if (empty($arr_fake_partners)) {
        error('Empty partners list');
    }
    $arr_fake_users = query_arr('select id from '.typetab('requests').' where is_fake and ref_id is null;');
    foreach ($arr_fake_users as $v) {
        $rnd_partner_id = random_event($arr_rnd);
        query('update '.typetab('requests').' set ref_id=\''.$rnd_partner_id.'\' where id=\''.$v['id'].'\';');
    }
    alert('Done');
}
