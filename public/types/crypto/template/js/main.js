document.addEventListener('DOMContentLoaded',crypto_init);

function crypto_init() {
	
}

function roleHandler(target) {
	if (target.dataset.role=='ajax_is_work_exist') {
		// alert('test');
		args={form_id:'form_is_work_exist',url:'/?page=work/submit_is_work_exist',
		button_id:'button_is_work_exist',result_id:'result_is_work_exist',
		location_reload:false};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_post_to_telegram') {
		if (!confirm('Точно запостить?')) {return;};
		ajax_request_new({id:'result_post_to_telegram_'+target.dataset.id,link:'/?page=work/telegram/submit_post&work_id='+target.dataset.id});
	};
	if (target.dataset.role=='fast_ref_link_text') {
		info=document.getElementById('ref_link_text_input_'+target.dataset.id);
		// сначала пробел, иначе не убирается placeholder
		info.value=' ';
		info.value=target.dataset.value;
	};
	if (target.dataset.role=='ajax_user_request') {
		// alert('test');
		args={form_id:'form_user_request',url:'/?page=airdrop/ajax/submit_user_request',
		result_id:'result',
		location_reload:false,success_function() {
			$('#form_user_request').empty();
		}};
		ajax_submit_post_button_modal(args);
	};
	if (target.dataset.role=='ajax_user_balance') {
		// alert('test');
		args={form_id:'form_user_balance',url:'/?page=airdrop/ajax/submit_user_balance',
		result_id:'result',
		location_reload:false,success_function() {
			$('#form_user_balance').empty();
		}};
		ajax_submit_post_button_modal(args);
	};
}

function ajax_submit_post_button_modal(args) {
	var	form_id=args.form_id;
	var url=args.url;
	var button_id=args.button_id;
	var modal_id=args.modal_id || false;
	var modal_hide=args.modal_hide || false;
	var reload_page=args.reload_page || false;
	var reload_id=args.reload_id || false;
	var result_id=args.result_id || false;
	var button_reset=args.button_reset || false;
	var success_text=args.success_text || false;
	var success_function=args.success_function || false;
	var success_function_reload=args.success_function_reload || false;
	var location_reload=args.location_reload || false;
	if (button_id) {
		button_default=$('#'+button_id).text();
		$btn=$('#'+button_id).button('loading');
	};
	queryString=$('#'+form_id).serialize();
	$.ajax({
		type: "POST",
		url: url,
		data: queryString,
		success: function(answer) {
			if (success_function) {success_function();};
			if (result_id) {
				if (success_text) {
					if (answer!=success_text) {alert(answer);};
				};
				$('#'+result_id).empty();
				$('#'+result_id).append(answer);
			};
			if (button_id) {
				$('#'+button_id).empty();
				$('#'+button_id).append(button_default);
				if (button_reset) {
					$('#'+button_id).button('reset');
				};
			};
			if (modal_hide && modal_hide==true) {
				$('#'+modal_id).modal('hide');
			};
			if (reload_id && reload_page) {
				ajax_request_new({id:reload_id,link:reload_page,success_function:success_function_reload});
			};
			if (location_reload) {location.reload();};
		}
	});
}