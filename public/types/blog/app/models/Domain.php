<?php

namespace blog\app\models;

use engine\app\models\Domain as EngineDomain;
use engine\app\models\F;
use engine\app\models\Template;

class Domain extends EngineDomain
{
    private $articlesPaginationUrlPattern = null;

    private $articlesLimit = null;

    private $viewArticleUrlPattern = null;

    public function __construct($domain = null)
    {
        parent::__construct($domain);
        $arr = $this->getData($domain);
        if (! $arr) {
            F::error('Domain doesn\'t exist for blog type');
        }
        $this->articlesPaginationUrlPattern = $arr['articlesPaginationUrlPattern'];
        $this->viewArticleUrlPattern = $arr['viewArticleUrlPattern'];
        $this->articlesLimit = $arr['articlesLimit'];
    }

    public static function getData($domain = null)
    {
        if (! $domain) {
            F::error('Specify domain first');
        }
        $arr = F::query_assoc('
			select domain,articlesPaginationUrlPattern,articlesLimit,viewArticleUrlPattern
			from '.F::typetab('domains').'
			where domain=\''.$domain.'\';
			');

        return $arr;
    }

    public function getArticlesPaginationUrlPattern()
    {
        return $this->articlesPaginationUrlPattern;
    }

    public function getViewArticleUrlPattern()
    {
        return $this->viewArticleUrlPattern;
    }

    public function getViewArticleLink($article = null)
    {
        if (! $this->getViewArticleUrlPattern()) {
            F::error('viewArticleUrlPattern required to use Domain::getViewArticleLink');
        }
        $t = new Template;
        $t->setContent($this->getViewArticleUrlPattern());
        $t->v('id', $article->getId());
        $t->v('chpu', $article->getChpu());

        return $t->get();
    }

    public function getArticlesLimit()
    {
        return $this->articlesLimit;
    }
}
