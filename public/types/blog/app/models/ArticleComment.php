<?php

namespace blog\app\models;

use engine\app\models\F;

class ArticleComment extends \engine\app\models\Comment
{
    public function __construct($id = null)
    {
        $this->setSqlTable(F::typetab('comments'));
        $this->setItemFieldName('article_id');
        parent::__construct($id);
    }
}
