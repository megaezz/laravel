<?php

namespace blog\app\models;

use engine\app\models\F;

class ArticleComments extends \engine\app\models\Comments
{
    public function __construct()
    {
        $this->setSqlTable(F::typetab('comments'));
        $this->setItemFieldName('article_id');
        parent::__construct($id);
    }
}
