<?php

namespace blog\app\models;

class Articles extends \engine\app\models\Articles
{
    protected const TYPE = 'blog';

    protected const CLASS_ARTICLE = 'blog\app\models\Article';

    public function setListVars($t = null, $article = null)
    {
        parent::setListVars($t, $article);
        if ($article->getDomain()) {
            $domain = new Domain($article->getDomain());
            $t->v('viewArticleLink', $domain->getViewArticleLink($article));
        } else {
            $t->v('viewArticleLink', '');
        }
    }
}
