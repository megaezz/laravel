<?php

namespace blog\app\controllers\admin;

use blog\app\models\Article;
use blog\app\models\Articles;
use cinema\app\controllers\CinemaAuthorizationController;
use engine\app\models\Domain;
use engine\app\models\Domains;
use engine\app\models\F;
use engine\app\models\Template;

class ArticlesController
{
    public function __construct()
    {
        $auth = new CinemaAuthorizationController(['admin-lite', 'admin']);
    }

    public function index()
    {
        $t = new Template('admin/index');
        $t->v('title', 'Статьи');
        $articles = new Articles;
        $articles->setOrder('articles.add_date desc');
        $articles->setLimit(30);
        $articles->setPage(1);
        $articles->setListTemplate('admin/articles/list');
        $t->v('articlesList', $articles->getList());

        return $t->get();
    }

    public function add()
    {
        $id = Article::add();
        F::redirect('/?r=admin/Articles/editor&id='.$id);
    }

    public function editor()
    {
        $id = empty($_GET['id']) ? F::error('Article ID required') : F::checkstr($_GET['id']);
        $article = new Article($id);
        $t = new Template('admin/articles/editor');
        //
        $images = $article->getImages();
        $listImg = '';
        $listOpt = '';
        foreach ($images as $v) {
            if ($article->getMainImage() === $v) {
                $active = 'active';
                $selected = 'selected';
            } else {
                $active = '';
                $selected = '';
            }
            $listImg .= '
			<img class="'.$active.'" src="'.$article->getImagesUrl().'/'.$v.'" data-file="'.$v.'">
			<!--
			<button class="btn btn-sm" style="position: relative; top: -35px; left: -15px;">
			<i class="fas fa-trash-alt"></i>
			</button>
			-->
			';
            $listOpt .= '<option '.$selected.'>'.$v.'</option>';
        }
        $t->v('imagesList', $listImg);
        $t->v('imagesOptions', $listOpt);
        //
        $t->v('title', 'Редактирование статьи #'.$article->getId());
        $t->v('articleId', $article->getId());
        $t->v('articleTitle', $article->getTitle());
        $t->vf('articleText', htmlspecialchars($article->getText()));
        $t->vf('articleTextPreview', htmlspecialchars($article->getTextPreview()));
        $t->v('articleChpu', $article->getChpu());
        $articles = new Articles;
        $articles->setOrder('articles.add_date desc');
        $articles->setLimit(30);
        $articles->setPage(1);
        $articles->setActiveId($id);
        $articles->setListTemplate('admin/articles/list');
        $t->v('articlesList', $articles->getList());
        // получаем список доменов
        $domains = new Domains;
        $domains->setType('blog');
        $list = '';
        foreach ($domains->get() as $v) {
            $domainObj = new Domain($v['domain']);
            $lastDomain = $domainObj->getLastRedirectDomain();
            $selected = ($article->getDomain() == $v['domain']) ? 'selected' : '';
            $list .= '<option value="'.$v['domain'].'" '.$selected.'>'.$lastDomain.'</option>';
        }
        $t->v('domainsOptionList', $list);
        // селекторы активации
        $list = '';
        $arr = ['Нет' => false, 'Да' => true];
        foreach ($arr as $txt => $v) {
            $selected = ($article->getActive() == $v) ? 'selected' : '';
            $list .= '<option value="'.($v ? 1 : 0).'" '.$selected.'>'.$txt.'</option>';
        }
        $t->v('activeOptionList', $list);

        return $t->get();
    }

    public function save()
    {
        $id = empty($_POST['id']) ? F::error('Article ID reuired') : $_POST['id'];
        $title = empty($_POST['title']) ? null : $_POST['title'];
        $mainImage = empty($_POST['mainImage']) ? null : $_POST['mainImage'];
        $text = empty($_POST['text']) ? null : $_POST['text'];
        $textPreview = empty($_POST['textPreview']) ? null : $_POST['textPreview'];
        $domain = empty($_POST['domain']) ? null : $_POST['domain'];
        $active = empty($_POST['active']) ? false : true;
        $chpu = empty($_POST['chpu']) ? null : $_POST['chpu'];
        $article = new Article($id);
        $article->setTitle($title);
        // убираем тэг редактора
        $text = str_replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>', '', $text);
        $text = str_replace('rel="noopener noreferrer"', '', $text);
        // F::pre($text);
        $article->setText($text);
        $article->setTextPreview($textPreview);
        $article->setMainImage($mainImage);
        $article->setDomain($domain);
        $article->setChpu($chpu ? $chpu : str_replace(' ', '_', F::translitRu($article->getTitle())));
        // если активируется то прописываем дату активации
        if ($article->getActive() === false and $active === true) {
            $date = new \DateTime;
            $article->setApplyDate($date->format('Y-m-d H:i:s'));
        }
        $article->setActive($active);
        $article->save();
        // F::alert('Article #'.$article->getId().' has been saved');
        F::redirect('/?r=admin/Articles/editor&id='.$article->getId());
    }

    public function delete()
    {
        $id = empty($_POST['id']) ? F::error('Article ID reuired') : F::checkstr($_POST['id']);
        $article = new Article($id);
        $article->delete();
        F::redirect('/?r=admin/Articles/index');
    }
}
