<?php

namespace blog\app\controllers\admin;

use cinema\app\controllers\CinemaAuthorizationController;
use engine\app\models\Engine;
use engine\app\models\F;

class FroalaController
{
    private $fileRoute = null;

    public function __construct()
    {
        $auth = new CinemaAuthorizationController(['admin-lite', 'admin']);
        $articleID = empty($_GET['articleId']) ? F::error('Article ID required') : F::checkstr($_GET['articleId']);
        // File Route.
        $this->fileRoute = '/static/types/blog/articles/'.$articleID.'/images/';
        Engine::setDebug(false);
    }

    public function uploadImage()
    {
        try {

            // File Route.
            $fileRoute = $this->fileRoute;

            $fieldname = 'file';

            if (empty($_FILES[$fieldname])) {
                F::error('Upload file required');
            }

            // Get filename.
            $filename = explode('.', $_FILES[$fieldname]['name']);

            // Validate uploaded files.
            // Do not use $_FILES["file"]["type"] as it can be easily forged.
            $finfo = finfo_open(FILEINFO_MIME_TYPE);

            // Get temp file name.
            $tmpName = $_FILES[$fieldname]['tmp_name'];

            // Get mime type.
            $mimeType = finfo_file($finfo, $tmpName);

            // Get extension. You must include fileinfo PHP extension.
            $extension = end($filename);

            // Allowed extensions.
            $allowedExts = ['gif', 'jpeg', 'jpg', 'png', 'svg', 'blob'];

            // Allowed mime types.
            $allowedMimeTypes = ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/x-png', 'image/png', 'image/svg+xml'];

            // Validate image.
            if (! in_array(strtolower($mimeType), $allowedMimeTypes) || ! in_array(strtolower($extension), $allowedExts)) {
                throw new \Exception('File does not meet the validation.');
            }

            // Generate new random name.
            $name = sha1(microtime()).'.'.$extension;
            $fullNamePath = Engine::getRootPath().$fileRoute.$name;

            // Save file in the uploads folder.
            move_uploaded_file($tmpName, $fullNamePath);

            // Generate response.
            $response = new \StdClass;
            $response->link = $fileRoute.$name;

            // Send response.
            return stripslashes(json_encode($response));

        } catch (\Exception $e) {
            // Send error response.
            http_response_code(404);

            return $e->getMessage();
        }
    }

    public function deleteImage()
    {
        try {

            // File Route.
            $fileRoute = $this->fileRoute;

            $fieldname = 'src';

            // Get filename.
            $filename = empty($_POST[$fieldname]) ? F::error('No file') : basename($_POST[$fieldname]);

            $fullNamePath = Engine::getRootPath().$fileRoute.$filename;

            // Save file in the uploads folder.
            unlink($fullNamePath);

            // Send response.
            return stripslashes(json_encode(true));

        } catch (\Exception $e) {
            // Send error response.
            return $e->getMessage();
            http_response_code(404);
        }
    }
}
