<?php

namespace blog\app\controllers;

use blog\app\models\Article;
use blog\app\models\Articles;
use blog\app\models\Domain;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class MainController
{
    private $domain = null;

    private $template = null;

    public function __construct()
    {
        // parent::__construct();
        $this->domain = new Domain(Engine::getDomain());
        $this->template = Engine::template();
        F::setPageVars($this->template);
        // $this->setMenuVars($this->template);
    }

    public function index($args = [])
    {
        $page = empty($args['page']) ? 1 : (int) $args['page'];
        $t = $this->template;
        $a = new Articles;
        $a->setDomain($this->domain->getDomain());
        $a->setActive(true);
        $a->setLimit($this->domain->getArticlesLimit());
        $a->setPage($page);
        $a->setOrder('articles.apply_date desc');
        $a->setListTemplate('listArticle');
        // dump($a->getList());
        $t->v('listArticles', $a->getList());
        $a->setPaginationUrlPattern($this->domain->getArticlesPaginationUrlPattern());
        $t->v('listPages', $a->getPages());

        return $t->get();
    }

    public function view($args = [])
    {
        $id = empty($args['id']) ? F::error('Article ID required') : F::checkstr($args['id']);
        $t = $this->template;
        $a = new Article($id);
        if ($a->getDomain() != $this->domain->getDomain()) {
            F::error('Article not found');
        }
        $t->v('articleText', $a->getText());
        $t->v('articleTextPreview', $a->getTextPreview());
        $t->v('articleTitle', $a->getTitle());
        // похожие статьи
        $articles = new Articles;
        $articles->setDomain($this->domain->getDomain());
        $articles->setLimit(4);
        $articles->setOrder('rand()');
        $articles->setListTemplate('list_same_article');
        $articles->setActive(true);
        $articles->setExcludeArticle($a->getId());
        $t->v('listSameArticles', $articles->getList());

        return $t->get();
    }

    // для обычной текстовой страницы
    public function text()
    {
        return $this->template->get();
    }
}
