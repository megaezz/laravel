<?php

namespace bankrot\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class MainController
{
    public function index()
    {
        Engine::setDebug(false);
        $t = new Template('index');

        return $t->get();
    }

    public function agreement()
    {
        $t = new Template('agreement');

        return $t->get();
    }

    public function refund()
    {
        $t = new Template('refund');

        return $t->get();
    }

    public function confirmEmail()
    {
        F::alertLite('Спасибо, адрес подтвержден');
    }

    public function unsubscribe()
    {
        F::alertLite('Отписка оформлена');
    }

    public function calculator()
    {
        Engine::setDebug(false);
        $t = new Template('calculator');

        return $t->get();
    }
}
