<?php

namespace bankrot\app\controllers;

use bankrot\app\models\Bankrot;
use engine\app\models\Engine;
use engine\app\models\Template;

class ApiController
{
    private $input = null;

    public function __construct()
    {
        Engine::setDebug(false);
        $this->input = json_decode(file_get_contents('php://input'));
        header('Content-Type: application/json; charset=utf-8');
    }

    public function route($args = null)
    {
        if (! isset($args['method'])) {
            $this->error('Method not specified');
        }
        if (! method_exists($this, $args['method'])) {
            $this->error('Method doesn\'t exist');
        }

        return json_encode(call_user_func([$this, $args['method']]), JSON_UNESCAPED_UNICODE);
    }

    public function error($text = null, $code = null)
    {
        $args['code'] = $code;
        $args['text'] = $text;
        if (! isset($args['code'])) {
            $args['code'] = 400;
        }
        if (! isset($args['text'])) {
            $args['text'] = 'Error text not specified';
        }
        http_response_code($args['code']);
        $result = new \stdClass;
        $result->message = $args['text'];
        logger()->info($text);
        exit(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    public function submitStep1()
    {
        $post = new \stdClass;
        $post->email = $this->input->email ?? $this->error('Не передан email');
        $post->firstName = $this->input->firstName ?? $this->error('Не передано имя');
        $post->lastName = $this->input->lastName ?? $this->error('Не передана фамилия');
        $post->phone = $this->input->phone ?? $this->error('Не передан телефон');
        $post->reason = $this->input->reason ?? $this->error('Не передана причина');
        $post->otherReason = $this->input->otherReason ?? null;
        $post->debt = $this->input->debt ?? $this->error('Не передана сумма задолженности');
        $tEmail = new Template('emails/join');
        $tEmail->v('firstName', $post->firstName);
        $tEmail->v('emailCode', 'emailCode');
        Bankrot::sendMail($post->email, 'Банкротство даром', 'Подтвердите адрес почты', $tEmail->get());

        return $post;
    }
}
