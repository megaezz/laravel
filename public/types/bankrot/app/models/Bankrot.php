<?php

namespace bankrot\app\models;

use engine\app\models\F;

class Bankrot
{
    public static function sendMail($email = null, $name = null, $subject = null, $body = null, $type = null)
    {
        $smtp_config = 6;
        $unsubscribe_link = 'https://bankrotstvo-darom.ru/?r=Main/unsubscribe&email='.urlencode($email);

        return F::sendMail($email, $name, $subject, $body, $unsubscribe_link, $smtp_config);
    }
}
