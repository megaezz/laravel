<?php

namespace MegawebV1;

function mysql_real_escape_string($text = '')
{
    global $config;

    return mysqli_real_escape_string($config['mysql_connection'], $text);
}

function mysql_fetch_assoc($data)
{
    return mysqli_fetch_assoc($data);
}

function mysql_fetch_array($data)
{
    // pre($const);
    // if ($const===MYSQL_ASSOC) {$const=MYSQLI_ASSOC;};
    return mysqli_fetch_array($data);
}

function mysql_affected_rows()
{
    global $config;

    return mysqli_affected_rows($config['mysql_connection']);
}

function mysql_num_rows($data)
{
    return mysqli_num_rows($data);
}

function mysql_query($query)
{
    global $config;

    return mysqli_query($config['mysql_connection'], $query);
}

function mysql_error()
{
    global $config;

    return mysqli_error($config['mysql_connection']);
}

function mysql_connect($host, $user, $pass, $port)
{
    return mysqli_connect($host, $user, $pass, null, $port);
}

function mysql_set_charset($charset)
{
    global $config;

    return mysqli_set_charset($config['mysql_connection'], $charset);
}

function mysql_insert_id()
{
    global $config;

    return mysqli_insert_id($config['mysql_connection']);
}

function mysql_fetch_row($data)
{
    global $config;

    return mysqli_fetch_row($data);
}
