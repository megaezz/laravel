<?php

namespace MegawebV1;

use Illuminate\Support\Facades\DB;

// include($_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/class/func.php');
// include($_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/class/writer.php');
include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/mysql.php';
include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/admin.php';
include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/editor.php';
include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/recaptcha/func.php';
include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/rfc822/func.php';
include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/geoip/func.php';
include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/mobiledetect.net/Mobile_Detect.php';
include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/num2str.php';

// ������������ ���� from � ���� to � ������� ext
// ���� ������ to, �� ������� ���� �� �������, ���� ���, �� ���������� ����������
function convertio($args)
{
    $args['from'] = empty($args['from']) ? error('�� ������� ���� � ����� - ���������') : check_input($args['from']);
    // $args['to']=empty($args['to'])?error('�� ������� ���� � ����� - ����'):check_input($args['to']);
    $args['to'] = empty($args['to']) ? false : check_input($args['to']);
    $args['ext'] = empty($args['ext']) ? error('�� �������� ���������� ����� - ����') : check_input($args['ext']);
    $API = new Convertio('aff2d7e1211f885b26eceaa14b5cabb0');
    try {
        if ($args['to']) {
            $API->start($args['from'], $args['ext'])->wait()->download($args['to'])->delete();

            return true;
        } else {
            $return = $API->start($args['from'], $args['ext'])->wait()->fetchResultContent()->result_content;
            $API->delete();

            return $text;
        }
    } catch (APIException $e) {
        error('API Exception: '.$e->getMessage().' [Code: '.$e->getCode().']');
    } catch (Exception $e) {
        error('Miscellaneous Exception occurred: '.$e->getMessage());
    }
}

// ���������� �������� ������ �� ������� �� ��� ������
function rus_month($num)
{
    $arr = [
        1 => '������',
        2 => '�������',
        3 => '����',
        4 => '������',
        5 => '���',
        6 => '����',
        7 => '����',
        8 => '������',
        9 => '��������',
        10 => '�������',
        11 => '������',
        12 => '�������',
    ];

    return isset($arr[$num]) ? $arr[$num] : false;
}

// ������ post ������ �� ����� url � ����������� � ������� postfields
function send_post_request($args = [])
{
    $postfields = empty($args['postfields']) ? error('�� �������� ��������� �������') : $args['postfields'];
    $url = empty($args['url']) ? error('�� �������� ������') : $args['url'];
    $myCurl = curl_init();
    curl_setopt_array($myCurl, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => http_build_query($postfields),
    ]);
    $response = curl_exec($myCurl);
    curl_close($myCurl);

    return $response;
}

// ������ ������ �����������, ���� ��� ��������� maxDim �� ����� ��� ������, ������������ ��� � png � ���������� base64
function resize_image_and_convert_to_base64($imgsrc, $maxDim = 800)
{
    [$width, $height, $type, $attr] = getimagesize($imgsrc);
    $fn = $imgsrc;
    ini_set('memory_limit', '128M');
    $src = imagecreatefromstring(file_get_contents($fn));
    // ini_set('memory_limit', '64M');
    if ($width > $maxDim || $height > $maxDim) {
        // $target_filename = $imgsrc;
        $size = getimagesize($fn);
        $ratio = $size[0] / $size[1]; // width/height
        if ($ratio > 1) {
            $width = $maxDim;
            $height = $maxDim / $ratio;
        } else {
            $width = $maxDim * $ratio;
            $height = $maxDim;
        }
        $dst = imagecreatetruecolor($width, $height);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
    }
    if (empty($dst)) {
        $return = imagesource_to_base64($src);
        $is_handled = '�� ����������';
    } else {
        $return = imagesource_to_base64($dst);
        imagedestroy($dst);
        $is_handled = '����������';
    }
    imagedestroy($src);

    return $return;
}

// ������ ������ �����������, ���� ��� ��������� maxDim �� ����� ��� ������, � ��������� ��� � png
function resize_image($imgsrc, $imgsrc_dest, $maxDim = 800)
{
    [$width, $height, $type, $attr] = getimagesize($imgsrc);
    $fn = $imgsrc;
    ini_set('memory_limit', '128M');
    $src = imagecreatefromstring(file_get_contents($fn));
    // ini_set('memory_limit', '64M');
    if ($width > $maxDim || $height > $maxDim) {
        // $target_filename = $imgsrc;
        $size = getimagesize($fn);
        $ratio = $size[0] / $size[1]; // width/height
        if ($ratio > 1) {
            $width = $maxDim;
            $height = $maxDim / $ratio;
        } else {
            $width = $maxDim * $ratio;
            $height = $maxDim;
        }
        $dst = imagecreatetruecolor($width, $height);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
    }
    if (empty($dst)) {
        imagepng($src, $imgsrc_dest);
        $is_handled = '�� ���������� (������ �� ��������)';
    } else {
        imagepng($dst, $imgsrc_dest);
        imagedestroy($dst);
        $is_handled = '����������';
    }
    // die($return);
    imagedestroy($src);

    return true;
}

// ��������� ������ ����������� ������������ � png � ���������� ��� � ���� ������ base64
function imagesource_to_base64($imagesource)
{
    // ob_start(function($c) {return "data:image/png;base64,".base64_encode($c);});
    ob_start();
    imagepng($imagesource);
    $content = ob_get_contents();
    ob_end_clean();

    return 'data:image/png;base64,'.base64_encode($content);
    // pre($content);
}

// ���������� ������� �������� ����
function typetab($table)
{
    global $config;
    $type = empty($config['type']) ? error('��� �� ���������') : $config['type'];

    return tabname($type, $table);
}

// ���������� ������ �� �� � ����� � ���������� � ��� ��� �������� ���������
function find_array_changes($args)
{
    $arr['old'] = (isset($args['old']) and is_array($args['old'])) ? $args['old'] : error('�� ������� ������ ������ ��� �� �� �������� ��������');
    $arr['new'] = (isset($args['new']) and is_array($args['new'])) ? $args['new'] : error('�� ������� ����� ������ ��� �� �� �������� ��������');
    $arr['changed'] = [];
    foreach ($arr['old'] as $k => $v) {
        if ($arr['new'][$k] != $v) {
            $arr['changed'][$k] = $arr['new'][$k];
        }
    }

    return $arr['changed'];
}

function log_changed_fields($args)
{
    global $config;
    if (empty($args['primary_key'])) {
        error('�� �������� �������� ���������� ����� �������');
    }
    if (empty($args['table'])) {
        error('�� �������� �������');
    }
    if (empty($args['change_log'])) {
        $args['change_log'] = $args['table'].'_changes';
        // error('�� �������� ������� ����');
    }
    if (empty($args['old'])) {
        error('�� ������� ������ ������ ��������');
    }
    if (! isset($args['login'])) {
        $type = empty($config['type']) ? error('��� �� ���������') : $config['type'];
        $args['login'] = auth_login(['type' => $type]);
        // error('�� ������� �����');
    }
    $info = query_assoc('show keys from '.$args['table'].' where key_name=\'primary\';');
    $new = query_assoc('select * from '.$args['table'].' where '.$info['Column_name'].'=\''.$args['primary_key'].'\';');
    $arr = find_array_changes(['old' => $args['old'], 'new' => $new]);
    foreach ($arr as $k => $v) {
        query('insert into '.$args['change_log'].' set
			primary_key=\''.mysql_real_escape_string($args['primary_key']).'\',
			tabname=\''.mysql_real_escape_string($args['table']).'\',field=\''.mysql_real_escape_string($k).'\',
			value=\''.mysql_real_escape_string($v).'\',login=\''.mysql_real_escape_string($args['login']).'\'
			;');
    }
}

// ���������� ������� �������. ������������ ���������, ����� ��������� �������
function remove_directory($path)
{
    global $config;
    if (empty($path)) {
        error('�������� ������ ����������');
    }
    if (strlen($path) < 40) {
        error('������� �������� ����');
    }
    $arr = list_directory($path);
    // pre($arr);
    foreach ($arr as $v) {
        if ($v['type'] == 'file') {
            unlink($path.'/'.$v['name']);
        }
        if ($v['type'] == 'dir') {
            remove_directory($path.'/'.$v['name']);
        }
    }
    rmdir($path);

}

// http://habrahabr.ru/post/140352/
// �������� ������ �� ����� xlsx, ��������� ��� ���������� ���������� temp_dir, � ������� ��������� ����
function get_array_from_xlsx($args)
{
    global $config;
    if (empty($args['file'])) {
        error('�� ������� ���� xlsx');
    }
    if (empty($args['temp_dir'])) {
        error('�� ������� ��������� �������');
    }
    $pathinfo = pathinfo($args['file']);
    $unzip_dir = $args['temp_dir'].$pathinfo['filename'];
    if (! mkdir($unzip_dir)) {
        error('���������� ������� ����������');
    }
    $zip = new ZipArchive;
    $res = $zip->open($args['file']);
    if ($res === false) {
        error('���������� ����������� ����');
    }
    $zip->extractTo($unzip_dir);
    $zip->close();
    $xls['sharedStrings'] = $unzip_dir.'/xl/sharedStrings.xml';
    $xls['worksheets'] = $unzip_dir.'/xl/worksheets';
    $xml = simplexml_load_file($xls['sharedStrings']);
    $sharedStringsArr = [];
    foreach ($xml->children() as $item) {
        $sharedStringsArr[] = (string) $item->t;
    }
    $handle = opendir($xls['worksheets']);
    $out = [];
    while ($file = readdir($handle)) {
        // �������� �� ���� ������ �� ���������� /xl/worksheets/
        if ($file != '.' && $file != '..' && $file != '_rels') {
            $xml = simplexml_load_file($xls['worksheets'].'/'.$file);
            // �� ������ ������
            $row = 0;
            foreach ($xml->sheetData->row as $item) {
                $out[$file][$row] = [];
                // �� ������ ������ ������
                $cell = 0;
                foreach ($item as $child) {
                    $attr = $child->attributes();
                    $value = isset($child->v) ? (string) $child->v : false;
                    $out[$file][$row][$cell] = isset($attr['t']) ? $sharedStringsArr[$value] : $value;
                    $cell++;
                }
                $row++;
            }
        }
    }
    remove_directory($unzip_dir);

    // pre($out);
    return $out;
}

// ��������� ��������� ������
function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 0) {
        return $min;
    } // not so random...
    $log = log($range, 2);
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);

    return $min + $rnd;
}

function getToken($length = 32)
{
    $token = '';
    $codeAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $codeAlphabet .= 'abcdefghijklmnopqrstuvwxyz';
    $codeAlphabet .= '0123456789';
    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, strlen($codeAlphabet))];
    }

    return $token;
}
//

function get_or_post($var)
{
    if (isset($_POST[$var])) {
        return check_input($_POST[$var]);
    }
    if (isset($_GET[$var])) {
        return check_input($_GET[$var]);
    }
}

function get_or_post_urlencode($var)
{
    return urlencode(get_or_post($var));
}

function getpost($var)
{
    if (isset($_POST[$var])) {
        return check_input($_POST[$var]);
    }
}

function getvar($var)
{
    if (isset($_GET[$var])) {
        return check_input($_GET[$var]);
    }
}

// ���������� ������ � enum ���������� ��� ���� field
// enum_to_array(tabname('shop','orders'),'status');
function enum_to_array($tabname, $field, $translation = false)
{
    $arr = query_assoc('show columns from '.$tabname.' where field=\''.$field.'\';');
    $enum_str = str_replace(['enum(', ')', '\''], '', $arr['Type']);
    $enum = explode(',', $enum_str);
    // asort($enum);
    if ($translation) {
        foreach ($enum as $v) {
            if (isset($translation[$v])) {
                $translated = $translation[$v];
            } else {
                $translated = $v.' (�� ����������)';
            }
            $enum_translated[$v] = $translated;
        }
        $return = $enum_translated;
    } else {
        $return = $enum;
    }

    return $return;
}

// �������� � ������� ������ �������� ��������� null ��� mysql �������
function null_if_empty($arr = [])
{
    foreach ($arr as $k => $v) {
        if ($v === '' or is_null($v)) {
            $arr[$k] = 'null';
        } else {
            $arr[$k] = '\''.$v.'\'';
        }
    }

    return $arr;
}

function query_arr($query)
{
    return readdata(query($query), 'nokey');
}

// ���������� ������ �� 10 ���
function cache_query_arr($query = '')
{
    return cache('preparing_query_arr', ['query' => $query], 100);
}

function preparing_query_arr($args = [])
{
    $args['query'] = empty($args['query']) ? error('�� ������� ������') : $args['query'];

    // pre($args);
    return query_arr($args['query']);
}

function query_assoc($query)
{
    return mysql_fetch_assoc(query($query));
}

function without_spaces($str)
{
    return str_replace(' ', '_', $str);
}

function with_spaces($str)
{
    return str_replace('_', ' ', $str);
}

function auto_encode($text = '', $encoding = 'windows-1251')
{
    return mb_convert_encoding($text, $encoding, mb_detect_encoding($text));
}

// ������ ��������� ������ value �� ��������� � engine.config.property engine_encoding �� ��������� ��� ������ � engine.domains charset
// ���� ������ ������ ��������, �� ������ �� ���� ���������.
function fix_encode($value, $encoding = false, $type = 'iconv')
{
    global $db,$config;
    if ($encoding) {
        $source_encoding = $encoding;
    } else {
        $source_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];
    }
    if ($source_encoding != $config['charset']) {
        if ($type == 'iconv') {
            $value = iconv($source_encoding, $config['charset'], $value);
        }
        if ($type == 'mb') {
            $value = mb_convert_encoding($value, $config['charset'], $source_encoding);
        }
    }

    return $value;
}

// ������� ������������� ����������
function check_input($value, $encoding = false)
{
    global $config,$db;
    // pre($encoding);
    $engine_encoding = $db[tabname('engine', 'config')]['property']['engine_encoding']['value'];
    // ���� magic_quotes_gpc �������� - ���������� stripslashes
    // if (get_magic_quotes_gpc()) {$value=stripslashes($value);};
    // ���� ���������� - �����, �� ������������ � �� �����
    // ���� ��� - �� ������� � ���������, � ����������
    // if (!is_numeric($value)) {$value="'".mysql_real_escape_string($value)."'";};
    // $value=str_replace(array('{','}'),'',$value);
    $value = braces2square($value);
    $value = mysql_real_escape_string($value);
    // or error('������������� �� ��������');
    $text_encoding = $encoding ? $encoding : $config['charset'];
    if ($encoding === 'auto') {
        // pre('test');
        $value = mb_convert_encoding($value, $engine_encoding, ['windows-1251', 'utf-8']);
    } else {
        if ($text_encoding != $engine_encoding) {
            $value = iconv($text_encoding, $engine_encoding, $value);
        }
    }

    return $value;
}

function reload()
{
    global $config;

    return '?'.value_from_config('engine', 'reload', $config['domain']);
}

// �������� �������� ����� sec ������
function js_reload_page($sec = 10)
{
    return '<script type="text/javascript">setTimeout("window.location.reload()",'.($sec * 1000).');</script>';
}

function check_site_availability($url)
{
    $headers = filegetcontents($url, 'no_error', 'default_ua', 'default_interface', 'default_proxy', 'return_getinfo');
    if ($headers['connect_time'] > 3 or $headers['http_code'] != '200') {
        return false;
    }

    return true;
}

function set_config($type, $property, $value)
{
    query('insert into '.tabname($type, 'config').' set property=\''.$property.'\',value=\''.$value.'\'
		on duplicate key update value=\''.$value.'\';');
}

function execute_function()
{
    if (empty($_GET['function'])) {
        error('�� �������� �������');
    }
    $function = $_GET['function'];
    if (! function_exists($function)) {
        error('������� '.$function.' �� ����������');
    }
    alert($function());
}

function update_local_ids($args)
{
    if (empty($args['table'])) {
        error('�� �������� �������');
    }
    if (empty($args['where_clause'])) {
        error('�� �������� �������');
    }
    if (empty($args['max_updates'])) {
        $args['max_updates'] = 0;
    }
    // �������� ������������ local_id
    $data = query('select ifnull(local_id,0) as local_id from '.$args['table'].'
		where '.$args['where_clause'].'
		order by local_id desc limit 1;');
    $arr = mysql_fetch_assoc($data);
    $max_local_id = $arr['local_id'];
    $changed = 0;
    do {
        $max_local_id++;
        $data = query('update '.$args['table'].' set local_id=\''.$max_local_id.'\'
		where '.$args['where_clause'].' and local_id is null
		order by id asc
		limit 1;');
        $affected = mysql_affected_rows();
        if ($affected > 0) {
            $changed++;
        }
        if ($args['max_updates'] != 0 and $changed >= $args['max_updates']) {
            break;
        }
    } while ($affected > 0);
    logger()->info('��������� local_id ��� ������� "'.$args['table'].'" � ������� "'.$args['where_clause'].'". ��������� �����: '.$changed.'.');

    return $changed;
}

// ���������� ���� ������ ���� �������
function referer_url()
{
    $url = '';
    if (! empty($_SERVER['HTTP_REFERER'])) {
        $url = $_SERVER['HTTP_REFERER'];
    }

    return $url;
}

function li_from_array($arr)
{
    if (! isset($arr) and ! is_array($arr)) {
        error('������ �� �������');
    }
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li>'.$v.'</li>';
    }

    return $list;
}

function is_section_active($section_pages, $section)
{
    global $config;
    $active = 0;
    if (empty($section_pages)) {
        error('�� ������� ������ ��������');
    }
    if (empty($section)) {
        error('�� ������� ������� ������');
    }
    if (isset($section_pages[$section]) == 0) {
        error('������ �� ������������', '������ '.$section.' �� ������������');
    }
    foreach ($section_pages[$section] as $page) {
        if ($page == $config['vars']['page']) {
            $active = 1;
        }
    }
    // die('sec: '.$section.' page: '.$config['vars']['page'].' active: '.$active);
    if ($active == 0) {
        return false;
    }
    if ($active == 1) {
        return true;
    }
}

// ��������� ��� ������� � �������� ���� � �������� � � ����� ����� ���������� ����������,
// �� ����� ������ ��� ������������
function what_sites_using_me($name)
{
    if (empty($name)) {
        error('�� ������� �������������� ��������');
    }
    logger()->info('wsum: '.$name);

}

function check_words($text)
{
    global $db;
    readdb(tabname('laravel', 'words_filter'), 'word');
    $return = $text;
    foreach ($db[tabname('laravel', 'words_filter')]['word'] as $v) {
        if (strstr($text, ' '.$v['word'])) {
            $return = str_replace($v['word'], $v['change'], $return);
        }
    }

    return $return;
}

// ���������� ok, spam ��� check
function spam_check($text)
{
    // ���� check ����� ��������� ������ ���, �.�. ��� hidden, ���� �����������
    if (! empty($_POST['check'])) {
        return 'spam';
    }
    if (preg_match('/^(.*)http(.*)$/', $text)) {
        return 'check';
    }
    if (strlen($text) > 80 or strlen($text) == 0) {
        return 'check';
    }
    if (preg_match('/^([\w-().,!?&:;\s\']+)$/', $text)) {
        return 'check';
    }

    return 'ok';
}

function delete_file_get()
{
    if (empty($_GET['file'])) {
        error('�� ������� ����');
    }
    if (empty($_GET['confirm'])) {
        alert('������� '.$_GET['file'].' ?<br/><a href="/?page=admin/delete_file&amp;file='.$_GET['file'].'&confirm=yes">OK</a>');
    }
    if ($_GET['confirm'] == 'yes') {
        $status = unlink($_GET['file']);
        if ($status) {
            alert('������ ����: '.$_GET['file']);
        } else {
            alert('�� ������� �������: '.$_GET['file']);
        }
    }
}

function config_page()
{
    global $config;
    if (empty($config['vars']['page'])) {
        error('���������� ����������');
    }

    return $config['vars']['page'];
}

function check_ext($args)
{
    if (! is_array($args)) {
        error('������� ������ ������ ���� ��������');
    }
    if (empty($args['whitelist'])) {
        error('�� ������� ������ ������ ������');
    }
    if (empty($args['filename'])) {
        error('�� �������� �������� �����');
    }
    $whitelist = ['jpg', 'jpeg', 'png'];
    foreach ($args['whitelist'] as $ext) {
        if (preg_match('/.'.$ext.'$/i', $args['filename'])) {
            return $ext;
        }
    }

    return false;
}

// ���� ���������� var1 ����� var2 - �������� true_text, ����� - false_text
function equal_text($var1, $var2, $true_text, $false_text)
{
    if ($var1 == $var2) {
        return $true_text;
    } else {
        return $false_text;
    }
}

// ���������� ������ option �� ������� array, ��� ��������� value ������� ���� value, � ������� - ���� text, �������� �������� selected_value
// ���� ������� ������ disabled_value �� ��� �������� ����� ���������
// ���� null_disabled=yes �� null ����� ���������
function get_select_option_list($arr)
{
    if (empty($arr['array'])) {
        error('�� ������� ������ ��������');
    }
    if (empty($arr['value'])) {
        error('�� �������� �������� ���� ��� value');
    }
    if (empty($arr['text'])) {
        error('�� �������� �������� ���� ��� ������');
    }
    $null_selected = '';
    if (! isset($arr['selected_value'])) {
        $arr['selected_value'] = 'null';
        $null_selected = ' selected="selected"';
    }
    $arr['null_disabled'] = isset($arr['null_disabled']) ? $arr['null_disabled'] : false;
    if ($arr['null_disabled']) {
        $null_disabled = 'disabled';
    } else {
        $null_disabled = '';
    }
    $arr['null_value'] = isset($arr['null_value']) ? $arr['null_value'] : 'null';
    $arr['show_null'] = isset($arr['show_null']) ? $arr['show_null'] : true;
    $list = '';
    if ($arr['show_null']) {
        $list = '<option'.$null_selected.' '.$null_disabled.' value="'.$arr['null_value'].'">�� �������</option>';
    }
    foreach ($arr['array'] as $k => $v) {
        if (is_array($v)) {
            if (empty($v[$arr['text']])) {
                $v[$arr['text']] = $arr['value'].': '.$v[$arr['value']].' ����������� �����';
            }
            if ($v[$arr['value']] == $arr['selected_value']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $disabled = '';
            if (isset($arr['disabled_values'])) {
                if (in_array($v[$arr['value']], $arr['disabled_values'])) {
                    $disabled = 'disabled';
                }
            }
            $list .= '<option '.$selected.' '.$disabled.' value="'.$v[$arr['value']].'">'.$v[$arr['text']].'</option>';
        } else {
            $var['key'] = $k;
            $var['value'] = $v;
            if ($var[$arr['value']] == $arr['selected_value']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $disabled = '';
            if (isset($arr['disabled_values'])) {
                if (in_array($var[$arr['value']], $arr['disabled_values'])) {
                    $disabled = 'disabled';
                }
            }
            $list .= '<option '.$selected.' '.$disabled.' value="'.$var[$arr['value']].'">'.$var[$arr['text']].'</option>';
        }
    }

    return $list;
}

function get_real_ip()
{
    request()->ip();
}

// ������� ��������� ��� �� ���� ������� ������� ����
function reset_cache_all_domains($func, $args, $type)
{
    $domains = query_arr('select domain from '.tabname('engine', 'domains').' where type=\''.$type.'\';');
    $sum_rows = 0;
    foreach ($domains as $domain) {
        $rows = reset_cache($func, $args, $domain['domain']);
        $sum_rows = $sum_rows + $rows;
    }

    return $sum_rows;
}

// ����� ���������� ������ ���� ��������������, �������� ��� ����
function reset_cache($func, $args = [], $domain = false)
{
    global $config;
    if (! $domain) {
        $domain = $config['domain'];
        $current_domain = true;
    }
    if (! isset($func,$args)) {
        error('�� �������� ����������� ���������');
    }
    if ($func == 'all') {
        if ($args == 'all') {
            // ����� ���
            $cache = new cache;
            $cache->setDomain($domain);
            $cache->resetDomain();
            //
            query('update '.tabname('engine', 'cache').' set reset=\'1\' where domain=\''.$domain.'\';');
            $rows = mysql_affected_rows();
            $params = '(����� ������� � ���������)';
        } else {
            // ��� ������ ���� ����� �� �������������
            ksort($args);
            $json_args = serialize($args);
            if (strlen($json_args) > 400) {
                error('������� ������� ������', '������� ������� ������ �������');
            }
            query('update '.tabname('engine', 'cache').' set reset=\'1\' where arguments=\''.mysql_real_escape_string($json_args).'\' and domain=\''.$domain.'\';');
            if (! $domain) {
                unset($config['cache'][$func][$json_args]);
            }
            $rows = mysql_affected_rows();
            $params = '(����� ������� � �����������: '.$json_args.')';
        }
    } else {
        if ($args == 'all') {
            // ��� ������ ���� ����� �� �������������
            query('update '.tabname('engine', 'cache').' set reset=\'1\' where function=\''.$func.'\' and domain=\''.$domain.'\';');
            $rows = mysql_affected_rows();
            $params = $func.' (����� ���������)';
        } else {
            // ����� ���
            $cache = new cache;
            $cache->setDomain($domain);
            $cache->setFunction($func);
            $cache->setArgs($args);
            $cache->reset();
            //
            ksort($args);
            $json_args = serialize($args);
            if (strlen($json_args) > 400) {
                error('������� ������� ������', '������� ������� ������ �������');
            }
            query('update '.tabname('engine', 'cache').' set reset=\'1\' where function=\''.$func.'\' and arguments=\''.mysql_real_escape_string($json_args).'\' and domain=\''.$domain.'\';');
            if (! $domain) {
                unset($config['cache'][$func][$json_args]);
            }
            $rows = mysql_affected_rows();
            $params = $func.' '.$json_args;
        }
    }
    logger()->info('����� ���� ��� '.$params.'. �����: '.$domain.' ��������� �����: '.$rows.'.');

    return $rows;
}

function cacheObject($object = null, $method = '', $args = [], $seconds = null)
{
    return \engine\app\models\F::cache($object, $method, $args, $seconds);
}

function cache_alt($function, $args = [], $seconds = 86400)
{
    $function = '\MegawebV1\\'.$function;

    return \engine\app\models\F::cache(null, $function, $args, $seconds);
}

function cache($function, $args = [], $seconds = 86400)
{
    $func = '\MegawebV1\\'.random_event(['cache_alt' => 100, 'cache_old' => 0]);

    return $func($function, $args, $seconds);
}

// ���� 0 ���, �� �� �������� � �� (without_db)
function cache_old($func, $args = [], $seconds = 86400)
{
    global $config;
    $action = 'select';
    if (! isset($func,$args,$seconds)) {
        error('�� �������� ����������� ���������');
    }
    ksort($args);
    // array_walk_recursive($args,'array_walk_convert_cp1251_to_utf8');
    $json_args = serialize($args);
    if (strlen($json_args) > 400) {
        error('������� ������� ������', '������� ������� ������ �������');
    }
    if ($seconds == 0) {
        $action = 'without_db';
    }
    if (isset($config['cache'][$func][$json_args]) == 1) {
        $action = 'from_array';
        $arr = $config['cache'][$func][$json_args];
    }
    if ($action != 'from_array' and $action != 'without_db') {
        $data = query('select function,arguments,result,unix_timestamp(date) as date,reset from '.tabname('engine', 'cache').' where function=\''.$func.'\' and arguments=\''.mysql_real_escape_string($json_args).'\' and domain=\''.$config['domain'].'\';');
        $arr = mysql_fetch_assoc($data);
        if (empty($arr['function'])) {
            $action = 'insert';
        }
        if (! empty($arr['function'])) {
            $date = getdate();
            // die('������: '.$date[0].', � ��: '.$arr['date'].', ������: '.$seconds.' ������: '.($date[0]-$arr['date']));
            if ((($date[0] - $arr['date']) >= $seconds) or ($arr['reset'] == '1')) {
                $action = 'update';
            }
        }
    }
    if ($action == 'insert' or $action == 'update' or $action == 'without_db') {
        $before_time = runing_time();
        if (empty($args)) {
            $result = $func();
        }
        if (! empty($args)) {
            $result = $func($args);
        }
        $after_time = runing_time();
        $time_range = $after_time - $before_time;
        // pre($time_range);
        if (is_array($result)) {
            $arr = $result;
        }
        if (! is_array($result)) {
            unset($arr);
            $arr['not_array'] = $result;
        }
        // $json_result=serialize($arr);
        $config['cache'][$func][$json_args] = $arr;
    }
    if ($action == 'insert' or $action == 'update') {
        $json_result = serialize($arr);
    }
    if ($action == 'insert') {
        // ignore �.�. ���������� ������ ��������� �������
        query('insert ignore into '.tabname('engine', 'cache').' set function=\''.$func.'\', arguments=\''.mysql_real_escape_string($json_args).'\', result=\''.mysql_real_escape_string($json_result).'\', date=current_timestamp, domain=\''.$config['domain'].'\', time=\''.$time_range.'\';');
        // logger()->info('���������: function=\''.$func.'\', arguments=\''.mysql_real_escape_string($json_args).'\'');
    }
    if ($action == 'update') {
        query('update '.tabname('engine', 'cache').' set result=\''.mysql_real_escape_string($json_result).'\', date=current_timestamp, reset=\'0\', time=\''.$time_range.'\' where function=\''.$func.'\' and arguments=\''.mysql_real_escape_string($json_args).'\' and domain=\''.$config['domain'].'\';');
    }
    if ($action == 'select') {
        $arr = unserialize($arr['result']);
        $config['cache'][$func][$json_args] = $arr;
    }
    if (isset($arr['not_array'])) {
        $result = $arr['not_array'];
    }
    if (! isset($arr['not_array'])) {
        $result = $arr;
    }

    // echo $func.' '.$action.'<br>';
    // pre(serialize($result));
    // if ($func=='preparing_videos_list') {pre($result);};
    return $result;
}

function url_get($var)
{
    if (isset($_GET[$var]) == 0) {
        error('��������� ����������', '���������� '.$var.' ����������');
    }

    return $_GET[$var];
}

function pre($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    exit();
    // die(var_dump($arr));
}

function pre_return($arr)
{
    ob_start();
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    $return = ob_get_contents();
    ob_end_clean();

    return $return;
}

function submit_mobile_version()
{
    if (isset($_GET['mobile_version']) == 0) {
        error('�� �������� ��������');
    }
    if ($_GET['mobile_version'] == 0) {
        setcookie('mobile_version', 0);
    }
    if ($_GET['mobile_version'] == 1) {
        setcookie('mobile_version', 1);
    }
    header('Location: /');
}

function rnd()
{
    return mt_rand(1, 20);
}

function temp_var($name, $var)
{
    global $config;
    // $var=strtolower($var);
    $config['temp_vars'][$name] = $var;
    // $config[$config['type']]['vars']['current'][$name]=$var;
}

function get_var($var)
{
    global $config;
    // $var=strtolower($var);
    if (! isset($config['temp_vars'][$var])) {
        error('���������� ����������', '���������� '.$var.' ����������');
    }

    return $config['temp_vars'][$var];
}

function clear_temp_vars()
{
    global $config;
    if (isset($config['temp_vars'])) {
        unset($config['temp_vars']);
    }
}

// ���������� ��������� ���������� �� $config; get_var('[\'vars\'][\'id\']');
function get_var2($var)
{
    global $config;
    // $var=strtolower($var);
    eval('if (isset($config'.$var.')) {$content=$config'.$var.';};');
    if (! isset($content)) {
        error('���������� ����������', '���������� $config'.$var.' ����������');
    }

    return $content;
}

function is_mobile()
{
    if (empty($_SERVER['HTTP_USER_AGENT'])) {
        return false;
    }
    $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
    if (strpos($ua, 'iphone')) {
        return true;
    }
    if (strpos($ua, 'ipad')) {
        return true;
    }
    if (strpos($ua, ' ios ')) {
        return true;
    }
    if (strpos($ua, 'android')) {
        return true;
    }
    if (strpos($ua, 'webos')) {
        return true;
    }
    if (strpos($ua, 'opera mini')) {
        return true;
    }
    if (strpos($ua, 'blackberry')) {
        return true;
    }
    if (strpos($ua, 'windows mobile')) {
        return true;
    }
    if (strpos($ua, 'midp-2')) {
        return true;
    }
    if (strpos($ua, 'symbian os')) {
        return true;
    }
    if (strpos($ua, 'symbianos')) {
        return true;
    }
    if (strpos($ua, 'windows ce')) {
        return true;
    }

    return false;
}

function detect_mobile_browser($useragent)
{
    if (empty($useragent)) {
        return false;
    }
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(ad|hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        return true;
    }

    return false;
}

function sendHtmlMail($from, $to, $subject, $body, $attachments = [])
{
    $message = Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom($from)
        ->setTo($to)
        ->setBody($body, 'text/html');

    $transport = Swift_SmtpTransport::newInstance('smtp.mail.ru', 465)
        ->setUsername('kovalev_mn@list.ru')
        ->setPassword('Koval111$');

    $mailer = Swift_Mailer::newInstance($transport);

    return $mailer->send($message);
}

function li_stat($domain)
{
    if (empty($domain)) {
        error('�� ������� �����');
    }
    $li_stat = [];
    $li_stat_page = @file('http://counter.yadro.ru/values?site='.$domain);
    foreach ($li_stat_page as $li_stat_str) {
        $li_stat_arr = explode(' = ', $li_stat_str);
        $li_stat[$li_stat_arr[0]] = str_replace(';', '', $li_stat_arr[1]);
    }

    return $li_stat;
}

function remote_filesize($file)
{
    $cmd = 'wget --spider "'.$file.'" 2>&1';
    exec($cmd, $arr);
    foreach ($arr as $v) {
        if (strstr($v, 'Length')) {
            $size = trim(copybetwen('Length: ', ' ', $v));
            if (empty($v)) {
                error('������ ����������� ������� ���������� ����� ��� '.$file);
            }

            return $size;
        }
    }
    error('������ ��� '.$file.' ����������');
}

function clear_old_cache()
{
    query('update '.tabname('engine', 'config').' set value=\'off\' where property=\'status\';');
    logger()->info('������� �������� ������� ���� �����.');
    $arr = query_assoc('
		select value from '.tabname('engine', 'config').' where property=\'cache_delete_range\'
		');
    $cacheDeleteRange = $arr['value'];
    $data = query('
		delete from '.tabname('engine', 'cache').'
		where (unix_timestamp(current_timestamp)-unix_timestamp(date))>'.$cacheDeleteRange.'
	');
    // die(var_dump());
    $rows = mysql_affected_rows();
    logger()->info('������� �������� ���� ��������. ������� �����: '.$rows);
    logger()->info('������� �������� ������ ���� �����.');
    //
    // $arr = query('truncate table '.tabname('engine','cacheObject_temp'));
    // ��������� �� ��������� ������� ��� �� ���������� ������
    /*
    query('
        insert into '.tabname('engine','cacheObject_temp').'
        select * from '.tabname('engine','cacheObject').' where
        (unix_timestamp(current_timestamp)-unix_timestamp(date))<('.$cacheDeleteRange.')
        ');
    */
    query('
		delete from '.tabname('engine', 'cacheObject').'
		where (unix_timestamp(current_timestamp)-unix_timestamp(date))>'.$cacheDeleteRange.'
	');
    $rows = mysql_affected_rows();
    logger()->info('������� �������� ������ ���� ��������. ������� �����: '.$rows);
    query('update '.tabname('engine', 'config').' set value=\'on\' where property=\'status\';');
}

function check_session_start()
{
    if (session_id() == '') {
        session_start();
    }
}

function header_json()
{
    header('Content-Type: application/json; charset=utf-8');
}

// ���������� $_GET['domain']
function get_domain()
{
    if (isset($_GET['domain']) == 1) {
        return $_GET['domain'];
    }
}

// ���������� $_POST['domain']
function post_domain()
{
    if (isset($_POST['domain']) == 1) {
        return $_POST['domain'];
    }
}

// ���������� ���������� ����� ��������������� �������, ��������� limit
// ������������ ������ � ���������� SQL_CALC_FOUND_ROWS � �������
function rows_without_limit()
{
    $data = query('select found_rows()');
    $arr = mysql_fetch_assoc($data);
    $rows = $arr['found_rows()']; // ����� �������

    return $rows;
}

// ���������� �������� �� ������� config, ���� �� ������� $config[$type]['config'], ���� ������ ��� ����������.
function value_from_config($type, $property, $return_error = true)
{
    global $config;
    if (! isset($config[$type]['config'][$property])) {
        $data = query('select value from '.tabname($type, 'config').' where property=\''.$property.'\'');
        if (mysql_num_rows($data) == 0) {
            if ($return_error) {
                error('�������� �����������', '�������� '.$property.' �����������');
            } else {
                return false;
            }
        }
        $arr = mysql_fetch_assoc($data);
        $config[$type]['config'][$property] = $arr['value'];
    }

    return $config[$type]['config'][$property];
}

// ������� ��������� �������� � ������� ��� ������, ����� � ����� �������
function return_value_for_domain($type, $property, $domain, $return_error = true)
{
    $data = query('select value from '.tabname($type, 'config_for_domain').' where property=\''.$property.'\' and domain=\''.$domain.'\';');
    if (mysql_num_rows($data) == 0) {
        $value = value_from_config($type, $property, $return_error);
    }
    if (mysql_num_rows($data) == 1) {
        $arr = mysql_fetch_assoc($data);
        $value = $arr['value'];
    }

    return $value;
}

// ��������� ��� ������� ��� ������ (����������� ������ config � config_for_domain)
function read_all_configs_for_domain($type, $domain)
{
    global $config;
    if (isset($config[$type]['config_for_domain'])) {
        return;
    }
    $data = query('
		(select property,value,\'config\' as tab from '.tabname($type, 'config').')
		union
		(select property,value,\'config_for_domain\' as tab from '.tabname($type, 'config_for_domain').'
			where domain=\''.$domain.'\');'
    );
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        if ($v['tab'] == 'config_for_domain') {
            $config[$type]['config_for_domain'][$v['property']] = $v['value'];
        }
        if ($v['tab'] == 'config') {
            if (! isset($config[$type]['config_for_domain'][$v['property']])) {
                $config[$type]['config_for_domain'][$v['property']] = $v['value'];
            }
        }
    }
    // pre($config[$type]['config_for_domain']);
}

// ������� ��������� �������� � ������� ��� ������, ����� � ����� �������
function value_from_config_for_domain($type, $property, $domain = '', $return_error = true)
{
    global $config;
    if ($domain == '') {
        $domain = $config['domain'];
    }
    // ���������� ����� ��� �������
    read_all_configs_for_domain($type, $domain);
    if (! isset($config[$type]['config_for_domain'][$property])) {
        // pre($config[$type]);
        $config[$type]['config_for_domain'][$property] = return_value_for_domain($type, $property, $domain, $return_error);
    }

    return $config[$type]['config_for_domain'][$property];
}

// ���������� class="active" ���� ��������� ��������� ��������
function menu_is_active($page)
{
    global $config;
    if (isset($config['vars']['page']) == 0) {
        error('�������� �� ����������', '����������� ���������� $config[vars][page]');
    }
    if ($config['vars']['page'] == $page) {
        $text = 'class="active"';
    } else {
        $text = '';
    }

    return $text;
}

// ���������� ��������� 404
function header_404()
{
    http_response_code(404);
    /* ��������� ��� ������ ���������, �.�. ������� ��������� �� 200, �� ���� ��� ��������� ��-������� */
    global $config;
    $config['http_code'] = 404;
}

// ���������� ��������� text/javascript
function js_header()
{
    global $config;
    // pre($config);
    header('Content-type: text/javascript; charset='.$config['charset']);
}

// ���������� ��������� text/html
function html_header()
{
    global $config;
    // pre($config);
    $default_charset = 'windows-1251';
    header('Content-type: text/html; charset='.$default_charset);
    // ���������������, �.�. ���� utf8 �� ������ ���������.
    /*
    if (isset($config['charset'])) {
        header('Content-Type: text/html; charset='.$config['charset']);
    } else {
        header('Content-type: text/html; charset='.$default_charset);
    };
    */
}

// ���� ������ �� ���������� ����������, ���������� ��������� ��� ����������� ������
function php_display_errors()
{
    restore_error_handler();
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}

// ���������� �������� �������, ������� ������ �� ��������� �������� ���������. $arr=array('�������'=>30%,..);
function random_event($arr)
{
    if (empty($arr) == 1) {
        error('�� �������� ����������� ���������');
    }
    $sum = 0;
    foreach ($arr as $v) {
        $sum = $sum + $v;
    }
    if ($sum != 100) {
        error('�������� ����� ���������');
    }
    $rnd = mt_rand(1, 100);
    $sum = 0;
    foreach ($arr as $k => $v) {
        $min = $sum + 1;
        $max = $min + $v - 1;
        // echo '���� '.$rnd.'>='.$min.' and '.$rnd.'<='.$max.' �� ������� '.$k.'<br/>';
        if ($rnd >= $min and $rnd <= $max) {
            $event = $k;
            break;
        }
        $sum = $sum + $v;
    }
    if (empty($k) == 1) {
        error('������ �������');
    }

    return $k;
}

// �������� ���� �� ��������� ������� (��� ������) ��������� �������
function rnd_from_array($arr)
{
    if (! is_array($arr)) {
        error('������� �� ������');
    }
    $rnd = rand(0, count($arr) - 1);
    $i = 0;
    foreach ($arr as $k => $v) {
        if ($i == $rnd) {
            $rnd = $v;
            break;
        } $i++;
    }

    return $rnd;
}

// ���������� ������� ������, �������� ������ ������� � ����������� �� �������
function show_since($since_time, $before, $after)
{
    // $since_time='2012-02-11T00:00+04:00';
    $since_time_unix = strtotime($since_time);
    $now_time_unix = time();
    print_r(getdate($since_time_unix));
    print_r(getdate($now_time_unix));
    $range = $since_time_unix - $now_time_unix;
    if ($range > 0) {
        $content = '{'.$after.'}';
    } else {
        $content = '{'.$before.'}';
    }

    return $content;
}

function client_ip()
{
    return request()->ip();
}

function server_ip()
{
    return request()->ip();
}

// ���������� PHP ������
function mgw_php_log($errno, $errmsg, $file, $line)
{
    // error('��������� ������','PHP ������: '."\r\n".'�����: '.$errmsg."\r\n".'����: '.$file."\r\n".'������: '.$line);
    logger()->info('PHP ������: '.$errmsg."\r\n".'����: '.$file.' ������: '.$line);
}

// ������� ������. �.�. ������� � ��������� ����� php ����� � ������� ����� �������, ������� ����� ������� � ����������� ������ ������ ������������.
// ������� ������� ftp_refrash, ������� ����� ������������ � ��������� �������� �� ��� � ���������� ����� � ��������� �������. ���������� ������� ������ ������������ � ������ ����� ������ ���� � ������ (������ ������� ���������� �������� type � ������� domain). ������� �����, ���������� ������ inc ��� ��� �����.
// ������� ������� ��� �������� ������ ���� ����.

// ���������, �������� �� ������ url
function mgw_is_url($str)
{
    if (empty($str)) {
        return false;
    }
    $parse = parse_url($str);
    if (empty($parse['scheme'])) {
        return false;
    }
    if ($parse['scheme'] != 'http' and $parse['scheme'] != 'ftp' and $parse['scheme'] != 'https') {
        return false;
    }

    return true;
}

// ���������� ��������� ������ md5
function generate_code($length = 10)
{
    $num = rand(11111, 99999);
    $code = md5($num);
    $code = substr($code, 0, (int) $length);

    return $code;
}

// ���������� ������ ��������������� � ������ base64 � ������������ ���������
function mime_header_encode($str, $data_charset, $send_charset)
{
    if ($data_charset != $send_charset) {
        $str = iconv($data_charset, $send_charset, $str);
    }

    return '=?'.$send_charset.'?B?'.base64_encode($str).'?=';
}

// ���������� email � �������� �������
function send_mail($to, $from_name, $from_email, $subject, $message)
{
    if (isset($to,$subject,$message) == 0) {
        error('�� �������� ����������� ����������');
    }
    $from_encode = mime_header_encode($from_name, 'CP1251', 'KOI8-R').'<'.$from_email.'>';
    $headers = 'From: '.$from_encode."\r\n".'Content-type: text/html; charset=Windows-1251'."\r\n";
    $subject_encode = mime_header_encode($subject, 'CP1251', 'KOI8-R');
    mail($to, $subject_encode, $message, $headers) or error('������ ��� �������� ������');
    // ������������
    logger()->info('���������� ��������� �� ����� "'.$to.'" � ���������� "'.$subject_encode.'" ����� "'.$message.'" ��������� "'.$headers.'"');
}

// ����������. ��������� ����� �� ������ � ������ �������� ������ ��������� �����, � �� ��������� �� ������.
// �������������� ��� url
function url_translit($words)
{
    $t = [
        '�' => 'a',
        '�' => 'b',
        '�' => 'v',
        '�' => 'g',
        '�' => 'd',
        '�' => 'e',
        '�' => 'e',
        '�' => 'zh',
        '�' => 'z',
        '�' => 'i',
        '�' => 'j',
        '�' => 'k',
        '�' => 'l',
        '�' => 'm',
        '�' => 'n',
        '�' => 'o',
        '�' => 'p',
        '�' => 'r',
        '�' => 's',
        '�' => 't',
        '�' => 'u',
        '�' => 'f',
        '�' => 'h',
        '�' => 'c',
        '�' => 'ch',
        '�' => 'sh',
        '�' => 'sch',
        '�' => '',
        '�' => 'y',
        '�' => '',
        '�' => 'e',
        '�' => 'yu',
        '�' => 'ya',
        '�' => 'A',
        '�' => 'B',
        '�' => 'V',
        '�' => 'G',
        '�' => 'D',
        '�' => 'E',
        '�' => 'E',
        '�' => 'Zh',
        '�' => 'Z',
        '�' => 'I',
        '�' => 'J',
        '�' => 'K',
        '�' => 'L',
        '�' => 'M',
        '�' => 'N',
        '�' => 'O',
        '�' => 'P',
        '�' => 'R',
        '�' => 'S',
        '�' => 'T',
        '�' => 'U',
        '�' => 'F',
        '�' => 'H',
        '�' => 'C',
        '�' => 'Ch',
        '�' => 'Sh',
        '�' => 'Sch',
        '�' => '',
        '�' => 'Y',
        '�' => '',
        '�' => 'E',
        '�' => 'Yu',
        '�' => 'Ya',
        ' ' => '_',
        '.' => '',
        ',' => '',
        '!' => '',
        '?' => '',
        '&' => '',
        '(' => '',
        ')' => '',
        '"' => '',
        '+' => '_',
        ';' => '',
        '-' => '_',
        ':' => '',
        '`' => '',
        '\'' => '',
        '�' => '',
    ];
    foreach ($t as $k => $v) {
        $words = str_replace($k, $v, $words);
    }

    return urlencode($words);
}

// �������������� �������� ������
function translit_ru($text)
{
    $t = [
        '�' => 'a',
        '�' => 'b',
        '�' => 'v',
        '�' => 'g',
        '�' => 'd',
        '�' => 'e',
        '�' => 'e',
        '�' => 'zh',
        '�' => 'z',
        '�' => 'i',
        '�' => 'j',
        '�' => 'k',
        '�' => 'l',
        '�' => 'm',
        '�' => 'n',
        '�' => 'o',
        '�' => 'p',
        '�' => 'r',
        '�' => 's',
        '�' => 't',
        '�' => 'u',
        '�' => 'f',
        '�' => 'h',
        '�' => 'c',
        '�' => 'ch',
        '�' => 'sh',
        '�' => 'sch',
        '�' => '\'',
        '�' => 'i',
        '�' => '\'',
        '�' => 'e',
        '�' => 'yu',
        '�' => 'ya',
        '�' => 'A',
        '�' => 'B',
        '�' => 'V',
        '�' => 'G',
        '�' => 'D',
        '�' => 'E',
        '�' => 'E',
        '�' => 'Zh',
        '�' => 'Z',
        '�' => 'I',
        '�' => 'J',
        '�' => 'K',
        '�' => 'L',
        '�' => 'M',
        '�' => 'N',
        '�' => 'O',
        '�' => 'P',
        '�' => 'R',
        '�' => 'S',
        '�' => 'T',
        '�' => 'U',
        '�' => 'F',
        '�' => 'H',
        '�' => 'C',
        '�' => 'Ch',
        '�' => 'Sh',
        '�' => 'Sch',
        '�' => '\'',
        '�' => 'I',
        '�' => '\'',
        '�' => 'E',
        '�' => 'Yu',
        '�' => 'Ya',
    ];
    foreach ($t as $k => $v) {
        $search[] = $k;
        $replace[] = $v;
    }
    $text = str_replace($search, $replace, $text);

    return $text;
}

function array_to_str_alt($arr, $type = 'value')
{
    $list = '';
    $i = 0;
    foreach ($arr as $k => $v) {
        if ($i != 0) {
            $list .= ',';
        }
        $i++;
        if ($type == 'value') {
            $list .= '\''.$v.'\'';
        }
        if ($type == 'key') {
            $list .= '\''.$k.'\'';
        }
    }
    if ($list == '') {
        $list = '\'\'';
    }

    return $list;
}

// ����������� ������ � ������ �������: 'one','two'..
function array_to_str($arr)
{
    $text = '';
    foreach ($arr as $k => $v) {
        $text .= '\''.$v.'\'';
        if ($k != (count($arr) - 1)) {
            $text .= ',';
        }
    }
    if ($text == '') {
        $text = '\'\'';
    }

    return $text;
}

function header_xml()
{
    header('Content-Type: text/xml; charset=windows-1251');
}

function header_txt()
{
    header('Content-Type: text/plain');
    header('Content-Disposition: attachment');
}

function header_jpg()
{
    header('Content-Type: image/jpeg');
}

// ���������� ��������� � ������ �� ���������� ��������
function alert($text)
{
    global $config;
    // $config['alert']=$text;
    if (empty($text)) {
        error('�� �������� ���������');
    }
    $url = '';
    if (! empty($_SERVER['HTTP_REFERER'])) {
        $url = $_SERVER['HTTP_REFERER'];
    }
    temp_var('url', $url);
    temp_var('text', $text);
    exit(template('alert'));
}

function truncate_table($table)
{
    query('TRUNCATE TABLE '.$table);
    alert('������� "'.$table.'" �������');
}

function globals_print_r()
{
    if (! isset($GLOBALS)) {
        error('���������� �� ������');
    }

    return get_print_r($GLOBALS);
}

function config_print_r()
{
    global $config;
    // pre($config);
    if (! isset($config)) {
        error('���������� �� ������');
    }

    return get_print_r($config);
}

function get_print_r($var)
{
    if (! isset($var)) {
        error('���������� �� ������');
    }
    ob_start();
    print_r($var);

    return '<pre>'.ob_get_clean().'</pre>';
}

function check_dbs()
{
    global $config;
    check_db(tabname('engine'), engine_db());
    if (! empty($config['type'])) {
        check_db(tabname($config['type']), $config['type']._db());
    }
}

// ���������� ������ � ���������� ���������� (������ �����)
function list_directory_files($dir)
{
    $arr = [];
    $dh = @opendir($dir) or error('������ ������� ����������', '������ ������� ���������� "'.$dir.'"');
    while (($file = readdir($dh)) !== false) {
        if (($file !== '.') and ($file !== '..')) {
            $current_file = $dir.'/'.$file;
            if (is_file($current_file)) {
                $arr[] = $file;
            }
        }
    }

    return $arr;
}

// ���������� ������ ���� ([name][name],[name][type]) � ���������� ����������
function list_directory($dir)
{
    $arr = [];
    $dh = @opendir($dir) or error('������ ������� ����������', '������ ������� ���������� "'.$dir.'"');
    while (($file = readdir($dh)) !== false) {
        if (($file !== '.') and ($file !== '..')) {
            $arr[$file]['name'] = $file;
            $current_file = $dir.'/'.$file;
            if (is_file($current_file)) {
                $arr[$file]['type'] = 'file';
            } else {
                $arr[$file]['type'] = 'dir';
            }
        }
    }

    return $arr;
}

// ��������� �������� �������, �������� �������
function tabname($db, $table = '')
{

    global $config;

    if (isset($db) == 0) {
        error('�� ������� �������');
    }

    /* ������� xrest.net */
    if (isset($config['domain']) and ($config['domain'] == 'xrest.net') and ($db == 'videohub') and ($table == 'tags_translation')) {
        $db = 'xrest_net_changed';
    }

    if ($db == 'engine' and in_array($table, ['config', 'domains', 'change_domain', 'redirect', 'rkn_block', 'words_filter'])) {
        $db = 'laravel';
    }

    /* /������� */
    if ($table == '') {
        return $config['mysql']['prefix'].$db;
    } else {
        return $config['mysql']['prefix'].$db.'.'.$table;
    }
}

function page_numbers_alt($args = [])
{
    $rows = ! isset($args['rows']) ? error('�� �������� ����� ���-�� �����') : $args['rows'];
    $per_page = empty($args['per_page']) ? error('�� ������� ���-�� ��������� �� ��������') : $args['per_page'];
    $page = empty($args['page']) ? error('�� ������� ����� ��������') : $args['page'];
    $link = empty($args['link']) ? error('�� �������� ������ ������') : $args['link'];
    $l = empty($args['l']) ? 5 : $args['l'];
    $link_end = empty($args['link_end']) ? '' : $args['link_end'];
    $text_previous = empty($args['text_previous']) ? '&laquo;' : $args['text_previous'];
    $text_next = empty($args['text_next']) ? '&raquo;' : $args['text_next'];
    $first_last_pages = empty($args['first_last_pages']) ? 'yes' : $args['first_last_pages'];
    $class_current = empty($args['class_current']) ? 'current' : $args['class_current'];
    $class_points = empty($args['class_points']) ? 'points' : $args['class_points'];
    $a_inside_inactive = isset($args['a_inside_inactive']) ? $args['a_inside_inactive'] : 0;
    $first_page_link = empty($args['first_page_link']) ? '' : $args['first_page_link'];
    $show_points = empty($args['show_points']) ? 'yes' : $args['show_points'];
    $show_current_page = empty($args['show_current_page']) ? 'yes' : $args['show_current_page'];
    $list = page_numbers($rows, $per_page, $page, $link, $l, $link_end, $text_previous, $text_next, $first_last_pages, $class_current, $class_points, $a_inside_inactive, $first_page_link, $show_points, $show_current_page);

    return $list;
}

// ������ ������� �������
function page_numbers($rows, $per_page, $page, $link, $l = 5, $link_end = '', $text_previous = '&laquo;', $text_next = '&raquo;', $first_last_pages = 'yes', $class_current = 'current', $class_points = 'points', $a_inside_inactive = 0, $first_page_link = '', $show_points = 'yes', $show_current_page = 'yes')
{
    if ((is_numeric($page) == 0) or ($page < 1)) {
        $page = 1;
    }
    $pages = ceil($rows / $per_page);
    // if ($pages==0) {error('������� ���������� �������');};
    if ($pages == 0) {
        return '<li>�������� �����������</li>';
    }
    if ($pages == 1) {
        return '';
    }
    // if ($page>$pages) {page_404();};
    if ($page > $pages) {
        header('Location: '.htmlspecialchars_decode($link.$pages.$link_end));
    }
    $list = '';
    // $l=5; //�� ������� ������� ���������� � ����� � � ������ �������
    // � ����� �� ����� �������� ����������
    $from_page = $page - $l;
    $to_page = $page + $l;
    // ��������
    if ($from_page < 1) {
        $from_page = 1;
    }
    if ($to_page >= $pages) {
        $to_page = $pages;
    }
    // ���� ������������ ������ $l ������� � ����� �� ������, �� ������������ �� �� ������ �������
    if (($page - $from_page) < $l) {
        $to_page = $to_page + $l - $page + $from_page;
    }
    if (($to_page - $page) < $l) {
        $from_page = $from_page - $l + $to_page - $page;
    }
    // ��� ��������
    if ($from_page < 1) {
        $from_page = 1;
    }
    if ($to_page >= $pages) {
        $to_page = $pages;
    }
    // ����-����
    if ($page == 1) {
        $show_left_quote = 0;
    } else {
        $show_left_quote = 1;
    }
    if ($page == $pages) {
        $show_right_quote = 0;
    } else {
        $show_right_quote = 1;
    }
    // ������ � ��������� ��������
    if (($pages > 1) and ($from_page > 1) and ($first_last_pages == 'yes')) {
        $show_first_number = 1;
    } else {
        $show_first_number = 0;
    }
    if (($pages > 1) and ($to_page < $pages) and ($first_last_pages == 'yes')) {
        $show_last_number = 1;
    } else {
        $show_last_number = 0;
    }
    // ����������
    if ($show_points == 'yes') {
        if ($from_page < 3) {
            $show_left_points = 0;
        } else {
            $show_left_points = 1;
        }
        if ($to_page > ($pages - 2)) {
            $show_right_points = 0;
        } else {
            $show_right_points = 1;
        }
    } else {
        $show_left_points = 0;
        $show_right_points = 0;
    }
    // ������� ������ �������
    if ($show_left_quote == 1) {
        $list .= '<li><a href="'.$link.($page - 1).$link_end.'">'.$text_previous.'</a></li>'."\r\n";
    }
    if ($show_first_number == 1) {
        $list .= '<li><a href="'.$link.'1'.$link_end.'">1</a></li>'."\r\n";
    }
    if ($show_left_points == 1) {
        if ($a_inside_inactive == 0) {
            $list .= '<li class="'.$class_points.'">...</li>'."\r\n";
        }
        if ($a_inside_inactive == 1) {
            $list .= '<li class="'.$class_points.'"><a href="#">...</a></li>'."\r\n";
        }
    }
    for ($i = $from_page; $i <= $to_page; $i++) {
        $full_link = $link.$i.$link_end;
        if (! empty($first_page_link) and $i == 1) {
            $full_link = $first_page_link;
        }
        if ($i != $page) {
            $list .= '<li><a href="'.$full_link.'">'.$i.'</a></li>'."\r\n";
        } else {
            if ($show_current_page == 'yes') {
                if ($a_inside_inactive == 0) {
                    $list .= '<li class="'.$class_current.'">'.$i.'</li>'."\r\n";
                }
                if ($a_inside_inactive == 1) {
                    $list .= '<li class="'.$class_current.'"><a href="'.$full_link.'">'.$i.'</a></li>'."\r\n";
                }
            }
        }
    }
    if ($show_right_points == 1) {
        if ($a_inside_inactive == 0) {
            $list .= '<li class="'.$class_points.'">...</li>'."\r\n";
        }
        if ($a_inside_inactive == 1) {
            $list .= '<li class="'.$class_points.'"><a href="#">...</a></li>'."\r\n";
        }
    }
    if ($show_last_number == 1) {
        $list .= '<li><a href="'.$link.$pages.$link_end.'">'.$pages.'</a></li>'."\r\n";
    }
    if ($show_right_quote == 1) {
        $list .= '<li><a href="'.$link.($page + 1).$link_end.'">'.$text_next.'</a></li>'."\r\n";
    }
    $content = ''.$list.'';

    return $list;
}

function page_404()
{
    error('�������� �� ����������', '�������� '.checkstr($_SERVER['REQUEST_URI']).' �� ����������', 0, 404, 0, '404');
}

// ��������� ������ �� ������������ �������� ������� � ��������� �������
function cronjob($table)
{
    $date = getdate();
    $data = query('select action,time_range,unix_timestamp(last_time) as unix_last_time,status from '.$table);
    $arr = readdata($data, 'nokey');
    // pre($arr);
    foreach ($arr as $v) {
        // echo '�������� � '.$v['action'].'<br/>';
        if ($v['status'] == 'on' and $v['time_range'] != 0 and ($date[0] - $v['unix_last_time']) > $v['time_range']) {
            $action = '\MegawebV1\\'.$v['action'];
            if (function_exists($action) == 0) {
                error('������� �� ����������', '���� �� ��������. ������� '.$action.' �� ����������');
            }
            query('update '.$table.' set last_time=current_timestamp where action=\''.$v['action'].'\'');
            $action();
            logger()->info('��������� �������: '.$v['action']);
        }
    }
}

// ��������� ������ �� ������������ �������� ������� �������� ���� � �������, ���� ��, �� ��������� ������� � ������������, ���� ���, ���������� ��� �� �������
function cash($func)
{
    global $config;
    /*
    // �������� ������ ����������
    if (!empty($arr_config['vars'])) {
        $vars=explode(',',$arr_config['vars']);
        $vars_str='';
        foreach ($vars as $v ) {
            if (!empty($_GET[$v])) {$vars_str.='_'.$v.'='.urlencode($_GET[$v]);};
        };
    };
    $str_id=$func.'_'.$config['domain'];
    if (!empty($vars_str)) {$str_id.=$vars_str;};
    // ������ ���������� ����������� ������� ������������ � ����� ������������� �����
    $data=query('
        select func,vars,seconds,
        (select unix_timestamp(update_time) from '.tabname('engine','cash').' where str_id=\''.$str_id.'\') as unix_update,
        (select text from '.tabname('engine','cash').' where str_id=\''.$str_id.'\') as text
        from '.tabname('engine','cash_config').' where type=\''.$config['type'].'\' and func=\''.$func.'\'
        ;');
    $arr=mysql_fetch_assoc($data);
    if (empty($arr['func']) or empty($arr['seconds'])) {error('�� ���������� ��������� ���� ��� �������','�� ���������� ��������� ���� ��� �������'.$func);};
    $cur_date=getdate();
    // ���� ������ ����� - ��������� ���, $content - �� ��� ������� �������
    if (($cur_date[0]-$arr['unix_update'])>=$arr['seconds']) {
        $content=$func();
        query('insert into '.tabname('engine','cash').' set str_id=\''.$str_id.'\', domain=\''.$config['domain'].'\', func=\''.$func.'\', update_time=current_timestamp, text=\''.mysql_real_escape_string($content).'\' on duplicate key update update_time=current_timestamp, text=\''.mysql_real_escape_string($content).'\'');
    };
    // ���� ���, �� $content - ���������� ����� ����� �� ����
    if (($cur_date[0]-$arr['unix_update'])<$arr['seconds']) {$content=$arr['text'];};
    */
    // ������ ���������� ����������� �������
    if (function_exists(strtolower($func)) == false) {
        error('������� '.$func.' �� ����������');
    }
    $data = query('select func,vars,seconds from '.tabname('engine', 'cash_config').' where type=\''.$config['type'].'\' and func=\''.$func.'\'');
    $arr_config = mysql_fetch_assoc($data);
    if (empty($arr_config['func']) or empty($arr_config['seconds'])) {
        error('�� ���������� ��������� ���� ��� �������', '�� ���������� ��������� ���� ��� �������'.$func);
    }
    // �������� ������ ����������
    if (! empty($arr_config['vars'])) {
        $vars = explode(',', $arr_config['vars']);
        $vars_str = '';
        foreach ($vars as $v) {
            if (! empty($_GET[$v])) {
                $vars_str .= '_'.$v.'='.urlencode($_GET[$v]);
            }
        }
    }
    $str_id = $func.'_'.$config['domain'];
    if (! empty($vars_str)) {
        $str_id .= $vars_str;
    }
    // ������ ������� ���������� ���� � ������
    $data = query('select unix_timestamp(update_time) as unix_update,text from '.tabname('engine', 'cash').' where str_id=\''.$str_id.'\'');
    $arr_cash = mysql_fetch_assoc($data);
    $cur_date = getdate();
    // ���� ������ ����� - ��������� ���, $content - �� ��� ������� �������
    if (($cur_date[0] - $arr_cash['unix_update']) >= $arr_config['seconds']) {
        $content = $func();
        query('insert into '.tabname('engine', 'cash').' set str_id=\''.$str_id.'\', domain=\''.$config['domain'].'\', func=\''.$func.'\', update_time=current_timestamp, text=\''.mysql_real_escape_string($content).'\' on duplicate key update update_time=current_timestamp, text=\''.mysql_real_escape_string($content).'\'');
    }
    // ���� ���, �� $content - ���������� ����� ����� �� ����
    if (($cur_date[0] - $arr_cash['unix_update']) < $arr_config['seconds']) {
        $content = $arr_cash['text'];
    }

    return $content;
}

function query_count()
{
    global $config;
    if (isset($config['mysql']['counter']) == 0) {
        return 0;
    } else {
        return $config['mysql']['counter'];
    }
}

function query_list()
{
    global $config;
    $list = '';
    if (! empty($config['mysql']['list'])) {
        foreach ($config['mysql']['list'] as $v) {
            $list .= '<li>'.$v['query'].' '.$v['time'].'</li>';
        }
        $content = '<ol>'.$list.'</ol>';

        return $content;
    }
}

function query($query, $text = '')
{
    global $config,$db;
    if (isset($config['mysql']['counter']) == 0) {
        $config['mysql']['counter'] = 0;
    }
    if (empty($text) == 1) {
        $text = '��� ��������';
    }
    $before_time = runing_time();
    $query = trim($query);
    $data = mysql_query($query) or error('������ � ������� � ��', '������ � �������: '.($text).': '.($query).'. ����� ������: '.(mysql_error()));
    $after_time = runing_time();
    $config['mysql']['counter']++;
    $config['mysql']['list'][$config['mysql']['counter']]['query'] = $query;
    $config['mysql']['list'][$config['mysql']['counter']]['time'] = $after_time - $before_time;
    // ���� ����� ��������� ��������� ������ ��������
    if (($db[tabname('engine', 'config')]['property']['log_long_queries']['value'] ?? null) == 'on') {
        if ($config['mysql']['list'][$config['mysql']['counter']]['time'] > 1) {
            logger()->info('������ ������: '.$query);
        }
    }

    return $data;
}

function http_current_domain()
{
    return 'http://'.current_domain();
}

function https_current_domain()
{
    return 'https://'.current_domain();
}

function current_domain()
{
    global $config;
    if (isset($config['domain_changed_correct'])) {
        return $config['domain_changed_correct'];
    }
}

function www_current_domain()
{
    return 'www.'.current_domain();
}

// �������� ��������� ���� $from �� ������ � ���������� $to
function copytoserv($from, $to, $args = [])
{
    if (empty($args)) {
        $file = @file_get_contents($from) or error('�� ������� ������� �������', '�� ������� ������� ���� '.$from);
    } else {
        $file = filegetcontents_alt($from, $args);
    }
    // $file=@file_get_contents($from);
    $newfile = @fopen($to, 'w') or error('�� ������� ������� ����', '�� ������� ������� ���� '.$to);
    fwrite($newfile, $file);
    fclose($newfile);
    chmod($to, 0774);
}

// ��� ������ �������
function my_date($unix)
{
    $date = getdate($unix);
    $dates['day'] = $date['mday'];
    // $dates['month']=strtoupper(substr($date['month'],0,3));
    $months = ['������', '�������', '�����', '������', '���', '����', '����', '�������', '��������', '�������', '������', '�������'];
    foreach ($months as $key => $rus) {
        if ($date['mon'] == $key + 1) {
            $dates['month'] = $rus;
        }
    }
    if (strlen($date['hours']) == 1) {
        $date['hours'] = '0'.$date['hours'];
    }
    if (strlen($date['minutes']) == 1) {
        $date['minutes'] = '0'.$date['minutes'];
    }
    $dates['clock'] = $date['hours'].':'.$date['minutes'];
    $dates['year'] = $date['year'];

    return $dates;
}

function copyright_years($year)
{
    $date = getdate();
    if ($year > $date['year']) {
        error('��� �� ����� ���� ������ ��������');
    }
    if ($date['year'] == $year) {
        $content = $year;
    } else {
        $content = $year.'-'.$date['year'];
    }

    return $content;
}

// ���������� ����, ����� � �����
function server_time()
{
    $time = getdate();
    $mytime = my_date($time[0]);
    $content = $mytime['day'].' '.$mytime['month'].' '.$mytime['clock'];

    return $content;
}

// �������� � ������ $string ���������� ����� $before � $after
function copybetwen($before, $after, $string)
{
    $result_before = explode($before, $string);
    // if (count($result_before)==1) {error('��� ��������� "'.$before.'" � ������ "'.$string.'"');};
    if (count($result_before) == 1) {
        return false;
    }
    $string = $result_before[1];
    $result_after = explode($after, $string);
    $string = $result_after[0];

    return $string;
}

function filegetcontents_curl_mobile($link)
{
    $interfaces[] = 'eth0';
    $interfaces[] = 'ppp0';
    $interfaces[] = 'ppp1';
    // $interfaces[]='ppp2';
    $interface = rnd_from_array($interfaces);
    // logger()->info($interface);
    $str = '/usr/bin/sudo /usr/bin/curl --interface '.$interface.' --user-agent \'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3\' \''.$link.'\'';

    return shell_exec($str);
}

function filegetcontents_alt($url, $args = [])
{
    if (empty($url)) {
        error('�� ������� URL');
    }
    if (empty($args['error'])) {
        $args['error'] = 'default_error';
    }
    if (empty($args['ua'])) {
        $args['ua'] = 'default_ua';
    }
    if (empty($args['interface'])) {
        $args['interface'] = 'default_interface';
    }
    if (empty($args['proxy'])) {
        $args['proxy'] = 'default_proxy';
    }
    if (empty($args['return_info'])) {
        $args['return_info'] = 'body';
    }
    if (empty($args['cookies'])) {
        $args['cookies'] = [];
    }
    if (empty($args['headers'])) {
        $args['headers'] = 'default_headers';
    }
    if (empty($args['timeout'])) {
        $args['timeout'] = 10;
    }

    return filegetcontents($url, $args['error'], $args['ua'], $args['interface'], $args['proxy'], $args['return_info'], $args['cookies'], $args['headers'], $args['timeout']);
}

function request_headers()
{
    return pre_return(apache_request_headers());
}

// ������������ file_get_contents
function filegetcontents($url, $error = 'default_error', $ua = 'default_ua', $interface = 'default_interface', $proxy = 'default_proxy', $return_info = 'body', $cookies = [], $headers = 'default_headers', $timeout = 10)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $uas['mozilla'] = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.0.10) Gecko/2009042316 MRA 5.4 (build 02620) Firefox/3.0.10';
    $uas['android'] = 'Android';
    $uas['iphone'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3';
    $uas['ipad'] = 'Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3';
    $uas['default_ua'] = $uas['mozilla'];
    curl_setopt($curl, CURLOPT_USERAGENT, $uas[$ua]);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
    // ����� ���� �������� � ��������� � �������������
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    if ($headers != 'default_headers') {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        // pre($headers);
    }
    if ($interface != 'default_interface') {
        curl_setopt($curl, CURLOPT_INTERFACE, $interface);
    }
    if (! empty($_GET['port'])) {
        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:'.$_GET['port']);
    }
    $cookies_str = '';
    if (! empty($cookies)) {
        $i = 0;
        foreach ($cookies as $ck => $cv) {
            if ($i != 0) {
                $cookies_str .= '; ';
            }
            $cookies_str .= $ck.'='.$cv;
            $i++;
        }
        // pre($cookies_str);
        curl_setopt($curl, CURLOPT_COOKIE, $cookies_str);
    }
    $chosed_proxy = $proxy;
    if ($proxy != 'default_proxy') {
        $proxies = proxies_array();
        $chosed_proxy = $proxies[$proxy];
        curl_setopt($curl, CURLOPT_PROXY, $chosed_proxy);
    }
    if ($return_info == 'headers') {
        curl_setopt($curl, CURLOPT_NOBODY, 1);
        curl_setopt($curl, CURLOPT_HEADER, 1);
    }
    if ($return_info == 'headers_body') {
        curl_setopt($curl, CURLOPT_HEADER, 1);
    }
    // if ($return_info=='return_page' or $return_info=='headers' or $return_info=='return_getinfo') {
    if ($error == 'default_error') {
        // $page=curl_exec($curl) or error('���������� ��������� ��������� ��������','���������� ��������� URL: "'.$url.'" ('.$chosed_proxy.')');
        $page = curl_exec($curl) or error('���������� ��������� ��������� ��������', '���������� ��������� URL: "'.curl_error($curl).'" "'.$url.'" ('.$chosed_proxy.')');
    }
    if ($error == 'no_error') {
        $page = curl_exec($curl) or $page = 'error';
        // $page=curl_exec($curl) or die('�������: '.$page);
    }
    if ($return_info == 'getinfo') {
        $return['body'] = $page;
        curl_setopt($curl, CURLINFO_HEADER_OUT, 1);
        $return['getinfo'] = curl_getinfo($curl);
    } else {
        $return = $page;
    }
    // };
    curl_close($curl);

    // die($page);
    return $return;
}

function proxies_array()
{
    $proxies['tor1'] = '127.0.0.1:8118';
    $proxies['tor2'] = '127.0.0.1:8111';
    $proxies['tor3'] = '127.0.0.1:8112';
    $proxies['tor4'] = '127.0.0.1:8113';
    $proxies['tor5'] = '127.0.0.1:8114';
    $proxies['tor6'] = '127.0.0.1:8115';
    $proxies['tor7'] = '127.0.0.1:8116';
    $rnd_proxies = [$proxies['tor1'], $proxies['tor2'], $proxies['tor3'], $proxies['tor4'], $proxies['tor5'], $proxies['tor6'], $proxies['tor7']];
    // $rnd_proxies=array($proxies['tor1'],$proxies['tor3'],$proxies['tor4'],$proxies['tor5'],$proxies['tor7']);
    // $rnd_proxies=array($proxies['tor4']);
    $proxies['rnd'] = rnd_from_array($rnd_proxies);
    foreach ($proxies as $k => $v) {
        if ($v == $proxies['rnd']) {
            $rnd_name = $k;
            break;
        }
    }
    $proxies['rnd_name'] = $rnd_name;

    // pre($proxies);
    return $proxies;
}

// �������� ���������� ������ �� ��������
function square2braces($text)
{
    $text = str_replace('[', '{', $text);
    $text = str_replace(']', '}', $text);

    return $text;
}

// �������� �������� ������ �� ����������
function braces2square($text)
{
    $text = str_replace('{', '[', $text);
    $text = str_replace('}', ']', $text);

    return $text;
}

// �������� �������� ������ � �������� ������ � �������� �� ����������, �����������
function braces2square_irreversible($text)
{
    $text = str_replace(['{(', '{'], '[', $text);
    $text = str_replace([')}', '}'], ']', $text);

    return $text;
}

function braces2square_alt($text)
{
    $text = str_replace('{', '&#123;', $text);
    $text = str_replace('}', '&#125;', $text);

    return $text;
}

function square2braces_alt($text)
{
    $text = str_replace('&#123;', '{', $text);
    $text = str_replace('&#125;', '}', $text);

    return $text;
}

// ������� ������������� ����������
function checkstr($value)
{
    // ���� magic_quotes_gpc �������� - ���������� stripslashes
    // if (get_magic_quotes_gpc()) {$value=stripslashes($value);};
    // ���� ���������� - �����, �� ������������ � �� �����
    // ���� ��� - �� ������� � ���������, � ����������
    // if (!is_numeric($value)) {$value="'".mysql_real_escape_string($value)."'";};
    // $value=str_replace(array('{','}'),'',$value);
    $value = mysql_real_escape_string($value);

    // or error('������������� �� ��������');
    return $value;
}

function login($type)
{
    if (isset($_SESSION[$type]['auth']['login']) == 1) {
        return $_SESSION[$type]['auth']['login'];
    } else {
        error('����� �����������');
    }
}

function login_engine()
{
    // return login('engine');
    return auth_login(['type' => 'engine']);
}

function authorizate_engine_root()
{
    // authorizate('engine',tabname('engine','users'),array('root'));
    authorizate_alt(['type' => 'engine', 'levels' => ['root']]);
}

function authorizate_engine_admin()
{
    // authorizate('engine',tabname('engine','users'),array('root','admin'));
    authorizate_alt(['type' => 'engine', 'levels' => ['root', 'admin']]);
}

function authorizate_engine_lite()
{
    // authorizate('engine',tabname('engine','users'),array('root','admin','lite'));
    authorizate_alt(['type' => 'engine', 'levels' => ['root', 'admin', 'lite']]);
}

function authorizate_engine_worker()
{
    // authorizate('engine',tabname('engine','users'),array('root','admin','lite'));
    authorizate_alt(['type' => 'engine', 'levels' => ['admin', 'worker', 'lite']]);
}

function preparing_user_info($args)
{
    // pre($args);
    if (empty($args['login'])) {
        error('�� ������� �����');
    }
    if (empty($args['type'])) {
        error('�� ������� ���');
    }
    $arr = query_assoc('select login,level from '.tabname($args['type'], 'users').' where login=\''.$args['login'].'\';');

    return $arr;
}

function engine_user_info()
{
    return preparing_sid_info(['type' => 'engine']);
}

// �� �������� ������ cache,0 ��� ������ ���� �������
// ���������� sid � type. ���������� id ������������, ����� � ������� �������
function preparing_sid_info($args)
{
    global $config;
    $type = empty($args['type']) ? error('�� ������� ���') : $args['type'];
    $sid = empty($config['vars']['auth'][$type]['sid']) ? error('�� ������� sid') : $config['vars']['auth'][$type]['sid'];
    $arr = query_assoc('select users.id,users.login,users.level
		from '.tabname($type, 'user_sessions').'
		join '.tabname($type, 'users').' on user_sessions.login=users.login
		where user_sessions.sid=\''.$sid.'\'
		;');

    return $arr;
}

function authorizate_logout($type)
{
    if (session_id() == '') {
        session_start();
    }
    if (isset($_SESSION[$type]['auth']['status']) == 1) {
        unset($_SESSION[$type]['auth']['status']);
    }
    alert('�� �����');
}

function authorizate_logout_redirect($args = [])
{
    $args['type'] = empty($args['type']) ? error('�� ������� ���') : $args['type'];
    if (session_id() == '') {
        session_start();
    }
    if (isset($_SESSION[$args['type']]['auth']['status']) == 1) {
        unset($_SESSION[$args['type']]['auth']['status']);
    }
    redirect('/');
}

function authorizate_engine_logout()
{
    authorizate_logout_alt(['type' => 'engine']);
}

function authorizate($type, $table, $levels)
{
    global $db,$config;
    // if (isset($_GET['mmnas'])) {die('OK '.session_start());};
    if (session_id() == '') {
        session_start();
    }
    // if (isset($_GET['mmnas'])) {pre($_SESSION);};
    if (! isset($_SESSION[$type]['auth']['status'])) {
        $_SESSION[$type]['auth']['status'] = 0;
    }
    if ($_SESSION[$type]['auth']['status'] == 0) {
        if (isset($_POST['action']) and $_POST['action'] == 'enter') {
            $_POST['login'] = strtolower(trim($_POST['login']));
            // �� �� ���� ��� ��� mysql "petr" � "petr " ���� � �� �� � Nick � nick ����
            if (! isset($_POST['login'])) {
                error('�� ����� �����');
            }
            if (! isset($_POST['pass'])) {
                error('�� ����� ������');
            }
            $arr = query_assoc('select pass,level from '.$table.' where login=\''.checkstr($_POST['login']).'\';');
            // pre($arr);
            if (! $arr) {
                error('������������ '.$_POST['login'].' �� ������');
            }
            if ($_POST['pass'] !== $arr['pass']) {
                error('�������� ������ ��� ������������ '.$_POST['login']);
            } else {
                $_SESSION[$type]['auth']['login'] = $_POST['login'];
                $_SESSION[$type]['auth']['status'] = 1;
                query('update '.$table.' set last_activ=current_timestamp where login=\''.$_SESSION[$type]['auth']['login'].'\'');
                logger()->info('������������� '.$_SESSION[$type]['auth']['login'].' � '.$type);
            }
        }
    }
    if ($_SESSION[$type]['auth']['status'] == 1) {
        $date = getdate();
        $arr = query_assoc('select level,unix_timestamp(last_activ) as unix_date,timezone from '.$table.' where login=\''.$_SESSION[$type]['auth']['login'].'\'');
        $auth_range = $db[tabname('engine', 'config')]['property']['auth_range']['value'];
        if (($date[0] - $arr['unix_date']) > $auth_range) {
            $_SESSION[$type]['auth']['status'] = 0;
            logger()->info('����� ������ ('.$auth_range.' ������) ������� ��� '.$_SESSION[$type]['auth']['login'].'. ���������� �����������.');
        } else {
            // ��� ��
            ! empty($arr['timezone']) ? set_timezone($arr['timezone']) : '';
            query('update '.$table.' set last_activ=current_timestamp where login=\''.$_SESSION[$type]['auth']['login'].'\'');
        }
        if (! in_array($arr['level'], $levels)) {
            error($_SESSION[$type]['auth']['login'].', �� �� ������ ���� ������� � ���� ��������');
        }
    }
    if ($_SESSION[$type]['auth']['status'] != 1) {
        temp_var('type', $type);
        exit(template('authorizate'));
    }
}

// ���������� die, ����� ��������� ������
function authorization_form($args = [])
{
    $type = empty($args['type']) ? error('�� ������� type') : $args['type'];
    temp_var('type', $type);
    exit(template('authorizate'));
}

// ����� ������
function authorizate_logout_alt($args = [])
{
    $type = empty($args['type']) ? error('�� ������� type') : $args['type'];
    auth_clear_sid(['type' => $type]);
    alert('�� �����');
}

// ����� ������
// ������� ���� ������
function auth_clear_sid($args = [])
{
    $type = empty($args['type']) ? error('�� ������� type') : $args['type'];
    setcookie('mgw_sid_'.$type, '', 1);
}

// ����� ������
function authorizate_logout_redirect_alt($args = [])
{
    $type = empty($args['type']) ? error('�� ������� ���') : $args['type'];
    auth_clear_sid(['type' => $type]);
    redirect('/');
}

// ����� ������
// ���������� �����
// ���� ������� $args['error']=true, �� � ������ ���������� ������ ������������ false ������ ������
function auth_login($args)
{
    global $config;
    $args['error'] = isset($args['error']) ? $args['error'] : true;
    $type = isset($args['type']) ? $args['type'] : error('�� ������� type');
    $login = isset($config['vars']['auth'][$type]['login']) ? $config['vars']['auth'][$type]['login'] : ($args['error'] ? error('����������� �� ��������') : false);

    return $login;
}

function auth_failure_attempt($args = [])
{
    $vars = empty($args['vars']) ? [] : $args['vars'];
    $description = empty($args['description']) ? '' : $args['description'];
    $ip = request()->ip();
    query('insert into '.tabname('laravel', 'auth_failures').' set
		ip=\''.mysql_real_escape_string($ip).'\',
		vars=\''.mysql_real_escape_string(serialize($vars)).'\',
		description=\''.mysql_real_escape_string($description).'\'
		;');
}

// ��������
function auth_failure_ip_exist($args = [])
{
    $ip = empty($args['ip']) ? error('�� ������� IP') : $args['ip'];
    $arr = query_assoc('select count(ip) as q from '.tabname('laravel', 'auth_failures').'
		where ip=inet_aton(\''.mysql_real_escape_string($ip).'\'),
		vars=\''.mysql_real_escape_string(serialize($args)).'\'
		;');
}

// ����� ������
function authorizate_alt($args)
{
    global $db,$config;
    $type = isset($args['type']) ? $args['type'] : error('�� ������� type');
    // ���� �������� ��� ��������� - ���������
    // pre($config);
    $status = (isset($config['vars']['auth'][$type]['login']) and $config['vars']['auth'][$type]['login'] !== false) ? true : false;
    if ($status) {
        return;
    }
    // ��������� ����������
    $levels = isset($args['levels']) ? $args['levels'] : error('�� ������� levels');
    $post['login'] = empty($_POST['login']) ? false : strtolower(trim($_POST['login']));
    $post['password'] = empty($_POST['pass']) ? false : trim($_POST['pass']);
    $post['action'] = empty($_POST['action']) ? false : $_POST['action'];
    $post['type'] = empty($_POST['type']) ? false : $_POST['type'];
    // �������� id ������ �� ����
    $sid = empty($_COOKIE['mgw_sid_'.$type]) ? false : $_COOKIE['mgw_sid_'.$type];
    // pre($_SERVER);
    // �������� ������� ��� ��������
    $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : false;
    // ���� sid ��� � �������� �������������� - ��������� ������
    // pre($sid);
    if ($post['action'] === 'enter' and $post['type'] === $type and $post['login'] and $post['password']) {
        // $sid=submit_authorization(array('login'=>$post_login,'password'=>$post_password));
        if (empty($post['login'])) {
            error('�� ����� �����');
        }
        if (empty($post['password'])) {
            error('�� ����� ������');
        }
        $arr = query_assoc('select login,pass,level from '.tabname($type, 'users').' where login=\''.mysql_real_escape_string($post['login']).'\';');
        if (! $arr) {
            auth_failure_attempt(['description' => '������������ �� ������', 'vars' => $post]);
            error('������������ �� ������');
        }
        if ($post['password'] !== $arr['pass']) {
            auth_failure_attempt(['description' => '�������� ������', 'vars' => $post]);
            error('�������� ������ ��� ������������ '.$arr['login']);
        }
        // ����������� �������, ���������� sid
        do {
            $sid = sha1(mt_rand().mt_rand());
            // ��������� ����� � ���� ��� ���� ����� sid
            $check_sid = query_assoc('select sid from '.tabname($type, 'user_sessions').' where sid=\''.$sid.'\';');
        } while (! empty($check_sid));
        query('insert into '.tabname($type, 'user_sessions').' set
    		sid=\''.$sid.'\',
    		login=\''.$arr['login'].'\',
    		user_agent=\''.mysql_real_escape_string($user_agent).'\',
    		ip=\''.mysql_real_escape_string(request()->ip()).'\',
    		last_activity=current_timestamp
    		;');
        // ������������� ���� � ID ������ �� ���
        setcookie('mgw_sid_'.$type, $sid, time() + 3600 * 24 * 365);
        logger()->info('������������� '.$arr['login'].' � '.$type);
    }
    // ���� sid ��� � �� ������� �� ������
    if (! $sid) {
        authorization_form(['type' => $type]);
    }
    // ��������� sid ���������� ���� ������ ��� ���������������
    $arr = query_assoc('select users.login,users.level,users.timezone,
    	unix_timestamp(user_sessions.last_activity) as unix_date, user_sessions.sid,
    	user_sessions.user_agent
    	from '.tabname($type, 'user_sessions').'
    	join '.tabname($type, 'users').' on users.login=user_sessions.login
    	where user_sessions.sid=\''.mysql_real_escape_string($sid).'\'
    	;');
    // ���� ��� ����� ������ - ������
    if (! $arr) {
        // ������� ���� (����� ��������� ������ � auth_failures �� ��������, �������� ���� ������� ���� ������)
        auth_clear_sid(['type' => $type]);
        auth_failure_attempt(['description' => '���������� sid ����������� � ����', 'vars' => ['sid' => $sid]]);
        authorization_form(['type' => $type]);
    }
    // ��������� �� �������� �� ������
    $date = getdate();
    $timeout = isset($db[tabname('engine', 'config')]['property']['auth_session_timeout']['value']) ? $db[tabname('engine', 'config')]['property']['auth_session_timeout']['value'] : error('�� ��������� �������');
    // ���� ������� ��������, �� ������� ���� � ������
    if (($date[0] - $arr['unix_date']) > $timeout) {
        // ������� ����, ����� ������ �� ����������� ��� �����
        auth_clear_sid(['type' => $type]);
        // logger()->info('������� ������ '.$arr['sid'].'.');
        error('������� ������. �������� ����������� �����.', '������� ������ '.$arr['sid'].'.');

        return authorization_form(['type' => $type]);
    }
    // ��������� �������
    if ($arr['user_agent'] !== $user_agent) {
        auth_failure_attempt(['description' => '������� �� ������', 'vars' => ['sid' => $sid, 'user_agent' => $user_agent]]);
        // ������� ����, ����� ������ �� ����������� ��� �����
        auth_clear_sid(['type' => $type]);
        error('������������ ��������. �������� ����������� �����.');
    }
    // ��������� ����� �� ������������ ������ ����� �������
    if (! in_array($arr['level'], $levels)) {
        error($arr['login'].', �� �� ������ ���� ������� � ���� ��������');
    }
    // ��� �������� ��������
    $config['vars']['auth'][$type]['login'] = $arr['login'];
    $config['vars']['auth'][$type]['sid'] = $sid;
    if (! empty($arr['timezone'])) {
        set_timezone($arr['timezone']);
    }
    query('update '.tabname($type, 'user_sessions').' set last_activity=current_timestamp,
    	cookies=\''.mysql_real_escape_string(serialize(empty($_COOKIE) ? [] : $_COOKIE)).'\'
    	where sid=\''.mysql_real_escape_string($sid).'\'');
}

function query_url()
{
    global $config;
    // pre($_SERVER);
    if (isset($config['domain'])) {
        // return 'http://'.$config['domain'].$_SERVER['REQUEST_URI'];
        return 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }
}

// ����������� ����� �� � while �������� $pattern_comment
// �������� � $content ��� {} �� ���. ������ ��������� ��������, ���� ��� �� ���������.
// ���� � {} ���� ����� �� �������� ������, ���� ����� ��� - �� ��������.
// ���� $is_text=0, �� ����������� �������� ���������� � page, ���� 1, �� ���������� page � ���� content.
function template_old($page, $is_text = 0)
{
    global $config;
    $pattern_comment = '/\\/\/{([\w\.\/()]+)}/e';
    $pattern_temp = '/\{([\w\.\/]+)\.([\w]*)\}/e';
    $pattern_func = '/\{([\w]+)\}/e';
    $pattern_func_param = '/\{([\w]+)\(([\w\.\/()\[\]\'\']+)\)\}/e'; // � ���������� ��������� ���������� ������ � ������� ��� get_var
    $pattern_var = '/\{\(([\w]+)\)\}/e';
    // $pattern_since_date='/\{([\w]+)\|([\w\.\/]+)\.([\w]*)\\|([\w\.\/]+)\.([\w]*)\}/e';
    if ($is_text == 0) {
        $content = temp_replacement($page.'.htm');
    } else {
        $content = $page;
    }
    do {
        $content = preg_replace($pattern_var, "get_var('\\1')", $content);
        $content = preg_replace($pattern_comment, "comment_replacement('\\1')", $content);
        $content = preg_replace($pattern_func, "func_replacement('\\1')", $content);
        $content = preg_replace($pattern_func_param, "func_replacement_param('\\1','\\2')", $content);
        // echo $content.'<hr/><hr/>'; flush();
        $content = preg_replace($pattern_temp, "temp_replacement('\\1.\\2')", $content);
        // echo $content.'<hr/><hr/>'; flush();
    } while (
        (preg_match($pattern_var, $content) == 1) or
        (preg_match($pattern_comment, $content) == 1) or
        (preg_match($pattern_func, $content) == 1) or
        (preg_match($pattern_func_param, $content) == 1) or
        (preg_match($pattern_temp, $content) == 1)
    );

    /*
    do {$content=preg_replace($pattern_comment,"comment_replacement('\\1')",$content);} while (preg_match($pattern_comment,$content)==1);
    do {$content=preg_replace($pattern_func,"func_replacement('\\1')",$content);} while (preg_match($pattern_func,$content)==1);
    do {$content=preg_replace($pattern_func_param,"func_replacement_param('\\1','\\2')",$content);} while (preg_match($pattern_func_param,$content)==1);
    do {$content=preg_replace($pattern_temp,"temp_replacement('\\1.\\2')",$content);} while (preg_match($pattern_temp,$content)==1);
    */
    return $content;
}

function get_var_callback($m)
{
    return get_var($m[1]);
}

function comment_replacement_callback($m)
{
    return comment_replacement($m[1]);
}

function obj_func_replacement_callback($m)
{
    return obj_func_replacement($m[1], $m[2]);
}

function func_replacement_callback($m)
{
    return func_replacement($m[1]);
}

function func_replacement_param_callback($m)
{
    return func_replacement_param($m[1], $m[2]);
}

function temp_replacement_callback($m)
{
    return temp_replacement($m[1].'.'.$m[2]);
}

function template($page, $is_text = 0)
{
    global $config;
    /*
    $pattern_comment='/\\/\/{([\w\.\/()]+)}/';
    $pattern_temp='/\{([\w\.\/]+)\.([\w]*)\}/';
    $pattern_func='/\{([\w]+)\}/';
    $pattern_func_param='/\{([\w]+)\(([\w\.\/()\[\]\'\'-]+)\)\}/'; //� ���������� ��������� ���������� ������ � ������� ��� get_var
    $pattern_var='/\{\(([\w]+)\)\}/';
    */
    $pattern_obj_func = '/(?!\/\/)\{\{([\w]+)\:\:([\w]+)}\}/';
    $pattern_comment = '/\/\/\{\{?\(?([\w\.\/\-():]+)\)?\}?\}/';
    $pattern_temp = '/(?!\/\/)\{([\w\.\/-]+)\.([\w]*)\}/';
    $pattern_func = '/(?!\/\/)\{([\w]+)\}/';
    $pattern_func_param = '/(?!\/\/)\{([\w]+)\(([\w\.\/()\[\]\'\'-]+)\)\}/'; // � ���������� ��������� ���������� ������ � ������� ��� get_var
    $pattern_var = '/(?!\/\/)\{\(([\w]+)\)\}/';
    // $pattern_since_date='/\{([\w]+)\|([\w\.\/]+)\.([\w]*)\\|([\w\.\/]+)\.([\w]*)\}/e';
    if ($is_text == 0) {
        $content = temp_replacement($page.'.htm');
    } else {
        $content = $page;
    }
    // ���� ��������. ���� func_replacement_callback ���������� ������������������ ������ � ��������, �� ��� ��� ����� ���������� ��������,
    // �.�. ����������� ������ temp_replacement_callback. � �������� ������� - ��������, ���� ����� �� preg_replace_callback ���������, ��
    // ������� continue � ���� ��������� ����� ������, ��� ����� ������ ��������� ����������������. �� �������� ��� ������� ������ ��������.
    // ��������������.
    do {
        // ������� ������� comment_replacement_callback � get_var_callback, ����� ���������������� ����������. ���������� ��� ��������
        $content = preg_replace_callback($pattern_comment, "\MegawebV1\\comment_replacement_callback", $content);
        // echo $content.'<hr/><hr/>'; flush();
        $content = preg_replace_callback($pattern_var, "\MegawebV1\\get_var_callback", $content);
        // echo $content.'<hr/><hr/>'; flush();
        $content = preg_replace_callback($pattern_obj_func, "\MegawebV1\\obj_func_replacement_callback", $content);
        $content = preg_replace_callback($pattern_func, "\MegawebV1\\func_replacement_callback", $content);
        // echo $content.'<hr/><hr/>'; flush();
        $content = preg_replace_callback($pattern_func_param, "\MegawebV1\\func_replacement_param_callback", $content);
        // echo $content.'<hr/><hr/>'; flush();
        $content = preg_replace_callback($pattern_temp, "\MegawebV1\\temp_replacement_callback", $content);
        // echo $content.'<hr/><hr/>'; flush();
        // die('');
    } while (
        (preg_match($pattern_var, $content) == 1) or
        (preg_match($pattern_comment, $content) == 1) or
        (preg_match($pattern_func, $content) == 1) or
        (preg_match($pattern_obj_func, $content) == 1) or
        (preg_match($pattern_func_param, $content) == 1) or
        (preg_match($pattern_temp, $content) == 1)
    );

    /*
    do {$content=preg_replace($pattern_comment,"comment_replacement('\\1')",$content);} while (preg_match($pattern_comment,$content)==1);
    do {$content=preg_replace($pattern_func,"func_replacement('\\1')",$content);} while (preg_match($pattern_func,$content)==1);
    do {$content=preg_replace($pattern_func_param,"func_replacement_param('\\1','\\2')",$content);} while (preg_match($pattern_func_param,$content)==1);
    do {$content=preg_replace($pattern_temp,"temp_replacement('\\1.\\2')",$content);} while (preg_match($pattern_temp,$content)==1);
    */
    return $content;
}

function comment_replacement($page)
{
    return '';
}

// function temp_replacement($page,$domain='') {
// 	global $config;
// 	if ($domain=='') {$domain=$config['domain'];};
// 	$page=strtolower($page);
// 	$path_site=$config['path']['server'].'sites/'.$domain.'/template/';
// 	// pre($path_site);
// 	if ((isset($path_site)==0) or (file_exists($path_site.$page)==0)) {
// 		if ((isset($config['path']['type'])==0) or file_exists($config['path']['type'].$page)==0) {
// 			if ((isset($config['path']['engine'])==0) or file_exists($config['path']['engine'].$page)==0) {
// 				error('�������� �� ����������','������ '.$page.' ����������',1,404);
// 			} else {$exist=$config['path']['engine'].$page;};
// 		} else {$exist=$config['path']['type'].$page;};
// 	} else {$exist=$path_site.$page;};
// 	$content=file_get_contents($exist);
// 	//echo $exist.'<br/>'; flush();
// 	return $content;
// }

function temp_replacement($page)
{
    global $config;
    $page = strtolower($page);
    if (isset($config['used_templates'][$page])) {
        return $config['used_templates'][$page];
    }
    if ((isset($config['path']['site']) == 0) or (file_exists($config['path']['site'].$page) == 0)) {
        if ((isset($config['path']['type']) == 0) or file_exists($config['path']['type'].$page) == 0) {
            if ((isset($config['path']['engine']) == 0) or file_exists($config['path']['engine'].$page) == 0) {
                error('�������� �� ����������', '������ '.$page.' ����������', 1, 404);
            } else {
                $exist = $config['path']['engine'].$page;
            }
        } else {
            $exist = $config['path']['type'].$page;
        }
    } else {
        $exist = $config['path']['site'].$page;
    }
    $content = file_get_contents($exist);
    $config['used_templates'][$page] = $content;

    // echo $exist.'<br/>';
    return $content;
}

// ��������� ���������� ����������� ������ ������� ��� get_var
function func_replacement_param($func, $param)
{
    global $config;
    $func = '\\MegawebV1\\'.strtolower($func);
    if (function_exists($func) == false) {
        error('������� '.$func.' �� ����������');
    }
    // ��������� ��������� �� ��� ����� �������, ���� ���, �� ���������, ���� ��, �� ������ ���������� � ������� � ����������� ������ �������.
    if ((isset($config['func_result'][$func.'('.$param.')']) == 0) or ($func == 'get_var')) {
        $config['func_result'][$func.'('.$param.')'] = $func($param);
    }

    // echo 'func_result'.'['.$func.'('.$param.')]'.'<br/>'; flush();
    return $config['func_result'][$func.'('.$param.')'];
}

function func_replacement($func)
{
    global $config;
    $func = '\\MegawebV1\\'.strtolower($func);
    if (function_exists($func) == false) {
        error('������� '.$func.' �� ����������');
    }
    // ��������� ��������� �� ��� ����� �������, ���� ���, �� ���������, ���� ��, �� ������ ���������� � ������� � ����������� ������ �������.
    if (isset($config['func_result'][$func]) == 0) {
        $config['func_result'][$func] = $func();
    }

    // echo 'func_result'.'['.$func.']'.'<br/>'; flush();
    return $config['func_result'][$func];
}

function obj_func_replacement($class, $method)
{
    global $config;
    $class = '\\MegawebV1\\'.$class;
    $object = new $class;
    // $func=strtolower($func);
    if (! method_exists($object, $method)) {
        error('Method '.$method.' is not exist in '.$class);
    }
    // ��������� ��������� �� ��� ����� �������, ���� ���, �� ���������, ���� ��, �� ������ ���������� � ������� � ����������� ������ �������.
    if (! isset($config['obj_func_result'][$class][$method])) {
        $config['obj_func_result'][$class][$method] = $object->$method();
    }

    // echo 'func_result'.'['.$func.']'.'<br/>'; flush();
    return $config['obj_func_result'][$class][$method];
}

function logs_alt($text, $args)
{
    if (! isset($args['backtrace'])) {
        $args['backtrace'] = '';
    }
    if (! isset($args['dest'])) {
        $args['dest'] = 'db';
    }
    if (! isset($args['type'])) {
        $args['type'] = '';
    }
    // logger()->info($text, $args['backtrace'], $args['dest'], $args['type']);
    logger()->info($text);
}

function logs($text, $backtrace = '', $dest = 'db', $type = '')
{
    global $config;
    // pre($_SERVER);
    // ��������� ����������
    if (isset($_SERVER['HTTP_USER_AGENT']) == 0) {
        $_SERVER['HTTP_USER_AGENT'] = '';
    }
    if (isset($_SERVER['HTTP_REFERER']) == 0) {
        $_SERVER['HTTP_REFERER'] = '';
    }
    if ($dest == 'db') {
        // $query='insert into '.tabname('engine','logs').' set
        // text=\''.checkstr($text).'\',
        // ip=\''.checkstr(request()->ip()).'\',
        // user_agent=\''.checkstr($_SERVER['HTTP_USER_AGENT']).'\',
        // query=\''.checkstr(query_url()).'\',
        // referer=\''.checkstr($_SERVER['HTTP_REFERER']).'\',
        // requests=\'get: '.checkstr(serialize($_GET))."\r\n".'post: '.checkstr(serialize($_POST))."\r\n".'cookie: '.checkstr(serialize($_COOKIE)).'\',
        // backtrace=\''.checkstr($backtrace).'\',
        // type=\''.$type.'\'
        // ';
        // pre($query);
        // query($query);
        if ($type == '404') {
            logger()->error($text);
        } else {
            logger()->info($text);
        }
    }
    if ($dest == 'file') {
        $log_path = $config['path']['server'].'logs/logmmnas.txt';
        $log_file = fopen($log_path, 'a');
        $item = '
		'.request()->ip().'
		'.$text.'
		'.query_url().'
		'.$_SERVER['HTTP_REFERER'].'
		get: '.serialize($_GET).'
		post: '.serialize($_POST).'
		cookie: '.serialize($_COOKIE).'
		'.$backtrace.'
		'.$_SERVER['HTTP_USER_AGENT'].'
		type: '.$type.'
		-----------
		';
        fwrite($log_file, trim($item));
        fclose($log_file);
    }
}

// ����� ������ � ������ error.htm � ����������� ���������� �������
// text - ����� ��������� �������, logtext - � ���, log - ������ �� ���, page404 - ��������� �� header 404,
// maxlogsfromip - ���� � ���� ������ �� ����� ip � ���� ��������� ������ ���������� ����������, �� �� ������, 0 - ������ ���
function error($text, $logtext = '', $log = 1, $page404 = 404, $maxlogsfromip = 0, $type = '')
{
    global $error,$db;
    if (empty($logtext) == 1) {
        $logtext = $text;
    }
    $stack_list = debug_backtrace();
    // pre($stack_list);
    // ��������� ����������, ��� NOTICE
    if (isset($error['backtrace']) == 0) {
        $error['backtrace'] = [];
    }
    if (isset($error['counter']) == 0) {
        $error['counter'] = 0;
    }
    // ���������. ���� ������ ������� �� �� ������� ��, �������� notice
    // pre($stack_list);
    $backtrace_list_other = '';
    for ($i = count($stack_list) - 1; $i >= 1; $i--) {
        if (! isset($stack_list[$i]['file'])) {
            $backtrace_list_other .= '<li>'.$stack_list[$i]['function'].'</li>';
        } else {
            $backtrace_list_other .= '<li>'.$stack_list[$i]['function'].' '.$stack_list[$i]['file'].':'.$stack_list[$i]['line'].'</li>';
        }
    }
    // $error['backtrace'][]=$stack_list[1]['function'].' '.$stack_list[0]['line'].' ('.$logtext.')'."\r\n";
    if (! isset($stack_list[0]['file'])) {
        $error['backtrace'][] = $stack_list[1]['function'];
    } else {
        $error['backtrace'][] = $stack_list[1]['function'].' '.$stack_list[0]['file'].':'.$stack_list[0]['line'];
    }
    $backtrace_list = '';
    $backtrace_text = '';
    foreach ($error['backtrace'] as $k => $v) {
        $backtrace_list .= '<li>'.$v.'</li>';
        if ($k != 0) {
            $backtrace_text .= "\r\n";
        }
        $backtrace_text .= $v;
    }
    $error_info = '<hr/><b>��� ������:</b><ol>'."\r\n".braces2square($backtrace_list_other).'</ol>'."\r\n";
    $error['counter']++;
    $error['text'] = $text;
    $text = str_replace(["\r\n", "\n"], ' ', $text);
    if ($error['counter'] > 2) {
        // ���� error ����������� ������ 2 ���, ������ ��������� ������ ��� ������ ���� ��� danger=1
        logger()->info($logtext);
        exit('������������. ���������� �������� ���.');
        // die('���������� �������� ���. ����� ������: '.$logtext.'<br/><br/>'.$error_info);
    }
    if ($error['counter'] > 1) {
        // ���� error ����������� ������ ������ ����, ������ ��������� ������������ (������ � logs ���� template)
        // ������������� ������ � ������� ����� ������.
        logger()->info('������������: '.$logtext.' '.$error_info);
        exit('������������. ���������� �������� � ���.');
        // die('<b>������������:</b> '.$logtext.'<br/><br/>'.$error_info);
    }
    /*
    if ($maxlogsfromip!=0) {
        $data=query('select count(*) as q from '.tabname('engine','logs').' where ip=\''.request()->ip().'\' and text=\''.checkstr($backtrace_text).'\'');
        $arr=mysql_fetch_assoc($data);
        //if ($arr['q']==$maxlogsfromip) {logger()->info('��������� ���� ��� ����� IP � �������: '.$logtext);};
        if ($arr['q']>=$maxlogsfromip) {$log=0;};
    };
    */
    if ($log == 1) {
        // logger()->info($logtext);
        logger()->info($logtext);
    }
    if ($page404 == 404) {
        header('HTTP/1.0 404 Not Found');
    }
    if ($page404 == 503) {
        header('HTTP/1.0 503 Service Unavailable');
    }
    if ($db[tabname('engine', 'config')]['property']['display_errors']['value'] == 'on') {
        temp_var('error_text', $logtext.'<br/>'.$error_info);
    } else {
        temp_var('error_text', $error['text']);
    }
    exit(template('error'));
}

function error_text()
{
    global $error;

    return $error['text'];
}

// ����������� � ��
function db_connect()
{
    global $config;
    // ������ � ��
    // include($config['path']['server'].'types/engine/app/config.php');
    $config['mysql']['prefix'] = config('database.connections.mysql.prefix');
    // ������������ � ��
    $config['mysql_connection'] = @mysql_connect(
        config('database.connections.mysql.host'),
        config('database.connections.mysql.username'),
        config('database.connections.mysql.password'),
        config('database.connections.mysql.port')
    ) or error('������ ����������� � ��', '', 0);
    // $charset=false;
    // $charset=true;
    // if ($config['charset']=='utf-8') {$charset='utf8'};
    // if ($config['charset']=='windows-1251') {$charset='cp1251'};
    // if ($charset) {mysqli_set_charset($mysql_connection,'utf8');};
    // mysql_set_charset('cp1251',$mysql_connection);
}

function pdo_connect() {}

// ������ ��������� �� utf-8
function header_utf8()
{
    // logger()->info('�������');
    header('Content-Type: text/html; charset=utf-8');
}

// ������ ��������� �� windows-1251
function header_windows1251()
{
    header('Content-Type: text/html; charset=windows-1251');
}

function engine_rewrite_rules()
{
    // ������������ � www �� ��� www
    // if (preg_match('/^www\.(.*)$/',$_SERVER['HTTP_HOST'],$args)) {redirect_301('http://'.$args[1].$_SERVER['REQUEST_URI']);};
    // if (preg_match('/^types\/(.*).htm$/',$_SERVER['REQUEST_URI'],$args)) {error('������ ��������');};

}

function apiObject()
{
    global $config;
    $class = empty($_GET['class']) ? error('Class required') : checkstr($_GET['class']).'Page';
    $method = empty($_GET['method']) ? error('Method required') : checkstr($_GET['method']);
    try {
        $object = new $class;
    } catch (\Throwable $e) {
        abort(404, $e->getMessage());
    }
    if (! method_exists($object, $method)) {
        return 'Method '.$method.' is not exist in '.$class;
    }

    return $object->$method();

}

function engine_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^api\/([\w]+)\/([\w]+)$/', $query, $args)) {
        $config['vars']['page'] = 'api_object';
        $_GET['class'] = $args[1];
        $_GET['method'] = $args[2];

        return;
    }
    if (preg_match('/^save-video\/([\d]+)\.flv$/', $query, $args)) {
        $config['vars']['page'] = 'save_flv';
        $_GET['id'] = $args[1];

        return;
    }
    if (preg_match('/^save-video\/([\d]+)\.mp4$/', $query, $args)) {
        // pre('test');
        $config['vars']['page'] = 'save';
        $_GET['id'] = $args[1];

        return;
    }
    if (preg_match('/^ph-embed\/([\w]+)$/', $query, $args)) {
        redirect_301('https://www.pornhub.ru/embed/'.$args[1]);

        // $config['vars']['page']='webmaster/google'.$args[1];
        return;
    }
    if (preg_match('/^csp_report$/', $query, $args)) {
        $config['vars']['page'] = 'csp_report';

        return;
    }
    if (preg_match('/^google([\w]+).html$/', $query, $args)) {
        $config['vars']['page'] = 'webmaster/google'.$args[1];

        return;
    }
    if (preg_match('/^yandex_([\w]+).txt$/', $query, $args)) {
        $config['vars']['page'] = 'webmaster/yandex_'.$args[1];

        return;
    }
    if (preg_match('/^robots.txt$/', $query, $args)) {
        $config['vars']['page'] = 'robots';
        header('Content-Type: text/plain; charset=windows-1251');

        return;
    }
    // if (preg_match('/^message_for_oss.pdf$/',$query,$args)) {
    // 	// $config['vars']['page']='robots';
    // 	// header('Content-Type: application/pdf');
    // 	header('Location: /template/message_for_oss.pdf');
    // 	die();
    // 	return;
    // };
    /*
    if (preg_match('/^sitemap.xml$/',$query,$args)) {
        $config['vars']['page']='sitemap';
        return;
    };
    */
    if (preg_match('/^crossdomain.xml$/', $query, $args)) {
        $config['vars']['page'] = 'crossdomain';

        return;
    }
    if (preg_match('/^(.*)3rd\/adriver.html$/', $query, $args)) {
        error('�������� �� ����������', '', 0);

        // $config['vars']['page']='empty';
        return;
    }
}

function redirect_301($url)
{
    if (empty($url)) {
        error('�� ������� URL');
    }
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: '.$url);
    // logger()->info('Redirect 301 to '.$url);
    exit();
}

function redirect($url)
{
    if (empty($url)) {
        error('�� ������� URL');
    }
    header('Location: '.$url);
    // logger()->info('Redirect to '.$url);
    exit();
}

function rkn_block_js()
{
    global $config;
    // ���� ��� ��� - �� ���������� ��������
    $bots = ['+http://yandex.com/bots', '+http://www.google.com/bot.html', '+//go.mail.ru/help/robots'];
    $ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    $is_bot = false;
    foreach ($bots as $v) {
        if (strpos($ua, $v) !== false) {
            $is_bot = true;
            break;
        }
    }
    if ($is_bot) {
        return;
    }
    // ���� ������������ ������ ��� � ���� - �� ���������� ��������
    $request_uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
    $arr = query_assoc('select for_operators,for_not_ru
		from '.tabname('laravel', 'rkn_block').'
		where domain=\''.$config['domain_origin'].'\'
		and page=\''.mysql_real_escape_string($request_uri).'\'
		and active=\'1\';
		');
    if (! $arr) {
        $arr = query_assoc('select for_operators,for_not_ru
		from '.tabname('laravel', 'rkn_block').'
		where domain=\''.$config['domain_origin'].'\'
		and page is null
		and active=\'1\';
		');
    }
    if (! $arr) {
        return;
    }
    //
    // ���� ����� ��������� �� ���������� ���� �� ������� �� ��������� �������� �� ���������
    if ($arr['for_not_ru'] === '0') {
        // ���� ��� �� ������ - �� ���������� ��������
        $geo_country = get_geoip_country_code(request()->ip());
        if (isset($_GET['country'])) {
            $geo_country = $_GET['country'];
        }
        if ($geo_country != 'RU') {
            return;
        }
    }
    //
    // ���� ����� ��������� �� ���������� ���� ����������, ��������� �������� �� ���������
    if ($arr['for_operators'] === '0') {
        // ���� �������� - �� ���������� ��������
        $operator = check_operator();
        if (in_array($operator, ['megafon', 'beeline', 'mts'])) {
            return;
        }
    }

    //
    // � ��������� ������� - ����������
    return template('rkn_block_js');
}

function redirect_to_mirror($args = [])
{
    global $config;
    $request_uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
    $ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    $is_yandex = false;
    if (strpos($ua, '+http://yandex.com/bots') !== false) {
        $is_yandex = true;
    }
    $is_google = false;
    if (strpos($ua, 'Googlebot') !== false) {
        $is_google = true;
    }
    $arr = query_assoc('select domain,mirror,type
		from '.tabname('laravel', 'redirects').'
		where domain=\''.$config['domain_origin_without_www'].'\' and active=\'1\';'
    );
    // die(var_dump($arr));
    if (! $arr) {
        return;
    }
    if (empty($arr['type'])) {
        error('�� ������� type ���������');
    }
    if ($arr['domain'] == $arr['mirror']) {
        error('������ ��������������� �����');
    }
    if (isset($args['type'])) {
        $type = $args['type'];
    } else {
        $type = $arr['type'];
    }
    if ($type == '301 except yandex and ru') {
        // ���� ��� ������ ��� ������ - �� ���������� 301�
        $geo_country = get_geoip_country_code(request()->ip());
        if (isset($_GET['country'])) {
            $geo_country = $_GET['country'];
        }
        if (($geo_country == 'RU' and ! $is_google) or $is_yandex) {
            // ����� ���������� ��� ����� � �������� ���� �� ��������� ��������
            // logger()->info('�� �������� 301 �������� ��� �� ��� �������');
        } else {
            $type = '301';
        }
    }
    if ($type == '301 except yandex') {
        if (! $is_yandex) {
            $type = '301';
        } else {
            // logger()->info('�� �������� 301 �������� ��� �������');
        }
    }
    if ($type == '301') {
        if ($request_uri != '/robots.txt') {
            redirect_301('http://'.$arr['mirror'].$request_uri);
        }
    }
    if ($type == 'meta for yandex') {
        if ($is_yandex) {
            return '<meta http-equiv="refresh" content="0;URL=\'http://'.$arr['mirror'].$request_uri.'\'">';
        }
    }
}

// TODO ���� ����������� ������ PRIMARY � from_domain (��-�� ���������� �����) �� ����� ������������ �������, �.�. ����� ������������ ��������� �����
function change_config_domain()
{
    global $config;
    $arr = query_assoc('select to_domain,for_country
		from '.tabname('laravel', 'change_domain').'
		where from_domain=\''.$config['domain'].'\' and active=\'1\';'
    );
    $by_geo = false;
    if ($arr) {
        // ���� ����� �������� ������ ��� ������������ ������, �� ��������� ��������
        if (! empty($arr['for_country'])) {
            // $geo_country=get_geoip_country_code(request()->ip());
            $geo_country = empty($_GET['geo_country']) ? get_geoip_country_code(request()->ip()) : $_GET['geo_country'];
            // ���� ��� ���� ������ �� ����� ������, �� ��������� �������
            if ($geo_country != $arr['for_country']) {
                return;
            }
            $by_geo = true;
        }
        $config['domain'] = $arr['to_domain'];
        if (! $by_geo) {
            // �������
            // ���� ����� �������� �� ������, �� �� ��������������, ����� ����� ����������� �������� � ���������
            $config['domain_changed_correct'] = $arr['to_domain'];
        }
    }
}

function meta_yandex_redirect_to_mirror()
{
    return redirect_to_mirror(['type' => 'meta for yandex']);
}

function set_timezone($timezone = '+3')
{
    // date_default_timezone_set('Europe/Moscow');
    query('SET SESSION time_zone=\''.$timezone.':00\'');
}

function set_timezone_alt($timezone)
{
    if (empty($timezone)) {
        error('�� �������� ��������� ����');
    }
    query('SET SESSION time_zone=\''.$timezone.'\'');
}

function get_timezone()
{
    $arr = query_assoc('select @@session.time_zone as time_zone;');

    return $arr['time_zone'];
}

// ���������/�������� ����
function switch_engine($value = 'on')
{
    $value = in_array($value, ['on', 'off']) ? $value : 'on';
    // pre($value);
    logger()->info('����������� ������ � ���������: '.$value);
    query('update '.tabname('engine', 'config').' set value=\''.$value.'\' where property=\'status\';');
    // switch_engine('on');
}

function classAutoloader($type = null)
{
    spl_autoload_register(function ($class_name) use ($type) {
        global $config;
        $class_name = str_replace('MegawebV1\\', '', $class_name);
        $filePath = $config['path']['server'].'types/'.$type.'/inc/class/'.$class_name.'.php';
        // dump($filePath);
        if (file_exists($filePath)) {
            // dump('include '.$filePath);
            include $filePath;
        }
    });
}

// ������������� ���������� � ����������� � ��
function init()
{
    /* ������ �������� ���������� �� � ���� �� cp1251 */
    DB::connection(config('database.default'))->statement("SET NAMES 'cp1251'");
    DB::connection(config('cache.stores.database.connection'))->statement("SET NAMES 'cp1251'");
    global $config, $db;
    // ���������� ��������� �����
    $config['time_start'] = show_time();
    // ������������� htmlspecialchars
    if (! defined('ENT_SUBSTITUTE')) {
        define('ENT_SUBSTITUTE', '0');
    }
    // ������ ����������
    $config['path']['server'] = $_SERVER['DOCUMENT_ROOT'].'/';
    $config['path']['sessions'] = $config['path']['server'].'types/engine/sessions';
    $config['path']['engine'] = $config['path']['server'].'types/engine/template/';
    // ������������ �������
    classAutoLoader('engine');
    // spl_autoload_register(function ($class_name) {
    // 	global $config;
    // 	$filePath=$config['path']['server'].'types/engine/inc/class/'.$class_name.'.php';
    // 	if (file_exists($filePath)) {include $filePath;};
    // });
    db_connect();
    // ��������� �������
    readdb(tabname('engine', 'config'), 'property');
    // ��������� PHP
    if ($db[tabname('engine', 'config')]['property']['display_errors']['value'] == 'on') {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ini_set('memory_limit', '64M');
        // set_error_handler('mgw_php_log');
    } else {
        ini_set('display_errors', 0);
        error_reporting(E_ALL);
        set_error_handler('\\MegawebV1\\mgw_php_log');
    }
    date_default_timezone_set('Europe/Moscow');
    query('SET SESSION time_zone=\''.$db[tabname('engine', 'config')]['property']['time_difference']['value'].':00\'');
    // �������� �������� ip ����� ��� �������������
    // check_db(tabname('engine'),engine_db());
    // ������� ��� ���� URL
    // engine_rewrite_rules();
    // ���������� �����
    $exphost = explode(':', $_SERVER['HTTP_HOST']);
    $config['domain_origin'] = strtolower($exphost[0]);
    $config['domain'] = strtolower(str_replace('www.', '', $exphost[0]));
    $config['domain_origin_without_www'] = $config['domain'];
    $config['domain_changed_correct'] = $config['domain'];
    // ������� ������
    if (preg_match('/local\.([\w\.]+)/', $config['domain'])) {
        $config['domain'] = preg_replace('/local\.([\w\.]+)/', '\\1', $config['domain']);
    }
    // addVisit();
    redirect_to_mirror();
    change_config_domain();
    // pre($db);
    // ��������� �� �������� �� ������
    if ($db[tabname('engine', 'config')]['property']['status']['value'] == 'off') {
        error('���� �������� ��������', '', 0, 503);
    }
    // ��� ���� ��������
    if ($db[tabname('engine', 'config')]['property']['log_queries']['value'] == 'on') {
        logger()->info('');
    }
    // ��������� ����� �� ����������� � ����
    $arr = query_assoc('select domain,type,status,www,charset,source_charset,mysql_charset,
		mobile_version,lang,rewrite_function,additional_path,only_https,`on`
		from '.tabname('engine', 'domains').' where domain=\''.$config['domain'].'\'');
    if (empty($arr)) {
        error('����� '.$config['domain'].' �� �������������.');
    }
    // ��������� �� �������� �� ����
    if ($arr['status'] == 'off') {
        error('���� �������� ��������', '', 0, 503);
    }
    if (! $arr['on']) {
        error('���� �������� ��������', '', 0, 503);
    }
    // ���� ������ empty
    if ($arr['status'] == 'empty') {
        exit(template('empty'));
    }
    if (empty($arr['type']) == 1) {
        error('����������� ���');
    }
    $config['charset'] = $arr['charset'];
    $config['source_charset'] = $arr['source_charset'];
    $config['lang'] = $arr['lang'];
    $config['rewrite_function'] = $arr['rewrite_function'];
    $config['additional_path'] = $arr['additional_path'];
    $config['mysql_charset'] = $arr['mysql_charset'];
    $config['protocol'] = empty($_SERVER['HTTP_X_FORWARDED_PROTO']) ? 'http' : $_SERVER['HTTP_X_FORWARDED_PROTO'];
    // pre($config);
    mysql_set_charset($config['mysql_charset']);
    header('Content-Type: text/html; charset='.$config['charset']);
    // �������� �� www
    if ($arr['www'] == 'off') {
        // pre($config);
        // die($config['domain_origin'].'|www.'.$config['domain']);
        if ($config['domain_origin'] == 'www.'.$config['domain']) {
            redirect_301($config['protocol'].'://'.$config['domain'].$_SERVER['REQUEST_URI']);
        }
    }
    if ($arr['www'] == 'www_only') {
        if ($config['domain_origin'] == $config['domain']) {
            redirect_301($config['protocol'].'://www.'.$config['domain'].$_SERVER['REQUEST_URI']);
        }
    }
    // if (isset($_GET['mmnas'])) {
    // pre($_SERVER);
    // };
    if ($config['protocol'] == 'http' and $arr['only_https']) {
        redirect_301('https://'.$config['domain_origin'].$_SERVER['REQUEST_URI']);
    }
    $config['type'] = $arr['type'];
    $config['path']['type'] = $config['path']['server'].'types/'.$config['type'].'/template/';
    $additional_path = empty($config['additional_path']) ? '' : $config['additional_path'].'/';
    // if (isset($_GET['mmnas'])) {pre($config);};
    $config['path']['site'] = $config['path']['server'].'sites/'.$additional_path.$config['domain'].'/';
    $config['Mobile_Detect'] = new Mobile_Detect;
    if ($arr['mobile_version'] == 'on') {
        // setcookie(name)
        // echo '<pre>'; var_dump($_COOKIE); echo '</pre>';
        $mobile_version = 0;
        if (isset($_COOKIE['mobile_version']) == 1) {
            if ($_COOKIE['mobile_version'] == 0) {
                $mobile_version = 0;
            }
            if ($_COOKIE['mobile_version'] == 1) {
                $mobile_version = 1;
            }
        }
        if (isset($_COOKIE['mobile_version']) == 0) {
            // if (detect_mobile_browser($_SERVER['HTTP_USER_AGENT'])==true) {$mobile_version=1;};
            if ($config['Mobile_Detect']->isMobile() or $config['Mobile_Detect']->isTablet()) {
                $mobile_version = 1;
            }
        }
        // die('mobile_version: '.$mobile_version);
        if ($mobile_version == 1) {
            $config['path']['site'] = $config['path']['site'].'mobile_version/';
        }
    }
    // pre($_SERVER);
    // ���� �� ������� query �� ����������� page �� $_GET['page']

    if (isset($_SERVER['REQUEST_URI'])) {
        $uri = parse_url($_SERVER['REQUEST_URI']);
        if (isset($uri['path'])) {
            $_GET['query'] = substr($uri['path'], 1);
        }
    }

    if (empty($_GET['query'])) {
        if (empty($_GET['page'])) {
            $config['vars']['page'] = 'index';
        }
        if (! empty($_GET['page'])) {
            $config['vars']['page'] = braces2square(checkstr($_GET['page']));
        }
    }
    // ������ ������� ��� ������
    if ($config['domain'] == 'xrest.net') {
        if ($_SERVER['REQUEST_URI'] == '/index.php') {
            redirect_301('/');
        }
        if (isset($_GET['action']) and $_GET['action'] == 'tags') {
            $config['vars']['page'] = 'tags_videos';
            if (empty($_GET['tag'])) {
                $_GET['tag'] = '';
            }
            $_GET['tag_ru'] = checkstr($_GET['tag']);
            // ��������� �������� �� ������������� ����
            if (isset($_GET['page'])) {
                $_GET['p'] = $_GET['page'];
            } else {
                $_GET['p'] = 1;
            }
            if (! isset($_GET['sort'])) {
                $_GET['sort'] = 'mr';
            }
        }
    }
    // ���� ������� query, �� �� $_GET['page'] ��� �� �������, ��������� URL
    if (! empty($_GET['query'])) {
        $_GET['query'] = braces2square($_GET['query']);
        engine_rewrite_query();
    }

    // ���������� ������� ��� ����� ���� ��� ����������
    $sitefunc = $config['path']['server'].'sites/'.$additional_path.$config['domain'].'/inc/func.php';
    if (file_exists($sitefunc)) {
        include $sitefunc;
    }
    // ���������� ������� ���� � ��������� ��� ������� �������������
    if ($config['type'] != 'engine') {
        // ������������ ������� ��� ����
        classAutoLoader($config['type']);
        $typefunc = $config['path']['server'].'types/'.$config['type'].'/inc/func.php';
        if (! file_exists($typefunc)) {
            error('��� �����������');
        }
        include $typefunc;
        $func_type_init = '\MegawebV1\\'.$config['type'].'_init';
        if (function_exists($func_type_init) == 0) {
            error('����������� ������� �������������');
        }
        $func_type_init();
    }
    // ���� page ��� �� ��������� � ������ rewrite_function ��� ������, �� ��������� ���
    if (empty($config['vars']['page']) and ! empty($config['rewrite_function'])) {
        if (empty($_GET['query'])) {
            error('�� ������� ������');
        }
        $rewrite_function = '\MegawebV1\\'.$config['rewrite_function'];
        if (! function_exists($rewrite_function)) {
            error('������� �� ����������', 'Rewrite ������� �� ����������');
        }
        $rewrite_function($_GET['query']);
    }
    // ���� �������� ��� � �� ���������� �� �������� �� ������
    if (empty($config['vars']['page'])) {
        // logger()->info('�������� �������� 404','','db','404');
        logs_alt('�������� �������� 404', ['type' => '404']);
        $config['vars']['page'] = '404';
    }
    // ��������� cronjob
    // cronjob(tabname('engine','crons'));
}

function addVisit()
{
    global $config;
    $query = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $host = $_SERVER['HTTP_HOST'] ?? '';
    $userAgent = $_SERVER['HTTP_USER_AGENT'] ?? '';
    $referer = $_SERVER['HTTP_REFERER'] ?? '';
    $get = serialize($_GET);
    $post = serialize($_POST);
    $cookie = serialize($_COOKIE);
    $ip = request()->ip();
    $domain = $config['domain'] ?? '';
    query('insert into '.tabname('engine', 'visits').' set
		host = \''.mysql_real_escape_string(mb_strimwidth($host, 0, 255)).'\',
		domain = \''.mysql_real_escape_string(mb_strimwidth($domain, 0, 255)).'\',
		`query` = \''.mysql_real_escape_string(mb_strimwidth($query, 0, 255)).'\',
		userAgent = \''.mysql_real_escape_string(mb_strimwidth($userAgent, 0, 255)).'\',
		referer = \''.mysql_real_escape_string(mb_strimwidth($referer, 0, 255)).'\',
		`get` = \''.mysql_real_escape_string(mb_strimwidth($get, 0, 255)).'\',
		post = \''.mysql_real_escape_string(mb_strimwidth($post, 0, 255)).'\',
		cookie = \''.mysql_real_escape_string(mb_strimwidth($cookie, 0, 255)).'\',
		ip = \''.mysql_real_escape_string($ip).'\'
		;');
}

// ������� ������� �� �����
function print_page()
{
    global $config;
    $return = template($config['vars']['page']);
    if ($config['charset'] != $config['source_charset']) {
        $return = iconv($config['source_charset'], $config['charset'], $return);
    }

    return $return.(isset($_GET['mmnas']) ? template('admin/testing_info') : '');
}

// ����� ���������� �������.
function runing_time()
{
    global $config;
    $time_end = show_time();
    $time = round($time_end - $config['time_start'], 4);

    return $time;
}

// ������ ������� �����
function show_time()
{
    [$msec, $sec] = explode(' ', microtime());
    $mtime = $sec + $msec;

    return $mtime;
}

// ���������� ��� ������� � ���������� $db[�������� �������]
function readdb($table, $key)
{
    global $db;
    if (isset($db[$table][$key]) == 0) {
        $data = query('select * from '.$table, '������ ��� readdb');
        if ($key == 'nokey') {
            while ($row = mysql_fetch_assoc($data)) {
                $db[$table][$key][] = $row;
            }
        } else {
            while ($row = mysql_fetch_assoc($data)) {
                $db[$table][$key][$row[$key]] = $row;
            }
        }
    }
}

// ���������� ��������� ������� � ������
// ���� ������� unique_key=false �� � ����� ����� ����� ���������� ��������� � ����� �� ������
function readdata($data, $key, $unique_key = true)
{
    $arr = [];
    if ($key == 'nokey') {
        while ($row = mysql_fetch_assoc($data)) {
            $arr[] = $row;
        }
    } else {
        while ($row = mysql_fetch_assoc($data)) {
            if (isset($row[$key]) == 0) {
                error('���� '.$key.' �� ����������');
            }
            if ($unique_key) {
                $arr[$row[$key]] = $row;
            } else {
                $arr[$row[$key]][] = $row;
            }
        }
    }

    return $arr;
}

// ��������� ��������� �� ���������� ������ �� $tables � ���� $name, ���� ���, �� ��������� ��� ������� �� $tables
function check_db($name, $tables)
{
    query('create database if not exists '.$name, '�������� ��');
    $data = query('show tables from '.$name);
    $exist = readdata($data, 'Tables_in_'.$name);
    if (count($tables) != count($exist)) {
        foreach ($tables as $k => $v) {
            if (isset($exist[$k]) == 0) {
                query($v, '�������� �������');
            }
        }
        logger()->info('��������� �������� ������ � �� '.$name);
    }
}
