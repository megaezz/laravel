<?php

namespace MegawebV1;

/*
 ���������� ����� ��������
 ������ �������������
 echo num2str('2133.93');
*/

function num2str($num, $args = [])
{
    $args['translite'] = empty($args['translite']) ? null : $args['translite'];
    $args['currency'] = empty($args['currency']) ? 'rub' : $args['currency'];
    $args['is_kop'] = isset($args['is_kop']) ? $args['is_kop'] : true;

    $defaultTranslite = [
        'null' => '����',
        'a1' => [0 => '', 1 => '����', 2 => '���', 3 => '���', 4 => '������', 5 => '����', 6 => '�����', 7 => '����', 8 => '������', 9 => '������'],
        'a2' => [0 => '', 1 => '����', 2 => '���', 3 => '���', 4 => '������', 5 => '����', 6 => '�����', 7 => '����', 8 => '������', 9 => '������'],
        'a10' => [0 => '������', 1 => '�����������', 2 => '����������', 3 => '����������', 4 => '������������', 5 => '����������', 6 => '�����������', 7 => '����������', 8 => '������������', 9 => '������������'],
        'a20' => [2 => '��������', 3 => '��������', 4 => '�����', 5 => '���������', 6 => '����������', 7 => '���������', 8 => '�����������', 9 => '���������'],
        'a100' => [1 => '���', 2 => '������', 3 => '������', 4 => '���������', 5 => '�������', 6 => '��������', 7 => '�������', 8 => '���������', 9 => '���������'],
        'u3' => ['������', '������', '�����'],
        'u2' => ['�������', '��������', '���������'],
        'u1' => ['��������', '���������', '����������'],
    ];
    $arr_currency['rub'] = [
        'uc' => ['�������', '�������', '������'],
        'ur' => ['�����', '�����', '������'],
    ];
    $arr_currency['kzt'] = [
        'uc' => ['����', '�����', '������'],
        'ur' => ['�����', '�����', '�����'],
    ];
    $arr_currency['no'] = [
        'uc' => ['', '', ''],
        'ur' => ['', '', ''],
    ];
    $translite = is_null($args['translite']) ? $defaultTranslite : $args['translite'];
    $translite = array_merge($translite, $arr_currency[$args['currency']]);
    [$rub, $kop] = explode('.', sprintf('%015.2f', floatval($num)));
    $out = [];
    if (intval($rub) > 0) {

        // ��������� ����� �� ��� �������
        $cRub = str_split($rub, 3);

        foreach ($cRub as $uk => $v) {
            if (! intval($v)) {
                continue;
            }
            [$i1, $i2, $i3] = array_map('intval', str_split($v, 1));

            $out[] = isset($translite['a100'][$i1]) ? $translite['a100'][$i1] : ''; // 1xx-9xx
            $ax = ($uk + 1 == 3) ? 'a2' : 'a1';
            if ($i2 > 1) {
                $out[] = $translite['a20'][$i2].' '.$translite[$ax][$i3];
            } // 20-99
            else {
                $out[] = $i2 > 0 ? $translite['a10'][$i3] : $translite[$ax][$i3];
            } // 10-19 | 1-9

            if (count($cRub) > $uk + 1) {
                $uName = $translite['u'.($uk + 1)];
                $out[] = morph($v, $uName);
            }
        }
    } else {
        $out[] = $translite['null'];
    }

    // ���������� �������� "�����"
    $out[] = morph(intval($rub), $translite['ur']); // rub
    // ���������� �������� "�������"
    if ($args['is_kop']) {
        $out[] = $kop.' '.morph($kop, $translite['uc']); // kop
    }

    // ���������� ��c��� � ������
    $str = implode(' ', $out);

    // ������� ������ ������� � ���������� ���������
    return trim(preg_replace('/ {2,}/', ' ', $str));
}

/**
 * �������� ����������
 */
function morph($number, $titles)
{
    $cases = [2, 0, 1, 1, 1, 2];

    return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
}
