<?php

include $_SERVER['DOCUMENT_ROOT'].'/types/engine/inc/geoip/geoip.php';

function geoip_country_code_client()
{
    // $client_ip=isset($_SERVER['HTTP_CF_CONNECTING_IP'])?$_SERVER['HTTP_CF_CONNECTING_IP']:request()->ip();
    $client_ip = request()->ip();
    $geo_country = get_geoip_country_code($client_ip);

    return $geo_country.'<br/>'.$client_ip;
}

function get_geoip_country_code($ip)
{
    $info = geoip_info($ip);

    // die($info['code']);
    // $info['code']=empty($_GET['geo_country'])?'':$_GET['geo_country'];
    return $info['code'];
}

function get_geoip_country_name($ip)
{
    $info = geoip_info($ip);

    return $info['name'];
}

function geoip_info($ip)
{
    global $config;
    $gi = geoip_open($config['path']['server'].'types/engine/inc/geoip/GeoIPv6.dat', GEOIP_STANDARD);
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
        $ip = '::'.$ip;
    }
    $arr['code'] = geoip_country_code_by_addr_v6($gi, $ip);
    $arr['name'] = geoip_country_name_by_addr_v6($gi, $ip);
    geoip_close($gi);
    if (empty($arr['code'])) {
        $arr['code'] = 'n/a';
        $arr['name'] = 'n/a';
        logger()->info('IP '.$ip.' не найден в базе geoip');
    }

    return $arr;
}
