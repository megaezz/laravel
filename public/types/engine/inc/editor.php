<?php

namespace MegawebV1;

function editor_delete_file()
{
    $path = empty($_GET['path']) ? error('Path is required') : urldecode($_GET['path']);
    editor_check_basedir(['path' => editor_get_full_path($path)]);
    if (isset($_GET['confirm'])) {
        $status = true;
        $status = unlink(editor_get_full_path($path));
        if ($status) {
            $return = '������ ����: '.$path;
        } else {
            $return = '�� ������� �������: '.$path;
        }
    } else {
        alert('������� '.$path.' ?<br/><a href="/?page=admin/editor/delete&amp;path='.$path.'&confirm">�����������</a>');
    }
    alert($return.'<p><a href="/?path='.pathinfo($path, PATHINFO_DIRNAME).'">'.pathinfo($path, PATHINFO_DIRNAME).'</a></p>');
}

function submit_editor_create_file()
{
    // pre(var_dump($_POST));
    global $config;
    // pre($config);
    $source = empty($_POST['source']) ? error('Source file is required') : editor_get_full_path('types/engine/template/admin/editor/sources/'.$_POST['source']);
    // pre(var_dump($source));
    $source_extension = pathinfo($source, PATHINFO_EXTENSION);
    // pre(var_dump($source_extension));
    $target_folder = empty($_POST['target_folder']) ? editor_get_user_basedir() : editor_get_full_path($_POST['target_folder']);
    // pre(var_dump($target_folder));
    $target_name = empty($_POST['target_name']) ? error('Target file name is required') : ($_POST['target_name'].'.'.$source_extension);
    // pre(var_dump($target_name));
    editor_check_basedir(['path' => $target_folder]);
    // pre(var_dump($target_folder));
    copytoserv($source, $target_folder.'/'.$target_name);
    // $short_basedir=editor_get_short_path($target_folder);
    redirect('/?path='.editor_get_short_path($target_folder.'/'.$target_name));
    // alert('������ ���� '.basename($target_name).'<p><a href="/?path='.$short_basedir.'">'.$short_basedir.'</a></p>');
}

function editor_show_path()
{
    $basedir = editor_get_short_path(editor_get_user_basedir());
    $path = empty($_REQUEST['path']) ? $basedir : urldecode($_REQUEST['path']);
    // pre($basedir);
    $last_dir = basename($basedir);
    // pre($last_dir);
    $shortpath = $last_dir.str_replace($basedir, '', $path);
    // pre($shortpath);
    $arr = explode('/', $shortpath);
    // if (empty($arr)) {$arr[0]='/';};
    // unset($arr[0]);
    // pre($arr);
    $list = '';
    $path_str = $basedir;
    $i = 0;
    foreach ($arr as $v) {
        if ($i != 0) {
            $path_str .= '/'.$v;
        }
        // pre($path_str);
        $is_active = ($path == $path_str) ? true : false;
        if ($is_active) {
            $list .= '<li class="breadcrumb-item active">'.$v.' <a href="/?page=admin/editor/delete&amp;path='.urlencode($path).'"><i class="far fa-trash-alt"></i></a></li>';
        } else {
            $list .= '<li class="breadcrumb-item"><a href="/?path='.urlencode($path_str).'">'.$v.'</a></li>';
        }
        $i++;
    }
    // pre($arr);
    $shortpath_end = basename($path);
    $shortpath_start = str_replace($shortpath_end, '', $shortpath);

    return $list;
}

function editor_get_user_basedir()
{
    $user_info = engine_user_info();
    $arr = query_assoc('select path from '.typetab('editor').' where login=\''.$user_info['login'].'\' limit 1;');
    if (! $arr['path']) {
        return false;
    }

    return editor_get_full_path($arr['path']);
}

function editor_get_full_path($path = '')
{
    global $config;

    // pre($config['path']['server'].$path);
    return realpath($config['path']['server'].$path);
}

function editor_get_short_path($path = '')
{
    global $config;
    // pre($path);
    // pre($config['path']['server']);
    // pre($path);
    $short_path = str_replace(realpath($config['path']['server']), '', $path);

    // if ($short_path===$path) {$short_path=editor_get_short_path(editor_get_user_basedir());};
    // pre($short_path);
    return $short_path;
}
// pre(var_dump(strstr('/','/sites',true)));
// pre(realpath('/usr/'));

function editor_check_basedir($args = [])
{
    // global $config;
    $user_info = engine_user_info();
    $path = empty($args['path']) ? (empty($_REQUEST['path']) ? editor_get_user_basedir() : editor_get_full_path(urldecode($_REQUEST['path']))) : $args['path'];
    $arr = query_arr('select path from '.typetab('editor').' where login=\''.$user_info['login'].'\';');
    $is_ok = false;
    foreach ($arr as $v) {
        // ��������� ������������� ���� � �������� ���������� ��� ������������ (�����). � ��� ����� ������ ���� �����.
        $strstr = strstr($path, editor_get_full_path($v['path']), true);
        if ($strstr === '') {
            $is_ok = true;

            continue;
        }
    }
    if (! $is_ok) {
        alert('Path is out of privileges.');
    }
    // if (!strstr($path,$basedir)) {};
}

function editor_edit_page_submit()
{
    $text = isset($_POST['text']) ? $_POST['text'] : error('No content');
    $path = isset($_POST['path']) ? editor_get_full_path($_POST['path']) : error('No path');
    file_put_contents($path, $text);
    // pre($text);
    // alert('Done');
    redirect(referer_url());
}

function editor_page_text()
{
    global $config;
    $path = empty($_REQUEST['path']) ? editor_get_user_basedir() : editor_get_full_path(urldecode($_REQUEST['path']));
    if (is_dir($path)) {
        return;
    }
    // die(mb_encode_numericentity(file_get_contents($path)));
    temp_var('text', braces2square_alt(htmlspecialchars(file_get_contents($path), ENT_QUOTES, 'cp1251')));
    // temp_var('filename',basename($path));
    temp_var('path', editor_get_short_path($path));

    return template('admin/editor/edit_form');
}

function editor_dir_list()
{
    // header('X-XSS-Protection: 0');
    // pre('ok');
    $path_origin = empty($_GET['path']) ? editor_get_user_basedir() : editor_get_full_path(urldecode($_GET['path']));
    // pre(editor_get_full_path(urldecode($_GET['path'])));
    // pre(var_dump($path_origin));
    $path = is_dir($path_origin) ? $path_origin : pathinfo($path_origin, PATHINFO_DIRNAME);
    // pre($path);
    $arr = scandir($path);
    unset($arr[0]);
    $tree['folders'] = [];
    $tree['files'] = [];
    // pre(var_dump($arr));
    foreach ($arr as $v) {
        // pre($path.$v);
        $realpath = realpath($path.'/'.$v);
        $a = ['name' => $v, 'path' => $realpath];
        if (is_dir($realpath)) {
            $tree['folders'][] = $a;
        } else {
            $tree['files'][] = $a;
        }
    }
    // pre($tree);
    $list = '';
    foreach ($tree['folders'] as $v) {
        temp_var('style_selected', ($v['path'] == $path_origin) ? 'style="font-weight: bold;"' : '');
        temp_var('path', urlencode(editor_get_short_path($v['path'])));
        temp_var('name', mb_strimwidth($v['name'], 0, 19, '...'));
        $list .= template('admin/editor/list_folder');
    }
    foreach ($tree['files'] as $v) {
        temp_var('style_selected', ($v['path'] == $path_origin) ? 'style="font-weight: bold;"' : '');
        temp_var('path', urlencode(editor_get_short_path($v['path'])));
        temp_var('name', mb_strimwidth($v['name'], 0, 19, '...'));
        $list .= template('admin/editor/list_file');
    }

    return $list;
}
