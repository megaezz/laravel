<?php

namespace MegawebV1;

// ������ ������ � ��������������� ������� ������� �� �������� ����� ����� get_object_vars � ����� ksort � ������������ ��� �� �� ������� � �� ����� ������� � �������� ������������� �������, ���� �� ���� ��������� ��� �������, �.�. ���� ������ ������� - ������ ��������� �������� �� ��������� � �������, ����� ��� �������� ����� ������ � ��� �������, ������� ������ � __construct. ����������� � ����� ������� �������
class cache
{
    public $seconds;

    public $args;

    public $type;

    public $startTime;

    public $useDB;

    public $engineType;

    public $domain;

    public $function;

    public $endTime;

    public $timeRange;

    public $object;

    public function __construct()
    {
        // ������ ���������� ��� type � domain
        global $config;
        $this->seconds = 43200;
        $this->args = [];
        $this->type = 'function';
        $this->startTime = runing_time();
        $this->useDB = true;
        $this->setEngineType($config['type']);
        $this->setDomain($config['domain']);
    }

    public function resetDomain()
    {
        // if (!$this->domain) {error('Set domain for cache deleting');};
        query('update '.tabname('engine', 'cacheObject').' set
			reset=1	where domain=\''.$this->domain.'\'
			;');
        if (! mysql_affected_rows()) {
            return false;
        }
        logger()->info('Reset all cache for domain: '.$this->domain);

        return true;

    }

    public function reset($function = '', $args = [])
    {
        if (! empty($function)) {
            $this->setFunction($function);
        }
        if (! empty($args)) {
            $this->setArgs($args);
        }
        $hash = $this->getHash();
        query('update '.tabname('engine', 'cacheObject').' set
			reset=1	where hash=\''.$hash.'\' and domain=\''.$this->domain.'\'
			;');
        if (! mysql_affected_rows()) {
            return false;
        }
        logger()->info('Reset cache '.$hash.'. Domain: '.$this->domain);

        return true;
    }

    public function result($function = '', $args = [], $seconds = null)
    {
        // ������ ��� ������ � ��������� ����
        global $config;
        $result = null;
        $is_result = false;
        $action = 'select';
        if (! empty($function)) {
            $this->setFunction($function);
        }
        if (! empty($args)) {
            $this->setArgs($args);
        }
        if (isset($seconds)) {
            $this->setSeconds($seconds);
        }
        $hash = $this->getHash();
        // ��������� ���� �� ������ � ��������� ����
        if (isset($config['cacheObject'][$hash])) {
            // pre('local');
            $result = $config['cacheObject'][$hash];

            return $result;
        }
        // ���� ����� ������������ �� �� ��������� ���� �� ��� ������ ������
        if ($this->useDB) {
            $arr = query_assoc('
			select result,unix_timestamp(date) as date,reset,date as date4logs
			from '.tabname('engine', 'cacheObject').'
			where hash=\''.$hash.'\' and domain=\''.$this->domain.'\'
			;');
            if ($arr) {
                $date = getdate();
                // die('������: '.$date[0].', � ��: '.$arr['date'].', ������: '.$seconds.' ������: '.($date[0]-$arr['date']));
                if ((($date[0] - $arr['date']) >= $this->seconds) or $arr['reset']) {
                    $action = 'update';
                } else {
                    $result = unserialize($arr['result']);
                    $is_result = true;
                }
            } else {
                $action = 'insert';
            }
        }
        // ���� ���������� �� ���� �� � ��������� ���� �� � ��, ���� � �� �� �������, �� ��������� ������� ��� �����
        if (! $is_result) {
            $result = $this->run();
            if ($this->function == 'getSame') {
                logger()->info('��������� '.$this->type.' '.$this->function.' ������� ���� '.(empty($arr['date4logs']) ? '�����������' : $arr['date4logs']));
            }
        }
        $this->endTime = runing_time();
        $this->timeRange = $this->endTime - $this->startTime;
        if ($this->useDB) {
            $serialized_result = serialize($result);
            if ($action == 'insert') {
                // ignore �.�. ���������� ������ ��������� �������
                query('insert ignore into '.tabname('engine', 'cacheObject').' set
					domain=\''.$this->domain.'\',
					hash=\''.$hash.'\',
					time=\''.$this->timeRange.'\',
					result=\''.mysql_real_escape_string($serialized_result).'\'
					;');
            }
            if ($action == 'update') {
                // ��������� ����, �.�. ���� ��������� �� ��������� �� mysql ������� ������ ����
                query('update '.tabname('engine', 'cacheObject').' set
					result=\''.mysql_real_escape_string($serialized_result).'\',
					reset=0, time=\''.$this->timeRange.'\', date=current_timestamp
					where hash=\''.$hash.'\' and domain=\''.$this->domain.'\'
					;');
            }
        }
        $config['cacheObject'][$hash] = $result;

        return $result;
    }

    public function setObject($object = null)
    {
        // ���� ������ ������ ������ ��������� ��� ������
        $this->setType('object');
        $this->object = is_object($object) ? $object : error('Object is required to create cache');

    }

    public function setEngineType($type = '')
    {
        $this->engineType = empty($type) ? error('Set engine type') : $type;
    }

    public function setDomain($domain = '')
    {
        $this->domain = empty($domain) ? error('Set domain') : $domain;
    }

    public function setSeconds($seconds = null)
    {
        if (! isset($seconds)) {
            error('Seconds is required');
        }
        if ($seconds == 0) {
            $this->useDB = false;
        }
        $this->seconds = $seconds;
    }

    public function setType($type = '')
    {
        $types = ['object', 'function'];
        if (! in_array($type, $types)) {
            error('Wrong cache type');
        }
        $this->type = $type;
    }

    public function getHash()
    {
        if (empty($this->function)) {
            error('Set function name');
        }
        if (empty($this->type)) {
            error('Set type');
        }
        if (empty($this->engineType)) {
            error('Set engine type');
        }
        if ($this->type == 'function') {
            $str = $this->engineType.$this->function.serialize($this->args);
        }
        if ($this->type == 'object') {
            $str = $this->engineType.serialize($this->object).$this->function.serialize($this->args);
        }

        // echo $str;
        // return $str;
        return hash('sha256', $str);
    }

    public function run()
    {
        if (empty($this->type)) {
            error('Set type');
        }
        if (empty($this->function)) {
            error('Set function');
        }
        $function = $this->function;
        if ($this->type == 'function') {
            if (! function_exists($this->function)) {
                error('Function doesn\'t exist');
            }
            $result = $function($this->args);
        }
        if ($this->type == 'object') {
            if (! isset($this->object) or ! is_object($this->object)) {
                error('Wrong object');
            }
            if (! method_exists($this->object, $this->function)) {
                error('Method doesn\'t exist');
            }
            if (! isset($this->object) or ! is_object($this->object)) {
                error('Wrong object');
            }
            $result = $this->object->$function($this->args);
        }

        return $result;
    }

    public function setFunction($function = '')
    {
        $this->function = empty($function) ? error('Specify function name') : $function;
    }

    // ��������� ��� ������� ���� �������� � ��������
    public function setMethod($method = '')
    {
        $this->setFunction($method);
    }

    public function setArgs($args = [])
    {
        ksort($args);
        $this->args = is_array($args) ? $args : error('Arguments should be an array');
    }
}
