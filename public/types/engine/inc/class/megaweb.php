<?php

namespace MegawebV1;

// ��������� ��� �� ������� ��� mysql � php ����������� ���������� ��������� ����
// �������� �� ��� ��� ���� ���������� redirectToMirror � change_config_domain
class megaweb
{
    public function __construct()
    {
        // ���������� ��������� �����
        $this->timeStart = show_time();
        // ������������� htmlspecialchars
        if (! defined('ENT_SUBSTITUTE')) {
            define('ENT_SUBSTITUTE', '0');
        }
        // ������ ����
        $this->serverPath = $_SERVER['DOCUMENT_ROOT'];
        $this->sessionPath = $this->serverPath.'types/engine/sessions';
        $this->enginePath = $this->serverPath.'types/engine';
        // ������������ �������
        classAutoLoader('engine');
        db_connect();
        $data = query('select property,value from '.tabname('engine', 'configNew').';');
        $config = readdata($data, 'property');
        // pre($config);
        $this->authRange = $config['auth_range']['value'];
        $this->status = $config['status']['value'];
        $this->reload = $config['reload']['value'];
        $this->displayErrors = $config['display_errors']['value'];
        $this->logQueries = $config['log_queries']['value'];
        $this->cacheDeleteRange = $config['cache_delete_range']['value'];
        $this->logLongQueries = $config['log_long_queries']['value'];
        $this->encoding = $config['engine_encoding']['value'];
        $this->authSessionTimeout = $config['auth_session_timeout']['value'];
        $this->clientIP = request()->ip();
        // ��������� PHP
        if ($this->displayErrors) {
            ini_set('display_errors', 1);
            error_reporting(E_ALL);
            ini_set('memory_limit', '64M');
        } else {
            ini_set('display_errors', 0);
            error_reporting(E_ALL);
            set_error_handler('mgw_php_log');
        }
        // ��������� ������������ ������� php � mysql
        $arr = query_assoc('select unix_timestamp(current_timestamp) as time;');
        if ($arr['time'] != time()) {
            error('Time dismatch');
        }
        $exphost = explode(':', $_SERVER['HTTP_HOST']);
        $this->requestedDomainOrigin = strtolower($exphost[0]);
        $this->requestedDomain = str_replace('www.', '', $this->requestedDomainOrigin);
        // ������� ������
        if (preg_match('/local\.([\w\.]+)/', $this->requestedDomain)) {
            $config['domain'] = preg_replace('/local\.([\w\.]+)/', '\\1', $this->requestedDomain);
        }
        // redirect_to_mirror();

    }

    // ����������
    private function redirectToMirror()
    {
        global $config;
        $request_uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        $ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $is_yandex = false;
        if (strpos($ua, '+http://yandex.com/bots') !== false) {
            $is_yandex = true;
        }
        $is_google = false;
        if (strpos($ua, 'Googlebot') !== false) {
            $is_google = true;
        }
        $arr = query_assoc('select domain,mirror,type
			from '.tabname('laravel', 'redirects').'
			where domain=\''.$config['domain_origin_without_www'].'\' and active=\'1\';'
        );
        // die(var_dump($arr));
        if (! $arr) {
            return;
        }
        if (empty($arr['type'])) {
            error('�� ������� type ���������');
        }
        if ($arr['domain'] == $arr['mirror']) {
            error('������ ��������������� �����');
        }
        if (isset($args['type'])) {
            $type = $args['type'];
        } else {
            $type = $arr['type'];
        }
        if ($type == '301 except yandex and ru') {
            // ���� ��� ������ ��� ������ - �� ���������� 301�
            $geo_country = get_geoip_country_code(request()->ip());
            if (isset($_GET['country'])) {
                $geo_country = $_GET['country'];
            }
            if (($geo_country == 'RU' and ! $is_google) or $is_yandex) {
                // ����� ���������� ��� ����� � �������� ���� �� ��������� ��������
                // logs('�� �������� 301 �������� ��� �� ��� �������');
            } else {
                $type = '301';
            }
        }
        if ($type == '301 except yandex') {
            if (! $is_yandex) {
                $type = '301';
            } else {
                // logs('�� �������� 301 �������� ��� �������');
            }
        }
        if ($type == '301') {
            if ($request_uri != '/robots.txt') {
                redirect_301('http://'.$arr['mirror'].$request_uri);
            }
        }
        if ($type == 'meta for yandex') {
            if ($is_yandex) {
                return '<meta http-equiv="refresh" content="0;URL=\'http://'.$arr['mirror'].$request_uri.'\'">';
            }
        }
    }

    public static function getDomains()
    {
        // $sql['type']=isset($args['type'])?'type=\''.$args['type'].'\'':'1';
        // $arr=query_arr('select domain from '.tabname('engine','domains').' where '.$sql['type'].' order by domain;');
        // return $arr;
        return query_arr('select domain from '.tabname('engine', 'domains').' order by domain;');
    }
}

function init_old()
{
    global $config, $db;
    // ���������� ��������� �����
    $config['time_start'] = show_time();
    // ������������� htmlspecialchars
    if (! defined('ENT_SUBSTITUTE')) {
        define('ENT_SUBSTITUTE', '0');
    }
    // ������ ����������
    $config['path']['server'] = $_SERVER['DOCUMENT_ROOT'].'/';
    $config['path']['sessions'] = $config['path']['server'].'types/engine/sessions';
    $config['path']['engine'] = $config['path']['server'].'types/engine/template/';
    // ������������ �������
    classAutoLoader('engine');
    // spl_autoload_register(function ($class_name) {
    // 	global $config;
    // 	$filePath=$config['path']['server'].'types/engine/inc/class/'.$class_name.'.php';
    // 	if (file_exists($filePath)) {include $filePath;};
    // });
    db_connect();
    // ��������� �������
    readdb(tabname('engine', 'config'), 'property');
    // ��������� PHP
    if ($db[tabname('engine', 'config')]['property']['display_errors']['value'] == 'on') {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        ini_set('memory_limit', '64M');
        // set_error_handler('mgw_php_log');
    } else {
        ini_set('display_errors', 0);
        error_reporting(E_ALL);
        set_error_handler('mgw_php_log');
    }
    date_default_timezone_set('Europe/Moscow');
    query('SET SESSION time_zone=\''.$db[tabname('engine', 'config')]['property']['time_difference']['value'].':00\'');
    // check_db(tabname('engine'),engine_db());
    // ������� ��� ���� URL
    // engine_rewrite_rules();
    // ���������� �����
    $exphost = explode(':', $_SERVER['HTTP_HOST']);
    $config['domain_origin'] = strtolower($exphost[0]);
    $config['domain'] = strtolower(str_replace('www.', '', $exphost[0]));
    $config['domain_origin_without_www'] = $config['domain'];
    $config['domain_changed_correct'] = $config['domain'];
    // ������� ������
    if (preg_match('/local\.([\w\.]+)/', $config['domain'])) {
        $config['domain'] = preg_replace('/local\.([\w\.]+)/', '\\1', $config['domain']);
    }
    redirect_to_mirror();
    change_config_domain();
    // pre($db);
    // ��������� �� �������� �� ������
    if ($db[tabname('engine', 'config')]['property']['status']['value'] == 'off') {
        error('���� �������� ��������', '', 0, 503);
    }
    // ��� ���� ��������
    if ($db[tabname('engine', 'config')]['property']['log_queries']['value'] == 'on') {
        logger()->info('');
    }
    // ��������� ����� �� ����������� � ����
    $arr = query_assoc('select domain,type,status,www,charset,source_charset,mysql_charset,
		mobile_version,lang,rewrite_function,additional_path,only_https,`on`
		from '.tabname('engine', 'domains').' where domain=\''.$config['domain'].'\'');
    if (empty($arr)) {
        error('����� '.$config['domain'].' �� �������������.');
    }
    // ��������� �� �������� �� ����
    if ($arr['status'] == 'off') {
        error('���� �������� ��������', '', 0, 503);
    }
    if (! $arr['on']) {
        error('���� �������� ��������', '', 0, 503);
    }
    // ���� ������ empty
    if ($arr['status'] == 'empty') {
        exit(template('empty'));
    }
    if (empty($arr['type']) == 1) {
        error('����������� ���');
    }
    $config['charset'] = $arr['charset'];
    $config['source_charset'] = $arr['source_charset'];
    $config['lang'] = $arr['lang'];
    $config['rewrite_function'] = $arr['rewrite_function'];
    $config['additional_path'] = $arr['additional_path'];
    $config['mysql_charset'] = $arr['mysql_charset'];
    $config['protocol'] = empty($_SERVER['HTTP_X_FORWARDED_PROTO']) ? 'http' : $_SERVER['HTTP_X_FORWARDED_PROTO'];
    // pre($config);
    mysql_set_charset($config['mysql_charset']);
    header('Content-Type: text/html; charset='.$config['charset']);
    // �������� �� www
    if ($arr['www'] == 'off') {
        // pre($config);
        // die($config['domain_origin'].'|www.'.$config['domain']);
        if ($config['domain_origin'] == 'www.'.$config['domain']) {
            redirect_301($config['protocol'].'://'.$config['domain'].$_SERVER['REQUEST_URI']);
        }
    }
    if ($arr['www'] == 'www_only') {
        if ($config['domain_origin'] == $config['domain']) {
            redirect_301($config['protocol'].'://www.'.$config['domain'].$_SERVER['REQUEST_URI']);
        }
    }
    // if (isset($_GET['mmnas'])) {
    // pre($_SERVER);
    // };
    if ($config['protocol'] == 'http' and $arr['only_https']) {
        redirect_301('https://'.$config['domain_origin'].$_SERVER['REQUEST_URI']);
    }
    $config['type'] = $arr['type'];
    $config['path']['type'] = $config['path']['server'].'types/'.$config['type'].'/template/';
    $additional_path = empty($config['additional_path']) ? '' : $config['additional_path'].'/';
    // if (isset($_GET['mmnas'])) {pre($config);};
    $config['path']['site'] = $config['path']['server'].'sites/'.$additional_path.$config['domain'].'/template/';
    $config['Mobile_Detect'] = new Mobile_Detect;
    if ($arr['mobile_version'] == 'on') {
        // setcookie(name)
        // echo '<pre>'; var_dump($_COOKIE); echo '</pre>';
        $mobile_version = 0;
        if (isset($_COOKIE['mobile_version']) == 1) {
            if ($_COOKIE['mobile_version'] == 0) {
                $mobile_version = 0;
            }
            if ($_COOKIE['mobile_version'] == 1) {
                $mobile_version = 1;
            }
        }
        if (isset($_COOKIE['mobile_version']) == 0) {
            // if (detect_mobile_browser($_SERVER['HTTP_USER_AGENT'])==true) {$mobile_version=1;};
            if ($config['Mobile_Detect']->isMobile() or $config['Mobile_Detect']->isTablet()) {
                $mobile_version = 1;
            }
        }
        // die('mobile_version: '.$mobile_version);
        if ($mobile_version == 1) {
            $config['path']['site'] = $config['path']['site'].'mobile_version/';
        }
    }
    // ���� �� ������� query �� ����������� page �� $_GET['page']
    if (empty($_GET['query'])) {
        if (empty($_GET['page'])) {
            $config['vars']['page'] = 'index';
        }
        if (! empty($_GET['page'])) {
            $config['vars']['page'] = braces2square(checkstr($_GET['page']));
        }
        // ������ ������� ��� ������
        if ($config['domain'] == 'xrest.net') {
            if ($_SERVER['REQUEST_URI'] == '/index.php') {
                redirect_301('/');
            }
            if (isset($_GET['action']) and $_GET['action'] == 'tags') {
                $config['vars']['page'] = 'tags_videos';
                if (empty($_GET['tag'])) {
                    $_GET['tag'] = '';
                }
                $_GET['tag_ru'] = checkstr($_GET['tag']);
                // ��������� �������� �� ������������� ����
                if (isset($_GET['page'])) {
                    $_GET['p'] = $_GET['page'];
                } else {
                    $_GET['p'] = 1;
                }
                if (! isset($_GET['sort'])) {
                    $_GET['sort'] = 'mr';
                }
            }
        }
    }
    // ���� ������� query, �� �� $_GET['page'] ��� �� �������, ��������� URL
    if (! empty($_GET['query'])) {
        $_GET['query'] = braces2square($_GET['query']);
        engine_rewrite_query();
    }
    // ���������� ������� ��� ����� ���� ��� ����������
    $sitefunc = $config['path']['server'].'sites/'.$additional_path.$config['domain'].'/inc/func.php';
    if (file_exists($sitefunc)) {
        include $sitefunc;
    }
    // ���������� ������� ���� � ��������� ��� ������� �������������
    if ($config['type'] != 'engine') {
        // ������������ ������� ��� ����
        classAutoLoader($config['type']);
        $typefunc = $config['path']['server'].'types/'.$config['type'].'/inc/func.php';
        if (file_exists($typefunc)) {
            include $typefunc;
        }
        if (! file_exists($typefunc)) {
            error('��� �����������');
        }
        $func_type_init = $config['type'].'_init';
        if (function_exists($func_type_init) == 0) {
            error('����������� ������� �������������');
        }
        $func_type_init();
    }
    // ���� page ��� �� ��������� � ������ rewrite_function ��� ������, �� ��������� ���
    if (empty($config['vars']['page']) and ! empty($config['rewrite_function'])) {
        if (empty($_GET['query'])) {
            error('�� ������� ������');
        }
        if (! function_exists($config['rewrite_function'])) {
            error('������� �� ����������', 'Rewrite ������� �� ����������');
        }
        $config['rewrite_function']($_GET['query']);
    }
    // ���� �������� ��� � �� ���������� �� �������� �� ������
    if (empty($config['vars']['page'])) {
        // logs('�������� �������� 404','','db','404');
        logs_alt('�������� �������� 404', ['type' => '404']);
        $config['vars']['page'] = '404';
    }
    // ��������� cronjob
    // cronjob(tabname('engine','crons'));
}
