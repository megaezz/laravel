<?php

namespace MegawebV1;

function check_tor_proxies($url = '')
{
    if (empty($url)) {
        if (empty($_GET['url'])) {
            error('�� ������� url');
        } else {
            $url = $_GET['url'];
        }
    }
    $tors = ['tor1', 'tor2', 'tor3', 'tor4', 'tor5', 'tor6', 'tor7'];
    $list = '';
    foreach ($tors as $tor) {
        $list .= '<li>'.$tor.': '.filegetcontents_alt($url, ['error' => 'no_error', 'proxy' => $tor]).'</li>';
    }

    return '<ul>'.$list.'</ul>';
}

function admin_reset_cache()
{
    $domain = empty($_GET['domain']) ? error('�� ������� �����') : $_GET['domain'];
    $reset_function = empty($_GET['reset_function']) ? error('�� �������� �������') : $_GET['reset_function'];
    $rows = reset_cache($reset_function, 'all', $domain);

    return '�������� ��� ��� '.$domain.' '.$reset_function.'. ���������: '.$rows.' �����.';
}

function html_source()
{
    if (empty($_GET['url'])) {
        alert('url=,proxy=[default_proxy | tor]');
    } else {
        $url = $_GET['url'];
    }
    if (empty($_GET['proxy'])) {
        $proxy = 'default_proxy';
    } else {
        $proxy = $_GET['proxy'];
    }
    if (empty($_GET['return_info'])) {
        $return_info = 'body';
    } else {
        $return_info = $_GET['return_info'];
    }
    // pre($_GET);
    $content = pre_return(request_headers());
    $curl['proxy'] = $proxy;
    $curl['ua'] = 'iphone';
    $curl['return_info'] = $return_info;
    // pre($curl);
    // $rnkey='1606817*1771087:4007175998:2742656117:1';
    // $curl['cookies']=array('RNKEY'=>$rnkey,'Referer'=>$url,'PHPSESSID'=>'5304a87b3703053e0424ddcab4e719e3');
    $page = filegetcontents_alt($url, $curl);
    // if (strpos($page,'Content-Encoding: gzip')!==false) {$page=gzdecode($page);};
    // $page=filegetcontents($url,'default_error',$curl['ua'],'default_interface','default_proxy','body',$curl['cookies']);
    // die($page);
    // $content=pre_return(htmlspecialchars(braces2square($page),ENT_QUOTES,'cp1251'));
    $content .= pre_return($page);

    // $page=filegetcontents($url,'default_error','default_ua','default_interface',$proxy);
    // exec('curl --interface '.$interface.' '.$url,$arr);
    // pre($arr);
    /*
    $rnkey=pornhub_leastfactor_key($page);
    // $rnkey='1606817*1771087:4007175998:2742656117:1';
    if ($rnkey) {
        $curl['cookies']=array('RNKEY'=>$rnkey,'Referer'=>$url);
        // pre($curl);
        $page_new=filegetcontents_alt($url,$curl);
        $content.=pre_return($rnkey);
        // $content.=pre_return(htmlspecialchars(braces2square($page_new),ENT_QUOTES,'cp1251'));
        $content.=pre_return($page_new);
    };
    */
    return $content;
    // return pre(file_get_contents($url));
}

function delete_file($file)
{
    // ��������� ���������� �� ����
    if (file_exists($file) == 0) {
        error('���� �� ������', '���� '.$file.' �� ������');
    }
    unlink($file);
}

function full_db_backup()
{
    global $config;
    $path = $config['path']['server'].'backups/'.date('Y-m-d_H:i').'/';
    mkdir($path);
    chmod($path, 0777);
    $data = query('show databases');
    $dbs = readdata($data, 'Database');
    unset($dbs['information_schema']);
    unset($dbs['mysql']);
    foreach ($dbs as $db => $v) {
        $data = query('show tables from '.$db);
        $tabs = readdata($data, 'Tables_in_'.$db);
        // echo '<pre>'; print_r($config); echo '</pre>';
        foreach ($tabs as $tab => $v) {
            echo '����� '.$db.'.'.$tab.' ';
            flush();
            query('select * into outfile \''.$path.$db.'.'.$tab.'.txt\' from '.$db.'.'.$tab);
            echo '������.<br/>';
            flush();
        }
    }
}

function logs_list()
{
    $per_page = 200;
    $current_page = 1;
    if (! empty($_GET['p'])) {
        $current_page = $_GET['p'];
    }
    $data = query('select count(*) as q from '.tabname('engine', 'logs'));
    $arr = mysql_fetch_assoc($data);
    $rows = $arr['q'];
    $pages_list = page_numbers($rows, $per_page, $current_page, '/?page=admin/log/log&amp;p=', 10);
    $data = query('select * from '.tabname('engine', 'logs').' order by date desc limit '.(($current_page - 1) * $per_page).','.$per_page);
    $arr = readdata($data, 'nokey');
    // print_r($arr);
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<tr>
		<td class="date"><div>'.$v['date'].'</div></td>
		<td class="text"><div onmousedown="this.style.height=\'auto\';">'.$v['text'].'</div></td>
		<td class="ip"><div>'.$v['ip'].'</div></td>
		<td class="user_agent"><div onmousedown="this.style.height=\'auto\';">'.$v['user_agent'].'</div></td>
		<td class="query"><div onmousedown="this.style.height=\'auto\';">'.$v['query'].'</div></td>
		<td class="referer"><div onmousedown="this.style.height=\'auto\';">'.$v['referer'].'</div></td>
		</tr>';
    }
    $content = '<div class="pages"><ul>
	'.$pages_list.'
	</ul></div>
	<table class="admin_table logs_table">
	<tr class="header"><td>����</td><td>�����</td><td>IP</td><td>USER_AGENT</td><td>������</td><td>REFERER</td></tr>
	'.$list.'
	</table>
	<div class="pages"><ul>
	'.$pages_list.'
	</ul></div>';

    return $content;
}

function truncate_logs()
{
    truncate_table(tabname('engine', 'logs'));
}
