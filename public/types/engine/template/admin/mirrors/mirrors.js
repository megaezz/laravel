window.addEventListener('DOMContentLoaded', (event) => {
	var vue = new Vue({
		el: '#domains',
		data: {
			domains: [],
			domainsFilterSettings: {
				ruKnownBlock: null,
				useAltDescriptions: null,
				withoutRussianMovies: null,
				rknForRuOnly: null,
				moviesOnly: null,
				serialsOnly: null,
				includeYoutube: null,
				domainSearch: null,
				brand: null,
				typeTemplate: null,
				type: 'cinema',
				yandexNotAllowed: null,
				withoutRiskyStudios: null,
				accessSchemeV2: null,
				with_traffic: null,
				hreflang: null,
				order: null,
				moviesMinYear: null,
				withoutClientDomain: null,
			},
			listTemplate: 'full',
			showBlocks: false,
			showFailedDomains: false,
			user: vueUser,
		},
		computed: {
			failedDomains: function() {
				return this.filteredDomains.filter(domain => (
					(!domain.redirect_to && domain.client_redirect_to) ||
					!domain.metrika_id ||
					!domain.site_name ||
					(domain.client_redirect_to && domain.client_redirect) ||
					!domain.on ||
					(((domain.mirrors.filter(mirror => ((domain.videoroll_id || (domain.parentDomain?domain.parentDomain.videoroll_id:null)) && ((domain.redirect_to && domain.redirect_to.domain == mirror.domain) || (domain.client_redirect_to && domain.client_redirect_to.domain == mirror.domain)) && (mirror.parentDomain && !mirror.parentDomain.videoroll_id)))).length && ((domain.metrikaDataCached && domain.metrikaDataCached.totals)?(((domain.metrikaDataCached.totals[0] > 30000))?true:false):false))?true:false)
					// (domain.isOn && (!domain.faviconSrc || !domain.logoSrc))
					));
			},
			blockedDomains: function() {
				var blockedDomains = [];
				this.filteredDomains.forEach(function(domain){
					var isBlocked = false;
					if (!domain.on) {
						return false;
					}
					if (!domain.cinema_domain || (domain.cinema_domain && !domain.cinema_domain.rknBanned)) {
						// если домен заблочен и не имеет редиректа, либо редирект есть, но, на самого себя, то отмечаем его заблокированным
						if (domain.ru_block && (!domain.redirect_to || (domain.redirect_to && domain.redirect_to.domain == domain.domain))) {
							isBlocked = true;
						}
						domain.mirrors.forEach(function(mirror){
							if ((domain.redirect_to && domain.redirect_to.domain == mirror.domain) && mirror.ru_block) {
								isBlocked = true;
							}
							if ((domain.client_redirect_to && domain.client_redirect_to.domain == mirror.domain) && mirror.ru_block) {
								isBlocked = true;
							}

						});
						if (isBlocked) {
							blockedDomains.push(domain);
						}
					}
				});
				return blockedDomains;
			},
			filteredDomains: function() {
				var domains = this.domains.filter(domain => (
					(this.domainsFilterSettings.ruKnownBlock?(domain.cinema_domain && domain.cinema_domain.rknBanned == this.domainsFilterSettings.ruKnownBlock):true) &&
					(this.domainsFilterSettings.useAltDescriptions?(domain.cinema_domain && domain.cinema_domain.useAltDescriptions == this.domainsFilterSettings.useAltDescriptions):true) &&
					(this.domainsFilterSettings.withoutRussianMovies?(domain.cinema_domain && !domain.cinema_domain.allowRussianMovies == this.domainsFilterSettings.withoutRussianMovies):true) &&
					(this.domainsFilterSettings.rknForRuOnly?(domain.cinema_domain && domain.cinema_domain.rknForRuOnly == this.domainsFilterSettings.rknForRuOnly):true) &&
					(this.domainsFilterSettings.moviesOnly?(domain.cinema_domain && (domain.cinema_domain.include_movies && !domain.cinema_domain.include_serials) == this.domainsFilterSettings.moviesOnly):true) &&
					(this.domainsFilterSettings.serialsOnly?(domain.cinema_domain && (!domain.cinema_domain.include_movies && domain.cinema_domain.include_serials) == this.domainsFilterSettings.serialsOnly):true) &&
					(this.domainsFilterSettings.includeYoutube?(domain.cinema_domain && domain.cinema_domain.include_youtube == this.domainsFilterSettings.includeYoutube):true) &&
					(this.domainsFilterSettings.domainSearch?(
						(domain.domain?domain.domain.includes(this.domainsFilterSettings.domainSearch):false) || 
						(domain.domainDecoded?domain.domainDecoded.includes(this.domainsFilterSettings.domainSearch):false) || 
						(domain.redirect_to?domain.redirect_to.domain.includes(this.domainsFilterSettings.domainSearch):false) || 
						(domain.client_redirect_to?domain.client_redirect_to.domain.includes(this.domainsFilterSettings.domainSearch):false) ||
						(domain.mirrors.filter(mirror => (
							mirror.domain.includes(this.domainsFilterSettings.domainSearch) ||
							mirror.domainDecoded.includes(this.domainsFilterSettings.domainSearch)
							)).length?true:false) ||
						(domain.comment?domain.comment.includes(this.domainsFilterSettings.domainSearch):false)
							// domain.typeTemplate.includes(this.domainsFilterSettings.domainSearch)
						):true) &&
					(this.domainsFilterSettings.brand?(domain.brand == this.domainsFilterSettings.brand):true) &&
					(this.domainsFilterSettings.typeTemplate?(domain.type_template == this.domainsFilterSettings.typeTemplate):true) &&
					(this.domainsFilterSettings.type?(domain.type == this.domainsFilterSettings.type):true) &&
					(this.domainsFilterSettings.yandexNotAllowed?(!domain.yandexAllowed == this.domainsFilterSettings.yandexNotAllowed):true) &&
					(this.domainsFilterSettings.withoutRiskyStudios?(domain.cinema_domain && !domain.cinema_domain.allowRiskyStudios == this.domainsFilterSettings.withoutRiskyStudios):true) &&
					(this.domainsFilterSettings.accessSchemeV2?(domain.access_scheme == 'v2'):true) &&
					(this.domainsFilterSettings.with_traffic?(domain.metrikaDataCached && domain.metrikaDataCached.totals && domain.metrikaDataCached.totals[0] >= 30000):true) &&
					(this.domainsFilterSettings.hreflang?(domain.ru_domain || domain.next_ru_domain):true) &&
					(this.domainsFilterSettings.moviesMinYear?(domain.cinema_domain && domain.cinema_domain.moviesMinYear):true) &&
					(this.domainsFilterSettings.withoutClientDomain?(!domain.client_redirect_to == this.domainsFilterSettings.withoutClientDomain):true)
					));
				if (this.domainsFilterSettings.order == 'add_date desc') {
					domains.sort((domain1, domain2) => (new Date(domain2.addDateISO8601)) - (new Date(domain1.addDateISO8601)));
				}
				if (this.domainsFilterSettings.order == 'add_date') {
					domains.sort((domain1, domain2) => (new Date(domain1.addDateISO8601)) - (new Date(domain2.addDateISO8601)));
				}
				return domains;
			},
			getMetrikaTotalVisits: function() {
				var total = 0;
				this.filteredDomains.forEach(function(domain){
					if (domain.metrikaDataCached && domain.metrikaDataCached.totals) {
						total = total + domain.metrikaDataCached.totals[0];
					}
				});
				return total;
			},
		},
		methods: {
			getTypes: function() {
				var types = [];
				this.domains.forEach(function(domain) {
					if (types.indexOf(domain.type) < 0) {
						types.push(domain.type);
					}
					// console.log(domain.type+': '+types.indexOf(domain.type));
				});
				// console.log(types);
				return types;
			},
			getMetrikaRuPercent: function(domain) {
				if (!domain.metrikaDataCached || !domain.metrikaDataCached.query || (domain.metrikaDataCached.query.preset != 'geo_country')) {
					return false;
				}
				var $percent = null;
				domain.metrikaDataCached.data.forEach(function(data){
					if (data.dimensions[0].iso_name == 'RU') {
						$percent = Math.ceil(data.metrics[0]/domain.metrikaDataCached.totals[0]*100);
					}
				});
				return $percent;
			},
			createMirror: function(domain,mirror) {
				// alert([domain,mirror]);
				axios
				.post('/?r=admin/api/Admin/createMirror',{
					mirror: mirror.nextMirror,
					domain: domain.domain,
				})
				.then(response => {
					mirror.nextMirrorCreated = true;
					// alert('Зеркало создано');
				})
				.catch(error => alert('Ошибка при создании зеркала'));
			},
			setMirror: function(domain,mirror,type) {
				axios
				.post('/?r=admin/api/Admin/setMirror',{
					mirror: mirror.nextMirror,
					domain: domain.domain,
					type: type,
				})
				.then(response => {
					// чтобы исчезло из списка
					mirror.ru_block = false;
					alert('Зеркало установлено');
				})
				.catch(error => alert('Ошибка при установки зеркала'));
			},
		},
		mounted() {
			if (localStorage.listTemplate) {
				this.listTemplate = JSON.parse(localStorage.listTemplate);
			}
			if (localStorage.selectedType) {
				// чтобы не выдывало ошибку когда localStorage.selectedType = undefined. В дальнейшем можно убрать проверку, повториться не должно такое.
				try {
					this.domainsFilterSettings.type = JSON.parse(localStorage.selectedType);
				} catch (e) {
					this.domainsFilterSettings.type = null;
				}
			}
			if (localStorage.showBlocks) {
				this.showBlocks = JSON.parse(localStorage.showBlocks);
			}
			axios
			.get('/?r=admin/Admin/mirrorsListDomainsEloquent')
			.then(response => this.domains = response.data);
		},
		watch: {
			listTemplate(value) {
				localStorage.listTemplate = JSON.stringify(value);
			},
			showBlocks(value) {
				localStorage.showBlocks = JSON.stringify(value);
			},
			'domainsFilterSettings.type': function(newVal,oldVal) {
				// решаем проблему, что вместо null приходит undefined
				if (!newVal) {
					newVal = null;
				}
				localStorage.selectedType = JSON.stringify(newVal);
			}
		}
	});
})