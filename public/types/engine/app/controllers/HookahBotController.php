<?php

namespace engine\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;

class HookahBotController
{
    // private $moySkladToken = '6bb6c856bf6f727d9277132a4bf678802de1aae0';
    private $moySkladToken = '3a0b99630b623f8965a038761a2745ef6a2b13fc';

    private $telegramToken = '1873363700:AAHm1SLT8NE119PxUdfhDfwo_cu8qzODkow';

    public function getMoySkladFolders($group_id = null)
    {
        // F::dump($this->apiMoySklad('entity/productfolder/878230a6-85ab-11eb-0a80-0115004ac81a'));
        $result = $this->apiMoySklad('entity/productfolder');
        // F::dump($result->rows);
        $new = [];
        foreach ($result->rows as $v) {
            if (isset($v->productFolder->meta->href) and $v->productFolder->meta->href == 'https://online.moysklad.ru/api/remap/1.2/entity/productfolder/'.$group_id) {
                $new[] = $v;
            }
        }

        // F::dump($new);
        return $new;
    }

    public function apiMoySklad($method = null)
    {
        $ch = curl_init('https://online.moysklad.ru/api/remap/1.2/'.$method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer '.$this->moySkladToken,
        ]);
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    public function answer()
    {
        $request = json_decode(file_get_contents('php://input'), true);
        if ($request) {
            F::log(F::pre_str($request));
        }
        Engine::setDebug(false);
        $bot = new \TelegramBot\Api\BotApi($this->telegramToken);
        $vars = new \stdClass;
        $vars->bot = $bot;
        $vars->chat_id = $request['message']['chat']['id'] ?? ($request['callback_query']['message']['chat']['id'] ?? null);
        $vars->text = $request['message']['text'] ?? ($request['callback_query']['message']['text'] ?? null);
        $vars->message_id = $request['callback_query']['message']['message_id'] ?? null;
        $vars->data = $request['callback_query']['data'] ?? null;
        if ($vars->data) {
            $vars->data = json_decode($vars->data);
        }
        // $vars->chat_id = 275834308;
        // $vars->message_id = 145;
        // $vars->text = 'Каталог';
        // $vars->data = 'b2986e62-abf2-11eb-0a80-014a0038427a';
        $commands = $this->getCommands();
        if ($vars->text) {
            if (isset($commands[$vars->text])) {
                $method = $commands[$vars->text];
                $this->$method($vars);
            } else {
                $bot->sendMessage($vars->chat_id, 'Команда "'.F::escape_string($vars->text).'" недоступна.', null, null, null, $this->getMainKeyboard());
            }
        } else {
            $bot->sendMessage($vars->chat_id, 'Привет я кальянный бот', null, null, null, $this->getMainKeyboard());
        }

        return 'ok';
    }

    public function getCommands()
    {
        $commands = [
            'Каталог' => 'getCatalog',
            // 'Корзина' => 'getCart',
            // 'Поиск' => 'getSearch',
            // 'Заказы' => 'getOrders',
            // 'FAQ' => 'getFaq',
            // 'Доставка' => 'getDelivery'
        ];

        return $commands;
    }

    public function getMainKeyboard()
    {
        $buttons = [];
        $row = 0;
        $i = 0;
        foreach ($this->getCommands() as $k => $v) {
            $buttons[$row][] = $k;
            if (empty(($i + 1) % 3)) {
                $row++;
            }
            $i++;
        }
        // F::dump($buttons);
        $keyboard = new \TelegramBot\Api\Types\ReplyKeyboardMarkup($buttons, true);

        return $keyboard;
    }

    public function getCatalog($vars = null)
    {
        $bot = $vars->bot ?? F::error('Bot required');
        $chatId = $vars->chat_id ?? F::error('ChatId required');
        $messageId = $vars->message_id ?? null;
        $text = $vars->text ?? F::error('Text required');
        $data = $vars->data ?? null;
        if (! $data) {
            $data = new \stdClass;
            $data->id = '878230a6-85ab-11eb-0a80-0115004ac81a';
        }
        $folders = $this->getMoySkladFolders($data->id);
        // F::dump($folders);
        $buttons = [];
        $row = 0;
        foreach ($folders as $i => $f) {
            $vdata = new \stdClass;
            $vdata->id = $f->id;
            // $vdata->prev = $data->id;
            $button = [
                'text' => $f->name,
                'callback_data' => json_encode($vdata),
            ];
            $buttons[$row][] = $button;
            if (empty(($i + 1) % 3)) {
                $row++;
            }
        }
        if (isset($f->productFolder->meta->href)) {
            $prev = str_replace('https://online.moysklad.ru/api/remap/1.2/entity/productfolder/', '', $f->productFolder->meta->href);
            $vdata = new \stdClass;
            $vdata->id = $prev;
            $buttons[][] = [
                'text' => 'Назад',
                'callback_data' => json_encode($vdata),
            ];
        }
        // F::dump($buttons);
        $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup($buttons);
        if ($messageId) {
            $bot->editMessageText($chatId, $messageId, 'Каталог', null, null, $keyboard);
        } else {
            $bot->sendMessage($chatId, 'Каталог', null, null, null, $keyboard);
        }
    }

    public function setWebhook()
    {
        $bot = new \TelegramBot\Api\BotApi($this->telegramToken);
        F::dump($bot->setWebhook('https://awmzone.net/?r=HookahBot/answer'));
        // $url = 'https://api.telegram.org/bot'.$this->telegramToken.'/setwebhook?url=https://awmzone.net/?r=HookahBot/answer';
        // F::dump(file_get_contents($url));
    }
}
