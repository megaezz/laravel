<?php

namespace engine\app\controllers;

use App\Models\Config;
use App\Models\Domain as ModelsDomain;
use engine\app\models\Cron;
use engine\app\models\Domain;
use engine\app\models\Domains;
use engine\app\models\Engine;
use engine\app\models\F;

class CronController
{
    public static function checkRequestsPerSecond()
    {
        $q = F::getVisitsPerSecond();
        $return = 'Запросов в секунду: '.$q;
        if ($q > Config::first()->requests_per_second_max) {
            return logger()->alert($return);
        }

        return $return;
    }

    public static function callSites()
    {
        $domains = new Domains;
        $domains->setType('cinema');
        $domains->setOrder('rand()');
        $domains->setLimit(10);
        $arr = $domains->get();
        $objects = [];
        foreach ($arr as $v) {
            $domain = new Domain($v['domain']);
            $object = new \stdClass;
            $object->curl = new \stdClass;
            $object->curl->follow_location = true;
            $object->curl->user_agent = 'awmzone';
            $object->curl->connect_timeout = 3;
            $object->url = 'https://'.$domain->getLastRedirectDomain();
            // $object->url = 'https://ru.turboserials.net';
            $objects[] = $object;
        }
        $start = Engine::getRunningTime();
        F::multicurl($objects);
        $end = Engine::getRunningTime();
        // F::dump($objects);
        $list = '';
        foreach ($objects as $object) {
            $list .= $object->url.PHP_EOL.PHP_EOL.mb_strimwidth($object->result, 0, 200, '...').PHP_EOL.PHP_EOL;
        }

        // foreach ($arr as $v) {
        // 	$domain = new Domain($v['domain']);
        // 	$start = Engine::getRunningTime();
        // 	$answer = file_get_contents('https://'.$domain->getLastRedirectDomain());
        // 	$end = Engine::getRunningTime();
        // 	$list.= '<li>'.$v['domain'].' '.($end - $start).' с. <pre>'.htmlspecialchars(mb_strimwidth($answer, 0, 200,'...')).'</pre></li>';
        // }
        return 'Выполнено за: '.($end - $start).' с.'.PHP_EOL.$list;
    }

    public static function ban()
    {
        $arrByUserAgent = F::query_arr('
			select userAgent,round(count(*)/300,1) as requestsPerSec
			from '.F::tabname('engine', 'visits').'
			where date between date_sub(current_timestamp,interval 5 minute) and current_timestamp
			group by userAgent
			having requestsPerSec > 20
			;');
        foreach ($arrByUserAgent as $v) {
            F::query('insert into '.F::tabname('engine', 'ban').' set
				user_agent = \''.F::escape_string($v['userAgent']).'\',
				active = 1,
				description = \'Добавлено автоматически. '.$v['requestsPerSec'].' з/с \'
				on duplicate key update
				active = 1
				;');
        }
        //
        $arrByIp = F::query_arr('
			select ip,round(count(*)/300,1) as requestsPerSec
			from '.F::tabname('engine', 'visits').'
			where date between date_sub(current_timestamp,interval 5 minute) and current_timestamp
			group by ip
			having requestsPerSec > 20
			;');
        foreach ($arrByIp as $v) {
            F::query('insert into '.F::tabname('engine', 'ban').' set
				ip = \''.F::escape_string($v['ip']).'\',
				active = 1,
				description = \'Добавлено автоматически. '.$v['requestsPerSec'].' з/с \'
				on duplicate key update
				active = 1
				;');
        }
    }

    public static function getMetrikaData()
    {
        $limit = 30;
        $domains = ModelsDomain
        /* добавляет в выдачу поле metrika_updated_at, чтобы потом по нему сортировать */
        ::withAggregate('metrika', 'updated_at')
            ->has('metrika')
            ->limit($limit)
            ->orderBy('metrika_updated_at')
            ->get()
            ->each(function ($domain) {
                $domain->metrika->data = $domain->metrika->metrika_data;
                /* Обновление поля updated_at, иначе если фактических изменений нет, то updated_at не обновляется и нарушается работа задания */
                $domain->metrika->touch();
                $domain->metrika->save();
            });

        // dump($domains);
        return 'Получено счетчиков: '.$domains->count();
    }

    public static function checkRuBlockFromRublacklist()
    {
        // ini_set('memory_limit', '1024M');
        $url = 'https://reestr.rublacklist.net/api/v2/domains/json/';
        $blacklist = json_decode(file_get_contents($url));

        $domains = new Domains;
        $domains->setType('cinema');
        $domains->setIsOn(true);
        $arr = $domains->get();
        $time1 = Engine::getRunningTime();
        $domains = [];
        foreach ($arr as $v) {
            $domain = new Domain($v['domain']);
            if ($domain->getRedirectTo()) {
                $domains[] = $domain->getRedirectTo();
            } else {
                $domains[] = $domain->getDomain();
            }
            if ($domain->getClientRedirectTo()) {
                $domains[] = $domain->getClientRedirectTo();
            }
        }
        $list = '';
        $time2 = Engine::getRunningTime();
        foreach ($domains as $domain) {
            $blocked = false;
            $d = new Domain($domain);
            if (in_array($d->getDomain(), $blacklist) or in_array($d->getDomainDecoded(), $blacklist)) {
                $blocked = true;
            }
            $d->setRuBlock($blocked);
            $d->save();
            $list .= '<li>'.$domain.' -> '.($blocked ? '<span style="color: red">блок</span>' : '<span style="color: green">ок</span>').'</li>';
        }
        $time3 = Engine::getRunningTime();

        return '
		<p>Получили список доменов: '.$time1.'</p>
		<p>Cформировали cписок с редиректами: '.$time2.'</p>
		<p>Выполнили проверку: '.$time3.'</p>
		<ol>'.$list.'</ol>';
    }

    public static function checkCrons()
    {
        // получаем кроны
        $arr['engine'] = (new Cron('engine'))->getInfo();
        if ((new Cron('cinema'))->isTableExist()) {
            $arr['cinema'] = (new Cron('cinema'))->getInfo();
        }
        $list = '';
        foreach ($arr as $type => $arr2) {
            foreach ($arr2 as $v) {
                if (! ($v['active'] and ! $v['done'])) {
                    continue;
                }
                $lastTime = (new \DateTime)->setTimestamp($v['unix_last_time']);
                $diff = (new \DateTime)->diff($lastTime);
                $minutes = $minutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;
                if ($minutes < 2) {
                    continue;
                }
                $list .= "\r\n".$type.': '.$v['action'].' ('.$minutes.' мин.)';
            }
        }
        $return = 'Отвалились кроны:'.$list;
        if ($list) {
            return logger()->alert($return);
        }

        return $return;
    }
}
