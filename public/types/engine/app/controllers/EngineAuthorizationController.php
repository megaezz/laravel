<?php

namespace engine\app\controllers;

// use engine\app\models\AuthorizationController;

class EngineAuthorizationController extends AuthorizationController
{
    public function __construct($levels = [])
    {
        parent::__construct('engine', $levels);
    }
}
