<?php

namespace engine\app\controllers;

use engine\app\controllers\admin\AdminController;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class MainController
{
    public function api($args = [])
    {
        // dump($args);
        $method = isset($args['method']) ? $args['method'] : F::error('Method is required for API route action');
        $class = 'API'.(isset($args['class']) ? $args['class'] : '');
        // dump($class);
        $foundClass = engine::findClassMethod('models', $class, $method);
        if (! $foundClass) {
            F::error('API class or method was not found');
        }
        $object = new $foundClass;

        return $object->$method();
    }

    public function index()
    {
        F::alertLite('Main page');
        // return (new AdminController)->index();
    }

    public function text()
    {

        $pathinfo = pathinfo(request()->path());

        $ext = $pathinfo['extension'] ?? null;

        if ($ext == 'txt') {
            header('Content-Type: text/plain');
        }
        if ($ext == 'html') {
            header('Content-Type: text/html');
        }
        if ($ext == 'js') {
            header('Content-type: text/javascript');
        }

        $t = new Template;
        $t->setContent('{topText}');
        F::setPageVars($t);

        return $t->get();
    }

    public function alloha()
    {
        $requestedDomain = Engine::getRequestedDomainEloquent();
        if (! $requestedDomain->alloha_txt) {
            throw new \Exception('Отсутствует alloha_txt');
        }

        return response($requestedDomain->alloha_txt)->header('Content-Type', 'text/plain');
    }
}
