<?php

namespace engine\app\controllers;

use engine\app\models\Authorization;
use engine\app\models\F;
use engine\app\models\Template;

class AuthorizationController extends Authorization
{
    public function __construct($type = null, $levels = [])
    {
        parent::__construct($type, $levels);
        if (! $this->check()) {
            if (isset($_POST['enter'],$_POST['login'],$_POST['pass'])) {
                // pre('enter');
                $this->setLogin(F::escape_string($_POST['login']));
                $this->setPassword(F::escape_string($_POST['pass']));
                if (! $this->enter()) {
                    $this->showForm();
                }
            } else {
                $this->showForm();
            }
        }
    }

    // ввел $t чтобы потомки могли передавать свои переменные на страницу авторизации
    public function showForm($t = null)
    {
        if (! $t) {
            $t = new Template('authorization_form');
        }
        $t->v('type', $this->getType());
        $t->v('error', $this->getError());
        exit($t->get());
    }
}
