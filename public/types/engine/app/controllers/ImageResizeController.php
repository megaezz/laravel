<?php

namespace engine\app\controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

/* Кропает изображение на лету, сохраняет и сразу отдает его */

class ImageResizeController
{
    public function handle(string $measure, int $size, string $src)
    {

        $validator = Validator::make([
            'measure' => $measure,
            'size' => $size,
            'src' => $src,
        ], [
            'measure' => 'required|in:w,h',
            'size' => 'required|in:300,200,100,40',
            'src' => 'required',
        ]);

        // не влияет
        // $src = strtolower($src);
        if ($validator->fails()) {
            throw new \Exception($validator->messages()->first());
        }
        // существует ли исходное изображение?
        if (! file_exists(public_path($src))) {
            // сделал аборт вместо исключения, чтобы локалка не подыхала от генерации тяжелой страницы с ошибкой на каждую картинку
            abort(404, 'Изображение не существует');
        }
        // это точно изображение?
        if (! getimagesize(public_path($src))) {
            throw new \Exception('Не является изображением');
        }
        // если целевое изображение уже существует, значит что-то пошло не так с правами файлов и будет too many redirects, избегаем этого
        if (Storage::disk('public')->exists("images/{$measure}{$size}/{$src}")) {
            throw new \Exception('Целевое изображение уже существует');
        }
        $image = Image::make(public_path($src));
        $image->resize(
            (($measure == 'w') ? $size : null),
            (($measure == 'h') ? $size : null),
            function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        // ->save(storage_path("app/public/images/{$measure}{$size}/{$src}"));

        // \Debugbar::disable();

        /* сохраняем не встроенным в Image способом, а через Storage, чтобы автоматически создавались папки */
        Storage::disk('public')->put("images/{$measure}{$size}/{$src}", $image->stream()->detach());

        // Вывод изображения в HTTP-ответе
        // return response($image->encode($image->extension))->header('Content-Type', $image->mime);

        // return redirect(request()->url());

        /* когда вывод megaweb.v2 будет не из middleware а из контроллера, то использовать return который ниже */
        return response()->file(Storage::disk('public')->path("images/{$measure}{$size}/{$src}"));
    }
}
