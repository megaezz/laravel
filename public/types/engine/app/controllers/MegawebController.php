<?php

namespace engine\app\controllers;

use App\Http\Controllers\Controller;
use Barryvdh\Debugbar\Facades\Debugbar;
use engine\app\models\Engine;
use engine\app\models\F;

class MegawebController extends Controller
{
    public function __invoke()
    {
        // dump(F::runtime('Начало mgwc'));
        Debugbar::startMeasure('MegawebController');

        if (Engine::getDomainObject()->getEngineVersion() == 'v1') {
            if (! include (__DIR__.'/../../inc/func.php')) {
                exit('Missing engine file');
            }

            \MegawebV1\init();

            $result = \MegawebV1\print_page();

            global $config;

            $code = $config['http_code'] ?? null;

        } else {

            $result = Engine::run();

        }

        /* получаем заголовки, которые были переданы через header(), парсим их и засовываем в response, т.к. laravel их игнорит */
        $headers = [];
        foreach (headers_list() as $header) {

            /* получаем заголовок: значение */
            preg_match('/^(.[^:]+):(.+)$/', $header, $matches);

            if (empty($matches) or empty($matches[1]) or empty($matches[2])) {
                /* что-то не так уходим */
                continue;
            }

            $key = trim($matches[1]);
            $value = trim($matches[2]);

            if ($key == 'Set-Cookie') {
                /* пропускаем, что-то с куками не то */
                continue;
            }

            $headers[$key] = $value;
        }

        /* если отдается файл, то не оборачиваем в response, из-за этого был косяк, картинка не выводилась, зато на страницу попадали HTTP заголовки */
        if ($result instanceof \Symfony\Component\HttpFoundation\BinaryFileResponse) {
            return $result;
        }

        /* если отдается редирект, то не оборачиваем в response, т.к. заголовки редиректа попадают в тело страницы */
        if ($result instanceof \Illuminate\Http\RedirectResponse) {
            return $result;
        }

        /* если отдается объект response, значит предполагаем, что ранее никакие заголовки не передавались и нечего исправлять */
        if ($result instanceof \Illuminate\Http\Response) {
            return $result;
        }

        Debugbar::stopMeasure('MegawebController');

        $response = response($result);

        if ($code ?? null) {
            $response->setStatusCode($code);
        }

        return $response->withHeaders($headers);
    }
}
