<?php

namespace engine\app\controllers;

use engine\app\models\F;

class RedirectsController
{
    private $url = null;

    public function __construct()
    {
        $this->url = request()->getRequestUri();
    }

    public function lentaFilms()
    {
        $arr = [
            '/drama.html?start=1890' => 'http://turbokino.club/online-kino/70097~смотреть-Крым-онлайн',
            '/drama.html?start=2289' => 'http://turbokino.club/online-kino/57950~смотреть-Планета+обезьян+Война-онлайн',
            '/drama.html?start=6839' => 'http://turbokino.club/search?year=2016&order=topRated&index',
            '/index.php/?start=19978' => 'http://turbokino.club/genres/11~Комедия',
            '/d3/komedii/' => 'http://turbokino.club/genres/11~Комедия',
            '/' => 'http://turbokino.club',
            '/100.php' => 'http://turbokino.club/search?order=topRated&index',
            '/d3/boeviki/' => 'http://turbokino.club/genres/65~Боевик',
            '/d3/fantastika/' => 'http://turbokino.club/genres/9~Фантастика',
            '/d3/fentezi/' => 'http://turbokino.club/genres/83~Фэнтези',
            '/d3/fentezi/fentezi-boeviki/' => 'http://turbokino.club/genres/83~Фэнтези',
            '/d3/fentezi/fentezi-priklyucheniya/' => 'http://turbokino.club/genres/12~Приключения',
            '/d3/fentezi/lyubovnoe-fentezi/' => 'http://turbokino.club/genres/83~Фэнтези',
            '/d3/komedii/filmy/filmy/' => 'http://turbokino.club/search?genre=11&order=topRated&index',
            '/d3/uzhasy/' => 'http://turbokino.club/genres/418~Ужасы',
            '/film-3.php' => 'http://turbokino.club/online-kino/67343~смотреть-Гоголь.+Начало-онлайн',
            '/istoricheskij.html' => 'http://turbokino.club/genres/1166~История',
            '/d3/fentezi/lyubovnoe-fentezi/lyubovnoe-fentezi-v-zhanre-erotika/' => 'http://turbokino.club/genres/83~Фэнтези',
            '/drama.html?start=5327' => 'http://turbokino.club/online-kino/59157~смотреть-Тачки+3-онлайн',
            '/d3/fentezi/lyubovnoe-fentezi/lyubovnye-romany-v-zhanre-fentezi/' => 'http://turbokino.club/genres/83~Фэнтези',
            '/index.php/?start=24073' => 'http://turbokino.club/genres/418~Ужасы',
            '/index.php/?start=3528' => 'http://turbokino.club/online-kino/57687~смотреть-Берега-онлайн',
            '/index.php/?start=23646' => 'http://turbokino.club/online-kino/67343~смотреть-Гоголь.+Начало-онлайн',
            '/index.php/?start=22246' => 'http://turbokino.club/online-kino/26632~смотреть-Время-онлайн',
            '/index.php/?start=25921' => 'http://turbokino.club/online-kino/46429~смотреть-Темная+башня-онлайн',
            '/d3/fentezi/zhanr-fentezi/' => 'http://turbokino.club/genres/83~Фэнтези',
            '/index.php/?start=22036' => 'http://turbokino.club/group/357',
            '/index.php/?start=12999' => 'http://turbokino.club/top50',
            '/index.php/?start=364' => 'http://turbokino.club/online-kino/59157~смотреть-Тачки+3-онлайн',
            '/index.php/?start=24647' => 'http://turbokino.club/search?year=2016&type=2&order=topRated&index',
            '/index.php/?start=2597' => 'http://turbokino.club/top50',
            '/index.php/?start=3661' => 'http://turbokino.club/search?order=topRated&index',
            '/index.php/?start=7413' => 'http://turbokino.club/online-kino/47642~смотреть-Форсаж-онлайн',
            '/index.php/?start=7406' => 'http://turbokino.club/top50',
            '/index.php/?start=2583' => 'http://turbokino.club/search?year=2017&type=2&order=topRated&index',
            '/index.php/?start=6356' => 'http://turbokino.club/genres/7~Аниме',
            '/index.php/?start=553' => 'http://turbokino.club/top50',
            '/index.php/?start=23359' => 'http://turbokino.club/online-kino/70097~смотреть-Крым-онлайн',
            '/index.php/?start=5365' => 'http://turbokino.club/online-kino/59157~смотреть-Тачки+3-онлайн',
            '/index.php/?start=7560' => 'http://turbokino.club/top50',
            '/smotret-9.php' => 'http://turbokino.club/online-kino/37473~смотреть-Возвращение-онлайн',
            '/indijskie.html' => 'http://turbokino.club/group/3',
            '/index.php/?start=9380' => 'http://turbokino.club/search?year=2016&type=2&order=topRated&index',
            '/luchshiy.php' => 'http://turbokino.club/search?year=2016&type=2&order=topRated&index',
            '/melodrama/3771-smotret-onlajn-snegurochka-1968.html' => 'http://turbokino.club/online-kino/52414~смотреть-Снегурочка-онлайн',
            '/drama/1809-smotret-onlajn-intim-intimacy-2001.html' => 'http://turbokino.club/online-kino/50650~смотреть-Интим-онлайн',
            '/istoricheskij.html' => 'http://turbokino.club/genres/1166~История',
            '/melodrama.html' => 'http://turbokino.club/genres/10~Мелодрама',
            '/index.php/?start=357' => 'http://turbokino.club/search?year=2017&type=2&order=topRated&index',
            '/fantastika.html' => 'http://turbokino.club/genres/9~Фантастика',
            '/dokumentalnyj.html' => 'http://turbokino.club/genres/6013~Документальный',
            '/anime.html' => 'http://turbokino.club/genres/7~Аниме',
            '/komedija.html' => 'http://turbokino.club/genres/11~Комедия',
        ];
        if (isset($arr[$this->url])) {
            F::redirect_301($arr[$this->url]);
        } else {
            F::redirect_301('http://turbokino.club');
        }
    }
}
