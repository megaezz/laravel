<?php

namespace engine\app\controllers;

use engine\app\models\Engine;
use engine\app\models\F;

class GorinController
{
    public function submitOrder()
    {

        // этот кусок проверки входных аргументов был переписан через чат гпт без проверки
        if (! request()->has('QUERY_STRING')) {
            F::error('Empty query string');
        }

        $queries = request()->query();
        $sum = empty($queries['sum']) ? (empty(request()->input('sum')) ? 'не указано' : F::checkstr(request()->input('sum'))) : F::checkstr($queries['sum']);
        $phone = empty($queries['phone']) ? (empty(request()->input('phone')) ? 'не указано' : F::checkstr(request()->input('phone'))) : F::checkstr($queries['phone']);
        $address = empty($queries['passport']) ? (empty(request()->input('passport')) ? 'не указано' : F::checkstr(request()->input('passport'))) : F::checkstr($queries['passport']);
        $name = empty($queries['name']) ? (empty(request()->input('name')) ? 'не указано' : F::checkstr(request()->input('name'))) : F::checkstr($queries['name']);
        $source = empty($queries['source']) ? (empty(request()->server('HTTP_REFERER')) ? 'не указано' : F::checkstr(request()->server('HTTP_REFERER'))) : F::checkstr($queries['source']);

        // $sum = empty($_POST['sum'])?'не указано':F::checkstr($_POST['sum']);
        // $phone = empty($_POST['sum'])?'не указано':F::checkstr($_POST['phone']);
        // $address = empty($_POST['passport'])?'не указано':F::checkstr($_POST['passport']);
        // $name = empty($_GET['name'])?'не указано':F::checkstr($_POST['name']);
        $to = 'gorin.kirill@helpmecentr.ru';
        // $to = 'megaezh@me.com';
        $subject = 'Заявка с сайта pnz.helpmecentr.ru';
        $headers = 'MIME-Version: 1.0'."\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
        $headers .= 'From: robot@pnz.helpmecentr.ru'."\r\n";
        $body = '
		<p>Новая заявка:</p>
		<p>
		Имя: '.$name.'<br>
		Телефон: '.$phone.'<br>
		Сумма: '.$sum.'<br>
		Адрес: '.$address.'<br>
		Источник: '.$source.'.<br>
		</p>
		';
        // F::dump($to);
        // F::dump($body);
        mail($to, $subject, $body, $headers);
        logger()->info("Заявка с сайта $source: имя: $name, телефон: $phone, сумма: $sum, адрес: $address");
        // engine::setDisplayErrors(true);
        $answer = new \stdClass;
        $answer->status = true;
        $answer->message = '<h1>Заявка принята, спасибо. Мы вам перезвоним</h1>';
        header('Content-Type: application/json');

        return '';

        return json_encode($answer);
    }
}
