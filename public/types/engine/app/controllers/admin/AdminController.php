<?php

namespace engine\app\controllers\admin;

use engine\app\controllers\EngineAuthorizationController;
use engine\app\models\ControllerMethod;
use engine\app\models\Engine;
use engine\app\models\F;
use engine\app\models\Template;

class AdminController
{
    private $auth = null;

    public function __construct()
    {
        $this->auth = new EngineAuthorizationController(['admin', 'admin-lite']);
    }

    public function setMenuVars($t = null)
    {
        $arr = [
            'Index' => ['admin\\Admin\\index'],
            'Routes' => [
                'admin\\Routes\\domains',
                'admin\\Routes\\route',
            ],
            'Methods' => ['admin\\Admin\\controllerMethods'],
            'Settings' => ['admin\\Admin\\engineSettings'],
            'State' => ['admin\\Admin\\state'],
            'Mirrors' => ['admin\\Admin\\mirrorsList'],
        ];
        $vueUser = new \stdClass;
        $vueUser->login = $this->auth->getLogin();
        $vueUser->level = $this->auth->getLevel();
        $t->v('vueUser', json_encode($vueUser));

        return F::setMenuActiveVars($t, Engine::router(), $arr);
    }

    public function controllerMethods()
    {
        $methodID = empty($_GET['methodID']) ? null : F::checkstr($_GET['methodID']);
        $t = new Template('admin/methods/page');
        $t = $this->setMenuVars($t);
        $list = '';
        foreach (ControllerMethod::getAll('videohub') as $v) {
            $method = new ControllerMethod($v['id']);
            $active = ($methodID === $v['id']) ? 'active' : '';
            $list .= '<li class="nav-item mb-2">
			<a class="btn btn-light '.$active.'" href="/?r=admin/Admin/controllerMethods&amp;methodID='.$method->getID().'">
			'.$method->getController().'::'.$method->getMethod().'
			</a>
			</li>';
        }
        $t->v('methodsList', $list);
        if ($methodID) {
            $method = new ControllerMethod($methodID);
            $t->vf('text', nl2br($method->getText()));
        } else {
            $t->v('text', 'Выберите метод');
        }

        return $t->get();
    }
}
