<?php

spl_autoload_register(function ($class = null) {
    $path1 = __DIR__.'/../../'.str_replace('\\', '/', $class).'.php';
    $path2 = __DIR__.'/../../../../'.str_replace('\\', '/', $class).'.php';
    // $path2 = null;
    if (file_exists($path1)) {
        require $path1;
    } elseif (file_exists($path2)) {
        require $path2;
    }
});
