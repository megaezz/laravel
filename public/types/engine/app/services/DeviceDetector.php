<?php

namespace engine\app\services;

use DeviceDetector\DeviceDetector as DeviceDetectorVendor;

class DeviceDetector
{
    protected $data;

    public function __construct($userAgent = null)
    {
        $userAgent = $userAgent ?? request()->header('User-Agent');
        if (! $userAgent) {
            return false;
        }
        $deviceDetector = new DeviceDetectorVendor($userAgent);
        $deviceDetector->parse();
        $this->data = $deviceDetector;
    }

    public function getClient($property = null)
    {
        if (! $this->data) {
            return false;
        }

        return $this->data->getClient($property);
    }
}
