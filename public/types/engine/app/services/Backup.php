<?php

namespace engine\app\services;

use App\Models\Config;
use App\Models\Node;
use Illuminate\Support\Facades\DB;

/*
Достаточно только указать каталог для бекапа и исключить ненужные таблички.
Исключенные БД не будут забекаплены, исключенные таблицы - будет бекап только структуры. (актуально для обычного дампа, если hotDump, то будет полный дамп)
Параметры подключения к БД берутся из конфига, список баз подтягивается автоматически.
*/

class Backup
{
    private $dumpDir;

    private $connection;

    private $host;

    private $username;

    private $password;

    private $borgRepository;

    private $borgRepositoryPassword;

    private $dirToBeBackedUp;

    private $backupName;

    private $hotDumpFile = 'mariabackup.xb.zst';

    private $dumpLogFile = 'mariabackup.log';

    private $backupLogFile = 'backup.log';

    /* действия по умолчанию если не передан actions */
    private $actions = 'hotDump,borg,cleanDump';

    public function __construct(private $dry)
    {
        /* DB */
        $this->dumpDir = Config::cached()->db_dump_dir;
        $this->connection = config('database.default');
        $this->host = config("database.connections.{$this->connection}.host");
        $this->username = config("database.connections.{$this->connection}.username");
        $this->password = config("database.connections.{$this->connection}.password");
        /* Borg */
        $this->borgRepository = Config::cached()->borg_repository;
        $this->borgRepositoryPassword = Config::cached()->borg_repository_password;
        $this->dirToBeBackedUp = Config::cached()->dir_to_be_backed_up;
        $this->backupName = now()->format('Y-m-d_H_i');
    }

    /* выдает список бд для дампа с настройками, используется только в обычном дампе, т.к. в горячем много ебли если исключать базы и таблицы */
    public function databasesToBeDumped()
    {

        /* указываем какие БД исключить и какие таблицы в БД исключить */
        $settings = [
            'engine' => [
                'exclude' => [
                    'cache',
                    'logs',
                    'visits',
                ],
            ],
            'laravel' => [
                'exclude' => [
                    'cache',
                    'logs',
                    'visits',
                ],
            ],
            'sys' => false,
            'mysql' => false,
            'information_schema' => false,
            'performance_schema' => false,
            // 'engine' => false,
            // 'cinema' => false,
            // 'helpme' => false,
            // 'laravel' => false,
            // 'videohub' => false,
        ];

        /* вычисляем исключенные БД */
        $excludedDatabases = collect($settings)->filter(function ($setting) {
            return ! $setting;
        })->keys()->toArray();

        /* получаем все имеющиеся бд, убираем системные базы, добавляем кастомные настройки и приводим в нужный формат */
        $databases = collect(DB::select('show databases'))
            ->reject(function ($database) use ($excludedDatabases) {
                return in_array($database->Database, $excludedDatabases);
            })
            ->mapWithKeys(function ($database) use ($settings) {
                return [$database->Database => $settings[$database->Database] ?? null];
            })
            ->toArray();

        return $databases;

    }

    public function checkAndCreateDumpDir()
    {
        if (! $this->dumpDir) {
            throw new \Exception('Не указан каталог для дампа');
        }

        if (! $this->dry) {
            if (is_dir($this->dumpDir) and file_exists($this->dumpDir)) {
                throw new \Exception('Каталог для дампа существует, а не должен');
            }

            /* создаем папку для дампа */
            mkdir($this->dumpDir);
        }
    }

    /* выдает готовую команду для горячего дампа бд */
    public function hotDumpCommands()
    {

        $this->checkAndCreateDumpDir();

        $node = Node::where('monitored', true)->where('mariadb', true)->first() ?? throw new \Exception('Не указан сервер бд');

        $databases = collect($this->databasesToBeDumped());

        /* mariabackup поддерживает частичный дамп, но через жопу, нужно вручную потом создавать структуру таблиц, которые не были включены в дамп, плюс ко всему --tables-exclude как у меня реализовано - не исключает таблицы из дампа, не стал разбираться почему, кто-то говорит что вместо этого нужно перечислять исключенные таблицы в --databases.exclude. пофиг, делаем полный дамп, чтобы избежать всяких проблем */

        /*$excludedTables = collect();

        $databases->filter(function($database){
            return isset($database['exclude']);
        })->each(function($parameters, $database) use (&$excludedTables) {
            foreach ($parameters['exclude'] as $table) {
                $excludedTables[] = "{$database}.{$table}";
            }
        });*/

        /*
        $commands[] = "{$node->ssh} 'sudo docker exec mariadb sh -c '\''mariadb-backup --backup --stream=xbstream --user={$username} --password={$password} --databases=\\\"{$databases->keys()->implode(' ')}\\\" --tables-exclude=\\\"{$excludedTables->implode(' ')}\\\" | zstd - -1'\''' > {$this->dumpDir}/{$this->hotDumpFile} 2>{$this->dumpDir}/{$this->dumpLogFile};";
        */

        /* со сжатием */
        // предыдущая рабочая команда
        /*
        $commands[] = "{$node->ssh} 'sudo docker exec mariadb sh -c '\''mariadb-backup --backup --stream=xbstream --user={$this->username} --password={$this->password} | zstd - -1'\''' >{$this->dumpDir}/{$this->hotDumpFile} 2>{$this->dumpDir}/{$this->dumpLogFile};";
        */

        // поскольку mariadb запущена сейчас как докер swarm сервис, то нельзя войти внутрь контейнера через по имени mariadb, поэтому получаем название нужного контейнера через $(docker ps -qf "name=mariadb\.")
        $commands[] = "{$node->ssh} 'sudo docker exec {$node->mariadb_container} sh -c '\''mariadb-backup --backup --stream=xbstream --user={$this->username} --password={$this->password} | zstd - -1'\''' >{$this->dumpDir}/{$this->hotDumpFile} 2>{$this->dumpDir}/{$this->dumpLogFile};";

        // TODO: надо еще делать дамп для lat.het, в принципе команд должна быть рабочей, единсвтенное надо складывать дампы в разные папки

        /* TODO: без сжатия, не получается придумать как распаковать полученный .xb одновременно с его удалением */
        // $commands[] = "{$node->ssh} 'sudo docker exec mariadb sh -c '\''mariadb-backup --backup --stream=xbstream --user={$this->username} --password={$this->password}'\''' >{$this->dumpDir}/{$this->hotDumpFile} 2>{$this->dumpDir}/{$this->dumpLogFile};";

        return $commands;
    }

    /* выдает готовые команды для создания обычного дампа бд */
    public function dumpCommands()
    {

        $this->checkAndCreateDumpDir();

        $databases = $this->databasesToBeDumped();

        $command = "mysqldump -h {$this->host} -u {$this->username} --password={$this->password} --skip-column-statistics";

        $commands = [];
        foreach ($databases as $database => $parameters) {
            $ignoreTables = '';
            if (isset($parameters['exclude'])) {
                $ignoreTables = collect($parameters['exclude'])->map(function ($table) use ($database) {
                    return "--ignore-table='{$database}.{$table}'";
                })->implode(' ');
                $commands[] = "{$command} --no-data '{$database}' | gzip > '{$this->dumpDir}/{$database}_structure.sql.gz';";
            }
            $commands[] = "{$command} --skip-lock-tables --single-transaction=TRUE {$ignoreTables} '{$database}' | gzip > '{$this->dumpDir}/{$database}.sql.gz';";
        }

        return $commands;
    }

    public function borgCommands()
    {

        $commands[] = "export BORG_PASSPHRASE='{$this->borgRepositoryPassword}';";
        /* отправляем созданный дамп в борг */
        /* --progress - для просмотра прогресса выполнения */

        // исключаем из бекапа
        $excludedPaths = collect([
            'storage/app/public/cinema/turbo',
        ])
            ->map(fn ($path) => "--exclude={$this->dirToBeBackedUp}/{$path}")
            ->implode(' ');

        $commands[] = "borg create {$this->borgRepository}::{$this->backupName} {$this->dirToBeBackedUp} {$excludedPaths};";

        /* удаляем старье */
        /* --dry-run --list - просто показать что будет удалено */
        $commands[] = "borg prune --keep-daily=10 --keep-weekly=5 --keep-monthly=5 {$this->borgRepository};";

        return $commands;
    }

    public function handler($actions = null)
    {

        if (! $actions) {
            $actions = $this->actions;
        }

        $actions = collect(explode(',', $actions));

        $commands = [];

        if ($actions->contains('hotDump')) {
            $commands = array_merge($commands, $this->hotDumpCommands());
        }

        if ($actions->contains('dump')) {
            $commands = array_merge($commands, $this->dumpCommands());
        }

        if ($actions->contains('borg')) {
            $commands = array_merge($commands, $this->borgCommands());
        }

        if ($actions->contains('cleanDump')) {
            $commands = array_merge($commands, ["rm -r {$this->dumpDir};"]);
        }

        if (! $commands) {
            return "Команды {$actions} отсутствуют";
        }

        /*
        Итак, чтобы запустить команду в фоне, надо в конце поставить символ &
        Но именно с этой командой этот способ конечно же сука не сработал
        Точнее он работает, только если запускать команды построчно, но тогда они все выполняются одновременно и создают большую нагрзку
        У меня была задача выполнять их последовательно. Пробовал обернуть их в скобки и в конце поставить & - работает последовательно, но не в фоне
        В итоге наткнулся на примерно такой же вопрос и там дали совет использовать такой способ:
        Обернуть всю цепочку запросов в sh -c и направить вывод nohup в очко. Не знаю что здесь происходит, но оно единственное сработало.
        Совет отсюда https://qna.habr.com/q/692648

        >/dev/null - отбрасываем стандартный вывод команды
        2>&1 - означает что ошибки идут туда же куда и стандартный вывод, т.е. в нашем случае вникуда

        >/dev/null сменил на >{$this->dumpDir}/{$this->backupLogFile} чтобы смотреть полный лог бекапа (пока что не получается, там всегда пусто)
        */

        $implodedCommands = collect($commands)->implode(' ');

        $commands = "nohup sh -c \"{$implodedCommands}\" >{$this->dumpDir}/{$this->backupLogFile} 2>&1 &";

        // dd($commands);

        if (! $this->dry) {

            try {
                $output = shell_exec($commands);
            } catch (\Exception $e) {
                logger()->alert($e->getMessage());
            }

            logger()->alert('Бекап запущен');
        }

        return $commands;
    }

    /* пока что не получилось сделать общий вывод процесса бекапа в backupLogFile. поэтому пока отображаем dumpLogFile */
    public function log()
    {
        // return "tail -f {$this->dumpDir}/{$this->backupLogFile}";
        return "tail -f {$this->dumpDir}/{$this->dumpLogFile}";
    }

    public function info()
    {
        return "
		Следить за прогрессом дампа:
		php artisan engine:backup --log

		Сделать просто горячий дамп, без бекапа и зарузки в borg:
		php artisan engine:backup hotDump

		Как достать бекап из borg:
		Выполняем например в workspace {$this->borgRepository}:
		Выбираем необходимый бекап из списка:
		borg list ./
		Смотрим инфу о нем, чтобы иметь представление о размере:
		borg info ./::{имя бекапа}
		Переходм в {$this->dirToBeBackedUp} и извлекаем бекап сразу сюда ('{$this->dirToBeBackedUp}/*' чтобы не создавались папки {$this->dirToBeBackedUp}/, а извлекалось сразу куда нужно, еще не пробовал так)
		borg extract --progress {$this->borgRepository}::{имя бекапа} '{$this->dirToBeBackedUp}/*'

		Как достать только дамп из borg:
		Переходим в папку в которую нужно извлечь дамп и выполняем:
		borg extract --progress {$this->borgRepository}::{имя бекапа} '{$this->dumpDir}/*'

		Как перенести бд на отдельный сервер без даунтайма:
		ssh-copy-id на новый сервер, чтобы workspace и далее созданный ssh туннель имели доступ к нему
		Создаем контейнер ssh туннеля на новый сервер
		Отключаем все кроны, отключаем кинотекст/киноконтент в cinema.configs
		php artisan engine:backup hotDump
		Разворачиваем дамп на новом сервере по инструкции ниже.
		Тестируем работоспособность. Меняем хост бд в .env и включаем все кроны и т.д. обратно.
		Вносим необходимые изменения в engine.nodes

		Как восстановить горячий дамп:
		Во-первых убедиться что на сервере есть место .zst * 10
		Если извлекал бекап из borg, то место может быть засрано подгруженными по сети в s3data данными, достаточно перезагрузиться
		Переносим архив в /root/.laradock/mariadb/dump чтобы он появился в контейнере mariadb (и чтобы файлы были в пределах одной файловой системы, иначе перенос работает через копирование, это долго и требует лишнее место на диске)
		В контейнере mariadb:
		Распаковываем архив: (минут 10)
		zstd -d {$this->hotDumpFile} -c | pv | mbstream -x -C ./mariabackup
		Переходим в папку с распакованным архивом и подготавливаем бекап:
		mariadb-backup --prepare --target-dir=./
		Отключаем контейнер mariadb
		Очищаем папку /root/.laradock/mariadb и переносим в нее mariabackup
		Запускаем контейнер mariadb, смотрим логи что все ок.

		Если с правами что-то не то, в контейнере mariadb выполнить:
		chown -R mysql:mysql /var/lib/mysql/
		";
    }
}
