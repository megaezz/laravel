<?php

namespace engine\app\services;

use Monolog\Logger;

class TelegramLogger
{
    /**
     * Create a custom Monolog instance.
     *
     * @return Logger
     */
    public function __invoke(array $config)
    {
        return new Logger('Telegram', [
            new TelegramHandler,
        ]);
    }
}
