<?php

namespace engine\app\services;

use App\Jobs\SendByAwmzoneBotTo;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\LogRecord;

class TelegramHandler extends AbstractProcessingHandler
{
    /**
     * {@inheritDoc}
     */
    protected function write(array|LogRecord $record): void
    {
        $levels = ['CRITICAL', 'ALERT', 'EMERGENCY'];

        /* почему срабатывает на любой уровень логов, хотя в logging.php указан уровень critical, поэтому такой костыль */
        if (! in_array($record['level_name'], $levels)) {
            return;
        }

        SendByAwmzoneBotTo::dispatch('awmzoneAdmins', $record['message']);
    }
}
