<?php

namespace engine\app\services;

use App\Models\Config;
use App\Models\Node;

class UpdateNodeDatasAndAlert
{
    public function handler()
    {
        $nodes = Node::whereMonitored(true)->get();
        foreach ($nodes as $node) {

            try {
                $node->setData();
            } catch (\Exception $e) {
                logger()->alert("Ошибка получения данных для ноды {$node->name}: {$e->getMessage()}");
                continue;
            }

            if ($node->type == 'secondary') {
                // плюс 25% чтобы не орал при транскодировании
                $maxLa = $node->nproc * 1.25;
            } else {
                $maxLa = $node->nproc;
            }

            if ($node->load_average[0] > $maxLa) {
                logger()->alert("{$node->name} load average: {$node->load_average[0]}");
            }

            if ($node->free_space < Config::first()->free_space_min) {
                logger()->alert("{$node->name} free space: {$node->free_space}%");
            }
        }

        return true;
    }
}
