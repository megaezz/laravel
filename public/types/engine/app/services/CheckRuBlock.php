<?php

namespace engine\app\services;

use App\Models\Config;
use App\Models\Domain;
use App\Models\Proxy;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Promise\Utils;

class CheckRuBlock
{
    public static function handler()
    {

        $clientSettings = [
            'timeout' => Config::cached()->ru_block_timeout,
            'headers' => [
                'User-Agent' => 'awmzone',
            ],
        ];

        $requestSettings = [
            'allow_redirects' => [
                'max' => 0,
            ],
            'stream' => false,
        ];

        /* используем случайный русский активный прокси */
        $proxy = Proxy::where('country', 'RU')->where('active', true)->where('rkn', true)->inRandomOrder()->first()->proxy;

        if (! $proxy) {
            throw new \Exception('Нет нужного прокси');
        }

        $clientSettings['proxy'] = $proxy;

        $client = new Client($clientSettings);

        $promises = [];

        $domains = Domain::query()
            ->with('redirectTo')
            ->with('yandexRedirectTo')
            ->with('clientRedirectTo')
            ->whereIn('type', ['cinema', 'copy', 'videohub'])
            ->whereOn(true)
            ->limit(30)
            ->orderBy('ru_block_checked_at')
        // ->inRandomOrder()
            ->get()
            ->each(function ($domain) use ($client, &$promises, $requestSettings) {

                /* перебираем актуальные зеркала домена */
                $domain->actualMirrors->each(function ($actualMirror) use ($client, &$promises, $requestSettings) {

                    /* создаем асинхронный запрос к домену и обрабатываем успешный ответ первым замыканием, а так же исключения - вторым */
                    $promises[] = $client->headAsync("https://{$actualMirror->domain}", $requestSettings)->then(

                        /* если ответ получен, то считаем домен не заблокированным */
                        function ($response) use (&$actualMirror) {
                            $actualMirror->ru_block = false;
                            $actualMirror->save();
                        },

                        function ($reason) use (&$actualMirror) {
                            /* если ошибка вызвана не ConnectException, то считаем домен изначально не заблокированным, иначе если домен отдает допустим 404, например vdb9.awmzone1.pro - и случайно произошел ConnectException, домену проставился статус заблочен и он не мог выбраться из него, т.к. у него не бывает успешного ответа, чтобы проставить false */
                            $actualMirror->ru_block = false;
                            /* если запрос вызвал ошибку ConnectException, то считаем домен заблокированным */
                            if ($reason instanceof ConnectException) {
                                $actualMirror->ru_block = true;
                            }
                            $actualMirror->save();
                        },

                    );

                });

                /* отмечаем, что была проверка на блокировку */
                $domain->ru_block_checked_at = now();
                $domain->save();

            });

        /* выполняем все асинхронные запросы */
        Utils::settle($promises)->wait();

        $results = [];

        /* выводим результаты */
        $domains->each(function ($domain) use (&$results) {
            $domain->actualMirrors->each(function ($actualMirror) use (&$results) {
                $results[] = [
                    $actualMirror->domain,
                    $actualMirror->ru_block ? 'заблокирован' : 'доступен',
                ];
            });
        });

        return $results;
    }
}
