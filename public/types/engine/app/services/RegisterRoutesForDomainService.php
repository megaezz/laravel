<?php

namespace engine\app\services;

use App\Models\Domain;
use engine\app\controllers\MegawebController;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class RegisterRoutesForDomainService
{
    public static array $routes;

    /* тут хранится домен, который был обработан без префикса */
    public static ?string $handledHostWithoutPrefix;

    /* тут хранятся домен, который были обработаны с префиксом */
    public static array $handledHostsWithPrefix;

    public static array $domainRoutes;

    public function register($domain, ?bool $withoutPrefix = false)
    {

        /* если передана строка - подгружаем домен */
        if (is_string($domain)) {
            $domain = Domain::findOrFail($domain);
        }

        if (! $domain) {
            return false;
        }

        $prefix = ! $withoutPrefix;

        if ($prefix) {
            /* если домен c префиксом уже был обработан, не выполняем повторно */
            if (isset(self::$handledHostsWithPrefix[$domain->domain])) {
                return true;
                // dump("{$domain->domain} - с префиксом, пропускаем, был обработан");
                // $do['withPrefix'] = false;
            } else {
                // dump("{$domain->domain} - регистрируем с префиксом");
                /* помечаем, что домен был обработан */
                self::$handledHostsWithPrefix[$domain->domain] = true;
            }
        } else {
            if (isset(self::$handledHostWithoutPrefix)) {
                throw new \Exception("{$domain->domain} - попытка повторно зарегистрировать роуты без префикса, уже зарегистрированы для ".self::$handledHostWithoutPrefix);
                // если пытаемся зарегистрировать роуты, для того же самого домена, то такое может быть в тестах, просто не выполняем повторно
                // а если другой домен, то ругаемся
                // if (self::$handledHostWithoutPrefix === $domain->domain) {
                //     return true;
                // } else {
                //     throw new \Exception("{$domain->domain} - попытка повторно зарегистрировать роуты без префикса, уже зарегистрированы для " . self::$handledHostWithoutPrefix);
                // }
            } else {
                // dump("{$domain->domain} - регистрируем без префикса");
                self::$handledHostWithoutPrefix = $domain->domain;
            }
        }

        /* проверяем содержит ли массив domainRoutes роуты для этого домена или зеркала, если да, то создаем роуты без запросов к бд, используя эти роуты */
        if (isset(self::$domainRoutes[$domain->primary_domain->domain])) {

            // dump("$domain->domain используем сохраненные роуты");

            foreach (self::$domainRoutes[$domain->primary_domain->domain] as $route) {

                $this->initRoute($route, $domain->domain, $prefix);

            }

        } else {

            /* применяем роуты в порядке: engine, type, site, чтобы например /monitoring не перезаписывался роутами сайта */
            $types = ($domain->primary_domain->type == 'engine') ? ['engine'] : ['engine', $domain->primary_domain->type];

            /* получаем сразу все роуты: engine, type и domain */
            $routes = $domain->mixed_routes;

            /* перебираем все роуты для типа домена и engine */
            foreach ($types as $type) {

                // if (!class_exists("{$type}\\app\\models\\eloquent\\Route")) {
                //     continue;
                // }

                // foreach ("{$type}\\app\\models\\eloquent\\Route"::whereNotNull('pattern')->whereNotNull('name')->get() as $route) {
                foreach ($routes->where('type', $type)->whereNull('domain') as $route) {

                    /* сохраняем роуты, чтобы использовать их потом для зеркал без запроса к бд */
                    self::$domainRoutes[$domain->primary_domain->domain][] = $route;

                    $this->initRoute($route, $domain->domain, $prefix);

                }
            }

            /* перебирем все доменные роуты */
            // foreach ($domain->primary_domain->routes()->whereNotNull('pattern')->whereNotNull('name')->get() as $route) {
            foreach ($routes->whereNotNull('domain') as $route) {

                /* сохраняем роуты, чтобы использовать их потом для зеркал без запроса к бд */
                self::$domainRoutes[$domain->primary_domain->domain][] = $route;

                $this->initRoute($route, $domain->domain, $prefix);

            }

        }

        // если движок просто laravel - то создаем fallback маршрут, в котором отображаем обычную 404 ошибку, применяя к ней middleware
        // если другой движок, то отправляем все нераспознанное в MegawebController, чтобы там попробовать найти megaweb роут
        if ($domain->primary_domain->engine == 'laravel') {

            Route::fallback(function (Request $request) {
                abort(404, 'The route could not be found.');
            })->middleware('dynamic');

        } else {

            Route::domain($domain->domain)->any('{any}', MegawebController::class)
                ->where('any', '.*')
                ->withoutMiddleware(VerifyCsrfToken::class)
                ->middleware('dynamic');

        }
    }

    /* принимает объект модели роута, домен (возможно уже и не надо его) и собирает laravel роут */
    public function initRoute(object $route, string $domain, bool $prefix)
    {
        if (! $domain) {
            throw new \Exception("Невозможно создать роут для {$route->pattern}. Остутствует домен.");
        }
        if (! $route->name) {
            throw new \Exception("Невозможно создать роут для {$route->pattern}. Нет имени роута.");
        }
        if ($prefix) {
            $routeName = $domain.'_'.$route->name;
        } else {
            $routeName = $route->name;
        }
        if (Route::has($routeName)) {
            throw new \Exception("Невозможно создать роут для {$routeName}. Роут с таким именем уже существует.");
        }
        if (! $route->pattern) {
            throw new \Exception("Невозможно создать роут {$routeName}. Остутствует паттерн.");
        }
        if (! $route->controller and ! $route->template) {
            throw new \Exception("Невозможно создать роут {$routeName}. Отсутствует и шаблон и контроллер.");
        }
        if ($route->controller) {
            if (! class_exists($route->controller)) {
                throw new \Exception("Невозможно создать роут {$routeName}. Контроллер {$route->controller} не существует.");
            }
            if ($route->method) {
                if (! method_exists($route->controller, $route->method)) {
                    throw new \Exception("Невозможно создать роут {$routeName}. Метод {$route->method} в контроллере {$route->controller} не существует.");
                }
            }
        }
        /* начинаем создавать новый роут */
        $new_route = Route::domain($domain);
        $new_route->name($routeName);
        /* если указан массив с переменными, применяем */
        if ($route->vars) {
            $new_route->where(unserialize($route->vars));
        }
        // начинаем собирать массив middleware
        $middlewares = collect();
        /* если есть посредники указанные в route->middleware - добавляем их */
        if ($route->middleware) {
            foreach (explode('|', $route->middleware) as $middleware) {
                $middlewares->push($middleware);
            }
        }
        /* если отключено проверка csrf токена - отключаем соответствующий middleware */
        // сделано через withoutMiddleware, потому что VerifyCsrfToken уже применяется по дефолту к web группе
        if (! $route->csrf_middleware) {
            $new_route->withoutMiddleware([VerifyCsrfToken::class]);
        }
        // тут начинаем добавлять свои middleware в нужном порядке
        if ($route->cron_switcher_middleware) {
            $middlewares->push(\App\Http\Middleware\Engine\CronSwitcher::class);
        }
        if ($route->ban_middleware) {
            $middlewares->push(\App\Http\Middleware\Engine\Ban::class);
        }
        if ($route->access_scheme_middleware) {
            $middlewares->push(\App\Http\Middleware\Engine\AccessScheme::class);
        }
        // заканчиваем добавлять свои middleware и применяем разом все к маршруту
        if ($middlewares->count()) {
            $new_route->middleware($middlewares->toArray());
        }
        /* определяем экшн либо шаблон в конце, т.к. иначе с какого то черта middleware не срабатывает */
        if ($route->controller) {
            if ($route->method) {
                $action = [$route->controller, $route->method];
            } else {
                $action = $route->controller;
            }
        } else {
            $action = function () use ($route) {
                return view($route->template);
            };
        }
        /* or !$route->http_methods - можно убрать, оставил для пуша, чтобы не было ошибок, пока бд не поправлю */
        if ($route->http_methods == 'any' or ! $route->http_methods) {
            $new_route->any($route->pattern, $action);
        } else {
            $new_route->match(explode(',', $route->http_methods), $route->pattern, $action);
        }

        /* создаем массив, в котором соотносим имена созданных роутов с соответствующими моделями роутов, чтобы потом при необходимости получить настройки роутов, без дополнительных запросов к БД */
        self::$routes[$routeName] = $route;
    }

    /* ищет по массиву соответствий laravel роутов моделям роутов */
    public static function getRouteSettings(\Illuminate\Routing\Route $route)
    {
        return self::$routes[$route->getName()] ?? null;
    }

    /* создает роуты для домена из переданного домена и затем возвращает соответствующий реквесту роут, если такой есть */
    public static function getRouteByRequest($request, ?Domain $domain)
    {

        /* можно передать домен, чтобы избежать дублирования запросов */
        if (! isset($domain)) {
            /* получаем домен из реквеста */
            $domain = Domain::findOrFail($request->getHost());
        }

        /* регистрируем для него роуты */
        $domain->registerRoutes();

        /* и теперь возвращаем роут, удовлетворяющий реквесту */
        try {
            return Route::getRoutes()->match($request);
        } catch (\Exception $e) {
            return false;
        }
    }
}
