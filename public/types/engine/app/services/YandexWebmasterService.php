<?php

namespace engine\app\services;

use App\Models\Domain;
use GuzzleHttp\Client;

class YandexWebmasterService
{
    public $token;

    public function __construct($token = null)
    {
        if (! $token) {
            throw new \Exception('Не передан яндекс токен');
        }
        $this->token = $token;
    }

    public function recrawl(array $urls = [])
    {

        $client = new Client([
            'base_uri' => 'https://api.webmaster.yandex.net',
            /* чтобы не выбрасывалось исключение при статусе 404 и других */
            'http_errors' => false,
            /* чтобы заголовок проставлялся к каждому запросу автоматически */
            'headers' => [
                'Authorization' => 'OAuth '.$this->token,
            ],
        ]);
        $user_id = json_decode($client->get('/v4/user')->getBody())->user_id;

        /* получать все хосты здесь нет смысла, т.к. host_id генерится предсказуемо и все сайты пока что под одним аккаунтом */
        /* $hosts = json_decode($client->get("https://api.webmaster.yandex.net/v4/user/{$user_id}/hosts", $settings)->getBody()); */

        /* квота для хоста */
        /* $quota = json_decode($client->get("https://api.webmaster.yandex.net/v4/user/{$userId}/hosts/{$hosts->hosts[10]->host_id}/recrawl/quota", $settings)->getBody()); */

        foreach ($urls as $url) {
            $host_id = $this->getHostIdByUrl($url);

            $results[$url] = $client->post(
                "/v4/user/{$user_id}/hosts/{$host_id}/recrawl/queue", ['json' => ['url' => $url]]
            );
        }

        return collect($results);
    }

    public static function getHostIdByDomain(Domain $domain)
    {
        $scheme = $domain->only_https ? 'https' : ($domain->only_http ? 'http' : 'https');
        $port = ($scheme == 'http') ? 80 : 443;
        $host_id = "{$scheme}:{$domain->domain}:{$port}";

        return $host_id;
    }

    public static function getHostIdByUrl($url)
    {
        $parsed_url = parse_url($url);
        $scheme_port = ($parsed_url['scheme'] == 'http') ? 80 : 443;
        $host_id = "{$parsed_url['scheme']}:{$parsed_url['host']}:{$scheme_port}";

        return $host_id;
    }
}
