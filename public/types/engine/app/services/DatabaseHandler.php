<?php

namespace engine\app\services;

use App\Models\Config;
use App\Models\Log;
use engine\app\models\Engine;
use Illuminate\Support\Facades\Log as FacadesLog;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\LogRecord;

class DatabaseHandler extends AbstractProcessingHandler
{
    /**
     * {@inheritDoc}
     */
    protected function write(array|LogRecord $record): void
    {
        if (! Config::cached()->save_logs) {
            return;
        }

        $log = new Log;
        $log->text = $record['message'];
        if (isset($record['context']['exception'])) {
            $log->backtrace = $record['context']['exception']->getFile().':'.$record['context']['exception']->getLine();
        }
        $log->type = $record['level_name'];

        /* TODO: убрать Engine, чтобы не было зацикливания при ошибках в Engine::__construct */
        /* в принципе try catch убирает зацикливание, при проблемах в construct лог все же запишется */
        try {
            $visit = Engine::visit();
        } catch (\Exception $e) {
            $visit = null;
        }

        try {
            /* если есть визит, то ассоциируем его */
            if ($visit) {
                /* сначала сохраняем визит */
                $visit->save();
                /* ассоциируем сохраненный визит с логом */
                $log->visit()->associate($visit);
            }
            $log->save();
        } catch (\Exception $e) {
            /* TODO: если ошибка при записи в этот лог, то надо сообщить об этом в другой лог (текстовый например) */
            FacadesLog::channel('single')->error('Failed to write log to database: '.$e->getMessage(), (is_array($record) ? $record : $record->toArray()));

            return;
        }
    }
}
