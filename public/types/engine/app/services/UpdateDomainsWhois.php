<?php

namespace engine\app\services;

use App\Models\Domain;
use engine\app\models\F;

class UpdateDomainsWhois
{
    public function __invoke(?string $domain = null)
    {

        /* сколько доменов за раз */
        $limit = 5;
        /* таймаут выполнения одного запроса whois перед тем как остановить итерации, в ms */
        $timeout = 10000;

        $domains = Domain::doesntHave('parentDomain')->where('renewed', true)->orderBy('whois_data_updated_at')->limit($limit);

        if ($domain) {
            $domains->where('domain', $domain);
        }

        $domains = $domains->get();

        foreach ($domains as $domain) {

            $overtime = false;

            $time1 = F::runtime();
            // записываем сырой whois
            $domain->whois_data = shell_exec(escapeshellcmd("whois {$domain->domain}"));
            $time2 = F::runtime();

            $time = $time2 - $time1;

            if ($time > $timeout) {
                $overtime = true;
            }

            /* всегда записываем время, даже если данных нет, иначе не будет двигаться очередь, в случае нулевых ответов для какого-то домена */
            $domain->whois_data_updated_at = now();
            $domain->save();

            if ($overtime) {
                logger()->info("Whois update для {$domain->domain} занял больше {$timeout} ms: {$time} ms. Останавливаем итерации.");
                break;
            }
        }

        return 'Обработаны домены: '.implode(', ', $domains->pluck('domain')->toArray());
    }
}
