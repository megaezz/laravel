<?php

namespace engine\app\models;

class Mirrors
{
    // private $type = null;
    private $rows = null;

    public function get()
    {
        // $sql['type'] = $this->getType()?'type=\''.$this->getType().'\'':1;
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS from_domain as `from`,to_domain as `to`,active
			from '.F::tabname('laravel', 'change_domain').'
			order by `to`
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }
}
