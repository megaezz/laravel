<?php

namespace engine\app\models;

class Bans extends Lists
{
    private $ip = null;

    private $user_agent = null;

    private $query = null;

    private $active = null;

    private $hide_player = null;

    private $regexp = null;

    private $strict_search = false;

    private $referer = null;

    public function __construct($id = null)
    {
        $this->setItemClass('Ban');
    }

    public function get()
    {
        // $sql = $this->getSql();
        if ($this->getRegexp()) {
            $this->setOrder('ban.active desc, ban.hide_player desc');
            $sql['ip'] = ! is_null($this->getIp()) ? ('('.($this->getStrictSearch() ? 'ban.ip != \'\' and ' : 'ban.ip = \'\' or ').'\''.F::escape_string($this->getIp()).'\' regexp ban.ip)') : 1;
            $sql['user_agent'] = ! is_null($this->getUserAgent()) ? ('('.($this->getStrictSearch() ? 'ban.user_agent != \'\' and ' : 'ban.user_agent = \'\' or ').'\''.F::escape_string($this->getUserAgent()).'\' regexp ban.user_agent)') : 1;
            $sql['referer'] = ! is_null($this->getReferer()) ? ('('.($this->getStrictSearch() ? 'ban.referer != \'\' and ' : 'ban.referer = \'\' or ').'\''.F::escape_string($this->getReferer()).'\' regexp ban.referer)') : 1;
            $sql['query'] = ! is_null($this->getQuery()) ? ('('.($this->getStrictSearch() ? 'ban.query != \'\' and ' : 'ban.query = \'\' or ').'\''.F::escape_string($this->getQuery()).'\' regexp ban.query)') : 1;
        } else {
            $sql['ip'] = ! is_null($this->getIp()) ? ('ban.ip = \''.F::escape_string($this->getIp()).'\'') : 1;
            $sql['user_agent'] = ! is_null($this->getUserAgent()) ? ('ban.user_agent = \''.F::escape_string($this->getUserAgent()).'\'') : 1;
            $sql['referer'] = ! is_null($this->getReferer()) ? ('ban.referer = \''.F::escape_string($this->getReferer()).'\'') : 1;
            $sql['query'] = ! is_null($this->getIp()) ? ('ban.query = \''.F::escape_string($this->getQuery()).'\'') : 1;
        }
        $sql['active'] = is_null($this->getActive()) ? 1 : ('ban.active = '.$this->getActive() ? 1 : 0);
        $sql['hide_player'] = is_null($this->getHidePlayer()) ? 1 : ('ban.active = '.$this->getHidePlayer() ? 1 : 0);
        $sql = array_merge($sql, $this->getSql());
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS ban.id
			from '.F::tabname('engine', 'ban').'
			where
			'.$sql['ip'].' and
			'.$sql['user_agent'].' and
			'.$sql['referer'].' and
			'.$sql['query'].' and
			'.$sql['active'].' and
			'.$sql['hide_player'].'

			'.$sql['order'].'
			'.$sql['limit'].'
		;');
        $this->setRows(F::rows_without_limit());

        return $arr;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip = null)
    {
        $this->ip = $ip;
    }

    public function getUserAgent()
    {
        return $this->user_agent;
    }

    public function setUserAgent($user_agent = null)
    {
        $this->user_agent = $user_agent;
    }

    public function getReferer()
    {
        return $this->referer;
    }

    public function setReferer($referer = null)
    {
        $this->referer = $referer;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function setQuery($query = null)
    {
        $this->query = $query;
    }

    public function getRegexp()
    {
        return $this->regexp;
    }

    public function setRegexp($b = null)
    {
        $this->regexp = $b;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }

    public function getHidePlayer()
    {
        return $this->hide_player;
    }

    public function setHidePlayer($b = null)
    {
        $this->hide_player = $b;
    }

    public function getStrictSearch()
    {
        return $this->strict_search;
    }

    public function setStrictSearch($b = null)
    {
        $this->strict_search = $b;
    }

    public static function getBanByIp($ip = null)
    {
        $bans = new Bans;
        $bans->setIp($ip);
        $bans->setLimit(1);
        $bans->setRegexp(true);
        $bans->setStrictSearch(true);
        $arr = $bans->get();

        return $arr ? (new Ban($arr[0]['id'])) : false;
    }
}
