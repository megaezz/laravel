<?php

namespace engine\app\models;

use App\Models\Log as EloquentLog;

class Log
{
    private $text = null;

    private $type = null;

    public function add($text = null, $type = null)
    {
        if ($text) {
            $this->setText($text);
        }
        if ($type) {
            $this->setType($type);
        }
        if (! $this->text) {
            F::error('use setText for Log first');
        }

        $log = new EloquentLog;
        $log->text = $this->text;
        $log->ip = request()->ip();
        $log->user_agent = request()->header('User-Agent');
        $log->query = mb_strimwidth(request()->getHost().request()->getRequestUri(), 0, 200);
        $log->referer = mb_strimwidth(request()->header('Referer'), 0, 100);
        $log->requests = 'get: '.serialize(request()->query()).PHP_EOL.'post: '.serialize(request()->post()).PHP_EOL.'cookie: '.serialize(request()->cookie());
        $log->type = $this->type;
        $log->save();
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }
}
