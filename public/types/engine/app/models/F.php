<?php

namespace engine\app\models;

use App\Models\Config;
use App\Models\Ip;
use App\Models\Mailer;
use App\Models\SentMail;
use App\Models\Visit;
use engine\app\models\Db as ModelsDb;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class F
{
    public static function fillDb($query = null)
    {
        $db = new ModelsDb;
        $db->setLink(Engine::getSqlConnection());
        $db->setLogLongQueries(Config::cached()->logLongQueries);
        $db->setQuery($query);

        return $db;
    }

    public static function query_arr($query = null, $key = 'nokey')
    {
        return F::fillDb($query)->getArr($key);
    }

    // запрос в массиве, для кеширования запросов
    public static function query_arr_arg($arr = [])
    {
        if (empty($arr['query'])) {
            F::error('Query required to use F:query_arr_arg');
        }

        return F::query_arr($arr['query']);
    }

    public static function query_assoc($query = null)
    {
        return F::fillDb($query)->getArrAssoc();
    }

    public static function query($query = null, $text = null)
    {
        return F::fillDb($query)->getData();
    }

    public static function query_object_arr($query = null, $key = 'nokey')
    {
        return F::fillDb($query)->getObjectArr($key);
    }

    public static function query_object($query = null)
    {
        return F::fillDb($query)->getObject();
    }

    public static function typetab($table = null)
    {
        return Engine::typetab($table);
    }

    public static function tabname($db = null, $table = null)
    {
        return Engine::tabname($db, $table);
    }

    public static function error($text = null, $logtext = null, $doLog = true, $errorCode = null)
    {
        $e = new Error;
        $e->setText($text);
        $e->setLogText($logtext);
        $e->setLog($doLog);
        if ($errorCode) {
            $e->setHeader($errorCode);
        }
        $e->setDisplayErrorInfo(Engine::getDisplayErrors());

        return $e->get();
    }

    public static function log($text = null)
    {
        $log = new Log;
        $log->setText($text);
        $log->add();
    }

    public static function escape_string($str = null)
    {
        return Engine::getSQLConnection()->escape_string((string) $str);
    }

    public static function checkstr($str = null)
    {
        return static::escape_string($str);
    }

    public static function referer_url()
    {
        return request()->header('Referer', '');
    }

    // точное текущее время в микросекундами
    public static function microtime_float()
    {
        [$usec, $sec] = explode(' ', microtime());

        return (float) $usec + (float) $sec;
    }

    public static function pre($arr)
    {
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
        exit();
    }

    public static function pre_str($var = null)
    {
        ob_start();
        print_r($var);
        $result = ob_get_clean();

        return $result;
    }

    public static function dump($var)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
        exit();
    }

    public static function alert($text = null)
    {
        $t = new Template('alert');
        $t->v('text', $text);
        $t->v('refererURL', static::referer_url());
        exit($t->get());
    }

    public static function alertLite($text = null)
    {
        $t = new Template('alert_lite');
        $t->v('text', $text);
        exit($t->get());
    }

    public static function redirect_301($url = null)
    {
        if (! $url) {
            static::error('Не передан URL');
        }
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: '.$url);
        exit();
    }

    public static function redirect($url = null)
    {
        if (! $url) {
            static::error('Не передан URL');
        }
        header('Location: '.$url);
        exit();
    }

    public static function jsRedirect($url = null, $seconds = 0)
    {
        return '<script>
		setTimeout(function(){window.location.replace(\''.stripcslashes($url).'\')},'.($seconds * 1000).');
		</script>';
    }

    // возвращает количество строк удовлетворяющих запросу, игнорируя limit
    // использовать только с директивой SQL_CALC_FOUND_ROWS в запросе
    public static function rows_without_limit()
    {
        $arr = static::query_assoc('select found_rows()');

        // F::dump($arr);
        return $arr['found_rows()'];
    }

    public static function cache($object = null, $method = null, $args = null, $seconds = null, $key = null, $domain = null, $debug = null)
    {
        // если передан resetCache и включен debugByGet то сбрасываем кеш на странице
        $reset = ((isset($_GET['resetCache']) ? true : false) and Config::cached()->debugByGet) ? true : false;
        // F::dump($reset);

        // if (!app()->bound('cache.model')) {
        //     Lottery::odds(Config::cached()->new_cache_chance, 100)
        //     ->winner(fn() => app()->instance('cache.model', CacheNew::class))
        //     ->loser(fn() => app()->instance('cache.model', Cache::class))
        //     ->choose();
        // }

        // $cacheClass = app('cache.model');
        // $cache = new $cacheClass;

        $cache = new Cache;

        if ($object) {
            $cache->setObject($object);
        }
        if ($method) {
            $cache->setMethod($method);
        }
        if ($args) {
            $cache->setArgs($args);
        }
        if ($key) {
            $cache->setKey($key);
        }
        if (! is_null($seconds)) {
            $cache->setSeconds($seconds);
        }
        if ($domain) {
            $cache->setDomain($domain);
        }
        // pre($cache->result($method,$args,$seconds));
        if ($reset) {
            $cache->reset();
        }
        if ($debug) {
            $cache->setDebug($debug);
        }

        return $cache->result();
    }

    public static function cacheReset($object = null, $method = null, $args = null, $key = null)
    {
        $cache = new Cache;
        if ($object) {
            $cache->setObject($object);
        }
        if ($method) {
            $cache->setMethod($method);
        }
        if ($args) {
            $cache->setArgs($args);
        }
        if ($key) {
            $cache->setKey($key);
        }

        return $cache->reset();
    }

    public static function cacheKey($key = null, $object = null, $method = null, $args = null, $seconds = null)
    {
        return static::cache($object, $method, $args, $seconds, "$key:$method");
    }

    public static function cacheKeyReset($key = null)
    {
        return static::cacheReset(null, null, null, $key);
    }

    public static function cacheFunction($function = null, $args = null, $seconds = null)
    {
        $cache = new Cache;
        if ($function) {
            $cache->setFunction($function);
        }
        if ($args) {
            $cache->setArgs($args);
        }
        if ($seconds) {
            $cache->setSeconds($seconds);
        }

        // pre($cache->result($method,$args,$seconds));
        return $cache->result();
    }

    // возвращает название события, которое выпало на основании заданных процентов. $arr=array('событие'=>30%,..);
    public static function random_event($arr = [])
    {
        if (empty($arr) == 1) {
            static::error('Не переданы необходимые параметры');
        }
        $sum = 0;
        foreach ($arr as $v) {
            $sum = $sum + $v;
        }
        if ($sum != 100) {
            static::error('Неверная сумма процентов');
        }
        $rnd = mt_rand(1, 100);
        $sum = 0;
        foreach ($arr as $k => $v) {
            $min = $sum + 1;
            $max = $min + $v - 1;
            // echo 'если '.$rnd.'>='.$min.' and '.$rnd.'<='.$max.' то событие '.$k.'<br/>';
            if ($rnd >= $min and $rnd <= $max) {
                $event = $k;
                break;
            }
            $sum = $sum + $v;
        }
        if (empty($k) == 1) {
            static::error('Пустое событие');
        }

        return $k;
    }

    // выбирает один из элементов массива (без ключей) случайным образом
    public static function rnd_from_array($arr = [])
    {
        if (! is_array($arr)) {
            static::error('Передан не массив');
        }
        $rnd = rand(0, count($arr) - 1);
        $i = 0;
        foreach ($arr as $k => $v) {
            if ($i == $rnd) {
                $rnd = $v;
                break;
            }
            $i++;
        }

        return $rnd;
    }

    public static function setMenuActiveVars($t = null, $router = null, $arr = null)
    {
        // $router = self::router();
        foreach ($arr as $k => $v) {
            $route = $router->getController().'\\'.$router->getMethod();
            $t->v('menu'.$k.'Active', in_array($route, $v) ? 'active' : '');
        }

        return $t;
    }

    // Альтернатива file_get_contents
    public static function filegetcontents($url, $error = 'default_error', $ua = 'default_ua', $interface = 'default_interface', $proxy = 'default_proxy', $return_info = 'body', $cookies = [], $headers = 'default_headers', $timeout = 10)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $uas['mozilla'] = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.0.10) Gecko/2009042316 MRA 5.4 (build 02620) Firefox/3.0.10';
        $uas['android'] = 'Android';
        $uas['iphone'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) GSA/79.0.259819395 Mobile/16F203 Safari/604.1';
        $uas['ipad'] = 'Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3';
        $uas['default_ua'] = $uas['mozilla'];
        curl_setopt($curl, CURLOPT_USERAGENT, $uas[$ua]);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        // иначе были проблемы с казпочтой с сертефикатами
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        if ($headers != 'default_headers') {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            // pre($headers);
        }
        if ($interface != 'default_interface') {
            curl_setopt($curl, CURLOPT_INTERFACE, $interface);
        }
        $cookies_str = '';
        if (! empty($cookies)) {
            $i = 0;
            foreach ($cookies as $ck => $cv) {
                if ($i != 0) {
                    $cookies_str .= '; ';
                }
                $cookies_str .= $ck.'='.$cv;
                $i++;
            }
            // pre($cookies_str);
            curl_setopt($curl, CURLOPT_COOKIE, $cookies_str);
        }
        $chosed_proxy = $proxy;
        if ($proxy != 'default_proxy' and Config::cached()->proxiesEnabled) {
            $proxies = static::proxies_array();
            $chosed_proxy = $proxies[$proxy];
            curl_setopt($curl, CURLOPT_PROXY, $chosed_proxy);
        }
        if ($return_info == 'headers') {
            curl_setopt($curl, CURLOPT_NOBODY, 1);
            curl_setopt($curl, CURLOPT_HEADER, 1);
        }
        if ($return_info == 'headers_body') {
            curl_setopt($curl, CURLOPT_HEADER, 1);
        }
        // if ($return_info=='return_page' or $return_info=='headers' or $return_info=='return_getinfo') {
        if ($error == 'default_error') {
            // $page=curl_exec($curl) or error('Невозможно загрузить удаленную страницу','Невозможно загрузить URL: "'.$url.'" ('.$chosed_proxy.')');
            $page = curl_exec($curl) or static::error('Невозможно загрузить удаленную страницу', 'Невозможно загрузить URL: "'.curl_error($curl).'" "'.$url.'" ('.$chosed_proxy.')');
        }
        if ($error == 'no_error') {
            $page = curl_exec($curl) or $page = 'error';
            // $page=curl_exec($curl) or die('Контент: '.$page);
        }
        if ($return_info == 'getinfo') {
            $return['body'] = $page;
            curl_setopt($curl, CURLINFO_HEADER_OUT, 1);
            $return['getinfo'] = curl_getinfo($curl);
        } else {
            $return = $page;
        }
        // };
        curl_close($curl);

        // die($page);
        return $return;
    }

    public static function proxies_array()
    {
        $proxies['tor1'] = '127.0.0.1:8118';
        $proxies['tor2'] = '127.0.0.1:8111';
        $proxies['tor3'] = '127.0.0.1:8112';
        $proxies['tor4'] = '127.0.0.1:8113';
        $proxies['tor5'] = '127.0.0.1:8114';
        $proxies['tor6'] = '127.0.0.1:8115';
        $proxies['tor7'] = '127.0.0.1:8116';
        $rnd_proxies = [$proxies['tor1'], $proxies['tor2'], $proxies['tor3'], $proxies['tor4'], $proxies['tor5'], $proxies['tor6'], $proxies['tor7']];
        // $rnd_proxies=array($proxies['tor1'],$proxies['tor3'],$proxies['tor4'],$proxies['tor5'],$proxies['tor7']);
        // $rnd_proxies=array($proxies['tor4']);
        $proxies['rnd'] = static::rnd_from_array($rnd_proxies);
        foreach ($proxies as $k => $v) {
            if ($v == $proxies['rnd']) {
                $rnd_name = $k;
                break;
            }
        }
        $proxies['rnd_name'] = $rnd_name;

        // pre($proxies);
        return $proxies;
    }

    // копирует в строке $string содержимое между $before и $after
    public static function copybetwen($before, $after, $string)
    {
        $result_before = explode($before, $string);
        // if (count($result_before)==1) {error('Нет вхождения "'.$before.'" в строке "'.$string.'"');};
        if (count($result_before) == 1) {
            return false;
        }
        $string = $result_before[1];
        $result_after = explode($after, $string);
        $string = $result_after[0];

        return $string;
    }

    // ucfirst для utf8
    public static function mb_ucfirst($text)
    {
        return mb_strtoupper(mb_substr((string) $text, 0, 1)).mb_substr((string) $text, 1);
    }

    public static function fixLayoutToRus($text = null)
    {
        $arr = [
            'f' => 'а',
            ',' => 'б',
            'd' => 'в',
            'u' => 'г',
            'l' => 'д',
            't' => 'е',
            '`' => 'ё',
            ';' => 'ж',
            'p' => 'з',
            'b' => 'и',
            'q' => 'й',
            'r' => 'к',
            'k' => 'л',
            'v' => 'м',
            'y' => 'н',
            'j' => 'о',
            'g' => 'п',
            'h' => 'р',
            'c' => 'с',
            'n' => 'т',
            'e' => 'у',
            'a' => 'ф',
            '[' => 'х',
            'w' => 'ц',
            'x' => 'ч',
            'i' => 'ш',
            'o' => 'щ',
            ']' => 'ъ',
            's' => 'ы',
            'm' => 'ь',
            '\'' => 'э',
            '.' => 'ю',
            'z' => 'я',
        ];
        // F::dump(mb_strlen($text));
        $newText = null;
        for ($i = 0; $i < mb_strlen($text); $i++) {
            $letter = mb_substr($text, $i, 1);
            $newLetter = isset($arr[$letter]) ? $arr[$letter] : $letter;
            $newText .= $newLetter;
        }

        return $newText;
    }

    // транслитерация русского текста
    public static function translitRu($text = null)
    {
        $t = [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'j',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sch',
            'ъ' => '\'',
            'ы' => 'i',
            'ь' => '\'',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'E',
            'Ж' => 'Zh',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'J',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'H',
            'Ц' => 'C',
            'Ч' => 'Ch',
            'Ш' => 'Sh',
            'Щ' => 'Sch',
            'Ъ' => '\'',
            'Ы' => 'I',
            'Ь' => '\'',
            'Э' => 'E',
            'Ю' => 'Yu',
            'Я' => 'Ya',
        ];
        foreach ($t as $k => $v) {
            $search[] = $k;
            $replace[] = $v;
        }
        $text = str_replace($search, $replace, $text);

        return $text;
    }

    public static function getUsdRubRate()
    {
        // $url = 'https://api.ratesapi.io/api/latest?base=USD&symbols=RUB';
        $url = 'http://api.exchangeratesapi.io/v1/latest?access_key=df62566f010ab9656871200bfb4f6c37';
        $arr = json_decode(file_get_contents($url));
        if (! isset($arr->rates->RUB)) {
            return;
        }

        return $arr->rates->RUB / $arr->rates->USD;
    }

    public static function getUsdRubRateCached()
    {
        return F::cache('engine\app\models\F', 'getUsdRubRate', null, 18000);
    }

    // многопоточная загрузка страниц
    // принимает массив объектов в формате $this->url, $this->... и возвращает этот же массив объектов с $this->result и $this->channel (убрать)
    public static function multiCurl($objects = [])
    {
        // инициализируем мультикурл
        $multi = curl_multi_init();
        // сначала прогружаем все ответы апи через мультикурл
        foreach ($objects as $v) {
            if (isset($v->curl->ch)) {
                $curl = $v->curl->ch;
            } else {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $v->url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                if (isset($v->curl->httpheader)) {
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $v->curl->httpheader);
                }
                if (isset($v->curl->userpwd)) {
                    curl_setopt($curl, CURLOPT_USERPWD, $v->curl->userpwd);
                }
                if (isset($v->curl->connect_timeout)) {
                    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $v->curl->connect_timeout);
                }
                if (isset($v->curl->follow_location)) {
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $v->curl->follow_location);
                }
                if (isset($v->curl->user_agent)) {
                    curl_setopt($curl, CURLOPT_USERAGENT, $v->curl->user_agent);
                }
                if (isset($v->curl->proxy)) {
                    curl_setopt($curl, CURLOPT_PROXY, $v->curl->proxy);
                }
            }
            curl_multi_add_handle($multi, $curl);
            $v->channel = $curl;
        }
        // ждем загрузки всех урлов
        do {
            $status = curl_multi_exec($multi, $active);
            if ($active) {
                curl_multi_select($multi);
            }
        } while ($active && $status == CURLM_OK);
        // записываем ответы и закрываем все дескрипторы
        foreach ($objects as $v) {
            $v->result = curl_multi_getcontent($v->channel);
            curl_multi_remove_handle($multi, $v->channel);
        }
        curl_multi_close($multi);

        return $objects;
    }

    public static function generateRandomStr($length = null)
    {
        $length = $length ? (int) $length : 40;
        $str = '';
        do {
            $str .= sha1(random_bytes(128));
        } while (strlen($str) < $length);
        $str = substr($str, 0, $length);

        // F::dump($str);
        return $str;
    }

    public static function getDiffProperties($oldProp = null, $newProp = null)
    {
        if (! is_array($oldProp)) {
            F::error('Old properties array required to use getDiffProperties');
        }
        if (! is_array($newProp)) {
            F::error('New properties array required to use getDiffProperties');
        }
        $changed = [];
        foreach ($newProp as $prop => $value) {
            // если это объект, то сравнивание не строго
            if (is_object($value)) {
                $matching = $oldProp[$prop] == $value;
            } else {
                $matching = $oldProp[$prop] === $value;
            }
            if (! $matching) {
                $changed[$prop]['old'] = $oldProp[$prop];
                $changed[$prop]['new'] = $value;
            }
        }

        return $changed;
    }

    // для использования в кеше (с аргументами)
    public static function file_get_contents($args = [])
    {
        $filename = empty($args['filename']) ? F::error('Filename required') : $args['filename'];

        return file_get_contents($filename);
    }

    public static function withoutSpaces($str = null)
    {
        return str_replace(' ', '_', $str);
    }

    public static function withSpaces($str = null)
    {
        return str_replace('_', ' ', $str);
    }

    // возвращает массив объектов с содержимым директории
    public static function readDirectory($dir = null, $type = null)
    {
        $arr = [];
        $dh = @opendir($dir) or F::error('Нельзя открыть директорию', 'Нельзя открыть директорию "'.$dir.'"');
        while (($file = readdir($dh)) !== false) {
            if (($file === '.') or ($file === '..')) {
                continue;
            }
            $fileObj = new \stdClass;
            $fileObj->name = $file;
            $fileObj->path = $dir.'/'.$file;
            if (is_file($fileObj->path)) {
                if ($type == 'dirs') {
                    continue;
                }
                $fileObj->type = 'file';
            } else {
                if ($type == 'files') {
                    continue;
                }
                $fileObj->type = 'dir';
            }
            $arr[] = $fileObj;
        }

        return $arr;
    }

    public static function buildQuery($arr = [], $changes = [], $htmlspecialchars = true)
    {
        if ($changes) {
            foreach ($changes as $k => $v) {
                $arr[$k] = $v;
            }
        }
        $result = '/?'.http_build_query($arr);
        if ($htmlspecialchars) {
            $result = htmlspecialchars($result);
        }

        return $result;
    }

    public static function filegetcontentsCached($args = [])
    {
        if (empty($args['url'])) {
            F::error('url required to use F::filegetcontentsCached');
        }

        return file_get_contents($args['url']);
    }

    /* получаем имя хоста по ip, затем проверяем, что хост действительно относится к поисковой системе и находим обратно ip по хосту, если полученный ip равен изначальному, то ip бота валидный. */
    public static function isRealBot($arr = [])
    {
        if (empty($arr['ip'])) {
            return false;
        }

        return Ip::getIpInfo($arr['ip'])->is_real_bot;
    }

    public static function isRealBotCached($ip = null, $log = true)
    {
        // для тестов, при передаче valid_bot_ip считаем ip валидным
        if (request()->input('valid_bot_ip') or request()->cookie('valid_bot_ip')) {
            Cookie::queue('valid_bot_ip', true);
            return true;
        }

        if (empty($ip)) {
            return false;
        }

        $ipModel = Ip::cachedOrCreate($ip);

        /* пишем лог если нужно */
        if (! $ipModel->is_real_bot and $log) {
            logger()->info('Is not real bot');
        }

        return $ipModel->is_real_bot;
    }

    public static function getHostname($arr = [])
    {
        if (empty($arr['ip'])) {
            return false;
        }

        return gethostbyaddr($arr['ip']);
    }

    public static function getHostnameCached($ip = null)
    {
        if (empty($ip)) {
            return false;
        }

        return F::cache('engine\app\models\F', 'getHostname', ['ip' => $ip], null, null, 'awmzone.net');
    }

    public static function getVisitsPerSecond()
    {
        $arr = Visit::query()
            ->selectRaw('round(count(*)/60,1) as requestsPerSec')
            ->whereBetween('date', [DB::raw('date_sub(current_timestamp,interval 1 minute)'), DB::raw('current_timestamp')])
            ->first();

        return $arr->requestsPerSec;
    }

    /* повторять указанное замыкание в течение указанного количества секунд */
    public static function repeatFor(\Closure $closure, $time)
    {
        $start_time = Engine::getRunningTime();
        do {
            $closure();
            $running_time = Engine::getRunningTime() - $start_time;
        } while ($running_time < $time);
    }

    public static function getMgw5La()
    {
        $command = 'ssh megaweb@tunnel_mgw5 uptime';
        exec($command, $output, $returnCode);
        preg_match('/load average: ([\d.]+), ([\d.]+), ([\d.]+)/', ($output[0] ?? ''), $matches);
        if (empty($matches)) {
            throw new \Exception('Не удалось получить данные');
        }

        return [
            (float) $matches[1],
            (float) $matches[2],
            (float) $matches[3],
        ];
    }

    /* выводит время прошедшее с момента начала генерации скрипта, до текущего момента и комменатрий, если указан */
    public static function runtime($comment = null)
    {
        $time = (int) (round(microtime(true) - LARAVEL_START, 3) * 1000);

        return $comment ? ($time.'ms ('.$comment.')') : $time;
    }

    /* ищет представление в шаблонах и внутри типа */
    public static function view($blade)
    {
        $typeTemplate = str_replace('templates/', '', Engine::getDomainEloquent()->type_template);
        $type = Engine::getDomainEloquent()->type;
        $insideTemplate = "types.{$type}.templates.{$typeTemplate}.{$blade}";
        $insideType = "types.{$type}.{$blade}";
        if (View::exists($insideTemplate)) {
            return view($insideTemplate);
        }

        return view($insideType);
    }

    public static function generateDmca()
    {
        return '~'.substr(bin2hex(openssl_random_pseudo_bytes(4)), 0, 2).'-'.date('d-m');
    }

    /* метод для рендеренга blade шаблона без создания компилированного файла, иначе (если просто через Blade::render) забивается к хуям место на диске, вместо этого компилируем налету, захватываем через ob_start и выводим без сохранения */
    public static function render($path = null, $data = [])
    {

        $compiledTemplate = Blade::compileString($path);

        /* чтобы работала директива @foreach и другие */
        $data['__env'] = app('view');

        // Старт буфера вывода
        ob_start();
        // Выполнение скомпилированного шаблона
        extract($data);
        try {
            eval('?>'.$compiledTemplate);
        } catch (\Throwable $e) {
            logger()->critical("Ошибка eval: {$e->getMessage()}, compiledTemplate: ".Str::limit($compiledTemplate, 200));
        }

        return ob_get_clean();
    }

    // обновляет local_id для переданной таблицы и переданного условия (например domain = 'some.domain')
    public static function updateLocalIds($args = [])
    {
        if (empty($args['table'])) {
            F::error('Не передана таблица');
        }
        if (empty($args['where_clause'])) {
            F::error('Не передано условие');
        }
        if (empty($args['max_updates'])) {
            $args['max_updates'] = 0;
        }
        // получаем максимальный local_id
        $arr = F::query_assoc('
			select ifnull(local_id,0) as local_id
			from '.$args['table'].'
			where '.$args['where_clause'].'
			order by local_id desc limit 1;
			');
        $max_local_id = $arr['local_id'];
        $changed = 0;
        do {
            $max_local_id++;
            F::query('update '.$args['table'].' set local_id=\''.$max_local_id.'\'
				where '.$args['where_clause'].' and local_id is null
				order by id asc
				limit 1;');
            $affected = Engine::getSqlConnection()->affected_rows;
            if ($affected > 0) {
                $changed++;
            }
            if ($args['max_updates'] != 0 and $changed >= $args['max_updates']) {
                break;
            }
        } while ($affected > 0);
        F::log('Обновлены local_id для таблицы "'.$args['table'].'" и условия "'.$args['where_clause'].'". Затронуто строк: '.$changed.'.');

        return $changed;
    }

    public static function sendMail($email = null, $name = null, $subject = null, $body = null, $unsubscribe_link = null, ?Mailer $mailer = null)
    {
        // Настройки PHPMailer
        $mail = new \PHPMailer\PHPMailer\PHPMailer;

        if (! $mailer) {
            throw new \Exception('Не передан mailer');
        }

        $result = false;
        $hash = hash('sha256', $email.$name.$subject.$body);
        $mail->isSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->SMTPAuth = true;
        $mail->Host = $mailer->host;
        $mail->Username = $mailer->username;
        $mail->Password = $mailer->password;
        $mail->SMTPSecure = $mailer->encryption ?? false;
        $mail->Timeout = 3;
        $mail->Port = $mailer->port;
        $mail->setFrom(Engine::getDomainEloquent()->email, $name);
        $mail->addAddress($email);
        // добавляем отписку
        if ($unsubscribe_link) {
            $mail->addCustomHeader('List-Unsubscribe', "<{$unsubscribe_link}>");
        }
        // Отправка сообщения
        $mail->isHTML(true);
        $mail->Subject = $subject;
        // $mail->Body = $body;
        // чтобы появилась текстовая версия письма и не было ошибки MIME_HTML_ONLY в mail-tester.com
        $mail->msgHTML($body);

        $sentMail = SentMail::where('hash', $hash)->orderBy('created_at', 'desc')->first();

        $timeout = false;

        if ($sentMail) {
            $timeout = $sentMail->created_at->diffInSeconds() < 300;
        }

        $sentMail = new SentMail;

        try {
            if ($timeout) {
                throw new \Exception('timeout');
            }
            // Отправляем письмо
            $result = $mail->send() ? true : false;
            logger()->info("Отправка почты: {$mail->Subject}");

        } catch (\Exception $e) {
            logger()->info("Ошибка отправки сообщения: {$e->getMessage()}");
            $sentMail->error_info = json_encode($e->getMessage() ?? null);
            $result = false;
        }

        $sentMail->email = $email;
        $sentMail->name = $name;
        $sentMail->subject = $subject;
        $sentMail->body = $body;
        $sentMail->mailer_id = $mailer->id;
        $sentMail->result = $result;
        $sentMail->hash = $hash;
        $sentMail->save();

        return $result;
    }

    public static function getMonths()
    {
        return [
            '01' => ['nominative' => 'январь', 'genitive' => 'января'],
            '02' => ['nominative' => 'февраль', 'genitive' => 'февраля'],
            '03' => ['nominative' => 'март', 'genitive' => 'марта'],
            '04' => ['nominative' => 'апрель', 'genitive' => 'апреля'],
            '05' => ['nominative' => 'май', 'genitive' => 'мая'],
            '06' => ['nominative' => 'июнь', 'genitive' => 'июня'],
            '07' => ['nominative' => 'июль', 'genitive' => 'июля'],
            '08' => ['nominative' => 'август', 'genitive' => 'августа'],
            '09' => ['nominative' => 'сентябрь', 'genitive' => 'сентября'],
            '10' => ['nominative' => 'октябрь', 'genitive' => 'октября'],
            '11' => ['nominative' => 'ноябрь', 'genitive' => 'ноября'],
            '12' => ['nominative' => 'декабрь', 'genitive' => 'декабря'],
        ];
    }

    public static function setPageVars($t = null)
    {
        $t->v('currentYear', date('Y'));
        $t->v('currentDomain', Engine::getDomain());
        $t->v('httpsCurrentDomain', 'https://'.Engine::getDomain());
        $t->v('httpCurrentDomain', 'http://'.Engine::getDomain());
        $t->v('lastRedirectDomain', Engine::getDomainObject()->getLastRedirectDomain());
        $t->v('siteName', Engine::getDomainObject()->getSiteName());
        $t->v('serverTime', date('H:i'));
        $t->v('runningTime', Engine::getRunningTime());
        $t->v('queryCount', ModelsDb::getQueryCount());
        $t->v('reload', Engine::getReload());
        $t->v('title', Engine::router()->title);
        $t->v('description', Engine::router()->description);
        $t->v('keywords', Engine::router()->keywords);
        $t->v('h1', Engine::router()->h1);
        $t->v('h2', Engine::router()->h2);
        $t->v('topText', Engine::router()->topText);
        $t->v('bottomText', Engine::router()->bottomText);
        $t->v('text1', Engine::router()->text1);
        $t->v('text2', Engine::router()->text2);
        $t->v('breadCrumbs', Engine::router()->bread);
        $t->v('http', 'http://');
        $t->v('https', 'https://');
        $t->v('requestUri', request()->getRequestUri() ?? '');
        $t->v('requestedDomain', Engine::getRequestedDomainObject()->getDomain());
        $t->v('sitePath', '/sites/'.Engine::getDomain().'/favicon.png');
        // dd(view('types.engine.metrika')->render(), Engine::getDomainObject()->getMetrika());
        // $t->v('metrika',Engine::getDomainObject()->getMetrika());
        $t->v('metrika', view('types.engine.metrika')->render());
        $t->v('logoSrc', Engine::getDomainObject()->getLogoSrc());
        $t->v('faviconSrc', Engine::getDomainObject()->getFaviconSrc());
        $t->v('bgSrc', Engine::getDomainObject()->getBgSrc() ?? '/template/images/bg.jpg');
        $t->v('protocol', Engine::getDomainObject()->getProtocol().'://');
        $t->v('requestedProtocol', request()->getScheme().'://');
        $t->v('ruDomain', Engine::getDomainObject()->getRuDomain());
        $t->v('nextRuDomain', Engine::getDomainObject()->getNextRuDomain());
        $t->v('csrf', function_exists('csrf_field') ? csrf_field() : '');

        // F::dump($t->getVar('metrika'));
        // $t->v('',);
        return true;
    }
}
