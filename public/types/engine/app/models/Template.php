<?php

namespace engine\app\models;

class Template
{
    protected $vars = [];

    protected $content = null;

    protected static $usedTemplates = [];

    protected $lang = null;

    protected $path = null;

    // protected $path=null;
    protected $encoding = null;

    public function __construct($path = null)
    {
        if ($path) {
            $this->setPath($path);
        }
    }

    public function setVar($var = null, $value = null, $lang = null)
    {
        if (! $var) {
            F::error('Var required');
        }
        if (isset($this->vars[$lang][$var])) {
            F::error('Variable '.$var.' for lang "'.$lang.'" was already set in Template');
        }
        $this->vars[$lang][$var] = $value;
    }

    public function setVars($object = null)
    {
        if (! $object) {
            F::error('Object is required');
        }
        foreach ($object as $k => $v) {
            $this->setVar($k, $v);
            // $this->vars[$k] = $v;
        }
    }

    // синоним для setVar
    public function v($var = null, $value = null)
    {
        // если $value - массив, то значит переданы языки
        if (is_array($value)) {
            foreach ($value as $lang => $v) {
                $this->setVar($var, $v, $lang);
            }
        } else {
            $this->setVar($var, $value);
        }
    }

    public function get($path = null, array $data = [])
    {
        /* TODO: временно для совместимости, если не указан домен, то подтягиваем текущий */
        if (empty($data['domain'])) {
            $data['domain'] = Engine::getDomainEloquent();
        }

        return F::render($this->oldGet($path), $data);
    }

    public function oldGet($path = null)
    {
        if ($path) {
            $this->setPath($path);
        }
        if (is_null($this->content)) {
            F::error('Null content in template');
        }
        // $pattern_obj_func='/(?!\/\/)\{\{([\w]+)\:\:([\w]+)}\}/';
        $pattern_controller = '/(?!\/\/)\{([\w\\\]+)\:\:([\w]+)\}/';
        $pattern_comment = '/\/\/\{\{?\(?([\w\.\/\-():\\\]+)\)?\}?\}/';
        $pattern_temp = '/(?!\/\/)\{([\w\.\/-]+)\.(html?)\}/i';
        // $pattern_func='/(?!\/\/)\{([\w]+)\}/';
        // $pattern_func_param='/(?!\/\/)\{([\w]+)\(([\w\.\/()\[\]\'\'-]+)\)\}/'; //в параметрах добавлены квадратные скобки и кавычки для get_var
        $pattern_var = '/(?!\/\/)\{\(?(([\w]+)(\([\w]+\))?)\)?\}/';
        // $pattern_var = '/(?!\/\/)\{([\w]+)\}/';
        $pattern_multilang = '/(?!\/\/)\{((([\w]+)=\[([\w {}(),.\-\—\!\?<>\/~:\;"\'=«»&\\\\]*)\]\|?)+)\}/u';
        // $pattern_multilang = '/(?!\/\/)\{((([\w]+)=\[(.*)\]\|?)+)\}/u';
        // $pattern_since_date='/\{([\w]+)\|([\w\.\/]+)\.([\w]*)\\|([\w\.\/]+)\.([\w]*)\}/e';
        // есть проблема. если func_replacement_callback возвращает закомментированную строку с шаблоном, то она все равно заменяется шаблоном,
        // т.к. выполняется следом temp_replacement_callback. В качестве решения - например, если какой то preg_replace_callback отработал, то
        // ставить continue и цикл наченется опять сверху, тем самым должно сработать комментрирование. но возможно это вызовет лишнюю нагрузку.
        // протестировать.
        $content = $this->content;
        do {
            // поменял местами comment_replacement_callback и get_var_callback, чтобы комментировались переменные. проследить как работает
            $content = preg_replace_callback($pattern_comment,
                function ($m) {
                    return $this->commentReplacement($m[1]);
                }, $content);
            // F::dump($content);
            $content = preg_replace_callback($pattern_multilang,
                function ($m) {
                    return $this->multilangReplacement($m);
                }, $content);
            // F::dump($content);
            $content = preg_replace_callback($pattern_var,
                function ($m) {
                    return $this->getVar($m[1]);
                }, $content);
            // $content=preg_replace_callback($pattern_obj_func,array($this,'obj_func_replacement_callback'),$content);
            $content = preg_replace_callback($pattern_controller,
                function ($m) {
                    // F::dump($m);
                    return $this->controllerReplacement($m[1], $m[2]);
                }, $content);
            // $content=preg_replace_callback($pattern_func,array($this,'func_replacement_callback'),$content);
            // $content=preg_replace_callback($pattern_func_param,array($this,'func_replacement_param_callback'),$content);
            $content = preg_replace_callback($pattern_temp,
                function ($m) {
                    // F::dump($m);
                    return $this->templateReplacement($m[1], $m[2]);
                }, $content);
        } while (
            (preg_match($pattern_multilang, $content)) or
            (preg_match($pattern_var, $content)) or
            (preg_match($pattern_comment, $content)) or
            // (preg_match($pattern_func,$content)==1) or
            // (preg_match($pattern_obj_func,$content)==1) or
            (preg_match($pattern_controller, $content)) or
            // (preg_match($pattern_func_param,$content)==1) or
            (preg_match($pattern_temp, $content))
        );

        return $content;
    }

    public function setContent($content = null)
    {
        $this->content = $content;
    }

    public function setPath($path = null, $ext = 'html')
    {
        if (! $path) {
            F::error('Null path at Template::setPath');
        }
        $pattern_temp = '/([\w\.\/\-]+)(\.(html?))/';
        preg_match($pattern_temp, $path, $matches);
        if (isset($matches[3])) {
            $path = $matches[1];
            $ext = $matches[3];
        }
        $this->path = $path.'.'.$ext;
        $path = $this->path;
        if (isset(self::$usedTemplates[$path])) {
            $this->content = self::$usedTemplates[$path];

            return;
        }
        $site = Engine::getRootPath().'/sites/'.Engine::getDomain().'/'.$path;
        $type = Engine::getRootPath().'/types/'.Engine::getType().'/template/'.$path;
        // если объект домена создан
        if (empty(Engine::getDomainObject())) {
            $typeTemplate = null;
        } else {
            $typeTemplate = Engine::getRootPath().'/types/'.Engine::getType().'/template/'.Engine::getDomainObject()->getTypeTemplate().'/'.$path;
        }
        $engine = Engine::getRootPath().'/types/engine/template/'.$path;
        if (file_exists($site)) {
            $resPath = $site;
        } else {
            if (file_exists((string) $typeTemplate)) {
                $resPath = $typeTemplate;
            } else {
                if (file_exists($type)) {
                    $resPath = $type;
                } else {
                    if (file_exists($engine)) {
                        $resPath = $engine;
                    } else {
                        F::error('Page doesn\'t exist', 'Template '.$path.' is not available', 1, 404);
                    }
                }
            }
        }
        $content = file_get_contents($resPath);
        // движок работает в utf-8 поэтому исправляем кодировку файлов если нужно
        // в правильной ли кодировке файл?
        $check = mb_check_encoding($content, 'utf-8');
        // если нет, то исправляем ее
        if (! $check) {
            $fromEncoding = null;
            // если исходная кодировка windows-1251, то переводим ее в необходимую
            if (mb_check_encoding($content, 'windows-1251')) {
                $fromEncoding = 'windows-1251';
            }
            if ($fromEncoding) {
                $content = mb_convert_encoding($content, 'utf-8', $fromEncoding);
            } else {
                F::error('Wrong encoding of content, can\'t fix');
            }
        }
        self::$usedTemplates[$path] = $content;
        $this->content = $content;
    }

    public function commentReplacement($page)
    {
        return '';
    }

    public function templateReplacement($path = null, $ext = null)
    {
        $this->setPath($path, $ext);

        return $this->oldGet();
    }

    public function controllerReplacement($controller = null, $method = null)
    {
        $foundController = engine::findClassMethod('controllers', $controller.'Controller', $method);
        if ($foundController) {
            $object = new $foundController;

            return call_user_func_array([$object, $method], []);
        }
        F::error('Controller replacement didn\'t find controller '.$controller.' or method '.$method);
    }

    public function getVar($var = null)
    {
        // $var=strtolower($var);
        // if (!isset($this->vars[$var])) {
        // если переменная недоступна ни в каком языке, то выдаем ошибку
        if (! $this->isSet($var)) {
            F::error('Переменная '.$var.' недоступна', 'Переменная '.$var.' (lang = "'.$this->getLang().'") недоступна в шаблоне '.($this->getPath() ? $this->getPath() : htmlspecialchars($this->getContent())));
        }
        // если переменная доступна в требуемом языке, то выводим ее, если нет, то выводим любую доступную
        if (isset($this->vars[$this->getLang()][$var])) {
            return $this->vars[$this->getLang()][$var];
        } else {
            foreach ($this->vars as $v) {
                if (array_key_exists($var, $v)) {
                    return $v[$var];
                }
            }
        }
    }

    public function multilangReplacement($arr = null)
    {
        $str = empty($arr[1]) ? null : $arr[1];
        // F::dump($str);
        // получаем переводы
        $pattern = '/([\w]+)=\[([\w {}(),.\-\—\!\?<>\/~:\;"\'=«»&\\\\]*)\]\|?/u';
        // $pattern = '/([\w]+)=\[(.*)\]\|?/u';
        preg_match_all($pattern, $str, $matches);
        // f::dump($matches);
        $lang = [];
        foreach ($matches[1] as $k => $m) {
            $lang[$m] = $matches[2][$k];
        }
        // F::dump(array_key_first($lang));
        $sel_lang = $this->getLang() ? $this->getLang() : 'ru';
        if (! isset($lang[$sel_lang])) {
            return array_shift($lang);
        }

        // F::dump($lang[$sel_lang]);
        return $lang[$sel_lang];
        // F::dump($lang);
        // return $value;
    }

    public function isSet($var = null)
    {
        foreach ($this->vars as $v) {
            if (array_key_exists($var, $v)) {
                return true;
            }
        }

        return false;
    }

    // function setVars($args=array()) {
    // 	foreach ($args as $k=>$v) {
    // 		$this->setVar($k,$v);
    // 	};
    // }

    // заменяет фигурные скобки на html сущности
    public function vf($var = null, $value = null)
    {
        if (is_array($value)) {
            foreach ($value as $lang => $v) {
                $v = str_replace('{', '&#123;', $v);
                $v = str_replace('}', '&#125;', $v);
                $this->setVar($var, $v, $lang);
            }
        } else {
            $value = str_replace('{', '&#123;', $value);
            $value = str_replace('}', '&#125;', $value);
            $this->setVar($var, $value);
        }
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setLang($lang = null)
    {
        $this->lang = $lang;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function getPath()
    {
        return $this->path;
    }
}
