<?php

namespace engine\app\models;

use App\Models\Config;

class Authorization
{
    private $type = null;

    private $sid = null;

    private $levels = [];

    private $userAgent = null;

    private $login = null;

    private $password = null;

    private $error = null;

    private $level = null;

    public function __construct($type = null, $levels = [])
    {
        $this->setType($type);
        $this->setLevels($levels);
        $this->userAgent = request()->header('User-Agent');
    }

    public function check()
    {
        if (! $this->type) {
            $this->setError('Set engine type for authorization');

            return false;
        }
        if (! $this->sid) {
            if ($this->getCookieSID()) {
                $this->setSID($this->getCookieSID());
            } else {
                $this->setError('Authorization is required', false);

                return false;
            }
        }
        // проверяем sid
        $arr = (array) \DB::table("{$this->type}.user_sessions")
            ->selectRaw('users.login,users.level,users.timezone,unix_timestamp(user_sessions.last_activity) as unix_date,
			user_sessions.sid,user_sessions.user_agent')
            ->join("{$this->type}.users", 'users.login', '=', 'user_sessions.login')
            ->where('user_sessions.sid', $this->sid)
            ->first();
        // $arr = F::query_assoc('select users.login,users.level,users.timezone,
        // 	unix_timestamp(user_sessions.last_activity) as unix_date, user_sessions.sid,
        // 	user_sessions.user_agent
        // 	from '.F::tabname($this->type,'user_sessions').'
        // 	join '.F::tabname($this->type,'users').' on users.login=user_sessions.login
        // 	where user_sessions.sid=\''.F::escape_string($this->sid).'\'
        // 	;');
        // если нет такой сессии - уходим
        if (! $arr) {
            // удаляем куки (чтобы постоянно ошибка в auth_failures не писалась, например если очищена база сессий)
            $this->clearSID();
            $this->setError('SID doesn\'t exist');

            return false;
        }
        $this->login = $arr['login'];
        $this->level = $arr['level'];
        // проверяем не устарела ли сессия
        $date = getdate();
        // если таймаут наступил
        if (($date[0] - $arr['unix_date']) > Config::cached()->sessionTimeout) {
            $this->setError('Session timeout');
            $this->clearSID();

            return false;
        }
        // проверяем браузер
        if ($arr['user_agent'] !== $this->userAgent) {
            $this->setError('Wrong browser');
            $this->clearSID();

            return false;
        }
        // проверяем имеет ли пользователь нужные права доступа
        if (! in_array($arr['level'], $this->levels)) {
            $this->setError($this->login.' you do not have permissions to this action');

            return false;
        }
        // все проверки пройдены
        // создаем объект юзера, хотя видимо это можно сделать раньше, сейчас нет времени разбираться
        // $user = new User($this->getLogin());
        // выставляем временную зону если нужно (хотел сделать но закомментировать, т.к. не класс User может быть в разных типах, надо это учесть)
        // if ($user->getTimezone()) {
        //     F::query('SET time_zone = \''.$user->getTimeZone().'\';');
        // }
        \DB::table("{$this->type}.user_sessions")
            ->where('sid', $this->sid)
            ->update([
                'cookies' => serialize(empty($_COOKIE) ? [] : $_COOKIE),
                'last_activity' => \DB::raw('current_timestamp'),
            ]);

        // F::query('
        //     update '.F::tabname($this->type,'user_sessions').' set last_activity=current_timestamp,
        // 	cookies=\''.F::escape_string(serialize(empty($_COOKIE)?array():$_COOKIE)).'\'
        // 	where sid=\''.F::escape_string($this->sid).'\'
        //     ');
        return true;
    }

    public function enter()
    {
        if (! $this->password) {
            $this->setError('Password needed');

            return false;
        }
        if (! $this->login) {
            $this->setError('Login needed');

            return false;
        }
        if (! $this->type) {
            $this->setError('Type needed');

            return false;
        }
        $arr = F::query_assoc('
    		select id,login,pass,level
    		from '.F::tabname($this->type, 'users').'
    		where login=\''.F::escape_string($this->login).'\'
    			;');
        if (! $arr) {
            $this->setError('User was not found');

            return false;
        }
        if ($this->password !== $arr['pass']) {
            $this->setError('Password is not correct');

            return false;
        }
        // авторизация успешна, генерируем sid
        do {
            $sid = F::generateRandomStr(40);
            // проверяем вдруг в базе уже есть такой sid
            $checkSid = F::query_assoc('select sid from '.F::tabname($this->type, 'user_sessions').' where sid=\''.$sid.'\';');
        } while (! empty($checkSid));
        $this->sid = $sid;
        F::query('insert into '.F::tabname($this->type, 'user_sessions').' set
    		sid=\''.$this->sid.'\',
    		login=\''.$this->login.'\',
    		user_agent=\''.F::escape_string($this->userAgent).'\',
    		ip=\''.F::escape_string(request()->ip()).'\',
    		last_activity=current_timestamp
    		;');
        // устанавливаем куки с ID сессии на год
        setcookie('mgw_sid_'.$this->type, $this->sid, time() + 3600 * 24 * 365, '/');
        logger()->info('Authorizated '.$this->login.' in '.$this->type);

        return $this->check();
    }

    public function setError($message = null, $log = true)
    {
        if ($log) {
            F::query('insert into '.F::tabname('laravel', 'auth_failures').' set
                ip=\''.F::escape_string(request()->ip()).'\',
                vars=\''.F::escape_string(serialize($this)).'\',
                description=\''.F::escape_string($message).'\'
                ;');
        }
        $this->error = $message;
    }

    public function getError()
    {
        return $this->error;
    }

    public function clearSID()
    {
        if (! $this->type) {
            F::error('Type needed to clear SID');
        }
        setcookie('mgw_sid_'.$this->type, '', 1);
    }

    public function setType($type = null)
    {
        if (! in_array($type, (new Domains)->getTypes())) {
            F::error('Such type doesn\'t exist');
        }
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setSID($sid = null)
    {
        $this->sid = $sid;
    }

    public function getCookieSID()
    {
        if (! $this->type) {
            F::error('Type needed to getCookieSID');
        }

        return isset($_COOKIE['mgw_sid_'.$this->type]) ? $_COOKIE['mgw_sid_'.$this->type] : null;
    }

    public function setLevels($arr = [])
    {
        $this->levels = $arr;
    }

    public function setLogin($login = null)
    {
        $this->login = $login;
    }

    public function setPassword($password = null)
    {
        $this->password = $password;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getLevel()
    {
        return $this->level;
    }
}
