<?php

namespace engine\app\models;

use App\Models\Cache as CacheEloquent;
use App\Models\Config;

// решить вопрос с упорядочиванием свойств объекта по алфавиту можно через get_object_vars и затем ksort и генерировать хэш не от объекта а от имени объекта и отдельно упорядоченных свойств, пока не стал применять это решение, т.к. есть другой вариант - всегда объявлять значения по умолчанию в классах, тогда все свойства будут стоять в том порядке, который указан в __construct. понаблюдать и затем принять решение
class Cache
{
    // private $seconds = 43200;
    private $seconds = null;

    private $args = [];

    private $type = 'function';

    private $useDB = true;

    private $function = null;

    private $object = null;

    // имя класса, для статичного выполнения и для таблицы cache
    private $class = null;

    private $startTime = null;

    private $endTime = 0;

    private $timeRange = null;

    private $domain = null;

    private $engineType = null;

    private static $cache = [];

    private $key = null;

    private $isStaticMethod = null;

    private $hashStr = null;

    private $debug = false;

    private $keepClonedObjects = true;

    private static $clonedObjects = [];

    public function __construct()
    {
        $this->startTime = F::microtime_float();
        $this->useDB = Config::cached()->cacheUseDB;
        // pre(Engine::getInstance());
        $this->setEngineType(engine::getType());
        $this->setDomain(engine::getDomain());
        $this->setSeconds(Config::cached()->cacheSeconds);
    }

    // function reset($function=null,$args=array()) {
    public function reset()
    {
        // if ($function) {$this->setFunction($function);};
        // if ($args) {$this->setArgs($args);};
        $hash = $this->getHash();
        // F::query('update '.F::tabname('engine','cacheObject').' set
        // 	reset = 1 where hash = \''.$hash.'\' and domain = \''.$this->domain.'\'
        // 	;');
        $rows = CacheEloquent::where('key', $hash)->update(['reset' => true]);
        if (! $rows) {
            return false;
        }
        logger()->info('Reset cache '.$hash.'. Domain: '.$this->domain);

        return true;
    }

    // function result($function=null,$args=array(),$seconds=null) {
    public function result()
    {
        $result = null;
        $is_result = false;
        $action = 'select';
        // if ($function) {$this->setFunction($function);};
        // if ($args) {$this->setArgs($args);};
        // if (isset($seconds)) {$this->setSeconds($seconds);};
        $hash = $this->getHash();
        // проверяем есть ли запись в локальном кеше
        if (isset(self::$cache[$hash])) {
            // pre('local');
            if ($this->getDebug()) {
                // logger()->info('взяли из кэша класса для '.$this->class.'::'.$this->function);
            }
            $result = self::$cache[$hash];

            return $result;
        }
        // если можно использовать БД то проверяем есть ли там нужная запись
        if ($this->useDB) {
            // $arr = F::query_assoc('
            // select SQL_NO_CACHE result,unix_timestamp(date) as date,reset,date as date4logs
            // from '.F::tabname('engine','cacheObject').'
            // where hash = \''.$hash.'\' and domain = \''.$this->domain.'\'
            // ;');
            $arr = CacheEloquent::where('key', $hash)->first();
            // если требуемый кеш уже есть в базе
            if ($arr) {
                $date = getdate();
                // если время вышло, то апдейт
                if ((($date[0] - $arr->updated_at->timestamp) >= $this->seconds) or $arr->reset) {
                    $action = 'update';
                } else {
                    // если время не вышло, то забираем результат из базы
                    $result = unserialize($arr->value);
                    $is_result = true;
                }
                // добавить условие - если требуемый кеш есть в базе, но в данный момент он высчитывается, то ждать
            } else {
                // если кеша нет в базе то высчитываем и записываем
                $action = 'insert';
            }
            if ($this->getDebug()) {
                logger()->info($action.' для '.$this->engineType.':'.$hash.' ('.(($action == 'insert') ? 0 : ($date[0] - $arr->updated_at->timestamp)).' прошло из '.$this->seconds.')');
            }
        }
        // если результата не было ни в локальном кеше ни в БД, либо в БД он устарел, то выполняем функцию или метод
        if (! $is_result) {
            $result = $this->run();
        }
        $this->endTime = F::microtime_float();
        $this->timeRange = $this->endTime - $this->startTime;
        if ($this->useDB) {
            $serialized_result = serialize($result);
            if ($action == 'insert') {
                // ignore т.к. появлялись ошибки повторной вставки
                // F::query('insert ignore into '.F::tabname('engine','cacheObject').' set
                // 	domain = \''.$this->domain.'\',
                // 	hash = \''.$hash.'\',
                // 	time = \''.$this->timeRange.'\',
                // 	result = \''.F::escape_string($serialized_result).'\'
                // 	;');
                CacheEloquent::insertOrIgnore([
                    'key' => $hash,
                    'time' => $this->timeRange,
                    'value' => $serialized_result,
                    'updated_at' => now(),
                ]);
                /*
                function = \''.$this->function.'\',
                    class = '.($this->class?('\''.F::escape_string($this->class).'\''):'null').',
                    args = \''.F::escape_string(json_encode($this->args)).'\',
                    engineType = \''.$this->engineType.'\',
                    hashStr = \''.F::escape_string($this->hashStr).'\'
                */

            }
            if ($action == 'update') {
                // указываем дату, т.к. если результат не изменился но mysql оставит старую дату
                // F::query('update '.F::tabname('engine','cacheObject').' set
                // 	result = \''.F::escape_string($serialized_result).'\',
                // 	reset = 0,
                // 	time = \''.$this->timeRange.'\',
                // 	date = current_timestamp
                // 	where hash = \''.$hash.'\' and domain = \''.$this->domain.'\'
                // 	;');
                CacheEloquent::where('key', $hash)->update([
                    'value' => $serialized_result,
                    'reset' => false,
                    'time' => $this->timeRange,
                    'updated_at' => now(),
                ]);
            }
        }
        self::$cache[$hash] = $result;

        return $result;
    }

    public function setObject($object = null)
    {
        // если задаем объект значит объявляем тип объект
        $this->setType('object');
        if (is_object($object)) {
            $this->object = $object;
            $this->class = get_class($object);
        } else {
            if (is_string($object)) {
                $this->class = $object;
                // $this->object = $object;
                $this->isStaticMethod = true;
            } else {
                F::error('Object is required to create cache');
            }
        }
        // $this->object=is_object($object)?$object:F::error('Object is required to create cache');
    }

    public function setEngineType($type = '')
    {
        $this->engineType = empty($type) ? F::error('Set engine type') : $type;
    }

    public function setDomain($domain = '')
    {
        $this->domain = empty($domain) ? F::error('Set domain') : $domain;
    }

    public function setSeconds($seconds = null)
    {
        if (! isset($seconds)) {
            F::error('Seconds is required');
        }
        if ($seconds == 0) {
            $this->useDB = false;
        }
        $this->seconds = $seconds;
    }

    public function setType($type = '')
    {
        $types = ['object', 'function'];
        if (! in_array($type, $types)) {
            F::error('Wrong cache type');
        }
        $this->type = $type;
    }

    public function getHash()
    {
        if ($this->key) {
            return $this->key;
        }
        if (empty($this->function)) {
            F::error('Set function name');
        }
        if (empty($this->type)) {
            F::error('Set type');
        }
        if (empty($this->engineType)) {
            F::error('Set engine type');
        }
        if ($this->type == 'function') {
            $str = $this->engineType.$this->function.serialize($this->args).$this->domain;
        }
        if ($this->type == 'object') {
            // $str = $this->engineType.json_encode($this->object).$this->function.json_encode($this->args);
            $str = $this->engineType.serialize($this->object).$this->function.serialize($this->args).$this->domain;
            // F::dump($this->object);
        }
        $hash = hash('sha256', $str);
        // для теста
        $this->hashStr = $str;

        // echo '<li>'.pre_str($this->object).' ||| '.$str.'</li>';
        return $hash;
    }

    public function run()
    {
        if (empty($this->type)) {
            F::error('Set type');
        }
        if (empty($this->function)) {
            F::error('Set function');
        }
        $function = $this->function;
        if ($this->type == 'function') {
            if (! function_exists($this->function)) {
                F::error('Function doesn\'t exist', 'Function "'.$this->function.'" doesn\'t exist');
            }
            $result = $function($this->args);
        }
        if ($this->type == 'object') {
            // если метод вызывается статично
            if ($this->isStaticMethod) {
                if (! method_exists($this->class, $this->function)) {
                    F::error('Method doesn\'t exist');
                }
                $result = $this->class::$function($this->args);
                // если метод вызывается через объект
            } else {
                if (! isset($this->object) or ! is_object($this->object)) {
                    F::error('Wrong object');
                }
                if (! method_exists($this->object, $this->function)) {
                    F::error('Method doesn\'t exist');
                }
                // если стоит настройка хранить клонированные объекты
                if ($this->getKeepClonedObjects()) {
                    $clonedObject = null;
                    // ищем в хранилище переданный экземпляр класса и если находим, используем его имеющийся клон
                    foreach (self::$clonedObjects as $v) {
                        if ($v['object'] == $this->object) {
                            if ($this->getDebug()) {
                                // logger()->info('используем клонированный объект для '.$this->class.'::'.$this->function);
                            }
                            // берем из хранилища клонированный объект соответствующий запрошенному объекту и работаем с ним, чтобы не было излишнего выполнения методов
                            $clonedObject = $v['cloned'];
                        }
                    }
                    // если нет клона, то клонируем объект, записываем в хранилище.
                    // Клонирование введено для того, чтобы избежать ситуации когда при выполнении метода в классе меняются свойства, например F::cache(Movies::getRows) пишет в Movies->rows = 123), и следующий вызов например F::cache(getList) закешируется с учетом того, что rows=123, а нам это не нужно, т.к. при повторном вызове страницы, данные будут браться из кеша и Movies::rows будет = 0, а значит будет заново расчитываться кэш. Чтобы убрать эту излишнюю работу, мы клонируем объекты и выполняем метод клона, и он никак не меняет свойств исходного объекта.
                    if (! $clonedObject) {
                        if ($this->getDebug()) {
                            // logger()->info('клонируeм объект для '.$this->class.'::'.$this->function);
                        }
                        $clonedObject = clone $this->object;
                        self::$clonedObjects[] = ['object' => clone $this->object, 'cloned' => $clonedObject];
                    }
                } else {
                    $clonedObject = clone $this->object;
                }
                $result = $clonedObject->$function($this->args);
            }
        }

        return $result;
    }

    public function setFunction($function = '')
    {
        $this->function = empty($function) ? F::error('Specify function name') : $function;
    }

    // псевдоним для красоты если работаем с классами
    public function setMethod($method = '')
    {
        $this->setFunction($method);
    }

    public function setArgs($args = [])
    {
        if (! is_array($args)) {
            F::error('Arguments should be an array');
        }
        ksort($args);
        $this->args = $args;
    }

    public function setKey($key = null)
    {
        $this->key = $key;
    }

    public function getObject()
    {
        return $this->object;
    }

    public static function getClonedObjects()
    {
        return self::$clonedObjects;
    }

    public function setDebug($b = null)
    {
        $this->debug = $b;
    }

    public function getDebug()
    {
        return $this->debug;
    }

    public function getKeepClonedObjects()
    {
        return $this->keepClonedObjects;
    }

    public function setKeepClonedObjects($b = null)
    {
        $this->keepClonedObjects = $b;
    }
}
