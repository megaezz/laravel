<?php

namespace engine\app\models;

class Comments
{
    private $item_id = null;

    private $page = null;

    private $limit = null;

    private $order = 'asc';

    private $active = null;

    private $status = null;

    private $listTemplate = null;

    private $rows = null;

    private $domain = null;

    private $item_field_name = null;

    private $sql_table = null;

    public function get()
    {
        $orders['asc'] = 'order by date';
        $orders['desc'] = 'order by date desc';
        $sql['order'] = isset($orders[$this->order]) ? $orders[$this->order] : F::error('Order of videoComments was not recognized');
        $sql['active'] = is_null($this->active) ? '1' : ($this->active ? 'active = 1' : 'active = 0');
        $sql['status'] = is_null($this->status) ? '1' : 'status = \''.$this->status.'\'';
        if ($this->limit) {
            if (! $this->page) {
                F::error('Use setPage to use limit');
            }
            $from = ($this->page - 1) * $this->limit;
            $sql['limit'] = 'limit '.$from.','.$this->limit.'';
        } else {
            $sql['limit'] = '';
        }
        $sql['itemId'] = $this->getItemId() ? ($this->getItemFieldName().' = \''.$this->getItemId().'\'') : 1;
        if ($this->getDomain()) {
            $sql['domain'] = 'domain_movies.domain = \''.$this->getDomain().'\'';
            $sql['joinDomainMovies'] = 'join '.F::typetab('domain_movies').' on domain_movies.id = movie_comments.domain_movie_id';
        } else {
            $sql['domain'] = 1;
            $sql['joinDomainMovies'] = '';
        }
        $arr = F::query_arr('
			select movie_comments.id
			from '.F::typetab('movie_comments').'
			'.$sql['joinDomainMovies'].'
			where
			'.$sql['domainMovieId'].'
			and '.$sql['domain'].'
			and '.$sql['active'].'
			and '.$sql['status'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getList()
    {
        if (! $this->listTemplate) {
            F::error('Use setListTemplate first to use Comments::getList');
        }
        $arr_says = ['говорит', 'молвит', 'уверен, что', 'кричит', 'шепчет', 'стонет'];
        $arr = $this->get();
        $list = '';
        foreach ($arr as $v) {
            $comment = new Comment($v['id']);
            $t = new Template($this->listTemplate);
            $t->v('itemId', $comment->getItemId());
            $t->v('date', $comment->getDate());
            $t->vf('text', htmlspecialchars($comment->getText()));
            $t->vf('username', htmlspecialchars($comment->getUsername()));
            $t->v('says', $arr_says[array_rand($arr_says)]);
            $date = new \DateTime($comment->getDate());
            // выводим дату на русском
            $formatter = new \IntlDateFormatter(
                'ru_RU',
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::SHORT,
                'Europe/Moscow'
            );
            $t->v('ruDate', $formatter->format($date));
            $list .= $t->get();
        }

        return $list;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setStatus($status = null)
    {
        $this->status = $status;
    }

    public function setActive($active = null)
    {
        $this->active = $active;
    }

    public function getItemId()
    {
        return $this->item_id;
    }

    public function setItemId($itemId = null)
    {
        $this->itemId = $itemId;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }
}
