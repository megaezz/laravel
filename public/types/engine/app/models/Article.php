<?php

namespace engine\app\models;

class Article
{
    // private $type = null;
    private $id = null;

    private $title = null;

    private $text = null;

    private $textPreview = null;

    private $addDate = null;

    private $applyDate = null;

    private $domain = null;

    private $url = null;

    private $staticPath = null;

    private $imagesPath = null;

    protected const TYPE = 'engine';

    protected const STATIC_PATH = 'types/engine/articles';

    private $chpu = null;

    private $mainImage = null;

    private $active = null;

    private $imagesUrl = null;

    public function __construct($id = null)
    {
        if (! $id) {
            F::error('Specify ID to create Article class');
        }
        $arr = F::query_assoc('
			select id,title,text,add_date,apply_date,url,domain,main_image,text_preview,active,chpu
			from '.F::tabname(static::TYPE, 'articles').'
			where id=\''.$id.'\'
			;');
        if (! $arr) {
            F::error('Article is not exist');
        }
        $this->id = $arr['id'];
        $this->title = $arr['title'];
        $this->text = $arr['text'];
        $this->textPreview = $arr['text_preview'];
        $this->addDate = $arr['add_date'];
        $this->applyDate = $arr['apply_date'];
        $this->url = $arr['url'];
        $this->domain = $arr['domain'];
        $this->mainImage = $arr['main_image'];
        $this->staticPath = Engine::getStaticPath().'/'.static::STATIC_PATH.'/'.$this->id;
        $this->imagesPath = $this->staticPath.'/images';
        $this->imagesUrl = Engine::getStaticUrl().'/'.static::STATIC_PATH.'/'.$this->id.'/images';
        $this->active = $arr['active'] ? true : false;
        $this->chpu = $arr['chpu'];
    }

    public static function getStaticPath()
    {
        return static::STATIC_PATH;
    }

    public static function add()
    {
        F::query('insert into '.F::tabname(static::TYPE, 'articles').' set id = null;');
        $id = Engine::getSqlConnection()->insert_id;
        if (! is_dir(Engine::getStaticPath().'/'.static::STATIC_PATH)) {
            mkdir(Engine::getStaticPath().'/'.static::STATIC_PATH);
        }
        mkdir(Engine::getStaticPath().'/'.static::STATIC_PATH.'/'.$id);
        mkdir(Engine::getStaticPath().'/'.static::STATIC_PATH.'/'.$id.'/images');

        return $id;
    }

    public function delete()
    {
        $isDirEmpty = count(glob($this->imagesPath.'/*')) ? false : true;
        if (! $isDirEmpty) {
            F::alert('Delete all images from article before deleting an article');
        }
        rmdir($this->imagesPath);
        rmdir($this->staticPath);
        F::query('delete from '.F::tabname(static::TYPE, 'articles').' where id=\''.$this->id.'\';');
    }

    public static function getIdByUrl($url = null)
    {
        $arr = F::query_assoc('select id from '.F::tabname(static::TYPE, 'articles').' where url = \''.$url.'\' limit 1;');

        return $arr['id'];
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setTitle($title = null)
    {
        $this->title = $title;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function getId()
    {
        return $this->id;
    }

    public function save()
    {
        F::query('
			update '.F::tabname(static::TYPE, 'articles').'
			set
			title = \''.F::escape_string($this->getTitle()).'\',
			text = \''.F::escape_string($this->getText()).'\',
			text_preview = \''.F::escape_string($this->getTextPreview()).'\',
			main_image = \''.F::escape_string($this->getMainImage()).'\',
			domain = '.($this->getDomain() ? '\''.F::escape_string($this->getDomain()).'\'' : 'null').',
			apply_date = '.($this->getApplyDate() ? '\''.F::escape_string($this->getApplyDate()).'\'' : 'null').',
			active = '.($this->getActive() ? 1 : 0).',
			chpu = \''.F::escape_string($this->getChpu()).'\'
			where id = \''.$this->getId().'\'
			');
    }

    public function getImagesUrl()
    {
        return $this->imagesUrl;
    }

    public function getImages()
    {
        return array_diff(scandir($this->imagesPath), ['..', '.', '.DS_Store']);
    }

    public function getMainImage()
    {
        return $this->mainImage;
    }

    public function setMainImage($file = null)
    {
        $this->mainImage = $file;
    }

    public function getMainImageUrl()
    {
        return $this->getMainImage() ? ($this->getImagesUrl().'/'.$this->getMainImage()) : null;
    }

    public function getTextPreview()
    {
        return $this->textPreview;
    }

    public function setTextPreview($text = null)
    {
        $this->textPreview = $text;
    }

    public function getAddDate()
    {
        return $this->addDate;
    }

    public function setApplyDate($date = null)
    {
        $this->applyDate = $date;
    }

    public function getApplyDate()
    {
        return $this->applyDate;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }

    public function getChpu()
    {
        return $this->chpu;
    }

    public function setChpu($chpu = null)
    {
        $this->chpu = $chpu;
    }

    public function setId($id = null)
    {
        $this->id = $id;
    }

    public function setAddDate($date = null)
    {
        $this->addDate = $date;
    }

    public function setStaticPath($path = null)
    {
        $this->staticPath = $path;
    }

    public function setImagesPath($path = null)
    {
        $this->imagesPath = $path;
    }

    public function setImagesUrl($url = null)
    {
        $this->imagesUrl = $url;
    }
}
