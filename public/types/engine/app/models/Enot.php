<?php

namespace engine\app\models;

class Enot
{
    private $api_key = null;

    private $wallet_id = null;

    // кошелек для вывода
    private $purse = null;

    private $amount = null;

    // примечание
    private $desc = null;

    // валюта для вывода
    private $currency = null;

    // вкл/выкл автоматический обмен
    private $disable_exchange = true;

    public function request($data = [])
    {
        if (empty($data)) {
            F::error('Array of args required to use FkWallet::request');
        }
        if (empty($this->getApiKey())) {
            F::error('ApiKey required to use FkWallet::request');
        }
        // $data['sign'] = md5($this->getWalletId().$this->getApiKey());
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.fkwallet.ru/api_v1.php');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = trim(curl_exec($ch));
        $c_errors = curl_error($ch);
        curl_close($ch);
        $response = json_decode($result, true);

        return $response;
    }

    public function cashout()
    {
        $this->getWalletId() ?? F::error('WalletId required');
        $this->getPurse() ?? F::error('Purse required');
        $this->getAmount() ?? F::error('Amount required');
        $this->getDesc() ?? F::error('Desc required');
        $this->getCurrency() ?? F::error('Currency required');
        $data = [
            'wallet_id' => $this->getWalletId(),
            'purse' => $this->getPurse(),
            'amount' => $this->getAmount(),
            'desc' => $this->getDesc(),
            'currency' => $this->getCurrency(),
            'sign' => md5($this->getWalletId().$this->getCurrency().$this->getAmount().$this->getPurse().$this->getApiKey()),
            'action' => 'cashout',
        ];

        return $this->request($data);
    }

    public function getApiKey()
    {
        return $this->api_key;
    }

    public function setApiKey($api_key = null)
    {
        $this->api_key = $api_key;
    }

    public function getWalletId()
    {
        return $this->wallet_id;
    }

    public function setWalletId($wallet_id = null)
    {
        $this->wallet_id = $wallet_id;
    }

    public function getPurse()
    {
        return $this->purse;
    }

    public function setPurse($purse = null)
    {
        $this->purse = $purse;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount = null)
    {
        $this->amount = $amount;
    }

    public function getDesc()
    {
        return $this->desc;
    }

    public function setDesc($desc = null)
    {
        $this->desc = $desc;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($currency = null)
    {
        $this->currency = $currency;
    }

    public static function getTimeInt($integer = null)
    {
        return (int) (time().$integer);
    }

    public static function getFromTimeInt($timeint = null)
    {
        return (int) (mb_substr($timeint, 10));
    }
}
