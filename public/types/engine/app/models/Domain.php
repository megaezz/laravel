<?php

namespace engine\app\models;

use App\Models\Domain as DomainEloquent;

class Domain implements \JsonSerializable
{
    private $domain = null;

    private $type = null;

    private $isOn = null;

    // на случай смены $this->domain в процессе работы
    private $domainID = null;

    private $auth = null;

    private $www = null;

    private $httpsOnly = null;

    private $httpOnly = null;

    // на какую версию движка указывает домен
    private $engineVersion = null;

    // reborn - так помечены домены которые экспайрились и с них нужно забрать контент для новых сайтов.
    private $reborn = null;

    private $ruBlock = null;

    private $expired = null;

    private $mirrorOf = null;

    private $redirectTo = null;

    private $typeTemplate = null;

    private $yandexRedirect = null;

    private $googleRedirect = null;

    private $clientRedirect = null;

    private $siteName = null;

    // хранит последний домен в цепочке редиректов
    private $lastRedirectDomain = null;

    private $clientRedirectTo = null;

    private $registrar = null;

    // объект родительского домена
    private $parentDomain = null;

    // мультиязычность (чтобы выводить данные о домене на указанном языке)
    private $lang = null;

    private $addDate = null;

    private $metrika_id = null;

    private $videoroll_id = null;

    private $logoSrc = null;

    private $faviconSrc = null;

    private $bgSrc = null;

    private $comment = null;

    private $brand = null;

    private $yandexAllowed = null;

    private $movieads_id = null;

    private $access_scheme = null;

    private $ru_domain = null;

    private $next_ru_domain = null;

    public function __construct($domain = null, ?DomainEloquent $domainEloquent = null)
    {
        if ($domainEloquent) {
            $arr = $domainEloquent->getAttributes();
        } else {
            if (! $domain) {
                F::error('Specify domain first to use Domain class');
            }
            $arr = $this->getInfo($domain);
        }

        if (! $arr) {
            // throw new \Exception ("Domain {$domain} doesn't exist");
            // исключение выдает 500 а мне хочется 404, поэтому abort
            abort(404, "Domain {$domain} doesn't exist");
        }
        $this->domain = $arr['domain'];
        $this->type = $arr['type'];
        $this->isOn = $arr['on'] ? true : false;
        $this->domainID = $this->domain;
        $this->auth = $arr['auth'] ? true : false;
        $this->www = $arr['www'];
        $this->httpsOnly = $arr['only_https'] ? true : false;
        $this->httpOnly = $arr['only_http'] ? true : false;
        $this->reborn = $arr['reborn'] ? true : false;
        $this->engineVersion = $arr['engine'];
        $this->comment = $arr['comment'];
        $this->ruBlock = $arr['ru_block'] ? true : false;
        $this->expired = $arr['expired'];
        $this->mirrorOf = $arr['mirror_of'];
        $this->redirectTo = $arr['redirect_to'];
        $this->typeTemplate = $arr['type_template'];
        $this->yandexRedirect = $arr['yandex_redirect'] ? true : false;
        $this->googleRedirect = $arr['google_redirect'] ? true : false;
        $this->clientRedirect = $arr['client_redirect'] ? true : false;
        $this->siteName = $arr['site_name'];
        $this->clientRedirectTo = $arr['client_redirect_to'];
        $this->registrar = $arr['registrar'];
        $this->addDate = $arr['add_date'];
        // $this->metrika_id = F::query_assoc('select metrika_id from '.F::tabname('engine','domain_metrikas').' where domain = \''.$this->domain.'\';')['metrika_id']??null;
        $this->metrika_id = $arr['metrika_id'];
        // $this->metrika_id = DomainMetrika::find($this->domain)->metrika_id ?? null;
        $this->videoroll_id = $arr['videoroll_id'];
        $this->logoSrc = $arr['logo_src'];
        $this->faviconSrc = $arr['favicon_src'];
        $this->bgSrc = $arr['bg_src'];
        $this->brand = $arr['brand'] ? true : false;
        $this->yandexAllowed = $arr['yandexAllowed'] ? true : false;
        $this->movieads_id = $arr['movieads_id'] ?? null;
        $this->access_scheme = $arr['access_scheme'];
        $this->ru_domain = $arr['ru_domain'];
        $this->next_ru_domain = $arr['next_ru_domain'];
    }

    final public static function isExist($domain = null)
    {
        if (! $domain) {
            F::error('Specify domain first to use Domain::isExist');
        }
        $arr = self::getInfo($domain);

        return $arr ? true : false;
    }

    final public static function getInfo($domain = null)
    {
        if (! $domain) {
            F::error('Specify domain first');
        }
        $arr = F::query_assoc('
			select domain,type,`on`,auth,www,only_https,only_http,reborn,engine,comment,ru_block,expired, mirror_of,redirect_to,type_template, yandex_redirect,
			google_redirect, client_redirect, site_name, client_redirect_to, registrar,add_date,
			metrika_id, videoroll_id, logo_src, favicon_src, bg_src, brand, yandexAllowed, movieads_id,
			access_scheme, ru_domain, next_ru_domain
			from '.F::tabname('laravel', 'domains').'
			where domain=\''.$domain.'\'
			;');

        return $arr;
    }

    final public function routes()
    {
        $routes = new Routes;
        $routes->setDomain($this->domain);

        return $routes;
    }

    final public static function add($domain = null)
    {
        if (! $domain) {
            F::error('Set domain to use Domain::add');
        }
        if (self::isExist($domain)) {
            F::error('Domain is already exist');
        }
        F::query('
			insert into '.F::tabname('laravel', 'domains').' set
			domain = \''.$domain.'\'
			');

        return true;
    }

    public function save()
    {
        $sql['domain'] = 'domain=\''.$this->getDomain().'\'';
        $sql['type'] = 'type=\''.$this->getType().'\'';
        $sql['on'] = $this->getIsOn() ? '`on`=1' : '`on`=0';
        $sql['auth'] = $this->getAuth() ? 'auth=1' : 'auth=0';
        $sql['httpsOnly'] = $this->getHttpsOnly() ? 'only_https=1' : 'only_https=0';
        $sql['httpOnly'] = $this->getHttpOnly() ? 'only_http=1' : 'only_http=0';
        $sql['www'] = 'www=\''.$this->getWww().'\'';
        $sql['engineVersion'] = 'engine = \''.$this->getEngineVersion().'\'';
        $sql['reborn'] = $this->getReborn() ? 'reborn = 1' : 'reborn = 0';
        $sql['comment'] = $this->getComment() ? ('comment = \''.$this->getComment().'\'') : 'comment = null';
        $sql['mirrorOf'] = is_null($this->getMirrorOf()) ? 'mirror_of = null' : 'mirror_of=\''.$this->getMirrorOf().'\'';
        $sql['redirectTo'] = is_null($this->getRedirectTo()) ? 'redirect_to = null' : 'redirect_to=\''.$this->getRedirectTo().'\'';
        $sql['clientRedirectTo'] = is_null($this->getClientRedirectTo()) ? 'client_redirect_to = null' : 'client_redirect_to=\''.$this->getClientRedirectTo().'\'';
        $sql['yandexRedirect'] = $this->getYandexRedirect() ? 'yandex_redirect = 1' : 'yandex_redirect = 0';
        $sql['googleRedirect'] = $this->getGoogleRedirect() ? 'google_redirect = 1' : 'google_redirect = 0';
        $sql['clientRedirect'] = $this->getClientRedirect() ? 'client_redirect = 1' : 'client_redirect = 0';
        $sql['siteName'] = is_null($this->getSiteName()) ? 'site_name = null' : 'site_name=\''.$this->getSiteName().'\'';
        $sql['typeTemplate'] = is_null($this->getTypeTemplate()) ? 'type_template = null' : 'type_template=\''.$this->getTypeTemplate().'\'';
        $sql['metrikaId'] = is_null($this->getMetrikaId()) ? 'metrika_id = null' : 'metrika_id = \''.$this->getMetrikaId().'\'';
        $sql['videorollId'] = is_null($this->getVideorollId()) ? 'videoroll_id = null' : 'videoroll_id = \''.$this->getVideorollId().'\'';
        $sql['logoSrc'] = is_null($this->getLogoSrc()) ? 'logo_src = null' : 'logo_src = \''.$this->getLogoSrc().'\'';
        $sql['faviconSrc'] = is_null($this->getFaviconSrc()) ? 'favicon_src = null' : 'favicon_src = \''.$this->getFaviconSrc().'\'';
        $sql['bgSrc'] = is_null($this->getBgSrc()) ? 'bg_src = null' : 'bg_src = \''.$this->getBgSrc().'\'';
        $sql['ruBlock'] = 'ru_block = '.($this->getRuBlock() ? 1 : 0);
        $sql['brand'] = 'brand = '.($this->getBrand() ? 1 : 0);
        F::query('
			update '.F::tabname('laravel', 'domains').' set
			'.$sql['domain'].',
			'.$sql['type'].',
			'.$sql['on'].',
			'.$sql['auth'].',
			'.$sql['www'].',
			'.$sql['httpsOnly'].',
			'.$sql['httpOnly'].',
			'.$sql['engineVersion'].',
			'.$sql['reborn'].',
			'.$sql['comment'].',
			'.$sql['mirrorOf'].',
			'.$sql['redirectTo'].',
			'.$sql['clientRedirectTo'].',
			'.$sql['yandexRedirect'].',
			'.$sql['googleRedirect'].',
			'.$sql['clientRedirect'].',
			'.$sql['siteName'].',
			'.$sql['typeTemplate'].',
			'.$sql['metrikaId'].',
			'.$sql['videorollId'].',
			'.$sql['logoSrc'].',
			'.$sql['faviconSrc'].',
			'.$sql['bgSrc'].',
			'.$sql['ruBlock'].',
			'.$sql['brand'].'
			where
			domain = \''.$this->domainID.'\'
			');

        return true;
    }

    final public function setType($type = null)
    {
        $this->type = $type;
    }

    final public function setIsOn($b = null)
    {
        $this->isOn = $b;
    }

    final public function getType()
    {
        return $this->type;
    }

    final public function getIsOn()
    {
        return $this->isOn;
    }

    final public function getDomain()
    {
        return $this->domain;
    }

    final public function delete()
    {
        F::query('delete from '.F::tabname('laravel', 'domains').' where domain=\''.$this->domainID.'\';');

        return true;
    }

    final public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    final public function getAuth()
    {
        return $this->auth;
    }

    final public function setAuth($b = null)
    {
        $this->auth = $b;
    }

    final public function getWww()
    {
        return $this->www;
    }

    final public function setWww($v = null)
    {
        $this->www = $v;
    }

    final public function getHttpsOnly()
    {
        return $this->httpsOnly;
    }

    final public function setHttpsOnly($b = null)
    {
        $this->httpsOnly = $b;
    }

    final public function getHttpOnly()
    {
        return $this->httpOnly;
    }

    final public function setHttpOnly($b = null)
    {
        $this->httpOnly = $b;
    }

    final public function getReborn()
    {
        return $this->reborn;
    }

    final public function getEngineVersion()
    {
        return $this->engineVersion;
    }

    final public function setReborn($b = null)
    {
        $this->reborn = $b;
    }

    final public function setEngineVersion($version = null)
    {
        $this->engineVersion = $version;
    }

    final public function getComment()
    {
        return $this->comment;
    }

    final public function setComment($comment = null)
    {
        $this->comment = $comment;
    }

    public function getMirrors()
    {
        $domains = new Domains;
        $domains->setShowMirrorsFor($this->getDomain());

        return $domains->get();
    }

    public function setRuBlock($b = null)
    {
        $this->ruBlock = $b;
    }

    public function getRuBlock()
    {
        return $this->ruBlock;
    }

    public function getExpired()
    {
        return $this->expired;
    }

    public function setExpired($b = null)
    {
        $this->expired = $b;
    }

    public function getMirrorOf()
    {
        return $this->mirrorOf;
    }

    public function setMirrorOf($b = null)
    {
        $this->mirrorOf = $b;
    }

    public function getRedirectTo()
    {
        return $this->redirectTo;
    }

    public function setRedirectTo($b = null)
    {
        $this->redirectTo = $b;
    }

    public function getTypeTemplate()
    {
        return $this->typeTemplate;
    }

    public function setTypeTemplate($template = null)
    {
        $this->typeTemplate = $template;
    }

    public function getYandexRedirect()
    {
        return $this->yandexRedirect;
    }

    public function setYandexRedirect($b = null)
    {
        $this->yandexRedirect = $b;
    }

    public function getGoogleRedirect()
    {
        return $this->googleRedirect;
    }

    public function setGoogleRedirect($b = null)
    {
        $this->googleRedirect = $b;
    }

    public function getClientRedirect()
    {
        return $this->clientRedirect;
    }

    public function setClientRedirect($b = null)
    {
        $this->clientRedirect = $b;
    }

    public function getSiteName()
    {
        return $this->siteName;
    }

    public function setSiteName($siteName = null)
    {
        $this->siteName = $siteName;
    }

    /* меняем логику, теперь этот метод не возвращает последний редирект домена, а возвращает домен для бота */
    public function getLastRedirectDomain()
    {
        /*
        if (!$this->lastRedirectDomain) {
            $domain = $this;
            while ($domain->getRedirectTo() and $domain->getRedirectTo() != $domain->getDomain()) {
                $domain = new self($domain->getRedirectTo());
            }
            $this->lastRedirectDomain = $domain->getDomain();
        }
        return $this->lastRedirectDomain;
        */
        if (! $this->lastRedirectDomain) {
            $this->lastRedirectDomain = DomainEloquent::findOrFail($this->getDomain())->getCrawlerDomain(Engine::getCrawler())->domain;
            // dd($this->lastRedirectDomain);
        }

        return $this->lastRedirectDomain;
    }

    public function setLastRedirectDomain($domain = null)
    {
        $this->lastRedirectDomain = $domain;
    }

    public function getClientRedirectTo()
    {
        return $this->clientRedirectTo;
    }

    public function setClientRedirectTo($domain = null)
    {
        $this->clientRedirectTo = $domain;
    }

    public function getRegistrar()
    {
        return $this->registrar;
    }

    // если это субдомен то выдается объект родительского домена
    public function getParentDomain()
    {
        $arr = explode('.', $this->domain);
        if (count($arr) > 2) {
            $arr = array_reverse($arr);
            $parentDomain = $arr[1].'.'.$arr[0];

            // $parentDomain = new Domain($arr[1].'.'.$arr[0]);
            return $parentDomain;
        }

        return null;
    }

    public function getParentDomainObject()
    {
        if ($this->getParentDomain()) {
            return new Domain($this->getParentDomain());
            // if (Domain::isExist($this->getParentDomain())) {
            // 	return new Domain($this->getParentDomain());
            // } else {
            // 	return null;
            // }
        }

        return null;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang = null)
    {
        $this->lang = $lang;
    }

    public function getDomainDecoded()
    {
        return idn_to_utf8($this->getDomain());
    }

    public function getAddDate()
    {
        return $this->addDate;
    }

    public function getMetrikaId()
    {
        return $this->metrika_id;
    }

    public function setMetrikaId($id = null)
    {
        $this->metrika_id = $id;
    }

    public function getVideorollId()
    {
        return $this->videoroll_id;
    }

    public function setVideorollId($id = null)
    {
        $this->videoroll_id = $id;
    }

    public function getProtocol()
    {
        if ($this->getHttpOnly()) {
            return 'http';
        }
        if ($this->getHttpsOnly()) {
            return 'https';
        }

        return 'https';
    }

    public function getLogoSrc()
    {
        return $this->logoSrc;
    }

    public function setLogoSrc($logoSrc = null)
    {
        $this->logoSrc = $logoSrc;
    }

    public function getFaviconSrc()
    {
        return $this->faviconSrc;
    }

    public function setFaviconSrc($faviconSrc = null)
    {
        $this->faviconSrc = $faviconSrc;
    }

    public function getBgSrc()
    {
        return $this->bgSrc;
    }

    public function setBgSrc($bgSrc = null)
    {
        $this->bgSrc = $bgSrc;
    }

    public function getNextMirror()
    {
        $domain = $this->getDomain();
        preg_match('/^([a-z\-]{1,20})([\d]*)\.(.+)$/', $domain, $arr);
        // F::dump($arr);
        if (empty($arr[1])) {
            return false;
        }
        $mirror['sub_name'] = $arr[1];
        $mirror['sub_digit'] = $arr[2];
        $mirror['sub_domain'] = $arr[3];
        // if ($mirror['sub_name'] == 'nu') {
        // 	$mirror['sub_name'] = 'n';
        // }
        if (! $mirror['sub_digit']) {
            $mirror['sub_digit'] = 0;
        }
        $mirror['sub_digit']++;

        // F::dump($mirror);
        return $mirror['sub_name'].$mirror['sub_digit'].'.'.$mirror['sub_domain'];
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($b = null)
    {
        $this->brand = $b;
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        $domain = [
            'domain' => $this->getDomain(),
            'domainDecoded' => $this->getDomainDecoded(),
            'type' => $this->getType(),
            'typeTemplate' => $this->getTypeTemplate(),
            'yandexRedirect' => $this->getYandexRedirect(),
            'googleRedirect' => $this->getGoogleRedirect(),
            'clientRedirect' => $this->getClientRedirect(),
            'redirectTo' => $this->getRedirectTo(),
            'clientRedirectTo' => $this->getClientRedirectTo(),
            'videorollId' => $this->getVideorollId(),
            'faviconSrc' => $this->getFaviconSrc(),
            'bgSrc' => $this->getBgSrc(),
            'logoSrc' => $this->getLogoSrc(),
            'brand' => $this->getBrand(),
            'addDate' => $this->getAddDate(),
            'metrikaId' => $this->getMetrikaId(),
            'comment' => $this->getComment(),
            'ruBlock' => $this->getRuBlock(),
            'siteName' => $this->getSiteName(),
            'isOn' => $this->getIsOn(),
            'mirrorOf' => $this->getMirrorOf(),
            'nextMirror' => $this->getNextMirror(),
            'parentDomain' => $this->getParentDomain(),
            'addDateISO8601' => (new \DateTime($this->getAddDate()))->format('c'),
            'yandexAllowed' => $this->getYandexAllowed(),
            'access_scheme' => $this->getAccessScheme(),
            'ru_domain' => $this->getNextRuDomain(),
            'next_ru_domain' => $this->getNextRuDomain(),
        ];

        return (object) $domain;
    }

    public function getYandexAllowed()
    {
        return $this->yandexAllowed;
    }

    public function getMovieadsId()
    {
        return $this->movieads_id;
    }

    public function getAccessScheme()
    {
        return $this->access_scheme;
    }

    public function getRuDomain()
    {
        return $this->ru_domain;
    }

    public function getNextRuDomain()
    {
        return $this->next_ru_domain;
    }
}
