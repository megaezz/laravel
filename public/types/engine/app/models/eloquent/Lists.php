<?php

// класс для создания классов-списков

namespace engine\app\models\eloquent;

use engine\app\models\F;
use engine\app\models\PaginatorCustom;

class Lists
{
    private $limit = null;

    private $page = null;

    private $order = null;

    private $rows = null;

    private $listTemplate = null;

    private $itemClass = null;

    private $listTemaplate = null;

    private $paginationUrlPattern = null;

    private $query = null;

    public function get()
    {
        return $this->getQuery()->get();
    }

    public function getQuery()
    {
        return is_null($this->query) ? $this->getItemClass()::query() : $this->query;
    }

    public function setQuery($query = null)
    {
        $this->query = $query;

        return $this->getQuery();
    }

    public function getList()
    {
        if (! $this->getListTemplate()) {
            F::error('use setListTemplate');
        }
        $list = '';
        $arr = $this->get();
        foreach ($arr as $v) {
            $t = new Template($this->getListTemplate());
            $this->setListVars($t, $v);
            $list .= $t->get();
        }

        return $list;
    }

    protected function setListVars($template = null, $itemObject = null)
    {
        // $t->v('var','value');
    }

    public function getRows()
    {
        // return $this->rows;
        return $this->getQuery()->count();
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;

        return $limit ? $this->setQuery($this->getQuery()->limit($limit)) : $this->getQuery();
    }

    public function setPage($page = null)
    {
        $this->page = $page;

        return $page ? $this->setQuery($this->getQuery()->offset(($this->page - 1) * $this->getLimit())) : $this->getQuery();
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
        if (! $order) {
            return $this->getQuery();
        }
        $explode = explode(',', $order);
        foreach ($explode as $v) {
            if (strstr($v, ' desc')) {
                $result = $this->getQuery()->orderByDesc(trim(str_replace(' desc', '', $v)));
            } else {
                $result = $this->getQuery()->orderByDesc(trim(str_replace(' asc', '', $v)));
            }
        }

        return $this->setQuery($result);
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getPages()
    {
        if (! $this->getPaginationUrlPattern()) {
            F::error('setPaginationUrlPattern first');
        }
        $Paginator = new PaginatorCustom;
        $Paginator->setUrlPattern($this->getPaginationUrlPattern());
        $Paginator->setTemplateList('pagination/list');
        $Paginator->setTemplatePrev('pagination/prev');
        $Paginator->setTemplateNext('pagination/next');
        $Paginator->setRows($this->getRows());
        $Paginator->setLimit($this->getLimit());
        $Paginator->setPage($this->getPage());
        $Paginator->setMaxPagesToShow(4);

        return $Paginator->get();
    }

    public function getPaginationUrlPattern()
    {
        return $this->paginationUrlPattern;
    }

    public function setPaginationUrlPattern($v = null)
    {
        $this->paginationUrlPattern = $v;
    }

    public function setRows($rows = null)
    {
        $this->rows = $rows;
    }

    protected function getItemClass()
    {
        return $this->itemClass;
    }

    protected function setItemClass($class = null)
    {
        $this->itemClass = $class;
    }
}
