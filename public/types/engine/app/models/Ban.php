<?php

namespace engine\app\models;

class Ban
{
    private $id = null;

    private $ip = null;

    private $user_agent = null;

    private $query = null;

    private $description = null;

    private $active = null;

    private $update_time = null;

    private $count = null;

    private $create_time = null;

    private $hide_player = null;

    private $count_player = null;

    private $referer = null;

    public function __construct($id = null)
    {
        $id ?? F::error('ID required to use Ban');
        $arr = F::query_assoc('select id, ip, user_agent, query, description, active, update_time, q, create_time, hide_player, q_player, referer
			from '.F::tabname('engine', 'ban').'
			where id = \''.F::escape_string($id).'\'
			;');
        if (! $arr) {
            F::error('Ban is not exist');
        }
        $this->id = $arr['id'];
        $this->ip = $arr['ip'];
        $this->user_agent = $arr['user_agent'];
        $this->referer = $arr['referer'];
        $this->query = $arr['query'];
        $this->description = $arr['description'];
        $this->active = $arr['active'] ? true : false;
        $this->update_time = $arr['update_time'];
        $this->count = $arr['q'];
        $this->create_time = $arr['create_time'];
        $this->hide_player = $arr['hide_player'] ? true : false;
        $this->count_player = $arr['q_player'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip = null)
    {
        $this->ip = $ip;
    }

    public function getUserAgent()
    {
        return $this->user_agent;
    }

    public function setUserAgent($user_agent = null)
    {
        $this->user_agent = $user_agent;
    }

    public function getReferer()
    {
        return $this->referer;
    }

    public function setReferer($referer = null)
    {
        $this->referer = $referer;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function setQuery($query = null)
    {
        $this->query = $query;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description = null)
    {
        $this->description = $description;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }

    public function getHidePlayer()
    {
        return $this->hide_player;
    }

    public function setHidePlayer($b = null)
    {
        $this->hide_player = $b;
    }

    public function getCountPlayer()
    {
        return $this->count_player;
    }

    public function setCountPlayer($count_player = null)
    {
        $this->count_player = $count_player;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count = null)
    {
        $this->count = $count;
    }

    public function save()
    {
        F::query('update '.F::tabname('engine', 'ban').'
			set
			ip = \''.F::escape_string($this->getIp()).'\',
			user_agent = \''.F::escape_string($this->getUserAgent()).'\',
			referer = \''.F::escape_string($this->getReferer()).'\',
			query = \''.F::escape_string($this->getQuery()).'\',
			description = \''.F::escape_string($this->getDescription()).'\',
			active = '.($this->getActive() ? 1 : 0).',
			hide_player = '.($this->getHidePlayer() ? 1 : 0).',
			q = '.$this->getCount().',
			q_player = '.$this->getCountPlayer().'
			where id = \''.F::escape_string($this->getId()).'\'
			;');
    }

    public static function add()
    {
        F::query('insert into '.F::tabname('engine', 'ban').' set id = null;');
        $id = Engine::getSqlConnection()->insert_id;

        return $id;
    }

    public function delete()
    {
        F::query('delete from '.F::tabname('engine', 'ban').' where id = \''.$this->getId().'\';');
    }
}
