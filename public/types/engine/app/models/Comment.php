<?php

namespace engine\app\models;

class Comment
{
    protected $id = null;

    protected $date = null;

    protected $text = null;

    protected $username = null;

    protected $item_id = null;

    protected $item_field_name = null;

    protected $sql_table = null;

    public function __construct($id = null)
    {
        // F::error('Specify Id first to creste videoComment class');
        $this->setId($id);
        $arr = F::query_assoc('
			select '.$this->getItemFieldName().',date,username,text
			from '.$this->getSqlTable().'
			where id=\''.$this->getId().'\'
			;');
        F::dump($arr);
        $this->setItemId($arr[$this->getItemFieldName()]);
        $this->setDate($arr['date']);
        $this->setUsername($arr['username']);
        $this->setText($arr['text']);
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date = null)
    {
        $this->date = $date;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id = null)
    {
        $this->id = $id;
    }

    public function setUsername($username = null)
    {
        $this->username = $username;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public static function add($item_id = null)
    {
        F::query('insert into '.$this->getSqlTable().' set
			'.$this->getItemFieldName().' = \''.$item_id.'\';');
        do {
            $id = Engine::getSqlConnection()->insert_id;
        } while (empty($id));

        return $id;
    }

    public function save()
    {
        F::query('update '.$this->getSqlTable().'
			set
			username = '.($this->getUsername() ? ('\''.F::escape_string($this->getUsername()).'\'') : 'null').',
			text = '.($this->getText() ? ('\''.F::escape_string($this->getText()).'\'') : 'null').'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }

    public function getItemFieldName()
    {
        return $this->item_field_name;
    }

    public function setItemFieldName($fieldName = null)
    {
        $this->item_field_name = $fieldName;
    }

    public function getItemId()
    {
        return $this->item_id;
    }

    public function setItemId($item_id = null)
    {
        $this->item_id = $item_id;
    }

    public function getSqlTable()
    {
        return $this->sql_table;
    }

    public function setSqlTable($table = null)
    {
        $this->sql_table = $table;
    }
}
