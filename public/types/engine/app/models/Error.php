<?php

namespace engine\app\models;

class Error
{
    // тип ошибки
    private $type = null;

    private $text = null;

    private $logText = null;

    private $log = null;

    private static $counter = 0;

    private $header = 404;

    private $displayErrorInfo = null;

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function setLogText($logText = null)
    {
        $this->logText = $logText;
    }

    public function setLog($b = null)
    {
        $this->log = $b;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    // вывод ошибки в шаблон error.htm с завершением выполнения скрипта
    // text - текст выводимый клиенту, logtext - в лог, log - писать ли лог, page404 - добавлять ли header 404,
    // maxlogsfromip - если в логе ошибок от этого ip с этим названием больше указанного количества, то не писать, 0 - писать все
    public function get($text = null, $logText = null)
    {
        if ($text) {
            $this->setText($text);
        }
        if ($logText) {
            $this->setLogText($logText);
        }
        if (! $this->logText) {
            $this->logText = $this->text;
        }
        $stack_list = debug_backtrace();
        // объявляем переменные, для NOTICE
        if (isset($error['backtrace'])) {
            $error['backtrace'] = [];
        }
        if (isset($error['counter'])) {
            $error['counter'] = 0;
        }
        // исправить. если ошибка вызвана не из функции, то выдается notice
        // pre($stack_list);
        $backtrace_list_other = '';
        for ($i = count($stack_list) - 1; $i >= 1; $i--) {
            if (! isset($stack_list[$i]['file'])) {
                $backtrace_list_other .= '<li>'.$stack_list[$i]['function'].'</li>';
            } else {
                $backtrace_list_other .= '<li>'.$stack_list[$i]['function'].' '.$stack_list[$i]['file'].':'.$stack_list[$i]['line'].'</li>';
            }
        }
        // $error['backtrace'][]=$stack_list[1]['function'].' '.$stack_list[0]['line'].' ('.$logtext.')'."\r\n";
        if (! isset($stack_list[0]['file'])) {
            $error['backtrace'][] = $stack_list[1]['function'];
        } else {
            $error['backtrace'][] = $stack_list[1]['function'].' '.$stack_list[0]['file'].':'.$stack_list[0]['line'];
        }
        $backtrace_list = '';
        $backtrace_text = '';
        foreach ($error['backtrace'] as $k => $v) {
            $backtrace_list .= '<li>'.$v.'</li>';
            if ($k != 0) {
                $backtrace_text .= "\r\n";
            }
            $backtrace_text .= $v;
        }
        $error_info = '<hr><b>Все ошибки:</b><ol>'."\r\n".$backtrace_list_other.'</ol>'."\r\n";
        self::$counter++;
        $error['text'] = $this->text;
        $text = str_replace(["\r\n", "\n"], ' ', $this->text);
        // если error запускается больше 2 раз, значит возникает ошибка при записи лога при danger=1
        if (self::getCounter() > 2) {
            throw new \Exception($this->logText);
        }
        // если error запускается больше одного раза, значит произошло зацикливание (ошибка в logs либо template)
        // останавливаем скрипт и выводим текст ошибки.
        if (self::getCounter() > 1) {
            throw new \Exception('Looping: '.$this->logText.' '.$error_info);
        }
        if ($this->log) {
            logger()->error($this->logText);
        }
        if ($this->header === 404) {
            header('HTTP/1.0 404 Not Found');
        }
        if ($this->header === 503) {
            header('HTTP/1.0 503 Service Unavailable');
        }
        $template = new Template('error');
        if ($this->displayErrorInfo) {
            // vf - т.к. в debug_backtrace анонимная функция обозначается как {closure} и вызывает ошибку
            $template->vf('errorText', $this->logText.'<br/>'.$error_info);
        } else {
            $template->v('errorText', $error['text']);
        }
        $template->v('refererURL', F::referer_url());
        exit($template->get());
    }

    public static function getCounter()
    {
        return self::$counter;
    }

    public function setHeader($code = null)
    {
        $this->header = $code;
    }

    public function setDisplayErrorInfo($b = null)
    {
        $this->displayErrorInfo = $b;
    }

    public static function simple($text = null, $code = null)
    {
        if ($code == 503) {
            header('HTTP/1.1 503 Service Temporarily Unavailable');
            header('Status: 503 Service Temporarily Unavailable');
        }
        $result = '
		<html>
		<head>
		<title>Произошла ошибка</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
		<script type="text/javascript">setTimeout("window.location.reload()",2000);</script>
		<style>
		body {
			background-color: #333;
			text-align: center;
			margin: 30px;
			color: #ccc;
		}
		</style>
		</head>
		<body>
		<h1>'.$text.'</h1>
		</body>
		</html>
		';
        exit($result);
    }
}
