<?php

namespace engine\app\models;

class SiteRoute
{
    private $domain = null;

    private $alias = null;

    private $cache = null;

    private $pattern = null;

    private $controller = null;

    private $method = null;

    private $title = null;

    private $description = null;

    private $keywords = null;

    private $h1 = null;

    private $topText = null;

    private $bottomText = null;

    private $bread = null;

    private $text1 = null;

    private $text2 = null;

    private $h2 = null;

    private $template = null;

    private $var1 = null;

    private $var2 = null;

    private $var3 = null;

    private $var4 = null;

    private $var5 = null;

    private $comment = null;

    private $patternID = null;

    private $redirectToAlias = null;

    private $priority = null;
    // private $section = null;

    public function __construct($domain = null, $pattern = null)
    {
        if (! $domain) {
            F::error('Specify domain to create siteRoute');
        }
        if (! $pattern) {
            F::error('Specify pattern to create siteRoute');
        }
        $arr = F::query_assoc('select domain,pattern,cache,alias,controller,method,var1,var2,var3,var4,var5,
			title,description,keywords,h1,topText,bottomText,bread,text1,text2,h2,template,comment,
			redirectToAlias,priority
			from '.F::tabname('engine', 'site_routes').'
			where domain=\''.$domain.'\' and pattern=\''.F::escape_string($pattern).'\'
			;');
        if (! $arr) {
            F::error('Such pattern is not exist for this domain');
        }
        $this->domain = $arr['domain'];
        $this->alias = $arr['alias'];
        $this->cache = $arr['cache'];
        $this->pattern = $arr['pattern'];
        // дубль на случай замены паттерна, чтобы потом правильно отработал save
        $this->patternID = $arr['pattern'];
        $this->controller = $arr['controller'];
        $this->method = $arr['method'];
        $this->title = $arr['title'];
        $this->description = $arr['description'];
        $this->keywords = $arr['keywords'];
        $this->h1 = $arr['h1'];
        $this->topText = $arr['topText'];
        $this->bottomText = $arr['bottomText'];
        $this->bread = $arr['bread'];
        $this->text1 = $arr['text1'];
        $this->text2 = $arr['text2'];
        $this->h2 = $arr['h2'];
        $this->template = $arr['template'];
        $this->var1 = $arr['var1'];
        $this->var2 = $arr['var2'];
        $this->var3 = $arr['var3'];
        $this->var4 = $arr['var4'];
        $this->var5 = $arr['var5'];
        $this->comment = $arr['comment'];
        $this->setRedirectToAlias($arr['redirectToAlias'] ? true : false);
        $this->setPriority($arr['priority'] ? true : false);
        // $this->section = $arr['section'];
    }

    public function getAlias($v = null)
    {
        return $this->alias;
    }

    public function getCache($v = null)
    {
        return $this->cache;
    }

    public function getPattern($v = null)
    {
        return $this->pattern;
    }

    public function getController($v = null)
    {
        return $this->controller;
    }

    public function getVar1($v = null)
    {
        return $this->var1;
    }

    public function getVar2($v = null)
    {
        return $this->var2;
    }

    public function getVar3($v = null)
    {
        return $this->var3;
    }

    public function getVar4($v = null)
    {
        return $this->var4;
    }

    public function getVar5($v = null)
    {
        return $this->var5;
    }

    public function getMethod($v = null)
    {
        return $this->method;
    }

    public function getTitle($v = null)
    {
        return $this->title;
    }

    public function getDescription($v = null)
    {
        return $this->description;
    }

    public function getKeywords($v = null)
    {
        return $this->keywords;
    }

    public function getH1($v = null)
    {
        return $this->h1;
    }

    public function getTopText($v = null)
    {
        return $this->topText;
    }

    public function getBottomText($v = null)
    {
        return $this->bottomText;
    }

    public function getBread($v = null)
    {
        return $this->bread;
    }

    public function getText1($v = null)
    {
        return $this->text1;
    }

    public function getText2($v = null)
    {
        return $this->text2;
    }

    public function getH2($v = null)
    {
        return $this->h2;
    }

    public function getTemplate($v = null)
    {
        return $this->template;
    }

    public function getDomain($v = null)
    {
        return $this->domain;
    }

    public function getComment($v = null)
    {
        return $this->comment;
    }

    // function getSection($v = null) {
    // 	return $this->section;
    // }

    public function setAlias($v = null)
    {
        $this->alias = $v;
    }

    public function setCache($v = null)
    {
        $this->cache = $v;
    }

    public function setPattern($v = null)
    {
        $this->pattern = $v;
    }

    public function setController($v = null)
    {
        $this->controller = $v;
    }

    public function setVar1($v = null)
    {
        $this->var1 = $v;
    }

    public function setVar2($v = null)
    {
        $this->var2 = $v;
    }

    public function setVar3($v = null)
    {
        $this->var3 = $v;
    }

    public function setVar4($v = null)
    {
        $this->var4 = $v;
    }

    public function setVar5($v = null)
    {
        $this->var5 = $v;
    }

    public function setMethod($v = null)
    {
        $this->method = $v;
    }

    public function setTitle($v = null)
    {
        $this->title = $v;
    }

    public function setDescription($v = null)
    {
        $this->description = $v;
    }

    public function setKeywords($v = null)
    {
        $this->keywords = $v;
    }

    public function setH1($v = null)
    {
        $this->h1 = $v;
    }

    public function setTopText($v = null)
    {
        $this->topText = $v;
    }

    public function setBottomText($v = null)
    {
        $this->bottomText = $v;
    }

    public function setBread($v = null)
    {
        $this->bread = $v;
    }

    public function setText1($v = null)
    {
        $this->text1 = $v;
    }

    public function setText2($v = null)
    {
        $this->text2 = $v;
    }

    public function setH2($v = null)
    {
        $this->h2 = $v;
    }

    public function setTemplate($v = null)
    {
        $this->template = $v;
    }

    public function setDomain($v = null)
    {
        $this->domain = $v;
    }

    public function setComment($v = null)
    {
        $this->comment = $v;
    }

    // function setSection($v = null) {
    // 	$this->section = $v;
    // }

    public function save()
    {
        $arr = F::query('
			update '.F::tabname('engine', 'site_routes').' set
			domain=\''.F::escape_string($this->domain).'\',
			pattern=\''.F::escape_string($this->pattern).'\',
			cache=\''.F::escape_string($this->cache).'\',
			alias=\''.F::escape_string($this->alias).'\',
			controller=\''.F::escape_string($this->controller).'\',
			method=\''.F::escape_string($this->method).'\',
			var1=\''.F::escape_string($this->var1).'\',
			var2=\''.F::escape_string($this->var2).'\',
			var3=\''.F::escape_string($this->var3).'\',
			var4=\''.F::escape_string($this->var4).'\',
			var5=\''.F::escape_string($this->var5).'\',
			title=\''.F::escape_string($this->title).'\',
			description=\''.F::escape_string($this->description).'\',
			keywords=\''.F::escape_string($this->keywords).'\',
			h1=\''.F::escape_string($this->h1).'\',
			h2=\''.F::escape_string($this->h2).'\',
			topText=\''.F::escape_string($this->topText).'\',
			bottomText=\''.F::escape_string($this->bottomText).'\',
			text1=\''.F::escape_string($this->text1).'\',
			text2=\''.F::escape_string($this->text2).'\',
			bread=\''.F::escape_string($this->bread).'\',
			template=\''.F::escape_string($this->template).'\',
			comment=\''.F::escape_string($this->comment).'\',
			redirectToAlias = \''.($this->getRedirectToAlias() ? 1 : 0).'\',
			priority = \''.($this->getPriority() ? 1 : 0).'\'
			where domain=\''.F::escape_string($this->domain).'\' and pattern=\''.F::escape_string($this->patternID).'\'
			;');

        return true;
    }

    public static function add($domain = null, $pattern = null)
    {
        F::query('insert into '.F::tabname('engine', 'site_routes').' set domain=\''.F::escape_string($domain).'\', pattern=\''.F::escape_string($pattern).'\';');
    }

    public static function delete($domain = null, $pattern = null)
    {
        F::query('delete from '.F::tabname('engine', 'site_routes').' where domain=\''.F::escape_string($domain).'\' and pattern=\''.F::escape_string($pattern).'\';');
    }

    public function getRedirectToAlias()
    {
        return $this->redirectToAlias;
    }

    public function setRedirectToAlias($b = null)
    {
        $this->redirectToAlias = $b;
    }

    public function setPriority($b = null)
    {
        $this->priority = $b;
    }

    public function getPriority()
    {
        return $this->priority;
    }
}
