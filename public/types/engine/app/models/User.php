<?php

namespace engine\app\models;

class User
{
    private $login = null;

    private $level = null;

    private $id = null;

    private $comments = null;

    private $timezone = null;

    private $password = null;

    private $email = null;

    private $usersTable = null;

    public function __construct($login = null, $type = null)
    {
        $type = empty($type) ? F::error('Type required to use User') : $type;
        $this->usersTable = F::tabname($type, 'users');
        if (! $login) {
            F::error('User ID required to create User Class');
        }
        $arr = F::query_assoc('
			select login,id,level,timezone, pass, email
			from '.$this->usersTable.'
			where login = \''.$login.'\'
			;');
        if (! $arr) {
            F::error('User was not found');
        }
        $this->id = $arr['id'];
        $this->login = $arr['login'];
        $this->level = $arr['level'];
        // $this->comments = $arr['comments'];
        $this->timezone = $arr['timezone'];
        $this->password = $arr['pass'];
        $this->email = $arr['email'];
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel($level = null)
    {
        $this->level = $level;
    }

    public function save()
    {
        F::query('update '.$this->usersTable.'
			set
			pass = '.($this->getPassword() ? ('\''.F::escape_string($this->getPassword()).'\'') : 'null').',
			level = '.($this->getLevel() ? ('\''.F::escape_string($this->getLevel()).'\'') : 'null').',
			email = '.($this->getEmail() ? ('\''.F::escape_string($this->getEmail()).'\'') : 'null').'
			where id = \''.$this->getId().'\'
			;');

        return true;
    }

    // function getComments() {
    // 	return $this->comments;
    // }

    public function getTimezone()
    {
        return $this->timezone;
    }

    public static function add($login = null)
    {
        F::query('insert into '.$this->usersTable.' set login = \''.F::escape_string($login).'\';');
        $user = new User($login);

        return $user->getId();
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password = null)
    {
        $this->password = $password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email = null)
    {
        $this->email = $email;
    }
}
