<?php

namespace engine\app\models;

class YandexApi
{
    private $token = null;

    private $client_id = null;

    public function request($args = [])
    {
        $args['url'] = $args['url'] ? $args['url'] : F::error('url required to use YandexApi::request');
        if (! $this->getToken()) {
            F::error('Token required to use YandexApi::request');
        }
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $args['url'],
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer '.$this->getToken(),
            ],
        ]);

        return json_decode(curl_exec($ch));
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token = null)
    {
        $this->token = $token;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function setClientId($client_id = null)
    {
        $this->client_id = $client_id;
    }
}
