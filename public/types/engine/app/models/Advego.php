<?php

namespace engine\app\models;

class Advego
{
    private $text = null;

    private $apiKey = null;

    private $callbackUrl = null;

    private $textId = null;

    private $exceptDomains = [];

    public function __construct() {}
    /*
        2 функции для взаимодействия с API Text.ru посредством POST-запросов.
        Ответы с сервера приходят в формате JSON.
    */

    // -----------------------------------------------------------------------

    /**
     * Добавление текста на проверку
     *
     * @param  string  $text  - проверяемый текст
     * @param  string  $user_key  - пользовательский ключ
     * @param  string  $exceptdomain  - исключаемые домены
     * @return string $text_uid - uid добавленного текста
     * @return int $error_code - код ошибки
     * @return string $error_desc - описание ошибки
     */
    public function addPost()
    {
        if (! $this->getText()) {
            F::error('use setText first');
        }
        if (! $this->getApiKey()) {
            F::error('use setApiKey first');
        }
        $postQuery = [];
        $postQuery['text'] = $this->getText();
        $postQuery['userkey'] = $this->getApiKey();
        // домены разделяются пробелами либо запятыми. Данный параметр является необязательным.
        if ($this->getExceptDomains()) {
            $postQuery['exceptdomain'] = implode(', ', $this->getExceptDomains());
        }
        // $postQuery['exceptdomain'] = "site1.ru, site2.ru, site3.ru";
        // Раскомментируйте следующую строку, если вы хотите, чтобы результаты проверки текста были по-умолчанию доступны всем пользователям
        // $postQuery['visible'] = "vis_on";
        // Раскомментируйте следующую строку, если вы не хотите сохранять результаты проверки текста в своём архиве проверок
        // $postQuery['copying'] = "noadd";
        // Указывать параметр callback необязательно
        if ($this->getCallbackUrl()) {
            $postQuery['callback'] = $this->getCallbackUrl();
        }

        $postQuery = http_build_query($postQuery, '', '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://api.text.ru/post');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postQuery);
        $json = curl_exec($ch);
        $errno = curl_errno($ch);
        // если произошла ошибка
        if (! $errno) {
            $resAdd = json_decode($json);
            if (isset($resAdd->text_uid)) {
                $text_uid = $resAdd->text_uid;

                return $text_uid;
            } else {
                $error_code = $resAdd->error_code;
                $error_desc = $resAdd->error_desc;
            }
        } else {
            $errmsg = curl_error($ch);
        }
        curl_close($ch);

        return false;
    }

    /**
     * Получение статуса и результатов проверки текста в формате json
     *
     * @param  string  $text_uid  - uid проверяемого текста
     * @param  string  $user_key  - пользовательский ключ
     * @return float $unique - уникальность текста (в процентах)
     * @return string $result_json - результат проверки текста в формате json
     * @return int $error_code - код ошибки
     * @return string $error_desc - описание ошибки
     */
    public function getResultPost()
    {
        if (! $this->getApiKey()) {
            F::error('use setApiKey first');
        }
        if (! $this->getTextId()) {
            F::error('use setTextId first');
        }
        $postQuery = [];
        $postQuery['uid'] = $this->getTextId();
        $postQuery['userkey'] = $this->getApiKey();
        // Раскомментируйте следующую строку, если вы хотите получить более детальную информацию в результатах проверки текста на уникальность
        // $postQuery['jsonvisible'] = "detail";

        $postQuery = http_build_query($postQuery, '', '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://api.text.ru/post');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postQuery);
        $json = curl_exec($ch);
        $errno = curl_errno($ch);

        if (! $errno) {
            $resCheck = json_decode($json);
            if (isset($resCheck->text_unique)) {
                $text_unique = $resCheck->text_unique;
                $result_json = $resCheck->result_json;

                return json_decode($result_json);
            } else {
                $error_code = $resCheck->error_code;
                $error_desc = $resCheck->error_desc;
                // return $error_desc;
            }
        } else {
            $errmsg = curl_error($ch);
        }

        curl_close($ch);

        return false;
    }

    public function setApiKey($key = null)
    {
        $this->apiKey = $key;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function setCallbackUrl($url = null)
    {
        $this->callbackUrl = $url;
    }

    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setTextId($id = null)
    {
        $this->textId = $id;
    }

    public function getTextId()
    {
        return $this->textId;
    }

    public function setExceptDomain($domain = null)
    {
        if ($domain) {
            $this->exceptDomains[] = $domain;
        }
    }

    public function getExceptDomains()
    {
        return $this->exceptDomains;
    }

    public static function getPlagiatUrls($result = null)
    {
        $list = '';
        $i = 0;
        foreach ($result->urls as $url) {
            if ($i > 2) {
                break;
            }
            $i++;
            $list .= '<li>'.$url->plagiat.'% совпадений: <a href="'.$url->url.'">'.$url->url.'</a></li>';
        }

        return $list;
    }
}
