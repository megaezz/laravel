<?php

namespace engine\app\models;

use App\Models\Domain as DomainEloquent;

class Domains
{
    private $type = null;

    protected $rows = null;

    private $reborn = null;

    private $showMirrorsFor = null;

    private $ruBlock = null;

    private $order = null;

    private $limit = null;

    private $page = null;

    private $admin = null;

    private $domain = null;

    private $isMirror = null;

    private $isOn = null;

    private $types = null;

    public function get()
    {
        if ($this->getLimit()) {
            if (! $this->getPage()) {
                $this->setPage(1);
            }
            $sql['limit'] = 'limit '.($this->getPage() - 1) * $this->getLimit().','.$this->getLimit();
        } else {
            $sql['limit'] = '';
        }
        $sql['domain'] = $this->getDomain() ? 'domain=\''.$this->getDomain().'\'' : 1;
        $sql['type'] = $this->getType() ? 'type=\''.$this->getType().'\'' : 1;
        $sql['types'] = $this->types ? ('type in (\''.implode('\',\'', $this->types).'\')') : 1;
        $sql['reborn'] = is_null($this->getReborn()) ? 1 : ($this->getReborn() ? 'reborn' : 'not reborn');
        $sql['showMirrorsFor'] = $this->getShowMirrorsFor() ? ('mirror_of = \''.$this->getShowMirrorsFor().'\'') : 1;
        $sql['ruBlock'] = is_null($this->getRuBlock()) ? 1 : ($this->getRuBlock() ? 'ru_block' : 'not ru_block');
        $sql['order'] = $this->getOrder() ? ('order by '.$this->getOrder()) : '';
        $sql['isOn'] = is_null($this->getIsOn()) ? 1 : ('`on` = \''.($this->getIsOn() ? 1 : 0).'\'');
        $sql['isMirror'] = is_null($this->getIsMirror()) ? 1 : ($this->getIsMirror() ? 'domains.mirror_of is not null' : 'domains.mirror_of is null');
        $arr = F::query_arr('
			select SQL_CALC_FOUND_ROWS domains.domain
			from '.F::tabname('laravel', 'domains').'
			where
			'.$sql['domain'].' and
			'.$sql['type'].' and
			'.$sql['types'].' and
			'.$sql['reborn'].' and
			'.$sql['showMirrorsFor'].' and
			'.$sql['ruBlock'].' and
			'.$sql['isMirror'].' and
			'.$sql['isOn'].'
			'.$sql['order'].'
			'.$sql['limit'].'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setReborn($b = null)
    {
        $this->reborn = $b;
    }

    public function getReborn()
    {
        return $this->reborn;
    }

    public function setShowMirrorsFor($domain = null)
    {
        $this->showMirrorsFor = $domain;
    }

    public function getShowMirrorsFor()
    {
        return $this->showMirrorsFor;
    }

    public function setRuBlock($b = null)
    {
        $this->ruBlock = $b;
    }

    public function getRuBlock()
    {
        return $this->ruBlock;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getAdmin()
    {
        return $this->admin;
    }

    public function setAdmin($login = null)
    {
        $this->admin = $login;
    }

    public function getIsMirror()
    {
        return $this->isMirror;
    }

    public function setIsMirror($b = null)
    {
        $this->isMirror = $b;
    }

    public function getTypes()
    {
        $types = DomainEloquent::distinct()->orderBy('type');
        $types = $types->get(['type'])->pluck('type')->toArray();

        return $types;
    }

    public function getIsOn()
    {
        return $this->isOn;
    }

    public function setIsOn($b = null)
    {
        $this->isOn = $b;
    }

    public function setTypes($arr = [])
    {
        $this->types = $arr;
    }
}
