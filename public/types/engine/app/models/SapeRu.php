<?php

namespace engine\app\models;

class SapeRu
{
    private static $sapeObject = null;

    public function __construct() {}

    public static function init()
    {
        if (! defined('_SAPE_USER')) {
            define('_SAPE_USER', '5040809775a978713f797c65741ad1e9ccaccda479f4935f1f9b92f579d54eae');
        }
        $sape_script = realpath(request()->server('DOCUMENT_ROOT').'/'._SAPE_USER.'/sape.php');
        if (! file_exists($sape_script)) {
            return false;
        }
        require_once $sape_script;
        $sape = new \SAPE_client(['multi_site' => true]);
        //
        static::$sapeObject = $sape;
    }

    public static function getSapeObject()
    {
        if (is_null(static::$sapeObject)) {
            // F::error('Init Sape first');
            self::init();
        }

        return static::$sapeObject;
    }

    public static function getLinks($q = null)
    {
        return static::getSapeObject()?->return_links($q);
    }
}
