<?php

namespace engine\app\models;

class Db
{
    private $link = null;

    private $query = null;

    private $logLongQueries = null;

    private static $list = [];

    private $text;

    public function __construct() {}

    public function getData($query = null)
    {
        if ($query) {
            $this->setQuery($query);
        }
        if (! $this->query) {
            F::error('Use setQuery first to use Db::getArr');
        }
        if (empty($text)) {
            $text = 'Без описания';
        }
        $before_time = F::microtime_float();
        $query = trim($this->query);
        try {
            $data = mysqli_query($this->link, $this->query);
        } catch (\Exception $e) {
            throw new \Exception("Ошибка в запросе: {$text}: {$this->query}. Текст ошибки: ".mysqli_error($this->link));
        }
        $after_time = F::microtime_float();
        $time = $after_time - $before_time;
        self::addList(['query' => $this->query, 'time' => $time]);
        // если стоит настройка получения долгих запросов
        if ($this->getLogLongQueries()) {
            if ($time > 1) {
                $log = new Log;
                $log->add('Долгий запрос: '.$this->query);
            }
        }

        return $data;
    }

    // записывает результат запроса в массив
    // если указать unique_key=false то в одном ключе будем собираться подмассив с таким же ключем
    public function getArr($key, $unique_key = true)
    {
        // if ($query) {
        // 	$this->setQuery($query);
        // }
        $data = $this->getData();
        $arr = [];
        if ($key == 'nokey') {
            while ($row = mysqli_fetch_assoc($data)) {
                $arr[] = $row;
            }
        } else {
            while ($row = mysqli_fetch_assoc($data)) {
                if (! isset($row[$key])) {
                    F::error('Ключ '.$key.' не существует');
                }
                if ($unique_key) {
                    $arr[$row[$key]] = $row;
                } else {
                    $arr[$row[$key]][] = $row;
                }
            }
        }

        return $arr;
    }

    public function getArrNokey($query = null)
    {
        if ($query) {
            $this->setQuery($query);
        }

        return $this->getArr('nokey');
    }

    public function getArrAssoc($query = null)
    {
        if ($query) {
            $this->setQuery($query);
        }
        $mysqli_result = $this->getData();
        $result = mysqli_fetch_assoc($mysqli_result);

        // если результат - null, то прописываем имена полей
        // не катит, т.к. в коде много где используется конструкция if (!$arr)
        // if (is_null($result)) {
        // 	$fields = mysqli_fetch_fields($mysqli_result);
        // 	// F::dump($fields);
        // 	$result = array();
        // 	foreach ($fields as $v) {
        // 		$result[$v->name] = null;
        // 	}
        // }
        return $result;
    }

    public function setQuery($query = null)
    {
        $this->query = $query;
    }

    public function setText($text = null)
    {
        $this->text = $text;
    }

    public function getLogLongQueries()
    {
        return $this->logLongQueries;
    }

    public function setLink($link = null)
    {
        $this->link = $link;
    }

    public function setLogLongQueries($b = null)
    {
        $this->logLongQueries = $b;
    }

    private static function addList($arr = [])
    {
        self::$list[] = $arr;
    }

    public static function getList()
    {
        return self::$list;
    }

    public static function getQueryCount()
    {
        return count(self::getList());
    }

    // дописать
    // записывает результат запроса в объект
    // если указать unique_key=false то в одном ключе будем собираться подмассив с таким же ключем
    public function getObjectArr($key, $unique_key = true)
    {
        // if ($query) {
        // 	$this->setQuery($query);
        // }
        $data = $this->getData();
        $arr = [];
        if ($key == 'nokey') {
            while ($row = mysqli_fetch_object($data)) {
                $arr[] = $row;
            }
        } else {
            while ($row = mysqli_fetch_object($data)) {
                if (! isset($row->$key)) {
                    F::error('Ключ '.$key.' не существует');
                }
                if ($unique_key) {
                    $arr[$row->$key] = $row;
                } else {
                    $arr[$row->$key][] = $row;
                }
            }
        }

        return $arr;
    }

    public function getObject($query = null)
    {
        if ($query) {
            $this->setQuery($query);
        }

        return mysqli_fetch_object($this->getData());
    }
}
