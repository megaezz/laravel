<?php

namespace engine\app\models;

class Kinopoisk
{
    private $page = null;

    public function __construct($id = null)
    {
        if (! $id) {
            F::error('Kinopoisk ID required');
        }
        $url = 'https://www.kinopoisk.ru/film/'.$id.'/';
        // $headers = get_headers($url);
        // F::dump($headers);
        F::pre(F::filegetcontents($url));
        if (strstr($headers[0], '200')) {
            $this->page = file_get_contents($url);
        } else {
            F::error('Kinopoisk ID not found');
        }
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getTitle()
    {
        F::dump($this->getPage());

        return F::copybetwen('f', 'f', $this->getPage());
    }
}
