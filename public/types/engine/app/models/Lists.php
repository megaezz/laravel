<?php

// класс для создания классов-списков

namespace engine\app\models;

class Lists
{
    private $limit = null;

    private $page = null;

    private $order = null;

    private $rows = null;

    private $listTemplate = null;

    private $itemClass = null;

    private $listTemaplate = null;

    private $paginationUrlPattern = null;

    public function get()
    {
        // $sql = $this->getSql();
    }

    protected function getSql()
    {
        if ($this->getLimit()) {
            if (! $this->getPage()) {
                $this->setPage(1);
            }
            $from = ($this->getPage() - 1) * $this->getLimit();
            $sql['limit'] = 'limit '.$from.','.$this->getLimit();
        } else {
            $sql['limit'] = '';
        }
        $sql['order'] = $this->getOrder() ? ('order by '.$this->getOrder()) : '';

        return $sql;
    }

    public function getList()
    {
        if (! $this->getListTemplate()) {
            F::error('use setListTemplate');
        }
        $list = '';
        $arr = $this->get();
        foreach ($arr as $v) {
            $t = new Template($this->getListTemplate());
            $a = (new \ReflectionClass($this->getItemClass()))->newInstance($v['id']);
            $this->setListVars($t, $a);
            $list .= $t->get();
        }

        return $list;
    }

    protected function setListVars($template = null, $itemObject = null)
    {
        // $t->v('var','value');
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    public function getPages()
    {
        if (! $this->getPaginationUrlPattern()) {
            F::error('setPaginationUrlPattern first');
        }
        $Paginator = new PaginatorCustom;
        $Paginator->setUrlPattern($this->getPaginationUrlPattern());
        $Paginator->setTemplateList('pagination/list');
        $Paginator->setTemplatePrev('pagination/prev');
        $Paginator->setTemplateNext('pagination/next');
        $Paginator->setRows($this->getRows());
        $Paginator->setLimit($this->getLimit());
        $Paginator->setPage($this->getPage());
        $Paginator->setMaxPagesToShow(4);

        return $Paginator->get();
    }

    public function getPaginationUrlPattern()
    {
        return $this->paginationUrlPattern;
    }

    public function setPaginationUrlPattern($v = null)
    {
        $this->paginationUrlPattern = $v;
    }

    public function setRows($rows = null)
    {
        $this->rows = $rows;
    }

    protected function getItemClass()
    {
        return $this->itemClass;
    }

    protected function setItemClass($class = null)
    {
        $this->itemClass = $class;
    }
}
