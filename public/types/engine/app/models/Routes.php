<?php

namespace engine\app\models;

class Routes
{
    private $domain = null;

    private $order = null;

    public function __construct() {}

    public function get()
    {
        // if ($this->type) {
        // 	$arr = F::query_arr('select pattern,cache,alias,controller,method,var1,var2,var3,var4,
        // 		title,description,keywords,h1,topText,bottomText,bread,text1,text2,h2,template
        // 		from '.F::tabname($this->type,'routes').'
        // 		where \''.$escapedURI.'\' regexp pattern
        // 		limit 1
        // 		;');
        // }
        $sql['domain'] = $this->domain ? 'domain=\''.$this->domain.'\'' : 1;
        $sql['order'] = $this->order ? ('order by'.$this->order) : '';
        $arr = F::query_arr('select pattern
			from '.F::tabname('engine', 'site_routes').'
			where '.$sql['domain'].'
			'.$sql['order'].'
			;');

        return $arr;
    }

    // function setType($type = null) {
    // 	// если передан тип engine то обнуляем, т.к. значит ничего не передано, т.к. таблица engine.routes опрашивается в любом случае
    // 	if ($type == 'engine') {
    // 		$type = null;
    // 	};
    // 	$this->type = $type;
    // }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }
}
