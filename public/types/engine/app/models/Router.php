<?php

namespace engine\app\models;

use App\Models\Domain as DomainEloquent;
use App\Models\Route;

// особенности экранирования символов в mysql запросах:
// в тестовом запросе паттерн экранировать двумя \\ а в таблицу заносить с одной \.
class Router
{
    private $controller = null;

    private $method = null;

    private $uri = null;

    private $route = null;

    private $type = null;

    private $domain = null;

    private $vars = [];

    private $routeType = null;

    // использовать ли кэш для роутера
    private $cache = null;

    // заголовки
    public $title = null;

    public $description = null;

    public $keywords = null;

    public $h1 = null;

    public $topText = null;

    public $bottomText = null;

    public $bread = null;

    public $text1 = null;

    public $text2 = null;

    public $h2 = null;

    public $template = null;

    public $priority = null;

    public function init()
    {
        // если передан route, то тип роутера - route
        if ($this->route) {
            $this->routeType = 'route';
        } else {
            if ($this->uri) {
                $this->routeType = 'uri';
            } else {
                abort(404, 'Null route');
            }
        }
        if ($this->routeType === 'uri') {
            $this->parseURI();
        }
        if ($this->routeType === 'route') {
            $this->parseRoute();
        }
    }

    public function run()
    {
        if (! $this->controller or ! $this->method) {
            abort(404, 'Route was not recognized');
        }
        $foundController = Engine::findClassMethod('controllers', $this->controller.'Controller', $this->method);

        /* Если не нашли контроллер, то пробуем искать по полному имени, для совместимости с Laravel */
        if (! $foundController) {
            $foundController = Engine::findClassMethod(null, $this->controller, $this->method);
        }

        if ($foundController) {
            $object = new $foundController;
            if ($this->cache) {
                return F::cache($object, $this->method, $this->vars);
            } else {
                return call_user_func_array([$object, $this->method], [$this->vars]);
            }
        }

        F::error('Method \''.$this->method.'\' doesn\'t exist in controller \''.$this->controller.'\'');
    }

    // получает controller и method из Route
    private function parseRoute()
    {
        if (! $this->route) {
            return false;
        }
        $explode = explode('/', $this->route);
        if (count($explode) < 2) {
            return false;
        }
        if (count($explode) === 2) {
            $this->controller = $explode[0];
            $this->method = $explode[1];
        }
        if (count($explode) === 3) {
            $this->controller = $explode[0].'\\'.$explode[1];
            $this->method = $explode[2];
        }
        if (count($explode) === 4) {
            $this->controller = $explode[0].'\\'.$explode[1].'\\'.$explode[2];
            $this->method = $explode[3];
        }

        return true;
    }

    // получает controller и method из URI
    private function parseURI()
    {

        if (! $this->uri) {
            return false;
        }

        // есть роутер для домена?
        $domainRoute = Route::query()
            ->where('domain', $this->domain)
            ->whereRaw('? REGEXP pattern', [$this->uri])
            ->whereNull('name')
            ->orderByDesc('priority')
            ->first();

        $route = $domainRoute;

        if (! $route) {
            // есть роутер для типа?
            // $typeRouteModel = "{$this->type}\\app\\models\\eloquent\\Route";

            // if (class_exists($typeRouteModel)) {
            // $typeRoute = $typeRouteModel::query()
            $typeRoute = Route::query()
                ->whereNull('domain')
                ->where('type', $this->type)
                ->whereRaw('? REGEXP pattern', [$this->uri])
                ->whereNull('name')
                ->orderByDesc('priority')
                ->first();

            $route = $typeRoute;
            // }
        }

        if ($route) {
            if ($route->name) {
                logger()->info("Вызван именованный роут через megaweb роутер: {$route->name}");
            }
            $this->setPropertiesFromURI($route->toArray());

            return true;
        } else {
            return false;
        }

    }

    private function setPropertiesFromURI($arr = [])
    {
        if ($arr['alias']) {
            if ($arr['redirectToAlias']) {
                F::redirect_301($arr['alias']);
            } else {
                $this->setURI($arr['alias']);
                $this->parseURI();

                return;
            }
        }
        $this->controller = $arr['controller'];
        $this->method = $arr['method'];
        if ($arr['cache']) {
            $this->setCache(true);
        }
        // self::$pattern = $arr['pattern'];
        $this->title = $arr['title'];
        $this->description = $arr['description'];
        $this->keywords = $arr['keywords'];
        $this->h1 = $arr['h1'];
        $this->topText = $arr['topText'];
        $this->bottomText = $arr['bottomText'];
        $this->bread = $arr['bread'];
        $this->text1 = $arr['text1'];
        $this->text2 = $arr['text2'];
        $this->h2 = $arr['h2'];
        $this->template = $arr['template'];
        $this->priority = $arr['priority'];
        // dump($arr['pattern']);
        for ($i = 1; $i <= 5; $i++) {
            if ($arr['var'.$i]) {
                // F::dump(preg_match('/'.$arr['pattern'].'/', $this->uri));
                $this->vars[$arr['var'.$i]] = preg_replace('/'.$arr['pattern'].'/', '$'.$i, $this->uri);
            }
        }
    }

    public function setURI($uri = null)
    {
        $this->uri = $uri;
    }

    public function setRoute($route = null)
    {
        $this->route = $route;
    }

    public function setCache($b = null)
    {
        $this->cache = $b;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setMethod($method = null)
    {
        $this->method = $method;
    }

    public function setController($controller = null)
    {
        $this->controller = $controller;
    }

    public function setVar($name = null, $value = null)
    {
        if (! $name) {
            F::error('Empty var name on Router::setVar');
        }
        $this->vars[$name] = $value;
    }

    public function getBread()
    {
        return $this->bread;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function getVars()
    {
        return $this->vars;
    }

    // возвращает megaweb роут по laravel реквесту
    public static function getRouteByRequest($request)
    {
        $domain = DomainEloquent::find(DomainEloquent::cleanDomain($request->getHost()))->primary_domain;
        $router = new self;
        $router->setURI($request->getPathInfo());
        $router->setType($domain->type);
        $router->setDomain($domain->domain);
        $router->init();
        if (! $router->getController()) {
            return false;
        }

        return $router;
    }
}
