<?php

namespace engine\app\models;

use App\Models\Config;

class Cron
{
    private $type = null;

    public function __construct($type = null)
    {
        $this->setType($type);
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    // проверяет прошел ли определенный интервал времени и выполняет функцию
    public function run()
    {
        if (! $this->getType()) {
            F::error('use setType first to use Cron::run');
        }
        $sql['dev_mode'] = Config::cached()->dev_mode ? 'dev' : 1;
        $date = getdate();
        $arr = F::query_arr('
			select action,time_range,unix_timestamp(last_time) as unix_last_time,active,done
			from '.F::tabname($this->getType(), 'crons').'
			where engine = \'v2\' and active and done and '.$sql['dev_mode'].'
			;');
        $className = $this->getType().'\app\controllers\CronController';
        $toRun = [];
        foreach ($arr as $v) {
            if ($v['time_range'] != 0 and ($date[0] - $v['unix_last_time']) > $v['time_range']) {
                if (! method_exists($className, $v['action'])) {
                    F::error('Метод не существует', 'Крон не выполнен. Метод '.$v['action'].' не существует в кроне для '.$this->getType());
                }
                $toRun[] = $v['action'];
                F::query('
					update '.F::tabname($this->getType(), 'crons').'
					set last_time = current_timestamp, done = 0
					where action=\''.$v['action'].'\'
					;');
            }
        }
        // выполняем после того как done для всех заданий проставлены, чтобы не было двойных выполнений
        foreach ($toRun as $method) {
            $start = F::microtime_float();
            $className::$method();
            $end = F::microtime_float();
            F::query('
				update '.F::tabname($this->getType(), 'crons').'
				set done = 1, run_time = '.(round($end - $start, 2)).'
				where action = \''.$method.'\'
				;');
            // logger()->info('Выполнили задание: '.$v['action'].' для типа '.$this->getType().' ('.(round($end - $start,3)).' сек)');
        }
    }

    public function getInfo()
    {
        if (! $this->getType()) {
            F::error('use setType first to use Cron::getInfo');
        }
        $arr = F::query_arr('
			select action,time_range,unix_timestamp(last_time) as unix_last_time,active,done
			from '.F::tabname($this->getType(), 'crons').'
			;');

        return $arr;
    }

    public function isTableExist()
    {
        $arr = F::query_arr('show databases like \''.$this->getType().'\';');
        if (! $arr) {
            return false;
        }
        $arr = F::query_arr('show tables from '.$this->getType().' like \'crons\';');
        if (! $arr) {
            return false;
        }

        return true;
    }
}
