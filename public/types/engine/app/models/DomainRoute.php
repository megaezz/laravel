<?php

namespace engine\app\models;

class DomainRoute extends TypeRoute
{
    private $domain = null;

    public function __construct($pattern = null, $domain = null)
    {
        if (! $domain) {
            F::error('Specify domain to create siteRoute');
        }
        $this->setDomain($domain);
        parent::__construct($pattern);
    }

    public function setProprerties($arr = [])
    {
        parent::setProprerties($arr);
        $this->setDomain($arr['domain']);
    }

    public function getArr()
    {
        return F::query_assoc('select domain,pattern,cache,alias,controller,method,var1,var2,var3,var4,var5,
			title,description,keywords,h1,topText,bottomText,bread,text1,text2,h2,template,comment,
			redirectToAlias
			from '.F::tabname('engine', 'site_routes').'
			where
			domain = \''.$this->getDomain().'\' and
			pattern = \''.F::escape_string($this->getPattern()).'\'
			;');
    }

    public function getDomain($v = null)
    {
        return $this->domain;
    }

    public function setDomain($v = null)
    {
        $this->domain = $v;
    }

    public function save()
    {
        F::query('
			update '.F::tabname('engine', 'site_routes').' set
			domain = \''.F::escape_string($this->getDomain()).'\',
			pattern = \''.F::escape_string($this->getPattern()).'\',
			cache = \''.F::escape_string($this->getCache()).'\',
			alias = \''.F::escape_string($this->getAalias()).'\',
			controller = \''.F::escape_string($this->getController()).'\',
			method = \''.F::escape_string($this->getMethod()).'\',
			var1 = \''.F::escape_string($this->getVar1()).'\',
			var2 = \''.F::escape_string($this->getVar2()).'\',
			var3 = \''.F::escape_string($this->getVar3()).'\',
			var4 = \''.F::escape_string($this->getVar4()).'\',
			var5 = \''.F::escape_string($this->getVar5()).'\',
			title = \''.F::escape_string($this->getTitle()).'\',
			description = \''.F::escape_string($this->getDescription()).'\',
			keywords = \''.F::escape_string($this->getKeywords()).'\',
			h1 = \''.F::escape_string($this->getH1()).'\',
			h2 = \''.F::escape_string($this->getH2()).'\',
			topText = \''.F::escape_string($this->getTopText()).'\',
			bottomText = \''.F::escape_string($this->getBottomText()).'\',
			text1 = \''.F::escape_string($this->getText1()).'\',
			text2 = \''.F::escape_string($this->getText2()).'\',
			bread = \''.F::escape_string($this->getBread()).'\',
			template = \''.F::escape_string($this->getTemplate()).'\',
			comment = \''.F::escape_string($this->getComment()).'\',
			redirectToAlias = \''.($this->getRedirectToAlias() ? 1 : 0).'\'
			where
			pattern = \''.F::escape_string($this->getPatternId()).'\' and
			domain = \''.F::escape_string($this->getDomain()).'\'
			;');

        return true;
    }

    public static function add($pattern = null, $domain = null)
    {
        F::query('insert into '.F::tabname('engine', 'site_routes').'
			set domain = \''.F::escape_string($domain).'\',
			pattern=\''.F::escape_string($pattern).'\'
			;');
    }

    public static function delete($pattern = null, $domain = null)
    {
        F::query('delete from '.F::tabname('engine', 'site_routes').'
			where
			domain = \''.F::escape_string($domain).'\' and
			pattern = \''.F::escape_string($pattern).'\'
			;');
    }
}
