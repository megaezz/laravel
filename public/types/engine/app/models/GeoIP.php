<?php

namespace engine\app\models;

use GeoIp2\Database\Reader;

/*

Использование:

$geo = new geoIP('2001:67c:2f5c:2::268');
dump($geo->name());

*/

class GeoIP
{
    private static $reader = null;

    private static $asnReader = null;

    private $record = null;

    private $ip = null;

    private $dbPath = null;

    private $asnDbPath = null;

    public function __construct($ip = null, $dbPath = null)
    {
        $this->setDbPath($dbPath ?? __DIR__.'/../../../../../modules/GeoLite.mmdb/GeoLite2-City.mmdb');
        $this->setAsnDbPath(__DIR__.'/../../../../../modules/GeoLite.mmdb/GeoLite2-ASN.mmdb');
        $this->setIp($ip);
        // если создаем второй экземпляр класса, чтобы заново не инициализировался reader
        if (! self::$reader) {
            self::$reader = new Reader($this->getDbPath());
        }
        if (! self::$asnReader) {
            self::$asnReader = new Reader($this->getAsnDbPath());
        }
    }

    public function getAsnDbPath()
    {
        return $this->asnDbPath;
    }

    public function setAsnDbPath($path = null)
    {
        $this->asnDbPath = $path;
    }

    public function getAsn()
    {
        try {
            return self::$asnReader->asn($this->getIp());
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getAsnOrganization()
    {
        return $this->getAsn()->autonomousSystemOrganization ?? false;
    }

    public function getRecord($ip = null)
    {
        if (! is_null($this->record)) {
            return $this->record;
        }

        return $this->calcRecord();
    }

    public function calcRecord()
    {
        try {
            if ($this->getDbType() == 'city') {
                return self::$reader->city($this->getIp());
            }

            return self::$reader->country($this->getIp());
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getCountryCode()
    {
        return $this->getRecord()->country->isoCode ?? false;
    }

    public function getCountryName()
    {
        return $this->getRecord()->country->name ?? false;
    }

    public function getCountryNameRu()
    {
        return $this->getRecord()->country->names['ru'] ?? false;
    }

    public function getCityName()
    {
        return $this->getRecord()->city->name ?? false;
    }

    public function getCityNameRu()
    {
        return $this->getRecord()->city->names['ru'] ?? false;
    }

    // алиас для совместимости
    public function code()
    {
        return $this->getCountryCode();
    }

    // алиас для совместимости
    public function name()
    {
        return $this->getCountryName();
    }

    public function getDbPath()
    {
        return $this->dbPath;
    }

    public function setDbPath($dbPath = null)
    {
        $this->dbPath = $dbPath;
    }

    public function getDbType()
    {
        if (strstr($this->getDbPath(), 'GeoLite2-City.mmdb')) {
            return 'city';
        }

        return 'country';
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip = null)
    {
        $this->ip = $ip;
    }
}
