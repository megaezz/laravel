<?php

namespace engine\app\models;

class ControllerMethod
{
    private $id = null;

    private $type = null;

    private $controller = null;

    private $method = null;

    private $text = null;

    public function __construct($id = null)
    {
        // if (!$type or !$controller or !$method) {
        // 	F::error('Set type, controller and method to use Method class');
        // };
        // $arr = self::getRow($type,$controller,$method);
        $arr = F::query_assoc('select id,type,controller,method,text
			from '.F::tabname('laravel', 'controllers').'
			where
			id=\''.$id.'\'
			;');
        if (! $arr) {
            F::error("Unable to find method $method in controller $controller with type $type");
        }
        $this->id = $arr['id'];
        $this->controller = $arr['controller'];
        $this->type = $arr['type'];
        $this->method = $arr['method'];
        $this->text = $arr['text'];
    }

    public function getID()
    {
        return $this->id;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getType()
    {
        return $this->type;
    }

    public static function getIDbyTypeControllerMethod($type = null, $controller = null, $method = null)
    {
        $arr = F::query_assoc('select id
			from '.F::tabname('laravel', 'controllers').'
			where
			type = \''.$type.'\' and
			controller = \''.$controller.'\' and
			method = \''.$method.'\'
			;');

        return $arr ? $arr['id'] : false;
    }

    public static function getAll($type = null)
    {
        $arr = F::query_arr('select id
			from '.F::tabname('laravel', 'controllers').'
			where type = \''.$type.'\'
			order by controller,method
			;');

        return $arr;
    }
}
