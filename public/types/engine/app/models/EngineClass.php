<?php

namespace engine\app\models;

use App\Models\Ban;
use App\Models\Config;
use App\Models\Domain;
use App\Models\Visit;
use Barryvdh\Debugbar\Facades\Debugbar;
use engine\app\services\DeviceDetector;
use engine\app\services\RegisterRoutesForDomainService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use ReCaptcha\ReCaptcha;

class EngineClass
{
    private $type = false;

    private $sqlConnection = null;

    private $displayErrors = null;

    private $debug = null;

    private $geoIpObject = null;

    private $router = null;

    // false специально, чтобы getClientBan не пересчитывался если вернет null
    private $ban = false;

    // false специально, чтобы requestedDomain не пересчитывался если вернет null
    private $requestedDomain = false;

    private $auth = null;

    private $crawler = null;

    private $crawlerDomain = null;

    private $crawlerOrClientDomain = null;

    private $visit = null;

    private Request $request;

    public function request()
    {
        return $this->request ?? request();
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
        $this->reset();
    }

    public function reset()
    {
        $this->type = false;
        $this->ban = false;
        $this->requestedDomain = false;
    }

    public function run()
    {
        // инициализируем класс роутер
        $this->router = new Router;
        // получаем URI без аргументов ссылки
        if ($this->request()->getRequestUri()) {
            if ($this->request()->path()) {
                $this->router->setURI('/'.ltrim($this->request()->path(), '/'));
            }
        }
        $this->router->setType($this->getType());
        $this->router->setDomain($this->getDomain());
        $this->router->setRoute($this->request()->query('r') ?? $argv[2] ?? null);
        $this->router->setCache(Config::cached()->routerCache ? true : false);
        $this->router->init();
        Debugbar::info($this->debug());

        return $this->router->run();
    }

    public function isAcceptedBot()
    {
        return $this->getAcceptedBotName() ? true : false;
    }

    public function getAcceptedBotName()
    {
        return in_array($this->getCrawler(), ['Googlebot', 'Google-Read-Aloud', 'Google-InspectionTool', 'Yandex', 'bingbot', 'Mail.RU', 'Slurp', 'Baiduspider', 'PetalBot', 'GoogleOther', 'DuckDuckBot', 'Amazonbot', 'BingPreview']) ? $this->getCrawler() : false;
    }

    public function getCrawler()
    {

        /* закрываем косяки пакета CrawlerDetect */
        $userAgents = [
            /* отображается как YandexBot хотя им не является */
            // 'Mozilla/5.0 (Linux; Android 9; YandexModule2-00001 Build/PI; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/113.0.5672.163 YaBrowser/24.1.2.86 (lite) TV Safari/537.36' => false,
            // 'Mozilla/5.0 (Linux; Android 9; YandexModule2-00001 Build/PI; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/113.0.5672.163 Mobile Safari/537.36' => false,
        ];

        /* если юзер агент из массива, то выводим ответ сразу */
        if (isset($userAgents[$this->request()->userAgent()])) {
            return $userAgents[$this->request()->userAgent()];
        }

        /* если юзер агент не из массива, то действуем как обычно */
        if (is_null($this->crawler)) {
            $CrawlerDetect = new CrawlerDetect;
            $CrawlerDetect->isCrawler($this->request()->userAgent());
            $this->crawler = $CrawlerDetect->getMatches() ?? false;
        }

        return $this->crawler;
    }

    public function isValidReferer()
    {
        // для тестов при передаче valid_referer - считаем реферер валидным
        if ($this->request()->query('valid_referer') or $this->request()->userAgent() == 'awmzone') {
            return true;
        }
        $referers = ['vk.com', 'bing.com', 'msn.com', 'mail.ru', 'google', 'yandex', 'rambler.ru', 'facebook.com', 'twitter.com', 'yahoo.com', 'ok.ru', 'duckduckgo.com', 'baidu.com', 'petalsearch.com', 'cinema.awmzone.net', 'ya.ru'];
        foreach ($referers as $referer) {
            if (strstr(($this->request()->header('referer') ?? ''), $referer)) {
                return true;
            }
        }

        return false;
    }

    public function accessSchemeV1()
    {
        if ($this->accessForTurbo()) {
            return true;
        }
        
        $domain = $this->getDomainObject();
        $isAnyBot = $this->isAcceptedBot();
        $redirectToDomain = $domain->getRedirectTo();
        $isNowRedirectToDomain = $_SERVER['HTTP_HOST'] == $redirectToDomain;
        $clientRedirectToDomain = $domain->getClientRedirectTo();
        $isNowClientRedirectToDomain = $_SERVER['HTTP_HOST'] == $clientRedirectToDomain;
        // если у домена есть актуальный домен и мы сейчас не на нем, то провеяем, надо ли редиректить на актуальный
        if ($domain->getRedirectTo() and ! $isNowRedirectToDomain) {
            $redirect = true;
            if ($isAnyBot) {
                if (in_array($this->getCrawler(), ['Yandex', 'bingbot', 'Mail.RU', 'Slurp', 'Baiduspider', 'PetalBot', 'DuckDuckBot', 'Amazonbot', 'BingPreview'])) {
                    $redirect = $domain->getYandexRedirect();
                }
                if (in_array($this->getCrawler(), ['Googlebot', 'Google-Read-Aloud', 'Google-InspectionTool', 'GoogleOther'])) {
                    $redirect = $domain->getGoogleRedirect();
                }
            } else {
                $redirect = $domain->getClientRedirect();
            }
            if ($redirect) {
                // F::redirect_301($this->getProtocol().'://'.$redirectToDomain.$_SERVER['REQUEST_URI']);
                return redirect("{$this->request()->getScheme()}://{$redirectToDomain}{$_SERVER['REQUEST_URI']}", 301);
            }
        }
        // срабатывает когда: не бот, запрошен актуальный домен, (домен не явлется целью редиректа - можно убрать наверное этот пункт, т.к. если запрошен актуальный домен, то он никак уже не может быть целью редиректа клиента, но надо затестить)
        if (! $isAnyBot and $domain->getClientRedirectTo() and ($domain->getRedirectTo() ? $isNowRedirectToDomain : true) and ! $isNowClientRedirectToDomain) {
            return redirect("{$this->request()->getScheme()}://{$clientRedirectToDomain}{$_SERVER['REQUEST_URI']}");
        }
    }

    public function accessSchemeV2()
    {
        if ($this->accessForTurbo()) {
            return true;
        }

        $domain = $this->getDomainObject();
        $isAnyBot = $this->isAcceptedBot();
        $isRealBot = $isAnyBot ? F::isRealBotCached($this->request()->ip()) : false;
        $redirectToDomain = $domain->getRedirectTo() ? $domain->getRedirectTo() : false;
        $clientRedirectToDomain = $domain->getClientRedirectTo() ? $domain->getClientRedirectTo() : false;
        // если актуального зеркала нет, то считаем что мы на нем
        $isNowRedirectToDomain = $redirectToDomain ? ($_SERVER['HTTP_HOST'] == $redirectToDomain) : true;
        $isNowClientRedirectToDomain = $clientRedirectToDomain ? ($_SERVER['HTTP_HOST'] == $clientRedirectToDomain) : false;
        // если не реальный бот, то вместо роботс и сайтмэп - заглушка
        if (! $isRealBot) {
            $uri = parse_url($_SERVER['REQUEST_URI']);
            if (isset($uri['path']) and in_array($uri['path'], ['/robots.txt', '/sitemap.xml'])) {
                // dd('показываем фейк заглушку для роботса и sitemap');
                return $this->fakeBrowserError();
            }
        }
        // если мы не на клиентском домене, не настоящий бот и не валидный реферер - показываем заглушку
        if ($isNowRedirectToDomain and ! $isRealBot and ! $this->isValidReferer()) {
            // dd('показываем фейк заглушку для не бота');
            return $this->fakeBrowserError();
        }
        // если реальный бот и у домена есть актуальный домен и мы сейчас не на нем, то редирект на актуальный
        if ($isRealBot and $domain->getRedirectTo() and ! $isNowRedirectToDomain) {
            // dd("редиректим на домен для бота {$redirectToDomain}");
            // F::redirect_301($this->getProtocol().'://'.$redirectToDomain.$_SERVER['REQUEST_URI']);
            return redirect("{$this->request()->getScheme()}://{$redirectToDomain}{$_SERVER['REQUEST_URI']}", 301);
        }
        // срабатывает когда: не бот + существует клиентский домен + существует актуальный домен и мы на нем, либо не существует актуального + мы сейчас не на клиентском домене
        if (! $isRealBot and $domain->getClientRedirectTo() and $isNowRedirectToDomain) {
            // dd("редиректим на клиентский домен {$clientRedirectToDomain}");
            // F::redirect($this->getProtocol().'://'.$clientRedirectToDomain.$_SERVER['REQUEST_URI']);
            return redirect("{$this->request()->getScheme()}://{$clientRedirectToDomain}{$_SERVER['REQUEST_URI']}");
        }
        // dd('впускаем');
    }

    public function accessSchemeV3()
    {
        if ($this->accessForTurbo()) {
            return true;
        }

        $domain = $this->domain();
        $isRealBot = $this->getAcceptedBotName() ? F::isRealBotCached($this->request()->ip()) : false;

        /* находимся ли мы сейчас на каком-либо домене для бота? */
        // $isNowCrawlerDomain = ($_SERVER['HTTP_HOST'] == self::getCrawlerDomain()->domain);
        $isNowAnyCrawlerDomain = in_array($_SERVER['HTTP_HOST'], [
            $domain->redirect_to ? $domain->redirect_to : $domain->domain,
            $domain->yandex_redirect_to ? $domain->yandex_redirect_to : $domain->domain,
        ]);
        $isNowCorrectCrawlerDomain = $_SERVER['HTTP_HOST'] == self::getCrawlerDomain()->domain;

        /* если реальный бот, проверяем что за поисковик и редиректим на нужный домен при необходимости */
        if ($isRealBot) {
            if (! $isNowCorrectCrawlerDomain) {
                // dd("редиректим на домен для бота " . self::getCrawlerDomain()->domain);
                // F::redirect_301($this->getProtocol() . '://' . self::getCrawlerDomain()->domain . $_SERVER['REQUEST_URI']);
                return redirect($this->request()->getScheme().'://'.self::getCrawlerDomain()->domain.$_SERVER['REQUEST_URI'], 301);
            }
        } else {
            /* если не реальный бот, то вместо роботс и сайтмэп - заглушка */
            $uri = parse_url($_SERVER['REQUEST_URI']);
            if (isset($uri['path']) and in_array($uri['path'], ['/robots.txt', '/sitemap.xml'])) {
                // dd('показываем фейк заглушку для роботса и sitemap');
                return $this->fakeBrowserError();
            }
            /* если не реальный бот и мы на актуальном домене для бота (или его нет) */
            if ($isNowAnyCrawlerDomain) {
                /* если валидный реферер или стоит натсройка разрешать невалидные рефереры */
                if ($this->isValidReferer() or $domain->allow_not_valid_referers_for_clients) {
                    /* и есть клиентский домен - редиректим на него */
                    if ($domain->client_redirect_to) {
                        // dd("редиректим на клиентский домен {$domain->client_redirect_to}");
                        // F::redirect($this->getProtocol() . '://' . $domain->client_redirect_to . $_SERVER['REQUEST_URI']);
                        return redirect($this->request()->getScheme().'://'.$domain->client_redirect_to.$_SERVER['REQUEST_URI']);
                    }
                } else {
                    /* если не валидный реферер - показываем заглушку */
                    // dd('показываем фейк заглушку для не бота');
                    return $this->fakeBrowserError();
                }
            }
        }
        // dd('впускаем');
    }

    public function accessForTurbo()
    {
        $hasVariable = collect(request()->all())->keys()->merge(array_keys(request()->cookie()))
            ->contains(fn ($key) => str_starts_with($key, 'acdhk'));
        if ($hasVariable or request()->cookie('access') !== null) {
            return true;
        }
    }

    public function isRealBot()
    {
        return $this->isAcceptedBot() ? F::isRealBotCached($this->request()->ip()) : false;
    }

    /* покывает кастомную страницу 404 в зависимости от браузера */
    public function fakeBrowserError()
    {
        $result = new DeviceDetector($this->request()->userAgent());
        $template = 'fake_error_pages/chrome';
        if (! empty($result->getClient('family'))) {
            if ($result->getClient('family') == 'Safari') {
                $template = 'fake_error_pages/safari';
            }
            if ($result->getClient('family') == 'Internet Explorer') {
                $template = 'fake_error_pages/edge';
            }
            if ($result->getClient('family') == 'Yandex Browser') {
                $template = 'fake_error_pages/yandex';
            }
            if ($result->getClient('family') == 'Firefox') {
                $template = 'fake_error_pages/firefox';
            }
            if ($result->getClient('family') == 'Opera') {
                $template = 'fake_error_pages/opera';
            }
            if ($result->getClient('family') == 'Chrome') {
                $template = 'fake_error_pages/chrome';
            }
        }
        // $template = 'fake_error_pages/firefox';
        // dd($result->browser->name);
        logger()->info("Показали фейковую заглушку {$template}");
        $t = new Template($template);
        $t->v('protocol', $this->request()->getScheme().'://');
        $t->v('requestUri', $this->request()->getRequestUri());
        $t->v('requestedDomain', $this->getRequestedDomainObject()->getDomain());

        // header('HTTP/1.0 404 Not Found');
        // die($t->get());
        return response($t->get(), 404);
    }

    public function visit()
    {
        if (is_null($this->visit)) {
            $query = $this->request()->host().$this->request()->getRequestUri();
            $visit = new Visit;
            $visit->host = $this->request()->host() ? mb_strimwidth($this->request()->host(), 0, 255) : null;
            $visit->domain = $this->request()->host() ? mb_strimwidth($this->request()->host(), 0, 255) : null;
            $visit->query = $query ? mb_strimwidth($query, 0, 255) : null;
            $visit->userAgent = $this->request()->userAgent() ? mb_strimwidth($this->request()->userAgent(), 0, 255) : null;
            $visit->referer = $this->request()->header('Referer') ? mb_strimwidth($this->request()->header('Referer'), 0, 255) : null;
            $visit->get = $this->request()->query() ? mb_strimwidth(serialize($this->request()->query()), 0, 255) : null;
            $visit->post = $this->request()->post() ? mb_strimwidth(serialize($this->request()->post()), 0, 255) : null;
            // $visit->cookie = $this->request()->cookie() ? mb_strimwidth(serialize($this->request()->cookie()), 0, 255) : null;
            // TODO: переименовать cookie в headers и увеличить его длину в идеале
            $visit->cookie = $this->request()->headers->all() ? mb_strimwidth(serialize($this->request()->headers->all()), 0, 255) : null;
            $visit->ip = $this->request()->ip() ?? null;
            $this->visit = $visit;
            /* не сохряняем пока что визит, чтобы минимизировать количество записей, визит сохряняется далее в методе terminate middleware Engine */
        }

        return $this->visit;
    }

    public function getRootPath()
    {
        return realpath(__DIR__.'/../../../..');
    }

    public function getStaticPath()
    {
        return $this->getRootPath().'/static';
    }

    public function getSqlConnection()
    {
        if (! $this->sqlConnection) {
            // подключаемся к БД
            $this->sqlConnection = @mysqli_connect(
                config('database.connections.mysql.host'),
                config('database.connections.mysql.username'),
                config('database.connections.mysql.password'),
                null,
                config('database.connections.mysql.port')
            );
            if (! $this->sqlConnection) {
                Error::simple('DB connection error: '.mysqli_connect_error(), 503);
            }
            $this->sqlConnection->set_charset('utf8mb4');
        }

        return $this->sqlConnection;
    }

    public function getType()
    {
        if ($this->type === false) {
            $this->type = $this->domain()->type;
        }

        return $this->type;
    }

    public function setType($type = null)
    {
        $this->type = $type;
    }

    public function getDisplayErrors()
    {
        return $this->displayErrors;
    }

    public function setDisplayErrors($b = null)
    {
        $this->displayErrors = $b;
        if ($this->getDisplayErrors()) {
            ini_set('display_errors', 1);
            error_reporting(E_ALL);
            set_error_handler([$this, 'PHPErrorLog']);
        } else {
            ini_set('display_errors', 0);
            error_reporting(E_ALL);
            set_error_handler([$this, 'PHPErrorLog']);
        }
    }

    public function getDomain()
    {
        return $this->domain()->domain;
    }

    public function getDebug()
    {
        return $this->debug;
    }

    // иногда нужно чтобы при включенно debug на каких то страницах его не было, например для контроллера загрузки изображений froala
    public function setDebug($b = null)
    {
        $this->debug = $b;
    }

    // Время выполнения скрипта.
    public function getRunningTime()
    {
        $time_end = F::microtime_float();
        $time = round($time_end - $this->getTimeStart(), 4);

        return $time;
    }

    public function getTimeStart()
    {
        return LARAVEL_START;
    }

    // возвращает таблицу текущего типа
    public function typetab($table = null)
    {
        return F::tabname($this->getType(), $table);
    }

    // формирует название таблицы, учитывая префикс
    public function tabname($db = null, $table = null)
    {
        return '`'.config('database.connections.mysql.prefix').$db.'`.`'.$table.'`';
    }

    // ищем класс в текущем типе движка или в engine, или в engine просто если тип engine
    public function findClassMethod($folder = null, $class = null, $method = null)
    {
        $types = ($this->getType() === 'engine') ? ['engine'] : [$this->getType(), 'engine'];

        foreach ($types as $type) {
            $namespace = $type.'\app\\'.$folder;
            $fullClass = $namespace.'\\'.$class;
            if (class_exists($fullClass)) {
                if (method_exists($fullClass, $method)) {
                    return $fullClass;
                }
            }
        }

        /* Если не нашли контроллер, то пробуем искать по полному имени, для совместимости с Laravel */
        if (class_exists($class) and method_exists($class, $method)) {
            return $class;
        }

        return null;
    }

    public function debug()
    {
        $content = '<p>Время генерации: '.$this->getRunningTime().'<br>Запросов: '.Db::getQueryCount().'</p>';
        $list = '';
        foreach (Db::getList() as $v) {
            $list .= '<li><pre>'.sprintf('%f', $v['time']).' сек - '.$v['query'].'</pre></li>';
        }
        $content .= '<ol>'.$list.'</ol>';
        $content = '<div style="padding: 20px; background-color: #fff; color: #000;">'.$content.'</div>';

        return $content;
    }

    /* костыль, если нет megaweb роутера, то возвращаем настройки laravel роутера, сделано для того, чтобы не переписывать старый код и работал F::setPageVars() */
    public function router()
    {
        if (! $this->router) {
            return \engine\app\services\RegisterRoutesForDomainService::getRouteSettings(Route::current());
        }

        return $this->router;
    }

    // обработчик PHP ошибок
    public function PHPErrorLog($errno, $errmsg, $file, $line)
    {
        // die($errmsg);
        // error('Произошла ошибка','PHP ошибка: '."\r\n".'Текст: '.$errmsg."\r\n".'Файл: '.$file."\r\n".'Строка: '.$line);
        logger()->error('PHP ошибка: '.$errmsg."\r\n".'Файл: '.$file.' строка: '.$line);
    }

    public function getReload()
    {
        return Config::cached()->reload;
    }

    public function getClientCountry()
    {
        return $this->getGeoIpObject()->code();
    }

    public function checkRecaptcha($response = null, $secret = null)
    {
        if (! $response) {
            return false;
        }
        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $this->request()->ip());
        if ($resp->isSuccess()) {
            return true;
        } else {
            return false;
        }
    }

    public function template($file = null)
    {
        $t = new Template;
        $t->setPath($file ? $file : $this->router()->template);

        return $t;
    }

    public function getStaticUrl()
    {
        return '/static';
    }

    public function getGeoIpObject()
    {
        if (is_null($this->geoIpObject)) {
            $this->geoIpObject = new GeoIP($this->request()->ip());
        }

        return $this->geoIpObject;
    }

    public function getDomainObject()
    {
        return $this->domain()?->megaweb;
    }

    public function domain()
    {
        return $this->requestedDomain()->mirror_of_or_self ?? null;
    }

    public function getRequestedDomainObject()
    {
        return $this->requestedDomain()->megaweb ?? null;
    }

    public function requestedDomain()
    {
        if ($this->requestedDomain === false) {
            $this->requestedDomain = Domain::find($this->request()->host());
        }

        return $this->requestedDomain;
    }

    // TODO: убрать
    public function getRequestedDomainEloquent()
    {
        return $this->requestedDomain();
    }

    public function getDomainEloquent()
    {
        return $this->domain();
    }
    //

    public function getCrawlerOrClientDomain()
    {
        if (is_null($this->crawlerOrClientDomain)) {
            $this->crawlerOrClientDomain = $this->domain()->getCrawlerOrClientDomain($this->getCrawler());
        }

        return $this->crawlerOrClientDomain;
    }

    public function getCrawlerDomain()
    {
        if (is_null($this->crawlerDomain)) {
            $this->crawlerDomain = $this->domain()->getCrawlerDomain($this->getCrawler());
        }

        return $this->crawlerDomain;
    }

    public function getClientBan()
    {
        if ($this->ban === false) {
            $this->ban = Ban::query()
                ->where(function ($query) {
                    $query
                        ->where('ip', '')
                        ->orWhereRaw('? REGEXP ip', [$this->request()->ip()]);
                })
                ->where(function ($query) {
                    $query
                        ->where('user_agent', '')
                        ->orWhereRaw('? REGEXP user_agent', [$this->request()->userAgent()]);
                })
                ->where(function ($query) {
                    $query
                        ->where('referer', '')
                        ->orWhereRaw('? REGEXP referer', [$this->request()->header('referer')]);
                })
                ->where(function ($query) {
                    $query->where('query', '')
                        ->orWhereRaw('? REGEXP query', [$this->request()->host().$this->request()->getRequestUri()]);
                })
                ->orderByDesc('active')
                ->orderByDesc('hide_player')
            // ->ddrawsql()
                ->first();
        }

        return $this->ban;
    }

    public function setClientBan(Ban $ban)
    {
        $this->ban = $ban;
    }

    public function route()
    {
        return RegisterRoutesForDomainService::getRouteSettings(Route::current());
    }

    public function auth()
    {
        return $this->auth;
    }

    public function setAuth($auth = null)
    {
        $this->auth = $auth;
    }
}
