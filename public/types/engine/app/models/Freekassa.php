<?php

namespace engine\app\models;

class Freekassa
{
    private $api_key = null;

    private $secret_1 = null;

    private $secret_2 = null;

    private $merchant_id = null;

    public function request($method = null, $data = [])
    {
        if (empty($method)) {
            F::error('Method required to ise Freekassa::request');
        }
        if (empty($data)) {
            F::error('Array of args required to use Freekassa::request');
        }
        if (empty($this->getApiKey())) {
            F::error('ApiKey required to use Freekassa::request');
        }
        $data['nonce'] = time();
        ksort($data);
        $sign = hash_hmac('sha256', implode('|', $data), $this->getApiKey());
        $data['signature'] = $sign;
        $request = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.freekassa.ru/v1/'.$method);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $result = trim(curl_exec($ch));
        curl_close($ch);
        $response = json_decode($result, true);

        return $response;
    }

    public function getApiKey()
    {
        return $this->api_key;
    }

    public function setApiKey($api_key = null)
    {
        $this->api_key = $api_key;
    }

    public function getAvailableCurrencies()
    {
        if (empty($this->getMerchantId())) {
            F::error('merchantId required to use Freekassa::request');
        }

        return $this->request('withdrawals/currencies', [
            'shopId' => $this->getMerchantId(),
        ]);
    }

    public function setMerchantId($id = null)
    {
        $this->merchant_id = $id;
    }

    public function getMerchantId()
    {
        return $this->merchant_id;
    }

    public function getSecret1()
    {
        return $this->secret_1;
    }

    public function setSecret1($value = null)
    {
        $this->secret_1 = $value;
    }

    public function getSecret2()
    {
        return $this->secret_2;
    }

    public function setSecret2($value = null)
    {
        $this->secret_2 = $value;
    }

    public static function checkIp($ip = null)
    {
        if (empty($ip)) {
            F::error('IP required to use Freekassa::checkIp');
        }
        if (! in_array($ip, ['168.119.157.136', '168.119.60.227', '138.201.88.124', '178.154.197.79'])) {
            return false;
        }

        return true;
    }
}
