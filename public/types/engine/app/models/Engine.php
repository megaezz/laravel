<?php

namespace engine\app\models;

final class Engine
{
    private static $instance = null;

    public static function __callStatic($name = null, $arguments = null)
    {
        // if ($name == 'tabname') {dump($arguments);}
        if (! method_exists(self::getInstance(), $name)) {
            F::error('Method "'.$name.'" doesn\'t exist in Engine class');
        }

        return call_user_func_array([self::getInstance(), $name], $arguments);
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new EngineClass;
        }

        return self::$instance;
    }
}
