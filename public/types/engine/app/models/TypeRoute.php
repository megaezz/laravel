<?php

namespace engine\app\models;

class TypeRoute
{
    private $alias = null;

    private $cache = null;

    private $pattern = null;

    private $controller = null;

    private $method = null;

    private $title = null;

    private $description = null;

    private $keywords = null;

    private $h1 = null;

    private $topText = null;

    private $bottomText = null;

    private $bread = null;

    private $text1 = null;

    private $text2 = null;

    private $h2 = null;

    private $template = null;

    private $var1 = null;

    private $var2 = null;

    private $var3 = null;

    private $var4 = null;

    private $var5 = null;

    private $comment = null;

    private $patternId = null;

    private $redirectToAlias = null;

    public function __construct($pattern = null)
    {
        if (! $pattern) {
            F::error('Specify pattern to create siteRoute');
        }
        $this->setPattern($pattern);
        $arr = $this->getArr();
        if (! $arr) {
            F::error('Such pattern is not exist for this type');
        }
        $this->setProprerties($arr);
    }

    public function setProprerties($arr = [])
    {
        $this->setAlias($arr['alias']);
        $this->setCache($arr['cache']);
        $this->setPattern($arr['pattern']);
        // дубль на случай замены паттерна, чтобы потом правильно отработал save
        $this->setPatternId($arr['pattern']);
        $this->setController($arr['controller']);
        $this->setMethod($arr['method']);
        $this->setTitle($arr['title']);
        $this->setDescription($arr['description']);
        $this->setKeywords($arr['keywords']);
        $this->setH1($arr['h1']);
        $this->setTopText($arr['topText']);
        $this->setBottomText($arr['bottomText']);
        $this->setBread($arr['bread']);
        $this->setText1($arr['text1']);
        $this->setText2($arr['text2']);
        $this->setH2($arr['h2']);
        $this->setTemplate($arr['template']);
        $this->setVar1($arr['var1']);
        $this->setVar2($arr['var2']);
        $this->setVar3($arr['var3']);
        $this->setVar4($arr['var4']);
        $this->setVar5($arr['var5']);
        $this->setComment($arr['comment']);
        $this->setRedirectToAlias($arr['redirectToAlias']);
    }

    public function getArr()
    {
        return F::query_assoc('select pattern,cache,alias,controller,method,var1,var2,var3,var4,var5,
			title,description,keywords,h1,topText,bottomText,bread,text1,text2,h2,template,comment,
			redirectToAlias
			from '.F::tabname('engine', 'routes').'
			where
			pattern = \''.F::escape_string($this->getPattern()).'\'
			;');
    }

    public function getAlias($v = null)
    {
        return $this->alias;
    }

    public function getCache($v = null)
    {
        return $this->cache;
    }

    public function getPattern($v = null)
    {
        return $this->pattern;
    }

    public function getController($v = null)
    {
        return $this->controller;
    }

    public function getVar1($v = null)
    {
        return $this->var1;
    }

    public function getVar2($v = null)
    {
        return $this->var2;
    }

    public function getVar3($v = null)
    {
        return $this->var3;
    }

    public function getVar4($v = null)
    {
        return $this->var4;
    }

    public function getVar5($v = null)
    {
        return $this->var5;
    }

    public function getMethod($v = null)
    {
        return $this->method;
    }

    public function getTitle($v = null)
    {
        return $this->title;
    }

    public function getDescription($v = null)
    {
        return $this->description;
    }

    public function getKeywords($v = null)
    {
        return $this->keywords;
    }

    public function getH1($v = null)
    {
        return $this->h1;
    }

    public function getTopText($v = null)
    {
        return $this->topText;
    }

    public function getBottomText($v = null)
    {
        return $this->bottomText;
    }

    public function getBread($v = null)
    {
        return $this->bread;
    }

    public function getText1($v = null)
    {
        return $this->text1;
    }

    public function getText2($v = null)
    {
        return $this->text2;
    }

    public function getH2($v = null)
    {
        return $this->h2;
    }

    public function getTemplate($v = null)
    {
        return $this->template;
    }

    public function getComment($v = null)
    {
        return $this->comment;
    }

    public function setAlias($v = null)
    {
        $this->alias = $v;
    }

    public function setCache($v = null)
    {
        $this->cache = $v;
    }

    public function setPattern($v = null)
    {
        $this->pattern = $v;
    }

    public function setController($v = null)
    {
        $this->controller = $v;
    }

    public function setVar1($v = null)
    {
        $this->var1 = $v;
    }

    public function setVar2($v = null)
    {
        $this->var2 = $v;
    }

    public function setVar3($v = null)
    {
        $this->var3 = $v;
    }

    public function setVar4($v = null)
    {
        $this->var4 = $v;
    }

    public function setVar5($v = null)
    {
        $this->var5 = $v;
    }

    public function setMethod($v = null)
    {
        $this->method = $v;
    }

    public function setTitle($v = null)
    {
        $this->title = $v;
    }

    public function setDescription($v = null)
    {
        $this->description = $v;
    }

    public function setKeywords($v = null)
    {
        $this->keywords = $v;
    }

    public function setH1($v = null)
    {
        $this->h1 = $v;
    }

    public function setTopText($v = null)
    {
        $this->topText = $v;
    }

    public function setBottomText($v = null)
    {
        $this->bottomText = $v;
    }

    public function setBread($v = null)
    {
        $this->bread = $v;
    }

    public function setText1($v = null)
    {
        $this->text1 = $v;
    }

    public function setText2($v = null)
    {
        $this->text2 = $v;
    }

    public function setH2($v = null)
    {
        $this->h2 = $v;
    }

    public function setTemplate($v = null)
    {
        $this->template = $v;
    }

    public function setComment($v = null)
    {
        $this->comment = $v;
    }

    public function save()
    {
        F::query('
			update '.F::tabname('engine', 'site_routes').' set
			pattern = \''.F::escape_string($this->getPattern()).'\',
			cache = \''.F::escape_string($this->getCache()).'\',
			alias = \''.F::escape_string($this->getAalias()).'\',
			controller = \''.F::escape_string($this->getController()).'\',
			method = \''.F::escape_string($this->getMethod()).'\',
			var1 = \''.F::escape_string($this->getVar1()).'\',
			var2 = \''.F::escape_string($this->getVar2()).'\',
			var3 = \''.F::escape_string($this->getVar3()).'\',
			var4 = \''.F::escape_string($this->getVar4()).'\',
			var5 = \''.F::escape_string($this->getVar5()).'\',
			title = \''.F::escape_string($this->getTitle()).'\',
			description = \''.F::escape_string($this->getDescription()).'\',
			keywords = \''.F::escape_string($this->getKeywords()).'\',
			h1 = \''.F::escape_string($this->getH1()).'\',
			h2 = \''.F::escape_string($this->getH2()).'\',
			topText = \''.F::escape_string($this->getTopText()).'\',
			bottomText = \''.F::escape_string($this->getBottomText()).'\',
			text1 = \''.F::escape_string($this->getText1()).'\',
			text2 = \''.F::escape_string($this->getText2()).'\',
			bread = \''.F::escape_string($this->getBread()).'\',
			template = \''.F::escape_string($this->getTemplate()).'\',
			comment = \''.F::escape_string($this->getComment()).'\',
			redirectToAlias = \''.($this->getRedirectToAlias() ? 1 : 0).'\'
			where
			pattern = \''.F::escape_string($this->getPatternId()).'\'
			;');

        return true;
    }

    public static function add($pattern = null)
    {
        F::query('
			insert into '.F::tabname('engine', 'routes').' set
			pattern = \''.F::escape_string($pattern).'\'
			;');
    }

    public static function delete($pattern = null)
    {
        F::query('
			delete from '.F::tabname('engine', 'routes').'
			where
			pattern = \''.F::escape_string($pattern).'\'
			;');
    }

    public function getRedirectToAlias()
    {
        return $this->redirectToAlias;
    }

    public function setRedirectToAlias($b = null)
    {
        $this->redirectToAlias = $b;
    }
}
