<?php

namespace engine\app\models\HamtimAmocrm;

use engine\app\models\F;

class HamtimAmocrm
{
    public $settings;

    // public $subdomain;
    public $auth;

    public function __construct($login = null, $api = null, $subdomain = null, $session_id = null)
    {
        $this->settings = new \stdClass;
        $this->settings->amocrm = new \stdClass;
        $this->settings->amocrm->api = $api;
        $this->settings->amocrm->login = $login;
        $this->settings->amocrm->subdomain = $subdomain;
        $this->settings->amocrm->session_id = $session_id;
        $this->amocrm_auth();
    }

    public function amocrm_auth()
    {
        if (isset($this->settings->amocrm)) {
            if ($this->settings->amocrm->api && $this->settings->amocrm->login && $this->settings->amocrm->subdomain) {
                $subdomain = $this->settings->amocrm->subdomain;
                // $this->subdomain = $subdomain;
                $user = [
                    'USER_LOGIN' => $this->settings->amocrm->login,
                    'USER_HASH' => $this->settings->amocrm->api,
                ];
            } else {
                return 'Нет данных для авторизации';
            }
        } else {
            return 'Нет данных для авторизации';
        }
        $link = 'https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';
        $curl = curl_init(); // Сохраняем дескриптор сеанса cURL
        // Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($user, null, '&', PHP_QUERY_RFC1738));
        // curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl, CURLOPT_HEADER, true);
        // curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        // curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        // if ($this->settings->amocrm->session_id) {
        // 	# sending manually set cookie
        // 	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Cookie: session_id='.$this->settings->amocrm->session_id));
        // }
        if ($this->getSessionId()) {
            // sending manually set cookie
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Cookie: session_id='.$this->getSessionId()]);
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl); // Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE); // Получим HTTP-код ответа сервера
        // $cookielist = curl_getinfo($curl,CURLINFO_COOKIELIST);
        // F::dump($cookielist);
        // F::pre($out);
        // получаем устанавливаемые куки
        // multi-cookie variant contributed by @Combuster in comments
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $out, $matches);
        $cookies = [];
        foreach ($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }
        if (isset($cookies['session_id'])) {
            $this->settings->amocrm->session_id = $cookies['session_id'];
        }
        // теперь удаляем заголовок из ответа
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($out, 0, $header_size);
        $out = substr($out, $header_size);
        // F::dump($out);
        // F::dump($cookies);
        // foreach ($cookielist as $v) {
        // 	$arr = explode("\t",$v);
        // 	$name = $arr[5];
        // 	$value = $arr[6];
        // 	if ($name == 'session_id') {
        // 		$session_id = $value;
        // 	}
        // }
        // F::dump($cookielist);
        // F::dump($session_id);
        curl_close($curl); // Заверашем сеанс cURL
        $auth = json_decode($out);
        // F::dump($auth);
        if ($out) {
            $this->auth = $auth->response->auth;
        }

        return $out;
    }

    public function q($path, $fields = [], $ifModifiedSince = false)
    {
        return $this->amocrm_query($path, $fields, $ifModifiedSince);
    }

    public function l($l)
    {
        echo '<pre>';
        var_dump($l);
        echo '</pre>';
    }

    public function amocrm_query($path, $fields = [], $ifModifiedSince = false)
    {
        $link = 'https://'.$this->settings->amocrm->subdomain.'.amocrm.ru'.$path;
        $curl = curl_init(); // Сохраняем дескриптор сеанса cURL
        // Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        if ($ifModifiedSince) {
            $httpHeader = ['IF-MODIFIED-SINCE: '.$ifModifiedSince];
        } else {
            $httpHeader = [];
        }
        if (count($fields)) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($fields));
            $httpHeader[] = 'Content-Type: application/json';
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($curl, CURLOPT_HEADER, false);
        // curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        // curl_setopt($curl,CURLOPT_COOKIEFILE,'');
        // curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        // F::dump($this->settings->amocrm->session_id);
        if ($this->getSessionId()) {
            // sending manually set cookie
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Cookie: session_id='.$this->getSessionId()]);
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl); // Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        // $cookies = curl_getinfo($curl, CURLINFO_COOKIELIST);
        // F::dump($cookies);
        // $this->l(curl_getinfo($curl));
        return json_decode($out);
    }

    public function getSessionId()
    {
        return $this->settings->amocrm->session_id;
    }
}
