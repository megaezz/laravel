<?php

namespace engine\app\models;

class Articles
{
    private $limit = null;

    private $page = null;

    private $order = null;

    private $rows = null;

    private $domain = null;

    // private $bookedDomain = null;
    private $active = null;

    protected const TYPE = 'engine';

    protected const CLASS_ARTICLE = 'engine\app\models\Article';

    private $listTemplate = null;

    private $activeId = null;

    private $paginationUrlPattern = null;

    private $excludeArticles = [];

    public function get()
    {
        if (! $this->limit) {
            $this->setLimit(1);
        }
        if (! $this->page) {
            $this->setPage(1);
        }
        $sql['from'] = ($this->page - 1) * $this->limit;
        $sql['domain'] = $this->domain ? ('domain = \''.$this->domain.'\'') : 1;
        // $sql['bookedDomain'] = $this->bookedDomain?('booked_domain = \''.$this->bookedDomain.'\''):1;
        $sql['order'] = $this->order ? ('order by '.$this->order) : '';
        if (is_null($this->active)) {
            $sql['active'] = 1;
        } else {
            $sql['active'] = $this->active ? 'active' : 'not active';
        }
        $sql['excludeArticles'] = $this->getExcludeArticles() ? ('articles.id not in ('.implode(',', $this->getExcludeArticles()).')') : 1;
        $arr = F::query_arr('select SQL_CALC_FOUND_ROWS id
			from '.F::tabname(static::TYPE, 'articles').'
			where
			'.$sql['domain'].' and
			'.$sql['active'].' and
			'.$sql['excludeArticles'].'
			'.$sql['order'].'
			limit '.$sql['from'].','.$this->limit.'
			;');
        $this->rows = F::rows_without_limit();

        return $arr;
    }

    public function getList()
    {
        if (! $this->getListTemplate()) {
            F::error('use setListTemplate');
        }
        $list = '';
        $arr = $this->get();
        foreach ($arr as $v) {
            $t = new Template($this->getListTemplate());
            $a = (new \ReflectionClass(static::CLASS_ARTICLE))->newInstance($v['id']);
            $this->setListVars($t, $a);
            $list .= $t->get();
        }

        return $list;
    }

    public function setListVars($t = null, $a = null)
    {
        $t->v('title', empty($a->getTitle()) ? 'Без названия' : $a->getTitle());
        // $t->v('text',$a->getText());
        $t->v('id', $a->getId());
        $t->v('classActive', ($a->getId() === $this->getActiveId()) ? 'active' : '');
        $t->v('mainImageUrl', $a->getMainImageUrl());
        $t->v('textPreview', $a->getTextPreview());
        $t->v('chpu', $a->getChpu());
        $t->v('domain', $a->getDomain() ? $a->getDomain() : '');
        $t->v('active', $a->getActive() ? 1 : 0);
        if ($a->getDomain()) {
            $domainObj = new Domain($a->getDomain());
            $t->v('lastRedirectDomain', $domainObj->getLastRedirectDomain() ? $domainObj->getLastRedirectDomain() : '');
        } else {
            $t->v('lastRedirectDomain', '');
        }
        if ($a->getApplyDate()) {
            $addDate = new \DateTime($a->getApplyDate());
            $formatter = new \IntlDateFormatter(
                'ru_RU',
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::NONE,
                'Europe/Moscow'
            );
            $t->v('applyDate', $formatter->format($addDate));
        } else {
            $t->v('applyDate', '');
        }
    }

    public function getRows()
    {
        if (is_null($this->rows)) {
            $this->get();
        }

        return $this->rows;
    }

    public function setLimit($limit = null)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setOrder($order = null)
    {
        $this->order = $order;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setDomain($domain = null)
    {
        $this->domain = $domain;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getListTemplate()
    {
        return $this->listTemplate;
    }

    public function setListTemplate($template = null)
    {
        $this->listTemplate = $template;
    }

    // выбранная категория, для css active
    public function setActiveId($id = null)
    {
        $this->activeId = $id;
    }

    public function getActiveId()
    {
        return $this->activeId;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($b = null)
    {
        $this->active = $b;
    }

    public function getPages()
    {
        if (! $this->getPaginationUrlPattern()) {
            F::error('setPaginationUrlPattern first');
        }
        $Paginator = new PaginatorCustom;
        $Paginator->setUrlPattern($this->getPaginationUrlPattern());
        $Paginator->setTemplateList('pagination/list');
        $Paginator->setTemplatePrev('pagination/prev');
        $Paginator->setTemplateNext('pagination/next');
        $Paginator->setRows($this->getRows());
        $Paginator->setLimit($this->getLimit());
        $Paginator->setPage($this->getPage());
        $Paginator->setMaxPagesToShow(4);

        return $Paginator->get();
        // return new Paginator($this->getRows(),$this->getLimit(),$this->getPage(),$this->getPaginationUrlPattern());
    }

    public function getPaginationUrlPattern()
    {
        return $this->paginationUrlPattern;
    }

    public function setPaginationUrlPattern($v = null)
    {
        $this->paginationUrlPattern = $v;
    }

    public function getExcludeArticles()
    {
        return $this->excludeArticles;
    }

    public function setExcludeArticles($array = null)
    {
        $this->excludeArticles = $array;
    }

    public function setExcludeArticle($id = null)
    {
        $this->excludeArticles[] = $id;
    }

    public function setRows($rows = null)
    {
        $this->rows = $rows;
    }
}
