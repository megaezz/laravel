<?php

namespace engine\app\models;

class PaginatorCustom
{
    // шаблоы элементов пагинатора
    private $templateList = null;

    private $templateNext = null;

    private $templatePrev = null;

    private $rows = null;

    private $limit = null;

    private $page = null;

    private $maxPagesToShow = 10;

    private $urlPattern = null;

    private $lang = null;

    public function setMaxPagesToShow($v = null)
    {
        $this->maxPagesToShow = $v;
    }

    public function setUrlPattern($v = null)
    {
        $this->urlPattern = $v;
    }

    public function setTemplateList($v = null)
    {
        $this->templateList = $v;
    }

    public function setTemplatePrev($v = null)
    {
        $this->templatePrev = $v;
    }

    public function setTemplateNext($v = null)
    {
        $this->templateNext = $v;
    }

    public function setRows($v = null)
    {
        $this->rows = $v;
    }

    public function setLimit($v = null)
    {
        $this->limit = $v;
    }

    public function setPage($v = null)
    {
        $this->page = $v;
    }

    public function get()
    {
        if (! $this->urlPattern) {
            F::error('Use setUrlPattern first');
        }
        if (! $this->templateList) {
            F::error('Use setTemplateList first');
        }
        if (! $this->templatePrev) {
            F::error('Use setTemplatePrev first');
        }
        if (! $this->templateNext) {
            F::error('Use setTemplateNext first');
        }
        if (is_null($this->rows)) {
            F::error('Use setRows first');
        }
        if (! $this->limit) {
            F::error('Use setLimit first');
        }
        if (! $this->page) {
            F::error('Use setPage first');
        }
        // if (!$this->urlPattern) {F::error('UrlPattern required');};
        $rows = $this->rows;
        $Paginator = new Paginator($rows, $this->limit, $this->page, $this->urlPattern);
        $Paginator->setMaxPagesToShow($this->maxPagesToShow);
        $arr = $Paginator->getPages();
        $list = '';
        // f::dump($Paginator->getNumPages());
        if ($Paginator->getNumPages() > 1) {
            $tPrev = new Template($this->templatePrev);
            $tPrev->setLang($this->getLang());
            $tPrev->v('disabled', $Paginator->getPrevPage() ? '' : 'disabled');
            $tPrev->v('num', $Paginator->getPrevPage() ? $Paginator->getPrevPage() : '');
            $tPrev->v('url', $Paginator->getPrevUrl() ? $Paginator->getPrevUrl() : '');
            $list .= $tPrev->get();
        }
        foreach ($arr as $page) {
            $tList = new Template($this->templateList);
            $tList->setLang($this->getLang());
            $tList->v('active', $page['isCurrent'] ? 'active' : '');
            $tList->v('num', $page['num']);
            $tList->v('disabled', $page['url'] ? '' : 'disabled');
            $tList->v('url', empty($page['url']) ? '#' : $page['url']);
            $list .= $tList->get();
        }
        if ($Paginator->getNumPages() > 1) {
            $tNext = new Template($this->templateNext);
            $tNext->setLang($this->getLang());
            $tNext->v('disabled', $Paginator->getNextPage() ? '' : 'disabled');
            $tNext->v('num', $Paginator->getNextPage() ? $Paginator->getNextPage() : '');
            $tNext->v('url', $Paginator->getNextUrl() ? $Paginator->getNextUrl() : '');
            $list .= $tNext->get();
        }

        return $list;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang = null)
    {
        $this->lang = $lang;
    }
}
