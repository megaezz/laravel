<?php

namespace copy\app\controllers;

class ZenvixController extends CopyController
{
    public function getContent($args = [])
    {
        $content = parent::getContent($args);
        $content = str_replace('var $url = "ze" + "nvix" + ".net"; if ( $url !== location.host) {location.href = \'https://\' + $url;}', '', $content);

        return $content;
    }
}
