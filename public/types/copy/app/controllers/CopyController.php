<?php

namespace copy\app\controllers;

use App\Models\Proxy;
use engine\app\models\Engine;
use engine\app\models\F;

class CopyController
{
    public function getContent($args = [])
    {
        Engine::setDebug(false);
        $args['domain'] = Engine::router()->getTemplate();
        $args['request_uri'] = request()->getRequestUri();
        $result = F::cache($this, 'getContentResult', $args, 3600);
        $content = convert_uudecode($result->content);
        if ($result->content_type) {
            // добавляем заголовок типа контента
            header($result->content_type);
            // меняем ссылки в контенте если это html,txt,xml
            if (
                strstr($result->content_type, 'text/html') or
                strstr($result->content_type, 'text/plain') or
                strstr($result->content_type, 'application/xhtml+xml') or
                strstr($result->content_type, 'application/xml')
            ) {
                $content = str_replace($args['domain'], 'https://'.Engine::getDomainObject()->getDomain(), $content);
            }
            // добавляем метрику перед body
            if (strstr($result->content_type, 'text/html')) {
                $content = str_replace('</body>', view('types.engine.metrika')->render().'</body>', $content);
            }
        }

        return $content;
    }

    public function getContentResult($args = [])
    {
        Engine::setDebug(false);
        $url = $args['domain'].$args['request_uri'];
        $proxy = Proxy::where('country', 'UA')->whereActive(true)->first();
        if (! $proxy) {
            throw new \Exception('Нет нужного прокси');
        }
        // загружаем страницу
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_PROXY => $proxy->proxy,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.4.1 Safari/605.1.15',
            CURLOPT_HEADER => true,
            CURLOPT_TIMEOUT => 2,
        ]);
        $ret = curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // F::pre($ret);
        // F::pre($response_code);
        $header_len = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($ret, 0, $header_len);
        $content = substr($ret, $header_len);
        curl_close($ch);
        $header_arr = explode("\r\n", $header);
        $content_type = null;
        foreach ($header_arr as $v) {
            if (strstr($v, 'Content-Type:') or strstr($v, 'content-type:')) {
                $content_type = $v;
            }
        }
        $result = new \stdClass;
        $result->content = convert_uuencode($content);
        $result->content_type = $content_type;
        $result->response_code = $response_code;

        return $result;
    }
}
