<?php

function sort_is_active($sort)
{
    if (empty($sort)) {
        error('�� �������� ����������');
    }
    if (empty($_GET['sort'])) {
        // error('�� �������� ����������');
        $_GET['sort'] = 'mr';
    }
    $content = '';
    if ($sort == $_GET['sort']) {
        $content = 'class="active"';
    }

    return $content;
}

// ���������� ������ ����� ��� �������� ��������
function video_collection_mobile()
{
    global $config;
    $arr = func_replacement('preparing_video_collection_mobile');

    return thumbs_from_array($arr['videos']);
}

function preparing_video_collection_mobile()
{
    return preparing_video_collection('yes');
}

function preparing_video_collection($mobile = 'no')
{
    global $config;
    if (empty($_GET['p']) == 1) {
        $_GET['p'] = 1;
    }
    if (empty($_GET['sort'])) {
        $_GET['sort'] = 'mr';
    }
    if ($_GET['sort'] == 'mv') {
        $sort = 'views';
    }
    if ($_GET['sort'] == 'mr') {
        $sort = 'add_date';
    }
    if ($_GET['sort'] == 'tr') {
        $sort = 'rating desc, votes';
    }
    if (empty($sort)) {
        error('�������� ����������');
    }
    if ($mobile == 'no') {
        $mobile_only_mp4 = '';
        $per_page = value_from_config_for_domain('smsvideo', 'collection_videos_per_page'); // ���������� �� ��������
    }
    if ($mobile == 'yes') {
        $mobile_only_mp4 = 'ext=\'mp4\'';
        $per_page = value_from_config_for_domain('smsvideo', 'collection_videos_per_page_mobile'); // ���������� �� ��������
    }
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_ru,title_ru_translit,description,unix_timestamp(add_date) as date,
		ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub
		from '.tabname('smsvideo', 'videos').' 
						left join '.tabname('smsvideo', 'videos_views').' on videos.id=videos_views.video_id
						where '.$mobile_only_mp4.'
						order by '.$sort.' desc limit '.$from.','.$per_page);
    $arr = readdata($data, 'nokey');
    $content_array['videos'] = $arr;
    $content_array['rows'] = rows_without_limit();

    return $content_array;
}

function thumbs_from_array($arr)
{
    global $config;
    $list = '';
    if (empty($arr)) {
        // alert('�� ������ �� ������ �����');
        $list = '��� ����������� ��� �����������.';
        logs('�� ������� �� ������ �����');
    }
    foreach ($arr as $v) {
        $date_arr = my_date($v['date']);
        $date = $date_arr['day'].' '.$date_arr['month'].' '.$date_arr['year'];
        if (date('d.m.Y', $v['date']) == date('d.m.Y')) {
            $date = '�������';
        }
        if (date('d.m.Y', $v['date']) == date('d.m.Y', time() - 86400)) {
            $date = '�����';
        }
        if (date('d.m.Y', $v['date']) == date('d.m.Y', time() - 86400 * 2)) {
            $date = '���������';
        }
        $config['smsvideo']['vars']['current']['date'] = $date;
        $config['smsvideo']['vars']['current']['id'] = $v['id'];
        $config['smsvideo']['vars']['current']['title_ru'] = $v['title_ru'];
        $config['smsvideo']['vars']['current']['title_ru_translit'] = $v['title_ru_translit'];
        $config['smsvideo']['vars']['current']['description'] = '';
        if (isset($v['description'])) {
            temp_var('description', $v['description']);
        }
        $config['smsvideo']['vars']['current']['views'] = $v['views'];
        $config['smsvideo']['vars']['current']['rating'] = $v['rating'];
        $config['smsvideo']['vars']['current']['votes'] = $v['votes'];
        $config['smsvideo']['vars']['current']['id_hub'] = $v['id_hub'];
        if (isset($v['categories_of_video'])) {
            temp_var('categories_of_video', $v['categories_of_video']);
        }
        $list .= template('li/thumbs');
    }

    return $list;
}

// ����� ������ ������� ��� �������� video_collection
function pronchik_pages_video_collection_mobile()
{
    $arr = func_replacement('preparing_video_collection_mobile');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('smsvideo', 'collection_videos_per_page_mobile'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('smsvideo', 'pages_length_mobile');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/'.$_GET['get_sort'].'/', $pages_length, '', '�����', '������', 'no', 'active', 'points', '1');

    return $list;
}

function pronchik_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^porno\-categories$/', $query, $args)) {
        $config['vars']['page'] = 'categories_list';

        return;
    }
    if (preg_match('/^save\/([\w]+)\/([\w-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = get_video_id_from_translit($args[2]);

        return;
    }
    if (preg_match('/^porno\/([\w-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['title_ru_translit'] = $args[1];
        $_GET['id'] = get_video_id_from_translit($args[1]);

        return;
    }
    if (preg_match('/^(new|popular)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        if ($args[1] == 'new') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'popular') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^(new|popular)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        if ($args[1] == 'new') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'popular') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^([\w-]+)\/(new|popular)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        if ($args[2] == 'new') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'popular') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^([\w-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^contact$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
}

function pronchik_pages_categories_videos_mobile()
{
    $arr = func_replacement('preparing_categories_videos');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('smsvideo', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('smsvideo', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/'.$_GET['name_translit'].'/'.$_GET['get_sort'].'/', $pages_length, '', '�����', '������', 'no', 'active', 'points', '1');

    return $list;
}

function categories_list_mobile()
{
    return categories_list('yes');
}

// ���������� ������ ��������� ��� ������.
function categories_list($mobile = 'no')
{
    global $config;
    if ($mobile == 'no') {
        $mobile_only_mp4 = '';
    }
    if ($mobile == 'yes') {
        $mobile_only_mp4 = 'ext=\'mp4\'';
    }
    $data = query('
		select categories_domains.category_id,categories_domains.id,categories_groups.name,categories_groups.name_translit
		from '.tabname('smsvideo', 'categories_domains').'
		inner join '.tabname('smsvideo', 'categories_groups').'  on categories_domains.category_id=categories_groups.id
		order by name');
    $arr = readdata($data, 'nokey');
    $print = '';
    $auto_categories = value_from_config_for_domain('smsvideo', 'auto_categories');
    foreach ($arr as $v) {
        if ($auto_categories == '0') {
            $data = query('
			SELECT count(videos.id) as q_videos
			FROM '.tabname('smsvideo', 'videos').'
			WHERE '.$mobile_only_mp4.' and category_id=\''.$v['id'].'\'
			');
        }
        if ($auto_categories == '1') {
            // ��� �� categories_videos
            // ������ �� ��������� ����� ����������� � ������ ���������
            $data = query('
				SELECT catgroups_tags.tag_ru
				FROM '.tabname('smsvideo', 'categories_domains').'
				INNER JOIN  '.tabname('smsvideo', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
				INNER JOIN  '.tabname('smsvideo', 'catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
				WHERE categories_domains.id=\''.$v['id'].'\'
				');
            $arr2 = readdata($data, 'nokey');
            // ��������� ������ ������ �����
            $cat_tags = [];
            foreach ($arr2 as $v2) {
                $cat_tags[] = $v2['tag_ru'];
            }
            if (empty($cat_tags)) {
                error('��������� ����������', '��� ��������� '.$v['id'].' ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
            }
            // ������ �� ��������� ���������� ���� ���������� ID, ��������������� �������
            $data = query('
				SELECT count(distinct tags.video_id) as q_videos
				FROM '.tabname('smsvideo', 'tags').'
				inner join '.tabname('smsvideo', 'tags_translation').' on tags.tag_en=tags_translation.tag_en
				inner join '.tabname('smsvideo', 'videos').' on videos.id=tags.video_id
				WHERE '.$mobile_only_mp4.' and tag_ru in ('.array_to_str($cat_tags).')
				');
        }
        $arr2 = mysql_fetch_array($data);
        $rows = $arr2[0];
        // *���
        $config['smsvideo']['vars']['current']['id'] = $v['id'];
        $config['smsvideo']['vars']['current']['name_translit'] = $v['name_translit'];
        $config['smsvideo']['vars']['current']['name'] = $v['name'];
        $config['smsvideo']['vars']['current']['name_url'] = str_replace(' ', '_', $v['name']);
        $config['smsvideo']['vars']['current']['rows'] = $rows;
        $print .= template('li/categories_list');

    }

    return $print;
}

function get_category_id_from_translit($translit)
{
    global $config;
    $data = query('
		select categories_domains.id from '.tabname('smsvideo', 'categories_groups').'
		join '.tabname('smsvideo', 'categories_domains').' on categories_domains.category_id=categories_groups.id
		where name_translit=\''.$translit.'\';
		');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('��������� �� �������');
    }

    return $arr['id'];
}

// ��������� ������������ ����� ID ���������, ������� � ����������
function check_category_translit()
{
    global $config;
    if (isset($_GET['category_id']) == 0) {
        error('�� ������� ID');
    }
    if (isset($_GET['name_translit']) == 0) {
        error('�� ������� ��������');
    }
    $data = query('select count(*) from '.tabname('smsvideo', 'categories_domains').'
		join '.tabname('smsvideo', 'categories_groups').' on categories_groups.id=categories_domains.category_id
		where categories_domains.id=\''.$_GET['category_id'].'\' and categories_groups.name_translit=\''.$_GET['name_translit'].'\';');
    $arr = mysql_fetch_array($data);
    if ($arr[0] == 0) {
        error('��������� �� �������', '��������� '.$_GET['category_id'].' ('.$_GET['name_translit'].') ��� ����� '.$config['domain'].' �� �������', 1, 404);
    }
}

function category_name_translit()
{
    global $config;
    if (empty($_GET['category_id'])) {
        error('�� ������� ID ���������');
    }
    $data = query('
		SELECT categories_groups.name_translit
		FROM '.tabname('smsvideo', 'categories_domains').'
		JOIN  '.tabname('smsvideo', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
		WHERE categories_domains.id=\''.$_GET['category_id'].'\'
		');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['name_translit'])) {
        error('�������� �� ����������', '�� ������ �������� � ���������');
    }

    return $arr['name_translit'];
}

function categories_videos_mobile()
{
    global $config;
    $arr = func_replacement('preparing_categories_videos_mobile');

    return thumbs_from_array($arr['videos']);
}

function preparing_categories_videos_mobile()
{
    return preparing_categories_videos('yes');
}

function preparing_categories_videos($mobile = 'no')
{
    global $config;
    // ������� ������ ���� ��� �����������, �� ������� ������ � exit
    if (empty($_GET['category_id'])) {
        error('�� ������ ���������');
    }
    if (empty($_GET['name_translit'])) {
        error('�� �������� ����������� ���������', '�� ������� �������� ���������');
    }
    if (empty($_GET['p']) == 1) {
        $_GET['p'] = 1;
    }
    if (empty($_GET['sort'])) {
        $_GET['sort'] = 'mr';
    }
    if ($_GET['sort'] == 'mv') {
        $sort = 'views';
    }
    if ($_GET['sort'] == 'mr') {
        $sort = 'add_date';
    }
    if ($_GET['sort'] == 'tr') {
        $sort = 'rating desc, votes';
    }
    if (empty($sort)) {
        error('�������� ����������');
    }
    if ($mobile == 'no') {
        $mobile_only_mp4 = '';
        $per_page = value_from_config_for_domain('smsvideo', 'collection_videos_per_page'); // ���������� �� ��������
    }
    if ($mobile == 'yes') {
        $mobile_only_mp4 = 'ext=\'mp4\' and';
        $per_page = value_from_config_for_domain('smsvideo', 'collection_videos_per_page_mobile'); // ���������� �� ��������
    }
    $from = ($_GET['p'] - 1) * $per_page;
    $auto_categories = value_from_config_for_domain('smsvideo', 'auto_categories');
    if ($auto_categories == '0') {
        $data = query('
			SELECT SQL_CALC_FOUND_ROWS videos.id as id, videos.title_ru, videos.title_ru_translit, videos.description, unix_timestamp(videos.add_date) as date,
			ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub
			FROM '.tabname('smsvideo', 'videos').'
			left join '.tabname('smsvideo', 'videos_views').' on videos.id=videos_views.video_id
			WHERE '.$mobile_only_mp4.' and category_id=\''.$_GET['category_id'].'\'
			order by '.$sort.' desc
			limit '.$from.','.$per_page
        );
    }
    if ($auto_categories == '1') {
        // ������ �� ��������� ����� ����������� � ������ ��������� � ����� ��������� �� ��������� ��� ���������
        $data = query('
			SELECT catgroups_tags.tag_ru, categories_groups.name_translit
			FROM '.tabname('smsvideo', 'categories_domains').'
			INNER JOIN  '.tabname('smsvideo', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
			INNER JOIN  '.tabname('smsvideo', 'catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
			WHERE categories_domains.id=\''.$_GET['category_id'].'\'
			');
        $arr = readdata($data, 'nokey');
        // �������� �������� ��������� �� ���������
        $content_array['category_name_translit'] = $arr[0]['name_translit'];
        if ($_GET['name_translit'] != $content_array['category_name_translit']) {
            error('�������� �� ����������', '������� ������ �������� ���������');
        }
        // ��������� ������ ������ �����
        $cat_tags = [];
        foreach ($arr as $v) {
            $cat_tags[] = $v['tag_ru'];
        }
        if (empty($cat_tags)) {
            error('��������� ����������', '��� ��������� ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
        }
        // ������ �� ��������� ���� ���������� ����� �����, ��������������� �������
        $data = query('
			SELECT SQL_CALC_FOUND_ROWS tags.video_id as id,tags.tag_en,tags_translation.tag_ru, videos.title_ru,
			videos.title_ru_translit, videos.description,
			unix_timestamp(videos.add_date) as date,
			ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub
			FROM '.tabname('smsvideo', 'tags').'
			inner join '.tabname('smsvideo', 'tags_translation').' on tags.tag_en=tags_translation.tag_en
			inner join '.tabname('smsvideo', 'videos').' on videos.id=tags.video_id
			left join '.tabname('smsvideo', 'videos_views').' on tags.video_id=videos_views.video_id
			WHERE '.$mobile_only_mp4.' tag_ru in ('.array_to_str($cat_tags).')
			group by id
			order by '.$sort.' desc
			limit '.$from.','.$per_page
        );
    }
    $arr = readdata($data, 'nokey');
    $content_array['videos'] = $arr;
    $content_array['rows'] = rows_without_limit();

    return $content_array;
}

function get_video_id_from_translit($translit)
{
    global $config;
    $data = query('select id from '.tabname('smsvideo', 'videos').' where title_ru_translit=\''.$translit.'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('����� �� �������');
    }

    return $arr['id'];
}

// ���������, ��������� �� ������ ID ����� � ������
function check_video_id()
{
    global $config;
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID');
    }
    $data = query('select count(*) from '.tabname('smsvideo', 'videos').' where id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_array($data);
    if ($arr[0] == 0) {
        error('����� �� �������', '����� '.$_GET['id'].' ��� ����� '.$config['domain'].' �� �������', 1, 404);
    }
}

// ������� ���������� ������
function views_count()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }
    query('update '.tabname('smsvideo', 'videos_views').' set views=(ifnull(views,0)+1) where video_id=\''.$_GET['id'].'\'');
    if (mysql_affected_rows() == 0) {
        $data = query('select * from '.tabname('smsvideo', 'videos').' where id=\''.$_GET['id'].'\'');
        if (mysql_num_rows($data) != 0) {
            query('insert into '.tabname('smsvideo', 'videos_views').' (video_id,views) values (\''.$_GET['id'].'\',\'1\')');
        }
    }
}

// ����� ������ �� �������� ����� (������)
function video_save_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return media_for_video($_GET['id'], 'video_save_link');
}

function media_for_video($id, $action)
{
    if (isset($id) == 0) {
        error('�� ����� ID');
    }
    $data = query('select hub,id_hub,title_ru_translit from '.tabname('smsvideo', 'videos').' where id=\''.$id.'\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr)) {
        error('����� �� ����������', '����� '.$id.' �� ����������', 1, 404);
    }
    $id_hub = $arr['id_hub'];
    $hub = $arr['hub'];
    if ($action == 'video_save_redirect_link') {
        return 'http://save.awmengine.net/?hub='.$arr['hub'].'&id_hub='.$arr['id_hub'].'&videokey='.$_GET['videokey'];
    }
    if ($action == 'video_image_link') {
        return '/types/videohub/images/'.$id.'.jpg';
    }
    if ($action == 'video_watch_link') {
        return '/video/'.$id.'-'.$arr['title_ru_translit'];
    }
    if ($action == 'video_save_link') {
        $data = query('select value from '.tabname('smsvideo', 'config').' where property=\'videokey_current\'');
        $arr2 = mysql_fetch_assoc($data);
        if (empty($arr2['value'])) {
            error('����������� ��� ��� ������');
        }
        temp_var('id', $id);
        temp_var('videokey', $arr2['value']);
        temp_var('title_ru_translit', $arr['title_ru_translit']);

        return template('li/video_save_link');
        // return '/save/'.$arr2['value'].'/'.$id.'-'.$arr['title_ru_translit'];
    }
    if ($action == 'video_load_link') {
        return '/load/'.$id.'-'.$arr['title_ru_translit'];
    }
    if ($hub == 'pornhub.com') {
        return media_from_pornhub($id_hub, $action);
    }
    if ($hub == 'hardsextube.com') {
        return media_from_hardsextube($id_hub, $action);
    }
    if ($hub == 'xvideos.com') {
        return media_from_xvideos($id_hub, $action);
    }
    if ($hub == 'paradisehill.tv') {
        return media_from_paradisehill($id_hub, $action);
    }
    error('���������� �������� �����, ��� '.$hub.' �� ��������������.');
}

// ����� ������ �� ����������� � �����
function video_image_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return media_for_video($_GET['id'], 'video_image_link');
}

// ���������� ���������� �����
function video_views()
{
    $data = query('select views from '.tabname('smsvideo', 'videos_views').' where video_id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_assoc($data);

    return $arr['views'];
}

function same_videos_mobile()
{
    return same_videos('yes');
}

function same_videos($mobile = 'no')
{
    global $config;
    if (empty($_GET['p']) == 1) {
        $_GET['p'] = 1;
    }
    if (empty($_GET['sort'])) {
        $_GET['sort'] = 'ms';
    }
    if ($_GET['sort'] == 'mv') {
        $sort = 'views desc';
    }
    if ($_GET['sort'] == 'mr') {
        $sort = 'add_date desc';
    }
    if ($_GET['sort'] == 'ms') {
        $sort = 'same_tags_q desc, rating_sko desc';
    }
    if (empty($sort)) {
        error('�������� ����������');
    }
    if ($mobile == 'no') {
        $mobile_only_mp4 = '';
        $per_page = value_from_config_for_domain('smsvideo', 'number_of_same_videos');
    }
    if ($mobile == 'yes') {
        $mobile_only_mp4 = 'ext=\'mp4\' and';
        $per_page = value_from_config_for_domain('smsvideo', 'number_of_same_videos_mobile');
    }
    $from = ($_GET['p'] - 1) * $per_page;
    // ������ �� ��������� ����� �������� �����
    $data = query('
SELECT tags_translation.tag_ru
FROM '.tabname('smsvideo', 'tags').'
INNER JOIN '.tabname('smsvideo', 'tags_translation').' ON tags.tag_en = tags_translation.tag_en
WHERE tags.video_id = \''.$_GET['id'].'\'
				');
    $arr = readdata($data, 'nokey');
    // ��������� ������ �����
    $video_tags = [];
    foreach ($arr as $v) {
        $video_tags[] = $v['tag_ru'];
    }
    $data = query('
SELECT tags.video_id as id,tags.tag_en,tags_translation.tag_ru, videos.title_ru, videos.title_ru_translit, videos.description,
unix_timestamp(videos.add_date) as date, ifnull(videos_views.views,0) as views, count(tags.video_id) as same_tags_q,
ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub, ((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko
FROM '.tabname('smsvideo', 'tags').'
inner join '.tabname('smsvideo', 'tags_translation').' on tags.tag_en=tags_translation.tag_en
inner join '.tabname('smsvideo', 'videos').' on videos.id=tags.video_id
left join '.tabname('smsvideo', 'videos_views').' on tags.video_id=videos_views.video_id
WHERE '.$mobile_only_mp4.' tag_ru in ('.array_to_str($video_tags).') and tags.video_id!='.$_GET['id'].'
group by id
order by '.$sort.'
limit '.$per_page.'
				');
    $arr = readdata($data, 'nokey');
    $list = thumbs_from_array($arr);

    return $list;
}

// �������� �� awmengine ��� ��������� �� ������ ������
function redirect_to_awmengine()
{
    global $config;
    if (empty($_GET['videokey']) == 1) {
        error('�� �������� �������� ������');
    }
    check_videokey($_GET['videokey']);
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID');
    }
    $data = query('select count(*) as q,id,saved,ext,subdomain from '.tabname('smsvideo', 'videos').'
		where id=\''.$_GET['id'].'\'
		group by id');
    $arr = mysql_fetch_assoc($data);
    if ($arr['q'] == 0) {
        error('����� �� �������', '����� '.$_GET['id'].' ��� ����� '.$config['domain'].' �� �������', 1, 404);
    }
    $query = '';
    $parse_url = parse_url($_SERVER['REQUEST_URI']);
    if ($arr['saved'] != 'yes') {
        if (empty($parse_url['query']) == 0) {
            $query = '&'.$parse_url['query'];
        }
        header('Location: '.media_for_video($_GET['id'], 'video_save_redirect_link').$query);
    }
    if ($arr['saved'] == 'yes') {
        if (empty($parse_url['query']) == 0) {
            $query = '?'.$parse_url['query'];
        }
        header('Location: http://'.$arr['subdomain'].'.save.epsilon.awmengine.net/'.$arr['id'].'.'.$arr['ext'].$query);
    }
}

// ������� ������ ���� ������ �� ������, � ����������� �� ���� ������ �� ������� videokey
function check_videokey($videokey)
{
    if (empty($videokey)) {
        error('�� ������� ���');
    }
    $data = query('select property,value from '.tabname('smsvideo', 'config').' where property in (\'videokey_current\',\'videokey_last\')');
    $arr = readdata($data, 'property');
    if (empty($arr)) {
        error('����������� ����������� ����');
    }
    if ($videokey != $arr['videokey_current']['value'] and $videokey != $arr['videokey_last']['value']) {
        error('�������� ����������� ���', '', 1, 1, 5);
    }
}
