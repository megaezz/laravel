<?php

function smsvideo_init()
{
    global $config;
    include $config['path']['server'].'types/smsvideo/inc/sms.php';
    include $config['path']['server'].'types/smsvideo/inc/grabber.php';
    $config['smsvideo']['path']['images'] = $config['path']['server'].'types/smsvideo/images/';
    $config['smsvideo']['path']['images_hub'] = $config['path']['server'].'types/smsvideo/images_hub/';
    // check_db(tabname('smsvideo'),smsvideo_db());
}

function feedback_proc()
{
    global $db,$config;
    if (empty($_POST['text']) == 1) {
        error('Не передан текст');
    }
    readdb(tabname('smsvideo', 'sms_config'), 'property');
    $email = $db[tabname('smsvideo', 'sms_config')]['property']['feedback_email']['value'];
    if (empty($email) == 1) {
        error('Не задан email');
    }
    mail($email, $config['domain'], htmlspecialchars($_POST['text']));

    return 'Сообщение отправлено';
}

function get_password()
{
    return mt_rand(1000, 5000);
}

function add_user($phone, $pass)
{
    if (isset($phone,$pass) == 0) {
        error('Не задан логин или пароль');
    }
    query('insert into '.tabname('smsvideo', 'users').' (phone,level,pass) values (\''.$phone.'\',\'user\',\''.$pass.'\')');
}

function check_pass()
{
    session_start();
    if (empty($_SESSION['status']) == 1) {
        if (empty($_POST['pass']) == 1) {
            error('Не передан пароль');
        } else {
            $data = query('select count(*) from '.tabname('smsvideo', 'users').' where pass=\''.$_POST['pass'].'\'');
            $arr = mysql_fetch_array($data);
            $q = $arr[0];
            if ($q == 0) {
                alert('Неверный пароль');
            }
            $_SESSION['status'] = 'ok';
        }
    }
}

function thumb_gallery()
{
    global $config;
    $images = list_directory($config['smsvideo']['path']['images']);
    shuffle($images);
    $list = '';
    for ($i = 0; $i <= 41; $i++) {
        $list .= '
		<li>
		<a href="/?page=sms"><img src="/types/smsvideo/images/'.$images[$i].'" width="320" height="240" alt="*" /></a><br/>
		<a href="/?page=sms">Просмотреть</a> | <a href="/?page=sms">Скачать</a>
		</li>';
    }

    return $list;
}

// вывод списка роликов
function video_collection()
{
    global $config;
    if (empty($_GET['p']) == 1) {
        $_GET['p'] = 1;
    }
    $data = query('select value from '.tabname('smsvideo', 'sms_config').' where property=\'collection_videos_per_page\'');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['value'])) {
        error('Не указано количество видео на странице');
    }
    $per_page = $arr['value']; // заголовков на странице
    $data = query('select count(*) from '.tabname('smsvideo', 'videos').'');
    $arr = mysql_fetch_array($data);
    $rows = $arr[0]; // всего записей
    $pages = ceil($rows / $per_page); // страниц
    $from = ($_GET['p'] - 1) * $per_page;
    $data = query('select id,title_ru,add_date,ifnull(views,0) as views from '.tabname('smsvideo', 'videos').' order by views desc limit '.$from.','.$per_page);
    $arr = readdata($data, 'nokey');
    $content = '<div id="pages"><ul>'.page_numbers($rows, $per_page, $_GET['p'], '/?page=video&amp;p=').'</ul></div>';
    $list = '';
    foreach ($arr as $k => $v) {
        $list .= '<li><a href="/view/id'.$v['id'].'"><img src="/types/smsvideo/images_hub/'.$v['id'].'.jpg" alt="'.$v['title_ru'].'" /></a>
		<br/><a href="/view/id'.$v['id'].'">'.$v['title_ru'].'</a><br/>Просмотров: '.$v['views'].'</li>'."\r\n";
    }
    $content .= '<div id="new_videos"><ul>'.$list.'</ul></div>';

    return $content;
}

// вывод плеера
function video_player()
{
    global $config;
    if (isset($_GET['id']) == 0) {
        error('Не задан ID');
    }
    $data = query('select count(*) from '.tabname('smsvideo', 'videos').' where id=\''.$_GET['id'].'\'');
    $arr = mysql_fetch_array($data);
    if ($arr[0] == 0) {
        error('Видео не найдено', 'Видео '.$_GET['id'].' для сайта '.$config['domain'].' не найдено', 1, 404);
    }

    return media_for_video($_GET['id'], 'player');
}

function smsvideo_db()
{
    $tables['sms_config'] = 'create table '.tabname('smsvideo', 'sms_config').' (
property varchar(255) not null,
value varchar(255),
description varchar(255),
primary key(property)
)';
    $tables['users'] = 'create table '.tabname('smsvideo', 'users').' (
id int(6) unsigned not null auto_increment,
phone varchar(255) not null,
pass varchar(255) not null,
level varchar(255) not null,
auth_date timestamp default current_timestamp not null,
primary key(id)
)';
    $tables['headers'] = 'create table '.tabname('smsvideo', 'headers').' (
domain varchar(255) not null,
page varchar(255) not null,
title varchar(1000),
description varchar(5000),
keywords varchar(5000),
h1 varchar(1000),
text varchar(5000)
)';
    $tables['videos'] = 'create table '.tabname('smsvideo', 'videos').' (
id int(6) unsigned zerofill not null auto_increment,
domain varchar(255),
hub varchar(255) not null,
id_hub varchar(255) not null,
link varchar(255),
add_date timestamp default current_timestamp not null,
size int(10),
title_en varchar(255) not null,
title_ru varchar(255),
translater varchar(255),
views int(10),
primary key(id)
)';

    return $tables;
}
