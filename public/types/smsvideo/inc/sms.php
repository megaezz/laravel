<?php

function short_number()
{
    global $db;
    readdb(tabname('smsvideo', 'sms_config'), 'property');
    if (empty($db[tabname('smsvideo', 'sms_config')]['property']['short_number']['value']) == 1) {
        error('�������� ����� �� �����');
    }

    return $db[tabname('smsvideo', 'sms_config')]['property']['short_number']['value'];
}

function text2()
{
    global $db;
    readdb(tabname('smsvideo', 'sms_config'), 'property');
    if (empty($db[tabname('smsvideo', 'sms_config')]['property']['text2']['value']) == 1) {
        error('����� 2� ��� �� �����');
    }

    return $db[tabname('smsvideo', 'sms_config')]['property']['text2']['value'];
}

function answer1()
{
    global $db;
    readdb(tabname('smsvideo', 'sms_config'), 'property');
    if (empty($db[tabname('smsvideo', 'sms_config')]['property']['answer1']['value']) == 1) {
        error('����� ��� 1� ��� �� �����');
    }

    return $db[tabname('smsvideo', 'sms_config')]['property']['answer1']['value'];
}

function sms_proc_xbil()
{
    global $db,$config;
    header('Content-type: text/xml; charset=utf-8'); // ����� ���8 =(
    $data = query('select property,value from '.tabname('smsvideo', 'sms_config').' where domain=\''.$config['domain'].'\'');
    $sms = readdata($data, 'property');
    if (isset($_GET['from'],$_GET['msg'],$_GET['short_number']) == 0) {
        error('�� ������ ����������� ���������');
    }
    $msg = iconv('UTF-8', 'CP1251', $_GET['msg']);
    $texts1 = explode(', ', $sms['xbil_texts1']['value']);
    $text1_ok = 0;
    foreach ($texts1 as $text1) {
        if ($msg == $text1) {
            $text1_ok = 1;
        }
    }
    if ($text1_ok == 1) {
        $answer = '��� ���� 18 ���? ��������� "'.$msg.' ��" �� "'.$_GET['short_number'].'" � �������� VIP ������';
    }
    $texts2 = explode(', ', $sms['xbil_texts2']['value']);
    $text2_ok = 0;
    foreach ($texts2 as $text2) {
        if ($msg == $text2) {
            $text2_ok = 1;
        }
    }
    if ($text2_ok == 1) {
        $pass = get_password();
        $answer = '��� ������: '.$pass;
        add_user($_GET['from'], $pass);
    }
    if (empty($answer) == 1) {
        $answer = '�������� ������';
    }
    $content = 'ok'."\n".$answer;
    $content = iconv('CP1251', 'UTF-8', $content);

    return $content;
}

function sms_proc_smsbil()
{
    global $db,$config;
    header('Content-type: text/xml; charset=utf-8'); // ����� ���8 =(
    $data = query('select property,value from '.tabname('smsvideo', 'sms_config').' where domain=\''.$config['domain'].'\'');
    $sms = readdata($data, 'property');
    if (isset($_GET['from'],$_GET['msg'],$_GET['short_number']) == 0) {
        error('�� ������ ����������� ���������');
    }
    $msg = iconv('UTF-8', 'CP1251', $_GET['msg']);
    $texts1 = explode(', ', $sms['smsbil_texts1']['value']);
    $text1_ok = 0;
    foreach ($texts1 as $text1) {
        if ($msg == $text1) {
            $text1_ok = 1;
        }
    }
    if ($text1_ok == 1) {
        $answer = '��� ���� 18 ���? ��������� "'.$msg.' ��" �� "'.$_GET['short_number'].'" � �������� VIP ������';
    }
    $texts2 = explode(', ', $sms['smsbil_texts2']['value']);
    $text2_ok = 0;
    foreach ($texts2 as $text2) {
        if ($msg == $text2) {
            $text2_ok = 1;
        }
    }
    if ($text2_ok == 1) {
        $pass = get_password();
        $answer = '��� ������: '.$pass;
        add_user($_GET['from'], $pass);
    }
    if (empty($answer) == 1) {
        $answer = '�������� ������';
    }
    $content = 'ok'."\n".$answer;
    $content = iconv('CP1251', 'UTF-8', $content);

    return $content;
}

function sms_proc_smsbil_old()
{
    global $db,$config;
    header('Content-type: text/xml; charset=utf-8'); // ����� ���8 =(
    $data = query('select property,value from '.tabname('smsvideo', 'sms_config').' where domain=\''.$config['domain'].'\'');
    $sms = readdata($data, 'property');
    $texts2 = explode(', ', $sms['smsbil_text2']['value']);
    if (isset($_GET['from'],$_GET['sms_id'],$_GET['msg'],$_GET['sign'],$sms['smsbil_secret_key']['value']) == 0) {
        error('�� ������ ����������� ���������');
    }
    $md5 = md5($_GET['sms_id'].$sms['smsbil_secret_key']['value']);
    if ($md5 != $_GET['sign']) {
        error('������������ ������');
    }
    $msg = iconv('UTF-8', 'CP1251', $_GET['msg']);
    if ($msg == $sms['smsbil_text1']['value']) {
        $answer = $sms['smsbil_answer1']['value'];
    }
    $text2_ok = 0;
    foreach ($texts2 as $text2) {
        if ($msg == $text2) {
            $text2_ok = 1;
        }
    }
    if ($text2_ok == 1) {
        $pass = get_password();
        $answer = '��� ������: '.$pass;
        add_user($_GET['from'], $pass);
    }
    if (empty($answer) == 1) {
        $answer = '�������� ������';
    }
    $content = 'ok'."\n".$answer;
    $content = iconv('CP1251', 'UTF-8', $content);

    return $content;
}

function sms_proc_a1()
{
    global $db;
    readdb(tabname('smsvideo', 'sms_config'), 'property');
    $sms = $db[tabname('smsvideo', 'sms_config')]['property'];
    if (isset($_GET['date'],$_GET['msg_trans'],$_GET['operator_id'],$_GET['user_id'],$_GET['smsid'],$_GET['cost_rur'],$_GET['ran'],$_GET['test'],$_GET['num'],$_GET['country_id'],$sms['secret_key_a1']['value']) == 0) {
        error('�� ������ ����������� ���������');
    }
    $md5 = md5($_GET['date'].$_GET['msg_trans'].$_GET['operator_id'].$_GET['user_id'].$_GET['smsid'].$_GET['cost_rur'].$_GET['ran'].$_GET['test'].$_GET['num'].$_GET['country_id'].$sms['secret_key_a1']['value']);
    if ($md5 != $_GET['sign']) {
        error('������������ ������');
    }
    if ($_GET['msg_trans'] == $sms['text1']['value']) {
        $answer = $sms['answer1']['value'];
    }
    if ($_GET['msg_trans'] == $sms['text2']['value']) {
        $pass = get_password();
        $answer = 'Vash parol\': '.$pass;
        add_user($_GET['user_id'], $pass);
    }
    if (empty($answer) == 1) {
        $answer = 'Nevernyj zapros';
    }
    $content = 'smsid: '.$_GET['smsid']."\r\n".'status: reply'."\r\n\r\n".$answer;

    return $content;
}
