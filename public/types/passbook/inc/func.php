<?php

function edit_servers()
{
    temp_var('options_types', get_select_option_list(['array' => $types, 'value' => 'type', 'text' => 'type', 'selected_value' => $v['type']]));

    return template('temp_var/form_edit_servers');
}

function servers_table()
{
    $arr = readdata(query('select servers.id,servers.host,servers.hoster_id,servers.status,
		concat(servers.cost,\' \',servers.cost_curr) as cost,servers.type,servers.ssh_login,servers.ssh_pswd,
		servers.ftp_login,servers.ftp_pswd,servers.mysql_login,servers.mysql_pswd,
		domains.domain,
		services.id as service_id,services.link as service_link,servers.comment
		from '.tabname('passbook', 'servers').'
		join '.tabname('passbook', 'domains').' on domains.id=servers.host
		join '.tabname('passbook', 'services').' on services.id=servers.hoster_id
		order by field(servers.status,\'active\',\'deleted\'),servers.type,domains.domain;'), 'nokey');
    $arr_ips = readdata(query('select * from '.tabname('passbook', 'server_ips').';'), 'nokey');
    $arr_domains = readdata(query('select s1.id as server_id,s2.id as proxy_id,si1.ip as server_ip,
		si2.ip as proxy_ip,domain
		from '.tabname('passbook', 'domains').'
		left join '.tabname('passbook', 'server_ips').' si1 on si1.id=domains.server_ip
		left join '.tabname('passbook', 'server_ips').' si2 on si2.id=domains.proxy_ip
		left join '.tabname('passbook', 'servers').' s1 on s1.id=si1.server_id
		left join '.tabname('passbook', 'servers').' s2 on s2.id=si2.server_id
		;'), 'nokey');
    $types[] = ['type' => 'proxy'];
    $types[] = ['type' => 'dynamic'];
    $types[] = ['type' => 'static'];
    $list = '';
    $i = 0;
    foreach ($arr as $v) {
        $ips = [];
        foreach ($arr_ips as $v2) {
            if ($v2['server_id'] == $v['id']) {
                $ips[] = $v2['ip'];
            }
        }
        $domains = [];
        $proxed_domains = [];
        foreach ($arr_domains as $v2) {
            if ($v2['server_id'] == $v['id']) {
                $domains[] = $v2['domain'];
            }
            if ($v2['proxy_id'] == $v['id']) {
                // ���� � ������� ����� 1 ip �� ���������� ������ ������ � ip ��� �����������
                $add_ip = '';
                if (count($ips) > 1) {
                    $add_ip = ' '.$v2['proxy_ip'];
                }
                $proxed_domains[] = $v2['domain'].$add_ip;
            }
        }
        sort($domains);
        sort($proxed_domains);
        $tr_class = '';
        if ($v['type'] != 'proxy') {
            $tr_class = ' class="info"';
        }
        if ($v['status'] == 'deleted') {
            $tr_class = ' class="error"';
        }
        $i++;
        $list .= '
		<tr'.$tr_class.'>
		<td>'.$i.'.</td>
		<td>'.$v['domain'].'</td>
		<td><ol>'.li_from_array($domains).'</ol></td>
		<td><ol>'.li_from_array($proxed_domains).'</ol></td>
		<td><ol>'.li_from_array($ips).'</ol></td>
		<td>
		ID: '.$v['id'].'<br/>
		<a href="/?page=show_service_info&amp;id='.$v['service_id'].'" class="service">'.$v['service_link'].'</a><br/>
		'.(empty($v['ssh_pswd']) ? '' : 'ssh: '.$v['ssh_login'].' / '.$v['ssh_pswd'].'<br/>').'
		'.(empty($v['ftp_pswd']) ? '' : 'ftp: '.$v['ftp_login'].' / '.$v['ftp_pswd'].'<br/>').'
		'.(empty($v['mysql_pswd']) ? '' : 'mysql: '.$v['mysql_login'].' / '.$v['mysql_pswd'].'<br/>').'
		'.nl2br($v['comment']).'
		</td>
		<td>'.$v['type'].'</td>
		<td><a href="/?page=edit_server" class="btn">Edit</a></td>
		</tr>';
    }
    temp_var('servers_list', $list);

    return template('temp_var/servers_table');
}

function passbook_init()
{
    global $config;
}

function day_charge($date, $rate, $amount)
{
    $year_days = 365 + date('L', strtotime($date));
    $day_charge = round($amount * ($rate / 100) / $year_days, 2);

    return $day_charge;
}

function deposits_list()
{
    $data = query('select *,
		datediff(current_date,start_date) as days_lasts
		from '.tabname('passbook', 'deposits').'
		where active=\'1\'
		order by start_date;');
    $arr = readdata($data, 'nokey');
    // $arr2=mysql_fetch_assoc(query('select sum(sum) as sum, round(sum(sum*(rate/100)/period)) as day_charge_sum from '.tabname('passbook','deposits').';'));
    $list = '';
    $i = 0;
    $all['amount'] = 0;
    $all['day_charge'] = 0;
    foreach ($arr as $v) {
        $i++;
        $v['days_to_end'] = $v['period'] - $v['days_lasts'];
        // $v['day_charge']=round($v['sum']*($v['rate']/100)/$v['period']);
        $v['day_charge'] = day_charge(date('Y-m-d'), $v['rate'], $v['sum']);
        $all['amount'] = $all['amount'] + $v['sum'];
        $all['day_charge'] = $all['day_charge'] + $v['day_charge'];
        $list .= '<tr><td>'.$i.'</td><td>'.$v['start_date'].'</td><td>'.$v['end_date'].'</td><td>'.$v['bank'].' ('.$v['office'].')</td><td>'.$v['sum'].'</td><td>'.$v['rate'].'%</td><td>'.$v['period'].'</td><td>'.$v['days_lasts'].'</td><td>'.$v['days_to_end'].'</td><td>'.$v['day_charge'].'</td></tr>';
    }
    $content = '
	<thead><tr><td colspan="10" style="font-size: 16px; font-weight: bold;">������:</td></tr></thead>
	<tr><th>#</th><th>��������</th><th>�������</th><th>����</th><th>�����</th><th>�������</th><th>����</th><th>������</th><th>��������</th><th>� �����</th></tr>
	'.$list.'
	<tr><td colspan="4"></td><td>'.$all['amount'].'</td><td colspan="4"></td><td>'.$all['day_charge'].'</td></tr>
	';

    return $content;
}

function deposits_charges_list()
{
    $var['year'] = date('Y');
    $var['month'] = date('m');
    if (! empty($_GET['y'])) {
        $var['year'] = $_GET['y'];
    }
    if (! empty($_GET['m'])) {
        $var['month'] = $_GET['m'];
    }
    $var['first_day'] = $var['year'].'-'.$var['month'].'-01';
    $first_day_unix = strtotime($var['first_day']);
    $var['last_day'] = $var['year'].'-'.$var['month'].'-'.date('t', $first_day_unix);
    $date = my_date($first_day_unix);
    $month = date('t', $first_day_unix).' '.$date['month'].' '.$date['year'];
    $data = query('select bank,sum,rate,period,start_date,end_date 
		from '.tabname('passbook', 'deposits').' 
		where active=\'1\'
		order by start_date;');
    $arr = readdata($data, 'nokey');
    $list = '';
    $i = 0;
    $all['month_sum'] = 0;
    foreach ($arr as $v) {
        if ($v['start_date'] > $var['first_day']) {
            $v['from_day'] = $v['start_date'];
        } else {
            $v['from_day'] = $var['first_day'];
        }
        if ($v['end_date'] < $var['last_day']) {
            $v['to_day'] = $v['end_date'];
        } else {
            $v['to_day'] = $var['last_day'];
        }
        $v['unix_range'] = strtotime($v['to_day']) - strtotime($v['from_day']);
        if ($v['unix_range'] < 0) {
            continue;
        }
        $i++;
        $v['month_days'] = date('d', $v['unix_range']);
        $v['day_charge'] = day_charge(date('Y-m-d'), $v['rate'], $v['sum']);
        $v['month_sum'] = round($v['month_days'] * $v['day_charge'], 2);
        $all['month_sum'] = $all['month_sum'] + $v['month_sum'];
        $list .= '<tr><td>'.$i.'</td><td>'.$v['bank'].'</td><td>'.$v['month_days'].'</td><td>'.$v['month_sum'].'</td></tr>';
    }
    $content = '<thead><tr><td colspan="4" style="font-size: 16px; font-weight: bold;">�� '.$month.':</td></tr></thead>
	<tr><th>#</th><th>����</th><th>����</th><th>�����</th></tr>'.$list.'
	<tr><td colspan="3"></td><td>'.$all['month_sum'].'</td>';

    return $content;
}

function submit_check_as_deleted()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    query('update '.tabname('passbook', 'todo').' set status=\'deleted\' where id=\''.$_GET['id'].'\';');
    header('Location: /?page=todo_list');
}

function submit_check_as_done()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    query('update '.tabname('passbook', 'todo').' set status=\'done\' where id=\''.$_GET['id'].'\';');
    header('Location: /?page=todo_list');
}

function submit_check_as_active()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    query('update '.tabname('passbook', 'todo').' set status=\'active\',date=current_timestamp where id=\''.$_GET['id'].'\';');
    header('Location: /?page=todo_list');
}

function submit_add_todo()
{
    global $config;
    // print_r($_POST);
    if (empty($_POST['text'])) {
        error('�� ������� �����');
    }
    if (empty($_POST['access'])) {
        error('�� ������� ��� �������');
    }
    query('insert into '.tabname('passbook', 'todo').' set text=\''.htmlspecialchars(trim(addslashes($_POST['text'])), ENT_SUBSTITUTE, $config['charset']).'\', user=\''.login_engine().'\', access=\''.$_POST['access'].'\', date=current_timestamp;');
    header('Location: /?page=todo_list');
}

function submit_edit_note()
{
    global $config;
    if (empty($_POST['text'])) {
        error('�� ������� �����');
    }
    if (empty($_POST['access'])) {
        error('�� ������� ��� �������');
    }
    // pre($_POST);
    query('update '.tabname('passbook', 'todo').' set text=\''.htmlspecialchars(trim(addslashes($_POST['text'])), ENT_SUBSTITUTE, $config['charset']).'\', access=\''.$_POST['access'].'\' where id=\''.$_POST['id'].'\' and user=\''.login_engine().'\';');
    if (mysql_affected_rows() == 0) {
        error('������ �� �������, ���� ������ ��������.');
    }
    header('Location: /?page=todo_list');
}

function edit_note()
{
    global $config;
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $data = query('select id,text,access
		from '.tabname('passbook', 'todo').'
		where id=\''.$_GET['id'].'\' and user=\''.login_engine().'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        alert('������ �� �������, ���� ������ ��������');
    }
    temp_var('id', $arr['id']);
    temp_var('common_selected', '');
    temp_var('private_selected', '');
    temp_var($arr['access'].'_selected', 'selected');
    temp_var('text', $arr['text']);

    return template('form_edit_note');
}

function todo_list()
{
    $data = query('
		(select id,date,text,status,user
		from '.tabname('passbook', 'todo').'
		where status!=\'deleted\' and
		access=\'common\')
	union
	(select id,date,text,status,user
		from '.tabname('passbook', 'todo').'
		where status!=\'deleted\' and
		access=\'private\' and user=\''.login_engine().'\'
		)
		order by
		case status
		when \'active\' then 0
		when \'done\' then 1
		end,
		date desc
	;');
    $arr = readdata($data, 'nokey');
    $list = '';
    $q = 0;
    foreach ($arr as $v) {
        $q++;
        if ($v['status'] == 'done') {
            $list .= '<tr class="info">
			<td style="width: 15px;">'.$q.'.</td>
			<td>'.nl2br($v['text']).'</td>
			<td style="text-align: center;">'.$v['user'].'</td>
			<td style="width: 65px; text-align: center;"><a href="/?page=submit_check_as_active&amp;id='.$v['id'].'">active</a>
			<a href="/?page=submit_check_as_deleted&amp;id='.$v['id'].'">del</a></td>
			</tr>';
        }
        if ($v['status'] == 'active') {
            $list .= '<tr>
			<td style="width: 15px;">'.$q.'.</td>
			<td>'.nl2br($v['text']).'</td>
			<td style="text-align: center;">'.$v['user'].'</td>
			<td style="width: 65px; text-align: center;"><a href="/?page=edit_note&amp;id='.$v['id'].'">edit</a> <a href="/?page=submit_check_as_done&amp;id='.$v['id'].'">done</a></td>
			</tr>';
        }
    }

    return $list;
}

function domains_list()
{
    $deleted[] = ['status' => '1', 'text' => 'deleted'];
    $deleted[] = ['status' => '0', 'text' => 'active'];
    $dns = readdata(query('select id,concat(ns1,\', \',ns2) as text
		from '.tabname('passbook', 'dns').'
		order by text'), 'nokey');
    $servers = readdata(query('select server_ips.id,concat(domains.domain,\' \',server_ips.ip) as domain
		from '.tabname('passbook', 'server_ips').'
		join '.tabname('passbook', 'servers').' on servers.id=server_ips.server_id
		left join '.tabname('passbook', 'domains').' on domains.id=servers.host
		where servers.type is null and servers.status=\'active\'
		order by servers.type,domain;'), 'nokey');
    // pre($servers);
    $proxies = readdata(query('select server_ips.id,concat(domains.domain,\' \',server_ips.ip) as domain
		from '.tabname('passbook', 'server_ips').'
		join '.tabname('passbook', 'servers').' on servers.id=server_ips.server_id
		left join '.tabname('passbook', 'domains').' on domains.id=servers.host
		where servers.type=\'proxy\' and servers.status=\'active\'
		order by servers.type,domain;'), 'nokey');
    // $ips_servers=readdata(pre('select servers.ip,domains.domain
    // 	from '.tabname('passbook','servers').'
    // 	left join '.tabname('passbook','domains').' on domains.id=servers.host;'),'ip');
    $ips_servers = readdata(query('select server_ips.ip,concat(domains.domain,\'&nbsp;\',server_ips.ip) as domain
		from '.tabname('passbook', 'server_ips').'
		join '.tabname('passbook', 'servers').' on servers.id=server_ips.server_id
		left join '.tabname('passbook', 'domains').' on domains.id=servers.host;'), 'ip');
    // pre($ips_servers);
    $ips_servers['']['domain'] = 'empty';
    $where['only_active'] = 'where domains.deleted=\'0\'';
    $where['only_deleted'] = 'where domains.deleted=\'1\'';
    $where['only_sub'] = 'where domains.sub=\'1\'';
    $where['not_sub'] = 'where domains.sub=\'0\'';
    $where['active_and_not_sub'] = 'where domains.deleted=\'0\' and domains.sub=\'0\'';
    $where['different'] = 'where domains.server_ip=\'49\' and domains.status=\'on\' and deleted=\'0\'';
    $where['without_server_or_proxy'] = 'where (domains.server_ip is null or domains.proxy_ip is null) and deleted!=\'1\'';
    $where['all'] = '';
    if (! isset($_GET['display'])) {
        $display = 'only_active';
    } else {
        $display = $_GET['display'];
    }
    if (! isset($where[$display])) {
        error('������� �� �������������');
    }
    $data = query('select domains.id as domain_id,domains.domain,domains.exp_date,domains.dns as dns_id,domains.deleted,
	s1.id as server_id,s1.ip as server_ip,s2.id as proxy_id,s2.ip as proxy_ip,
	services.id as registrar_id,services.link as registrar_link
		from '.tabname('passbook', 'domains').'
		left join '.tabname('passbook', 'server_ips').' as s1 on s1.id=domains.server_ip
		left join '.tabname('passbook', 'server_ips').' as s2 on s2.id=domains.proxy_ip
		left join '.tabname('passbook', 'services').' on services.id=domains.reg_id
		'.$where[$display].'
		order by domains.deleted,domains.sub,domains.domain
		;');
    $arr = readdata($data, 'nokey');
    // pre($arr);
    $list = '';
    $li_stat_sum = 0;
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    if (! isset($action['li_stat'])) {
        $action['li_stat'] = 0;
    }
    if (! isset($action['check_ips'])) {
        $action['check_ips'] = 0;
    }
    if (! isset($action['view'])) {
        $action['view'] = 'full';
    }
    $i = 0;
    foreach ($arr as $k => $v) {
        $css_class = '';
        if ($action['li_stat'] == 1) {
            $li_stat = li_stat($v['domain']);
        }
        if (empty($li_stat['LI_day_vis'])) {
            $li_stat_link = '<a href="http://liveinternet.ru/stat/'.$v['domain'].'" target="_blank">Check</a>';
        } else {
            $li_stat_link = '<a href="http://liveinternet.ru/stat/'.$v['domain'].'" target="_blank">'.$li_stat['LI_day_vis'].'</a>';
            $li_stat_sum = $li_stat_sum + $li_stat['LI_day_vis'];
        }
        if (empty($v['proxy_ip'])) {
            $main_ip = $v['server_ip'];
        } else {
            $main_ip = $v['proxy_ip'];
        }
        $real_host = '';
        $real_hosts_list = '';
        $real_ips = [];
        $server_host = '';
        $ns_str = '';
        if ($action['check_ips'] == 1) {
            // dns
            // pre($v);
            $ns = dns_get_record($v['domain'], DNS_ANY);
            // pre($ns);
            if (empty($ns)) {
                $real_ip = '�� ���������';
                $real_ips[] = '�� ����������';
                $ns_str = '�� ����������';
            }
            foreach ($ns as $v1) {
                if ($v1['type'] == 'NS') {
                    if ($ns_str != '') {
                        $ns_str .= '<br/>';
                    }
                    $ns_str .= $v1['target'];
                }
                if ($v1['type'] == 'A') {
                    $real_ip = $v1['ip'];
                    $real_ips[] = $v1['ip'];
                }
            }
            //
            // $real_ip=gethostbyname($v['domain']);
            $server_ip = @file_get_contents('http://'.$v['domain'].'/?page=ip');
            $server_ip_length = strlen($server_ip);
            foreach ($real_ips as $ip) {
                if (isset($ips_servers[$ip]['domain'])) {
                    $real_hosts_list .= '<li>'.$ips_servers[$ip]['domain'].'</li>';
                } else {
                    $real_hosts_list .= '<li>'.$ip.'</li>';
                }
            }
            if (isset($ips_servers[$real_ip]['domain'])) {
                $real_host = $ips_servers[$real_ip]['domain'];
            } else {
                $real_host = $real_ip;
            }
            if ($server_ip_length > 15 or $server_ip_length == 0) {
                $server_ip = 'can\'t detect';
                $server_host = 'unknown';
            } else {
                if (isset($ips_servers[$server_ip]['domain'])) {
                    $server_host = $ips_servers[$server_ip]['domain'];
                } else {
                    $server_host = $server_ip;
                }
            }
            if ($real_ip == $main_ip and $server_ip == $v['server_ip']) {
                $css_class = 'success';
            } else {
                $css_class = 'error';
            }
        }
        if ($v['deleted'] == '1') {
            $css_class = 'warning';
        }
        $i++;
        if ($action['view'] == 'full') {
            $list .= '<tr class="'.$css_class.'">
			<td class="num">'.$i.'.</td>
			<td><a class="domain" href="http://'.$v['domain'].'" target="_blank">'.$v['domain'].'</a> (<a class="domain" href="http://www.'.$v['domain'].'" target="_blank">www</a>)</td>
			<td>'.$v['exp_date'].'</td>
			<td>'.$server_host.'</td>
			<td>'.$real_host.'<ol>'.$real_hosts_list.'</ol></td>
			<td>'.$ns_str.'</td>
			<td>'.$li_stat_link.'</td>
			<td><a href="/?page=show_service_info&amp;id='.$v['registrar_id'].'">Link</a></td>
			<td colspan="6">
			<form method="post" action="/?page=submit_domains_servers" class="margin">
			<input type="hidden" name="domain_id" value="'.$v['domain_id'].'" />
			<table class="table-bordered middle"><tr>
			<td><select class="form margin" name="server_id">'.get_select_option_list(['array' => $servers, 'value' => 'id', 'text' => 'domain', 'selected_value' => $v['server_id']]).'</select></td>
			<td><select class="form margin" name="proxy_id">'.get_select_option_list(['array' => $proxies, 'value' => 'id', 'text' => 'domain', 'selected_value' => $v['proxy_id']]).'</select></td>
			<td><select class="form margin" name="dns_id">'.get_select_option_list(['array' => $dns, 'value' => 'id', 'text' => 'text', 'selected_value' => $v['dns_id']]).'</select></td>
			<td><select class="form deleted margin" name="deleted">'.get_select_option_list(['array' => $deleted, 'value' => 'status', 'text' => 'text', 'selected_value' => $v['deleted']]).'</select></td>
			<td><a href="/?page=edit_domain&amp;id='.$v['domain_id'].'" class="btn">Edit</a></td>
			<td><input class="btn btn-primary" type="submit" name="submit" value="OK"></td>
			</tr></table>
			</form>
			</td>
			</tr>';
        }
        if ($action['view'] == 'text') {
            $list .= '<tr>
			<td>'.$i.'</td>
			<td>'.$v['domain'].'</td>
			<td>'.$ips_servers[$v['server_ip']]['domain'].'</td>
			<td>'.$ips_servers[$v['proxy_ip']]['domain'].'</td>
			</tr>';
        }
    }
    temp_var('domains_table', $list);
    temp_var('li_stat_sum', equal_text($li_stat_sum, 0, 'no&nbsp;data', $li_stat_sum));
    temp_var('action_check_ips', $action['check_ips']);
    temp_var('display', $display);
    temp_var('action_li_stat', $action['li_stat']);
    temp_var('action_view', $action['view']);
    temp_var('action_view_other', equal_text($action['view'], 'full', 'text', 'full'));
    temp_var('disabled_action_check_ips', equal_text($action['check_ips'], 1, ' disabled', ''));
    temp_var('disabled_action_li_stat', equal_text($action['li_stat'], 1, ' disabled', ''));
    temp_var('disabled_display_all', equal_text($display, 'all', ' disabled', ''));
    temp_var('disabled_display_only_active', equal_text($display, 'only_active', ' disabled', ''));
    temp_var('disabled_display_only_deleted', equal_text($display, 'only_deleted', ' disabled', ''));
    temp_var('disabled_display_only_sub', equal_text($display, 'only_sub', ' disabled', ''));
    temp_var('disabled_display_not_sub', equal_text($display, 'not_sub', ' disabled', ''));
    temp_var('disabled_display_active_and_not_sub', equal_text($display, 'active_and_not_sub', ' disabled', ''));
    temp_var('disabled_display_different', equal_text($display, 'different', ' disabled', ''));
    if ($action['view'] == 'full') {
        $content = template('temp_var/domains_list');
    }
    if ($action['view'] == 'text') {
        $content = template('temp_var/domains_list_text');
    }

    return $content;
}

function test_ns()
{
    $ns = dns_get_record('fuckhome.net', DNS_ANY);
    pre($ns);
}

function form_add_domain()
{
    $subs[] = ['status' => '0', 'text' => 'main domain'];
    $subs[] = ['status' => '1', 'text' => 'subdomain'];
    $registrars = readdata(query('select id,concat(link,\' \',login) as text
		from '.tabname('passbook', 'services').'
		where type=\'REGISTRAR\'
		order by text'), 'nokey');
    temp_var('exp_date', date('Y-m-d', strtotime(date('Y-m-d').' +1 year')));
    temp_var('options_sub', get_select_option_list(['array' => $subs, 'value' => 'status', 'text' => 'text']));
    temp_var('options_registrars', get_select_option_list(['array' => $registrars, 'value' => 'id', 'text' => 'text']));

    return template('temp_var/form_add_domain');
}

function form_add_server()
{
    $types = [['type' => 'proxy']];
    $hosters = query_arr('select id,concat(link,\' \',login) as text
		from '.tabname('passbook', 'services').'
		where type=\'HOSTER\'
		order by text');
    $hosts = query_arr('select domains.id,domain from '.tabname('passbook', 'domains').' 
		left join '.tabname('passbook', 'servers').' on servers.host=domains.id
		where sub=\'1\' and domains.status=\'on\' and servers.host is null
		order by domain;');
    temp_var('options_types', get_select_option_list(['array' => $types, 'value' => 'type', 'text' => 'type']));
    temp_var('options_hosters', get_select_option_list(['array' => $hosters, 'value' => 'id', 'text' => 'text']));
    temp_var('options_hosts', get_select_option_list(['array' => $hosts, 'value' => 'id', 'text' => 'domain']));

    return template('temp_var/form_add_server');
}

function submit_add_server()
{
    // pre($_POST);
    if (empty($_POST['host_id']) or empty($_POST['type']) or empty($_POST['hoster_id']) or empty($_POST['ips'])) {
        error('�� �������� ����������� ��������');
    }
    if ($_POST['type'] == 'null') {
        $var['type'] = '';
    } else {
        $var['type'] = $_POST['type'];
    }
    $var = null_if_empty($var);
    $ips = explode("\r\n", trim($_POST['ips']));
    query('insert into '.tabname('passbook', 'servers').' set host=\''.$_POST['host_id'].'\',hoster_id=\''.$_POST['hoster_id'].'\',type='.$var['type'].';');
    $server_id = mysql_insert_id();
    foreach ($ips as $ip) {
        query('insert into '.tabname('passbook', 'server_ips').' set server_id=\''.$server_id.'\',ip=\''.trim($ip).'\';');
    }
    header('Location: /?page=servers');
}

function submit_add_domain()
{
    if (empty($_POST['domain']) or ! isset($_POST['sub']) or $_POST['sub'] == 'null') {
        error('�� �������� ����������� ��������');
    }
    if ($_POST['sub'] == '0') {
        if (empty($_POST['exp_date']) or ! isset($_POST['reg_id']) or $_POST['reg_id'] == 'null') {
            error('�� �������� ����������� ��������');
        }
        query('insert into '.tabname('passbook', 'domains').' set domain=\''.$_POST['domain'].'\',sub=\''.$_POST['sub'].'\',exp_date=\''.$_POST['exp_date'].'\',reg_id=\''.$_POST['reg_id'].'\';');
    }
    if ($_POST['sub'] == '1') {
        query('insert into '.tabname('passbook', 'domains').' set domain=\''.$_POST['domain'].'\',sub=\''.$_POST['sub'].'\';');
    }
    header('Location: /?page=domains');
}

function form_edit_domain()
{
    if (empty($_GET['id'])) {
        error('�� ������� ID ������');
    }
    $data = query('select id,domain,exp_date,reg_id,sub from '.tabname('passbook', 'domains').' where id=\''.$_GET['id'].'\';');
    $arr = mysql_fetch_assoc($data);
    temp_var('id', $arr['id']);
    temp_var('domain', $arr['domain']);
    if ($arr['sub'] == '0') {
        $registrars = readdata(query('select id,concat(link,\' \',login) as text
		from '.tabname('passbook', 'services').'
		where type=\'REGISTRAR\'
		order by text'), 'nokey');
        temp_var('exp_date', $arr['exp_date']);
        temp_var('options_registrars', get_select_option_list(['array' => $registrars, 'value' => 'id', 'text' => 'text', 'selected_value' => $arr['reg_id']]));

        return template('temp_var/form_edit_domain');
    }
    if ($arr['sub'] == '1') {
        return template('temp_var/form_edit_subdomain');
    }
}

function submit_edit_subdomain()
{
    if (empty($_POST['id']) or empty($_POST['domain'])) {
        error('�� �������� ����������� ��������');
    }
    query('update '.tabname('passbook', 'domains').' set domain=\''.$_POST['domain'].'\' where id=\''.$_POST['id'].'\';');
    header('Location: /?page=domains');
}

function submit_edit_domain()
{
    if (empty($_POST['id']) or empty($_POST['domain']) or empty($_POST['exp_date']) or empty($_POST['reg_id'])) {
        error('�� �������� ����������� ��������');
    }
    query('update '.tabname('passbook', 'domains').' set domain=\''.$_POST['domain'].'\',exp_date=\''.$_POST['exp_date'].'\',reg_id=\''.$_POST['reg_id'].'\' where id=\''.$_POST['id'].'\';');
    header('Location: /?page=domains');
}

function submit_delete_domain()
{
    if (empty($_POST['id'])) {
        error('�� ������� ID ������');
    }
    query('delete from '.tabname('passbook', 'domains').' where id=\''.$_POST['id'].'\' and deleted=\'1\';');
    if (mysql_affected_rows() == 0) {
        alert('����� �� ������ �� ��. ������� ��������� �������� ��� ��� ���������. �������� �� ������ � ������ ��������.');
    }
    header('Location: /?page=domains');
}

function submit_domains_servers()
{
    if (! isset($_POST['domain_id'],$_POST['proxy_id'],$_POST['server_id'])) {
        error('�� �������� ����������� ��������');
    }
    if ($_POST['proxy_id'] == 'null') {
        $mysql['proxy_id'] = 'null';
    } else {
        $mysql['proxy_id'] = '\''.$_POST['proxy_id'].'\'';
    }
    if ($_POST['server_id'] == 'null') {
        $mysql['server_id'] = 'null';
    } else {
        $mysql['server_id'] = '\''.$_POST['server_id'].'\'';
    }
    if ($_POST['dns_id'] == 'null') {
        $mysql['dns_id'] = 'null';
    } else {
        $mysql['dns_id'] = '\''.$_POST['dns_id'].'\'';
    }
    query('update '.tabname('passbook', 'domains').'
		set server_ip='.$mysql['server_id'].', proxy_ip='.$mysql['proxy_id'].', dns='.$mysql['dns_id'].', deleted=\''.$_POST['deleted'].'\'
		where id=\''.$_POST['domain_id'].'\';');
    if (mysql_affected_rows() > 1) {
        error('��������� > 1 ������');
    }
    // header('Location: /?page=domains');
    redirect(referer_url());
}

function submit_add_service()
{
    if (! empty($_POST['service'])) {
        $service = trim($_POST['service']);
        $arr = explode("\r\n", $service);
        $link = $arr[0];
        $login = $arr[1];
        $pswd = $arr[2];
        if (! empty($arr[3])) {
            $type = $arr[3];
        } else {
            $type = null;
        }
        if (! empty($arr[4])) {
            $comment = trim(str_replace($link."\r\n".$login."\r\n".$pswd."\r\n".$type, '', $_POST['service']));
        } else {
            $comment = null;
        }
    }
    if (empty($_POST['service'])) {
        if (! empty($_POST['link'])) {
            $link = $_POST['link'];
        }
        if (! empty($_POST['login'])) {
            $login = $_POST['login'];
        }
        if (! empty($_POST['pswd'])) {
            $pswd = $_POST['pswd'];
        }
        if (! empty($_POST['type'])) {
            $type = $_POST['type'];
        } else {
            $type = null;
        }
        if (! empty($_POST['comment'])) {
            $comment = $_POST['comment'];
        } else {
            $comment = null;
        }
    }
    if (empty($link) or empty($login) or empty($pswd)) {
        error('�� �������� ����������� ���������');
    }
    query('insert into '.tabname('passbook', 'services').' set link=\''.$link.'\', login=\''.$login.'\', pswd=\''.$pswd.'\', type=\''.$type.'\', comment=\''.$comment.'\' ');
    // alert('���������');
    header('Location: /?page=add_service');
}

function show_service_info()
{
    if (! isset($_GET['id'])) {
        error('�� ������� ID');
    }
    $data = query('select * from '.tabname('passbook', 'services').' where id=\''.$_GET['id'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (! empty($v['type'])) {
        $str_type = 'Type: '.$v['type'].'<br/>';
    } else {
        $str_type = '';
    }
    if (! empty($v['comment'])) {
        $str_comment = 'Comment:<br/>'.nl2br($v['comment']).'<br/>';
    } else {
        $str_comment = '';
    }
    $content = '
	<b>ID: '.$arr['id'].'</b>
	<b>'.$arr['link'].'</b><br/>
	Login: '.$arr['login'].'<br/>
	Pswd: '.$arr['pswd'].'<br/>
	'.$str_type.'
	'.$str_comment;

    return $content;
}

function search_services_result()
{
    if (empty($_POST['query'])) {
        error('�� ������� ��������� ������');
    }
    $password = empty($_POST['password']) ? '' : check_input($_POST['password']);
    $query = trim($_POST['query']);
    $data = query('select * from '.tabname('passbook', 'services').' where concat_ws(\' \',link,login,pswd,type,comment) like \'%'.$query.'%\' order by link');
    // $data=query('select id,link,login,type,
    // 	des_decrypt(pswd,\''.$password.'\') as pswd,
    // 	des_decrypt(comment,\''.$password.'\') as comment
    // 	from '.tabname('passbook','services').' having concat_ws(\' \',link,login,pswd,type,comment) like \'%'.$query.'%\' order by link');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $str_type = '';
        $str_comment = '';
        if (! empty($v['type'])) {
            $str_type = 'Type: '.$v['type'].'<br/>';
        }
        if (! empty($v['comment'])) {
            $str_comment = 'Comment:<br/>'.nl2br($v['comment']).'<br/>';
        }
        $list .= '<li style="margin: 10px;">
		<b>'.$v['link'].'</b><br/>
		ID: '.$v['id'].'<br/>
		Login: '.$v['login'].'<br/>
		Pswd: '.$v['pswd'].'<br/>
		'.$str_type.'
		'.$str_comment.'
		</li>';
    }

    return $list;
}
