<?php

function exchanger_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('Не передан запрос');
    }
    $query = $_GET['query'];
    if (preg_match('/^articles\/([a-z0-9-]+)(\/?)$/', $query, $args)) {
        $config['vars']['page'] = 'articles/'.$args[1];

        return;
    }
}

function exchanger_init()
{
    global $config;
    if (empty($config['vars']['page'])) {
        exchanger_rewrite_query();
    }
}
