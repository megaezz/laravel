<?php

function items_list()
{
    $data = query('select items.id,items.img_ext,items.category_id,items.price,items.size,items.material,
		categories.name as category_name
		from '.tabname('ezhevikina', 'items').'
		join '.tabname('ezhevikina', 'categories').' on categories.id=items.category_id
		');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        temp_var('item_id', $v['id']);
        temp_var('category_name', $v['category_name']);
        temp_var('img_url', '/types/ezhevikina/template/images/items/'.$v['id'].'.'.$v['img_ext']);
        $list .= template('items/item');
    }

    return $list;
}

function admin_add_item() {}

function admin_photos_copy()
{
    global $config;
    $data = query('select * from '.tabname('ezhevikina', 'categories').' where img_dir is not null;');
    $arr_categories = readdata($data, 'img_dir');
    $images_dir = $config['path']['type'].'images/';
    $images_dir_old = $config['path']['type'].'images/old/';
    $dirs = list_directory($images_dir_old);
    // pre($dirs);
    foreach ($dirs as $dir) {
        $cat_dir = $images_dir_old.$dir['name'].'/';
        $cat_id = $arr_categories[$dir['name']]['id'];
        $photos = list_directory($cat_dir);
        // pre($photos);
        foreach ($photos as $photo) {
            $item_photo_path = $cat_dir.$photo['name'];
            $pathinfo = pathinfo($item_photo_path);
            $img_ext = strtolower($pathinfo['extension']);
            if ($img_ext == 'jpeg') {
                $img_ext = 'jpg';
            }
            // query('insert into '.tabname('ezhevikina','items').' set category_id=\''.$cat_id.'\',img_ext=\''.$img_ext.'\'');
            $arr_last_id = mysql_fetch_array(query('select last_insert_id() as id'));
            $item_id = $arr_last_id['id'];
            $item_photo_path_new = $images_dir.'items/'.$item_id.'.'.$img_ext;
            // copytoserv($item_photo_path,$item_photo_path_new);
        }
    }
}

function admin_pages_photos_list()
{
    $arr = cache('preparing_admin_photos_list', ['page' => isset($_GET['p']) ? $_GET['p'] : false, 'clause' => isset($_GET['clause']) ? $_GET['clause'] : false], 0);

    return $arr['pages_list'];
}

function admin_photos_list()
{
    $arr = cache('preparing_admin_photos_list', ['page' => isset($_GET['p']) ? $_GET['p'] : false, 'clause' => isset($_GET['clause']) ? $_GET['clause'] : false], 0);

    return $arr['items_list'];
}

function admin_clauses_photos_list()
{
    $arr = cache('preparing_admin_photos_list', ['page' => isset($_GET['p']) ? $_GET['p'] : false, 'clause' => isset($_GET['clause']) ? $_GET['clause'] : false], 0);

    return $arr['clauses_list'];
}

function preparing_admin_photos_list($args)
{
    $where['not_edited'] = ['name' => '�� �����������������', 'clause' => 'where source_url is null or source_url=\'\''];
    $where['edited'] = ['name' => '�����������������', 'clause' => 'where source_url is not null and source_url!=\'\''];
    $clause = 'not_edited';
    if ($args['clause']) {
        $clause = $args['clause'];
    }
    $list = '';
    foreach ($where as $k => $v) {
        $class = '';
        if ($k == $clause) {
            $class = ' class="disabled"';
        }
        temp_var('class', $class);
        temp_var('clause', $k);
        temp_var('name', $v['name']);
        $list .= template('admin/items/clause');
    }
    $return['clauses_list'] = $list;
    $page = 1;
    if ($args['page']) {
        $page = (int) $args['page'];
    }
    $per_page = 50;
    $from = ($page - 1) * $per_page;
    $data = query('select sql_calc_found_rows id,img_ext,source_url,category_id,price,size,material
		from '.tabname('ezhevikina', 'items').'
		'.$where[$clause]['clause'].' 
		order by category_id
		limit '.$from.','.$per_page.'
		;');
    $arr = readdata($data, 'nokey');
    $rows = rows_without_limit();
    $return['pages_list'] = page_numbers($rows, $per_page, $page, '/?page=admin/index&amp;clause='.$clause.'&amp;p=', $l = 5, $link_end = '', $text_previous = '&laquo;', $text_next = '&raquo;', $first_last_pages = 'yes', $class_current = 'active', $class_points = 'disabled', $a_inside_inactive = 1, $first_page_link = '');
    $arr_categories = readdata(query('select id,name from '.tabname('ezhevikina', 'categories').' order by name;'), 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $options_categories = get_select_option_list(['array' => $arr_categories, 'value' => 'id', 'text' => 'name', 'selected_value' => $v['category_id']]);
        temp_var('options_categories', $options_categories);
        temp_var('item_id', $v['id']);
        if (! isset($v['size'])) {
            $v['size'] = '';
        }
        if (! isset($v['material'])) {
            $v['material'] = '';
        }
        if (! isset($v['source_url'])) {
            $v['source_url'] = '';
        }
        if (! isset($v['price']) or $v['price'] == 0) {
            $v['price'] = '';
        }
        temp_var('size', $v['size']);
        temp_var('material', $v['material']);
        temp_var('source_url', $v['source_url']);
        temp_var('price', $v['price']);
        temp_var('img_path', '/types/ezhevikina/template/images/items/'.$v['id'].'.'.$v['img_ext']);
        $list .= template('admin/items/edit_item');
    }
    $return['items_list'] = '<p>��������: '.count($arr).' �� '.$rows.'</p>'.$list;

    return $return;
}

function admin_submit_edit_item()
{
    if (empty($_POST['item'])) {
        error('�� �������� ��������');
    }
    $q = 0;
    pre($_POST);
    foreach ($_POST['item'] as $item_id => $v) {
        query('update '.tabname('ezhevikina', 'items').' set 
			price=\''.$v['price'].'\',
			material=\''.$v['material'].'\',
			source_url=\''.$v['source'].'\',
			size=\''.$v['size'].'\',
			category_id=\''.$v['category_id'].'\'
			where id=\''.$item_id.'\'
			;');
        $q = $q + mysql_affected_rows();
    }
    alert('��������� ���������. ��������� �����: '.$q.'. <a href="/?page=admin/index">������ �������</a>');
}

function authorizate_ezhevikina_admin()
{
    authorizate('ezhevikina', tabname('ezhevikina', 'users'), ['admin']);
}

function ezhevikina_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^admin$/', $query, $args)) {
        $config['vars']['page'] = 'admin/index';

        return;
    }
}

function ezhevikina_init()
{
    global $config;
    if (empty($config['vars']['page'])) {
        ezhevikina_rewrite_query();
    }
}
