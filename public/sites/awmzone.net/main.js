document.addEventListener('DOMContentLoaded',init);

function prevent_form_submit(form_id) {
  document.getElementById(form_id).addEventListener('submit',function (evnt) {evnt.preventDefault();});
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {
    createCookie(name,"",-1);
}
function init() {
  document.addEventListener('click',eventHandler);
}
//���� roleHandler ���������� � event.target �������� data-role �� ��������� ����������
function eventHandler(event) {
  if ((typeof roleHandler!='undefined') && (event.target.dataset.role)) {roleHandler(event.target);};
}
//��������� ������� func ��� ������� data-role="role"
function forEachRole(role,func) {
  roles=document.querySelectorAll('[data-role="'+role+'"]');
  [].forEach.call(roles,function(role) {
    func(role);
  });
}
// ���� ������� id - ���� ���������� �����, ���� ���, �� ���������� ������ ��������� �� ������
// ���� ������� succes_function - ����������� ����� readyState==4
function ajax_request_new(args) {
  var link=args.link;
  var id=args.id || false;
  var success_function=args.success_function || false;
  // ���� true �� ������� ������� �������
  var clear=args.clear || false;
  // ���� true �� ��������� � �������� � �� ��������
  var add=args.add || false;
  if (id) {
    var cont=document.getElementById(id);
    // ������� �������
    if (clear) {
      // alert('������');
      cont.innerHTML='';
    };
  };
    //cont.innerHTML='���� ��������...';
    var http=new XMLHttpRequest;
    http.open('get', link);
    http.onreadystatechange=function () {
      if (http.readyState==4) {
        if (id) {
          if (add) {
            cont.innerHTML+=http.responseText;
          } else {
            cont.innerHTML=http.responseText;
          };
        };
        if (success_function) {success_function();};
          // if (type=='clear') {cont.innerHTML=http.responseText;};
          // if (type=='add') {cont.innerHTML+=http.responseText;};
        };
      }
      http.send(null);
}
function ajax_request(id,link) {
    var cont=document.getElementById(id);
    // ������� �������
    // cont.innerHTML='';
    //cont.innerHTML='���� ��������...';
    var http=new XMLHttpRequest;
    http.open('get', link);
    http.onreadystatechange=function () {
      if (http.readyState==4) {
          cont.innerHTML=http.responseText;
          // if (type=='clear') {cont.innerHTML=http.responseText;};
          // if (type=='add') {cont.innerHTML+=http.responseText;};
        };
      }
      http.send(null);
}
function ajax_request_by_element(element,link) {
    var http=new XMLHttpRequest;
    window.returned='created';
    // var returned;
    http.open('get', link);
    http.onreadystatechange=function () {
      if (http.readyState==4) {
        element.innerHTML=http.responseText;
        window.returned=http.responseText;
      };
      // alert(window.returned);
      }
      http.send(null);
}

function symbol_counter(text_block_id,counter_block_id,max_symbols) {
  counter_block=document.getElementById(counter_block_id);
  text_block=document.getElementById(text_block_id);
  text_length=text_block.value.length;
  counter_block.innerHTML=max_symbols-text_length;
}