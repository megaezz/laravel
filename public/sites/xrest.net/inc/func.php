<?php

namespace MegawebV1;

function xrest_sitemap()
{
    global $config;
    $domain = 'http://www.xrests.net/';
    if (in_array($config['domain_origin'], ['xrest.net', 'www.xrest.net'])) {
        $domain = 'http://www.xrest.net/';
    }
    $priority = [
        'main' => '1.000', 'pages' => '0.500', 'categories' => '0.500', 'videos' => '0.500',
        'articles' => '0.500', 'photos' => '0.500', 'tags' => '0.500',
    ];
    $urls = cache('preparing_xrest_sitemap');
    $list = '';
    foreach ($urls as $type => $links) {
        foreach ($links as $v) {
            $list .= '<url><loc>'.$domain.$v.'</loc><priority>'.$priority[$type].'</priority></url>
			';
        }
    }

    return $list;
}

function preparing_xrest_sitemap()
{
    global $config;
    $urls['main'][] = '';
    $urls['pages'][] = 'top.html';
    $urls['pages'][] = 'photos/';
    $urls['pages'][] = 'articles/';
    $urls['pages'][] = 'categories.html';
    $urls['pages'][] = 'book.html';
    $urls['pages'][] = 'news.html';
    $urls['pages'][] = 'info.html';
    $urls['pages'][] = 'parents.html';
    $urls['pages'][] = 'faq.html';
    $urls['pages'][] = 'feedback.html';
    // ��������� �����
    $arr = cache('preparing_categories_list', ['mobile' => 'no', 'template' => 'li/categories_list']);
    foreach ($arr as $v) {
        $urls['categories'][] = $v['name_translit'].'/';
    }
    // ������ �����
    $data = query('select id,title_ru_translit,unix_timestamp(apply_date) as date from '.tabname('videohub', 'videos').' where domain=\''.$config['domain'].'\'');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        $urls['videos'][] = 'videos/'.$v['title_ru_translit'].'.html';
    }
    // ������
    $data = query('select title_translit,unix_timestamp(apply_date) as date from '.tabname('videohub', 'stories').' where domain=\''.$config['domain'].'\';');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        $urls['articles'][] = date('Y/m/d', $v['date']).'/article-'.$v['title_translit'].'.html';
    }
    // ����
    $data = query('select id,title_ru_translit,unix_timestamp(apply_date) as date from '.tabname('videohub', 'galleries').' where domain=\''.$config['domain'].'\'');
    $arr = readdata($data, 'nokey');
    foreach ($arr as $v) {
        $urls['photos'][] = date('Y/m/d', $v['date']).'/photo-'.$v['title_ru_translit'].'.html';
    }
    // ���� �����
    $arr = cache('preparing_all_tags_list');
    foreach ($arr as $v) {
        $urls['tags'][] = 'index.php?action=tags&amp;tag='.urlencode($v['tag_ru']);
    }

    return $urls;
}

function xrest_pages_games_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'games_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_games_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/games/', $args['pages_length'], '/', '�����', '������', 'yes', 'unlinked', 'points');

    return $list;
}

function xrest_pages_galleries_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'galleries_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_galleries_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/photos/', $args['pages_length'], '/', '�����', '������', 'yes', 'unlinked', 'points');

    return $list;
}

function xrest_top100()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = 1;
    $args['sort'] = 'mv';
    $args['per_page'] = 100;
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args, 43200);

    return thumbs_from_array($arr['videos']);
}

function xrest_pages_stories_list_all()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'stories_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_stories_list', $args);
    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows = $arr['rows']; // ����� �������
    $pages = ceil($rows / $args['per_page']); // �������
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/atricles/', $pages_length, '/', '�����', '������', 'yes', 'unlinked', 'points');

    return $list;
}

function xrest_join_videos()
{
    $data1 = query('select * from '.tabname('videos', 'xrest_videos').';');
    $xrest_videos = readdata($data1, 'nokey');
    $data2 = query('select * from '.tabname('videos', 'xrest_videos_mgw').';');
    $xrest_videos_mgw = readdata($data2, 'nokey');
    foreach ($xrest_videos as $xv) {

    }
}

function xrest_change_same_title_ru_translits()
{
    $data = query('select title_ru_translit,count(*) as q from '.tabname('videohub', 'xrest_videos').'
		group by title_ru_translit having q!=1;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $data = query('select id,apply_date from '.tabname('videohub', 'xrest_videos').'
			where title_ru_translit=\''.$v['title_ru_translit'].'\' order by apply_date asc;');
        $arr2 = readdata($data, 'nokey');
        $list2 = '';
        $i = 0;
        foreach ($arr2 as $v2) {
            $i++;
            if ($i == count($arr2)) {
                $newid = $v['title_ru_translit'];
            } else {
                $newid = $v['title_ru_translit'].$i;
            }
            $list2 .= '<li>'.$v2['id'].' - '.$v2['apply_date'].' - '.$newid.'</li>';
            // query('update '.tabname('videohub','xrest_videos').' set title_ru_translit=\''.$newid.'\'
            // 	where id=\''.$v2['id'].'\';');
        }
        $list .= '<li>'.$v['title_ru_translit'].'<ol>'.$list2.'</ol></li>';
    }
    $content = '<ol>'.$list.'</ol>';

    return $content;
}

// ��������� ������ �� ��������� ���� � mysql
function xrest_transfer_base_db()
{
    global $config;
    ini_set('memory_limit', '1000M');
    $file = file($config['path']['server'].'sites/xrest.net/db.hard/db.dat');
    $inserted = 0;
    foreach ($file as $n => $str) {
        $v = explode(' | ', $str);
        query('insert into '.tabname('videohub', 'xrest_videos').' set
			domain=\'xrest.net\',
			hub=\'pornhub.com\',
			id_hub=\''.$v[0].'\',
			add_date=from_unixtime(\''.$v[1].'\'),
			apply_date=from_unixtime(\''.$v[1].'\'),
			title_en=\''.mysql_real_escape_string($v[2]).'\',
			title_ru=\''.mysql_real_escape_string($v[3]).'\',
			title_ru_translit=\''.mysql_real_escape_string($v[7]).'\',
			description=\''.trim($v[8]).'\',
			ph_viewkey=\''.trim($v[9]).'\',
			ph_date=\''.trim($v[10]).'\';'
        );
        $tags_arr = explode(', ', $v[4]);
        $data = query('select last_insert_id();');
        $last_insert_arr = mysql_fetch_array($data);
        $last_insert = $last_insert_arr[0];
        foreach ($tags_arr as $tag) {
            query('insert into '.tabname('videohub', 'xrest_tags').' set video_id=\''.$last_insert.'\', tag_en=\''.mysql_real_escape_string(trim($tag)).'\';');
            // die;
        }
        $inserted++;
    }

    return '��������� '.$inserted.' �����';
}

function xrest_compare_db_and_videohub()
{
    global $config;
    ini_set('memory_limit', '1000M');
    $file = file($config['path']['server'].'types/xrest.net/xrest.net/db/db.dat');
    $inserted = 0;
    $updated = 0;
    $list = '';
    foreach ($file as $n => $str) {
        $v = explode(' | ', $str);
        $data = query('select id_hub,title_ru,title_ru_translit,description
			from '.tabname('videohub', 'videos').'
			where domain=\'xrest.net\' and id_hub=\''.$v[0].'\'
			and title_ru_translit=\''.mysql_real_escape_string($v[7]).'\'
				/*
				and	(date(apply_date)=date(from_unixtime('.$v[1].'))
				or (unix_timestamp(apply_date)='.$v[1].'-7200)
				or (unix_timestamp(apply_date)='.$v[1].'-10800)
				)
		*/
		;');

        $arr = mysql_fetch_assoc($data);
        if (isset($arr['id_hub'])) {
            if ($arr['description'] != $v[8]) {
                // $list.='<li>'.$v[0].' ������ �������� "mgw:'.$arr['description'].'"|"'.$v[8].'"</li>';
            }
            if ($arr['title_ru_translit'] != $v[7]) {
                // $list.='<li>'.$v[0].' ������ �������� "mgw:'.$arr['title_ru_translit'].'"|"'.$v[7].'"</li>';
            }
            if ($arr['title_ru'] != $v[3]) {
                // $list.='<li>'.$v[0].' ������ ��������� "mgw:'.$arr['title_ru'].'"|"'.$v[3].'"</li>';
                /*
                query('update '.tabname('videohub','videos').' set title_ru=\''.mysql_real_escape_string($v[3]).'\',
                title_ru_translit=\''.$v[7].'\'
                where domain=\'xrest.net\' and id_hub=\''.$v[0].'\' and unix_timestamp(add_date)='.($v[1]+7200).';');
                */
                // $list.=('update '.tabname('videohub','videos').' set title_ru=\''.mysql_real_escape_string($v[3]).'\',
                // title_ru_translit=\''.$v[7].'\'
                // where domain=\'xrest.net\' and id_hub=\''.$v[0].'\';');
                $updated++;
                // };
            }
            // $list.='<li>'.$v[0].' ���� � ����</li>';
        } else {
            /*
            $data2=query('select id_hub,title_ru,title_ru_translit,description,apply_date,unix_timestamp(apply_date) as unix_apply_date
            from '.tabname('videohub','videos').'
            where domain=\'xrest.net\' and id_hub=\''.$v[0].'\';');
            $arr2=mysql_fetch_assoc($data2);
            if (isset($arr2['id_hub'])) {
                $dif=($arr2['unix_apply_date']-$v[1]);
                $list.='<li>'.$v[0].' ������ ���� "�������:'.$dif.'</li>';
                // query('update '.tabname('videohub','videos').' set apply_date=from_unixtime('.$v[1].')
                    // where domain=\'xrest.net\' and id_hub=\''.$v[0].'\' and apply_date=\''.$arr2['apply_date'].'\';');
                // date('Y-m-d H-i:s',$v[1])
            } else {
                */
            $data2 = query('select id_hub,title_ru,title_ru_translit,description,apply_date,unix_timestamp(apply_date) as unix_apply_date
			from '.tabname('videohub', 'videos').'
			where domain=\'xrest.net\' and id_hub=\''.$v[0].'\'
			and	date(apply_date)=date(from_unixtime('.$v[1].'))
			;');
            $arr2 = mysql_fetch_assoc($data2);
            if (isset($arr2['id_hub'])) {
                // $list.='<li>'.$v[0].' ���� � ������ ���������: "mgw:'.$arr2['title_ru'].'"|"'.$v[3].'"</li>';
                $list .= '<li>'.$v[0].' ���� � ������ ���������: "mgw:'.$arr2['title_ru_translit'].'"|"'.$v[7].'"</li>';
                // query('update '.tabname('videohub','videos').' set apply_date=from_unixtime('.$v[1].')
                // where domain=\'xrest.net\' and id_hub=\''.$v[0].'\' and apply_date=\''.$arr2['apply_date'].'\';');
                // date('Y-m-d H-i:s',$v[1])
            } else {
                $data3 = query('select id_hub,title_ru,title_ru_translit,description
					from '.tabname('videohub', 'videos').'
					where domain=\'xrest.net\' and id_hub=\''.str_pad($v[0], 9, '0', STR_PAD_LEFT).'\'
					and title_ru=\''.mysql_real_escape_string($v[3]).'\'
					;');
                // and (date(apply_date)=date(from_unixtime('.$v[1].')) or (unix_timestamp(apply_date)='.$v[1].'-7200) or (unix_timestamp(apply_date)='.$v[1].'-10800))
                $arr3 = mysql_fetch_assoc($data3);
                if (isset($arr3['id_hub'])) {
                    $list .= '<li>'.$v[0].' ������ id_hub "mgw:'.$arr3['id_hub'].'"</li>';
                    // query('update '.tabname('videohub','videos').' set id_hub=\''.$v[0].'\'
                    // where domain=\'xrest.net\' and id_hub=\''.str_pad($v[0], 9, '0', STR_PAD_LEFT).'\';');
                } else {
                    $list .= '<li>'.$v[0].' ��� � ����</li>';
                    /*
                    query('insert into '.tabname('videohub','videos').' set
                        domain=\'xrest.net\',
                        hub=\'pornhub.com\',
                        id_hub=\''.$v[0].'\',
                        add_date=from_unixtime(\''.$v[1].'\'),
                        apply_date=from_unixtime(\''.$v[1].'\'),
                        title_en=\''.mysql_real_escape_string($v[2]).'\',
                        title_ru=\''.mysql_real_escape_string($v[3]).'\',
                        title_ru_translit=\''.mysql_real_escape_string($v[7]).'\',
                        description=\''.$v[8].'\',
                        ph_viewkey=\''.trim($v[9]).'\',
                        ph_date=\''.trim($v[10]).'\';'
                        );
                    $tags_arr=explode(', ',$v[4]);
                    $data=query('select last_insert_id();');
                    $last_insert_arr=mysql_fetch_array($data);
                    $last_insert=$last_insert_arr[0];
                    foreach ($tags_arr as $tag) {
                        query('insert into '.tabname('videohub','tags').' set video_id=\''.$last_insert.'\', tag_en=\''.mysql_escape_string(trim($tag)).'\';');
                    };
                    $inserted++;
                    */
                }
            }
        }
    }
    echo '<ol>�������� ����������: '.$updated.'<br/>���������: '.$inserted.'<br/> '.$list.'</ol>';
    exit('�����');
    exit;
    foreach ($file as $n => $str) {
        $v = explode(' | ', $str);
        if (isset($arr[$v[0]]) == 1) {
            if ($arr[$v[0]]['title_ru'] != $v[3]) {
                query('update '.tabname('videohub', 'videos').' set title_ru=\''.mysql_real_escape_string($v[3]).'\',
					title_ru_translit=\''.$v[7].'\'
					where domain=\'xrest.net\' and id_hub=\''.$v[0].'\';');
                $updated++;
            }
        }
        if ((isset($arr[$v[0]]) == 0) and (empty($v[3]) == 0)) {
            query('insert into '.tabname('videohub', 'videos').' set
				domain=\'xrest.net\',
				hub=\'pornhub.com\',
				id_hub=\''.$v[0].'\',
				add_date=from_unixtime(\''.$v[1].'\'),
				apply_date=from_unixtime(\''.$v[1].'\'),
				title_en=\''.mysql_real_escape_string($v[2]).'\',
				title_ru=\''.mysql_real_escape_string($v[3]).'\',
				title_ru_translit=\''.mysql_real_escape_string($v[7]).'\',
				description=\''.$v[8].'\',
				ph_viewkey=\''.trim($v[9]).'\',
				ph_date=\''.trim($v[10]).'\';'
            );
            $tags_arr = explode(', ', $v[4]);
            $data = query('select last_insert_id();');
            $last_insert_arr = mysql_fetch_array($data);
            $last_insert = $last_insert_arr[0];
            foreach ($tags_arr as $tag) {
                query('insert into '.tabname('videohub', 'tags').' set video_id=\''.$last_insert.'\', tag_en=\''.mysql_escape_string(trim($tag)).'\';');
            }
            $inserted++;
        }
    }

    return '��������� '.$inserted.' �����<br/>���������: '.$updated;
}

// ����� ����� ��� �����
function xrest_video_tags_list()
{
    if (empty($_GET['id'])) {
        error('�� �������� ����������� ����������');
    }
    $args['id'] = $_GET['id'];
    $arr = cache('preparing_video_tags_list_array', $args);
    $list = '';
    $i = 0;
    foreach ($arr as $v) {
        $i++;
        if ($i > 6) {
            break;
        }
        if ($i != 1) {
            $list .= ', ';
        }
        $list .= '<a href="/index.php?action=tags&amp;tag='.urlencode($v['tag_ru']).'" title="'.$v['tag_ru'].'">'.$v['tag_ru'].'</a>';
    }

    return $list;
}

function xrest_videos_list_category()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'no';
    // if (isset($_GET['secret'])) {pre($args);};
    $arr = cache('preparing_videos_list', $args);

    return thumbs_from_array($arr['videos'], 'li/thumbs_category');
}

function xrest_pages_videos_list_tag()
{
    global $config;
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $args['domain'] = $config['domain'];
    $args['type'] = 'tag';
    $args['value'] = $_GET['tag_ru'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/index.php?action=tags&tag='.urlencode($_GET['tag_ru']).'&page=', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'unlinked', 'points');
    $content = '<h3>��� ����� � �����: {tag_name} ('.$rows.')</h3>
	<ul>'.$list.'</ul>
	<hr />';

    return $content;
}

function xrest_pages_videos_list_category()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    // $pages_length=value_from_config_for_domain('videohub','pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    // pre($args);
    $arr = cache('preparing_videos_list', $args);
    // $arr=preparing_videos_list($args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/'.category_name_translit().'/', $args['pages_length'], '.html', '&laquo;', '&raquo;', 'yes', 'unlinked', 'points');

    // $list=page_numbers($rows,$args['per_page'],$args['page'],'/page/',$args['pages_length'],'/','&laquo;','&raquo;','yes','active','active','1');
    return $list;
}

function xrest_videos_list_new()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = 1;
    $args['sort'] = 'mr';
    $args['per_page'] = 100;
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args, 43200);

    return thumbs_from_array($arr['videos'], 'li/thumbs_new');
}

function xrest_pages_videos_list_all()
{
    global $config;
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 6;
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    // $arr=preparing_videos_list($args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/', $pages_length, '.html', '&laquo;', '&raquo;', 'yes', 'unlinked', 'points');

    // $list=page_numbers($rows,$args['per_page'],$args['page'],'/page/',$args['pages_length'],'/','&laquo;','&raquo;','yes','active','active','1');
    return $list;
}

function xrest_videos_list_all()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (! isset($_GET['p'])) {
        $args['page'] = 6;
    } else {
        $args['page'] = (int) $_GET['p'];
    }
    if (! isset($_GET['sort'])) {
        $args['sort'] = 'mr';
    } else {
        $args['sort'] = $_GET['sort'];
    }
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args, 43200);

    return thumbs_from_array($arr['videos']);
}

function xrest_guestbook_pages()
{
    $args['link'] = '/guestbook/';

    return page_comments_pages($args);
}

function xrest_guestbook_posts()
{
    $args['template'] = 'guestbook/post';
    global $config;
    $data = query('select SQL_CALC_FOUND_ROWS unix_timestamp(date) as date,guest_name,text,marked
		from '.tabname('videohub', 'page_comments').' where domain=\''.$config['domain'].'\' and active=\'1\' and page=\''.$config['vars']['page'].'\'
		order by date asc
		;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $date = my_date($v['date']);
        temp_var('date', $date['day'].' '.$date['month'].' '.$date['year'].' �. � '.$date['clock']);
        temp_var('guest_name', $v['guest_name']);
        temp_var('text', $v['text']);
        if ($v['marked'] == '1') {
            temp_var('marked', 'marked');
        } else {
            temp_var('marked', '');
        }
        $list .= template($args['template']);
    }

    return $list;
}

// ////////////////////////

function xrest_embed_or_video_player()
{
    $ph_viewkey = ph_viewkey();
    if (empty($ph_viewkey) or ($ph_viewkey == 'no')) {
        return template('player/embed');
    } else {
        return template('player/iframe');
    }
}

function xrest_url_encode_video_save_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return urlencode('/download/'.get_title_ru_translit().'.flv');
}

function xrest_video_html_codes()
{
    $data = query('select id,title_ru,title_en,title_ru_translit,id_hub from '.tabname('videohub', 'videos').' where id=\''.$_GET['id'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($_GET['name_translit'])) {
        $save_link = http_current_domain().'/download/'.get_title_ru_translit().'.flv';
        $watch_link = http_current_domain().'/videos/'.get_title_ru_translit().'.html';
    }
    if (! empty($_GET['name_translit'])) {
        $save_link = http_current_domain().'/download/'.$_GET['name_translit'].'/'.get_title_ru_translit().'.flv';
        $watch_link = http_current_domain().'/videos/'.$_GET['name_translit'].'/'.get_title_ru_translit().'.html';
    }
    $img_dir_link = http_current_domain().'/pics/'.xrest_pornhub_id_slashed($arr['id_hub']).'/';
    $html_link = '<br /><span style="font-size: 14px; font-weight: bold;"><a href="'.$watch_link.'" title="'.$arr['title_ru'].'" target="_blank">'.$arr['title_ru'].'</a></span>'."\r\n";
    $htmlcode_big = htmlspecialchars('<object type="application/x-shockwave-flash" data="/player/xrestnet_v4.swf" width="705" height="565">
		<param name="bgcolor" value="#ffffff" />
		<param name="allowFullScreen" value="true" />
		<param name="allowScriptAccess" value="always" />
		<param name="wmode" value="transparent" />
		<param name="movie" value="/player/xrestnet_v4.swf" />
		<param name="flashvars" value="st=/player/xrest_go_big_v3.txt&amp;file='.$save_link.'" />
		</object>
		'.$html_link, ENT_QUOTES, 'cp1251');
    $htmlcode_small = htmlspecialchars('
		<object type="application/x-shockwave-flash" data="/player/xrestnet.swf" width="505" height="415">
		<param name="bgcolor" value="#ffffff" />
		<param name="allowFullScreen" value="true" />
		<param name="allowScriptAccess" value="always" />
		<param name="wmode" value="transparent" />
		<param name="movie" value="/player/xrestnet.swf" />
		<param name="flashvars" value="st=/player/video_small.txt&amp;file='.$save_link.'" />
		</object>
		'.$html_link, ENT_QUOTES, 'cp1251');
    $htmlcode_img9 = '';
    for ($i = 1; $i <= 9; $i++) {
        $htmlcode_img9 .= '<img src="'.$img_dir_link.$i.'.jpg" alt="'.$arr['title_ru'].': ���� #'.$i.'" />';
        if (($i % 3 == 0) & ($i != 9)) {
            $htmlcode_img9 .= '<br />';
        }
    }
    $htmlcode_9 = htmlspecialchars('
		<div style="width: 480px; text-align: left;">
		<a href="'.$watch_link.'">
		<span style="font-size: 14px; font-weight: bold;">'.$arr['title_ru'].'</span>
		</a><br />'.$htmlcode_img9.'<br />
		<a href="'.$save_link.'">
		<span style="font-size: 12px; font-weight: bold;">������� ��� ����� �����</span>
		</a></div>
		', ENT_QUOTES, 'cp1251');
    $bbcode_1 = htmlspecialchars('[size=4][b][u][url='.$watch_link.']'.$arr['title_ru'].'[/url][/u][/b][/size]'."\r\n".'[img]'.$img_dir_link.'0.jpg[/img]'."\r\n".'[url='.$watch_link.'][size=3][b][u]������� ��� ����� �����[/u][/b][/size][/url]', ENT_QUOTES, 'cp1251');
    $bbcode_img9 = '';
    for ($i = 1; $i <= 9; $i++) {
        $bbcode_img9 .= '[img]'.$img_dir_link.$i.'.jpg[/img]';
        if (($i % 3 == 0) & ($i != 9)) {
            $bbcode_img9 .= "\r\n";
        }
    }
    $bbcode_9 = htmlspecialchars('[size=4][b][u][url='.$watch_link.']'.$arr['title_ru'].'[/url][/u][/b][/size]'."\r\n".$bbcode_img9."\r\n".'[url='.$save_link.'][size=3][b][u]������� ��� ����� �����[/u][/b][/size][/url]', ENT_QUOTES, 'cp1251');
    $bbcode_img16 = '';
    for ($i = 1; $i <= 16; $i++) {
        $bbcode_img16 .= '[img]'.$img_dir_link.$i.'.jpg[/img]';
        if (($i % 4 == 0) & ($i != 16)) {
            $bbcode_img16 .= "\r\n";
        }
    }
    $bbcode_16 = htmlspecialchars('[size=4][b][u][url='.$watch_link.']'.$arr['title_ru'].'[/url][/u][/b][/size]'."\r\n".$bbcode_img16."\r\n".'[url='.$save_link.'][size=3][b][u]������� ��� ����� �����[/u][/b][/size][/url]', ENT_QUOTES, 'cp1251');
    $bbcode_16_en = htmlspecialchars('[size=4][b][u][url='.$watch_link.']'.$arr['title_en'].'[/url][/u][/b][/size]'."\r\n".$bbcode_img16."\r\n".'[url='.$save_link.'][size=3][b][u]Download this porn video for free[/u][/b][/size][/url]', ENT_QUOTES, 'cp1251');
    $content = '
	<h3>HTML ��� &mdash; ������� ����� (700�525)</h3>
	<textarea class="htmlcode" rows="10" cols="40" onclick="this.focus(); this.select();">
	'.$htmlcode_big.'</textarea>
	<h3>HTML ��� &mdash; ������� ����� (500�375)</h3>
	<textarea class="htmlcode" rows="10" cols="40" onclick="this.focus(); this.select();">
	'.$htmlcode_small.'</textarea>
	<h3>HTML ��� &mdash; �������� (9 ������)</h3>
	<textarea class="htmlcode" rows="10" cols="40" onclick="this.focus(); this.select();">
	'.$htmlcode_9.'</textarea>
	<h3>BB ��� ��� ������ � ������� (1 ����)</h3>
	<textarea class="htmlcode" rows="10" cols="40" onclick="this.focus(); this.select();">
	'.$bbcode_1.'</textarea>
	<h3>BB ��� ��� ������ � ������� (9 ������)</h3>
	<textarea class="htmlcode" rows="10" cols="40" onclick="this.focus(); this.select();">
	'.$bbcode_9.'</textarea>
	<h3>BB ��� ��� ������ � ������� (16 ������)</h3>
	<textarea class="htmlcode" rows="10" cols="40" onclick="this.focus(); this.select();">
	'.$bbcode_16.'</textarea>
	<h3>BB ��� ��� ������ � ������� (16�. ����.)</h3>
	<textarea class="htmlcode" rows="10" cols="40" onclick="this.focus(); this.select();">
	'.$bbcode_16_en.'</textarea>
	<div class="rules" onclick="show_hide_text(this)" title="�������� ��������">������� ���������� ����</div>
	<div class="nodisp" style="text-align: justify;">
	�� ��������� ������� ����, ������� ����� ��������� ������������ �� ����� �����, ���������� �� �������. �������, �� ��� �� ��������� �� ��������������.
	����� ����, ��� ������ ����������� �� ����� <b>���</b> ����� ���� <b>���������</b>, ������ ����������. � �������� ������ ��� ���� ����� ������� � ������ ������ �
	����� ��� �������� �� ��� ������������ �� �����. ���� ��� ��� �� ����� ��������������� ���, ��������� � ���� ����� �������� ��� �������� ����� � �� ����� ��������.
	�� ����� ������� �� �����������, ��� ����� ������ ��� ��������� ��� ����, ����� ������ �� ����� ������ ���� ��������� � �������� ��� ���� :).
</div>
';

    return $content;
}

function xrest_url_encode_video_image_link()
{
    if (isset($_GET['id']) == 0) {
        error('�� ����� ID �����');
    }

    return urlencode('http://images.xrest.net/'.$_GET['id'].'.jpg');
}

function xrest_auth()
{
    if (! isset($_GET['mmnas'])) {
        authorizate_videohub_viewer();
    }
}

// ��������� 30 ����� ��� �������� RSS
function xrest_rss_videos_list()
{
    global $config;
    $data = query('select id,title_ru,title_ru_translit,date_format(add_date,\'%a, %d %b %Y %T GMT\') as date from '.tabname('videohub', 'videos').' where domain=\''.$config['domain'].'\' order by add_date desc limit 30');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '
		<item>
		<title>'.$v['title_ru'].'</title>
		<link>'.http_current_domain().'/videos/'.$v['title_ru_translit'].'.html</link>
		<pubDate>'.$v['date'].'</pubDate>
		</item>
		';
    }

    return $list;
}

function xrest_top_20_videos()
{
    global $config;
    $per_page = 20;
    $data = query('select SQL_CALC_FOUND_ROWS id,title_ru,title_ru_translit,unix_timestamp(add_date) as date,
		ifnull(videos_views.views,0) as views, ifnull(rating,0) as rating, ifnull(votes,0) as votes, videos.id_hub,
		((videos_views.rating)*(1-(0.5/sqrt((videos_views.votes))))) as rating_sko
		from '.tabname('videohub', 'videos').'
						left join '.tabname('videohub', 'videos_views').' on videos.id=videos_views.video_id
						where domain=\''.$config['domain'].'\'
						order by rating_sko desc, views desc
						limit '.$per_page);
    $arr = readdata($data, 'nokey');

    return thumbs_from_array($arr);
}

function xrest_tags_list()
{
    global $config;
    // $data=query('
    // 	SELECT DISTINCT tags.tag_en, tags_translation.tag_ru,count(distinct videos.id) as q_tag_ru
    // 	FROM '.tabname('videohub','tags').'
    // 	INNER JOIN '.tabname('videohub','tags_translation').' ON tags.tag_en = tags_translation.tag_en
    // 	INNER JOIN '.tabname('videohub','videos').' ON videos.id = tags.video_id
    // 	WHERE videos.domain=\''.$config['domain'].'\'
    // 	group by tag_ru
    // 	order by q_tag_ru desc
    // 	limit 70
    // 	');
    /*
    $data=query('
        SELECT DISTINCT tags.tag_en, tags_translation_small.tag_ru,count(distinct videos.id) as q_tag_ru
        FROM '.tabname('videohub','tags').'
        INNER JOIN '.tabname('xrest_net_changed','tags_translation_small').' ON tags.tag_en = tags_translation_small.tag_en
        INNER JOIN '.tabname('videohub','videos').' ON videos.id = tags.video_id
        WHERE tags_translation_small.tag_ru is not null and tags_translation_small.tag_ru!=\'\' and videos.domain=\''.$config['domain'].'\'
        group by tags_translation_small.tag_ru
        order by tags_translation_small.tag_ru
        ');
    */
    $data = query('
		SELECT DISTINCT tags.tag_en, tags.tag_ru,count(distinct videos.id) as q_tag_ru
		FROM '.tabname('videohub', 'tags').'
		INNER JOIN '.tabname('videohub', 'videos').' ON videos.id = tags.video_id
		WHERE tags.tag_ru is not null and tags.tag_ru!=\'\' and videos.domain=\''.$config['domain'].'\'
		group by tags.tag_ru
		order by q_tag_ru desc
		limit 205
		');
    $arr = readdata($data, 'nokey');
    $max_q = 0;
    foreach ($arr as $v) {
        if ($v['q_tag_ru'] > $max_q) {
            $max_q = $v['q_tag_ru'];
        }
    }
    // $max_q=$arr[0]['q_tag_ru']; //������������ ���������� ������� �� ������-�� ����
    $max_px = 40; // ������������ ������ ������ ����
    $min_px = 10; // ����������� ������ ������ ����
    $list = '';
    foreach ($arr as $k => $v) {
        if ($k != 0) {
            $hearts = ' &hearts; ';
        } else {
            $hearts = '';
        }
        $font_size = ceil($max_px * ($v['q_tag_ru'] / $max_q));
        if ($font_size < $min_px) {
            $font_size = $min_px;
        }
        $list .= '<li>'.$hearts.'<a href="/index.php?action=tags&amp;tag='.urlencode($v['tag_ru']).'" title="'.$v['q_tag_ru'].' �����" style="font-size: '.$font_size.'px;">'.$v['tag_ru'].'</a></li>'."\r\n";
    }

    return $list;
}

function cached_xrest_tags_list()
{
    $content = cache('xrest_tags_list', [], 86400);

    return $content;
}

function xrest_comments_list()
{
    global $config;
    if (empty($_GET['id'])) {
        error('�� ������� ID');
    }
    $data = query('select guest_name,text,unix_timestamp(date) as date,user_id from '.tabname('videohub', 'comments').' where video_id=\''.$_GET['id'].'\' and active=\'1\' order by date');
    $arr = readdata($data, 'nokey');
    $says = ['�������', '������', '������, ���', '������', '������', '������'];
    $list = '';
    foreach ($arr as $v) {
        $date_arr = my_date($v['date']);
        $date = $date_arr['day'].' '.$date_arr['month'].' '.$date_arr['year'].' � '.$date_arr['clock'];
        temp_var('date', $date);
        temp_var('user', $v['guest_name']);
        temp_var('text', $v['text']);
        temp_var('says', $says[mt_rand(0, count($says) - 1)]);
        $list .= template('comments/li');
    }
    if (empty($list)) {
        $list = '<div class="first">��� ���������� ����� ������ =)</div>';
    }

    return $list;
}

function xrest_pornhub_id_slashed($id)
{
    $id9 = str_pad($id, 9, '0', STR_PAD_LEFT);
    $ph_id = substr($id9, 0, 3).'/'.substr($id9, 3, 3).'/'.substr($id9, 6, 3);
    //
    $data = query('select ph_date from '.tabname('videohub', 'videos').' where id_hub=\''.$id.'\';');
    $arr = mysql_fetch_assoc($data);
    if ($arr['ph_date'] != '' and $arr['ph_date'] != 'no') {
        if (! empty($_GET['name_translit'])) {
            return '/pics-n/'.$_GET['name_translit'].'/'.$arr['ph_date'].'/'.ltrim($id, '0');
        }

        return '/pics-n/'.$arr['ph_date'].'/'.ltrim($id, '0');
    }
    //
    if (! empty($_GET['name_translit'])) {
        return '/pics/'.$_GET['name_translit'].'/'.$ph_id;
    }

    return '/pics/'.$ph_id;
}

function xrest_last_viewed_videos()
{
    global $config;
    $data = query('select videos.title_ru_translit, videos.title_ru from '.tabname('videohub', 'videos_views').'
		join '.tabname('videohub', 'videos').' on videos.id=videos_views.video_id
		where domain=\''.$config['domain'].'\'
		order by update_time desc
		limit 5;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li><a href="/videos/'.$v['title_ru_translit'].'.html">'.$v['title_ru'].'</a></li>';
    }

    return $list;
}

function xrest_text_popular_videos()
{
    global $config;
    $data = query('select videos_views.views, videos.title_ru_translit, videos.title_ru from '.tabname('videohub', 'videos_views').'
		join '.tabname('videohub', 'videos').' on videos.id=videos_views.video_id
		where domain=\''.$config['domain'].'\'
		order by rating desc, votes desc, views desc
		limit 5;');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<li><a href="/videos/'.$v['title_ru_translit'].'.html">'.$v['title_ru'].'</a> <span class="hits">(����������: '.$v['views'].')</span></li>';
    }

    return $list;
}

function template_block_player()
{
    return template('block_player');
}

function xrest_block_player_category()
{
    return template('block_player_category');
}

function xrest_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^sitemap.xml$/', $query, $args)) {
        $config['vars']['page'] = 'sitemap';

        return;
    }
    if (preg_match('/^top\.html$/', $query, $args)) {
        $config['vars']['page'] = 'top';

        return;
    }
    // ����� � videohub_rewrite_query
    if (preg_match('/^contact$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^rss$/', $query, $args)) {
        // $config['vars']['page']='rss';
        error('�������� �� ����������', '', 0);

        return;
    }
    if (preg_match('/^search$/', $query, $args)) {
        if (empty($_POST['search_query'])) {
            error('�� ����� ��������� ������');
        }
        $config['vars']['page'] = 'submit_search';

        return;
    }
    if (preg_match('/^search\/([^\/]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $args[1] = htmlspecialchars($args[1]);
        $_GET['search_query'] = $args[1];
        $_REQUEST['search_query'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $args[1] = htmlspecialchars($args[1]);
        $_GET['search_query'] = $args[1];
        $_REQUEST['search_query'] = $args[1];
        $_GET['sort'] = $args[2];
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^save\/([\w]+)\/([\d]+)-([\w\-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = $args[2];
        $_GET['title_ru_translit'] = $args[3];

        return;
    }
    if (preg_match('/^guestbook$/', $query, $args)) {
        $config['vars']['page'] = 'guestbook/page';
        $_GET['p'] = 1;

        return;
    }
    // if (preg_match('/^guestbook\/([\d]+)$/',$query,$args)) {
    if (preg_match('/^book.html$/', $query, $args)) {
        $config['vars']['page'] = 'guestbook/page';

        // $_GET['p']=$args[1];
        return;
    }
    // ����� ����� � videohub_rewrite_query
    if (preg_match('/^categories.html$/', $query, $args)) {
        $config['vars']['page'] = 'categories_list';

        return;
    }
    if (preg_match('/^chat.html$/', $query, $args)) {
        $config['vars']['page'] = 'chat';

        return;
    }
    if (preg_match('/^news.html$/', $query, $args)) {
        $config['vars']['page'] = 'site_news/page';

        return;
    }
    if (preg_match('/^info.html$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^parents.html$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    if (preg_match('/^faq.html$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^feedback.html$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    // ������
    if (preg_match('/^articles\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^articles\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/article-([\w]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_translit($args[4]);
        story_views_count();

        return;
    }
    // //������
    // ����� �������� ������ �����
    if (preg_match('/^([\d]+).html$/', $query, $args)) {
        $_GET['p'] = $args[1];
        $config['vars']['page'] = 'collection';

        return;
    }
    // �������� �����
    if (preg_match('/^videos\/([\w\-()]+).html$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['title_ru_translit'] = $args[1];
        $_GET['id'] = xrest_get_video_id_from_translit($args[1]);
        views_count();

        return;
    }
    // �������� ����� �� ���������
    if (preg_match('/^videos\/([\w\-()]+)\/([\w\-()]+).html$/', $query, $args)) {
        // ��� �����
        $_GET['title_ru_translit'] = $args[2];
        $_GET['id'] = xrest_get_video_id_from_translit($args[2]);
        // ��� ���������
        $_GET['name_translit'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;
        $ycr = yandex_categories_redirect($_GET['name_translit']);
        if ($ycr) {
            redirect_301('/videos/'.$ycr.'/'.$_GET['title_ru_translit'].'.html');
        }
        $_GET['category_id'] = xrest_get_category_id_from_translit($args[1]);
        $config['vars']['page'] = 'video_category';
        redirect_301('/videos/'.$args[2].'.html');
        views_count();

        return;
    }
    // �������� �����
    if (preg_match('/^load\/([\w\-()]+).html$/', $query, $args)) {
        $_GET['title_ru_translit'] = $args[1];
        $_GET['id'] = xrest_get_video_id_from_translit($args[1]);
        $config['vars']['page'] = 'load';
        loads_count();

        return;
    }
    // �������� ����� �� ���������
    if (preg_match('/^load\/([\w\-()]+)\/([\w\-()]+).html$/', $query, $args)) {
        $_GET['name_translit'] = $args[1];
        $_GET['title_ru_translit'] = $args[2];
        $ycr = yandex_categories_redirect($args[1]);
        if ($ycr) {
            redirect_301('/load/'.$ycr.'/'.$_GET['title_ru_translit'].'.html');
        }
        $_GET['category_id'] = xrest_get_category_id_from_translit($_GET['name_translit']);
        $_GET['id'] = xrest_get_video_id_from_translit($args[2]);
        $config['vars']['page'] = 'load_category';
        redirect_301('/load/'.$args[2].'.html');
        loads_count();

        return;
    }
    // ����
    if (preg_match('/^games\/$/', $query, $args)) {
        // redirect_301('/index.html');
        $config['vars']['page'] = 'games/all_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^games\/([\d]+)\/$/', $query, $args)) {
        // redirect_301('/index.html');
        $config['vars']['page'] = 'games/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/game-([\w\-();]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'games/play';
        $_GET['game_id'] = get_game_id_from_translit($args[4]);
        games_views_count();

        return;
    }
    //
    // �����
    if (preg_match('/^photos\/$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^photos\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/photo-([\w\-();]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/view_gallery';
        $_GET['gallery_id'] = get_gallery_id_from_translit($args[4]);
        gallery_views_count();

        return;
    }
    //
    // ������ ����� ���������
    if (preg_match('/^([\w\-()]+)\/$/', $query, $args)) {
        $_GET['name_translit'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;
        $ycr = yandex_categories_redirect($_GET['name_translit']);
        if ($ycr) {
            redirect_301('/'.$ycr.'/');
        }
        $_GET['category_id'] = xrest_get_category_id_from_translit($args[1]);
        $config['vars']['page'] = 'categories_videos_main';

        return;
    }
    // ������ ����� ��������� � ����� ��������
    if (preg_match('/^([\w\-()]+)\/([\d]+).html$/', $query, $args)) {
        $_GET['name_translit'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = $args[2];
        $ycr = yandex_categories_redirect($_GET['name_translit']);
        if ($ycr) {
            redirect_301('/'.$ycr.'/'.$_GET['p'].'.html');
        }
        $_GET['category_id'] = xrest_get_category_id_from_translit($args[1]);
        $config['vars']['page'] = 'categories_videos';

        return;
    }
    // ��������
    // if (preg_match('/^book.html$/',$query,$args)) {
    // 	redirect_301('/guestbook');
    // };
    if (preg_match('/^(index.htm|index.html)$/', $query, $args)) {
        $config['vars']['page'] = '404_lite';
        // die('�������� �������������');
        // $config['vars']['page']='index';
        // redirect_301('/');
    }
    if (preg_match('/^rss.php$/', $query, $args)) {
        redirect_301('/rss');
    }
    if (preg_match('/^download\.php$/', $query, $args)) {
        if (empty($_GET['title_translit'])) {
            error('�� �������� �������� �����');
        }
        $start = '';
        if (! empty($_GET['start'])) {
            $start = '?start='.$_GET['start'];
        }
        redirect_301(media_for_video(xrest_get_video_id_from_translit($_GET['title_translit']), 'video_save_link').$start);
    }
    if (preg_match('/^download\/([\w\-();]+)\.flv$/', $query, $args)) {
        $start = '';
        // if (!empty($_GET['start'])) {$start='?start='.$_GET['start'];};
        // redirect_301(media_for_video(xrest_get_video_id_from_translit($args[1]),'video_save_link').$start);
        $_GET['id'] = xrest_get_video_id_from_translit($args[1]);
        redirect_to_video_direct_link();
    }
    if (preg_match('/^download\/([\w\-()]+)\/([\w\-();]+)\.flv$/', $query, $args)) {
        $start = '';
        if (! empty($_GET['start'])) {
            $start = '?start='.$_GET['start'];
        }
        redirect_301(media_for_video(xrest_get_video_id_from_translit($args[2]), 'video_save_link').$start);
    }
    /*
    if (preg_match('/^img\/([\d]+)\.jpg$/',$query,$args)) {
        $data=query('select id from '.tabname('videohub','videos').' where domain=\'xrest.net\' and id_hub=\''.$args[1].'\' limit 1;');
        $arr=mysql_fetch_assoc($data);
        if (empty($arr['id'])) {error('����������� �����������');};
        redirect_301('http://images.xrest.net/'.$arr['id'].'.jpg');
    };
    if (preg_match('/^img\/([\w-()]+)\/([\d]+)\.jpg$/',$query,$args)) {
        $data=query('select id from '.tabname('videohub','videos').' where domain=\'xrest.net\' and id_hub=\''.$args[2].'\' limit 1;');
        $arr=mysql_fetch_assoc($data);
        if (empty($arr['id'])) {error('����������� �����������');};
        redirect_301('http://images.xrest.net/'.$arr['id'].'.jpg');
    };
    */
    // �������� � ��������� �������, �� ������� ������������ �����, ����� �� ����� ��� �������
    if (preg_match('/^(.*)\/N\/A(.*)$/', $query, $args) or preg_match('/^(.*)\/null(.*)$/', $query, $args) or preg_match('/^(.*)undefined$/', $query, $args)) {
        if (! empty($_SERVER['HTTP_REFERER'])) {
            $url = $_SERVER['HTTP_REFERER'];
        }
        if (! empty($url)) {
            if (strpos($url, 'N/A') or strpos($url, 'null') or strpos($url, 'undefined')) {
                logger()->info('������� �������� N/A,null,undefined');
            }
            if (strpos($url, 'xrest.net') & ! strpos($url, 'N/A') & ! strpos($url, 'null') & ! strpos($url, 'undefined')) {
                // logs('�������� � N/A,null,undefined �� '.$url);
                header('Location: '.$url);
            }
        }
        // logs('�������� � N/A,null,undefined �� /');
        header('Location: /');
        exit();
    }
    if (preg_match('/^player\/(.*)$/', $query, $args)) {
        redirect_301('/template/player/xrest.net/'.$args[1]);
    }
}

function yandex_categories_redirect($translit)
{
    $categories = [
        'Oralnyj_sex' => 'Oralnyj_seks',
        'Analnyj_sex' => 'Analnyj_seks',
        'Bryunetki' => 'Porno_s_bryunetkami',
        'Aziatki' => 'Porno_s_aziatkami',
        'Studentki' => 'Porno_so_shkolnicami_(studentkami)',
        'Gruppovoj_sex' => 'Gruppovoj_seks',
        'Chuzhie_zheny' => 'Porno_s_chuzhimi_zhenami',
        'Nizhnee_belje' => 'Nizhnee_bele',
        'Molodenkie' => 'Porno_s_molodenkimi',
        'Bikini' => 'Porno_v_bikini',
        'Latinki' => 'Porno_s_latinki',
        'Uchilki' => 'Porno_s_uchilkami',
        'Chernye' => 'Porno_s_chernymi',
        'Pirsing' => 'Porno_s_pirsingom',
        'Blondinki' => 'Porno_s_blondinkami',
        'Porno_so_shkolnicami_studentkami' => 'Porno_so_shkolnicami_(studentkami)',
    ];
    if (isset($categories[$translit])) {
        return $categories[$translit];
    } else {
        return false;
    }
}

// ���������� ������ ��������� ��� ������.
function xrest_categories_list()
{
    global $config;
    $data = query('
			select categories_domains.category_id,categories_domains.id,categories_groups.name,categories_groups.name_translit
			from '.tabname('videohub', 'categories_domains').'
			inner join '.tabname('videohub', 'categories_groups').'  on categories_domains.category_id=categories_groups.id
			where domain=\''.$config['domain'].'\' order by name');
    $arr = readdata($data, 'nokey');
    $print = '';
    foreach ($arr as $v) {
        // ��� �� categories_videos
        // ������ �� ��������� ����� ����������� � ������ ���������
        $data = query('
SELECT catgroups_tags.tag_ru
FROM '.tabname('videohub', 'categories_domains').'
INNER JOIN  '.tabname('videohub', 'categories_groups').' ON categories_groups.id = categories_domains.category_id
INNER JOIN  '.tabname('videohub', 'catgroups_tags').' ON catgroups_tags.group_id = categories_groups.group_id
WHERE categories_domains.id=\''.$v['id'].'\' and categories_domains.domain=\''.$config['domain'].'\'
				');
        $arr2 = readdata($data, 'nokey');
        // ��������� ������ ������ �����
        $cat_tags = [];
        foreach ($arr2 as $v2) {
            $cat_tags[] = $v2['tag_ru'];
        }
        if (empty($cat_tags)) {
            error('��������� ����������', '��� ��������� '.$v['id'].' ��� �����, ���� ������� ���������, �� ����������� � ������� �����', 1, 404);
        }
        // ������ �� ��������� ���������� ���� ���������� ID, ��������������� �������
        $data = query('
SELECT count(distinct tags.video_id) as q_videos
FROM '.tabname('videohub', 'tags').'
inner join '.tabname('videohub', 'tags_translation').' on tags.tag_en=tags_translation.tag_en
inner join '.tabname('videohub', 'videos').' on videos.id=tags.video_id
WHERE domain=\''.$config['domain'].'\' and tag_ru in ('.array_to_str($cat_tags).')
				');
        $arr2 = mysql_fetch_array($data);
        $rows = $arr2[0]; // ����� �������
        // *���
        $print .= '<li><span><a href="/'.$v['name_translit'].'/" title="'.$v['name'].'">'.$v['name'].'</a> ('.$rows.')</span></li>'."\r\n";
    }

    return $print;
}

// �������� id_hub ���� 000/000/000 ��� ���������� ��������
// �������� ������ ��� ����� �� ��������� - ��� �� ��������
function xrest_cat_thumbs_from_array($arr)
{
    global $config;
    $list = '';
    if (empty($_GET['name_translit'])) {
        error('�� ������� �������� ���������');
    }
    if (empty($arr)) {
        // alert('�� ������ �� ������ �����');
        $list = '��� ����������� ��� �����������.';
        logger()->info('�� ������� �� ������ �����');
    }
    // pre($arr);
    foreach ($arr as $v) {
        $date_arr = my_date($v['date']);
        $date = $date_arr['day'].' '.$date_arr['month'].' '.$date_arr['year'];
        temp_var('date', $date);
        temp_var('id', $v['id']);
        temp_var('title_ru', $v['title_ru']);
        temp_var('title_ru_translit', $v['title_ru_translit']);
        temp_var('views', $v['views']);
        temp_var('rating', $v['rating']);
        temp_var('votes', $v['votes']);
        temp_var('category_name_translit', $_GET['name_translit']);
        temp_var('id_hub', $v['id_hub']);
        $list .= template('li/thumbs_category');
    }

    return $list;
}

function xrest_categories_videos()
{
    global $config;
    $arr = func_replacement('preparing_categories_videos');

    // pre($arr);
    return xrest_cat_thumbs_from_array($arr['videos']);
}

function xrest_pages_search_videos()
{
    $arr = func_replacement('preparing_search_videos');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    $list = page_numbers($rows, $per_page, $_GET['p'], '/search/'.get_query_urlencode().'/'.$_GET['sort'].'/', $pages_length);

    return $list;
}

function xrest_pages_tags_videos()
{
    $arr = func_replacement('preparing_tags_videos');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['p'] > $pages) {
        header('Location: /index.php?action=tags&tag='.urlencode($_GET['tag_ru']).'&page='.$pages);
        exit();
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/index.php?action=tags&tag='.urlencode($_GET['tag_ru']).'&page=', $pages_length);

    return $list;
}

function xrest_pages_categories_videos()
{
    $arr = func_replacement('preparing_categories_videos');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['p'] > $pages) {
        header('Location: /'.$arr['category_name_translit'].'/'.$pages.'.html');
        exit();
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/'.$arr['category_name_translit'].'/', $pages_length, '.html');

    return $list;
}

// ����� ������ ������� ��� �������� video_collection
function xrest_pages_video_collection()
{
    $arr = func_replacement('preparing_video_collection');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['p'] > $pages) {
        header('Location: /'.$pages.'.html');
        exit();
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/', $pages_length, '.html');

    return $list;
}

function xrest_get_video_id_from_translit($translit)
{
    $data = query('select id from '.tabname('videohub', 'videos').' where domain=\'xrest.net\' and title_ru_translit=\''.$translit.'\';');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('����� �� �������');
    }

    return $arr['id'];
}

function xrest_get_category_id_from_translit($translit)
{
    $data = query('
		select categories_domains.id from '.tabname('videohub', 'categories_groups').'
		join '.tabname('videohub', 'categories_domains').' on categories_domains.category_id=categories_groups.id
		where domain=\'xrest.net\' and name_translit=\''.$translit.'\';
		');
    $arr = mysql_fetch_assoc($data);
    if (empty($arr['id'])) {
        error('��������� �� �������');
    }

    return $arr['id'];
}

function rewrite_xrest_tags_url()
{
    global $config;
    if (empty($_GET['tag'])) {
        error('�� �������� ����������� ���������');
    }
    if (empty($_GET['p'])) {
        $redirect_p = '';
        $_GET['p'] = 1;
    } else {
        $redirect_p = '&p='.$_GET['p'];
    }
    $_GET['tag_ru'] = $_GET['tag'];
    $_GET['sort'] = 'mr';
    $config['vars']['page'] = 'tags_videos';
    // ������ �����
    $data = query('select tag_ru_xrest from '.tabname('xrest_net_changed', 'tags_changed').' where tag_ru_videohub=\''.$_GET['tag_ru'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (! empty($arr['tag_ru_xrest'])) {
        $_GET['tag_ru'] = $arr['tag_ru_xrest'];

        return;
    }
    // ���� �������� ��� �� ����������
    $data = query('select tag_ru from '.tabname('videohub', 'tags_translation').' where tag_en=\''.$_GET['tag_ru'].'\';');
    $arr = mysql_fetch_assoc($data);
    if (! empty($arr['tag_ru'])) {
        redirect_301('/index.php?action=tags&tag='.urlencode($arr['tag_ru']).$redirect_p);
    }

}
