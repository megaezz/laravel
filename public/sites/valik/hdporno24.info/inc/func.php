<?php

namespace MegawebV1;

function hdporno24_index_vars()
{
    $_GET['p'] = 1;
    $_GET['sort'] = 'mr';
    $_GET['get_sort'] = 'most_recent';
}

function hdporno24_pages_videos_list_category()
{
    return videohub_pages_list([
        'type' => 'category',
        'p' => empty($_GET['p']) ? error('�� ����� ����� ��������') : $_GET['p'],
        'value' => empty($_GET['category_id']) ? error('�� ����� ID ���������') : $_GET['category_id'],
        'sort' => empty($_GET['sort']) ? error('�� ������ ����������') : $_GET['sort'],
        'link' => '/'.category_name_translit().'/'.$_GET['sort'].'/',
    ]);
}

function hdporno24_pages_videos_list_all()
{
    return videohub_pages_list([
        'type' => 'all',
        'p' => empty($_GET['p']) ? error('�� ����� ����� ��������') : $_GET['p'],
        'value' => 'value',
        'sort' => empty($_GET['sort']) ? error('�� ������ ����������') : $_GET['sort'],
        'link' => '/all/'.$_GET['sort'].'/',
    ]);

    return $list;
}

function pornoruso_rnd_class($tag)
{
    $arr = ['default', 'primary', 'success', 'info', 'warning', 'danger'];

    return rnd_from_array($arr);
}

function valik_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    // $query=$_GET['query'];
    $query = iconv('utf-8', 'windows-1251', $_GET['query']) or error('������ ���������', '������ ��� ����� ��������� ��� "'.$_GET['query'].'"');
    // ����� ��� ����
    if (preg_match('/^tag\/([�-��-߸�a-zA-Z0-9_-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'tags_videos';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['tag_ru'] = with_spaces($args[1]);

        return;
    }
    // ������ �����
    if (preg_match('/^tags$/', $query, $args)) {
        $config['vars']['page'] = 'all_tags_list';

        return;
    }
    if (preg_match('/^top$/', $query, $args)) {
        $config['vars']['page'] = 'top';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        // pre($_GET);
        return;
    }
    // ������
    if (preg_match('/^stars$/', $query, $args)) {
        $config['vars']['page'] = 'stars';

        return;
    }
    // ����� ��� ������
    if (preg_match('/^stars\/video\/([�-��-߸�a-zA-Z0-9_-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'star_videos_main';
        // pre($args);
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_name_url($args[1]);

        return;
    }
    // �������� �������� �����
    if (preg_match('/^load-video\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'load';
        $_GET['id'] = get_video_id_from_local_id($args[1]);
        loads_count();

        return;
    }
    // �������� �� ������ ������
    if (preg_match('/^get-video\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = get_video_id_from_local_id($args[2]);

        return;
    }
    if (preg_match('/^feedback$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    // ����� �����
    if (preg_match('/^sitemap.xml$/', $query, $args)) {
        $config['vars']['page'] = 'sitemap';

        return;
    }
    // �������
    if (preg_match('/^news\/$/', $query, $args)) {
        $config['vars']['page'] = 'site_news/page';

        return;
    }
    if (preg_match('/^glossary$/', $query, $args)) {
        $config['vars']['page'] = 'glossary';

        return;
    }
    if (preg_match('/^faq$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^info$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^rules$/', $query, $args)) {
        $config['vars']['page'] = 'terms_of_service';

        return;
    }
    if (preg_match('/^parents$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    // ����� ��� ������
    if (preg_match('/^search$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    // ����� ��� ������ ���������
    if (preg_match('/^search\/([�-��-߸�a-zA-Z0-9_-]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = $args[2];
        $_GET['sort'] = 'mv';

        return;
    }
    // �������� �����
    if (preg_match('/^video\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        // pre($args);
        $_GET['id'] = get_video_id_from_local_id($args[1]);
        views_count();

        return;
    }
    // ��������
    if (preg_match('/^index.html$/', $query, $args)) {
        // $config['vars']['page']='contact';
        redirect_301('/');

        return;
    }
    // ������ ���������
    if (preg_match('/^articles$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    // �������� ��������
    if (preg_match('/^articles\/read\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_local_id($args[1]);
        story_views_count();

        return;
    }

    // ���������� � ��������� ������� ��������
    if (preg_match('/^all\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['sort'] = $args[1];
        $_GET['p'] = $args[2];

        return;
    }
    // ���������� � ��������� ��� ���������
    if (preg_match('/^([\w]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['p'] = $args[3];
        $_GET['sort'] = $args[2];
        $_GET['category_id'] = get_category_id_from_translit($args[1]);

        return;
    }
    // ����� ��� ���������
    if (preg_match('/^([\w]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);

        return;
    }

}
function hdporno24_menu_active($section)
{
    global $config;
    $active = 0;
    $section_pages['index'] = ['index'];
    $section_pages['about'] = ['about'];
    $section_pages['feedback'] = ['contact'];
    $section_pages['top'] = ['top'];
    $section_pages['stars'] = ['stars'];
    $section_pages['news'] = ['site_news/page'];
    // $section_pages['videos']=array('index','collection','collection_main','categories_videos','categories_videos_main','video');
    // $section_pages['photos']=array('gallery/all','gallery/all_main','gallery/view_gallery','gallery/category','gallery/category_main');
    $section_pages['articles'] = ['stories/all', 'stories/all_main', 'stories/read'];
    // $section_pages['games']=array('games/all','games/all_main','games/play');
    // $section_pages['chat']=array('chat');
    if (isset($section_pages[$section]) == 0) {
        error('������ �� ������������', '������ '.$section.' �� ������������');
    }
    foreach ($section_pages[$section] as $page) {
        if ($page == $config['vars']['page']) {
            $active = 1;
        }
    }
    if ($active == 0) {
        return '';
    }
    if ($active == 1) {
        return ' id="active"';
    }
}
