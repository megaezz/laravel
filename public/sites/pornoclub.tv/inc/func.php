<?php

namespace MegawebV1;

function pornoclub_pages_videos_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    // $arr=preparing_videos_list($args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/videos/all/'.$_GET['get_sort'].'/', $args['pages_length'], '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornoclub_videos_list_same()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'same';
    $args['value'] = $_GET['id'];
    $args['page'] = 1;
    $args['per_page'] = value_from_config_for_domain('videohub', 'number_of_same_videos');
    $args['sort'] = 'ms';
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);

    return thumbs_from_array($arr['videos'], 'li/thumbs_same');
}

function pornoclub_pages_games_list_all()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'games_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_games_list', $args);
    $rows = $arr['rows'];
    // pre($rows);
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/games/all/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornoclub_index_vars()
{
    $_GET['p'] = 1;
    $_GET['sort'] = 'mr';
    $_GET['get_sort'] = 'most_recent';
}

function pornoclub_games_list_index()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = 1;
    $args['per_page'] = 8;
    $args['sort'] = 'mr';
    $args['mobile'] = 'no';
    $arr = cache('preparing_games_list', $args);

    return games_thumbs_from_array($arr['games']);
}

function pornoclub_videos_list_index()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = 1;
    $args['per_page'] = 8;
    $args['sort'] = 'mr';
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);

    return thumbs_from_array($arr['videos']);
}

function pornoclub_stories_list_index()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = 1;
    $args['per_page'] = 4;
    $args['sort'] = 'mr';
    $arr = cache('preparing_stories_list', $args);

    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],10,'mr');
    return story_thumbs_from_array($arr['stories']);
}

function pornoclub_galleries_list_index()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = 1;
    $args['per_page'] = 6;
    $args['sort'] = 'mr';
    $arr = cache('preparing_galleries_list', $args);

    // $arr=preparing_galleries_list($config['domain'],'all','value',$_GET['p'],20,'mr');
    return gallery_thumbs_from_array($arr['galleries']);
}

function pornoclub_menu_is_active($section)
{
    global $config;
    $active = 0;
    $section_pages['videos'] = ['collection', 'collection_main', 'categories_videos', 'video'];
    $section_pages['gallery'] = ['gallery/all', 'gallery/all_main', 'gallery/view_gallery'];
    $section_pages['stories'] = ['stories/all', 'stories/all_main', 'stories/read'];
    $section_pages['films'] = [''];
    $section_pages['games'] = ['games/all', 'games/all_main', 'games/play'];
    if (isset($section_pages[$section]) == 0) {
        error('������ �� ������������');
    }
    if ($section == 'films' and $config['vars']['page'] == 'categories_videos') {
        if ($_GET['name_translit'] == 'Porno_filmy') {
            $active = 1;
        }
    }
    foreach ($section_pages[$section] as $page) {
        if ($page == $config['vars']['page']) {
            $active = 1;
        }
    }
    if ($active == 0) {
        return '';
    }
    if ($active == 1) {
        return 'class="active"';
    }
}

function pornoclub_pages_categories_videos_alt()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    $args['page'] = (int) $_GET['p'];
    $args['sort'] = $_GET['sort'];
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    // $arr=preparing_videos_list($args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    // $list=page_numbers($rows,$args['per_page'],$_GET['p'],'/videos/all/'.$_GET['get_sort'].'/',$args['pages_length'],'','&laquo;','&raquo;','yes','active','active','1');
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/'.category_name_translit().'/'.$_GET['get_sort'].'/', $args['pages_length'], '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornoclub_pages_categories_videos()
{
    $arr = func_replacement('preparing_categories_videos');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    $list = page_numbers($rows, $per_page, $_GET['p'], '/videos/'.$_GET['name_translit'].'/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornoclub_pages_stories_list_all()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'stories_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_stories_list', $args);
    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows = $arr['rows']; // ����� �������
    $pages = ceil($rows / $args['per_page']); // �������
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/stories/all/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornoclub_pages_galleries_list_category()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'galleries_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_galleries_list', $args);
    // $arr=preparing_galleries_list($config['domain'],'category',$_GET['category_id'],$_GET['p'],$per_page,'mr');
    $rows = $arr['rows']; // ����� �������
    $pages = ceil($rows / $args['per_page']); // �������
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/photos/'.$_GET['name_translit'].'/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornoclub_pages_galleries_list_all()
{
    global $config;
    $pages_length = 3;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'galleries_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_galleries_list', $args);
    // $arr=preparing_galleries_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows = $arr['rows']; // ����� �������
    $pages = ceil($rows / $args['per_page']); // �������
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/photos/all/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornoclub_pages_video_collection()
{
    $arr = func_replacement('preparing_video_collection');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    $list = page_numbers($rows, $per_page, $_GET['p'], '/videos/all/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornoclub_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^faq\.html$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^parents\.html$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    if (preg_match('/^pornoclub\.html$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^save\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = $args[2];

        return;
    }
    if (preg_match('/^videos\/watch\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['id'] = $args[1];

        return;
    }
    if (preg_match('/^videos\/load\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'load';
        $_GET['id'] = $args[1];

        return;
    }
    if (preg_match('/^videos\/all\/(most_viewed|most_recent)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^videos\/$/', $query, $args)) {
        $config['vars']['page'] = 'collection_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^videos\/all\/(most_viewed|most_recent)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^videos\/([\w\-]+)\/(most_viewed|most_recent)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = $args[2];
        if ($args[2] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^videos\/([\w\-]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos_main';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^videos\/([\w\-]+)\/(most_viewed|most_recent)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = $args[2];
        if ($args[2] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^photos\/watch\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/view_gallery';
        $_GET['gallery_id'] = $args[1];

        return;
    }
    if (preg_match('/^photos\/all\/(most_viewed|most_recent)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^photos\/$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^photos\/all\/(most_viewed|most_recent)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^photos\/([\w\-]+)\/(most_viewed|most_recent)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/category';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = $args[2];
        if ($args[2] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^photos\/([\w\-]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/category_main';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^photos\/([\w\-]+)\/(most_viewed|most_recent)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/category';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = $args[2];
        if ($args[2] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^stories\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^stories\/read\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = $args[1];

        return;
    }
    if (preg_match('/^stories\/all\/(most_viewed|most_recent)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^stories\/all\/(most_viewed|most_recent)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^save\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = $args[2];

        return;
    }
    if (preg_match('/^games\/$/', $query, $args)) {
        $config['vars']['page'] = 'games/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^games\/play\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'games/play';
        $_GET['game_id'] = $args[1];

        return;
    }
    if (preg_match('/^games\/all\/(most_viewed|most_recent)$/', $query, $args)) {
        $config['vars']['page'] = 'games/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^games\/all\/(most_viewed|most_recent)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'games/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^seolinks$/', $query, $args)) {
        $config['vars']['page'] = 'links';

        return;
    }
}
