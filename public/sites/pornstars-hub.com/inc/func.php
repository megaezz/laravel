<?php

namespace MegawebV1;

function pornstarshub_menu_active($section)
{
    global $config;
    $active = 0;
    $section_pages['videos'] = ['index', 'collection', 'collection_main', 'categories_videos', 'categories_videos_main', 'video'];
    $section_pages['photos'] = ['gallery/all', 'gallery/all_main', 'gallery/view_gallery', 'gallery/category', 'gallery/category_main'];
    $section_pages['articles'] = ['stories/all', 'stories/all_main', 'stories/read'];
    // $section_pages['games']=array('games/all','games/all_main','games/play');
    // $section_pages['chat']=array('chat');
    if (isset($section_pages[$section]) == 0) {
        error('Section is not available', '������ '.$section.' �� ������������');
    }
    foreach ($section_pages[$section] as $page) {
        if ($page == $config['vars']['page']) {
            $active = 1;
        }
    }
    if ($active == 0) {
        return '';
    }
    if ($active == 1) {
        return ' class="active"';
    }
}

function pornstarshub_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    // $query=$_GET['query'];
    $query = iconv('utf-8', 'windows-1251', $_GET['query']) or error('������ ���������', '������ ��� ����� ��������� ��� "'.$_GET['query'].'"');
    // ������ �����
    if (preg_match('/^tags$/', $query, $args)) {
        $config['vars']['page'] = 'all_tags_list';

        return;
    }
    // �������� �������� �����
    if (preg_match('/^load-video\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'load';
        $_GET['id'] = get_video_id_from_local_id($args[1]);
        loads_count();

        return;
    }
    // �������� �� ������ ������
    if (preg_match('/^get-video\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = get_video_id_from_local_id($args[2]);

        return;
    }
    if (preg_match('/^f$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^2257$/', $query, $args)) {
        $config['vars']['page'] = '2257';

        return;
    }
    if (preg_match('/^q$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^pp$/', $query, $args)) {
        $config['vars']['page'] = 'privacy_policy';

        return;
    }
    if (preg_match('/^tos$/', $query, $args)) {
        $config['vars']['page'] = 'terms_of_service';

        return;
    }
    // //������ �����
    // if (preg_match('/^videos$/',$query,$args)) {
    // 	$config['vars']['page']='index';
    // 	return;
    // };
    // ���������� ���� �����
    if (preg_match('/^videos\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['p'] = $args[2];
        $_GET['sort'] = pornstarshub_get_sort($args[1], 'short_from_full');

        return;
    }
    // ����� ��� ���������
    if (preg_match('/^videos\/([\w]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_name_url($args[1]);

        return;
    }
    // ���������� ����� ��� ���������
    if (preg_match('/^videos\/([\w]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['p'] = $args[3];
        $_GET['sort'] = pornstarshub_get_sort($args[2], 'short_from_full');
        $_GET['category_id'] = get_category_id_from_name_url($args[1]);

        return;
    }
    // ����� ��� ����
    if (preg_match('/^videos\/tag\/([\w\-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'tags_videos';
        // pre($args);
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['tag_en'] = with_spaces($args[1]);

        // pre($_GET);
        return;
    }
    // ���������� ����� ��� ����
    if (preg_match('/^videos\/tag\/([\w\-]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'tags_videos';
        $_GET['p'] = $args[3];
        $_GET['sort'] = pornstarshub_get_sort($args[2], 'short_from_full');
        $_GET['tag_en'] = with_spaces($args[1]);

        return;
    }
    // ����� ��� ������
    if (preg_match('/^videos\/star\/([\w]+)$/', $query, $args)) {
        $config['vars']['page'] = 'star_videos_main';
        // pre($args);
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_name_url($args[1]);

        // �������� �� ������������� ������
        // preparing_star_info(array('name_ru'=>$_GET['tag_ru']));
        return;
    }
    // ���������� ����� ��� ������
    if (preg_match('/^videos\/star\/([\w]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'star_videos';
        $_GET['p'] = $args[3];
        $_GET['sort'] = pornstarshub_get_sort($args[2], 'short_from_full');
        $_GET['category_id'] = get_category_id_from_name_url($args[1]);

        // �������� �� ������������� ������
        // preparing_star_info(array('name_ru'=>$_GET['tag_ru']));
        return;
    }
    // ����� ��� ������
    if (preg_match('/^search$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    // ����� ��� ������ ���������
    if (preg_match('/^search\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = $args[2];
        $_GET['sort'] = 'mv';

        return;
    }
    // �������� �����
    if (preg_match('/^video\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['id'] = get_video_id_from_local_id($args[1]);
        views_count();

        return;
    }
    // ������ ���������
    if (preg_match('/^entries$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    // ���������� ��� ���������
    if (preg_match('/^entries\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['p'] = $args[2];
        $_GET['sort'] = pornstarshub_get_sort($args[1], 'short_from_full');

        return;
    }
    // �������� ��������
    if (preg_match('/^entry\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_local_id($args[1]);
        story_views_count();

        return;
    }
    /*
    // ������
    if (preg_match('/^sex-texts\/$/',$query,$args)) {
        $config['vars']['page']='stories/all_main';
        $_GET['get_sort']='most_recent';
        $_GET['sort']='mr';
        $_GET['p']=1;
        return;
    };
    if (preg_match('/^sex-texts\/page\/([\d]+)\/$/',$query,$args)) {
        $config['vars']['page']='stories/all';
        $_GET['p']=$args[1];
        $_GET['sort']='mr';
        return;
    };
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/sex-text\/([\w]+)\.html$/',$query,$args)) {
        $config['vars']['page']='stories/read';
        $_GET['story_id']=get_story_id_from_translit($args[4]);
        story_views_count();
        return;
    };
    // //������
    // �������� �����
    if (preg_match('/^video\/([\d]+)$/',$query,$args)) {
        $config['vars']['page']='video';
        $_GET['id']=get_video_id_from_local_id($args[1]);
        views_count();
        return;
    };
    if (preg_match('/^search$/',$query,$args)) {
        $config['vars']['page']='search_videos';
        $_GET['p']=1;
        $_GET['sort']='mv';
        return;
    };
    if (preg_match('/^search\/([^\/]+)\/$/',$query,$args)) {
        $config['vars']['page']='search_videos';
        $_REQUEST['search_query']=$args[1];
        $_GET['p']=1;
        $_GET['sort']='mv';
        return;
    };
    if (preg_match('/^search\/([^\/]+)\/([\d]+)\/$/',$query,$args)) {
        $config['vars']['page']='search_videos';
        $_REQUEST['search_query']=$args[1];
        $_GET['p']=$args[2];
        $_GET['sort']='mv';
        return;
    };
    // ����� ��� ���������
    if (preg_match('/^([\w]+)\/?$/',$query,$args)) {
        $config['vars']['page']='categories_videos_main';
        $_GET['p']=1;
        $_GET['sort']='mr';
        $_GET['category_id']=get_category_id_from_title_translit($args[1]);
        return;
    };
    if (preg_match('/^([\w]+)\/([\d]+)\/$/',$query,$args)) {
        $config['vars']['page']='categories_videos';
        $_GET['p']=$args[2];
        $_GET['sort']='mr';
        $_GET['category_id']=get_category_id_from_title_translit($args[1]);
        return;
    };
    */
}

function pornstarshub_get_sort($sort, $type)
{
    $arr = ['mr' => 'new', 'mv' => 'popular', 'mc' => 'commented', 'tr' => 'rated', 'tf' => 'favorite'];
    if ($type == 'full_from_short') {
        if (isset($arr[$sort])) {
            return $arr[$sort];
        }
    }
    if ($type == 'short_from_full') {
        foreach ($arr as $k => $v) {
            if ($v == $sort) {
                return $k;
            }
        }
    }
    error('Sorting is not exist');
}

function pornstarshub_pages_videos_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/'.pornstarshub_get_sort($args['sort'], 'full_from_short').'/', $args['pages_length'], '', 'Back', 'Next', 'yes', 'active', 'active', '1');

    return $list;
}

function pornstarshub_pages_videos_list_category()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/'.category_name_url().'/'.pornstarshub_get_sort($args['sort'], 'full_from_short').'/', $args['pages_length'], '', 'Back', 'Next', 'yes', 'active', 'active', '1');

    return $list;
}

function pornstarshub_pages_videos_list_category_star()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/star/'.category_name_url().'/'.pornstarshub_get_sort($args['sort'], 'full_from_short').'/', $args['pages_length'], '', 'Back', 'Next', 'yes', 'active', 'active', '1');

    return $list;
}

function pornstarshub_pages_videos_list_star()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'tag';
    $args['value'] = $_GET['tag_ru'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/star/'.tag_ru_url().'/'.pornstarshub_get_sort($args['sort'], 'full_from_short').'/', $args['pages_length'], '', 'Back', 'Next', 'yes', 'active', 'active', '1');

    return $list;
}

function pornstarshub_pages_videos_list_search()
{
    global $config;
    if (empty($_REQUEST['search_query'])) {
        error('�� ������� ��������� ������');
    }
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'search';
    $args['value'] = $_REQUEST['search_query'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 1;
    }
    $args['sort'] = 'mv';
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/search/'.htmlspecialchars(without_spaces($args['value']), ENT_QUOTES, 'cp1251').'/', $args['pages_length'], '', 'Back', 'Next', 'yes', 'active', 'active', '1');

    return $list;
}

function pornstarshub_pages_videos_list_tag()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'tag_en';
    $args['value'] = $_GET['tag_en'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/tag/'.without_spaces($args['value']).'/'.pornstarshub_get_sort($args['sort'], 'full_from_short').'/', $args['pages_length'], '', 'Back', 'Next', 'yes', 'active', 'active', '1');

    return $list;
}
