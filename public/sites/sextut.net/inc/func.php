<?php

namespace MegawebV1;

function sextut_pages_videos_list_search()
{
    global $config;
    if (empty($_REQUEST['search_query'])) {
        error('�� ������� ��������� ������');
    }
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'search';
    $args['value'] = $_REQUEST['search_query'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    $args['sort'] = 'mv';
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/search/'.urlencode($args['value']).'/', $args['pages_length'], '/', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function sextut_sitemap()
{
    global $config;
    $urls = '';
    $urls .= '<url><loc>http://'.$config['domain'].'/index.html</loc><priority>1.000</priority></url>'."\r\n";
    $urls .= '<url><loc>http://'.$config['domain'].'/videos/</loc><priority>0.900</priority></url>'."\r\n";
    $urls .= '<url><loc>http://'.$config['domain'].'/photos/</loc><priority>0.900</priority></url>'."\r\n";
    $urls .= '<url><loc>http://'.$config['domain'].'/articles/</loc><priority>0.900</priority></url>'."\r\n";
    $urls .= '<url><loc>http://'.$config['domain'].'/sextut.html</loc><priority>0.700</priority></url>'."\r\n";
    $urls .= '<url><loc>http://'.$config['domain'].'/faq.html</loc><priority>0.700</priority></url>'."\r\n";
    $urls .= '<url><loc>http://'.$config['domain'].'/for-parents.html</loc><priority>0.700</priority></url>'."\r\n";
    $urls .= '<url><loc>http://'.$config['domain'].'/mail.html</loc><priority>0.700</priority></url>'."\r\n";
    $urls .= '<url><loc>http://'.$config['domain'].'/porno-slova.html</loc><priority>0.700</priority></url>'."\r\n";
    // ������ �����
    $data = query('select id,title_ru_translit,unix_timestamp(apply_date) as date from '.tabname('videohub', 'videos').' where domain=\''.$config['domain'].'\'');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<url><loc>http://'.$config['domain'].'/'.date('Y/m/d', $v['date']).'/video/'.$v['title_ru_translit'].'.html</loc><priority>0.800</priority></url>'."\r\n";
    }
    $urls .= $list;
    // ������ ����
    $data = query('select id,title_ru_translit,unix_timestamp(apply_date) as date from '.tabname('videohub', 'galleries').' where domain=\''.$config['domain'].'\'');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<url><loc>http://'.$config['domain'].'/'.date('Y/m/d', $v['date']).'/photo/'.$v['title_ru_translit'].'.html</loc><priority>0.800</priority></url>'."\r\n";
    }
    $urls .= $list;
    // ������ ������
    $data = query('select title_translit,unix_timestamp(apply_date) as date from '.tabname('videohub', 'stories').' where domain=\''.$config['domain'].'\';');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<url><loc>http://'.$config['domain'].'/'.date('Y/m/d', $v['date']).'/article/'.$v['title_translit'].'.html</loc><priority>0.800</priority></url>'."\r\n";
    }
    $urls .= $list;
    // ������ ��������� �����
    $data = query('
			select categories_domains.id,categories_groups.name_translit,title_translit
			from '.tabname('videohub', 'categories_domains').'
			join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
			where domain=\''.$config['domain'].'\' order by categories_domains.id
			');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<url><loc>http://'.$config['domain'].'/videos/'.$v['name_translit'].'/</loc><priority>0.900</priority></url>'."\r\n";
    }
    $urls .= $list;
    // ������ ��������� ����
    $data = query('
			select categories_domains.id,categories_groups.name_translit,title_translit
			from '.tabname('videohub', 'categories_domains').'
			join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
			left join '.tabname('videohub', 'categories_groups_photos_excluded').' on categories_groups_photos_excluded.group_id=categories_groups.group_id
			where domain=\''.$config['domain'].'\' and categories_groups_photos_excluded.group_id is null
			order by categories_domains.id
			');
    $arr = readdata($data, 'nokey');
    $list = '';
    foreach ($arr as $v) {
        $list .= '<url><loc>http://'.$config['domain'].'/photos/'.$v['name_translit'].'/</loc><priority>0.900</priority></url>'."\r\n";
    }
    $urls .= $list;

    return $urls;
}

function sextut_menu_current($section)
{
    global $config;
    $active = 0;
    $section_pages['index'] = ['index'];
    $section_pages['videos'] = ['collection', 'collection_main', 'categories_videos', 'categories_videos_main', 'video'];
    $section_pages['gallery'] = ['gallery/all', 'gallery/all_main', 'gallery/view_gallery', 'gallery/category', 'gallery/category_main'];
    $section_pages['usefull'] = ['faq', 'glossary', 'parents_control', 'about'];
    $section_pages['mail'] = ['contact'];
    $section_pages['articles'] = ['stories/all', 'stories/all_main', 'stories/read'];
    if (isset($section_pages[$section]) == 0) {
        error('������ �� ������������');
    }
    foreach ($section_pages[$section] as $page) {
        if ($page == $config['vars']['page']) {
            $active = 1;
        }
    }
    if ($active == 0) {
        return '';
    }
    if ($active == 1) {
        return 'class="current"';
    }
}

function sextut_index_vars()
{
    $_GET['p'] = 1;
    $_GET['sort'] = 'mr';
    $_GET['get_sort'] = 'most_recent';
}

function sextut_pages_stories_list_all()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'stories_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_stories_list', $args);
    $rows = $arr['rows']; // ����� �������
    $pages = ceil($rows / $args['per_page']); // �������
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/articles/all/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function sextut_pages_galleries_list_category()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'galleries_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_galleries_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/photos/'.category_name_translit().'/'.$_GET['get_sort'].'/', $args['pages_length'], '', '&laquo; �����', '������ &raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function sextut_pages_galleries_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'galleries_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_galleries_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/photos/all/'.$_GET['get_sort'].'/', $args['pages_length'], '', '&laquo; �����', '������ &raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function sextop_pages_videos_list_category()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/'.category_name_translit().'/'.$_GET['get_sort'].'/', $args['pages_length'], '', '&laquo; �����', '������ &raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function sextut_pages_videos_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/all/'.$_GET['get_sort'].'/', $args['pages_length'], '', '&laquo; �����', '������ &raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function sextut_videos_index()
{
    global $config;
    $content = '';
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = 1;
    $args['sort'] = 'mr';
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args, 43200);
    $content .= '
	<div class="extra-wrap bot-2 tab-content" id="all">
	'.thumbs_from_array($arr['videos']).'
	</div>';
    $data = query('
		select categories_domains.id,categories_groups.name_translit from '.tabname('videohub', 'categories_domains').'
		join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
		where categories_domains.domain=\''.$config['domain'].'\';');
    $arr = readdata($data, 'nokey');
    $args['type'] = 'category';
    foreach ($arr as $v) {
        $args['value'] = $v['id'];
        $arr = cache('preparing_videos_list', $args);
        $content .= '
		<div class="extra-wrap tab-content bot-2" id="'.$v['name_translit'].'">
		'.thumbs_from_array($arr['videos']).'
		</div>';
    }

    return $content;
}

function sextut_categories_list_index()
{
    return categories_list_new(['mobile' => 'no', 'template' => 'li/categories_list_index']);
}

function sextut_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^news\.html$/', $query, $args)) {
        $config['vars']['page'] = 'site_news/page';

        return;
    }
    if (preg_match('/^sitemap.xml$/', $query, $args)) {
        $config['vars']['page'] = 'sitemap';

        return;
    }
    if (preg_match('/^porno-slova\.html$/', $query, $args)) {
        $config['vars']['page'] = 'glossary';

        return;
    }
    if (preg_match('/^sextut\.html$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^faq\.html$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^for-parents\.html$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    if (preg_match('/^mail\.html$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^index.html$/', $query, $args)) {
        $config['vars']['page'] = 'index';
        $_GET['get_sort'] = 'most_recent';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/video\/([\w\-();]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['id'] = get_video_id_from_translit($args[4]);
        views_count();

        return;
    }
    if (preg_match('/^videos\/$/', $query, $args)) {
        $config['vars']['page'] = 'collection_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^videos\/all\/(most_viewed|most_recent|top_rated)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[1] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^videos\/all\/(most_viewed|most_recent|top_rated)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[1] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^videos\/([\w\-]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos_main';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^videos\/([\w\-]+)\/(most_viewed|most_recent|top_rated)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = $args[2];
        if ($args[2] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[2] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^videos\/([\w\-]+)\/(most_viewed|most_recent|top_rated)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = $args[2];
        if ($args[2] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[2] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/photo\/([\w\-();]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/view_gallery';
        $_GET['gallery_id'] = get_gallery_id_from_translit($args[4]);
        gallery_views_count();

        return;
    }
    if (preg_match('/^photos\/all\/(most_viewed|most_recent|top_rated)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[1] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^photos\/$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^photos\/all\/(most_viewed|most_recent|top_rated)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[1] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^photos\/([\w\-]+)\/(most_viewed|most_recent|top_rated)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/category';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = $args[2];
        if ($args[2] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[2] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^photos\/([\w\-]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/category_main';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^photos\/([\w\-]+)\/(most_viewed|most_recent|top_rated)$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/category';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['get_sort'] = $args[2];
        if ($args[2] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[2] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^articles\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/article\/([\w]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_translit($args[4]);
        story_views_count();

        return;
    }
    if (preg_match('/^articles\/all\/(most_viewed|most_recent)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[1] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^articles\/all\/(most_viewed|most_recent)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['get_sort'] = $args[1];
        if ($args[1] == 'most_recent') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'most_viewed') {
            $_GET['sort'] = 'mv';
        }
        if ($args[1] == 'top_rated') {
            $_GET['sort'] = 'tr';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^search$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = $args[2];
        $_GET['sort'] = 'mv';

        return;
    }
}
