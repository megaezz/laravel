<?php

namespace MegawebV1;

function pornoruso_rnd_class($tag)
{
    $arr = ['default', 'primary', 'success', 'info', 'warning', 'danger'];

    return rnd_from_array($arr);
}

function pornoruso_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    // $query=$_GET['query'];
    // $query=iconv('utf-8','windows-1251',$_GET['query']) or error('������ ���������','������ ��� ����� ��������� ��� "'.$_GET['query'].'"');
    $query = mb_convert_encoding($_GET['query'], 'windows-1251', 'utf-8') or error('������ ���������', '������ ��� ����� ��������� ��� "'.$_GET['query'].'"');
    // ����� ��� ����
    if (preg_match('/^tag\/([�-��-߸�a-zA-Z0-9_-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'tags_videos';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['tag_ru'] = with_spaces($args[1]);

        return;
    }
    // ������ �����
    if (preg_match('/^tags$/', $query, $args)) {
        $config['vars']['page'] = 'all_tags_list';

        return;
    }
    if (preg_match('/^top$/', $query, $args)) {
        $config['vars']['page'] = 'top';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        // pre($_GET);
        return;
    }
    // �������� �������� �����
    if (preg_match('/^load-video\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'load';
        $_GET['id'] = get_video_id_from_local_id($args[1]);
        loads_count();

        return;
    }
    // �������� �� ������ ������
    if (preg_match('/^get-video\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = get_video_id_from_local_id($args[2]);

        return;
    }
    if (preg_match('/^contact$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^slovar$/', $query, $args)) {
        $config['vars']['page'] = 'glossary';

        return;
    }
    if (preg_match('/^faq$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^pornoruso$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^roditeli$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    // ����� ��� ������
    if (preg_match('/^search$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    // ����� ��� ������ ���������
    if (preg_match('/^search\/([�-��-߸�a-zA-Z0-9_-]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = $args[2];
        $_GET['sort'] = 'mv';

        return;
    }
    // �������� �����
    if (preg_match('/^video\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['id'] = get_video_id_from_local_id($args[1]);
        views_count();

        return;
    }
    // ������ ���������
    if (preg_match('/^entries$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    // �������� ��������
    if (preg_match('/^entry\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_local_id($args[1]);
        story_views_count();

        return;
    }
    // ����� ��� ���������
    if (preg_match('/^([\w]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_title_translit($args[1]);

        return;
    }
}
