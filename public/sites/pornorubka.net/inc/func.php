<?php

namespace MegawebV1;

function pornorubka_pages_stories_list_all()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'stories_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_stories_list', $args);
    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows = $arr['rows']; // всего записей
    $pages = ceil($rows / $args['per_page']); // страниц
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/articles/page/', $pages_length, '/', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pornorubka_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('Не передан запрос');
    }
    $query = $_GET['query'];
    // статьи
    if (preg_match('/^articles\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^articles\/page\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/article\/([\w]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_translit($args[4]);
        story_views_count();

        return;
    }
    // //статьи
    if (preg_match('/^news$/', $query, $args)) {
        $config['vars']['page'] = 'site_news/page';

        return;
    }
    if (preg_match('/^faq.html$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^parents.html$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    if (preg_match('/^guestbook$/', $query, $args)) {
        $config['vars']['page'] = 'guestbook/page';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^guestbook\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'guestbook/page';
        $_GET['p'] = $args[1];

        return;
    }
    // просмотр видео
    if (preg_match('/^video\/([\d]+)-([\w\-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['id'] = $args[1];
        $_GET['title_ru_translit'] = $args[2];

        return;
    }
    if (preg_match('/^load\/([\d]+)-([\w\-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'load';
        $_GET['id'] = $args[1];
        $_GET['title_ru_translit'] = $args[2];

        return;
    }
    if (preg_match('/^save\/([\w]+)\/([\d]+)-([\w\-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = $args[2];
        $_GET['title_ru_translit'] = $args[3];

        return;
    }
    if (preg_match('/^videos\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['sort'] = $args[1];
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^categories$/', $query, $args)) {
        $config['vars']['page'] = 'categories_list';

        return;
    }
    if (preg_match('/^categories\/([\d]+)-([\w\-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = $args[1];
        $_GET['name_translit'] = $args[2];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^categories\/([\d]+)-([\w\-]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = $args[1];
        $_GET['name_translit'] = $args[2];
        $_GET['sort'] = $args[3];
        $_GET['p'] = $args[4];

        return;
    }
    if (preg_match('/^search$/', $query, $args)) {
        if (empty($_POST['search_query'])) {
            error('Не задан поисковый запрос');
        }
        $config['vars']['page'] = 'submit_search';

        return;
    }
    if (preg_match('/^search\/([^\/]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $args[1] = htmlspecialchars($args[1], ENT_QUOTES, 'cp1251');
        $_GET['search_query'] = $args[1];
        $_REQUEST['search_query'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $args[1] = htmlspecialchars($args[1], ENT_QUOTES, 'cp1251');
        $_GET['search_query'] = $args[1];
        $_REQUEST['search_query'] = $args[1];
        $_GET['sort'] = $args[2];
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^tags$/', $query, $args)) {
        $config['vars']['page'] = 'all_tags_list';

        return;
    }
    if (preg_match('/^tags\/([\W\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'tags_videos';
        $_GET['tag_ru'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^tags\/([\W\d]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'tags_videos';
        $_GET['tag_ru'] = $args[1];
        $_GET['sort'] = $args[2];
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^rss$/', $query, $args)) {
        $config['vars']['page'] = 'rss';

        return;
    }
    if (preg_match('/^about$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^links$/', $query, $args)) {
        $config['vars']['page'] = 'links';

        return;
    }
    if (preg_match('/^glossary$/', $query, $args)) {
        $config['vars']['page'] = 'glossary';

        return;
    }
    if (preg_match('/^agreement$/', $query, $args)) {
        $config['vars']['page'] = 'agreement';

        return;
    }
    if (preg_match('/^contact$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^admin$/', $query, $args)) {
        $config['vars']['page'] = 'admin/index';

        return;
    }
    // для яндекса
    if (preg_match('/^load\/([\d]+)-([\w\-()]+)\/$/', $query, $args)) {
        redirect_301('/load/'.$args[1].'-'.$args[2]);
    }
    if (preg_match('/^categories\/$/', $query, $args)) {
        redirect_301('/categories');
    }
    if (preg_match('/^categories\/([\d]+)-([\w\-()]+)\/$/', $query, $args)) {
        redirect_301('/categories/'.$args[1].'-'.$args[2]);
    }
    if (preg_match('/^tags\/([\W\d]+)\/$/', $query, $args)) {
        redirect_301('/tags/'.$args[1]);
    }
    // редирект со старых адресов страниц
    if (preg_match('/^view\/id([\d]+)$/', $query, $args)) {
        $data = query('select title_ru_translit from '.tabname('videohub', 'videos').' where id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        if (mysql_num_rows($data) == 0) {
            error('Видео не найдено', '', 1, 404);
        }
        redirect_301('/video/'.$args[1].'-'.$arr['title_ru_translit']);
    }
    if (preg_match('/^video\/([\d]+)$/', $query, $args)) {
        $data = query('select title_ru_translit from '.tabname('videohub', 'videos').' where id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        if (mysql_num_rows($data) == 0) {
            error('Видео не найдено', '', 1, 404);
        }
        redirect_301('/video/'.$args[1].'-'.$arr['title_ru_translit']);
    }
    if (preg_match('/^load\/id([\d]+)$/', $query, $args)) {
        $data = query('select title_ru_translit from '.tabname('videohub', 'videos').' where id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        if (mysql_num_rows($data) == 0) {
            error('Видео не найдено', '', 1, 404);
        }
        redirect_301('/load/'.$args[1].'-'.$arr['title_ru_translit']);
    }
    if (preg_match('/^collection\/([\d]+)$/', $query, $args)) {
        redirect_301('/videos/mr/'.$args[1]);
    }
    if (preg_match('/^categories\/([\d]+)$/', $query, $args)) {
        $data = query('select categories_groups.name_translit from '.tabname('videohub', 'categories_domains').'
							join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
							where categories_domains.id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        redirect_301('/categories/'.$args[1].'-'.$arr['name_translit']);
    }
    if (preg_match('/^categories\/([\d]+)\/([\w]+)\/([\d]+)$/', $query, $args)) {
        $data = query('select categories_groups.name_translit from '.tabname('videohub', 'categories_domains').'
							join '.tabname('videohub', 'categories_groups').' on categories_groups.id=categories_domains.category_id
							where categories_domains.id=\''.$args[1].'\'');
        $arr = mysql_fetch_assoc($data);
        redirect_301('/categories/'.$args[1].'-'.$arr['name_translit'].'/'.$args[2].'/'.$args[3]);
    }
    if (preg_match('/^flv\/([\w]+)\/([\d]+).flv$/', $query, $args)) {
        $data = query('select title_ru_translit from '.tabname('videohub', 'videos').' where id=\''.$args[2].'\'');
        $arr = mysql_fetch_assoc($data);
        if (mysql_num_rows($data) == 0) {
            error('Видео не найдено', '', 1, 404);
        }
        $data = query('select property,value from '.tabname('videohub', 'config').' where property=\'videokey_current\'');
        $arr2 = readdata($data, 'property');
        redirect_301('/save/'.$arr2['videokey_current']['value'].'/'.$args[2].'-'.$arr['title_ru_translit']);
    }
    if (preg_match('/^img\/([\d]+).jpg$/', $query, $args)) {
        redirect_301('/types/videohub/images/'.$args[1].'.jpg');
    }
}
