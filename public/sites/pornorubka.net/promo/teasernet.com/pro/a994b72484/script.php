<?php

if (empty($_POST['client_js']) || empty($_POST['apikey'])) {
    exitWithCode(1, 'bad params '
                        .var_export($_REQUEST, true)
                        .' raw: '.file_get_contents('php://input')
    );
}

ini_set('display_errors', 1);

$filename = '/var/www/uer555/data/www/megaweb/sites/pornorubka.net/template/promo/teasernet.com/pro/e1d996.js';
$apikey = 'd8a6aa2747b8250f133fa314813b1172';

if ($_POST['apikey'] != $apikey) {
    exitWithCode(2, 'apikey is wrong');
}

ob_start();
ini_set('display_errors', 1);
$code = get_magic_quotes_gpc() ? stripslashes($_POST['client_js']) : $_POST['client_js'];
$need_len = strlen($code);
$f_res = fopen($filename, 'w+');
if ($f_res) {
    $res = fwrite($f_res, $code);
    if ($res != $need_len) {
        exitWithCode(4, 'write only '.$res.' need '.$need_len);
    }
    fclose($f_res);
} else {
    exitWithCode(3, 'couldn\'t open the file '.$filename.' for writing');
}
$result = ob_get_clean();
if (strlen($result) > 0) {
    exitWithCode(5, "Unknown error: $result");
} else {
    exitWithCode(0, 'ok');
}

function exitWithCode($code, $message)
{
    @ob_end_clean();
    echo 'code:'.$code.';'.$message;
    exit;
}
