<?php

namespace MegawebV1;

function potrahun_pages_games_list_all()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'games_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_games_list', $args);
    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows = $arr['rows']; // ����� �������
    $pages = ceil($rows / $args['per_page']); // �������
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/games/', $pages_length, '/', '�����', '������', 'yes', 'current', 'points', '1');

    return $list;
}

function potrahun_breadcrumb_category_link()
{
    $content = '';
    if (! empty($_GET['category'])) {
        $content = '&raquo; <a href="/'.category_title_translit().'">'.category_name().'</a>';
    } else {
        $content = '&raquo; <a href="/videos/">����� �����</a>';
    }

    return $content;
}

function potrahun_menu_active($section)
{
    global $config;
    $active = 0;
    $section_pages['videos'] = ['collection', 'collection_main', 'categories_videos', 'categories_videos_main', 'video'];
    $section_pages['photos'] = ['gallery/all', 'gallery/all_main', 'gallery/view_gallery', 'gallery/category', 'gallery/category_main'];
    $section_pages['articles'] = ['stories/all', 'stories/all_main', 'stories/read'];
    $section_pages['games'] = ['games/all', 'games/all_main', 'games/play'];
    $section_pages['chat'] = ['chat'];
    if (isset($section_pages[$section]) == 0) {
        error('������ �� ������������', '������ '.$section.' �� ������������');
    }
    foreach ($section_pages[$section] as $page) {
        if ($page == $config['vars']['page']) {
            $active = 1;
        }
    }
    if ($active == 0) {
        return '';
    }
    if ($active == 1) {
        return 'class="active"';
    }
}

function potrahun_pages_videos_list_search()
{
    global $config;
    if (empty($_REQUEST['search_query'])) {
        error('�� ������� ��������� ������');
    }
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'search';
    $args['value'] = $_REQUEST['search_query'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    $args['sort'] = 'mv';
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/search/'.urlencode($args['value']).'/', $args['pages_length'], '/', '�����', '������', 'yes', 'current', 'points', '1');

    return $list;
}

function potrahun_index_vars()
{
    $_GET['p'] = 1;
    $_GET['sort'] = 'mr';
}

function potrahun_pages_galleries_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'galleries_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_galleries_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/photos/', $args['pages_length'], '/', '�����', '������', 'yes', 'current', 'points', '1');

    return $list;
}

function potrahun_pages_stories_list_all()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'stories_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_stories_list', $args);
    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows = $arr['rows']; // ����� �������
    $pages = ceil($rows / $args['per_page']); // �������
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/atricles/', $pages_length, '/', '�����', '������', 'yes', 'current', 'points', '1');

    return $list;
}

function potrahun_video_category_link()
{
    $content = '';
    if (! empty($_GET['category_id'])) {
        $content = '?category='.category_name_translit();
    }

    return $content;
}

function potrahun_pages_videos_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    // $arr=preparing_videos_list($args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/videos/', $args['pages_length'], '/', '�����', '������', 'yes', 'current', 'points', '1');

    return $list;
}

function potrahun_pages_videos_list_category()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    // $arr=preparing_videos_list($args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/'.category_title_translit().'/', $args['pages_length'], '/', '�����', '������', 'yes', 'current', 'points', '1');

    return $list;
}

function potrahun_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^chat.html$/', $query, $args)) {
        $config['vars']['page'] = 'chat';

        return;
    }
    if (preg_match('/^reviews\.html$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^faq\.html$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^parents_and_children\.html$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    if (preg_match('/^contacts\.html$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^news\/$/', $query, $args)) {
        $config['vars']['page'] = 'site_news/page';

        return;
    }
    if (preg_match('/^videos\/$/', $query, $args)) {
        $config['vars']['page'] = 'collection_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^videos\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    // ������
    if (preg_match('/^articles\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^articles\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/article-([\w]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_translit($args[4]);
        story_views_count();

        return;
    }
    // //������
    // ����
    if (preg_match('/^games\/$/', $query, $args)) {
        $config['vars']['page'] = 'games/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^games\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'games/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/game-([\w]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'games/play';
        $_GET['game_id'] = get_game_id_from_translit($args[4]);
        games_views_count();

        return;
    }
    // //����
    // �������� �����
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/video-([\w\-();]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['id'] = get_video_id_from_translit($args[4]);
        if (! empty($_GET['category'])) {
            redirect_301('/'.$args[1].'/'.$args[2].'/'.$args[3].'/video-'.$args[4].'.html');
            // $_GET['category_id']=get_category_id_from_translit($_GET['category']);
        }
        views_count();

        return;
    }
    if (preg_match('/^search$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = $args[2];
        $_GET['sort'] = 'mv';

        return;
    }
    // �����
    if (preg_match('/^photos\/$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^photos\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/photo-([\w\-();]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'gallery/view_gallery';
        $_GET['gallery_id'] = get_gallery_id_from_translit($args[4]);
        gallery_views_count();

        return;
    }
    //
    // ����� ��� ���������
    if (preg_match('/^([\w]+)\/?$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_title_translit($args[1]);

        return;
    }
    if (preg_match('/^([\w]+)\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['p'] = $args[2];
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_title_translit($args[1]);

        return;
    }
    //
}
