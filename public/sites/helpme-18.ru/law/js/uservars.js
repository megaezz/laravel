/**
 * Возвращает список юзерских переменных:
 *
 * Примеры использования:
 *
 * uservars.get("phone", function(data) {
 * 		console.log(data.phone);
 * });
 *
 * uservars.get(['phone', 'tariff', 'visitorsCount'], function(data) {
 * 		console.log(data.phone);
 * 		console.log(data.tariff);
 * });
 *
 * 	Доступные переменные:
 *
 *  'id',
 *  'adminprofile',
 *  'username',
 *  'first_name',
 *  'last_name',
 *  'date_joined',
 *  'current_tariff',
 *  'have_paid',
 *  'current_tariff_begin',
 *  'current_tariff_end',
 *  'pages_count',
 *  'published_pages_count',
 *  'leads_count',
 *  'domains_count',
 *  'our_domains_count',
 *  'uniques_count',
 *  'payments_count',
 *  'domains_and_tariffs_sum',
 *
 */

(function() {
	if (window.uservars) {
		return;
	}
	var vars = {};
	window.uservars = {
		get: function(varNames, callback) {
			if ( !(varNames instanceof Array)) {
				varNames = [varNames];
			}
			var data = {};
			var needLoad = [];

			varNames.forEach(function(varName) {
				if (vars[varName]) {
					data[varName] = vars[varName];
				} else {
					needLoad.push(varName);
				}
			});
			if (needLoad.length) {
				var url = "/api/v1/accounts/user-vars/?" + needLoad.join("&");
				rest({
					url: url,
					onReady: function(res) {
						Object.keys(res).forEach(function (varName) {
							vars[varName] = data[varName] = res[varName];
						});
						callback(data);
					}
				});
			} else {
				callback(data);
			}
		},
		dbSet: function(name, value, callback) {
			if (!vars['additional_data']) {
				console.log(vars['additional_data'], vars);
				uservars.get('additional_data', function() {
					uservars.dbSet(name, value, callback);
				});
			} else {
				var hashValue = vars['additional_data'];
				hashValue[name] = value;
				var url = "/api/v1/accounts/user-vars/?additional_data";
				rest({
					url: url,
					method: 'PATCH',
					data: JSON.stringify({additional_data: hashValue}),
					onReady: function() {
						callback();
					}
				});
			}
		},
		dbGet: function(varNames, callback) {
			if ( !(varNames instanceof Array)) {
				varNames = [varNames];
			}

			if (vars['additional_data']) {
				var response = {};
				varNames.forEach(function (key) {
					response[key] = vars['additional_data'][key];
				});
				callback(response);
			} else {
				uservars.get('additional_data', function(response) {
					if (response) {
						uservars.dbGet(varNames, callback);
					}
				});
			}
		}
	};

	var rest = function(cfg) {
		var csrf = document.cookie.match(/csrftoken=(.*?);/)[1];
		var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
		xmlhttp.open(cfg.method || "GET", cfg.url);
		if (cfg.data) {
			xmlhttp.setRequestHeader('Content-Type', 'application/json');
		}
		xmlhttp.setRequestHeader('X-CSRFToken', csrf);
		xmlhttp.onload = function() {
			var response = xmlhttp.response;
			if (this.status === 200) {
				if (typeof response === 'string') {
					response = JSON.parse(response);
				}
				//console.log("%cNet response: ", "background:orange; color:white;", response);
				cfg.onReady(response);
			}
		};
		xmlhttp.send(cfg.data);
	}
})();
