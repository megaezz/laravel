 $(document).ready(function() {

 
 	$(function() {
    $(".youtube").each(function() {
		$(this).css('background-image', 'url(https://img.youtube.com/vi/' + this.id + '/maxresdefault.jpg)');
        $(this).append($('<div/>', {'class': 'play'}));

        $(document).delegate('#'+this.id, 'click', function() {
            var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
            if ($(this).data('params')) iframe_url+='&'+$(this).data('params');
            var iframe = $('<iframe/>', {'frameborder': '0', 'src': iframe_url, 'width': $(this).width(), 'height': $(this).height() })
            $(this).replaceWith(iframe);
        });
    });
	
	});


$('.reviews-list').owlCarousel({
	loop:true,
	items:3,
	margin:18,
	nav:true,
	dots: true,	
	autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    smartSpeed:450,
	responsive : {   
	0 : {
		items:1,
    },
	500 : {
		items:1,
		margin:0,
	},
	600 : {
		items:2,
    },
    768 : {
		items:2,
    },
	992 : {
		items:3,
    }
	},

});
 
$('.works-list').owlCarousel({
	loop:true,
	items:1,
	margin:0,
	nav:true,
	dots: true,	
	autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    smartSpeed:450,	

});

$('.certificates-list').owlCarousel({
	loop:true,
	items:5,
	margin:80,
	nav:false,
	dots: false,	
	autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    smartSpeed:450,	
	responsive : {   
	0 : {
		items:1,
    },
	600 : {
		items:2,
    },
    768 : {
		items:3,
    },
	992 : {
		items:3,
    },
	1200 : {
		items:5,
    }
	},

});

$('.team-list').owlCarousel({
	loop:true,
	items:1,
	margin:0,
	nav:true,
	dots: false,	
	autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    smartSpeed:450,	

});

$("input[name='phone']").mask("+7 (999) 999-99-99");
  
$('.nav-tabs li').on('click', function(e) {   
	 e.preventDefault();
    if (!$(this).hasClass('active')) {     
	  $('.nav-tabs li').removeClass('active');	  
	   $(this).addClass('active');   
    } else {
     
    }
});
 
});
