<?php

namespace MegawebV1;

function pronchik_pages_video_collection_alt()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 1;
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
        $_GET['sort'] = 'mr';
    }
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $args['mobile'] = 'yes';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/'.$_GET['get_sort'].'/', $args['pages_length'], '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pronchik_pages_video_collection_mobile_alt()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 1;
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
        $_GET['sort'] = 'mr';
    }
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $args['mobile'] = 'yes';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/'.$_GET['get_sort'].'/', $args['pages_length'], '', '�����', '������', 'no', 'active', 'points', '1');

    return $list;
}

function pronchik_pages_categories_videos_alt()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 1;
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
        $_GET['sort'] = 'mr';
    }
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/'.category_name_translit().'/'.$_GET['get_sort'].'/', $args['pages_length'], '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pronchik_pages_categories_videos_mobile_alt()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = 1;
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
        $_GET['sort'] = 'mr';
    }
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $args['mobile'] = 'yes';
    $arr = cache('preparing_videos_list', $args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/'.category_name_translit().'/'.$_GET['get_sort'].'/', $args['pages_length'], '', '�����', '������', 'no', 'active', 'points', '1');

    return $list;
}

function pronchik_categories_videos()
{
    $arr = func_replacement('preparing_categories_videos');
    foreach ($arr['videos'] as $k => $v) {
        $arr['videos'][$k]['teasers'] = '';
        if ($k == '0') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs1');
        }
        if ($k == '10') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs2');
        }
        if ($k == '20') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs3');
        }
        if ($k == '30') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs4');
        }
    }

    return thumbs_from_array($arr['videos']);
}

function pronchik_same_videos()
{
    global $config;
    $arr = func_replacement('preparing_same_videos');
    foreach ($arr['videos'] as $k => $v) {
        $arr['videos'][$k]['teasers'] = '';
        if ($k == '0') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs1');
        }
        if ($k == '10') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs2');
        }
        if ($k == '20') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs3');
        }
        if ($k == '30') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs4');
        }
    }

    return thumbs_from_array($arr['videos']);
}

function pronchik_video_collection()
{
    global $config;
    $arr = func_replacement('preparing_video_collection');
    foreach ($arr['videos'] as $k => $v) {
        $arr['videos'][$k]['teasers'] = '';
        if ($k == '0') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs1');
        }
        if ($k == '10') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs2');
        }
        if ($k == '20') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs3');
        }
        if ($k == '30') {
            $arr['videos'][$k]['teasers'] = template('promo/teasernet.com/thumbs4');
        }
    }

    return thumbs_from_array($arr['videos']);
}

// ����� ������ ������� ��� �������� video_collection
function pronchik_pages_video_collection_mobile()
{
    $arr = func_replacement('preparing_video_collection_mobile');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page_mobile'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length_mobile');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/'.$_GET['get_sort'].'/', $pages_length, '', '�����', '������', 'no', 'active', 'points', '1');

    return $list;
}

function pronchik_pages_categories_videos()
{
    $arr = func_replacement('preparing_categories_videos');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/'.$_GET['name_translit'].'/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pronchik_pages_categories_videos_mobile()
{
    $arr = func_replacement('preparing_categories_videos');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/'.$_GET['name_translit'].'/'.$_GET['get_sort'].'/', $pages_length, '', '�����', '������', 'no', 'active', 'points', '1');

    return $list;
}

// ����� ������ ������� ��� �������� video_collection
function pronchik_pages_video_collection()
{
    $arr = func_replacement('preparing_video_collection');
    $rows = $arr['rows']; // ����� �������
    $per_page = value_from_config_for_domain('videohub', 'collection_videos_per_page'); // ���������� �� ��������
    $pages_length = value_from_config_for_domain('videohub', 'pages_length');
    $pages = ceil($rows / $per_page); // �������
    if ($_GET['sort'] == 'mr') {
        $_GET['get_sort'] = 'new';
    }
    if ($_GET['sort'] == 'mv') {
        $_GET['get_sort'] = 'popular';
    }
    $list = page_numbers($rows, $per_page, $_GET['p'], '/'.$_GET['get_sort'].'/', $pages_length, '', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function pronchik_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (articles_rewrite_function_same()) {
        return;
    }
    if (preg_match('/^site-info\.html$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^questions\.html$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^parents-control\.html$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    if (preg_match('/^feedback$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^porno\-categories$/', $query, $args)) {
        $config['vars']['page'] = 'categories_list';

        return;
    }
    if (preg_match('/^save\/([\w]+)\/([\w\-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'save';
        $_GET['videokey'] = $args[1];
        $_GET['id'] = get_video_id_from_translit($args[2]);

        return;
    }
    if (preg_match('/^porno\/([\w\-();]+)$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['title_ru_translit'] = $args[1];
        $_GET['id'] = get_video_id_from_translit($args[1]);

        return;
    }
    if (preg_match('/^(new|popular)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        if ($args[1] == 'new') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'popular') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^(new|popular)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        if ($args[1] == 'new') {
            $_GET['sort'] = 'mr';
        }
        if ($args[1] == 'popular') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[2];

        return;
    }
    if (preg_match('/^([\w\-]+)\/(new|popular)\/([\d]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        if ($args[2] == 'new') {
            $_GET['sort'] = 'mr';
        }
        if ($args[2] == 'popular') {
            $_GET['sort'] = 'mv';
        }
        $_GET['p'] = $args[3];

        return;
    }
    if (preg_match('/^([\w\-]+)$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos_main';
        $_GET['category_id'] = get_category_id_from_translit($args[1]);
        $_GET['name_translit'] = $args[1];
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
}
