<?php

namespace MegawebV1;

function kustiki_menu_current($section)
{
    global $config;
    $active = 0;
    $section_pages['chat'] = ['chat'];
    $section_pages['video'] = ['index', 'collection', 'collection_main', 'categories_videos', 'categories_videos_main', 'video'];
    $section_pages['articles'] = ['stories/all', 'stories/all_main', 'stories/read'];
    $section_pages['contact'] = ['contact'];
    $section_pages['faq'] = ['faq'];
    $section_pages['parents_control'] = ['parents_control'];
    $section_pages['guestbook'] = ['about'];
    if (isset($section_pages[$section]) == 0) {
        error('������ �� ������������', '������ '.$section.' �� ������������');
    }
    foreach ($section_pages[$section] as $page) {
        if ($page == $config['vars']['page']) {
            $active = 1;
        }
    }
    if ($active == 0) {
        return '';
    }
    if ($active == 1) {
        return 'class="active"';
    }
}

function kustiki_pages_stories_list_all()
{
    global $config;
    $pages_length = 5;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = (int) $_GET['p'];
    $args['per_page'] = value_from_config_for_domain('videohub', 'stories_per_page');
    $args['sort'] = $_GET['sort'];
    $arr = cache('preparing_stories_list', $args);
    // $arr=preparing_stories_list($config['domain'],'all','value',$_GET['p'],$per_page,'mr');
    $rows = $arr['rows']; // ����� �������
    $pages = ceil($rows / $args['per_page']); // �������
    $list = page_numbers($rows, $args['per_page'], $_GET['p'], '/sex-texts/page/', $pages_length, '/', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function kustiki_most_popular_text()
{
    global $config;
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    $args['page'] = '1';
    $args['sort'] = 'mv';
    $args['per_page'] = 5;
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args, 43200);
    $list = '';
    foreach ($arr['videos'] as $v) {
        $v['date_slashed'] = date('Y/m/d', $v['date']);
        $list .= '<p><a href="/'.$v['date_slashed'].'/'.$v['title_ru_translit'].'.html">'.$v['title_ru'].'</a></p>';
    }

    return $list;
}

function kustiki_video_category_link()
{
    $content = '';
    if (! empty($_GET['category_id'])) {
        $content = '?category='.category_name_translit();
    }

    return $content;
}

function kustiki_breadcrumb_category_link()
{
    $content = '';
    if (! empty($_GET['category'])) {
        $_GET['category_id'] = get_category_id_from_translit($_GET['category']);
        $content = '
		<li>
		<a href="/'.category_title_translit().'/">'.category_name().'</a>
		<img src="/template/images/right-arrow.png" alt="right arrow" class="arrow-right" />
		</li>
		';
    }

    return $content;
}

function kustiki_pages_videos_list_all()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'all';
    $args['value'] = 'value';
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    // $arr=preparing_videos_list($args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/page/', $args['pages_length'], '/', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function kustiki_pages_videos_list_category()
{
    global $config;
    $args['pages_length'] = value_from_config_for_domain('videohub', 'pages_length');
    $args['per_page'] = value_from_config_for_domain('videohub', 'collection_videos_per_page');
    $args['domain'] = $config['domain'];
    $args['type'] = 'category';
    $args['value'] = $_GET['category_id'];
    if (isset($_GET['p'])) {
        $args['page'] = (int) $_GET['p'];
    } else {
        $args['page'] = '1';
    }
    if (isset($_GET['sort'])) {
        $args['sort'] = $_GET['sort'];
    } else {
        $args['sort'] = 'mr';
    }
    $args['mobile'] = 'no';
    $arr = cache('preparing_videos_list', $args);
    // $arr=preparing_videos_list($args);
    $rows = $arr['rows'];
    $pages = ceil($rows / $args['per_page']);
    $list = page_numbers($rows, $args['per_page'], $args['page'], '/'.category_title_translit().'/', $args['pages_length'], '/', '&laquo;', '&raquo;', 'yes', 'active', 'active', '1');

    return $list;
}

function kustiki_rewrite_query()
{
    global $config;
    if (empty($_GET['query'])) {
        error('�� ������� ������');
    }
    $query = $_GET['query'];
    if (preg_match('/^chat.html$/', $query, $args)) {
        $config['vars']['page'] = 'chat';

        return;
    }
    if (preg_match('/^sitemap.xml$/', $query, $args)) {
        $config['vars']['page'] = 'sitemap';

        return;
    }
    if (preg_match('/^guestbook\.html$/', $query, $args)) {
        $config['vars']['page'] = 'about';

        return;
    }
    if (preg_match('/^faq\.html$/', $query, $args)) {
        $config['vars']['page'] = 'faq';

        return;
    }
    if (preg_match('/^4parents\.html$/', $query, $args)) {
        $config['vars']['page'] = 'parents_control';

        return;
    }
    if (preg_match('/^fb\.html$/', $query, $args)) {
        $config['vars']['page'] = 'contact';

        return;
    }
    if (preg_match('/^news\/$/', $query, $args)) {
        $config['vars']['page'] = 'site_news/page';

        return;
    }
    if (preg_match('/^page\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'collection';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    // ������
    if (preg_match('/^sex-texts\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all_main';
        $_GET['get_sort'] = 'most_recent';
        $_GET['sort'] = 'mr';
        $_GET['p'] = 1;

        return;
    }
    if (preg_match('/^sex-texts\/page\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'stories/all';
        $_GET['p'] = $args[1];
        $_GET['sort'] = 'mr';

        return;
    }
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/sex-text\/([\w]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'stories/read';
        $_GET['story_id'] = get_story_id_from_translit($args[4]);
        story_views_count();

        return;
    }
    // //������
    // �������� �����
    if (preg_match('/^([\d]+)\/([\d]+)\/([\d]+)\/([\w\-();]+)\.html$/', $query, $args)) {
        $config['vars']['page'] = 'video';
        $_GET['id'] = get_video_id_from_translit($args[4]);
        if (! empty($_GET['category'])) {
            redirect_301('/'.$args[1].'/'.$args[2].'/'.$args[3].'/'.$args[4].'.html');
        }
        views_count();

        return;
    }
    if (preg_match('/^search$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = 1;
        $_GET['sort'] = 'mv';

        return;
    }
    if (preg_match('/^search\/([^\/]+)\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'search_videos';
        $_REQUEST['search_query'] = $args[1];
        $_GET['p'] = $args[2];
        $_GET['sort'] = 'mv';

        return;
    }
    // ����� ��� ���������
    if (preg_match('/^([\w]+)\/?$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos_main';
        $_GET['p'] = 1;
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_title_translit($args[1]);

        return;
    }
    if (preg_match('/^([\w]+)\/([\d]+)\/$/', $query, $args)) {
        $config['vars']['page'] = 'categories_videos';
        $_GET['p'] = $args[2];
        $_GET['sort'] = 'mr';
        $_GET['category_id'] = get_category_id_from_title_translit($args[1]);

        return;
    }
}
