<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#" xmlns:og="http://ogp.me/ns#">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>{{ $headers->title }}</title>
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="website" property="og:type">
  <meta content="https://doctu.ru/ekb" property="og:url">
  <meta content="ru_RU" property="og:locale">
  <meta content="{{ ucfirst($domain->domain) }}" property="og:site_name">
  <meta content="summary" property="twitter:card">
  {{-- <meta content="@docteledoc" property="twitter:site"> --}}
  {{-- <meta content="https://doctu.ru" property="twitter:domain"> --}}
  <meta content="/types/doctor/images/logo_doctu_share.png" property="og:image">
  <meta content="/types/doctor/images/logo_doctu.png" property="twitter:image:src">
  <meta property="og:title" content="{{ $domain->site_name }} в {{ $city->name }}: 2252 клиники, 13314 врачей, 59326 отзывов">
  <meta property="twitter:title" content="{{ $domain->site_name }} в {{ $city->name }}: 2252 клиники, 13314 врачей, 59326 отзывов">
  <meta name="description" content="{{ $domain->site_name }} в {{ $city->name }}: 2252 лечебных учреждения, 13314 специалистов, 59326 проверенных отзывов, цены на услуги, адреса и телефоны, запись на прием.">
  {{-- повыключать лишние стили или перенести их на необходимые страницы в слот head --}}
  <link rel="stylesheet" href="/types/doctor/css/entry._AN4bX2R.css">
  <link rel="stylesheet" href="/types/doctor/css/TopBlock.6UZojxgM.css">
  <link rel="stylesheet" href="/types/doctor/css/page-home.YjT-IaQd.css">
  <link rel="stylesheet" href="/types/doctor/css/main.css">
  {{ $head ?? '' }}
</head>
<body>
  <div class="nuxt-loading-indicator" style="position: fixed; top: 0px; right: 0px; left: 0px; pointer-events: none; width: auto; height: 3px; opacity: 0; background: rgb(19, 165, 70); transform: scaleX(0); transform-origin: left center; transition: transform 0.1s ease 0s, height 0.4s ease 0s, opacity 0.4s ease 0s; z-index: 999999;">

  </div>
  <div class="main">
    <header class="main__header header">
      <div class="container">
        <div class="header__wrap">
          <div class="header__wrap_burger">
            <img src="/types/doctor/images/burger.svg" alt="">
          </div>
          <div class="header__wrap_logo">
            <a href="/" class="" title="{{ ucfirst($domain->domain) }}">
              <img src="{{ $domain->logo_src }}" alt="{{ $domain->domain }}" style="height: 30px;">
            </a>
          </div>
          <div class="header__wrap_menu main-menu">
            <div class="main-menu__wrap">
              <div class="main-menu__close mobile">
                <img src="/types/doctor/images/clear_12x12.svg" alt="">
              </div>
              <div class="main-menu__logo mobile">
                <a href="/" class="" title="{{ ucfirst($domain->domain) }}">
                  <img height="24" width="93" src="/types/doctor/images/logo.svg" alt="{{ $domain->domain }}">
                </a>
              </div>
              <div class="main-menu__scroller">
                <div class="main-menu__place mobile">
                  <img src="/types/doctor/images/place.svg" alt=""> {{ $city->name }}
                </div>
                <div class="main-menu__user mobile">
                  <img src="/types/doctor/images/user.svg" alt=""> Личный кабинет 
                </div><div class="main-menu__menu">
                  <a href="#" class="menu_link">Врачи</a>
                  <a href="#" class="menu_link">Клиники</a>
                  <a href="#" class="menu_link">Услуги</a>
                  <a href="#" class="menu_link">Консультации</a>
                </div>
              </div>
            </div>
          </div>
          <div class="header__wrap_placeanduser">
            <div class="header-place">
              <img src="/types/doctor/images/place.svg" alt=""> {{ $city->name }}
            </div>
            <div class="header-user-popup">
              <a href="#" class="header-user">
                <img src="/types/doctor/images/account.svg" alt="Личный кабинет"> Личный кабинет 
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
    
    {{ $slot }}

    <footer class="main__footer footer">
      <div class="container">
        <div class="footer__wrap">
          <div class="footer__wrap_menu">
            <a href="#" class="" rel="nofollow">Для клиник</a>
            <a href="#" class="" rel="nofollow">Для врачей</a>
            <a href="#" class="" rel="nofollow">Все города</a>
            <a href="#" class="" rel="nofollow">О проекте</a>
            <a href="#" class="" rel="nofollow">О компании</a>
            <a href="#" class="" rel="nofollow">Авторы</a>
            <a href="#" class="" rel="nofollow">Оферта</a>
            <a href="#" class="" rel="nofollow">Контакты</a>
          </div>
          <div class="footer__wrap_register">
            Сетевое издание {{ $domain->site_name }}. E-mail: <a href="mailto:{{ "info@{$domain->domain}" }}">{{ "info@{$domain->domain}" }}</a>
          </div>
          <div class="footer__wrap_info">
            Информация, представленная на сайте, не может быть использована для постановки диагноза, назначения лечения и не заменяет прием врача.
          </div>
          <div class="footer__wrap_copiright"> © {{ now()->year }} ООО "Рога и копыта" ОГРН 01234567890 </div>
        </div>
      </div>
    </footer>
  </div>
  <script type="text/javascript" src="/types/doctor/js/data.js"></script>
</body>
</html>