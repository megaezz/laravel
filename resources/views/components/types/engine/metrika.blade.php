<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   document.addEventListener("DOMContentLoaded", function() {
      (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
      m[i].l=1*new Date();
      for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
      k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
      (window, document, "script", "https://mc.webvisor.org/metrika/tag_ww.js", "ym");

      @if ($domain->metrika?->metrika_id)

      ym({{ $domain->metrika->metrika_id }}, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:false
     });

      @endif

      ym({{ $engineConfig->common_metrika_id }}, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:false
     });
   });
</script>
<noscript>
   @if ($domain->metrika?->metrika_id)
   <div><img src="https://mc.yandex.ru/watch/{{ $domain->metrika->metrika_id }}" style="position:absolute; left:-9999px;" alt="" /></div>
   @endif
   <div><img src="https://mc.yandex.ru/watch/{{ $engineConfig->common_metrika_id }}" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->

{{-- @if ($domain->google_analytics_id) --}}
{{-- 
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{ $domain->google_analytics_id }}"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '{{ $domain->google_analytics_id }}');
</script>
@endif 
--}}