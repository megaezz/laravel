<ol>
    @foreach ($nodes as $node)
    <li>
        {{ $node->title }} &mdash;
        @if ($node->available)
        <span style="color: green;">true</span>
        @else
        <span style="color: red;">false</span>
        @endif
    </li>
    @endforeach
</ol>