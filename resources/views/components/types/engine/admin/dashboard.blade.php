<html>

<head>
    <title>Awmzone</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <link rel="icon" href="/types/engine/template/admin/images/favicon.png">
    <!-- @vite(['resources/css/app.css']) -->
    <style>
        body {
            background-image: url('/types/cinema/template/admin/customer/images/extra_clean_paper.png');
            /*			background-image: url('/types/engine/template/images/leaves-pattern.png');*/
            /*padding-top: 20px;*/
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('index') }}">MGW</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link {{ request()->routeIs('index') ? 'active' : '' }}"
                            href="{{ route('index') }}">
                            Домены
                        </a>
                    </li>
                    {{-- <li class="nav-item dropdown d-none">
						<a class="nav-link dropdown-toggle {{ request()->routeIs('cinema.*') ? 'active' : '' }}" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							Cinema
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="dropdown-item {{ request()->routeIs(['cinema.abuses', 'cinema.load-abuses']) ? 'active' : '' }}" href="{{ route('cinema.abuses') }}">
									<i class="fa-regular fa-envelope"></i> Абузы
								</a>
							</li>
							<li>
								<a class="dropdown-item {{ request()->routeIs('cinema.articles.*') ? 'active' : '' }}" href="{{ route('cinema.articles.list') }}">
									<i class="far fa-file"></i> Статьи
								</a>
							</li>
							<li>
								<a class="dropdown-item {{ request()->routeIs('cinema.images') ? 'active' : '' }}" href="{{ route('cinema.images') }}">
									<i class="fa-regular fa-images"></i> Картинки
								</a>
							</li>
							@can('everything')
							<li>
								<a class="dropdown-item {{ request()->routeIs('cinema.actions') ? 'active' : '' }}" href="{{ route('cinema.actions') }}">
									<i class="fas fa-tasks"></i> Действия
								</a>
							</li>
							@endcan
						</ul>
					</li> --}}
                    {{-- <li>
						<a class="nav-link" href="https://blog.awmzone.net/admin">
							Blog
						</a>
					</li> --}}
                    @can('everything')
                        {{-- <li>
						<a class="nav-link {{ request()->routeIs('wiki') ? 'active' : '' }}" href="/?r=admin/Admin/controllerMethods">
							Вики
						</a>
					</li> --}}
                        <li>
                            <a class="nav-link {{ request()->routeIs('state') ? 'active' : '' }}"
                                href="{{ route('state') }}">
                                Состояние
                            </a>
                        </li>
                    @endcan
                    <li>
                        <a class="nav-link" href="{{ route('filament.awmzone.home') }}">
                            Панель
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    {{ $slot }}

    <!-- @vite(['resources/js/app.js']) -->
    <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.11/dist/clipboard.min.js"></script>
    <!-- <script src="/clipboard.min.js"></script> -->
    <script>
        new ClipboardJS('.copy');
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
</body>

</html>
