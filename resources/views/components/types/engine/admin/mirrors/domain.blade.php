{{-- отбражать ли меню домена --}}
@if (isset($primaryDomain) and $primaryDomain)
    <button class="btn btn-light" type="button" id="dropdown{{ $domain->domain }}" data-bs-toggle="dropdown"
        aria-haspopup="true">
        <i class="fa-solid fa-ellipsis-vertical"></i>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdown{{ $domain->domain }}">
        <x-types.engine.admin.domain-menu :domain="$domain" />
    </div>
@endif

<button class="btn btn-sm btn-light far fa-copy copy" data-clipboard-text="{{ $domain->domain }}"></button>
<button data-bs-toggle="modal" data-bs-target="#domainSettings"
    wire:click="$dispatchTo('types.engine.admin.mirrors.domain-settings-modal', 'set-domain', { domain: '{{ $domain->domain }}' })"
    @class([
        'btn btn-link p-0',
        'text-muted' => !$domain->parent_domain_or_self->renewed,
        'fw-bold' => $bold ?? false,
    ])>
    {{ $domain->domain_decoded }}
</button>
<br>
<span class="badge text-bg-light">
    {{ $domain->add_date->format('j F Y') }}
</span>

{{-- отбражать ли кнопку яндекс метрики --}}
@if (isset($metrika) and $metrika)
    <a class="btn btn-sm btn-light fa-solid fa-chart-simple"
        href="https://metrika.yandex.ru/stat/search_engines?group=day&amp;period=month&amp;accuracy=1&amp;id={{ $domain->metrika?->metrika_id }}">
    </a>
@endif

{{-- отбражать ли кнопку яндекс вебмастера --}}
@if (isset($yandexWebmaster) and $yandexWebmaster)
    <a class="btn btn-sm btn-light fa-brands fa-yandex"
        href="https://webmaster.yandex.ru/site/{{ \engine\app\services\YandexWebmasterService::getHostIdByDomain($domain) }}/dashboard/">
    </a>
@endif

@if ($domain->renewal_error)
    <span class="badge text-bg-danger"><i class="fa-solid fa-clock"></i></span>
@endif

{{-- если родительский домен продлен а текущий заблочен или есть известный блок - отображаем пометку --}}
@if ($domain->parent_domain_or_self->renewed and (!$domain->monitoring_ru or $domain->known_ru_block))
    <span style="cursor: pointer;"
    @class([
        'badge',
        'text-bg-info' => $domain->known_ru_block,
        'text-bg-danger' => !$domain->known_ru_block,
    ])
        wire:click="$dispatchTo('types.engine.admin.mirrors.domain-settings-modal', 'show-rkns', { domain: '{{ $domain->domain }}' })"
        data-bs-toggle="modal" data-bs-target="#domainSettings">
        <i class="fa-solid fa-ban"></i>
    </span>
@endif
