<a class="dropdown-item" href="{{ route('domain.routes', ['domain' => $domain->domain]) }}">Страницы</a>
<a class="dropdown-item" href="{{ route('domain.settings',[ 'domain' => $domain->domain ]) }}">Настройки</a>
@if ($domain->type == 'cinema')
<a class="dropdown-item" href="{{ route('cinema.domain.settings',[ 'domain' => $domain->domain ]) }}">
	Настройки Cinema
</a>
@endif
{{-- <a class="dropdown-item" href="/?r=admin/Admin/resetDomainCache&amp;domain={{ $domain->domain }}">Сбросить кеш</a> --}}