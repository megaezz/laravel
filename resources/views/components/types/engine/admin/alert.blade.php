@if ($message)
<div class="col-12">
  <div @class(['alert', 'alert-warning' => ($type == 'error'), 'alert-success' => ($type == 'message'), 'alert-dismissible fade show']) role="alert">
    {{ $message }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
</div>
@endif
