<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Киноконтент</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="/types/engine/template/css/font-awesome/5.3.1/css/all.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/types/cinema/template/admin/images/icon.png">
    <link rel="stylesheet" href="/types/cinema/template/admin/customer/css/style.css">
</head>
<body>
    <div class="container mb-5">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4" style="background-color: #e3f2fd;">
            <a class="navbar-brand" href="/">Киноконтент</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="menu">
                <ul class="navbar-nav">
                    <li @class(['nav-item', 'active' => (isset($page) and $page == 'index')])>
                        <a class="nav-link" href="/">
                            <i class="fas fa-list-ul"></i>
                            Очередь
                        </a>
                    </li>
                    <li @class(['nav-item', 'active' => (isset($page) and $page == 'done')])>
                        <a class="nav-link" href="/done">
                            <i class="fas fa-check-circle"></i>
                            Выполнено <span class="badge badge-secondary">{{ $user->createdTasks()->done()->where('customer_archived', false)->count() }}</span>
                        </a>
                    </li>
                    <li @class(['nav-item', 'active' => (isset($page) and $page == 'stats')])>
                        <a class="nav-link" href="/statByDays">
                            <i class="far fa-list-alt"></i>
                            Статистика
                        </a>
                    </li>
                </ul>
            </div>
            <span class="navbar-text">
                <span title="Сменить тему" class="mr-2 themeToggle"><i></i></span>
                <a href="/deposit" title="Пополнить баланс"><i class="fas fa-money-bill-alt"></i> {{ $user->megaweb->getRewriteBalance() }} руб.</a>
                <span class="ml-2">
                    <a href="/userInfo">{{ $user->login }}</a>
                    <a href="/logout" title="Выйти"><i class="fas fa-sign-out-alt"></i></a>
                </span>
            </span>
        </nav>

        {{ $slot }}

    </div>
    <script src="/types/cinema/template/admin/customer/js/jquery.min.js"></script>
    <script src="/types/cinema/template/js/jquery.cookie.js"></script>
    <script src="/types/cinema/template/admin/customer/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="/types/cinema/template/admin/customer/js/change-theme.js"></script>
    <script src="/types/cinema/template/admin/customer/js/clipboard.min.js"></script>
    <script>
        new ClipboardJS('.copy');
    </script>
    
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

       ym(52431304, "init", {
        id:52431304,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/52431304" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- BEGIN JIVOSITE CODE literal -->
    <script type='text/javascript'>
        (function(){ var widget_id = 'ojul3Tj71C';var d=document;var w=window;function l(){
          var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
          s.src = '//code.jivosite.com/script/widget/'+widget_id
          ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
          if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
          else{w.addEventListener('load',l,false);}}})();
    </script>
    <!-- {/literal} END JIVOSITE CODE -->
</body>
</html>