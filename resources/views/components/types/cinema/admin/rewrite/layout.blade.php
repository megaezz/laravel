<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Кинотекст</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="/types/engine/template/css/font-awesome/5.13.0/css/all.min.css">
    <link href="/types/cinema/template/admin/rewrite/css/style.css?3" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/types/cinema/template/admin/images/icon.png">
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="menu">
                <ul class="navbar-nav">
                    <li class="nav-item {{ $menuBaseActive }}">
                        <a class="nav-link" href="/?r=Worker/base">
                            <i class="fas fa-list-ul"></i>
                            Все задания <span class="badge badge-secondary baseRows" data-value="{{ $baseRows }}">{{ $baseRows }}</span>
                        </a>
                    </li>
                    <li class="nav-item" title="Оповестить, если появятся задания">
                        <span class="nav-link sound" style="padding-left: 0;">
                            <i class="fas fa-volume-mute"></i>&nbsp;
                        </span>
                    </li>
                    <script>
                        var sound = null;

                        function getBaseRows(){
                            $.ajax({
                                url: '/?r=Worker/getBaseRows',
                                type: 'get',
                                success: function(response) {
                                    var oldRows = $('.baseRows').data('value');
                                    var newRows = response;
                                    /*var newRows = oldRows + 1;*/
                                    if (oldRows != newRows) {
                                        $('.baseRows').fadeOut('fast',function(){
                                            $('.baseRows').html(newRows);
                                            $('.baseRows').data('value',newRows);
                                            $('.baseRows').fadeIn('fast');
                                        });
                                        if (oldRows == 0) {
                                            if (sound) {
                                                sound.src = '/types/cinema/template/admin/rewrite/sound/juntos.mp3';
                                                sound.play();
                                            }
                                        }
                                        /*alert('Изменилось');*/
                                    }
                                }
                            });
                        }

                        document.addEventListener("DOMContentLoaded", function() {
                            /*$(document).bind("click keydown keyup mousemove", initSound());*/
                            $('.sound').on('click',function(){
                                if (sound == null) {
                                    sound = new Audio();
                                    $(this).html('<i class="fas fa-volume-down"></i>&nbsp;');
                                } else {
                                    sound = null;
                                    $(this).html('<i class="fas fa-volume-mute"></i>&nbsp;');
                                }
                            });
                            setInterval(getBaseRows,10000);
                        });
                    </script>
                    <li class="nav-item {{ $menuRewriteActive }}">
                        <a class="nav-link" href="/?r=Worker/rewrite">
                            <i class="fas fa-pencil-alt"></i>
                            Мои задания <span class="badge badge-secondary">{{ $myRows }}</span>
                        </a>
                    </li>
                    @if(in_array($user->getLevel(),array('admin','moderator')))
                    <li class="nav-item {{ $menuModerationActive }}">
                        <a class="nav-link" href="/?r=Moderator/rewrited&amp;showToCustomer=0&amp;withDomainDescription=1&amp;rework=0&amp;btnActive=premoderation">
                            <i class="fas fa-glasses"></i>
                            Модерация <span class="badge badge-secondary moderationRows" data-value="{{ $moderationRows }}">{{ $moderationRows }}</span>
                        </a>
                    </li>
                    <script>
                        function getModerationRows(){
                            $.ajax({
                                url: '/?r=Moderator/getModerationRows',
                                type: 'get',
                                success: function(response) {
                                    var oldRows = $('.moderationRows').data('value');
                                    var newRows = response;
                                    // var newRows = oldRows + 1;
                                    if (oldRows != newRows) {
                                        $('.moderationRows').fadeOut('fast',function(){
                                            $('.moderationRows').html(newRows);
                                            $('.moderationRows').data('value',newRows);
                                            $('.moderationRows').fadeIn('fast');
                                        });
                                    }
                                }
                            });
                        }
                        document.addEventListener("DOMContentLoaded", function() {
                            setInterval(getModerationRows,10000);
                        });
                    </script>
                    @endif
                    <li class="nav-item {{ $menuRewritedActive }}">
                        <a class="nav-link" href="/?r=Worker/rewrited">
                            <i class="fas fa-check-circle"></i>
                            Выполненные
                        </a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('payment') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('payment') }}">
                            <i class="fas fa-money-bill-alt"></i>
                            Выплаты
                        </a>
                    </li>
                    <li class="nav-item {{ $menuRulesActive }}">
                        <a class="nav-link" href="/?r=Worker/rules">
                            <i class="far fa-file-alt"></i>
                            Правила
                        </a>
                    </li>
                </ul>
            </div>
            <span class="navbar-text">
                <span title="Сменить тему" class="mr-2 themeToggle"><i></i></span>
                <i class="fas fa-money-bill-alt"></i> {{ $user->getRewriteBalance() }} руб.
                <span class="ml-2">
                    <a href="/?r=Worker/userInfo">{{ $user->getLogin() }}</a>
                    <a href="/?r=Worker/logout"><i class="fas fa-sign-out-alt"></i></a>
                </span>
            </span>
        </nav>
        {!! $emailAlertBlock !!}
        {!! $reworkAlertBlock !!}

        {{ $slot }}

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="/types/cinema/template/admin/customer/js/change-theme.js"></script>
    <script src="/types/cinema/template/js/autosize.min.js"></script>
    <!-- счетчик символов -->
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            autosize($('textarea[name="description"]'));
            $('textarea[name="description"]').each(function(){
                $('#symbols_'+$(this).data('id')).html($(this).val().length);
            });
            $('textarea[name="description"]').on('keyup',function(){
                $('#symbols_'+$(this).data('id')).html($(this).val().length);
            });
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>
    <script>
        new ClipboardJS('.copy');
    </script>
    {!! $metrika !!}
    {!! $jivosite !!}
</body>
</html>