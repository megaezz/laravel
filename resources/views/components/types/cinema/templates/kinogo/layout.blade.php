<!doctype html>
<html lang="ru" {!! isset($htmlPrefix) ? "prefix=\"{$htmlPrefix}\"" : null !!}>

<head>
    <meta http-equiv="x-dns-prefetch-control" content="on">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>{{ $headers->title }}</title>
    <meta name="description" content="{{ $headers->description }}" />
    <meta name="keywords" content="{{ $headers->keywords }}" />

    {{ $head ?? null }}

    {{-- TODO: href чего за opensearch --}}
    {{-- <link rel="search" type="application/opensearchdescription+xml" href="https://kinogo.inc/engine/opensearch.php" title="{{ $headers->title }}" /> --}}

    {{-- TODO: заменить на <link rel="icon" href="{{ $domain->engineDomain->favicon_src }}"> --}}
    <link rel="icon" href="{{ $domain->favicon_src }}" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    {{-- <link rel="image_src" href="/types/cinema/template/templates/kinogo/kinogo.png"> --}}
    <meta name="robots" content="noarchive"/>

    {{-- <link rel="preconnect" href="https://cdn77.aj1907.online/">
    <link rel="preconnect" href="https://aj1907.online/">
    <link rel="preconnect" href="https://counter.yadro.ru/">
    <link rel="preconnect" href="https://pimg.vb24132nightdwellers.com/"> --}}
    <meta name="theme-color" content="#404040">
    <link href="/types/cinema/template/templates/kinogo/css/fontawesome.css" rel="stylesheet" type="text/css" />
    <link href="/types/cinema/template/templates/kinogo/css/styles.min3.css" rel="stylesheet" media="screen"/>
    <!--[if lt IE 8]>
    <style type="text/css">
    .pomoshnik, .content {padding-bottom: 0em; margin-bottom: 0em;}
    </style>
    <![endif]-->

    <style>
        div[id*=epom].epom-brend {
            z-index: 0!important;
        }
        body > div[id*=epom-pushdown] {
            cursor: pointer;
            position: absolute !important;
            height: 100% !important;
            z-index: 1 !important;
            width: 100% !important;
        }
        .contener,  .footer, .blk-header__bg, .horizontal2, table.menu {
            z-index: 2 !important;
        }
        .contener, .footer, .horizontal2, table.menu {
            position: relative !important;
        }

        .player-loading-style {
            min-height: 388px;
            display: block;
            background: #000 url(/types/cinema/template/templates/kinogo/images/loader.svg) no-repeat 50% 50%;
            position: relative;
        }
    </style>
    {{-- <link rel="dns-prefetch" href="https://cdn77.aj1907.online">
    <link rel="dns-prefetch" href="https://aj1907.online/">
    <link rel="dns-prefetch" href="https://counter.yadro.ru">
    <link rel="dns-prefetch" href="https://pimg.vb24132nightdwellers.com"> --}}
</head>

<body style="background: #141414;">

    {{-- рекламка --}}
    {{-- <ins class="604c7625" data-key="11a282ad7ff3acafdd59e7101c77d0df" data-cp-host="e4ddeac60ea001c63fb7fa9ce27a77c0|1|kinogo.inc"></ins> --}}
    <div id="trickorlife"></div>

    <style>
        @media (max-width: 768px) {
            div[id*=epom].epom-brend {
                margin-top: 34px!important;
            }
        }
    </style>

    <style>
        @media (max-width: 768px) {
            body > #trickorlife {
                margin-bottom: 0px;
            }
            .wrapper {
                display:flex;
                flex-direction:column;
            }

            .header {position:relative; order:1; height:auto; margin:0 0 20px 0; z-index:1;}
            .contener {order:2;}
            .footer {order:3;}

            .leftblok_contener2.open {line-height:160%;}
        }
    </style>

    <div class="wrapper" x-data="{ isMobileMenuVisible: false }">
        <div class="contener">
            <div class="contener2">
                <div class="content">
                    <div class="funct">
                        <div class="speedbar lines">
                            <span id="dle-speedbar" itemscope itemtype="https://schema.org/BreadcrumbList">
                                <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                    <meta itemprop="position" content="1">
                                    <a href="{{ $domain->toRoute->index() }}" itemprop="item"><span itemprop="name">{{ $domain->site_name }} - {{ $requestedDomain->domain }}</span></a>
                                </span>
                                {{ $speedbar ?? null }}
                            </span>
                        </div>
                    </div>

                    <div id="dle-content">
                        {{ $slot }}
                    </div>

                    <div class="pomoshnik"></div>
                </div>

                <div class="leftblok_contener">
                    <div class="dmobile" style="padding: 7px 20px;position: relative;border-bottom: 1px solid #3f413f;"><i style="font-size:15px;"> Навигация</i></div>

                    <!--filter-->

                    <div class="leftblok_contener2" id="menu-head" :class="isMobileMenuVisible ? 'open' : ''">
                        <div class="leftblok1">
                            <div class="miniblock">

                                <div class="mini" style="border-top:0px; padding-top:0px;">

                                    <i style="font-size:14px;">Жанры</i><br>
                                    @foreach ($genres as $group)
                                    <a href="{{ $domain->toRoute->groupView($group) }}">{{ \Str::title($group->plural) }}</a><br>
                                    @endforeach
                                </div>

                                <div style="padding-top:20px;"></div>
                            </div>
                        </div>

                        <div class="leftblok2">
                            <div class="miniblock">
                                <div class="mini" style="border-top:0px; padding-top:0px;">
                                    <i style="font-size:14px;">По году</i><br>
                                    @foreach($lastYearsGroups as $group)
                                    <a href="{{ $domain->toRoute->groupView($group) }}">{{ $group->name }} года</a><br>
                                    @endforeach
                                    <div class="poloska"></div>
                                </div>

                                <div class="mini">
                                    <i style="font-size:14px;">По странам</i><br>

                                    @foreach($countryGroups as $group)
                                    <a href="{{ $domain->toRoute->groupView($group) }}">{{ $group->plural }}</a><br>
                                    @endforeach

                                    <div class="poloska"></div>
                                </div>

                                <div class="mini">
                                    <i style="font-size:14px;">Сериалы</i><br>
                                    @foreach ($showCountryGroups as $group)
                                    <a href="{{ $domain->toRoute->groupView($group) }}">{{ $group->name }}</a><br>
                                    @endforeach
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="poloska_bloka"></div>

                    <div style="padding: 7px 20px;position: relative;border-bottom: 1px solid #3f413f;"><i style="font-size:15px;">Скоро в кино</i></div>

                    <div class="mimiserblock">
                        <ul>
                            @foreach ($mostWanted as $domainMovie)
                            <li class="li_block" >
                                <div class="li_block_title">
                                    <a href="{{ $domain->toRoute->movieView($domainMovie) }}">
                                        <img loading="lazy" src="/types/cinema/template/templates/kinogo/images/noposter.jpg" width="83" height="118" data-src="{{ $domainMovie->movie->poster_or_placeholder }}" alt="{{ $domainMovie->movie->title }}" style="float:left;">
                                        {{ $domainMovie->movie->title }}
                                    </a>
                                </div>
                                <!--noindex-->
                                <div class="blockstory">
                                    {{ \Str::limit($domainMovie->movie->description ?? '', 90) }}<br /><br />
                                    Год: {{ $domainMovie->movie->year }}<br />
                                    Страна: {{ $domainMovie->movie->tags->where('type', 'country')->pluck('tag')->implode(', ') }}
                                </div>
                                <!--/noindex-->
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="poloska_bloka"></div>

                    <div style="padding: 7px 0 7px 20px;position: relative;border-bottom: 1px solid #3f413f;"><i style="font-size:15px;">Обновления сериалов</i></div>
                    <div class="mimiserblock">
                        <ul style="overflow: hidden!important;">
                            @foreach ($lastEpisodes as $domainMovie)
                            <li class="li_serial" style="width: 126px!important;">
                                <a href="{{ $domain->toRoute->movieView($domainMovie) }}" title="{{ $domainMovie->movie->title }}" style="color:#fff;">
                                    <div class="lenta22" style="right: 12px;!important;">
                                        <div class="edge-left22"></div>
                                        <div class="cont22">{{ $domainMovie->episodes->first()->season }} сезон {{ $domainMovie->episodes->first()->episode }} серия</div>
                                    </div>
                                    <img loading="lazy" src="/types/cinema/template/templates/kinogo/images/noposter.jpg" data-src="{{ $domainMovie->movie->poster_or_placeholder }}" width="133" height="185" alt="{{ $domainMovie->movie->title }}">
                                    <br />
                                    {{ $domainMovie->movie->title }}
                                    <br/>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="poloska_bloka"></div>

                    <div style="padding: 7px 20px;border-bottom: 1px solid #3f413f;"><i style="font-size:15px;">Последние комментарии</i></div>

                    <div style="border-top: 1px solid #848480; padding-top: 15px;">
                        <ul class="lastcomm">
                            @foreach ($lastComments as $comment)
                            <li class="lcomm">
                                <a href="{{ $domain->toRoute->movieView($comment->domainMovie) }}#comment" style="text-decoration:none;">
                                    <div class="lc-body">
                                        <div class="lc-author"><b>{{ $comment->username }}</b></div>
                                        <div class="lc-text">{{ $comment->text }}</div>
                                    </div>
                                    <img src="/types/cinema/template/templates/kinogo/images/noavatar2.png" width="40" height="40" alt="0000aaaa"/>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="header" style="width: 100%;">
            <div class="header55">
                <div class="leftblok1_header">
                    <div class="navigation">
                        <div class="topmenu"></div>

                        <div class="deepnavigation">
                            <table class="menu">
                                <tbody>
                                    <tr>
                                        <td class="item">
                                            <a href="{{ $domain->toRoute->index() }}" rel="nofollow">
                                                <span class="name" style="color:#fff;">Главная</span>
                                            </a>
                                        </td>
                                        <td class="item">
                                            <a href="{{ $domain->toRoute->groupView($lastYearFilmsGroup) }}" style="background: #8b3b32!important;">
                                                <span class="name" style="color:#fff;">Новинки</span>
                                            </a>
                                        </td>
                                        <td class="item">
                                            <a href="{{ $domain->toRoute->groupView($foreignShowsGroup) }}" >
                                                <span class="name" style="color:#fff;">Сериалы</span>
                                            </a>
                                        </td>
                                        {{-- TODO стол заказов --}}
                                        {{-- <td class="item">
                                            <a href="/index.php?do=orderdesc" rel="nofollow">
                                                <span class="name" style="color:#fff;">Стол заказов</span>
                                            </a>
                                        </td> --}}
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div style="clear:both"></div>

            <div class="horizontal2">
                <div class="horizontal">
                    <!--noindex-->
                    <div class="carousel">
                        <span class="carousel_prev carousel_left">prev</span>
                        <div class="carousel_container">
                            <ul class="portfolio_items">
                                @foreach ($topMovies as $domainMovie)
                                <li class="visible_1">
                                    <div class="inner">
                                        <a href="{{ $domain->toRoute->movieView($domainMovie) }}" title="({{ $domainMovie->movie->year }})">
                                            <img loading="lazy" src="/types/cinema/template/templates/kinogo/images/noposter.jpg" data-src="{{ $domainMovie->movie->poster_or_placeholder }}" style="width:118px;height:174px;" alt="({{ $domainMovie->movie->year }})" />
                                        </a>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <span class="carousel_next carousel_right">next</span>
                    </div>
                    <!--/noindex-->
                </div>
            </div>
        </div>

        <div class="footer" style="padding:0px!important;">

            <div class="footer-text">
                {{ $bottomTextExtra ?? '' }}
                @if($headers->bottomText)
                {!! nl2br(e($headers->bottomText)) !!}
                @endif
            </div>

            <div style="float:left;" class="partners">
                <!--noindex-->
                <!--/noindex-->
            </div>
            <div class="foot-mobile">
                <span style="float:left; padding-top: 12px; padding-left:3px; ">
                    <a href="/" style="font-weight:bold;">Правообладателям</a> •
                    <a href="/index.php?do=feedback" rel="nofollow">Контакты</a>
                </span>
                <span style="float:right; padding-top: 12px; padding-left:3px; "><a href="{{ $domain->toRoute->index() }}" style="font-weight:bold;">{{ ucfirst($requestedDomain->domain) }}</a></span>
            </div>
            <div style="float:right;  text-align: right; width:300px; padding-top: 3px;"></div>
        </div>

        <div class="blk-header__bg" style="height: 33px;background-color: #000;width: 100%;position: fixed;left: 0;right: 0;z-index: 999999;top: 0px;">
            <div class="wrap wrapnew" style="margin: 5px auto; width: 976px ">
                <div class="blk_header" style="display: flex;  align-items: center;">
                    <div id="in-toolbar" style="width: 45px;float: right;right: 0;display: block;position: absolute;border-bottom: none!important;">
                        <span id="menu-btn" x-on:click="isMobileMenuVisible = !isMobileMenuVisible"><span id="hamburger"></span></span>
                    </div>
                    <div class="desk_logo" style="flex:1;">
                        <a href="{{ $domain->toRoute->index() }}" style="line-height: 0;display: block;" aria-label="Перейти на главную страницу киного">
                            <svg width="378pt" height="82pt" style="height:17px;width:90px" version="1.1" viewBox="0 0 378 82" xmlns="http://www.w3.org/2000/svg">
                                <g transform="translate(0 82) scale(.1 -.1)" fill="#fff">
                                    <path d="m1859 724c-64-19-141-91-176-162-24-50-27-69-27-157 0-87 3-107 25-155 31-66 85-121 148-152 39-19 62-23 146-23s107 4 146 23c148 72 213 246 160 422-19 62-31 82-79 130-39 39-73 62-106 73-60 21-173 21-237 1zm186-73c77-35 129-134 128-246-2-132-60-230-153-255-133-36-241 79-241 255 0 87 24 158 70 204 59 59 127 74 196 42z"></path>
                                    <path d="m2600 731c-90-28-155-86-196-176-24-51-28-73-28-146 0-77 3-93 32-152 40-80 102-138 181-168 45-17 72-20 136-17 93 5 153 28 214 83l41 37v119 120l-137-3-138-3v-35-35l78-3 77-3v-84c0-84 0-85-30-100-107-55-241-7-303 109-18 34-22 57-22 141s3 107 22 142c55 102 182 138 293 83 45-23 69-50 85-97 11-32 18-38 45-41l33-3-5 45c-10 85-80 159-174 185-44 12-166 13-204 2z"></path>
                                    <path d="m3272 726c-184-60-268-297-174-489 54-110 152-167 287-167 136 0 236 63 290 182 25 55 29 75 29 153 0 76-5 99-28 151-31 69-80 122-145 155-56 29-193 36-259 15zm184-77c48-23 98-89 115-153 19-71 7-184-27-246-73-136-226-144-311-17-115 171-22 430 156 436 14 1 44-8 67-20z"></path>
                                    <path d="m100 405v-325h60 60v145c0 80 3 145 8 144 4 0 61-65 127-144l120-144h78c43-1 76 2 74 7-3 4-71 86-151 182l-145 174 72 76c40 41 100 104 134 140l62 65-47 3-47 4-135-141c-74-78-138-141-142-141-5 0-8 63-8 140v140h-60-60v-325z"></path>
                                    <path d="m720 405v-325h60 60v325 325h-60-60v-325z"></path>
                                    <path d="m970 405v-325h40 40v276 275l118-153c65-84 160-208 211-276 91-119 95-122 132-122h39v325 325h-40-40l-2-217-3-217-165 217-165 216-82 1h-83v-325z"></path>
                                </g>
                            </svg>
                        </a>
                    </div>
                    <script>
                        function switchLight() {
                            if (localStorage.getItem('theme') === 'light') {
                                document.querySelector('body').classList.add('lt-is-active');
                            } else {
                                document.querySelector('body').classList.remove('lt-is-active');
                            };
                        };
                        switchLight();
                    </script>

                    {{-- TODO регистрация --}}
                    {{-- <div class="login login--not-logged d-none" id="openwindow" style="display:none;">
                        <div class="login__header d-flex jc-space-between ai-center">
                            <div class="login__title stretch-free-width ws-nowrap">Войти <a href="/?do=register">Регистрация</a></div>
                            <div class="login__close" onclick="openbox('openwindow'); return false"><span class="fas fa-times"></span></div>
                        </div>
                        <form method="post">
                            <div class="login__content">
                                <div class="login__row">
                                    <div class="login__caption">Логин:</div>
                                    <div class="login__input"><input type="text" name="login_name" id="login_name" placeholder="Ваш логин"/></div>
                                    <span class="fas fa-user"></span>
                                </div>
                                <div class="login__row">
                                    <div class="login__caption">Пароль: <a href="https://kinogo.inc/index.php?do=lostpassword">Забыли пароль?</a></div>
                                    <div class="login__input"><input type="password" name="login_password" id="login_password" placeholder="Ваш пароль" /></div>
                                    <span class="fas fa-lock"></span>
                                </div>
                                <label class="login__row checkbox" for="login_not_save">
                                    <input type="checkbox" name="login_not_save" id="login_not_save" value="1"/>
                                    <span>Не запоминать меня</span>
                                </label>
                                <div class="login__row">
                                    <button onclick="submit();" type="submit" title="Вход">Войти на сайт</button>
                                    <input name="login" type="hidden" id="login" value="submit" />
                                </div>
                            </div>

                        </form>
                    </div> --}}

                    <div class="visible-tablet-mobile" x-data="{ isMobileSearchVisible: false }">
                        {{-- TODO вход на сайт --}}
                        <div style="margin:0 40px 0 0; display: flex; align-items: center; gap: 5px;">
                            {{-- <span style="text-decoration: underline;cursor: pointer;color: #fff;" onclick="openbox('openwindow'); return false">Вход</span> / <a rel="nofollow" href="/index.php?do=register">Регистрация</a> --}}
                            <div class="desk_login-group5" style="line-height: 1;"></div>
                            <div class="open-search" x-on:click="isMobileSearchVisible = !isMobileSearchVisible"></div>
                        </div>

                        <div class="desk_search" id="desk_search-mobile" style="position: absolute;display: none;float: right;margin-top: 3px" x-bind:style="{ 'display': isMobileSearchVisible ? 'block' : 'none' }">
                            <livewire:types.cinema.dropdown-search :$domain template="livewire.types.cinema.templates.kinogo.dropdown-search" />
                        </div>
                    </div>

                    <style>
                        @media (max-width: 1000px) {
                            .search_login {
                                display: none!important;
                            }
                        }
                    </style>

                    <div class="search_login" style="display: flex;gap: 10px;align-items: center;">
                        {{-- <div class="desk_login" style="display: flex;align-items: center;gap:6px;">
                            <script type="text/javascript">
                                function change(idName) {
                                    if (document.getElementById(idName).style.display == 'none') {
                                        document.getElementById(idName).style.display = '';
                                    } else {
                                        document.getElementById(idName).style.display = 'none';
                                    }
                                    return false;
                                }
                            </script>
                            <span style="text-decoration: underline;cursor: pointer;color: #fff;" onclick="openbox('openwindow'); return false">Вход</span> / <a rel="nofollow" href="/index.php?do=register">Регистрация</a>
                            <div class="desk_login-group5" style="line-height: 1;"></div>
                            <div class="open-search" onclick="openbox('desk_search-mobile'); return false"></div>



                        </div> --}}
                        <div class="desk_search not">
                            <livewire:types.cinema.dropdown-search :$domain template="livewire.types.cinema.templates.kinogo.dropdown-search" />
                        </div>

                        {{-- <div class="desk_search not">
                            <form class="js-lightsearch lightsearch" action="/" name="searchform" method="post">
                                <input name="titleonly" value="3" type="hidden">
                                <input type="hidden" name="do" value="search">
                                <input type="hidden" name="subaction" value="search">
                                <input id="story" name="story" type="text" class="js-lightsearch-input lightsearch-input" autocomplete="off" placeholder="Поиск..." value="">
                                <button type="button" class="js-lightsearch-reset lightsearch-reset lightsearch-btn" title="Очистить поле ввода" hidden=""></button>
                                <button type="submit" class="lightsearch-btn">ok</button>
                            </form>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- убираю скрипты --}}
    {{-- <script type="text/javascript" src="/types/cinema/template/templates/kinogo/js/jquery.js" defer></script> --}}
    {{-- <script type="text/javascript" src="/types/cinema/template/templates/kinogo/js/dle_js.js" defer></script> --}}
    {{ $scripts ?? null }}

    {{-- убираю скрипты --}}
    {{-- <script type="text/javascript">
        var dle_root       = '/';
        var dle_admin      = '';
        var dle_login_hash = '';
        var dle_group      = 5;
        var dle_skin       = 'tmp-new';
        var dle_wysiwyg    = '-1';
        var quick_wysiwyg  = '1';
        var dle_act_lang   = ["Да", "Нет", "Ввод", "Отмена", "Сохранить", "Удалить", "Загрузка. Пожалуйста, подождите..."];
        var menu_short     = 'Быстрое редактирование';
        var menu_full      = 'Полное редактирование';
        var menu_profile   = 'Просмотр профиля';
        var menu_send      = 'Отправить сообщение';
        var menu_uedit     = 'Админцентр';
        var dle_info       = 'Информация';
        var dle_confirm    = 'Подтверждение';
        var dle_prompt     = 'Ввод информации';
        var dle_req_field  = 'Заполните все необходимые поля';
        var dle_del_agree  = 'Вы действительно хотите удалить? Данное действие невозможно будет отменить';
        var dle_spam_agree = 'Вы действительно хотите отметить пользователя как спамера? Это приведёт к удалению всех его комментариев';
        var dle_complaint  = 'Укажите текст Вашей жалобы для администрации:';
        var dle_big_text   = 'Выделен слишком большой участок текста.';
        var dle_orfo_title = 'Укажите комментарий для администрации к найденной ошибке на странице';
        var dle_p_send     = 'Отправить';
        var dle_p_send_ok  = 'Уведомление успешно отправлено';
        var dle_save_ok    = 'Изменения успешно сохранены. Обновить страницу?';
        var dle_reply_title= 'Ответ на комментарий';
        var dle_tree_comm  = '0';
        var dle_del_news   = 'Удалить статью';
        var dle_sub_agree  = 'Вы действительно хотите подписаться на комментарии к данной публикации?';
        var allow_dle_delete_news   = false;
        var dle_search_delay   = false;
        var dle_search_value   = '';
    </script> --}}
    {{-- <script type="text/javascript" src="/types/cinema/template/templates/kinogo/js/lib.js" defer></script>  --}}

    {{-- метрику тут --}}

    {{-- убираю скрипты --}}
    {{-- <script type="text/javascript" src="/types/cinema/template/templates/kinogo/js/mylists.js" defer></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){ MyLists = new MyLists(false, '{"_1_":["\u0421\u043c\u043e\u0442\u0440\u044e","smotru"],"_2_":["\u041f\u043e\u0441\u043c\u043e\u0442\u0440\u0435\u0442\u044c \u043f\u043e\u0442\u043e\u043c","na-buduschee"],"_3_":["\u0421\u043c\u043e\u0442\u0440\u0435\u043b","smotrel"],"_4_":["\u041d\u0435 \u043f\u043e\u043d\u0440\u0430\u0432\u0438\u043b\u043e\u0441\u044c","ne-ponravilos"]}'); });
    </script> --}}

    {{-- убираю скрипты --}}
    {{-- <script type="text/javascript">
        function openbox(id){
            display = document.getElementById(id).style.display;

            if(display=='none'){
                document.getElementById(id).style.display='block';
            }else{
                document.getElementById(id).style.display='none';
            }
        }

        document.oncopy = function() {
            var body = document.getElementsByTagName('body')[0];
            var selection = window.getSelection();
            var div = document.createElement('div');

            div.style.position = 'absolute';
            div.style.left = '-99999px';
            body.appendChild(div);
            div.innerHTML = selection + ' Источник: ' + window.location.href;
            selection.selectAllChildren(div);

            window.setTimeout(function(){
                body.removeChild(div);
            }, 0);
        }
    </script> --}}

    <x-types.engine.lazy-load-script />

    <x-types.engine.metrika />

    {{-- рекламка --}}
    {{-- <script src="//cdn77.aj1907.online/63c0d7d8.js" async></script> --}}
</body>
</html>
