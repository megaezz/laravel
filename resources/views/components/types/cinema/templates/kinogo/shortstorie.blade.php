<div class="shortstory">
    <div class="shortstorytitle">
        <div style="width: 120px;float: right;">
            <span class="izbrannoe"> <span class="favoriticon"></span> </span>
            <div class="podrobnosti_right">
                <div class="rating">
                    <ul class="unit-rating">
                        <li class="current-rating" style="width:{{ round((float)$domainMovie->movie->rating_sko, 1) * 10 }}%;">
                            {{ round((float)$domainMovie->movie->rating_sko, 1) * 10 }}
                        </li>
                    </ul>
                    <div class="rating_digits">
                        <div class="rating_digits_1">
                            Рейтинг: <span itemprop="ratingValue">{{ round((float)$domainMovie->movie->rating_sko, 1) }}</span>/10
                            (<span itemprop="ratingCount">{{ round(((int)$domainMovie->movie->kinopoisk_votes + (int)$domainMovie->movie->imdb_votes) / 2) }}</span>)
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h2 class="zagolovki">
            <a href="{{ $domain->toRoute->movieView($domainMovie) }}">
                {{ $domainMovie->movie->title }}
                @if($domainMovie->movie->year) ({{ $domainMovie->movie->year }}) @endif
            </a>
        </h2>
    </div>
    <div class="shortimg">
        <div id="{{ $domainMovie->id }}" style="display:inline;">
            <div class="poster-br">
                <a href="{{ $domain->toRoute->movieView($domainMovie) }}">
                    <picture>   
                        <source src="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder_webp }}" type="image/webp">
                        <img src="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder }}" alt="{{ $domainMovie->movie->title }} {{ $domainMovie->movie->year }}" title="{{ $domainMovie->movie->title }} {{ $domainMovie->movie->year }}">
                    </picture>
                    {{-- отключил loading="lazy", т.к. не дружит c livewire --}}
                    {{-- <img loading="lazy" src="/types/cinema/template/templates/kinogo/images/noposter.jpg" data-src="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder }}" alt="{{ $domainMovie->movie->title }} {{ $domainMovie->movie->year }}" title="{{ $domainMovie->movie->title }} {{ $domainMovie->movie->year }}"> --}}
                </a>
            </div>

            <!--noindex-->
            {{ \Str::limit($domainMovie->movie->description ?? '', 300) }}
            <br><br> 
            <b>Год:</b> {{ $domainMovie->movie->year }}<br>
            <b>Страна:</b> {{ $domainMovie->movie->tags->where('type', 'country')->pluck('tag')->implode(', ') }}<br>
            <b>Жанр:</b>
            @foreach ($domainMovie->movie->groups->where('type', 'genre')->all() as $group)
            <a href="{{ $domain->toRoute->groupView($group) }}">{{ $group->name }}</a>@if (!$loop->last), @endif
            @endforeach
            <br>
            {{-- TODO: продолжительность откуда-то брать --}}
            {{-- <b>Продолжительность:</b> 27 мин.<br> --}}
            {{-- TODO: качество откуда-то брать --}}
            {{-- <b>Качество:</b> Трейлер<br> --}}
            <b>В ролях:</b> {{ $domainMovie->movie->tags->where('type', 'actor')->take(10)->pluck('tag')->implode(', ') }}
            <br><br>
            <!--/noindex-->
        </div>
    </div>
</div>

<div class="icons">
    <span class="podrobnee pseudo-link" style="padding: 4px 0 4px 20px;" data-link="{{ $domain->toRoute->movieView($domainMovie) }}">
        <span class="fbutton-img">
            <img src="/types/cinema/template/templates/kinogo/images/viewmore.png" width="102" height="16" alt="смотреть {{ $domainMovie->movie->title }}">
        </span>
    </span>  
    
    <span style="float:right; padding: 7px 20px 19px 20px;">
        <span class="dateicon" title="Дата">
            @if($domainMovie->add_date->isToday())
            Сегодня, {{ $domainMovie->add_date->format('H:i') }}
            @elseif($domainMovie->add_date->isYesterday())
            Вчера, {{ $domainMovie->add_date->format('H:i') }}
            @else
            {{ $domainMovie->add_date->format('d-m-Y, H:i') }}
            @endif
        </span> 
        
    </span>
</div>