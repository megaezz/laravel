<div class="col-6 col-lg-3 sameMovie text-center mb-3">
	<a href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}">
		<picture>   
			<source data-srcset="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder_webp }}" type="image/webp">   
			<img data-srcset="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder }}" alt="{{ $domainMovie->movie->title }}" class="lazyload"> 
		</picture>
		<div class="title">{{ $domainMovie->movie->title }}</div>
	</a>
</div>