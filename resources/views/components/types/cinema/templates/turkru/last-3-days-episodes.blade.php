@foreach ($days as $day)
<div class="one-day">
    <div class="caption">
        @if ($day->isToday())
        сегодня
        @elseif ($day->isYesterday())
        вчера
        @else
        {{ $day->isoFormat('dddd') }}
        @endif
        <i></i>
        {{ $day->isoFormat('DD MMMM') }}
    </div>
    @foreach ($domain
        ->movies()
        ->whereHas('videodbs',function($query) use ($day) {
                $query->whereBetween('date',[$day->copy()->startOfDay(),$day->copy()->endOfDay()]);
        })->get() as $movie)
    <div class="day-episodes">
        <!-- TODO: $movie->episodes()->whereHas('videodbs',...) тогда получим объекты MovieEpisode и сможем генерить ссылки -->
        @foreach ($movie->videodbs()->select('movie_id','season','episode')->whereBetween('date',[$day->copy()->startOfDay(),$day->copy()->endOfDay()])->distinct()->get() as $videodb)
        <div class="episode">
            <a href="{{ $domain->engineDomain->toRoute->movieView($movie) }}" class="episode-url">
                <span>
                    <b>&laquo;{{ $movie->movie->title_ru }}&raquo;</b>
                    {{ $videodb->season }} сезон {{ $videodb->episode }} серия
                </span>
            </a>

            @foreach ($videodb->dubbings as $dubbing)
            <!-- TODO роут для дубляжа -->
            <a href="{{ $domain->engineDomain->toRoute->movieView($movie) }}#{{ urlencode($dubbing->translation) }}" class="episode-sound">{{ $dubbing->dubbing_name }}</a>
            @if (!$loop->last)
            <span class="episode-sound">, </span>
            @endif
            @endforeach
        </div>
        @endforeach
    </div>
    @endforeach
</div>
@endforeach