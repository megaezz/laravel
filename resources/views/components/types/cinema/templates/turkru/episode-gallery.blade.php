<div class="secondary frame-gallery base-block-top-offset">
    <h3 class="sub-heading sub-heading__bottom_offset">Кадры</h3>
    <div class="gallery-wrapper" id="episode-gallery" data-label="Кадры из «Летняя песня» 3 серии">
        <script>
            window.episode_gallery = [
                @foreach ($episode->myshows->images as $image_number => $image_path)
                {
                    "id": {{ $image_number }},
                    "url": "{{ str_replace('normal','small',str_replace($image_path,'',$episode->myshows->image)) }}{{ $image_path }}",
                    "x1": "{{ str_replace($image_path,'',$episode->myshows->image) }}{{ $image_path }}",
                    "x2": "{{ str_replace($image_path,'',$episode->myshows->image) }}{{ $image_path }}",
                },
                @endforeach
                ];
/*
            window.episode_gallery = [{
                "id": 310055,
                "url": "\/bitrix_storage\/frames\/0\/310\/310055.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310055.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310055.jpg?v=1"
            }, {
                "id": 310056,
                "url": "\/bitrix_storage\/frames\/0\/310\/310056.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310056.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310056.jpg?v=1"
            }, {
                "id": 310057,
                "url": "\/bitrix_storage\/frames\/0\/310\/310057.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310057.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310057.jpg?v=1"
            }, {
                "id": 310271,
                "url": "\/bitrix_storage\/frames\/0\/310\/310271.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310271.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310271.jpg?v=1"
            }, {
                "id": 310272,
                "url": "\/bitrix_storage\/frames\/0\/310\/310272.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310272.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310272.jpg?v=1"
            }, {
                "id": 310273,
                "url": "\/bitrix_storage\/frames\/0\/310\/310273.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310273.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310273.jpg?v=1"
            }, {
                "id": 310274,
                "url": "\/bitrix_storage\/frames\/0\/310\/310274.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310274.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310274.jpg?v=1"
            }, {
                "id": 310275,
                "url": "\/bitrix_storage\/frames\/0\/310\/310275.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310275.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310275.jpg?v=1"
            }, {
                "id": 310276,
                "url": "\/bitrix_storage\/frames\/0\/310\/310276.jpg",
                "x1": "\/bitrix_storage\/thumbs\/450\/260\/frames\/0\/310\/310276.jpg?v=1",
                "x2": "\/bitrix_storage\/thumbs\/900\/520\/frames\/0\/310\/310276.jpg?v=1"
            }];
*/
        </script>
    </div>
</div>