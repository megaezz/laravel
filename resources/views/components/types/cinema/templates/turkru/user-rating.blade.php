<div class="user-rate-info">
    <div class="item">
        <div class="item-content">
            <div class="label">
               TOTAL 
               <svg data-tippy-content="Главный рейтинг — в нем учитываются абсолютно все зрители. Эти цифры влияют на длительность сериала." data-tippy-mobile-placement="top" data-tippy-placement="right-start" data-tippy-max-width="247">
                <use xlink:href="#icon-info" width="15" height="15"></use>
            </svg>
        </div>
        <div class="value">
            <span class="val">2.54</span>
            <span class="change up">0.13</span>
        </div>
    </div>
</div>
<div class="item">
    <div class="item-content">
        <div class="label">
           AB 
           <svg data-tippy-content="Здесь оценивается количество состоятельных и образованных зрителей сериала." data-tippy-mobile-placement="top" data-tippy-placement="right-start" data-tippy-max-width="247">
            <use xlink:href="#icon-info" width="15" height="15"></use>
        </svg>
    </div>
    <div class="value">
        <span class="val">1.45</span>
        <span class="change down">0.54</span>
    </div>
</div>
</div>
<div class="item">
    <div class="item-content">
        <div class="label">
           ABC1 
           <svg data-tippy-content="В этой группе учитываются молодежь и студенты." data-tippy-mobile-placement="top" data-tippy-placement="right-start" data-tippy-max-width="247">
            <use xlink:href="#icon-info" width="15" height="15"></use>
        </svg>
    </div>
    <div class="value">
        <span class="val">2.18</span>
        <span class="change up">0.30</span>
    </div>
</div>
</div>
</div>