<div class="comments-container">
    <div class="inner-padding">
        <div class="comments-wrapper" data-cackle-id="82721">
            <div class="s-place" data-name="comments" data-id="82901">
                <div class="comments">
                    <meta itemprop="commentCount" content="22">
                    <h3 class="sub-heading">Комментарии</h3>
                    <!--v-if-->
                    <div>
                        <div class="comments-form has-small-view">
                            <!--v-if-->
                            <form class="comments-form__form">
                                <div id="form-content" class="form-item__textarea" data-field>
                                    <textarea rows="1" placeholder="введите комментарий"></textarea>
                                    <div class="file-uploader">
                                        <label>
                                            <input type="file">
                                            <span>
                                                <i class="icon-img"></i>
                                                загрузить изображение 
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <!--v-if-->
                                <div class="comments__soc-auth-block" style="display:none;">
                                    <div class="comments__capture">
                                        <p>отправьте через аккаунт</p>
                                    </div>
                                    <div class="comments-oauth">
                                        <div class="oauth">
                                            <button class="email" data-trigger-login-form></button>
                                            <!--[-->
                                            <!--]-->
                                        </div>
                                    </div>
                                </div>
                                <div class="comments__anonym-block" style="display:none;">
                                    <div class="comments__capture">
                                        <p>или анонимно</p>
                                    </div>
                                    <div class="comments-form-anonym">
                                        <label class="comments-form-anonym__avatar">
                                            <input type="file" accept="image/jpeg, image/png">
                                            <i class="icon-user-heart"></i>
                                        </label>
                                        <div class="p-form-item-text p-form-item">
                                            <div id="form-name" class="p-form-item-text__input" data-field>
                                                <input type="text" placeholder="Ваше имя" value>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="p-button_type_gray-border p-button comments-form__submit" type="submit">
                                    <span>
                                        <span>
                                            <i class="icon-arrow-up-thin"></i>
                                            Отправить
                                        </span>
                                    </span>
                                </button>
                            </form>
                        </div>
                        <div class="comments__list-wrap">
                            <!--[-->
                            <div class="comments__types">
                                <!--[-->
                                <a href="javascript:;" class="button comments__type active">
                                    <span>ЛУЧШИЕ</span>
                                </a>
                                <a href="javascript:;" class="button comments__type">
                                    <span>НОВЫЕ</span>
                                </a>
                                <!--]-->
                            </div>
                            <div class="comments__list">
                                <div class="comments-list">
                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-339715">
                                        <!--v-if-->
                                        <div class="comment">
                                            <div class="comment__header">
                                                <div class="comments-user">
                                                    <div class="comments-user__avatar">
                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/6.svg?4f4ea9023e000f7edb3c" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/6.svg?4f4ea9023e000f7edb3c" sizes="48px">
                                                    </div>
                                                    <div class="comments-user__content">
                                                        <div class="comments-user__top">
                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                            <div class="comments-user__right">
                                                                <meta itemprop="dateCreated" content="2023-07-16T20:52:00.000Z">
                                                                <!--[-->
                                                                <!--]-->
                                                                <div class="comment__date">16 июл. 2023</div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                    </div>
                                                </div>
                                                <div class="comment__more">
                                                    <i class="icon-ellipsis"></i>
                                                    <!--v-if-->
                                                </div>
                                            </div>
                                            <div class="comment__body" itemprop="text">
                                                <p>Просто пушка,очень классный летник🔥</p>
                                                <!--v-if-->
                                            </div>
                                            <div class="comment__footer">
                                                <button class="button-link">
                                                    <i class="icon-message"></i>
                                                    <span>Ответить</span>
                                                </button>
                                                <div class="comment__likes">
                                                    <button class="comment__likes__like-btn">
                                                        <i class="icon-like"></i>
                                                        32
                                                    </button>
                                                    <button class="comment__likes__dislike-btn">
                                                        <i class="icon-dislike"></i>
                                                        4
                                                    </button>
                                                    <meta itemprop="upvoteCount" content="32">
                                                    <meta itemprop="downvoteCount" content="4">
                                                </div>
                                            </div>
                                        </div>
                                        <!--v-if-->
                                        <div class="comment-tree__answers">
                                            <!--[-->
                                            <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-342541">
                                                <meta itemprop="parentItem" content="339715">
                                                <div class="comment">
                                                    <div class="comment__header">
                                                        <div class="comments-user">
                                                            <div class="comments-user__avatar">
                                                                <img src="/types/cinema/template/templates/turkru/assets/app/images/6.svg?4f4ea9023e000f7edb3c" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/6.svg?4f4ea9023e000f7edb3c" sizes="48px">
                                                            </div>
                                                            <div class="comments-user__content">
                                                                <div class="comments-user__top">
                                                                    <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                                    <div class="comments-user__right">
                                                                        <meta itemprop="dateCreated" content="2023-07-24T13:09:40.000Z">
                                                                        <!--[-->
                                                                        <!--]-->
                                                                        <div class="comment__date">24 июл. 2023</div>
                                                                    </div>
                                                                </div>
                                                                <div class="comment__parent-author">
                                                                    <i class="icon-link"></i>
                                                                    Гость
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="comment__more">
                                                            <i class="icon-ellipsis"></i>
                                                            <!--v-if-->
                                                        </div>
                                                    </div>
                                                    <div class="comment__body" itemprop="text">
                                                        <p>Это не пушка,а хлопушка...!))
                                                        сериал пшик))</p>
                                                        <!--v-if-->
                                                    </div>
                                                    <div class="comment__footer">
                                                        <button class="button-link">
                                                            <i class="icon-message"></i>
                                                            <span>Ответить</span>
                                                        </button>
                                                        <div class="comment__likes">
                                                            <button class="comment__likes__like-btn">
                                                                <i class="icon-like"></i>
                                                                1
                                                            </button>
                                                            <button class="comment__likes__dislike-btn">
                                                                <i class="icon-dislike"></i>
                                                                2
                                                            </button>
                                                            <meta itemprop="upvoteCount" content="1">
                                                            <meta itemprop="downvoteCount" content="2">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--v-if-->
                                                <!--v-if-->
                                            </div>
                                            <!--]-->
                                        </div>
                                    </div>
                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-339717">
                                        <!--v-if-->
                                        <div class="comment">
                                            <div class="comment__header">
                                                <div class="comments-user">
                                                    <div class="comments-user__avatar">
                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" sizes="48px">
                                                    </div>
                                                    <div class="comments-user__content">
                                                        <div class="comments-user__top">
                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                            <div class="comments-user__right">
                                                                <meta itemprop="dateCreated" content="2023-07-16T20:57:43.000Z">
                                                                <!--[-->
                                                                <!--]-->
                                                                <div class="comment__date">16 июл. 2023</div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                    </div>
                                                </div>
                                                <div class="comment__more">
                                                    <i class="icon-ellipsis"></i>
                                                    <!--v-if-->
                                                </div>
                                            </div>
                                            <div class="comment__body" itemprop="text">
                                                <p>Надеюсь, что все-таки будет с Муратом) ну ведь не может героиня в сериале сразу влюбиться в кого нужно 🤪</p>
                                                <!--v-if-->
                                            </div>
                                            <div class="comment__footer">
                                                <button class="button-link">
                                                    <i class="icon-message"></i>
                                                    <span>Ответить</span>
                                                </button>
                                                <div class="comment__likes">
                                                    <button class="comment__likes__like-btn">
                                                        <i class="icon-like"></i>
                                                        23
                                                    </button>
                                                    <button class="comment__likes__dislike-btn">
                                                        <i class="icon-dislike"></i>
                                                        3
                                                    </button>
                                                    <meta itemprop="upvoteCount" content="23">
                                                    <meta itemprop="downvoteCount" content="3">
                                                </div>
                                            </div>
                                        </div>
                                        <!--v-if-->
                                        <div class="comment-tree__answers">
                                            <!--[-->
                                            <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-339794">
                                                <meta itemprop="parentItem" content="339717">
                                                <div class="comment">
                                                    <div class="comment__header">
                                                        <div class="comments-user">
                                                            <div class="comments-user__avatar">
                                                                <img src="/types/cinema/template/templates/turkru/assets/app/images/2.svg?4d8d1532845f565a5fb9" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/2.svg?4d8d1532845f565a5fb9" sizes="48px">
                                                            </div>
                                                            <div class="comments-user__content">
                                                                <div class="comments-user__top">
                                                                    <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                                    <div class="comments-user__right">
                                                                        <meta itemprop="dateCreated" content="2023-07-17T07:12:09.000Z">
                                                                        <!--[-->
                                                                        <!--]-->
                                                                        <div class="comment__date">17 июл. 2023</div>
                                                                    </div>
                                                                </div>
                                                                <div class="comment__parent-author">
                                                                    <i class="icon-link"></i>
                                                                    Гость
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="comment__more">
                                                            <i class="icon-ellipsis"></i>
                                                            <!--v-if-->
                                                        </div>
                                                    </div>
                                                    <div class="comment__body" itemprop="text">
                                                        <p>Яз влюблена в Кемаля. Не помню ни один турецкий сериал, где бы главная героиня оставалась без ответной любви своего возлюбленного. Видимо Кемаль перевоспитается из бабского угодника в примерного мужчину и перестанет ради денег и славы идти на все. Мурат внешне конечно по приятнее будет, да и его персонаж более серьезный, ответственный, надежный. Но молоденькие девушки всегда выбирают плохих парней</p>
                                                        <!--v-if-->
                                                    </div>
                                                    <div class="comment__footer">
                                                        <button class="button-link">
                                                            <i class="icon-message"></i>
                                                            <span>Ответить</span>
                                                        </button>
                                                        <div class="comment__likes">
                                                            <button class="comment__likes__like-btn">
                                                                <i class="icon-like"></i>
                                                                15
                                                            </button>
                                                            <button class="comment__likes__dislike-btn">
                                                                <i class="icon-dislike"></i>
                                                                0
                                                            </button>
                                                            <meta itemprop="upvoteCount" content="15">
                                                            <meta itemprop="downvoteCount" content="0">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--v-if-->
                                                <div class="comment-tree__answers">
                                                    <!--[-->
                                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-340353">
                                                        <meta itemprop="parentItem" content="339794">
                                                        <div class="comment">
                                                            <div class="comment__header">
                                                                <div class="comments-user">
                                                                    <div class="comments-user__avatar">
                                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/5.svg?6953c5657ce02b2ac94a" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/5.svg?6953c5657ce02b2ac94a" sizes="48px">
                                                                    </div>
                                                                    <div class="comments-user__content">
                                                                        <div class="comments-user__top">
                                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                                            <div class="comments-user__right">
                                                                                <meta itemprop="dateCreated" content="2023-07-18T18:30:53.000Z">
                                                                                <!--[-->
                                                                                <!--]-->
                                                                                <div class="comment__date">18 июл. 2023</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="comment__parent-author">
                                                                            <i class="icon-link"></i>
                                                                            Гость
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="comment__more">
                                                                    <i class="icon-ellipsis"></i>
                                                                    <!--v-if-->
                                                                </div>
                                                            </div>
                                                            <div class="comment__body" itemprop="text">
                                                                <p>В играх судьбы героиня влюбилась в другого потом.</p>
                                                                <!--v-if-->
                                                            </div>
                                                            <div class="comment__footer">
                                                                <button class="button-link">
                                                                    <i class="icon-message"></i>
                                                                    <span>Ответить</span>
                                                                </button>
                                                                <div class="comment__likes">
                                                                    <button class="comment__likes__like-btn">
                                                                        <i class="icon-like"></i>
                                                                        1
                                                                    </button>
                                                                    <button class="comment__likes__dislike-btn">
                                                                        <i class="icon-dislike"></i>
                                                                        0
                                                                    </button>
                                                                    <meta itemprop="upvoteCount" content="1">
                                                                    <meta itemprop="downvoteCount" content="0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                        <!--v-if-->
                                                    </div>
                                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-340215">
                                                        <meta itemprop="parentItem" content="339794">
                                                        <div class="comment">
                                                            <div class="comment__header">
                                                                <div class="comments-user">
                                                                    <div class="comments-user__avatar">
                                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/5.svg?6953c5657ce02b2ac94a" alt="Алина" srcset="/types/cinema/template/templates/turkru/assets/app/images/5.svg?6953c5657ce02b2ac94a" sizes="48px">
                                                                    </div>
                                                                    <div class="comments-user__content">
                                                                        <div class="comments-user__top">
                                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Алина</div>
                                                                            <div class="comments-user__right">
                                                                                <meta itemprop="dateCreated" content="2023-07-18T11:14:04.000Z">
                                                                                <!--[-->
                                                                                <!--]-->
                                                                                <div class="comment__date">18 июл. 2023</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="comment__parent-author">
                                                                            <i class="icon-link"></i>
                                                                            Гость
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="comment__more">
                                                                    <i class="icon-ellipsis"></i>
                                                                    <!--v-if-->
                                                                </div>
                                                            </div>
                                                            <div class="comment__body" itemprop="text">
                                                                <p>Согласна, тоже Мурат больше нравится)</p>
                                                                <!--v-if-->
                                                            </div>
                                                            <div class="comment__footer">
                                                                <button class="button-link">
                                                                    <i class="icon-message"></i>
                                                                    <span>Ответить</span>
                                                                </button>
                                                                <div class="comment__likes">
                                                                    <button class="comment__likes__like-btn">
                                                                        <i class="icon-like"></i>
                                                                        11
                                                                    </button>
                                                                    <button class="comment__likes__dislike-btn">
                                                                        <i class="icon-dislike"></i>
                                                                        3
                                                                    </button>
                                                                    <meta itemprop="upvoteCount" content="11">
                                                                    <meta itemprop="downvoteCount" content="3">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                        <!--v-if-->
                                                    </div>
                                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-340133">
                                                        <meta itemprop="parentItem" content="339794">
                                                        <div class="comment">
                                                            <div class="comment__header">
                                                                <div class="comments-user">
                                                                    <div class="comments-user__avatar">
                                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" sizes="48px">
                                                                    </div>
                                                                    <div class="comments-user__content">
                                                                        <div class="comments-user__top">
                                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                                            <div class="comments-user__right">
                                                                                <meta itemprop="dateCreated" content="2023-07-18T06:59:04.000Z">
                                                                                <!--[-->
                                                                                <!--]-->
                                                                                <div class="comment__date">18 июл. 2023</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="comment__parent-author">
                                                                            <i class="icon-link"></i>
                                                                            Гость
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="comment__more">
                                                                    <i class="icon-ellipsis"></i>
                                                                    <!--v-if-->
                                                                </div>
                                                            </div>
                                                            <div class="comment__body" itemprop="text">
                                                                <p>В ожидании солнца, госпожа фазилет, запах клубники... может не в каких сериалах, но в этих героиня сначала ьвоа влюблена в одного, потом в другого)</p>
                                                                <!--v-if-->
                                                            </div>
                                                            <div class="comment__footer">
                                                                <button class="button-link">
                                                                    <i class="icon-message"></i>
                                                                    <span>Ответить</span>
                                                                </button>
                                                                <div class="comment__likes">
                                                                    <button class="comment__likes__like-btn">
                                                                        <i class="icon-like"></i>
                                                                        7
                                                                    </button>
                                                                    <button class="comment__likes__dislike-btn">
                                                                        <i class="icon-dislike"></i>
                                                                        1
                                                                    </button>
                                                                    <meta itemprop="upvoteCount" content="7">
                                                                    <meta itemprop="downvoteCount" content="1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                        <div class="comment-tree__answers">
                                                            <!--[-->
                                                            <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-342555">
                                                                <meta itemprop="parentItem" content="340133">
                                                                <div class="comment">
                                                                    <div class="comment__header">
                                                                        <div class="comments-user">
                                                                            <div class="comments-user__avatar">
                                                                                <img src="/types/cinema/template/templates/turkru/assets/app/images/2.svg?4d8d1532845f565a5fb9" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/2.svg?4d8d1532845f565a5fb9" sizes="48px">
                                                                            </div>
                                                                            <div class="comments-user__content">
                                                                                <div class="comments-user__top">
                                                                                    <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                                                    <div class="comments-user__right">
                                                                                        <meta itemprop="dateCreated" content="2023-07-24T13:36:36.000Z">
                                                                                        <!--[-->
                                                                                        <!--]-->
                                                                                        <div class="comment__date">24 июл. 2023</div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="comment__parent-author">
                                                                                    <i class="icon-link"></i>
                                                                                    Гость
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="comment__more">
                                                                            <i class="icon-ellipsis"></i>
                                                                            <!--v-if-->
                                                                        </div>
                                                                    </div>
                                                                    <div class="comment__body" itemprop="text">
                                                                        <p>Ну там они главные герои!А тут не он главный герой.Поэтому,более вероятно,что Кемаль будет 😀</p>
                                                                        <!--v-if-->
                                                                    </div>
                                                                    <div class="comment__footer">
                                                                        <button class="button-link">
                                                                            <i class="icon-message"></i>
                                                                            <span>Ответить</span>
                                                                        </button>
                                                                        <div class="comment__likes">
                                                                            <button class="comment__likes__like-btn">
                                                                                <i class="icon-like"></i>
                                                                                0
                                                                            </button>
                                                                            <button class="comment__likes__dislike-btn">
                                                                                <i class="icon-dislike"></i>
                                                                                0
                                                                            </button>
                                                                            <meta itemprop="upvoteCount" content="0">
                                                                            <meta itemprop="downvoteCount" content="0">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--v-if-->
                                                                <!--v-if-->
                                                            </div>
                                                            <!--]-->
                                                        </div>
                                                    </div>
                                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-340065">
                                                        <meta itemprop="parentItem" content="339794">
                                                        <div class="comment">
                                                            <div class="comment__header">
                                                                <div class="comments-user">
                                                                    <div class="comments-user__avatar">
                                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" alt="Лето" srcset="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" sizes="48px">
                                                                    </div>
                                                                    <div class="comments-user__content">
                                                                        <div class="comments-user__top">
                                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Лето</div>
                                                                            <div class="comments-user__right">
                                                                                <meta itemprop="dateCreated" content="2023-07-17T22:21:53.000Z">
                                                                                <!--[-->
                                                                                <!--]-->
                                                                                <div class="comment__date">17 июл. 2023</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="comment__parent-author">
                                                                            <i class="icon-link"></i>
                                                                            Гость
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="comment__more">
                                                                    <i class="icon-ellipsis"></i>
                                                                    <!--v-if-->
                                                                </div>
                                                            </div>
                                                            <div class="comment__body" itemprop="text">
                                                                <p>Сюжет  похож на сериал &quot; Любовь моей жизни&quot; 2016 года с Ханде Догандемир и Серкан Чайоглу. 

                                                                Там тоже героиня была влюблена в своего начальника, так что висели плакаты)) ну и там тоже был современный офис, рекламное агентство, а герой был бабник, им всем восторгались и т. п. Ну, конечно, он перевоспитался)</p>
                                                                <!--v-if-->
                                                            </div>
                                                            <div class="comment__footer">
                                                                <button class="button-link">
                                                                    <i class="icon-message"></i>
                                                                    <span>Ответить</span>
                                                                </button>
                                                                <div class="comment__likes">
                                                                    <button class="comment__likes__like-btn">
                                                                        <i class="icon-like"></i>
                                                                        2
                                                                    </button>
                                                                    <button class="comment__likes__dislike-btn">
                                                                        <i class="icon-dislike"></i>
                                                                        0
                                                                    </button>
                                                                    <meta itemprop="upvoteCount" content="2">
                                                                    <meta itemprop="downvoteCount" content="0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                        <!--v-if-->
                                                    </div>
                                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-339910">
                                                        <meta itemprop="parentItem" content="339794">
                                                        <div class="comment">
                                                            <div class="comment__header">
                                                                <div class="comments-user">
                                                                    <div class="comments-user__avatar">
                                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/5.svg?6953c5657ce02b2ac94a" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/5.svg?6953c5657ce02b2ac94a" sizes="48px">
                                                                    </div>
                                                                    <div class="comments-user__content">
                                                                        <div class="comments-user__top">
                                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                                            <div class="comments-user__right">
                                                                                <meta itemprop="dateCreated" content="2023-07-17T14:27:05.000Z">
                                                                                <!--[-->
                                                                                <!--]-->
                                                                                <div class="comment__date">17 июл. 2023</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="comment__parent-author">
                                                                            <i class="icon-link"></i>
                                                                            Гость
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="comment__more">
                                                                    <i class="icon-ellipsis"></i>
                                                                    <!--v-if-->
                                                                </div>
                                                            </div>
                                                            <div class="comment__body" itemprop="text">
                                                                <p>Госпожа Фазилет, там так было</p>
                                                                <!--v-if-->
                                                            </div>
                                                            <div class="comment__footer">
                                                                <button class="button-link">
                                                                    <i class="icon-message"></i>
                                                                    <span>Ответить</span>
                                                                </button>
                                                                <div class="comment__likes">
                                                                    <button class="comment__likes__like-btn">
                                                                        <i class="icon-like"></i>
                                                                        4
                                                                    </button>
                                                                    <button class="comment__likes__dislike-btn">
                                                                        <i class="icon-dislike"></i>
                                                                        1
                                                                    </button>
                                                                    <meta itemprop="upvoteCount" content="4">
                                                                    <meta itemprop="downvoteCount" content="1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                        <!--v-if-->
                                                    </div>
                                                    <!--]-->
                                                </div>
                                            </div>
                                            <!--]-->
                                        </div>
                                    </div>
                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-339933">
                                        <!--v-if-->
                                        <div class="comment">
                                            <div class="comment__header">
                                                <div class="comments-user">
                                                    <div class="comments-user__avatar">
                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" alt="Гость" srcset="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" sizes="48px">
                                                    </div>
                                                    <div class="comments-user__content">
                                                        <div class="comments-user__top">
                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Гость</div>
                                                            <div class="comments-user__right">
                                                                <meta itemprop="dateCreated" content="2023-07-17T16:43:43.000Z">
                                                                <!--[-->
                                                                <!--]-->
                                                                <div class="comment__date">17 июл. 2023</div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                    </div>
                                                </div>
                                                <div class="comment__more">
                                                    <i class="icon-ellipsis"></i>
                                                    <!--v-if-->
                                                </div>
                                            </div>
                                            <div class="comment__body" itemprop="text">
                                                <p>Вот этот  сериал можно назвать комедияй Я смеялись 😂</p>
                                                <!--v-if-->
                                            </div>
                                            <div class="comment__footer">
                                                <button class="button-link">
                                                    <i class="icon-message"></i>
                                                    <span>Ответить</span>
                                                </button>
                                                <div class="comment__likes">
                                                    <button class="comment__likes__like-btn">
                                                        <i class="icon-like"></i>
                                                        26
                                                    </button>
                                                    <button class="comment__likes__dislike-btn">
                                                        <i class="icon-dislike"></i>
                                                        6
                                                    </button>
                                                    <meta itemprop="upvoteCount" content="26">
                                                    <meta itemprop="downvoteCount" content="6">
                                                </div>
                                            </div>
                                        </div>
                                        <!--v-if-->
                                        <!--v-if-->
                                    </div>
                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-340240">
                                        <!--v-if-->
                                        <div class="comment">
                                            <div class="comment__header">
                                                <div class="comments-user">
                                                    <div class="comments-user__avatar">
                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/3.svg?e681f0d29c4297e6f659" alt="Elle" srcset="/types/cinema/template/templates/turkru/assets/app/images/3.svg?e681f0d29c4297e6f659" sizes="48px">
                                                    </div>
                                                    <div class="comments-user__content">
                                                        <div class="comments-user__top">
                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Elle</div>
                                                            <div class="comments-user__right">
                                                                <meta itemprop="dateCreated" content="2023-07-18T12:53:17.000Z">
                                                                <!--[-->
                                                                <!--]-->
                                                                <div class="comment__date">18 июл. 2023</div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                    </div>
                                                </div>
                                                <div class="comment__more">
                                                    <i class="icon-ellipsis"></i>
                                                    <!--v-if-->
                                                </div>
                                            </div>
                                            <div class="comment__body" itemprop="text">
                                                <p>Очень лёгкий и романтичный сериал! Так круто, что глав герой сразу раскусил &quot;псевдоподружку&quot;, а не слушал ее 50 серий!) Надеюсь только, что разборок старших поколений будет в меру и основная тенденция все таки на романтику и всякие молодежные авантюры.
                                                Хотя вот все таки с клипом могли обыграть более романтично, создать волшебную обстановку, а все как то быстрее быстрее.</p>
                                                <!--v-if-->
                                            </div>
                                            <div class="comment__footer">
                                                <button class="button-link">
                                                    <i class="icon-message"></i>
                                                    <span>Ответить</span>
                                                </button>
                                                <div class="comment__likes">
                                                    <button class="comment__likes__like-btn">
                                                        <i class="icon-like"></i>
                                                        14
                                                    </button>
                                                    <button class="comment__likes__dislike-btn">
                                                        <i class="icon-dislike"></i>
                                                        2
                                                    </button>
                                                    <meta itemprop="upvoteCount" content="14">
                                                    <meta itemprop="downvoteCount" content="2">
                                                </div>
                                            </div>
                                        </div>
                                        <!--v-if-->
                                        <!--v-if-->
                                    </div>
                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-339706">
                                        <!--v-if-->
                                        <div class="comment">
                                            <div class="comment__header">
                                                <div class="comments-user">
                                                    <div class="comments-user__avatar">
                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/4.svg?4f4df1075504b3e15db1" alt="Агент -007" srcset="/types/cinema/template/templates/turkru/assets/app/images/4.svg?4f4df1075504b3e15db1" sizes="48px">
                                                    </div>
                                                    <div class="comments-user__content">
                                                        <div class="comments-user__top">
                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Агент -007</div>
                                                            <div class="comments-user__right">
                                                                <meta itemprop="dateCreated" content="2023-07-16T20:23:04.000Z">
                                                                <!--[-->
                                                                <!--]-->
                                                                <div class="comment__date">16 июл. 2023</div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                    </div>
                                                </div>
                                                <div class="comment__more">
                                                    <i class="icon-ellipsis"></i>
                                                    <!--v-if-->
                                                </div>
                                            </div>
                                            <div class="comment__body" itemprop="text">
                                                <p>Что за мама такая дикая и наглая! Приехала с пушкой и всем угрожает!!!</p>
                                                <!--v-if-->
                                            </div>
                                            <div class="comment__footer">
                                                <button class="button-link">
                                                    <i class="icon-message"></i>
                                                    <span>Ответить</span>
                                                </button>
                                                <div class="comment__likes">
                                                    <button class="comment__likes__like-btn">
                                                        <i class="icon-like"></i>
                                                        10
                                                    </button>
                                                    <button class="comment__likes__dislike-btn">
                                                        <i class="icon-dislike"></i>
                                                        0
                                                    </button>
                                                    <meta itemprop="upvoteCount" content="10">
                                                    <meta itemprop="downvoteCount" content="0">
                                                </div>
                                            </div>
                                        </div>
                                        <!--v-if-->
                                        <!--v-if-->
                                    </div>
                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-339707">
                                        <!--v-if-->
                                        <div class="comment">
                                            <div class="comment__header">
                                                <div class="comments-user">
                                                    <div class="comments-user__avatar">
                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" alt="V - 3" srcset="/types/cinema/template/templates/turkru/assets/app/images/1.svg?8868642c41af3622260b" sizes="48px">
                                                    </div>
                                                    <div class="comments-user__content">
                                                        <div class="comments-user__top">
                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">V - 3</div>
                                                            <div class="comments-user__right">
                                                                <meta itemprop="dateCreated" content="2023-07-16T20:30:27.000Z">
                                                                <!--[-->
                                                                <!--]-->
                                                                <div class="comment__date">16 июл. 2023</div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                    </div>
                                                </div>
                                                <div class="comment__more">
                                                    <i class="icon-ellipsis"></i>
                                                    <!--v-if-->
                                                </div>
                                            </div>
                                            <div class="comment__body" itemprop="text">
                                                <p>Большое спасибо за СУПЕР- профессиональный перевод!</p>
                                                <!--v-if-->
                                            </div>
                                            <div class="comment__footer">
                                                <button class="button-link">
                                                    <i class="icon-message"></i>
                                                    <span>Ответить</span>
                                                </button>
                                                <div class="comment__likes">
                                                    <button class="comment__likes__like-btn">
                                                        <i class="icon-like"></i>
                                                        9
                                                    </button>
                                                    <button class="comment__likes__dislike-btn">
                                                        <i class="icon-dislike"></i>
                                                        0
                                                    </button>
                                                    <meta itemprop="upvoteCount" content="9">
                                                    <meta itemprop="downvoteCount" content="0">
                                                </div>
                                            </div>
                                        </div>
                                        <!--v-if-->
                                        <!--v-if-->
                                    </div>
                                    <div class="comment-tree" itemprop="comment" itemscope itemtype="https://schema.org/Comment" id="comment-339713">
                                        <!--v-if-->
                                        <div class="comment">
                                            <div class="comment__header">
                                                <div class="comments-user">
                                                    <div class="comments-user__avatar">
                                                        <img src="/types/cinema/template/templates/turkru/assets/app/images/5.svg?6953c5657ce02b2ac94a" alt="Аноним" srcset="/types/cinema/template/templates/turkru/assets/app/images/5.svg?6953c5657ce02b2ac94a" sizes="48px">
                                                    </div>
                                                    <div class="comments-user__content">
                                                        <div class="comments-user__top">
                                                            <div class="comments-user__name" itemprop="author" itemscope itemtype="https://schema.org/Person">Аноним</div>
                                                            <div class="comments-user__right">
                                                                <meta itemprop="dateCreated" content="2023-07-16T20:50:18.000Z">
                                                                <!--[-->
                                                                <!--]-->
                                                                <div class="comment__date">16 июл. 2023</div>
                                                            </div>
                                                        </div>
                                                        <!--v-if-->
                                                    </div>
                                                </div>
                                                <div class="comment__more">
                                                    <i class="icon-ellipsis"></i>
                                                    <!--v-if-->
                                                </div>
                                            </div>
                                            <div class="comment__body" itemprop="text">
                                                <p>Фраг № 1 к 3 серии -просто огненный! Разборки между двумя мамами -бандитками
                                                продолжаются! А , Кемаль не теряет зря время : любовь- морковь с Яз !</p>
                                                <!--v-if-->
                                            </div>
                                            <div class="comment__footer">
                                                <button class="button-link">
                                                    <i class="icon-message"></i>
                                                    <span>Ответить</span>
                                                </button>
                                                <div class="comment__likes">
                                                    <button class="comment__likes__like-btn">
                                                        <i class="icon-like"></i>
                                                        11
                                                    </button>
                                                    <button class="comment__likes__dislike-btn">
                                                        <i class="icon-dislike"></i>
                                                        2
                                                    </button>
                                                    <meta itemprop="upvoteCount" content="11">
                                                    <meta itemprop="downvoteCount" content="2">
                                                </div>
                                            </div>
                                        </div>
                                        <!--v-if-->
                                        <!--v-if-->
                                    </div>
                                </div>
                            </div>
                            <button class="t-btn-2 t-btn-block t-btn-shadow t-btn-one-line">
                                <span>Следующие комментарии</span>
                            </button>
                            <!--]-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>