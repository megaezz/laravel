<!DOCTYPE html>
<html lang="ru-RU" prefix="{{ $html_prefix }}">
<head>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=yes, maximum-scale=5.0">
    <title>{{ $headers->title }}</title>
    <link rel="stylesheet" href="/types/cinema/template/templates/turkru/assets/app/css/show.css"/>
    <link rel="preload" as="script" href="/types/cinema/template/templates/turkru/assets/app/main.js"/>
    <link rel="preload" type="font/woff2" as="font" href="/types/cinema/template/templates/turkru/assets/app/fonts/subset-Roboto-Regular.woff2" crossorigin/>
    <link rel="preload" type="font/woff2" as="font" href="/types/cinema/template/templates/turkru/assets/app/fonts/subset-Roboto-Bold.woff2" crossorigin/>
    <link rel="preload" type="font/woff2" as="font" href="/types/cinema/template/templates/turkru/assets/app/fonts/subset-Roboto-Black.woff2" crossorigin/>
    <link rel="preload" type="font/woff2" as="font" href="/types/cinema/template/templates/turkru/assets/app/fonts/subset-Roboto-Medium.woff2" crossorigin/>
    <!-- TODO opensearch -->
    <link rel="search" type="application/opensearchdescription+xml" href="https://turkru.tv/openSearch/" title="{{ $domain->engineDomain->site_name }}">
    <meta http-equiv="Content-Security-Policy" content="default-src * 'unsafe-inline' 'unsafe-eval' data: blob:;">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-TileImage" content="/types/cinema/template/templates/turkru/favicon/ms-icon-144x144.png">
    <meta name="msapplication-navbutton-color" content="#000000">
    <meta name="apple-mobile-web-app-status-bar-style" content="#000000">
    <meta name="theme-color" content="#000000">
    <link rel="apple-touch-icon" sizes="57x57" href="/types/cinema/template/templates/turkru/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/types/cinema/template/templates/turkru/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/types/cinema/template/templates/turkru/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/types/cinema/template/templates/turkru/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/types/cinema/template/templates/turkru/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/types/cinema/template/templates/turkru/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/types/cinema/template/templates/turkru/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/types/cinema/template/templates/turkru/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/types/cinema/template/templates/turkru/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/types/cinema/template/templates/turkru/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/types/cinema/template/templates/turkru/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/types/cinema/template/templates/turkru/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/types/cinema/template/templates/turkru/favicon/favicon-16x16.png">
    <meta name="twitter:card" content="summary_large_image">
    <link rel="shortcut icon" href="/types/cinema/template/templates/turkru/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/types/cinema/template/templates/turkru/favicon/favicon.ico" type="image/x-icon">
    <link rel="manifest" href="/types/cinema/template/templates/turkru/favicon/manifest.json?v=1.1">
    {{ $head }}
</head>
<body ontouchstart>
    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <linearGradient id="ic-gradient" x1="8.79365e-08" y1="12.826" x2="26" y2="12.826" gradientUnits="userSpaceOnUse">
                <stop stop-color="#F47F21"/>
                <stop offset="1" stop-color="#EE403B"/>
            </linearGradient>
            <symbol id="ic-ellipsis" viewBox="0 0 16 4">
                <path d="M4 2C4 3.10455 3.10458 4 2 4C0.895416 4 0 3.10455 0 2C0 0.895447 0.895416 0 2 0C3.10458 0 4 0.895447 4 2Z"/>
                <path d="M10 2C10 3.10455 9.10458 4 8 4C6.89542 4 6 3.10455 6 2C6 0.895447 6.89542 0 8 0C9.10458 0 10 0.895447 10 2Z"/>
                <path d="M14 4C15.1046 4 16 3.10455 16 2C16 0.895447 15.1046 0 14 0C12.8954 0 12 0.895447 12 2C12 3.10455 12.8954 4 14 4Z"/>
            </symbol>
            <symbol id="ic-chevron" viewBox="0 0 133.1465 235.54015">
                <path d="m 128.00075,205.3 c 6.861,6.938 6.861,18.151 0,25.037 -6.861,6.912 -17.945,6.963 -24.807,0 L 5.14575,130.292 c -6.861,-6.912 -6.861,-18.099 0,-25.063 l 98.048,-100.045 c 6.835,-6.912 17.945,-6.912 24.807,0 6.861,6.938 6.861,18.15 0,25.037 l -80.409,87.578 z" id="path16"/>
            </symbol>
            <symbol id="ic-info" viewBox="0 0 26 26">
                <path d="M12.5062 10.8511C13.3382 10.8511 14.0012 10.1841 14.0012 9.37611C14.0012 8.55524 13.3252 7.90112 12.5062 7.90112C11.6872 7.90112 11.0112 8.56807 11.0112 9.37611C10.9982 10.1841 11.6742 10.8511 12.5062 10.8511Z"/>
                <path d="M10.998 11.8384H14.001V15.7888L15.002 16.7764V17.764H10.998V16.7764L11.999 15.7888V12.826H10.998V11.8384Z"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M13 0C5.824 0 0 5.74605 0 12.826C0 19.906 5.824 25.652 13 25.652C20.176 25.652 26 19.906 26 12.826C26 5.74605 20.176 0 13 0ZM13 24.6644C6.383 24.6644 1.001 19.3544 1.001 12.826C1.001 6.29757 6.383 0.987602 13 0.987602C19.617 0.987602 24.999 6.29757 24.999 12.826C24.999 19.3544 19.617 24.6644 13 24.6644Z"/>
            </symbol>
            <symbol id="ic-info-filled" viewBox="0 0 26 26" fill="none">
                <circle cx="13" cy="13" r="13" fill="url(#ic-gradient)"/>
                <path d="M12.5081 10.95C13.3401 10.95 14.0031 10.283 14.0031 9.47499C14.0031 8.65413 13.3271 8 12.5081 8C11.6891 8 11.0131 8.66695 11.0131 9.47499C11.0001 10.283 11.6761 10.95 12.5081 10.95Z" fill="white"/>
                <path d="M11 11.9373H14.003V15.8877L15.004 16.8753V17.8629H11V16.8753L12.001 15.8877V12.9249H11V11.9373Z" fill="white"/>
            </symbol>
            <symbol id="ic-play" viewBox="0 0 26 26">
                <path d="M16.6402 13.3389L11.4402 16.4172V10.2607L16.6402 13.3389Z"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M0 12.826C0 5.74605 5.824 0 13 0C20.176 0 26 5.74605 26 12.826C26 19.906 20.176 25.652 13 25.652C5.824 25.652 0 19.906 0 12.826ZM1.04 12.826C1.04 19.3288 6.409 24.6259 13 24.6259C19.591 24.6259 24.96 19.3288 24.96 12.826C24.96 6.32322 19.591 1.02608 13 1.02608C6.409 1.02608 1.04 6.32322 1.04 12.826Z"/>
            </symbol>
            <symbol id="ic-play-filled" viewBox="0 0 26 26">
                <circle cx="13" cy="13" r="13" fill="url(#ic-gradient)"/>
                <path d="M16.6402 13.339L11.4402 16.4172V10.2607L16.6402 13.339Z" fill="white"/>
            </symbol>
            <symbol id="ic-message" viewBox="0 0 31 25">
                <path d="M10 11C10 12.1046 9.10457 13 8 13C6.89543 13 6 12.1046 6 11C6 9.89543 6.89543 9 8 9C9.10457 9 10 9.89543 10 11Z"/>
                <path d="M18 11C18 12.1046 17.1046 13 16 13C14.8954 13 14 12.1046 14 11C14 9.89543 14.8954 9 16 9C17.1046 9 18 9.89543 18 11Z"/>
                <path d="M24 13C25.1046 13 26 12.1046 26 11C26 9.89543 25.1046 9 24 9C22.8954 9 22 9.89543 22 11C22 12.1046 22.8954 13 24 13Z"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M6 0.5C2.96243 0.5 0.5 2.96243 0.5 6V15C0.5 18.0376 2.96243 20.5 6 20.5H10V24.2385L19.3463 20.5H25C28.0376 20.5 30.5 18.0376 30.5 15V6C30.5 2.96243 28.0376 0.5 25 0.5H6ZM1.5 6C1.5 3.51472 3.51472 1.5 6 1.5H25C27.4853 1.5 29.5 3.51472 29.5 6V15C29.5 17.4853 27.4853 19.5 25 19.5H19.1537L11 22.7615V19.5H6C3.51472 19.5 1.5 17.4853 1.5 15V6Z"/>
            </symbol>
            <symbol id="ic-message-filled" viewBox="0 0 30 24">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M5.5 0C2.46243 0 0 2.46243 0 5.5V14.5C0 17.5376 2.46243 20 5.5 20H9.5V23.7385L18.8463 20H24.5C27.5376 20 30 17.5376 30 14.5V5.5C30 2.46243 27.5376 0 24.5 0H5.5Z" fill="url(#ic-gradient)"/>
                <path d="M22.5 12.5C23.6046 12.5 24.5 11.6046 24.5 10.5C24.5 9.39543 23.6046 8.5 22.5 8.5C21.3954 8.5 20.5 9.39543 20.5 10.5C20.5 11.6046 21.3954 12.5 22.5 12.5Z" fill="white"/>
                <path d="M17.5 10.5C17.5 11.6046 16.6046 12.5 15.5 12.5C14.3954 12.5 13.5 11.6046 13.5 10.5C13.5 9.39543 14.3954 8.5 15.5 8.5C16.6046 8.5 17.5 9.39543 17.5 10.5Z" fill="white"/>
                <path d="M9.5 10.5C9.5 11.6046 8.60457 12.5 7.5 12.5C6.39543 12.5 5.5 11.6046 5.5 10.5C5.5 9.39543 6.39543 8.5 7.5 8.5C8.60457 8.5 9.5 9.39543 9.5 10.5Z" fill="white"/>
            </symbol>
        </defs>
    </svg>
    <header>
        <div class="container">
            <a class="logo" href="{{ route('index') }}" title="{{ $domain->engineDomain->site_name }}"></a>
            <!-- TODO роуты -->
            <nav class="top-nav">
                <a href="/turk-72#popular-and-novelty">Популярные</a>
                <a href="/best-serials4/">Каталог</a>
                <a href="/new114/">Новые серии</a>
                <a href="/schedule30/">Расписание</a>
                <a href="/film/">Фильмы</a>
                <a href="/actors/">Актёры</a>
            </nav>
            <!-- TODO поиск -->
            <livewire:dropdown-search :template="'types.cinema.templates.turkru.livewire.search'" />
            <div class="header-profile">
                <div class="header-profile__avatar" data-trigger-login-form></div>
            </div>
        </div>
    </header>

    {{ $slot }}

    <footer class="container footer">
        <div class="copyright">© {{ now()->year }} {{ $domain->engineDomain->site_name }}</div>
        <div class="footer-nav">
            <a href="/licenzionnoe-soglashenie.html">Соглашение</a>
            <a href="/abuse.html">Правообладателям</a>
            <a href="/site/">О нас</a>
        </div>
        <!-- TODO социальный ссылки -->
        {{-- <x-types.cinema.templates.turkru.socials/> --}}
    </footer>
    <script type="text/javascript" src="/types/cinema/template/templates/turkru/assets/app/main.js"></script>
</body>
</html>
