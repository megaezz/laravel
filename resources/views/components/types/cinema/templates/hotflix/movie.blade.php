<div class="col-6 col-sm-4 col-lg-6">
	<div class="card">
		<div class="card__cover">
			<picture>   
				<source srcset="{{ $domainMovie->movie->poster_or_placeholder_webp }}" type="image/webp">   
				<img width="100%" height="200" data-srcset="{{ $domainMovie->movie->poster_or_placeholder }}" alt="{{ $domainMovie->movie->title }}" class="lazyload" style="width: 100%; height: auto;">
			</picture>
			<a href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}" class="card__play">
				<i class="icon ion-ios-play"></i>
			</a>
			<span class="card__rate card__rate--green">{{ round((int) $domainMovie->movie->rating_sko, 1) }}</span>
		</div>
		<div class="card__content">
			<h3 class="card__title">
				<a href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}">{{ $domainMovie->movie->title }}</a>
			</h3>
			<span class="card__category">
				@foreach ($domainMovie->movie->tags->where('type', 'genre') as $tag)
				<a href="{{ $domain->cinemaDomain->megaweb->getGenreLink($tag->megaweb) }}">{{ $tag->tag }}</a>
				@endforeach
			</span>
		</div>
	</div>
</div>