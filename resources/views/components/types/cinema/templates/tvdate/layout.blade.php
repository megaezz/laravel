<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-ru" lang="ru-ru" dir="ltr" prefix="og: http://ogp.me/ns# video: http://ogp.me/ns/video# article: http://ogp.me/ns/article# ya: http://webmaster.yandex.ru/vocabularies/">
<head>
	<title>{{ $headers->title }}</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta name="keywords" content="{{ $headers->keywords }}"/>
	<meta name="description" content="{{ $headers->description }}"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="referrer" content="unsafe-url"/>
	<meta http-equiv="x-dns-prefetch-control" content="on"/>
	<link href="/types/cinema/template/templates/tvdate/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
	<link href="/types/cinema/template/templates/tvdate/css/template-0.4.min.css" rel="stylesheet" type="text/css"/>

    {{ $head ?? '' }}

</head>
<body class="tvd {{ $body_extra_class ?? '' }}">
    <div id="page">
    	<div id="header">
    		<div class="headinfo">
    			<div class="container">
    				<div class="logo">
    					<a href="/" title="TVdate"></a>
    				</div>
    				<div class="slo">
    					<span>{{ ___('Даты выхода популярных сериалов') }}</span>
    				</div>
    				<div class="mmenu">
    					<span class="button-menu"></span>
    				</div>
    				<livewire:types.cinema.dropdown-search :$domain template="types.cinema.templates.tvdate.livewire.search"/>
    			</div>
    		</div>
    		<div class="headnav">
    			<div class="container">
    				<span class="navbut"></span>
    				<div class="navtop">
    					<span>{{ ___('Главное меню') }}</span>
    				</div>
    				<ul class="nav menu">
    					<li class="item-101">
    						<a href="/">
    							<span class="name">{{ ___('Дата выхода') }}</span>
    						</a>
    					</li>
    					<li class="item-102">
    						<a href="/serials" title="{{ ___('Дата выхода сериалов') }}">
    							<span class="name">{{ ___('Сериалы') }}</span>
    						</a>
    					</li>
    					<li class="item-246">
    						<span class="nav-header ">
    							<span class="name">{{ ___('Фильмы') }}</span>
    						</span>
    					</li>
    					<li class="item-247">
    						<span class="nav-header ">
    							<span class="name">{{ ___('Мультсериалы') }}</span>
    						</span>
    					</li>
    					<li class="item-248">
    						<span class="nav-header ">
    							<span class="name">{{ ___('Телепередачи') }}</span>
    						</span>
    					</li>
    					<li class="item-243">
    						<a href="/news" title="{{ ___('Новости сериалов и ТВ Шоу') }}">
    							<span class="name">{{ ___('Новости') }}</span>
    						</a>
    					</li>
    					<li class="item-835">
    						<a href="/lists" title="{{ ___('Самые популярные сериалы, списки, рейтинги, топ') }}">
    							<span class="name">{{ ___('Списки') }}</span>
    						</a>
    					</li>
    				</ul>
    			</div>
    		</div>
    		<div class="navin"></div>
    	</div>
        <div id="main">

        	{{ $slot }}
        	
        </div>
        <div id="footer">
        	<div class="container">
        		<p class="pull-right">
        			<a href="#top" id="back-top">{{ ___('Вернуться к началу') }}</a>
        		</p>
        		<p>&copy; {{ date('Y') }} {{ $domain->site_name }}</p>
        	</div>
        </div>
    </div>
    <script src="/types/cinema/template/templates/tvdate/js/jquery.min.js" type="text/javascript"></script>
    {{-- <script src="/types/cinema/template/templates/tvdate/js/script-0.3.min.js" type="text/javascript"></script> --}}
    <script src="/types/cinema/template/templates/tvdate/js/lightbox.min.js" type="text/javascript" async="async"></script>

    <x-types.engine.metrika />

</body>
</html>