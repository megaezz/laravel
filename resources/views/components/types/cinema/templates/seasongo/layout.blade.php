<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>{{ $headers->title }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{ $headers->description }}" />
	<meta name="keywords" content="{{ $headers->keywords }}" />
	{{ $head }}

	<link rel="icon" href="{{ $domain->favicon_src }}">
	<!-- <link href="/types/cinema/template/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
	<!-- <link href="/types/cinema/template/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet"> -->
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous"> -->
	<link rel="preload" as="style" href="/types/cinema/template/templates/seasongo/bootstrap/darkly/bootstrap.min.css">
	<link rel="preload" as="style" href="/types/cinema/template/templates/seasongo/css/lordfilm.css?{{ $engineConfig->reload }}">
	<style>
		body {
			/*background: url('/template/images/spiration-dark.png');*/
			/*background: url('/types/cinema/template/templates/seasongo/images/dark_wood.png');*/
			background: url('{{ $domain->bg_src }}');
		}
	</style>
	<link rel="preload" as="style" href="/types/cinema/template/templates/seasongo/css/style.css?{{ $engineConfig->reload }}">
	<link rel="preload" as="style" href="/types/engine/template/css/font-awesome/5.3.1/css/all.min.css">
	<link rel="preload" as="style" href="/types/cinema/template/templates/seasongo/js/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
	<link rel="preload" as="style" href="/types/cinema/template/templates/seasongo/js/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css">
	<link rel="preload" as="font" type="font/woff2" crossorigin="anonymous" href="/types/engine/template/css/font-awesome/5.3.1/webfonts/fa-solid-900.woff2">

	<!-- <link rel="preload" href="/types/cinema/template/templates/seasongo/bootstrap/darkly/bootstrap.min.css?{{ $engineConfig->reload }}" as="style" onload="this.onload=null;this.rel='stylesheet'"> -->
	<!-- <noscript><link rel="stylesheet" href="/types/cinema/template/templates/seasongo/bootstrap/darkly/bootstrap.min.css?{{ $engineConfig->reload }}"></noscript> -->
	<link rel="stylesheet" href="/types/cinema/template/templates/seasongo/bootstrap/darkly/bootstrap.min.css">
	<link rel="stylesheet" href="/types/cinema/template/templates/seasongo/css/lordfilm.css?{{ $engineConfig->reload }}">
	<link href="/types/cinema/template/templates/seasongo/css/style.css?{{ $engineConfig->reload }}" rel="stylesheet">
	<!-- <link href="/sites/turboserial.com/css/style.css?{{ $engineConfig->reload }}" rel="stylesheet"> -->
	<link rel="stylesheet" href="/types/engine/template/css/font-awesome/5.3.1/css/all.min.css">
	<link rel="stylesheet" href="/types/cinema/template/templates/seasongo/js/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="/types/cinema/template/templates/seasongo/js/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css">
	<!-- <link rel="stylesheet" href="/template/css/social-likes_flat.css"> -->
	<script type="application/ld+json">
		{
			"@context" : "http://schema.org",
			"@type" : "Organization",
			"name" : "{{ $domain->site_name }}",
			"url" : "{{ $domain->protocol }}://{{ $requestedDomain->domain }}"
		}
	</script>
</head>
<body>
	<div class="container mainContainer">
		<nav class="navbar navbar-expand-lg navbar-light bg-light mb-3 main-menu">
			<a href="/">
				<img style="height: 38px; width: auto; opacity: 1; border-radius: 5px; margin-right: 10px;" width="38" heght="38" src="{{ $domain->logo_src }}" alt="{{ $domain->site_name }}">
			</a>
			<a class="navbar-brand" href="/">{{ $domain->site_name }}</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item dropdown {{ request()->routeIs('group_view') && in_array(request()->route('group'), [7, 359, 4]) }}">
						<a class="nav-link dropdown-toggle" href="#" id="films" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							🎞 Фильмы
						</a>
						<div class="dropdown-menu" aria-labelledby="films">
							@foreach (\cinema\app\models\eloquent\Group::whereIn('id',[7,359,4])->orderByRaw('FIELD(id, 7,359,4)')->cacheFor($engineConfig->cacheSeconds)->get() as $item)
							<a class="dropdown-item" href="{{ $domain->cinemaDomain->megaweb->getGroupLink($item->megaweb) }}">{{ $item->emoji }} {{ $item->name }}</a>
							@endforeach
						</div>
					</li>
					<li class="nav-item dropdown {{ request()->routeIs('group_view') && in_array(request()->route('group'), [6,2,10,368,365,362,363,364]) }}">
						<a class="nav-link dropdown-toggle" href="#" id="serials" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							📺 Сериалы
						</a>
						<div class="dropdown-menu" aria-labelledby="serials">
							@foreach (\cinema\app\models\eloquent\Group::whereIn('id',[6,2,10])->orderByRaw('FIELD(id, 6,2,10)')->cacheFor($engineConfig->cacheSeconds)->get() as $item)
				    		<a class="dropdown-item" href="{{ $domain->cinemaDomain->megaweb->getGroupLink($item->megaweb) }}">{{ $item->emoji }} {{ $item->name }}</a>
				    		@endforeach
				    		<hr class="dropdown-divider">
				    		@foreach (\cinema\app\models\eloquent\Group::whereIn('id',[368,365,362,363,364])->orderByRaw('FIELD(id, 368,365,362,363,364)')->cacheFor($engineConfig->cacheSeconds)->get() as $item)
				    		<a class="dropdown-item" href="{{ $domain->cinemaDomain->megaweb->getGroupLink($item->megaweb) }}">{{ $item->emoji }} {{ $item->name }}</a>
				    		@endforeach
						</div>
					</li>
					<!-- <li class="nav-item ">
						<a class="nav-link" href="/group/1">🇷🇺 Русские</a>
					</li>
					<li class="nav-item ">
						<a class="nav-link" href="/group/2">🇺🇸 Зарубежные</a>
					</li>
					<li class="nav-item ">
						<a class="nav-link" href="/group/10">🇹🇷 Турецкие</a>
					</li> -->
				    <li class="nav-item dropdown {{ request()->routeIs('genre_view') }}">
				    	<a class="nav-link dropdown-toggle" href="#" id="genres" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    		Жанры
				    	</a>
				    	<div class="dropdown-menu" aria-labelledby="genres">
				    		@foreach (\cinema\app\models\eloquent\Tag::whereType('genre')->where('active', true)->orderBy('tag')->cacheFor($engineConfig->cacheSeconds)->get() as $item)<a class="dropdown-item {{ request()->routeIs('genre_view') && request()->route('genre_view') == $item->id }}" href="{{ $domain->cinemaDomain->megaweb->getGenreLink($item->megaweb) }}">{{ $item->emoji }} {{ \engine\app\models\F::mb_ucfirst($item->tag) }}</a>@endforeach
				    	
				    	</div>
				    </li>
				    <li class="nav-item dropdown {{ request()->routeIs('group_view') && in_array(request()->route('group'), [270,314,283,280,267,266,265,282,263,271,276]) }}">
				    	<a class="nav-link dropdown-toggle" href="#" id="dubbings" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    		🎙 Озвучки
				    	</a>
				    	<div class="dropdown-menu" aria-labelledby="dubbings">
				    		@foreach (\cinema\app\models\eloquent\Group::whereType('translator')->whereIn('id',[270,314,283,280,267,266,265,282,263,271,276])->cacheFor($engineConfig->cacheSeconds)->get() as $item)
				    		<a class="dropdown-item" href="{{ $domain->cinemaDomain->megaweb->getGroupLink($item->megaweb) }}">{{ $item->name }}</a>
				    		@endforeach
				    	</div>
				    </li>
				    <li class="nav-item dropdown">
				    	<a class="nav-link dropdown-toggle" href="#" id="bestByYears" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    		⭐ Лучшее
				    	</a>
				    	<div class="dropdown-menu" aria-labelledby="bestByYears">
				    		<a class="dropdown-item" href="/articles">Статьи про кино</a>
				    		@php
				    		$lastYears = [];
				    		for ($i = 0; $i < 8; $i++) {
				    			$lastYears[] = now()->subYears($i)->format('Y');
				    		}
				    		@endphp
				    		@foreach ($lastYears as $item)
				    		<a class="dropdown-item" href="/search?year={{ $item }}&amp;type=1&amp;order=topRated&amp;index">Лучшие сериалы {{ $item }} года</a>
				    		@endforeach
				    		<a class="dropdown-item" href="/search?type=1&amp;order=worstRated&amp;index">Самые <span style="color: red;">худшие</span> сериалы 🤮</a>
				    	</div>
				    </li>
				</ul>
				<!-- <form class="form-inline my-2 my-lg-0">
					<select class="form-control mr-sm-2">


					</select>
				</form> -->
				<livewire:types.cinema.dropdown-search :$domain template="types.cinema.templates.seasongo.livewire.search"/>

			</div>
		</nav>

		<ul class="alphabet mb-3 d-none d-md-block">
			@foreach (['а','б','в','г','д','е','ж','з','и','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','э','ю','я','1','2','3','4','5','6','7','8','9'] as $item)
			<li><a href="/search?q={{ $item }}&amp;leftSearch=1&amp;order=topRated" rel="nofollow">{{ mb_strtoupper($item) }}</a></li>
			@endforeach
			<!-- <li><a href="/search?q=0&amp;leftSearch=1&amp;order=topRated" rel="nofollow">0</a></li> -->
		</ul>

		<div class="search-mobile">
		</div>

		<div class="genresBlock sd-none d-none d-md-block">
			<div class="row mb-3">
				@foreach (\cinema\app\models\eloquent\Tag::whereType('genre')->where('active', true)->whereIn('tag',['боевик','военный','детектив','детский','документальный','драма','комедия','криминал','мелодрама','мультфильм','приключения','триллер','ужасы','фантастика','фэнтези'])->cacheFor($engineConfig->cacheSeconds)->get() as $item)
				<div class="col-md-3 col-lg-2"><a class="dropdown-item " href="{{ $domain->cinemaDomain->megaweb->getGenreLink($item->megaweb) }}">{{ $item->emoji }} {{ \engine\app\models\F::mb_ucfirst($item->tag) }}</a></div>
				@endforeach
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-2">
				<button class="btn btn-success mb-3" type="button" data-toggle="collapse" data-target="#search" aria-expanded="false" aria-controls="search">
					🔎 Расширенный поиск
				</button>
			</div>
			<div class="col-12 col-md-10 text-right">
				<button class="btn btn-warning savedMoviesBtn mb-3" data-toggle="collapse" data-target="#savedMoviesWrapper">
					<i class="fas fa-play" style="color: red;"></i>
					<span style="color: #000;">Продолжить просмотр</span>
				</button>
				<script>
					document.addEventListener("DOMContentLoaded", function() {
						savedMoviesLoaded = false;
						$('.savedMoviesBtn').on('click',function(){
							if (!savedMoviesLoaded) {
								showSavedMovies('savedMovies');
								savedMoviesLoaded = true;
							}
						});
					});
				</script>
				<a class="btn btn-warning mb-3" href="/top50">
					<i class="fas fa-trophy" style="color: red;"></i>
					<span style="color: #000;">ТОП 50</span>
				</a>
				<a class="btn btn-warning mb-3" href="/api/lucky">
					<i class="fas fa-gift" style="color: red;"></i>
					<span style="color: #000;">МНЕ ПОВЕЗЕТ</span>
				</a>
				<a href="/compilations" class="btn btn-warning mb-3">
					<i class="fas fa-medal" style="color: red;"></i>
					<span style="color: #000;">Подборки</span>
				</a>
			</div>
		</div>
		<div class="collapse mb-3" id="savedMoviesWrapper">
			<div class="row" id="savedMovies">
			</div>
		</div>
		<!-- убрать  show если верну кнопку -->
		<div class="collapse mb-3" id="search">
			<div class="card card-body">
				<form class="my-2 my-lg-0" method="post" action="/search">
					<div class="row">
						<!-- <div class="col-6 col-md-3 col-lg-2">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control mr-sm-2 cool-search" type="search" name="q" placeholder="Кино или сериал" aria-label="Search" value="" autocomplete="off">
								<div class="dropdown-menu cool-search-helper" style="width: 200px; padding: 0;">
									<ul class="list-group cool-search-helper-list">

									</ul>
								</div>
							</div>
						</div> -->
						<script>
							document.addEventListener("DOMContentLoaded", function() {
								$('.cool-search').keyup(function(){
									query = $('.cool-search').val();
									if (query.length > 1) {
										$.ajax({
											method: "GET",
											url: "/api/searchInputHelper",
											data: { q: query }
										})
										.done(function( msg ) {
              								// alert(msg);
											$('.cool-search-helper').fadeIn();
											$('.cool-search-helper-list').html(msg);
										})
									} else {
										$('.cool-search-helper-list').html('');
									}
								});
							});
						</script>
						<!-- <div class="col-6 col-md-3 col-lg-3 types">
							<div class="btn-group btn-group-toggle" data-toggle="buttons">
								<label class="btn btn-secondary all">
									<input type="radio" name="options" value="" autocomplete="off"> Все
								</label>
								<label class="btn btn-secondary movie">
									<input type="radio" name="options" value="2" autocomplete="off"> Фильмы
								</label>
								<label class="btn btn-secondary serial">
									<input type="radio" name="options" value="1" autocomplete="off"> Сериалы
								</label>
							</div>
						</div> -->
						<div class="col-6 col-md-3 col-lg-2">
							<div class="form-group">
								<label>Год</label>
								<select class="form-control" name="year">
									<option value="">Любой</option>
									@foreach (\cinema\app\models\eloquent\Movie::select('year')->whereNotNull('year')->groupBy('year')->orderByDesc('year')->cacheFor($engineConfig->cacheSeconds)->get() as $item)
									<option value="{{ $item->year }}" {{ ($item->year == request()->input('year')) ? 'selected' : '' }}>{{ $item->year }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-6 col-md-3 col-lg-2">
							<div class="form-group">
								<label>Жанр</label>
								<select class="form-control" name="genre">
									<option value="">Любой</option>
									@foreach (\cinema\app\models\eloquent\Tag::select(['id','tag'])->whereType('genre')->whereActive(true)->orderBy('id')->cacheFor($engineConfig->cacheSeconds)->get() as $item)
									<option value="{{ $item->id }}" {{ ($item->id == request()->input('genre')) ? 'selected' : '' }}>{{ $item->tag }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-6 col-md-3 col-lg-2">
							<div class="form-group">
								<label>Страна</label>
								<select class="form-control" name="country">
									<option value="">Любая</option>
									@foreach (\cinema\app\models\eloquent\Tag::select(['id','tag'])->whereType('country')->whereActive(true)->orderBy('tag')->cacheFor($engineConfig->cacheSeconds)->get() as $item)
									<option value="{{ $item->id }}" {{ ($item->id == request()->input('country')) ? 'selected' : '' }}>{{ $item->tag }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-6 col-md-3 col-lg-2">
							<div class="form-group">
								<label>Сортировать</label>
								<select class="form-control" name="order">
									@foreach ($movieOrders as $item )
									<option value="{{ $item['name'] }}" {{ ($item['name'] == request()->input('order')) ? 'selected' : '' }}>{{ $item['text'] }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-6 col-md-3 col-lg-2">
							<label>&nbsp;</label>
							<button class="btn btn-outline-success my-0 my-sm-0" type="submit" style="display: block;">Найти</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<script>
			document.addEventListener("DOMContentLoaded", function() {
				if (false) {
					$('#search').collapse('show');
				}
			});
		</script>

		{{ $slot }}
	</div>
	<div class="footer">
		<a href="/content-remove">Правообладателям</a><br>
	</div>
	<script src="/types/cinema/template/templates/seasongo/js/jquery.min.js"></script>
	<script src="/types/cinema/template/js/jquery.cookie.js"></script>
	<script src="/types/cinema/template/js/main.js?{{ $engineConfig->reload }}"></script>
	<script src="/types/cinema/template/js/lazysizes.min.js"></script>
	<script src="/types/cinema/template/js/popper.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> -->
	<script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<!-- <script src="/types/cinema/template/js/social-likes.min.js"></script> -->
	<script src="/types/cinema/template/templates/seasongo/js/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
	<!-- <script src="//code.jivosite.com/widget.js" jv-id="UIk8S2xVnY" async></script> -->
	
	<div id="gotop">
		<span class="fa fa-arrow-up"></span>
	</div>

	<script>
		/* кнока вверх */
		var gotopButton = document.getElementById('gotop');

		function toggleGotopButton() {
			if (window.scrollY > 300) {
				gotopButton.style.display = 'block';
			} else {
				gotopButton.style.display = 'none';
			}
		}

		window.addEventListener('scroll', toggleGotopButton);

		gotopButton.addEventListener('click', function() {
			window.scrollTo({
				top: 0,
				behavior: 'smooth'
			});
		});

		/* сохраненки */
		var savedMoviesLoaded = false;
		var savedMoviesBtn = document.querySelector('.savedMoviesBtn');

		savedMoviesBtn.addEventListener('click', function() {
			if (!savedMoviesLoaded) {
				showSavedMovies('savedMovies');
				savedMoviesLoaded = true;
			}
		});

		/*перемещаем поиск для маленького экрана*/
		function changeSearchPosition() {
			/*Получаем ширину окна браузера*/
			var documentWidth = document.documentElement.clientWidth; 

    		/*Получаем ссылки на элементы меню и поиска*/
			var mainMenuSearch = document.querySelector('.main-menu-search');
			var searchMobile = document.querySelector('.search-mobile');
			var mainMenu = document.querySelector('.main-menu');

    		/*Проверяем ширину окна браузера и перемещаем элемент поиска*/
			if (documentWidth < 992) {
				searchMobile.appendChild(mainMenuSearch);
			} else {
				mainMenu.appendChild(mainMenuSearch);
			}
		}

        /*выполняем после загрузки страницы и в случае события изменения ширины экрана*/
		changeSearchPosition();

		window.addEventListener("resize", function() {
			changeSearchPosition();
		});
	</script>

	{{-- подключаем рекламные скрипты --}}
	{!! $footerScripts !!}
	{{-- подключаем метрику --}}
	<x-types.engine.metrika />
</body>
</html>