<div class="th-item">
	<a class="th-in with-mask" href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}" title="Смотреть онлайн &laquo;{{ $domainMovie->movie->title }}&raquo;">
		<div class="th-img img-resp-vert">
			<picture>   
				<source srcset="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder_webp }}" type="image/webp">   
				<img srcset="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder }}" alt="{{ $domainMovie->movie->title }}" class="lazyload" width="200" height="300">
			</picture>
		</div>
		<div class="th-desc">
			<div class="th-title">{{ $domainMovie->movie->title }}</div>
			<div class="th-year">({{ $domainMovie->movie->year }})</div>
			@if($domainMovie->movie->type == 'serial')
			<div class="th-year lastEpisodes">{{ $domainMovie->movie->last_episode?->season }} сезон {{ $domainMovie->movie->last_episode?->episode }} серия</div>
			@endif
			<div class="th-rates fx-row">
				<div class="th-rate th-rate-kp" data-text="kp"><span>{{ $domainMovie->movie->kinopoisk_rating }}</span></div>
				<div class="th-rate th-rate-imdb" data-text="imdb"><span>{{ $domainMovie->movie->imdb_rating }}</span></div>
			</div>
		</div>
		<div class="th-series">HD 1080</div>
		<div class="th-mask fx-col fx-center fx-middle anim">
			<span class="fa fa-play"></span>
		</div>
	</a>
</div>