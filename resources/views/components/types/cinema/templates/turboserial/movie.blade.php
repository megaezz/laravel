<div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mb-3 movie">
	<span class="showFastInfo d-none d-md-inline" data-id="{{ $domainMovie->id }}">
		<i class="fas fa-info-circle"></i>
	</span>
	<div class="cinema-img mb-1">
		<a href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}" title="Смотреть онлайн &laquo;{{ $domainMovie->movie->title }}&raquo;">
			<picture>   
				<source data-srcset="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder_webp }}" type="image/webp">   
				<img data-srcset="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder }}" alt="{{ $domainMovie->movie->title }}" class="lazyload">
			</picture>
		</a>
	</div>
	<div class="over-image align-text-bottom">
		<div class="row no-gutters">
			<div class="col-12">
				<div class="title">
					<a href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}" title="Смотреть онлайн бесплатно &laquo;{title}&raquo;">
						<span>{{ $domainMovie->movie->title }}</span>
					</a>
				</div>
			</div>
			<div class="col-12">
				<div class="year text-left">
					{{ $domainMovie->movie->localed_type }}, {{ $domainMovie->movie->year }}
				</div>
			</div>
		</div>
	</div>
</div>