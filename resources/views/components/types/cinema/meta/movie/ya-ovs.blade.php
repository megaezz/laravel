<meta name="ya:ovs:upload_date" content="{{ $domainMovie->add_date->format('Y-m-d') }}" />
<meta name="ya:ovs:adult" content="false" />
<meta name="ya:ovs:content_id" content="{{ $domainMovie->id }}" />
<meta name="ya:ovs:status" content="published" />
<meta name="ya:ovs:quality" content="HD" />
<meta name="ya:ovs:allow_embed" content="false" />
<meta name="ya:ovs:genre" content="{{ $domainMovie->movie->tags->where('type', 'genre')->pluck('tag')->implode(', ') }}" />
<meta name="ya:ovs:languages" content="ru" />
<meta name="ya:ovs:views_total" content="{{ $domainMovie->movie->imdb_votes ?? 300 }}" />
<meta name="ya:ovs:comments" content="{{ $domainMovie->comments->count() }}" />
<meta name="ya:ovs:rating" content="{{ $domainMovie->movie->rating_sko }}" />
<meta name="ya:ovs:person" content="{{ $domainMovie->movie->tags->where('type', 'actor')->pluck('tag')->implode(', ') }}" />
<meta name="ya:ovs:person:role" content="actor" />
{{-- TODO --}}
<meta name="ya:ovs:poster" content="{{ $domainMovie->movie->poster_or_placeholder }}" />
<meta name="ya:ovs:original_name" content="{{ $domainMovie->movie->title_en ?? $domainMovie->movie->title_ru }}" />