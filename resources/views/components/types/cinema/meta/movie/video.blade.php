<meta name="video:tag" content="{{ $domainMovie->movie->tags->pluck('tag')->implode(', ') }}" />
<meta name="video:duration" content="PT3600S" />
<meta name="video:release_date" content="{{ $domainMovie->movie->kinopoisk?->premiere_world?->translatedFormat('j F Y') }}" />