{{-- TODO тут должен быть тайтл главной страницы судя по всему, чекнуть на киного --}}
<meta property="og:site_name" content="{{ $headers->title }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ $domainMovie->movie->title }}" />
{{-- TODO --}}
<meta property="og:url" content="{{ $domain->toRoute->movieView($domainMovie) }}" />
{{-- TODO --}}
<meta property="og:image" content="{{ $domainMovie->movie->poster_or_placeholder }}" />
<meta name="og:site_name" content="Фильмы онлайн на {{ $domain->site_name }}" />
<meta name="og:title" content="{{ $domainMovie->movie->title }} ({{ $domainMovie->movie->year }}) смотреть онлайн на {{ $domain->site_name }}" />
{{-- TODO --}}
<meta name="og:url" content="{{ $domain->toRoute->movieView($domainMovie) }}" />
{{-- TODO --}}
<meta name="og:image" content="{{ $domainMovie->movie->poster_or_placeholder }}" />
<meta name="og:image:type" content="image/jpeg" />
<meta name="og:description" content="{{ $domainMovie->description_or_copypaste}}" />
{{-- TODO --}}
<meta name="og:video" content="{{ $domain->toRoute->movieView($domainMovie) }}" />
<meta name="og:video:width" content="1280" />
<meta name="og:video:height" content="720" />
{{-- TODO --}}
<meta name="og:duration" content="PT3600S" />
<meta name="og:locale" content="ru_RU" />
<meta name="og:video:type" content="flash,html5" />
<meta name="og:type" content="video.movie" />