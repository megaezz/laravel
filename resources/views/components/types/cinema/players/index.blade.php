@if ($activePlayer->component == 'component')
<x-dynamic-component :component="'types.cinema.players.' . $activePlayer->player" :$domainMovie :$country />
@endif
@if ($activePlayer->component == 'livewire')
<livewire:dynamic-component :is="'types.cinema.players.' . $activePlayer->player" :$domainMovie :$country />
@endif