<div>
    <div class="page-home__top">
        <div class="container">
            <h1 class="page-title">
                Поиск врачей в
                <span x-on:click="@this.showCityPopup = true" class="city">{{ $this->city->name_prepositional }}</span>
            </h1>
            <!--noindex-->
            <div class="home-form">
                <div x-on:click.outside="@this.showDropDownSpecialties = false" dir="auto" class="v-select vs--single vs--searchable" :class="{ 'vs--open': @this.showDropDownSpecialties, 'desktop-only': @this.showDropDownSpecialties }">
                    <!--[--><!--]-->
                    <div id="vs2026__combobox" class="vs__dropdown-toggle" role="combobox" aria-expanded="false" aria-owns="vs2026__listbox" aria-label="Search for option">
                        <div class="vs__selected-options">
                            @if ($specialty)
                            {{-- не отображать когда есть ввод, а то не красиво --}}
                            <span x-show="!@this.dropDownSpecialtySearch" class="vs__selected">{{ $specialty->name }}</span>
                            @endif
                            <!--[--><!--]--><!--[-->
                            <input wire:model.live="dropDownSpecialtySearch" x-on:focus="@this.showDropDownSpecialties = true" class="vs__search" placeholder="{{ $specialty ? '' : 'Выберите специальность врача' }}" aria-autocomplete="list" aria-labelledby="vs2026__combobox" aria-controls="vs2026__listbox" type="search" autocomplete="off" value="">
                            <!--]-->
                        </div>
                        <div class="vs__actions">
                            <button type="button" class="vs__clear" title="Clear Selected" aria-label="Clear Selected" style="display:none;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10">
                                    <path d="M6.895455 5l2.842897-2.842898c.348864-.348863.348864-.914488 0-1.263636L9.106534.261648c-.348864-.348864-.914489-.348864-1.263636 0L5 3.104545 2.157102.261648c-.348863-.348864-.914488-.348864-1.263636 0L.261648.893466c-.348864.348864-.348864.914489 0 1.263636L3.104545 5 .261648 7.842898c-.348864.348863-.348864.914488 0 1.263636l.631818.631818c.348864.348864.914773.348864 1.263636 0L5 6.895455l2.842898 2.842897c.348863.348864.914772.348864 1.263636 0l.631818-.631818c.348864-.348864.348864-.914489 0-1.263636L6.895455 5z">
                                    </path>
                                </svg>
                            </button>
                            <!--[-->
                            <span role="presentation" class="vs__open-indicator" style="display:initial;">
                                <div x-on:click="@this.showDropDownSpecialties = !@this.showDropDownSpecialties" class="field-select__wrap_chevron">
                                </div>
                            </span>
                            <!--]--><!--[-->
                            <div class="vs__spinner" style="display:none;">Loading...</div>
                            <!--]-->
                        </div>
                    </div>
                    {{-- <ul id="vs8__listbox" class="vs__dropdown-menu" role="listbox" tabindex="-1">
                        <li id="vs8__option-0" role="option" class="vs__dropdown-option">Абдоминальный хирург</li>
                    </ul> --}}
                    <ul x-show="@this.showDropDownSpecialties" style="display: none;" class="vs__dropdown-menu" id="vs2026__listbox" role="listbox">
                        @foreach ($this->dropDownSpecialties as $dropDownSpecialty)
                        <li 
                        x-data="{ showClass: false }"
                        x-on:mouseover="showClass = true"
                        x-on:mouseleave="showClass = false"
                        :class="{ 'vs__dropdown-option--highlight': showClass }"
                        wire:click="$set('specialtyId', {{ $dropDownSpecialty->id }})"
                        role="option" class="vs__dropdown-option">{{ $dropDownSpecialty->name }}</li>
                        @endforeach
                    </ul>
                </div>
                <button type="submit" class="btn">Найти</button>
            </div>
            <!--/noindex-->
            <div class="counters">
                <div class="counters__item">
                    <div class="counters__item_img">
                        <img src="/types/doctor/images/reviews-icon-w.svg" alt="">
                    </div>
                    <div class="counters__item_counter">{{ number_format($reviewsCount) }}</div>
                    <div class="counters__item_title">отзывов</div>
                </div>
                <div class="counters__item">
                    <div class="counters__item_img">
                        <img src="/types/doctor/images/doctors-icon-w.svg" alt="">
                    </div>
                    <div class="counters__item_counter">{{ number_format($doctorsCount) }}</div>
                    <div class="counters__item_title">врачей</div>
                </div>
                <div class="counters__item">
                    <div class="counters__item_img">
                        <img src="/types/doctor/images/clinics-icon-w.svg" alt="">
                    </div>
                    <div class="counters__item_counter">{{ number_format($clinicsCount) }}</div>
                    <div class="counters__item_title">клиник</div>
                </div>
            </div>
        </div>
    </div>
    @if ($showCityEntities)
    <div class="container">
        <div class="page-with-sidebar__main">
            <div class="entitys">
                <div class="entitys__entity doctors">
                    <div class="entitys__entity_title"> Врачи </div>
                    <div class="entitys__entity_list">
                        @foreach ($this->cityDoctorSpecialties as $cityDoctorSpecialty)
                        <a href="#" class="item">
                            <div class="item__title">{{ $cityDoctorSpecialty->name_plural }}</div>
                            <div class="item__count">{{ number_format($cityDoctorSpecialty->doctors_count) }}</div>
                        </a>
                        @endforeach
                    </div>
                    <a href="#" class="btn btn-white-green entitys__entity_all"> Все врачи </a>
                </div>
                <div class="entitys__entity clinics">
                    <div class="entitys__entity_title"> Клиники </div>
                    <div class="entitys__entity_list">
                        @foreach ($this->cityClinicTypes as $cityClinicType)
                        <a href="#" class="item">
                            <div class="item__title">{{ $cityClinicType->name_plural }}</div>
                            <div class="item__count">{{ number_format($cityClinicType->clinics_count) }}</div>
                        </a>
                        @endforeach
                    </div>
                    <a href="#" class="btn btn-white-green entitys__entity_all"> Все клиники </a>
                </div>
                <div class="entitys__entity services">
                    <div class="entitys__entity_title"> Услуги </div>
                    <div class="entitys__entity_list">
                        @foreach($this->cityServices as $service)
                        <a href="#" class="item">
                            <div class="item__title">{{ $service->parent->name }}</div>
                            <div class="item__count">?</div>
                        </a>
                        @endforeach
                    </div>
                    <a href="#" class="btn btn-white-green entitys__entity_all"> Все услуги </a>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div x-show="@this.showCityPopup" class="popup popup-city open vfm vfm--fixed vfm--inset" role="dialog" aria-modal="true" style="display: none; z-index: 1000;">
        <div x-on:click.outside="@this.showCityPopup = false" class="vfm__content vfm--outline-none" tabindex="0">
            <div class="popup__wrap">
                <div x-on:click="@this.showCityPopup = false" class="popup__close"></div>
                <div class="popup__title"> Выберите город </div>
                <div class="popup__scroller">
                    <div class="popup__content">
                        <div x-on:click.outside="@this.showDropDown = false" dir="auto" class="v-select vs--single vs--searchable">
                            <div id="vs2__combobox" class="vs__dropdown-toggle" role="combobox" aria-expanded="false" aria-owns="vs2__listbox" aria-label="Search for option">
                                <div class="vs__selected-options">
                                    <input x-on:focus="@this.showDropDown = true" wire:model.live="dropDownCitySearch" class="vs__search" placeholder="Введите название города" aria-autocomplete="list" aria-labelledby="vs2__combobox" aria-controls="vs2__listbox" type="search" autocomplete="off" value="">
                                </div>
                                <div class="vs__actions">
                                    <button type="button" class="vs__clear" title="Clear Selected" aria-label="Clear Selected" style="display: none;">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10">
                                            <path d="M6.895455 5l2.842897-2.842898c.348864-.348863.348864-.914488 0-1.263636L9.106534.261648c-.348864-.348864-.914489-.348864-1.263636 0L5 3.104545 2.157102.261648c-.348863-.348864-.914488-.348864-1.263636 0L.261648.893466c-.348864.348864-.348864.914489 0 1.263636L3.104545 5 .261648 7.842898c-.348864.348863-.348864.914488 0 1.263636l.631818.631818c.348864.348864.914773.348864 1.263636 0L5 6.895455l2.842898 2.842897c.348863.348864.914772.348864 1.263636 0l.631818-.631818c.348864-.348864.348864-.914489 0-1.263636L6.895455 5z"></path>
                                        </svg>
                                    </button>
                                    <div class="field-text__wrap_search">
                                        <img src="types/doctor/images/search_16x16.svg" alt="">
                                    </div>
                                    <div class="vs__spinner" style="display: none;">Loading...</div>
                                </div>
                            </div>
                            <ul x-show="@this.showDropDown" id="vs2__listbox" class="vs__dropdown-menu" role="listbox" tabindex="-1">
                                @foreach ($this->dropDownCities as $dropDownCity)
                                <li 
                                x-data="{ showClass: false }"
                                x-on:mouseover="showClass = true"
                                x-on:mouseleave="showClass = false"
                                :class="{ 'vs__dropdown-option--highlight': showClass }"
                                wire:click="$set('cityId', {{ $dropDownCity->id }})" role="option" class="vs__dropdown-option">{{ $dropDownCity->name }}</li>
                                {{-- <li id="vs3__option-5" role="option" class="vs__dropdown-option vs__dropdown-option--highlight" aria-selected="true">Анапа</li> --}}
                                @endforeach
                            </ul>
                        </div>
                        <div class="cities-list">
                            @foreach ($cities as $city)
                            @if (in_array($city->name, ['Москва', 'Санкт-Петербург', 'Нижний Новгород', 'Казань', 'Новосибирск', 'Омск', 'Краснодар', 'Красноярск', 'Пермь', 'Уфа']))
                            <a class="cities-link marked" wire:click="$set('cityId', {{ $city->id }})" href="#">{{ $city->name }}</a>
                            @else
                            <a class="cities-link" wire:click="$set('cityId', {{ $city->id }})" href="#">{{ $city->name }}</a>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>