<div>
    <button class="btn btn-link pl-0" type="button" data-toggle="collapse" data-target="#massLoad">
        Массовое добавление заданий в очередь
    </button>
    <div @class(['collapse', 'show' => $opened]) id="massLoad">
        <x-types.cinema.admin.kinocontent.validation-errors :$errors />
        <form wire:submit="add">
            <div class="form-group">
                <textarea wire:model="list" class="form-control" placeholder="Список ID или URL кинопоиска (построчно)" rows="10"></textarea>
            </div>
            <div class="form-group">
                <input type="checkbox" wire:model="checkDuplicates" id="checkDouble">
                <label for="checkDouble">Убрать дубли из списка</label>
                <br>
                <input type="checkbox" wire:model="checkDone" id="checkDone">
                <label for="checkDone">Не добавлять фильмы, которые уже выполнялись</label>
                <br>
                <input type="checkbox" wire:model="checkInQueue" id="checkInQueue">
                <label for="checkInQueue">Не добавлять фильмы, которые уже есть в очереди</label>
            </div>
            <button class="btn btn-secondary ml-2" type="submit">Добавить</button>
        </form>
    </div>
    @if ($result)
    <div class="row mt-3">

        <div class="col-12">
            <h5>Результат выполнения:</h5>
        </div>

        @if ($result['done'])
        <div class="col-12 col-md-6">
            <h5 style="color: green;">Добавлено</h5>
            <ol>
                @foreach ($result['done'] as $string)
                <li>{{ $string }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <div class="col-12 col-md-6">

            @if ($result['notFound'])
            <h5 style="color: red;">Не найдено в базе</h5>
            <ol>
                @foreach ($result['notFound'] as $string)
                <li>{{ $string }}</li>
                @endforeach
            </ol>
            @endif

            @if ($result['alreadyDone'])
            <h5 style="color: red;">Уже выполнялось</h5>
            <ol>
                @foreach ($result['alreadyDone'] as $string)
                <li>{{ $string }}</li>
                @endforeach
            </ol>
            @endif

            @if ($result['alreadyInQueue'])
            <h5 style="color: red;">Уже есть в очереди</h5>
            <ol>
                @foreach ($result['alreadyInQueue'] as $string)
                <li>{{ $string }}</li>
                @endforeach
            </ol>
            @endif

            @if ($result['duplicated'])
            <h5 style="color: red;">Дубли</h5>
            <ol>
                @foreach ($result['duplicated'] as $string)
                <li>{{ $string }}</li>
                @endforeach
            </ol>
            @endif

        </div>
    </div>
    @endif
</div>
