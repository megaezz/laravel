<div>
    <div class="form-inline my-2 my-lg-0 pb-5">
        <div class="input-group mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-search"></i>
                </div>
            </div>
            <input class="form-control mr-sm-2" type="search" placeholder="Название фильма или ссылка на кинопоиск" aria-label="Search" autocomplete="off" style="width: 400px;" wire:model.live="searchQuery">
            @if ($searchQuery)
            <div class="dropdown-menu show" style="max-width: 440px; padding: 0;">
                <ul class="list-group">
                    @if ($this->filteredMovies->count())
                        @foreach ($this->filteredMovies as $movie)
                        <livewire:types.cinema.admin.kinocontent.movie-in-search :$movie :$user :key="$movie->id" />
                        @endforeach
                    @else
                        <li class="list-group-item">
                            <small>Материал на найден в нашей базе. Вы можете создать это задание через вкладку &laquo;<b>Другие тексты</b>&raquo; указав в комментариях название фильма/сериала или ссылку на кинопоиск.</small>
                        </li>
                    @endif
                </ul>
            </div>
            @endif
        </div>
    </div>

    <livewire:types.cinema.admin.kinocontent.customer-default-settings :$user />
    <livewire:types.cinema.admin.kinocontent.mass-adding-of-tasks :$user />

</div>