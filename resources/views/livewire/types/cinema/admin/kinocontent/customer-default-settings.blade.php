<div>
    <button class="btn btn-link pl-0" data-toggle="collapse" data-target="#defaultSettings" aria-expanded="false">Настройки по умолчанию</button>
    <div id="defaultSettings" @class(['collapse alert alert-secondary', 'show' => $opened])>
        <div class="mb-2">
            <b>Настройки по умолчанию для новых заданий:</b>
        </div>
        <x-types.cinema.admin.kinocontent.validation-errors :$errors />
        <form wire:submit="save">
            <div class="form-inline mb-3">
                Размер текста:&nbsp;
                <div class="input-group" style="width: 200px;">
                    <input wire:model="symbolsFrom" type="text" placeholder="От" class="form-control" style="width: 50px;">
                    <div class="input-group-prepend">
                        <div class="input-group-text">&mdash;</div>
                    </div>
                    <input wire:model="symbolsTo" type="text" placeholder="До" class="form-control" style="width: 50px;">
                </div>
            </div>
            <div class="form-group">
                <textarea wire:model="customerComment" class="form-control" placeholder="Комментарий для исполнителя" maxlength="255" rows="4" type="text"></textarea>
            </div>
            <div class="form-group">
                <input wire:model="customerPrivateComment" class="form-control" placeholder="Комментарий для себя" type="text">
            </div>
            <div class="form-group form-check">
                <input wire:model="checkIsMovieInDoneTasks" id="checkIsMovieInDoneTasks" class="form-check-input" type="checkbox">
                <small>
                    <label for="checkIsMovieInDoneTasks" class="form-check-label">Предупреждать, если добавляемое задание уже есть в выполненных</label>
                </small>
            </div>
            <div class="form-group form-check">
                <input wire:model="customerAutoExpress" id="autoExpress" class="form-check-input" name="autoExpress" type="checkbox">
                <small>
                    <label for="autoExpress" class="form-check-label">Экспресс задание</label>
                </small>
            </div>
            <button class="btn btn-secondary" type="submit">Сохранить</button>
        </form>
    </div>
</div>