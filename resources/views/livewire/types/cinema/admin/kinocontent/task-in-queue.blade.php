<li class="list-group-item">
    <div class="row">
        <div @class(['col-8' => $task->is_confirmed, 'col-7' => !$task->is_confirmed]) @style(['color: gray' => $task->is_confirmed])>
            @if ($task->task_type == 'movie')
            <small>{{ $task->movie->title_ru }} / {{ $task->movie->title_en }} ({{ $task->movie->type }} {{ $task->movie->year }})</small>
            @else
            <small>{{ \Str::limit($task->customer_comment ?? '', 90) }}</small>
            @endif
            @if ($task->is_confirmed)
            <small><span title="ID задания">#{{ $task->id }}</span></small>
            @endif
        </div>
        <div @class(['col-4' => $task->is_confirmed, 'col-5' => !$task->is_confirmed])>
            
            <button @class(['btn btn-sm btn-light', 'active' => $task->is_express, 'disabled' => $task->is_booked]) type="button" title="Экспресс выполнение от проверенных исполнителей (+{{ $cinemaConfig->customerExpressFare}} руб. за 1000 симв.)" wire:click="expressToggle">
                <i class="fas fa-arrow-up"></i>
            </button>
            <button wire:click="$toggle('opened')" class="btn btn-sm btn-light" type="button" title="Настройки задания" data-toggle="collapse" data-target="#settings{{ $task->id }}">
                <i class="fas fa-wrench" @style(['color: orange' => ($task->customer_comment or $task->private_comment)])></i>
            </button>

            @if (!$task->is_confirmed)
            <button class="btn btn-sm btn-light" type="button" title="Создать копию задания" data-toggle="collapse" wire:click="duplicate">
                <i class="far fa-copy"></i>
            </button>
            <button class="btn btn-sm btn-light" type="button" title="Удалить" wire:click="delete" wire:loading.attr="disabled">
                <i class="far fa-trash-alt"></i>
            </button>
            @endif

            @if ($task->is_booked)
            <i class="fas fa-user ml-2" title="Забронировано исполнителем"></i>
            @elseif ($task->is_confirmed)
            <button class="btn btn-sm btn-light" type="button" title="Ожидает исполнителя, нажмите чтобы отменить выполнение заказа" wire:click="confirmToggle">
                <i class="far fa-clock"></i>
            </button>
            @else
            <button class="btn btn-sm btn-light" type="button" title="Отправить на выполнение" wire:click="confirmToggle">
                <i class="fas fa-check"></i>
            </button>
            @endif

        </div>
    </div>

    <form @class(['collapse', 'show' => $opened]) id="settings{{ $task->id }}" wire:submit="save">
        <x-types.cinema.admin.kinocontent.validation-errors :$errors />
        <fieldset @if ($task->is_confirmed) disabled @endif>
            <div class="form-group">
                <textarea class="form-control mt-3" rows="3" placeholder="Комментарий для исполнителя" wire:model="customerComment"></textarea>
                <input class="form-control mt-3" placeholder="Комментарий для себя" wire:model="privateComment">
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label><small><b>Символов</b></small></label>
                        <div class="input-group input-group-sm" style="width: 200px;">
                            <input type="text" placeholder="От" class="form-control" style="width: 50px;" wire:model="symbolsFrom">
                            <div class="input-group-prepend">
                                <div class="input-group-text">&mdash;</div>
                            </div>
                            <input type="text" placeholder="До" class="form-control" style="width: 50px;" wire:model="symbolsTo">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-sm btn-secondary float-right" type="submit">Сохранить</button>
                </div>
            </div>
        </fieldset>
    </form>
</li>