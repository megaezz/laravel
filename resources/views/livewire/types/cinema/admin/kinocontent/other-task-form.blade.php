<div>
    <div class="alert alert-secondary" role="alert">
        Помимо фильмов, у нас можно заказывать описания к играм, книгам, видеороликам (в том числе адалт), комментарии, отзывы, тексты для разделов сайта (со ссылкой на источник рерайта). Если у вас другая ниша, то уточните в поддержке или просто отправьте пару заданий на тест.
    </div>
    <div class="card">
        <div class="card-header">
            <b>Добавление задания:</b>
        </div>
        <div class="card-body">
            <x-types.cinema.admin.kinocontent.validation-errors :$errors />
            <form wire:submit="add">
                <div class="form-group">
                    <textarea wire:model="comment" class="form-control" placeholder="Напишите здесь задание для исполнителя, желательно с ссылкой на источник для рерайта." maxlength="1000" rows="10" type="text"></textarea>
                </div>
                <div class="form-inline mb-3">
                    Длина текста:&nbsp;
                    <div class="input-group" style="width: 200px;">
                        <input wire:model="symbolsFrom" type="text" placeholder="От" class="form-control" style="width: 50px;">
                        <div class="input-group-prepend">
                            <div class="input-group-text">&mdash;</div>
                        </div>
                        <input wire:model="symbolsTo" type="text" placeholder="До" class="form-control" style="width: 50px;">
                    </div>
                    &nbsp;символов с пробелами
                </div>
                <div class="form-group">
                    <label>Комментарий для себя:</label>
                    <input wire:model="privateComment" class="form-control" placeholder="Комментарий для себя" type="text">
                </div>
                <div class="form-group">
                    <div class="form-check" title="Некоторые исполнители не хотят получать задания по описанию порно роликов. Если создаваемое задание является таковым, отметьте пожалуйста галочкой, чтобы не травмировать их.">
                        <input wire:model="isAdult" class="form-check-input" type="checkbox" value="1" id="adultInput">
                        <label class="form-check-label" for="adultInput">
                            Это задание 18+ (отметьте, если адалт)
                        </label>
                    </div>
                </div>
                <button class="btn btn-secondary">Добавить</button>
                <div><small>*Задание отобразится в очереди и будет ждать пока вы подтвердите его галочкой.</small></div>
            </form>
        </div>
    </div>
</div>