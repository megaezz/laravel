<li class="list-group-item">
    <div class="row">
        <div class="col-10">
            <small>{{ $movie->title_ru }} / {{ $movie->title_en }} ({{ $movie->localed_type }} {{ $movie->year }})</small>
        </div>
        <div class="col-2">
            <button class="btn btn-sm btn-light addMovieToQueue" type="button" title="Добавить в очередь" wire:click="add">
                @if ($added)
                <i class="fas fa-check"></i>
                @else
                <i class="fas fa-plus"></i>
                @endif
            </button>
        </div>
    </div>
</li>