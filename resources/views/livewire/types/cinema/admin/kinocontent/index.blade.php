<div class="row">
	<x-types.cinema.admin.kinocontent.alert type="error" :message="session('error')" />
	<x-types.cinema.admin.kinocontent.alert type="message" :message="session('message')" />
	<div class="col-md-12">
		<div class="mb-4">
			<button wire:click="$set('taskType', 'movie')" @class(['btn btn-outline-secondary', 'active' => ($taskType == 'movie')])>
				Описания фильмов
			</button>
			<button wire:click="$set('taskType', 'other')" @class(['btn btn-outline-secondary', 'active' => ($taskType == 'other')])>
				Другие тексты
			</button>
		</div>
	</div>
	<div class="col-md-6">
		@if ($taskType == 'movie')
		<livewire:types.cinema.admin.kinocontent.movie-task-form :$user />
		@endif
		@if ($taskType == 'other')
		<livewire:types.cinema.admin.kinocontent.other-task-form :$user />
		@endif
	</div>
	<div class="col-md-6">
		<livewire:types.cinema.admin.kinocontent.task-queue :$user :$cinemaConfig />
	</div>
</div>