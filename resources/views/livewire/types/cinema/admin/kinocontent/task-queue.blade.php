<div>
    <div class="row mb-3">
        <div class="col-md-4">
            В очереди {{ $this->tasks->count() }} шт.
        </div>
        <div class="col-md-8 text-right">
            Рекомендуемый баланс: {{ ceil($this->tasks->sum(function($task){return $task->megaweb->getExpectedPrice();})) }} руб.
        </div>
    </div>
    <ul class="list-group">
        @foreach ($this->tasks as $task)
        {{-- указываем уникальный :key, иначе лаги при апдейтах компонента index --}}
        <livewire:types.cinema.admin.kinocontent.task-in-queue :$task :$user :$cinemaConfig :key="$task->id" />
        @endforeach
    </ul>
    <div class="col-12 text-right mt-2">
        <button class="btn btn-sm btn-danger mr-2" wire:click="clearAll" wire:confirm="Удалить из очереди все неподтвержденные задания?">
            <i class="far fa-trash-alt"></i> Удалить неподтвержденные
        </button>
        <button class="btn btn-sm btn-secondary" wire:click="confirmAll" wire:confirm="Подтвердить все задания в очереди?">
            <i class="fas fa-check"></i> Подтвердить все
        </button>
    </div>
</div>
