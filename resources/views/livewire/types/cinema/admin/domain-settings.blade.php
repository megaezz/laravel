<div class="container">
	<div class="row">
		<div class="col-12">
			@error('domain')
			<div>{{ $message }}</div>
			@enderror
		</div>
		<div class="col-12 col-md-8 mt-2 mb-2">
			<h3>
				Настройки Cinema <a href="{{ route('domain.menu', [ 'domain' => $domain->domain ]) }}">{{ $domain->domain }}</a>
				@if ($domain->mirror_of)
				<a href="{{ route('domain.settings',[ 'domain' => $domain->mirror_of ]) }}">
					<i class="fas fa-cog"></i>
				</a>
				@endif
			</h3>
		</div>
		@if (!$domain->is_mirror)
		<div class="col-12 col-md-4 mt-2 mb-2">
			<label>Скопировать настройки:</label>
			<select class="form-select" wire:model.live="copy_domain">
				<option value="">Не выбрано</option>
				@foreach ($this->copy_domains as $copy_domain)
				<option value="{{ $copy_domain->domain }}">{{ $copy_domain->domain }}</option>
				@endforeach
			</select>
		</div>
		@endif
	</div>
	<div class="row">
		@foreach ($domain->toArray() as $property => $value)
		@if (!isset($this->properties[$property]))
		<div class="col-md-2 mb-2">
			<div class="form-group">
				<label>{{ $property }}</label>
			</div>
		</div>
		@endif
		@endforeach
		@foreach ($this->properties as $property_name => $property)
		@if ($property['type'] == 'select')
		<div class="col-md-2 mb-2">
			<div class="form-group">
				<label>{{ $property['name'] ?? $property_name }}</label>
				<select class="form-select" wire:model.live="domain.{{ $property_name }}">
					<option value="">Не выбрано</option>
					@foreach ($property['options'] as $option)
					<option value="{{ $option[$property['option_name']] }}">{{ $option[$property['option_name']] }}</option>
					@endforeach
				</select>
			</div>
		</div>
		@endif
		@if ($property['type'] == 'boolean')
		<div class="col-md-2 mb-2">
			<label>&nbsp;</label>
			<div class="form-check form-switch">
				<input class="form-check-input" type="checkbox" id="{{ $property_name }}" wire:model.live="domain.{{ $property_name }}">
				<label class="form-check-label" for="{{ $property_name }}">
					{{ $property['name'] ?? $property_name }}
				</label>
			</div>
		</div>
		@endif
		@if ($property['type'] == 'int')
		<div class="col-md-2 mb-2">
			<div class="form-group">
				<label>{{ $property['name'] ?? $property_name }}</label>
				<input class="form-control" type="text" wire:model.live="domain.{{ $property_name }}">
			</div>
		</div>
		@endif
		@if ($property['type'] == 'text')
		<div class="col-md-4 mb-2">
			<div class="form-group">
				<label>{{ $property['name'] ?? $property_name }}</label>
				<input class="form-control" type="text" wire:model.live="domain.{{ $property_name }}">
			</div>
		</div>
		@endif
		@endforeach
		<div class="col-12">
			<button class="btn btn-primary mb-3" wire:click="updateDomain">Сохранить</button>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<form wire:submit="isMovieAllowed">
				<label>Подходит ли фильм для этого домена?</label>
				<div class="row">
					<div class="col-md-8">
						<input type="text" wire:model="isMovieAllowedVars.movie_id" class="form-control">
					</div>
					<div class="col-md-4">
						<button class="btn btn-primary" type="submit">Проверить</button>
					</div>
					<div class="col-md-12">
						@if($isMovieAllowedVars['result'])
						Результат: {{ ($isMovieAllowedVars['result']['result'] ?? null) ? 'разрешено' : 'не разрешено' }}<br>
						@if($isMovieAllowedVars['result']['message'] ?? null)
						Сообщение: {{ $isMovieAllowedVars['result']['message'] ?? 'отсутствует' }}
						@endif
						@endif
					</div>
				</div>
			</form>
		</div>
	</div>

</div>