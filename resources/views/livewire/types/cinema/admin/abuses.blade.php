<div class="row">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="col-12 mb-3">
        <div class="row">
            <div class="col-6 col-md-3 col-lg-2 col-xxl-2 mb-3">
                <label class="form-label">Сервис</label>
                <select wire:model.live="service" class="form-control">
                    <option value="">Любой</option>
                    @foreach ($this->services as $service)
                    <option value="{{ $service }}">{{ $service }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-6 col-md-3 col-lg-2 col-xxl-2 mb-3">
                <label class="form-label">Обработка</label>
                <select wire:model.live="handled" class="form-control">
                    <option value="">Любые</option>
                    <option value="true">Обработан</option>
                    <option value="false">Не обработан</option>
                </select>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xxl-2 mb-3">
                <label class="form-label">Ошибка при обработке</label>
                <select wire:model.live="handler_error" class="form-control">
                    <option value="">Любые</option>
                    <option value="true">С ошибкой</option>
                    <option value="false">Без ошибки</option>
                </select>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xxl-2 mb-3">
                <label class="form-label">Подлежат оспариванию</label>
                <select wire:model.live="toBeCounterComplained" class="form-control">
                    <option value="">Любые</option>
                    <option value="true">Подлежат оспариванию</option>
                    <option value="false">Не подлежат оспариванию</option>
                </select>
            </div>
            <div class="col-6 col-md-4 col-lg-2 col-xxl-2 mb-3">
                <label class="form-label">Оспорены</label>
                <select wire:model.live="challenged_on_google" class="form-control">
                    <option value="">Любые</option>
                    <option value="true">Оспорены</option>
                    <option value="false">Не оспорены</option>
                </select>
            </div>
            <div class="col-6 col-md-4 col-lg-2 col-xxl-2 mb-3">
                <label class="form-label">Восстановлены</label>
                <select wire:model.live="reinstated_on_google" class="form-control">
                    <option value="">Любые</option>
                    <option value="true">Восстановлены</option>
                    <option value="false">Не восстановлены</option>
                </select>
            </div>
            <div class="col-6 col-md-4 col-lg-2 col-xxl-2 mb-3">
                <label class="form-label">Отклонены</label>
                <select wire:model.live="canceled_on_google" class="form-control">
                    <option value="">Любые</option>
                    <option value="true">Отклонены</option>
                    <option value="false">Не отклонены</option>
                </select>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xxl-2 mb-3">
                <label class="form-label">Ссылка содержит</label>
                <input class="form-control" type="text" wire:model.live="urlSearch">
            </div>
            <div class="col-6 col-md-3 col-lg-2 col-xxl-2 mb-3">
                <label class="form-label">Выводить по</label>
                <select wire:model.live="limit" class="form-control">
                    <option>8</option>
                    <option>15</option>
                    <option>50</option>
                    <option>100</option>
                    <option>200</option>
                    <option>300</option>
                </select>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xxl-2 mb-3">
                <label class="form-label">Массовый поиск</label>
                <button class="btn btn-secondary" wire:click="$toggle('showMassUrlSearch')">Массовый поиск</button>
            </div>
            @if ($showMassUrlSearch)
            <div class="col-12 mb-3">
                <label class="form-label">Массовый поиск</label>
                <textarea class="form-control mb-2" wire:model.live="massUrlSearch" rows="10"></textarea>
                @if ($massUrlSearchRows)
                Найдено {{ $this->links->count() }} из {{ count($massUrlSearchRows) }}
                @endif
            </div>
            @endif
        </div>
    </div>
    <div class="col-12 table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                  <th scope="col">
                    <input class="form-check-input" type="checkbox" wire:model.live="selectAllLinks">
                </th>
                <th scope="col">Ссылка</th>
                <th scope="col">Сервис</th>
                <th scope="col">Добавлено</th>
                <th scope="col">Обработка</th>
                <th scope="col">Оспаривание</th>
            </tr>
        </thead>
        @foreach ($this->links as $link)
        <tr>
            <td scope="col">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" wire:model="selectedLinks" value="{{ $link->id }}">
                </div>
            </td>
            <td style="word-break: break-all;">
                <a href="https://{{ $link->query }}">{{ urldecode($link->query) }}</a>
            </td>
            <td>
                {{ $link->service }}
            </td>
            <td>
                {{ $link->created_at->translatedFormat('j F Y H:i') }}
            </td>
            <td style="word-break: break-all; max-width: 1000px;">
                @if ($link->handled and $link->handler_error)
                <i class="fa-solid fa-circle-exclamation" style="color: red;"></i>
                @else
                @if ($link->handled)
                <i class="fa-regular fa-circle-check" style="color: green;"></i>
                @else
                <i class="fa-solid fa-clock-rotate-left"></i>
                @endif
                @endif
                {{ $link->handler_message }}
            </td>
            <td>
                @if ($link->challenged_on_google_at)
                <i class="fa-regular fa-envelope"></i>
                {{ $link->challenged_on_google_at?->translatedFormat('j F Y') }}
                @endif

                @if ($link->reinstated_on_google_at)
                <i class="fa-solid fa-arrow-right-long"></i>
                <i class="fa-regular fa-circle-check" style="color: green;"></i>
                {{ $link->reinstated_on_google_at?->translatedFormat('j F Y') }}
                @if (isset($link->is_canonical['error']))
                <span 
                class="badge text-bg-danger" 
                data-bs-toggle="tooltip" 
                data-bs-placement="top"
                data-bs-title="{{ $link->is_canonical['error'] }}">
                ошибка проверки на каноничность
                </span>
                @else
                @if ($link->is_canonical['result'])
                {{-- <span class="badge text-bg-success">каноническая</span> --}}
                @else
                <span 
                class="badge text-bg-danger" 
                data-bs-toggle="tooltip" 
                data-bs-placement="top"
                data-bs-title="{{ $link->is_canonical['canonicals']->implode(', ') }}">
                не каноническая
                </span>
                {{-- {{ $link->is_canonical['canonicals']->implode(', ') }} --}}
                @endif
                @endif
                @elseif ($link->canceled_on_google_at)
                <i class="fa-solid fa-arrow-right-long"></i>
                <i class="fa-solid fa-circle-exclamation" style="color: red;"></i>
                {{ $link->canceled_on_google_at?->translatedFormat('j F Y') }}
                @endif
            </td>
        </tr>
        @endforeach
        </table>
        <div class="row justify-content-between">
            <div class="col-12 col-sm-6 col-md-6">
                {{ $this->links->count() ? $this->links->links() : null }}
            </div>
            <div class="col-12 col-sm-6 col-md-4 text-end">
                <div class="input-group">
                    <select wire:model="actionWithSelectedLinks" class="form-control">
                        <option value="">С выбранными</option>
                        <option value="setHandledErrorFalse">Отметить как без ошибок</option>
                        <option value="formGoogleCounterComplaint">Оспорить в Гугле</option>
                        <option value="setAsReinstatedOnGoogle">Отметить восстановленными в Гугле</option>
                        <option value="setAsCanceledOnGoogle">Отметить отклоненными в Гугле</option>
                    </select>
                    <button class="btn btn-secondary" wire:click="submitActionWithSelectedLinks">ОК</button>
                </div>
            </div>
        </div>
    </div>
</div>
