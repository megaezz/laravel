<div>

    <ul class="nav nav-tabs" style="margin: 2px 2px 0 2px;">
        @foreach ($availablePlayers as $player)
        <li class="nav-item">
            <a wire:click="setActivePlayer('{{ $player->player }}')" style="color: #fff;" class="nav-link {{ ($activePlayer->player == $player->player) ? 'active' : '' }}">{{ $player->name }}</a>
        </li>
        @endforeach
    </ul>

    @if ($activePlayer)
    <div class="box visible" id="firstTab">
        @if ($activePlayer->component == 'component')
        <x-dynamic-component :component="'types.cinema.players.' . $activePlayer->player" :$domainMovie :$country />
        @endif
        @if ($activePlayer->component == 'livewire')
        <livewire:dynamic-component :is="'types.cinema.players.' . $activePlayer->player" :$domainMovie :$country />
        @endif
    </div>
    @endif
</div>