<div>
    <div id="player"></div>

    <style>
        #player {
            min-height: 420px !important;
            width: 100%;
            height: 100%;
        }
    </style>
    
    @assets
    <script src="/types/cinema/template/players/videodb/playerjs.com/playerjs-{{ $playerjsVersion }}.js"></script>
    <script src="/types/cinema/template/players/videodb/main.js?{{ $reload }}"></script>
    @endassets

    @script
    <script>
        initPlayer('{{ $this->playerJsData }}');
    </script>
    @endscript

    <!-- Yandex.Metrika counter -->
    @script
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym");

        ym(56489905, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:false
        });
    </script>
    @endscript
    
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/56489905" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</div>