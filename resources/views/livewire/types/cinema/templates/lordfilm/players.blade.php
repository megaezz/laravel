<div>
	<div class="fctrl fx-row fx-middle">
		<div class="tabs-sel fx-1">
			@foreach ($availablePlayers as $player)
			<span wire:click="setActivePlayer('{{ $player->player }}')" class="show-player-btn {{ ($activePlayer->player == $player->player) ? 'current' : '' }}">{{ $player->name }}</span>
			@endforeach
		</div>
		<div class="flight">Свет</div>
		<div class="ffav icon-left">
		</div>
		<div class="fcompl icon-left">
			<a href="javascript:AddComplaint('20434', 'news')"><span class="fa fa-exclamation-triangle"></span>Есть жалоба?</a>
		</div>
	</div>

	<div class="player-message"></div>
	<div wire:loading.remove class="tabs-b video-box visible" style="min-height: 400px;">
		@if ($activePlayer)
        <x-types.cinema.players :$activePlayer :$domainMovie :$country />
        @else
        <x-types.cinema.players.no />
        @endif
	</div>
</div>