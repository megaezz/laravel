<form method="get" action="/search" x-data="{ focus: false }" @@click.outside="focus = false">
    <input type="hidden" name="do" value="search" />
    <input type="hidden" name="subaction" value="search" />
    <div class="search-box">
        <input class="main-search" name="q" placeholder="Начните вводить название" autocomplete="off" type="text" wire:model.live="query" @@focus="focus = true" />
        <button type="submit"><span class="fa fa-search"></span></button>
    </div>
    <div class="search-helper" x-show="focus">
        <ul class="search-helper-list">
            @foreach ((object) $movies as $domainMovie)
            <li>
                <a href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}">{{ $domainMovie->movie->title_ru }} / {{ $domainMovie->movie->title_en }} ({{ $domainMovie->movie->localed_type_ru }} {{ $domainMovie->movie->year }})</a>
            </li>
            @endforeach
        </ul>
    </div>
</form>
