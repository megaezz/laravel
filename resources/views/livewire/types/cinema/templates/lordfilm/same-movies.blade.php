<div>
	@if ($this->movies->count())
	<div class="sect frels">
		<div class="sect-header fx-row fx-middle fx-start">
			<div class="frels-title">Рекомендуем ещё фильмы</div>
		</div>
		<div class="sect-cont sect-items clearfix" id="movies">
			@foreach ($this->movies as $domainMovie)
			<x-types.cinema.templates.lordfilm.movie :$domain :$domainMovie />
			@endforeach
		</div>
		@if ($this->showMoreButton)
		<div style="text-align: center;">
			<button class="btnShowMoreMovies" wire:click="showMore">Показать еще</button>
		</div>
		@endif
	</div>
	@endif
</div>