<button class="btn btn-warning savedMoviesBtn mb-3" data-toggle="collapse" data-target="#savedMoviesWrapper">
  <i class="fas fa-heart" style="color: red;"></i>
  <span style="color: #000;">
    Я смотрю
    @if ($this->movies->count())
    <span class="badge badge-danger">{{ $this->movies->count() }}</span>
    @endif
  </span>

  @teleport('#savedMoviesWrapper')
  @if ($this->movies->count())
  <div class="row">
      @foreach ($this->movies as $domainMovie)
      <x-types.cinema.templates.seasongo.movie :$domain :$domainMovie />
      @endforeach
      @else
      <h5>Чтобы добавить сюда фильм, нажмите в плеере на кнопку &laquo;<i class="fas fa-heart"></i> Я смотрю&raquo;</h5>
      @endif
  </div>
  @endteleport
</button>
