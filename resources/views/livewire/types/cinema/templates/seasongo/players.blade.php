<div class="panel-body">
    <div class="row p-2">
        <div class="col-12 col-sm-8">
            @foreach ($availablePlayers as $player)
            <button wire:click="setActivePlayer('{{ $player->player }}')" class="btn btn-secondary show-player-btn {{ ($activePlayer->player == $player->player) ? 'active' : '' }}" data-player="{{ $player->player }}">{{ $player->name }}</button>
            @endforeach
        </div>
        <div class="col-12 col-sm-4 text-right">
            <button wire:click="toggleWatchList" @class(['btn', 'btn-outline-light', 'active' => $this->isInWatchList()])>
                <i class="fas fa-heart"></i>
                &nbsp;Я смотрю
            </button>
        </div>
    </div>
    <div wire:loading.remove style="width: 100%; min-height: 400px;">
        @if ($activePlayer)
        <x-types.cinema.players :$activePlayer :$domainMovie :$country />
        @else
        <x-types.cinema.players.no />
        @endif
    </div>
</div>