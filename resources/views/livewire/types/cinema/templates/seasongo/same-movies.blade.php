<div>
	@if ($this->movies->count())
	<h3 style="font-size: 20px;">Вам может понравиться</h3>
	<div class="sect">
		<div class="sect-cont sect-items clearfix">
			@foreach ($this->movies as $domainMovie)
			<x-types.cinema.templates.seasongo.movie :$domain :$domainMovie />
			@endforeach
		</div>
	</div>
	@if ($this->showMoreButton)
	<div class="text-center">
		<button class="btn btn-success mb-3 btnShowMoreMovies" wire:click="showMore">Показать еще</button>
	</div>
	@endif
	@endif
</div>