<div class="full-text playerBlock">
    <div>
        @foreach ($availablePlayers as $player)
        <button wire:click="setActivePlayer('{{ $player->player }}')" class="playerBtn show-player-btn" @style(['background: #3F04CA;' => ($activePlayer->player == $player->player)])>{{ $player->name }}</button>
        @endforeach
    </div>

    <div wire:loading.remove class="tabs-b video-box visible" style="min-height: 400px;">
        @if ($activePlayer)
            <x-types.cinema.players :$activePlayer :$domainMovie :$country />
        @else
            <x-types.cinema.players.no />
        @endif
    </div>
</div>
