<form action="/search" class="main-menu-search" x-data="{ focus: false }" @@click.outside="focus = false">
    <input class="header__search-input main-search" type="text" name="q" placeholder="Поиск..." wire:model.live="query" @@focus="focus = true">
    <div class="search-helper" x-show="focus">
        <ul class="search-helper-list">
            @foreach ((object) $movies as $domainMovie)
            <li class="list-group-item">
                <a href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}">
                    {{ $domainMovie->movie->title }} ({{ $domainMovie->movie->localed_type }} {{ $domainMovie->movie->year }})
                </a>
            </li>
            @endforeach
        </ul>
    </div>
    <button class="header__search-button" type="button">
        <i class="icon ion-ios-search"></i>
    </button>
    <button class="header__search-close" type="button">
        <i class="icon ion-md-close"></i>
    </button>
</form>
