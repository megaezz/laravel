<div class="col-12 col-lg-6">
    <div class="mb-2">
        @foreach ($availablePlayers as $player)
        <button class="profile__btn show-player-btn {{ ($activePlayer->player == $player->player) ? 'active' : '' }}" wire:click="setActivePlayer('{{ $player->player }}')" data-player="{{ $player->player }}">{{ $player->name }}</button>
        @endforeach
    </div>
    <div class="player-message card__description"></div>
    <div>
        @if ($activePlayer)
        <x-types.cinema.players :$activePlayer :$domainMovie :$country />
        @else
        <x-types.cinema.players.no />
        @endif
    </div>
</div>