<div class="col-12 col-lg-4 col-xl-4">
	<div class="row" id="movies">
		<div class="col-12">
			<h2 class="section__title">Вам понравится</h2>
		</div>
		@foreach ($this->movies as $domainMovie)
		<x-types.cinema.templates.hotflix.movie :$domain :$domainMovie />
		@endforeach
	</div>
</div>