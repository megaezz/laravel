<div>
	@if ($this->movies->count())
	<div class="card">
		<div class="card-header">
			<h3 style="font-size: 20px;">
				<a href="/search?sameMovieId={{ $domainMovie->id }}&amp;order=topRated&amp;index">Фильмы и сериалы похожие на {{ $domainMovie->movie->title }}</a>
			</h3>
		</div>
		<div class="card-body">
			<div class="row" id="movies">
				@foreach ($this->movies as $domainMovie)
				<x-types.cinema.templates.turboserial.movie :$domain :$domainMovie />
				@endforeach
			</div>
			@if ($this->showMoreButton)
			<div class="text-center">
				<button class="btn btn-success mb-3 btnShowMoreMovies" wire:click="showMore">Показать еще</button>
			</div>
			@endif
		</div>
	</div>
	@endif
</div>