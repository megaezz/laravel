<form class="form-inline my-2 my-lg-0 main-menu-search" method="get" action="/search" x-data="{ focus: false }" @@click.outside="focus = false">
    <div class="btn-group">
        <input class="form-control mr-sm-2 main-search" type="search" name="q" placeholder="Начните вводить название" aria-label="Search" autocomplete="off" style="width: 216px;" wire:model.live="query" @@focus="focus = true">
        <div class="dropdown-menu search-helper" x-bind:class="{ 'show': focus }" style="width: 220px; padding: 0;">
            <ul class="list-group">
                @foreach ((object) $movies as $domainMovie)
                <li class="list-group-item">
                    <a href="{{ $domain->cinemaDomain->megaweb->getWatchLink($domainMovie->megaweb) }}">{{ $domainMovie->movie->title_ru }} / {{ $domainMovie->movie->title_en }} ({{ $domainMovie->movie->localed_type_ru }} {{ $domainMovie->movie->year }})</a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <button class="btn btn-outline-success my-0 my-sm-0" type="submit"><i class="fas fa-search"></i> Найти</button>
</form>
