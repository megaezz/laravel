<div>
	@if ($this->movies->count())
	<div class="text-underlined">
		Смотрите также
	</div>
	<div class="row mb-4">
		@foreach ($this->movies as $domainMovie)
		<x-types.cinema.templates.rserial.movie-short :$domain :$domainMovie />
		@endforeach
	</div>
	@if ($this->showMoreButton)
	<div class="text-center">
		<button class="btn btn-success mb-3 btnShowMoreMovies" wire:click="showMore">Показать еще</button>
	</div>
	@endif
	@endif
</div>