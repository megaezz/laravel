<div class="mb-3">
	<div class="p-2">
		@foreach ($availablePlayers as $player)
		<button wire:click="setActivePlayer('{{ $player->player }}')" @class(['btn', 'btn-outline-light', 'active' => ($activePlayer->player == $player->player)])>
			{{ $player->name }}
		</button>
		@endforeach
    </div>
	<div wire:loading.remove>
		@if ($activePlayer)
		<x-types.cinema.players :$activePlayer :$domainMovie :$country />
		@else
		<x-types.cinema.players.no />
		@endif
	</div>
</div>