<div>
    <div class="second-menu">
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs">
                    @foreach ($this->sections as $section)
                    <li @class(['nav-item'])>
                        <button wire:click="$set('activeSection', '{{ $section['name'] }}')" @class(['btn nav-link', 'active' => ($activeSection == $section['name'])])>
                            {!! $section['button'] !!}
                        </button>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    @if ($activeSection)
    <ul @class(['second-menu-content tabs clearfix mb-4'])>
        @if ($activeSection == 'genres')
        @foreach ($this->genres as $tag)
        <li><a href="{{ $domain->cinemaDomain->megaweb->getGenreLink($tag->megaweb) }}">{{ mb_ucfirst($tag->tag) }}</a></li>
        @endforeach
        @endif

        @if ($activeSection == 'countries')
        <li><a href="/group/83~Россия">Российские</a></li>
        <li><a href="/group/110~Украина">Украинские</a></li>
        <li><a href="/group/126~Беларусь">Белорусские</a></li>
        @endif

        @if ($activeSection == 'channels')
        @foreach ($this->channels as $tag)
        <li><a href="{{ $domain->cinemaDomain->megaweb->getGenreLink($tag->megaweb) }}">{{ mb_ucfirst($tag->tag) }}</a></li>
        @endforeach
        @endif

        @if ($activeSection == 'compilations')
        @foreach ($this->compilations as $group)
        <li><a href="{{ $domain->cinemaDomain->megaweb->getGroupLink($group->megaweb) }}">{{ mb_ucfirst($group->name) }}</a></li>
        @endforeach
        @endif
    </ul>
    @endif
</div>
