<div x-data="{ focus: false }">
    <div class="js-lightsearch-shader lightsearch__shader" x-bind:hidden="!focus"></div>
    <form class="js-lightsearch lightsearch" name="searchform" wire:submit="submit">
        <input type="text" class="js-lightsearch-input lightsearch-input" autocomplete="off" placeholder="Поиск..." wire:model.live="query" @@focus="focus = true"">
        <button type="button" class="js-lightsearch-reset lightsearch-reset lightsearch-btn" title="Очистить поле ввода" hidden=""></button>
        <button type="submit" class="lightsearch-btn">ok</button>

        <div class="js-lightsearch-content lightsearch__content" x-bind:hidden="!focus">
            {{-- TODO ссылка на все результаты --}}
            <a href="#" class="lightsearch__showAll">Все результаты поиска →</a>
            {{-- <a href="/search/%D0%BF%D1%80%D0%B8" class="lightsearch__showAll">Все результаты поиска (3158) →</a> --}}
            <a href="#" class="js-lightsearch-close lightsearch__close" @@click="focus = false"></a>

            <div class="js-lightsearch-results lightsearch__results" style="max-height: 698px;">

                @foreach ((object) $movies as $domainMovie)
                <a href="{{ $domain->toRoute->movieView($domainMovie) }}" class="lightsearch__item">
                    <div class="lightsearch__itemTitle">
                        {{ $domainMovie->movie->title }}
                        @if ($domainMovie->movie->year) ({{$domainMovie->movie->year}}) @endif
                    </div>

                    <div class="lightsearch__itemContent">
                        <div class="lightsearch__itemImage" style="background-image: url('/storage/images/w100{{ $domainMovie->movie->poster_or_placeholder }}')"></div>
                        <div class="lightsearch__itemInfo">
                            <div>{{ $domainMovie->movie->title }}</div>
                            {{-- <div style="color: #F8D268">
                                BDRip 1080
                            </div> --}}
                            <div>
                                <b style="color: #eee">{{ $domainMovie->movie->localed_type }}</b>
                                @if ($domainMovie->movie->tags->where('type', 'country')->first())
                                , {{ $domainMovie->movie->tags->where('type', 'country')->first()->tag }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="lightsearch__itemRating">
                        <div class="lightsearch__itemRating-kp">КП: {{ $domainMovie->movie->kinopoisk_rating }}</div>
                        <div class="lightsearch__itemRating-imdb">IMDb: {{ $domainMovie->movie->imdb_rating }}</div>
                        <div class="lightsearch__itemRating-main">
                            <b>{{ round(($domainMovie->movie->rating_sko ?? 0), 1) * 10 }} / 10</b>
                            ({{ round((($domainMovie->movie->kinopoisk_votes + $domainMovie->movie->imdb_votes) ?? 0) / 2) }} гол)
                        </div>
                    </div>
                </a>
                @endforeach

            </div>


            {{-- <div class="js-lightsearch-form-category">
                <div class="lightsearch__category">Поиск в категории</div>


                <a href="/search/filmy/%D0%BF%D1%80%D0%B8" class="lightsearch__categoryItem">
                    <b>при</b> в категории Фильмы (2056)
                </a>

                <a href="/search/serialy/%D0%BF%D1%80%D0%B8" class="lightsearch__categoryItem">
                    <b>при</b> в категории Сериалы (373)
                </a>

                <a href="/search/anime/%D0%BF%D1%80%D0%B8" class="lightsearch__categoryItem">
                    <b>при</b> в категории Аниме (252)
                </a>

                <a href="/search/multfilmy/%D0%BF%D1%80%D0%B8" class="lightsearch__categoryItem">
                    <b>при</b> в категории Мультфильмы (194)
                </a>

                <a href="/search/zarubezhnye-serialy/%D0%BF%D1%80%D0%B8" class="lightsearch__categoryItem">
                    <b>при</b> в категории Зарубежные сериалы (186)
                </a>
            </div> --}}
        </div>
    </form>
</div>
