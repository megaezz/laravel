<div class="section" id="contentss">
    <ul class="tabs">
        @foreach ($availablePlayers as $player)
        <li wire:click="setActivePlayer('{{ $player->player }}')" class="{{ ($activePlayer->player == $player->player) ? 'current' : '' }}">{{ $player->name }}</li>
        @endforeach
    </ul>
    {{-- TODO: рейтинг наползает на кнопки плееров в моб версии, а на самом плеере черный блок (особенно видно, если плееры отсутствуют) --}}
    <div id="ratig-layer-{{ $domainMovie->id }}" style="float: right;right: 10px;position: absolute;background: black;margin-top: 30px">
        <div class="rating">
            <ul class="unit-rating">
                <li class="current-rating" style="width:{{ round((float)$domainMovie->movie->rating_sko, 1) * 10 }}%;">
                    {{ round((float)$domainMovie->movie->rating_sko, 1) * 10 }}
                </li>
                <li><a href="#" title="Плохо" class="r1-unit" onclick="doRate('1', '{{ $domainMovie->id }}'); return false;">1</a></li>
                <li><a href="#" title="Приемлемо" class="r2-unit" onclick="doRate('2', '{{ $domainMovie->id }}'); return false;">2</a></li>
                <li><a href="#" title="Средне" class="r3-unit" onclick="doRate('3', '{{ $domainMovie->id }}'); return false;">3</a></li>
                <li><a href="#" title="Хорошо" class="r4-unit" onclick="doRate('4', '{{ $domainMovie->id }}'); return false;">4</a></li>
                <li><a href="#" title="Отлично" class="r5-unit" onclick="doRate('5', '{{ $domainMovie->id }}'); return false;">5</a></li>
            </ul>
            <div class="rating_digits_1" style="display: inline-block;float: right;">
                <span>{{ round((float)$domainMovie->movie->rating_sko, 1) }}</span>/10
                (<span>{{ round(((int)$domainMovie->movie->kinopoisk_votes + (int)$domainMovie->movie->imdb_votes) / 2) }} гол.</span>)
            </div>
        </div>
    </div>

    <style>
        #contentss > a {
            float: right;
            margin-top: 5px;
            margin-right: 15px;
        }
    </style>

    <div style="clear:both"></div>

    {{-- чтобы при смене плеера отбражался спинер, вместо того, чтобы продолжал отображаться прошлый --}}
    <div wire:loading.remove class="box visible" id="firstTab">
        @if ($activePlayer)
        <x-types.cinema.players :$activePlayer :$domainMovie :$country />
        @else
        <x-types.cinema.players.no />
        @endif
    </div>
</div>