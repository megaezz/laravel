<div>
    <!--noindex-->
    <div class="funct">
        <div class="xsort-area ignore-select">
            <div style="color:#fff;">Сортировка:</div>
            <div class="xsort-div" x-data="{ isOrderVisible: false }">
                <div class="xsort-selected" x-on:click="isOrderVisible = !isOrderVisible">
                    <span>{{ $orders[$order] }}</span>
                </div>
                <ul class="xsort-ul" x-bind:style="{ 'display': isOrderVisible ? 'block' : 'none' }">
                    @foreach ($orders as $orderType => $orderName)
                    <li wire:click="$set('order', '{{ $orderType }}')" x-on:click="isOrderVisible = false">
                        {{ $orderName }}
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="xsort-div" x-data="{ isGenreVisible: false }">
                <div class="xsort-selected" x-on:click="isGenreVisible = !isGenreVisible">{{ $this->genre->name ?? 'Все жанры' }}</div>
                <ul class="xsort-ul" x-bind:style="{ 'display': isGenreVisible ? 'block' : 'none' }">
                    <li wire:click="$set('genre_id', null)" x-on:click="isGenreVisible = false">Все жанры</li>
                    @foreach ($genres as $loopGenre)
                    <li wire:click="$set('genre_id', '{{ $loopGenre->id }}')" x-on:click="isGenreVisible = false">{{ $loopGenre->name }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="xsort-div" x-data="{ isYearVisible: false }">
                <div class="xsort-selected" x-on:click="isYearVisible = !isYearVisible">{{ $this->year ?? 'Все года' }}</div>
                <ul class="xsort-ul" x-bind:style="{ 'display': isYearVisible ? 'block' : 'none' }">
                    <li wire:click="$set('year', null)" x-on:click="isYearVisible = false">Все года</li>
                    @foreach($years as $loopYear)
                    <li wire:click="$set('year', {{ $loopYear }})" x-on:click="isYearVisible = false">{{ $loopYear }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="xsort-div" x-data="{ isCountryVisible: false }">
                <div class="xsort-selected" x-on:click="isCountryVisible = !isCountryVisible">{{ $this->country->name ?? 'Все страны' }}</div>
                <ul class="xsort-ul" x-bind:style="{ 'display': isCountryVisible ? 'block' : 'none' }">
                    <li wire:click="$set('country_id', null)" x-on:click="isCountryVisible = false">Все страны</li>
                    @foreach ($countries as $loopCountry)
                    <li wire:click="$set('country_id', '{{ $loopCountry->id }}')" x-on:click="isCountryVisible = false">{{ $loopCountry->name }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="xsort-div xsort-div-clearall" title="Сбросить выбранные значения" wire:click="resetFilters"></div>
        </div>
    </div>
    <!--/noindex-->
    @foreach($this->movies as $domainMovie)
    <x-types.cinema.templates.kinogo.shortstorie :$domain :$domainMovie />
    @endforeach
    {{ $this->movies->links('types.cinema.templates.kinogo.paginator-livewire') }}

    @script
    <script>
        $wire.on('change-images', () => {
            lazyLoadImages();
        });
    </script>
    @endscript
</div>