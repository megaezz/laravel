<div class="container">
    <div class="row g-2 mb-3">
        @if ($this->blocked_domains->count() or ($domain_filter['hasBlockedDomains'] ?? null))
        <div class="col-auto">
            <button @class(['btn btn-danger', 'active' => ($domain_filter['hasBlockedDomains'] ?? null)]) type="button" wire:click="$toggle('domain_filter.hasBlockedDomains')">
                Блокировки
                <span class="badge text-bg-light">{{ $this->blocked_domains->count() }}</span>
            </button>
        </div>
        @endif
        @if ($this->failed_domains->count() or ($domain_filter['hasFailedDomains'] ?? null))
        <div class="col-auto">
            <button @class(['btn btn-warning', 'active' => ($domain_filter['hasFailedDomains'] ?? null)]) type="button" wire:click="$toggle('domain_filter.hasFailedDomains')">
                Проблемы
                <span class="badge text-bg-light">{{ $this->failed_domains->count() }}</span>
            </button>
        </div>
        @endif
        <div class="col-auto">
            <div class="dropdown">
                <button class="btn btn-light dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    @if ($domain_filter['type'])
                    {{ ucfirst($domain_filter['type']) }} <span class="badge text-bg-light">{{ $this->types->where('type', $domain_filter['type'])->first()->q }}</span>
                    @else
                    Все <span :data-type="null" class="badge text-bg-light">{{ $allDomainsCount }}</span>
                    @endif
                  {{-- Dropdown button --}}
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <button class="dropdown-item {{ $domain_filter['type'] ? '' : 'active' }}" wire:click="setDomainFilter('type',null)" type="button">
                            Все <span :data-type="null" class="badge text-bg-light">{{ $allDomainsCount }}</span>
                        </button>
                    </li>
                    @foreach ($this->types as $type)
                    <li>
                        <button class="dropdown-item {{ ($type->type == $domain_filter['type'])?'active':'' }}" wire:click="setDomainFilter('type','{{ $type->type }}')">
                            {{ ucfirst($type->type) }} <span class="badge text-bg-light">{{ $type->q }}</span>
                        </button>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-6 col-xs-6 col-md-3 col-lg-3 col-xl-2 form-group">
            <select class="form-select" wire:model.live="domain_filter.order" wire:change="setDomainFilter('order',$event.target.value)">
                <option value="">Выберите сортировку</option>
                <option value="add_date desc">Сначала новые</option>
                <option value="add_date">Сначала старые</option>
                <option value="metrika.visits desc">Сначала c трафиком</option>
                <option value="metrika.visits">Сначала без трафика</option>
                <option value="recent mirrors first">Сначала недавние переклейки</option>
                <option value="old mirrors first">Сначала давно не клеенные</option>
            </select>
        </div>
        <div class="col-auto">
            <button type="button" class="btn btn-light" data-bs-toggle="collapse" data-bs-target="#filters">
                <i class="fa-regular fa-filter"></i> Фильтры
                @if ($this->active_domain_filters->count())
                <span class="badge text-bg-light">
                    {{ $this->active_domain_filters->count() }}
                </span>
                @endif
            </button>
        </div>
        @can('everything')
        <div class="col-auto">
            <button class="btn btn-light" type="button" data-bs-toggle="collapse" data-bs-target="#domainList">
                <i class="fa-regular fa-rectangle-list"></i> Список
            </button>
        </div>
        <div class="col-auto">
            <button class="btn btn-light" type="button" data-bs-toggle="collapse" data-bs-target="#addDomain">
                <i class="fa-solid fa-plus"></i> Создать
            </button>
        </div>
        @endcan
        <div class="col-auto col-md-4 col-lg-3 col-xxl-2 ms-md-auto">
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Поиск" wire:model.live="domain_filter.search" />
                @if (!empty($domain_filter['search']))
                <div class="input-group-text">
                    <button class="btn btn-sm" wire:click="$set('domain_filter.search', '')">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                @endif
            </div>
            @if ($searchedDomain)
            <div class="dropdown-menu show">
                <div class="dropdown-item">
                    <x-types.engine.admin.mirrors.domain :domain="$searchedDomain" />
                </div>
            </div>
            @endif
        </div>
    </div>

    @can('everything')
	<div class="row mb-3">
        <!-- игнорируем перерисовку, чтобы колапс не закрывался -->
        <div class="col-12 collapse" id="addDomain" wire:ignore.self>
            <form class="col-md-6 col-lg-4" wire:submit.prevent="createDomain">
                <div class="input-group">
                    <input class="form-control" type="text" name="domain" value="" placeholder="Домен" wire:model.live="create_domain_name">
                    <button class="btn btn-secondary" wire:loading.attr="disabled">Создать {{ $create_domain_name }}</button>
                </div>
            </form>
        </div>
        <div class="col-12 {{ $this->domainList ? 'collapsed' : 'collapse' }}" id="domainList">
            <div class="row">
                <div class="col-12 col-md-3">
                    <textarea placeholder="Список доменов" class="form-control" wire:model.live="domainList" rows="10"></textarea>
                </div>
                <div class="col-12 col-md-3">
                    <p>Доменов: {{ $this->domainsInList->count() }}</p>
                    @foreach ($this->domainsInList as $domain)
                    <a href="{{ route('domain.settings',[ 'domain' => $domain->domain ]) }}">{{ $domain->domain_decoded }}</a><br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endcan

    <div class="row mb-3">
        <div class="col-12">
			<div class="form-inline mb-3 collapse" id="filters" wire:ignore.self>
				@if($this->domain_filter['type'] == 'cinema')
				<div class="row mb-2">
					<div class="col-6 col-lg-3">
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithRussianMovies" wire:model.live="domain_filter.withRussianMovies">
							<label class="form-check-label" for="filterWithRussianMovies">С РФ фильмами</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithoutRussianMovies" wire:model.live="domain_filter.withoutRussianMovies">
							<label class="form-check-label" for="filterWithoutRussianMovies">Без РФ фильмов</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithRiskyStudios" wire:model.live="domain_filter.withRiskyStudios">
							<label class="form-check-label" for="filterWithRiskyStudios">С риск. контентом</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithoutRiskyStudios" wire:model.live="domain_filter.withoutRiskyStudios">
							<label class="form-check-label" for="filterWithoutRiskyStudios">Без риск. контента</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithAiDescriptions" wire:model.live="domain_filter.withAiDescriptions">
							<label class="form-check-label" for="filterWithAiDescriptions">AI описания</label>
						</div>
					</div>
					<div class="col-6 col-lg-3">
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterSerialsOnly" wire:model.live="domain_filter.serialsOnly">
							<label class="form-check-label" for="filterSerialsOnly">Только сериалы</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterUseAltDescriptions" wire:model.live="domain_filter.useAltDescriptions">
							<label class="form-check-label" for="filterUseAltDescriptions">Альт. описания</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterEpisodically" wire:model.live="domain_filter.episodically">
							<label class="form-check-label" for="filterEpisodically">Посерийники</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterMoviesMinYear" wire:model.live="domain_filter.moviesMinYear">
							<label class="form-check-label" for="filterMoviesMinYear">С минимальным годом</label>
						</div>
						{{-- <div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithProblems" wire:model.live="domain_filter.with_problems">
							<label class="form-check-label" for="filterWithProblems">С проблемами</label>
						</div> --}}
					</div>
					<div class="col-6 col-lg-4">
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithDeletePlayerOnlyIfMainDomain" wire:model.live="domain_filter.withDeletePlayerOnlyIfMainDomain">
							<label class="form-check-label" for="filterWithDeletePlayerOnlyIfMainDomain">Удалять плеер только если ркн на основу</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithDeletePlayerOnlyIfAbuseOnPage" wire:model.live="domain_filter.withDeletePlayerOnlyIfAbuseOnPage">
							<label class="form-check-label" for="filterWithDeletePlayerOnlyIfAbuseOnPage">Удалять плеер с заменой дмца</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithDeletePlayerWhenRuAndNotCanonical" wire:model.live="domain_filter.withDeletePlayerWhenRuAndNotCanonical">
							<label class="form-check-label" for="filterWithDeletePlayerWhenRuAndNotCanonical">Скрывать плеер на неканон. страницах для рф</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterRknForRuOnly" wire:model.live="domain_filter.rknForRuOnly">
							<label class="form-check-label" for="filterRknForRuOnly">Показывать заблоченный плеер для УКР</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterShowTrailerForNonSng" wire:model.live="domain_filter.showTrailerForNonSng">
							<label class="form-check-label" for="filterShowTrailerForNonSng">Показывать трейлеры для не СНГ</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterHideCanonical" wire:model.live="domain_filter.hideCanonical">
							<label class="form-check-label" for="filterHideCanonical">Скрывать каноникал</label>
						</div>
					</div>
					<div class="col-6 col-lg-2">
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterIncludeYoutube" wire:model.live="domain_filter.includeYoutube">
							<label class="form-check-label" for="filterIncludeYoutube">Ютуб</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithMovieads" wire:model.live="domain_filter.withMovieads">
							<label class="form-check-label" for="filterWithMovieads">Movieads</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithVideoroll" wire:model.live="domain_filter.withVideoroll">
							<label class="form-check-label" for="filterWithVideoroll">Videoroll</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithHdvbSticker" wire:model.live="domain_filter.withHdvbSticker">
							<label class="form-check-label" for="filterWithHdvbSticker">Hdvb Sticker</label>
						</div>
					</div>
				</div>
				@endif
				<div class="row">
					<div class="col-6 col-lg-3">
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterYandexNotAllowed" wire:model.live="domain_filter.yandexNotAllowed">
							<label class="form-check-label" for="filterYandexNotAllowed">Закрыт от Яндекса</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterGoogleMoreThenYandex" wire:model.live="domain_filter.googleMoreThenYandex">
							<label class="form-check-label" for="filterGoogleMoreThenYandex">Больше Гугла</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterYandexMoreThenGoogle" wire:model.live="domain_filter.yandexMoreThenGoogle">
							<label class="form-check-label" for="filterYandexMoreThenGoogle">Больше Яндекса</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterIsOff" wire:model.live="domain_filter.is_off">
							<label class="form-check-label" for="filterIsOff">Отключен</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterDoesntHaveRenewedDomains" wire:model.live="domain_filter.doesntHaveRenewedDomains">
							<label class="form-check-label" for="filterDoesntHaveRenewedDomains">Нет продленных доменов</label>
						</div>
                        <div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterHasRenewedDomains" wire:model.live="domain_filter.hasRenewedDomains">
							<label class="form-check-label" for="filterHasRenewedDomains">С продленными доменами</label>
						</div>
					</div>
					<div class="col-6 col-lg-3">
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterBlocked" wire:model.live="domain_filter.blocked">
							<label class="form-check-label" for="filterBlocked">Новые блокировки</label>
						</div>
                        <div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterFailedMonitoring" wire:model.live="domain_filter.failedMonitoring">
							<label class="form-check-label" for="filterFailedMonitoring">Проблемы мониторинга</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithTraffic" wire:model.live="domain_filter.with_traffic">
							<label class="form-check-label" for="filterWithTraffic">Трафик > 10k/мес</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterHreflang" wire:model.live="domain_filter.hreflang">
							<label class="form-check-label" for="filterHreflang">Hreflang</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterRuKnownBlock" wire:model.live="domain_filter.ruKnownBlock">
							<label class="form-check-label" for="filterRuKnownBlock">Имеет известные блоки по РФ</label>
						</div>
                        <div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterRenewError" wire:model.live="domain_filter.renewError">
							<label class="form-check-label" for="filterRenewError">Заканчиваются / закончились</label>
						</div>
					</div>
					<div class="col-6 col-lg-4">
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithoutClientDomain" wire:model.live="domain_filter.withoutClientDomain">
							<label class="form-check-label" for="filterWithoutClientDomain">Без клиентского поддомена</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterWithClientRedirect" wire:model.live="domain_filter.with_client_redirect">
							<label class="form-check-label" for="filterWithClientRedirect">C редиректом клиентов на основу</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterIsMaskingDomain" wire:model.live="domain_filter.is_masking_domain">
							<label class="form-check-label" for="filterIsMaskingDomain">Маскировочный</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterAllowNotValidReferersForClients" wire:model.live="domain_filter.allow_not_valid_referers_for_clients">
							<label class="form-check-label" for="filterAllowNotValidReferersForClients">Разрешать не валидные рефереры для клиентов (v3)</label>
						</div>
					</div>
					<div class="col-6 col-lg-2">
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterAccessSchemeV1" wire:model.live="domain_filter.accessSchemeV1">
							<label class="form-check-label" for="filterAccessSchemeV1">Схема доступа v1</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterAccessSchemeV2" wire:model.live="domain_filter.accessSchemeV2">
							<label class="form-check-label" for="filterAccessSchemeV2">Схема доступа v2</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterAccessSchemeV3" wire:model.live="domain_filter.accessSchemeV3">
							<label class="form-check-label" for="filterAccessSchemeV3">Схема доступа v3</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterHasFolder" wire:model.live="domain_filter.hasFolder">
							<label class="form-check-label" for="filterHasFolder">Шаблон изменен</label>
						</div>
						<div class="form-check form-check-inline form-switch mr-3">
							<input type="checkbox" class="form-check-input" id="filterTechnitium" wire:model.live="domain_filter.technitium">
							<label class="form-check-label" for="filterTechnitium">Technitium</label>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-6">
            <h3>Домены <span class="badge text-bg-light">{{ $this->filtered_domains->count() }}</span></h3>
        </div>
        <div class="col-6 d-flex align-items-center justify-content-end">
            <span class="badge text-bg-light">{{ round($this->metrika_total_visits / 1000,1) }}к виз./мес.</span>
        </div>
        <div class="col-12">
            {{-- эти дивы нужны, иначе появляется иногда ошибка JS appendChild  --}}
            <div>
			@foreach ($this->filtered_domains->take($filtered_domains_limit) as $domain)
			<livewire:types.engine.admin.mirrors.domain-card :$domain :key="$domain->domain" />
			@endforeach
            </div>

			@if ($filtered_domains_limit and $this->filtered_domains->count() > $filtered_domains_limit)
			<div class="text-center mb-3">
				<button class="btn btn-secondary" wire:click="setFilteredDomainsLimit(null)">Показать еще {{ $this->filtered_domains->count() - $filtered_domains_limit }}</button>
			</div>
			@endif
		</div>
	</div>
    <livewire:types.engine.admin.mirrors.domain-settings-modal />
</div>
