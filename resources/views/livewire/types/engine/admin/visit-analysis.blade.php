<div>
    <div class="row">
        <div class="col-md-2">
            <label>Тип выдачи</label>
            <form>
                <select class="form-control" wire:model.live="type">
                    @foreach ($this->types as $type)
                    <option value="{{ $type }}">{{ $type }}</option>
                    @endforeach
                </select>
            </form>
        </div>
        <div class="col-md-2">
            <label>Сервис</label>
            <form>
                <select class="form-control" wire:model.live="service">
                    <option value="null">Не важно</option>
                    @foreach ($this->services as $service)
                    <option value="{{ $service }}">{{ $service }}</option>
                    @endforeach
                </select>
            </form>
        </div>
        <div class="col-md-2">
            <label>Баны</label>
            <form>
                <select class="form-control" wire:model.live="withBanInfo">
                    <option value="null">Не важно</option>
                    <option value="true">Только с банами</option>
                    <option value="false">Только без банов</option>
                </select>
            </form>
        </div>
        <div class="col-md-3">
            <label>Реальные боты</label>
            <form>
                <select class="form-control" wire:model.live="withRealBots">
                    <option value="null">Не важно</option>
                    <option value="true">Только с реальными ботами</option>
                    <option value="false">Только без реальных ботов</option>
                </select>
            </form>
        </div>
        <div class="col-md-1">
            <label>Дней</label>
            <form>
                <select class="form-control" wire:model.live="days">
                    @foreach ([1,2,3,4,5,6,7] as $days)
                    <option value="{{ $days }}">{{ $days }}</option>
                    @endforeach
                </select>
            </form>
        </div>
        <div class="col-md-1">
            <label>Лимит</label>
            <form>
                <select class="form-control" wire:model.live="limit">
                    @foreach ([100,200,500,1000,1500,2000,3000] as $limit)
                    <option value="{{ $limit }}">{{ $limit }}</option>
                    @endforeach
                </select>
            </form>
        </div>
        <div class="col-md-1">
            <label>q ></label>
            <form>
                <select class="form-control" wire:model.live="q">
                    @foreach ([0, 1, 2, 3, 5, 10, 20, 50, 100] as $q)
                    <option value="{{ $q }}">{{ $q }}</option>
                    @endforeach
                </select>
            </form>
        </div>
    </div>
    {{-- в класе компонента тоже назначен вывод точек в методе placeholder, но пришлось повторить еще и здесь, т.к. placeholder выводится только при первой загрузке страницы, а wire:loading срабатывает в процессе изменений уже загруженного компонента  --}}
    <div wire:loading>
        <x-placeholders.dots />
    </div>
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>IP</th>
                <th>ASN</th>
                <th>Hostname</th>
                <th>q</th>
                <th>Description</th>
                <th>Hide Player</th>
                <th>Active</th>
            </tr>
        </thead>
        @foreach ($this->visits as $visit)
        <tr>
            <td>{{ $loop->index }}</td>
            <td>
                <a style="{{ $visit->ipRelation?->is_real_bot ? 'color: green;' : '' }}" href="{{ route('visits', ['ip' => $visit->ip, 'days' => $this->days, 'calcQueryWasAbusedByServices' => (($this->type == 'links') ? true : null)]) }}">{{ $visit->ip }}</a>
            </td>
            <td>{{ $visit->geoIp->getAsnOrganization() }}</td>
            <td>{{ $visit->ipRelation?->hostname }}</td>
            <td>{{ $visit->q }}</td>
            <td>{{ $visit->description }}</td>
            <td>{{ $visit->hide_player }}</td>
            <td>{{ $visit->active }}</td>
        </tr>
        @endforeach
    </table>
</div>
