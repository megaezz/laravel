<div class="container" {{ $poll ? 'wire:poll.1000ms' : '' }}>
    <div class="row">
        @foreach ($nodes as $node)
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                <div class="card text-bg-light mb-3">
                    <div class="card-header">{{ $node->name }}</div>
                    <div class="card-body">
                        <h5 class="card-title">LA <code>{{ $node->load_average[0] ?? '?' }}</code></h5>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="card text-bg-light mb-3">
                <div class="card-header">HTTP</div>
                <div class="card-body">
                    <h5 class="card-title"><code>{{ $requestsPerSec }}</code> rps /
                        <code>{{ round($avgTime->avg_time ?? 0) }}</code> ms</h5>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="card text-bg-light mb-3">
                <div class="card-header">Banned</div>
                <div class="card-body">
                    <h5 class="card-title"><code>{{ $banRequestsPerSec }}</code> rps</h5>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="card text-bg-light mb-3">
                <div class="card-header">DB</div>
                <div class="card-body">
                    <h5 class="card-title">
                        <code>{{ $mysqlRequestsPerSec }}</code> qps / <code>{{ $mysqlConnections }}</code> con
                    </h5>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="card text-bg-light mb-3">
                <div class="card-header">PHP-FPM</div>
                <div class="card-body">
                    <h5 class="card-title"><code>{{ $phpFpmProcesses }}</code> proc.</h5>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="card text-bg-light mb-3">
                <div class="card-header">Opcache</div>
                <div class="card-body">
                    Used: <code>{{ round($opcache['memory_usage']['used_memory'] / 1024 / 1024) }} Mb.</code><br>
                    Free: <code>{{ round($opcache['memory_usage']['free_memory'] / 1024 / 1024) }} Mb.</code><br>
                    Wasted: <code>{{ round($opcache['memory_usage']['current_wasted_percentage'], 1) }}%</code>
                    <code>
                        {{ $opcache['opcache_enabled'] ? '' : 'отключен' }}
                        {{ $opcache['cache_full'] ? 'переполнен' : '' }}
                    </code>
                </div>
            </div>
        </div>
    </div>
    <button wire:click="$toggle('poll')" class="btn btn-light mb-3">
        &nbsp;
        @if ($poll)
            <i class="fa-solid fa-pause"></i>
        @else
            <i class="fa-solid fa-play"></i>
        @endif
        &nbsp;
    </button>
    <button wire:click="$toggle('showFakePlugsStat')"
        class="btn btn-light mb-3 {{ $showFakePlugsStat ? 'active' : '' }}">
        Фейк заглушки
    </button>
    <button wire:click="$toggle('showIsNotRealBotStat')"
        class="btn btn-light mb-3 {{ $showIsNotRealBotStat ? 'active' : '' }}">
        Фейк боты
    </button>
    <button wire:click="$toggle('showMysqlProcesslist')"
        class="btn btn-light mb-3 {{ $showMysqlProcesslist ? 'active' : '' }}">
        Запросы БД
    </button>
    <button wire:click="$toggle('showCloudflareStat')"
        class="btn btn-light mb-3 {{ $showCloudflareStat ? 'active' : '' }}">
        Cloudflare
    </button>
    <a class="btn btn-light mb-3" href="{{ route('visits') }}">
        Визиты и логи
    </a>
    <a class="btn btn-light mb-3"
        href="{{ route('visit-analysis', ['type' => 'links', 'days' => 1, 'withRealBots' => 0, 'q' => 3]) }}">
        Анализ визитов
    </a>
    @if ($showFakePlugsStat)
        <div class="row">
            <div class="col-12 col-md-5">
                <table class="table table-sm table-light table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Fake plugs by User Agent</th>
                            <th>Rows</th>
                        </tr>
                    </thead>
                    @foreach ($this->fakePlugsStat->byUserAgent as $v)
                        <tr>
                            <td>
                                <a
                                    href="{{ route('visits', ['userAgent' => $v->userAgent, 'data' => 'logs', 'filterLogText' => 'Показали фейк заглушку']) }}">
                                    {{ $v->userAgent }}
                                </a>
                            </td>
                            <td>{{ $v->q }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-12 col-md-5">
                <table class="table table-sm table-light table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Fake plugs by Referer</th>
                            <th>Rows</th>
                        </tr>
                    </thead>
                    @foreach ($this->fakePlugsStat->byReferer as $v)
                        <tr>
                            <td style="word-break: break-all">
                                {{ $v->referer }}
                            </td>
                            <td>{{ $v->q }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-12 col-md-2">
                <table class="table table-sm table-light table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Fake plugs by IP</th>
                            <th>Rows</th>
                        </tr>
                    </thead>
                    @foreach ($this->fakePlugsStat->byIp as $v)
                        <tr>
                            <td>
                                <a style="{{ $v->visit?->ipRelation?->is_real_bot ? 'color: green;' : '' }}"
                                    href="{{ route('visits', ['ip' => $v->ip, 'data' => 'logs', 'filterLogText' => 'Показали фейк заглушку']) }}">
                                    {{ $v->ip }}
                                </a>
                            </td>
                            <td>{{ $v->q }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endif
    @if ($showIsNotRealBotStat)
        <div class="row">
            <div class="col-12 col-md-6">
                <table class="table table-sm table-light table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Is not real bot by User Agent</th>
                            <th>Rows</th>
                        </tr>
                    </thead>
                    @foreach ($this->isNotRealBotStat->byUserAgent as $v)
                        <tr>
                            <td>
                                <a
                                    href="{{ route('visits', ['userAgent' => $v->userAgent, 'data' => 'logs', 'filterLogText' => 'Is not real bot']) }}">
                                    {{ $v->userAgent }}
                                </a>
                            </td>
                            <td>{{ $v->q }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-12 col-md-6">
                <table class="table table-sm table-light table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Is not real bot by IP</th>
                            <th>Rows</th>
                        </tr>
                    </thead>
                    @foreach ($this->isNotRealBotStat->byIp as $v)
                        <tr>
                            <td>
                                <a style="{{ $v->visit?->ipRelation?->is_real_bot ? 'color: green;' : '' }}"
                                    href="{{ route('visits', ['ip' => $v->ip, 'data' => 'logs', 'filterLogText' => 'Is not real bot']) }}">
                                    {{ $v->ip }}
                                </a>
                            </td>
                            <td>{{ $v->q }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endif
    @if ($showMysqlProcesslist)
        <div class="row">
            <div class="col-12">
                <table class="table table-sm table-light table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Db</th>
                            <th>Command</th>
                            <th>Time</th>
                            <th>Info</th>
                        </tr>
                    </thead>
                    @foreach ($this->mysqlProcesslist() as $v)
                        <tr>
                            <td>{{ $v->db }}</td>
                            <td>{{ $v->Command }}</td>
                            <td>{{ $v->Time }}</td>
                            <td>{{ $v->Info }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12 col-md-4 col-lg-3">
            <table class="table table-sm table-light table-striped">
                <thead class="table-dark">
                    <tr>
                        <th>Route</th>
                        <th>Rpm</th>
                        <th>Avg time</th>
                    </tr>
                </thead>
                @foreach ($requestsByRoute as $v)
                    <tr>
                        <td>{{ $v->route }}</td>
                        <td>{{ $v->q }}</td>
                        <td>{{ $v->avg_time ? round($v->avg_time) : '?' }} ms</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-12 col-md-8 col-lg-9">
            <table class="table table-sm table-light table-striped">
                <thead class="table-dark">
                    <tr>
                        <th>User Agent</th>
                        <th>q/sec</th>
                    </tr>
                </thead>
                @foreach ($visitsByUa as $v)
                    <tr>
                        <td><a
                                href="{{ route('visits', ['userAgent' => $v['userAgent']]) }}">{{ $v['userAgent'] }}</a>
                        </td>
                        <td>{{ $v['requestsPerSec'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    @if (
        $failedCrons->filter(function ($crons) {
                return $crons->count() ? true : false;
            })->count())
        <table class="table table-sm table-light">
            <thead class="table-dark">
                <tr>
                    <th>Command</th>
                    <th>Active</th>
                    <th>Done</th>
                    <th>Last Time</th>
                </tr>
            </thead>
            @foreach ($failedCrons as $cron)
                <tr style="color: red;">
                    <td>{{ $cron->command }}</td>
                    <td>{{ $cron->active }} [<a href="#"
                            wire:click="makeCronAsDone({{ $cron->id }})">вернуть</a>]
                    </td>
                    <td>{{ $cron->done }}</td>
                    <td>{{ $cron->last_time }}</td>
                </tr>
            @endforeach
        </table>
    @endif
    <table class="table table-sm table-light" style="font-size: 11px;">
        <thead class="table-dark">
            <tr>
                <th>Date</th>
                <th>IP</th>
                <th>Text</th>
                <th>Query</th>
                <th>User Agent</th>
            </tr>
        </thead>
        @foreach ($logs as $log)
            <tr>
                <td>{{ $log->date }}</td>
                <td>{{ $log->visit?->ip }}</td>
                <td>{{ \Str::limit($log->text ?? '', 50) }}</td>
                <td>{{ \Str::limit($log->visit?->query ?? '', 50) }}</td>
                <td>{{ \Str::limit($log->visit?->userAgent ?? '', 50) }}</td>
            </tr>
        @endforeach
    </table>
    <div class="row">
        <div class="col-12 col-md-6">
            <table class="table table-sm table-light table-striped">
                <thead class="table-dark">
                    <tr>
                        <th>IP</th>
                        <th>q/sec</th>
                        <th>Geo</th>
                        <th>Ban</th>
                        <th>Hide</th>
                    </tr>
                </thead>
                @foreach ($visitsByIp as $v)
                    <tr>
                        <td><a href="{{ route('visits', ['ip' => $v['ip']]) }}">{{ $v['ip'] }}</a></td>
                        <td>{{ $v['requestsPerSec'] }}</td>
                        <td>{{ $v['geoIp']->getCountryNameRu() }} {{ $v['geoIp']->getCityNameRu() }}
                            {{ $v['geoIp']->getAsnOrganization() }}</td>
                        <td>{{ $v['ban'] ? $v['ban']->description : '' }}</td>
                        <td>{{ $v['ban'] ? ($v['ban']->hide_player ? 1 : 0) : '' }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-12 col-md-6">
            <table class="table table-sm table-light table-striped">
                <thead class="table-dark">
                    <tr>
                        <th>Domain</th>
                        <th>q/sec</th>
                    </tr>
                </thead>
                @foreach ($visitsByDomain as $item)
                    <tr>
                        <td>
                            <a href="{{ route('visits', ['host' => $item['host']]) }}">{{ $item['host'] }}</a>
                        </td>
                        <td>{{ $item['requestsPerSec'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        @if ($showCloudflareStat)
            <div class="col-12 col-md-6">
                <table class="table table-sm table-light table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Account</th>
                            <th>Total</th>
                            <th>Active</th>
                        </tr>
                    </thead>
                    @foreach ($this->cloudflareAccounts as $cloudflareAccount)
                        <tr>
                            <td>{{ $cloudflareAccount->email }}</td>
                            <td>{{ collect($cloudflareAccount->zones?->result)->count() }}</td>
                            <td>
                                {{-- {{ collect($cloudflareAccount->zones?->result)->where('status', 'active')->pluck('name')->implode(', ') }} --}}
                                {{ collect($cloudflareAccount->zones?->result)->where('status', 'active')->count() }}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
    </div>
</div>
