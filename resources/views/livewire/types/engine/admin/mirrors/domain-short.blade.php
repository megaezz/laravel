<div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-2">
    <div class="dropdown">
        <div class="btn-group">
            <button class="btn btn-light far fa-copy copy" data-clipboard-text="{{ $domain->domain }}">
            </button>
            <button class="btn btn-light" type="button" id="dropdown{{ $domain->domain }}" data-bs-toggle="dropdown" aria-haspopup="true">
                {{ $domain->domain_decoded }}
                @if ($domain->redirectTo)
                <span style="color: red; font-weight: bold;"><br>{{ $domain->redirectTo->domain_decoded }}</span>
                @endif
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdown{{ $domain->domain }}">
                <x-types.engine.admin.domain-menu :domain="$domain" />
            </div>
        </div>
    </div>
</div>