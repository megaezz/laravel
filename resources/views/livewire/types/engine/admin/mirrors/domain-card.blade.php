<div class="card mb-3">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-12 col-md-6 col-xl-4 px-3 py-2" style="white-space: nowrap;">
                        <div class="mb-2">Домен</div>
                        <x-types.engine.admin.mirrors.domain :$domain :metrika="true" primary-domain="true" :bold="true" />
                        <div class="col-12">
                            @if ($domain->favicon_src)
                            <img class="d-inline m-2" style="height: 20px; width: auto;" src="/storage/images/h40{{ $domain->favicon_src }}" alt="favicon" />
                            @endif
                            @if ($domain->logo_src and $domain->logo_src != $domain->favicon_src)
                            <img class="d-inline m-2" style="height: 20px; width: auto;" src="/storage/images/h40{{ $domain->logo_src }}" alt="logo" />
                            @endif
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-xl-4 px-3 py-2" style="overflow: hidden;">
                        @if ($domain->redirectTo or $domain->yandexRedirectTo)
                            <div>
                                <div class="mb-2">Редирект</div>
                                @if ($domain->redirectTo)
                                    <div style="white-space: nowrap;">
                                        {{-- показываем иконку гугла, только если есть отдельный домен для яндекса --}}
                                        @if ($domain->yandexRedirectTo)
                                            <i class="fa-brands fa-google"></i>
                                        @endif
                                        {{-- если есть домен для яндекса, то не отборажаем кнопку яндекс вебмастера --}}
                                        <x-types.engine.admin.mirrors.domain :domain="$domain->redirectTo" :yandex-webmaster="!$domain->yandexRedirectTo" :bold="true" />
                                    </div>
                                @endif
                                @if ($domain->yandexRedirectTo)
                                    <div style="white-space: nowrap;">
                                        <i class="fa-brands fa-yandex"></i>
                                        <x-types.engine.admin.mirrors.domain :domain="$domain->yandexRedirectTo" :yandex-webmaster="true" :bold="true" />
                                    </div>
                                @endif
                            </div>
                        @endif
                        @if ($domain->ru_domain)
                            <div>
                                <div class="mt-2 mb-2">RU домен</div>
                                <div style="white-space: nowrap;">
                                    <button class="btn btn-sm btn-light far fa-copy copy" data-clipboard-text="{{ $domain->ru_domain }}">
                                    </button>
                                    <a href="{{ route('domain.settings', ['domain' => $domain->ru_domain]) }}">{{ $domain->ru_domain }}</a>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-12 col-md-6 col-xl-4 px-3 py-2" style="overflow: hidden;">
                        @if ($domain->clientRedirectTo)
                            <div>
                                <div class="mb-2">Редирект&nbsp;клиента</div>
                                <div style="white-space: nowrap;">
                                    <x-types.engine.admin.mirrors.domain :domain="$domain->clientRedirectTo" :bold="true" />
                                </div>
                            </div>
                        @endif
                        @if ($domain->next_ru_domain)
                            <div>
                                <div class="mt-2 mb-2">Следующий RU домен</div>
                                <div style="white-space: nowrap;">
                                    <button class="btn btn-sm btn-light far fa-copy copy" data-clipboard-text="{{ $domain->next_ru_domain }}">
                                    </button>
                                    <a href="{{ route('domain.settings', ['domain' => $domain->next_ru_domain]) }}">{{ $domain->next_ru_domain }}</a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-xxl-3 px-3 py-2">
                        <button class="btn btn-link" type="button"
                            wire:click="$toggle('showMirrors')">Раскрыть зеркала <span
                                class="badge text-bg-light">{{ $domain->mirrors->count() }}</span></button>
                        @if ($domain->type == 'cinema')
                            <button class="btn btn-link" type="button" wire:click="$toggle('showGroups')">
                                Группы <span class="badge text-bg-light">{{ $domain->cinemaDomain->groups_count }}</span>
                            </button>
                            <button class="btn btn-link" type="button" wire:click="$toggle('showGenres')">
                                Тэги <span class="badge text-bg-light">{{ $domain->cinemaDomain->tags_count }}</span>
                            </button>
                        @endif
                        <button class="btn btn-link" type="button" wire:click="showMetrika">Метрика</button>
                        <button class="btn btn-link" type="button" wire:click="showWhois">Whois</button>
                    </div>
                    <div class="col-12 col-md-6 col-xxl-9 px-3 py-2">
                        <span class="badge text-bg-light" wire:click="setDomainFilter('type_template','{{ $domain->type_template }}')">{{ $domain->type_template }}</span>
                        @if ($domain->metrika?->visits)
                            <span class="badge text-bg-light">
                                Трафик {{ $domain->metrika?->visits }} виз./мес.
                            </span>
                        @endif
                        @if ($domain->metrika?->ru_percent)
                            <span class="badge text-bg-light">РФ {{ $domain->metrika->ru_percent }}%</span>
                        @endif
                        @if ($domain->metrika?->yandex_percent)
                            <span class="badge text-bg-light">Yandex {{ $domain->metrika->yandex_percent }}%</span>
                        @endif
                        @if ($domain->metrika?->google_percent)
                            <span class="badge text-bg-light">Google {{ $domain->metrika->google_percent }}%</span>
                        @endif
                        @if ($domain->metrika?->bing_percent)
                            <span class="badge text-bg-light">Bing {{ $domain->metrika->bing_percent }}%</span>
                        @endif
                        @if ($domain->metrika?->yahoo_percent)
                            <span class="badge text-bg-light">Yahoo {{ $domain->metrika->yahoo_percent }}%</span>
                        @endif
                        @if ($domain->metrika?->duckduckgo_percent)
                            <span class="badge text-bg-light">DuckDuckGo
                                {{ $domain->metrika->duckduckgo_percent }}%</span>
                        @endif
                        @if (!$domain->yandexAllowed)
                            <span class="badge text-bg-light">Закрыт от Яндекса</span>
                        @endif
                        <span class="badge text-bg-light">Схема доступа {{ $domain->access_scheme }}</span>
                        @if ($domain->ru_domain or $domain->next_ru_domain)
                            <span class="badge text-bg-light">Hreflang</span>
                        @endif
                        @if (!$domain->on)
                            <span class="badge text-bg-light">Отключен</span>
                        @endif
                        @if ($domain->yandex_token_email)
                            <span class="badge text-bg-light">Акк Яндекса {{ $domain->yandex_token_email }}</span>
                        @endif
                        @if ($domain->is_masking_domain)
                            <span class="badge text-bg-light">Маскировочный</span>
                        @endif
                        @if ($domain->every_actual_crawler_mirror_in_known_ru_block)
                            <span class="badge text-bg-light">Полностью заблочен в РФ</span>
                        @endif
                        @if ($domain->allow_not_valid_referers_for_clients)
                            <span class="badge text-bg-light">Разрешать не валидные рефереры для клиентов (v3)</span>
                        @endif
                        @if ($domain->has_folder)
                            <span class="badge text-bg-light">Шаблон изменен</span>
                        @endif
                        @if ($domain->cinemaDomain)
                            @if ($domain->cinemaDomain->allowRussianMovies == false)
                                <span class="badge text-bg-light">Без РФ фильмов</span>
                            @endif
                            @if ($domain->cinemaDomain->useAltDescriptions)
                                <span class="badge text-bg-light">Альт. описания</span>
                            @endif
                            @if ($domain->cinemaDomain->rknForRuOnly)
                                <span class="badge text-bg-light">Показывать заблоченный плеер для УКР</span>
                            @endif
                            @if ($domain->cinemaDomain->include_movies and !$domain->cinemaDomain->include_serials)
                                <span class="badge text-bg-light">Только фильмы</span>
                            @endif
                            @if (!$domain->cinemaDomain->include_movies && $domain->cinemaDomain->include_serials)
                                <span class="badge text-bg-light">Только сериалы</span>
                            @endif
                            @if ($domain->cinemaDomain->include_youtube)
                                <span class="badge text-bg-light">Ютуб</span>
                            @endif
                            @if ($domain->cinemaDomain->allowRiskyStudios === false)
                                <span class="badge text-bg-light">Без контента рискованных студий</span>
                            @endif
                            @if ($domain->cinemaDomain->moviesMinYear)
                                <span class="badge text-bg-light">Фильмы с {{ $domain->cinemaDomain->moviesMinYear }}
                                    г.</span>
                            @endif
                            @if ($domain->cinemaDomain->rkn_scheme == 'delete_player_only_if_main_domain')
                                <span class="badge text-bg-light">Удалять плеер только если жалоба на основу</span>
                            @endif
                            @if ($domain->cinemaDomain->rkn_scheme == 'delete_player_only_if_abuse_on_page')
                                <span class="badge text-bg-light">Удалять плеер только если жалоба на страницу</span>
                            @endif
                            @if ($domain->cinemaDomain->rkn_scheme == 'delete_player_only_if_ru_and_abuse_on_page')
                                <span class="badge text-bg-light">Удалять плеер только если жалоба на страницу
                                    (рф)</span>
                            @endif
                            @if ($domain->cinemaDomain->rkn_scheme == 'delete_player_when_ru_and_not_canonical')
                                <span class="badge text-bg-light">Скрывать плеер на неканонических страницах для
                                    рф</span>
                            @endif
                            @if ($domain->blocks->where('template', 'promo/movieads.ru/script.html')->where('active', true)->count())
                                <span class="badge text-bg-light">Movieads</span>
                            @endif
                            @if ($domain->blocks->where('template', 'promo/videoroll.net/script_vpaut.html')->where('active', true)->count())
                                <span class="badge text-bg-light">Videoroll</span>
                            @endif
                            @if ($domain->blocks->where('template', 'promo/hdvb/sticker.html')->where('active', true)->count())
                                <span class="badge text-bg-light">Hdvb Sticker</span>
                            @endif
                            <span class="badge text-bg-light">По {{ $domain->cinemaDomain->moviesPerDay }}
                                фильмов</span>
                            @if ($domain->cinemaDomain->ai_descriptions)
                                <span class="badge text-bg-light">AI описания</span>
                            @endif
                            @if (!$domain->cinemaDomain->index_search)
                                <span class="badge text-bg-light">Поиск закрыт в роботс</span>
                            @endif
                            @if (!$domain->cinemaDomain->index_movies_in_yandex)
                                <span class="badge text-bg-light">Фильмы закрыты для Яндекса</span>
                            @endif
                            @if ($domain->cinemaDomain->show_trailer_for_non_sng)
                                <span class="badge text-bg-light">Показывать трейлеры для не СНГ</span>
                            @endif
                            @if ($domain->cinemaDomain->hide_canonical)
                                <span class="badge text-bg-light">Скрывать каноникал</span>
                            @endif
                            {{-- <span class="badge text-bg-light">Заблокировано: {{ $domain->cinemaDomain->movies()->cacheFor(now()->addDays(1))->whereDeleted(true)->count() }}</span> --}}
                        @endif
                    </div>
                </div>
                @if ($showMirrors)
                    <div class="row">
                        @foreach ($domain->mirrors->sortBy('add_date') as $mirror)
                            <div class="col-12 col-md-4 px-3 py-2" style="white-space: nowrap;">
                                <x-types.engine.admin.mirrors.domain :domain="$mirror" />
                            </div>
                        @endforeach
                    </div>
                @endif
                @if ($showGroups)
                    @if ($domain->cinemaDomain->groups)
                        @foreach ($domain->cinemaDomain->groups as $group)
                            <div class="row">
                                <div class="col-1">
                                    {{ $group->group->id }}
                                </div>
                                <div class="col-2">
                                    {{ $group->group->name }}
                                </div>
                                <div class="col-5">
                                    {{ mb_strimwidth($group->text, 0, 30, '...') }}
                                </div>
                                <div class="col-1">
                                    {{ $group->dmca }}
                                </div>
                                <div class="col-2">
                                    <button class="btn btn-light btn-sm editGroup" type="button" wire:click="$toggle('showGroupEditor')">
                                        <i class="far fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="row">
                            <div class="col-12 px-3 py-2">
                                Групп нет
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <select class="form-select" wire:model.live="choosed_group">
                                    <option value="null">Не выбрано</option>
                                    @foreach ($groups as $group)
                                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                                    @endforeach
                                </select>
                                <button class="btn btn-light">Добавить</button>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-lg-3 pt-2">
                <div class="mb-2" style="word-wrap: break-word;">
                    @if ($domain->comment)
                        <i class="far fa-comment-alt"></i>
                    @endif
                    @if (!$showCommentEditor)
                        <span>{!! nl2br($domain->comment ?? '') !!}</span>
                    @endif
                    <button class="btn btn-light btn-sm editComment" type="button" wire:click="$toggle('showCommentEditor')">
                        <i class="far fa-edit"></i>
                    </button>
                </div>
                @if ($showCommentEditor)
                    <form wire:submit.prevent="editComment" class="text-end">
                        <div class="form-group">
                            <textarea name="comment" class="form-control mb-2" style="height: 100px;" wire:model="comment"></textarea>
                        </div>
                        <button class="btn btn-secondary">ОК</button>
                    </form>
                @endif
            </div>
        </div>
        <div class="row g-3">
            <div class="col-12 col-lg-6">
                @if ($domain->ru_blocked_current_mirrors->count())
                <div class="alert alert-warning mb-0">
                    <h4 class="alert-heading">Блокировки</h4>
                    @foreach($domain->actual_mirrors->where('monitoring_ru', false)->where('known_ru_block', false) as $mirror)
                    <div class="row mb-2">
                        <div class="col-6">
                            <x-types.engine.admin.mirrors.domain :domain="$mirror" />
                        </div>
                        @if($mirror->next_mirror)
                        @if($mirror->next_mirror_created)
                        <div class="col-6">
                            <div class="input-group">
                                <button class="form-control form-control-sm" wire:click="setMirror('{{ $mirror->next_mirror }}', '{{ $mirror->mirror_redirect_type }}')" wire:loading.attr="disabled">
                                    Настроить {{ $mirror->mirror_redirect_type_ru }} на {{ $mirror->next_mirror_decoded }}
                                </button>
                                <div class="input-group-text">
                                    <span class="btn btn-light copy">
                                        <button class="btn btn-sm far fa-copy copy" data-clipboard-text="{{ $mirror->next_mirror }}"></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="col-6">
                            <div class="input-group">
                                <button class="form-control form-control-sm" wire:click="createNextMirror('{{ $mirror->domain }}')" wire:loading.attr="disabled">
                                    Создать {{ $mirror->next_mirror_decoded }}
                                </button>
                            </div>
                        </div>
                        @endif
                        @else
                        <div class="col-6">
                            <span style="margin-right: 10px;">{{ $mirror->domain }} &mdash; ?</span>
                        </div>
                        @endif
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="col-12 col-lg-6">
                @if ($domain->errors->count() or $domain->mirrors->contains(function ($mirror) {
                    return $mirror->errors->count();
                }))
                <div class="alert alert-warning mb-0">
                    <h4 class="alert-heading">Найдены проблемы</h4>
                    <ul>
                        @foreach($domain->errors as $error)
                        <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                    @foreach($domain->mirrors as $mirror)
                    @if ($mirror->errors->count())
                    <div class="row mb-2">
                        <div class="col-6">
                            <x-types.engine.admin.mirrors.domain :domain="$mirror" />
                        </div>
                        <div class="col-6">
                            <ul>
                                @foreach($mirror->errors as $error)
                                <li>{!! $error !!}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
