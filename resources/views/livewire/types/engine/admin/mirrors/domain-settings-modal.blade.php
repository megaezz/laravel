<div>
    {{-- обязательно wire:ignore.self иначе шляпа полная --}}
    <div id="domainSettings" wire:ignore.self class="modal" tabindex="-1">
        <div class="modal-dialog modal-xl">
            <div class="modal-content"
                style="background-image: url('/types/cinema/template/admin/customer/images/extra_clean_paper.png')">
                <div class="modal-header">
                    @if ($domain)
                        <h5 class="modal-title">
                            Настройки домена <a
                                href="{{ route('domain.menu', ['domain' => $domain->domain]) }}">{{ $domain->domain }}</a>
                            @if ($domain->mirror_of)
                                <i class="fas fa-cog" wire:click="setDomain('{{ $domain->mirror_of }}')"></i>
                            @endif
                        </h5>
                    @endif
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div wire:loading>
                        <x-placeholders.dots />
                    </div>
                    @if ($domain)
                        @if ($contentType == 'rkn')
                            <table class="table">
                                <tr>
                                    <th>#</th>
                                    <th>Дата</th>
                                    <th>Решение</th>
                                    <th>Организация</th>
                                    <th>Домен</th>
                                    <th>Страница</th>
                                    <th>IP</th>
                                </tr>
                                @foreach ($domain->rkns->merge($domain->parent_domain_or_self->mask_rkns)->sortByDesc('id') as $rkn)
                                <tr @class(['table-success' => $rkn->deleted_at])>
                                    <td>{{ $rkn->data->id }}</td>
                                    <td class="text-nowrap">{{ $rkn->data->applyDate }}</td>
                                    <td>{{ $rkn->data->decisionDate }} {{ $rkn->data->decisionNumber }}</td>
                                    <td>{{ $rkn->data->authority->name }}</td>
                                    <td>{{ collect($rkn->data->domains)->implode(', ') }}</td>
                                    <td class="text-break">{{ collect($rkn->data->urls)->implode(', ') }}</td>
                                    <td>{{ collect($rkn->data->ips)->implode(', ') }}</td>
                                </tr>
                                @endforeach
                            </table>
                        @else
                            <livewire:types.engine.admin.domain-settings :$domain :key="$domain->domain" :is-modal="true" />
                        @endif
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                    <button class="btn btn-primary"
                        wire:click="$dispatchTo('types.engine.admin.domain-settings', 'update-domain')">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

    @script
        <script>
            // при закрытии модального окна, обнуляем домен, чтобы вложенный компонент domain-settings убрался со страницы,
            // это нужно, чтобы каждый раз при открытии модального окна, генерировался свежий компонент а не отображался старый
            // типа вдруг там настроек понакрутил каких-то, каких не надо, а потом сохраню
            document.getElementById('domainSettings').addEventListener('hidden.bs.modal', function() {
                $wire.dispatch('set-domain', {
                    domain: null
                });
            });
        </script>
    @endscript
</div>
