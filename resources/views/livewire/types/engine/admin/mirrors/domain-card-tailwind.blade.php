<div class="card mb-3">
    <div class="card-body">
        <div class="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-4">
            <div class="px-3 py-2 whitespace-nowrap">
                <div class="mb-2">Домен</div>
                <x-types.engine.admin.mirrors.domain :$domain :metrika="true" primary-domain="true" :bold="true" />
                <div class="col-12">
                    @if ($domain->favicon_src)
                    <img class="inline-block m-2" style="height: 20px; width: auto;" src="/storage/images/h40{{ $domain->favicon_src }}" alt="favicon" />
                    @endif
                    @if ($domain->logo_src and $domain->logo_src != $domain->favicon_src)
                    <img class="inline-block m-2" style="height: 20px; width: auto;" src="/storage/images/h40{{ $domain->logo_src }}" alt="logo" />
                    @endif
                </div>
            </div>
            <div class="px-3 py-2 overflow-hidden">
                @if ($domain->redirectTo or $domain->yandexRedirectTo)
                    <div>
                        <div class="mb-2">Редирект</div>
                        @if ($domain->redirectTo)
                            <div class="whitespace-nowrap">
                                @if ($domain->yandexRedirectTo)
                                    <i class="fa-brands fa-google"></i>
                                @endif
                                <x-types.engine.admin.mirrors.domain :domain="$domain->redirectTo" :yandex-webmaster="!$domain->yandexRedirectTo" :bold="true" />
                            </div>
                        @endif
                        @if ($domain->yandexRedirectTo)
                            <div class="whitespace-nowrap">
                                <i class="fa-brands fa-yandex"></i>
                                <x-types.engine.admin.mirrors.domain :domain="$domain->yandexRedirectTo" :yandex-webmaster="true" :bold="true" />
                            </div>
                        @endif
                    </div>
                @endif
                @if ($domain->ru_domain)
                    <div>
                        <div class="mt-2 mb-2">RU домен</div>
                        <div class="whitespace-nowrap">
                            <button class="btn btn-sm btn-light far fa-copy copy" data-clipboard-text="{{ $domain->ru_domain }}">
                            </button>
                            <a href="{{ route('domain.settings', ['domain' => $domain->ru_domain]) }}">{{ $domain->ru_domain }}</a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="px-3 py-2 overflow-hidden">
                @if ($domain->clientRedirectTo)
                    <div>
                        <div class="mb-2">Редирект клиента</div>
                        <div class="whitespace-nowrap">
                            <x-types.engine.admin.mirrors.domain :domain="$domain->clientRedirectTo" :bold="true" />
                        </div>
                    </div>
                @endif
                @if ($domain->next_ru_domain)
                    <div>
                        <div class="mt-2 mb-2">Следующий RU домен</div>
                        <div class="whitespace-nowrap">
                            <button class="btn btn-sm btn-light far fa-copy copy" data-clipboard-text="{{ $domain->next_ru_domain }}">
                            </button>
                            <a href="{{ route('domain.settings', ['domain' => $domain->next_ru_domain]) }}">{{ $domain->next_ru_domain }}</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4">
            <div class="px-3 py-2">
                <button class="btn btn-link" type="button" wire:click="$toggle('showMirrors')">Раскрыть зеркала <span class="badge bg-light">{{ $domain->mirrors->count() }}</span></button>
                @if ($domain->type == 'cinema')
                    <button class="btn btn-link" type="button" wire:click="$toggle('showGroups')">
                        Группы <span class="badge bg-light">{{ $domain->cinemaDomain->groups_count }}</span>
                    </button>
                    <button class="btn btn-link" type="button" wire:click="$toggle('showGenres')">
                        Тэги <span class="badge bg-light">{{ $domain->cinemaDomain->tags_count }}</span>
                    </button>
                @endif
                <button class="btn btn-link" type="button" wire:click="showMetrika">Метрика</button>
                <button class="btn btn-link" type="button" wire:click="showWhois">Whois</button>
            </div>
            <div class="px-3 py-2">
                <span class="badge bg-light" wire:click="setDomainFilter('type_template','{{ $domain->type_template }}')">{{ $domain->type_template }}</span>
                @if ($domain->metrika?->visits)
                    <span class="badge bg-light">
                        Трафик {{ $domain->metrika?->visits }} виз./мес.
                    </span>
                @endif
                @if ($domain->metrika?->ru_percent)
                    <span class="badge bg-light">РФ {{ $domain->metrika->ru_percent }}%</span>
                @endif
                @if ($domain->metrika?->yandex_percent)
                    <span class="badge bg-light">Yandex {{ $domain->metrika->yandex_percent }}%</span>
                @endif
                @if ($domain->metrika?->google_percent)
                    <span class="badge bg-light">Google {{ $domain->metrika->google_percent }}%</span>
                @endif
                @if ($domain->metrika?->bing_percent)
                    <span class="badge bg-light">Bing {{ $domain->metrika->bing_percent }}%</span>
                @endif
                @if ($domain->metrika?->yahoo_percent)
                    <span class="badge bg-light">Yahoo {{ $domain->metrika->yahoo_percent }}%</span>
                @endif
                @if ($domain->metrika?->duckduckgo_percent)
                    <span class="badge bg-light">DuckDuckGo {{ $domain->metrika->duckduckgo_percent }}%</span>
                @endif
                @if (!$domain->yandexAllowed)
                    <span class="badge bg-light">Закрыт от Яндекса</span>
                @endif
                <span class="badge bg-light">Схема доступа {{ $domain->access_scheme }}</span>
                @if ($domain->ru_domain or $domain->next_ru_domain)
                    <span class="badge bg-light">Hreflang</span>
                @endif
                @if (!$domain->on)
                    <span class="badge bg-light">Отключен</span>
                @endif
                @if ($domain->yandex_token_email)
                    <span class="badge bg-light">Акк Яндекса {{ $domain->yandex_token_email }}</span>
                @endif
                @if ($domain->is_masking_domain)
                    <span class="badge bg-light">Маскировочный</span>
                @endif
                @if ($domain->every_actual_crawler_mirror_in_known_ru_block)
                    <span class="badge bg-light">Полностью заблочен в РФ</span>
                @endif
                @if ($domain->allow_not_valid_referers_for_clients)
                    <span class="badge bg-light">Разрешать не валидные рефереры для клиентов (v3)</span>
                @endif
                @if ($domain->has_folder)
                    <span class="badge bg-light">Шаблон изменен</span>
                @endif
                @if ($domain->cinemaDomain)
                    <span class="badge bg-light">По {{ $domain->cinemaDomain->moviesPerDay }} фильмов</span>
                @endif
            </div>
        </div>
        @if ($showMirrors)
            <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
                @foreach ($domain->mirrors->sortBy('add_date') as $mirror)
                    <div class="px-3 py-2 whitespace-nowrap">
                        <x-types.engine.admin.mirrors.domain :domain="$mirror" />
                    </div>
                @endforeach
            </div>
        @endif
        @if ($showGroups)
            <div class="grid gap-4">
                @if ($domain->cinemaDomain->groups)
                    @foreach ($domain->cinemaDomain->groups as $group)
                        <div class="grid grid-cols-12 gap-2 py-2">
                            <div class="col-span-1">{{ $group->group->id }}</div>
                            <div class="col-span-2">{{ $group->group->name }}</div>
                            <div class="col-span-5">{{ mb_strimwidth($group->text, 0, 30, '...') }}</div>
                            <div class="col-span-1">{{ $group->dmca }}</div>
                            <div class="col-span-2">
                                <button class="btn btn-light btn-sm editGroup" type="button" wire:click="$toggle('showGroupEditor')">
                                    <i class="fa-solid fa-pen"></i>
                                </button>
                            </div>
                        </div>
                    @endforeach
                @else
                    <span class="badge bg-light">Нет групп</span>
                @endif
            </div>
        @endif
    </div>
</div>
