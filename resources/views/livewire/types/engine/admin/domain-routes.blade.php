<div class="container">
    <div class="row">
        <div class="col-3">
            Добавить роут
            <form wire:submit="createRoute">
                <div class="row mb-4">
                    <div class="col-md-8">
                        <input class="form-control" type="text" wire:model="newRoutePattern" placeholder="Паттерн">
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-primary" type="submit">Добавить</button>
                    </div>
                </div>
            </form>
            <ul class="nav flex-column" style="overflow: hidden;">
                @foreach ($domain->routes as $r)
                <li class="nav-item mb-2">
                    <button class="btn btn-light {{ ($r->id == $route?->id) ? 'active' : '' }}" wire:click="$set('selectedRouteId', '{{ $r->id }}')">
                        {{ $r->pattern }}
                    </button>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="col-9">
            <div class="row mb-4">
                <div class="col-md-6">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('domain.menu', ['domain' => $domain->domain]) }}">{{ $domain->domain_decoded }}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Страницы
                            </li>
                        </ol>
                    </nav>
                </div>
                <div class="col-md-5">
                    <label>Скопировать настройки из</label>
                    <select class="form-control" wire:model="copyFromDomain">
                        <option selected>Выберите домен</option>
                        @foreach ($this->copy_from_domains as $d)
                        <option value="{{ $d->domain }}">{{ $d->domain }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-1">
                    <button class="btn btn-primary mt-4" wire:click="copyRoutes">ОК</button>
                </div>
            </div>
            @if ($route)
            @if (session()->has('message'))
            <div class="alert alert-success">{{ session('message') }}</div>
            @endif
            <form wire:submit="saveRoute">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Pattern</label>
                            <input class="form-control" type="text" wire:model="route.pattern" value="{{ $route->pattern }}">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label>Alias</label>
                            <input class="form-control" type="text" wire:model="route.alias" value="{{ $route->alias }}">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label>Redirect</label>
                            {{-- TODO --}}
                            <select class="form-control" wire:model="route.redirectToAlias">
                                @foreach([0 => 'Нет', 1 => 'Да'] as $value => $name)
                                <option value="{{ $value }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label>HTTP Methods</label>
                            {{-- TODO --}}
                            <select class="form-control" wire:model="route.http_methods">
                                @foreach(['any','get','post','put','delete','patch'] as $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" type="text" wire:model="route.name" value="{{ $route->name }}">
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label>Controller</label>
                            <input class="form-control" type="text" wire:model="route.controller" value="{{$route->controller}}">
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label>Method</label>
                            <input class="form-control" type="text" wire:model="route.method" value="{{$route->method}}">
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label>Template</label>
                            <input class="form-control" type="text" wire:model="route.template" value="{{$route->template}}">
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Var 1</label>
                                    <input class="form-control" type="text" wire:model="route.var1" value="{{$route->var1}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Var 2</label>
                                    <input class="form-control" type="text" wire:model="route.var2" value="{{$route->var2}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Var 3</label>
                                    <input class="form-control" type="text" wire:model="route.var3" value="{{$route->var3}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Var 4</label>
                                    <input class="form-control" type="text" wire:model="route.var4" value="{{$route->var4}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Var 5</label>
                                    <input class="form-control" type="text" wire:model="route.var5" value="{{$route->var5}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Vars</label>
                            <input class="form-control" type="text" wire:model="route.vars" value="{{$route->vars}}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Comment (комментарий для этого паттерна, нигде не отображается)</label>
                            <textarea class="form-control" type="text" wire:model="route.comment">{{$route->comment}}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Title</label>
                            <textarea class="form-control" wire:model="route.title">{{$route->title}}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" wire:model="route.description">{{$route->description}}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Keywords</label>
                            <input class="form-control" type="text" wire:model="route.keywords" value="{{$route->keywords}}">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label>H1</label>
                            <textarea class="form-control" wire:model="route.h1">{{$route->h1}}</textarea>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label>H2</label>
                            <textarea class="form-control" wire:model="route.h2">{{$route->h2}}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>BreadCrumbs</label>
                            <textarea class="form-control" wire:model="route.bread">{{$route->bread}}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Top Text</label>
                            <textarea class="form-control" wire:model="route.topText">{{$route->topText}}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Bottom Text</label>
                            <textarea class="form-control" wire:model="route.bottomText">{{$route->bottomText}}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Text 1</label>
                            <textarea class="form-control" wire:model="route.text1">{{$route->text1}}</textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Text 2</label>
                            <textarea class="form-control" wire:model="route.text2">{{$route->text2}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <button class="btn btn-primary" type="submit">Сохранить</button>
                    </div>
                    <div class="col-md-6 mt-2 text-end">
                        <button class="btn btn-warning" type="button" wire:click="deleteRoute" wire:confirm="Точно удалить роут {{ $route->pattern }}?">Удалить</button>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>