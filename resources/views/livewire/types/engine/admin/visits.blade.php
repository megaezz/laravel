<div class="container" {{ $poll ? 'wire:poll' : ''}}>
    <div class="mb-3">
        {{-- здесь отображаем фильтры для которых нет кнопок, для наглядности --}}
        @foreach ([
            'ip',
            'host',
            'userAgent',
            'ipRegexp',
            'userAgentRegexp',
            ] as $filter)
        @if ($this->{$filter})
        <span class="badge text-bg-secondary">
            {{ $filter }}: {{ $this->{$filter} }} <i class="fa-regular fa-circle-xmark" wire:click="$set('{{ $filter }}', null)"></i>
        </span>
        @endif
        @endforeach

        @if ($this->ban)
        <span class="badge text-bg-secondary">
            ban: #{{ $this->ban->id }} <i class="fa-regular fa-circle-xmark" wire:click="$set('ban', null)"></i>
        </span>
        <livewire:types.engine.admin.ban :ban="$this->ban" />
        @endif

        @if ($message)
        <div class="alert alert-primary">
            {{ $message }}
        </div>
        @endif
    </div>
    @if($this->ip)
    <div class="card border-light mb-3">
        <div class="card-body">
            <b>Provider:</b> {{ $this->geoIp?->getAsnOrganization() }},
            <b>Country:</b> {{ $this->geoIp?->getCountryName() }} {{ $this->geoIp?->getCityName() }},
            <b>Hostname:</b> {{ \engine\app\models\F::getHostnameCached($this->ip) }}
            @if ($this->ipBan)
            <form wire:submit="deleteBan">
                <span>
                    Бан: {{ $this->ipBan->ip }}<br>
                    [active={{ $this->ipBan->active ? 1 : 0 }}] [player={{ $this->ipBan->hide_player ? 1 : 0 }}] {{ $this->ipBan->description }}
                </span>
                <input class="btn btn-sm btn-primary" type="submit" value="Удалить">
            </form>
            @else
            <button class="btn btn-sm btn-light {{ $showBanForm ? 'active' : '' }}" wire:click="$toggle('showBanForm', true)">Забанить IP</button>
            @if($showBanForm)
            <form wire:submit="createBan" class="mt-3">
                <div class="row">
                    <div class="col-md-2">
                        <input type="text" wire:model="createBanForm.ip" class="form-control" placeholder="Правило">
                    </div>
                    <div class="col-md-2">
                        <input type="text" wire:model="createBanForm.description" class="form-control" placeholder="Причина">
                    </div>
                    <div class="col-md-2">
                        <input class="btn btn-primary" type="submit" value="Заблокировать">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="checkbox-inline mr-2" for="hide_player">
                            <input type="checkbox" id="hide_player" wire:model="createBanForm.hide_player">
                            Скрыть плеер
                        </label>
                    </div>
                    <div class="col-md-2">
                        <label class="checkbox-inline" for="active">
                            <input type="checkbox" id="active" wire:model="createBanForm.active">
                            Забанить
                        </label>
                    </div>
                </div>
            </form>
            @endif
            @endif
        </div>
    </div>
    @endif

    <div class="row mb-3">
        <div class="col-12">
            <button wire:click="$toggle('poll')" class="btn btn-light">
                &nbsp;
                @if ($poll)
                <i class="fa-solid fa-pause"></i>
                @else
                <i class="fa-solid fa-play"></i>
                @endif
                &nbsp;
            </button>
            <div class="btn-group mx-2">
                <button wire:click="$set('data', 'visits')" class="btn btn-light {{ ($data == 'visits') ? 'active' : '' }}">Визиты</button>
                <button wire:click="$set('data', 'logs')" class="btn btn-light {{ ($data == 'logs') ? 'active' : '' }}" >Логи</button>
                <a class="btn btn-light" href="{{ route('bans') }}">Баны</a>
            </div>
            <div class="dropdown d-inline mx-2">
                <button class="btn btn-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                    {{ $days ? "Дней: {$days}" : 'Дней' }}
                </button>

                <ul class="dropdown-menu">
                    <li wire:click="$set('days', 0)" class="dropdown-item {{ ($days == 0) ? 'active' : '' }}">За все время</li>
                    @foreach ([1,2,3,4,5,6,7] as $itemDays)
                    <li wire:click="$set('days', {{ $itemDays }})" class="dropdown-item {{ ($days == $itemDays) ? 'active' : '' }}">{{ $itemDays }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="dropdown d-inline mx-2">
                <button class="btn btn-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                    Лимит: {{ $limit }}
                </button>

                <ul class="dropdown-menu">
                    @foreach ([100,200,500,1000,1500,2000,3000] as $itemLimit)
                    <li wire:click="$set('limit', {{ $itemLimit }})" class="dropdown-item {{ ($limit == $itemLimit) ? 'active' : '' }}">{{ $itemLimit }}</li>
                    @endforeach
                </ul>
            </div>
            <button wire:click="$toggle('calcQueryWasAbusedByServices')" class="btn btn-light mx-2 {{ $calcQueryWasAbusedByServices ? 'active' : '' }}">Абузы</button>
            <button wire:click="$toggle('filterOnlyRealBotIp')" class="btn btn-light mx-2 {{ $filterOnlyRealBotIp ? 'active' : '' }}">Реал. боты</button>
            <button wire:click="$toggle('filterNotRenewed')" class="btn btn-light mx-2 {{ $filterNotRenewed ? 'active' : '' }}">Непродл.</button>
            <button wire:click="$toggle('filterHasBan')" class="btn btn-light mx-2 {{ $filterHasBan ? 'active' : '' }}">Баны</button>
            @if ($data == 'logs')
            <div class="dropdown d-inline mx-2">
                <button class="btn btn-light dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                    Логи: {{ $filterLogText ?? ($filterErrors ? 'Исключения (без мусора)' : 'Все') }}
                </button>
                <ul class="dropdown-menu">
                    <li wire:click="$set('filterErrors', null)" class="dropdown-item {{ ($filterLogText or $filterErrors) ? '' : 'active' }}">
                        Все
                    </li>
                    <li wire:click="$set('filterErrors', true)" class="dropdown-item {{ $filterErrors ? 'active' : '' }}">
                        Исключения (без мусора)
                    </li>
                    @foreach ($logTexts as $logTextName => $logText)
                    {{-- используем {{ json_encode($logTextName) }} вместо '{{ $logTextName }}', т.к. "doesn't" вызывало ошибку js --}}
                    <li wire:click="$set('filterLogText', {{ json_encode($logTextName) }})" class="dropdown-item {{ ($filterLogText == $logTextName) ? 'active' : '' }}">
                        {{ $logTextName }}
                    </li>
                    @endforeach

                </ul>
            </div>
            @endif
            <input type="text" wire:model.live="host" class="form-control d-inline mx-2" placeholder="Host" style="max-width: 100px;">
        </div>
    </div>

    <div wire:loading>
        <x-placeholders.dots />
    </div>

    @if ($data == 'visits')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Визиты</li>
        </ol>
    </nav>
    <table class="table table-sm table-bordered visits">
        <thead>
            <tr>
                <th>Date</th>
                <th>Query</th>
                <th>User Agent</th>
                <th>Referer</th>
                <th>IP</th>
                <th>Time</th>
            </tr>
        </thead>
        @foreach($this->visits as $visit)
        <tr>
            <td class="date">{{ $visit->date }}</td>
            <td class="query">
                {{ urldecode($visit->query) }}
                @if ($calcQueryWasAbusedByServices)
                @if ($visit->queryWasAbusedByServices)
                <span class="service">
                    {{ $visit->queryWasAbusedByServices->pluck('service')->implode(', ') }}
                </span>
                @endif
                @endif
            </td>
            <td class="user-agent">{{ $visit->userAgent }}</td>
            <td class="referer">{{ $visit->referer }}</td>
            <td class="ip">
                @if ($this->ip)
                <span style="{{ $visit->ipRelation?->is_real_bot ? 'font-weight: bold; color: green;' : '' }}">
                    {{ $visit->ip }}
                </span>
                @else
                <a href="{{ route('visits', ['ip' => $visit->ip]) }}" style="{{ $visit->ipRelation?->is_real_bot ? 'font-weight: bold; color: green;' : '' }}">
                    {{ $visit->ip }}
                </a>
                @endif
                @if ($filterHasBan)
                <div class="service">
                    @foreach ($visit->bans as $ban)
                    <a href="{{ route('visits', ['ban' => $ban]) }}">
                        #{{ $ban->id }}
                    </a>
                    @endforeach
                </div>
                @endif
            </td>
            <td class="time">{{ $visit->time }}</td>
        </tr>
        @endforeach
    </table>
    @endif

    @if ($data == 'logs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Логи</li>
        </ol>
    </nav>
    <table class="table table-sm table-bordered logs">
        <thead>
            <tr>
                <th>Date</th>
                <th>Text</th>
                <th>Backtrace</th>
                <th>Type</th>
                <th>Query</th>
                <th>User Agent</th>
                <th>Referer</th>
                <th>IP</th>
            </tr>
        </thead>
        @foreach($this->logs as $log)
        <tr>
            <td class="date">{{ $log->date }}</td>
            <td class="text">{{ $log->text }}</td>
            <td class="backtrace">{{ $log->backtrace }}</td>
            <td class="type">{{ $log->type }}</td>
            <td class="query">
                {{ $log->visit?->query ? urldecode($log->visit->query) : '' }}
                @if ($calcQueryWasAbusedByServices)
                @if ($log->visit?->queryWasAbusedByServices)
                <span class="service">
                    {{ $log->visit?->queryWasAbusedByServices->pluck('service')->implode(', ') }}
                </span>
                @endif
                @endif
            </td>
            <td class="user-agent">{{ $log->visit?->userAgent }}</td>
            <td class="referer">{{ $log->visit?->referer }}</td>
            <td class="ip">
                @if ($this->ip)
                <span style="{{ $log->visit?->ipRelation?->is_real_bot ? 'font-weight: bold; color: green;' : '' }}">
                    {{ $log->visit?->ip }}
                </span>
                @else
                <a href="{{ route('visits', ['ip' => $log->visit?->ip]) }}" style="{{ $log->visit?->ipRelation?->is_real_bot ? 'font-weight: bold; color: green;' : '' }}">
                    {{ $log->visit?->ip }}
                </a>
                @endif
            </td>
        </tr>
        @endforeach
    </table>
    @endif

    <ul class="pagination">
        {{ $this->visits->count() ? $this->visits->links() : null }}
    </ul>
</div>
