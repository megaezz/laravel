<div class="container">
	<div class="row">
        <x-types.engine.admin.alert type="message" :message="session('message')" />
		<div class="col-12">
			@error('domain')
			<div>{{ $message }}</div>
			@enderror
		</div>
        @if (!$isModal)
		<div class="col-12 col-md-8 mt-2 mb-2">
			<h3>
				Настройки домена <a href="{{ route('domain.menu', [ 'domain' => $domain->domain ]) }}">{{ $domain->domain }}</a>
				@if ($domain->mirror_of)
				<a href="{{ route('domain.settings',[ 'domain' => $domain->mirror_of ]) }}">
					<i class="fas fa-cog"></i>
				</a>
				@endif
				{{-- нужно для каждого домена сделать, а не один  --}}
				{{-- @if ($domain->parentDomain)
				<a href="{{ route('domain.settings',[ 'domain' => $domain->parentDomain->domain ]) }}">
					<i class="fa-solid fa-house"></i>
				</a>
				@endif --}}
			</h3>
		</div>
        @endif
		@if (!$domain->is_mirror)
		<div class="col-12 col-md-4 mt-2 mb-2">
			<label>Скопировать настройки:</label>
			<select class="form-select" wire:model.live="copy_domain">
				<option value="">Не выбрано</option>
				@foreach ($this->copy_domains as $copy_domain)
				<option value="{{ $copy_domain->domain }}">{{ $copy_domain->domain }}</option>
				@endforeach
			</select>
		</div>
		@endif
	</div>
	<div class="row">
		@foreach ($domain->toArray() as $property => $value)
		@if (!isset($this->properties[$property]))
		<div class="col-md-2 mb-2">
			<div class="form-group">
				<label>{{ $property }}</label>
			</div>
		</div>
		@endif
		@endforeach
		@foreach ($this->properties as $property_name => $property)
		@if ($property['type'] == 'select')
		<div class="col-md-3 mb-2">
			<div class="form-group">
				<label>{{ $property['name'] }}</label>
				<select class="form-select" wire:model.live="domain.{{ $property_name }}">
					<option value="">Не выбрано</option>
					@foreach ($property['options'] as $option)
					<option value="{{ $option[$property['option_value'] ?? $property['option_name']] }}">{{ $option[$property['option_name']] }}</option>
					@endforeach
				</select>
			</div>
		</div>
		@endif
		@if ($property['type'] == 'boolean')
		<div class="col-md-3 mb-2">
			<label>&nbsp;</label>
			<div class="form-check form-switch">
				<input class="form-check-input" type="checkbox" id="{{ $property_name }}" wire:model.live="domain.{{ $property_name }}">
				<label class="form-check-label" for="{{ $property_name }}">
					{{ $property['name'] }}
				</label>
			</div>
		</div>
		@endif
		@if ($property['type'] == 'int' or $property['type'] == 'text')
		<div class="col-md-3 mb-2">
			<div class="form-group">
				<label>{{ $property['name'] }}</label>
				<input class="form-control" type="text" wire:model.live="domain.{{ $property_name }}">
			</div>
		</div>
		@endif
		@if ($property['type'] == 'textarea')
		<div class="col-md-6 mb-2">
			<div class="form-group">
				<label>{{ $property['name'] }}</label>
				<textarea class="form-control" wire:model.live="domain.{{ $property_name }}" rows="4"></textarea>
			</div>
		</div>
		@endif
		@endforeach
		<div class="col-md-3 mb-2">
			<div class="form-group">
				<label>Metrika ID</label>
				<input class="form-control" type="text" wire:model.live="metrika_id">
			</div>
		</div>
        @if (!$isModal)
		<div class="col-12">
			<button class="btn btn-primary" wire:click="updateDomain">Сохранить</button>
		</div>
        @endif
	</div>

	<div class="row mt-5">

		@can('everything')
        <div class="col-12 mb-3">
            <button class="btn btn-outline-secondary dropdown-toggle" wire:click="$toggle('showDnsSettings')">Настройки DNS</button>
        </div>
        @if ($showDnsSettings)
		<div class="col-md-4 mb-3">
			@if (!$domain->is_mirror)
			<label>
				Создать зеркало <a href="{{ route('domain.settings',[ 'domain' => $mirror ]) }}">{{ $mirror }}</a>
			</label>
			<div class="input-group mb-2">
				<input class="form-control" type="text" wire:model.live="mirror">
				{{-- Здесь есть косяк в поведении wire:confirm, если хотя бы один раз был найден $this->existedMirror, то wire:confirm сохраняет свое значение на этом этапе и постоянно выдает предупреждение, хотя там уже другой домен --}}
				@if ($this->existedMirror and $this->existedMirror->type == 'trash')
				<button class="btn btn-secondary" wire:click="createMirror" wire:loading.attr="disabled" wire:confirm="Зеркало {{ $this->existedMirror->domain }} уже существует и имеет тип trash, можно продолжить. Создаем из него зеркало?">Создать</button>
				@else
				<button class="btn btn-secondary" wire:click="createMirror" wire:loading.attr="disabled">Создать</button>
				@endif
			</div>
			@endif

			@if ($domain->is_parent_domain)
			<button class="btn btn-sm btn-secondary mb-2" wire:confirm="Точно очистить днс записи?" wire:click="clearDomainDnsRecords" wire:loading.attr="disabled">
				Очистить записи домена
			</button>
			<button class="btn btn-sm btn-success mb-2" wire:click="createZoneAndSetDefaultSettings" wire:loading.attr="disabled">
				Создать и настроить
			</button>
			@if($domain->dnsProvider?->service == 'cloudflare')
			<button class="btn btn-sm btn-secondary mb-2" wire:click="activationCheck" wire:loading.attr="disabled">
				Пингануть активацию
			</button>
			@endif
			@if ($this->domain->dnsProvider)
			<button class="btn btn-sm btn-secondary mb-2" wire:click="saveRecordsFromProviderToDatabase" wire:loading.attr="disabled">
                <i class="fa-brands fa-cloudflare"></i>
                <i class="fa-solid fa-arrow-right"></i>
                <i class="fa-solid fa-database"></i>
				{{-- Сохранить записи из {{ $this->domain->dnsProvider->service }} {{ $this->domain->dnsProvider->email }} в базу --}}
			</button>
			<button class="btn btn-sm btn-secondary mb-2" wire:click="saveRecordsFromDatabaseToProvider" wire:loading.attr="disabled">
                <i class="fa-solid fa-database"></i>
                <i class="fa-solid fa-arrow-right"></i>
                <i class="fa-brands fa-cloudflare"></i>
				{{-- Сохранить записи из базы в {{ $this->domain->dnsProvider->service }} {{ $this->domain->dnsProvider->email }} --}}
			</button>
			<button class="btn btn-sm btn-danger mb-2" wire:confirm="Точно удалить зону из {{ $this->domain->dnsProvider->service }} {{ $this->domain->dnsProvider->email }}?" wire:click="deleteZone" wire:loading.attr="disabled">
				Удалить зону из {{ $this->domain->dnsProvider->service }} {{ $this->domain->dnsProvider->email }}
			</button>
			<button class="btn btn-sm btn-secondary mb-2" wire:confirm="Точно установить CNAME запись Google на {{ $domain->domain }}?" wire:click="addGoogleDns" wire:loading.attr="disabled">
				Создать CNAME google
			</button>
			<button class="btn btn-sm btn-secondary mb-2" wire:confirm="Точно установить CNAME запись Mosopen на {{ $domain->domain }}?" wire:click="addMosopenDns" wire:loading.attr="disabled">
				Создать CNAME Mosopen
			</button>
			<button class="btn btn-sm btn-secondary mb-2" wire:confirm="Точно удалить CNAME запись на домене {{ $domain->domain }}?" wire:click="deleteCname" wire:loading.attr="disabled">
				Убрать CNAME
			</button>
			@endif
			@endif
			{{-- Создать CNAME отображаем для всех доменов --}}
			<button class="btn btn-sm btn-secondary mb-2" wire:confirm="Точно установить CNAME запись VPS на {{ $domain->domain }}?" wire:click="addVpsDns" wire:loading.attr="disabled">
				Создать CNAME vps
			</button>
		</div>

		@if ($domain->is_parent_domain)
		<div class="col-md-4 mb-3">
			<div class="row mb-2">
				<div class="col-12">
					<label>
						Создать поддомен
						<a href="{{ route('domain.settings',[ 'domain' => $subdomain.'.'.$domain->domain ]) }}">
							{{ $subdomain ? ($subdomain.'.'.$domain->domain) : '' }}
						</a>
					</label>
					<div class="input-group mb-2">
						<input class="form-control" type="text" wire:model.live="subdomain">
						<select class="form-select" wire:model.live="subdomain_type">
							<option value="">Не выбрано</option>
							@foreach ($this->types as $type)
							<option value="{{ $type->type }}">{{ $type->type }}</option>
							@endforeach
						</select>
						<button class="btn btn-secondary" wire:click="createSubdomain" wire:loading.attr="disabled">
							Создать
						</button>
					</div>
				</div>
			</div>
			@if($this->domain->dnsProvider?->service == 'cloudflare')
			<div class="row mb-2">
				<div class="col-12">
					<label>
						Переехать на другой аккаунт Cloudflare
					</label>
				</div>
				<div class="col-10">
					<select wire:model="moveToThisCloudflareAccount" class="form-control" wire:loading.attr="disabled">
						<option>Выберите аккаунт для переноса</option>
						@foreach ($this->availableForTransferCloudflareAccounts as $provider)
						<option value="{{ $provider->id }}">{{ $provider->email }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-2">
					<button type="submit" class="btn btn-primary" wire:click="moveDomainToAnotherCloudflareAccount" wire:loading.attr="disabled">ОК</button>
				</div>
			</div>
			@endif
			<div class="row">
				<div class="col-12">
					<ul>
						@foreach ($dnsMessages as $message)
						<li>{{ $message }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
		@endif
        @endif
		@endcan

		<div class="col-md-4 mb-3">
			<label>Создать TXT запись</label>
			<div class="input-group mb-2">
				<input class="form-control" type="text" wire:model.live="txt_record_content" placeholder="Content">
				<button class="btn btn-secondary" wire:click="createTxtRecord" wire:loading.attr="disabled">
					Создать
				</button>
			</div>
		</div>

		<div class="col-12 mb-3">
			<label>Связанные родительские домены</label>
			@foreach ($domain->related_parent_domains->sortBy('domain') as $parent_domain)
            @if ($isModal)
            <button class="btn btn-link" wire:click="$dispatchTo('types.engine.admin.mirrors.domain-settings-modal', 'set-domain', { domain: '{{ $parent_domain->domain }}' })">
				{{ $parent_domain->domain }}
			</button>
            @else
			<a class="btn btn-link" href="{{ route('domain.settings',[ 'domain' => $parent_domain->domain ]) }}">
				{{ $parent_domain->domain }}
			</a>
            @endif
			@endforeach
		</div>

	</div>

    @script
    <script>
        $wire.on('domain-updated', () => {
            bootstrap.Modal.getInstance('#domainSettings').hide();
        });
    </script>
    @endscript

</div>
