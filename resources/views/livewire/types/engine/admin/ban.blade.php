<div class="card mb-2">
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-1">
                #{{ $ban->id }}
            </div>
            <div class="col-12 col-md-6">
                @if ($descriptionEditor)
                <form wire:submit="updateDescription">
                    <textarea class="form-control" wire:model="description"></textarea>
                    <button class="btn">ОК</button>
                </form>
                @else
                <span wire:click="$set('descriptionEditor', true)">{{ $ban->description }}</span>
                @endif
            </div>
            <div class="col-12 col-md-2">
                <div class="form-check form-switch">
                    <input wire:model.live="active" class="form-check-input" type="checkbox" role="switch" id="active{{ $ban->id }}">
                    <label class="form-check-label" for="active{{ $ban->id }}">
                        <span @class(['text-muted' => !$ban->active, 'fw-bold' => $ban->active])>бан</span> [{{ $ban->q }}]
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-2">
                <div class="form-check form-switch">
                    <input wire:model.live="hidePlayer" class="form-check-input" type="checkbox" role="switch" id="hidePlayer{{ $ban->id }}">
                    <label class="form-check-label" for="hidePlayer{{ $ban->id }}">
                        <span @class(['text-muted' => !$ban->hide_player, 'fw-bold' => $ban->hide_player])>плеер</span> [{{ $ban->q_player }}]
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-1">
                <a href="{{ route('visits', ['ban' => $ban]) }}">визиты</a>
            </div>
            <div class="col-12">
                <code>{{ stripslashes($ban->ip) }}</code>
            </div>
            <div class="col-12">
                <code>{{ stripslashes($ban->user_agent) }}</code>
            </div>
            <div class="col-12">
                <code>{{ stripslashes($ban->referer) }}</code>
            </div>
            <div class="col-12">
                <code>{{ stripslashes($ban->query) }}</code>
            </div>
            <div class="col-12 col-md-5">
                <b>Добавлено:</b> {{ $ban->created_at }}
            </div>
            <div class="col-12 col-md-5">
                <b>Обновлено:</b> {{ $ban->updated_at }}
            </div>
            <div class="col-12 col-md-2 text-end">
                <button wire:click="delete" class="btn btn-sm btn-danger">
                    <i class="fa-solid fa-trash"></i>
                    @if ($deleteConfirmed)
                    Точно?
                    @endif
                </button>
            </div>
        </div>
    </div>
</div>