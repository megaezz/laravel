<div class="container">

    <div class="row mb-2">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
            <select wire:model.live="orderBy" class="form-select">
                @foreach ($orders as $order)
                <option value="{{ $order }}" {{ ($orderBy == $order) ? 'selected' : '' }}>{{ $order }}</option>
                @endforeach
            </select>
        </div>
    </div>

    @foreach ($this->bans as $ban)
    {{-- компонент при удалении вызывает событие deleted и тут описано что это событие вызывает рефреш родительского компонента --}}
    <livewire:types.engine.admin.ban :$ban :key="$ban->id" @deleted="$refresh" />
    @endforeach

    <ul class="pagination">
        {{ $this->bans->count() ? $this->bans->links() : null }}
    </ul>
</div>
