<legend class="text-black px-3">
    {{ __('Checking data') }}
</legend>
<div class="rounded bg-white mb-3 p-4">
    <ul>
        @forelse ($task->fields as $field)
        <li>{{ $field->name }}</li>
        @empty
        <li>{{ __('no fields') }}</li>
        @endforelse
    </ul>
</div>