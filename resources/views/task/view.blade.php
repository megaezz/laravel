<legend class="text-black px-3">
    {{ $task->title }}
</legend>
<div class="rounded bg-white mb-3 p-4">
    {!! $task->text !!}
</div>
