<div>
    <livewire:types.engine.admin.mirrors.domain-card :domain="$getRecord()" view="livewire.types.engine.admin.mirrors.domain-card-tailwind" :key="$getRecord()->domain" />
</div>
