<x-filament-panels::page>
    <div class="container mx-auto p-4">
        @foreach ($this->getTypes() as $type => $images)
            <h1 class="text-2xl font-semibold text-gray-800 border-b-2 border-gray-200 mb-6 pb-2">{{ $type }}</h1>
            <div class="grid grid-cols-1 md:grid-cols-3 gap-6 mb-8">
                @foreach ($images as $image)
                    <div class="flex flex-col bg-white shadow rounded-lg overflow-hidden">
                        <div class="bg-gray-100 p-4">
                            <img src="/storage/images/w200{{ $image }}" class="w-full h-auto object-cover">
                        </div>
                        <div class="p-4">
                            <p class="text-sm text-gray-600 truncate">{{ $image }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
</x-filament-panels::page>
