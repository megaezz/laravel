<x-pulse>

    <section class="overflow-x-auto pb-px default:col-span-full default:lg:col-span-1 default:row-span-1">
        <a href="/"
            class="inline-flex items-center px-4 py-2 bg-blue-500 text-white font-semibold text-sm rounded-lg shadow-sm hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-400 focus:ring-offset-2">
            ← Панель
        </a>
    </section>

    <livewire:pulse.servers cols="11" />

    <livewire:requests-graph cols="full" />

    {{-- <livewire:pulse.latest-logs cols="4" rows="1" /> --}}

    <livewire:fpm cols="7" />

    <livewire:database cols='5' title="Active threads" :values="['Threads_connected', 'Threads_running']" :graphs="['avg' => ['Threads_connected' => '#ffffff', 'Threads_running' => '#3c5dff']]" />

    <livewire:pulse.usage cols="4" rows="2" />

    <livewire:pulse.queues cols="4" />

    <livewire:pulse.cache cols="4" />

    <livewire:pulse.slow-queries cols="8" />

    {{-- <livewire:database cols='6' title="Connections" :values="['Connections', 'Max_used_connections']" /> --}}

    {{-- <livewire:database cols='full' title="Innodb" :values="['Innodb_buffer_pool_reads', 'Innodb_buffer_pool_read_requests','Innodb_buffer_pool_pages_total']" :graphs="['avg' => ['Innodb_buffer_pool_reads' => '#ffffff', 'Innodb_buffer_pool_read_requests' => '#3c5dff'],]" /> --}}

    <livewire:pulse.exceptions cols="6" />

    <livewire:pulse.slow-requests cols="6" />

    <livewire:pulse.slow-jobs cols="6" />

    <livewire:pulse.slow-outgoing-requests cols="6" />

    <livewire:pulse.schedule cols="6" />
</x-pulse>
