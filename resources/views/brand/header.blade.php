@push('head')
    <!-- <link href="/favicon.ico" id="favicon" rel="icon"> -->
@endpush

<div class="h2 fw-light d-flex align-items-center">
   <x-orchid-icon path="rocket" width="1.2em" height="1.2em"/>

    <p class="ms-3 my-0 d-none d-sm-block">
        Crypto
        <small class="align-top opacity">Platform</small>
    </p>
</div>