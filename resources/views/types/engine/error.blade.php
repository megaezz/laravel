<html>
    <head>
        <title>Произошла ошибка</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        @if ($refreshed)
        <script type="text/javascript">
        setTimeout("window.location.reload()", 2000);
        </script>
        @endif
        <style>
        body {
            background-color: #333;
            text-align: center;
            margin: 30px;
            color: #ccc;
        }
        </style>
    </head>
    <body>
        <h1>{{ $text }}</h1>
    </body>
</html>
