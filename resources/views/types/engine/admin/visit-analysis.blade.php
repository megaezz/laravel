<x-types.engine.admin.dashboard>
	<div class="container">
		<livewire:types.engine.admin.visit-analysis :$type :$service :$days :$limit :$withBanInfo :$withRealBots :$q lazy />
	</div>
</x-types.engine.admin.dashboard>