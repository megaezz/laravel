<x-types.engine.admin.dashboard>
	<div class="container">
		<h3>Меню {{ $domain->domain_decoded }}</h3>
		<x-types.engine.admin.domain-menu :$domain />
	</div>
</x-types.engine.admin.dashboard>