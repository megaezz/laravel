<x-types.engine.admin.dashboard>
	<style>
		.visits, .logs {
			font-size: 12px;
		}
		.visits .date, .logs .date {
			width: 140px;
		}
		.visits .user-agent, .logs .user-agent {
			width: 300px;
			word-break: break-all;
		}
		.visits .query, .logs .query {
			min-width: 100px;
			word-break: break-all;
		}
		.visits .referer, .logs .referer {
			width: 200px;
			word-break: break-all;
		}
		.visits .service, .logs .service {
			color: red;
			font-weight: bold;
		}
		.visits .ip, .logs .ip {
			width: 100px;
			word-break: break-all;
		}

		.logs .text {
			min-width: 100px;
			word-break: break-all;
		}
		.logs .backtrace {
			width: 100px;
			word-break: break-all;
		}
		.logs .type {
			width: 50px;
		}
	</style>
	<livewire:types.engine.admin.visits />
</x-types.engine.admin.dashboard>