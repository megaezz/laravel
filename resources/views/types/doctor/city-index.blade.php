<x-types.doctor.layout :$domain :$city :$headers>
	<div class="main__page page page-home page-home_with-top">
		<livewire:doctor.index :$city showCityEntities="true" />
	</div>
</x-types.doctor.layout>
