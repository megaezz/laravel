<x-types.doctor.layout :$domain :$city :$headers>
	<x-slot name="head">
		<link rel="stylesheet" href="/types/doctor/css/pressAboutUs.BIlxYDhL.css">
	</x-slot>
	<div class="main__page page page-home page-home_with-top">
		<livewire:doctor.index :$city />
		<div class="container">
			<div class="page-with-sidebar__main">
				<div class="home-citys">
					<div class="home-citys__title"> Популярные города </div>
					<div class="home-citys__list">
						@foreach ($popularCitiesWithDoctorsCount as $popularCity)
						<a href="{{ $domain->toRoute->cityIndex(['cityDoctuId' => $popularCity->doctu_id ]) }}" class="item marked">
							<div class="item__counter">
								{{ number_format($popularCity->doctors_count) }}
							</div>
							<div class="item__title">{{ $popularCity->name }}</div>
						</a>
						@endforeach
					</div>
					<a href="#" class="btn btn-white-green home-citys__all"> Все города </a>
				</div>
				<div class="press-about-as">
					<div class="press-about-as__title"> СМИ о нас </div>
					<div class="splide press-about-as__slider is-initialized" id="splide03" role="region">
						<div class="splide__track" id="splide03-track">
							<ul class="splide__list" id="splide03-list" style="">
								<li class="splide__slide press-about-as-teaser" id="splide03-slide01">
									<div class="press-about-as-teaser__desc">
										Все отзывы сотрудники сервиса модерируют, проверяя на соответствие
									</div>
									<div class="press-about-as-teaser__img">
										<img alt="metro_logo" src="/types/doctor/images/press/press001.png">
									</div>
								</li>
								<li class="splide__slide press-about-as-teaser" id="splide03-slide02">
									<div class="press-about-as-teaser__desc">
										Сервис позволяет записаться как в муниципальные, так и в частные клиники.
									</div>
									<div class="press-about-as-teaser__img">
										<img alt="afisha_daily" src="/types/doctor/images/press/press002.png">
									</div>
								</li>
								<li class="splide__slide press-about-as-teaser" id="splide03-slide03">
									<div class="press-about-as-teaser__desc">
										На сайте всё максимально понятно оформлено, им очень удобно пользоваться.
									</div>
									<div class="press-about-as-teaser__img">
										<img alt="Дневник" src="/types/doctor/images/press/press003.png">
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</x-types.doctor.layout>
