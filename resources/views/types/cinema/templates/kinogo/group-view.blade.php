<x-types.cinema.templates.kinogo.layout :$headers>
	<x-slot name="head">
		<link rel="canonical" href="{{ $requestedDomain->toRoute->absolute()->groupView($group) }}">
	</x-slot>
	<livewire:types.cinema.templates.kinogo.movie-list :$domain :$group />
</x-types.cinema.templates.kinogo.layout>