<x-types.cinema.templates.kinogo.layout :$headers>
	<x-slot name="head">
		<link rel="canonical" href="{{ $requestedDomain->toRoute->absolute()->index() }}">
	</x-slot>
	<x-slot name="bottomTextExtra">
		<h1 style="text-align: center;"><span style="font-size:17px;">{{ $headers->h1 }}</span></h1>
	</x-slot>
	<livewire:types.cinema.templates.kinogo.movie-list :$domain />
</x-types.cinema.templates.kinogo.layout>