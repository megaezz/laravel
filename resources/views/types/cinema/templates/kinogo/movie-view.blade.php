<x-types.cinema.templates.kinogo.layout :$headers>
	<x-slot name="htmlPrefix">
		og: http://ogp.me/ns# video: http://ogp.me/ns/video# ya: http://webmaster.yandex.ru/vocabularies/
	</x-slot>
	<x-slot name="head">
		<link rel="canonical" href="{{ $requestedDomain->toRoute->absolute()->movieView($domainMovie) }}">
		<x-types.cinema.meta.movie.og :$domain :$headers :$domainMovie />
		<x-types.cinema.meta.movie.ya-ovs :$domainMovie />
		<x-types.cinema.meta.movie.video :$domainMovie />
		<meta name="article:published_time" content="{{ $domainMovie->add_date->format('Y-m-d') }}" />
	</x-slot>

	<x-slot name="speedbar">
		&raquo; {{ $domainMovie->movie->title }} ({{ $domainMovie->movie->year }}) смотреть онлайн бесплатно
	</x-slot>

	<div itemscope itemtype="http://schema.org/Movie">
		<div style="display:none;" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
			{{-- TODO абсолют? и тут разные размеры на картинках и разные расширения --}}
			<link itemprop="thumbnail" href="{{ $domainMovie->movie->poster_or_placeholder }}" />
			<img itemprop="thumbnailUrl" src="{{ $domainMovie->movie->poster_or_placeholder }}" title="{{ $domainMovie->movie->title }}" alt="{{ $domainMovie->movie->title }}">
			<meta itemprop="description" content="{{ \Str::limit($headers->description, 200) }}" />
			@if($domainMovie->movie->type == 'serial')
			<meta itemprop="name" content="{{ $domainMovie->movie->title }} {{ $domainMovie->movie->year }} смотреть онлайн сериал все серии" />
			@endif
			{{-- посмотреть как на киного для фильма --}}
			@if($domainMovie->movie->type == 'movie')
			<meta itemprop="name" content="{{ $domainMovie->movie->title }} {{ $domainMovie->movie->year }} смотреть фильм онлайн" />
			@endif
			<meta itemprop="uploadDate" content="{{ $domainMovie->add_date->toIso8601String() }}" />
			<meta itemprop="datePublished" content="{{ $domainMovie->add_date->format('Y-m-d') }}">
			{{-- был эмбед из ютуба, но гугл ругался на пустое значение embedUrl, заменил на роут embed_players --}}
			{{-- <link itemprop="embedUrl" href="{{ $domainMovie->movie->kinopoisk?->changeYoutubeUrlToEmbed($domainMovie->movie->kinopoisk?->main_trailer?->url) }}" /> --}}
			<link itemprop="embedUrl" href="{{ $requestedDomain->toRoute->absolute()->embedPlayers(['domainMovie' => $domainMovie->id]) }}" />
			{{-- абсолютный путь должен быть --}}
			<link itemprop="url" href="{{ $requestedDomain->toRoute->absolute()->movieView($domainMovie) }}" />
			{{-- брать инфу откуда нибудь --}}
			<meta itemprop="isFamilyFriendly" content="true">
			<meta itemprop="playerType" content="HTML5">
			{{-- брать инфу откуда нибудь --}}
			<meta itemprop="duration" content="PT3600S" />
			<meta itemprop="width" content="1280">
			<meta itemprop="height" content="720">
			<meta itemprop="videoQuality" content="high">
		</div>
		<meta itemprop="dateCreated" content="{{ $domainMovie->add_date->format('Y-m-d') }}">
		<meta itemprop="inLanguage" content="ru">
		<div class="funct">
			<h1 style="text-align: center;">
				<span style="font-size:17px;color:#fff;" itemprop="name">
					{{ $headers->h1 }}
				</span>
			</h1>
		</div>
		<div class="fullstory">
			<div class="shortstorytitle">
				<div style="width: 120px;float: right;">
					<div class="podrobnosti_right">
						<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
							<meta itemprop="bestRating" content="10">
							<meta itemprop="worstRating" content="1">
							<meta itemprop="ratingValue" content="{{ round((float)$domainMovie->movie->rating_sko, 1) }}">
							{{-- минимальный ratingCount должен быть 1, поэтому max(1, $votes) чтобы гугл не заебывал ошибкой: Значение, заданное для элемента данных "ratingCount", должно быть положительным. --}}
							<meta itemprop="ratingCount" content="{{ max(1, round(((int) $domainMovie->movie->kinopoisk_votes + (int) $domainMovie->movie->imdb_votes) / 2)) }}">
							{{-- сделать рейтинг --}}
							<div id="ratig-layer-{{ $domainMovie->id }}">
								<div class="rating">
									<ul class="unit-rating">
										<li class="current-rating" style="width:{{ round((float)$domainMovie->movie->rating_sko, 1) * 10 }}%;">
											{{ round((float)$domainMovie->movie->rating_sko, 1) * 10 }}
										</li>
										<li><a href="#" title="Плохо" class="r1-unit" onclick="doRate('1', '{{ $domainMovie->id }}'); return false;">1</a></li>
										<li><a href="#" title="Приемлемо" class="r2-unit" onclick="doRate('2', '{{ $domainMovie->id }}'); return false;">2</a></li>
										<li><a href="#" title="Средне" class="r3-unit" onclick="doRate('3', '{{ $domainMovie->id }}'); return false;">3</a></li>
										<li><a href="#" title="Хорошо" class="r4-unit" onclick="doRate('4', '{{ $domainMovie->id }}'); return false;">4</a></li>
										<li><a href="#" title="Отлично" class="r5-unit" onclick="doRate('5', '{{ $domainMovie->id }}'); return false;">5</a></li>
									</ul>
									<div class="rating_digits">
										<div class="rating_digits_1">Рейтинг: <span>{{ round((float)$domainMovie->movie->rating_sko, 1) }}</span>/10
											(<span>{{ round(((int)$domainMovie->movie->kinopoisk_votes + (int)$domainMovie->movie->imdb_votes) / 2) }}</span>)
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					{{-- TODO хз че тут --}}
					<span style="display:none;">
						<span id="vote-num-id-{{ $domainMovie->id }}">11</span> 
					</span>
					<span class="izbrannoe">
						<span class="favoriticon" onclick="openbox('openwindow'); return false">
							<img width="27" height="37" src="/types/cinema/template/templates/kinogo/images/plus.webp" title="Добавить в свои закладки на сайте" style="vertical-align: middle;border: none;" alt="">
						</span>
					</span>
				</div>
			</div>

			<div class="fullimg">
				<div id="news-id-{{ $domainMovie->id }}-descr" style="display:inline;">
					<img itemprop="image" width="200" height="300" src="{{ $domainMovie->movie->poster_or_placeholder }}" style="float:left;box-sizing: unset!important;" alt="{{ $domainMovie->movie->title }}" title="{{ $domainMovie->movie->title }}"/>
					{!! nl2br(e($domainMovie->description_or_copypaste)) !!}
					<div style="clear:both"></div>
					<br/>
					<b>Год:</b> {{ $domainMovie->movie->year }}<br>
					<b>Страна:</b> {{ $domainMovie->movie->tags->where('type', 'country')->pluck('tag')->implode(', ') }}<br>
					<b>Жанр:</b>
					<span itemprop="genre">
						@foreach ($domainMovie->movie->groups->where('type', 'genre')->all() as $group)
						<a href="{{ $domain->toRoute->groupView($group) }}">{{ $group->name }}</a>@if (!$loop->last), @endif
						@endforeach
					</span>
					<br/>
					{{-- TODO подумать надо --}}
					<b>Качество:</b> Трейлер<br>
					<b>Перевод:</b> Не требуется<br>
					@if ($domainMovie->movie->last_episode)
					<b>Последняя серия онлайн:</b> {{ $domainMovie->movie->last_episode->season }} сезон {{ $domainMovie->movie->last_episode->episode }} серия <br/> 
					@endif
					<b>Продолжительность:</b> <span>60 мин.</span> <br/>


					<b>Режиссер:</b>
					<span itemprop="director">
						@foreach ($domainMovie->movie->tags->where('type', 'director') as $tag)
						{{-- TODO ссылка --}}
						<a href="#">{{ $tag->tag }}</a>@if (!$loop->last), @endif
						@endforeach
					</span>
					<br/>
					<b>В ролях:</b>
					<span itemprop="actor">
						{{ $domainMovie->movie->tags->where('type', 'actor')->pluck('tag')->implode(', ') }}
					</span>
					<br/><br/>
					<div style="clear:both">
					</div>
				</div>
				{{-- рекламка --}}
				{{-- <ins class="604c7625" data-key="47959b1c28094532b37e5577cf33cde9" data-cp-host="e4ddeac60ea001c63fb7fa9ce27a77c0|2|kinogo.inc"></ins> --}}
				<div class="stronk">
					<h2>{{ $headers->h2 }}</h2>
				</div>
			</div>
		</div>

		<div style="clear:both"></div>
		
		<div class="player-loading-style">
			<livewire:types.cinema.players template="livewire.types.cinema.templates.kinogo.players" :domainMovieId="$domainMovie->id" lazy  />
		</div>
    
		{{-- <ul class="mylists-switch" data-id="{{ $domainMovie->id }}"></ul> --}}
		<!--noindex-->
		{{-- <style>
			.kreativ::after {
				font-size: 13px;
				content: " и смотри БЕЗ РЕКЛАМЫ. Для Зарегистрированных посетителей мы сокращаем рекламу! Оставь комментарий :)";
				margin-bottom: 10px;
			}
		</style>
		<div class="trivia" style="background-color: #000;padding: 20px 20px 10px;margin-top: -10px;">
			<div class="kreativ"><a href="/index.php?do=register">Регистрируйся</a></div>
		</div> --}}
		{{-- <div class="icons2" style="background-color: #000;border: unset!important;">
			<div class="hm-flex">
			</div>
		</div> --}}
		<!--/noindex-->

		<div class="relatednews_title">
			<p>
				<strong>Похожие материалы</strong>:
			</p>
		</div>
		<div class="relatednews">
			<div style="margin-left: 2em" class="ul_related">
				<ul>
					@foreach ($domainMovie->relatedMovies()->activeRelatedMovies()->limit(5)->get() as $relatedDomainMovie)
					<li>
						<a href="{{ $domain->toRoute->movieView($relatedDomainMovie) }}" title="{{ $relatedDomainMovie->movie->title }}">
							<img loading="lazy" src="/types/cinema/template/templates/kinogo/images/noposter.webp" data-src="{{ $relatedDomainMovie->movie->poster_or_placeholder_webp }}" alt="{{ $relatedDomainMovie->movie->title }} {{ $relatedDomainMovie->movie->year }}">
							{{ $relatedDomainMovie->movie->title }}
						</a>
					</li>
					@endforeach
				</ul>
			</div>
		</div>

		{{-- рекламка --}}
		{{-- <ins class="604c7625" data-key="47959b1c28094532b37e5577cf33cde9" data-cp-host="e4ddeac60ea001c63fb7fa9ce27a77c0|2|kinogo.inc"></ins> --}}
    
		<div style="clear:both"></div>
		<div class="sociallll" style="padding:25px 20px 20px 20px;">
			<span class="dateicon" title="Дата">
				@if($domainMovie->add_date->isToday())
				Сегодня, {{ $domainMovie->add_date->format('H:i') }}
				@elseif($domainMovie->add_date->isYesterday())
				Вчера, {{ $domainMovie->add_date->format('H:i') }}
				@else
				{{ $domainMovie->add_date->format('d-m-Y, H:i') }}
				@endif
			</span>

		</div>

		<div style="clear:both"></div>
	</div>
	<style>
		textarea#comments {
			max-width:99%;
			width:99%;
		}    
	</style>
	<form method="post" action="" name="dlemasscomments" id="dlemasscomments">
		<div id="dle-comments-list">
			<a name="comment"></a>
			@foreach ($domainMovie->comments()->orderByDesc('date')->get() as $comment)
			<div id="comment-id-{{ $comment->id }}">
				<div class="poloska_comment"></div>
				<div class="padding_border_comment">
					<div style="float:left;width:60px">
						<div class="commentimg">
							<img src="/types/cinema/template/templates/kinogo/images/noavatar1.png" width="60" height="60" title="Фото" alt="Фото">
						</div>
					</div>
					<div class="commennnnty">
						<b style="text-shadow: 1px 1px rgb(0, 0, 0); font-size:13.5px;">{{ $comment->username }}</b>
						{{ $comment->date->translatedFormat('j F Y') }} |
						<a onclick="dle_fastreply('{{ $comment->username }}'); return false;" href="#">[Ответить]</a>
						<br>
						<br>
						<div class="comentarii">
							<div id="{{ $loop->index }}" style="word-break: break-all;">
								<span id="selection_index121" class="selection_index"></span>
								<div id="comm-id-{{ $comment->id }}">
									{!! nl2br(e($comment->text)) !!}
								</div>
								<div class="ratebox2">
									<ul class="reset">
										<li>
											<a href="#" onclick="doCommentsRate('plus', '{{ $comment->id }}'); return false;" >
												<img src="/types/cinema/template/templates/kinogo/images/like.png" title="Нравится" alt="Нравится" width="14" height="14" />
											</a>
										</li>
										<li>
											<span id="comments-ratig-layer-{{ $comment->id }}" class="ignore-select">
												{{-- TODO лайки --}}
												<span class="ratingtypeplus ignore-select" >?</span>
											</span>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div style="clear:both"></div>
					</div>
				</div>
			</div>
			@endforeach
			<div id="dle-ajax-comments"></div>
		</div>
	</form>
	<form  method="post" name="dle-comments-form" id="form-comment" action="/api/addComment">
		<!--noindex-->
		<div class="ux-form">
			<ul class="ui-form" style="padding:10px;">

				<li>
					<input placeholder="Имя" type="text" name="name" id="name" class="f_input f_wide">
				</li>

				<li>
					<div class="bb-editor">
						<label for="comments">
							Добавить комментарий<br><br>
							<textarea name="message" id="comments" cols="70" rows="10"></textarea>
						</label>
					</div>
				</li>
			</ul>

			<div class="submitline" style="margin-left:10px;margin-bottom: 10px;">
				<button class="fbutton" id="form-comment-button" type="submit" style="height:48px;">Отправить комментарий</button>
			</div>
		</div>
		<!--/noindex-->
		<input id="g-recaptcha-response" type="hidden" name="g-recaptcha-response" value="">
		<input type="hidden" name="domain_movie_id" value="{{ $domainMovie->id }}">				
		@csrf
	</form>
	<script>
		document.getElementById('form-comment-button').onclick = function(e) {
			e.preventDefault();
			// load recaptcha script
			script = document.createElement("script");
			script.src = 'https://www.google.com/recaptcha/api.js?render={{ $requestedDomain->parent_domain_or_self->recaptchaKey?->public }}';
			// script.onreadystatechange = callback;
			script.onload = function(){
				grecaptcha.ready(function() {
					grecaptcha.execute('{{ $requestedDomain->parent_domain_or_self->recaptchaKey?->public }}', {action: 'submit'}).then(function(token) {
						document.getElementById('g-recaptcha-response').value = token;
						// alert(grecaptcha.getResponse());
						// alert(document.getElementById('form-comment'));
						document.getElementById('form-comment').submit();
					});
				});
			};
			document.body.appendChild(script);
		}
	</script>
</x-types.cinema.templates.kinogo.layout>