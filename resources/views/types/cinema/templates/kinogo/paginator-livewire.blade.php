@if ($paginator->hasPages())
<div class="bot-left">
    <div class="bot-navigation" style="padding:15px 0!important;text-align:center;">
        {{-- Кнопка "Раньше" --}}
        @if ($paginator->onFirstPage())
        <span>Раньше</span>
        @else
        <button wire:click="previousPage" dusk="previousPage">Раньше</button>
        @endif

        {{-- Ссылки на страницы --}}
        @foreach ($paginator->links() as $page => $url)
        @if (is_string($url))
        <span class="nav_ext">{{ $url }}</span>
        @else
        @if ($page == $paginator->currentPage())
        <span>{{ $page }}</span>
        @else
        <button wire:click="gotoPage({{ $page }})" dusk="page-{{ $page }}">{{ $page }}</button>
        @endif
        @endif
        @endforeach

        {{-- Кнопка "Позже" --}}
        @if ($paginator->hasMorePages())
        <button wire:click="nextPage" dusk="nextPage">Позже</button>
        @else
        <span>Позже</span>
        @endif
    </div>
</div>
@endif
