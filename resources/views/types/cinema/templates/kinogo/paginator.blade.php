@if ($paginator->hasPages())
<div class="bot-left">
    <div class="bot-navigation" style="padding:15px 0!important;text-align:center;">
        {{-- Кнопка "Раньше" --}}
        @if ($paginator->onFirstPage())
        <span>Раньше</span>
        @else
        <a href="{{ $paginator->previousPageUrl() }}">Раньше</a>
        @endif

        {{-- Ссылки на страницы --}}
        @foreach ($elements as $element)
        @if (is_string($element))
        <span class="nav_ext">{{ $element }}</span>
        @endif

        @if (is_array($element))
        @foreach ($element as $page => $url)
        @if ($page == $paginator->currentPage())
        <span>{{ $page }}</span>
        @else
        <a href="{{ $url }}">{{ $page }}</a>
        @endif
        @endforeach
        @endif
        @endforeach

        {{-- Кнопка "Позже" --}}
        @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}">Позже</a>
        @else
        <span>Позже</span>
        @endif
    </div>
</div>
@endif
