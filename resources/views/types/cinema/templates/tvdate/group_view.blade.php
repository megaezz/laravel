<x-types.cinema.templates.tvdate.layout :headers="$headers">
    <div class="content">
        <div class="container">
            <div class="moduletable interest">
                <div class="head-block">
                    <h3>Фильмы в категории {{ $group->name }}</h3>
                    {{-- TODO: создать раздел и раскоментировать --}}
                    {{-- <a href="/lists" title="Все списки">{{ ___('Все списки') }}</a> --}}
                </div>
                <div class="content-block">
                    <ul>
                        @foreach ($domain->cinemaDomain->movies()->group($group)->limit(10)->get() as $domainMovie)
                        <li>
                            <div class="image-block">
                                <a href="{{ $domain->toRoute->movieView($domainMovie) }}" title="{{ $domainMovie->movie->title }}">
                                    <img src="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder }}" alt="{{ $domainMovie->movie->title }}"/>
                                </a>
                            </div>
                            <div class="title-block">
                                <h4>
                                    <a href="{{ $domain->toRoute->movieView($domainMovie) }}" title="{{ $domainMovie->movie->title }}">{{ $domainMovie->movie->title }}</a>
                                </h4>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</x-types.cinema.templates.tvdate.layout>