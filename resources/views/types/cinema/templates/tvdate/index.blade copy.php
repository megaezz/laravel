<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-ru" lang="ru-ru" dir="ltr" prefix="og: http://ogp.me/ns# video: http://ogp.me/ns/video# article: http://ogp.me/ns/article# ya: http://webmaster.yandex.ru/vocabularies/">
<head>
    @include('types.cinema.templates.tvdate.blocks.meta')
    <link rel="canonical" title="{{ $domain->site_name }}" href="/">
    <meta property="og:description" content="{{ $headers->description }}"/>
    <meta property="og:image" content="/types/cinema/template/templates/tvdate/images/2.png">
</head>
<body class="tvd featured">
    <div id="page">
        @include('types.cinema.templates.tvdate.blocks.header')
        <div id="main">
            <div class="container">
                <div class="content">
                    <div class="custom finfo">
                        <div>
                            <p>Телевидение сейчас предоставляет самый обширный выбор телесерилов и шоу, за выходом которых обычный пользователь попросту не успевает следить. Сотни развлекательных программ, комедийных и детективных сериалов выпускают новые эпизоды каждый день! К Вам на выручку приходит сайт tvdate.ru, который собрал в себе расписания выхода зарубежных и русских сериалов которые сейчас идут на ТВ. С нами Вы не пропустите новую серию и будете в курсе планов продюсеров на выход новых сезонов шоу. Ждете выхода новой игры или какого либо события? Опять тут как тут tvdate - мы посчитали срок выхода за Вас и Вам осталось только следить за счетчиком расположенным на сайте.</p>
                        </div>
                        <ul>
                            <li>
                                <h2>Большой каталог сериалов</h2>
                                <p>На сайте представлена обширная картотека современных телевизионных шоу. Если Вы не нашли страницу любимого сериала напишите нам через форму обратной связи, возможно новая страница появится на сайте уже сегодня!</p>
                            </li>
                            <li>
                                <h2>Как поблагодарить</h2>
                                <p>Больше всего мы ценим наших драгоценных пользователей, которые заходят не только, чтобы узнать время выхода новой серии любимого шоу, а и поддерживает наш проект оставляя под каждой страницей вебсайта комментарии. Эта несложная манипуляция лучший вариант сказать «Спасибо!» разработчикам бесплатно. Присоединяйтесь к нам прямо сейчас!</p>
                            </li>
                        </ul>
                    </div>
                    <div class="blog-featured" itemscope itemtype="http://schema.org/Blog"></div>
                </div>
            </div>
        </div>
        @include('types.cinema.templates.tvdate.blocks.footer')
    </div>
    @include('types.cinema.templates.tvdate.blocks.scripts')
</body>
</html>

