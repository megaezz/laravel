<x-types.cinema.templates.tvdate.layout :headers="$headers">
    <x-slot name="head">
        <meta property="og:site_name" content="{{ $domain->site_name }}"/>
        <meta property="og:title" content="{{ $domainMovie->movie->title }} {{ ___('дата выхода серий') }}"/>
        <meta property="og:description" content="{{ $headers->description }}"/>
        <meta property="og:type" content="video.tv_show"/>
        <meta property="og:url" content="{{ $requestedDomain->toRoute->absolute()->movieView($domainMovie) }}"/>
        <meta property="og:image" content="{{ asset($domainMovie->movie->poster_or_placeholder) }}"/>
        <meta property="og:image:type" content="image/jpeg"/>
        <meta property="og:image:width" content=""/>
        <meta property="og:image:height" content=""/>
        {{-- <meta property="og:video:url" content="https://www.youtube.com/watch?v=YEqoTid5Aac"/> --}}
        {{-- <meta property="og:video" content="https://www.youtube.com/watch?v=YEqoTid5Aac"/> --}}
        <meta property="og:video:type" content="application/x-shockwave-flash"/>
        <meta property="og:video:width" content=""/>
        <meta property="og:video:height" content=""/>
        <meta property="og:video:duration" content=""/>
        <meta property="video:duration" content=""/>
        <meta property="og:video:release_date" content=""/>
        <meta property="og:video:tag" content="{keywords}"/>
        <meta property="ya:ovs:upload_date" content="{{ $domainMovie->add_date->toIso8601String() }}"/>
        <meta property="ya:ovs:status" content="published"/>
        <meta property="ya:ovs:adult" content="false"/>
        <meta property="ya:ovs:poster" content=""/>
        <meta property="ya:ovs:original_name" content="{{ $domainMovie->movie->title_en }}"/>
        <meta property="twitter:card" content="summary_large_image"/>
        <meta property="twitter:site" content="@tvdate"/>
        <meta property="twitter:creator" content="@tvdate"/>
        <meta property="article:published_time" content="{{ $domainMovie->add_date->toIso8601String() }}"/>
        <meta property="article:modified_time" content="{{ $domainMovie->add_date->toIso8601String() }}"/>
        <meta property="article:section" content="Сериалы"/>
        <link href="{{ $requestedDomain->toRoute->absolute()->movieView($domainMovie) }}" rel="canonical" title="Cериал: {{ $domainMovie->movie->title }}"/>
        {{-- TODO --}}
        {{-- <link href="//tvdate.ru/tainstvennaya-strast" rel="prev" title="Предыдущий сериал: Таинственная страсть"/> --}}
        {{-- <link href="//tvdate.ru/klim" rel="next" title="Следующий сериал: Клим"/> --}}
        <link href="/types/cinema/template/templates/tvdate/css/lightbox.min.css" rel="stylesheet" type="text/css"/>
    </x-slot>

    <div class="content">
        <div class="show serial">
            <div class="top">
                <div class="container">
                    <div class="breadcrumbs">
                        <span itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
                            <a href="/" rel="home" itemprop="url" title="{{ $domain->site_name }} - {{ ___('Дата выхода') }}">
                                <span itemprop="title">{{ ___('Дата выхода') }}</span>
                            </a>
                        </span>
                        {{-- создать раздел Сериалы и раскоментировать --}}
                        {{-- <span itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
                            <a href="/serials" itemprop="url" title="Все сериалы">
                                <meta itemprop="title" content="Сериалы"/>
                                <span>{{ ___('Сериалы') }}</span>
                            </a>
                        </span> --}}
                        <span itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
                            <link itemprop="url" href="{{ $domain->toRoute->movieView($domainMovie) }}"/>
                            <span itemprop="title">
                                {{ $domainMovie->movie->title }}
                            </span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="show serial" itemscope itemtype="https://schema.org/TVSeries">
                        <link itemprop="url" href="{{ $domain->toRoute->movieView($domainMovie) }}"/>
                        <meta itemprop="description" content="{{ isset($domainMovie->movie->myshows->data->description)?$domainMovie->movie->myshows->data->description:null }}"/>
                        <meta itemprop="datePublished" content="{{ $domainMovie->add_date->toIso8601String() }}"/>
                        <div class="info">
                            <div class="container"></div>
                            <div class="container">
                                <div class="title">
                                    <div class="lt">
                                        <div class="maint">
                                            <h1>
                                                <span itemprop="name">
                                                    {{ $domainMovie->movie->title }}
                                                </span>
                                            </h1>
                                            <span class="status st6">{{ ___('продолжается') }}</span>
                                        </div>
                                        <div class="subt">
                                            <meta itemprop="alternativeHeadline" content="{{ $domainMovie->movie->title_en }}"/>
                                            <span class="orig-title">{{ $domainMovie->movie->title_en }}</span>
                                        </div>
                                    </div>
                                    <div class="rt"></div>
                                </div>
                                <div class="poster" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                                    <a href="{{ $domain->toRoute->movieView($domainMovie) }}" data-lightbox="poster" data-title="{{ $domainMovie->movie->title }} постер">
                                        <picture>   
                                            <source srcset="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder_webp }}" type="image/webp">   
                                            <img srcset="/storage/images/w300{{ $domainMovie->movie->poster_or_placeholder }}" width="200" height="300" alt="{{ ___('Дата выхода сериала') }} «{{ $domainMovie->movie->title }}»"/>
                                        </picture>
                                    </a>
                                    <link itemprop="contentUrl" href="{{ $domainMovie->movie->poster_or_placeholder }}"/>
                                    <link itemprop="url" href="{{ $domain->toRoute->movieView($domainMovie) }}"/>
                                    <meta itemprop="width" content="300"/>
                                    <meta itemprop="height" content="200"/>
                                </div>
                                <div class="extra">
                                    <div class="lt">
                                        <ul>
                                            @if($domainMovie->movie->tagline)
                                            <li class="slogan">
                                                <span class="sub">{{ ___('Слоган') }}: </span>
                                                <q>{{ $domainMovie->movie->tagline }}</q>
                                            </li>
                                            @endif
                                            <li class="genres">
                                                <span class="sub">{{ ___('Жанр') }}: </span>
                                                @forelse ($domainMovie->movie->tags->where('type','genre')->all() as $tag)
                                                {{-- <a href="/tag/{{ $tag->id }}"> --}}
                                                   {{ $tag->tag }}
                                                {{-- </a> --}}
                                                @empty
                                                {{ ___('нет данных') }}
                                                @endforelse
                                            </li>
                                            <li class="premiere">
                                                <span class="sub">{{ ___('Премьера') }}: </span>
                                                <span>{{ \Carbon\Carbon::parse($domainMovie->movie->started)->format('d.m.Y') }}</span>
                                            </li>
                                            <li class="country" itemprop="countryOfOrigin" itemscope itemtype="http://schema.org/Country">
                                                <span class="sub">{{ ___('Страна') }}: </span>
                                                @forelse ($domainMovie->movie->tags->where('type','country')->all() as $tag)
                                                {{-- <a href="/tag/{{ $tag->id }}"> --}}
                                                   {{ $tag->tag }}
                                                {{-- </a> --}}
                                                @empty
                                                {{ ___('нет данных') }}
                                                @endforelse
                                            </li>
                                            <li class="chanel">
                                                <span class="sub">{{ ___('Канал') }}: </span>
                                                @forelse ($domainMovie->movie->tags->where('type','studio')->all() as $tag)
                                                {{-- <a href="/tag/{{ $tag->id }}"> --}}
                                                   {{ $tag->tag }}
                                                {{-- </a> --}}
                                                @empty
                                                {{ ___('нет данных') }}
                                                @endforelse
                                            </li>
                                            <li class="directors">
                                                <span class="sub">{{ ___('Режиссеры') }}: </span>
                                                @forelse ($domainMovie->movie->tags->where('type','director')->all() as $tag)
                                                {{-- <a href="/tag/{{ $tag->id }}"> --}}
                                                   {{ $tag->tag }}
                                                {{-- </a> --}}
                                                @empty
                                                {{ ___('нет данных') }}
                                                @endforelse
                                            </li>
                                            {{--
                                            <li class="authors">
                                                <span class="sub">Сценарий: </span>
                                                <span itemprop="author" itemscope itemtype="https://schema.org/Person">
                                                    <span itemprop="name">Джордж Р.Р. Мартин</span>
                                                </span>
                                                , 
                                                <span itemprop="author" itemscope itemtype="https://schema.org/Person">
                                                    <span itemprop="name">Дэвид Бениофф</span>
                                                </span>
                                                , 
                                                <span itemprop="author" itemscope itemtype="https://schema.org/Person">
                                                    <span itemprop="name">Д.Б. Уайсс</span>
                                                </span>
                                            </li>
                                            --}}
                                            <li class="actors">
                                                <span class="sub">{{ ___('Актеры') }}: </span>
                                                @forelse ($domainMovie->movie->tags->where('type','actor')->all() as $tag)
                                                {{-- <a href="/tag/{{ $tag->id }}"> --}}
                                                   {{ $tag->tag }}
                                                {{-- </a> --}}
                                                @empty
                                                {{ ___('нет данных') }}
                                                @endforelse
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="rt">
                                        <div class="ratings">
                                            <div>
                                                <ul>
                                                    <li class="rating-tvdate">
                                                        <span class="rating-title">
                                                            <span class="sub">{{ ___('Рейтинг') }}: </span>
                                                        </span>
                                                        <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                                            <span class="rating-info num extravote-info" id="extravote_197_0">
                                                                <meta itemprop="worstRating" content="1"/>
                                                                <span class="rating-value" itemprop="ratingValue">{{ $domainMovie->movie->rating_sko?round($domainMovie->movie->rating_sko,1):0 }}</span>
                                                                 / 
                                                                <span class="rating-count" itemprop="reviewCount">
                                                                    {{-- 1 чтобы гугл не доебывал ошибкой: Значение, заданное для элемента данных "reviewCount", должно быть положительным. --}}
                                                                    {{ $domainMovie->movie->kinopoisk_votes ?? 1 }}
                                                                </span>
                                                                <span class="hnone" itemprop="bestRating">10</span>
                                                            </span>
                                                            <span class="rating-stars">
                                                                <span class="rating-current" id="rating_197_0" style="width:{{ round($domainMovie->movie->rating_sko * 10) }}%;"></span>
                                                            </span>
                                                        </span>
                                                    </li>
                                                    <li class="rating-kp">
                                                        <span class="sub">{{ ___('Кинопоиск') }}: </span>
                                                        <span class="num">
                                                            {{ $domainMovie->movie->kinopoisk_ratinig??0 }}
                                                            <span> / {{ $domainMovie->movie->kinopoisk_votes??0 }}</span>
                                                        </span>
                                                    </li>
                                                    <li class="rating-imdb">
                                                        <span class="sub">IMDb: </span>
                                                        <span class="num">
                                                            {{ $domainMovie->movie->imdb_rating??0 }}
                                                            <span> / {{ $domainMovie->movie->imdb_votes??0 }}</span>
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        {{-- <div class="airdt">
                                            <span class="sub">{{ ___('Время выхода') }}: </span>
                                            <span>
                                                {{ ___('Воскресенье') }}
                                                <span>21:00</span>
                                            </span>
                                        </div> --}}
                                        <div id="button-fav" class="add" onclick="addremove('')">
                                            {{ ___('Добавить в список') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="smenu">
                            <div class="container">
                                <ul>
                                    <li>
                                        <span class="mstory">
                                            <span>{{ ___('Сюжет') }}</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="mschedule">
                                            <span>{{ ___('Расписание') }}</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="mcharters">
                                            <span>{{ ___('Роли') }}</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="mphotos">
                                            <span>{{ ___('Галерея') }}</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="mtrailers">
                                            <span>{{ ___('Трейлеры') }}</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="mfacts">
                                            <span>{{ ___('Факты') }}</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="mnews">
                                            <span>{{ ___('Новости') }}</span>
                                        </span>
                                    </li>
                                    <li>
                                        <span class="mcomments">
                                            <span>{{ ___('Обсуждение') }}</span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="container">
                            <div class="left">
                                <div id="story" class="story">
                                    <h2>
                                        <span>{{ $domainMovie->movie->title }}</span>
                                         {{ ___('дата выхода') }}
                                    </h2>
                                    <p>{!! isset($domainMovie->movie->myshows->data->description)?$domainMovie->movie->myshows->data->description:___('нет данных') !!}</p>
                                </div>
                                <div id="schedule" class="eplist">
                                    <h3>
                                        <span>
                                            {{ ___('Расписание выхода серий сериала') }}
                                            <span>{{ $domainMovie->movie->title }}</span>
                                        </span>
                                    </h3>
                                    <div class="eplin">
                                        {{ ___('Новые серии сериала «:movie_title» выходят в :weekday в :time на канале «:channel». Указанное время выхода соответствует стране - :country. По московскому времени эпизод будет доступен после :time того же дня.',
                                        [
                                        'movie_title' => $domainMovie->movie->title,
                                        'time' => '21:00',
                                        'channel' => isset($domainMovie->movie->myshows->data->network->title)?$domainMovie->movie->myshows->data->network->title:___('нет данных'),
                                        'country' => 'США',
                                        'weekday' => 'воскресенье'
                                        ]) }}
                                    </div>
                                    <div class="seasons">
                                        @if(isset($domainMovie->movie->myshows->data->episodes))
                                        @for($season = $domainMovie->movie->myshows->data->episodes[0]->seasonNumber; $season >= 1; $season--)
                                        <div id="s{{ $season }}" class="season toggle {{ ($season == $domainMovie->movie->myshows->data->episodes[0]->seasonNumber)?'full':'' }}" itemprop="containsSeason" itemscope itemtype="https://schema.org/TVSeason">
                                            <link itemprop="url" href="{{ $domain->toRoute->movieView($domainMovie) }}#s{{ $season }}"/>
                                            <meta itemprop="name" content="{{ $domainMovie->movie->title }} {{ $season }} {{ ___('сезон') }}"/>
                                            <table class="table">
                                                <caption class="season-head">
                                                    <span class="season-title">
                                                        {{ $domainMovie->movie->title }}
                                                        <span>{{ $season }} {{ ___('сезон') }}</span>
                                                        <span class="episodes-on-season"> - {{ count($domainMovie->movie->myshows->getEpisodesOfSeason($season)) }} {{ ___('серий') }}</span>
                                                        <span class="button-toggle on"></span>
                                                    </span>
                                                    <meta itemprop="numberOfEpisodes" content="?"/>
                                                </caption>
                                                <thead style="display: none;">
                                                    <tr>
                                                        <th>
                                                            <span>{{ ___('Номер серии') }}</span>
                                                        </th>
                                                        <th>{{ ___('Название серии') }}</th>
                                                        <th></th>
                                                        <th>{{ ___('Дата показа') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($domainMovie->movie->myshows->data->episodes as $episode)
                                                    @if($episode->seasonNumber == $season)
                                                    <tr id="{{ $episode->shortName }}" itemprop="episode" itemscope="" itemtype="https://schema.org/TVEpisode">
                                                        <td class="tbn">{{ $episode->shortName }}</td>
                                                        <td class="tbt">
                                                            <link itemprop="url" href="{{ $domain->toRoute->movieView($domainMovie) }}#{{ $episode->shortName }}">
                                                            <meta itemprop="episodeNumber" content="{{ $episode->episodeNumber }}">
                                                            <meta itemprop="name" content="{{ $episode->title }}">
                                                            <meta itemprop="description" content="">
                                                            <div class="episod-title">{{ $episode->title }}</div>
                                                        </td>
                                                        <td class="tbb">
                                                            <span class="button-fav"></span>
                                                            <span class="button-saw"></span>
                                                        </td>
                                                        <td class="tbd">{{ \Carbon\Carbon::parse($episode->airDate)->format('d.m.Y') }}
                                                            <div class="button-timer">
                                                                <div class="eptimer">
                                                                    <span class="timeago ep" title="{{ $episode->airDate }}"></span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <meta itemprop="datePublished" content="{{ \Carbon\Carbon::parse($episode->airDate)->format('Y-m-d') }}"/>
                                        </div>
                                        @endfor
                                        @endif
                                    </div>
                                </div>
                                @if(!empty($domainMovie->movie->kinopoisk->videos->items))
                                <div id="trailers" class="trailer">
                                    <h3>
                                        <span>
                                            {{ ___('Трейлеры сериала') }}
                                            <span>{{ $domainMovie->movie->title }}</span>
                                        </span>
                                    </h3>
                                    <div class="player" itemprop="video" itemscope itemtype="https://schema.org/VideoObject">
                                        <meta itemprop="name" content="{{ $domainMovie->movie->title }} {{ ___('дата выхода серий') }} {{ ___('трейлер') }}"/>
                                        <link itemprop="url" href="{{ $domain->toRoute->movieView($domainMovie) }}"/>
                                        {{-- TODO: duration --}}
                                        {{-- <meta itemprop="duration" content="PT0H02M48S"/> --}}
                                        <meta itemprop="isFamilyFriendly" content="True"/>
                                        <meta itemprop="uploadDate" content="{{ $domainMovie->add_date->toIso8601String() }}"/>
                                        <link itemprop="embedUrl" href="{{ $requestedDomain->toRoute->absolute()->embedPlayers(['domainMovie' => $domainMovie->id]) }}"/>
                                        <link itemprop="thumbnailUrl" href="{{ asset($domainMovie->movie->poster_or_placeholder) }}"/>
                                        {{-- TODO: тут указать правильный канал --}}
                                        <meta itemprop="description" content="Узнайте расписание выхода новых эпизодов телесериала {{ $domainMovie->movie->title }} от кабельного канала HBO. Трейлер к телешоу и подробности сюжета."/>
                                        <div class="youtube" itemprop="thumbnail" itemscope itemtype="https://schema.org/ImageObject">
                                            <link itemprop="url" href="{{ asset($domainMovie->movie->poster_or_placeholder) }}"/>
                                            <meta itemprop="width" content="700"/>
                                            <meta itemprop="height" content="380"/>
                                        </div>
                                        {{--
                                        <div id="videoplayer" class="player">
                                        </div>
                                        --}}
                                        @foreach($domainMovie->movie->kinopoisk->videos->items as $video)
                                        @if($video->site == 'YOUTUBE')
                                        <iframe type="text/html" width="100%" height="400px;" src="{{ str_replace('/v/','/embed/',$video->url) }}" frameborder="0"></iframe>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                                @if(!empty($domainMovie->movie->kinopoisk->facts->items))
                                <div id="news" class="newslist">
                                    <h3>
                                        <span>
                                            {{ ___('Новости сериала') }}
                                            <span>{{ $domainMovie->movie->title }}</span>
                                        </span>
                                    </h3>
                                    <ul>
                                        @foreach($domainMovie->movie->kinopoisk->facts->items as $fact)
                                        <li>
                                            <div class="nl_title">
                                                <a href="{{ $domain->toRoute->movieView($domainMovie) }}" title="{{ $domainMovie->movie->title }} {{ $domainMovie->movie->myshows->data->totalSeasons??'?' }} {{ ___('сезон') }}">
                                                    <span>{{ $domainMovie->movie->title }} {{ $domainMovie->movie->myshows->data->totalSeasons??'?' }} {{ ___('сезон') }}</span>
                                                </a>
                                            </div>
                                            <div class="nl_text">
                                                <p>{!! $fact->text !!}</p>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="moduletable interest">
                                    <div class="head-block">
                                        <h3>{{ ___('Подборки интересных сериалов') }}</h3>
                                        <a href="/lists" title="Все списки">{{ ___('Все списки') }}</a>
                                    </div>
                                    <div class="content-block">
                                        <ul>
                                            @foreach($domainMovie->movie->groups()->whereType('compilation')->whereNotNull('thumb')->limit(6)->get() as $group)
                                            <li>
                                                <div class="image-block">
                                                    <a href="{{ $domain->toRoute->groupView($group) }}" title="{{ $group->name }}">
                                                        <img src="/storage/images/w300/types/cinema/template/images/compilations/{{ $group->thumb }}" alt="{{ $group->name }}"/>
                                                    </a>
                                                </div>
                                                <div class="title-block">
                                                    <h4>
                                                        <a href="{{ $domain->toRoute->groupView($group) }}" title="{{ $group->name }}">{{ $group->name }}</a>
                                                    </h4>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <br/>
                                <div class="social-likes">
                                    <div class="vkontakte" title="{{ ___('Поделиться ссылкой во Вконтакте') }}">{{ ___('Вконтакте') }}</div>
                                    <div class="facebook" title="{{ ___('Поделиться ссылкой на Фейсбуке') }}">Facebook</div>
                                    <div class="twitter" title="{{ ___('Поделиться ссылкой в Твиттере') }}">Twitter</div>
                                    <div class="odnoklassniki" title="{{ ___('Поделиться ссылкой в Одноклассниках') }}">{{ ___('Одноклассники') }}</div>
                                    <div class="mailru" title="{{ ___('Поделиться ссылкой в Моём мире') }}">{{ ___('Мой мир') }}</div>
                                </div>
                                <br/>
                                <br/>
                                <div id="comments" class="comments">
                                    <h3>{{ ___('Обсуждение записи') }}</h3>
                                   
                                </div>
                            </div>
                            <div class="right">
                                <ul class="stats">
                                    <li class="statsico"></li>
                                    <li class="season-count">
                                        <span class="count">
                                            <meta itemprop="numberOfSeasons" content="{{ $domainMovie->movie->myshows->data->totalSeasons??'?' }}"/>
                                             {{ $domainMovie->movie->myshows->data->totalSeasons??'?' }}
                                        </span>
                                        <span class="unit"> {{ ___('сезонов') }} </span>
                                    </li>
                                    <li class="series-count">
                                        <span class="count">
                                            {{ isset($domainMovie->movie->myshows->data->episodes)?count($domainMovie->movie->myshows->data->episodes):'?' }}
                                        </span>
                                        <span class="unit">
                                           {{ ___('серии') }}
                                           <meta itemprop="numberOfEpisodes" content="73"/>
                                       </span>
                                    </li>
                                    @if(isset($domainMovie->movie->myshows->data->runtime))
                                    <li class="times-count">
                                        <span class="count">
                                            {{ round($domainMovie->movie->myshows->data->runtime * count($domainMovie->movie->myshows->data->episodes) / 60) }}
                                        </span>
                                        <span class="unit">
                                            {{ ___('часов') }}
                                        </span>
                                        <span class="runtime-count">
                                            <span class="sub">
                                                {{ ___('Длительность') }}:
                                            </span>
                                            <span>
                                                {{ $domainMovie->movie->myshows->data->runtime }}
                                            </span>
                                        </span>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="right">
                                <div class="moduletable seealso">
                                    <h3>{{ ___('Смотрите также') }}</h3>
                                    <div class="custom seealso">
                                        @foreach($domainMovie->relatedMovies()->with('movie', 'movie.myshows')->activeRelatedMovies()->limit(6)->get() as $v)
                                        <div class="im-news">
                                            <a href="{{ $domain->toRoute->movieView($v) }}" title="{{ $v->movie->title }} {{ $v->movie->myshows->data->totalSeasons??'?' }} {{ ___('сезон') }} {{ ___('дата выхода') }}">
                                                <picture>   
                                                    <source srcset="/storage/images/w300{{ $v->movie->poster_or_placeholder_webp }}" type="image/webp">
                                                    <img
                                                        srcset="/storage/images/w300{{ $v->movie->poster_or_placeholder }}"
                                                        width="300" height="200"
                                                        alt="{{ $v->movie->title }} {{ $v->movie->myshows->data->totalSeasons??'?' }} {{ ___('сезон') }} {{ ___('дата выхода') }}"
                                                        > 
                                                </picture>
                                                <span>
                                                    {{ $v->movie->title }} {{ $v->movie->myshows->data->totalSeasons??'?' }} {{ ___('сезон') }} {{ ___('дата выхода') }}
                                                </span>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                {{-- <div class="shownav">
                                    <div class="shownav">
                                        <a href="//tvdate.ru/tainstvennaya-strast" rel="prev" class="prev" title="{{ ___('Предыдущий сериал') }}: Таинственная страсть">
                                            <span>{{ ___('Предыдущий сериал') }}</span>
                                        </a>
                                        <br/>
                                        <a href="//tvdate.ru/klim" rel="next" class="next" title="{{ ___('Следующий сериал') }}: Клим">
                                            <span>{{ ___('Следующий сериал') }}</span>
                                        </a>
                                        <br/>
                                    </div>
                                </div> --}}
                            </div>
                            <div class="left"></div>
                            <div class="right"></div>
                        </div>
                        <div class="clr"></div>
                        <div class="bottom"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    
</x-types.cinema.templates.tvdate.layout>