<div class="search">
    <form name="form-search-show" action="/search" method="post">
        <div class="thc {{ $focus?'result hint backdrop focus':'' }} {{ $filter_opened?'filter':'' }}">
            <div class="thfield">
                <span class="thquery">
                    <input id="search-show" name="searchword" type="search" placeholder="{{ ___('Поиск сериала') }}" autocomplete="off" wire:model.live="query" wire:focus="onFocus" wire:blur="onBlur">
                </span>
                <span class="thfilter">
                    <button type="button" class="thfilter-button" wire:click="toggleFilterOpened">
                        <span class="thfilter-value">{{ $filter_name }}</span> <span class="thcaret"></span>
                    </button>
                    <ul class="thdropdown" style="">
                        <li wire:click="setFilter('Сериалы')"><a href="javascript:;">Сериалы</a></li>
                        <li wire:click="setFilter('Мультсериалы')"><a href="javascript:;">Мультсериалы</a></li>
                        <li class="divider"></li>
                        <li wire:click="setFilter('Все')"><a href="javascript:;">Все</a></li>
                    </ul>
                </span>
                <span class="thbutton">
                    <button type="submit">
                        <i class="search-icon"></i>
                    </button>
                </span>
            </div>
            <div class="thresult">
                <ul class="thlist">
                    @foreach ((object) $movies as $movie)
                    <li>
                        <a href="{{ $domain->toRoute->movieView($movie) }}" data-group="shows" data-index="0">
                            <div class="item">
                                <div class="img">
                                    <div>
                                        <img src="/storage/images/w40{{ $movie->movie->poster_or_placeholder }}" width="36" height="53" alt="{{ $movie->movie->title_ru }} ({{ $movie->movie->title_ru }})">
                                    </div>
                                </div>
                                <div class="stit">
                                    <span class="name">{{ $movie->movie->title }}</span>
                                    <span class="year">{{ $movie->movie->year }} - ...</span>
                                    <span class="orig">{{ $movie->movie->title_en }}</span>
                                </div>
                                <span class="sstatus {{ ($movie->movie?->myshows?->status == 'Canceled/Ended')?'st2':'' }}">
                                    {{ $movie->movie?->myshows?->status }}
                                </span>
                                <span class="cat">{{ $movie->movie->type }}</span>
                                <span class="rait">
                                    <span :style="width: {{ round($movie->movie->rating_sko ?? 0) * 10 }}%"></span>
                                </span>
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </form>
</div>