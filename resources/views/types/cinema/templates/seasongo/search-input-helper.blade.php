@foreach($movies as $movie)
<li class="list-group-item">
	<a href="{{ $domain->megaweb->getWatchLink($movie->megaweb) }}">
		{{ $movie->movie->title }} ({{ $movie->movie->localed_type }} {{ $movie->movie->year }})
	</a>
</li>
@endforeach