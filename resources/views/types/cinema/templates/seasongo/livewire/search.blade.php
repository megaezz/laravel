<form class="form-inline my-2 my-lg-0 main-menu-search" method="get" action="/search">
    <div class="btn-group">
        <input class="form-control mr-sm-2 main-search" type="search" name="q" placeholder="Начните вводить название" aria-label="Search" autocomplete="off" style="width: 216px;" wire:model.live="query" wire:focus="onFocus" wire:blur="onBlur">
        <div class="dropdown-menu search-helper" style="width: 200px; padding: 0; {{ $movies ? 'display: block;' : '' }}">
            <ul class="list-group search-helper-list">
                @foreach((object) $movies as $movie)
                <li class="list-group-item">
                    <a href="{{ $domain->toRoute->movieView($movie) }}">
                        {{ $movie->movie->title }} ({{ $movie->movie->localed_type }} {{ $movie->movie->year }})
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <button class="btn btn-outline-success my-0 my-sm-0" type="submit">Найти</button>
</form>