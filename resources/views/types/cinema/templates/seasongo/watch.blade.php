<x-types.cinema.templates.seasongo.layout :$domain :$headers :$engineConfig :$requestedDomain :$footerScripts :$movieOrders>
	<x-slot name="head">
		@if ($domain->cinemaDomain->hide_canonical and !\engine\app\models\Engine::isRealBot())
		<!-- c -->
		@else
		<link rel="canonical" href="{{ \engine\app\models\Engine::getCrawlerOrClientDomain()->toRoute->absolute()->movieView($domainMovie) }}">
		@endif

		{{-- {hreflang} --}}
		<meta property="og:site_name" content="Смотреть сериалы HD онлайн бесплатно в хорошем качестве">
		<meta property="og:type" content="article">
		<meta property="og:title" content="{{ $domainMovie->movie->localed_type }} {{ $domainMovie->movie->title }} смотреть онлайн бесплатно в хорошем качестве HD">
		<meta property="og:url" content="{{ $requestedDomain->toRoute->absolute()->movieView($domainMovie) }}">
		<meta property="og:description" content="{{ \Str::limit($domainMovie->description ?? $domainMovie->movie->description, 157) }}">
		<meta property="og:image" content="{{ asset($domainMovie->movie->poster_or_placeholder) }}">

	</x-slot>

	<x-types.cinema.schema-org.video-object :$requestedDomain :$domainMovie />
	
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb" itemscope="" itemtype="https://schema.org/BreadcrumbList">
			<li class="breadcrumb-item" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
				<a itemprop="item" href="/">
					<span itemprop="name">{{ $domain->site_name }}</span>
				</a>
				<meta itemprop="position" content="1">
			</li>
			<li class="breadcrumb-item active" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem" aria-current="page">
				<span itemprop="name">{{ $domainMovie->movie->title }}</span>
				<meta itemprop="item" content="{{ $requestedDomain->toRoute->absolute()->movieView($domainMovie) }}">
				<meta itemprop="position" content="2">
			</li>
		</ol>
	</nav>
	<div class="card mb-4">
		<!-- <div class="card-header">
			<h1 style="color: rgb(205, 185, 158); font-size: 2em;">{{ $headers->h1 }}</h1>
		</div> -->
	<div class="card-body">
		<div class="row">
			<div class="col-md-4" style="margin-bottom: 20px;">
				<div class="poster-hd">HD 1080</div>
				<picture>   
					<source srcset="{{ $domainMovie->movie->poster_or_placeholder_webp }}" type="image/webp">   
					<img class="mb-3" srcset="{{ $domainMovie->movie->poster_or_placeholder }}" alt="Смотреть {{ $domainMovie->movie->title }} онлайн в HD качестве" title="Обложка к {{ $domainMovie->movie->localed_type }} {{ $domainMovie->movie->title }}" style="width: 100%; height: auto;" width="200" height="300">
				</picture>
				<div class="rating-bar mb-3">
					<div class="rate-0">
						<span class="animate orange"></span>
					</div>
				</div>
				<script>
					document.addEventListener("DOMContentLoaded", function() {
						ceilRate = Math.ceil({{ $domainMovie->movie->kinopoisk_rating ?? 0 }});
						$('.rate-0').addClass('rate-'+ceilRate);
					});
				</script>
				<p style="font-size: 2em; text-align: center;">
					<span class="thumb-up">
						<i class="far fa-thumbs-up"></i>
						<i class="fas fa-thumbs-up"></i>
					</span>
					<span style="margin-left: 20px;"></span>
					<span class="thumb-down">
						<i class="far fa-thumbs-down"></i>
						<i class="fas fa-thumbs-down"></i>
					</span>
				</p>
				<p style="text-align: center;">
					<span class="thumb-message" style="display: none;"></span>
				</p>
				<style>
					.thumb-up .fas, .thumb-down .fas {
						display: none;
					}
					.thumb-up:hover .far, .thumb-down:hover .far {
						display: none;
					}
					.thumb-up:hover .fas, .thumb-down:hover .fas {
						display: inline;
					}
				</style>
				<script>
					function thumb_recorded() {
						$('.thumb-message').html('Спасибо, запомнили');
						$('.thumb-message').fadeIn('slow');
						$('.thumb-message').fadeOut('slow');
					}

					document.addEventListener("DOMContentLoaded", function() {
						$('.thumb-up').on('click',function() {
							$.ajax({
								method: "POST",
								url: "/?r=Main/setThumb",
								data: { action: true }
							})
							.done(function( msg ) {
								thumb_recorded();
								// alert( "Data Saved: " + msg );
							})
							.fail(function( jqXHR, textStatus ) {
								thumb_recorded();
								// alert( "Request failed: " + textStatus );
							})
						});
						$('.thumb-down').on('click',function() {
							$.ajax({
								method: "POST",
								url: "/?r=Main/setThumb",
								data: { action: false }
							})
							.done(function( msg ) {
								thumb_recorded();
								// alert( "Data Saved: " + msg );
							})
							.fail(function( jqXHR, textStatus ) {
								thumb_recorded();
								// alert( "Request failed: " + textStatus );
							})
						});
					});
				</script>

				<div class="competition d-none" style="background-color: #454545;">
					<div style="text-align: center;">
						<div style="color: #fc0; font-size: 20px;">Наша группа Вконтакте</div>
						<div class="text-center align-middle mt-3 mb-3">
							<i class="fab fa-vk" style="color: #5b88bd;"></i>
							<a href="https://vk.com/seasongo" target="_blank" rel="nofollow" style="color: white; font-weight: bold; text-decoration: underline;">https://vk.com/seasongo</a>
						</div>
						<div style="color: red; font-size: 20px;">Внимание!</div>
						<div style="color: #fc0; font-size: 16px;">Розыгрыш подарков среди подписчиков нашей группы вконтакте.</div>
						<div class="text-center mt-3 mb-3">
							<i class="fab fa-vk" style="color: #5b88bd;"></i>
							<a href="https://vk.com/seasongo" target="_blank" rel="nofollow" style="color: white; font-weight: bold; text-decoration: underline;">https://vk.com/seasongo</a>
						</div>
						<div style="font-size: 14px;" class="mb-3">Подпишись на нашу группу и получи возможность выиграть портативную колонку! Результаты конкурса будут подведены <br><b>1 сентября 2019г</b>.</div>
						<img style="width: 200px; height: auto;" data-src="/types/cinema/template/images/competition/jbl_go2_black2.png" class="lazyload">
					</div>
				</div>

			<!-- 	<script type="text/javascript">
					document.addEventListener("DOMContentLoaded", function() {
						VK.Widgets.Group("vk_groups", {mode: 1, width: "250", height: "400"}, 180662647);
					});
				</script> -->

			</div>
			<div class="col-md-8 movie-info">
				<h1 class="mb-3" style="color: rgb(205, 185, 158); font-size: 1.5em;">{{ $headers->h1 }}</h1>
				<p>
					Внимание: вы здесь для того, чтобы бесплатно и без регистрации смотреть онлайн {{ $domainMovie->movie->localed_type }} {{ $domainMovie->movie->title }} {{ $domainMovie->movie->year }} в хорошем HD качестве и на русском языке. Сьемки происходили в {{ $domainMovie->movie->tags->where('type', 'country')->pluck('tag')->implode(', ') }}, талантливым режиссером {{ $domainMovie->movie->tags->where('type', 'director')->pluck('tag')->implode(', ') }} начиная с {{ $domainMovie->movie->year }} года.
				</p>
				<div class="updatedAtBlock mb-2 alert alert-success" style="font-size: 1em; display: none;">Обновление: {{ $domainMovie->movie->last_episode?->season }} сезон {{ $domainMovie->movie->last_episode?->episode }} серия - {{ $domainMovie->movie->last_episode?->air_date->format('d.m.Y') ?? '?' }}</div>
				<p><b>Описание к {{ $domainMovie->movie->title }} {{ empty($domainMovie->movie->title_en) ? '' : '/ ' . $domainMovie->movie->title_en }} ({{ $domainMovie->movie->year }}) HD:</b> {{ nl2br($domainMovie->description ?? $domainMovie->movie->description) }} <strong>{{ $domainMovie->movie->localed_type }} {{ $domainMovie->movie->title }} онлайн бесплатно.</strong></p>
				<div class="row">
					<div class="col-md-6">
						<p>
							<b>Год выхода:</b>
							<a href="/search?year={{ $domainMovie->movie->year }}" title="Показать все фильмы и сериалы {{ $domainMovie->movie->year }} года">{{ $domainMovie->movie->year }}</a>
						</p>
						<div class="mb-3"><b>Страна:</b> <ul class="movie-tags">@foreach ($domainMovie->movie->tags->where('active', true)->where('type', 'country') as $tag)<li><a href="/search?tag={{ $tag->id }}&amp;index" title="Сериалы из {{ $tag->tag }}">{{ $tag->tag }}</a> {{ $tag->emoji }}</li>
							@endforeach
						</ul></div>
						<p class="movieTitleEn" style="display: none;"><b>Оригинальное название:</b> {{ $domainMovie->movie->title_en }}</p>
						<div class="mb-3"><b>Жанр:</b> <ul class="movie-tags">@foreach ($domainMovie->movie->tags->where('active', true)->where('type', 'genre') as $tag)<li><a href="{{ $domain->cinemaDomain->megaweb->getGenreLink($tag->megaweb) }}" title="Смотреть все &laquo;{{ $tag->tag }}&raquo;">{{ $tag->tag }}</a> {{ $tag->emoji }}</li>
							@endforeach
						</ul></div>
					</div>
					<div class="col-md-6">
						<p class="movieStatusStr"><b>Статус сериала:</b> <span class="movieStatus">{{ $domainMovie->movie->localed_status }}</span></p>
						<p class="movieStartedStr"><b>Дата выхода:</b> <span class="movieStarted">{{ $domainMovie->movie->started?->format('d.m.Y') }}</span></p>
						<p class="movieEndedStr"><b>Дата окончания:</b> <span class="movieEnded">{{ $domainMovie->movie->ended?->format('d.m.Y') }}</span></p>
						<script>
							document.addEventListener("DOMContentLoaded", function() {
								movieType = '{{ $domainMovie->movie->type }}';
								if ($('.movieStatus').is(':empty')) {
									$('.movieStatusStr').hide();
								}
								if ($('.movieStarted').is(':empty')) {
									$('.movieStartedStr').hide();
								}
								if ($('.movieEnded').is(':empty')) {
									$('.movieEndedStr').hide();
								}
								// alert(movieType);
								if (movieType == 'serial') {
									$('.updatedAtBlock').show();
								}
							});
						</script>
					</div>
				</div>
				<div class="mb-3"><b>В категориях:</b> <ul class="movie-tags">@foreach ($domainMovie->groups->where('active', true) as $group)<li><a href="{{ $domain->cinemaDomain->megaweb->getGroupLink($group->megaweb) }}" title="Сериалы в категории &laquo;{{ $group->name }}&raquo;">{{ $group->name }}</a></li>
						@endforeach
				</ul></div>
				<div class="mb-3"><b>Перевод:</b> <ul class="movie-tags">@foreach ($domainMovie->groups->where('type', 'translator') as $group)<li><a href="{{ $domain->cinemaDomain->megaweb->getGroupLink($group->megaweb) }}" title="Сериалы в категории &laquo;{{ $group->name }}&raquo;">{{ $group->name }}</a></li>
						@endforeach
				</ul></div>
				<div class="mb-3"><b>В ролях:</b> <ul class="movie-tags">@foreach ($domainMovie->movie->tags->where('type','actor') as $tag)<li><a href="/search?tag={{ $tag->id }}" title="Смотреть сериалы с &laquo;{{ $tag->tag }}&raquo;" rel="nofollow">{{ $tag->tag }}</a></li>
						@endforeach
				</ul></div>
				<div class="row">
					<div class="col-6">
						<div class="movieDirectors mb-3"><b>Режиссер:</b> <ul class="movie-tags">@foreach ($domainMovie->movie->tags->where('type','director') as $tag)<li><a href="/search?tag={{ $tag->id }}" title="Смотреть сериалы &laquo;{{ $tag->tag }}&raquo;">{{ $tag->tag }}</a> {{ $tag->emoji }}</li>
								@endforeach
						</ul></div>
						<div class="movieStudios mb-3"><b>Студия:</b> <ul class="movie-tags">@foreach ($domainMovie->movie->tags->where('type','studio') as $tag)
								<li><a href="/search?tag={{ $tag->id }}" title="Смотреть сериалы &laquo;{{ $tag->tag }}&raquo;">{{ $tag->tag }}</a> {{ $tag->emoji }}</li>
								@endforeach
						</ul></div>
					</div>
					<div class="col-6">
						<p><b>Рейтинг IMDb:</b> {{ $domainMovie->movie->imdb_rating ?? 0 }} из 10</p>
						<p><b>Рейтинг Кинопоиска:</b> {{ $domainMovie->movie->kinopoisk_rating ?? 0 }} из 10</p>
					</div>
				</div>
				@if ($domainMovie->movie->type == 'movie')
				<button class="btn" id="showScreens" style="margin-bottom: 10px;">Скриншоты (внимание спойлеры)</button>
				<img id="screens" src="" alt="Скриншоты {{ $domainMovie->movie->localed_type }} &laquo;{{ $domainMovie->movie->title }}&raquo;" style="width: 100%; height: auto;">
				<script>
					document.addEventListener("DOMContentLoaded", function() {
						// есть проблема, если дважды нажать на старт скриншотов, то картинки начинают сменяться быстро, т.к. идет параллельно 2 потока - старый и новый и картинки перемешиваются. и так далее будут добавляться новые потоки.
						movieId = {{ $domainMovie->movie->id }};
						arr_thumbnails = false;
						$('#screens').hide();
						// thumbnails = '';
						on = false;
						$('#showScreens').on('click', function(){
							if (on) {
								on = false;
								$('#screens').fadeOut();
								$('#screens').hide();
							} else {
								if (!arr_thumbnails) {
									$.get( "/api/getThumbnails?id=" + movieId, function( data ) {
										arr_thumbnails = JSON.parse(data);
										// alert(arr);
										runSreenshots(arr_thumbnails);
										// alert( "Load was performed." );
									});
								} else {
									runSreenshots(arr_thumbnails);
								}

							}
						});
					});

					function runSreenshots(arr = null) {
						if (arr.length === 0) {
							alert('Для этого кино постеров нет');
							return false;
						}
						on = true;
						var time = 0;
						$('#screens').fadeIn();
						jQuery.each(arr,function(index,value) {
							setTimeout(function() {
								if (on) {
									// $('#screens').fadeOut();
									// $('#screens').fadeIn();
									$('#screens').attr('src',value);
								}
							}, time);
							time += 500;
						});
					}
				</script>
				@endif

				@if ($domainMovie->articles->count())
				<p>Статьи про <strong>{{ $domainMovie->movie->localed_type }} {{ $domainMovie->movie->title }}</strong> на нашем сайте:</p>
				<ul>
					@foreach ($domainMovie->articles as $article)
					<li><a href="/article/{{ $article->id }}">{{ $article->title }}</a></li>
					@endforeach
				</ul>
				@endif
				<!-- <div class="episodes episodesList mb-3">
					<div class="collapse" id="episodes">
						<b>Список доступных серий:</b>
						<ul style="list-style-type: none;">
							
						</ul>
					</div>
					<a role="button" class="show-more collapsed" data-toggle="collapse" href="#episodes" data-toggle="collapse"></a>
				</div> -->
				<div class="movieEpisodesDataList episodes">
					<h5>Дата выхода новых серий:</h5>
					<div class="collapse" id="episodesDataList">
						@foreach ($domainMovie->episodes()->orderByDesc('season')->orderByDesc('episode')->get() as $episode)
						<div class="row no-gutters mb-2">
							<div class="col-6 col-sm-3 col-md-3">
								@if ($domain->cinemaDomain->episodically)
								<a href="{{ $domain->toRoute->episodeView($domainMovie, $episode) }}">{{ $episode->season }} сезон {{ $episode->episode }} серия</a>
								@else
								{{ $episode->season }} сезон {{ $episode->episode }} серия
								@endif
							</div>
							<div class="col-6 col-sm-4 col-md-4">
								<b>{{ $episode->title }}</b>
							</div>
							<div class="col-8 col-sm-3 col-md-3">
								{{ $episode->air_date->translatedFormat('j F Y') }} г.
							</div>
							<div class="col-4 col-sm-2 col-md-2 daysToRelease" data-days-to-release="{{ - ($episode->air_date->diffInDays(now(), false) ?? 0) + 1 }}">
							</div>
						</div>@endforeach
					</div>
					<a role="button" class="show-more collapsed" data-toggle="collapse" href="#episodesDataList" data-toggle="collapse"></a>
				</div>
				<script>
					document.addEventListener("DOMContentLoaded", function() {
						$('.daysToRelease').each(function() {
							if ($(this).data('days-to-release') <= 0) {
								$(this).html('<i class="fas fa-check" style="color: green;"></i>');
							} else {
								$(this).html('<i class="fas fa-clock" style="color: orange;"></i> '+$(this).data('days-to-release')+' дней');
							}
						});
						// если есть график выхода новых серий, то скрываем блок "Список доступных серий"
						// alert($('#episodesDataList').is(':empty'));
						$('#episodesDataList').html($.trim($('#episodesDataList').html()));
						// alert($('#episodesDataList').is(':empty'));
						if ($('#episodesDataList').is(':empty')) {
							$('.movieEpisodesDataList').hide();
						} else {
							$('.episodesList').hide();
						}
						// если нет графика выхода новых серий, то скрываем этот блок
						$('#episodesDataList').html($.trim($('#episodesDataList').html()));
						if ($('#episodesDataList').is(':empty')) {
							$('.movieEpisodesDataList').hide();
						}
					});
				</script>
			</div>
		</div>
	</div>
</div>
<div class="row playerBlock" style="margin-bottom: 20px;;">
	<!-- <div class="col-md-12"> -->
		<div class="col-12 col-lg-9">
			<div class="card">
				<div class="card-header">
					<h2 style="color: rgb(237, 214, 183); font-size: 1.6em;">{{ $headers->h2 }}</h2>
				</div>

				<livewire:types.cinema.players template="livewire.types.cinema.templates.seasongo.players" :domainMovieId="$domainMovie->id" lazy  />
				
			</div>
			<x-types.cinema.promo.under-player />

		</div>
	<div class="col-12 col-lg-3">
		<x-types.cinema.promo.player-right />
	</div>
</div>

<div class="card mb-3">
	<div class="card-body">
		<div class="mb-3">
			@foreach($domainMovie->comments()->orderBy('date')->get() as $comment)
			<div class="card text-white bg-secondary mb-2">
				<div class="card-body">
					<h5 class="card-title">🗣 {{ $comment->username }}</h5>
					<p class="card-text">{!! nl2br(e($comment->text)) !!}</p>
					<div class="small" style="color: #aaa;">🗓 {{ $comment->date->translatedFormat('j F Y г. в H:i') }}</div>
				</div>
			</div>@endforeach

		</div>
		<div class="mb-2">
			<button data-toggle="collapse" data-target="#commentsList" class="btn">💬 Написать комментарий</button>
		</div>
		<div class="collapse" id="commentsList">
			<form id="form-comment" method="post" action="/api/addComment">
				<div class="form-group">
					<label>Имя:</label>
					<input style="margin-bottom: 10px;" type="text" name="name" size="30" class="form-control">
				</div>
				<div class="form-group">
					<label>Сообщение:</label>
					<textarea style="margin-bottom: 10px;" name="message" cols="40" rows="4" class="form-control"></textarea>
				</div>
				<input id="g-recaptcha-response" type="hidden" name="g-recaptcha-response" value="">
				<input type="hidden" name="domain_movie_id" value="{{ $domainMovie->id }}">				
				@csrf

				<button	class="btn" id="form-comment-button">
					Отправить
				</button>
			</form>
		</div>
	</div>
</div>

<div class="mb-4 d-sm-none d-none d-md-block">
	В наше время появляется все больше качественных телесериалов в HD 720 и 1080 и многосерийных фильмов. Мы знаем про киного, fanserials и seasonvar, но мы намного лучше, у нашего онлайн кинотеатра вы можете <strong>смотреть онлайн {{ $domainMovie->movie->title }}</strong> на мобильном под управлением Андроид, Android, IOS, IPAD и IPHONE. Вы обязательно сможете скачать в ХД качестве любое кино. А также доступен mp4 формат. Без рекламы не бывает, но у нас ее почти нет.
</div>
<!-- <div class="card"> -->
	<!-- <div class="card-header"> -->
		<h3 style="font-size: 20px;">Вам может понравиться</h3>
	<!-- </div> -->
	<!-- <div class="card-body"> -->
		<!-- <div class="row"> -->
			<div class="sect">
				<div class="sect-cont sect-items clearfix" id="movies">

				</div>
			</div>
			<div class="text-center">
				<button class="btn btn-success mb-3 btnShowMoreMovies">Показать еще</button>
			</div>
		<!-- </div> -->
	<!-- </div> -->
<!-- </div> -->
<script>
	var page = 1;
	document.addEventListener("DOMContentLoaded", function() {
		moviesListUrl = '/show-more/search?sameMovieId={{ $domainMovie->id }}&order=topRated&p=';
		loadContent($('#movies'),moviesListUrl + page);
		$('.btnShowMoreMovies').on('click',function() {
			page = page + 1;
			loadContent($('#movies'), moviesListUrl + page);
		});
	});
</script>
<script src="/types/cinema/template/js/watch.js?{{ $engineConfig->reload }}"></script>
<script defer src="//yastatic.net/share2/share.js" charset="utf-8"></script>
<!-- <script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script> -->

<script>
	document.getElementById('form-comment-button').onclick = function(e) {
		e.preventDefault();
		// load recaptcha script
		script = document.createElement("script");
		script.src = 'https://www.google.com/recaptcha/api.js?render={{ $requestedDomain->parent_domain_or_self->recaptchaKey?->public }}';
		// script.onreadystatechange = callback;
		script.onload = function(){
			grecaptcha.ready(function() {
				grecaptcha.execute('{{ $requestedDomain->parent_domain_or_self->recaptchaKey?->public }}', {action: 'submit'}).then(function(token) {
					document.getElementById('g-recaptcha-response').value = token;
					// alert(grecaptcha.getResponse());
					document.getElementById('form-comment').submit();
				});
			});
		};
		document.body.appendChild(script);
	}
</script>
</x-types.cinema.templates.seasongo.layout>