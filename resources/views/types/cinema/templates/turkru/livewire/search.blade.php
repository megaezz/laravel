<div class="search-component" id="search-component">
  <form autocomplete="off" method="get" action="/search/">
    <input type="text" name="query" placeholder="введите название">
    <button type="submit" title="Поиск">
      <i class="icon-search1"></i>
      <span>Поиск</span>
    </button>
  </form>

  <div data-v-app="">
    <div class="search-module-content">
      <div class="search-module-content__backdrop"></div>
      <div class="search-module-content__container">
        <div class="search-module-content__place with-serials" data-scrollbar="" data-overlayscrollbars="host">
          <div class="os-size-observer os-size-observer-appear">
            <div class="os-size-observer-listener ltr"></div>
          </div>
          <div class="os-viewport os-viewport-scrollbar-hidden" style="margin-right: -63px; margin-bottom: -70px; margin-left: 0px; top: -50px; right: auto; left: -18px; width: calc(100% + 63px); padding: 50px 45px 20px 18px; overflow-y: scroll;">
            <div>
              <a href="/vse-prekrasno-s-toboy/" title="Все прекрасно с тобой" class="search-serial search-serial__fast">
                <div class="search-serial__name__wrapper">
                  <div class="search-serial__name">Все прекрасно с тобой</div>
                  <span class="bookmarks-button"></span>
                  <!--v-if-->
                </div>
                <div class="search-serial__alt-name"></div>
                <div class="search-serial__content-wrapper">
                  <div class="search-serial__left">
                    <img alt="Все прекрасно с тобой" src="/images/NDU3ODh8fHx8MQ/vse-prekrasno-s-toboy.jpg" srcset="/images/NDU3ODh8fDM1MHw1MDZ8MQ/vse-prekrasno-s-toboy.jpg 1x, /images/NDU3ODh8fDcwMHwxMDEyfDE/vse-prekrasno-s-toboy.jpg 2x" class="lza">
                    <!--v-if-->
                  </div>
                  <div class="search-serial__right">
                    <div class="search-serial__pane">
                      <div class="search-serial__rate">
                        <div class="rate-component">   
                          <div class="rate-element long" data-tippy-placement="right" data-tippy-content="558 оценок"> 3.6 </div>  
                        </div> 
                      </div>
                      <div class="search-serial__year">2018</div>
                      <div class="search-serial__genre"><span>Мелодрама, Комедия</span></div>
                    </div>
                    <div class="search-serial__description">«Все прекрасно с тобой» это адаптация американского фильма «Свадьба лучшего друга». Эмре с Дениз дружат уже много лет, они всегда были вместе и делились между собой всеми тайнами. Однажды Эмре решает жениться, он встречается с девушкой по имени Мелиса.</div>
                    <div class="search-serial__bottom">
                      <button data-trailer="13585" href="" class="t-btn t-btn-2" title="Трейлер">
                        <span><i class="icon-categoria_about_serial"></i> <span>Трейлер</span></span>
                      </button>
                      <a disabled="false" href="/vse-prekrasno-s-toboy/" class="t-btn t-btn-2" title="Смотреть">
                        <span><i class="icon-categoria_trailer"></i><span class="show__desktop">Смотреть</span><span class="show__mobile">Смотреть</span></span>
                      </a>
                    </div>
                  </div>
                </div>
              </a>
              <a href="/chastnyy-urok/" title="Частный урок" class="search-serial search-serial__fast">
                <div class="search-serial__name__wrapper">
                  <div class="search-serial__name">Частный урок</div>
                  <span class="bookmarks-button"></span>
                  <!--v-if-->
                </div>
                <div class="search-serial__alt-name"></div>
                <div class="search-serial__content-wrapper">
                  <div class="search-serial__left">
                    <img alt="Частный урок" src="/images/NDk4OTN8fHx8MQ/chastnyy-urok.jpg" srcset="/images/NDk4OTN8fDM1MHw1MDZ8MQ/chastnyy-urok.jpg 1x, /images/NDk4OTN8fDcwMHwxMDEyfDE/chastnyy-urok.jpg 2x" class="lza">
                    <!--v-if-->
                  </div>
                  <div class="search-serial__right">
                    <div class="search-serial__pane">
                      <div class="search-serial__rate">
                        <div class="rate-component">   
                          <div class="rate-element long" data-tippy-placement="right" data-tippy-content="1381 оценок"> 4.5 </div>  
                        </div> 
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>