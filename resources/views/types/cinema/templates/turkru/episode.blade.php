<x-types.cinema.templates.turkru.layout :headers="$headers" :movie="$movie" :episode="$episode" :domain="$domain">

    <x-slot name="html_prefix">
        og: http://ogp.me/ns# video: http://ogp.me/ns/video#
    </x-slot>

    <x-slot name="head">
        <link rel="canonical" href="{{ $requestedDomain->engineDomain->toRoute->absolute()->episodeView($episode) }}"/>
        <meta property="og:image" content="{{ $movie->movie->poster_or_placeholder }}"/>
        <meta http-equiv="Last-Modified" content="{{ $movie->last_modified->isoFormat('ddd, DD MMM YYYY HH:mm:ss \G\M\T') }}"/>
        <meta property="image_src" content="{{ $movie->movie->poster_or_placeholder }}"/>
        <meta name="description" content="{{ $movie->movie->title_ru }} {{ $episode->season_episode }} в русской озвучке или с субтитрами. Заходи на {{ $domain->engineDomain->site_name }} смотреть турецкий сериал «{{ $movie->movie->title_ru }}» на русском языке!"/>
        <meta name="keywords" content="{{ $movie->movie->title_ru }} {{ $episode->season_episode }} русская озвучка смотреть онлайн турецкий сериал на русском языке {{ $episode->videodb?->dubbings->pluck('dubbing_name')->implode(' ') }}"/>
        <meta property="og:title" content="«{{ $movie->movie->title_ru }}» {{ $episode->season_episode }} на русском в FullHD!"/>
        <meta property="og:description" content="Турецкий сериал «{{ $movie->movie->title_ru }}» {{ $episode->season_episode }} смотреть онлайн на {{ $domain->engineDomain->site_name }} в озвучке: {{ $episode->videodb?->dubbings->pluck('dubbing_name')->implode(', ') }}"/>
        <meta property="og:image:alt" content="{{ $movie->movie->title_ru }} {{ $episode->season_episode }}"/>
        <meta property="og:image:type" content="image/jpeg"/>
    </x-slot>

    <main class="container">
        <div class="page">
            <section class="breadcrumbs-share box-breadcrumbs">
                <ul class="breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ route('index') }}" title="Турецкие сериалы">
                            <span itemprop="name">Турецкие сериалы</span>
                            <meta itemprop="position" content="0">
                        </a>
                    </li>
                    <!-- TODO роут на страницу сериала -->
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ $domain->engineDomain->toRoute->movieView($movie) }}" title="{{ $movie->movie->title_ru }}">
                            <span itemprop="name">{{ $movie->movie->title_ru }}</span>
                            <meta itemprop="position" content="1">
                        </a>
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="{{ $domain->engineDomain->toRoute->episodeView($episode) }}" title="{{ $episode->season_episode }} смотреть онлайн">
                            <span itemprop="name">{{ $episode->season_episode }} смотреть онлайн</span>
                            <meta itemprop="position" content="2">
                        </a>
                    </li>
                </ul>
            </section>
            <div itemscope itemtype="http://schema.org/TVEpisode">
                <div class="episode-page" data-serial-id="{{ $movie->id }}" data-episode-id="{{ $episode->id }}">
                    <meta itemprop="episodeNumber" content="{{ $episode->episode }}"/>
                    <meta itemprop="partOfSeason" content="{{ $episode->season }}"/>
                    <meta itemprop="partOfSeries" content="{{ $episode->episode }}"/>
                    <meta itemprop="inLanguage" content="RU"/>
                    <meta itemprop="dateCreated" content="{{ $movie->add_date->toIso8601String() }}"/>
                    <div class="inner-padding">
                        <div class="media-page-header">
                            <h1 itemprop="name">{{ $movie->movie->title_ru }} {{ $episode->season_episode }} русская озвучка</h1>
                        </div>
                        <div class="player-block" id="player">
                            <div class="player-block__player">
                                <div class="player-block__sounds-container">
                                    <div class="player-block__sounds-wrapper swiper-container">
                                        <div class="player-block__sounds swiper-wrapper">
                                            @foreach ($episode->videodb->dubbings ?? [] as $dubbing)
                                            <!-- TODO ссылка на плеер с картинкой желательно -->
                                            <button class="player-block__sound-button  active" data-sound-hash="#{{ $dubbing->translation }}" data-url="">
                                                <span>{{ $dubbing->dubbing_name }}</span>
                                            </button>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="prev"></div>
                                    <div class="next"></div>
                                </div>
                                <div class="player-block__player-iframe">
                                    <div>
                                        {{ $movie->movie->alloha()->first()->player ?? 'Плеер отсутствует' }}
                                    </div>
                                </div>
                                <div class="serial-series-info">
                                    <!-- TODO голосование -->
                                    <div class="rating-component" data-tippy-placement="top-end" data-tippy-content="Оставьте свою оценку!" data-tippy-flip-on-update="false" data-tippy-init-in-view="episode-82721" style="--rating: {{ $movie->movie->rating_sko ?? 0 }}" data-type="episode" data-id="82721" data-tippy-init-in-view-delay="1200">
                                        <div class="rating-component__rate-wrapper">
                                            <div class="rating-component__rate">
                                                <div class="rating-component__filler"></div>
                                                <div class="rating-component__stars"></div>
                                            </div>
                                        </div>
                                        <div class="rating-component__votes">{{ $movie->movie->kinopoisk_votes }} голосов</div>
                                        <span itemtype="http://schema.org/AggregateRating" itemscope itemprop="aggregateRating" hidden>
                                            <meta itemprop="ratingValue" content="{{ $movie->movie->rating_sko ?? 0 }}"/>
                                            <meta itemprop="bestRating" content="10"/>
                                            <meta itemprop="ratingCount" content="{{ $movie->movie->kinopoisk_votes }}"/>
                                            <meta itemprop="worstRating" content="0"/>
                                            <span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Movie">
                                                <meta itemprop="director" content="{{ $movie->movie->tags->where('type','director')->pluck('tag')->implode(', ') }}">
                                                <meta itemprop="producer" content="{{ $movie->movie->tags->where('type','producer')->pluck('tag')->implode(', ') }}">
                                                <meta itemprop="genre" content="{{ $movie->movie->tags->where('type','genre')->pluck('tag')->implode(', ') }}">
                                                <meta itemprop="actor" content="{{ $movie->movie->tags->where('type','actor')->pluck('tag')->implode(', ') }}">
                                                <meta itemprop="name" content="{{ $movie->movie->title_ru }} {{ $episode->season_episode }}">
                                                <meta itemprop="dateCreated" content="{{ $movie->add_date->toIso8601String() }}">
                                                <link itemprop="image" href="{{ $movie->movie->poster_or_placeholder }}">
                                            </span>
                                        </span>
                                    </div>
                                    <div class="viewed-button" data-trigger-login-form></div>
                                </div>
                                <div class="player-block__bottom-controls">
                                    <div>
                                        @if ($episode->previous)
                                        <a class="circle-control" title="{{ $movie->movie->title_ru }} {{ $episode->previous->season }} сезон {{ $episode->previous->episode }}" href="{{ $domain->engineDomain->toRoute->episodeView($episode->previous) }}">
                                            <div>
                                                <svg class="circle-control__svg-arrow-left">
                                                    <use xlink:href="#ic-chevron"></use>
                                                </svg>
                                            </div>
                                            <span class="show__desktop">предыдущая серия</span>
                                            <span class="show__mobile">пред.</span>
                                        </a>
                                        @endif
                                    </div>
                                    <div>
                                        <!-- TODO роут на все серии -->
                                        <a class="circle-control" href="/letnyaya-pesnya-2/" title="Все серии турецкого сериала {{ $movie->movie->title_ru }}">
                                            <div>
                                                <svg class="circle-control__svg-ellipsis">
                                                    <use xlink:href="#ic-ellipsis"></use>
                                                </svg>
                                            </div>
                                            <span class="show__desktop">все серии</span>
                                        </a>
                                    </div>
                                    <div>
                                        @if ($episode->next)
                                        <a class="circle-control" title="{{ $movie->movie->title_ru }} {{ $episode->next->season }} сезон {{ $episode->next->episode }}" href="{{ $domain->engineDomain->toRoute->episodeView($episode->next) }}">
                                            <span class="show__desktop">следующая серия</span>
                                            <span class="show__mobile">след.</span>
                                            <div>
                                                <svg class="circle-control__svg-arrow-right">
                                                    <use xlink:href="#ic-chevron"></use>
                                                </svg>
                                            </div>
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{-- 
                            <div class="wrapper-day-series" data-scrollbar>
                                <!-- TODO обновления -->
                                <x-types.cinema.templates.turkru.last-3-days-episodes :domain="$domain" />
                                <!-- TODO роут для новинок -->
                                <a href="/new114/" class="btn">
                                    <span class="help">
                                        <span class="name-trailer">Показать всё</span>
                                    </span>
                                </a>
                            </div>
                            --}}
                        </div>
                    </div>
                    <div class="episode-info-block inner-padding ">
                        <h2 class="sub-heading">Описание «{{ $movie->movie->title_ru }}» {{ $episode->season_episode }} на русском</h2>
                        <div class="episode-info-top erow">
                            <div class="primary">
                                <div> Дата выхода: {{ $episode->air_date->isoFormat('D MMMM Y') }}</div>
                            </div>
                            <div class="secondary">
                                <div>Кадры к серии</div>
                            </div>
                        </div>
                        <div class="episode-description-wrapper erow">
                            <div class="primary">
                                <svg xmlns="http://www.w3.org/2000/svg" style="position: absolute; pointer-events: none; visibility: hidden;">
                                    <symbol id="icon-info" viewBox="0 0 1024 1024" height="1024" width="1024">
                                        <path fill="var(--color)" d="M512 0C229.632 0 0 229.632 0 512C0 794.368 229.632 1024 512 1024C794.368 1024 1024 794.368 1024 512C1024 229.632 794.368 0 512 0ZM512 973.568C257.024 973.568 50.432 766.976 50.432 512C50.432 257.024 257.024 50.432 512 50.432C766.976 50.432 973.568 257.024 973.568 512C973.568 766.976 766.976 973.568 512 973.568ZM503.552 320.256C458.752 320.256 423.936 331.264 399.36 353.536C374.528 375.808 361.984 407.04 361.728 446.976H463.104C463.616 431.872 467.456 420.096 474.88 411.392C482.304 402.688 492.032 398.336 504.064 398.336C529.92 398.336 542.976 413.44 542.976 443.648C542.976 455.936 539.136 467.456 531.456 477.696C523.776 487.936 512.512 499.456 497.664 511.744C482.816 524.032 472.064 538.88 465.664 555.52C459.008 572.416 455.68 595.456 455.68 624.64H541.44C541.952 609.536 544 596.992 547.84 587.008C551.68 577.024 558.336 567.552 568.32 558.08L602.88 526.08C617.472 512 627.968 497.92 634.624 484.352C641.28 470.784 644.608 455.68 644.608 439.04C644.608 401.152 632.32 371.712 608 351.232C582.656 330.752 548.096 320.256 503.552 320.256V320.256ZM499.2 665.088C483.328 665.088 469.76 669.952 459.264 679.424C448.512 688.896 443.136 700.928 443.136 715.776C443.136 730.624 448.512 742.656 459.264 752.128C470.016 761.6 483.328 766.464 499.2 766.464C515.072 766.464 528.384 761.6 539.136 752.128C549.888 742.656 555.264 730.368 555.264 715.776C555.264 700.928 549.888 688.896 539.136 679.424C528.384 669.696 515.072 665.088 499.2 665.088Z"/>
                                    </symbol>
                                    <linearGradient id="gradient-orange-red-to-right" x1="0" y1="512" x2="1024" y2="512" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F58220"/>
                                        <stop offset="1" stop-color="#EE3D3C"/>
                                    </linearGradient>
                                </svg>
                                <!-- TODO рейтинг какой-то хз откуда брать и нахуя -->
                                {{-- <x-types.cinema.templates.turkru.user-rating/> --}}
                                <div class="description">
                                    <div class="description-spoiler" data-spoiler itemprop="description">{{ $movie->movie->description }}</div>
                                </div>
                            </div>
                            <!-- TODO кадры -->
                            {{-- <x-types.cinema.templates.turkru.episode-gallery :episode="$episode"/> --}}
                        </div>
                    </div>
                    <!-- TODO комменты -->
                    {{-- <x-types.cinema.templates.turkru.comments/> --}}
                    <div itemscope itemtype="http://schema.org/ImageObject">
                        <meta itemprop="name" content="Летняя песня 3 серия">
                        <link itemprop="contentUrl" href="https://turkru.tv/bitrix_storage/og/e/letnyaya-pesnya-3-seriya.jpg?v=2"/>
                        <meta itemprop="description" content="Летняя песня 3 серия русская озвучка"/>
                    </div>
                </div>
            </div>
        </div>
        <!-- TODO шаринг в социальные сети -->
        <div id="social-share" class="social-share-wrapper" data-url="https://turkru.tv/letnyaya-pesnya-3-seriya.html">
            <div class="social-share-items">
                <i class="icon-whatsapp social-share-item circle" data-url="//api.whatsapp.com/send?text=" style="--color: var(--color-whatsapp)"></i>
                <i class="social-share-item circle icon-viber" data-url="viber://forward?text=" style="--color: var(--color-viber)"></i>
                <i class="social-share-item circle icon-odnoklassniki" data-url="//connect.ok.ru/offer?url=" style="--color: var(--color-ok)"></i>
                <i class="social-share-item circle icon-telegram-plane-brands" data-url="//t.me/share/url?url=" style="--color: var(--color-tg)"></i>
                <i class="social-share-item circle icon-vk" data-url="//vk.com/share.php?url=" style="--color: var(--color-vk)"></i>
            </div>
            <div class="social-share-toggle circle">
                <svg width="27" height="23" viewBox="0 0 27 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M26.5822 10.3172C26.8047 10.5172 26.8031 10.8662 26.5788 11.0642C24.9114 12.5358 18.7358 17.991 16.1071 20.3768C15.783 20.6709 15.2609 20.4376 15.2609 20V15.3463C15.2609 15.0702 15.0326 14.8456 14.7566 14.8539C7.7925 15.0641 3.51342 19.5979 1.46291 22.5331C1.1418 22.9927 0.343769 22.7184 0.465071 22.171C1.52032 17.4086 4.83829 8.16368 14.8154 6.69111C15.0654 6.6542 15.2609 6.44111 15.2609 6.18837V1.18325C15.2609 0.741218 15.7935 0.509006 16.1166 0.810666C18.7771 3.29465 24.9253 8.82771 26.5822 10.3172Z"/>
                </svg>
            </div>
        </div>
    </main>
</x-types.cinema.templates.turkru.layout>