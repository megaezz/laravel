<x-types.cinema.templates.turkru.layout :headers="$headers" :movie="$movie" :domain="$domain">

    <x-slot name="html_prefix">
        og: http://ogp.me/ns# video: http://ogp.me/ns/video#
    </x-slot>

    <x-slot name="head">
        <link rel="canonical" href="{{ $requestedDomain->engineDomain->toRoute->movieView($movie) }}"/>
        <meta property="og:image" content="{{ $movie->movie->poster_or_placeholder }}"/>
        <meta http-equiv="Last-Modified" content="{{ $movie->last_modified->isoFormat('ddd, DD MMM YYYY HH:mm:ss \G\M\T') }}"/>
        <meta property="image_src" content="{{ $movie->movie->poster_or_placeholder }}"/>

        <meta name="description" content="Турецкий сериал «{{ $movie->movie->title_ru }}» смотреть онлайн на русском языке или с субтитрами. Все серии «{{ $movie->movie->title_ru }}» в хорошем качестве бесплатно на {{ $domain->engineDomain->site_name }}!"/>
        <meta name="keywords" content="{{ $movie->movie->title_ru }} на русском языке турецкий сериал {{ $movie->movie->title_en }} смотреть онлайн русские субтитры {{ $movie->movie->videodbs()->dubbings()->get()->pluck('dubbing_name')->implode(', ') }} все серии русская озвучка бесплатно в хорошем качестве"/>
        <meta property="og:type" content="video.tv_show"/>
        <meta property="og:title" content="Турецкий сериал «{{ $movie->movie->title_ru }}» {{ $movie->movie->first_episode?->season_episode }} - {{ $movie->movie->last_episode?->season_episode }} смотреть онлайн на русском"/>
        {{-- TODO реализовать вывод озвучек через аттрибут, т.к. повторяются запросы --}}
        <meta property="og:description" content="Все серии «{{ $movie->movie->title_ru }}» в русской озвучке или с субтитрами на {{ $domain->engineDomain->site_name }}: {{ $movie->movie->videodbs()->dubbings()->get()->pluck('dubbing_name')->implode(', ') }}"/>

    </x-slot>

    <main class="container">
        <section class="breadcrumbs-wrapper">
            <ul class="breadcrumbs breadcrumbs-wrapper__base" itemscope="" itemtype="https://schema.org/BreadcrumbList">
                <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="{{ route('index') }}" title="Турецкие сериалы">
                        <span itemprop="name">Турецкие сериалы</span>
                        <meta itemprop="position" content="0">
                    </a>
                </li>
                <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
                    <a itemprop="item" href="{{ $domain->engineDomain->toRoute->movieView($movie) }}" title="{{ $movie->movie->title_ru }} смотреть онлайн">
                        <span itemprop="name">{{ $movie->movie->title_ru }} смотреть онлайн</span>
                        <meta itemprop="position" content="1">
                    </a>
                </li>
            </ul>
            <div class="breadcrumbs-wrapper__badge">{{ $movie->movie->localed_status }}</div>
        </section> 

        <div class="serial-page" itemscope="" itemtype="http://schema.org/TVSeries" data-serial-id="2802">
            <meta itemprop="inLanguage" content="ru">
            <meta itemprop="genre" content="{{ $movie->movie->tags->where('type','genre')->pluck('tag')->implode(', ') }}">
            <div class="inner-padding"> 
                <div class="serial-page-top">
                    <div class="page-title-wrap">
                        <h1 class="page-title" itemprop="name">{{ $movie->movie->title_ru }} турецкий сериал</h1>
                        {{-- TODO закладки --}}
                        {{-- <span class="bookmarks-button" data-id="2802"></span> --}}
                    </div>
                    <div class="top-buttons">
                        <div>
                            <a href="#o-seriale" title="О сериале">о сериале</a>
                        </div>
                        <div>
                            {{-- TODO трейлер --}}
                            <a href="#" data-trailer="2802">трейлер</a>
                        </div>
                        <div>
                            <a href="#actors" title="Актёры">актёры</a>
                        </div>
                        <div>
                            <a href="#frames" title="Кадры">кадры</a>
                        </div>
                    </div>
                </div>
                <div class="serial-series-list">
                    <div class="sorting">
                        <span>все серии:</span>
                        <a href="{{ $domain->engineDomain->toRoute->movieView($movie) }}" class="active">с последней</a>
                        <a href="{{ $domain->engineDomain->toRoute->movieView($movie) }}?order=asc">с первой</a>
                    </div>
                    <ul id="episode_list" class="episode-grid">
                        @foreach ($movie->movie->episodes()->orderByDesc('season')->orderByDesc('episode')->limit(28)->get() as $episode)
                        <li>
                            <div class="short-episode ">
                                <a href="{{ $domain->engineDomain->toRoute->episodeView($episode) }}" title="{{ $movie->movie->title_ru }} {{ $episode->season_episode }}">
                                    <div class="short-episode__image">
                                        <img src="{{ $episode->myshows->image }}" srcset="{{ $episode->myshows->image }} 1x" alt="{{ $movie->movie->title_ru }} {{ $episode->season_episode }}" class="lza">
                                    </div>
                                    <div class="short-episode__description">
                                        <div> {{ $episode->season_episode }} </div>
                                    </div>
                                </a>
                                <div class="short-episode__players">
                                    @foreach ($episode->videodb->dubbings ?? [] as $dubbing)
                                    <a href="{{ $domain->engineDomain->toRoute->episodeView($episode) }}#{{ $dubbing->translation }}" title="{{ $dubbing->dubbing_name }}">{{ $dubbing->dubbing_name }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>


                    <div class="episode-list-pagination after-episode-grid">
                        <div data-count="2" id="more" data-next="2" data-url="/rannyaya-ptashka-68/" class="t-btn-3 t-btn-block t-btn-large t-btn-color-2 t-btn-large-shadow-2" title="Показать все фрагменты">
                            <span>
                                Показать ещё серии
                                <i class="icon-chevron-down"></i>
                            </span>
                        </div>
                        <div id="pagination" class="pagination">
                            <div class="pagination2">
                                <div class="item active">
                                    <span>1</span>
                                </div>
                                <a class="item" href="/rannyaya-ptashka-68/page/2">
                                    <span>2</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="inner-padding" id="o-seriale">
                <div class="cinema-info">
                    <h2 class="sub-heading">Подробнее о турецком сериале «Ранняя пташка»</h2>
                    <div>
                        <div class="poster">
                            <div class="poster__gallery-wrapper">
                                <div class="poster__gallery-place">
                                    <div class="swiper-container gallery swiper-initialized swiper-horizontal swiper-backface-hidden" data-gallery="">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide gallery__item swiper-slide-active" style="width: 347px;">
                                                <img src="/images/MjE1Nzh8fHx8MQ/rannyaya-ptashka21.jpg" srcset="/images/MjE1Nzh8fDM1MHw1MDZ8MQ/rannyaya-ptashka21.jpg 1x, /images/MjE1Nzh8fDcwMHwxMDEyfDE/rannyaya-ptashka21.jpg 2x" itemprop="image" alt="Ранняя пташка постер" class="lza">
                                            </div>
                                            <div class="swiper-slide gallery__item swiper-slide-next" style="width: 347px;">
                                                <img src="/images/Mzl8MjAyMi0wMy0wNCAxNDowMTozNXx8fDE/rannyaya-ptashka21.jpg" srcset="/images/Mzl8MjAyMi0wMy0wNCAxNDowMTozNXwzNTB8NTA2fDE/rannyaya-ptashka21.jpg 1x, /images/Mzl8MjAyMi0wMy0wNCAxNDowMTozNXw3MDB8MTAxMnwx/rannyaya-ptashka21.jpg 2x" itemprop="image" alt="Ранняя пташка постер" class="lza">
                                            </div>
                                            <div class="swiper-slide gallery__item" style="width: 347px;">
                                                <img src="/images/OTk5fDIwMjItMDMtMDQgMTQ6MDE6MzV8fHwx/rannyaya-ptashka21.jpg" data-srcset="/images/OTk5fDIwMjItMDMtMDQgMTQ6MDE6MzV8MzUwfDUwNnwx/rannyaya-ptashka21.jpg 1x, /images/OTk5fDIwMjItMDMtMDQgMTQ6MDE6MzV8NzAwfDEwMTJ8MQ/rannyaya-ptashka21.jpg 2x" srcset="/static/pixel.png 1x" itemprop="image" alt="Ранняя пташка постер" class="lz lza">
                                            </div>
                                        </div>
                                        <div class="counter-component active">
                                            <i class="active"></i>
                                            <i></i><i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="badge red">Завершен</div>
                            <a href="#" class="btn btn-trailer" title="Смотреть трейлер онлайн" data-trailer="2802">
                                <span class="help">
                                    <span class="icon-categoria_trailer"></span>
                                    <span class="text-bg-gradient">Трейлер</span>
                                </span>
                            </a>
                        </div>
                        <div class="serial-characteristics">
                            <h2 class="sub-heading sub-heading__crop_top">Подробнее о турецком сериале «Ранняя пташка»</h2>
                            <div id="chr1" data-scrollbar="" data-scrollbar-min-width="771" data-spoiler="" data-spoiler-max-width="770" data-open-text="Все характеристики" class="char-wrap" data-overlayscrollbars="host">
                                <div class="os-size-observer os-size-observer-appear">
                                    <div class="os-size-observer-listener ltr"></div>
                                </div>
                                <div class="os-viewport os-viewport-scrollbar-hidden" style="margin-right: 0px; margin-bottom: 0px; margin-left: 0px; top: 0px; right: auto; left: 0px; width: calc(100% + 0px); padding: 0px; overflow-y: scroll;">
                                    <div class="characteristics">
                                        <div>
                                            <div class="field-label">Оригинальное:</div>
                                            <div class="field-text" itemprop="alternativeHeadline">Erkenci Kus</div>
                                        </div>
                                        <div>
                                            <div class="field-label">Рейтинги:</div>
                                            <div class="field-text">
                                                <div class="rate-component">
                                                    <div class="rate-element" data-tippy-placement="right" data-tippy-content="30849 оценок"> 7.9 </div>
                                                    <div class="rate-element long" data-tippy-placement="right" data-tippy-content="9349 оценок"> 7.4 </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="field-label">Наш рейтинг:</div>
                                            <div class="field-text">
                                                <div class="rating-component" data-tippy-placement="top-end" data-tippy-content="Оставьте свою оценку!" data-tippy-flip-on-update="false" data-tippy-init-in-view="serial-2802" style="--rating: 9.4" data-type="serial" data-id="2802">
                                                    <div class="rating-component__rate-wrapper">
                                                        <div class="rating-component__rate">
                                                            <div class="rating-component__filler"></div>
                                                            <div class="rating-component__stars"></div>
                                                        </div>
                                                    </div>
                                                    <div class="rating-component__votes">1882 голоса</div>
                                                    <span itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating" hidden="">
                                                        <meta itemprop="ratingValue" content="9.4">
                                                        <meta itemprop="bestRating" content="10">
                                                        <meta itemprop="ratingCount" content="1882">
                                                        <meta itemprop="worstRating" content="1">
                                                        <span itemprop="itemReviewed" itemscope="" itemtype="http://schema.org/Movie">
                                                            <meta itemprop="director" content="Чагры Байрак, Aytac Cicek">
                                                            <meta itemprop="producer" content="Ali Aktürk, Фарук Тургут">
                                                            <meta itemprop="musicBy" content="Cem Öget">
                                                            <meta itemprop="genre" content="Драма, Мелодрама, Комедия">
                                                            <meta itemprop="actor" content="Джан Яман, Демет Оздемир, Башак Гюмюлджинелиолу, Фери Байджу Гюлер, Озлем Токаслан, Аныл Челик, Биранд Тунджа, Джихан Эрджан, Ахмет Сомерс, Севинч Эрбулак, Nuray Serefoglu, Кимья Гёкче Айтач, Айше Акын, Тугче Кумрал, Утку Атеш, Сибель Шишман, Асуман Чакыр, Ипек Тенолджай, Ахмет Олгун Сюнеар, Берат Енильмез, Айшен Гюрлер, Озгюр Озберк, Алие Узунатаган, Аслы Мелиса Узун, Али Яджи, Джерен Ташчы, Гамзе Топуз, Маджит Тастан, Дженгиз Бекташ, Ознур Серчелер, Can Ceylan">
                                                            <meta itemprop="name" content="Ранняя пташка">
                                                            <meta itemprop="dateCreated" content="2018-06-26T18:50:16">
                                                            <link itemprop="image" href="/bitrix_storage/images/0/66/66884.jpg">
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="field-label">Год:</div>
                                            <div class="field-text">2018 - 2019</div>
                                            <meta itemprop="dateCreated" content=" 2018">
                                        </div>
                                        <div>
                                            <div class="field-label">Серий:</div>
                                            <div class="field-text" itemprop="numberOfEpisodes">51</div>
                                        </div>
                                        <div>
                                            <div class="field-label">Жанр:</div>
                                            <div class="field-text" itemprop="genre">Драма, Мелодрама, Комедия</div>
                                        </div>   
                                        <div> 
                                            <div class="field-label">Озвучка:</div> 
                                            <div class="field-text">SesDizi, Ирина Котова, Моб телевидение, Субтитры DiziMania, Субтитры TurkSinema</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Страна:</div> 
                                            <div class="field-text" itemprop="countryOfOrigin">Турция</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Телеканал:</div> 
                                            <div class="field-text" itemprop="productionCompany">Star TV</div> 
                                        </div>    
                                        <div> 
                                            <div class="field-label">Актёры:</div> 
                                            <div class="field-text" itemprop="actors">   
                                                <a href="/actors/djan_yaman/">Джан Яман, </a>    
                                                <a href="/actors/demet_ozdemir/">Демет Оздемир, </a>    
                                                <a href="/actors/bashak_gyumyuldjineliolu/">Башак Гюмюлджинелиолу, </a>    
                                                <a href="/actors/feri_baydju_gyuler/">Фери Байджу Гюлер, </a>    
                                                <a href="/actors/ozlem_tokaslan/">Озлем Токаслан, </a>    
                                                <a href="/actors/anyl_chelik/">Аныл Челик, </a>    
                                                <a href="/actors/birand_tundja/">Биранд Тунджа, </a>    <a href="/actors/djihan_erdjan/">Джихан Эрджан, </a>    <a href="/actors/ahmet_somers/">Ахмет Сомерс, </a>    <a href="/actors/sevinch_erbulak/">Севинч Эрбулак, </a>    <a href="/actors/nuray_serefoglu/">Nuray Serefoglu, </a>    <a href="/actors/kimya_gekche_aytach/">Кимья Гёкче Айтач, </a>    <a href="/actors/ayshe_akyn/">Айше Акын, </a>    <a href="/actors/tugche_kumral/">Тугче Кумрал, </a>    <a href="/actors/utku_atesh/">Утку Атеш, </a>    <a href="/actors/sibel_shishman/">Сибель Шишман, </a>    <a href="/actors/asuman_chakyr/">Асуман Чакыр, </a>    <a href="/actors/ipek_tenoldjay/">Ипек Тенолджай, </a>    <a href="/actors/ahmet_olgun_syunear/">Ахмет Олгун Сюнеар, </a>    <a href="/actors/berat_enilmez/">Берат Енильмез, </a>    <a href="/actors/ayshen_gyurler/">Айшен Гюрлер, </a>    <a href="/actors/ozgyur_ozberk/">Озгюр Озберк, </a>    <a href="/actors/alie_uzunatagan/">Алие Узунатаган, </a>    <a href="/actors/asly_melisa_uzun/">Аслы Мелиса Узун, </a>    Али Яджи,     Джерен Ташчы,     Гамзе Топуз,     Маджит Тастан,     Дженгиз Бекташ,     Ознур Серчелер,     Can Ceylan   
                                            </div>
                                        </div>   
                                        <div> 
                                            <div class="field-label">Режиссер:</div> 
                                            <div class="field-text" itemprop="director">Чагры Байрак, Aytac Cicek</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Сценарий:</div> 
                                            <div class="field-text">Ешим Чытак, Айсе Кутлу, Бану Зенгин Так</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Продюсер:</div> 
                                            <div class="field-text" itemprop="producer">Ali Aktürk, Фарук Тургут</div> 
                                        </div>
                                        <div> 
                                            <div class="field-label">Оператор:</div> 
                                            <div class="field-text">Тарык Эркан</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Композитор:</div> 
                                            <div class="field-text" itemprop="musicBy">Cem Öget</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Художник:</div> 
                                            <div class="field-text">Saadet Gizem Sahin, Erdem Özçelik, Ipek Kiziltan</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Монтаж:</div> 
                                            <div class="field-text">Умут Бёлюкоглу</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Премьера:</div> 
                                            <div class="field-text">26 июня 2018</div> 
                                        </div>   
                                        <div> 
                                            <div class="field-label">Длительность:</div> 
                                            <div class="field-text">120 мин</div> 
                                        </div>  
                                    </div> 
                                </div>
                                <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-handle-interactive os-scrollbar-cornerless os-scrollbar-unusable os-theme-light">
                                    <div class="os-scrollbar-track">
                                        <div class="os-scrollbar-handle" style="width: 100%;"></div>
                                    </div>
                                </div>
                                <div class="os-scrollbar os-scrollbar-vertical os-scrollbar-handle-interactive os-scrollbar-visible os-scrollbar-cornerless os-theme-light">
                                    <div class="os-scrollbar-track">
                                        <div class="os-scrollbar-handle" style="height: 49.318%; transform: translateY(0%);"></div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                    </div> 
                </div>
            </div> 

            <div class="s-place" data-name="serial_actors" data-id="2802" data-v-app="">
                <div id="actors" class="base-block-top-offset">
                    <h3 class="sub-heading sub-heading__bottom_offset inner-padding"> 
                        Актеры турецкого сериала 
                        <span class="nowrap">2018 - 2019 г.</span>
                    </h3>
                    <div class="base-items-slider-container actors-slider">
                        <div class="base-items-slider">
                            <div class="swiper swiper-initialized swiper-horizontal">
                                <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
                                    <div class="swiper-slide swiper-slide-active" style="width: 190px;">
                                        <div class="actor-item">
                                            <div class="photo-wrapper">
                                                <div class="photo">
                                                    <img alt="Джан Яман" src="/images/NzU4MXx8fHww/7581.jpg" srcset="/images/NzU4MXx8MTQ3fDE5NHww/7581.jpg 1x, /images/NzU4MXx8Mjk0fDM4OHww/7581.jpg 2x" class="lza">
                                                    <a href="/actors/djan_yaman/" title="Джан Яман"></a>
                                                </div>
                                            </div>
                                            <a href="/actors/djan_yaman/" title="Джан Яман" class="name">
                                                <span>Джан Яман</span>
                                                <div class="rating">9.6</div>
                                            </a>
                                        </div>
                                        <!---->
                                    </div>
                                    <div class="swiper-slide swiper-slide-next" style="width: 190px;">
                                        <div class="actor-item">
                                            <div class="photo-wrapper">
                                                <div class="photo">
                                                    <img alt="Демет Оздемир" src="/images/NzU2OXx8fHww/7569.jpg" srcset="/images/NzU2OXx8MTQ3fDE5NHww/7569.jpg 1x, /images/NzU2OXx8Mjk0fDM4OHww/7569.jpg 2x" class="lza">
                                                    <a href="/actors/demet_ozdemir/" title="Демет Оздемир"></a>
                                                </div>
                                            </div>
                                            <a href="/actors/demet_ozdemir/" title="Демет Оздемир" class="name">
                                                <span>Демет Оздемир</span>
                                                <div class="rating">9.6</div>
                                            </a>
                                        </div>
                                        <!---->
                                    </div>
                                </div>
                                <!----><!----><!---->
                            </div>
                        </div>
                        <!--v-if-->
                        <div class="swiper-button swiper-button-next" style="top: 99px;">
                            <span class="icon-chevron-small-right"></span>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="inner-padding" id="frames">
                <div class="secondary frame-gallery base-block-top-offset">
                    <h3 class="sub-heading sub-heading__bottom_offset">Кадры</h3>
                    <div class="gallery-wrapper showed" id="episode-gallery" data-once="" data-label="Кадры из «Ранняя пташка»">
                        <div class="episode-gallery-overflow-container">
                            <div class="swiper-container swiper-initialized swiper-horizontal">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide swiper-slide-active" style="width: 297.714286px; margin-right: 20px;">
                                        <div class="img-container">
                                            <img src="/bitrix_storage/frames/0/34/34301.jpg" class="" alt="Кадры из «Ранняя пташка»" srcset="/bitrix_storage/thumbs/450/260/frames/0/34/34301.jpg?v=1 1x, /bitrix_storage/thumbs/900/520/frames/0/34/34301.jpg?v=1 2x">
                                        </div>
                                    </div>
                                    <div class="swiper-slide swiper-slide-next" style="width: 297.714286px; margin-right: 20px;">
                                        <div class="img-container">
                                            <img src="/bitrix_storage/frames/0/34/34303.jpg" class="" alt="Кадры из «Ранняя пташка»" srcset="/bitrix_storage/thumbs/450/260/frames/0/34/34303.jpg?v=1 1x, /bitrix_storage/thumbs/900/520/frames/0/34/34303.jpg?v=1 2x">
                                        </div>
                                    </div>
                                    <div class="swiper-slide" style="width: 297.714286px; margin-right: 20px;">
                                        <div class="img-container">
                                            <img src="/bitrix_storage/frames/0/34/34305.jpg" class="" alt="Кадры из «Ранняя пташка»" srcset="/bitrix_storage/thumbs/450/260/frames/0/34/34305.jpg?v=1 1x, /bitrix_storage/thumbs/900/520/frames/0/34/34305.jpg?v=1 2x">
                                        </div>
                                    </div>
                                </div>

                                <div class="swiper-button swiper-button-prev swiper-button-disabled">
                                    <span class="icon-chevron-small-left"></span>
                                </div>
                                <div class="swiper-button swiper-button-next">
                                    <span class="icon-chevron-small-right"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="inner-padding base-block-top-offset">
                <h2 class="sub-heading sub-heading__bottom_offset">Описание «Ранняя пташка» на русском языке</h2>
                <div class="cinema-description-text" itemprop="description">
                    <div class="ttt">
                        <p>Главная героиня турецкого сериала «Ранняя пташка» Санем привыкла просыпаться рано и радоваться новому дню. Она живет с родителями и сестрой, помогает отцу в семейной лавке и мечтает стать писателем. Санем докучает соседский паренек, который уже много лет безуспешно пытается влюбить в себя нашу героиню и неустанно зовет замуж.</p>
                        <p>В сериале «Early bird» привычная жизнь девушки меняется, когда однажды утром старшая сестра Лейла приносит новость — в рекламное агентство, где она работает, требуется новый сотрудник. Узнав об этом, родители уговаривают Санем попробовать устроиться на это место, и девушка уступает. На собеседовании девушка очаровывает владельца агентства Азиза и его сына Эмре — финансового директора, и ее принимает на работу.</p>
                        <p>Как раз в это же время агентство отмечает юбилей, куда все приглашены все сотрудники, в том числе Санем, Лейла и Джан – другой сын Азиза, загадочный фотограф. На празднике девушка заблудилась и нечаянно зашла не в тот зал, из-за чего в темноте ее поцеловал незнакомец. Санем не может забыть этот поцелуй и, кажется, заочно влюбляется в этого человека. Она пытается найти мужчину, которого про себя называет загадочным Альбатросом, не догадываясь, что ее возлюбленный находится намного ближе, чем она предполагает.</p>
                        <p>Смотреть онлайн сериал «Ранняя пташка» можно на нашем сайте в русской озвучке, чтобы узнать, чем закончатся поиски загадочного Альбатроса, как решаться семейные дела Санем и не изменят ли оптимизм и жизнерадостной нашей героине.</p>
                    </div>
                </div>
            </div>

            <div itemscope="" itemtype="http://schema.org/ImageObject">
                <meta itemprop="name" content="Ранняя пташка">
                <link itemprop="contentUrl" href="https://turkru.tv/bitrix_storage/images/0/66/66884.jpg">
                <meta itemprop="description" content="Ранняя пташка турецкий сериал">
            </div>
        
            <div class="s-place" data-name="related_serials" data-id="2802" data-v-app="">
                <div id="related-slider" class="related-serials base-block-top-offset">
                    <h3 class="sub-heading sub-heading__bottom_offset inner-padding">Похожие сериалы</h3>
                    <div class="base-items-slider-container serial-slider slides-5">
                        <div class="base-items-slider">
                            <div class="swiper swiper-initialized swiper-horizontal swiper-backface-hidden">
                                <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
                                    <div class="swiper-slide serial-slide-wrapper swiper-slide-active" style="width: 224px;">
                                        <div class="short-serial-item serial-slide">
                                            <div class="poster-popover-place">
                                                <a href="/povsyudu-ty-vezde-ty-8/" class="poster" title="Турецкий сериал Повсюду ты/Везде ты">
                                                    <div class="swiper swiper-initialized swiper-horizontal swiper-backface-hidden" style="width: 100%; height: 100%; position: absolute; left: 0px; top: 0px;">
                                                        <div class="swiper-wrapper">
                                                            <div class="swiper-slide swiper-slide-active" style="width: 204px; height: 100%;">
                                                                <img alt="Повсюду ты/Везде ты" src="/images/MjE2MTV8fHx8MQ/povsyudu-ty-vezde-ty4.jpg" srcset="/images/MjE2MTV8fDM1MHw1MDZ8MQ/povsyudu-ty-vezde-ty4.jpg 1x" class="lza">
                                                                <!---->
                                                            </div>
                                                            <div class="swiper-slide swiper-slide-next" style="width: 204px; height: 100%;">
                                                                <img alt="Повсюду ты/Везде ты" src="/images/MTk3fDIwMjItMDQtMTggMDU6MTA6MDR8fHwx/povsyudu-ty-vezde-ty4.jpg" srcset="/images/MTk3fDIwMjItMDQtMTggMDU6MTA6MDR8MzUwfDUwNnwx/povsyudu-ty-vezde-ty4.jpg 1x" class="lza">
                                                                <!---->
                                                            </div>
                                                            <div class="swiper-slide" style="width: 204px; height: 100%;">
                                                                <img alt="Повсюду ты/Везде ты" src="/images/MTIzNXwyMDIyLTA0LTE4IDA1OjEwOjA0fHx8MQ/povsyudu-ty-vezde-ty4.jpg" srcset="/static/pixel.png 1x" class="lz lza" data-srcset="/images/MTIzNXwyMDIyLTA0LTE4IDA1OjEwOjA0fDM1MHw1MDZ8MQ/povsyudu-ty-vezde-ty4.jpg 1x">
                                                                <!---->
                                                            </div>
                                                        </div>
                                                        <!----><!----><!---->
                                                    </div>
                                                    <div class="active not-mobile counter-component">
                                                        <i class="active"></i>
                                                        <i class=""></i>
                                                        <i class=""></i>
                                                    </div>
                                                </a>
                                                <div class="popover-data-wrapper">
                                                    <!--v-if-->
                                                    <span class="icon-about_serial popover-button"></span>
                                                </div>
                                                <!--v-if-->
                                            </div>
                                            <span class="bookmarks-button"></span>
                                            <a class="poster-label" href="/povsyudu-ty-vezde-ty-8/" title="Повсюду ты/Везде ты">
                                                <div>
                                                    <span>Повсюду ты/Везде ты</span>
                                                </div>
                                            </a>
                                        </div>
                                        <!---->
                                    </div>
                                </div>
                                <!----><!----><!---->
                            </div>
                        </div>
                        <!--v-if-->
                        <div class="swiper-button swiper-button-next" style="top: 147px;">
                            <span class="icon-chevron-small-right"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="social-share" class="social-share-wrapper" data-url="https://turkru.tv/erkenci-kus/">
            <div class="social-share-items">
                <i class="icon-whatsapp social-share-item circle" data-url="//api.whatsapp.com/send?text=" style="--color: var(--color-whatsapp)"></i>
                <i class="social-share-item circle icon-viber" data-url="viber://forward?text=" style="--color: var(--color-viber)"></i>
                <i class="social-share-item circle icon-odnoklassniki" data-url="//connect.ok.ru/offer?url=" style="--color: var(--color-ok)"></i>
                <i class="social-share-item circle icon-telegram-plane-brands" data-url="//t.me/share/url?url=" style="--color: var(--color-tg)"></i>
                <i class="social-share-item circle icon-vk" data-url="//vk.com/share.php?url=" style="--color: var(--color-vk)"></i>
            </div>
            <div class="social-share-toggle circle">
                <svg width="27" height="23" viewBox="0 0 27 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M26.5822 10.3172C26.8047 10.5172 26.8031 10.8662 26.5788 11.0642C24.9114 12.5358 18.7358 17.991 16.1071 20.3768C15.783 20.6709 15.2609 20.4376 15.2609 20V15.3463C15.2609 15.0702 15.0326 14.8456 14.7566 14.8539C7.7925 15.0641 3.51342 19.5979 1.46291 22.5331C1.1418 22.9927 0.343769 22.7184 0.465071 22.171C1.52032 17.4086 4.83829 8.16368 14.8154 6.69111C15.0654 6.6542 15.2609 6.44111 15.2609 6.18837V1.18325C15.2609 0.741218 15.7935 0.509006 16.1166 0.810666C18.7771 3.29465 24.9253 8.82771 26.5822 10.3172Z"></path> 
                </svg> 
            </div> 
        </div>   
    </main>

</x-types.cinema.templates.turkru.layout>