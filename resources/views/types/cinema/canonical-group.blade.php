@if ($domain->hide_canonical and !\engine\app\models\Engine::isRealBot())
<!-- c -->
@else
<link rel="canonical" href="{{ \engine\app\models\Engine::getCrawlerOrClientDomain()->canonical_route . $groupLink }}">
@endif