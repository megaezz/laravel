<x-types.cinema.dashboard>
	<table class="table table-sm table-striped" style="font-size: 12px;">
		<thead>
			<tr>
				<th>Ссылка</th>
				<th>Результат</th>
			</tr>
		</thead>
		@foreach ($results as $url => $result)
		<tr class="{{ ($result->getStatusCode() == 202) ? 'table-success' : 'table-danger'}}">
			<td>
				<a href="{{ $url }}">
					{{ $url }}
				</a>
			</td>
			<td>
				@if ($result->getStatusCode() == 202)
				ОК ({{ json_decode($result->getBody())->quota_remainder }} осталось)
				@else
				{{ json_decode($result->getBody())->error_message }}
				@endif
			</td>
		</tr>
		@endforeach
	</table>
	<a class="btn btn-primary" href="{{ url()->previous() }}">Назад</a>
</x-types.cinema.dashboard>