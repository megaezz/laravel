<x-types.cinema.dashboard>
	<table class="table table-sm table-striped" style="font-size: 12px;">
		<thead>
			<tr>
				<th>Ссылка</th>
				<th>Домен</th>
				<th>Результат</th>
			</tr>
		</thead>
		@foreach ($routes as $route)
		@if ($route['error'] ?? false)
		<tr class="table-danger">
		@elseif ($route['handler']['deleted'] ?? false)
		<tr class="table-success">
		@elseif ($route['handler']['dmca'] ?? false)
		<tr class="table-primary">
		@elseif ($route['handler']['main_domain'] ?? false)
		<tr class="table-warning">
		@else
		<tr>
		@endif
			<td style="max-width: 500px; overflow-wrap: break-word;">
				<a href="{{ $route['url'] }}">
					{{ $route['url'] }}
				</a>
			</td>
			<td>
				@if ($route['handler']['domain'] ?? null)
				{{ 
					implode(', ', [
						$route['handler']['domain']->primary_domain->access_scheme ?? null,
						$route['handler']['domain']->primary_domain->cinemaDomain->rkn_scheme ?? null
					])
				}}
				@endif
			</td>
			@if ($route['error'] ?? false)
			<td style="min-width: 400px;">{{ $route['error'] }}</td>
			@else
			<td style="min-width: 400px;">{{ $route['handler']['message'] }}</td>
			@endif
		</tr>
		@endforeach
	</table>
	<a class="btn btn-primary" href="{{ url()->previous() }}">Назад</a>
</x-types.cinema.dashboard>