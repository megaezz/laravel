<x-types.engine.admin.dashboard>

	<div class="container">
		<link rel="stylesheet" href="/types/cinema/template/admin/articles/style.css">
		<div class="row">
			<div class="col-12">
				<form class="mb-3" action="{{ route('cinema.articles.create') }}" method="post">
                    @csrf
					<button class="btn btn-success" type="submit">Создать статью</button>
				</form>
				<table class="table articles">
					<tr style="font-weight: bold;">
						<td>Название</td>
						<td>Домен</td>
						<td>Акцептор</td>
						<td>Активирована</td>
					</tr>
					{!! $articlesList !!}
				</table>
				<script>
					document.addEventListener("DOMContentLoaded", function() {
						$('.articleActive[data-active="1"]').addClass('fas fa-check');
						$('.articleActive[data-active="0"]').addClass('far fa-clock');
					});
				</script>
			</div>
		</div>
	</div>

	<script src="/types/cinema/template/admin/customer/js/jquery.min.js"></script>

</x-types.engine.admin.dashboard>
