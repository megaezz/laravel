<x-types.cinema.admin.kinocontent.layout :$user>
    <style>
        .calcWmz {
            margin-left: 20px;
        }
    </style>
    <h4 class="mb-4">Пополнить баланс</h4>
    <div class="row">
        <div class="col-12 col-sm-12 d-none">
            <h5 class="mb-0">Webmoney WMR</h5>
            <div class="small mb-2">без комиссии</div>
            <form action="https://merchant.webmoney.ru/lmi/payment.asp" method="POST" class="form-inline mb-5">
                <div class="input-group mr-3" style="max-width: 160px;">
                    <input type="text" name="LMI_PAYMENT_AMOUNT" placeholder="Сумма" class="form-control" style="width: 100px;">
                    <div class="input-group-prepend">
                        <div class="input-group-text">руб.</div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" value="Оплатить">
                <!-- <input type="hidden" name="LMI_PAYMENT_DESC_BASE64" value="0J/QvtC/0L7Qu9C90LXQvdC40LUg0LHQsNC70LDQvdGB0LAgS2lub0NvbnRlbnQuY2x1Yg=="> -->
                <input type="hidden" name="LMI_PAYEE_PURSE" value="R085008526052">
                <input type="hidden" name="login" value="{{ $user->login }}">
                <input type="hidden" name="LMI_PAYMENT_DESC" value="Deposit KinoContent.club {{ $user->login }}">
            </form>
        </div>
        <div class="col-12 col-sm-12">
            <h5 class="mb-0">Webmoney WMZ</h5>
            <div class="small mb-2">без комиссии, по курсу <span class="usdRubRate"></span></div>
            <form action="https://merchant.webmoney.ru/lmi/payment.asp" method="POST" class="form-inline mb-5">
                <div class="input-group mr-3" style="max-width: 160px;">
                    <input type="text" name="LMI_PAYMENT_AMOUNT" placeholder="Сумма" class="form-control inputWmz" style="width: 100px;">
                    <div class="input-group-prepend">
                        <div class="input-group-text">USD</div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" value="Оплатить">
                <!-- <input type="hidden" name="LMI_PAYMENT_DESC_BASE64" value="0J/QvtC/0L7Qu9C90LXQvdC40LUg0LHQsNC70LDQvdGB0LAgS2lub0NvbnRlbnQuY2x1Yg=="> -->
                <input type="hidden" name="LMI_PAYEE_PURSE" value="Z703935021742">
                <input type="hidden" name="login" value="{{ $user->login }}">
                <input type="hidden" name="LMI_PAYMENT_DESC" value="Deposit KinoContent.club {{ $user->login }}">
                <span class="calcWmz"></span>
                <script>
                    document.addEventListener("DOMContentLoaded", function() {
                        usdRubRate = ({{ $usdRubRate }}).toFixed(2);
                        $('.usdRubRate').html(usdRubRate + ' руб.');
                        $('.inputWmz').on('keyup',function(){
                            $('.calcWmz').html(($(this).val()*usdRubRate).toFixed(2) + ' руб.');
                        });

                        $('.unitpay input[name="sum"]').keyup(function(){
                            $.ajax({
                                method: "POST",
                                url: "/?r=UnitPay/getSignature",
                                data: {
                                    account: $('.unitpay input[name="account"]').val(),
                                    sum: $('.unitpay input[name="sum"]').val(),
                                    desc: $('.unitpay input[name="desc"]').val(),
                                    _token: '{{ csrf_token() }}'
                                }
                            })
                            .done(function( msg ) {
                                $('.unitpay input[name="signature"]').val(msg);
                            });
                        });

                        $('.freekassa input[name="oa"]').keyup(function(){
                            $('.freekassa input[type="submit"]').prop('disabled', true);
                            $.ajax({
                                method: "POST",
                                url: "/freekassa/getSignature",
                                data: {
                                    o: $('.freekassa input[name="o"]').val(),
                                    oa: $('.freekassa input[name="oa"]').val(),
                                    _token: '{{ csrf_token() }}'
                                }
                            })
                            .done(function( msg ) {
                                $('.freekassa input[name="s"]').val(msg);
                                $('.freekassa input[type="submit"]').prop('disabled', false);
                            });
                        });

                        $('.enot input[name="oa"]').keyup(function(){
                            $('.enot input[type="submit"]').prop('disabled', true);
                            $.ajax({
                                method: "POST",
                                url: "/?r=Enot/getSignature",
                                data: {
                                    o: $('.enot input[name="o"]').val(),
                                    oa: $('.enot input[name="oa"]').val(),
                                    _token: '{{ csrf_token() }}'
                                }
                            })
                            .done(function( msg ) {
                                $('.enot input[name="s"]').val(msg);
                                $('.enot input[type="submit"]').prop('disabled', false);
                            });
                        });
                    });
                </script>
            </form>
        </div>
        <div class="col-12 col-sm-12 d-none">
            <h5 class="mb-0">Чем угодно</h5>
            <div class="small mb-2">c комиссией платежного агрегатора</div>
            <form action="https://unitpay.ru/pay/431981-75529" class="form-inline mb-5 unitpay">
                <div class="input-group mr-3" style="max-width: 160px;">
                    <input type="text" name="sum" class="form-control" placeholder="Сумма" style="width: 100px;">
                    <div class="input-group-prepend">
                        <div class="input-group-text">руб.</div>
                    </div>
                </div>
                <input type="hidden" name="signature" value="">
                <input type="hidden" name="account" value="{{ $user->login }}">
                <input type="hidden" name="desc" value="Deposit KinoContent.club {{ $user->login }}">
                <input type="submit" value="Оплатить" class="btn btn-primary">
            </form>
        </div>
        <div class="col-12 col-sm-12">
            <h5 class="mb-0">Freekassa (ЮMoney, VISA, MasterCard и т.д.)</h5>
            <div class="small mb-2">c комиссией платежного агрегатора</div>
            <form method="get" action="https://pay.freekassa.com/" class="form-inline mb-5 freekassa">
                <div class="input-group mr-3" style="max-width: 160px;">
                    <input type="text" name="oa" class="form-control" placeholder="Сумма" style="width: 100px;">
                    <div class="input-group-prepend">
                        <div class="input-group-text">руб.</div>
                    </div>
                </div>
                <input type="hidden" name="lang" value="ru">
                <input type="hidden" name="currency" value="RUB">
                <input type="hidden" name="m" value="2360">
                <input type="hidden" name="s" value="">
                <input type="hidden" name="o" value="{{ $user->login }}">
                <input type="submit" value="Оплатить" class="btn btn-primary">
            </form>
        </div>
        <div class="col-12 col-sm-12 d-none">
            <h5 class="mb-0">Карты, USDT, Perfect Money</h5>
            <div class="small mb-2">c комиссией платежного агрегатора (4.5% - 7%)</div>
            <form method="get" action="https://enot.io/pay" class="form-inline mb-5 enot">
                <div class="input-group mr-3" style="max-width: 160px;">
                    <input type="text" name="oa" class="form-control" placeholder="Сумма" style="width: 100px;">
                    <div class="input-group-prepend">
                        <div class="input-group-text">руб.</div>
                    </div>
                </div>
                <input type="hidden" name="m" value="50929">
                <input type="hidden" name="s" value="">
                <input type="hidden" name="o" value="{{ $enotUserId }}">
                <input type="submit" value="Оплатить" class="btn btn-primary">
            </form>
        </div>
        <div class="col-12 col-sm-12">
            <h3>Пополнения</h3>
            <table class="table">
                <tr style="font-weight: bold;">
                    <td>Дата</td>
                    <td>Сумма</td>
                    <td>Комментарий</td>
                </tr>
                @foreach ($user->transfers()->orderByDesc('date')->get() as $transfer)
                <tr>
                    <td>{{ $transfer->date->translatedFormat('j F Y H:i') }}</td>
                    <td>{{ $transfer->sum }}</td>
                    <td>{{ $transfer->comment }}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</x-types.cinema.admin.kinocontent.layout>