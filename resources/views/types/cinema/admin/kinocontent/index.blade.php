<x-types.cinema.admin.kinocontent.layout :$user page="index">
	<div class="row mb-3">
		<div class="col-12">
			<div class="row">
				<div class="col-12 col-md-6">
					<p>
						Ваш статус:
						@if ($user->megaweb->getCustomerIsEnoughBalance())
						<span style="color: green;">активен</span>
						@else
						<span style="color: red;">выполнение заданий приостановлено, <a href="/deposit">пополните баланс</a></span>
						@endif
						<br>
						<span title="Влияет на ожидание заказа. При низкой загруженности заказ выполнится в течение дня." style="opacity: 0.7;">
							Загруженность сервиса:
							@if ($config->service_load > 90)
							<span style="color: red;">Высокая</span>
							@elseif ($config->service_load > 40)
							<span style="color: orange;">Средняя</span>
							@else
							<span style="color: green;">Низкая</span>
							@endif
						</span>
						<br>
					</p>
				</div>
				<div class="col-12 col-md-6">
					<div class="alert alert-secondary">
						<p><a onclick="this.href='https://aeza.net/?ref=362541'" href="https://aeza.net">Aeza</a> &mdash; лучший хостинг под киносайты в 2024 г.</p>
						<p>Стоимость <code>1k</code> символов &mdash; <code>{{ $customerPricePer1k }} руб.</code></p>
					</div>
				</div>
			</div>
			<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#howTo" style="padding: 0;">Как сделать заказ?</button>
			<div class="collapse" id="howTo">
				<div class="card card-body">
					<h4>Описания к фильмам:</h4>
					<ol>
						<li>Через поиск <i class="fas fa-search"></i> выберите фильм или сериал, к которому нужно описание (можно искать по ссылке на кинопоиск или просто ID)</li>
						<li>Если в нашей базе нет того, что вам нужно, сделайте заказ через вкладку &laquo;Другие тексты&raquo;</li>
						<li>Добавьте его в очередь заказов <i class="fas fa-plus"></i></li>
						<li>Ваш заказ еще не подтвержден и его можно отредактировать или удалить</li>
						<li>Если нужно, измените настройки задания <i class="fas fa-wrench"></i> (добавить комментарий, изменить длину текста)</li>
						<li>Подтвердите галочкой <i class="fas fa-check"></i> задания, которые нужно отправить на выполнение</li>
						<li>Как только баланс станет положительным &mdash; начнется выполнение заданий</li>
						<li>Готовые описания будут появляться в разделе <a href="/done">Выполненное</a></li>
						<li>Стоимость <code>1k</code> символов &mdash; <code>{{ $customerPricePer1k }} руб.</code></li>
					</ol>
					<h4>Другие тексты:</h4>
					<ol>
						<li>Перейдите во вкладку &laquo;Другие тексты&raquo;, заполните форму и добавьте задание.</li>
						<li>Ваш заказ отобразился в очереди, но еще не подтвержден и его можно отредактировать или удалить</li>
						<li>Если нужно, измените настройки задания <i class="fas fa-wrench"></i> (добавить комментарий, изменить длину текста)</li>
						<li>Подтвердите галочкой <i class="fas fa-check"></i> задания, которые нужно отправить на выполнение</li>
						<li>Как только баланс станет положительным &mdash; начнется выполнение заданий</li>
						<li>Готовые тексты будут появляться в разделе <a href="/done">Выполненное</a></li>
						<li>Стоимость <code>1k</code> символов &mdash; <code>{{ $customerPricePer1k }} руб.</code></li>
					</ol>
					<p>по всем вопросам телеграм <a href="tg://resolve?domain=megaezz">@megaezz</a></p>
				</div>
			</div>
		</div>
	</div>

	<livewire:types.cinema.admin.kinocontent.index :$user lazy />
	
</x-types.cinema.admin.kinocontent.layout>