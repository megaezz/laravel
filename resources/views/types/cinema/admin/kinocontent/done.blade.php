<x-types.cinema.admin.kinocontent.layout :$user page="done">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <h4 class="mb-4">Всего: {moviesRows} шт.</h4>
                </div>
                <div class="col-md-3">
                    <form class="form-inline my-2 my-lg-0 main-menu-search">
                        <div class="btn-group">
                            <input class="form-control mr-sm-2 main-search" type="search" name="q" placeholder="Поиск по выполненным заказам" aria-label="Search" autocomplete="off" style="width: 300px;">
                            <div class="dropdown-menu search-helper" style="width: 300px; padding: 0;">
                                <div class="list-group search-helper-list">

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-5 text-right">
                    <a class="btn btn-light" href="/blacklist"><i class="fas fa-user-slash"></i> Черный список</a>
                    <a class="btn btn-light {archivedActive}" href="/done?archived=1"><i class="fas fa-archive"></i> Архив <span class="badge badge-light">{archivedRows}</span></a>
                    <a class="btn btn-light" href="/doneCsv?archived={jsonArchived}" title="Экспортировать последние 300 заданий"><i class="fas fa-file-export"></i> Экспорт</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-2">
                    <a class="btn btn-light {moderatedActive}" href="/done?moderated=1">
                        <i class="far fa-check-square"></i> Проверено <span class="badge badge-light">{moderatedRows}</span>
                    </a>
                    <a class="btn btn-light {notModeratedActive}" href="/done?moderated=0">
                        <i class="far fa-clock"></i> Не проверено <span class="badge badge-light">{notModeratedRows}</span>
                    </a>
                </div>
            </div>
            {moviesList}
            <ul class="pagination">
                {moviesPages}
            </ul>
        </div>
    </div>
</x-types.cinema.admin.kinocontent.layout>