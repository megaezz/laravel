<x-types.cinema.admin.rewrite.layout>
	<div class="row">
		<div class="col-md-12">
			<p>
				Выплаты производятся автоматически каждую неделю <b>по четвергам</b>.<br>
				Это значит, что заказывать выплату не нужно, в четверг вам выплатится вся сумма, которая есть на счету.<br>
				Минимальная сумма выплаты &mdash; 20 руб.
			</p>
			<p>
				<b>Ваши реквизиты для оплаты:</b><br>
			</p>
			<form method="post" action="{{ route('set-wallet') }}" class="mb-5">
				<div class="form-inline">
					<select name="type" class="form-control">
						@foreach ($user->wallets->where('available', true) as $wallet)
						<option value="{{ $wallet['type'] }}" {{ ($user->wallet and $user->wallet['type'] === $wallet['type']) ? 'selected' : '' }}>{{ $wallet['serviceName'] }}</option>
						@endforeach
					</select>
					<input type="text" name="wallet" value="{{ $user->wallet ? $user->wallet['wallet'] : '' }}" class="form-control mr-2">
					<button type="submit" class="btn btn-secondary">Сохранить</button>
					@csrf
				</div>
			</form>
			<h3>Выплаты</h3>
			<table class="table table-striped bg-light">
				<tr style="font-weight: bold;">
					<td>Дата</td>
					<td>Сумма</td>
					<td>Комментарий</td>
				</tr>
				@foreach($transfers as $transfer)
				<tr>
					<td>{{ $transfer->date }}</td>
					<td>{{ $transfer->sum }}</td>
					<td>{{ $transfer->comment }}</td>
				</tr>
				@endforeach
			</table>
			<div class="row justify-content-between">
				<div class="col-12 col-sm-6 col-md-6">
					{{ $transfers->links() }}
				</div>
			</div>
		</div>
	</div>
</x-types.cinema.admin.rewrite.layout>