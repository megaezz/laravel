<x-types.engine.admin.dashboard>
	<div class="container">
		<div class="row">
			<div class="col-6">
				<h4 class="mb-4">
					<i class="fa-regular fa-envelope"></i> Абузы
				</h4>
			</div>
			<div class="col-6 text-end">
				<a class="btn btn-dark mb-3" href="{{ route('cinema.load-abuses') }}">
					<i class="fa-regular fa-circle-down"></i> Загрузить
				</a>
			</div>
		</div>
		<livewire:types.cinema.admin.abuses lazy />
	</div>
</x-types.engine.admin.dashboard>