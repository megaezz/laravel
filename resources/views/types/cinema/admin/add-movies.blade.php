<x-types.cinema.dashboard>
    
    @if ($resetCheckers)
    <p>Чеккеры Cinema сброшены</p>
    @endif

    <p>Всего: {{ $moviesRows }}, лимит: {{ $limit }}</p>

    <ul style="columns: 3; font-size: 10px;">
        @foreach ($actions as $movie_id => $movie_data)
        <li>
            <b>#{{ $movie_id }}</b>
            <ul>
                <li><b>Теги:</b> {{ $movie_data['model']->tags->pluck('tag')->implode(', ') }}</li>
                <li><b>СНГ:</b> {{ $movie_data['model']->is_sng ? 'Да' : 'Нет' }}</li>
                @if (isset($movie_data['messages']))
                <ul>
                    <b>Действия:</b>
                    @foreach ($movie_data['messages'] as $message)
                    <li style="color: {{ isset($message['color']) ? $message['color'] : 'gray' }}">
                        {{ $message['message'] }}
                    </li>
                    @endforeach
                </ul>
                @endif
            </ul>
            <ul>
                @foreach ($movie_data['domains'] as $domain => $domain_data)
                <li>
                    {{ $domain }}
                    <ul>
                        @foreach ($domain_data as $name => $result)
                            @if ($name == 'messages')
                            Действия:
                            <ul>
                                @foreach ($result as $message)
                                <li style="color: {{ isset($message['color']) ? $message['color'] : 'gray' }}">
                                    {{ $message['message'] }}
                                </li>
                                @endforeach
                            </ul>
                            @endif

                            @if ($name == 'allowed')
                            <li>
                                {{ $result['result'] ? "Разрешено" : "Не разрешено, {$result['message']}" }}
                            </li>
                            @endif

                        @endforeach
                    </ul>
                </li>
                @endforeach
            </ul>
        </li>
        @endforeach
    </ul>

    @if ($moviesRows)
    {!! $jsRedirect !!}
    @else
    <a href="/">Главная</a>
    @endif

</x-types.cinema.dashboard>