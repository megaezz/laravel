<x-types.engine.admin.dashboard>
	<div class="container">
		<div class="row">
			<div class="col-6">
				<h4 class="mb-4">
					<i class="fa-regular fa-circle-down"></i> Загрузить абузы
				</h4>
			</div>
			<div class="col-6 text-end">
				<a class="btn btn-dark mb-3" href="{{ route('cinema.abuses') }}">
					<i class="fa-regular fa-envelope"></i> Список
				</a>
			</div>
		</div>

		@if (session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
		@endif

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<div class="row">
			<div class="row">
				<div class="col-12 col-md-4">
					<h5>Яндекс Меморандум</h5>
					<form method="post" action="{{ route('cinema.memorandum-saver') }}">
						<div class="input-group mb-3">
							<input type="text" name="csvUrl" placeholder="Ссылка на CSV выгрузку" class="form-control">
							<button type="submit" class="btn btn-secondary">Отправить</button>
							@csrf
						</div>
					</form>
				</div>
			</div>
			{{-- <div class="col-12 col-md-4">
				<h5>Запостить в канал</h5>
				<form method="post" action="https://cinema.awmzone.net/?r=AdminLite/sendTelegramByKpId">
					<div class="input-group mb-3">
						<input type="text" name="kp_id" placeholder="ID кинопоиска" class="form-control">
						<button type="submit" class="btn btn-secondary">Запостить</button>
					</div>
				</form>
			</div> --}}
			<div class="col-12 col-md-4">
				<h5>Google Dmca</h5>
				<form method="post" action="{{ route('cinema.dmca-saver') }}">
					<div class="mb-3">
						<textarea name="text" rows="10" placeholder="Текст писем" class="form-control"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Отправить</button>
					@csrf
				</form>
			</div>
			<div class="col-12 col-md-4">
				<h5>РКН</h5>
				<form method="post" action="{{ route('cinema.rkn-saver') }}">
					<div class="mb-3">
						<textarea name="text" rows="10" placeholder="Текст писем" class="form-control"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Отправить</button>
					@csrf
				</form>
			</div>
			<div class="col-12 col-md-4">
				<h5>Переобход Яндекс</h5>
				<form method="post" action="{{ route('cinema.yandex-recrawl') }}">
					<div class="mb-3">
						<textarea name="text" rows="10" placeholder="Список ссылок" class="form-control"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Отправить</button>
					@csrf
				</form>
			</div>
			<div class="col-12 col-md-4">
				<h5>Жалобы</h5>
				<form method="post" action="{{ route('cinema.abuse-handler') }}">
					<div class="form-group">
						<select name="service" class="form-control">
							<option value="">Выберите сервис</option>
							<option value="group-ib">group-ib</option>
							<option value="kinopoisk">kinopoisk</option>
							<option value="starmedia">starmedia</option>
							<option value="vindex">vindex</option>
							<option value="axghouse">axghouse</option>
							<option value="прав">прав</option>
							<option value="ркн">ркн</option>
							<option value="inter.ua">inter.ua</option>
							<option value="webkontrol">webkontrol</option>
							<option value="icm">icm</option>
							<option value="sudum">sudum</option>
							<option value="isola">isola</option>
							<option value="film.ua">film.ua</option>
							<option value="rico">rico</option>
							<option value="dmca">dmca</option>
							<option value="aiplex">aiplex</option>
							<option value="digi">digi</option>
							<option value="contentscan">contentscan</option>
							<option value="start-film.com">start-film.com</option>
							<option value="1+1">1+1</option>
							<option value="tnt">tnt</option>
						</select>
					</div>
					<div class="mb-3">
						<textarea name="text" rows="10" placeholder="Текст писем" class="form-control"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Отправить</button>
					@csrf
				</form>
			</div>
		</div>
	</div>
</x-types.engine.admin.dashboard>