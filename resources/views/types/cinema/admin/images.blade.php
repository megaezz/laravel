<x-types.engine.admin.dashboard>
	<div class="container">
		@foreach ($types as $type => $images)
		<h1>{{ $type }}</h1>
		<div class="row mb-2">
			@foreach ($images as $image)
			<div class="col-6 col-md-2">
				<img src="/storage/images/w200{{ $image }}" style="max-width: 100px; height: auto;">
			</div>
			<div class="col-6 col-md-10">
				{{ $image }}
			</div>
			@endforeach
		</div>
		@endforeach
	</div>
</x-types.engine.admin.dashboard>
