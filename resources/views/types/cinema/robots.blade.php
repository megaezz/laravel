User-agent: *
Allow: /
Disallow: /group/*/
Disallow: /group/*?*
Disallow: /movies/
Disallow: /genres/*/
Disallow: /search
Disallow: /show-more/
Disallow: /embed-players/
Disallow: /updates
Disallow: /api
<?php /* пишем комментарии именно таким образом - в эстетических целях, чтобы не появлялось пустых строк */ ?>
<?php /* если установлен флаг index_search, то разрешаем индексацию поиска с &index для всех */ ?>
@if ($domain->cinemaDomain->index_search)
Allow: /search*&index
@endif
<?php /* если не установлен флаг index_movies_in_yandex, то закрываем фильмы для яндекса */ ?>
@if (!$domain->cinemaDomain->index_movies_in_yandex)
User-agent: Yandex
<?php /* повторяем общие правила, т.к. иначе яндекс их игнорит */ ?>
Disallow: /group/*/
Disallow: /group/*?*
Disallow: /movies/
Disallow: /genres/*/
Disallow: /search
Disallow: /show-more/
Disallow: /updates
Disallow: /api
@foreach ($domain->cinemaDomain->getWatchDirs() as $watchDir)
Disallow: {{ $watchDir }}/*
@endforeach
@endif
<?php /* если не установлен флаг yandexAllowed, то запрещаем индексацию всех страниц для яндекса */ ?>
@if (!$domain->yandexAllowed)
User-agent: Yandex
Disallow: /
@endif
Sitemap: {{ $domain->protocol }}://{{ \engine\app\models\Engine::getCrawlerDomain()->domain }}/sitemap.xml
