@if ($movie->movie->kinopoisk?->mainTrailer)
<script>
	document.addEventListener("DOMContentLoaded", function() {
		iframe = document.createElement("iframe");
		iframe.id = 'trailer_iframe';
		iframe.width = '100%';
		iframe.height = 420;
		iframe.setAttribute('frameborder',0);
		iframe.setAttribute('allowfullscreen','');
		document.getElementById('player-trailer').appendChild(iframe);

		var src = '{{ $movie->movie->kinopoisk?->changeYoutubeUrlToEmbed($movie->movie->kinopoisk?->main_trailer?->url) }}';
		var isLoaded = false;

		let trailerPlayer = document.getElementById('player-trailer');
		let observer = new IntersectionObserver((entries,observer) => {
			entries.forEach(entry => {
				if (entry.isIntersecting) {
					$('#trailer_iframe').attr('src',src);
					// console.log(entry.target + ' видимый');
					observer.unobserve(entry.target);
		 	}
		 });
		}, { threshold: 0.4 });

		// если есть кнопка плеера то подгружаем src только по ее нажатию
		// если это главный плеер - подгружаем сразу
		if (typeof mainPlayer !== 'undefined' && mainPlayer == 'trailer') {
			// $('#hdvb_iframe').attr('src',src);
			// console.log(hdvbPlayer + ' следим');
			observer.observe(trailerPlayer);
		} else {
			$('.show-player-btn[data-player=trailer]').on('click',function(){
				if (!isLoaded) {
					$('#trailer_iframe').attr('src',src);
					isLoaded = true;
				}
			});
		}
	});
</script>
@endif