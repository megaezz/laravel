<?php

return [

    'legacy_model_binding' => true,

    'inject_assets' => true,

    'inject_morph_markers' => true,

    'navigate' => false,

    // 'pagination_theme' => 'tailwind',
    'pagination_theme' => 'bootstrap',

];
