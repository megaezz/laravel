<?php

return [
    App\Providers\AppServiceProvider::class,
    App\Providers\Filament\AwmzonePanelProvider::class,
    App\Providers\Filament\ZdravzdravPanelProvider::class,
    App\Providers\TelescopeServiceProvider::class,
    Intervention\Image\ImageServiceProvider::class,
];
