<?php

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

// тесты не используют index.php поэтому там нет LARAVEL_START, поэтому определяем здесь
if (! defined('LARAVEL_START')) {
    define('LARAVEL_START', microtime(true));
}

// подключение автолоадера megaweb
require __DIR__.'/../public/types/engine/app/autoload.php';

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
    )
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->prepend([
            // ставлю ModifyHeaders в самом начале, по аналогии с TrustProxies
            \App\Http\Middleware\Engine\ModifyHeaders::class,
            // Engine тут, потому что там общий выключатель например, чего ресурсы дальше зря тратить.
            // И чтобы 421 отбражалось если домена нет, иначе 404 если Engine в группе web - т.к. группа не отрабатывает,
            // потому что роуты не регистрируются, в том числе fallback.
            // Можно его переименовть в Aborter - лучше отражает суть, но придумать куда перенести terminate метод
            \App\Http\Middleware\Engine\Engine::class,
        ]);
        $middleware->web(
            prepend: [
                //
            ],
            append: [
                // только когда Debug в web - появляется массив с megaweb запросами в дебагбаре
                \App\Http\Middleware\Engine\Debug::class,
                // AutoBan перед Ban, чтобы сразу блочился запрос
                \App\Http\Middleware\Engine\AutoBan::class,
            ]
        );
        $middleware->appendToGroup('dynamic', [
            \App\Http\Middleware\Engine\CronSwitcher::class,
            \App\Http\Middleware\Engine\Ban::class,
            \App\Http\Middleware\Engine\AccessScheme::class,
        ]);
        $middleware->alias([
            // 'auth' => \App\Http\Middleware\Authenticate::class,
            // 'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
            'megaweb.auth' => \App\Http\Middleware\MegawebAuthorization::class,
            'permission' => \Spatie\Permission\Middleware\PermissionMiddleware::class,
        ]);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        $exceptions->render(function (NotFoundHttpException $e, Request $request) {
            logger()->warning($e->getMessage());
        });
    })->create();
