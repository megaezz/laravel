<?php

namespace Tests\Feature;

use App\Models\Domain;
use App\Services\Tests\Clients;
use Tests\TestCase;

// $this->assertTrue($response->isRedirect());
// $response->assertRedirect('/target-url');
// $response->assertRedirectToRoute('target.route.name');
// $response->assertStatus(301);
// Следовать редиректу и проверить содержимое
// $this->followRedirects($response)->assertSee('Target Page Content');
// $response->headers->get('location');

class AccessSchemeTest extends TestCase
{
    private Clients $clients;

    private Domain $domain;

    protected function setUp(): void
    {
        parent::setUp();

        $this->clients = new Clients($this);
        $this->domain = Domain::findOrFail('local.kinogo.awmzone.net');
    }

    /** @test */
    public function гугл_бот_успешно_заходит_на_актуальное_зеркало_для_гугла()
    {
        $this->clients->googleBot()->get("http://{$this->domain->getCrawlerDomain('Googlebot')->domain}")->assertOk()->assertSee('Киного');
    }

    /** @test */
    public function гугл_бот_заходит_на_актуальное_зеркало_для_яндекса_и_его_редиректит_на_актуальное_зеркало_для_гугла()
    {
        $this->clients->googleBot()->get("http://{$this->domain->getCrawlerDomain('Yandex')->domain}")->assertOk()->assertSee('Киного');
    }

    public function гугл_бот_заходит_на_актуальное_клиентское_зеркало_и_его_редиректит_на_актуальное_зеркало_для_гугла() {}

    public function гугл_бот_заходит_на_неактуальное_зеркало_и_его_редиректит_на_актуальное_зеркало_для_гугла() {}

    public function яндекс_бот_успешно_заходит_на_актуальное_зеркало_для_яндекса() {}

    public function яндекс_бот_заходит_на_актуальное_зеркало_для_гугла_и_его_редиректит_на_актуальное_зеркало_для_яндекса() {}

    public function яндекс_бот_заходит_на_актуальное_клиентское_зеркало_и_его_редиректит_на_актуальное_зеркало_для_яндекса() {}

    public function яндекс_бот_заходит_на_неактуальное_зеркало_и_его_редиректит_на_актуальное_зеркало_для_яндекса() {}

    public function бинг_бот_успешно_заходит_на_актуальное_зеркало_для_гугла() {}

    public function бинг_бот_заходит_на_актуальное_зеркало_для_яндекса_и_его_редиректит_на_актуальное_зеркало_для_гугла() {}

    public function бинг_бот_заходит_на_клиентский_домен_и_его_редиректит_на_актуальное_зеркало_для_гугла() {}

    public function бинг_бот_заходит_на_неактуальное_зеркало_и_его_редиректит_на_актуальное_зеркало_для_гугла() {}

    public function клиент_успешно_заходит_на_любое_клиентское_зеркало() {}

    public function клиент_успешно_заходит_на_любое_неактуальное_зеркало() {}

    public function клиент_заходит_на_актуальное_зеркало_для_гугла_без_валидного_реферера_и_видит_заглушку() {}

    public function клиент_заходит_на_актуальное_зеркало_для_яндекса_без_валидного_реферера_и_видит_заглушку() {}

    public function клиент_заходит_на_актуальное_зеркало_для_гугла_с_валидным_реферером_и_его_редиректит_на_актуальное_клиентское_зеркало() {}

    public function клиент_заходит_на_актуальное_зеркало_для_яндекса_с_валидным_реферером_и_его_редиректит_на_актуальное_клиентское_зеркало() {}
}
