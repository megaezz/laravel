# ************************************************************
# Sequel Ace SQL dump
# Version 3038
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 127.0.0.1 (MySQL 5.7.33-log)
# Database: cinema
# Generation Time: 2021-09-15 15:08:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

# Dump of table alloha
# ------------------------------------------------------------

DROP TABLE IF EXISTS `alloha`;

CREATE TABLE `alloha` (
  `token_movie` varchar(255) NOT NULL DEFAULT '',
  `data` mediumtext,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `movie_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`token_movie`),
  KEY `checked` (`checked`),
  KEY `movie_id` (`movie_id`),
  CONSTRAINT `alloha_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` text,
  `title` varchar(255) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `apply_date` timestamp NULL DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `domain` char(30) DEFAULT NULL,
  `main_image` varchar(255) DEFAULT NULL,
  `text_preview` text,
  `domain_movie_id` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `acceptor` char(30) DEFAULT NULL,
  `chpu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `domain` (`domain`),
  KEY `domain_movie_id` (`domain_movie_id`),
  KEY `acceptor` (`acceptor`),
  CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE,
  CONSTRAINT `articles_ibfk_3` FOREIGN KEY (`domain_movie_id`) REFERENCES `domain_movies` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `articles_ibfk_4` FOREIGN KEY (`acceptor`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cinema
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cinema`;

CREATE TABLE `cinema` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title_ru` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `title_en` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `kinopoisk_id` varchar(255) DEFAULT NULL,
  `mw_unique_id` varchar(255) DEFAULT NULL,
  `copy_poster_url` varchar(255) DEFAULT NULL,
  `poster` tinyint(1) NOT NULL DEFAULT '0',
  `added_at` timestamp NULL DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `mw_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `kinopoisk_rating` float DEFAULT NULL,
  `kinopoisk_votes` int(11) DEFAULT NULL,
  `imdb_rating` float DEFAULT NULL,
  `imdb_votes` int(11) DEFAULT NULL,
  `type` enum('serial','movie','trailer','youtube') DEFAULT NULL,
  `description` mediumtext CHARACTER SET utf8mb4,
  `kinopoisk_rating_sko` float DEFAULT NULL,
  `imdb_rating_sko` float DEFAULT NULL,
  `rkn_block` tinyint(1) NOT NULL DEFAULT '0',
  `embed` varchar(255) DEFAULT '',
  `embed_trailer` varchar(255) DEFAULT '',
  `direct` varchar(255) DEFAULT NULL,
  `do_update` tinyint(1) NOT NULL DEFAULT '1',
  `rating_sko` float DEFAULT NULL,
  `myshows_id` int(10) unsigned DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `started` date DEFAULT NULL,
  `ended` date DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `source_type` varchar(255) DEFAULT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `lordDescription` text,
  `hdvb_4k` tinyint(1) NOT NULL DEFAULT '0',
  `mainPlayer` enum('mnw','videocdn') DEFAULT NULL,
  `blockStr` varchar(255) DEFAULT NULL,
  `collaps` tinyint(1) NOT NULL DEFAULT '0',
  `videodb` tinyint(1) NOT NULL DEFAULT '0',
  `videocdn` tinyint(1) NOT NULL DEFAULT '0',
  `imdb_id` varchar(255) DEFAULT NULL,
  `youtube_id` char(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mw_unique_id` (`mw_unique_id`),
  UNIQUE KEY `kinopoisk_id` (`kinopoisk_id`),
  UNIQUE KEY `youtube_id` (`youtube_id`),
  KEY `myshows_id` (`myshows_id`),
  KEY `type` (`type`),
  KEY `deleted` (`deleted`),
  KEY `imdb_id` (`imdb_id`),
  KEY `checked` (`checked`),
  KEY `poster` (`poster`),
  CONSTRAINT `cinema_ibfk_1` FOREIGN KEY (`myshows_id`) REFERENCES `myshows.me` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `cinema_ibfk_2` FOREIGN KEY (`youtube_id`) REFERENCES `youtube_videos` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cinema_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cinema_tags`;

CREATE TABLE `cinema_tags` (
  `cinema_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cinema_id`,`tag_id`),
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `cinema_tags_ibfk_2` FOREIGN KEY (`cinema_id`) REFERENCES `cinema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cinema_tags_ibfk_3` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table collaps
# ------------------------------------------------------------

DROP TABLE IF EXISTS `collaps`;

CREATE TABLE `collaps` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `unique_id` varchar(255) DEFAULT '',
  `data` mediumtext,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `movie_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unique_id` (`unique_id`),
  KEY `checked` (`checked`),
  KEY `movie_id` (`movie_id`),
  CONSTRAINT `collaps_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table configs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `configs`;

CREATE TABLE `configs` (
  `rewrite_price_per_1k` int(3) NOT NULL,
  `customer_price_per_1k` int(3) NOT NULL,
  `service_load` int(3) NOT NULL DEFAULT '0',
  `book_minutes` int(11) NOT NULL COMMENT 'сколько времени есть у рерайтера для выполнения задания',
  `high_rewrite_price_per_1k` int(3) NOT NULL,
  `regular_rewrite_price_per_1k` int(3) NOT NULL,
  `tasks100percent` int(3) NOT NULL COMMENT 'какое кол-во активных заданий принимаем за 100% нагрузку',
  `highPricePercent` int(3) NOT NULL COMMENT 'с какого процента нагрузки включается повышенная цена',
  `refPercent` int(3) NOT NULL COMMENT 'реферальный процент рерайтеру',
  `videocdnDomain` char(30) DEFAULT '',
  `refCustomer` int(3) NOT NULL COMMENT 'реферальный процент заказчикам',
  `refWorker` int(3) NOT NULL COMMENT 'реф процент для рерайтера',
  `minWorkerPayment` int(3) NOT NULL COMMENT 'минимальная выплата',
  `usdRubShave` decimal(2,1) NOT NULL COMMENT 'сколько шейвим рублей при оплате в долларах',
  `rewriteExpressFare` int(3) NOT NULL,
  `customerExpressFare` int(11) NOT NULL,
  `moderationPricePer1k` decimal(2,1) NOT NULL,
  `descriptionWaitTime` decimal(4,1) NOT NULL,
  `sitemapMoviesLimit` int(11) NOT NULL,
  `playerjsVersion` char(10) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table crons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `crons`;

CREATE TABLE `crons` (
  `action` char(30) CHARACTER SET cp1251 NOT NULL,
  `time_range` int(10) DEFAULT NULL,
  `last_time` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'для движка v2',
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `run_time` float(5,2) DEFAULT NULL,
  PRIMARY KEY (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table customer_blacklist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer_blacklist`;

CREATE TABLE `customer_blacklist` (
  `customer` char(30) NOT NULL DEFAULT '',
  `worker` char(30) NOT NULL DEFAULT '',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `customer_2` (`customer`,`worker`),
  KEY `customer` (`customer`),
  KEY `worker` (`worker`),
  CONSTRAINT `customer_blacklist_ibfk_1` FOREIGN KEY (`customer`) REFERENCES `users` (`login`) ON UPDATE CASCADE,
  CONSTRAINT `customer_blacklist_ibfk_2` FOREIGN KEY (`worker`) REFERENCES `users` (`login`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table dmca
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dmca`;

CREATE TABLE `dmca` (
  `domain` char(30) DEFAULT NULL,
  `movie_id` int(11) unsigned DEFAULT NULL,
  `domain_movie_id` int(11) unsigned DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `domain_2` (`domain`,`domain_movie_id`),
  UNIQUE KEY `domain` (`domain`,`movie_id`),
  KEY `movie_id` (`movie_id`),
  KEY `domain_movie_id` (`domain_movie_id`),
  CONSTRAINT `dmca_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dmca_ibfk_2` FOREIGN KEY (`domain_movie_id`) REFERENCES `domain_movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dmca_ibfk_3` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table domain_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domain_groups`;

CREATE TABLE `domain_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain` char(30) NOT NULL DEFAULT '',
  `group_id` smallint(5) unsigned NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` mediumtext,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `alt_id` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `dmca` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`domain`,`group_id`),
  UNIQUE KEY `domain_2` (`domain`,`alt_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `domain_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `domain_groups_ibfk_3` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table domain_movie_changes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domain_movie_changes`;

CREATE TABLE `domain_movie_changes` (
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(11) unsigned DEFAULT NULL,
  `property` varchar(255) NOT NULL DEFAULT '',
  `old_value` text,
  `new_value` text,
  `login` char(30) NOT NULL DEFAULT '',
  KEY `login` (`login`),
  KEY `property` (`property`),
  KEY `id` (`id`),
  CONSTRAINT `domain_movie_changes_ibfk_1` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `domain_movie_changes_ibfk_2` FOREIGN KEY (`id`) REFERENCES `domain_movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table domain_movies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domain_movies`;

CREATE TABLE `domain_movies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain` char(30) DEFAULT NULL,
  `movie_id` int(11) unsigned DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text,
  `description_writer` char(30) DEFAULT NULL,
  `description_date` timestamp NULL DEFAULT NULL,
  `booked_for_login` char(30) DEFAULT NULL,
  `description_price_per_1k` int(3) DEFAULT NULL,
  `text_ru_description_id` varchar(255) DEFAULT NULL,
  `text_ru_result` text,
  `text_ru_wide_result` text,
  `text_ru_unique` float DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `dmca` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `customer` char(30) DEFAULT NULL,
  `customer_price_per_1k` int(3) DEFAULT NULL,
  `rework` tinyint(1) NOT NULL DEFAULT '0',
  `rework_comment` text,
  `show_to_customer` tinyint(1) NOT NULL DEFAULT '1',
  `customer_archived` tinyint(1) NOT NULL DEFAULT '0',
  `to_top` tinyint(1) NOT NULL DEFAULT '0',
  `writing` tinyint(1) NOT NULL DEFAULT '0',
  `customer_comment` text,
  `symbols_from` int(11) DEFAULT NULL,
  `symbols_to` int(11) DEFAULT NULL,
  `book_date` timestamp NULL DEFAULT NULL,
  `private_comment` text,
  `to_bottom` tinyint(1) NOT NULL DEFAULT '0',
  `rkn` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_page` tinyint(1) NOT NULL DEFAULT '0',
  `do_redirect` tinyint(1) NOT NULL DEFAULT '1',
  `block_foreigners` tinyint(1) NOT NULL DEFAULT '0',
  `customer_like` tinyint(1) DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `expected_price` decimal(6,1) DEFAULT NULL,
  `description_length` int(5) NOT NULL DEFAULT '0',
  `rework_date` timestamp NULL DEFAULT NULL,
  `moderator` char(30) DEFAULT NULL,
  `moderation_price_per_1k` decimal(2,1) NOT NULL DEFAULT '0.0',
  `customer_confirm_date` timestamp NULL DEFAULT NULL,
  `adult` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`domain`,`movie_id`),
  KEY `movie_id` (`movie_id`),
  KEY `description_writer` (`description_writer`),
  KEY `booked_for_login` (`booked_for_login`),
  KEY `customer` (`customer`),
  KEY `rework` (`rework`),
  KEY `writing` (`writing`),
  KEY `show_to_customer` (`show_to_customer`),
  KEY `checked` (`checked`),
  KEY `description_length` (`description_length`),
  KEY `text_ru_description_id` (`text_ru_description_id`),
  KEY `active` (`active`),
  KEY `domain_2` (`domain`),
  KEY `description_price_per_1k` (`description_price_per_1k`),
  KEY `moderator` (`moderator`),
  KEY `customer_confirm_date` (`customer_confirm_date`),
  KEY `deleted_page` (`deleted_page`),
  KEY `adult` (`adult`),
  CONSTRAINT `domain_movies_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `domain_movies_ibfk_2` FOREIGN KEY (`description_writer`) REFERENCES `users` (`login`) ON UPDATE CASCADE,
  CONSTRAINT `domain_movies_ibfk_3` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE,
  CONSTRAINT `domain_movies_ibfk_4` FOREIGN KEY (`booked_for_login`) REFERENCES `users` (`login`) ON UPDATE CASCADE,
  CONSTRAINT `domain_movies_ibfk_5` FOREIGN KEY (`customer`) REFERENCES `users` (`login`) ON UPDATE CASCADE,
  CONSTRAINT `domain_movies_ibfk_6` FOREIGN KEY (`moderator`) REFERENCES `users` (`login`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dump of table domain_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domain_tags`;

CREATE TABLE `domain_tags` (
  `domain` char(30) NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  `text` text,
  `dmca` varchar(255) DEFAULT NULL,
  UNIQUE KEY `domain_2` (`domain`,`tag_id`),
  KEY `domain` (`domain`),
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `domain_tags_ibfk_1` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE,
  CONSTRAINT `domain_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table domains
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domains`;

CREATE TABLE `domains` (
  `domain` char(30) NOT NULL DEFAULT '',
  `useInRedirectToAnyMovie` tinyint(1) NOT NULL DEFAULT '0',
  `include_serials` tinyint(1) NOT NULL DEFAULT '1',
  `include_movies` tinyint(1) NOT NULL DEFAULT '1',
  `include_trailers` tinyint(1) NOT NULL DEFAULT '1',
  `include_games` tinyint(1) NOT NULL DEFAULT '0',
  `include_youtube` tinyint(1) NOT NULL DEFAULT '0',
  `movies_with_unique_description` tinyint(1) NOT NULL DEFAULT '1',
  `topMoviesLimit` int(2) unsigned NOT NULL DEFAULT '6',
  `watchLink` varchar(255) DEFAULT NULL,
  `groupLink` varchar(255) DEFAULT NULL,
  `genreLink` varchar(255) DEFAULT NULL,
  `useMovieId` tinyint(1) NOT NULL DEFAULT '0',
  `useTagIdForGenres` tinyint(1) NOT NULL DEFAULT '0',
  `moviesLimit` int(2) unsigned NOT NULL DEFAULT '28',
  `moviesPerDay` int(3) unsigned DEFAULT NULL COMMENT 'сколько фильмов добавлять за обновление',
  `moviesAddedPerDay` int(3) unsigned DEFAULT NULL COMMENT 'сколько фильмов добавлено за последнее обновление',
  `legalMovies` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'добавлять только фильмы, для которых есть легальный плеер',
  `watchCalcSameMovies` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'просчитывать похожие видео',
  `watchLinkEn` varchar(255) DEFAULT NULL,
  `calcThumbGenresList` tinyint(1) NOT NULL DEFAULT '0',
  `watchMovieLink` varchar(255) DEFAULT NULL,
  `watchSerialLink` varchar(255) DEFAULT NULL,
  `dmcaQuestion` tinyint(1) NOT NULL DEFAULT '0',
  `useAltDescriptions` tinyint(1) NOT NULL DEFAULT '0',
  `recaptchaVersion` enum('v2','v3','v3_2','v3_3') NOT NULL DEFAULT 'v2',
  `allowRussianMovies` tinyint(1) NOT NULL DEFAULT '1',
  `rknForRuOnly` tinyint(1) DEFAULT '0' COMMENT 'удалять ролики только для РУ трафика',
  `moviesWithPoster` tinyint(1) NOT NULL DEFAULT '1',
  `rknBanned` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `theme` varchar(255) DEFAULT NULL,
  `text` text,
  `domain` char(30) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `name` char(40) CHARACTER SET utf8 DEFAULT '',
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `type` enum('genre','compilation','country','translator','youtube') CHARACTER SET utf8 DEFAULT NULL,
  `plural` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `plural2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `emoji` text,
  `thumb` varchar(255) DEFAULT NULL,
  `name_en` char(40) DEFAULT NULL,
  `name_ua` char(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table groups_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups_tags`;

CREATE TABLE `groups_tags` (
  `group_id` smallint(5) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  KEY `group_id` (`group_id`),
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `groups_tags_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `groups_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kinopoisk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kinopoisk`;

CREATE TABLE `kinopoisk` (
  `id` int(11) unsigned NOT NULL,
  `data` text,
  `rating` decimal(10,2) DEFAULT NULL,
  `ratingVoteCount` int(10) unsigned DEFAULT NULL,
  `yearFrom` int(10) unsigned DEFAULT NULL,
  `yearTo` int(10) unsigned DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `update_time` (`update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kodik
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kodik`;

CREATE TABLE `kodik` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `unique_id` varchar(255) DEFAULT '',
  `data` text,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `unique_id` (`unique_id`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table lordsfilms.tv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lordsfilms.tv`;

CREATE TABLE `lordsfilms.tv` (
  `url` varchar(255) NOT NULL DEFAULT '',
  `movie_id` int(11) unsigned DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  PRIMARY KEY (`url`),
  KEY `movie_id` (`movie_id`),
  KEY `checked` (`checked`),
  CONSTRAINT `lordsfilms.tv_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table moonwalk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moonwalk`;

CREATE TABLE `moonwalk` (
  `token` varchar(255) NOT NULL DEFAULT '',
  `unique_id` varchar(255) DEFAULT '',
  `data` text,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`token`),
  KEY `unique_id` (`unique_id`),
  KEY `checked` (`checked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table moonwalk_updates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `moonwalk_updates`;

CREATE TABLE `moonwalk_updates` (
  `cinema_id` int(11) unsigned NOT NULL,
  `data` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` varchar(255) NOT NULL DEFAULT '',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `unique_id` varchar(255) DEFAULT NULL,
  KEY `cinema_id` (`cinema_id`,`token`),
  KEY `date` (`date`),
  KEY `token` (`token`),
  CONSTRAINT `moonwalk_updates_ibfk_1` FOREIGN KEY (`cinema_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table movie_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movie_comments`;

CREATE TABLE `movie_comments` (
  `domain_movie_id` int(11) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(60) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `ip` varchar(40) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `domain_movie_id` (`domain_movie_id`),
  CONSTRAINT `movie_comments_ibfk_1` FOREIGN KEY (`domain_movie_id`) REFERENCES `domain_movies` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table movie_episodes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movie_episodes`;

CREATE TABLE `movie_episodes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) unsigned NOT NULL,
  `translator` varchar(255) NOT NULL DEFAULT '',
  `season` int(3) unsigned NOT NULL,
  `episode` int(5) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `movie_id` (`movie_id`,`season`,`episode`,`translator`),
  KEY `movie_id_2` (`movie_id`,`translator`),
  CONSTRAINT `movie_episodes_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table movie_episodes_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movie_episodes_data`;

CREATE TABLE `movie_episodes_data` (
  `movie_id` int(11) unsigned NOT NULL,
  `season` int(10) unsigned NOT NULL,
  `episode` int(10) unsigned NOT NULL,
  `air_date` timestamp NULL DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT NULL,
  UNIQUE KEY `season` (`season`,`movie_id`,`episode`),
  KEY `movie_id` (`movie_id`),
  CONSTRAINT `movie_episodes_data_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table movie_players
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movie_players`;

CREATE TABLE `movie_players` (
  `movie_id` int(11) unsigned NOT NULL,
  `season` varchar(255) DEFAULT NULL,
  `episode` varchar(255) DEFAULT '',
  `player` text,
  UNIQUE KEY `season` (`season`,`movie_id`,`episode`),
  KEY `movie_id` (`movie_id`),
  CONSTRAINT `movie_players_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table movies_same
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movies_same`;

CREATE TABLE `movies_same` (
  `movie_id` int(11) unsigned NOT NULL,
  `same_movie_id` int(10) unsigned NOT NULL,
  `q` int(4) unsigned NOT NULL DEFAULT '0',
  KEY `movie_id` (`movie_id`),
  KEY `q` (`q`),
  KEY `same_movie_id` (`same_movie_id`),
  CONSTRAINT `movies_same_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `movies_same_ibfk_2` FOREIGN KEY (`same_movie_id`) REFERENCES `cinema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table myshows.me
# ------------------------------------------------------------

DROP TABLE IF EXISTS `myshows.me`;

CREATE TABLE `myshows.me` (
  `id` int(11) unsigned NOT NULL,
  `data` mediumtext,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `movie_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `movie_id` (`movie_id`),
  KEY `checked` (`checked`),
  CONSTRAINT `myshows.me_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes` (
  `pattern` char(100) NOT NULL DEFAULT '',
  `template` char(30) DEFAULT NULL,
  `cache` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'кешировать ли результат роутера',
  `alias` char(100) DEFAULT NULL,
  `controller` char(30) DEFAULT NULL,
  `method` char(30) DEFAULT NULL,
  `var1` char(30) DEFAULT NULL,
  `var2` char(30) DEFAULT NULL,
  `var3` char(30) DEFAULT NULL,
  `var4` char(30) DEFAULT NULL,
  `var5` char(30) DEFAULT NULL,
  `comment` char(100) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` mediumtext,
  `keywords` varchar(300) DEFAULT NULL,
  `h1` varchar(300) DEFAULT NULL,
  `topText` mediumtext,
  `bottomText` mediumtext,
  `bread` text,
  `text1` mediumtext,
  `text2` mediumtext,
  `h2` varchar(200) DEFAULT NULL,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `redirectToAlias` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rserial.com
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rserial.com`;

CREATE TABLE `rserial.com` (
  `url` varchar(255) NOT NULL DEFAULT '',
  `movie_id` int(11) unsigned DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`url`),
  KEY `movie_id` (`movie_id`),
  KEY `checked` (`checked`),
  CONSTRAINT `rserial.com_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rus_test_answers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rus_test_answers`;

CREATE TABLE `rus_test_answers` (
  `test_id` int(11) unsigned NOT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `correct` tinyint(1) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `rus_test_answers_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `rus_test_questions` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table rus_test_questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rus_test_questions`;

CREATE TABLE `rus_test_questions` (
  `question` varchar(255) DEFAULT NULL,
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table seo-clicks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `seo-clicks`;

CREATE TABLE `seo-clicks` (
  `domain` char(30) NOT NULL,
  `se` enum('https://yandex.ru','https://google.com') NOT NULL DEFAULT 'https://yandex.ru',
  `query` varchar(255) NOT NULL,
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`),
  CONSTRAINT `seo-clicks_ibfk_1` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tag` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('genre','country','type','category','camrip','actor','director','studio','year','channel','translator') CHARACTER SET utf8 DEFAULT NULL,
  `plural` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `emoji` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plural2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tag_ua` varchar(255) DEFAULT NULL,
  `tag_en` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`),
  KEY `type` (`type`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table transfers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transfers`;

CREATE TABLE `transfers` (
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login` char(30) NOT NULL DEFAULT '',
  `sum` decimal(8,2) NOT NULL DEFAULT '0.00',
  `comment` varchar(255) DEFAULT NULL,
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `unitpay` text,
  `payout` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `login` (`login`),
  KEY `payout` (`payout`),
  CONSTRAINT `transfers_ibfk_1` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `videodb_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `q` int(11) unsigned NOT NULL DEFAULT '0',
  `order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `videodb_name` (`videodb_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table user_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_sessions`;

CREATE TABLE `user_sessions` (
  `sid` char(43) NOT NULL DEFAULT '',
  `login` char(30) NOT NULL DEFAULT '',
  `last_activity` timestamp NULL DEFAULT NULL,
  `user_agent` varchar(255) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(40) DEFAULT NULL,
  `user_id` int(3) unsigned DEFAULT NULL,
  `cookies` text,
  PRIMARY KEY (`sid`),
  KEY `login` (`login`),
  KEY `login_id` (`user_id`),
  CONSTRAINT `user_sessions_ibfk_1` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON UPDATE CASCADE,
  CONSTRAINT `user_sessions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `login` char(30) NOT NULL DEFAULT '',
  `premoderation` tinyint(1) NOT NULL DEFAULT '1',
  `pass` char(30) NOT NULL DEFAULT '',
  `level` char(30) NOT NULL DEFAULT '',
  `last_activ` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timezone` char(6) DEFAULT NULL,
  `comments` text,
  `rewrite_price_per_1k` int(3) DEFAULT NULL,
  `customer_price_per_1k` int(3) DEFAULT NULL,
  `active_rewriter` tinyint(1) NOT NULL DEFAULT '1',
  `wmr` varchar(255) DEFAULT NULL,
  `yandex` varchar(255) DEFAULT NULL,
  `advcash` varchar(255) DEFAULT NULL,
  `sberbank` varchar(255) DEFAULT NULL,
  `temp_balance` decimal(8,2) DEFAULT NULL,
  `symbols_from` int(11) DEFAULT NULL,
  `symbols_to` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `customer_comment` text,
  `customerTemp1TxtPrice` decimal(6,2) DEFAULT NULL,
  `lower_down` tinyint(1) NOT NULL DEFAULT '0',
  `customer_private_comment` tinytext,
  `customer_active_tasks` int(3) unsigned DEFAULT NULL,
  `checkIsMovieInDoneTasks` tinyint(1) NOT NULL DEFAULT '0',
  `partner` char(30) DEFAULT NULL,
  `wmz` varchar(255) DEFAULT NULL,
  `qiwi` varchar(255) DEFAULT NULL,
  `unique_auto_check` tinyint(1) NOT NULL DEFAULT '0',
  `expressRewriter` tinyint(1) NOT NULL DEFAULT '0',
  `customerAutoExpress` tinyint(1) NOT NULL DEFAULT '0',
  `api_key` char(43) DEFAULT NULL,
  `adult` tinyint(1) NOT NULL DEFAULT '0',
  `emailConfirmed` tinyint(1) NOT NULL DEFAULT '0',
  `emailCode` char(40) DEFAULT NULL,
  `temp_balance_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `restrict_end_date` timestamp NULL DEFAULT NULL,
  `test_passed` tinyint(1) NOT NULL DEFAULT '0',
  `test_start_date` timestamp NULL DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `api_key` (`api_key`),
  KEY `partner` (`partner`),
  KEY `text_ru_auto_check` (`unique_auto_check`),
  KEY `temp_balance` (`temp_balance`),
  KEY `temp_balance_date` (`temp_balance_date`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`partner`) REFERENCES `users` (`login`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table vast
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vast`;

CREATE TABLE `vast` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country` enum('RU','EN','UA','KZ') DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `code` text,
  `active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table videodb
# ------------------------------------------------------------

DROP TABLE IF EXISTS `videodb`;

CREATE TABLE `videodb` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) unsigned DEFAULT NULL,
  `data` text,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `max_quality` int(5) DEFAULT NULL,
  `translation` varchar(255) DEFAULT NULL,
  `episode` char(10) DEFAULT NULL,
  `season` int(5) DEFAULT NULL,
  `videodb_id` int(11) unsigned DEFAULT NULL,
  `videodb_show_id` int(11) unsigned DEFAULT NULL,
  `kinopoisk_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unique_id` (`movie_id`),
  KEY `checked` (`checked`),
  KEY `date` (`date`),
  CONSTRAINT `videodb_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `cinema` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table videodb_rnd
# ------------------------------------------------------------

DROP TABLE IF EXISTS `videodb_rnd`;

CREATE TABLE `videodb_rnd` (
  `hour` int(2) unsigned NOT NULL,
  `ru` int(2) unsigned NOT NULL,
  `ua` int(2) unsigned NOT NULL,
  `other` int(2) unsigned NOT NULL,
  PRIMARY KEY (`hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table youtube_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `youtube_categories`;

CREATE TABLE `youtube_categories` (
  `id` int(11) unsigned NOT NULL,
  `data` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table youtube_sitemap
# ------------------------------------------------------------

DROP TABLE IF EXISTS `youtube_sitemap`;

CREATE TABLE `youtube_sitemap` (
  `url` varchar(500) NOT NULL DEFAULT '',
  `youtube_id` char(30) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `youtube_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `publish_date` timestamp NULL DEFAULT NULL,
  `youtube_videos_id` char(30) DEFAULT NULL,
  PRIMARY KEY (`url`),
  KEY `youtube_id` (`youtube_id`),
  KEY `youtube_deleted` (`youtube_deleted`),
  KEY `date` (`date`),
  KEY `youtube_videos_id` (`youtube_videos_id`),
  CONSTRAINT `youtube_sitemap_ibfk_1` FOREIGN KEY (`youtube_videos_id`) REFERENCES `youtube_videos` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table youtube_videos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `youtube_videos`;

CREATE TABLE `youtube_videos` (
  `id` char(30) NOT NULL DEFAULT '',
  `data` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `youtube_id` (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;