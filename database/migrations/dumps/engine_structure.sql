# ************************************************************
# Sequel Ace SQL dump
# Version 3038
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 127.0.0.1 (MySQL 5.7.33-log)
# Database: engine
# Generation Time: 2021-09-15 13:18:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

# Dump of table auth_failures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_failures`;

CREATE TABLE `auth_failures` (
  `ip` char(46) NOT NULL DEFAULT '',
  `vars` text CHARACTER SET cp1251 NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` char(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ban
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ban`;

CREATE TABLE `ban` (
  `ip` varchar(255) NOT NULL DEFAULT '',
  `user_agent` varchar(255) NOT NULL DEFAULT '',
  `referer` varchar(255) NOT NULL DEFAULT '',
  `query` varchar(255) NOT NULL DEFAULT '',
  `description` char(100) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `q` int(11) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `hide_player` tinyint(1) NOT NULL DEFAULT '0',
  `q_player` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_2` (`ip`,`user_agent`,`referer`,`query`),
  KEY `ip` (`ip`),
  KEY `user_agent` (`user_agent`),
  KEY `query` (`query`),
  KEY `referer` (`referer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ban_zerocool
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ban_zerocool`;

CREATE TABLE `ban_zerocool` (
  `ip` varchar(255) NOT NULL DEFAULT '',
  `user_agent` varchar(255) NOT NULL DEFAULT '',
  `query` varchar(255) NOT NULL DEFAULT '',
  `description` char(100) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `q` int(11) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `hide_player` tinyint(1) NOT NULL DEFAULT '0',
  `q_player` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_ua_query` (`ip`,`user_agent`,`query`),
  KEY `ip` (`ip`),
  KEY `user_agent` (`user_agent`),
  KEY `query` (`query`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cache
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cache`;

CREATE TABLE `cache` (
  `domain` char(30) NOT NULL,
  `function` char(50) NOT NULL,
  `arguments` varchar(400) NOT NULL DEFAULT '',
  `result` mediumtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reset` enum('0','1') NOT NULL DEFAULT '0',
  `time` double(5,2) DEFAULT NULL,
  PRIMARY KEY (`domain`,`function`,`arguments`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;



# Dump of table cacheObject
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cacheObject`;

CREATE TABLE `cacheObject` (
  `hash` char(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `domain` char(30) CHARACTER SET utf8 NOT NULL,
  `result` mediumtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reset` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `time` double(5,2) unsigned NOT NULL,
  `class` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `function` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `args` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `engineType` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `hashStr` text CHARACTER SET utf8,
  PRIMARY KEY (`hash`,`domain`),
  KEY `date` (`date`),
  KEY `domain` (`domain`),
  KEY `reset` (`reset`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;



# Dump of table cacheObject_copy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cacheObject_copy`;

CREATE TABLE `cacheObject_copy` (
  `hash` char(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `domain` char(30) CHARACTER SET utf8 NOT NULL,
  `result` mediumtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reset` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `time` double(5,2) unsigned NOT NULL,
  `class` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `function` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `args` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `engineType` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `hashStr` text CHARACTER SET utf8,
  PRIMARY KEY (`hash`,`domain`),
  KEY `date` (`date`),
  KEY `domain` (`domain`),
  KEY `reset` (`reset`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;



# Dump of table change_domain
# ------------------------------------------------------------

DROP TABLE IF EXISTS `change_domain`;

CREATE TABLE `change_domain` (
  `from_domain` char(30) NOT NULL DEFAULT '',
  `to_domain` char(30) NOT NULL DEFAULT '',
  `active` enum('0','1') NOT NULL DEFAULT '0',
  `for_country` enum('RU','KZ') DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`from_domain`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;



# Dump of table checker_queue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `checker_queue`;

CREATE TABLE `checker_queue` (
  `domain` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `property` char(40) NOT NULL,
  `value` char(40) DEFAULT NULL,
  `description` char(40) DEFAULT NULL,
  PRIMARY KEY (`property`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;



# Dump of table configs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `configs`;

CREATE TABLE `configs` (
  `sessionTimeout` int(6) unsigned NOT NULL COMMENT 'время жизни сессии',
  `switch` tinyint(1) unsigned NOT NULL COMMENT 'движок вкл выкл',
  `reload` int(3) unsigned NOT NULL COMMENT 'переменная {reload} для ссылок',
  `displayErrors` tinyint(1) unsigned NOT NULL COMMENT 'отображать ошибки php',
  `logLongQueries` tinyint(1) unsigned NOT NULL COMMENT 'записывать долгие запросы в лог',
  `cacheLifetime` int(6) unsigned NOT NULL COMMENT 'удалять кэш старее чем lifetime',
  `cacheSeconds` int(6) DEFAULT NULL,
  `cacheUseDB` tinyint(1) unsigned NOT NULL COMMENT 'использует ли кеш БД? на локалке омжно выключать, для тестов',
  `routerCache` tinyint(1) unsigned NOT NULL COMMENT 'кешировать роуты',
  `debug` tinyint(1) unsigned NOT NULL COMMENT 'режим отладки',
  `proxiesEnabled` tinyint(1) unsigned NOT NULL,
  `debugByGet` tinyint(1) unsigned NOT NULL COMMENT 'отладка по get ?mmnas',
  `logsLifeTime` int(6) unsigned NOT NULL COMMENT 'время жизни логов',
  `recaptchaV2ClientKey` varchar(255) DEFAULT NULL,
  `recaptchaV2ServerKey` varchar(255) DEFAULT NULL,
  `recaptchaV3ClientKey` varchar(255) DEFAULT NULL,
  `recaptchaV3ServerKey` varchar(255) DEFAULT NULL,
  `maxQueryQueue` int(2) NOT NULL,
  `cronSwitch` tinyint(1) NOT NULL,
  `smtpConfig` int(3) unsigned DEFAULT NULL,
  `proxy` varchar(100) DEFAULT NULL,
  KEY `smtpConfig` (`smtpConfig`),
  CONSTRAINT `configs_ibfk_1` FOREIGN KEY (`smtpConfig`) REFERENCES `smtp_configs` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table controllers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `controllers`;

CREATE TABLE `controllers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(30) NOT NULL DEFAULT '',
  `controller` char(30) NOT NULL DEFAULT '',
  `method` char(30) NOT NULL DEFAULT '',
  `text` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`controller`,`method`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table crons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `crons`;

CREATE TABLE `crons` (
  `action` char(30) CHARACTER SET cp1251 NOT NULL,
  `time_range` int(10) DEFAULT NULL,
  `last_time` timestamp NULL DEFAULT NULL,
  `status` enum('on','off') CHARACTER SET cp1251 NOT NULL DEFAULT 'off' COMMENT 'для движка v1',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'для движка v2',
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `run_time` float(5,2) NOT NULL,
  PRIMARY KEY (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table domains
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domains`;

CREATE TABLE `domains` (
  `domain` char(30) NOT NULL DEFAULT '',
  `metrika_id` int(11) unsigned DEFAULT NULL,
  `videoroll_id` int(11) unsigned DEFAULT NULL,
  `to_transfer` tinyint(1) DEFAULT '0',
  `to_registrar` enum('cloudflare.com','namecheap.com','ahnames.com') DEFAULT NULL,
  `code` blob COMMENT 'зашифровано через des_decrypt используя мастер пароль (в заметках маковских)',
  `transfer_reason` varchar(255) DEFAULT NULL,
  `comment` mediumtext,
  `ru_block` tinyint(1) NOT NULL DEFAULT '0',
  `mirror_of` char(30) DEFAULT NULL,
  `redirect_to` char(30) DEFAULT NULL,
  `expired` tinyint(1) NOT NULL DEFAULT '0',
  `reborn` tinyint(1) NOT NULL DEFAULT '0',
  `engine` enum('v1','v2') NOT NULL DEFAULT 'v1',
  `owner` enum('me','not') CHARACTER SET cp1251 NOT NULL DEFAULT 'me',
  `type` char(30) CHARACTER SET cp1251 DEFAULT '',
  `on` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('on','off','empty') CHARACTER SET cp1251 NOT NULL DEFAULT 'on',
  `www` enum('www_only','on','off') CHARACTER SET cp1251 NOT NULL DEFAULT 'on',
  `only_https` tinyint(1) NOT NULL DEFAULT '0',
  `charset` enum('windows-1251','utf-8') CHARACTER SET cp1251 NOT NULL DEFAULT 'windows-1251',
  `registrar` enum('ahnames.com','namecheap.com') DEFAULT NULL,
  `parent_domain` char(30) DEFAULT NULL,
  `mobile_version` enum('on','off') CHARACTER SET cp1251 NOT NULL DEFAULT 'off',
  `create_date` date DEFAULT NULL,
  `lang` enum('ru','en') CHARACTER SET cp1251 NOT NULL DEFAULT 'ru',
  `source_charset` enum('windows-1251','utf-8') CHARACTER SET cp1251 NOT NULL DEFAULT 'windows-1251',
  `rewrite_function` char(100) CHARACTER SET cp1251 DEFAULT NULL,
  `additional_path` enum('valik') CHARACTER SET cp1251 DEFAULT NULL,
  `mysql_charset` enum('cp1251','utf8') CHARACTER SET cp1251 NOT NULL DEFAULT 'cp1251',
  `auth` tinyint(1) NOT NULL DEFAULT '0',
  `type_template` char(30) DEFAULT NULL,
  `only_http` tinyint(1) NOT NULL DEFAULT '0',
  `yandex_redirect` tinyint(1) NOT NULL DEFAULT '1',
  `google_redirect` tinyint(1) NOT NULL DEFAULT '1',
  `client_redirect` tinyint(1) NOT NULL DEFAULT '1',
  `site_name` varchar(255) DEFAULT '',
  `client_redirect_to` char(30) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `favicon_src` varchar(255) DEFAULT NULL,
  `logo_src` varchar(255) DEFAULT NULL,
  `brand` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`domain`),
  UNIQUE KEY `metrika_id` (`metrika_id`),
  KEY `mirror_of` (`mirror_of`),
  KEY `redirect_to` (`redirect_to`),
  KEY `client_redirect_to` (`client_redirect_to`),
  CONSTRAINT `domains_ibfk_1` FOREIGN KEY (`mirror_of`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE,
  CONSTRAINT `domains_ibfk_2` FOREIGN KEY (`redirect_to`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE,
  CONSTRAINT `domains_ibfk_3` FOREIGN KEY (`client_redirect_to`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table domains_admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domains_admins`;

CREATE TABLE `domains_admins` (
  `domain` char(30) NOT NULL DEFAULT '',
  `admin` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`domain`,`admin`),
  KEY `admin` (`admin`),
  CONSTRAINT `domains_admins_ibfk_1` FOREIGN KEY (`admin`) REFERENCES `users` (`login`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `domains_admins_ibfk_2` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table domains_ahnames
# ------------------------------------------------------------

DROP TABLE IF EXISTS `domains_ahnames`;

CREATE TABLE `domains_ahnames` (
  `domain` char(30) NOT NULL DEFAULT '',
  `status` varchar(255) DEFAULT NULL,
  `whois_protect` varchar(255) DEFAULT NULL,
  `protection` varchar(255) DEFAULT NULL,
  `registered` varchar(255) DEFAULT NULL,
  `paid_till` varchar(255) DEFAULT NULL,
  `auto_renew` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`domain`),
  CONSTRAINT `domains_ahnames_ibfk_1` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table editor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `editor`;

CREATE TABLE `editor` (
  `login` char(30) NOT NULL DEFAULT '',
  `path` char(100) NOT NULL DEFAULT '',
  KEY `login` (`login`),
  CONSTRAINT `editor_ibfk_1` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` char(30) CHARACTER SET cp1251 NOT NULL,
  `text` text CHARACTER SET cp1251 NOT NULL,
  `query` char(200) CHARACTER SET cp1251 NOT NULL,
  `user_agent` char(200) CHARACTER SET cp1251 DEFAULT NULL,
  `referer` char(100) CHARACTER SET cp1251 DEFAULT NULL,
  `requests` text CHARACTER SET cp1251,
  `backtrace` text CHARACTER SET cp1251,
  `type` char(100) CHARACTER SET cp1251 DEFAULT NULL,
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table lumen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lumen`;

CREATE TABLE `lumen` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data` mediumtext,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `access_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table mirrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mirrors`;

CREATE TABLE `mirrors` (
  `group` char(30) NOT NULL DEFAULT '',
  `domain` char(30) NOT NULL DEFAULT '',
  UNIQUE KEY `group` (`group`,`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;



# Dump of table recaptcha
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recaptcha`;

CREATE TABLE `recaptcha` (
  `domain` char(30) NOT NULL,
  `private_key` char(100) NOT NULL,
  `public_key` char(100) NOT NULL,
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;



# Dump of table recaptcha_keys
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recaptcha_keys`;

CREATE TABLE `recaptcha_keys` (
  `name` char(10) NOT NULL DEFAULT '',
  `public` varchar(255) DEFAULT NULL,
  `private` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table redirects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redirects`;

CREATE TABLE `redirects` (
  `domain` char(30) NOT NULL DEFAULT '',
  `mirror` char(30) NOT NULL DEFAULT '',
  `type` enum('301 except yandex and ru','meta','301','301 except yandex') DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;



# Dump of table rkn_block
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rkn_block`;

CREATE TABLE `rkn_block` (
  `domain` char(30) NOT NULL DEFAULT '',
  `page` char(30) DEFAULT NULL,
  `for_operators` enum('0','1') NOT NULL DEFAULT '0',
  `for_not_ru` enum('0','1') NOT NULL DEFAULT '0',
  `active` enum('0','1') NOT NULL DEFAULT '0',
  UNIQUE KEY `domain_2` (`domain`,`page`),
  KEY `domain` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;



# Dump of table rkn_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rkn_links`;

CREATE TABLE `rkn_links` (
  `query` varchar(255) NOT NULL DEFAULT '',
  `service` enum('tnt','ркн','group-ib','webkontrol','kinopoisk','icm','sudum','isola','webkontrol','vindex','inter.ua','film.ua','vindex','starmedia','rico','dmca','starlight','aiplex','digi','прав') NOT NULL DEFAULT 'ркн',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`query`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes` (
  `pattern` char(100) NOT NULL DEFAULT '',
  `template` char(30) DEFAULT NULL,
  `cache` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'кешировать ли результат роутера',
  `alias` char(100) DEFAULT NULL,
  `controller` char(30) DEFAULT NULL,
  `method` char(30) DEFAULT NULL,
  `var1` char(30) DEFAULT NULL,
  `var2` char(30) DEFAULT NULL,
  `var3` char(30) DEFAULT NULL,
  `var4` char(30) DEFAULT NULL,
  `var5` char(30) DEFAULT NULL,
  `comment` char(100) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `keywords` varchar(300) DEFAULT NULL,
  `h1` varchar(300) DEFAULT NULL,
  `topText` text,
  `bottomText` text,
  `bread` text,
  `text1` text,
  `text2` text,
  `h2` varchar(200) DEFAULT NULL,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `redirectToAlias` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sent_mails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sent_mails`;

CREATE TABLE `sent_mails` (
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `body` text,
  `smtp_config` int(3) unsigned DEFAULT NULL,
  `result` tinyint(1) NOT NULL DEFAULT '0',
  `error_info` text,
  `hash` char(64) DEFAULT '',
  KEY `smtp_config` (`smtp_config`),
  KEY `date` (`date`),
  KEY `hash` (`hash`),
  CONSTRAINT `sent_mails_ibfk_1` FOREIGN KEY (`smtp_config`) REFERENCES `smtp_configs` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table site_routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `site_routes`;

CREATE TABLE `site_routes` (
  `domain` char(30) NOT NULL DEFAULT '',
  `pattern` char(100) NOT NULL DEFAULT '',
  `template` char(30) DEFAULT NULL,
  `cache` tinyint(1) NOT NULL DEFAULT '0',
  `alias` char(100) DEFAULT NULL,
  `controller` char(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `method` char(30) DEFAULT NULL,
  `var1` char(30) DEFAULT NULL,
  `var2` char(30) DEFAULT NULL,
  `var3` char(30) DEFAULT NULL,
  `var4` char(30) DEFAULT NULL,
  `var5` char(30) DEFAULT NULL,
  `comment` char(100) DEFAULT NULL,
  `title` text,
  `description` text,
  `keywords` varchar(300) DEFAULT NULL,
  `h1` text,
  `topText` text,
  `bottomText` text,
  `bread` text,
  `text1` text,
  `text2` text,
  `h2` text,
  `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `redirectToAlias` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`domain`,`pattern`),
  CONSTRAINT `site_routes_ibfk_1` FOREIGN KEY (`domain`) REFERENCES `domains` (`domain`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table smtp_configs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `smtp_configs`;

CREATE TABLE `smtp_configs` (
  `email` varchar(100) NOT NULL DEFAULT '',
  `host` varchar(100) NOT NULL DEFAULT '',
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `smtp_secure` varchar(100) DEFAULT '',
  `port` int(11) NOT NULL,
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table user_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_sessions`;

CREATE TABLE `user_sessions` (
  `sid` char(43) NOT NULL DEFAULT '',
  `login` char(30) NOT NULL DEFAULT '',
  `last_activity` timestamp NULL DEFAULT NULL,
  `user_agent` varchar(255) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(40) DEFAULT NULL,
  `user_id` int(3) unsigned DEFAULT NULL,
  `cookies` text,
  PRIMARY KEY (`sid`),
  KEY `login` (`login`),
  KEY `login_id` (`user_id`),
  CONSTRAINT `user_sessions_ibfk_1` FOREIGN KEY (`login`) REFERENCES `users` (`login`) ON UPDATE CASCADE,
  CONSTRAINT `user_sessions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `login` char(30) NOT NULL DEFAULT '',
  `pass` char(30) NOT NULL DEFAULT '',
  `level` char(30) NOT NULL DEFAULT '',
  `last_activ` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timezone` char(3) DEFAULT NULL,
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table visits
# ------------------------------------------------------------

DROP TABLE IF EXISTS `visits`;

CREATE TABLE `visits` (
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `host` varchar(255) NOT NULL DEFAULT '',
  `query` varchar(255) NOT NULL DEFAULT '',
  `userAgent` varchar(255) NOT NULL DEFAULT '',
  `referer` varchar(255) NOT NULL,
  `get` varchar(255) NOT NULL DEFAULT '',
  `post` varchar(255) NOT NULL DEFAULT '',
  `cookie` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  `domain` varchar(255) NOT NULL DEFAULT '',
  KEY `date` (`date`),
  KEY `ip` (`ip`),
  KEY `host` (`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table visits_memory
# ------------------------------------------------------------

DROP TABLE IF EXISTS `visits_memory`;

CREATE TABLE `visits_memory` (
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `host` varchar(255) NOT NULL DEFAULT '',
  `query` varchar(255) NOT NULL DEFAULT '',
  `userAgent` varchar(255) NOT NULL DEFAULT '',
  `referer` varchar(255) NOT NULL,
  `get` varchar(255) NOT NULL DEFAULT '',
  `post` varchar(255) NOT NULL DEFAULT '',
  `cookie` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(255) NOT NULL DEFAULT '',
  `domain` varchar(255) NOT NULL DEFAULT '',
  KEY `date` (`date`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;



# Dump of table words_filter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `words_filter`;

CREATE TABLE `words_filter` (
  `word` varchar(30) NOT NULL DEFAULT '',
  `change` varchar(30) NOT NULL DEFAULT '',
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;